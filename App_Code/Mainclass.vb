Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Web
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports GemBox.Spreadsheet


Public Class Mainclass

#Region "elements"
    Private _message As String
    Private _msgvalue As String()
    Private _spretvalue As Integer
#End Region

    Public Sub New()
        _message = ""
        _msgvalue = New String(2) {}
        _spretvalue = 0
    End Sub


#Region "Properties"
    Public Property MESSAGE() As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
        End Set
    End Property
    Public Property SPRETVALUE() As Integer
        Get
            Return _spretvalue
        End Get
        Set(ByVal value As Integer)
            _spretvalue = value
        End Set
    End Property
    Public Property MSGVALUE() As String()
        Get
            Return _msgvalue
        End Get
        Set(ByVal value As String())
            _msgvalue = value
        End Set
    End Property
#End Region

#Region "Functions"
    Public Sub doClear()
        Me.MESSAGE = ""
        Me.MSGVALUE = New String(2) {}
    End Sub


    Private Function getSqlparams(ByVal _params As String(,)) As SqlParameter()
        Dim _Sqlparam As SqlParameter() = New SqlParameter(_params.GetLength(0)) {}
        Dim j As Integer = 0
        For i As Integer = 0 To _params.GetLength(0) - 1
            If _params(i, 0) IsNot Nothing Then

                _Sqlparam(j) = New SqlParameter(_params(i, 0), _params(i, 1))
                If _params(i, 1) = "" Then
                    _Sqlparam(j).Value = DBNull.Value
                End If

                j += 1
            End If
        Next
        Return _Sqlparam
    End Function

    Public Function doExcutive(ByVal _spName As String, ByVal _params As String(,), ByVal _dbName As String, ByVal retParaname As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings(_dbName).ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim Insert As Int32
        Insert = 0
        Dim _command As New SqlCommand(_spName, objConn, stTrans)
        _command.CommandType = CommandType.StoredProcedure
        Try
            Dim _Sqlparam As SqlParameter() = New SqlParameter(_params.GetLength(0)) {}
            Dim j As Integer = 0
            For i As Integer = 0 To _params.GetLength(0) - 1
                If _params(i, 0) IsNot Nothing Then

                    _Sqlparam(j) = New SqlParameter(_params(i, 0), _params(i, 1))
                    If _params(i, 1) = "" Then
                        _Sqlparam(j).Value = DBNull.Value
                    End If
                    _command.Parameters.Add(_Sqlparam(j))
                    j += 1
                End If
            Next

            Dim retParam As SqlParameter = _command.Parameters.Add(retParaname, SqlDbType.VarChar, 1000)
            retParam.Direction = ParameterDirection.Output

            Dim retValue As SqlParameter = _command.Parameters.Add("@Return_Value", SqlDbType.BigInt)
            retValue.Direction = ParameterDirection.ReturnValue
            _command.ExecuteScalar()
            SPRETVALUE = retValue.Value
            If (SPRETVALUE.Equals(0)) Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
            MESSAGE = retParam.Value.ToString()
            If MESSAGE = "" Then
                MESSAGE = UtilityObj.getErrorMessage(SPRETVALUE)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Throw ex
        Finally
            objConn.Close()
            _command.Connection.Close()
            _command.Connection.Dispose()
        End Try
        Return MESSAGE
    End Function
    Public Function doExcutiveRetvalue(ByVal _spName As String, ByVal _params As String(,), ByVal _dbName As String, ByVal _output As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings(_dbName).ConnectionString

        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim Insert As Int32
        Insert = 0
        Dim _command As New SqlCommand(_spName, objConn, stTrans)
        _command.CommandType = CommandType.StoredProcedure
        _command.CommandTimeout = 0
        Try
            Dim _Sqlparam As SqlParameter() = New SqlParameter(_params.GetLength(0)) {}
            Dim j As Integer = 0
            For i As Integer = 0 To _params.GetLength(0) - 1
                If _params(i, 0) IsNot Nothing Then
                    _Sqlparam(j) = New SqlParameter(_params(i, 0), _params(i, 1))
                    If _params(i, 1) = "" Then
                        _Sqlparam(j).Value = DBNull.Value
                    End If
                    _command.Parameters.Add(_Sqlparam(j))
                    j += 1
                End If
            Next
            Dim retMsgparam As SqlParameter = _command.Parameters.Add("@v_ReturnMsg", SqlDbType.NVarChar, 500)
            retMsgparam.Direction = ParameterDirection.Output

            Dim retParam As SqlParameter = _command.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            retParam.Direction = ParameterDirection.ReturnValue

            If _output <> "" Then
                Dim OutputParam As SqlParameter = _command.Parameters.Add(_output, SqlDbType.VarChar, 100)
                OutputParam.Direction = ParameterDirection.Output
            End If
            _command.ExecuteNonQuery()

            SPRETVALUE = CInt(retParam.Value)
            MESSAGE = retMsgparam.Value.ToString()
            If (SPRETVALUE.Equals(0)) Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Throw ex
        Finally
            objConn.Close()
            _command.Connection.Close()
            _command.Connection.Dispose()
        End Try
        Return MESSAGE
    End Function

    Public Shared Function getConnection(ByVal _connectionString As String) As OleDbConnection
        Dim _connection As New OleDbConnection(_connectionString)
        If _connection.State = ConnectionState.Closed Then
            _connection.Open()
        End If
        Return _connection
    End Function

    Public Shared Function getConnectionODBC(ByVal _connectionString As String) As OdbcConnection
        Dim _connection As New OdbcConnection(_connectionString)
        If _connection.State = ConnectionState.Closed Then
            _connection.Open()
        End If
        Return _connection
    End Function


    Public Shared Function ReadValueFromExcel(ByVal FileName As String, ByVal SheetNo As Int16, ByVal RowNo As Int16, ByVal ColNo As Int16) As String
        Try
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            '' Dim ef As ExcelFile = New ExcelFile
            Dim ef = ExcelFile.Load(FileName)
            ' ef.Load(FileName)
            ReadValueFromExcel = ef.Worksheets(SheetNo - 1).Rows(RowNo - 1).Cells(ColNo - 1).Value
            ef = Nothing
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function CheckIfArrayRowBlank(ByVal xlsRow As ExcelRow, Optional ByVal MaxCol As Int16 = -1) As Boolean
        Dim i, colCount As Int16
        If MaxCol = -1 Then MaxCol = xlsRow.AllocatedCells.Count
        colCount = 0
        For i = 0 To MaxCol - 1
            If xlsRow.Cells(i).Value Is Nothing Then colCount += 1
        Next
        If colCount = MaxCol Then
            CheckIfArrayRowBlank = True
        Else
            CheckIfArrayRowBlank = False
        End If
    End Function

    Public Shared Function FetchFromExcelIntoDataTable(ByVal FileName As String, ByVal SheetNo As Int16, ByVal StartingRowNo As Int16, Optional ByVal MaxCol As Int16 = 100) As DataTable
        Dim iRowRead As Boolean
        iRowRead = True
        Dim iRow, iCol, i As Int32
        iRow = StartingRowNo - 1
        Dim mTable As New DataTable
        Dim mDataRow As DataRow
        Dim mObj As ExcelRowCollection
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        ' Dim ef As ExcelFile = New ExcelFile
        Dim ef = ExcelFile.Load(FileName)
        Try
            ' ef.LoadXls(FileName)
            ' Dim ef = ExcelFile.Load(FileName)
            Dim mRowObj As ExcelRow
            mObj = ef.Worksheets(SheetNo - 1).Rows
            iCol = 0
            Dim NewColName As String
            While iRowRead
                mRowObj = mObj(iRow)
                If Not CheckIfArrayRowBlank(mRowObj, MaxCol) Then
                    If StartingRowNo = iRow + 1 Then
                        While iCol <= MaxCol    ' Create the columns
                            If mRowObj.Cells(iCol).Value <> "" And Not mRowObj.Cells(iCol).Value Is Nothing Then
                                NewColName = mRowObj.Cells(iCol).Value
                                NewColName = Replace(NewColName, " ", "")
                                NewColName = Replace(NewColName, ",", "")
                                NewColName = Replace(NewColName, ".", "")
                                mTable.Columns.Add(NewColName, System.Type.GetType("System.String"))
                            End If
                            iCol += 1
                        End While
                        iRow += 1
                    Else
                        mDataRow = mTable.NewRow
                        For i = 0 To MaxCol - 1
                            If Not mRowObj.Cells(i).Value Is Nothing Then mDataRow(i) = mRowObj.Cells(i).Value
                        Next
                        mTable.Rows.Add(mDataRow)
                        iRow += 1
                    End If
                Else
                    iRowRead = False
                End If
            End While
            mTable.AcceptChanges()
            FetchFromExcelIntoDataTable = mTable
        Catch ex As Exception
        Finally
            ef = Nothing
        End Try
    End Function

    Public Shared Function FetchFromExcel(ByVal _query As String, ByVal filePath As String) As DataTable

        Dim _connectionString As String
        Dim _dt As DataTable = Nothing
        If filePath.EndsWith("xls") Then
            _connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filePath & ";Extended Properties=Excel 8.0;"
        Else

            _connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filePath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
        End If


        Dim _conn As New OleDbConnection()

        _conn = getConnection(_connectionString)
        Dim _command As New OleDbCommand()
        _command.Connection = _conn


        _dt = _conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        Dim sheetName As String = _dt.Rows(0)("TABLE_NAME").ToString()
        _query = _query.Replace("TableName", sheetName)
        _command.CommandText = _query


        Dim _adapter As New OleDbDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function

    Public Shared Function FetchFromCSV(ByVal _query As String, ByVal filePath As String) As DataTable

        Dim _connectionString As String

        _connectionString = "Driver={Microsoft Text Driver (*.txt;*.csv)}; Dbq=" & filePath & "; Extensions=asc,csv,tab,txt;Persist Security Info=False"
        '_connectionString = "Driver={Microsoft dBase Driver (*.txt;*.csv)};SourceType=CSV; SourceDB=" & filePath & ";Exclusive=No; Collate=Machine;NULL=NO;DELETED=NO;BACKGROUNDFETCH=NO;"
        Dim _conn As New OdbcConnection()

        _conn = getConnectionODBC(_connectionString)
        Dim _command As New OdbcCommand()
        _command.Connection = _conn

        _command.CommandText = _query
        'Dim _adapter As New OleDbDataAdapter(_command)

        Dim _dataTable As New DataTable()
        Try
            '_adapter.Fill(_dataTable)
            _dataTable.Load(_command.ExecuteReader)

        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function

    Public Shared Function FecthFromDBF(ByVal _query As String, ByVal filePath As String) As DataTable

        Dim _connectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & filePath & ";Extended Properties=dBase III"
        'C:\webprojects\airports
        Dim _conn As New OleDbConnection()

        _conn = getConnection(_connectionString)
        Dim _command As New OleDbCommand()
        _command.Connection = _conn

        _command.CommandText = _query
        'Dim _adapter As New OleDbDataAdapter(_command)

        Dim _dataTable As New DataTable()
        Try
            '_adapter.Fill(_dataTable)
            _dataTable.Load(_command.ExecuteReader)

        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function

    Public Shared Function FecthFromDBFodbc(ByVal _query As String, ByVal filePath As String) As DataTable

        Dim _connectionString As String = "Driver={Microsoft dBase Driver (*.dbf)};SourceType=DBF; SourceDB=" & filePath & ";Exclusive=No; Collate=Machine;NULL=NO;DELETED=NO;BACKGROUNDFETCH=NO;"
        Dim _conn As New OdbcConnection()

        _conn = getConnectionODBC(_connectionString)
        Dim _command As New OdbcCommand()
        _command.Connection = _conn

        _command.CommandText = _query
        'Dim _adapter As New OleDbDataAdapter(_command)

        Dim _dataTable As New DataTable()
        Try
            '_adapter.Fill(_dataTable)
            _dataTable.Load(_command.ExecuteReader)

        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function



    Public Function ListRecords(ByVal _spName As String, ByVal _paraName As String(,), ByVal _table As DataTable, ByVal _dbName As String) As DataTable

        _table = dbHandler.fetchRecords(_spName, _paraName, _table, _dbName)
        Return _table
    End Function
    Public Function ListRecords(ByVal _spName As String, ByVal _parameter As String(,), ByVal _dbName As String) As DataTable
        Dim _table As DataTable = Nothing
        _table = dbHandler.fetchRecords(_spName, _parameter, _dbName)
        Return _table
    End Function
    Public Function ListRecords(ByVal _spName As String, ByVal value As Int32) As DataTable
        Dim _table As DataTable = Nothing
        _table = dbHandler.fetchRecords(_spName, value)
        Return _table
    End Function
    Public Function ListRecords(ByVal Query As String, ByVal _dbName As String) As DataTable
        Dim _table As DataTable = Nothing
        _table = dbHandler.fetchRecords(Query, _dbName)
        Return _table
    End Function
    Public Function getRecords(ByVal _spName As String, ByVal _params As String(,), ByVal returnMsg As String, ByVal _dbName As String) As DataTable
        Dim _Sqlparam As SqlParameter() = New SqlParameter(_params.GetLength(0) - 1) {}
        Dim j As Integer = 0
        For i As Integer = 0 To _params.GetLength(0) - 1
            If _params(i, 0) IsNot Nothing Then
                _Sqlparam(j) = New SqlParameter(_params(i, 0), _params(i, 1))
                j += 1
            End If
        Next

        Dim _conn As New SqlConnection()
        _conn = ConnectionManager.getConnection(_dbName)
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandType = CommandType.StoredProcedure
        ' sets the type of command to stored procedure 
        _command.CommandText = _spName
        'sets command text as procedurename to be executed 
        Dim _paramsLength As Integer = _Sqlparam.Length
        For i As Integer = 0 To _paramsLength - 1
            'Add values as parameters 
            _command.Parameters.Add(_Sqlparam(i))
        Next
        _command.Parameters.Add("v_ReturnMsg", SqlDbType.VarChar, 1000)
        _command.Parameters("v_ReturnMsg").Direction = ParameterDirection.Output

        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
            MESSAGE = _command.Parameters("v_ReturnMsg").Value.ToString()

        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function
    Public Function getRecords(ByVal _spName As String, ByVal _params As String(,), ByVal _dbName As String) As DataTable
        Dim _Sqlparam As SqlParameter() = New SqlParameter(_params.GetLength(0) - 1) {}
        Dim j As Integer = 0
        For i As Integer = 0 To _params.GetLength(0) - 1
            If _params(i, 0) IsNot Nothing Then

                _Sqlparam(j) = New SqlParameter(_params(i, 0), _params(i, 1))
                If _params(i, 1).Equals("") Then
                    _Sqlparam(j).Value = DBNull.Value
                End If
                j += 1
            End If
        Next

        Dim _conn As New SqlConnection()
        _conn = ConnectionManager.getConnection(_dbName)
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandType = CommandType.StoredProcedure
        ' sets the type of command to stored procedure 
        _command.CommandText = _spName
        'sets command text as procedurename to be executed 
        Dim _paramsLength As Integer = _Sqlparam.Length
        For i As Integer = 0 To _paramsLength - 1
            'Add values as parameters 
            _command.Parameters.Add(_Sqlparam(i))
        Next

        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable

    End Function
    Public Function getRecords(ByVal _query As String, ByVal _dbName As String) As DataTable
        Dim _conn As New SqlConnection()

        _conn = ConnectionManager.getConnection(_dbName)
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function
    Public Shared Function getDataValue(ByVal _query As String, ByVal _dbName As String) As String
        Dim _conn As New SqlConnection()

        _conn = ConnectionManager.getConnection(_dbName)
        Dim _RetValue As String = ""
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
            If _dataTable.Rows.Count > 0 Then
                _RetValue = _dataTable.Rows(0).Item(0)
            Else
                _RetValue = ""
            End If

        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
            getDataValue = _RetValue
        End Try

    End Function

    Public Shared Function getDataTable(ByVal _query As String, ByVal _ConnStr As String) As DataTable
        Dim _conn As New SqlConnection(_ConnStr)
        Dim _RetTable As New DataTable
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
            _RetTable = _dataTable
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
            getDataTable = _RetTable
        End Try
    End Function
    Public Shared Function getDataTable(ByVal _query As String, ByVal _sqlParams() As SqlParameter, ByVal _ConnStr As String) As DataTable
        Dim _conn As New SqlConnection(_ConnStr)
        Dim _RetTable As New DataTable
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        _command.CommandType = CommandType.StoredProcedure
        Dim mSqlParam As SqlParameter
        For Each mSqlParam In _sqlParams
            If mSqlParam Is Nothing Then Continue For
            _command.Parameters.Add(mSqlParam)
        Next
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
            _RetTable = _dataTable
            For Each mSqlParam In _sqlParams
                If mSqlParam Is Nothing Then Continue For
                If mSqlParam.Direction = ParameterDirection.InputOutput Or mSqlParam.Direction = ParameterDirection.Output Then
                    mSqlParam.Value = _command.Parameters(mSqlParam.ParameterName).Value
                End If
            Next
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
            getDataTable = _RetTable
        End Try
    End Function
    Public Function SpellNumber(ByVal MyNumber) As String

        Dim Dirhams = Nothing, Fils = Nothing, Temp = Nothing
        Dim DecimalPlace = Nothing, Count = Nothing
        Dim Place As String() = New String(9) {}
        Place(2) = " Thousand "
        Place(3) = " Million "
        Place(4) = " Billion "
        Place(5) = " Trillion "
        ' String representation of amount.
        MyNumber = Trim(Str(MyNumber))
        ' Position of decimal place 0 if none.
        DecimalPlace = InStr(MyNumber, ".")
        ' Convert cents and set MyNumber to Dirhams amount.
        If DecimalPlace > 0 Then
            Fils = GetTens(Left(Mid(MyNumber, DecimalPlace + 1) & _
                      "00", 2))
            MyNumber = Trim(Left(MyNumber, DecimalPlace - 1))
        End If
        Count = 1
        Do While MyNumber <> ""
            Temp = GetHundreds(Right(MyNumber, 3))
            If Temp <> "" Then Dirhams = Temp & Place(Count) & Dirhams
            If Len(MyNumber) > 3 Then
                MyNumber = Left(MyNumber, Len(MyNumber) - 3)
            Else
                MyNumber = ""
            End If
            Count = Count + 1
        Loop
        Select Case Dirhams
            Case ""
                Dirhams = " "
            Case "One"
                Dirhams = "One Dirhams"
            Case Else
                Dirhams = "AED " & Dirhams
        End Select
        Select Case Fils
            Case ""
                Fils = " "
            Case "One"
                Fils = " and One Fils"
            Case Else
                Fils = " and " & Fils & " Fils"
        End Select
        SpellNumber = Dirhams & Fils
        Return SpellNumber & " Only"

    End Function

    Public Function SpellNumberWithDenomination(ByVal MyNumber As Object, ByVal Currency As String, ByVal Roundoff As Integer) As String
        Dim Denomination As String = UtilityObj.GetDataFromSQL("SELECT CUR_DENOMINATION FROM CURRENCY_M WHERE (CUR_ID = '" & Currency & "')", ConnectionManger.GetOASISFINConnectionString)
        Dim AmountText = Nothing, DenominationText = Nothing, Temp = Nothing
        Dim DecimalPlace = Nothing, Count = Nothing
        Dim Place As String() = New String(9) {}
        Place(2) = " Thousand "
        Place(3) = " Million "
        Place(4) = " Billion "
        Place(5) = " Trillion "
        ' String representation of amount.
        MyNumber = Trim(Str(MyNumber))
        ' Position of decimal place 0 if none.
        DecimalPlace = InStr(MyNumber, ".")
        ' Convert cents and set MyNumber to   amount.
        If DecimalPlace > 0 Then
            DenominationText = GetHundreds(Left(Mid(MyNumber, DecimalPlace + 1) & "00", Roundoff))
            MyNumber = Trim(Left(MyNumber, DecimalPlace - 1))
        End If
        Count = 1
        Do While MyNumber <> ""
            Temp = GetHundreds(Right(MyNumber, 3))
            If Temp <> "" Then AmountText = Temp & Place(Count) & AmountText
            If Len(MyNumber) > 3 Then
                MyNumber = Left(MyNumber, Len(MyNumber) - 3)
            Else
                MyNumber = ""
            End If
            Count = Count + 1
        Loop
        Select Case AmountText
            Case ""
                AmountText = " "
            Case Else
                AmountText = Currency & " " & AmountText
        End Select
        Select Case DenominationText
            Case ""
                DenominationText = " "
            Case Else
                DenominationText = " and " & DenominationText & " " & Denomination
        End Select

        Return AmountText & DenominationText & " Only"

    End Function

    ' Converts a number from 100-999 into text
    Function GetHundreds(ByVal MyNumber)
        Dim Result As String
        If Val(MyNumber) = 0 Then Exit Function
        MyNumber = Right("000" & MyNumber, 3)
        ' Convert the hundreds place.
        If Mid(MyNumber, 1, 1) <> "0" Then
            Result = GetDigit(Mid(MyNumber, 1, 1)) & " Hundred "
        End If
        ' Convert the tens and ones place.
        If Mid(MyNumber, 2, 1) <> "0" Then
            Result = Result & GetTens(Mid(MyNumber, 2))
        Else
            Result = Result & GetDigit(Mid(MyNumber, 3))
        End If
        GetHundreds = Result
    End Function

    ' Converts a number from 10 to 99 into text.
    Function GetTens(ByVal TensText)
        Dim Result As String
        Result = ""           ' Null out the temporary function value.
        If Val(Left(TensText, 1)) = 1 Then   ' If value between 10-19...
            Select Case Val(TensText)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else                                 ' If value between 20-99...
            Select Case Val(Left(TensText, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select
            Result = Result & GetDigit _
                (Right(TensText, 1))  ' Retrieve ones place.
        End If
        GetTens = Result
    End Function

    ' Converts a number from 1 to 9 into text.
    Function GetDigit(ByVal Digit)
        Select Case Val(Digit)
            Case 1 : GetDigit = "One"
            Case 2 : GetDigit = "Two"
            Case 3 : GetDigit = "Three"
            Case 4 : GetDigit = "Four"
            Case 5 : GetDigit = "Five"
            Case 6 : GetDigit = "Six"
            Case 7 : GetDigit = "Seven"
            Case 8 : GetDigit = "Eight"
            Case 9 : GetDigit = "Nine"
            Case Else : GetDigit = ""
        End Select
    End Function
    Public Shared Function ExecuteParamQRY(ByRef objConn As SqlConnection, ByRef stTrans As SqlTransaction, ByVal sqlQueryText As String, ByRef SqlParam() As SqlParameter) As String
        Try
            Dim cmd As New SqlCommand(sqlQueryText, objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            Dim mSqlParam As SqlParameter
            For Each mSqlParam In SqlParam
                If mSqlParam Is Nothing Then Continue For
                cmd.Parameters.Add(mSqlParam)
            Next
            cmd.ExecuteNonQuery()
            For Each mSqlParam In SqlParam
                If mSqlParam Is Nothing Then Continue For
                If mSqlParam.Direction = ParameterDirection.InputOutput Or mSqlParam.Direction = ParameterDirection.Output Then
                    mSqlParam.Value = cmd.Parameters(mSqlParam.ParameterName).Value
                End If
            Next
            ExecuteParamQRY = 0
            cmd.Parameters.Clear()
        Catch ex As Exception
            ExecuteParamQRY = -1
        End Try
    End Function

    Public Shared Function ExecuteParamQRY(ByVal ConnStr As String, ByVal sqlQueryText As String, ByRef SqlParam() As SqlParameter) As String
        Dim objConn As New SqlConnection(ConnStr)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand(sqlQueryText, objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            Dim mSqlParam As SqlParameter
            For Each mSqlParam In SqlParam
                If mSqlParam Is Nothing Then Continue For
                cmd.Parameters.Add(mSqlParam)
            Next
            cmd.ExecuteNonQuery()
            For Each mSqlParam In SqlParam
                If mSqlParam Is Nothing Then Continue For
                If mSqlParam.Direction = ParameterDirection.InputOutput Or mSqlParam.Direction = ParameterDirection.Output Then
                    mSqlParam.Value = cmd.Parameters(mSqlParam.ParameterName).Value
                End If
            Next
            ExecuteParamQRY = 0
            cmd.Parameters.Clear()
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
            ExecuteParamQRY = -1
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Shared Function CreateSqlParameter(ByVal ParamName As String, ByVal ParamValue As String, ByVal ParamType As SqlDbType, Optional ByVal IsOutPutParam As Boolean = False, Optional ByVal MaxSize As Int16 = 0) As SqlParameter
        Try
            Dim mSqlParam As New SqlParameter
            mSqlParam.Value = ParamValue
            mSqlParam.ParameterName = ParamName
            mSqlParam.SqlDbType = ParamType
            'mSqlParam.DbType = ParamType
            If MaxSize <> 0 Then
                mSqlParam.Size = MaxSize
            End If
            If IsOutPutParam Then mSqlParam.Direction = ParameterDirection.InputOutput
            CreateSqlParameter = mSqlParam
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function CreateSqlParameter(ByVal ParamName As String, ByVal ParamValue As Object, ByVal ParamType As SqlDbType, Optional ByVal IsOutPutParam As Boolean = False, Optional ByVal MaxSize As Int16 = 0) As SqlParameter
        Try
            Dim mSqlParam As New SqlParameter
            mSqlParam.Value = ParamValue
            mSqlParam.ParameterName = ParamName
            mSqlParam.SqlDbType = ParamType
            'mSqlParam.DbType = ParamType
            If MaxSize <> 0 Then
                mSqlParam.Size = MaxSize
            End If
            If IsOutPutParam Then mSqlParam.Direction = ParameterDirection.InputOutput
            CreateSqlParameter = mSqlParam
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function ConvertBytesToImage(ByVal imgByte As Byte()) As System.Drawing.Image
        Try
            Dim ms As New MemoryStream(imgByte)
            Dim returnImage As System.Drawing.Image
            returnImage = System.Drawing.Image.FromStream(ms)
            Return returnImage
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Shared Function GetBSUName(ByVal BSUID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = "SELECT  BSU_NAME  FROM BUSINESSUNIT_M where BSU_ID ='" & BSUID & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetBSUName = RetVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function GetBSUCurrency(ByVal BSUID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = "SELECT BSU_CURRENCY  FROM BUSINESSUNIT_M where BSU_ID ='" & BSUID & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetBSUCurrency = RetVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function getDataSet(ByVal _query As String, ByVal _sqlParams() As SqlParameter, ByVal _ConnStr As String) As DataSet
        Dim _conn As New SqlConnection(_ConnStr)
        Dim _RetSet As New DataSet
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        _command.CommandType = CommandType.StoredProcedure
        Dim mSqlParam As SqlParameter
        For Each mSqlParam In _sqlParams
            If mSqlParam Is Nothing Then Continue For
            _command.Parameters.Add(mSqlParam)
        Next
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataSet As New DataSet
        Try
            _adapter.Fill(_dataSet)
            _RetSet = _dataSet
            For Each mSqlParam In _sqlParams
                If mSqlParam Is Nothing Then Continue For
                If mSqlParam.Direction = ParameterDirection.InputOutput Or mSqlParam.Direction = ParameterDirection.Output Then
                    mSqlParam.Value = _command.Parameters(mSqlParam.ParameterName).Value
                End If
            Next
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
            getDataSet = _RetSet
        End Try
    End Function
    Public Shared Function SaveEmailSendStatus(ByVal BSU_ID As String, ByVal TYPE As String, ByVal EMP_ID As String, ByVal ToEmailID As String, ByVal Email_Subject As String, ByVal Email_Body As String, ByVal Email_Status As Int16, ByVal Email_Remarks As String, Optional ByVal APS_ID As Integer = 0) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(10) As SqlParameter  'changes done by swapna  to add aps_id
            sqlParam(0) = Mainclass.CreateSqlParameter("@EMS_ID", 0, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@EMS_BSU_ID", BSU_ID, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@EMS_TYPE", TYPE, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@EMS_EMP_ID", EMP_ID, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@EMS_TOEMAIL_ID", ToEmailID, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@EMS_EMAIL_SUBJECT", Email_Subject, SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@EMS_EMAIL_BODY", Email_Body, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@EMS_STATUS", Email_Status, SqlDbType.Int)
            sqlParam(8) = Mainclass.CreateSqlParameter("@EMS_REMARKS", Email_Remarks, SqlDbType.VarChar)
            sqlParam(9) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            sqlParam(10) = CreateSqlParameter("@EMS_APS_ID", APS_ID, SqlDbType.VarChar)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[SaveEmailSendStatus]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(9).Value = "" Then
                SaveEmailSendStatus = ""
            Else
                SaveEmailSendStatus = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(9).Value)
            End If
        Catch ex As Exception
            SaveEmailSendStatus = ex.Message
        End Try
    End Function
    Public Shared Function cleanString(ByVal p_searchtext As String) As String
        If p_searchtext Is Nothing Then Return p_searchtext
        If (p_searchtext.Length - p_searchtext.Replace("'", "").Replace(";", "").Replace("%", "").Length) > 1 Then
            'more than 2 suspicous char's 
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            UtilityObj.operOnAudiTable("", "", "ATTACK", , , p_searchtext)
            Return ""
        Else
            Return p_searchtext.Replace("'", "''")
        End If
    End Function
    Public Shared Function GetMenuCaption(ByVal Mnu_Code As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = "select MNU_TEXT  from MENUS_M where MNU_CODE ='" & Mnu_Code & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetMenuCaption = RetVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            Dim pattern As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"  '"(.+@.+\.[a-z]+)"
            Dim expression As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(pattern)
            Return expression.IsMatch(inputEmail)
        End If
    End Function

#End Region

End Class

