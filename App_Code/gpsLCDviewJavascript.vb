Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class gpsLCDviewJavascript
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function GpsLcdDisplay(ByVal paging As Integer, ByVal bsu_id As String, ByVal SearchOptions As String) As XmlDocument
        Dim ds As DataSet
        Dim LastCount As String = "0"
        Dim Xdoc As New XmlDocument


        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@lastcount", paging)

        If bsu_id <> "900501" Then
            pParms(1) = New SqlClient.SqlParameter("@bsu_id", bsu_id)
        End If

        If SearchOptions <> "" Then

            ''Search Parameters
            Dim val As String() = SearchOptions.Split("$")
            Dim dateselected = val(0).Trim()
            Dim maxspeedFrom = val(1).Trim()
            Dim maxspeedTo = val(2).Trim()
            Dim panicalert = val(3).Trim()
            Dim geoalert = val(4).Trim()

            pParms(2) = New SqlClient.SqlParameter("@date", dateselected)
            pParms(3) = New SqlClient.SqlParameter("@MaxSpeedFrom", maxspeedFrom)
            pParms(4) = New SqlClient.SqlParameter("@MaxSpeedTo", maxspeedTo)

            If panicalert = "true" Then
                pParms(5) = New SqlClient.SqlParameter("@Panic_Alert", panicalert)
            End If

            If geoalert = "true" Then
                pParms(6) = New SqlClient.SqlParameter("@GeoFenceAlert", geoalert)
            End If


        End If

        pParms(7) = New SqlClient.SqlParameter("@option", 1)


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_LCD_DISPLAY", pParms)
        Dim strXdoc As String
        strXdoc = ds.GetXml
        Xdoc.LoadXml(strXdoc)

        Return Xdoc


    End Function


End Class
