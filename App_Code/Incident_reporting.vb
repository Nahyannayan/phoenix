﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Incident_reporting
    Private _IR_ITS_ID As Integer
    Private _IR_CTR_NAME As String
    Private _IR_ITS_SHORT As String
    Private _IR_ITS_bDATE_TIME As Boolean
    Private _IR_ITS_YESNO_TXTTYPE As String
    Private _IR_ITS_LOOKUP As String
    Private _IR_DISPLAY_TYPE As String
    Private _IR_OUT_DATA As String
    Private _IR_STATUS As String

    Private _IRS_STXT As String
    Private _IRS_MTXT As String
    Private _IRS_DDM As String
    Private _IRM_NUM As String
    Private _IRM_DT As String
    Private _IRM_RB As String
    Private _IRM_CHK As String
    Private _IRM_SCHK As String
    Private _IRM_YESNO As String
    Private _IRM_LOOKUP As String


    Public Property IRS_STXT() As String
        Get
            Return _IRS_STXT
        End Get
        Set(ByVal value As String)
            _IRS_STXT = value
        End Set
    End Property

    Public Property IRS_MTXT() As String
        Get
            Return _IRS_MTXT
        End Get
        Set(ByVal value As String)
            _IRS_MTXT = value
        End Set
    End Property
    Public Property IRS_DDM() As String
        Get
            Return _IRS_DDM
        End Get
        Set(ByVal value As String)
            _IRS_DDM = value
        End Set
    End Property
    Public Property IRM_NUM() As String
        Get
            Return _IRM_NUM
        End Get
        Set(ByVal value As String)
            _IRM_NUM = value
        End Set
    End Property

    Public Property IRM_DT() As String
        Get
            Return _IRM_DT
        End Get
        Set(ByVal value As String)
            _IRM_DT = value
        End Set
    End Property
    Public Property IRM_RB() As String
        Get
            Return _IRM_RB
        End Get
        Set(ByVal value As String)
            _IRM_RB = value
        End Set
    End Property
    Public Property IRM_CHK() As String
        Get
            Return _IRM_CHK
        End Get
        Set(ByVal value As String)
            _IRM_CHK = value
        End Set
    End Property

    Public Property IRM_SCHK() As String
        Get
            Return _IRM_SCHK
        End Get
        Set(ByVal value As String)
            _IRM_SCHK = value
        End Set
    End Property
    Public Property IRM_YESNO() As String
        Get
            Return _IRM_YESNO
        End Get
        Set(ByVal value As String)
            _IRM_YESNO = value
        End Set
    End Property

    Public Property IRM_LOOKUP() As String
        Get
            Return _IRM_LOOKUP
        End Get
        Set(ByVal value As String)
            _IRM_LOOKUP = value
        End Set
    End Property

    Public Property IR_ITS_ID() As Integer
        Get
            Return _IR_ITS_ID
        End Get
        Set(ByVal value As Integer)
            _IR_ITS_ID = value
        End Set
    End Property
    Public Property IR_CTR_NAME() As String
        Get
            Return _IR_CTR_NAME
        End Get
        Set(ByVal value As String)
            _IR_CTR_NAME = value
        End Set
    End Property

    Public Property IR_ITS_SHORT() As String
        Get
            Return _IR_ITS_SHORT
        End Get
        Set(ByVal value As String)
            _IR_ITS_SHORT = value
        End Set
    End Property
    Public Property IR_ITS_bDATE_TIME() As Boolean
        Get
            Return _IR_ITS_bDATE_TIME
        End Get
        Set(ByVal value As Boolean)
            _IR_ITS_bDATE_TIME = value
        End Set
    End Property
    Public Property IR_ITS_YESNO_TXTTYPE() As String
        Get
            Return _IR_ITS_YESNO_TXTTYPE
        End Get
        Set(ByVal value As String)
            _IR_ITS_YESNO_TXTTYPE = value
        End Set
    End Property
    Public Property IR_ITS_LOOKUP() As String
        Get
            Return _IR_ITS_LOOKUP
        End Get
        Set(ByVal value As String)
            _IR_ITS_LOOKUP = value
        End Set
    End Property

    Public Property IR_DISPLAY_TYPE() As String
        Get
            Return _IR_DISPLAY_TYPE
        End Get
        Set(ByVal value As String)
            _IR_DISPLAY_TYPE = value
        End Set
    End Property
    Public Property IR_OUT_DATA() As String
        Get
            Return _IR_OUT_DATA
        End Get
        Set(ByVal value As String)
            _IR_OUT_DATA = value
        End Set
    End Property
    Public Property IR_STATUS() As String
        Get
            Return _IR_STATUS
        End Get
        Set(ByVal value As String)
            _IR_STATUS = value
        End Set
    End Property







    Public Shared Function SAVEINCIDENT_FOLLOWUP(ByVal trans As SqlTransaction, ByVal str As String, ByVal IRM_ID As String) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASIS_REPORTINGConnection
            Try
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@str", str)
                pParms(1) = New SqlClient.SqlParameter("@IRM_ID", IRM_ID)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[TRANS].[SAVEINCIDENT_FOLLOWUP]", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value
                Return ReturnFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return -1
            End Try
        End Using
    End Function
    Public Shared Function SAVEINCIDENT_REPORTING_M(ByVal trans As SqlTransaction, ByVal IRM_BSU_ID As String, _
        ByVal IRM_CATEGORY_ID As String, ByVal IRM_CTM_ID As String, ByVal IRM_DESCRIPTION As String, _
        ByVal IRM_INCIDENT_DT As String, ByVal IRM_CREATEDDT As String, ByVal IRM_USR_ADD As String, ByRef IRM_ID As Integer) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASIS_REPORTINGConnection
            Try
                Dim pParms(15) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@IRM_BSU_ID", IRM_BSU_ID)
                pParms(1) = New SqlClient.SqlParameter("@IRM_CATEGORY_ID", IRM_CATEGORY_ID)
                pParms(2) = New SqlClient.SqlParameter("@IRM_CTM_ID", IRM_CTM_ID)
                pParms(3) = New SqlClient.SqlParameter("@IRM_DESCRIPTION", IRM_DESCRIPTION)
                pParms(4) = New SqlClient.SqlParameter("@IRM_INCIDENT_DT", IRM_INCIDENT_DT)
                pParms(5) = New SqlClient.SqlParameter("@IRM_CREATEDDT", IRM_CREATEDDT)
                pParms(6) = New SqlClient.SqlParameter("@IRM_USR_ADD", IRM_USR_ADD)
                pParms(7) = New SqlClient.SqlParameter("@IRM_ID", SqlDbType.BigInt)
                pParms(7).Direction = ParameterDirection.Output
                pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[TRANS].[SAVEINCIDENT_REPORTING_M]", pParms)
                Dim ReturnFlag As Integer = pParms(8).Value
                If ReturnFlag = 0 Then
                    IRM_ID = IIf(TypeOf (pParms(7).Value) Is DBNull, String.Empty, pParms(7).Value)
                End If

                Return ReturnFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return -1
            End Try
        End Using

    End Function

    Public Shared Function SAVEINCIDENT_REPORTING_S(ByVal trans As SqlTransaction, ByVal datamode As String, _
        ByVal CAT_ID As String, ByVal CTM_ID As String, ByVal IRM_ID As String, ByVal ITS_ID As String, _
        ByVal ITS_SHORT As String, ByVal CTR_NAME As String, ByVal DISPLAY_TYPE As String, _
        ByVal ITS_bDATE_TIME As String, ByVal ITS_LOOKUP As String, ByVal ITS_YESNO_TXTTYPE As String, _
        ByVal OUT_DATA As String, ByVal IR_STATUS As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASIS_REPORTINGConnection
            Try
                Dim pParms(15) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@datamode", datamode)
                pParms(1) = New SqlClient.SqlParameter("@CAT_ID", CAT_ID)
                pParms(2) = New SqlClient.SqlParameter("@CTM_ID", CTM_ID)


                pParms(3) = New SqlClient.SqlParameter("@IRM_ID", IRM_ID)
                pParms(4) = New SqlClient.SqlParameter("@ITS_ID", ITS_ID)
                pParms(5) = New SqlClient.SqlParameter("@ITS_SHORT", ITS_SHORT)

                pParms(6) = New SqlClient.SqlParameter("@CTR_NAME", CTR_NAME)
                pParms(7) = New SqlClient.SqlParameter("@DISPLAY_TYPE", DISPLAY_TYPE)
                pParms(8) = New SqlClient.SqlParameter("@ITS_bDATE_TIME", ITS_bDATE_TIME)


                pParms(9) = New SqlClient.SqlParameter("@ITS_LOOKUP", ITS_LOOKUP)
                pParms(10) = New SqlClient.SqlParameter("@ITS_YESNO_TXTTYPE", ITS_YESNO_TXTTYPE)
                pParms(11) = New SqlClient.SqlParameter("@OUT_DATA", OUT_DATA)
                pParms(12) = New SqlClient.SqlParameter("@IR_STATUS", IR_STATUS)

                pParms(13) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(13).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[TRANS].[SAVEINCIDENT_REPORTING_S]", pParms)
                Dim ReturnFlag As Integer = pParms(13).Value
                Return ReturnFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return -1
            End Try
        End Using
    End Function
End Class
