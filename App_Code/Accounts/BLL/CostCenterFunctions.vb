﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI

Public Class CostCenterFunctions

    Public Shared Function CreateDataTableCostCenter() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
        Dim cVoucherid As New DataColumn("VoucherId", System.Type.GetType("System.String"))
        Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
        Dim cCostCenterTypeID As New DataColumn("CostCenterTypeID", System.Type.GetType("System.String"))
        Dim cCostCenterTypeName As New DataColumn("CostCenterTypeName", System.Type.GetType("System.String"))
        Dim cMember As New DataColumn("Memberid", System.Type.GetType("System.String"))
        Dim cMemberCode As New DataColumn("MemberCode", System.Type.GetType("System.String"))
        Dim cName As New DataColumn("Name", System.Type.GetType("System.String"))
        Dim cERN_ID As New DataColumn("ERN_ID", System.Type.GetType("System.String"))
        Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
        Dim cSubMemberId As New DataColumn("SubMemberId", System.Type.GetType("System.String"))
        dtDt.Columns.Add(cId)
        dtDt.Columns.Add(cVoucherid)
        dtDt.Columns.Add(cCostCenterTypeID)
        dtDt.Columns.Add(cCostCenterTypeName)
        dtDt.Columns.Add(cCostcenter)
        dtDt.Columns.Add(cMember)
        dtDt.Columns.Add(cMemberCode)
        dtDt.Columns.Add(cName)
        dtDt.Columns.Add(cERN_ID)
        dtDt.Columns.Add(cAmount)
        dtDt.Columns.Add(cSubMemberId)
        Return dtDt
    End Function

    Public Shared Function CreateDataTable_CostAllocation() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
        Dim cAccountid As New DataColumn("CostCenterID", System.Type.GetType("System.String"))
        Dim cAccountname As New DataColumn("ASM_NAME", System.Type.GetType("System.String"))
        Dim cNarrn As New DataColumn("ASM_ID", System.Type.GetType("System.String"))
        Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
        dtDt.Columns.Add(cId)
        dtDt.Columns.Add(cAccountid)
        dtDt.Columns.Add(cAccountname)
        dtDt.Columns.Add(cNarrn)
        dtDt.Columns.Add(cAmount)
        Return dtDt
    End Function

    Public Shared Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
        ByVal p_docno As String, ByVal SUB_ID As String, ByVal sBsuid As String, ByVal F_YEAR As String, _
        ByVal BANKTRAN As String) As String
        Dim cmd As New SqlCommand 
        cmd.Dispose()
        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = SUB_ID
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = sBsuid
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = F_YEAR
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = BANKTRAN
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        Return retValParam.Value()
    End Function

    Public Shared Function SaveVOUCHER_D_S_NEW(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
            ByVal p_docno As String, ByVal p_crdr As String, ByVal p_slno As Integer, _
            ByVal p_accountid As String, ByVal SUB_ID As String, ByVal sBsuid As String, _
            ByVal F_YEAR As String, ByVal BANKTRAN As String, ByVal ERN_ID As Object, _
            ByVal Amount As String, ByVal costcenter As String, ByVal Memberid As String, _
            ByVal name As String, ByVal SubMemberid As String, ByVal docdate As String, _
            ByVal VDS_NO As Object, ByVal isEdit As Boolean, ByRef VDS_ID_NEW As String) As String

        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand("SaveVOUCHER_D_S_NEW", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        ' cmd.Parameters.AddWithValue("@GUID", GUID)
        Dim sqlpVDS_ID_NEW As New SqlParameter("@VDS_ID_NEW", SqlDbType.VarChar, 20)
        sqlpVDS_ID_NEW.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpVDS_ID_NEW)

        cmd.Parameters.AddWithValue("@VDS_SUB_ID", SUB_ID)
        cmd.Parameters.AddWithValue("@VDS_BSU_ID", sBsuid)
        cmd.Parameters.AddWithValue("@VDS_FYEAR", F_YEAR)
        cmd.Parameters.AddWithValue("@VDS_DOCTYPE", BANKTRAN)
        cmd.Parameters.AddWithValue("@VDS_DOCNO", p_docno)
        cmd.Parameters.AddWithValue("@VDS_ERN_ID", ERN_ID)
        cmd.Parameters.AddWithValue("@VDS_DOCDT", docdate)
        cmd.Parameters.AddWithValue("@VDS_ACT_ID", p_accountid)
        cmd.Parameters.AddWithValue("@VDS_SLNO", p_slno)
        cmd.Parameters.AddWithValue("@VDS_AMOUNT", Amount)
        cmd.Parameters.AddWithValue("@VDS_CCS_ID", costcenter)
        cmd.Parameters.AddWithValue("@VDS_CODE", Memberid)
        cmd.Parameters.AddWithValue("@VDS_DESCR", name)
        cmd.Parameters.AddWithValue("@VDS_DRCR", p_crdr)
        cmd.Parameters.AddWithValue("@VDS_bPOSTED", False)
        cmd.Parameters.AddWithValue("@VDS_BDELETED", False)
        cmd.Parameters.AddWithValue("@VDS_CSS_CSS_ID", SubMemberid)
        cmd.Parameters.AddWithValue("@bEdit", isEdit)
        cmd.Parameters.AddWithValue("@VDS_NO", VDS_NO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        If retValParam.Value = "0" Then
            VDS_ID_NEW = sqlpVDS_ID_NEW.Value
        End If
        Return retValParam.Value
    End Function
    ''' <summary>
    ''' new function by shakeel for the above - updated by swapna 30-oct-2012
    ''' </summary>
    
    Public Shared Function SaveJOURNAL_D_S_REALLOCATE(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
          ByVal p_docno As String, ByVal p_crdr As String, ByVal p_slno As Integer, _
          ByVal p_accountid As String, ByVal SUB_ID As String, ByVal sBsuid As String, _
          ByVal F_YEAR As String, ByVal BANKTRAN As String, ByVal ERN_ID As Object, _
          ByVal Amount As String, ByVal costcenter As String, ByVal Memberid As String, _
          ByVal name As String, ByVal SubMemberid As String, ByVal docdate As String, _
          ByVal VDS_NO As Object, ByVal isEdit As Boolean, ByRef VDS_ID_NEW As String) As String

        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand("SaveJOURNAL_D_S_REALLOCATE", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        ' cmd.Parameters.AddWithValue("@GUID", GUID)
        Dim sqlpVDS_ID_NEW As New SqlParameter("@VDS_ID_NEW", SqlDbType.VarChar, 20)
        sqlpVDS_ID_NEW.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpVDS_ID_NEW)

        cmd.Parameters.AddWithValue("@VDS_SUB_ID", SUB_ID)
        cmd.Parameters.AddWithValue("@VDS_BSU_ID", sBsuid)
        cmd.Parameters.AddWithValue("@VDS_FYEAR", F_YEAR)
        cmd.Parameters.AddWithValue("@VDS_DOCTYPE", BANKTRAN)
        cmd.Parameters.AddWithValue("@VDS_DOCNO", p_docno)
        cmd.Parameters.AddWithValue("@VDS_ERN_ID", ERN_ID)
        cmd.Parameters.AddWithValue("@VDS_DOCDT", docdate)
        cmd.Parameters.AddWithValue("@VDS_ACT_ID", p_accountid)
        cmd.Parameters.AddWithValue("@VDS_SLNO", p_slno)
        cmd.Parameters.AddWithValue("@VDS_AMOUNT", Amount)
        cmd.Parameters.AddWithValue("@VDS_CCS_ID", costcenter)
        cmd.Parameters.AddWithValue("@VDS_CODE", Memberid)
        cmd.Parameters.AddWithValue("@VDS_DESCR", name)
        cmd.Parameters.AddWithValue("@VDS_DRCR", p_crdr)
        cmd.Parameters.AddWithValue("@VDS_bPOSTED", False)
        cmd.Parameters.AddWithValue("@VDS_BDELETED", False)
        cmd.Parameters.AddWithValue("@VDS_CSS_CSS_ID", SubMemberid)
        cmd.Parameters.AddWithValue("@bEdit", isEdit)
        cmd.Parameters.AddWithValue("@VDS_NO", VDS_NO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        If retValParam.Value = "0" Then
            VDS_ID_NEW = sqlpVDS_ID_NEW.Value
        End If
        Return retValParam.Value
    End Function

    Public Shared Function SaveVOUCHER_D_SUB_ALLOC(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
        ByVal p_docno As String, ByVal p_slno As Integer, ByVal p_accountid As String, _
        ByVal VSB_ID As String, ByVal SUB_ID As String, ByVal sBsuid As String, _
        ByVal F_YEAR As String, ByVal BANKTRAN As String, ByVal VSB_DPT_ID As String, _
        ByVal Amount As String, ByVal VSB_VDS_ID As String, ByVal VSB_CODE As String) As String

        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand("SaveVOUCHER_D_SUB_ALLOC", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@VSB_ID", VSB_ID)
        cmd.Parameters.AddWithValue("@VSB_VDS_ID", VSB_VDS_ID)
        cmd.Parameters.AddWithValue("@VSB_SUB_ID", SUB_ID)
        cmd.Parameters.AddWithValue("@VSB_BSU_ID", sBsuid)
        cmd.Parameters.AddWithValue("@VSB_FYEAR", F_YEAR)
        cmd.Parameters.AddWithValue("@VSB_DOCTYPE", BANKTRAN)
        cmd.Parameters.AddWithValue("@VSB_DOCNO", p_docno)
        cmd.Parameters.AddWithValue("@VSB_ACT_ID", p_accountid)
        cmd.Parameters.AddWithValue("@VSB_SLNO", p_slno)
        cmd.Parameters.AddWithValue("@VSB_AMOUNT", Amount)
        cmd.Parameters.AddWithValue("@VSB_Bposted", False)
        cmd.Parameters.AddWithValue("@VSB_bDELETED", False)
        cmd.Parameters.AddWithValue("@VSB_DPT_ID", VSB_DPT_ID)
        cmd.Parameters.AddWithValue("@VSB_CODE", VSB_CODE)
        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        Return retValParam.Value
    End Function
    ''' <summary>
    ''' New sp by shakeel for above function- updated by swapna - 30 oct 2012
    ''' </summary>
    
    Public Shared Function SaveJOURNAL_D_SUB_REALLOCATE(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
       ByVal p_docno As String, ByVal p_slno As Integer, ByVal p_accountid As String, _
       ByVal VSB_ID As String, ByVal SUB_ID As String, ByVal sBsuid As String, _
       ByVal F_YEAR As String, ByVal BANKTRAN As String, ByVal VSB_DPT_ID As String, _
       ByVal Amount As String, ByVal VSB_VDS_ID As String, ByVal VSB_CODE As String) As String

        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand("SaveJOURNAL_D_SUB_REALLOCATE", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@VSB_ID", VSB_ID)
        cmd.Parameters.AddWithValue("@VSB_VDS_ID", VSB_VDS_ID)
        cmd.Parameters.AddWithValue("@VSB_SUB_ID", SUB_ID)
        cmd.Parameters.AddWithValue("@VSB_BSU_ID", sBsuid)
        cmd.Parameters.AddWithValue("@VSB_FYEAR", F_YEAR)
        cmd.Parameters.AddWithValue("@VSB_DOCTYPE", BANKTRAN)
        cmd.Parameters.AddWithValue("@VSB_DOCNO", p_docno)
        cmd.Parameters.AddWithValue("@VSB_ACT_ID", p_accountid)
        cmd.Parameters.AddWithValue("@VSB_SLNO", p_slno)
        cmd.Parameters.AddWithValue("@VSB_AMOUNT", Amount)
        cmd.Parameters.AddWithValue("@VSB_Bposted", False)
        cmd.Parameters.AddWithValue("@VSB_bDELETED", False)
        cmd.Parameters.AddWithValue("@VSB_DPT_ID", VSB_DPT_ID)
        cmd.Parameters.AddWithValue("@VSB_CODE", VSB_CODE)
        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)
        cmd.ExecuteNonQuery()
        cmd.Parameters.Clear()
        Return retValParam.Value
    End Function

    Public Shared Function ViewVoucherDetails(ByVal VHD_SUB_ID As String, ByVal VHD_BSU_ID As String, _
    ByVal VHD_DOCTYPE As String, ByVal VHD_DOCNO As String) As DataTable
        'exec ViewVoucherDetails @VHD_SUB_ID='007',@VHD_BSU_ID='131001',@VHD_DOCTYPE='BP',@VHD_DOCNO ='10BP-0000414'  
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = VHD_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = VHD_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(2).Value = VHD_DOCTYPE
        pParms(3) = New SqlClient.SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
        pParms(3).Value = VHD_DOCNO
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "ViewVoucherDetails", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function ViewCostCenterDetails(ByVal VHD_SUB_ID As String, ByVal VHD_BSU_ID As String, _
        ByVal VHD_DOCTYPE As String, ByVal VHD_DOCNO As String) As DataTable
        'exec ViewVoucherDetails @VHD_SUB_ID='007',@VHD_BSU_ID='131001',@VHD_DOCTYPE='BP',@VHD_DOCNO ='10BP-0000414'  
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = VHD_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = VHD_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(2).Value = VHD_DOCTYPE
        pParms(3) = New SqlClient.SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
        pParms(3).Value = VHD_DOCNO
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "ViewCostCenterDetails", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function ViewCostCenterAllocDetails(ByVal VHD_SUB_ID As String, ByVal VHD_BSU_ID As String, _
        ByVal VHD_DOCTYPE As String, ByVal VHD_DOCNO As String) As DataTable
        'exec ViewVoucherDetails @VHD_SUB_ID='007',@VHD_BSU_ID='131001',@VHD_DOCTYPE='BP',@VHD_DOCNO ='10BP-0000414'  
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = VHD_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = VHD_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(2).Value = VHD_DOCTYPE
        pParms(3) = New SqlClient.SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
        pParms(3).Value = VHD_DOCNO
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "ViewCostCenterAllocDetails", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetCostCenterDataAll(ByVal sBsuid As String, ByVal docDate As String, _
    ByVal CostOTH As String, ByVal CostCenter As String) As DataTable
        Dim str_Sql As String
        Dim str_query_header As String = GetQueryForCostCenter(CostCenter)
        str_Sql = str_query_header.Split("||")(0)
        Dim str_headers As String()
        str_headers = str_query_header.Split("|")
        str_Sql = str_Sql.Replace("###", sBsuid)
        str_Sql = str_Sql.Replace("@@@", docDate)
        Dim ds As New DataSet
        If CostCenter = CostOTH Then
            str_Sql = "SELECT * FROM" _
            & " (SELECT CCS_ID as ID , CCS_DESCR as NAME," _
            & " ' ' as DES_ID, ' ' AS BSU_ID, ' ' AS COLUMN1 ," _
            & " ' ' AS COLUMN2,' ' AS COLUMN3" _
            & " FROM COSTCENTER_S  WHERE CCS_CCT_ID <> '9999') DB where name<>''"
        End If
        str_Sql = str_Sql & " order by name "
        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Function GetQueryForCostCenter(ByVal CostCenter As String) As String
        Dim str_Sql As String = "SELECT CCS_ID ,CCS_DESCR ,CCS_QUERY, CCS_COLUMNS" _
        & " FROM COSTCENTER_S" _
        & " WHERE  CCS_CCT_ID='9999'" _
        & " AND CCS_ID='" & CostCenter & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("CCS_QUERY") & "|" & ds.Tables(0).Rows(0)("CCS_COLUMNS")
        Else
            Return ("")
        End If
    End Function

    Public Shared Function GetCostCenterData(ByVal text As String, ByVal CostCenter As String, ByVal sBsuid As String, ByVal DocDate As String, ByVal CostOTH As String) As DataTable
        Dim str_Sql As String
        Dim str_query_header As String = CostCenterFunctions.GetQueryForCostCenter(CostCenter)
        str_Sql = str_query_header.Split("||")(0)
        Dim str_headers As String()
        str_headers = str_query_header.Split("|")
        str_Sql = str_Sql.Replace("###", sBsuid)
        str_Sql = str_Sql.Replace("@@@", DocDate)
        Dim ds As New DataSet
        If CostCenter = CostOTH Then
            str_Sql = "SELECT * FROM" _
            & " (SELECT CCS_ID as ID , CCS_DESCR as NAME," _
            & " ' ' as DES_ID, ' ' AS BSU_ID, ' ' AS COLUMN1 ," _
            & " ' ' AS COLUMN2,' ' AS COLUMN3" _
            & " FROM COSTCENTER_S  WHERE CCS_CCT_ID <> '9999') DB where name<>''"
        End If
        str_Sql = str_Sql & " and name like '%" & text & "%' order by name"
        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Sub AddCostCenter(ByVal iVoucherid As Integer, ByVal sBsuid As String, _
    ByVal CostOTH As String, ByVal DocDate As String, ByRef idCostChild As Integer, ByRef dtCostChild As DataTable, _
    ByVal dtGridCostDetails As DataTable, ByRef idCostAlocation As Integer, ByRef CostAllocation As DataTable, ByVal dtGridCostAlloc As DataTable)
        ClearCurrentCostCenterPrev(iVoucherid, dtCostChild)
        For iGridTableLoop As Integer = 0 To dtGridCostDetails.Rows.Count - 1
            Dim CostCenter As String = dtGridCostDetails.Rows(iGridTableLoop)("CostCenterTypeId")
            Dim str_Sql As String = String.Empty
            Dim str_Costcenter_no As String = String.Empty
            Dim str_Costcenter_name As String = String.Empty
            If CostCenter <> CostOTH Then  'Normal Cost Center
                str_Sql = GetQueryForCostCenter(CostCenter).Split("|")(0) & ""
                str_Sql = str_Sql.Replace("###", sBsuid)
                str_Sql = str_Sql & " AND ID='" & dtGridCostDetails.Rows(iGridTableLoop)("Memberid") & "'"
                str_Sql = str_Sql.Replace("@@@", DocDate)
            Else
                str_Sql = "SELECT * FROM" _
                            & " (SELECT CCS_ID as ID , CCS_DESCR as NAME," _
                            & " ' ' as DES_ID, ' ' AS BSU_ID, ' ' AS COLUMN1 ," _
                            & " ' ' AS COLUMN2,' ' AS COLUMN3" _
                            & " FROM COSTCENTER_S  WHERE CCS_CCT_ID <> '9999') DB where name<>''"
                str_Sql = str_Sql & " AND ID='" & dtGridCostDetails.Rows(iGridTableLoop)("Memberid") & "'"
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                str_Costcenter_name = ds.Tables(0).Rows(0)("NAME") & ""
                str_Costcenter_no = ds.Tables(0).Rows(0)("ID") & ""
            End If
            Dim rDt As DataRow
            If str_Costcenter_name <> "" Then
                If Not dtGridCostAlloc Is Nothing AndAlso dtGridCostAlloc.Rows.Count > 0 Then
                    dtGridCostAlloc.DefaultView.RowFilter = "CostCenterID =  '" & dtGridCostDetails.Rows(iGridTableLoop)("ID") & "' "
                End If
                If CostCenter <> CostOTH Then
                    rDt = dtCostChild.NewRow
                    rDt("Id") = idCostChild
                    If Not dtGridCostAlloc Is Nothing And Not CostAllocation Is Nothing Then
                        AddCostCenter_Allocation(idCostChild, CostAllocation, _
                                          idCostAlocation, dtGridCostAlloc.DefaultView.ToTable)
                        idCostChild = idCostChild + 1
                    End If
                    rDt("VoucherId") = iVoucherid & ""
                    rDt("Costcenter") = CostCenter
                    rDt("CostcenterTypeName") = dtGridCostDetails.Rows(iGridTableLoop)("CostCenterTypeName")
                    rDt("Memberid") = dtGridCostDetails.Rows(iGridTableLoop)("Memberid")
                    rDt("SubMemberid") = ""
                    rDt("Name") = str_Costcenter_name
                    rDt("Amount") = dtGridCostDetails.Rows(iGridTableLoop)("Amount")
                    rDt("MemberCode") = str_Costcenter_no
                    dtCostChild.Rows.Add(rDt)
                Else
                    rDt = dtCostChild.NewRow
                    rDt("Id") = idCostChild
                    If Not dtGridCostAlloc Is Nothing Then
                        AddCostCenter_Allocation(idCostChild, CostAllocation, _
                                          idCostAlocation, dtGridCostAlloc.DefaultView.ToTable)
                        idCostChild = idCostChild + 1
                    End If
                    rDt("VoucherId") = iVoucherid & ""
                    rDt("Costcenter") = CostCenter
                    rDt("Memberid") = dtGridCostDetails.Rows(iGridTableLoop)("Memberid")
                    rDt("SubMemberid") = ""
                    rDt("Name") = str_Costcenter_name
                    rDt("Amount") = dtGridCostDetails.Rows(iGridTableLoop)("Amount")
                    rDt("MemberCode") = str_Costcenter_no
                    dtCostChild.Rows.Add(rDt)
                End If
            End If
        Next
    End Sub

    Public Shared Sub ClearCurrentCostCenterPrev(ByVal VoucherId As String, ByRef dtCostChild As DataTable)
        If dtCostChild.Rows.Count > 0 Then
            dtCostChild.DefaultView.RowFilter = "VoucherId <> '" & VoucherId & "' "
        End If
        dtCostChild = dtCostChild.DefaultView.ToTable
    End Sub

    Public Shared Sub AddCostCenter_Allocation(ByVal CostCenterID As Integer, ByRef CostAllocation As DataTable, _
    ByRef idCostAlocation As Integer, ByVal dtGridCostAlloc As DataTable)
        ClearCurrentCostCenterAllocation(CostCenterID, CostAllocation)
        For iGridTableLoop As Integer = 0 To dtGridCostAlloc.Rows.Count - 1
            Dim rDt As DataRow
            rDt = CostAllocation.NewRow
            rDt("Id") = idCostAlocation
            idCostAlocation = idCostAlocation + 1
            rDt("CostCenterID") = CostCenterID
            rDt("ASM_ID") = dtGridCostAlloc.Rows(iGridTableLoop)("ASM_ID")
            rDt("ASM_NAME") = dtGridCostAlloc.Rows(iGridTableLoop)("ASM_NAME")
            rDt("Amount") = dtGridCostAlloc.Rows(iGridTableLoop)("Amount")
            CostAllocation.Rows.Add(rDt)
        Next
    End Sub

    Public Shared Sub ClearCurrentCostCenterAllocation(ByVal costCenterId As String, ByRef CostAllocation As DataTable)
        If CostAllocation.Rows.Count > 0 Then
            CostAllocation.DefaultView.RowFilter = "CostCenterID <> '" & costCenterId & "'"
        End If
        CostAllocation = CostAllocation.DefaultView.ToTable
    End Sub

    Public Shared Sub UploadAndPopulateEmployeeDataFromExcel(ByVal dtCostGrid As DataTable, _
        ByVal dtCostAlloc As DataTable, ByVal sBSuid As String, ByVal ASM_ID As String, ByVal ASM_NAME As String, _
        ByVal AsOnDate As String, ByVal fuEmployeeData As WebControls.FileUpload, ByVal sUsr_name As String)
        'Upload here 
        If fuEmployeeData.HasFile Then
            Dim FName As String = "Online" & sUsr_name & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString()
            FName += fuEmployeeData.FileName.ToString().Substring(fuEmployeeData.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            If Not Directory.Exists(filePath & "\temp") Then
                Directory.CreateDirectory(filePath & "\temp")
            End If
            Dim FolderPath As String = filePath & "\temp\"
            filePath = filePath & "\temp\" & FName.Replace(":", "@")

            If File.Exists(filePath) Then
                File.Delete(filePath)
            End If
            fuEmployeeData.SaveAs(filePath)
            Dim excelQuery As String = ""
            excelQuery = " SELECT * FROM  [TableName]"
            Dim xltable As DataTable
            xltable = Mainclass.FetchFromExcel(excelQuery, filePath)
            If xltable.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process the request.Invalid data in excel..!")
            End If
            File.Delete(filePath)
            Dim sb As New StringBuilder()
            Dim strRootElementName As String = "EMP_DET"
            sb.Append("<Root>")
            For Each dr As DataRow In xltable.Rows
                sb.Append("<" & strRootElementName & ">")
                sb.Append(("<EMPNO>" & dr("Code").ToString() & "</EMPNO>"))
                sb.Append(("<AMOUNT>" & dr("Amount").ToString() & "</AMOUNT>"))
                sb.Append("</" & strRootElementName & ">")
            Next
            sb.Append("</Root>")

            Dim table As DataTable
            Dim parameter As String(,) = New String(2, 1) {}
            parameter(0, 0) = "@EMP_DET"
            parameter(0, 1) = sb.ToString
            parameter(1, 0) = "@EMP_BSU_ID"
            parameter(1, 1) = sBSuid
            parameter(2, 0) = "@AsOnDate"
            parameter(2, 1) = AsOnDate
            Dim MainObj As New Mainclass
            table = MainObj.getRecords("VerifyEmployee_List", parameter, "MAINDB")
            For Each mRow As DataRow In table.Rows
                ' Id, Memberid, Name, ERN_ID, Amount
                Dim IdMax As Int32 = GetMaxIDFromDataTable(dtCostGrid, "ID") + 1
                If ASM_ID <> "" Then
                    Dim rDtAlloc As DataRow
                    rDtAlloc = dtCostAlloc.NewRow
                    Dim IdMaxAlloc As Int32 = GetMaxIDFromDataTable(dtCostAlloc, "ID") + 1
                    rDtAlloc("Id") = IdMaxAlloc
                    rDtAlloc("CostCenterID") = IdMax
                    rDtAlloc("ASM_ID") = ASM_ID
                    rDtAlloc("ASM_NAME") = ASM_NAME
                    rDtAlloc("Amount") = mRow("Amount")
                    dtCostAlloc.Rows.Add(rDtAlloc)
                End If
                Dim rDt As DataRow
                rDt = dtCostGrid.NewRow
                rDt("Id") = IdMax
                rDt("Memberid") = mRow("EMP_ID")
                rDt("Name") = mRow("NAME")
                rDt("Amount") = mRow("Amount")
                rDt("ERN_ID") = ""
                dtCostGrid.Rows.Add(rDt)
            Next
        End If
    End Sub

    Public Shared Function GetMaxIDFromDataTable(ByVal dtData As DataTable, ByVal ColumnName As String, Optional ByVal FilterCondition As String = "") As Integer
        Dim maxObject As Object
        maxObject = dtData.Compute("max(" & ColumnName & ")", FilterCondition)
        If IsNumeric(maxObject) Then
            Return Convert.ToInt32(maxObject)
        Else
            Return 1
        End If
    End Function

    Public Shared Sub SetGridSessionDataForEdit(ByVal Voucherid As Integer, ByVal dtCostChild As DataTable, _
    ByVal dtCostAllocation As DataTable, ByVal dtCostGrid As DataTable, ByVal dtCostAllocGrid As DataTable)
        dtCostChild.DefaultView.RowFilter = "VoucherId ='" & Voucherid & "'"
        For Each mRow As DataRow In dtCostChild.DefaultView.ToTable.Rows
            Dim IdMax As Int32 = GetMaxIDFromDataTable(dtCostGrid, "ID") + 1
            dtCostAllocation.DefaultView.RowFilter = "CostCenterID = '" & mRow("Id") & "'"
            For Each mRowAlloc As DataRow In dtCostAllocation.DefaultView.ToTable.Rows
                Dim rDtAlloc As DataRow
                rDtAlloc = dtCostAllocGrid.NewRow
                Dim IdMaxAlloc As Int32 = GetMaxIDFromDataTable(dtCostAllocGrid, "ID") + 1
                rDtAlloc("Id") = IdMaxAlloc
                rDtAlloc("CostCenterID") = IdMax
                rDtAlloc("ASM_ID") = mRowAlloc("ASM_ID")
                rDtAlloc("ASM_NAME") = mRowAlloc("ASM_NAME")
                rDtAlloc("Amount") = mRowAlloc("Amount")
                dtCostAllocGrid.Rows.Add(rDtAlloc)
            Next
            Dim rDt As DataRow
            rDt = dtCostGrid.NewRow
            rDt("Id") = IdMax
            rDt("CostCenterTypeID") = mRow("CostCenter")
            rDt("CostCenterTypeName") = mRow("CostCenterTypeName")
            rDt("Memberid") = mRow("Memberid")
            rDt("Name") = mRow("Name")
            rDt("Amount") = mRow("Amount")
            rDt("ERN_ID") = ""
            dtCostGrid.Rows.Add(rDt)
        Next
        'dtCostGrid.AcceptChanges()
        'dtCostAllocGrid.AcceptChanges()
    End Sub

    Public Shared Function GetSumFromDataTable(ByVal dtData As DataTable, ByVal ColumnName As String, Optional ByVal FilterCondition As String = "") As Decimal
        Dim maxObject As Object
        maxObject = dtData.Compute("sum(" & ColumnName & ")", FilterCondition)
        If IsNumeric(maxObject) Then
            Return Convert.ToDecimal(maxObject)
        Else
            Return 0
        End If
    End Function

    Public Shared Function VerifyCostCenterAmount(ByVal dtGridCostDetails As DataTable, ByVal dtGridCostAlloc As DataTable, _
                                                  ByVal LineTotal As Decimal) As Boolean
        Dim TotalCostCenterAmout As Decimal = GetSumFromDataTable(dtGridCostDetails, "Amount")
        If TotalCostCenterAmout = 0 Then
            Return True
        End If
        If TotalCostCenterAmout = LineTotal Then
            For iGridTableLoop As Integer = 0 To dtGridCostDetails.Rows.Count - 1
                If Not dtGridCostAlloc Is Nothing AndAlso dtGridCostAlloc.Rows.Count > 0 Then
                    dtGridCostAlloc.DefaultView.RowFilter = "CostCenterID =  '" & dtGridCostDetails.Rows(iGridTableLoop)("ID") & "' "
                    If dtGridCostAlloc.DefaultView.ToTable.Rows.Count > 0 Then
                        Dim allocSubledger As Decimal = GetSumFromDataTable(dtGridCostAlloc.DefaultView.ToTable, "Amount")
                        If Not allocSubledger = dtGridCostDetails.Rows(iGridTableLoop)("Amount") Then
                            Return False
                        End If
                    End If
                End If
            Next
        Else
            Return False
        End If
        Return True
    End Function

    Shared Sub CostCenterTypeRadComboBox_SelectedIndexChanged_Common(ByVal source As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs, ByRef h_CostCenterType As HiddenField)
        Dim CostCenterType As HiddenField = DirectCast(DirectCast(DirectCast(source, RadComboBox).NamingContainer, GridDataItem).FindControl("CostCenterTypeTextBox"), HiddenField)
        CostCenterType.Value = e.Text

        Dim dd As RadComboBox = DirectCast(DirectCast(DirectCast(source, RadComboBox).NamingContainer, GridDataItem).FindControl("CostCenterTypeRadComboBox"), RadComboBox)
        If Not dd Is Nothing Then
            h_CostCenterType.Value = dd.Items.FindItemByValue(dd.Items(dd.SelectedIndex).Value).Value
            Dim ddMember As RadComboBox = DirectCast(DirectCast(DirectCast(source, RadComboBox).NamingContainer, GridDataItem).FindControl("MemberidRadComboBox"), RadComboBox)

            ddMember.DataBind()
            ddMember.ClearSelection()


            Dim NameTextBox As HiddenField = DirectCast(DirectCast(DirectCast(source, RadComboBox).NamingContainer, GridDataItem).FindControl("NameTextBox"), HiddenField)
            Dim MemberidTextBox As HiddenField = DirectCast(DirectCast(DirectCast(source, RadComboBox).NamingContainer, GridDataItem).FindControl("MemberidTextBox"), HiddenField)

            If ddMember.Items.Count > 0 Then
                ddMember.SelectedIndex = 0
                If Not ddMember.Items.FindItemByValue(ddMember.Items(ddMember.SelectedIndex).Value) Is Nothing Then
                    ddMember.Items.FindItemByValue(ddMember.Items(ddMember.SelectedIndex).Value).Selected = True
                    NameTextBox.Value = ddMember.Items.FindItemByValue(ddMember.Items(ddMember.SelectedIndex).Value).Text
                    MemberidTextBox.Value = ddMember.Items(ddMember.SelectedIndex).Value
                End If
            Else
                ddMember.Text = ""
                NameTextBox.Value = ""
                MemberidTextBox.Value = ""
            End If
        End If
    End Sub

    Shared Sub MemberidRadComboBox_SelectedIndexChanged_Common(ByVal source As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        Dim NameTextBox As HiddenField = DirectCast(DirectCast(DirectCast(source, RadComboBox).NamingContainer, GridDataItem).FindControl("NameTextBox"), HiddenField)
        NameTextBox.Value = e.Text
        Dim MemberidTextBox As HiddenField = DirectCast(DirectCast(DirectCast(source, RadComboBox).NamingContainer, GridDataItem).FindControl("MemberidTextBox"), HiddenField)
        MemberidTextBox.Value = e.Value
    End Sub

    Shared Sub MemberidRadComboBox_DataBound_Common(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim NameTextBox As HiddenField = DirectCast(DirectCast(DirectCast(sender, RadComboBox).NamingContainer, GridDataItem).FindControl("NameTextBox"), HiddenField)
        Dim MemberidTextBox As HiddenField = DirectCast(DirectCast(DirectCast(sender, RadComboBox).NamingContainer, GridDataItem).FindControl("MemberidTextBox"), HiddenField)
        Dim dd As RadComboBox = sender
        If dd.SelectedIndex <> -1 Then
            If Not dd.Items.FindItemByValue(dd.Items(dd.SelectedIndex).Value) Is Nothing Then
                NameTextBox.Value = dd.Items.FindItemByValue(dd.Items(dd.SelectedIndex).Value).Text
                MemberidTextBox.Value = dd.Items(dd.SelectedIndex).Value
            End If
        End If
        dd.Filter = RadComboBoxFilter.Contains
        dd.AllowCustomText = True
    End Sub

End Class
