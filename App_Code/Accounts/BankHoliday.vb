﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj

Public Class BankHoliday
#Region "Public Variables"

    Private pBHY_ID As Integer
    Public Property BHY_ID() As Integer
        Get
            Return pBHY_ID
        End Get
        Set(ByVal value As Integer)
            pBHY_ID = value
        End Set
    End Property

    Private pBHY_CTY_ID As String
    Public Property BHY_CTY_ID() As String
        Get
            Return pBHY_CTY_ID
        End Get
        Set(ByVal value As String)
            pBHY_CTY_ID = value
        End Set
    End Property

    Private pBHY_FDT As DateTime
    Public Property BHY_FDT() As DateTime
        Get
            Return pBHY_FDT
        End Get
        Set(ByVal value As DateTime)
            pBHY_FDT = value
        End Set
    End Property

    Private pBHY_TDT As DateTime
    Public Property BHY_TDT() As DateTime
        Get
            Return pBHY_TDT
        End Get
        Set(ByVal value As DateTime)
            pBHY_TDT = value
        End Set
    End Property

    Private pBHY_COMMENTS As String
    Public Property BHY_COMMENTS() As String
        Get
            Return pBHY_COMMENTS
        End Get
        Set(ByVal value As String)
            pBHY_COMMENTS = value
        End Set
    End Property

    Private pUSER As String
    Public Property USER() As String
        Get
            Return pUSER
        End Get
        Set(ByVal value As String)
            pUSER = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function SAVE_BANK_HOLIDAYS(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        SAVE_BANK_HOLIDAYS = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("dbo.SAVE_BANKHOLIDAY_M", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBHY_ID As New SqlParameter("@BHY_ID", SqlDbType.Int)
            sqlpBHY_ID.Direction = ParameterDirection.InputOutput
            sqlpBHY_ID.Value = BHY_ID
            cmd.Parameters.Add(sqlpBHY_ID)

            Dim sqlpbDELETE As New SqlParameter("@bDELETE", SqlDbType.Bit)
            sqlpbDELETE.Value = False
            cmd.Parameters.Add(sqlpbDELETE)

            Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 50)
            sqlpUSER.Value = USER
            cmd.Parameters.Add(sqlpUSER)

            Dim sqlpBHY_CTY_ID As New SqlParameter("@BHY_CTY_ID", SqlDbType.VarChar, 5)
            sqlpBHY_CTY_ID.Value = BHY_CTY_ID
            cmd.Parameters.Add(sqlpBHY_CTY_ID)

            Dim sqlpBHY_FDT As New SqlParameter("@BHY_FDT", SqlDbType.DateTime)
            sqlpBHY_FDT.Value = BHY_FDT
            cmd.Parameters.Add(sqlpBHY_FDT)

            Dim sqlpBHY_TDT As New SqlParameter("@BHY_TDT", SqlDbType.DateTime)
            sqlpBHY_TDT.Value = BHY_TDT
            cmd.Parameters.Add(sqlpBHY_TDT)

            Dim sqlpBHY_COMMENTS As New SqlParameter("@BHY_COMMENTS", SqlDbType.VarChar, 300)
            sqlpBHY_COMMENTS.Value = BHY_COMMENTS
            cmd.Parameters.Add(sqlpBHY_COMMENTS)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            SAVE_BANK_HOLIDAYS = retValParam.Value
            If retValParam.Value = 0 Then
                BHY_ID = sqlpBHY_ID.Value
            End If
        Catch ex As Exception
            SAVE_BANK_HOLIDAYS = 1000
        End Try
    End Function

    Public Function DELETE_BANK_HOLIDAYS(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        DELETE_BANK_HOLIDAYS = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("dbo.SAVE_BANKHOLIDAY_M", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBHY_ID As New SqlParameter("@BHY_ID", SqlDbType.Int)
            sqlpBHY_ID.Direction = ParameterDirection.InputOutput
            sqlpBHY_ID.Value = BHY_ID
            cmd.Parameters.Add(sqlpBHY_ID)

            Dim sqlpbDELETE As New SqlParameter("@bDELETE", SqlDbType.Bit)
            sqlpbDELETE.Value = True
            cmd.Parameters.Add(sqlpbDELETE)

            Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 50)
            sqlpUSER.Value = USER
            cmd.Parameters.Add(sqlpUSER)

            Dim sqlpBHY_CTY_ID As New SqlParameter("@BHY_CTY_ID", SqlDbType.VarChar, 5)
            sqlpBHY_CTY_ID.Value = BHY_CTY_ID
            cmd.Parameters.Add(sqlpBHY_CTY_ID)

            Dim sqlpBHY_FDT As New SqlParameter("@BHY_FDT", SqlDbType.DateTime)
            sqlpBHY_FDT.Value = BHY_FDT
            cmd.Parameters.Add(sqlpBHY_FDT)

            Dim sqlpBHY_TDT As New SqlParameter("@BHY_TDT", SqlDbType.DateTime)
            sqlpBHY_TDT.Value = BHY_TDT
            cmd.Parameters.Add(sqlpBHY_TDT)

            Dim sqlpBHY_COMMENTS As New SqlParameter("@BHY_COMMENTS", SqlDbType.VarChar, 300)
            sqlpBHY_COMMENTS.Value = BHY_COMMENTS
            cmd.Parameters.Add(sqlpBHY_COMMENTS)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            DELETE_BANK_HOLIDAYS = retValParam.Value
            'If retValParam.Value = 0 Then
            '    BHY_ID = sqlpBHY_ID.Value
            'End If
        Catch ex As Exception
            DELETE_BANK_HOLIDAYS = 1000
        End Try
    End Function

    Public Sub LOAD_BANK_HOLIDAYS()
        Try
            Dim qry As String = "SELECT BHY_CTY_ID ,BHY_FDT ,BHY_TDT ,BHY_COMMENTS ,CTY_DESCR FROM dbo.VW_BANKHOLIDAYS_M WHERE BHY_ID=" & BHY_ID & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                BHY_CTY_ID = ds.Tables(0).Rows(0)("BHY_CTY_ID").ToString
                BHY_FDT = ds.Tables(0).Rows(0)("BHY_FDT")
                BHY_TDT = ds.Tables(0).Rows(0)("BHY_TDT")
                BHY_COMMENTS = ds.Tables(0).Rows(0)("BHY_COMMENTS").ToString
            End If
        Catch ex As Exception
            BHY_CTY_ID = "0"
            BHY_COMMENTS = ""
        End Try
    End Sub
#End Region
End Class
