﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Public Class VoucherReports
    Public Shared Function PDCVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal PrintChq As Boolean, ByVal HideCC As Boolean) As MyReportClass
        Try
            Dim str_Sql, strFilter As String
            strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCTYPE = 'BP' and VHH_DOCNO = '" & DocNo & "'"
            strFilter &= " and VHH_BSU_ID in('" & BsuId & "')"
            str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim DocDt As String
                DocDt = CDate(ds.Tables(0).Rows(0).Item("VHH_DOCDT")).ToString("dd/MMM/yyyy")
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                '"' as DETAILACCOUNT ,VHD_AMOUNT,VHD_CHQDT  from VOUCHER_D" _
                If PrintChq Then
                    repSource = AccountsReports.ChquePrint(DocNo, DocDt, BsuId, FinYear, DocType)
                    Return repSource
                Else
                    Dim formulas As New Hashtable
                    formulas("HideCC") = HideCC
                    repSource.Formulas = formulas

                    params("UserName") = HttpContext.Current.Session("sUsr_name")
                    params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, DocType, DocNo)
                    params("voucherName") = "PAYMENT VOUCHER (PDC)"
                    params("Summary") = False
                    repSource.VoucherName = "PAYMENT VOUCHER (PDC)"
                    params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                    repSource.IncludeBSUImage = True
                    Dim repSourceSubRep() As MyReportClass
                    repSourceSubRep = AccountFunctions.AddPostingDetails(DocNo, "BP", BsuId)
                    Dim subReportLength As Integer = 0
                    If repSourceSubRep Is Nothing Then
                        ReDim repSourceSubRep(0)
                        repSource.SubReport = repSourceSubRep
                    Else
                        subReportLength = repSourceSubRep.Length
                        ReDim Preserve repSourceSubRep(subReportLength)
                    End If
                    'repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, "BP", BsuId)
                    repSource.Parameter = params
                    repSource.Command = cmd

                    repSourceSubRep(subReportLength) = New MyReportClass
                    repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                    repSource.SubReport = repSourceSubRep

                    repSource.ResourceName = "../RPT_Files/PDCVoucherReport.rpt"
                    Return repSource
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function BankReceiptVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal HideCC As Boolean) As MyReportClass
        Try

            'TAX CODE
            Dim BSU_IsTAXEnabled As Boolean = False
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = BsuId
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))


            Dim str_Sql, strFilter As String
            strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCTYPE = '" & DocType & "' and VHH_DOCNO = '" & DocNo & "'"
            'strFilter &= " and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(txtdocDate.Text)) & "'"
            strFilter &= " and VHH_BSU_ID in('" & BsuId & "')"
            str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas
                params("UserName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, DocType, DocNo)
                params("voucherName") = "BANK RECEIPT VOUCHER"
                'params("voucherName") = "RECEIPT VOUCHER(BANK)"
                params("Summary") = False
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, DocType, BsuId)

                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 0
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(1)
                    repSource.SubReport = repSourceSubRep
                Else
                    subReportLength = repSourceSubRep.Length + 1
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If
                repSourceSubRep(subReportLength) = New MyReportClass
                repSourceSubRep(subReportLength - 1) = New MyReportClass
                Dim cmdNarrationDetails As New SqlCommand
                cmdNarrationDetails.CommandText = "SELECT VHH_SUB_ID, VHH_BSU_ID, VHH_FYEAR, VHH_DOCTYPE, " & _
                " VHH_DOCNO, VHD_LINEID, VHD_NARRATION FROM vw_OSA_VOUCHER where" + strFilter
                cmdNarrationDetails.Connection = New SqlConnection(str_conn)
                cmdNarrationDetails.CommandType = CommandType.Text
                repSourceSubRep(subReportLength - 1).Command = cmdNarrationDetails

                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                repSource.SubReport = repSourceSubRep

                repSource.VoucherName = "BANK RECEIPT VOUCHER"
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../RPT_Files/BankReceiptReport.rpt"
                If BSU_IsTAXEnabled = True Then
                    repSource.ResourceName = "../RPT_Files/BankReceiptTaxReport.rpt"
                End If
                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function InternetCollectionVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal HideCC As Boolean) As MyReportClass
        Try
            Dim str_Sql, strFilter As String
            strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCTYPE = '" & DocType & "' and VHH_DOCNO = '" & DocNo & "'"
            'strFilter &= " and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(txtdocDate.Text)) & "'"
            strFilter &= " and VHH_BSU_ID in('" & BsuId & "')"
            str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas
                params("UserName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, DocType, DocNo)
                params("voucherName") = "INTERNET COLLECTION VOUCHER"
                'params("voucherName") = "RECEIPT VOUCHER(BANK)"
                params("Summary") = False
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, DocType, BsuId)

                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 0
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(1)
                    repSource.SubReport = repSourceSubRep
                Else
                    subReportLength = repSourceSubRep.Length + 1
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If
                repSourceSubRep(subReportLength) = New MyReportClass
                repSourceSubRep(subReportLength - 1) = New MyReportClass
                Dim cmdNarrationDetails As New SqlCommand
                cmdNarrationDetails.CommandText = "SELECT VHH_SUB_ID, VHH_BSU_ID, VHH_FYEAR, VHH_DOCTYPE, " & _
                " VHH_DOCNO, VHD_LINEID, VHD_NARRATION FROM vw_OSA_VOUCHER where" + strFilter
                cmdNarrationDetails.Connection = New SqlConnection(str_conn)
                cmdNarrationDetails.CommandType = CommandType.Text
                repSourceSubRep(subReportLength - 1).Command = cmdNarrationDetails

                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                repSource.SubReport = repSourceSubRep

                repSource.VoucherName = "INTERNET COLLECTION VOUCHER"
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../RPT_Files/BankReceiptReport.rpt"
                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function BankPaymentVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal PrintChq As Boolean, ByVal HideCC As Boolean) As MyReportClass
        Try
            'TAX CODE
            Dim BSU_IsTAXEnabled As Boolean = False
            Dim BSU_IsOnDAX As Boolean = False
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = BsuId
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))
            BSU_IsOnDAX = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsOnDAX"))


            Dim str_Sql, strFilter As String
            If BSU_IsOnDAX Then
                strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCTYPE = '" & DocType & "' and VHH_DOCNO = '" & DocNo & "'"
                strFilter &= " and VHH_BSU_ID in('" & BsuId & "')"
                str_Sql = "SELECT * FROM OASIS_DAX.FIN.vw_OSA_VOUCHER where" + strFilter
                str_Sql = "SELECT [VHH_SUB_ID],[VHH_BSU_ID],[VHH_FYEAR],[VHH_DOCTYPE],[VHH_DOCNO],[VHH_TYPE],[VHH_DOCDT],[VHH_ACT_ID],[HeaderAccount] " & _
          ",[VHH_PARTY_ACT_ID],[VHD_LINEID],[VHD_ACT_ID],[DETAILACCOUNT],[VHD_AMOUNT],[VHD_NARRATION],[VHD_CHQNO],[VHD_CHQDT] " & _
          ",[VHH_CUR_ID],[VHH_EXGRATE1],[VHH_EXGRATE2],[VHH_NARRATION],[VHH_bDELETED],[VHH_bPOSTED],[VHH_bPDC],[BSU_NAME] " & _
          ",[BSU_IMAGE],[CUR_DESCR],[CUR_DENOMINATION],[VHD_INTEREST],[VHH_INT_ACT_ID],[VHH_ACRU_INT_ACT_ID],[VHH_CHQ_pdc_ACT_ID] " & _
          ",[VHH_PROV_ACT_ID],[VHH_COL_ACT_ID],[VHH_CHQDT],[GUID],[INT_ACT_NAME],[CHQ_ACT_NAME],[ACRU_ACT_NAME],[PROV_ACT_NAME] " & _
          ",[RSS_DESCR],[CRI_DESCR],[H_COL_DESCR],[D_COL_DESCR],[VHH_REFNO],[VHH_RECEIVEDBY],[VHD_ACT_BANKCASH],[VHH_INTPERCT] " & _
          ",[VHD_OPBAL],[VHD_CHQID],[ACT_FLAG],[VHH_BANKCHARGE],[VHH_BBearer],[VHD_bCheque],  " & _
      " [VHH_COL_ID] FROM OASIS_DAX.FIN.[vw_OSA_VOUCHER] where" + strFilter

            Else
                strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCTYPE = '" & DocType & "' and VHH_DOCNO = '" & DocNo & "'"
                strFilter &= " and VHH_BSU_ID in('" & BsuId & "')"
                str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
                str_Sql = "SELECT [VHH_SUB_ID],[VHH_BSU_ID],[VHH_FYEAR],[VHH_DOCTYPE],[VHH_DOCNO],[VHH_TYPE],[VHH_DOCDT],[VHH_ACT_ID],[HeaderAccount] " & _
          ",[VHH_PARTY_ACT_ID],[VHD_LINEID],[VHD_ACT_ID],[DETAILACCOUNT],[VHD_AMOUNT],[VHD_NARRATION],[VHD_CHQNO],[VHD_CHQDT] " & _
          ",[VHH_CUR_ID],[VHH_EXGRATE1],[VHH_EXGRATE2],[VHH_NARRATION],[VHH_bDELETED],[VHH_bPOSTED],[VHH_bPDC],[BSU_NAME],[BSU_IsTAXEnabled] " & _
          ",[BSU_IMAGE],[CUR_DESCR],[CUR_DENOMINATION],[VHD_INTEREST],[VHH_INT_ACT_ID],[VHH_ACRU_INT_ACT_ID],[VHH_CHQ_pdc_ACT_ID] " & _
          ",[VHH_PROV_ACT_ID],[VHH_COL_ACT_ID],[VHH_CHQDT],[GUID],[INT_ACT_NAME],[CHQ_ACT_NAME],[ACRU_ACT_NAME],[PROV_ACT_NAME] " & _
          ",[RSS_DESCR],[CRI_DESCR],[H_COL_DESCR],[D_COL_DESCR],[VHH_REFNO],[VHH_RECEIVEDBY],[VHD_ACT_BANKCASH],[VHH_INTPERCT] " & _
          ",[VHD_OPBAL],[VHD_CHQID],[ACT_FLAG],[VHH_BANKCHARGE],[VHH_BBearer],[VHD_bCheque],[VHD_TAX_CODE], TAX_AMNT  ,  LINE_AMNT,  " & _
      "isnull((select top 1 case when wrk_username='ananth.patil' then 1 else 0 end " & _
      "from oasisfin..spr_d inner join oasisfin..spr_h on sph_id=spd_sph_id inner join oasisfin..workflowspr on spd_id=wrk_spd_id where spd_docno=[VHH_DOCNO] and SPH_BSU_ID=[VHH_BSU_ID] " & _
      "order by wrk_id desc),0) [VHH_COL_ID] FROM [OASISFIN].[dbo].[vw_OSA_VOUCHER] where" + strFilter
            End If
           
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas
                Dim DocDt As String
                DocDt = CDate(ds.Tables(0).Rows(0).Item("VHH_DOCDT")).ToString("dd/MMM/yyyy")
                If PrintChq Then
                    repSource = AccountsReports.ChquePrint(DocNo, DocDt, BsuId, FinYear, DocType)
                    Return repSource
                Else
                    params("UserName") = HttpContext.Current.Session("sUsr_name")
                    params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, DocType, DocNo)
                    params("voucherName") = "BANK PAYMENT VOUCHER"
                    params("Summary") = False
                    params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                    repSource.VoucherName = "PAYMENT VOUCHER(BANK)"
                    repSource.Parameter = params
                    repSource.Command = cmd
                    repSource.IncludeBSUImage = True

                    Dim repSourceSubRep() As MyReportClass
                    repSourceSubRep = AccountFunctions.AddPostingDetails(DocNo, DocType, BsuId)

                    Dim subReportLength As Integer = 0
                    If repSourceSubRep Is Nothing Then
                        ReDim repSourceSubRep(0)
                        repSource.SubReport = repSourceSubRep
                    Else
                        subReportLength = repSourceSubRep.Length + 1
                        ReDim Preserve repSourceSubRep(subReportLength)
                    End If

                    repSourceSubRep(subReportLength) = New MyReportClass
                    repSourceSubRep(subReportLength - 1) = New MyReportClass
                    Dim cmdNarrationDetails As New SqlCommand
                    cmdNarrationDetails.CommandText = "SELECT VHH_SUB_ID, VHH_BSU_ID, VHH_FYEAR, VHH_DOCTYPE, " & _
                    " VHH_DOCNO, VHD_LINEID, VHD_NARRATION FROM vw_OSA_VOUCHER where" + strFilter
                    cmdNarrationDetails.Connection = New SqlConnection(str_conn)
                    cmdNarrationDetails.CommandType = CommandType.Text
                    repSourceSubRep(subReportLength - 1).Command = cmdNarrationDetails
                    repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)

                    ReDim Preserve repSourceSubRep(subReportLength + 1)
                    repSourceSubRep(subReportLength + 1) = New MyReportClass
                    Dim cmdPostDetails As New SqlCommand
                    cmdPostDetails.CommandText = "EXEC GetPurchaseInfoForVoucher '" & BsuId & "','" & DocType & "','" & DocNo & "'   "
                    cmdPostDetails.Connection = New SqlConnection(str_conn)
                    cmdPostDetails.CommandType = CommandType.Text
                    repSourceSubRep(subReportLength + 1).Command = cmdPostDetails
                    repSource.SubReport = repSourceSubRep

                    If BSU_IsOnDAX Then
                        repSource.ResourceName = "../RPT_Files/BankPaymentReport_DAX.rpt"
                    Else
                        repSource.ResourceName = "../RPT_Files/BankPaymentReport.rpt"
                    End If


                    If BSU_IsTAXEnabled = True Then
                        If BSU_IsOnDAX Then
                            repSource.ResourceName = "../RPT_Files/BankPaymentTaxReport.rpt"
                        Else                            repSource.ResourceName = "../RPT_Files/BankPaymentTaxReport.rpt"
                        End If

                    End If
                    Return repSource
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function CashPaymentVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal HideCC As Boolean) As MyReportClass
        Try
            'TAX CODE
            Dim BSU_IsTAXEnabled As Boolean = False
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = BsuId
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))

            Dim str_Sql, strFilter As String
            strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCTYPE = '" & DocType & "' and VHH_DOCNO = '" & DocNo & "'"
            strFilter &= " and VHH_BSU_ID in('" & BsuId & "')"
            str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                params("UserName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, DocType, DocNo)
                params("voucherName") = "CASH PAYMENT VOUCHER"
                params("Summary") = False
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.VoucherName = "CASH PAYMENT VOUCHER"
                repSource.Parameter = params
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, DocType, BsuId)
                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport

                Dim subReportLength As Integer = 0
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(0)
                    repSource.SubReport = repSourceSubRep
                Else
                    subReportLength = repSourceSubRep.Length + 1
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If

                repSource.Command = cmd
                repSourceSubRep(subReportLength) = New MyReportClass
                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                repSource.SubReport = repSourceSubRep
                repSource.ResourceName = "../RPT_Files/CashPaymentVoucher.rpt"
                If BSU_IsTAXEnabled = True Then
                    repSource.ResourceName = "../RPT_Files/CashPaymentTaxVoucher.rpt"
                End If
                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
    Public Shared Function CashReceiptVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal HideCC As Boolean) As MyReportClass
        Try

            'TAX CODE
            Dim BSU_IsTAXEnabled As Boolean = False
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = BsuId
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))

            Dim str_Sql, strFilter As String
            strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCTYPE = '" & DocType & "' and VHH_DOCNO = '" & DocNo & "'"
            strFilter &= " and VHH_BSU_ID in('" & BsuId & "')"
            str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                params("UserName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, DocType, DocNo)
                params("voucherName") = "CASH RECEIPT VOUCHER"
                params("Summary") = False
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.VoucherName = "CASH RECEIPT VOUCHER"
                repSource.Parameter = params
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, DocType, BsuId)
                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                ReDim Preserve repSourceSubRep(3)
                Dim subReportLength As Integer = 0
                subReportLength = repSourceSubRep.Length

                repSourceSubRep(subReportLength - 1) = New MyReportClass
                repSourceSubRep(subReportLength - 1) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                repSource.SubReport = repSourceSubRep
                repSource.Command = cmd
                repSource.ResourceName = "../RPT_Files/CashPaymentVoucher.rpt"
                If BSU_IsTAXEnabled = True Then
                    repSource.ResourceName = "../RPT_Files/CashPaymentTaxVoucher.rpt"
                End If
                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
    Public Shared Function JournalVouchers(ByVal BSuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal docType As String, ByVal DocNo As String, ByVal HideCC As Boolean, Optional ByVal DocNoMore As String = "") As MyReportClass
        Try

            'TAX CODE
            Dim BSU_IsTAXEnabled As Boolean = False
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = BSuId
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim strFilter As String = String.Empty
            strFilter = "BSU_ID = '" & BSuId & "' and FYEAR = " & FinYear & " and DOCTYPE = '" & docType & "' and DOCNO = '" & DocNo & "'"
            str_Sql = "SELECT * FROM vw_OSA_JOURNAL where " + strFilter
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Dim repSource As New MyReportClass
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim params As New Hashtable
                params("userName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, docType, DocNo)
                params("VoucherName") = "JOURNAL VOUCHER"
                params("reportHeading") = "JOURNAL VOUCHER"
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                repSource.VoucherName = "JOURNAL VOUCHER"
                repSource.IncludeBSUImage = True

                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 1
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(1)
                    repSource.SubReport = repSourceSubRep
                Else
                    subReportLength = repSourceSubRep.Length
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If

                repSourceSubRep(subReportLength - 1) = New MyReportClass
                repSourceSubRep(subReportLength) = New MyReportClass
                Dim cmdNarrationDetails As New SqlCommand
                cmdNarrationDetails.CommandText = "SELECT SUB_ID VHH_SUB_ID, BSU_ID VHH_BSU_ID, " & _
                " FYEAR VHH_FYEAR, DOCTYPE VHH_DOCTYPE, DOCNO VHH_DOCNO, DEBIT VHD_LINEID, " & _
                " JHD_NARRATION VHD_NARRATION  FROM vw_OSA_JOURNAL where " + strFilter
                cmdNarrationDetails.Connection = New SqlConnection(str_conn)
                cmdNarrationDetails.CommandType = CommandType.Text
                repSourceSubRep(subReportLength - 1).Command = cmdNarrationDetails
                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BSuId, SubID, FinYear, docType, DocNo)
                repSource.Command = cmd
                repSource.ResourceName = "../RPT_Files/JournalVoucherReport.rpt"
                If BSU_IsTAXEnabled = True Then
                    repSource.ResourceName = "../RPT_Files/JournalVoucherTaxReport.rpt"
                End If
                Return repSource
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
    Public Shared Function RecurringJournalVouchers(ByVal BSuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal docType As String, ByVal DocNo As String, ByVal HideCC As Boolean, Optional ByVal DocNoMore As String = "") As MyReportClass
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim strFilter As String = String.Empty
            strFilter = "BSU_ID = '" & BSuId & "' and FYEAR = " & FinYear & " and DOCTYPE = '" & docType & "' and DOCNO = '" & DocNo & "'"
            str_Sql = "SELECT * FROM vw_OSA_RJOURNAL where " + strFilter
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Dim repSource As New MyReportClass
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim params As New Hashtable
                params("userName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = False
                params("VoucherName") = "Recurring Voucher"
                params("reportHeading") = "Recurring Voucher"
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                repSource.VoucherName = "JOURNAL VOUCHER"
                repSource.IncludeBSUImage = True

                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 1
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(1)
                    repSource.SubReport = repSourceSubRep
                Else
                    subReportLength = repSourceSubRep.Length
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If

                repSourceSubRep(subReportLength - 1) = New MyReportClass
                repSourceSubRep(subReportLength) = New MyReportClass
                Dim cmdNarrationDetails As New SqlCommand
                cmdNarrationDetails.CommandText = "SELECT SUB_ID VHH_SUB_ID, BSU_ID VHH_BSU_ID, " & _
                " FYEAR VHH_FYEAR, DOCTYPE VHH_DOCTYPE, DOCNO VHH_DOCNO, DEBIT VHD_LINEID, " & _
                " JHD_NARRATION VHD_NARRATION  FROM vw_OSA_JOURNAL where " + strFilter
                cmdNarrationDetails.Connection = New SqlConnection(str_conn)
                cmdNarrationDetails.CommandType = CommandType.Text
                repSourceSubRep(subReportLength - 1).Command = cmdNarrationDetails
                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BSuId, SubID, FinYear, docType, DocNo)
                repSource.Command = cmd
                repSource.ResourceName = "../RPT_Files/JournalVoucherReport.rpt"
                Return repSource
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function DebitCreditNoteVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal HideCC As Boolean) As MyReportClass
        Try

            'TAX CODE
            Dim BSU_IsTAXEnabled As Boolean = False
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = BsuId
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))


            Dim str_Sql As String = String.Empty
            Dim strFilter As String = String.Empty
            Select Case DocType
                Case "CN"
                    strFilter = " and VHH_DOCTYPE = 'CN'"
                Case "DN"
                    strFilter = " and VHH_DOCTYPE = 'DN'"
            End Select
            strFilter = " VHH_FYEAR = " & FinYear & " and VHH_DOCNO = '" & DocNo _
            & "' and VHH_BSU_ID in('" & BsuId & "')"
            str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                Dim params As New Hashtable
                params("UserName") = HttpContext.Current.Session("sUsr_name")
                Select Case DocType
                    Case "CN"
                        params("voucherName") = "CREDIT NOTE"
                        params("Summary") = False
                        params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, "CN", DocNo)
                        repSource.VoucherName = "CREDIT NOTE"
                    Case "DN"
                        params("voucherName") = "DEBIT NOTE"
                        params("Summary") = False
                        params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, "DN", DocNo)
                        repSource.VoucherName = "DEBIT NOTE"
                End Select
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, DocType, BsuId)

                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 0
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(0)
                    repSource.SubReport = repSourceSubRep
                Else
                    subReportLength = repSourceSubRep.Length + 1
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If

                repSourceSubRep(subReportLength) = New MyReportClass
                repSourceSubRep(subReportLength - 1) = New MyReportClass

                Dim cmdNarrationDetails As New SqlCommand
                cmdNarrationDetails.CommandText = "SELECT VHH_SUB_ID, VHH_BSU_ID, VHH_FYEAR, VHH_DOCTYPE, " & _
                " VHH_DOCNO, VHD_LINEID, VHD_NARRATION FROM vw_OSA_VOUCHER where" + strFilter
                cmdNarrationDetails.Connection = New SqlConnection(str_conn)
                cmdNarrationDetails.CommandType = CommandType.Text
                repSourceSubRep(subReportLength - 1).Command = cmdNarrationDetails
                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                repSource.SubReport = repSourceSubRep

                repSource.ResourceName = "../RPT_Files/CREDITDEBITReport.rpt"
                If BSU_IsTAXEnabled = True Then
                    repSource.ResourceName = "../RPT_Files/CREDITDEBITTaxReport.rpt"
                End If
                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function PurchaseJournalVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal HideCC As Boolean) As MyReportClass
        Try
            'TAX CODE
            Dim BSU_IsTAXEnabled As Boolean = False
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = BsuId
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))

            Dim strFilter As String, sqlStr As String
            strFilter = " PUH_DOCNO = '" & DocNo _
            & "' and  BSU_ID in('" & BsuId & "')"
            Dim cmd As New SqlCommand
            sqlStr = "SELECT * FROM vw_OSA_PURCHASE where " + strFilter + " ORDER BY PUD_LINENO ASC"
            sqlStr &= " select isnull(max(prf_type),'') prf_type, isnull(max(prf_no),'') prf_no, isnull(max(prf_date),'1-jan-1980') prf_date, isnull(max(prf_total-prf_adjust),0.0) prf_amt, isnull(max(grn_no),'') grn_no, "
            sqlStr &= "isnull(max(grn_date),'1-jan-1980') grn_date, isnull(sum(grn_total),0.0) grn_amt, prf_budgets PRF_ACTUAL, xbudytd PRF_BUDGET, act_name,"
            sqlStr &= "CAST((( isnull(max(PRF_GROSS),0.0) *  ISNULL(max(CESS_PERC_VALUE),0))/100) as decimal(10,3)) CESS_AMNT, CESS_TYPE CESSTYPE,(max(PRF_ALT_TOTAL)+CAST(((isnull(max(PRF_GROSS),0.0)*  ISNULL(max(CESS_PERC_VALUE),0))/100) as decimal(10,3))) AS PRF_ALT_TOTAL1  "
            sqlStr &= "from oasis_pur_inv..grn_h inner join oasis_pur_inv..prf_h on grn_prf_id=prf_id and prf_bsu_id='" & BsuId & "'  "
            sqlStr &= "cross apply getbudgets(prf_bsu_id, prf_act_id, prf_date) "
            sqlStr &= "inner join oasis_pur_inv..vw_prf_actual bu on vw_act_id=prf_act_id and bsu_id=prf_bsu_id and month(prf_date)=mth and prf_fyear=fyear and prf_dpt_id=dpt_id "
            sqlStr &= "inner join accounts_m on act_id=prf_act_id "
            sqlStr &= "LEFT OUTER JOIN oasis.tax.TAX_CODES_M on TAX_CODE=PRF_TAX "
            sqlStr &= "LEFT OUTER JOIN [OASIS].[DBO].[CESS_M]​ on [CESS_GST_CODE]=PRF_TAX  "
            sqlStr &= "where grn_pjv_no='" & DocNo & "' "
            sqlStr &= "group by prf_budgets, xbudytd, act_name,CESS_TYPE"

            cmd.CommandText = sqlStr
            cmd.CommandType = Data.CommandType.Text

            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If ds.Tables(0).Rows.Count >= 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                Dim params As New Hashtable
                params("userName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = PrinterFunctions.UpdatePrintCopy(SubID, DocType, DocNo)
                params("VoucherName") = "Purchase Report"
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                If ds.Tables(1).Rows.Count > 0 Then
                    params("Type") = IIf(ds.Tables(1).Rows(0)("PRF_No") = "F", "Framwork", "Non Framework")
                    params("PRF_No") = ds.Tables(1).Rows(0)("PRF_No")
                    params("PRF_Date") = ds.Tables(1).Rows(0)("PRF_Date")
                    params("PRF_Amt") = ds.Tables(1).Rows(0)("PRF_Amt")
                    params("GRN_No") = ds.Tables(1).Rows(0)("GRN_No")
                    params("GRN_Date") = ds.Tables(1).Rows(0)("GRN_Date")
                    params("GRN_Amt") = ds.Tables(1).Rows(0)("PRF_ALT_TOTAL1") 'ds.Tables(1).Rows(0)("GRN_Amt")
                    params("PRF_ACTNAME") = ds.Tables(1).Rows(0)("ACT_NAME")
                    params("PRF_BUDGET") = ds.Tables(1).Rows(0)("PRF_BUDGET")
                    params("PRF_ACTUAL") = ds.Tables(1).Rows(0)("PRF_ACTUAL")
                Else
                    params("Type") = "Direct"
                    params("PRF_No") = ""
                    params("PRF_Date") = Now
                    params("PRF_Amt") = 0
                    params("GRN_No") = ""
                    params("GRN_Date") = Now
                    params("GRN_Amt") = 0
                    params("PRF_ACTNAME") = ""
                    params("PRF_BUDGET") = 0
                    params("PRF_ACTUAL") = 0
                End If

                repSource.Parameter = params
                repSource.VoucherName = "Purchase Report"
                repSource.Command = cmd
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, DocType, BsuId)


                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 0
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(0)
                Else
                    subReportLength = repSourceSubRep.Length + 1
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If

                repSourceSubRep(subReportLength) = New MyReportClass
                repSourceSubRep(subReportLength - 1) = New MyReportClass

                Dim cmdNarrationDetails As New SqlCommand
                cmdNarrationDetails.CommandText = "SELECT TDS_ACT_ID ,M2.ACT_NAME TDS_ACT_NAME, PUH_PARTY_ACT_ID PARTY_ACT_ID,M1.ACT_NAME PARTY_ACT_NAME, ((PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE)+  (OASISFIN.[dbo].[GET_TAX_VALUES_FN] (ISNULL(PURCHASE_D.PUD_TAX_CODE,''),(PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE) ) +  OASISFIN.[dbo].[GET_CESS_VALUES_FN] (ISNULL(PURCHASE_D.PUD_TAX_CODE,''),'KFC',(PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE) )   )+ISNULL(PURCHASE_H.PUH_ROUND_OFF_AMOUNT,0)) AS AMOUNT, " & _
                " ((PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE)+  (OASISFIN.[dbo].[GET_TAX_VALUES_FN] (ISNULL(PURCHASE_D.PUD_TAX_CODE,''),(PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE) ) +  OASISFIN.[dbo].[GET_CESS_VALUES_FN] (ISNULL(PURCHASE_D.PUD_TAX_CODE,''),'KFC',(PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE) )   )+ISNULL(PURCHASE_H.PUH_ROUND_OFF_AMOUNT,0))- OASISFIN.[dbo].[GET_TDS_VALUES_FN] (PURCHASE_D.PUD_TDS_CODE,(OASISFIN.dbo.PURCHASE_D.PUD_QTY * OASISFIN.dbo.PURCHASE_D.PUD_RATE) ) AS PARTY_AMOUNT, " & _
                " OASISFIN.[dbo].[GET_TDS_VALUES_FN] (PURCHASE_D.PUD_TDS_CODE,(OASISFIN.dbo.PURCHASE_D.PUD_QTY * OASISFIN.dbo.PURCHASE_D.PUD_RATE) ) AS TDS_AMOUNT,PURCHASE_D.PUD_SUB_ID AS VHH_SUB_ID, PUD_BSU_ID AS VHH_BSU_ID, " & _
                " PURCHASE_D.PUD_FYEAR AS VHH_FYEAR, PURCHASE_D.PUD_DOCTYPE AS VHH_DOCTYPE, " & _
                " PURCHASE_D.PUD_DOCNO AS VHH_DOCNO, PURCHASE_D.PUD_LINENO AS VHD_LINEID, " & _
                " CASE WHEN PUH_BSU_ID='223006' THEN PURCHASE_D.PUD_ITEM +' ['+ ISNULL(TDS_ACT_ID,'')+' - '+ISNULL(M2.ACT_NAME,'') +' - INR ' + CAST(CAST(OASISFIN.[dbo].[GET_TDS_VALUES_FN] (PURCHASE_D.PUD_TDS_CODE,(OASISFIN.dbo.PURCHASE_D.PUD_QTY * OASISFIN.dbo.PURCHASE_D.PUD_RATE) )As Numeric(18,2)) AS varchar) + ']' +' ['+PUH_PARTY_ACT_ID +' - '+M1.ACT_NAME+' - INR '+  CAST(CAST(((PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE)+  (OASISFIN.[dbo].[GET_TAX_VALUES_FN] (ISNULL(PURCHASE_D.PUD_TAX_CODE,''),(PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE) ) +  OASISFIN.[dbo].[GET_CESS_VALUES_FN] (ISNULL(PURCHASE_D.PUD_TAX_CODE,''),'KFC',(PURCHASE_D.PUD_QTY * PURCHASE_D.PUD_RATE) )   )+ISNULL(PURCHASE_H.PUH_ROUND_OFF_AMOUNT,0))- OASISFIN.[dbo].[GET_TDS_VALUES_FN] (PURCHASE_D.PUD_TDS_CODE,(OASISFIN.dbo.PURCHASE_D.PUD_QTY * OASISFIN.dbo.PURCHASE_D.PUD_RATE) ) As Numeric(18,2)) As Varchar)+'] ' ELSE  PURCHASE_D.PUD_ITEM END AS VHD_NARRATION  FROM PURCHASE_D  " & _
                " INNER JOIN PURCHASE_H ON PURCHASE_D.PUD_SUB_ID = PURCHASE_H.PUH_SUB_ID " & _
                " AND PURCHASE_D.PUD_BSU_ID = PURCHASE_H.PUH_BSU_ID AND " & _
                " PURCHASE_D.PUD_FYEAR = PURCHASE_H.PUH_FYEAR AND " & _
                " PURCHASE_D.PUD_DOCTYPE = PURCHASE_H.PUH_DOCTYPE And " & _
                " PURCHASE_D.PUD_DOCNO = PURCHASE_H.PUH_DOCNO LEFT OUTER JOIN OASISFIN.dbo.VW_FIN_TDS_CODES ON PURCHASE_D.PUD_TDS_CODE=TDS_CODE " & _
                " LEFT OUTER JOIN OASISFIN..ACCOUNTS_M M1 ON PUH_PARTY_ACT_ID= M1.ACT_ID " & _
                " LEFT OUTER JOIN OASISFIN..ACCOUNTS_M M2 ON TDS_ACT_ID= M2.ACT_ID where " + strFilter.Replace("BSU_ID", "PUH_BSU_ID") + " ORDER BY PUD_LINENO ASC"
                cmdNarrationDetails.Connection = New SqlConnection(str_conn)
                cmdNarrationDetails.CommandType = CommandType.Text
                repSourceSubRep(subReportLength - 1).Command = cmdNarrationDetails
                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                repSource.SubReport = repSourceSubRep

                repSource.ResourceName = "../RPT_Files/PurchaseReport.rpt"
                If BSU_IsTAXEnabled = True Then
                    repSource.ResourceName = "../RPT_Files/PurchaseTaxReport.rpt"
                End If

                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
    Public Shared Function PrePaymentVoucher(ByVal Bsuid As String, ByVal FinYear As String, ByVal SubId As String, ByVal DocType As String, ByVal DocNo As String, ByVal GuId As String, ByVal HideCC As Boolean) As MyReportClass
        Try
            Dim viewid As String = String.Empty
            Dim str_Sql, strFilter As String
            strFilter = "WHERE  GUID = '" & GuId & "'"
            str_Sql = "SELECT * FROM vw_OSA_PREPAYMENT " + strFilter
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim repSource As New MyReportClass
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                Dim params As New Hashtable
                params("UserName") = HttpContext.Current.Session("sUsr_name")
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, DocType, Bsuid)

                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 0
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(0)
                Else
                    subReportLength = repSourceSubRep.Length
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If
                repSourceSubRep(subReportLength) = New MyReportClass
                repSourceSubRep(subReportLength) = getCostCenterTransDetail(Bsuid, SubId, FinYear, DocType, DocNo)
                repSource.SubReport = repSourceSubRep
                repSource.ResourceName = "../RPT_Files/rptPrepaymentVoucher.rpt"
                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function InterUnitVoucher(ByVal BsuId As String, ByVal FinYear As String, ByVal SubID As String, ByVal DocType As String, ByVal DocNo As String, ByVal Posted As Boolean, ByVal HideCC As Boolean) As MyReportClass
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim strFilter As String = String.Empty
            strFilter = " IJH_DOCNO = '" & DocNo & "' "
            If Posted = True Then
                strFilter = "IJH_ISS_BSU_ID='" & BsuId & "' and   IJH_DOCNO = '" & DocNo & "' "
                str_Sql = "SELECT * FROM vw_OSA_IJOURNAL_POSTED where  " + strFilter
            Else
                strFilter = " IJH_DOCNO = '" & DocNo & "' "
                str_Sql = "SELECT * FROM vw_OSA_IJOURNAL where " + strFilter
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                Dim formulas As New Hashtable
                formulas("HideCC") = HideCC
                repSource.Formulas = formulas

                params("userName") = HttpContext.Current.Session("sUsr_name")
                'params("Summary") = chkSummary.Checked
                params("VoucherName") = "InterUnit Journal Voucher"
                params("reportHeading") = "InterUnit Journal Voucher"
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                repSource.VoucherName = "InterUnit Journal Voucher"
                repSource.Command = cmd
                repSource.IncludeBSUImage = True
                repSource.SubReport = AccountFunctions.AddPostingDetails(DocNo, "IJV", BsuId)
                Dim repSourceSubRep() As MyReportClass
                repSourceSubRep = repSource.SubReport
                Dim subReportLength As Integer = 0
                If repSource.SubReport Is Nothing Then
                    ReDim repSourceSubRep(0)
                Else
                    subReportLength = repSourceSubRep.Length
                    ReDim Preserve repSourceSubRep(subReportLength)
                End If
                repSourceSubRep(subReportLength) = New MyReportClass
                repSourceSubRep(subReportLength) = getCostCenterTransDetail(BsuId, SubID, FinYear, DocType, DocNo)
                If Posted = True Then
                    repSource.ResourceName = "../RPT_Files/rptIJVReportOpen.rpt"
                Else
                    repSource.ResourceName = "../RPT_Files/rptIJVReportOpen.rpt"
                End If
                repSource.SubReport = repSourceSubRep
                Return repSource
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Public Shared Function getCostCenterTransDetail(ByVal Bsuid As String, ByVal SubId As String, ByVal FinYear As String, ByVal DocType As String, ByVal DocNo As String) As MyReportClass
        Try
            Dim SubRepCostCenter As New MyReportClass
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim cmdCostCenter As New SqlCommand
            Dim Connection As New SqlConnection(str_conn)
            Dim param(3) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@VHD_BSU_ID", Bsuid, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(0))
            param(1) = Mainclass.CreateSqlParameter("@VHD_SUB_ID", SubId, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(1))
            param(2) = Mainclass.CreateSqlParameter("@VHD_DOCTYPE", DocType, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(2))
            param(3) = Mainclass.CreateSqlParameter("@VHD_DOCNO", DocNo, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(3))
            cmdCostCenter.CommandText = "GetCostCenterTransDetails"
            cmdCostCenter.CommandType = CommandType.StoredProcedure
            cmdCostCenter.Connection = Connection
            SubRepCostCenter.Command = cmdCostCenter
            getCostCenterTransDetail = SubRepCostCenter
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
End Class
