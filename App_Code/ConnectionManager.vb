Imports Microsoft.VisualBasic
Imports System
Imports System.Data
'using Oracle.DataAccess.Client; 
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections.Specialized
Imports System.Collections.ObjectModel
Imports System.Collections
Imports System.Text

Public Class ConnectionManager
    Public Sub New()
        ' 
        ' TODO: Add constructor logic here 
        ' 
    End Sub

    'Public Shared Function getConnection(ByVal _connectionString As String) As SqlConnection
    '    '_connectionString = ""; 
    '    Dim _connection As New SqlConnection(_connectionString)
    '    _connection.Open()
    '    Return _connection'MainDB
    'End Function

    Public Shared Function getConnection(ByVal _nameString As String) As SqlConnection

        Dim ConnectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
        'NameValueCollection appSettings =ConfigurationManager.AppSettings; 
        Dim _connectionString As String = ConnectionStrings(_nameString).ConnectionString
        Dim _connection As New SqlConnection(_connectionString)
        _connection.Open()
        Return _connection
    End Function
    Public Shared Function getTransaction(ByVal _nameString As String) As IDbTransaction
        Return getConnection(_nameString).BeginTransaction()
    End Function
End Class

