Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Transaction
    Public Shared Function AccessControl(ByVal formName As String, ByVal UserRoleID As Integer) As Integer
        Dim sqlString As String = "select ur.operationId as OperationID from FormRight as fr join UserRights as ur on ur.formid=fr.formid where ur.roleid='" & UserRoleID & "' and fr.formname= '" & formName & "'"

        Dim result As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
        End Using

        Return result


    End Function

    Public Shared Sub InsertRecordsFormRights(ByVal RoleID As Integer, ByVal FormID As Integer, ByVal OperationID As Integer)

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand("InsertFormRight", connection)
            command.CommandType = CommandType.StoredProcedure

            command.Parameters.Add("@RoleID", SqlDbType.Int).Value = RoleID
            command.Parameters.Add("@FormID", SqlDbType.Int).Value = FormID
            command.Parameters.Add("@OperationID", SqlDbType.Int).Value = OperationID


            command.ExecuteNonQuery()

        End Using

    End Sub

    Public Shared Sub GridInsertRecordsFormRights(ByVal RoleID As Integer, ByVal FormID As Integer, ByVal OperationID As Integer, ByVal formtext As String, ByVal ParentID As Integer, ByVal Description As String)



        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand("InsertFormRight", connection)
            command.CommandType = CommandType.StoredProcedure

            command.Parameters.Add("@RoleID", SqlDbType.Int).Value = RoleID


            command.Parameters.Add("@FormID", SqlDbType.Int).Value = FormID


            command.Parameters.Add("@OperationID", SqlDbType.Int).Value = CType(Description, Integer)


            command.ExecuteNonQuery()

        End Using
    End Sub

    Public Shared Function GetUserInfo() As SqlDataReader

        Dim sqlUright As String = "select RoleId,FormId,OperationId from userRights"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlUright, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function DisplayEditGrid() As SqlDataReader
        'Dim sqlDGrid As String = "select userRights.formId as FormID,parent.formtext as FormText,Parent.parentid as ParentID,userrights.roleid as RoleId,userrights.Operationid as OperationID,UserOperation.Description as Description from formright right join formright as Parent on formRight.formID=Parent.parentID join userrights on parent.formID=userRights.formID join UserOperation on userRights.operationID=UserOperation.operationID"
        Dim sqlDGrid As String = "select Parent.formId as FormID,parent.formtext as FormText,Parent.parentid as ParentID,userrights.roleid as RoleId,userrights.Operationid as OperationID,UserOperation.Description as Description from formright right join formright as Parent on formRight.formID=Parent.parentID left join userrights on parent.formID=userRights.formID  left join UserOperation on UserOperation.operationID=userRights.operationID"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlDGrid, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader


    End Function
End Class
