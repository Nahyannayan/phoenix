﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections.Generic
Imports System.Text
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports System.IO
Imports System.Xml
Imports System.Web.Configuration
Imports System.Drawing.Graphics
Imports UtilityObj


Public Class TreeBuilder
    Implements IDisposable

#Region "Private Members"

    Private _NameFontColor As Color = Color.Navy
    Private _DescrFontColor As Color = Color.Black
    Private _BoxWidth As Integer = 250
    Private _BoxHeight As Integer = 60
    Private _Margin As Integer = 20
    Private _HorizontalSpace As Integer = 30
    Private _VerticalSpace As Integer = 30
    Private _FontSize As Integer = 9
    Private imgWidth As Integer = 0
    Private imgHeight As Integer = 0
    Private gr As Graphics
    Private _LineColor As Color = Color.Black
    Private _LineWidth As Single = 2
    Private _GradeWise As Boolean = False
    Private _BoxFillColor As Color = Color.White
    Private _BGColor As Color = Color.White
    Private dtTree As New DataTable 'TreeData.TreeDataTableDataTable
    Private nodeTree As XmlDocument
    Private PercentageChangeX As Double
    Dim _LevelNo As Integer
    ' = ActualWidth / imgWidth;
    Private PercentageChangeY As Double
    Public imgPath As String
    Public NoImgPath As String
    ' = ActualHeight / imgHeight;
#End Region
#Region "Public Properties"
    Public ReadOnly Property xmlTree() As XmlDocument
        Get
            Return nodeTree
        End Get
    End Property
    Public Property BoxFillColor() As Color
        Get
            Return _BoxFillColor
        End Get
        Set(ByVal value As Color)
            _BoxFillColor = value
        End Set
    End Property
    Public Property GradeWise() As Boolean
        Get
            Return _GradeWise
        End Get
        Set(ByVal value As Boolean)
            _GradeWise = value
        End Set
    End Property
    Public Property LevelNo() As Integer
        Get
            Return _LevelNo
        End Get
        Set(ByVal value As Integer)
            _LevelNo = value
        End Set
    End Property
    Public Property BoxWidth() As Integer
        Get
            Return _BoxWidth
        End Get
        Set(ByVal value As Integer)
            _BoxWidth = value
        End Set
    End Property
    Public Property BoxHeight() As Integer
        Get
            Return _BoxHeight
        End Get
        Set(ByVal value As Integer)
            _BoxHeight = value
        End Set
    End Property
    Public Property Margin() As Integer
        Get
            Return _Margin
        End Get
        Set(ByVal value As Integer)
            _Margin = value
        End Set
    End Property
    Public Property HorizontalSpace() As Integer
        Get
            Return _HorizontalSpace
        End Get
        Set(ByVal value As Integer)
            _HorizontalSpace = value
        End Set
    End Property
    Public Property VerticalSpace() As Integer
        Get
            Return _VerticalSpace
        End Get
        Set(ByVal value As Integer)
            _VerticalSpace = value
        End Set
    End Property
    Public Property FontSize() As Integer
        Get
            Return _FontSize
        End Get
        Set(ByVal value As Integer)
            _FontSize = value
        End Set
    End Property
    Public Property LineColor() As Color
        Get
            Return _LineColor
        End Get
        Set(ByVal value As Color)
            _LineColor = value
        End Set
    End Property
    Public Property LineWidth() As Single
        Get
            Return _LineWidth
        End Get
        Set(ByVal value As Single)
            _LineWidth = value
        End Set
    End Property


    Public Property BGColor() As Color
        Get
            Return _BGColor
        End Get
        Set(ByVal value As Color)
            _BGColor = value
        End Set
    End Property

    Public Property NameFontColor() As Color
        Get
            Return _NameFontColor
        End Get
        Set(ByVal value As Color)
            _NameFontColor = value
        End Set
    End Property
    Public Property DescrFontColor() As Color
        Get
            Return _DescrFontColor
        End Get
        Set(ByVal value As Color)
            _DescrFontColor = value
        End Set
    End Property

#End Region
#Region "Public Methods"

    ''' <summary>
    ''' ctor
    ''' </summary>
    ''' <param name="TreeData"></param>
    Public Sub New(ByVal TreeData As DataTable)
        dtTree = TreeData
    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        dtTree = Nothing

        If gr IsNot Nothing Then
            gr.Dispose()
            gr = Nothing
        End If
    End Sub
    ''' <summary>
    ''' This overloaded method can be used to return the image using it's default calculated size, without resizing
    ''' </summary>
    ''' <param name="StartFromNodeID"></param>
    ''' <param name="ImageType"></param>
    ''' <returns></returns>
    Public Function GenerateTree(ByVal StartFromNodeID As String, ByVal ImageType As System.Drawing.Imaging.ImageFormat) As System.IO.Stream
        Return GenerateTree(-1, -1, StartFromNodeID, ImageType)
    End Function
    ''' <summary>
    ''' Creates the tree
    ''' </summary>
    ''' <param name="Width"></param>
    ''' <param name="Height"></param>
    ''' <param name="StartFromNodeID"></param>
    ''' <param name="ImageType"></param>
    ''' <returns></returns>
    Public Function GenerateTree(ByVal Width As Integer, ByVal Height As Integer, ByVal StartFromNodeID As String, ByVal ImageType As System.Drawing.Imaging.ImageFormat) As System.IO.Stream
        Try
            Dim Result As New MemoryStream()
            'reset image size
            imgHeight = 0
            imgWidth = 0
            'reset percentage change
            PercentageChangeX = 1.0
            PercentageChangeY = 1.0
            'define the image
            nodeTree = Nothing
            nodeTree = New XmlDocument()
            Dim rootDescription1 As String = String.Empty
            Dim rootDescription2 As String = String.Empty
            Dim rootDescription3 As String = String.Empty
            Dim rootDetail As String = String.Empty
            Dim rootGrade As String = String.Empty
            Dim rootColor As String = String.Empty
            Dim rootImage As String = String.Empty
            If dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID)).Length > 0 Then
                rootDescription1 = DirectCast(dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID))(0), DataRow)("nodeDescription1")
                rootDescription2 = DirectCast(dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID))(0), DataRow)("nodeDescription2")
                rootDescription3 = DirectCast(dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID))(0), DataRow)("nodeDescription3")
                rootDetail = DirectCast(dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID))(0), DataRow)("nodeDetail")
                rootImage = DirectCast(dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID))(0), DataRow)("nodeImage")
                rootColor = DirectCast(dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID))(0), DataRow)("nodeColor")
                rootGrade = DirectCast(dtTree.[Select](String.Format("nodeID='{0}'", StartFromNodeID))(0), DataRow)("Grade")
            End If

            Dim RootNode As XmlNode = GetXMLNode(StartFromNodeID, rootDescription1, rootDescription2, rootDescription3, rootDetail, rootImage, rootGrade, rootColor)
            nodeTree.AppendChild(RootNode)
            BuildTree(RootNode, 0)

            'check for intersection. line below should be remarked if not debugging
            'as it affects performance measurably.
            'OverlapExists();
            Dim bmp As New Bitmap(imgWidth, imgHeight)
            gr = Graphics.FromImage(bmp)
            gr.Clear(_BGColor)
            DrawChart(RootNode)

            'if caller does not care about size, use original calculated size
            If Width < 0 Then
                Width = imgWidth
            End If
            If Height < 0 Then
                Height = imgHeight
            End If

            Dim ResizedBMP As New Bitmap(bmp, New Size(Width, Height))
            'after resize, determine the change percentage
            PercentageChangeX = Convert.ToDouble(Width) / imgWidth
            PercentageChangeY = Convert.ToDouble(Height) / imgHeight
            'after resize - change the coordinates of the list, in order return the proper coordinates
            'for each node
            If PercentageChangeX <> 1.0 OrElse PercentageChangeY <> 1.0 Then
                'only resize coordinates if there was a resize
                CalculateImageMapData()
            End If
            ResizedBMP.Save(Result, ImageType)
            ResizedBMP.Dispose()
            bmp.Dispose()
            gr.Dispose()
            Return Result
        Catch ex As Exception

        End Try
    End Function
    ''' <summary>
    ''' the node holds the x,y in attributes
    ''' use them to calculate the position
    ''' This is public so it can be used by other classes trying to calculate the 
    ''' cursor/mouse location
    ''' </summary>
    ''' <param name="oNode"></param>
    ''' <returns></returns>
    Public Function getRectangleFromNode(ByVal oNode As XmlNode) As Rectangle
        If oNode.Attributes("X") Is Nothing OrElse oNode.Attributes("Y") Is Nothing Then
            Throw New Exception("Both attributes X,Y must exist for node.")
        End If
        Dim X As Integer = Convert.ToInt32(oNode.Attributes("X").InnerText)
        Dim Y As Integer = Convert.ToInt32(oNode.Attributes("Y").InnerText)

        Dim Result As New Rectangle(X, Y, CInt(Math.Truncate(_BoxWidth * PercentageChangeX)), CInt(Math.Truncate(_BoxHeight * PercentageChangeY)))
        Return Result

    End Function
#End Region
#Region "Private Methods"
    ''' <summary>
    ''' convert the datatable to an XML document
    ''' </summary>
    ''' <param name="oNode"></param>
    ''' <param name="y"></param>
    Private Sub BuildTree(ByVal oNode As XmlNode, ByVal y As Integer)
        Dim childNode As XmlNode = Nothing
        'has children
        For Each childRow As DataRow In dtTree.[Select](String.Format("parentNodeID='{0}'", oNode.Attributes("nodeID").InnerText))
            'for each child node call this function again
            childNode = GetXMLNode(childRow("nodeID"), childRow("nodeDescription1"), childRow("nodeDescription2"), childRow("nodeDescription3"), childRow("nodeDetail"), childRow("nodeImage"), childRow("Grade"), childRow("nodeColor"))
            oNode.AppendChild(childNode)

            BuildTree(childNode, y + 1)
            ' BuildTree(childNode, childRow("Grade") + 1)

        Next
        'build node data
        'after checking for nodes we can add the current node
        Dim StartX As Integer
        Dim StartY As Integer
        Dim ResultsArr As Integer() = New Integer() {GetXPosByOwnChildren(oNode), GetXPosByParentPreviousSibling(oNode), GetXPosByPreviousSibling(oNode), _Margin}
        Array.Sort(ResultsArr)
        StartX = ResultsArr(3)

        If GradeWise Then
            StartY = (Val(oNode.Attributes("Grade").InnerText) * (_BoxHeight + _VerticalSpace)) + _Margin
        Else
            StartY = (y * (_BoxHeight + _VerticalSpace)) + _Margin
        End If

        Dim width As Integer = _BoxWidth
        Dim height As Integer = _BoxHeight
        'update the coordinates of this box into the matrix, for later calculations
        oNode.Attributes("X").InnerText = StartX.ToString()
        oNode.Attributes("Y").InnerText = StartY.ToString()
        LevelNo = 1
        'update the image size
        If imgWidth < (StartX + width + _Margin) Then
            imgWidth = StartX + width + _Margin
        End If
        If imgHeight < (StartY + height + _Margin) Then
            imgHeight = StartY + height + _Margin
        End If
    End Sub

    '***********************************************************************************************************************
    '         * The box position is affected by:
    '         * 1. The previous sibling (box on the same level)
    '         * 2. The positions of it's children
    '         * 3. The position of it's uncle (parents' previous sibling)/ cousins (parents' previous sibling children)
    '         * What determines the position is the farthest x of all the above. If all/some of the above have no value, the margin 
    '         * becomes the dtermining factor.
    '         * **********************************************************************************************************************
    '        


    Private Function GetXPosByPreviousSibling(ByVal CurrentNode As XmlNode) As Integer
        Dim Result As Integer = -1
        Dim X As Integer = -1
        Dim PrevSibling As XmlNode = CurrentNode.PreviousSibling
        If PrevSibling IsNot Nothing Then
            If PrevSibling.HasChildNodes Then

                'Result = Convert.ToInt32(PrevSibling.LastChild.Attributes["X"].InnerText ) + _BoxWidth + _HorizontalSpace;
                'need to loop through all children for all generations of previous sibling
                X = Convert.ToInt32(GetMaxXOfDescendants(PrevSibling.LastChild))

                Result = X + _BoxWidth + _HorizontalSpace
            Else

                Result = Convert.ToInt32(PrevSibling.Attributes("X").InnerText) + _BoxWidth + _HorizontalSpace
            End If
        End If
        Return Result
    End Function

    Private Function GetXPosByOwnChildren(ByVal CurrentNode As XmlNode) As Integer
        Dim Result As Integer = -1

        If CurrentNode.HasChildNodes Then
            Dim lastChildX As Integer = Convert.ToInt32(CurrentNode.LastChild.Attributes("X").InnerText)
            Dim firstChildX As Integer = Convert.ToInt32(CurrentNode.FirstChild.Attributes("X").InnerText)


            Result = (((lastChildX + _BoxWidth) - firstChildX) \ 2) - (_BoxWidth \ 2) + firstChildX
        End If
        Return Result
    End Function
    Private Function GetXPosByParentPreviousSibling(ByVal CurrentNode As XmlNode) As Integer
        Dim Result As Integer = -1
        Dim X As Integer = -1
        Dim ParentPrevSibling As XmlNode = CurrentNode.ParentNode.PreviousSibling

        If ParentPrevSibling IsNot Nothing Then
            If ParentPrevSibling.HasChildNodes Then

                'X = Convert.ToInt32(ParentPrevSibling.LastChild.Attributes["X"].InnerText);
                X = GetMaxXOfDescendants(ParentPrevSibling.LastChild)
                Result = X + _BoxWidth + _HorizontalSpace
            Else

                X = Convert.ToInt32(ParentPrevSibling.Attributes("X").InnerText)
                Result = X + _BoxWidth + _HorizontalSpace
            End If
        Else
            'ParentPrevSibling == null

            If CurrentNode.ParentNode.Name <> "#document" Then
                Result = GetXPosByParentPreviousSibling(CurrentNode.ParentNode)
            End If
        End If
        Return Result
    End Function
    ''' <summary>
    ''' Get the maximum x of the lowest child on the current tree of nodes
    ''' Recursion does not work here, so we'll use a loop to climb down the tree
    ''' Recursion is not a solution because we need to return the value of the last leaf of the tree.
    ''' That would require managing a global variable.
    ''' </summary>
    ''' <param name="CurrentNode"></param>
    ''' <returns></returns>
    Private Function GetMaxXOfDescendants(ByVal CurrentNode As XmlNode) As Integer
        Dim Result As Integer = -1

        While CurrentNode.HasChildNodes

            CurrentNode = CurrentNode.LastChild
        End While

        Result = Convert.ToInt32(CurrentNode.Attributes("X").InnerText)

        Return Result
        'int Result = -1;
        'if (CurrentNode.HasChildNodes)
        '{
        '    GetMaxXOfDescendants(CurrentNode.LastChild);
        '}
        'else
        '{
        '    Result = Convert.ToInt32(CurrentNode.Attributes["X"].InnerText);
        '}
        'return Result;
    End Function

    ''' <summary>
    ''' create an xml node based on supplied data
    ''' </summary>
    ''' <returns></returns>
    Private Function GetXMLNode(ByVal nodeID As String, ByVal nodeDescription1 As String, ByVal nodeDescription2 As String, ByVal nodeDescription3 As String, ByVal nodeDetail As String, ByVal nodeImage As String, ByVal Grade As String, ByVal nodeColor As String) As XmlNode
        'build the node
        Dim resultNode As XmlNode = nodeTree.CreateElement("Node")
        Dim attNodeID As XmlAttribute = nodeTree.CreateAttribute("nodeID")

        Dim attNodeDescription1 As XmlAttribute = nodeTree.CreateAttribute("nodeDescription1")
        Dim attNodeDescription2 As XmlAttribute = nodeTree.CreateAttribute("nodeDescription2")
        Dim attNodeDescription3 As XmlAttribute = nodeTree.CreateAttribute("nodeDescription3")
        Dim attNodeDetail As XmlAttribute = nodeTree.CreateAttribute("nodeDetail")
        Dim attNodeImage As XmlAttribute = nodeTree.CreateAttribute("nodeImage")
        Dim attNodeGrade As XmlAttribute = nodeTree.CreateAttribute("Grade")
        Dim attNodeColor As XmlAttribute = nodeTree.CreateAttribute("nodeColor")
        Dim attStartX As XmlAttribute = nodeTree.CreateAttribute("X")
        Dim attStartY As XmlAttribute = nodeTree.CreateAttribute("Y")

        'set the values of what we know
        attNodeID.InnerText = nodeID

        attNodeDescription1.InnerText = nodeDescription1
        attNodeDescription2.InnerText = nodeDescription2
        attNodeDescription3.InnerText = nodeDescription3
        attNodeDetail.InnerText = nodeDetail
        attNodeImage.InnerText = nodeImage
        attNodeColor.InnerText = nodeColor
        attNodeGrade.InnerText = Grade
        attStartX.InnerText = "0"
        attStartY.InnerText = "0"

        resultNode.Attributes.Append(attNodeID)

        resultNode.Attributes.Append(attNodeDescription1)
        resultNode.Attributes.Append(attNodeDescription2)
        resultNode.Attributes.Append(attNodeDescription3)
        resultNode.Attributes.Append(attNodeDetail)
        resultNode.Attributes.Append(attNodeImage)
        resultNode.Attributes.Append(attNodeGrade)
        resultNode.Attributes.Append(attNodeColor)
        resultNode.Attributes.Append(attStartX)
        resultNode.Attributes.Append(attStartY)

        Return resultNode

    End Function

    ''' <summary>
    ''' Draws the actual chart image.
    ''' </summary>
    Private Sub DrawChart(ByVal oNode As XmlNode)
        ' Create font and brush.
        Dim drawNameFont As New Font("verdana", _FontSize, FontStyle.Bold)
        Dim drawDescrFont1 As New Font("verdana", _FontSize, FontStyle.Italic)
        Dim drawDescrFont2 As New Font("verdana", _FontSize - 1)

        Dim drawNameBrush As New SolidBrush(_NameFontColor)
        Dim drawDescrBrush As New SolidBrush(_DescrFontColor)


        Dim boxPen As New Pen(_LineColor, _LineWidth)
        Dim drawFormat As New StringFormat()
        drawFormat.Alignment = StringAlignment.Center
        'find children
        If Not GradeWise Then
            If LevelNo >= 15 Then
                LevelNo = 1
            Else
                LevelNo += 1
            End If
        End If
        For Each childNode As XmlNode In oNode.ChildNodes
            DrawChart(childNode)
        Next
        LevelNo -= 1

        If GradeWise Then LevelNo = Val(oNode.Attributes("Grade").InnerText)

        Dim currentRectangle As Rectangle '= getRectangleFromNode(oNode)
        ' Create string to draw.
        Dim drawString As [String] = ""
        ' Draw string to screen.
        Dim imgWidth As Integer = _BoxWidth * 30 / 100
        Dim imgHeight As Integer = _BoxHeight
        currentRectangle = getRectangleFromNode(oNode)
        gr.DrawRectangle(boxPen, currentRectangle)
        Dim myBoxColor As String
        If oNode.Attributes("nodeColor").InnerText <> "" Then
            myBoxColor = oNode.Attributes("nodeColor").InnerText
        Else
            myBoxColor = "White"
        End If
        gr.FillRectangle(New SolidBrush(TreeData.getBoxLevelColor(myBoxColor)), currentRectangle)
        Dim imgAvail As Boolean = False
        Dim NodeImg As System.Drawing.Image
        Dim imgFileName As String


        If oNode.Attributes("nodeImage").InnerText <> "" And IO.File.Exists(imgPath & oNode.Attributes("nodeImage").InnerText) Then
            imgFileName = imgPath & oNode.Attributes("nodeImage").InnerText
        Else
            imgFileName = NoImgPath
        End If
        NodeImg = System.Drawing.Image.FromFile(imgFileName)
        Dim pt As New Point
        pt.X = currentRectangle.Left
        pt.Y = currentRectangle.Top
        gr.DrawImage(NodeImg, pt.X, pt.Y, imgWidth, imgHeight)
        imgAvail = True
        Dim LineLetterNo As Integer
        LineLetterNo = 20
        Dim myDrawRectangle As Rectangle
        myDrawRectangle = New Rectangle(currentRectangle.Left + imgWidth, currentRectangle.Top, currentRectangle.Width - imgWidth, currentRectangle.Height)
        Dim NewLineString As [String] = "" 'Environment.NewLine
        gr.DrawString(oNode.Attributes("nodeDescription1").InnerText, drawNameFont, drawNameBrush, myDrawRectangle, drawFormat)
        NewLineString = NewLineString & getNewLineString(oNode.Attributes("nodeDescription1").InnerText, drawNameFont, myDrawRectangle.Width)
        gr.DrawString(NewLineString & oNode.Attributes("nodeDescription2").InnerText, drawDescrFont1, drawDescrBrush, myDrawRectangle, drawFormat)
        NewLineString = NewLineString & getNewLineString(oNode.Attributes("nodeDescription2").InnerText, drawDescrFont1, myDrawRectangle.Width)
        gr.DrawString(Environment.NewLine & NewLineString & oNode.Attributes("nodeDescription3").InnerText, drawDescrFont2, drawDescrBrush, myDrawRectangle, drawFormat)
        'draw connecting lines

        If GradeWise Then
            Dim curNodeX, curNodeY As Integer
            curNodeX = currentRectangle.Left + (_BoxWidth \ 2) ' + (_LineWidth / 2)
            curNodeY = currentRectangle.Top - (_VerticalSpace \ 2)
            If oNode.ParentNode.Name <> "#document" Then
                'all but the top box should have lines growing out of their top
                gr.DrawLine(boxPen, currentRectangle.Left + (_BoxWidth \ 2), currentRectangle.Top, currentRectangle.Left + (_BoxWidth \ 2), currentRectangle.Top - (_VerticalSpace \ 2))
            End If
            If oNode.HasChildNodes Then
                'all nodes which have nodes should have lines coming from bottom down
                Dim myChild As XmlNode
                Dim myChildX, myChildY As Integer
                gr.DrawLine(boxPen, curNodeX, currentRectangle.Top + _BoxHeight, curNodeX, currentRectangle.Top + _BoxHeight + (_VerticalSpace \ 2))
                For Each myChild In oNode.ChildNodes
                    myChildX = getRectangleFromNode(myChild).Left + (_BoxWidth \ 2) '- (_LineWidth / 2)
                    myChildY = getRectangleFromNode(myChild).Top - (_VerticalSpace \ 2)
                    gr.DrawLine(boxPen, curNodeX, currentRectangle.Top + (_VerticalSpace \ 2) + _BoxHeight, myChildX, currentRectangle.Top + _BoxHeight + (_VerticalSpace \ 2))
                    gr.DrawLine(boxPen, myChildX, currentRectangle.Top + (_VerticalSpace \ 2) + _BoxHeight, myChildX, myChildY)
                Next
            End If
        Else
            If oNode.ParentNode.Name <> "#document" Then
                'all but the top box should have lines growing out of their top
                gr.DrawLine(boxPen, currentRectangle.Left + (_BoxWidth \ 2), currentRectangle.Top, currentRectangle.Left + (_BoxWidth \ 2), currentRectangle.Top - (_VerticalSpace \ 2))
            End If
            If oNode.HasChildNodes Then
                'all nodes which have nodes should have lines coming from bottom down
                gr.DrawLine(boxPen, currentRectangle.Left + (_BoxWidth \ 2), currentRectangle.Top + _BoxHeight, currentRectangle.Left + (_BoxWidth \ 2), currentRectangle.Top + _BoxHeight + (_VerticalSpace \ 2))
            End If
            If oNode.PreviousSibling IsNot Nothing Then
                'the prev node has the same boss - connect the 2 nodes
                gr.DrawLine(boxPen, getRectangleFromNode(oNode.PreviousSibling).Left + (_BoxWidth \ 2) - (_LineWidth / 2), getRectangleFromNode(oNode.PreviousSibling).Top - (_VerticalSpace \ 2), currentRectangle.Left + (_BoxWidth \ 2) + (_LineWidth / 2), currentRectangle.Top - (_VerticalSpace \ 2))
            End If
        End If

    End Sub
    Private Function getNewLineString(ByVal mStr As String, ByVal drawFont As Drawing.Font, ByVal MaxLength As Double) As String
        Dim i As Integer
        Dim RetString As String = Environment.NewLine
        Dim currLen As Double
        MaxLength = MaxLength - 15
        currLen = gr.MeasureString(mStr, drawFont).Width
        If currLen > MaxLength Then
            For i = 0 To Math.Ceiling((currLen - MaxLength) / MaxLength) - 1
                RetString = RetString & Environment.NewLine
            Next
        End If
        getNewLineString = RetString
    End Function

    ''' <summary>
    ''' After resizing the image, all positions of the rectanlges need to be 
    ''' recalculated too.
    ''' </summary>
    ''' <param name="ActualWidth"></param>
    ''' <param name="ActualHeight"></param>
    Private Sub CalculateImageMapData()

        Dim X As Integer = 0
        Dim newX As Integer = 0
        Dim Y As Integer = 0
        Dim newY As Integer = 0
        For Each oNode As XmlNode In nodeTree.SelectNodes("//Node")
            'go through all nodes and resize the coordinates
            X = Convert.ToInt32(oNode.Attributes("X").InnerText)
            Y = Convert.ToInt32(oNode.Attributes("Y").InnerText)
            newX = CInt(Math.Truncate(X * PercentageChangeX))
            newY = CInt(Math.Truncate(Y * PercentageChangeY))
            oNode.Attributes("X").InnerText = newX.ToString()

            oNode.Attributes("Y").InnerText = newY.ToString()
        Next

    End Sub
    ''' <summary>
    ''' used for testing purposes, to see if overlap exists between at least 2 boxes.
    ''' </summary>
    ''' <returns></returns>
    Private Function OverlapExists() As Boolean

        Dim listOfRectangles As New List(Of Rectangle)()
        'the list of all objects
        Dim X As Integer
        Dim Y As Integer
        Dim currentRect As Rectangle
        For Each oNode As XmlNode In nodeTree.SelectNodes("//Node")
            'go through all nodes and resize the coordinates
            X = Convert.ToInt32(oNode.Attributes("X").InnerText)
            Y = Convert.ToInt32(oNode.Attributes("Y").InnerText)
            currentRect = New Rectangle(X, Y, _BoxWidth, _BoxHeight)
            'before adding the node we check if the space it is supposed to occupy is already occupied.
            For Each rect As Rectangle In listOfRectangles
                If currentRect.IntersectsWith(rect) Then
                    'problem

                    Return True


                End If
            Next

            listOfRectangles.Add(currentRect)
        Next
        Return False
    End Function


#End Region
End Class


'Partial Public Class TreeData
'    Partial Public Class TreeDataTableDataTable
'    End Class
'End Class

Public Class NodePos
    'throw new System.NotImplementedException();
    Public Sub New()
    End Sub
    Private _NodeID As String

    Public Property NodeID() As String
        Get
            Return _NodeID
        End Get
        Set(ByVal value As String)
            _NodeID = value
        End Set
    End Property
    Private _NodePosition As System.Drawing.RectangleF

    Public Property NodePosition() As System.Drawing.RectangleF
        Get
            Return _NodePosition
        End Get
        Set(ByVal value As System.Drawing.RectangleF)
            _NodePosition = value
        End Set
    End Property


End Class

Public Class Box
    Public Width As Integer
    Public Height As Integer
    Public LineColor As System.Drawing.Color
    Public LineWidth As Integer
End Class
Public Class TreeData
    Public Shared TreeDataTable As New DataTable

    Public nodeID As String
    Public nodeDescription1 As String
    Public nodeDescription2 As String
    Public nodeDescription3 As String
    Public nodeDetail As String
    Public ParentNodeID As String
    Public Sub New()
        CreateTreetable(TreeDataTable)
    End Sub
    Public Shared Function getBoxLevelColor(ByVal myColor As String) As Color
        Try
            If myColor.Contains(",") Then
                Dim mArr(), R, G, B As String
                R = 255 : G = 255 : B = 255
                mArr = myColor.Split(",")
                If mArr.Length > 0 Then R = mArr(0)
                If mArr.Length > 1 Then G = mArr(1)
                If mArr.Length > 2 Then B = mArr(2)
                Return Color.FromArgb(R, G, B)
            Else
                Return ColorTranslator.FromHtml(myColor)
            End If

        Catch ex As Exception
            Return Color.White
        End Try
    End Function

    Public Shared Sub CreateTreetable(ByRef TreeDataTable As DataTable)
        If TreeDataTable Is Nothing Then
            TreeDataTable = New DataTable
        End If
        If Not TreeDataTable.Columns.Contains("nodeID") Then TreeDataTable.Columns.Add("nodeID", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("nodeDescription1") Then TreeDataTable.Columns.Add("nodeDescription1", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("nodeDescription2") Then TreeDataTable.Columns.Add("nodeDescription2", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("nodeDescription3") Then TreeDataTable.Columns.Add("nodeDescription3", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("ParentNodeID") Then TreeDataTable.Columns.Add("ParentNodeID", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("NodeDetail") Then TreeDataTable.Columns.Add("NodeDetail", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("NodeImage") Then TreeDataTable.Columns.Add("NodeImage", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("Grade") Then TreeDataTable.Columns.Add("Grade", System.Type.GetType("System.String"))
        If Not TreeDataTable.Columns.Contains("nodeColor") Then TreeDataTable.Columns.Add("nodeColor", System.Type.GetType("System.String"))
    End Sub
    Public Shared Sub AddTreeDataRow(ByRef TreeDataTable As DataTable, ByVal NodeRow As DataRow)
        Try
            CreateTreetable(TreeDataTable)
            Dim mRow As DataRow
            mRow = TreeDataTable.NewRow
            mRow("nodeId") = NodeRow("nodeId")
            mRow("nodeDescription1") = NodeRow("nodeDescription1")
            mRow("nodeDescription2") = NodeRow("nodeDescription2")
            mRow("nodeDescription3") = NodeRow("nodeDescription3")
            mRow("ParentNodeID") = NodeRow("ParentNodeID")
            mRow("NodeDetail") = NodeRow("NodeDetail")
            mRow("NodeImage") = NodeRow("NodeImage")
            mRow("nodeColor") = NodeRow("nodeColor")
            mRow("Grade") = NodeRow("Grade")
            TreeDataTable.Rows.Add(mRow)
            'Return mRow
        Catch ex As Exception

        End Try
    End Sub
End Class

