Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Imports System.Data.Oledb
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class GetMessageStatus
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function GetSmsNormalStatus(ByVal Sending_id As String) As XmlDocument

        Dim ds As New DataSet()
        Dim Xdoc As New XmlDocument
        Try

            Dim strConn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim condition = "select TOTAL_MESSAGE,(convert(int,SUCCESSFULLY_SEND) + convert(int,SENDING_FAILED)) AS CURRENT_COUNT,SUCCESSFULLY_SEND,SENDING_FAILED, CONVERT(VARCHAR(20),START_TIME, 100)AS START_TIME, CONVERT(VARCHAR(20),END_TIME, 100) AS END_TIME from dbo.COM_EXCEL_SMS_SENDING_LOG WHERE SENDING_ID='" & Sending_id & "'"

            ds = SqlHelper.ExecuteDataset(strConn, CommandType.Text, condition)
            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)

        Catch ex As Exception

        End Try

        Return Xdoc

    End Function

    <WebMethod()> _
    Public Function GetEmailNormalStatus(ByVal Sending_id As String) As XmlDocument

        Dim ds As New DataSet()
        Dim Xdoc As New XmlDocument
        Try

            Dim strConn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim condition = "select TOTAL_MESSAGE,(convert(int,SUCCESSFULLY_SEND) + convert(int,SENDING_FAILED)) AS CURRENT_COUNT,SUCCESSFULLY_SEND,SENDING_FAILED, CONVERT(VARCHAR(20),START_TIME, 100)AS START_TIME, CONVERT(VARCHAR(20),END_TIME, 100) AS END_TIME from dbo.COM_EXCEL_EMAIL_SENDING_LOG WHERE SENDING_ID='" & Sending_id & "'"

            ds = SqlHelper.ExecuteDataset(strConn, CommandType.Text, condition)
            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)

        Catch ex As Exception

        End Try

        Return Xdoc

    End Function


End Class
