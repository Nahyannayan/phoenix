Imports Microsoft.VisualBasic

Public Class SMS_HASH
    Dim vStudFname As String
    Dim vGrade As String
    Dim vSTU_ID As Integer
    Dim vMob_No As String
    Dim vFrom_Date As String
    Dim vTo_Date As String
    Dim vStr1 As String
    Dim vStr2 As String
    Dim vStr3 As String
    Dim vStr4 As String
    Dim vPAYMODE As String
    Dim vCREDIT_TYPE As String
    Dim vCREDIT_AUTHNO As String
    Dim vCHQ_BANK As String
    Dim vCHQ_DATE As String
    Dim vCHQ_NO As String
    Dim vEDU_AMT As String
    Dim vCreditNo As String
    Public Property From_DT() As String
        Get
            Return vFrom_Date
        End Get
        Set(ByVal value As String)
            vFrom_Date = value
        End Set
    End Property
    Public Property To_Date() As String
        Get
            Return vTo_Date
        End Get
        Set(ByVal value As String)
            vTo_Date = value
        End Set
    End Property
    Public Property STU_ID() As Integer
        Get
            Return vSTU_ID
        End Get
        Set(ByVal value As Integer)
            vSTU_ID = value
        End Set
    End Property

    Public Property StudFname() As String
        Get
            Return vStudFname
        End Get
        Set(ByVal value As String)
            vStudFname = value
        End Set
    End Property

    Public Property Grade() As String
        Get
            Return vGrade
        End Get
        Set(ByVal value As String)
            vGrade = value
        End Set
    End Property
    Public Property Mob_No() As String
        Get
            Return vMob_No
        End Get
        Set(ByVal value As String)
            vMob_No = value
        End Set
    End Property
    Public Property str1() As String
        Get
            Return vStr1
        End Get
        Set(ByVal value As String)
            vStr1 = value
        End Set
    End Property
    Public Property str2() As String
        Get
            Return vStr2
        End Get
        Set(ByVal value As String)
            vStr2 = value
        End Set
    End Property
    Public Property str3() As String
        Get
            Return vStr3
        End Get
        Set(ByVal value As String)
            vStr3 = value
        End Set
    End Property
    Public Property str4() As String
        Get
            Return vStr4
        End Get
        Set(ByVal value As String)
            vStr4 = value
        End Set
    End Property
    Public Property PAYMODE() As String
        Get
            Return vPAYMODE
        End Get
        Set(ByVal value As String)
            vPAYMODE = value
        End Set
    End Property
    Public Property CREDIT_TYPE() As String
        Get
            Return vCREDIT_TYPE
        End Get
        Set(ByVal value As String)
            vCREDIT_TYPE = value
        End Set
    End Property
    Public Property CREDIT_AUTHNO() As String
        Get
            Return vCREDIT_AUTHNO
        End Get
        Set(ByVal value As String)
            vCREDIT_AUTHNO = value
        End Set
    End Property
    Public Property CHQ_BANK() As String
        Get
            Return vCHQ_BANK
        End Get
        Set(ByVal value As String)
            vCHQ_BANK = value
        End Set
    End Property
    Public Property CHQ_DATE() As String
        Get
            Return vCHQ_DATE
        End Get
        Set(ByVal value As String)
            vCHQ_DATE = value
        End Set
    End Property
    Public Property CHQ_NO() As String
        Get
            Return vCHQ_NO
        End Get
        Set(ByVal value As String)
            vCHQ_NO = value
        End Set
    End Property
    Public Property EDU_AMT() As String
        Get
            Return vEDU_AMT
        End Get
        Set(ByVal value As String)
            vEDU_AMT = value
        End Set
    End Property
    Public Property CREDITNO() As String
        Get
            Return vCreditNo
        End Get
        Set(ByVal value As String)
            vCreditNo = value
        End Set
    End Property
    
End Class
