﻿Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports System.IO

Namespace pluginwiris
    ''' <summary>
    ''' Summary description for libwiris.
    ''' </summary>
    Public Class Libwiris
        Inherits System.ComponentModel.Component
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.Container = Nothing

        Public Shared FormulaDirectory As String = "../formulas"
        Public Shared CacheDirectory As String = "../cache"
        Public Shared configFile As String = "../configuration.ini"

        Public Shared Function base64Decode(ByVal data As String) As String
            Dim encoder As New System.Text.UTF8Encoding()
            Dim utf8Decode As System.Text.Decoder = encoder.GetDecoder()

            Dim toDecode_byte As Byte() = Convert.FromBase64String(data)
            Dim charCount As Integer = utf8Decode.GetCharCount(toDecode_byte, 0, toDecode_byte.Length)
            Dim decoded_char As Char() = New Char(charCount - 1) {}
            utf8Decode.GetChars(toDecode_byte, 0, toDecode_byte.Length, decoded_char, 0)

            Return New [String](decoded_char)
        End Function

        Public Shared Function md5(ByVal input As String) As String
            Dim md5Provider As New System.Security.Cryptography.MD5CryptoServiceProvider()
            Dim stream As Byte() = System.Text.Encoding.UTF8.GetBytes(input)
            stream = md5Provider.ComputeHash(stream)
            Dim stringBuilder As New System.Text.StringBuilder()

            For Each currentByte As Byte In stream
                stringBuilder.Append(currentByte.ToString("x2").ToLower())
            Next

            Return stringBuilder.ToString()
        End Function

        Public Shared Function loadConfig(ByVal file__1 As String) As Hashtable
            Dim toReturn As New Hashtable()

            Dim reader As StreamReader = File.OpenText(file__1)
            Dim content As String = reader.ReadToEnd()
            reader.Close()

            content = content.Replace(vbCr, "")
            Dim content_lines As String() = content.Split(vbLf.ToCharArray(0, 1))

            For i As Integer = 0 To content_lines.Length - 1
                Dim line_words As String() = content_lines(i).Split("=".ToCharArray(0, 1), 2)

                If line_words.Length = 2 Then
                    Dim key As String = line_words(0).Trim()
                    Dim values As String = line_words(1).Trim()
                    toReturn.Add(key, values)
                End If
            Next

            Return toReturn
        End Function

        Public Sub New(ByVal container As System.ComponentModel.IContainer)
            '''
            ''' Required for Windows.Forms Class Composition Designer support
            '''
            container.Add(Me)

            '
            ' TODO: Add any constructor code after InitializeComponent call
            '
            InitializeComponent()
        End Sub

        Public Sub New()
            '''
            ''' Required for Windows.Forms Class Composition Designer support
            '''

            '
            ' TODO: Add any constructor code after InitializeComponent call
            '
            InitializeComponent()
        End Sub

        ''' <summary> 
        ''' Clean up any resources being used.
        ''' </summary>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub


#Region "Component Designer generated code"
        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
        End Sub
#End Region
    End Class
End Namespace
