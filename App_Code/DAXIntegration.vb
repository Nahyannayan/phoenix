﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class DAXIntegration
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function stagingPull(ByVal bsuId As String) As DataSet
        Dim dsSynchData As New DataSet
        Dim tableNames() As String = {"TRANSFER"}
        Try
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = bsuId
            dsSynchData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "getStagingData", pParms)
            stagingPull = dsSynchData
        Catch ex As Exception
            stagingPull = Nothing
        End Try
        Return stagingPull
    End Function

    'getSynchData
    <WebMethod()> _
    Public Function stagingPush(ByVal pushData As DataSet) As String
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        stagingPush = ""
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@strTransfer", SqlDbType.VarChar)
            pParms(1).Value = DataTableToString(pushData.Tables(0), "transfer")
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "putStagingData", pParms)
            stTrans.Commit()
            stagingPush = ""
        Catch ex As Exception
            stTrans.Rollback()
            stagingPush = ex.Message
        Finally
            objConn.Close()
        End Try
        Return stagingPush
    End Function

    Private Function DataTableToString(ByRef dt As DataTable, ByVal strRootElementName As String) As String
        Dim sb As New StringBuilder()
        If strRootElementName = "" Then
            strRootElementName = Convert.ToString(dt.TableName)
        End If
        For Each dr As DataRow In dt.Rows
            sb.Append("<" & strRootElementName & ">")
            For Each dc As DataColumn In dt.Columns
                If dc.ColumnName.ToLower.Contains("date") AndAlso dr(dc).ToString().Length > 0 Then
                    sb.Append(("<" & dc.ColumnName & ">" & Format(dr(dc), "dd-MMM-yyyy hh:mm:ss") & "</") & dc.ColumnName & ">")
                Else
                    sb.Append(("<" + dc.ColumnName & ">" & dr(dc).ToString().Replace("’", "`") & "</") + dc.ColumnName & ">")
                End If
            Next
            sb.Append("</" & strRootElementName & ">")
        Next
        Return sb.ToString()
    End Function

End Class