﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Configuration
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web.Services.Protocols

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
<ScriptService()>
Public Class GetBulkProcessProgress
    Inherits System.Web.Services.WebService


    <System.Web.Script.Services.ScriptMethod(),
     System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetStatus(ByVal BATCHNO As String, ByVal SOURCE As String, ByVal PAGE As String) As FeeBulkProcess

        Dim Response As New FeeBulkProcess
        Dim TOTAL_ROWS As New Int32
        Dim PENDING As New Int32
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Using con As New SqlConnection(str_conn)
            con.Open()
            Dim stTrans As SqlTransaction = con.BeginTransaction
            Dim sqlParam(3) As SqlParameter
            Dim RequestParam As String = TryCast(HttpContext.Current.Request.Params(0), String)

            Using cmd As New SqlCommand("API.GET_BULK_PROCESS_PROGRESS", con, stTrans)
                sqlParam(0) = New SqlParameter("@SOURCE", SOURCE)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = New SqlParameter("@BATCH_NO", BATCHNO)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = New SqlParameter("@PERCENTAGE", SqlDbType.VarChar, 50)
                sqlParam(2).Direction = ParameterDirection.Output
                cmd.Parameters.Add(sqlParam(2))
                cmd.CommandTimeout = 0
                cmd.CommandType = CommandType.StoredProcedure
                cmd.ExecuteNonQuery()
                Response.PERCENTAGE = sqlParam(2).Value
                Response.WORKDESCRIPTION = " processed"
                If (Response.PERCENTAGE = 100) Then
                    Response.ISCOMPLETE = True
                    Response.MESSAGE = GetMessage(PAGE, True) 'IIf(PAGE = "FEE_REV", "Charge reversal processed successfully", " Adjustment imported successfully")
                End If
                If (Session("API_ERROR-" + PAGE) = True) Then
                    Response.ISERROR = True
                    Response.MESSAGE = GetMessage(PAGE, False) 'IIf(PAGE = "FEE_REV", "Charge reversal failed", " Adjustment import Failed")
                End If
            End Using
        End Using
        Return Response
    End Function

    Private Shared Function GetMessage(ByVal pType As String, ByVal isSuccess As Boolean) As String
        GetMessage = ""
        Select Case pType
            Case "FEE_REV"
                GetMessage = IIf(isSuccess, "Charge reversal processed successfully", " Charge reversal failed")
            Case "FEES_ADJ", "TPT_ADJ", "SRV_ADJ"
                GetMessage = IIf(isSuccess, "Adjustment imported successfully", " Adjustment import Failed")
            Case "DFUP"
                GetMessage = IIf(isSuccess, "Debt followup file upload processed successfully", " Debt followup file upload failed")
        End Select
    End Function
End Class