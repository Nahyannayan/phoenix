Imports Microsoft.VisualBasic

Public Class fontColor1


    Public Function ReturnBGColor(ByVal OpertationID As String) As String
        Dim myfontColor As String

        Select Case OpertationID


            Case "1"

                myfontColor = "#FFFFC0"

            Case "2"

                myfontColor = "#88F580"
            Case "3"

                myfontColor = "#19F680"
            Case "4"

                myfontColor = "#11448A"

            Case "5"

                myfontColor = "#C0C0C0"
            Case "6"

                myfontColor = "#FFC0C0"
            Case "7"

                myfontColor = "#C0FFFF"
            Case Else
                myfontColor = "#C0C0FF"
        End Select

        Return myfontColor

    End Function


End Class
