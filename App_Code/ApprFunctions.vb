Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class ApprFunctions
    Public Shared Function SaveApproval_M( _
            ByVal p_APM_DOC_ID As String, _
            ByVal p_APM_BSU_ID As String, _
            ByVal p_APM_DPT_ID As Integer, _
            ByVal p_APM_ORDERID As Integer, _
            ByVal p_APM_USR_ID As String, _
            ByVal p_APM_bREQPREVAPPROVAL As Boolean, _
            ByVal p_APM_bREQPHIGHAPPROVAL As Boolean, _
            ByVal p_APM_LIMIT As Decimal, _
            ByVal p_APM_DTFROM As String, _
            ByVal p_bFirstRec As Integer, _
             ByVal p_bEdit As Boolean, _
            ByVal stTrans As SqlTransaction) As Integer

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Try
            Dim pParms(11) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APM_DOC_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_APM_DOC_ID
            pParms(1) = New SqlClient.SqlParameter("@APM_BSU_ID", SqlDbType.VarChar, 10)
            pParms(1).Value = p_APM_BSU_ID
            pParms(2) = New SqlClient.SqlParameter("@APM_DPT_ID", SqlDbType.Int)
            pParms(2).Value = p_APM_DPT_ID
            pParms(3) = New SqlClient.SqlParameter("@APM_ORDERID", SqlDbType.TinyInt)
            pParms(3).Value = p_APM_ORDERID
            pParms(4) = New SqlClient.SqlParameter("@APM_USR_ID", SqlDbType.VarChar, 20)
            pParms(4).Value = p_APM_USR_ID
            pParms(5) = New SqlClient.SqlParameter("@APM_bREQPREVAPPROVAL", SqlDbType.Bit)
            pParms(5).Value = p_APM_bREQPREVAPPROVAL
            pParms(6) = New SqlClient.SqlParameter("@APM_bREQPHIGHAPPROVAL", SqlDbType.Bit)
            pParms(6).Value = p_APM_bREQPHIGHAPPROVAL
            pParms(7) = New SqlClient.SqlParameter("@APM_LIMIT ", SqlDbType.Decimal)
            pParms(7).Value = p_APM_LIMIT
            pParms(8) = New SqlClient.SqlParameter("@APM_DTFROM", SqlDbType.DateTime)
            pParms(8).Value = p_APM_DTFROM
            pParms(9) = New SqlClient.SqlParameter("@bFirstRec", SqlDbType.Int)
            pParms(9).Value = p_bFirstRec
            pParms(10) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
            pParms(10).Value = p_bEdit
            pParms(11) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(11).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer

            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVEAPPROVAL_M", pParms)
            SaveApproval_M = pParms(11).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            SaveApproval_M = 1000
        Finally

        End Try
    End Function
End Class
