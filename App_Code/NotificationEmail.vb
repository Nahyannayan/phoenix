﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Public Class NotificationEmail

    Public Shared Function SendEmailNotification(ByVal OPTIONS As Integer, ByVal INCIDENT_ID As String, ByVal BSU_ID As String) As Integer
        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@INCIDENT_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = INCIDENT_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = BSU_ID

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
         CommandType.StoredProcedure, "OASIS.dbo.SEND_PARENT_NOTIFICATION_EMAIL", pParms)
        Return ReturnFlag

    End Function

    Public Shared Function InsertIntoEmailSendSchedule(ByVal EML_BSU_ID As String, ByVal EML_TYPE As String,
                                                   ByVal EML_PROFILE_ID As String, ByVal EML_TOEMAIL As String, ByVal EML_SUBJECT As String, ByVal EML_MESSAGE As String) As Integer


        Dim pParms(10) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EML_BSU_ID", SqlDbType.VarChar, 50)
        pParms(0).Value = EML_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 50)
        pParms(1).Value = EML_TYPE
        pParms(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SqlDbType.VarChar, 500)
        pParms(2).Value = EML_PROFILE_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", SqlDbType.VarChar, 500)
        pParms(3).Value = EML_TOEMAIL
        pParms(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SqlDbType.VarChar, 500)
        pParms(4).Value = EML_SUBJECT
        pParms(5) = New SqlClient.SqlParameter("@EML_MESSAGE", SqlDbType.VarChar)
        pParms(5).Value = EML_MESSAGE
        pParms(6) = New SqlClient.SqlParameter("@EML_ID", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.Output

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "OASIS.dbo.InsertIntoEmailSendSchedule", pParms)
        Dim ReturnValue As Integer = pParms(6).Value
        Return ReturnValue

    End Function

    Public Shared Function GetParentDetails(ByVal OPTIONS As Integer, ByVal STU_NO As String) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 50)
        pParms(1).Value = STU_NO

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "DBO.GET_STUDENT_PARENT_DETAILS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function getEmailText(ByVal OPTIONS As Integer) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS



        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "DBO.GET_PARENT_NOTIFICATION_EMAIL", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0).Rows(0).Item("EMAIL_TXT")
        Else
            Return ""
        End If
    End Function


    Public Shared Function GetStudentDetails(ByVal sBsuid As String, ByVal stu_id As String) As DataTable

        Dim str_Sql As String = "select STU_PHOTOPATH,STU_NO,STU_ID,STU_FIRSTNAME+' '+STU_MIDNAME+' '+STU_LASTNAME AS NAME from [OASIS].[dbo].[STUDENT_M] where stu_id='" & stu_id & "' AND STU_BSU_ID='" & sBsuid & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Function GetStudentNotificationFlag(ByVal sBsuid As String) As DataTable

        Dim str_Sql As String = "select [PNT_bEMAIL_ACHIEVES], [PNT_bSMS_ACHIEVES], [PNT_bEMAIL_MERIT],[PNT_bSMS_MERIT], [PNT_bEMAIL_DEMERIT], [PNT_bSMS_DEMERIT] from [OASIS].[DBO].[PARENT_NOTIFICATION_M] where [PNT_BSU_ID]='" & sBsuid & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function
End Class
