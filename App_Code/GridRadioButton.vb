﻿Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections.Specialized
Imports Telerik.Web.UI

Namespace GridRadioButtonDemo

    Public Class GridRadioButton
        Inherits RadioButton

        Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
            Dim stringWriter As StringWriter = New StringWriter()
            Dim htmlWriter As HtmlTextWriter = New HtmlTextWriter(stringWriter)
            MyBase.Render(htmlWriter)
            Dim html As String = htmlWriter.InnerWriter.ToString()
            Dim regex As Regex = New Regex("name="".*?""", RegexOptions.Compiled Or RegexOptions.IgnoreCase Or RegexOptions.Multiline)
            html = html.Replace(regex.Match(html).Value, "name=""" & GroupName & """")
            regex = New Regex("value="".*?""", RegexOptions.Compiled Or RegexOptions.IgnoreCase Or RegexOptions.Multiline)
            html = html.Replace(regex.Match(html).Value, "value=""" & UniqueID & """")
            writer.Write(html)
        End Sub

        Protected Overrides Function LoadPostData(ByVal postDataKey As String, ByVal postCollection As NameValueCollection) As Boolean
            Dim eventArgument As Object = postCollection(Me.GroupName)
            Dim flag As Boolean = False
            If eventArgument IsNot Nothing AndAlso eventArgument.Equals(Me.UniqueID) Then
                If Not Checked Then
                    Checked = True
                    flag = True
                End If

                Return flag
            End If

            If Checked Then
                Checked = False
            End If

            Return flag
        End Function
    End Class

End Namespace
'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by Refactoring Essentials.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================

