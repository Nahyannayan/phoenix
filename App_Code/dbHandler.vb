Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections


Public Class dbHandler
    Public Sub New()
        ' 
        ' TODO: Add constructor logic here 
        ' 
    End Sub
#Region "Stored Procedure Handlers"


    Private Shared Function paramCountWithOutNull(ByVal _params As String(,)) As Integer
        Dim j As Integer = 0
        For i As Integer = 0 To _params.GetLength(0) - 1
            If _params(i, 0) IsNot Nothing Then
                j += 1
            End If
        Next
        Return j
    End Function

#End Region

#Region "Fetch Methods"


    '''  Fech records use Store Procedure 
    ''' 

    Public Shared Function fetchRecords(ByVal _spName As String, ByVal Kno As Int32, ByVal _dbName As String) As DataTable
        Dim _conn As New SqlConnection()
        _conn = ConnectionManager.getConnection(_dbName)
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandType = CommandType.StoredProcedure
        ' sets the type of command to stored procedure 
        _command.CommandText = _spName
        'sets command text as procedurename to be executed 
        Kno = 0
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function
    '------------------------------ 
    Public Shared Function fetchRecords(ByVal _spName As String, ByVal _params As String(,), ByVal _dataTable As DataTable, ByVal _dbName As String) As DataTable

        Dim _Sqlparam As SqlParameter() = New SqlParameter(paramCountWithOutNull(_params) - 1) {}
        Dim j As Integer = 0
        For i As Integer = 0 To _params.GetLength(0) - 1
            If _params(i, 0) IsNot Nothing Then

                _Sqlparam(j) = New SqlParameter(_params(i, 0), _params(i, 1))
                If _params(i, 1).Equals("") Then
                    _Sqlparam(j).Value = DBNull.Value
                End If
                j += 1
            End If
        Next

        Dim _conn As New SqlConnection()
        _conn = ConnectionManager.getConnection(_dbName)
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandType = CommandType.StoredProcedure
        ' sets the type of command to stored procedure 
        _command.CommandText = _spName
        'sets command text as procedurename to be executed 
        Dim _paramsLength As Integer = _Sqlparam.Length
        'SqlParameter returnMsg = _command.Parameters.Add("v_MessageId", SqlDbType.Int, 4); 
        For i As Integer = 0 To _paramsLength - 1
            'Add values as parameters 
            _command.Parameters.Add(_Sqlparam(i))
        Next
        'returnMsg.Value = 0; 
        ' returnMsg.Direction = ParameterDirection.Output; 

        Dim _adapter As New SqlDataAdapter(_command)
        'DataTable _dataTable = new DataTable("_Tab1"); 
        Try
            _adapter.Fill(_dataTable)
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function
    '------------------------------------------------- 
    Public Shared Function fetchRecords(ByVal _spName As String, ByVal _params As String(,), ByVal _dbName As String) As DataTable

        Dim _Sqlparam As SqlParameter() = New SqlParameter(paramCountWithOutNull(_params) - 1) {}
        Dim j As Integer = 0
        For i As Integer = 0 To _params.GetLength(0) - 1
            If _params(i, 0) IsNot Nothing Then

                _Sqlparam(j) = New SqlParameter(_params(i, 0), _params(i, 1))
                If _params(i, 1).Equals("") Then
                    _Sqlparam(j).Value = DBNull.Value
                End If
                j += 1
            End If
        Next

        Dim _conn As New SqlConnection()
        _conn = ConnectionManager.getConnection(_dbName)
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandType = CommandType.StoredProcedure
        ' sets the type of command to stored procedure 
        _command.CommandText = _spName
        'sets command text as procedurename to be executed 
        Dim _paramsLength As Integer = _Sqlparam.Length
        'SqlParameter returnMsg = _command.Parameters.Add("v_MessageId", SqlDbType.Int, 4); 
        For i As Integer = 0 To _paramsLength - 1
            'Add values as parameters 
            _command.Parameters.Add(_Sqlparam(i))
        Next
        'returnMsg.Value = 0; 
        ' returnMsg.Direction = ParameterDirection.Output; 

        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function

    Public Shared Function fetchRecords(ByVal _query As String, ByVal _dbName As String) As DataTable
        'this is a newly added methods which takes only a query as param 
        'this is mainly used for lists 
        Dim _conn As New SqlConnection()

        _conn = ConnectionManager.getConnection(_dbName)
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function

    Private Shared Function fetchRecords(ByVal _query As String, ByVal _params As SqlTransaction(), ByVal _trans As SqlTransaction) As DataTable
        Dim _command As New SqlCommand()
        _command.CommandText = _query
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable()
        If _params IsNot Nothing Then
            Dim _paramsLength As Integer = _params.Length
            For i As Integer = 0 To _paramsLength - 1
                _command.Parameters.Add(_params(i))
            Next
        End If
        _adapter.Fill(_dataTable)
        _command.Dispose()
        _adapter.Dispose()
        Return _dataTable
    End Function
    Private Shared Function fetchRecords(ByVal _query As String, ByVal _params As SqlTransaction(), ByVal _dbName As String) As DataTable
        Dim _conn As SqlConnection = Nothing
        Dim _trans As SqlTransaction = Nothing
        Dim _dataTable As DataTable = Nothing
        Try
            _conn = ConnectionManager.getConnection(_dbName)
            _trans = _conn.BeginTransaction()
            _dataTable = fetchRecords(_query, _params, _trans)
            _trans.Commit()
        Catch e As Exception
            If _trans IsNot Nothing Then
                _trans.Rollback()
            End If
            Throw e
        Finally
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _trans.Dispose()
                _conn.Close()
                _conn.Dispose()
            End If
        End Try
        Return _dataTable
    End Function
    Private Shared Function fetchRecords(ByVal _query As String, ByVal _trans As SqlTransaction) As DataTable
        Return fetchRecords(_query, New SqlTransaction() {}, _trans)
    End Function

#End Region

#Region "Others"

#End Region


    



    

    

End Class

