﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Data
Imports System.IO
Imports System.Linq
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Net
Imports System.Runtime.Serialization.Json
Imports Newtonsoft.Json
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
<ScriptService()>
Public Class Student_Medical_Service
    Inherits System.Web.Services.WebService
    Public Encr_decrData As New Encryption64

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function CheckSession() As String

        Dim Returnval As String
        Try

            If (HttpContext.Current.Session("sUsr_name") = "" OrElse HttpContext.Current.Session("sUsr_name") Is Nothing) Then
                Returnval = "1"
            Else
                Returnval = "0"
            End If

        Catch ex As Exception
            Returnval = "0"
            UtilityObj.Errorlog(ex.Message, "CheckSession - Medical Module")
        End Try
        Return Returnval
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SearchStudent(ByVal prefixText As String) As List(Of String)


        'ImagePath = ImagePath.TrimEnd().Substring(0, ImagePath.Length - 1)
        Dim strConn As String = ""
        Dim STUTYPE As String = "S"
        Dim studentList As New List(Of String)
        Dim Qry As New StringBuilder
        Qry.Append("EXEC dbo.GLOBAL_SEARCH @bEXCLUDE_CSS_STYLE = 1, @SEARCH_TERM = 'STUDENT', @STU_TYPE = '" & STUTYPE & "', @BSU_ID = '" & System.Web.HttpContext.Current.Session("sBsuid") & "', @PREFIX_TEXT = '" & prefixText.Trim & "'")
        strConn = ConnectionManger.GetOASISConnectionString
        Using conn As New SqlConnection()
            conn.ConnectionString = strConn
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        studentList.Add(sdr("STU_NAME") & " | " & sdr("STU_NO"))
                    End While
                End Using
                conn.Close()
            End Using
            Return studentList
        End Using
    End Function



    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetAiderNames(ByVal prefixText As String) As List(Of String)

        Dim AiderList As New List(Of String)
        Dim strConn As String = ""
        Dim Qry As New StringBuilder
        Qry.Append("EXEC dbo.GET_OTHER_STAFF  @BSU_ID = '" & System.Web.HttpContext.Current.Session("sBsuid") & "', @PREFIX_TEXT = '" & prefixText.Trim & "'")
        strConn = ConnectionManger.GetOASISMedicalConnectionString
        Using conn As New SqlConnection()
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                conn.ConnectionString = strConn
                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        AiderList.Add(sdr("USR_NAME") & " | " & sdr("USR_ID"))
                    End While
                End Using
                conn.Close()
            End Using
            Return AiderList
        End Using
    End Function



    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SAVE_STUDENT_MEDICATION(ByVal MEDICATION_ID As String, ByVal ACTION As String, ByVal STU_ID As String,
        ByVal MEDICATIONCONDITION As String,
        ByVal SIDEEFFECTS As String,
        ByVal DOSAGE_TIME As String,
        ByVal LAST_DOSAGE As String,
        ByVal SPECIALPRECAUTION As String,
        ByVal CONDITION As String,
        ByVal MEDICATION As String,
        ByVal ATTACHMENTS As String
        ) As ServiceResponse

        Dim ReturnVal As New ServiceResponse
        Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
        Dim Serializer = New JavaScriptSerializer()


        Try
            Dim param(13) As SqlParameter

            param(0) = New SqlParameter("@MEDICATION_ID", MEDICATION_ID)
            param(1) = New SqlParameter("@INCIDENT_ID", 0)
            param(2) = New SqlParameter("@MEDICATION", MEDICATION)

            param(3) = New SqlParameter("@MEDICATION_CREATED_USER", HttpContext.Current.Session("sUsr_name"))
            param(4) = New SqlParameter("@STU_ID", STU_ID)
            param(5) = New SqlParameter("@MEDICALCONDITION", Nothing)

            param(6) = New SqlParameter("@SIDEEFFECTS", SIDEEFFECTS)
            param(7) = New SqlParameter("@LASTDOSAGE", If(LAST_DOSAGE.Trim() = "", "", Convert.ToDateTime(LAST_DOSAGE)))

            param(8) = New SqlParameter("@NEXTDOSAGEIFANY", If(DOSAGE_TIME.Trim() = "", "", Convert.ToDateTime(DOSAGE_TIME)))

            param(9) = New SqlParameter("@SPECIALPRECAUTION", SPECIALPRECAUTION)
            param(10) = New SqlParameter("@CONDITION", CONDITION)
            param(11) = New SqlParameter("@ACTION", ACTION)

            param(12) = New SqlClient.SqlParameter("@NEW_MEDICATION_ID", SqlDbType.Int)
            param(12).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[dbo].[SAVE_STUDENT_MEIDICATION]", param)




            If param(12).Value <> "0" Then

                Dim conditionparam(5) As SqlParameter
                conditionparam(0) = New SqlParameter("@MEDICATION_ID", param(12).Value)
                If (ACTION = "UPDATE") Then
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[dbo].[DELETE_MEDICALCONDITIONDETAILS]", conditionparam)
                End If

                Dim ConditionLists() = JsonConvert.DeserializeObject(Of MEDICATIONCONDITIONS())(MEDICATIONCONDITION)
                For Each item As MEDICATIONCONDITIONS In ConditionLists

                    conditionparam(0) = New SqlParameter("@MEDICATION_ID", param(12).Value)
                    conditionparam(1) = New SqlParameter("@NAMEOFMEDICATION", item.NAMEOFMEDICATION)
                    conditionparam(2) = New SqlParameter("@DOSAGEOFMEDICATION", item.DOSAGE)
                    conditionparam(3) = New SqlParameter("@ADMINISTRATION", item.ADMIN)
                    conditionparam(4) = New SqlParameter("@MEDICATIONGIVENON", item.MEDICATIONGIENON.Trim())
                    conditionparam(5) = New SqlParameter("@CREATED_USER", HttpContext.Current.Session("sUsr_name"))
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[dbo].[SAVE_MEDICALCONDITIONDETAILS]", conditionparam)
                Next

                Dim attachmentparam(8) As SqlParameter

                attachmentparam(0) = New SqlParameter("@MEDICATION_ID", param(12).Value)

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[dbo].[DELETE_ATTACHMENT_BY_MEDICATION_ID]", attachmentparam)

                Dim DeserializedATTACHMENTS = Serializer.Deserialize(Of List(Of MEDICATIONATTACHMENTS))(ATTACHMENTS)

                For i As Integer = 0 To DeserializedATTACHMENTS.Count - 1
                    Dim File As MEDICATIONATTACHMENTS = DeserializedATTACHMENTS(i)

                    File.FILEPATH = Encr_decrData.Decrypt(File.FILEPATH)
                    File.STU_NO = STU_ID ' convert into stu_id in sp

                    attachmentparam(0) = New SqlParameter("@MEDICATION_ID", param(12).Value)
                    attachmentparam(1) = New SqlParameter("@STU_NO", File.STU_NO)
                    attachmentparam(3) = New SqlParameter("@ATTACHFILENAME", File.ATTACHFILENAME)
                    attachmentparam(4) = New SqlParameter("@FILEPATH", File.FILEPATH)
                    attachmentparam(5) = New SqlParameter("@CREATED_USER", HttpContext.Current.Session("sUsr_name"))
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[dbo].[SAVE_STU_MEDICATION_ATTACHMENTS]", attachmentparam)
                Next

                ReturnVal.IsSuccess = True
                If (ACTION = "INSERT") Then
                    ReturnVal.Message = "Successfully Inserted"
                Else
                    ReturnVal.Message = "Successfully Updated"
                End If
            End If
        Catch ex As Exception
            ReturnVal.Message = ex.Message
            ReturnVal.IsSuccess = False
            UtilityObj.Errorlog(ex.Message, "SAVE_STUDENT_MEDICATION - Medical Module")
        End Try

        Return ReturnVal
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetMedicalConditionByBSUID(ByVal Grade As String, ByVal Section As String) As List(Of StudMedicationDetail)

        Dim ReturnObj As New List(Of StudMedicationDetail)

        Try

            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", System.Web.HttpContext.Current.Session("sBsuid"))
            param(1) = New SqlParameter("@GRADE", Grade)
            param(2) = New SqlParameter("@SECTION", Section)


            Dim connection As SqlConnection = ConnectionManger.GetOASISMedicalConnection()
            Using readerMedication_Detail As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GET_STUDENT_MEIDICATION_By_BSU", param)

                If readerMedication_Detail.HasRows = True Then

                    While readerMedication_Detail.Read

                        Dim _StudMedicationDetail As New StudMedicationDetail
                        _StudMedicationDetail.STU_ID = readerMedication_Detail("STU_ID").ToString()
                        _StudMedicationDetail.STU_NAME = readerMedication_Detail("STU_NAME").ToString()
                        _StudMedicationDetail.LASTDOSAGE = readerMedication_Detail("LASTDOSAGE_DATE").ToString()
                        _StudMedicationDetail.MEDICALCONDITION = readerMedication_Detail("MEDICALCONDITION").ToString()
                        _StudMedicationDetail.MEDICATION = readerMedication_Detail("MEDICATION").ToString()
                        _StudMedicationDetail.MEDICATION_ID = readerMedication_Detail("MEDICATION_ID").ToString()
                        _StudMedicationDetail.NEXTDOSAGEIFANY = readerMedication_Detail("NEXTDOSAGEIFANY").ToString()
                        _StudMedicationDetail.SIDEEFFECTS = readerMedication_Detail("SIDEEFFECTS").ToString()
                        _StudMedicationDetail.SPECIALPRECAUTIONS = readerMedication_Detail("SPECIALPRECAUTIONS").ToString()
                        _StudMedicationDetail.CONDITIONDESCR = readerMedication_Detail("CONDITIONDESCR").ToString()
                        _StudMedicationDetail.ATTACHMENT = "<i onclick=ViewAttachments('" + readerMedication_Detail("MEDICATION_ID").ToString() + "') class='fa fa-file pr-2 text-primary'></i>"
                        ReturnObj.Add(_StudMedicationDetail)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetMedicalConditionByBSUID - Medical Module")
        End Try
        Return ReturnObj
    End Function



    Public Function SendNotification(ByVal INCIDENT_ID As String, ByVal STU_NO As String)
        Try


            Dim STU_ID As String = ""
            Dim Email As String = ""
            Dim Staff As String = ""
            Dim TUTOR As String = ""
            Dim str_conn_Medical As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim str_conn_Notify As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(6) As SqlParameter
            param(0) = New SqlParameter("@STU_NO", STU_NO)
            param(1) = New SqlParameter("@INCIDENT_ID", INCIDENT_ID)
            Dim readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn_Medical, CommandType.StoredProcedure, "GET_INFO_NOTIFICATION", param)

            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read

                    STU_ID = Convert.ToString(readerStudent_Detail("STU_ID"))
                    If (Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "M") Then
                        Email = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                    ElseIf (Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "F") Then
                        Email = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                    ElseIf (Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "G") Then
                        Email = Convert.ToString(readerStudent_Detail("STS_GEMAIL"))
                    End If
                    Staff = readerStudent_Detail("STAFF").ToString()
                    TUTOR = readerStudent_Detail("TUTOR").ToString()
                End While

                param(0) = New SqlParameter("@STU_ID", STU_ID)
                param(1) = New SqlParameter("@ETM_TYPE", "INCIDENT_LOG")
                param(2) = New SqlParameter("@EMAIL", Email)
                param(3) = New SqlParameter("@INCIDENT_ID", INCIDENT_ID)
                param(4) = New SqlParameter("@EMAILTO", "PARENT")
                param(5) = New SqlParameter("@EMP_ID", Nothing)
                SqlHelper.ExecuteReader(str_conn_Notify, CommandType.StoredProcedure, "UPDATE_INCIDENTEMAILTEMPLATE", param)


                If (TUTOR <> "0" And String.IsNullOrEmpty(TUTOR) = False) Then
                    SendEmail(INCIDENT_ID, TUTOR, STU_ID, "TUTOR")
                End If

                If (String.IsNullOrEmpty(Staff) = False) Then
                    SendEmail(INCIDENT_ID, Staff, STU_ID, "STAFF")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "SendNotification - Medical Module")
        End Try
    End Function

    Public Function SendEmail(ByVal INCIDENT_ID As Int32, ByVal Staff As String, ByVal STU_ID As String, ByVal EmailTo As String)
        Try
            Dim str_conn_Oasis As SqlConnection = ConnectionManger.GetOASISConnection
            Dim str_conn_Medical As SqlConnection = ConnectionManger.GetOASISMedicalConnection

            Dim emp_param(1) As SqlParameter
            emp_param(0) = New SqlParameter("@EMP_ID", Staff)
            Dim readerEMP_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn_Oasis, CommandType.StoredProcedure, "GET_EMP_DETAIL", emp_param)
            If readerEMP_Detail.HasRows = True Then
                While readerEMP_Detail.Read
                    Dim param(5) As SqlParameter
                    param(0) = New SqlParameter("@STU_ID", STU_ID)
                    param(1) = New SqlParameter("@ETM_TYPE", "INCIDENT_LOG")
                    param(2) = New SqlParameter("@EMAIL", readerEMP_Detail("EMAIL").ToString())
                    param(3) = New SqlParameter("@INCIDENT_ID", INCIDENT_ID)
                    param(4) = New SqlParameter("@EMAILTO", EmailTo)
                    param(5) = New SqlParameter("@EMP_ID", readerEMP_Detail("EMD_EMP_ID").ToString())
                    SqlHelper.ExecuteReader(str_conn_Medical, CommandType.StoredProcedure, "UPDATE_INCIDENTEMAILTEMPLATE", param)
                End While
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "SendEmailToStaffNTutor - Medical Module")
        End Try
    End Function



    <System.Web.Script.Services.ScriptMethod(),
     System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function FileUploadHandler()
        Dim path As String

        Try
            Dim File As HttpPostedFile
            File = HttpContext.Current.Request.Files(0)
            Dim Stu_NO = HttpContext.Current.Request.Headers("Stu_NO")
            Dim UploadPath = WebConfigurationManager.AppSettings.Item("MedicationCondition")
            path = UploadPath & Stu_NO & "/"

            If Directory.Exists(path) = False Then
                Directory.CreateDirectory(path)

            End If
            Dim NewFileName As String = Guid.NewGuid().ToString() + "-" + System.IO.Path.GetFileName(File.FileName)
            path = path & NewFileName
            File.SaveAs(path)
            Dim customHeader As New NameValueCollection
            customHeader.Add("originalfilename", System.IO.Path.GetFileName(File.FileName))
            customHeader.Add("filedir", Encr_decrData.Encrypt(Stu_NO & "/" & NewFileName))
            customHeader.Add("contenttype", GetContentType(System.IO.Path.GetExtension(path)))
            HttpContext.Current.Response.Headers.Add(customHeader)

        Catch ex As Exception
            HttpContext.Current.Response.Headers.Add("Error", "Error")
            UtilityObj.Errorlog(ex.Message, "uploadfile - Medical Module")
        End Try

    End Function

    Private Function GetContentType(ByVal FileExtension As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Dim ContentType As String = ""
        Select Case FileExtension
            Case ".doc"
                ContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                ContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                ContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                ContentType = "image/jpg"
                Exit Select
            Case ".png"
                ContentType = "image/png"
                Exit Select
            Case ".gif"
                ContentType = "image/gif"
                Exit Select
            Case ".pdf"
                ContentType = "application/pdf"
                Exit Select
            Case ".txt"
                ContentType = "text/plain"
                Exit Select
        End Select
        Return ContentType
    End Function

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function DownloadFile(ByVal FILE As String) As String

        Dim DownloadPath = WebConfigurationManager.AppSettings.Item("MedicationCondition") & Encr_decrData.Decrypt(FILE)
        If (System.IO.File.Exists(DownloadPath)) Then
            Return Convert.ToBase64String(System.IO.File.ReadAllBytes(DownloadPath))
        Else
            Return "0"
        End If

    End Function


    Public Shared Function GetKeyPairList(ByVal DT As DataTable, ByVal Text As String, ByVal Value As String, Optional ByVal SelectedValue As String = "") As List(Of KeyPairValue)

        Return (From dr In DT.Rows Select New KeyPairValue() With {
                      .Text = dr(Text),
                      .Value = dr(Value)
                      }).ToList()
    End Function

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetOption(ByVal prefixText As String) As List(Of KeyPairValue)

        Dim ReturnVal As New List(Of KeyPairValue)
        Dim ds As DataSet
        Try

            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(2) As SqlParameter

            param(0) = New SqlParameter("@Prefix_Text", prefixText)
            param(1) = New SqlParameter("@BSU_ID", System.Web.HttpContext.Current.Session("sBsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Meidcal_Masters", param)
            Dim view As New DataView(ds.Tables(0))
            If (prefixText = "INJURED") Then
                Dim UPDATEDATATABLE As DataTable = view.ToTable(True, "INJURED_AREA_DESCR", "INJURED_AREA_ID")
                ReturnVal = GetKeyPairList(UPDATEDATATABLE, "INJURED_AREA_DESCR", "INJURED_AREA_ID")
            ElseIf (prefixText = "LOCATION") Then
                Dim UPDATEDATATABLE As DataTable = view.ToTable(True, "LOC_DESCR", "LOC_ID")
                ReturnVal = GetKeyPairList(UPDATEDATATABLE, "LOC_DESCR", "LOC_ID")
            ElseIf (prefixText = "SYMPTOMS") Then
                Dim UPDATEDATATABLE As DataTable = view.ToTable(True, "SYMPTOMS_DESCR", "SYMPTOMS_ID")
                ReturnVal = GetKeyPairList(UPDATEDATATABLE, "SYMPTOMS_DESCR", "SYMPTOMS_ID")
            ElseIf (prefixText = "WHATHAPPENDNEXT") Then
                Dim UPDATEDATATABLE As DataTable = view.ToTable(True, "DESCR", "ID")
                ReturnVal = GetKeyPairList(UPDATEDATATABLE, "DESCR", "ID")
            ElseIf (prefixText = "HOWITHAPPENED") Then
                Dim UPDATEDATATABLE As DataTable = view.ToTable(True, "DESCR", "ID")
                ReturnVal = GetKeyPairList(UPDATEDATATABLE, "DESCR", "ID")
            ElseIf (prefixText = "MED_CONDITION") Then
                Dim UPDATEDATATABLE As DataTable = view.ToTable(True, "DESCR", "ID")
                ReturnVal = GetKeyPairList(UPDATEDATATABLE, "DESCR", "ID")
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetOption - Medical Module")
        End Try
        Return ReturnVal
    End Function

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStaff() As List(Of KeyPairValue)

        Dim ReturnVal As New List(Of KeyPairValue)
        Dim ds As DataSet
        Try

            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(1) As SqlParameter

            param(0) = New SqlParameter("@BSU_ID", System.Web.HttpContext.Current.Session("sBsuid"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetBSUStaff]", param)
            Dim view As New DataView(ds.Tables(0))

            Dim UPDATEDATATABLE As DataTable = view.ToTable(True, "EMPNAME", "emp_id")
            ReturnVal = GetKeyPairList(UPDATEDATATABLE, "EMPNAME", "emp_id")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetStaff - Medical Module")
        End Try
        Return ReturnVal
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetIncidentHistoryByID(ByVal Incident_ID As String) As String

        Dim ReturnVal As String = ""
        Dim ds As DataSet
        Try

            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@INCIDENT_ID", Incident_ID)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_STU_INCIDENT_HISTORY_By_ID", param)

            ReturnVal = GetJson(ds.Tables(0))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetIncidentHistoryByID - Medical Module")
        End Try
        Return ReturnVal
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GET_ATTACHMENTS_BY_ID(ByVal MEDICATION_ID As Int32) As String

        Dim ReturnVal As String = ""
        Dim ds As DataSet
        Try

            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@MEDICATION_ID", MEDICATION_ID)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GET_ATTACHMENTS_BY_ID]", param)
            If (ds.Tables(0).Rows.Count = 0) Then
                Return ""
            End If
            Dim dcContentType = New DataColumn("CONTENTTYPE", GetType(String))
            ds.Tables(0).Columns.Add(dcContentType)

            For Each row As DataRow In ds.Tables(0).Rows
                row.Item("CONTENTTYPE") = GetContentType(System.IO.Path.GetExtension(WebConfigurationManager.AppSettings.Item("MedicationCondition") & row.Item("FILEPATH").ToString()))
                row.Item("FILEPATH") = Encr_decrData.Encrypt(row.Item("FILEPATH").ToString())

            Next row
            ReturnVal = GetJson(ds.Tables(0))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GET_ATTACHMENTS_BY_ID - Medical Module")
        End Try
        Return ReturnVal
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetMedicalConditionByID(ByVal MEDICATION_ID As String) As String

        Dim ReturnVal As String = ""
        Dim ds As DataSet
        Try

            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@Medication_ID", MEDICATION_ID)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_STU_MEDICATION_CONDITION_By_ID", param)

            Dim DTMEDICATION = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_MEDICALCONDITIONDETAILS_BY_MID", param)
            Dim MEDICATIONLIST As List(Of MEDICATIONCONDITIONS) = New List(Of MEDICATIONCONDITIONS)()
            MEDICATIONLIST = (From dr In DTMEDICATION.Tables(0).Rows Select New MEDICATIONCONDITIONS() With {
            .ADMIN = Convert.ToInt32(dr("ADMINISTRATION")),
            .DOSAGE = dr("DOSAGEOFMEDICATION").ToString(),
            .MEDICATIONGIENON = Convert.ToDateTime(dr("MEDICATIONGIVENON")).ToString("dd/MMM/yyyy"),
            .NAMEOFMEDICATION = dr("NAMEOFMEDICATION").ToString()
             }).ToList()

            ds.Tables(0).Rows(0)("MEDICALCONDITION") = JsonConvert.SerializeObject(MEDICATIONLIST)
            ReturnVal = GetJson(ds.Tables(0))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetMedicalConditionByID - Medical Module")
        End Try
        Return ReturnVal
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetIncidentHistoryByBSUID(ByVal Grade As String, ByVal Section As String) As List(Of StudIncidentDetail)

        Dim ReturnObj As New List(Of StudIncidentDetail)

        Try

            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", System.Web.HttpContext.Current.Session("sBsuid"))
            param(1) = New SqlParameter("@GRADE", Grade)
            param(2) = New SqlParameter("@SECTION", Section)


            Dim connection As SqlConnection = ConnectionManger.GetOASISMedicalConnection()
            Using readerIncident_Detail As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GET_STU_INCIDENT_HISTORY_By_BSU", param)

                If readerIncident_Detail.HasRows = True Then

                    While readerIncident_Detail.Read

                        Dim _StudIncidentDetail As New StudIncidentDetail
                        _StudIncidentDetail.STUDENT_NAME = readerIncident_Detail("STU_NAME").ToString()
                        _StudIncidentDetail.FIRSTAIDER = readerIncident_Detail("AIDER_NAME").ToString()
                        _StudIncidentDetail.SYMPTOMS_DESCR = readerIncident_Detail("SYMPTOMS_DESCR").ToString()
                        _StudIncidentDetail.LOC_DESCR = readerIncident_Detail("LOC_DESCR").ToString()
                        _StudIncidentDetail.INJURED_AREA_DESCR = readerIncident_Detail("INJURED_AREA_DESCR").ToString()
                        _StudIncidentDetail.WHAHAPPENDNEXT = readerIncident_Detail("WHAT_HAPPENED_NEXT").ToString()
                        _StudIncidentDetail.INCIDENT_DATE = readerIncident_Detail("INCIDENT_DATE")
                        _StudIncidentDetail.NOTIFY = "<i onclick=OpenNotifyDetails('" + readerIncident_Detail("INCIDENT_ID").ToString() + "') class='fa fa-envelope-square text-primary'></i>"
                        _StudIncidentDetail.ATTACHMENT = ""
                        ReturnObj.Add(_StudIncidentDetail)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetIncidentHistoryByBSUID - Medical Module")
        End Try
        Return ReturnObj
    End Function

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStudent_Detail(ByVal STU_ID As String) As String

        Dim ReturnObj As New Student_Medical_Info
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection

        Try
            Dim STR As String = "exec [STU].[GET_STU_PROFILE_DETAILS_BY_ID] " + STU_ID + ""
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, STR)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ReturnObj.STU_NAME = Convert.ToString(readerStudent_Detail("Sname"))
                        ReturnObj.STU_NO = Convert.ToString(readerStudent_Detail("stu_no"))
                        ReturnObj.STU_AGE = GetCurrentAge(Convert.ToDateTime(readerStudent_Detail("STU_DOB")))
                        ReturnObj.STU_PHOTO = Convert.ToString(readerStudent_Detail("PHOTOPATH"))
                        ReturnObj.STU_RELIGION = Convert.ToString(readerStudent_Detail("RLG_ID"))
                        ReturnObj.STU_GRD = Convert.ToString(readerStudent_Detail("grm_display")) & "-" & Convert.ToString(readerStudent_Detail("sct_descr"))
                        ReturnObj.STU_DOB = If(String.IsNullOrEmpty(readerStudent_Detail("STU_DOB").ToString()), "-", Convert.ToDateTime(readerStudent_Detail("STU_DOB")).ToString("dd/MMM/yyyy"))
                        ReturnObj.STU_GENDER = Convert.ToString(readerStudent_Detail("STU_GENDER"))

                    End While
                End If
            End Using


            Dim param(1) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", STU_ID)

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "stu_contactDetails", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        If readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "F" Then
                            ReturnObj.STU_RELATION = "Father"
                            ReturnObj.STU_RNAME = If(String.IsNullOrEmpty(readerStudent_Detail("STS_FNAME").ToString()), "-", readerStudent_Detail("STS_FNAME").ToString())
                            ReturnObj.STU_RCompany = If(String.IsNullOrEmpty(readerStudent_Detail("STS_FCOMPANY").ToString()), "-", readerStudent_Detail("STS_FCOMPANY").ToString())
                            ReturnObj.STU_ROccupation = If(String.IsNullOrEmpty(readerStudent_Detail("STS_FOCC").ToString()), "-", readerStudent_Detail("STS_FOCC").ToString())
                            ReturnObj.STU_RContactNumber = If(String.IsNullOrEmpty(readerStudent_Detail("STS_FMOBILE").ToString()), "-", readerStudent_Detail("STS_FMOBILE").ToString())
                            ReturnObj.STU_RCity = If(String.IsNullOrEmpty(readerStudent_Detail("STS_FPRMCITY").ToString()), "-", readerStudent_Detail("STS_FPRMCITY").ToString())
                            ReturnObj.STU_RAddress = If(String.IsNullOrEmpty(readerStudent_Detail("STS_FPRMADDR1").ToString()), "-", readerStudent_Detail("STS_FPRMADDR1").ToString())
                            ReturnObj.STU_REmail = If(String.IsNullOrEmpty(readerStudent_Detail("STS_FEMAIL").ToString()), "-", readerStudent_Detail("STS_FEMAIL").ToString())
                            ReturnObj.STU_RNationality = If(String.IsNullOrEmpty(readerStudent_Detail("FNATIONALITY").ToString()), "-", readerStudent_Detail("FNATIONALITY").ToString())
                            'ReturnObj.STU_ECContactNumber = If(String.IsNullOrEmpty(readerStudent_Detail("STU_EMGCONTACT").ToString()), "-", readerStudent_Detail("STU_EMGCONTACT").ToString())
                            'ReturnObj.STU_ECContactName = If(String.IsNullOrEmpty(readerStudent_Detail("STU_EMGCONTACT_NAME").ToString()), "-", readerStudent_Detail("STU_EMGCONTACT_NAME").ToString())
                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "M" Then
                            ReturnObj.STU_RELATION = "Mother"
                            ReturnObj.STU_RNAME = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MNAME").ToString()), "-", readerStudent_Detail("STS_MNAME").ToString())
                            ReturnObj.STU_RCompany = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MCOMPANY").ToString()), "-", readerStudent_Detail("STS_MCOMPANY").ToString())
                            ReturnObj.STU_ROccupation = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MOCC").ToString()), "-", readerStudent_Detail("STS_MOCC").ToString())
                            ReturnObj.STU_RContactNumber = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MMOBILE").ToString()), "-", readerStudent_Detail("STS_MMOBILE").ToString())
                            ReturnObj.STU_RNationality = If(String.IsNullOrEmpty(readerStudent_Detail("MNATIONALITY").ToString()), "-", readerStudent_Detail("MNATIONALITY").ToString())
                            ReturnObj.STU_RCountry = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MPRMCOUNTRY").ToString()), "-", readerStudent_Detail("STS_MPRMCOUNTRY").ToString())
                            ReturnObj.STU_RCity = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MPRMCITY").ToString()), "-", readerStudent_Detail("STS_MPRMCITY").ToString())
                            ReturnObj.STU_RAddress = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MPRMADDR1").ToString()), "-", readerStudent_Detail("STS_MPRMADDR1").ToString())
                            ReturnObj.STU_REmail = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MEMAIL").ToString()), "-", readerStudent_Detail("STS_MEMAIL").ToString())
                            'ReturnObj.STU_ECContactNumber = If(String.IsNullOrEmpty(readerStudent_Detail("STU_EMGCONTACT").ToString()), "-", readerStudent_Detail("STU_EMGCONTACT").ToString())
                            'ReturnObj.STU_ECContactName = If(String.IsNullOrEmpty(readerStudent_Detail("STU_EMGCONTACT_NAME").ToString()), "-", readerStudent_Detail("STU_EMGCONTACT_NAME").ToString())
                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "G" Then
                            ReturnObj.STU_RELATION = "Guardian"
                            ReturnObj.STU_RNAME = If(String.IsNullOrEmpty(readerStudent_Detail("STS_GNAME").ToString()), "-", readerStudent_Detail("STS_GNAME").ToString())
                            ReturnObj.STU_RCompany = If(String.IsNullOrEmpty(readerStudent_Detail("STS_GCOMPANY").ToString()), "-", readerStudent_Detail("STS_GCOMPANY").ToString())
                            ReturnObj.STU_ROccupation = If(String.IsNullOrEmpty(readerStudent_Detail("STS_MOCC").ToString()), "-", readerStudent_Detail("STS_MOCC").ToString())
                            ReturnObj.STU_RContactNumber = If(String.IsNullOrEmpty(readerStudent_Detail("STS_GOCC").ToString()), "-", readerStudent_Detail("STS_GOCC").ToString())
                            ReturnObj.STU_RNationality = If(String.IsNullOrEmpty(readerStudent_Detail("GNATIONALITY").ToString()), "-", readerStudent_Detail("GNATIONALITY").ToString())
                            ReturnObj.STU_RCountry = If(String.IsNullOrEmpty(readerStudent_Detail("STS_GPRMCOUNTRY").ToString()), "-", readerStudent_Detail("STS_GPRMCOUNTRY").ToString())
                            ReturnObj.STU_RCity = If(String.IsNullOrEmpty(readerStudent_Detail("STS_GPRMCITY").ToString()), "-", readerStudent_Detail("STS_GPRMCITY").ToString())
                            ReturnObj.STU_RAddress = If(String.IsNullOrEmpty(readerStudent_Detail("STS_GPRMADDR1").ToString()), "-", readerStudent_Detail("STS_GPRMADDR1").ToString())
                            ReturnObj.STU_REmail = If(String.IsNullOrEmpty(readerStudent_Detail("STS_GEMAIL").ToString()), "-", readerStudent_Detail("STS_GEMAIL").ToString())
                            'ReturnObj.STU_ECContactNumber = If(String.IsNullOrEmpty(readerStudent_Detail("STU_EMGCONTACT").ToString()), "-", readerStudent_Detail("STU_EMGCONTACT").ToString())
                            'ReturnObj.STU_ECContactName = If(String.IsNullOrEmpty(readerStudent_Detail("STU_EMGCONTACT_NAME").ToString()), "-", readerStudent_Detail("STU_EMGCONTACT_NAME").ToString())
                        End If
                    End While
                End If
            End Using
            param(0) = New SqlClient.SqlParameter("@stu_id", STU_ID)
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "Get_HealthInfo_bystudentId", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ReturnObj.STU_bALLERGIES = readerStudent_Detail("Allergies").ToString()
                        ReturnObj.STU_ALLERGIES = readerStudent_Detail("AllergiesNotes").ToString()

                        ReturnObj.STU_bRCVSPMEDICATION = readerStudent_Detail("Medication").ToString()
                        ReturnObj.STU_SPMEDICATION = readerStudent_Detail("Medication Notes").ToString()

                        ReturnObj.STU_bPHYSICAL = readerStudent_Detail("PhysicalEducationRestrictions").ToString()
                        ReturnObj.STU_PHYSICAL = readerStudent_Detail("Physical EducationRestrictionsNotes").ToString()

                        ReturnObj.STU_bDisabled = readerStudent_Detail("Disabilities").ToString()
                        ReturnObj.STU_Disabled = readerStudent_Detail("DisabilitiesNotes").ToString()

                        ReturnObj.STU_bHEALTH = readerStudent_Detail("Any other information related to health?").ToString()
                        ReturnObj.STU_HEALTH = readerStudent_Detail("Other information related to health note").ToString()

                        ReturnObj.STU_bSEN = readerStudent_Detail("specialeducationneeds").ToString()
                        ReturnObj.STU_SEN_REMARK = readerStudent_Detail("SpecialEducationNeedsNote").ToString()

                    End While
                End If
            End Using
            param(0) = New SqlClient.SqlParameter("@stu_id", STU_ID)
            Dim connection As SqlConnection = ConnectionManger.GetOASISMedicalConnection()
            Using readerIncident_Detail As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GET_STU_INCIDENT_HISTORY", param)

                If readerIncident_Detail.HasRows = True Then
                    ReturnObj.StudIncidentDetail = New Generic.List(Of StudIncidentDetail)
                    While readerIncident_Detail.Read

                        Dim _StudIncidentDetail As New StudIncidentDetail
                        _StudIncidentDetail.INCIDENT_ID = readerIncident_Detail("ID").ToString()
                        _StudIncidentDetail.CATEGORY = readerIncident_Detail("CAT_DESC").ToString()
                        _StudIncidentDetail.SUB_CATEGORY = readerIncident_Detail("SCAT_DESC").ToString()
                        _StudIncidentDetail.SYMPTOMS_DESCR = readerIncident_Detail("SYMPTOMS_DESCR").ToString()
                        _StudIncidentDetail.LOC_DESCR = readerIncident_Detail("LOC_DESCR").ToString()
                        _StudIncidentDetail.INJURED_AREA_DESCR = readerIncident_Detail("INJURED_AREA_DESCR").ToString()
                        _StudIncidentDetail.INCIDENT_DATE = Convert.ToDateTime(readerIncident_Detail("INCIDENT_DATE")).ToString("dd/MMM/yyyy hh:mm tt")
                        ReturnObj.StudIncidentDetail.Add(_StudIncidentDetail)

                    End While
                End If
            End Using

            Using readerMedication_Detail As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "Get_Student_Medication", param)
                If readerMedication_Detail.HasRows = True Then
                    ReturnObj.StudMedicationDetail = New Generic.List(Of StudMedicationDetail)
                    While readerMedication_Detail.Read
                        Dim _StudMedicationDetail As New StudMedicationDetail
                        _StudMedicationDetail.STU_ID = readerMedication_Detail("STU_ID").ToString()
                        ' _StudMedicationDetail.MEDICALCONDITION = readerMedication_Detail("MEDICALCONDITION").ToString()
                        _StudMedicationDetail.MEDICATION = readerMedication_Detail("MEDICATION").ToString()
                        _StudMedicationDetail.MEDICATION_ID = readerMedication_Detail("MEDICATION_ID").ToString()
                        _StudMedicationDetail.SIDEEFFECTS = readerMedication_Detail("SIDEEFFECTS").ToString()
                        _StudMedicationDetail.SPECIALPRECAUTIONS = readerMedication_Detail("SPECIALPRECAUTIONS").ToString()
                        _StudMedicationDetail.CONDITIONDESCR = readerMedication_Detail("CONDITIONDESCR").ToString()
                        ReturnObj.StudMedicationDetail.Add(_StudMedicationDetail)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetStudent_Detail - Medical Module")
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim ReturnValue = serializer.Serialize(ReturnObj)
        Return ReturnValue
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Get_Grade() As List(Of KeyPairValue)

        Dim ReturnVal As New List(Of KeyPairValue)
        Dim ds As DataSet
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String


            str_Sql = " SELECT  distinct GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER ,GRADE_M.GRD_DISPLAY_ARABIC  FROM GRADE_BSU_M INNER JOIN " &
                      " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ReturnVal = GetKeyPairList(ds.Tables(0), "GRM_DISPLAY", "GRD_ID")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Get_Grade - Medical Module")
        End Try
        Return ReturnVal
    End Function

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Get_Section(ByVal GRD_ID As String) As List(Of KeyPairValue)

        Dim ReturnVal As New List(Of KeyPairValue)
        Dim ds As DataSet
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM  OASIS.dbo.SECTION_M  WHERE  SCT_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' AND  SCT_ACY_ID = " & Session("Current_ACY_ID") & "  AND SCT_GRD_ID ='" & GRD_ID & "' ORDER BY SCT_DESCR "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ReturnVal = GetKeyPairList(ds.Tables(0), "SCT_DESCR", "SCT_ID")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Get_Section - Medical Module")
        End Try
        Return ReturnVal
    End Function

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SAVE_STUDENT_HISTORY(ByVal ID As String, ByVal ACTION As String, ByVal INCIDENT_DATE As String,
        ByVal LOC_ID As String,
        ByVal SYMPTOMS_ID As String,
        ByVal INJURED_AREA_ID As String,
        ByVal WHAT_HAPPENED_NEXT As String,
        ByVal NAME_OF_MEDICATION As String,
        ByVal DOSAGE_OF_MEDICATION As String,
        ByVal ADMINISTRATION As String,
        ByVal MEDICATION_GIVE_ON As String,
        ByVal SPECIAL_PRECUATIONS As String,
        ByVal INJURY_DESC As String,
        ByVal HOW_IT_HAPPENED As String,
        ByVal TREATED_ADMIN As String,
        ByVal AIDER_NAME As String,
        ByVal SIDE_EFFECTS As String,
        ByVal NEXTDOSAGEDATE As String,
        ByVal FOLLOWUPDATE As String,
        ByVal NOTIFYCLASSTUTOR As Boolean,
        ByVal NOTIFYPARENT As Boolean,
        ByVal NOTIFYOTHERSTAFF As String,
          ByVal STAFF As String, ByVal STU_ID As String, ByVal OTHERAIDER As Boolean) As ServiceResponse

        Dim ReturnVal As New ServiceResponse

        Try
            Dim str_conn As SqlConnection = ConnectionManger.GetOASISMedicalConnection

            Dim param(27) As SqlParameter

            param(0) = New SqlParameter("@ID", ID)
            param(1) = New SqlParameter("@ACTION", ACTION)

            param(2) = New SqlParameter("@LOC_ID", LOC_ID)
            param(3) = New SqlParameter("@SYMPTOMS_ID", SYMPTOMS_ID)
            param(4) = New SqlParameter("@INJURED_AREA_ID", INJURED_AREA_ID)
            param(5) = New SqlParameter("@WHAT_HAPPENED_NEXT", WHAT_HAPPENED_NEXT)
            param(6) = New SqlParameter("@NAME_OF_MEDICATION", NAME_OF_MEDICATION)
            param(7) = New SqlParameter("@DOSAGE_OF_MEDICATION", DOSAGE_OF_MEDICATION)
            param(8) = New SqlParameter("@ADMINISTRATION", ADMINISTRATION)

            param(9) = New SqlParameter("@MEDICATION_GIVE_ON", If(MEDICATION_GIVE_ON.Trim() = "", "", Convert.ToDateTime(MEDICATION_GIVE_ON)))
            param(10) = New SqlParameter("@SPECIAL_PRECUATIONS", SPECIAL_PRECUATIONS)
            param(11) = New SqlParameter("@INJURY_DESC", INJURY_DESC)
            param(12) = New SqlParameter("@HOW_IT_HAPPENED", HOW_IT_HAPPENED)
            param(13) = New SqlParameter("@AIDER_NAME", AIDER_NAME)
            param(14) = New SqlParameter("@SIDE_EFFECTS", SIDE_EFFECTS)
            param(15) = New SqlParameter("@NEXTDOSAGEDATE", If(NEXTDOSAGEDATE.Trim() = "", "", Convert.ToDateTime(NEXTDOSAGEDATE)))
            param(16) = New SqlParameter("@FOLLOWUPDATE", If(FOLLOWUPDATE.Trim() = "", "", Convert.ToDateTime(FOLLOWUPDATE)))
            param(17) = New SqlParameter("@NOTIFYCLASSTUTOR", NOTIFYCLASSTUTOR)
            param(18) = New SqlParameter("@NOTIFYPARENT", NOTIFYPARENT)
            param(19) = New SqlParameter("@NOTIFYOTHERSTAFF", NOTIFYOTHERSTAFF)
            param(20) = New SqlParameter("@STAFF", STAFF)
            param(21) = New SqlParameter("@INCIDENT_DATE", Convert.ToDateTime(INCIDENT_DATE))
            param(22) = New SqlParameter("@TREATED_ADMIN", TREATED_ADMIN)
            param(23) = New SqlParameter("@STU_ID", STU_ID)
            param(24) = New SqlParameter("@USER", HttpContext.Current.Session("sUsr_name"))
            param(25) = New SqlParameter("@OTHERAIDER", OTHERAIDER)
            param(26) = New SqlClient.SqlParameter("@NEW_INCIDENT_ID", SqlDbType.Int)
            param(26).Direction = ParameterDirection.Output
            Dim INCIDENT_ID = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_STUDENT_HISTORY", param)
            ReturnVal.IsSuccess = True
            If (ACTION = "INSERT") Then

                SendNotification(param(26).Value, STU_ID)
                ReturnVal.Message = "Successfully Inserted"

            Else
                ReturnVal.Message = "Successfully Updated"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "SAVE_STUDENT_HISTORY - Medical Module")
            ReturnVal.Message = ex.Message
            ReturnVal.IsSuccess = False

        End Try
        Return ReturnVal
    End Function
    Public Shared Function GetCurrentAge(ByVal dob As Date) As Integer
        Dim age As Integer
        age = Today.Year - dob.Year
        If (dob > Today.AddYears(-age)) Then age -= 1
        Return age
    End Function
    Public Function GetJson(ByVal dt As DataTable) As String
        Dim serializer As System.Web.Script.Serialization.JavaScriptSerializer = New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object) = Nothing

        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)()

            For Each col As DataColumn In dt.Columns
                row.Add(col.ColumnName, dr(col))
            Next

            rows.Add(row)
        Next

        Return serializer.Serialize(rows)
    End Function

    Private Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & Title & Extension)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.[End]()
    End Sub
    Private Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "File Conversion - Medical Module")
        End Try
    End Function


    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GET_INCIDENT_NOTIFICATION_BY_ID(ByVal Incident_ID As Int32) As String

        Try

            Dim ParentName As String = ""
            Dim con As String = ConnectionManger.GetOASISMedicalConnectionString
            Dim param(1) As SqlClient.SqlParameter
            param(0) = New SqlParameter("@INCIDENT_ID", Incident_ID)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "dbo.GET_INCIDENT_NOTIFICATION_EMAIL", param)

            If (Convert.ToString(ds.Tables(1).Rows(0)("STU_PRIMARYCONTACT")) = "M") Then
                ParentName = ds.Tables(1).Rows(0)("STS_MNAME")
            ElseIf (Convert.ToString(ds.Tables(1).Rows(0)("STU_PRIMARYCONTACT")) = "F") Then
                ParentName = ds.Tables(1).Rows(0)("STS_FNAME")
            ElseIf (Convert.ToString(ds.Tables(1).Rows(0)("STU_PRIMARYCONTACT")) = "G") Then
                ParentName = ds.Tables(1).Rows(0)("STS_GNAME")
            End If
            For Each row As DataRow In ds.Tables(0).Rows
                If (row.Item("EMAILTO").ToString() = "PARENT") Then
                    row.Item("Name") = ParentName
                End If
            Next row

            Return GetJson(ds.Tables(0))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GET_INCIDENT_NOTIFICATION_BY_ID - Medical Module")
        End Try
    End Function

    <WebMethod(EnableSession:=True), ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Get_Students(ByVal Grade As String, ByVal Section As String) As String

        Try
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlParameter("@PREFIX_TEXT ", "")
            param(1) = New SqlParameter("@BSU_ID", HttpContext.Current.Session("sBsuid"))
            param(2) = New SqlParameter("@GRADE", Grade)
            param(3) = New SqlParameter("@SECTION", Section)
            param(4) = New SqlParameter("@ACY_ID", HttpContext.Current.Session("Current_ACD_ID"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "dbo.GET_STU_MEDICAL_DETAILS", param)
            Dim dcName = New DataColumn("Encr_STU_ID", GetType(String))
            ds.Tables(0).Columns.Add(dcName)

            Dim dcMedicalCondition = New DataColumn("MedicalCondition", GetType(String))
            ds.Tables(0).Columns.Add(dcMedicalCondition)

            Dim dcIncidentReport = New DataColumn("IncidentReport", GetType(String))
            ds.Tables(0).Columns.Add(dcIncidentReport)

            For Each row As DataRow In ds.Tables(0).Rows
                row("Encr_STU_ID") = Encr_decrData.Encrypt(row("STU_ID").ToString.Trim)
                If (Convert.ToInt32(row("MEDICATIONCOUNT").ToString) > 0) Then
                    row("MedicalCondition") = "<span class='float-left text-danger p-2 medical-tag-bg' data-toggle='tooltip' title='Medical Condition' data-placement='right'><i class='fas fa-user-md'><span class='notification-count-left'>" + row("MEDICATIONCOUNT").ToString + "</span></i></span>"
                End If
                If (Convert.ToInt32(row("INCIDENTCOUNT").ToString) > 0) Then
                    row("IncidentReport") = "<span class='float-right text-warning p-2 medical-tag-bg' data-toggle='tooltip' title='Incidents Reported' data-placement='left'><i class='fas fa-pills'><span  class='notification-count-right'>" + row("INCIDENTCOUNT").ToString + "</span></i></span>"
                End If
            Next row
            Return GetJson(ds.Tables(0))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Get_Students - Medical Module")
        End Try
    End Function


    Public Class KeyPairValue

        Public Text As String
        Public Value As String

    End Class

    Public Class ServiceResponse

        Public Message As String
        Public IsSuccess As Boolean

    End Class

    Public Class MEDICATIONATTACHMENTS

        Public MEDICATION_ID As String
        Public STU_NO As String
        Public ATTACHFILENAME As String
        Public FILEPATH As String
        Public ACTION As String

    End Class

    Public Class MEDICATIONCONDITIONS

        Public NAMEOFMEDICATION As String
        Public DOSAGE As String
        Public ADMIN As String
        Public MEDICATIONGIENON As String

    End Class



End Class