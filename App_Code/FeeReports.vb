Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class FeeReports

    
    Public Shared Function StudentCountTransport(ByVal DocDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("Students_Count")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRMDT As New SqlParameter("@DDATE", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(DocDate)
        cmd.Parameters.Add(sqlpFRMDT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable

        params("userName") = HttpContext.Current.Session("sUsr_name")
        params("ReportHead") = " STUDENTS COUNT ( TRANSPORT ) AS ON : " & Convert.ToDateTime(DocDate).ToString("dd/MMM/yyyy")

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = BSuId
        repSource.ResourceName = "../../FEES/REPORTS/RPT/StudentCountChart.rpt"
        'repSource.ResourceName = "../FEES/REPORTS/RPT/StudentCountChart.rpt"

        Return repSource

    End Function


End Class
