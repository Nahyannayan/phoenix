﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Script.Services
Imports System.Web.Services


''' <summary>
''' Summary description for AjaxServices
''' </summary>
<WebService([Namespace]:="http://tempuri.org/"), WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1), System.Web.Script.Services.ScriptService>
Public Class AjaxServices
    Inherits System.Web.Services.WebService

    ''' <summary>
    ''' Handle to the long-running work processor.
    ''' </summary>
    Private Shared workProcessor As New FeeDayendProcess()

    <WebMethod, ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Function StartProcess() As String
        ' Non-blocking - kick off the long-running process and return immediately.
        workProcessor.StartProcess()

        Return "OK"
    End Function
    <WebMethod, ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Function InitProgress(ByVal prbsuid As String, ByVal stbsuid As String, ByVal chDate As String, ByVal bTransport As Boolean) As String
        ' Non-blocking - kick off the long-running process and return immediately.
        workProcessor.InitProgress(prbsuid, stbsuid, chDate, bTransport)
        Return ""
    End Function
    Public Function EndProcess(ByVal status As Boolean, ByVal msg As String) As String
        ' Non-blocking - kick off the long-running process and return immediately.
        workProcessor.EndProcess(status, msg)

        Return "OK"
    End Function

    Public Function UpdatePercentage(ByVal newPercentComplete As Double, ByVal workDescription As String) As String
        ' Non-blocking - kick off the long-running process and return immediately.
        workProcessor.UpdatePercent(newPercentComplete, workDescription)

        Return "OK"
    End Function


    <WebMethod, ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStatus() As FeeDayendProcess
        If workProcessor IsNot Nothing AndAlso workProcessor.IsRunning Then
            ' Return a dynamic object, jQuery will read the properties
            If workProcessor.IsTransport Then
                workProcessor.GetProgress()
            End If
            Return workProcessor
        Else
            ' If null, the client knows it's not running/not initialized.
            Return Nothing
        End If
    End Function
End Class
