Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.ComponentModel
Imports System.Collections
''' <summary> 
''' Summary description for DynamicTemplate 
''' </summary> 
Public Class DynamicTemplate
    Implements System.Web.UI.ITemplate

    Private templateType As System.Web.UI.WebControls.ListItemType
    Private htControls As New System.Collections.Hashtable()
    Private htBindPropertiesNames As New System.Collections.Hashtable()
    Private htBindExpression As New System.Collections.Hashtable()

    Public Sub New(ByVal type As System.Web.UI.WebControls.ListItemType)
        templateType = type
    End Sub

    Public Sub AddControl(ByVal wbControl As WebControl, ByVal BindPropertyName As [String], ByVal BindExpression As [String])
        htControls.Add(htControls.Count, wbControl)
        htBindPropertiesNames.Add(htBindPropertiesNames.Count, BindPropertyName)
        htBindExpression.Add(htBindExpression.Count, BindExpression)
    End Sub

    Public Sub InstantiateIn(ByVal container As System.Web.UI.Control)
        Dim ph As New PlaceHolder()

        For i As Integer = 0 To htControls.Count - 1

            'clone control 
            Dim cntrl As Control = CloneControl(DirectCast(htControls(i), Control))

            Select Case templateType
                Case ListItemType.Header
                    Exit Select
                Case ListItemType.Item
                    ph.Controls.Add(cntrl)
                    Exit Select
                Case ListItemType.AlternatingItem
                    ph.Controls.Add(cntrl)
                    AddHandler ph.DataBinding, AddressOf Item_DataBinding
                    Exit Select
                Case ListItemType.Footer
                    Exit Select
            End Select
        Next
        AddHandler ph.DataBinding, AddressOf Item_DataBinding


        container.Controls.Add(ph)
    End Sub
    Public Sub Item_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ph As PlaceHolder = DirectCast(sender, PlaceHolder)
        Dim ri As GridViewRow = DirectCast(ph.NamingContainer, GridViewRow)
        Try
            For i As Integer = 0 To htControls.Count - 1
                If htBindPropertiesNames(i).ToString().Length > 0 Then
                    Dim tmpCtrl As Control = DirectCast(htControls(i), Control)
                    Dim item1Value As [String] = DirectCast(DataBinder.Eval(ri.DataItem, htBindExpression(i).ToString()), [String])
                    Dim ctrl As Control = ph.FindControl(tmpCtrl.ID)
                    Dim t As Type = ctrl.[GetType]()

                    If ctrl.GetType.Name.ToString.ToLower <> "dropdownlist" Then
                        Dim pi As System.Reflection.PropertyInfo = t.GetProperty(htBindPropertiesNames(i).ToString())
                        pi.SetValue(ctrl, item1Value.ToString(), Nothing)
                    Else
                        Dim dr As DropDownList
                        dr = DirectCast(ctrl, DropDownList)
                        Dim pi As System.Reflection.PropertyInfo = t.GetProperty("SelectedValue")
                        pi.SetValue(ctrl, HttpContext.Current.Session("ddlListSelValue"), Nothing)
                    End If
                    'Dim pi As System.Reflection.PropertyInfo = t.GetProperty(htBindPropertiesNames(i).ToString())

                    'pi.SetValue(ctrl, item1Value.ToString(), Nothing)

                End If

            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Function CloneControl(ByVal src_ctl As System.Web.UI.Control) As Control
        Dim t As Type = src_ctl.[GetType]()
        Dim obj As [Object] = Activator.CreateInstance(t)
        Dim dst_ctl As Control = DirectCast(obj, Control)
        Dim src_pdc As PropertyDescriptorCollection = TypeDescriptor.GetProperties(src_ctl)
        Dim dst_pdc As PropertyDescriptorCollection = TypeDescriptor.GetProperties(dst_ctl)

        For i As Integer = 0 To src_pdc.Count - 1

            Try
                If src_pdc(i).Attributes.Contains(DesignerSerializationVisibilityAttribute.Content) Then

                    Dim collection_val As Object = src_pdc(i).GetValue(src_ctl)
                    If (TypeOf collection_val Is IList) = True Then
                        For Each child As Object In DirectCast(collection_val, IList)
                            Dim new_child As Control = CloneControl(TryCast(child, Control))
                            Dim dst_collection_val As Object = dst_pdc(i).GetValue(dst_ctl)
                            DirectCast(dst_collection_val, IList).Add(new_child)
                        Next
                    End If
                Else
                    dst_pdc(src_pdc(i).Name).SetValue(dst_ctl, src_pdc(i).GetValue(src_ctl))
                End If
            Catch ex As Exception

            End Try
        Next


        Return dst_ctl
    End Function

    Public Sub InstantiateIn1(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
        Dim ph As New PlaceHolder()

        For i As Integer = 0 To htControls.Count - 1

            'clone control 
            Dim cntrl As Control = CloneControl(DirectCast(htControls(i), Control))

            Select Case templateType
                Case ListItemType.Header
                    Exit Select
                Case ListItemType.Item
                    ph.Controls.Add(cntrl)
                    Exit Select
                Case ListItemType.AlternatingItem
                    ph.Controls.Add(cntrl)
                    AddHandler ph.DataBinding, AddressOf Item_DataBinding
                    Exit Select
                Case ListItemType.Footer
                    Exit Select
            End Select
        Next
        AddHandler ph.DataBinding, AddressOf Item_DataBinding


        container.Controls.Add(ph)
    End Sub
End Class