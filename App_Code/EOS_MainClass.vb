﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class EOS_MainClass
    Public Shared Function GetEmployeeIDFromUserName(ByVal UserName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = "SELECT EMPLOYEE_M.EMP_ID FROM USERS_M inner JOIN EMPLOYEE_M ON USERS_M.USR_EMP_ID = EMPLOYEE_M.EMP_ID where USERS_M.USR_NAME='" & UserName & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetEmployeeIDFromUserName = RetVal
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetEmployeeNameFromID(ByVal EMPID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = " SELECT Isnull(EMP_PASSPORTNAME,'') EMP_PASSPORTNAME FROM EMPLOYEE_M  WHERE EMP_ID='" & EMPID & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetEmployeeNameFromID = RetVal
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetDepenedantNameFromID(ByVal EDDID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = " SELECT Isnull(EDD_NAME,'') EDD_NAME  FROM EMPDEPENDANTS_D  WHERE EDD_ID='" & EDDID & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetDepenedantNameFromID = RetVal
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function ConvertBytesToImage(ByVal imgByte As Byte()) As System.Drawing.Image
        Try
            Dim ms As New MemoryStream(imgByte)
            Dim returnImage As System.Drawing.Image
            returnImage = System.Drawing.Image.FromStream(ms, True, True)
            Return returnImage
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Sub SendMailToDelegates(ByVal EMP_ID As Integer, ByVal DOCTYPE As String, ByVal Subject As String, ByVal sb As StringBuilder)
        Try
            Dim Mailstatus As String = ""
            Dim dtDelUsrs As New DataTable
            Dim sql As String
            sql = "SELECT DISTINCT EMP_ID,EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME EMP_NAME,isnull(EMD_EMAIL,'')EMD_EMAIL from EMPJOBDELEGATE_S "
            sql &= " INNER JOIN EMPLOYEE_M INNER JOIN EMPLOYEE_D on EMP_ID=EMD_EMP_ID on EJD_DEL_EMP_ID = EMP_ID "
            sql &= " WHERE EJD_EJM_ID='" & DOCTYPE & "' and EJD_EMP_ID = " & EMP_ID.ToString
            sql &= " AND getdate() BETWEEN EJD_DTFROM and EJD_DTTO AND isnull(EJD_bForward,0)=1 and isnull(EJD_bDisable,0)=0"
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            dtDelUsrs = Mainclass.getDataTable(sql, str_conn)
            Dim mRow As DataRow
            Dim ds2 As New DataSet
            ds2 = GetCommunicationSettings()
            Dim username = ""
            Dim password = ""
            Dim port = ""
            Dim host = ""
            If ds2.Tables(0).Rows.Count > 0 Then
                username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            End If
            Dim ToEmailId As String = ""
            Dim fromemailid = "system@gemseducation.com"
            For Each mRow In dtDelUsrs.Rows
                Try
                    ToEmailId = mRow("EMD_EMAIL").ToString
                    Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, sb.ToString.Replace("XXXXXXXXXXXXX", mRow("EMP_NAME")), username, password, host, port, 0, False)
                Catch ex As Exception
                Finally
                    Mainclass.SaveEmailSendStatus(HttpContext.Current.Session("sBsuid"), "Expense", "", ToEmailId, Subject, sb.ToString.Replace("XXXXXXXXXXXXX", mRow("EMP_NAME")), IIf(LCase(Mailstatus).StartsWith("error"), 0, 1), Mailstatus)
                End Try
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Shared Function GetCommunicationSettings() As DataSet
        Dim ds As DataSet
        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & HttpContext.Current.Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
        Return ds
    End Function

End Class
