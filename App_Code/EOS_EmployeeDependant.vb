﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class EOS_EmployeeDependant

    Structure DependantDetail
        Public EDD_ID As Integer
        Public EDD_DOC_COUNT As Integer
        Public EDD_EMP_ID As String
        Public EDD_STU_ID As String
        Public EDD_NAME As String
        Public EDD_DOB As Date
        Public EDD_RELATION As String
        Public EDD_RELATION_DESCR As String
        Public EDD_PHOTO As Byte()

        Public bConcession As Boolean
        Public EDD_NoofTicket As Integer
        Public EDD_Insu_id As Integer
        Public InsuranceCategoryDESC As String
        Public EDD_bCompanyInsurance As Boolean
        Public EDD_GEMS_TYPE As String
        Public EDD_GEMS_TYPE_DESCR As String
        Public EDD_GEMS_ID As String
        Public EDD_BSU_ID As String
        Public EDD_BSU_NAME As String
        Public EDC_DOCUMENT_NO As String
        Public EDC_EXP_DT As String
        Public EDD_bMALE As Boolean
        Public EDD_MARITALSTATUS As Integer
        Public Status As String
        Public EDD_CTY_ID As Integer
        Public EDD_PASSPRTNO As String
        Public EDD_UIDNO As String
        Public EDD_VISAISSUEPLACE As String
        Public EDD_VISAEXPDATE As Date
        Public EDD_VISAISSUEDATE As Date
        Public EDC_Gender As String
        Public EDD_MSTATUS As String
        Public CTY_NATIONALITY As String
        Public EDD_VISASPONSOR As Integer
        Public EDD_EMIRATE As Integer
        Public EDD_EMIRATE_AREA As Integer
        Public EDD_INSURANCENUMBER As String
        Public EDD_STU_DETAIL As DataTable
        Public EDD_FILENO As String
        Public EDD_RESIDENCE_CTY_ID As Integer 'Added by vikranth on 4th mar 2020
        Public EDD_GEMS_STAFF_STU_NO As String 'Added by vikranth on 4th mar 2020
    End Structure


    Structure EligibilityCriteria
        Public ELG_ID As Integer
        Public ELG_BSU_ID As String
        Public ELG_EMP_ID As String
        Public ELG_ELIGIBLE_AndOr_OPTION As String
        Public ELG_ELIGIBLE_CHILD_COUNT_1 As Integer
        Public ELG_ELIGIBLE_SCHOOL_TYP_ID_1 As Integer
        Public ELG_ELIGIBLE_SCHOOL_ID_1 As String
        Public ELG_ELIGIBLE_PERCENTAGE_1 As Integer
        Public ELG_ELIGIBLE_VALID_TILL_1 As Date

        Public ELG_ELIGIBLE_CHILD_COUNT_2 As Integer
        Public ELG_ELIGIBLE_SCHOOL_TYP_ID_2 As Integer
        Public ELG_ELIGIBLE_SCHOOL_ID_2 As String
        Public ELG_ELIGIBLE_PERCENTAGE_2 As Integer
        Public ELG_ELIGIBLE_VALID_TILL_2 As Date

        Public ELG_ELIGIBLE_CHILD_COUNT_3 As Integer
        Public ELG_ELIGIBLE_SCHOOL_TYP_ID_3 As Integer
        Public ELG_ELIGIBLE_SCHOOL_ID_3 As String
        Public ELG_ELIGIBLE_PERCENTAGE_3 As Integer
        Public ELG_ELIGIBLE_VALID_TILL_3 As Date
        Public ELG_ACTIVE_OPTION As Integer

        Public ELG_bVerified As Boolean
        Public USER As String

    End Structure

    Public Shared Function GetDependantDetail(ByVal EDD_ID As Integer) As EOS_EmployeeDependant.DependantDetail
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDD_ID", EDD_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("EOS_GetEmpDependantDetail", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
            End If
            If mTable.Rows.Count > 0 Then
                Dim EDD_Detail As New EOS_EmployeeDependant.DependantDetail
                EDD_Detail.EDD_ID = mTable.Rows(0).Item("EDD_ID")
                EDD_Detail.EDD_EMP_ID = mTable.Rows(0).Item("EDD_EMP_ID")
                EDD_Detail.EDD_NAME = mTable.Rows(0).Item("EDD_NAME")
                EDD_Detail.EDD_DOB = mTable.Rows(0).Item("EDD_DOB")
                EDD_Detail.EDD_DOC_COUNT = mTable.Rows(0).Item("EDD_DOC_COUNT")
                EDD_Detail.EDD_RELATION = mTable.Rows(0).Item("EDD_RELATION")
                EDD_Detail.EDD_RELATION_DESCR = mTable.Rows(0).Item("EDD_RELATION_DESCR")
                EDD_Detail.EDD_STU_ID = mTable.Rows(0).Item("EDD_STU_ID")
                If Not IsDBNull(mTable.Rows(0).Item("EDD_PHOTO")) Then
                    EDD_Detail.EDD_PHOTO = mTable.Rows(0).Item("EDD_PHOTO")
                Else
                    EDD_Detail.EDD_PHOTO = Nothing
                End If

                EDD_Detail.bConcession = mTable.Rows(0).Item("EDD_bCONCESSION")
                EDD_Detail.EDD_NoofTicket = mTable.Rows(0).Item("EDD_NoofTicket")
                EDD_Detail.EDD_Insu_id = mTable.Rows(0).Item("EDD_Insu_id")
                EDD_Detail.InsuranceCategoryDESC = mTable.Rows(0).Item("InsuranceCategoryDESC")
                EDD_Detail.EDD_bCompanyInsurance = mTable.Rows(0).Item("EDD_bCompanyInsurance")
                EDD_Detail.EDD_GEMS_TYPE = mTable.Rows(0).Item("EDD_GEMS_TYPE")
                EDD_Detail.EDD_GEMS_TYPE_DESCR = mTable.Rows(0).Item("EDD_GEMS_TYPE_DESCR")
                EDD_Detail.EDD_GEMS_ID = mTable.Rows(0).Item("EDD_GEMS_ID")
                EDD_Detail.EDD_BSU_ID = mTable.Rows(0).Item("EDD_BSU_ID")
                EDD_Detail.EDD_BSU_NAME = mTable.Rows(0).Item("EDD_BSU_NAME")
                EDD_Detail.EDC_DOCUMENT_NO = mTable.Rows(0).Item("EDC_DOCUMENT_NO")
                EDD_Detail.EDC_EXP_DT = mTable.Rows(0).Item("EDC_EXP_DT")
                EDD_Detail.EDD_bMALE = mTable.Rows(0).Item("EDD_bMALE")
                EDD_Detail.EDD_MARITALSTATUS = mTable.Rows(0).Item("EDD_MARITALSTATUS")

                EDD_Detail.EDD_CTY_ID = mTable.Rows(0).Item("EDD_CTY_ID")
                EDD_Detail.EDD_PASSPRTNO = mTable.Rows(0).Item("EDD_PASSPRTNO")
                EDD_Detail.EDD_UIDNO = mTable.Rows(0).Item("EDD_UIDNO")
                EDD_Detail.EDD_VISAISSUEPLACE = mTable.Rows(0).Item("EDD_VISAISSUEPLACE")
                EDD_Detail.EDD_VISAEXPDATE = mTable.Rows(0).Item("EDD_VISAEXPDATE")
                EDD_Detail.EDD_VISAISSUEDATE = mTable.Rows(0).Item("EDD_VISAISSUEDATE")
                EDD_Detail.EDC_Gender = mTable.Rows(0).Item("EDC_Gender")
                EDD_Detail.EDD_MSTATUS = mTable.Rows(0).Item("EDD_MSTATUS")
                EDD_Detail.CTY_NATIONALITY = mTable.Rows(0).Item("CTY_NATIONALITY")

                EDD_Detail.EDD_VISASPONSOR = mTable.Rows(0).Item("EDD_VISA_SPONSOR")
                EDD_Detail.EDD_EMIRATE = mTable.Rows(0).Item("EDD_EMR_ID")
                EDD_Detail.EDD_EMIRATE_AREA = mTable.Rows(0).Item("EDD_EMA_ID")
                EDD_Detail.EDD_INSURANCENUMBER = mTable.Rows(0).Item("edd_insurance_number")
                EDD_Detail.EDD_FILENO = mTable.Rows(0).Item("EDD_FILENO")
                EDD_Detail.EDD_RESIDENCE_CTY_ID = mTable.Rows(0).Item("EDD_RESIDENCE_CTY_ID") 'Added by vikranth on 4th mar 2020
                EDD_Detail.EDD_GEMS_STAFF_STU_NO = mTable.Rows(0).Item("EDD_GEMS_STAFF_STU_NO") 'Added by vikranth on 4th mar 2020
                If mSet.Tables.Count > 1 Then
                    EDD_Detail.EDD_STU_DETAIL = mSet.Tables(1)
                End If
                Return EDD_Detail
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetDependantIDOfStudent(ByVal STU_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlStr, RetVal As String
            sqlStr = "select EDD_ID from EMPDEPENDANTS_D where EDD_STU_ID='" & STU_ID & "'"
            RetVal = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
            GetDependantIDOfStudent = RetVal
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetDependantDetailList(ByVal EDD_ID As Integer) As DataTable
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDD_EMP_ID", EDD_ID, SqlDbType.Int)
            mTable = Mainclass.getDataTable("EOS_GetEmpDependantDetailList", sqlParam, str_conn)
            Return mTable
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetChildDetailList(ByVal EDD_ID As Integer) As DataTable
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDD_EMP_ID", EDD_ID, SqlDbType.Int)
            mTable = Mainclass.getDataTable("EOS_GetEmpChildDetailList", sqlParam, str_conn)
            Return mTable
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function SaveDependantDetail(ByRef EDD_Detail As EOS_EmployeeDependant.DependantDetail) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(33) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDD_ID", EDD_Detail.EDD_ID, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@EDD_EMP_ID", EDD_Detail.EDD_EMP_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@EDD_NAME", EDD_Detail.EDD_NAME, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@EDD_DOB", EDD_Detail.EDD_DOB, SqlDbType.DateTime)
            sqlParam(4) = Mainclass.CreateSqlParameter("@EDD_RELATION", EDD_Detail.EDD_RELATION, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@EDD_STU_ID", EDD_Detail.EDD_STU_ID, SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@EDD_PHOTO", EDD_Detail.EDD_PHOTO, SqlDbType.Image)
            sqlParam(7) = Mainclass.CreateSqlParameter("@EDD_NoofTicket", EDD_Detail.EDD_NoofTicket, SqlDbType.Int)
            sqlParam(8) = Mainclass.CreateSqlParameter("@EDD_bCONCESSION", EDD_Detail.bConcession, SqlDbType.Bit)
            sqlParam(9) = Mainclass.CreateSqlParameter("@EDD_Insu_id", EDD_Detail.EDD_Insu_id, SqlDbType.Int)
            sqlParam(10) = Mainclass.CreateSqlParameter("@EDD_bCompanyInsurance", EDD_Detail.EDD_bCompanyInsurance, SqlDbType.Bit)
            sqlParam(11) = Mainclass.CreateSqlParameter("@EDD_GEMS_TYPE", EDD_Detail.EDD_GEMS_TYPE, SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@EDD_GEMS_ID", EDD_Detail.EDD_GEMS_ID, SqlDbType.VarChar)
            sqlParam(13) = Mainclass.CreateSqlParameter("@EDD_BSU_ID", EDD_Detail.EDD_BSU_ID, SqlDbType.VarChar)
            sqlParam(14) = Mainclass.CreateSqlParameter("@EDC_DOCUMENT_NO", EDD_Detail.EDC_DOCUMENT_NO, SqlDbType.VarChar)
            sqlParam(15) = Mainclass.CreateSqlParameter("@EDC_EXP_DT", EDD_Detail.EDC_EXP_DT, SqlDbType.VarChar)
            sqlParam(16) = Mainclass.CreateSqlParameter("@EDD_bMALE", EDD_Detail.EDD_bMALE, SqlDbType.Bit)
            sqlParam(17) = Mainclass.CreateSqlParameter("@EDD_MARITALSTATUS", EDD_Detail.EDD_MARITALSTATUS, SqlDbType.Int)
            sqlParam(18) = Mainclass.CreateSqlParameter("@EDD_CTY_ID", EDD_Detail.EDD_CTY_ID, SqlDbType.VarChar)
            sqlParam(19) = Mainclass.CreateSqlParameter("@Action", EDD_Detail.Status, SqlDbType.VarChar)
            sqlParam(20) = Mainclass.CreateSqlParameter("@EDD_PASSPRTNO", EDD_Detail.EDD_PASSPRTNO, SqlDbType.VarChar)
            sqlParam(21) = Mainclass.CreateSqlParameter("@EDD_UIDNO", EDD_Detail.EDD_UIDNO, SqlDbType.VarChar)
            sqlParam(22) = Mainclass.CreateSqlParameter("@EDD_VISAISSUEDATE", EDD_Detail.EDD_VISAISSUEDATE, SqlDbType.DateTime)
            sqlParam(23) = Mainclass.CreateSqlParameter("@EDD_VISAEXPDATE", EDD_Detail.EDD_VISAEXPDATE, SqlDbType.DateTime)
            sqlParam(24) = Mainclass.CreateSqlParameter("@EDD_VISAISSUEPLACE", EDD_Detail.EDD_VISAISSUEPLACE, SqlDbType.VarChar)
            ''added by nahyan on 12Feb2016 for audit while save dependent
            sqlParam(25) = Mainclass.CreateSqlParameter("@ModifiedUser", Convert.ToString(HttpContext.Current.Session("sUsr_id")), SqlDbType.VarChar)
            sqlParam(26) = Mainclass.CreateSqlParameter("@EDDEMIRATE", EDD_Detail.EDD_EMIRATE, SqlDbType.Int)
            sqlParam(27) = Mainclass.CreateSqlParameter("@VISASPONSOR", EDD_Detail.EDD_VISASPONSOR, SqlDbType.Int)
            sqlParam(28) = Mainclass.CreateSqlParameter("@EDDAREA", EDD_Detail.EDD_EMIRATE_AREA, SqlDbType.Int)
            sqlParam(29) = Mainclass.CreateSqlParameter("@EDD_INSUR_NUMBER", EDD_Detail.EDD_INSURANCENUMBER, SqlDbType.VarChar)
            sqlParam(30) = Mainclass.CreateSqlParameter("@EDD_FILENO", EDD_Detail.EDD_FILENO, SqlDbType.VarChar)
            sqlParam(31) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            sqlParam(32) = Mainclass.CreateSqlParameter("@EDD_RESIDENCE_CTY_ID", EDD_Detail.EDD_RESIDENCE_CTY_ID, SqlDbType.Int) 'Added by vikranth on 4th mar 2020
            sqlParam(33) = Mainclass.CreateSqlParameter("@EDD_GEMS_STAFF_STU_NO", EDD_Detail.EDD_GEMS_STAFF_STU_NO, SqlDbType.VarChar) 'Added by vikranth on 4th mar 2020

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "EOS_SaveEmpDependantDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(31).Value = "" Then
                SaveDependantDetail = ""
                EDD_Detail.EDD_ID = sqlParam(0).Value
            Else
                SaveDependantDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(31).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteDependantDetail(ByVal EDD_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDD_ID", EDD_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "EOS_DeleteEmpDependantDetail", sqlParam)
            If (Retval = "0" Or Retval = "") Then
                DeleteDependantDetail = ""
            Else
                DeleteDependantDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function ApplyConcession(ByVal STU_NO As String, ByVal EMP_ID As String, ByVal USER As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim sqlParam(5) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SCR_ID", 0, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SCR_REF_EMP_ID", EMP_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SCD_STU_NO", STU_NO, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@USER", USER, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "FEES.SAVE_ESS_STUDENT_CONCESSION_REQUEST", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(4).Value = "" Then
                ApplyConcession = ""

            Else
                ApplyConcession = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(4).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function ApproveConcession(ByVal SCR_ID As String, ByVal SCD_ID As String, ByVal STU_ID As String, ByVal EMP_ID As String, ByVal SCD_APPR_USER As String,
                                             ByVal APPROVAL_STATUS As String, ByVal FIN_APPROVAL_STATUS As String,
                                             ByVal SCD_GROSS_AMT As Double, ByVal SCD_ELIGIBLE_AMOUNT As Double, ByVal SCD_CONCESSION_AMT As Double, ByVal SCD_APPR_REMARKS As String, ByVal SCD_UPLOADED_DOC_PATH As String,
                                             ByVal SCD_ISELIGIBILITY_CHECK As Boolean, ByVal SCD_ISFIN_APPR_CHECK As Boolean,
                                             ByVal SCD_CONC_SCHOOL_BSU_ID As String, ByVal SCD_CONC_ELIGIBLE_STARTDT As String, ByVal SCD_ELG_ID As Integer, ByVal SCD_CONCESSION_TODT As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim sqlParam(19) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SCR_ID", SCR_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SCD_ID", SCD_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SCR_REF_EMP_ID", EMP_ID, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@SCD_STU_ID", STU_ID, SqlDbType.Int)
            sqlParam(4) = Mainclass.CreateSqlParameter("@SCD_APPR_STATUS", APPROVAL_STATUS, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@SCD_FIN_APPR_STATUS", FIN_APPROVAL_STATUS, SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@SCD_APPR_USER", SCD_APPR_USER, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@SCD_GROSS_AMT", SCD_GROSS_AMT, SqlDbType.Decimal)
            sqlParam(8) = Mainclass.CreateSqlParameter("@SCD_ELIGIBLE_AMOUNT", SCD_ELIGIBLE_AMOUNT, SqlDbType.Decimal)
            sqlParam(9) = Mainclass.CreateSqlParameter("@SCD_CONCESSION_AMT", SCD_CONCESSION_AMT, SqlDbType.Decimal)
            sqlParam(10) = Mainclass.CreateSqlParameter("@SCD_APPR_REMARKS", SCD_APPR_REMARKS, SqlDbType.VarChar)
            sqlParam(11) = Mainclass.CreateSqlParameter("@SCD_UPLOADED_DOC_PATH", SCD_UPLOADED_DOC_PATH, SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@SCD_ISELIGIBILITY_CHECK", SCD_ISELIGIBILITY_CHECK, SqlDbType.Bit)
            sqlParam(13) = Mainclass.CreateSqlParameter("@SCD_ISFIN_APPR_CHECK", SCD_ISFIN_APPR_CHECK, SqlDbType.Bit)

            sqlParam(14) = Mainclass.CreateSqlParameter("@SCD_CONC_SCHOOL_BSU_ID", SCD_CONC_SCHOOL_BSU_ID, SqlDbType.VarChar)
            sqlParam(15) = Mainclass.CreateSqlParameter("@SCD_CONC_ELIGIBLE_STARTDT", SCD_CONC_ELIGIBLE_STARTDT, SqlDbType.Date)
            sqlParam(16) = Mainclass.CreateSqlParameter("@SCD_ELG_ID", SCD_ELG_ID, SqlDbType.Int)
            sqlParam(17) = Mainclass.CreateSqlParameter("@SCD_CONCESSION_TODT", SCD_CONCESSION_TODT, SqlDbType.Date)
            sqlParam(18) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String = "0"
            Retval = Mainclass.ExecuteParamQRY(str_conn, "FEES.UPDATE_ESS_STUDENT_CONCESSION_APPROVAL", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(18).Value = "" Then
                ApproveConcession = ""

            Else
                ApproveConcession = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(18).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function ApproveConcessionByFinance(ByVal SCR_ID As String, ByVal SCD_ID As String, ByVal STU_ID As String, ByVal APPROVED_AMT As Double, ByVal USER As String, ByVal APPROVAL_STATUS As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim sqlParam(9) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SCR_ID", SCR_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SCD_ID", SCD_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SCD_CONCESSION_AMT", APPROVED_AMT, SqlDbType.Decimal)
            sqlParam(3) = Mainclass.CreateSqlParameter("@SCD_STU_ID", STU_ID, SqlDbType.Int)
            sqlParam(4) = Mainclass.CreateSqlParameter("@SCD_FIN_APPR_STATUS", APPROVAL_STATUS, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@USER", USER, SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String = "0"
            Retval = Mainclass.ExecuteParamQRY(str_conn, "FEES.UPDATE_ESS_STUDENT_CONCESSION_FINANCE_APPROVAL", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(6).Value = "" Then
                ApproveConcessionByFinance = ""

            Else
                ApproveConcessionByFinance = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(6).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function




    Public Shared Function UpdateEligibility(ByRef ELG_Criteria As EOS_EmployeeDependant.EligibilityCriteria) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(23) As SqlParameter

            sqlParam(0) = Mainclass.CreateSqlParameter("@ELG_ID", ELG_Criteria.ELG_ID, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ELG_BSU_ID", ELG_Criteria.ELG_BSU_ID, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ELG_EMP_ID", ELG_Criteria.ELG_EMP_ID, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_AndOr_OPTION", ELG_Criteria.ELG_ELIGIBLE_AndOr_OPTION, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_CHILD_COUNT_1", ELG_Criteria.ELG_ELIGIBLE_CHILD_COUNT_1, SqlDbType.Int)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_SCHOOL_TYP_ID_1", ELG_Criteria.ELG_ELIGIBLE_SCHOOL_TYP_ID_1, SqlDbType.Int)
            sqlParam(6) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_SCHOOL_ID_1", ELG_Criteria.ELG_ELIGIBLE_SCHOOL_ID_1, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_PERCENTAGE_1", ELG_Criteria.ELG_ELIGIBLE_PERCENTAGE_1, SqlDbType.Int)
            sqlParam(8) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_VALID_TILL_1", ELG_Criteria.ELG_ELIGIBLE_VALID_TILL_1, SqlDbType.DateTime)
            sqlParam(9) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_CHILD_COUNT_2", ELG_Criteria.ELG_ELIGIBLE_CHILD_COUNT_2, SqlDbType.Int)
            sqlParam(10) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_SCHOOL_TYP_ID_2", ELG_Criteria.ELG_ELIGIBLE_SCHOOL_TYP_ID_2, SqlDbType.Int)
            sqlParam(11) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_SCHOOL_ID_2", ELG_Criteria.ELG_ELIGIBLE_SCHOOL_ID_2, SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_PERCENTAGE_2", ELG_Criteria.ELG_ELIGIBLE_PERCENTAGE_2, SqlDbType.Int)
            sqlParam(13) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_VALID_TILL_2", ELG_Criteria.ELG_ELIGIBLE_VALID_TILL_2, SqlDbType.DateTime)
            sqlParam(14) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_CHILD_COUNT_3", ELG_Criteria.ELG_ELIGIBLE_CHILD_COUNT_3, SqlDbType.Int)
            sqlParam(15) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_SCHOOL_TYP_ID_3", ELG_Criteria.ELG_ELIGIBLE_SCHOOL_TYP_ID_3, SqlDbType.Int)
            sqlParam(16) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_SCHOOL_ID_3", ELG_Criteria.ELG_ELIGIBLE_SCHOOL_ID_3, SqlDbType.VarChar)
            sqlParam(17) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_PERCENTAGE_3", ELG_Criteria.ELG_ELIGIBLE_PERCENTAGE_3, SqlDbType.Int)
            sqlParam(18) = Mainclass.CreateSqlParameter("@ELG_ELIGIBLE_VALID_TILL_3", ELG_Criteria.ELG_ELIGIBLE_VALID_TILL_3, SqlDbType.DateTime)
            sqlParam(19) = Mainclass.CreateSqlParameter("@ELG_bVerified", ELG_Criteria.ELG_bVerified, SqlDbType.Bit)
            sqlParam(20) = Mainclass.CreateSqlParameter("@ELG_ACTIVE_OPTION", ELG_Criteria.ELG_ACTIVE_OPTION, SqlDbType.Int)
            sqlParam(21) = Mainclass.CreateSqlParameter("@USER", ELG_Criteria.USER, SqlDbType.VarChar)
            sqlParam(22) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String = "0"
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[DBO].[SAVE_ELIGIBILITY_CRITERIA]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(22).Value = "" Then
                UpdateEligibility = ""

            Else
                UpdateEligibility = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(22).Value)
            End If
        Catch ex As Exception

            Throw ex
        End Try
    End Function

    Public Shared Function UpdateNotes(ByVal OPTIONS As Integer, ByVal SCR_ID As String, ByVal SCD_ID As String, ByVal Notes As String, ByVal USER As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim sqlParam(6) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@OPTIONS", OPTIONS, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SCR_ID", SCR_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SCD_ID", SCD_ID, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@SCD_APPR_REMARKS", Notes, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@SCD_APPR_USER", USER, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String = "0"
            Retval = Mainclass.ExecuteParamQRY(str_conn, "FEES.UPDATE_ESS_STUDENT_CONCESSION_APPROVAL_NOTES", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(5).Value = "" Then
                UpdateNotes = ""

            Else
                UpdateNotes = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(5).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function UpdateUploadFilePath(ByVal SCR_ID As String, ByVal SCD_ID As String, ByVal UploadFilePath As String, ByVal UploadFileName As String, ByVal USER As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim sqlParam(6) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SCR_ID", SCR_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SCD_ID", SCD_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SCD_UPLOADED_DOC_PATH", UploadFilePath, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@SCD_UPLOADED_DOC_NAME", UploadFileName, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@SCD_APPR_USER", USER, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String = "0"
            Retval = Mainclass.ExecuteParamQRY(str_conn, "FEES.UPDATE_ESS_STUDENT_CONCESSION_UPLOAD_DOC_PATH", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(5).Value = "" Then
                UpdateUploadFilePath = ""

            Else
                UpdateUploadFilePath = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(5).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
End Class
