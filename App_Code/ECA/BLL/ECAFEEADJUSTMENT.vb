﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class ECAFEEADJUSTMENT
    Public Shared Function PrintFEEAdjustmentVoucher(ByVal vFAH_ID As Integer, ByVal UserName As String, _
    ByVal isHeadtoHead As Boolean) As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETFEEADJUSTMENTFORVOUCHER]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpFAR_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
        sqlpFAR_ID.Value = vFAH_ID
        cmd.Parameters.Add(sqlpFAR_ID)
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("RPT_CAPTION") = "FEE ADJUSTMENT VOUCHER"
        params("UserName") = UserName
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If isHeadtoHead Then
            repSource.ResourceName = "../../fees/Reports/RPT/rptFEEAdjustmentVoucher_.rpt"
        Else
            repSource.ResourceName = "../../fees/Reports/RPT/rptFEEAdjustmentVoucher.rpt"
        End If
        Return repSource
    End Function
End Class

Public Class ECAFEEADJUSTMENT_S
    Dim vFAD_ID As Integer
    Dim vFAD_FAH_ID As Integer
    Dim vFAD_FEE_ID As Integer
    Dim vFRS_TO_FEE_ID As Integer
    Dim vFAD_TO_FEE_ID As Integer
    Dim vTO_FEE_DESCR As String
    Dim vFAD_AMOUNT As Double
    Dim vFAD_REMARKS As String
    Dim vFEE_DESCR As String
    Dim vFPM_ID As Integer
    Dim vAdjType As Integer
    Dim vOtherCount As Integer
    Dim voneway As String
    Dim vssv_id As String
    Dim vFRS_Description As String
    'Public bMonth As Boolean
    Public bDelete As Boolean
    'Public bEdit As Boolean
    Dim vAPPR_AMOUNT As Double
    Dim vDurationDescription As String
    Dim vFEE_DESCR_TO As String
    Dim vFAD_TAX_CODE As String
    Dim vFAD_TAX_AMOUNT As Double
    Dim vFAD_NET_AMOUNT As Double

    Public Property FRS_Description() As String
        Get
            Return vFRS_Description
        End Get
        Set(ByVal value As String)
            vFRS_Description = value
        End Set
    End Property

    Public Property FEE_TYPE_TO() As String
        Get
            Return vFEE_DESCR_TO
        End Get
        Set(ByVal value As String)
            vFEE_DESCR_TO = value
        End Set
    End Property

    Public Property FEE_TYPE() As String
        Get
            Return vFEE_DESCR
        End Get
        Set(ByVal value As String)
            vFEE_DESCR = value
        End Set
    End Property

    Public Property TO_FEE_TYPE() As String
        Get
            Return vTO_FEE_DESCR
        End Get
        Set(ByVal value As String)
            vTO_FEE_DESCR = value
        End Set
    End Property

    Public Property FAD_TO_FEE_ID() As Integer
        Get
            Return vFAD_TO_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_TO_FEE_ID = value
        End Set
    End Property

    Public Property FAD_REMARKS() As String
        Get
            Return vFAD_REMARKS
        End Get
        Set(ByVal value As String)
            vFAD_REMARKS = value
        End Set
    End Property

    Public Property FAD_AMOUNT() As Double
        Get
            Return vFAD_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFAD_AMOUNT = value
        End Set
    End Property
    Public Property FAD_TAX_AMOUNT() As Double
        Get
            Return vFAD_TAX_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFAD_TAX_AMOUNT = value
        End Set
    End Property
    Public Property FAD_NET_AMOUNT() As Double
        Get
            Return vFAD_NET_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFAD_NET_AMOUNT = value
        End Set
    End Property
    Public Property FAD_TAX_CODE() As String
        Get
            Return vFAD_TAX_CODE
        End Get
        Set(ByVal value As String)
            vFAD_TAX_CODE = value
        End Set
    End Property

    Public Property AdjustmentType() As Integer
        Get
            Return vAdjType
        End Get
        Set(ByVal value As Integer)
            vAdjType = value
        End Set
    End Property
    Public Property oneway() As String
        Get
            Return voneway
        End Get
        Set(ByVal value As String)
            voneway = value
        End Set
    End Property
    Public Property SSV_ID() As String
        Get
            Return vssv_id
        End Get
        Set(ByVal value As String)
            vssv_id = value
        End Set
    End Property

    Public Property DurationCount() As Integer
        Get
            Return vOtherCount
        End Get
        Set(ByVal value As Integer)
            vOtherCount = value
        End Set
    End Property

    'Public Property DurationDescription() As String
    '    Get
    '        Return vDurationDescription
    '    End Get
    '    Set(ByVal value As String)
    '        vDurationDescription = value
    '    End Set
    'End Property

    Public Property FAD_FEE_ID() As Integer
        Get
            Return vFAD_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_FEE_ID = value
        End Set
    End Property

    Public Property FRS_TO_FEE_ID() As Integer
        Get
            Return vFRS_TO_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFRS_TO_FEE_ID = value
        End Set
    End Property

    Public Property FPM_ID() As Integer
        Get
            Return vFPM_ID
        End Get
        Set(ByVal value As Integer)
            vFPM_ID = value
        End Set
    End Property

    Public Property FAD_FAH_ID() As Integer
        Get
            Return vFAD_FAH_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_FAH_ID = value
        End Set
    End Property

    Public Property FAD_ID() As Integer
        Get
            Return vFAD_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_ID = value
        End Set
    End Property

    Public Property APPROVEDAMOUNT() As Double
        Get
            Return vAPPR_AMOUNT
        End Get
        Set(ByVal value As Double)
            vAPPR_AMOUNT = value
        End Set
    End Property

End Class


