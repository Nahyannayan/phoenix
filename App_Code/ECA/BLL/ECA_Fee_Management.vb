﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class ECA_Fee_Management
    Public Shared Function F_GetFeeDetailsForCollectionECA(ByVal p_DOCDT As String, ByVal p_STU_ID As String, _
   ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString

        Try
            pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
            pParms(0).Value = p_DOCDT
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(1).Value = p_STU_ID
            pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
            pParms(2).Value = p_STU_TYPE
            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = BSU_ID
            pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
            pParms(4).Value = ACD_ID
           
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(str_conn, _
              CommandType.StoredProcedure, "FEES.F_GetFeeDetailsForCollectionECA", pParms)
            If Not dsData Is Nothing Then
                Return dsData.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function
    Public Shared Function F_SaveFEECOLLECTION_H_ECA(ByVal p_FCL_ID As Integer, ByVal p_FCL_SOURCE As String, _
    ByVal p_FCL_DATE As DateTime, ByVal p_FCL_RECNO As String, ByVal p_FCL_ACD_ID As Integer, ByVal p_FCL_STU_ID As Integer, _
    ByVal p_FCL_AMOUNT As Decimal, ByVal p_FCL_EMP_ID As String, ByVal p_FCL_Bposted As Boolean, _
    ByRef p_newFCL_ID As Long, ByVal p_FCL_BSU_ID As String, ByVal p_FCL_NARRATION As String, _
    ByVal p_FCL_DRCR As String, ByRef p_NEW_FCL_RECNO As String, ByVal p_FCL_OUTSTANDING As String, _
    ByVal p_FCL_STU_BSU_ID As String, ByVal p_FCL_SBL_ID As String, ByVal p_FCL_STU_TYPE As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(23) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEECOLLECTION_H]
        '@FCL_ID = 0 ,
        '@FCL_SOURCE = N'COUNTER1',
        '@FCL_RECNO = N'VHH123',
        '@FCL_DATE = N'12-MAY-2008',
        '@FCL_ACD_ID = 79,

        Try
            pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
            pParms(0).Value = p_FCL_ID
            pParms(1) = New SqlClient.SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 100)
            pParms(1).Value = p_FCL_SOURCE
            pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 100)
            pParms(2).Value = p_FCL_RECNO
            pParms(3) = New SqlClient.SqlParameter("@FCL_DATE", SqlDbType.DateTime)
            pParms(3).Value = p_FCL_DATE
            pParms(4) = New SqlClient.SqlParameter("@FCL_ACD_ID", SqlDbType.VarChar, 100)
            pParms(4).Value = p_FCL_ACD_ID
            '@FCL_STU_ID = 52001,
            '@FCL_AMOUNT = 1200,
            '@FCL_EMP_ID = 122,
            pParms(5) = New SqlClient.SqlParameter("@FCL_STU_ID", SqlDbType.BigInt)
            pParms(5).Value = p_FCL_STU_ID
            pParms(6) = New SqlClient.SqlParameter("@FCL_AMOUNT", SqlDbType.Decimal, 21)
            pParms(6).Value = p_FCL_AMOUNT
            pParms(7) = New SqlClient.SqlParameter("@FCL_EMP_ID", SqlDbType.VarChar, 100)
            pParms(7).Value = p_FCL_EMP_ID
            pParms(8) = New SqlClient.SqlParameter("@FCL_Bposted", SqlDbType.Bit)
            pParms(8).Value = p_FCL_Bposted
            pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue
            '@FCL_Bposted = 0
            '@NEW_FCL_ID
            '@FCL_NARRATION VARCHAR(100) , 
            '@FCL_DRCR VARCHAR(5)
            pParms(10) = New SqlClient.SqlParameter("@NEW_FCL_ID", SqlDbType.BigInt)
            pParms(10).Direction = ParameterDirection.Output
            pParms(11) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 100)
            pParms(11).Value = p_FCL_BSU_ID
            pParms(12) = New SqlClient.SqlParameter("@FCL_NARRATION", SqlDbType.VarChar, 100)
            pParms(12).Value = p_FCL_NARRATION
            pParms(13) = New SqlClient.SqlParameter("@FCL_DRCR", SqlDbType.VarChar, 5)
            pParms(13).Value = p_FCL_DRCR
            pParms(14) = New SqlClient.SqlParameter("@NEW_FCL_RECNO", SqlDbType.VarChar, 30)
            pParms(14).Direction = ParameterDirection.Output
            '@FCL_OUTSTANDING NUMERIC(18,3), 
            '@FCL_STU_BSU_ID        
            '@FCL_REFNO varchar(20), 
            '@FCL_SBL_ID INT,
            pParms(15) = New SqlClient.SqlParameter("@FCL_OUTSTANDING", SqlDbType.Decimal)
            pParms(15).Value = p_FCL_OUTSTANDING
            pParms(16) = New SqlClient.SqlParameter("@FCL_STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(16).Value = p_FCL_STU_BSU_ID
            'pParms(17) = New SqlClient.SqlParameter("@FCL_REFNO", SqlDbType.VarChar, 20)
            'pParms(17).Value = System.DBNull.Value
            'pParms(17) = New SqlClient.SqlParameter("@FCL_SBL_ID", SqlDbType.VarChar)
            'pParms(17).Value = p_FCL_SBL_ID
            '@FCL_SCH_LIST  INT,
            '@FCL_SCH_ID ,
            '@FCL_CONC_AMOUNT NUMERIC(18,3) ,
            '@FCL_CONC_EMPDESCR  VARCHAR(200) ,
            'pParms(18) = New SqlClient.SqlParameter("@FCL_SCH_LIST", SqlDbType.VarChar, 200)
            'pParms(18).Value = System.DBNull.Value
            'pParms(19) = New SqlClient.SqlParameter("@FCL_SCH_ID", SqlDbType.Int)
            'pParms(19).Value = System.DBNull.Value
            pParms(17) = New SqlClient.SqlParameter("@FCL_STU_TYPE", SqlDbType.VarChar, 5)
            pParms(17).Value = p_FCL_STU_TYPE
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H", pParms)
            If pParms(9).Value = 0 Then
                p_newFCL_ID = pParms(10).Value
                p_NEW_FCL_RECNO = pParms(14).Value
            End If
            F_SaveFEECOLLECTION_H_ECA = pParms(9).Value
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Public Shared Function F_SaveFEECOLLSUB_ECA(ByVal p_FCS_ID As Integer, ByVal p_FCS_FCL_ID As Integer, _
    ByVal p_FCS_FEE_ID As Integer, ByVal p_FCS_AMOUNT As Decimal, ByVal bChargeandPost As Boolean, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEECOLLSUB]
        '@FCS_ID = 0,
        '@FCS_FCL_ID = 1,
        '@FCS_FEE_ID = 3,
        '@FCS_AMOUNT = 500

        Try
            pParms(0) = New SqlClient.SqlParameter("@FCS_ID", SqlDbType.Int)
            pParms(0).Value = p_FCS_ID
            pParms(1) = New SqlClient.SqlParameter("@FCS_FCL_ID", SqlDbType.Int)
            pParms(1).Value = p_FCS_FCL_ID
            pParms(2) = New SqlClient.SqlParameter("@FCS_FEE_ID", SqlDbType.Int)
            pParms(2).Value = p_FCS_FEE_ID
            pParms(3) = New SqlClient.SqlParameter("@FCS_AMOUNT", SqlDbType.Decimal, 21)
            pParms(3).Value = p_FCS_AMOUNT
            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            pParms(5) = New SqlClient.SqlParameter("@bChargeandPost", SqlDbType.Bit)
            pParms(5).Value = bChargeandPost
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB", pParms)
            F_SaveFEECOLLSUB_ECA = pParms(4).Value
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_ECA(ByVal p_FCL_ID As Integer, ByVal p_FCL_SOURCE As String, _
    ByVal p_FCL_DATE As DateTime, ByVal p_FCL_RECNO As String, ByVal p_FCL_ACD_ID As Integer, ByVal p_FCL_STU_ID As Integer, _
    ByVal p_FCL_AMOUNT As Decimal, ByVal p_FCL_EMP_ID As String, ByVal p_FCL_Bposted As Boolean, _
    ByRef p_newFCL_ID As Long, ByVal p_FCL_BSU_ID As String, ByVal p_FCL_NARRATION As String, _
    ByVal p_FCL_DRCR As String, ByRef p_NEW_FCL_RECNO As String, ByVal p_FCL_OUTSTANDING As String, _
    ByVal p_FCL_STU_BSU_ID As String, ByVal p_FCL_SBL_ID As String, ByVal p_FCL_STU_TYPE As String, _
    ByVal p_CRH_ID As Long, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(19) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
            pParms(0).Value = p_FCL_ID
            pParms(1) = New SqlClient.SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 100)
            pParms(1).Value = p_FCL_SOURCE
            pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 100)
            pParms(2).Value = p_FCL_RECNO
            pParms(3) = New SqlClient.SqlParameter("@FCL_DATE", SqlDbType.DateTime)
            pParms(3).Value = p_FCL_DATE
            pParms(4) = New SqlClient.SqlParameter("@FCL_ACD_ID", SqlDbType.VarChar, 100)
            pParms(4).Value = p_FCL_ACD_ID
            pParms(5) = New SqlClient.SqlParameter("@FCL_STU_ID", SqlDbType.BigInt)
            pParms(5).Value = p_FCL_STU_ID
            pParms(6) = New SqlClient.SqlParameter("@FCL_AMOUNT", SqlDbType.Decimal, 21)
            pParms(6).Value = p_FCL_AMOUNT
            pParms(7) = New SqlClient.SqlParameter("@FCL_EMP_ID", SqlDbType.VarChar, 100)
            pParms(7).Value = p_FCL_EMP_ID
            pParms(8) = New SqlClient.SqlParameter("@FCL_Bposted", SqlDbType.Bit)
            pParms(8).Value = p_FCL_Bposted
            pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue
            pParms(10) = New SqlClient.SqlParameter("@NEW_FCL_ID", SqlDbType.BigInt)
            pParms(10).Direction = ParameterDirection.Output
            pParms(11) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 100)
            pParms(11).Value = p_FCL_BSU_ID
            pParms(12) = New SqlClient.SqlParameter("@FCL_NARRATION", SqlDbType.VarChar, 100)
            pParms(12).Value = p_FCL_NARRATION
            pParms(13) = New SqlClient.SqlParameter("@FCL_DRCR", SqlDbType.VarChar, 5)
            pParms(13).Value = p_FCL_DRCR
            pParms(14) = New SqlClient.SqlParameter("@NEW_FCL_RECNO", SqlDbType.VarChar, 30)
            pParms(14).Direction = ParameterDirection.Output
            pParms(15) = New SqlClient.SqlParameter("@FCL_OUTSTANDING", SqlDbType.Decimal)
            pParms(15).Value = p_FCL_OUTSTANDING
            pParms(16) = New SqlClient.SqlParameter("@FCL_STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(16).Value = p_FCL_STU_BSU_ID
            pParms(17) = New SqlClient.SqlParameter("@FCL_STU_TYPE", SqlDbType.VarChar, 5)
            pParms(17).Value = p_FCL_STU_TYPE
            pParms(18) = New SqlClient.SqlParameter("@CRH_ID", SqlDbType.BigInt)
            pParms(18).Value = p_CRH_ID
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H", pParms)
            If pParms(9).Value = 0 Then
                p_newFCL_ID = pParms(10).Value
                p_NEW_FCL_RECNO = pParms(14).Value
            End If
            F_SaveFEECOLLECTION_H_ECA = pParms(9).Value
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Public Shared Function F_SaveFEECOLLSUB_ECA(ByVal p_FCS_ID As Integer, ByVal p_FCS_FCL_ID As Integer, _
    ByVal p_FCS_FEE_ID As Integer, ByVal p_FCS_AMOUNT As Decimal, ByVal bChargeandPost As Boolean, ByVal CRD_ID As Int64, _
    ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEECOLLSUB]
        '@FCS_ID = 0,
        '@FCS_FCL_ID = 1,
        '@FCS_FEE_ID = 3,
        '@FCS_AMOUNT = 500

        Try
            pParms(0) = New SqlClient.SqlParameter("@FCS_ID", SqlDbType.Int)
            pParms(0).Value = p_FCS_ID
            pParms(1) = New SqlClient.SqlParameter("@FCS_FCL_ID", SqlDbType.Int)
            pParms(1).Value = p_FCS_FCL_ID
            pParms(2) = New SqlClient.SqlParameter("@FCS_FEE_ID", SqlDbType.Int)
            pParms(2).Value = p_FCS_FEE_ID
            pParms(3) = New SqlClient.SqlParameter("@FCS_AMOUNT", SqlDbType.Decimal, 21)
            pParms(3).Value = p_FCS_AMOUNT
            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            pParms(5) = New SqlClient.SqlParameter("@bChargeandPost", SqlDbType.Bit)
            pParms(5).Value = bChargeandPost
            pParms(6) = New SqlClient.SqlParameter("@CRD_ID", SqlDbType.BigInt)
            pParms(6).Value = CRD_ID
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB", pParms)
            F_SaveFEECOLLSUB_ECA = pParms(4).Value
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Public Shared Function F_SaveFEECOLLSUB_D_ECA(ByVal p_FCD_ID As Integer, ByVal p_FCD_FCL_ID As String, _
        ByVal p_FCD_CLT_ID As Integer, ByVal p_FCD_AMOUNT As String, ByVal p_FCD_REFNO As String, _
         ByVal p_FCD_DATE As String, ByVal p_FCD_STATUS As String, ByVal p_FCD_VHH_DOCNO As String, _
        ByVal p_FCD_REF_ID As String, ByVal p_FCD_EMR_ID As String, ByVal p_FCD_CCM_ID As Integer, _
        ByVal p_stTrans As SqlTransaction, Optional ByVal p_FCD_BANK_ACT_ID As String = "") As String
        Dim pParms(12) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '@FCD_ID = 0,
        '@FCD_FCL_ID = 1,
        '@FCD_CLT_ID = 23,
        '@FCD_AMOUNT = 700,
        '@FCD_REFNO = N'VOUC34',
        Try
            pParms(0) = New SqlClient.SqlParameter("@FCD_ID", SqlDbType.Int)
            pParms(0).Value = p_FCD_ID
            pParms(1) = New SqlClient.SqlParameter("@FCD_FCL_ID", SqlDbType.BigInt)
            pParms(1).Value = p_FCD_FCL_ID
            pParms(2) = New SqlClient.SqlParameter("@FCD_CLT_ID", SqlDbType.Int)
            pParms(2).Value = p_FCD_CLT_ID
            pParms(3) = New SqlClient.SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
            pParms(3).Value = p_FCD_AMOUNT
            pParms(4) = New SqlClient.SqlParameter("@FCD_REFNO", SqlDbType.VarChar, 20)
            pParms(4).Value = p_FCD_REFNO
            '@FCD_DATE = N'12-MAY-2008',
            '@FCD_STATUS = 1,
            '@FCD_VHH_DOCNO = N'VOUN5666'
            '@FCD_REF_ID = N'VOUN5666', 
            '@FCD_EMR_ID = N'VOUN5666'
            pParms(5) = New SqlClient.SqlParameter("@FCD_DATE", SqlDbType.DateTime)
            pParms(5).Value = p_FCD_DATE
            pParms(6) = New SqlClient.SqlParameter("@FCD_STATUS", SqlDbType.VarChar, SqlDbType.Int)
            pParms(6).Value = p_FCD_STATUS
            pParms(7) = New SqlClient.SqlParameter("@FCD_VHH_DOCNO", SqlDbType.VarChar, 20)
            pParms(7).Value = p_FCD_VHH_DOCNO
            pParms(8) = New SqlClient.SqlParameter("@FCD_REF_ID", SqlDbType.VarChar, 20)
            pParms(8).Value = p_FCD_REF_ID
            pParms(9) = New SqlClient.SqlParameter("@FCD_EMR_ID", SqlDbType.VarChar, 20)
            pParms(9).Value = p_FCD_EMR_ID
            pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(10).Direction = ParameterDirection.ReturnValue
            pParms(11) = New SqlClient.SqlParameter("@FCD_CCM_ID", SqlDbType.Int)
            If p_FCD_CCM_ID = 0 Then
                pParms(11).Value = System.DBNull.Value
            Else
                pParms(11).Value = p_FCD_CCM_ID
            End If
            pParms(12) = New SqlClient.SqlParameter("@FCD_BANK_ACT_ID", SqlDbType.VarChar, 20)
            pParms(12).Value = p_FCD_BANK_ACT_ID
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D", pParms)
            F_SaveFEECOLLSUB_D_ECA = pParms(10).Value
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Public Shared Function F_SaveFEECOLLALLOCATION_D_ECA(ByVal p_BSU_ID As String, ByVal p_FCL_ID As String, _
    ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        '     EXEC	@return_value = [FEES].[F_SaveFEECOLLALLOCATION_D] 
        Try
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            pParms(2) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
            pParms(2).Value = p_FCL_ID
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLALLOCATION_D", pParms)
            F_SaveFEECOLLALLOCATION_D_ECA = pParms(1).Value
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
        
    End Function

    Public Shared Function F_SAVECHEQUEDATA_ECA(ByVal p_BSU_ID As String, ByVal p_FCL_IDs As String, _
                ByVal p_STU_BSU_ID As String, ByVal p_FCL_EMP_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '   EXEC [FEES].[F_SAVECHEQUEDATA]
        '@BSU_ID  ='900500' ,
        '@FCL_IDS  ='1|2|3|345|4456777',
        '@STU_BSU_ID   ='125016'
        Try
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_STU_BSU_ID
            pParms(3) = New SqlClient.SqlParameter("@FCL_IDs", SqlDbType.VarChar, 2000)
            pParms(3).Value = p_FCL_IDs
            pParms(4) = New SqlClient.SqlParameter("@FCL_EMP_ID", SqlDbType.VarChar, 50)
            pParms(4).Value = p_FCL_EMP_ID
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SAVECHEQUEDATA", pParms)
            F_SAVECHEQUEDATA_ECA = pParms(1).Value
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
        
    End Function

    Public Shared Function GetECAStudentID(ByVal STU_NO As String, ByVal BSU_ID As String, _
    ByVal bEnquiry As Boolean) As String
        Dim str_data, str_sql, str_conn As String

        str_conn = ConnectionManger.GetOASIS_SERVICESConnectionString

        Dim iStdnolength As Integer = STU_NO.Length
        If bEnquiry Then
            str_sql = "SELECT STU_ID  FROM FEES.vw_OSO_ENQUIRY_COMP" _
             & " WHERE     (STU_BSU_ID = '" & BSU_ID & "') AND  STU_NO='" & STU_NO & "'"
            str_data = GetDataFromSQL(str_sql, str_conn)
        Else
            If iStdnolength < 9 Then
                If STU_NO.Trim.Length < 8 Then
                    For i As Integer = iStdnolength + 1 To 8
                        STU_NO = "0" & STU_NO
                    Next
                End If
                STU_NO = BSU_ID & STU_NO
            End If
            str_sql = "SELECT STU_ID  FROM VW_OSO_STUDENT_M" _
             & " WHERE (STU_BSU_ID = '" & BSU_ID & "') AND  STU_NO='" & STU_NO & "'"
            str_data = GetDataFromSQL(str_sql, str_conn)
        End If
        If str_data <> "--" Then
            Return str_data
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetECABSUAcademicYear(ByVal BSU_ID As String) As DataTable
        Dim sql_query As String = " EXEC FEES.GetBSUAcademicYear '" & BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnection, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
End Class
