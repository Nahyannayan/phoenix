Imports Microsoft.VisualBasic

Public Class MENURIGHTS
    Dim vRoleId As String
    Dim vBusUnit As String
    Dim vmenucode As String
    Dim vOperationId As String
    Dim vDescription As String
    Dim vFormText As String
    Dim vModules As String
    Dim vID As Integer

    Public Property ID() As Integer
        Get
            Return vID
        End Get
        Set(ByVal value As Integer)
            vID = value
        End Set
    End Property

    Public Property Modules() As String
        Get
            Return vModules
        End Get
        Set(ByVal value As String)
            vModules = value
        End Set
    End Property

    Public Property FormText() As String
        Get
            Return vFormText
        End Get
        Set(ByVal value As String)
            vFormText = value
        End Set
    End Property

    Public Property RoleId() As String
        Get
            Return vRoleId
        End Get
        Set(ByVal value As String)
            vRoleId = value
        End Set
    End Property

    Public Property BusUnit() As String
        Get
            Return vBusUnit
        End Get
        Set(ByVal value As String)
            vBusUnit = value
        End Set
    End Property

    Public Property Menucode() As String
        Get
            Return vmenucode
        End Get
        Set(ByVal value As String)
            vmenucode = value
        End Set
    End Property

    Public Property OperationId() As String
        Get
            Return vOperationId
        End Get
        Set(ByVal value As String)
            vOperationId = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return vDescription
        End Get
        Set(ByVal value As String)
            vDescription = value
        End Set
    End Property

End Class
