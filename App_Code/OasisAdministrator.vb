Imports Microsoft.VisualBasic

Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
'Test

Namespace Oasis_Administrator

    Public Class OasisAdministrator
        Public Shared Function GetSource() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SOURCE", pParms)
            Return ds
        End Function
        Public Shared Function GetClm() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CURRICULUM", pParms)
            Return ds
        End Function
        Public Shared Function InsertSource(ByVal SourceName As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SOURCE_DES", SourceName)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SOURCE", pParms)
            Return ReturnString
        End Function
        Public Shared Function UpdateSource(ByVal SourceName As String, ByVal Record_id As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SOURCE_DES", SourceName)
            pParms(1) = New SqlClient.SqlParameter("@RECORD_ID", Record_id)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 2)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SOURCE", pParms)
            Return ReturnString
        End Function
        Public Shared Function DeleteSource(ByVal Record_id As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RECORD_ID", Record_id)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 3)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SOURCE", pParms)
            Return ReturnString
        End Function


        Public Shared Function GetSubject() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SUBJECT", pParms)
            Return ds
        End Function
        Public Shared Function Get_IntCity(ByVal CountryId As Integer) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "select INC_ID,INC_DESCR from oasis.dbo. INTR_CITY_M  WHERE INC_CTY_ID='" & CountryId & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Return ds
        End Function


        Public Shared Function InsertSubject(ByVal SubjectName As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SUBJECT", SubjectName)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SUBJECT", pParms)
            Return ReturnString
        End Function
        Public Shared Function UpdateSubject(ByVal SubjectName As String, ByVal Record_id As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SUBJECT", SubjectName)
            pParms(1) = New SqlClient.SqlParameter("@RECORD_ID", Record_id)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 2)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SUBJECT", pParms)
            Return ReturnString
        End Function
        Public Shared Function DeleteSubject(ByVal Record_id As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RECORD_ID", Record_id)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 3)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_SUBJECT", pParms)
            Return ReturnString
        End Function


        Public Shared Function GetCategory() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CATEGORY", pParms)
            Return ds
        End Function
        Public Shared Function InsertCategory(ByVal Category As String, ByVal TeachingMode As Boolean) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CATEGORY_DES", Category)
            pParms(1) = New SqlClient.SqlParameter("@TEACHING_MODE", TeachingMode)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 1)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CATEGORY", pParms)
            Return ReturnString
        End Function
        Public Shared Function UpdateCategory(ByVal Category As String, ByVal Record_id As String, ByVal TeachingMode As Boolean) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CATEGORY_DES", Category)
            pParms(1) = New SqlClient.SqlParameter("@RECORD_ID", Record_id)
            pParms(2) = New SqlClient.SqlParameter("@TEACHING_MODE", TeachingMode)
            pParms(3) = New SqlClient.SqlParameter("@OPTION", 2)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CATEGORY", pParms)
            Return ReturnString
        End Function
        Public Shared Function DeleteCategory(ByVal Record_id As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RECORD_ID", Record_id)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 3)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CATEGORY", pParms)
            Return ReturnString
        End Function
        Public Shared Function Countries() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "SELECT * FROM COUNTRY  ORDER BY COUNTRY"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Return ds
        End Function
        Public Shared Function Nationality() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT * FROM COUNTRY_M WHERE CTY_NATIONALITY <>'-' ORDER BY CTY_NATIONALITY "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Return ds
        End Function
        Public Shared Function CountriesHrListed() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "SELECT * FROM COUNTRY where Active='True' ORDER BY COUNTRY"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Return ds
        End Function
        Public Shared Function Get_BSU() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "select * from oasis.dbo. BUSINESSUNIT_M  a inner join bsu b on a.bsu_id = b.bsu_id ORDER BY a.BSU_NAME"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Return ds
        End Function
        Public Shared Function GetApplicantInfo(ByVal Application_No As String, ByVal Optionalval As Integer) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            If Application_No <> Nothing Then
                pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_No)
            End If
            pParms(1) = New SqlClient.SqlParameter("@OPTION", Optionalval)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_APPLICATION_DATA", pParms)
            Return ds
        End Function
        Public Shared Function GetWorkExperienceInfo(ByVal Application_No As String) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_Query = "select * from APPLICATION_WORK_EXPERIENCE a inner join country b on a.country_id = b.country_id " & _
                            " where a.APPLICATION_NO='" & Application_No & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Query)
            Return ds
        End Function
        'Public Shared Function ChangeApplicationStatus(ByVal Application_No As String, ByVal Status As String) As String
        '    Dim Returnstring As String = ""
        '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        '    Dim pParms(2) As SqlClient.SqlParameter
        '    pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Convert.ToInt32(Application_No))
        '    pParms(1) = New SqlClient.SqlParameter("@STATUS", Status)
        '    Returnstring = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "CHANGE_APPLICATION_STATUS", pParms)
        '    Return Returnstring
        'End Function
        Public Shared Function GetTown() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_Query = "SELECT DISTINCT TOWN FROM APPLICATION_MASTER"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Query)
            Return ds
        End Function

        Public Shared Function InsertCorrespondence(ByVal Application_No As String, ByVal Correspondence As String, ByVal mode As String, ByVal emp_id As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_No)
            pParms(1) = New SqlClient.SqlParameter("@CORRESPONDENCE", Correspondence)
            pParms(2) = New SqlClient.SqlParameter("@MODE", mode)
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", emp_id)
            pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_APPLICATION_CORRESPONDENCE", pParms)
            Return ReturnString
        End Function

        Public Shared Function GetCorrespondence(ByVal Application_No As String) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_No)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 4)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_APPLICATION_CORRESPONDENCE", pParms)
            Return ds
        End Function

        Public Shared Function GetApplicantCategory(ByVal Application_No As String) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "SELECT * FROM APPLICANTION_APPLIED_CATEGORY A " & _
                            " INNER JOIN CATEGORY B ON A.CATEGORY_ID = B.CATEGORY_ID AND A.APPLICATION_NO='" & Application_No & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Return ds
        End Function
        'Public Shared Function GetStatus() As DataSet
        '    Dim ds As DataSet
        '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        '    Dim pParms(1) As SqlClient.SqlParameter
        '    pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_STATUS", pParms)
        '    Return ds
        'End Function
        'Public Shared Function InsertStatus(ByVal Status As String) As String
        '    Dim ReturnString As String = ""
        '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        '    Dim pParms(2) As SqlClient.SqlParameter
        '    pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        '    pParms(1) = New SqlClient.SqlParameter("@STATUS", Status)
        '    ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_STATUS", pParms)
        '    Return ReturnString
        'End Function
        'Public Shared Function UpdateStatus(ByVal Record_Id As String, ByVal Status As String) As String
        '    Dim ReturnString As String = ""
        '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        '    Dim pParms(3) As SqlClient.SqlParameter
        '    pParms(0) = New SqlClient.SqlParameter("@OPTION", 2)
        '    pParms(1) = New SqlClient.SqlParameter("@STATUS", Status)
        '    pParms(2) = New SqlClient.SqlParameter("@RECORD_ID", Record_Id)
        '    ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_STATUS", pParms)
        '    Return ReturnString
        'End Function
        'Public Shared Function DeleteStatus(ByVal Record_Id As String) As String
        '    Dim ReturnString As String = ""
        '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        '    Dim pParms(2) As SqlClient.SqlParameter
        '    pParms(0) = New SqlClient.SqlParameter("@OPTION", 3)
        '    pParms(1) = New SqlClient.SqlParameter("@RECORD_ID", Record_Id)
        '    ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_STATUS", pParms)
        '    Return ReturnString
        'End Function
        Public Shared Function GetMode() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_MODE", pParms)
            Return ds
        End Function
        Public Shared Function InsertMode(ByVal Mode As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
            pParms(1) = New SqlClient.SqlParameter("@MODE", Mode)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_MODE", pParms)
            Return ReturnString
        End Function
        Public Shared Function UpdateMode(ByVal Record_Id As String, ByVal Mode As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 2)
            pParms(1) = New SqlClient.SqlParameter("@MODE", Mode)
            pParms(2) = New SqlClient.SqlParameter("@RECORD_ID", Record_Id)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_MODE", pParms)
            Return ReturnString
        End Function
        Public Shared Function DeleteMode(ByVal Record_Id As String) As String
            Dim ReturnString As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 3)
            pParms(1) = New SqlClient.SqlParameter("@RECORD_ID", Record_Id)
            ReturnString = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_MODE", pParms)
            Return ReturnString
        End Function
        Public Shared Function GetJobCode() As Integer
            Dim ReturnVal As String
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "SELECT ISNULL(MAX(JOB_CODE),0)  FROM JOBS"
            ReturnVal = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            Return Convert.ToInt32(ReturnVal)
        End Function
        Public Shared Function Get_Job_Code_Open_Jobs(ByVal Category_ID As String) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim srt_query = "SELECT DISTINCT JOB_CODE,JOB_DES FROM JOBS WHERE JOB_STATUS='OPEN' AND CATEGORY_ID='" & Category_ID & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
            Return ds
        End Function
        Public Shared Function Get_All_Job_Code_Open_Jobs() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim srt_query = "SELECT DISTINCT JOB_CODE,JOB_DES FROM JOBS WHERE JOB_STATUS='OPEN'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
            Return ds
        End Function
        Public Shared Function Get_Job_Details_Open_Jobs(ByVal JobeCode As String) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim srt_query = "SELECT * FROM JOBS INNER JOIN OASIS.DBO.BUSINESSUNIT_M ON JOBS.BSU= OASIS.DBO.BUSINESSUNIT_M.BSU_ID AND JOB_CODE='" & JobeCode & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
            Return ds
        End Function

        Public Shared Function SaveJobs(ByVal JobCode As Integer, ByVal Bsu As String, ByVal Job_Des As String, ByVal Job_Info As String, ByVal Category_ID As Integer, ByVal ReportedBy As String, ByVal Vacant_No As Integer, ByVal CloseDate As String) As String
            Dim returnvalue As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
            pParms(1) = New SqlClient.SqlParameter("@JOB_CODE", JobCode)
            pParms(2) = New SqlClient.SqlParameter("@BSU", Bsu)
            pParms(3) = New SqlClient.SqlParameter("@JOB_DES", Job_Des)
            pParms(4) = New SqlClient.SqlParameter("@JOB_INFO", Job_Info)
            pParms(5) = New SqlClient.SqlParameter("@CATEGORY_ID", Category_ID)

            pParms(6) = New SqlClient.SqlParameter("@REPORTED_BY", ReportedBy)
            pParms(7) = New SqlClient.SqlParameter("@VACANT_NO", Vacant_No)
            pParms(8) = New SqlClient.SqlParameter("@CLOSE_DATE", CloseDate)

            returnvalue = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_JOBS", pParms)
            Return returnvalue
        End Function
        Public Shared Function SaveJobsEdit(ByVal JobCode As Integer, ByVal Bsu As String, ByVal Job_Des As String, ByVal Job_Info As String, ByVal Category_ID As Integer, ByVal ReportedBy As String, ByVal Vacant_No As Integer, ByVal CloseDate As String) As String
            Dim returnvalue As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 11)
            pParms(1) = New SqlClient.SqlParameter("@JOB_CODE", JobCode)
            pParms(2) = New SqlClient.SqlParameter("@BSU", Bsu)
            pParms(3) = New SqlClient.SqlParameter("@JOB_DES", Job_Des)
            pParms(4) = New SqlClient.SqlParameter("@JOB_INFO", Job_Info)
            pParms(5) = New SqlClient.SqlParameter("@CATEGORY_ID", Category_ID)

            pParms(6) = New SqlClient.SqlParameter("@REPORTED_BY", ReportedBy)
            pParms(7) = New SqlClient.SqlParameter("@VACANT_NO", Vacant_No)
            pParms(8) = New SqlClient.SqlParameter("@CLOSE_DATE", CloseDate)

            returnvalue = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_JOBS", pParms)
            Return returnvalue
        End Function

        Public Shared Function SaveJobInformation(ByVal JobCode As Integer, ByVal Bsu_id As String, ByVal Reported_by As String, ByVal Vacant As Integer, ByVal CloseDate As String) As String
            Dim returnvalue As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 5)
            pParms(1) = New SqlClient.SqlParameter("@JOB_CODE", JobCode)
            pParms(2) = New SqlClient.SqlParameter("@BSU", Bsu_id)
            pParms(3) = New SqlClient.SqlParameter("@REPORTED_BY", Reported_by)
            pParms(4) = New SqlClient.SqlParameter("@VACANT_NO", Vacant)
            pParms(5) = New SqlClient.SqlParameter("@CLOSE_DATE", CloseDate)
            returnvalue = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_JOBS", pParms)
            Return returnvalue
        End Function
        Public Shared Sub Update_Transactions(ByVal Application_No As Integer, ByVal Parent_Stage As Integer, ByVal Sub_Stage As Integer, ByVal Job_Code As Integer, ByVal ShortListed_BSU_ID As Integer)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_No)
            pParms(1) = New SqlClient.SqlParameter("@TRAN_PARENT_STAGE", Parent_Stage)
            pParms(2) = New SqlClient.SqlParameter("@TRAN_SUB_STAGE", Sub_Stage)
            If ShortListed_BSU_ID <> Nothing Then
                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", ShortListed_BSU_ID)
            End If
            If Job_Code <> Nothing Then
                pParms(4) = New SqlClient.SqlParameter("@JOB_ID", Job_Code)
            End If


            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "UPDATE_TRANSACTIONS", pParms)
        End Sub
        Public Shared Function Get_Candidates_For_ShortListing() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim srt_query = "SELECT (SELECT C.COUNTRY AS CRE FROM COUNTRY C WHERE A.COUNTRY_OF_RESIDENCE=C.COUNTRY_ID) AS COUNTRY_RES_NAME, " & _
                        " (SELECT CC.COUNTRY AS CIP FROM  COUNTRY CC WHERE A.COUNTRY_ISSUE_PASSPORT=CC.COUNTRY_ID) AS COUNTRY_PASS_ISS_NAME " & _
                        " ,* FROM APPLICATION_MASTER A WHERE STATUS=0 AND ACTIVE='True'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
            Return ds
        End Function
        'Public Shared Function Get_Candidates_For_Stage_Change() As DataSet
        '    Dim ds As DataSet
        '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        '    Dim srt_query = "SELECT (SELECT C.COUNTRY AS CRE FROM COUNTRY C WHERE A.COUNTRY_OF_RESIDENCE=C.COUNTRY_ID) AS COUNTRY_RES_NAME, " & _
        '                    " (SELECT CC.COUNTRY AS CIP FROM  COUNTRY CC WHERE A.COUNTRY_ISSUE_PASSPORT=CC.COUNTRY_ID) AS COUNTRY_PASS_ISS_NAME " & _
        '                    " ,* FROM APPLICATION_MASTER A WHERE INTERVIEW_STAGE NOT LIKE COALESCE('240',INTERVIEW_STAGE) AND OFFER_STAGE NOT LIKE COALESCE('330',OFFER_STAGE) AND OFFER_STAGE NOT LIKE COALESCE('320',OFFER_STAGE) AND SHORTLIST_STAGE  LIKE COALESCE('100',SHORTLIST_STAGE)" ''AND JOIN_STAGE NOT LIKE COALESCE('400',JOIN_STAGE)
        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
        '    Return ds
        'End Function
        Public Shared Function Get_Allowed_Stage(ByVal Stage_id As String) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim srt_query = "SELECT *,(SELECT DESCRIPTION FROM STAGE WHERE STAGE=ALLOWED_STAGES.ALLOWED_STAGES)as ALLOW_DES FROM STAGE INNER JOIN ALLOWED_STAGES ON STAGE=ALLOWED_STAGES.STAGE_ID AND STAGE='" & Stage_id & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
            Return ds
        End Function
        Public Shared Sub Insert_Stage_Remarks(ByVal Application_No As String, ByVal Stage_id As Integer, ByVal Remarks As String)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("APPLICATION_NO", Application_No)
            pParms(1) = New SqlClient.SqlParameter("@STAGE_ID", Stage_id)
            pParms(2) = New SqlClient.SqlParameter("@REMARKS", Remarks)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_APPLICATION_STAGE_REMARKS", pParms)
        End Sub
        Public Shared Function Get_Stages() As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim srt_query = "SELECT * FROM STAGE_MASTER WHERE PARENT_STAGE !=0 AND (FINAL_STAGE != -1  AND PARENT_STAGE >0)"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
            Return ds
        End Function
        'Public Shared Function Get_Applicants_Data_By_Stages(ByVal Stage_Id As String, ByVal Only_Shorlisted As Boolean) As DataSet
        '    Dim ds As DataSet
        '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        '    Dim srt_query = "SELECT (SELECT C.COUNTRY AS CRE FROM COUNTRY C WHERE A.COUNTRY_OF_RESIDENCE=C.COUNTRY_ID) AS COUNTRY_RES_NAME, " & _
        '                    " (SELECT CC.COUNTRY AS CIP FROM  COUNTRY CC WHERE A.COUNTRY_ISSUE_PASSPORT=CC.COUNTRY_ID) AS COUNTRY_PASS_ISS_NAME " & _
        '                    " ,* FROM APPLICATION_MASTER A WHERE "
        '    If Only_Shorlisted = True Then
        '        srt_query = srt_query & "SHORTLIST_STAGE='100' AND INTERVIEW_STAGE='0' AND OFFER_STAGE='0' AND JOIN_STAGE='0' "
        '    Else

        '        srt_query = srt_query & " SHORTLIST_STAGE LIKE '" & Stage_Id & "' OR " & _
        '                                " INTERVIEW_STAGE LIKE '" & Stage_Id & "' OR " & _
        '                                " OFFER_STAGE LIKE '" & Stage_Id & "' OR " & _
        '                                " JOIN_STAGE LIKE '" & Stage_Id & "' "
        '    End If

        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
        '    Return ds
        'End Function
        Public Shared Function Get_Job_Information(ByVal Job_Code As String) As String
            Dim ReturnValue As String = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "SELECT JOB_INFORMATION  FROM JOBS WHERE JOB_CODE='" & Job_Code & "'"
            ReturnValue = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            Return ReturnValue
        End Function
        Public Shared Function Get_Applicant_Stage_Remarks(ByVal Application_No As String) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "SELECT * FROM APPLICATION_STAGE_REMARKS " & _
                            " INNER JOIN STAGE_MASTER ON APPLICATION_STAGE_REMARKS.STAGE_ID=STAGE_MASTER.STAGE_ID " & _
                            " WHERE APPLICATION_STAGE_REMARKS.APPLICATION_NO='" & Application_No & "' ORDER BY ENTRY_DATE"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Return ds
        End Function
        Public Shared Function Get_Job_Stage_Reports(ByVal Stage_ID As String, ByVal JobCode As String, ByVal optionval As Integer) As DataSet
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            If Stage_ID <> 0 Then
                pParms(0) = New SqlClient.SqlParameter("@SUB_STAGE_ID", Stage_ID)
            End If
            If JobCode <> 0 Then
                pParms(1) = New SqlClient.SqlParameter("@JOB_ID", JobCode)
            End If
            pParms(2) = New SqlClient.SqlParameter("@OPTION", optionval)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORTS_JOB_CODE_SUB_STAGE_ID", pParms)
            Return ds
        End Function
    End Class
End Namespace