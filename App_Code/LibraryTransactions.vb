﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Public Class LibraryTransactions


    Private Shared Function TabDisplay(ByVal Hash As Hashtable, ByRef ReturnValue As String) As Boolean
        ReturnValue = ""
        Dim returnvalueB As Boolean = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = ""
        Dim ds As DataSet
        ''Check if the item exists in selected library
        str_query = " SELECT * FROM dbo.LIBRARY_ITEMS_QUANTITY B " & _
                    " INNER JOIN dbo.LIBRARY_RACKS C ON B.RACK_ID=C.RACK_ID " & _
                    " INNER JOIN dbo.LIBRARY_SHELFS D ON C.SHELF_ID=D.SHELF_ID  " & _
                    " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS E ON E.LIBRARY_SUB_DIVISION_ID= D.LIBRARY_SUB_DIVISION_ID " & _
                    " INNER JOIN dbo.LIBRARY_DIVISIONS F ON F.LIBRARY_DIVISION_ID = E.LIBRARY_DIVISION_ID " & _
                    " WHERE  F.LIBRARY_DIVISION_ID='" & Hash.Item("ddLibrary") & "' AND CONVERT(VARCHAR, B.STOCK_ID)='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "' AND B.ACTIVE='True' AND B.STOCK_ID_POUCH_ID IS NULL "


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim Staus_Check As String = "0"
        If ds.Tables(0).Rows.Count > 0 Then

            If Convert.ToString(ds.Tables(0).Rows(0).Item("STATUS_ID")) <> "" Then
                Staus_Check = Convert.ToString(ds.Tables(0).Rows(0).Item("STATUS_ID"))
            End If

            If (Staus_Check = "17") Then
                ReturnValue = "you are not allowed to issue this book.please check the status."
                returnvalueB = False
            Else
                str_query = "SELECT RECORD_ID FROM  LIBRARY_TRANSACTIONS WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "' AND ISNULL(ITEM_ACTUAL_RETURN_DATE,'')= ''"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                If ds.Tables(0).Rows.Count > 0 Then ''Item Issued

                    ReturnValue = "Wrong entry. This Item has been issued. Please view the transaction history."

                    returnvalueB = False

                Else ''Item Return Back

                    returnvalueB = True

                End If
            End If
        Else
            ReturnValue = "Transaction cannot be done.<br>Reason 1: Please check the Accession No. <br> Reason 2: This Item does not belong to this library.<br>Reason 3: Item may be removed.<br>Reason 4: Item may be in a Pouch.<br>Reason 5: Does not exists."
            returnvalueB = False
        End If



            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    str_query = "SELECT RECORD_ID FROM  LIBRARY_TRANSACTIONS WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "' AND ISNULL(ITEM_ACTUAL_RETURN_DATE,'')= ''"
            '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            '    If ds.Tables(0).Rows.Count > 0 Then ''Item Issued

            '        ReturnValue = "Wrong entry. This Item has been issued. Please view the transaction history."

            '        returnvalueB = False

            '    Else ''Item Return Back

            '        returnvalueB = True

            '    End If
            'Else
            '    ReturnValue = "Transaction cannot be done.<br>Reason 1: Please check the Accession No. <br> Reason 2: This Item does not belong to this library.<br>Reason 3: Item may be removed.<br>Reason 4: Item may be in a Pouch.<br>Reason 5: Does not exists."
            '    returnvalueB = False
            'End If



            Return returnvalueB

    End Function

    Private Shared Function Circulation(ByVal Hash As Hashtable, ByRef ReturnValue As String) As Boolean
        ReturnValue = ""
        Dim returnvalueB As Boolean = True

        ''Check Circulatory Status
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT CASE CIRCULATORY WHEN 'False' THEN STATUS_DESCRIPTION ELSE '' END STATUS FROM dbo.LIBRARY_ITEMS_QUANTITY A " & _
                        " INNER JOIN  dbo.LIBRARY_ITEM_STATUS B ON A.STATUS_ID=B.STATUS_ID " & _
                        " WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "' AND CIRCULATORY='FALSE' AND A.ACTIVE='True' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            ReturnValue = "This item has been, " & ds.Tables(0).Rows(0).Item("STATUS").ToString() & " .<br>Item cannot be circulated."

            returnvalueB = False
        Else
            returnvalueB = True
        End If

        Return returnvalueB

    End Function

    Private Shared Function Reservations(ByVal Hash As Hashtable, ByRef ReturnValue As String) As Boolean
        Dim returnvalueB As Boolean = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        ''Get the reservation id for this item reserved by other user except this current issue user.
        Dim Sql_Query = " SELECT DISTINCT RESERVATION_ID FROM dbo.LIBRARY_ITEM_RESERVATION " & _
                        " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.PRODUCT_BSU_ID='" & Hash.Item("HiddenBsuID") & "'" & _
                        " WHERE LIBRARY_ITEM_RESERVATION.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "')" & _
                        " AND USER_ID != '" & Hash.Item("HiddenUserID") & "' AND RESERVATION_CANCEL='FALSE' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then

            '' Check if current user has reserved this item
            Sql_Query = " SELECT DISTINCT RESERVATION_ID FROM dbo.LIBRARY_ITEM_RESERVATION " & _
                        " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.PRODUCT_BSU_ID='" & Hash.Item("HiddenBsuID") & "'" & _
                        " WHERE LIBRARY_ITEM_RESERVATION.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "')" & _
                        " AND USER_ID = '" & Hash.Item("HiddenUserID") & "' AND USER_TYPE='" & Hash.Item("RadioUserTypeIssueItem") & "' AND  GETDATE()  BETWEEN RESERVE_START_DATE  AND RESERVE_END_DATE  AND RESERVATION_CANCEL='FALSE' "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            If ds.Tables(0).Rows.Count > 0 Then
                returnvalueB = True
            Else
                ''Check for excess stock for circulation available.
                Dim remitems = 0

                Sql_Query = " SELECT * FROM ( " & _
                                " select " & _
                                " (SELECT COUNT(*)  FROM dbo.LIBRARY_ITEMS_QUANTITY A " & _
                                " INNER JOIN dbo.LIBRARY_RACKS B ON A.RACK_ID=B.RACK_ID " & _
                                " INNER JOIN dbo.LIBRARY_SHELFS C ON B.SHELF_ID=C.SHELF_ID " & _
                                " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS D ON C.LIBRARY_SUB_DIVISION_ID= D.LIBRARY_SUB_DIVISION_ID " & _
                                " INNER JOIN dbo.LIBRARY_DIVISIONS E ON D.LIBRARY_DIVISION_ID=E.LIBRARY_DIVISION_ID " & _
                                " WHERE E.LIBRARY_DIVISION_ID='" & Hash.Item("Hiddenlibrarydivid") & "' AND A.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "')) LIBCOUNT, " & _
                                " ( " & _
                                " SELECT COUNT(*) FROM LIBRARY_ITEM_RESERVATION C  " & _
                                " WHERE C.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "') " & _
                                " AND isnull(RESERVE_START_DATE,'') != '' " & _
                                " AND isnull(RESERVE_END_DATE,'') != '' AND ISNULL(TRAN_RECORD_ID,'') = '' " & _
                                " AND C.LIBRARY_DIVISION_ID='" & Hash.Item("Hiddenlibrarydivid") & "' " & _
                                " )RCOUNT, " & _
                                " ( " & _
                                " SELECT count(*) FROM LIBRARY_TRANSACTIONS D " & _
                                " INNER JOIN dbo.LIBRARY_ITEMS_QUANTITY M ON M.STOCK_ID=D.STOCK_ID " & _
                                " WHERE M.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "') " & _
                                " AND isnull(ITEM_ACTUAL_RETURN_DATE,'') = ''  AND D.LIBRARY_DIVISION_ID='" & Hash.Item("Hiddenlibrarydivid") & "' " & _
                                " )TCOUNT" & _
                                " )AB "


                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
                Dim lcount = ds.Tables(0).Rows(0).Item("LIBCOUNT").ToString()
                Dim rcount = ds.Tables(0).Rows(0).Item("RCOUNT").ToString()
                Dim tcount = ds.Tables(0).Rows(0).Item("TCOUNT").ToString()
                remitems = Convert.ToInt16(lcount) - Convert.ToInt16(rcount) + Convert.ToInt16(tcount)

                If remitems > 0 Then
                    '' has excess stock
                    returnvalueB = True
                Else
                    '' has no excess stock
                    ReturnValue = "This item has been reserved by another user.Please view the reservation history.<br>Excess Stock not available."

                    returnvalueB = False
                End If


            End If

        Else
            returnvalueB = True
        End If


        Return returnvalueB
    End Function

    Public Shared Function CheckTakenCount(ByVal Hash As Hashtable, ByRef ReturnValue As String) As Boolean
        Dim returnvalueB = True
        If Hash.Item("Pouch") = False Then
            ReturnValue = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
            Dim Sql_Query = " SELECT MAX_ITEMS,LENDING_DAYS FROM LIBRARY_MEMBERSHIPS WHERE MEMBERSHIP_ID='" & Hash.Item("ddmemebershipissue") & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            Dim TakeCount = ""
            If ds.Tables(0).Rows.Count > 0 Then
                Dim Days = ds.Tables(0).Rows(0).Item("LENDING_DAYS")
                TakeCount = ds.Tables(0).Rows(0).Item("MAX_ITEMS")
            End If

            Sql_Query = " SELECT COUNT(*) FROM vw_LIBRARY_TRANSACTIONS_CHECK A " & _
                        " WHERE A.USER_TYPE='" & Hash.Item("RadioUserTypeIssueItem") & "' AND USER_ID='" & Hash.Item("HiddenUserID") & "' AND ISNULL(ITEM_ACTUAL_RETURN_DATE,'')= '' " & _
                        " AND A.MEMBERSHIP_ID='" & Hash.Item("ddmemebershipissue") & "'"

            Dim TakenCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)

            If TakenCount >= TakeCount Then

                ReturnValue = "Current membership allows only " & TakeCount & " Items for this member. Please try with any other membership."

                Hash.Item("btnIssue").Visible = False
                returnvalueB = False
            Else
                Hash.Item("btnIssue").Visible = True
                returnvalueB = True
            End If
        End If

        Return returnvalueB

    End Function

    Public Shared Function IssueItems(ByVal Hash As Hashtable) As String

        Dim ReturnValue As String = ""
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)

        If TabDisplay(Hash, ReturnValue) Then

            If Circulation(Hash, ReturnValue) Then

                ''Check Today Reservation
                If Reservations(Hash, ReturnValue) Then

                    '' Check for excess count item taken
                    If CheckTakenCount(Hash, ReturnValue) Then

                        ''If Reserved,then get the reservation id
                        Dim Sql_Query = " SELECT DISTINCT RESERVATION_ID FROM dbo.LIBRARY_ITEM_RESERVATION " & _
                                    " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.PRODUCT_BSU_ID='" & Hash.Item("HiddenBsuID") & "'" & _
                                    " WHERE LIBRARY_ITEM_RESERVATION.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")) & "')" & _
                                    " AND USER_ID = '" & Hash.Item("HiddenUserID") & "' AND USER_TYPE='" & Hash.Item("RadioUserTypeIssueItem") & "' AND RESERVATION_CANCEL='FALSE' "

                        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.Text, Sql_Query)

                        Dim HiddenReservationId As String = ""
                        If ds.Tables(0).Rows.Count > 0 Then
                            HiddenReservationId = ds.Tables(0).Rows(0).Item("RESERVATION_ID").ToString()
                        End If

                        Dim transaction As SqlTransaction
                        connection.Open()
                        transaction = connection.BeginTransaction()
                        ReturnValue = ""
                        Try

                            Dim pParms(10) As SqlClient.SqlParameter
                            pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", LibraryData.GetStockIDForAccessonNo(Hash.Item("txtstockid"), Hash.Item("HiddenBsuID")))
                            pParms(1) = New SqlClient.SqlParameter("@USER_TYPE", Hash.Item("RadioUserTypeIssueItem"))
                            pParms(2) = New SqlClient.SqlParameter("@USER_ID", Hash.Item("HiddenUserID"))
                            pParms(3) = New SqlClient.SqlParameter("@MEMBERSHIP_ID", Hash.Item("ddmemebershipissue"))

                            If Hash.Item("ddissuestatus") <> 0 Then

                                pParms(4) = New SqlClient.SqlParameter("@ITEM_BEFORE_STATUS_ID", Hash.Item("ddissuestatus"))

                            End If
                            If HiddenReservationId <> "" Then
                                pParms(5) = New SqlClient.SqlParameter("@RESERVATION_ID", HiddenReservationId)
                            End If
                            pParms(6) = New SqlClient.SqlParameter("@NOTES", Hash.Item("txtissuenotes"))
                            pParms(7) = New SqlClient.SqlParameter("@ISSUE_EMP_ID", Hash.Item("HiddenEmpid"))
                            pParms(8) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", Hash.Item("ddLibrary"))


                            ReturnValue = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_TRANSACTIONS", pParms)
                            transaction.Commit()


                        Catch ex As Exception

                            ReturnValue = "Error occurred while transactions. " & ex.Message
                            transaction.Rollback()

                        Finally

                            connection.Close()

                        End Try

                    End If

                End If

            End If

        End If

        Return ReturnValue

    End Function






    Private Shared Function Circulation(ByRef transaction As SqlTransaction, ByVal Hash As Hashtable) As Boolean

        Dim returnvalue As Boolean = True

        ''Check Circulatory Status
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT CASE CIRCULATORY WHEN 'False' THEN STATUS_DESCRIPTION ELSE '' END STATUS FROM dbo.LIBRARY_ITEMS_QUANTITY A " & _
                        " INNER JOIN  dbo.LIBRARY_ITEM_STATUS B ON A.STATUS_ID=B.STATUS_ID " & _
                        " WHERE STOCK_ID='" & GetStockIDForAccessonNo(Hash.Item("txtreturnstockid"), Hash.Item("HiddenBsuID"), transaction) & "' AND CIRCULATORY='FALSE' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            returnvalue = False
        Else
            returnvalue = True
        End If

        Return returnvalue

    End Function

    Private Shared Function SendMessageIfItemExists(ByRef transaction As SqlTransaction, ByVal Hash As Hashtable, ByRef ReturnValue As String) As Integer
        Dim messageflag = 0

        If Circulation(transaction, Hash) Then


            '' Check if Book Exists in library,if yes then update the reserve start date as today.
            Dim remitems = 0

            Dim sql_query = " select count(*)LIBCOUNT, " & _
                            " (SELECT COUNT(*) FROM LIBRARY_ITEM_RESERVATION C WHERE C.MASTER_ID = A.MASTER_ID  AND isnull(RESERVE_START_DATE,'') != '' AND isnull(RESERVE_END_DATE,'') != '' AND ISNULL(TRAN_RECORD_ID,'') = '' ) RCOUNT " & _
                            " from dbo.LIBRARY_ITEMS_QUANTITY A " & _
                            " left JOIN dbo.LIBRARY_TRANSACTIONS B ON B.STOCK_ID=A.STOCK_ID  " & _
                            " where A.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & GetStockIDForAccessonNo(Hash.Item("txtreturnstockid"), Hash.Item("HiddenBsuID"), transaction) & "')  AND A.ACTIVE='True' " & _
                            " AND PRODUCT_BSU_ID='" & Hash.Item("HiddenBsuID") & "' AND ISNULL(RACK_ID,'') != '' " & _
                            " AND A.STOCK_ID NOT IN (SELECT STOCK_ID FROM dbo.LIBRARY_TRANSACTIONS WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'') = '') " & _
                            " GROUP BY A.MASTER_ID "


            Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sql_query)
            Dim lcount = ds.Tables(0).Rows(0).Item("LIBCOUNT").ToString()
            Dim rcount = ds.Tables(0).Rows(0).Item("RCOUNT").ToString()

            remitems = lcount - rcount

            If remitems > 0 Then
                Dim query = ""
                ''Check if only one record (reservation), if yes then set start and end date and send message
                query = " select TOP 1  RESERVATION_ID from LIBRARY_ITEM_RESERVATION A " & _
                            " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.STOCK_ID='" & GetStockIDForAccessonNo(Hash.Item("txtreturnstockid"), Hash.Item("HiddenBsuID"), transaction) & "'" & _
                            " where A.MASTER_ID=(SELECT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE STOCK_ID='" & GetStockIDForAccessonNo(Hash.Item("txtreturnstockid"), Hash.Item("HiddenBsuID"), transaction) & "')" & _
                            " AND ISNULL(TRAN_RECORD_ID,'') = '' AND RESERVATION_CANCEL='False' AND isnull(RESERVE_START_DATE,'') = '' AND isnull(RESERVE_END_DATE,'') = ''"

                ds = SqlHelper.ExecuteDataset(transaction, CommandType.Text, query)

                If ds.Tables(0).Rows.Count = 1 Then

                    Dim NoReserveDays = 1
                    query = "UPDATE LIBRARY_ITEM_RESERVATION SET RESERVE_START_DATE=GETDATE(),RESERVE_END_DATE=DATEADD(Day," & NoReserveDays & " , GETDATE()) WHERE RESERVATION_ID='" & ds.Tables(0).Rows(0).Item("RESERVATION_ID").ToString() & "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                    messageflag = 1

                End If

            End If

        End If


        Return messageflag

    End Function

    Private Shared Function GetStockIDForAccessonNo(ByVal Accession_No As String, ByVal Bsu_id As String, ByRef transaction As SqlTransaction) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "Select STOCK_ID from LIBRARY_ITEMS_QUANTITY WHERE ACCESSION_NO='" & Accession_No & "' AND PRODUCT_BSU_ID='" & Bsu_id & "' "
        Dim stock_id = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

        Return stock_id
    End Function

    Public Shared Function ReturnItems(ByVal Hash As Hashtable) As String
        Dim ReturnValue As String = ""
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RECORD_ID", Hash.Item("HiddenRecordID"))

            If Hash.Item("ddreturnstatusentry") <> 0 Then
                pParms(1) = New SqlClient.SqlParameter("@ITEM_AFTER_STATUS_ID", Hash.Item("ddreturnstatusentry"))
            End If

            pParms(2) = New SqlClient.SqlParameter("@RETURN_NOTES", Hash.Item("txtReturnnotes"))
            pParms(3) = New SqlClient.SqlParameter("@FINE_AMOUNT", Hash.Item("txtreturnfineamount"))
            pParms(4) = New SqlClient.SqlParameter("@CURRENCY_ID", Hash.Item("ddReturncurrency"))
            pParms(5) = New SqlClient.SqlParameter("@RECEIVE_EMP_ID", Hash.Item("HiddenEmpid"))


            ReturnValue = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_RETURN_TRANSACTION", pParms)

            Dim messageflag = 0

            ''Send message if book exists 
            'messageflag = SendMessageIfItemExists(transaction, Hash, ReturnValue)

            transaction.Commit()

            If messageflag = 1 Then
                ''Send Message
            End If

         
        Catch ex As Exception
            ReturnValue = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

        Return ReturnValue
    End Function


End Class
