Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Public Class rptClass

    Dim crIns As String
    Dim crUserName As String
    Dim crUserPassword As String
    Dim crInstance As String

    Property crInstanceName() As String
        Set(ByVal value As String)
            crInstance = value
        End Set
        Get
            If crInstance = String.Empty Then
                Return System.Configuration.ConfigurationManager.AppSettings("InstanceName")
            Else
                Return crInstance
            End If
        End Get
    End Property

    Property crUser() As String
        Set(ByVal value As String)
            crUserName = value
        End Set
        Get
            If crUserName = String.Empty Then
                Return System.Configuration.ConfigurationManager.AppSettings("UserName")
            Else
                Return crUserName
            End If
        End Get
    End Property

    Property crPassword() As String
        Set(ByVal value As String)
            crUserPassword = value
        End Set
        Get
            If crUserPassword = String.Empty Then
                Return System.Configuration.ConfigurationManager.AppSettings("Password")
            Else
                Return crUserPassword
            End If
        End Get
    End Property

    Dim crDat As String
    Property crDatabase() As String
        Get
            Return crDat
        End Get
        Set(ByVal value As String)
            crDat = value
        End Set
    End Property

    Dim rptPath As String
    Property reportPath() As String
        Get
            Return rptPath
        End Get
        Set(ByVal value As String)
            rptPath = value
        End Set
    End Property

    Dim prev As rptClass
    Property previousClass() As rptClass
        Get
            Return prev
        End Get
        Set(ByVal value As rptClass)
            prev = value
        End Set
    End Property


    Dim param As Hashtable
    Property reportParameters() As Hashtable
        Get
            Return param
        End Get
        Set(ByVal value As Hashtable)
            If param Is Nothing Then
                param = New Hashtable()
            End If
            param = value
        End Set
    End Property

    Dim strFormula As String
    Property selectionFormula() As String
        Get
            Return strFormula
        End Get
        Set(ByVal value As String)
            strFormula = value
        End Set
    End Property

    Dim vPhoto As OASISPhotos
    Property Photos() As OASISPhotos
        Get
            Return vPhoto
        End Get
        Set(ByVal value As OASISPhotos)
            vPhoto = value
        End Set
    End Property

    Dim sr As Integer = 0

    Property subReportCount() As Integer
        Get
            Return sr
        End Get
        Set(ByVal value As Integer)
            sr = value
        End Set
    End Property
End Class

Public Class OASISPhotos
    Dim vPhototType As OASISPhotoType
    Dim arrIDs As ArrayList
    Dim vBSU_ID As String

    Public vHTPhoto_ID As Hashtable

    Public Sub New()
        arrIDs = New ArrayList
        vHTPhoto_ID = New Hashtable
    End Sub

    Public Property PhotoType() As OASISPhotoType
        Get
            Return vPhototType
        End Get
        Set(ByVal value As OASISPhotoType)
            vPhototType = value
        End Set
    End Property

    Public Property BSU_ID() As String
        Get
            Return vBSU_ID
        End Get
        Set(ByVal value As String)
            vBSU_ID = value
        End Set
    End Property

    Public Property IDs() As ArrayList
        Get
            Return arrIDs
        End Get
        Set(ByVal value As ArrayList)
            arrIDs = value
        End Set
    End Property

End Class