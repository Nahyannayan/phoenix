Imports System
Imports System.Collections.Generic
Imports System.Web.Services
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml
<WebService([Namespace]:="http://gemsStudentServices.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
Public Class StudentService
    Inherits System.Web.Services.WebService
    Public Sub New()
    End Sub
    
    <WebMethod(Description:="Student Name Info", EnableSession:=True)> _
           Public Function StudentNAME(ByVal prefixText As String) As String()

        Dim ContextVAL As String = HttpContext.Current.Session("WEB_SER_VAR")
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "|"
        Dim ACD_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim STRFILTER As String = String.Empty

        arInfo = ContextVAL.Split(splitter)

        If arInfo.Length = 2 Then
            ACD_ID = arInfo(0)
            GRD_ID = arInfo(1)

        Else
            ACD_ID = arInfo(0)
        End If

        If GRD_ID = "" Or UCase(GRD_ID) = "ALL" Or GRD_ID = "0" Then
            STRFILTER = " AND STU_GRD_ID<>'' "
        Else
            STRFILTER = " AND STU_GRD_ID='" & GRD_ID & "'"
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String

        str_Sql = " Select top 7  SNAME from (SELECT STU_FIRSTNAME + ' '+isnull(STU_MIDNAME,'') +' '+isnull(STU_LASTNAME,'') as SNAME from student_M " & _
                  " where stu_acd_id='" & ACD_ID & "' " & STRFILTER & " )A where SNAME like '%" + prefixText + "%' "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer
        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function
    <WebMethod(Description:="Student NO Info", EnableSession:=True)> _
          Public Function StudentNO(ByVal prefixText As String) As String()

        Dim ContextVAL As String = HttpContext.Current.Session("WEB_SER_VAR")
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "|"
        Dim ACD_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim STRFILTER As String = String.Empty

        arInfo = ContextVAL.Split(splitter)

        If arInfo.Length = 2 Then
            ACD_ID = arInfo(0)
            GRD_ID = arInfo(1)

        Else
            ACD_ID = arInfo(0)
        End If

        If GRD_ID = "" Or UCase(GRD_ID) = "ALL" Or GRD_ID = "0" Then
            STRFILTER = " AND STU_GRD_ID<>'' "
        Else
            STRFILTER = " AND STU_GRD_ID='" & GRD_ID & "'"
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String

        str_Sql = " Select top 7 stu_fee_id,stu_no from (SELECT  stu_fee_id,stu_no from student_M " & _
                  " where stu_acd_id='" & ACD_ID & "' " & STRFILTER & " )A where STU_fee_id like '%" + prefixText + "%' or STU_NO like '%" + prefixText + "%' "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count

        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function
End Class
