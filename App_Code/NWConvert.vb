Imports Microsoft.VisualBasic

Public Class NWConvert
    ''' <summary>
    ''' 'Day - 1 to 31
    ''' 'Month - 1 to 12
    ''' 'Year - 1901 to 2099
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function DateConvert(ByVal day As String, ByVal month As Integer, ByVal year As String) As String
        Dim retutnval As String = ""
        Try

            If Convert.ToInt16(day) > 0 And Convert.ToInt16(day) <= 31 And month > 0 And month <= 12 And Convert.ToInt16(year) > 1900 And Convert.ToInt16(year) < 3000 Then
                If day.Length = 1 Then
                    day = "0" & day
                End If

                retutnval = TenthPlace(day) & "  " & Monthcal(month) & "  " & yearword(year)
            Else
                retutnval = "Error In Date"
            End If
        Catch ex As Exception
            retutnval = "Error In Date"
        End Try
        Return retutnval
    End Function


    Private Shared Function ThousandPlace(ByVal val As Integer) As String
        Dim returnstring As String = ""
        If val = 1 Then
            returnstring = "ONE THOUSAND"
        ElseIf val = 2 Then
            returnstring = "TWO THOUSAND"
        End If

        Return returnstring

    End Function
    Private Shared Function HundredPlace(ByVal val As Integer) As String
        Dim returnstring As String = ""
        If val <> 0 Then
            returnstring = LastPlace(val) & " HUNDRED"
        End If

        Return returnstring
    End Function
    Private Shared Function TenthPlace(ByVal val As String) As String
        Dim value As String = val.ToString()

        Dim split0 = value.Substring(0, 1)
        Dim split1 = value.Substring(1)
        Dim returnstring As String = ""
        If split0 = 0 Then
            returnstring = LastPlace(split1)
        ElseIf split0 = 1 Then
            If val = 10 Then
                returnstring = LastPlace(val)
            Else
                returnstring = NTeenPlace(val)
            End If
        ElseIf split0 = 2 Then
            returnstring = "TWENTY " & LastPlace(split1)
        ElseIf split0 = 3 Then
            returnstring = "THIRTY " & LastPlace(split1)
        ElseIf split0 = 4 Then
            returnstring = "FOURTY " & LastPlace(split1)
        ElseIf split0 = 5 Then
            returnstring = "FIFTY " & LastPlace(split1)
        ElseIf split0 = 6 Then
            returnstring = "SIXTY " & LastPlace(split1)
        ElseIf split0 = 7 Then
            returnstring = "SEVENTY " & LastPlace(split1)
        ElseIf split0 = 8 Then
            returnstring = "EIGHTY " & LastPlace(split1)
        ElseIf split0 = 9 Then
            returnstring = "NINTY " & LastPlace(split1)
        End If

        Return returnstring
    End Function
    Private Shared Function LastPlace(ByVal val As Integer) As String
        Dim returnstring As String = ""
        If val <> 0 Then
            returnstring = " AND "
        End If
        If val = 0 Then
            ''returnstring = "ZERO"
        ElseIf val = 1 Then
            returnstring = "ONE"
        ElseIf val = 2 Then
            returnstring = "TWO"
        ElseIf val = 3 Then
            returnstring = "THREE"
        ElseIf val = 4 Then
            returnstring = "FOUR"
        ElseIf val = 5 Then
            returnstring = "FIVE"
        ElseIf val = 6 Then
            returnstring = "SIX"
        ElseIf val = 7 Then
            returnstring = "SEVEN"
        ElseIf val = 8 Then
            returnstring = "EIGHT"
        ElseIf val = 9 Then
            returnstring = "NINE"
        ElseIf val = 10 Then
            returnstring = "TEN"
        End If
        Return returnstring
    End Function
    Private Shared Function NTeenPlace(ByVal val As Integer)
        Dim returnstring As String = ""
        If val = 11 Then
            returnstring = "ELEVEN"
        ElseIf val = 12 Then
            returnstring = "TWELVE"
        ElseIf val = 13 Then
            returnstring = "THIRTEEN"
        ElseIf val = 14 Then
            returnstring = "FOURTEEN"
        ElseIf val = 15 Then
            returnstring = "FIFTEEN"
        ElseIf val = 16 Then
            returnstring = "SIXTEEN"
        ElseIf val = 17 Then
            returnstring = "SEVENTEEN"
        ElseIf val = 18 Then
            returnstring = "EIGHTEEN"
        ElseIf val = 19 Then
            returnstring = "NINETEEN"
        End If

        Return returnstring
    End Function
    Private Shared Function Monthcal(ByVal val As Integer) As String
        Dim returnval As String = ""
        If val = 1 Then
            returnval = "January"
        ElseIf val = 2 Then
            returnval = "February"
        ElseIf val = 3 Then
            returnval = "March"
        ElseIf val = 4 Then
            returnval = "April"
        ElseIf val = 5 Then
            returnval = "May"
        ElseIf val = 6 Then
            returnval = "June"
        ElseIf val = 7 Then
            returnval = "July"
        ElseIf val = 8 Then
            returnval = "August"
        ElseIf val = 9 Then
            returnval = "September"
        ElseIf val = 10 Then
            returnval = "October "
        ElseIf val = 11 Then
            returnval = "November"
        ElseIf val = 12 Then
            returnval = "December"
        End If
        Return returnval.ToUpper()
    End Function
    Private Shared Function yearword(ByVal year As String) As String
        Dim val As String = year
        Dim returnstring As String = ""
        Dim len = val.Length

        Dim split1 = val.Substring(0, 2)
        Dim split2 = val.Substring(2, 2)
        If split1 < 20 Then
            returnstring = NTeenPlace(split1) & " " & TenthPlace(split2)
        Else
            split1 = val.Substring(0, 1)
            returnstring = LastPlace(split1) & " THOUSAND" & " " & TenthPlace(split2)
        End If
        Return returnstring
    End Function
End Class
