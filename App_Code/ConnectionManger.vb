Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Public Class ConnectionManger

    Public Shared Function GetASPNETDBConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("ASPNETDB").ConnectionString
    End Function

    Public Shared Function GetASPNETDBConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ASPNETDB").ConnectionString)
        Connection.Open()
        GetASPNETDBConnection = Connection
    End Function
    Public Shared Function GetOASIS_ENRConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_ENRConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_ENRConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_ENRConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_ENRConnection = Connection
    End Function
    Public Shared Function GetOASISConnectionhr1() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString)
        Connection.Open()
        GetOASISConnectionhr1 = Connection
    End Function

    Public Shared Function GetOASIS_CURRICULUMConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_CCAConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_CCAConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_PDP_V2Connection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V2ConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_PDP_V2Connection = Connection
    End Function
    Public Shared Function GetOASIS_PDP_V2ConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_PDP_V2ConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_PDP_V3Connection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_PDP_V3Connection = Connection
    End Function
    Public Shared Function GetOASIS_PDP_V3ConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_QBConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_QBConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_GLGConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_GLGConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_SERVICESConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_SERVICESConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_SERVICESConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_SERVICESConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_SERVICESConnection = Connection
    End Function


    Public Shared Function GetGLGConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("GLG_OASISConnectionString").ConnectionString)
        Connection.Open()
        GetGLGConnection = Connection
    End Function

    Public Shared Function GetOASIS_CURRICULUMConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_CURRICULUMConnection = Connection
    End Function

    Public Shared Function GetOASIS_CCAConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CCAConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_CCAConnection = Connection
    End Function

    Public Shared Function GetOASISConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_COMTRACK_ConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASISConnectionStringCOMTRACK").ConnectionString
    End Function

    Public Shared Function GetOASISPROMOConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASISPROMOConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASISConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Connection.Open()
        GetOASISConnection = Connection
    End Function
    Public Shared Function GetOASIS_COMTRACKConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringCOMTRACK").ConnectionString)
        Connection.Open()
        GetOASIS_COMTRACKConnection = Connection
    End Function
    Public Shared Function GetOASISTRANSPORTConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    End Function
    Public Shared Function GetGPSConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
    End Function

    Public Shared Function GetOASISTransportConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString)
        Connection.Open()
        GetOASISTransportConnection = Connection
    End Function

    Public Shared Function GetOASISSurveyConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_SURVEYConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASISSurveyConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_SURVEYConnectionString").ConnectionString)
        Connection.Open()
        GetOASISSurveyConnection = Connection
    End Function

    Public Shared Function GetOASISFINConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
    End Function

    Public Shared Function GetOASISFinConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("MainDB").ConnectionString)
        Connection.Open()
        GetOASISFinConnection = Connection
    End Function

    Public Shared Function GetOASISAuditConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString)
        Connection.Open()
        GetOASISAuditConnection = Connection
    End Function

    Public Shared Function GetOASIS_QBConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_QBConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_QBConnection = Connection
    End Function

    Public Shared Function GetOASISAuditConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_FEESConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_FEESConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_FEESConnection = Connection
    End Function

    Public Shared Function GetOASIS_ASSETConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_ASSETConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_ASSETConnection = Connection
    End Function
    Public Shared Function GetOASIS_REPORTINGConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_REPORTINGConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_REPORTINGConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_REPORTINGConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_REPORTINGConnection = Connection
    End Function

    Public Shared Function GetOASIS_TRANSPORT_ATTENDANCEConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_TRANSPORT_ATTENDANCEConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_TRANSPORT_ATTENDANCEConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORT_ATTENDANCEConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_TRANSPORT_ATTENDANCEConnection = Connection
    End Function
    Public Shared Function GetOASIS_HRConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_HRConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_HRConnection = Connection
    End Function

    Public Shared Function GetOASIS_DOCSConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_TIMETABLEConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_TIMETABLEConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_TIMETABLEConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_TIMETABLEConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_TIMETABLEConnection = Connection
    End Function
    Public Shared Function GetOASIS_HR_ERecruitmentConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_HR_V2_NEW3_ConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_PUR_INVConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_PDPConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_PDPConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_PDPConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDPConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_PDPConnection = Connection
    End Function


    Public Shared Function GetOASIS_PDP_PRINCIPALConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_PDP_PRINCIPALConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_PDP_PRINCIPALConnection = Connection
    End Function

    Public Shared Function GetOASIS_INTEGRATIONConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
    End Function
    Public Shared Function OASIS_DAX_AUDIT_ConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_DAX_AUDIT_ConnectionString").ConnectionString
    End Function
    Public Shared Function OASIS_DAX_ConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_CLMConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_CLMConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_CLMConnection = Connection
    End Function

    Public Shared Function GetTellalPDConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("Tellal_PD_ConnectionString").ConnectionString
    End Function


    Public Shared Function Get_TellalPD_Connection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Tellal_PD_ConnectionString").ConnectionString)
        Connection.Open()
        Get_TellalPD_Connection = Connection
    End Function

    Public Shared Function GetCORPAccessConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_CORP_ACCESS_CONTROL").ConnectionString
    End Function

    Public Shared Function Get_PHOENIXMISC_Connection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("PHOENIXMISC_ConnectionString").ConnectionString)
        Connection.Open()
        Get_PHOENIXMISC_Connection = Connection
    End Function

    Public Shared Function Get_PHOENIXMISC_ConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("PHOENIXMISC_ConnectionString").ConnectionString
    End Function


    Public Shared Function GetOASISMedicalConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString
    End Function
    Public Shared Function GetOASISMedicalConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString)
        Connection.Open()
        GetOASISMedicalConnection = Connection
    End Function

    Public Shared Function GetPhoenix_ASCConnectionString() As String

        Return ConfigurationManager.ConnectionStrings("Phoenix_ASC").ConnectionString
    End Function

    Public Shared Function GetPhoenix_ASCConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Phoenix_ASC").ConnectionString)
        Connection.Open()
        GetPhoenix_ASCConnection = Connection
    End Function

End Class
