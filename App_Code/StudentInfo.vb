﻿Imports Microsoft.VisualBasic

Public Class StudentInfo
    Public Sub New(ByVal STU_ID As String, ByVal STU_NAME As String)
        Me.STU_ID = STU_ID
        Me.STU_NAME = STU_NAME
    End Sub
    Private _STUID As String
    Private _STUNAME As String
    Public Property STU_ID() As String
        Get
            Return _STUID
        End Get
        Set(ByVal value As String)
            _STUID = value
        End Set
    End Property
    Public Property STU_NAME() As String
        Get
            Return _STUNAME
        End Get
        Set(ByVal value As String)
            _STUNAME = value
        End Set
    End Property
End Class
