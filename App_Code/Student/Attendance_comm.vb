Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Attendance
    Private var_GRD_TEXT As String
    Private Var_GRD_ID As String
    Private var_SCT_TEXT As String
    Private Var_SCT_ID As String


    Public Property GRD_TEXT() As String
        Get
            Return var_GRD_TEXT
        End Get
        Set(ByVal value As String)

            var_GRD_TEXT = value
        End Set
    End Property

    Public Property GRD_ID() As String
        Get
            Return Var_GRD_ID
        End Get
        Set(ByVal value As String)
            Var_GRD_ID = value
        End Set
    End Property
    Public Property SCT_TEXT() As String
        Get
            Return var_SCT_TEXT
        End Get
        Set(ByVal value As String)
            var_SCT_TEXT = value
        End Set
    End Property

    Public Property SCT_ID() As String
        Get
            Return Var_SCT_ID
        End Get
        Set(ByVal value As String)
            Var_SCT_ID = value
        End Set
    End Property
    Public Sub New(ByVal GTEXT As String, ByVal GID As String, ByVal STEXT As String, ByVal SID As String)

        GRD_TEXT = GTEXT
        GRD_ID = GID
        SCT_TEXT = STEXT
        SCT_ID = SID
    End Sub

End Class
