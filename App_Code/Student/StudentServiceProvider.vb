Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
''' The class Created for handling the Concession Type
''' 
''' Author : SHIJIN C A
''' Date : 12-JUNE-2008 
''' </summary>
''' <remarks></remarks>
''' 
Public Class StudentServiceProvider

    ''' <summary>
    ''' Variables used to Store the 
    ''' </summary>
    ''' <remarks></remarks>
    Dim vSVB_ID As Integer
    Dim vBSU_ID As String
    Dim vPROV_BSU_ID As String
    Dim vSVC_ID As Integer
    Dim vACD_ID As Integer
    Dim vRATE As Double
    Dim bServiceAvailable As Boolean

    Dim bDelete As Boolean

    Public Property Delete() As Boolean
        Get
            Return bDelete
        End Get
        Set(ByVal value As Boolean)
            bDelete = value
        End Set
    End Property

    Public Property ServiceAvailable() As Boolean
        Get
            Return bServiceAvailable
        End Get
        Set(ByVal value As Boolean)
            bServiceAvailable = value
        End Set
    End Property

    Public Property SVB_ID() As Integer
        Get
            Return vSVB_ID
        End Get
        Set(ByVal value As Integer)
            vSVB_ID = value
        End Set
    End Property

    Public Property PROVIDER_BSU_ID() As String
        Get
            Return vPROV_BSU_ID
        End Get
        Set(ByVal value As String)
            vPROV_BSU_ID = value
        End Set
    End Property

    Public Property BSU_ID() As String
        Get
            Return vBSU_ID
        End Get
        Set(ByVal value As String)
            vBSU_ID = value
        End Set
    End Property

    Public Property SERVICE_ID() As Integer
        Get
            Return vSVC_ID
        End Get
        Set(ByVal value As Integer)
            vSVC_ID = value
        End Set
    End Property

    Public Property ACD_ID() As Integer
        Get
            Return vACD_ID
        End Get
        Set(ByVal value As Integer)
            vACD_ID = value
        End Set
    End Property

    Public Property RATE() As Double
        Get
            Return vRATE
        End Get
        Set(ByVal value As Double)
            vRATE = value
        End Set
    End Property

    Public Shared Function GetSERVICES_SYS_M() As DataTable
        Dim sql_query As String = "SELECT SVC_ID,  SVC_DESCRIPTION FROM SERVICES_SYS_M"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function SaveDetails(ByVal vSER_PROV As StudentServiceProvider, ByRef vNEW_SVB_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

        If vSER_PROV Is Nothing Then
            Return -1
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        '@FCM_BSU_ID	varchar(20),
        '@FCM_DESCR	varchar(50),
        '@userID varchar(20),

        cmd = New SqlCommand("std_SaveSTUDENTSERVICEPROVIDER", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpSVB_BSU_ID As New SqlParameter("@SVB_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSVB_BSU_ID.Value = vSER_PROV.BSU_ID
        cmd.Parameters.Add(sqlpSVB_BSU_ID)

        Dim sqlpSVB_PROVIDER_BSU_ID As New SqlParameter("@SVB_PROVIDER_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSVB_PROVIDER_BSU_ID.Value = vSER_PROV.PROVIDER_BSU_ID
        cmd.Parameters.Add(sqlpSVB_PROVIDER_BSU_ID)

        Dim sqlpSVB_ACD_ID As New SqlParameter("@SVB_ACD_ID", SqlDbType.Int)
        sqlpSVB_ACD_ID.Value = vSER_PROV.ACD_ID
        cmd.Parameters.Add(sqlpSVB_ACD_ID)

        Dim sqlpSVB_SVC_ID As New SqlParameter("@SVB_SVC_ID", SqlDbType.Int)
        sqlpSVB_SVC_ID.Value = vSER_PROV.SERVICE_ID
        cmd.Parameters.Add(sqlpSVB_SVC_ID)

        Dim sqlpSVB_bAVAILABLE As New SqlParameter("@SVB_bAVAILABLE", SqlDbType.Bit)
        sqlpSVB_bAVAILABLE.Value = vSER_PROV.ServiceAvailable
        cmd.Parameters.Add(sqlpSVB_bAVAILABLE)

        Dim sqlpSVB_RATE As New SqlParameter("@SVB_RATE", SqlDbType.Decimal)
        sqlpSVB_RATE.Value = vSER_PROV.RATE
        cmd.Parameters.Add(sqlpSVB_RATE)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vSER_PROV.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        If vSER_PROV.SVB_ID <> 0 Then
            Dim sqlpSVB_ID As New SqlParameter("@SVB_ID", SqlDbType.Int)
            sqlpSVB_ID.Value = vSER_PROV.SVB_ID
            cmd.Parameters.Add(sqlpSVB_ID)
        End If

        Dim sqlpNEWSVB_ID As New SqlParameter("@NEWSVB_ID", SqlDbType.Int)
        sqlpNEWSVB_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEWSVB_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If Not sqlpNEWSVB_ID Is Nothing And (Not sqlpNEWSVB_ID.Value Is DBNull.Value) Then
            vNEW_SVB_ID = sqlpNEWSVB_ID.Value
        End If
        Return iReturnvalue
    End Function

    Public Shared Function GetDetails(ByVal vSVB_ID As Integer) As StudentServiceProvider
        Dim cmd As SqlCommand
        Dim vServProv As New StudentServiceProvider
        Dim str_sql As String
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        str_sql = " SELECT SVB_ID, SVB_BSU_ID, SVB_ACD_ID, SVB_RATE, SVB_SVC_ID, isnull(SVB_bAVAILABLE, 0) SVB_bAVAILABLE, " & _
        " SVB_PROVIDER_BSU_ID FROM SERVICES_BSU_M WHERE SVB_ID = " & vSVB_ID

        cmd = New SqlCommand(str_sql, conn)
        cmd.CommandType = CommandType.Text
        Try
            conn.Close()
            conn.Open()
            Dim sqlReader As SqlDataReader = cmd.ExecuteReader()
            While (sqlReader.Read())
                vServProv.BSU_ID = sqlReader("SVB_BSU_ID")
                vServProv.PROVIDER_BSU_ID = sqlReader("SVB_PROVIDER_BSU_ID")
                vServProv.ACD_ID = sqlReader("SVB_ACD_ID")
                vServProv.RATE = sqlReader("SVB_RATE")
                vServProv.SERVICE_ID = sqlReader("SVB_SVC_ID")
                vServProv.ServiceAvailable = sqlReader("SVB_bAVAILABLE")
                vServProv.SVB_ID = sqlReader("SVB_ID")
                Exit While
            End While
        Catch ex As Exception
            Return Nothing
        Finally
            conn.Close()
        End Try
        Return vServProv
    End Function
End Class
