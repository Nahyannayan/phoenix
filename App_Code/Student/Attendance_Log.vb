Imports Microsoft.VisualBasic

Public Class Attendance_Log
    Dim vStud_Id As String
    Dim vRemark As String
 

    Public Property Stud_Id() As String
        Get
            Return vStud_Id
        End Get
        Set(ByVal value As String)
            vStud_Id = value
        End Set
    End Property

    Public Property Remark() As String
        Get
            Return vRemark
        End Get
        Set(ByVal value As String)
            vRemark = value
        End Set
    End Property

End Class
