﻿Imports System.Data
Imports System.Web
Imports System.Collections
Imports System.Web.Services
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Encryption64
Imports System.Web.Services.Protocols
Imports Microsoft.VisualBasic
Imports System.DirectoryServices
Public Class LDAP_CHANGEPWD
    Public Shared Function ChangePassword(ByVal username As String, ByVal oldPassword As String, ByVal newPassword As String) As Boolean
        Dim ADPassword As Boolean = ChangeADPassword(username, oldPassword, newPassword)
        Return ADPassword
    End Function


    Public Shared Function ChangeADPassword(ByVal username As String, ByVal oldPassword As String, ByVal newPassword As String) As Boolean
        Try
            Dim uEntry As DirectoryEntry = getUserByUsername(username, oldPassword)
            Try
                'UtilityObj.Errorlog(uEntry.Name, "chang-uEntry")
                uEntry.AuthenticationType = AuthenticationTypes.Secure
                ''  uEntry.Invoke("ChangePassword", oldPassword, newPassword)
                uEntry.Invoke("SetPassword", New Object() {newPassword})
                uEntry.CommitChanges()
                uEntry.Close()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "ChangePWDLDAP-ChangeADPassword0")
                UtilityObj.Errorlog(ex.InnerException.ToString, "ChangePWDLDAP-ChangeADPassword000")

                Return False
            End Try
            uEntry.Properties("LockOutTime").Value = 0
            uEntry.Close()
            Return True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "ChangePWDLDAP-ChangeADPassword")
            Return False
        End Try

    End Function

    Public Shared Function getUserByUsername(ByVal username As String, ByVal newPassword As String) As DirectoryEntry
        Try
            Dim SearchRoot As DirectoryEntry = New DirectoryEntry("LDAP://10.100.100.5", "oasisadmin@gemslearninggateway.com", "GEM$Oas!s15")
            Dim mySearch As DirectorySearcher = New DirectorySearcher(SearchRoot)
            'UtilityObj.Errorlog(SearchRoot.Name, "chang-SearchRoot")

            mySearch.Filter = String.Format("(samAccountName={0})", username)
            Dim myResult As SearchResult = mySearch.FindOne()

            'UtilityObj.Errorlog(myResult.Path, "chang-myresult")
            If (Not (myResult) Is Nothing) Then
                Return myResult.GetDirectoryEntry()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "ChangePWDLDAP-getUserByUsername")
            Return Nothing
        End Try
    End Function

End Class
