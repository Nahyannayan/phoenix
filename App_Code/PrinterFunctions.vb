Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration

Public Class PrinterFunctions

    Public Shared Function GetPostBackControl(ByVal page As System.Web.UI.Page) As System.Web.UI.Control
        Dim control As Control = Nothing
        Dim ctrlname As String = page.Request.Params("__EVENTTARGET")
        If ctrlname IsNot Nothing AndAlso ctrlname <> [String].Empty Then
            control = page.FindControl(ctrlname)
        Else
            ' if __EVENTTARGET is null, control is a button type and need to 
            ' iterate over the form collection to find it 
            Dim ctrlStr As String = [String].Empty
            Dim c As Control = Nothing
            For Each ctl As String In page.Request.Form

                ' handle ImageButton controls 
                If ctl.EndsWith(".x") OrElse ctl.EndsWith(".y") Then
                    ctrlStr = ctl.Substring(0, ctl.Length - 2)
                    c = page.FindControl(ctrlStr)
                Else
                    c = page.FindControl(ctl)
                End If
                If TypeOf c Is System.Web.UI.WebControls.Button OrElse TypeOf c Is System.Web.UI.WebControls.ImageButton Then
                    control = c
                    Exit For
                End If
            Next
        End If
        Return control
    End Function

    Public Shared Function UpdatePrintCopy(ByVal subID As String, ByVal docType As String, ByVal docNo As String) As Boolean
        HttpContext.Current.Session("PrintUpdate") = True
        HttpContext.Current.Session("PrintDocType") = docType
        HttpContext.Current.Session("PrintSUBID") = subID
        HttpContext.Current.Session("PrintDocNo") = docNo
        Dim str_sql As String = "Select VHH_Count FROM VOUCHER_H where VHH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & _
        "' and VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR") & " and VHH_DOCTYPE = '" & HttpContext.Current.Session("PrintDocType") & "' and VHH_DOCNO = '" & HttpContext.Current.Session("PrintDocNo") & "'"
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_sql)
        If Not TypeOf count Is DBNull Then
            If count > 0 Then
                Return True
            End If
        End If
        Return False
    End Function
End Class
