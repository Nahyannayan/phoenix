﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class GlobalSearchService
    Inherits System.Web.Services.WebService
    Dim Encr_decrData As New Encryption64



    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()> _
    Public Function SearchStudent(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String, ByVal key As String, ByVal modulekey As String) As List(Of StudentInfo)
        Dim student As New List(Of StudentInfo)()
        Dim ImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString()
        'ImagePath = ImagePath.TrimEnd().Substring(0, ImagePath.Length - 1)
        Dim strConn As String = ""
        Dim COMPID = -1, BSUID As String = "", STUTYPE = "S", usr As String = "", modulename As String = ""
        If Not contextKey Is Nothing Then
            BSUID = Encr_decrData.Decrypt(contextKey)
        End If
        If Not key Is Nothing Then
            usr = Encr_decrData.Decrypt(key)
        End If
        If Not modulekey Is Nothing Then
            modulename = Encr_decrData.Decrypt(modulekey)
        End If
        Dim Qry As New StringBuilder
        Qry.Append("EXEC dbo.GLOBAL_SEARCH @SEARCH_TERM = 'STUDENT', @STU_TYPE = '" & STUTYPE & "', @BSU_ID = '" & BSUID & "',@username='" & usr & "',@modulekey='" & modulename & "', @PREFIX_TEXT = '" & prefixText.Trim & "'")
        strConn = ConnectionManger.GetOASISConnectionString

        Using conn As New SqlConnection()
            conn.ConnectionString = strConn
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                'cmd.Parameters.AddWithValue("@BSUID", BSUID)
                'cmd.Parameters.AddWithValue("@prefix", Trim(prefixText))
                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        'student.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem((String.Format("{0}$${1}$${2}", sdr("STU_NO").ToString, sdr("STU_NAME").ToString, sdr("STU_PHOTOPATH").ToString)), (sdr("STU_ID").ToString)))
                        'student.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(("."), (String.Format("{0}$${1}$${2}$${3}$${4}", sdr("STU_ID").ToString, sdr("STU_NO").ToString, sdr("STU_NAME").ToString, sdr("STU_PHOTOPATH").ToString, sdr("STU_GRADE_DESCR").ToString))))
                        'student.Add(String.Format("{0}$${1}$${2}$${3}$${4}", sdr("STU_ID").ToString, sdr("STU_NO").ToString, sdr("STU_NAME").ToString, sdr("STU_PHOTOPATH").ToString, sdr("STU_GRADE_DESCR").ToString))
                        Dim stuinfo = New StudentInfo(sdr("STU_ID"), sdr("STU_NAME"))
                        student.Add(stuinfo)
                    End While
                End Using
                conn.Close()
            End Using
            'If student.Count = 0 Then
            '    student.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(("No matches found!"), 0))
            'End If
            Return student
        End Using
    End Function

    <System.Web.Script.Services.ScriptMethod(), _
     System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Function GET_UNITS(ByVal prefix As String) As List(Of BsuInfo)
        Dim bsunits As New List(Of BsuInfo)()
        Dim UserId = HttpContext.Current.Session("sUsr_id")
        Dim roleid = HttpContext.Current.Session("sroleid")
        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_ID", SqlDbType.VarChar, 50)
                pParms(0).Value = UserId
                cmd.Parameters.Add(pParms(0))
                pParms(1) = New SqlClient.SqlParameter("@USR_ROL_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = roleid
                cmd.Parameters.Add(pParms(1))
                pParms(2) = New SqlClient.SqlParameter("@INFO_TYPE", SqlDbType.VarChar, 10)
                pParms(2).Value = "UNIT"
                cmd.Parameters.Add(pParms(2))
                pParms(3) = New SqlClient.SqlParameter("@SEARCH_STR", SqlDbType.VarChar, 500)
                pParms(3).Value = prefix
                cmd.Parameters.Add(pParms(3))
                cmd.CommandText = "[usr_db].[GETUSR_BSU_LOGIN]"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        Dim bsuinfo = New BsuInfo(sdr("T_BSU_ID"), sdr("BSU_NAME"))
                        bsunits.Add(bsuinfo)
                    End While
                End Using
                conn.Close()
            End Using
            Return bsunits
        End Using
    End Function

    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()> _
    Public Function GetStudent(ByVal BSUID As String, ByVal STU_TYPE As String, ByVal prefix As String, ByVal COMPID As String, ByVal STUBSUID As String) As String()
        Dim student As New List(Of String)()
        Dim strConn As String = ""
        Dim Qry As New StringBuilder

        Qry.Append("EXEC dbo.GLOBAL_SEARCH @SEARCH_TERM = 'STUDENT', @STU_TYPE = '" & STU_TYPE & "', @BSU_ID = '" & BSUID & "', @STU_BSU_ID = '" & STUBSUID & "',@username='',@modulekey='', @PREFIX_TEXT = '" & prefix.Trim & "', @bEXCLUDE_CSS_STYLE = 1")
        strConn = ConnectionManger.GetOASISConnectionString
        Using conn As New SqlConnection()
            conn.ConnectionString = strConn
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        student.Add(String.Format("{0}$${1}$${2}", sdr("STU_NAME").ToString.Replace("-", " "), sdr("STU_NO"), sdr("STU_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return student.ToArray()
        End Using
    End Function

    <WebMethod()> _
    Public Function GetCompany(ByVal prefix As String, ByVal COUNTRY_ID As String) As String()
        Dim company As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT COMP_ID,COMP_NAME FROM dbo.COMP_LISTED_M WITH(NOLOCK) WHERE ISNULL(COMP_bACTIVE,0)=1 AND ISNULL(COMP_COUNTRY,'')='" & COUNTRY_ID & "' AND " & "COMP_NAME like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        company.Add(String.Format("{0}-{1}", sdr("COMP_NAME").ToString.Replace("-", " "), sdr("COMP_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return company.ToArray()
        End Using
    End Function
End Class