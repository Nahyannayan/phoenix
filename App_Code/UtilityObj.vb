Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.Web.Configuration
Imports System.Web
Imports System.Xml
Imports System

Public Class UtilityObj
    Private _name As String
    Private _value As String

    Shared Sub New()
        'HttpContext.Current.Session("beforeArrayList") = New List(Of UtilityObj)
        'HttpContext.Current.Session("afterArraylist") = New List(Of UtilityObj)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Value As String)
        _name = Name
        _value = Value
    End Sub

    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Name
        End Set
    End Property

    Public Property Value() As String
        Get
            Return (_value)
        End Get
        Set(ByVal Value As String)
            _value = Value
        End Set
    End Property

    Public Shared Sub afterLoopingControls(ByVal oControl As Control)
        Try
            Dim afterArraylist As New List(Of UtilityObj)
            afterArraylist = afterLoopingControls1(oControl, afterArraylist)
            HttpContext.Current.Session("afterArraylist") = afterArraylist
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Shared Sub NoOpen(ByVal pageHeader As HtmlHead)
        Dim hm As New HtmlMeta()
        hm.Name = "DownloadOptions"
        hm.Content = "noopen"
        pageHeader.Controls.Add(hm)
    End Sub

    Public Shared Function afterLoopingControls1(ByVal oControl As Control, ByVal afterlist As Object) As Object
        Try
            Dim afterArraylist = afterlist
            Dim frmCtrl As Control
            Dim strRem As String = ""
            For Each frmCtrl In oControl.Controls
                If TypeOf frmCtrl Is TextBox Then
                    afterArraylist.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, TextBox).Text))
                ElseIf TypeOf frmCtrl Is CheckBox Then
                    afterArraylist.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, CheckBox).Checked))
                ElseIf TypeOf frmCtrl Is RadioButton Then
                    afterArraylist.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, RadioButton).Checked))
                ElseIf TypeOf frmCtrl Is DropDownList Then
                    afterArraylist.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, DropDownList).SelectedItem.Text))
                ElseIf TypeOf frmCtrl Is ListBox Then
                    afterArraylist.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, ListBox).SelectedItem.Text))
                ElseIf TypeOf frmCtrl Is CheckBoxList Then
                    Dim cdl As CheckBoxList = DirectCast(frmCtrl, CheckBoxList)
                    For Each item As ListItem In cdl.Items
                        If item.Selected = True Then
                            afterArraylist.Add(New UtilityObj(item.Text, item.Value))
                        Else
                            afterArraylist.Add(New UtilityObj(item.Text, ""))
                        End If
                    Next
                End If
                If frmCtrl.HasControls Then
                    afterLoopingControls1(frmCtrl, afterArraylist)
                End If
            Next
            Return afterArraylist
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetFilepath(ByVal strcode() As String, ByVal pathDepth As Integer) As String
        Dim menu_text As String = String.Empty
        Dim menu_home As String = String.Empty
        Dim menu_name As String = String.Empty
        Dim i As Integer
        For i = 0 To strcode.Length - 1
            Using readerMenuText As SqlDataReader = AccessRoleUser.GetMenuText(strcode(i))
                While readerMenuText.Read()
                    If pathDepth = 0 Then
                        menu_text = ""
                    Else
                        menu_text = menu_text + " | " + Convert.ToString(readerMenuText("mnu_text"))
                    End If
                End While
                readerMenuText.Close()
            End Using
        Next
        Return menu_text
    End Function

    Public Shared Sub beforeLoopingControls(ByVal oControl As Control)
        'Dim beforeArrayList = HttpContext.Current.Session("beforeArrayList")
        Dim beforeArrayList As New List(Of UtilityObj)
        beforeArrayList = beforeLoopingControls1(oControl, beforeArrayList)
        HttpContext.Current.Session("beforeArrayList") = beforeArrayList
    End Sub

    Public Shared Function GetBusinessUnits(ByVal usrName As String, Optional ByVal showActiveOnly As Boolean = False) As DataSet
        Dim str_sql As String = "select BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & usrName & "') "
        If showActiveOnly Then
            str_sql = "select BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & usrName & "') where BSU_bSHOW=1 "
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function beforeLoopingControls1(ByVal oControl As Control, ByVal beforeList As Object) As Object
        Try
            Dim frmCtrl As Control
            Dim beforeArrayList = beforeList
            For Each frmCtrl In oControl.Controls
                If TypeOf frmCtrl Is TextBox Then
                    beforeArrayList.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, TextBox).Text))
                ElseIf TypeOf frmCtrl Is CheckBox Then
                    beforeArrayList.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, CheckBox).Checked))
                ElseIf TypeOf frmCtrl Is RadioButton Then
                    beforeArrayList.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, RadioButton).Checked))
                ElseIf TypeOf frmCtrl Is DropDownList Then
                    beforeArrayList.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, DropDownList).SelectedItem.Text))
                ElseIf TypeOf frmCtrl Is ListBox Then
                    beforeArrayList.Add(New UtilityObj(frmCtrl.ID, DirectCast(frmCtrl, ListBox).SelectedItem.Text))
                ElseIf TypeOf frmCtrl Is CheckBoxList Then
                    Dim cdl As CheckBoxList = DirectCast(frmCtrl, CheckBoxList)
                    For Each item As ListItem In cdl.Items
                        If item.Selected = True Then
                            beforeArrayList.Add(New UtilityObj(item.Text, item.Value))
                        Else
                            beforeArrayList.Add(New UtilityObj(item.Text, ""))
                        End If
                    Next
                End If
                If frmCtrl.HasControls Then
                    beforeLoopingControls1(frmCtrl, beforeArrayList)
                End If
            Next
            Return beforeArrayList

        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    'if updating insert the change record  through strRemark i.e call AudiChange
    'If Inserting then StrRemark---New Record Inserted Successfully
    'If Deleteing then StrRemark---Delete Record was executed Successfully
    'aud_form should be Page.Title
    Public Shared Function AudiChange() As String
        Dim strRemark As String = ""
        Dim strChangePass As String = String.Empty
        Dim afterArraylist = HttpContext.Current.Session("afterArraylist")
        Dim beforeArrayList = HttpContext.Current.Session("beforeArrayList")
        Try
            For Each afteritem As UtilityObj In afterArraylist
                For Each beforeitem As UtilityObj In beforeArrayList
                    If afteritem.Value <> beforeitem.Value And afteritem.Name = beforeitem.Name Then
                        If afteritem.Name.ToLower = "txtpassword" Or afteritem.Name.ToLower = "txtconfpassword" Then
                            strChangePass = " Password Changed "
                        Else
                            strRemark += String.Format("{{{0}}}-- : {1}({2}); ", afteritem.Name, afteritem.Value, beforeitem.Value)
                        End If
                    End If
                Next
            Next
            clearallResource()
            Return strRemark + strChangePass
        Catch ex As Exception
            Return "Record Updated"
        End Try
    End Function

    Public Shared Sub clearallResource()
        Dim afterArraylist = IIf(Not HttpContext.Current.Session("afterArraylist") Is Nothing, HttpContext.Current.Session("afterArraylist"), Nothing)
        Dim beforeArrayList = IIf(Not HttpContext.Current.Session("beforeArrayList") Is Nothing, HttpContext.Current.Session("beforeArrayList"), Nothing)
        If Not beforeArrayList Is Nothing Then
            beforeArrayList.Clear()
            HttpContext.Current.Session("beforeArrayList") = beforeArrayList
        End If
        If Not afterArraylist Is Nothing Then
            afterArraylist.Clear()
            HttpContext.Current.Session("afterArraylist") = afterArraylist
        End If
    End Sub

    Public Shared Function operOnAudiTable(ByVal aud_form As String, _
    ByVal aud_docno As String, ByVal aud_action As String, _
    Optional ByVal userInfo As String = "", _
    Optional ByVal ocontrol As Control = Nothing, _
    Optional ByVal aud_remarks As String = "") As Integer
        Dim aud_remark As String = ""
        Dim Encr_decrData As New Encryption64
        Try
            If Trim(HttpContext.Current.Request.QueryString("MainMnu_code") + "") <> "" Then
                aud_form = Encr_decrData.Decrypt(HttpContext.Current.Request.QueryString("MainMnu_code").Replace(" ", "+"))
            End If
            aud_action = aud_action.ToLower()
            Select Case aud_action
                Case "insert"
                    aud_remark = "New Record inserted"
                    clearallResource()
                Case "delete"
                    aud_remark = "Record Deleted"
                    clearallResource()
                Case "edit"
                    If Not ocontrol Is Nothing Then
                        afterLoopingControls(ocontrol)
                    Else
                        Throw New ArgumentException("-1")
                    End If
                    aud_remark = AudiChange()
                Case Else
                    aud_remark = aud_action
                    clearallResource()
            End Select
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@Aud_user", HttpContext.Current.Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@Aud_bsu_id", HttpContext.Current.Session("sBsuid"))
                pParms(2) = New SqlClient.SqlParameter("@Aud_module", HttpContext.Current.Session("sModule"))
                pParms(3) = New SqlClient.SqlParameter("@Aud_form", aud_form)

                pParms(4) = New SqlClient.SqlParameter("@Aud_docNo", aud_docno)
                pParms(5) = New SqlClient.SqlParameter("@Aud_action", aud_action)
                pParms(6) = New SqlClient.SqlParameter("@Aud_remarks", aud_remark & " " & aud_remarks)
                pParms(7) = New SqlClient.SqlParameter("@AUD_HOST", HttpContext.Current.Request.UserHostAddress.ToString()) 'host.HostName)
                pParms(8) = New SqlClient.SqlParameter("@AUD_WINUSER", userInfo)

                pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "dbo.SaveAudit", pParms)
                Dim ReturnFlag As Integer = pParms(9).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Public Shared Sub InsertAuditdetails(ByVal transaction As SqlTransaction, ByVal action As String, ByVal tableName As String, ByVal pkColumnName As String, ByVal refColumName As String, ByVal filterQuery As String, Optional ByVal remarks As String = "")
        Dim user As String
        Dim bsuid As String
        user = HttpContext.Current.Session("sUsr_name")
        bsuid = HttpContext.Current.Session("sBsuid")
        Dim str_query As String
        Dim ids As New ArrayList

        '  Dim guid As String
        ' Dim pkvalue As String
        Dim xmlStr As String = ""
        Dim reader As SqlDataReader
        str_query = "select guid,convert(varchar(100)," + pkColumnName + "),convert(varchar(100)," + refColumName + ") from " + tableName + " where " + filterQuery
        reader = SqlHelper.ExecuteReader(transaction, CommandType.Text, str_query)
        While reader.Read
            Dim id(1, 2) As String
            id(0, 0) = reader.GetGuid(0).ToString
            id(0, 1) = reader.GetString(1)
            id(0, 2) = reader.GetString(2)
            ids.Add(id)
        End While
        reader.Close()

        Dim i As Integer
        For i = 0 To ids.Count - 1
            Dim id(1, 2) As String
            id = ids.Item(i)
            str_query = "select * from " + tableName + " where guid='" + id(0, 0) + "'  and " + pkColumnName + "=" + id(0, 1) + " for xml auto"
            reader = SqlHelper.ExecuteReader(transaction, CommandType.Text, str_query)
            While reader.Read
                xmlStr += reader.GetString(0)
            End While
            reader.Close()
            str_query = "exec saveAUDITDETAILS '" + id(0, 2) + "','" + id(0, 0) + "','" + id(0, 1) + "','" + bsuid + "','" + tableName + "','" + action + "','" + xmlStr.Replace("'", "''") + "','" + user + "','" + remarks + "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query).ToString()
        Next
    End Sub

    Public Shared Sub InsertAuditdetails(ByVal str_conn As String, ByVal action As String, ByVal tableName As String, ByVal pkColumnName As String, ByVal refColumName As String, ByVal filterQuery As String, Optional ByVal remarks As String = "")
        Dim user As String
        Dim bsuid As String
        user = HttpContext.Current.Session("sUsr_name")
        bsuid = HttpContext.Current.Session("sBsuid")
        Dim str_query As String
        Dim ids As New ArrayList

        '  Dim guid As String
        ' Dim pkvalue As String
        Dim xmlStr As String = ""
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim reader As SqlDataReader
        str_query = "select guid,convert(varchar(100)," + pkColumnName + "),convert(varchar(100)," + refColumName + ") from " + tableName + " where " + filterQuery
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            Dim id(1, 2) As String
            id(0, 0) = reader.GetGuid(0).ToString
            id(0, 1) = reader.GetString(1)
            id(0, 2) = reader.GetString(2)
            ids.Add(id)
        End While
        reader.Close()

        Dim i As Integer
        For i = 0 To ids.Count - 1
            Dim id(1, 2) As String
            id = ids.Item(i)
            str_query = "select * from " + tableName + " where guid='" + id(0, 0) + "'  and " + pkColumnName + "=" + id(0, 1) + " for xml auto"
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                xmlStr += reader.GetString(0)
            End While
            reader.Close()
            str_query = "exec OASIS..saveAUDITDETAILS '" + id(0, 2) + "','" + id(0, 0) + "','" + id(0, 1) + "','" + bsuid + "','" + tableName + "','" + action + "','" + xmlStr.Replace("'", "''") + "','" + user + "','" + remarks + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query).ToString()
        Next
    End Sub


    Public Shared Sub Errorlog(ByVal p_Description As String, Optional ByVal p_source As String = "Not Set")
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ERR_PAGE", SqlDbType.VarChar, 200)
                pParms(0).Value = HttpContext.Current.Request.Url.ToString()
                pParms(1) = New SqlClient.SqlParameter("@ERR_SOURCE", SqlDbType.VarChar, 50)
                pParms(1).Value = p_source
                pParms(2) = New SqlClient.SqlParameter("@ERR_DESCRIPTION", SqlDbType.VarChar, 300)
                pParms(2).Value = p_Description
                SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "SAVEERROR_RUNTIME", pParms)
            End Using
        Catch ex As Exception
            Try
                Dim f As New IO.DirectoryInfo(HttpContext.Current.Server.MapPath("Logs"))
                If f.Exists = False Then
                    f.Create()
                End If
                Dim objStreamWriter As StreamWriter
                objStreamWriter = File.AppendText(HttpContext.Current.Server.MapPath("Logs\Logs.txt"))
                objStreamWriter.WriteLine("Page : ( " & HttpContext.Current.Request.Url.ToString() & " ) Source : ( " & p_source & " ) Description : ( " & p_Description & " ) Time : (" & DateTime.Now.ToString() & ")")
                objStreamWriter.Close()
            Catch ex1 As Exception
            End Try
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Public Shared Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("Entry not found for error # " & p_errorno)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ("Entry not found for error # " & p_errorno)
        End Try
    End Function

    Public Shared Function GenerateXML(ByVal p_IDs As String, ByVal p_Type As XMLType) As String
        Dim xmlDoc As New XmlDocument
        Dim xmlDetails As XmlElement
        Dim XMLE_ID As XmlElement
        Dim XMLE_Detail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case p_Type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.STUDENT
                elements(0) = "STU_DETAILS"
                elements(1) = "STU_DETAIL"
                elements(2) = "STU_ID"
            Case XMLType.CheckList
                elements(0) = "CHKLIST_DETAILS"
                elements(1) = "CHKLIST_DETAIL"
                elements(2) = "CHKLIST_ID"
            Case XMLType.Grade
                elements(0) = "Root"
                elements(1) = "GRADE_DET"
                elements(2) = "GRD_ID"
            Case XMLType.Bus
                elements(0) = "Root"
                elements(1) = "BUS_DET"
                elements(2) = "BUS_ID"
            Case XMLType.ConcessionType
                elements(0) = "CONC_DETAILS"
                elements(1) = "CONC_DETAIL"
                elements(2) = "CONC_ID"
        End Select
        Try
            xmlDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(xmlDetails)
            Dim IDs As String() = p_IDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLE_Detail = xmlDoc.CreateElement(elements(1))
                XMLE_ID = xmlDoc.CreateElement(elements(2))
                XMLE_ID.InnerText = IDs(i)
                XMLE_Detail.AppendChild(XMLE_ID)
                xmlDoc.DocumentElement.InsertBefore(XMLE_Detail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Shared Function GetBSUnitWithSeperator(ByVal p_IDs As String, ByVal separator As String) As String
        Try
            Dim str_appendedString As String = String.Empty
            Dim IDs As String() = p_IDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                str_appendedString += IDs(i) + separator
                i += 1
            Next
            Return str_appendedString
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Shared Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        If pVal = "" Then Return ""
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If
        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function

    Public Shared Function GetDataFromSQL(ByVal p_str_sql As String, ByVal p_str_conn As String) As String
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(p_str_conn, CommandType.Text, p_str_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0) & ""
        Else
            Return "--"
        End If
    End Function

    Public Shared Function GetDiplayDate(Optional ByVal p_date As String = "") As String
        If IsDate(HttpContext.Current.Session("F_TODT").ToString) Then
            Return HttpContext.Current.Session("F_TODT")
        Else
            Return Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Function

    Public Shared Function PasswordVerify(ByVal strPassword As String, ByVal iMinNumbers As Integer, _
    ByVal iMinSplcharacters As Integer, ByVal iMinCharacter As Integer, _
    Optional ByVal iMinCharactersCaps As Integer = 0, Optional ByVal iMinCharactersSmalls As Integer = 0) As Boolean
        Dim strLen As String
        Dim strCounter, numbers, charactersCaps, charactersSmalls, splcharacters As Integer
        numbers = 0
        charactersCaps = 0
        charactersSmalls = 0
        'Dim strClean As String
        strLen = strPassword.Length
        'strClean = ""
        For strCounter = 1 To strLen
            Select Case Asc(Mid(strPassword, strCounter, 1))
                Case 65 To 90 'A-Z 
                    charactersCaps = charactersCaps + 1
                Case 97 To 122 'a-z 
                    charactersSmalls = charactersSmalls + 1
                Case 48 To 57 ' 0123456789
                    numbers = numbers + 1
                Case Else  'All other characters are stripped out 
                    splcharacters = splcharacters + 1
            End Select
        Next
        'Return LCase(strClean)
        If numbers < iMinNumbers Then
            Return False
        ElseIf splcharacters < iMinSplcharacters Then
            Return False
        End If
        If iMinCharactersCaps = 0 And iMinCharactersSmalls = 0 Then
            If charactersCaps + charactersSmalls < iMinCharacter Then
                Return False
            End If
        Else
            If iMinCharactersCaps < charactersCaps Or iMinCharactersSmalls < charactersSmalls Then
                Return False
            End If
        End If
        Return True
    End Function

    Public Shared Sub SendEmail(ByVal p_To_Email As String, ByVal p_Subject As String, _
         ByVal p_Host As String, ByVal p_Port As String, ByVal p_From_Username As String, _
         ByVal p_Body As String, ByVal p_From_Email As String)
        Dim msgMail As New System.Net.Mail.MailMessage()
        Dim smtp As New System.Net.Mail.SmtpClient(p_Host, p_Port) '10.0.5.3
        ' Dim smtp As New System.Net.Mail.SmtpClient(host) '10.0.5.3
        msgMail.To.Add(p_To_Email)
        msgMail.Subject = p_Subject
        Dim mailfrom As New System.Net.Mail.MailAddress(p_From_Email, p_From_Username)
        msgMail.From = mailfrom
        msgMail.IsBodyHtml = True
        msgMail.Body = p_Body
        '  Dim att As New Net.Mail.Attachment(attchment, "PaySlip_" & subj & ".pdf")
        ' msgMail.Attachments.Add(att)
        smtp.Send(msgMail)
    End Sub

    Private Function Pad(ByVal numberOfSpaces As Int32) As String
        Dim Spaces As String = ""
        For items As Int32 = 1 To numberOfSpaces
            Spaces &= "&nbsp;"
        Next
        Return HttpContext.Current.Server.HtmlDecode(Spaces)
    End Function

    Public Shared Function CleanupStringForJavascript(ByVal Str_TexttoCleanup As String) As String
        Str_TexttoCleanup = HttpContext.Current.Server.UrlEncode(Str_TexttoCleanup)
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("'", "\'")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d%0a", " ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0a", "  ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d", " ")
        Return HttpContext.Current.Server.UrlDecode(Str_TexttoCleanup)
    End Function

    Public Shared Function getBsuSegmentSplit(ByVal p_BSUIDS As String, _
             ByRef p_BSUSEGS As String, ByRef p_BSUINAMES As String, ByRef p_BSUCURRENCY As String) As String
        'EXEC	@return_value = [dbo].[getBsuSegmentSplit]
        '@BSUIDS = N'111001|121009|121012|121013|121014|125010|125011|131001|141001|151001|115002|123004|123006|125005|126008|114003|124001|125002|125003|125004|125015|125016|125017|111001|121009|121012|121013|121014|125010|125011|131001|141001|151001|115002|123004|123006|125005|126008|114003|124001|125002|125003|125004|125015|125016|125017|',
        '@BSUSEGS = @BSUSEGS OUTPUT,
        '@BSUINAMES = @BSUINAMES OUTPUT

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUIDS", SqlDbType.VarChar, 400)
        pParms(0).Value = p_BSUIDS
        pParms(1) = New SqlClient.SqlParameter("@BSUSEGS", SqlDbType.VarChar, 400)
        pParms(1).Direction = ParameterDirection.Output
        pParms(2) = New SqlClient.SqlParameter("@BSUINAMES", SqlDbType.VarChar, 400)
        pParms(2).Direction = ParameterDirection.Output
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@BSUCURRENCY", SqlDbType.VarChar, 400)
        pParms(4).Direction = ParameterDirection.Output
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.StoredProcedure, "getBsuSegmentSplit", pParms)
        p_BSUSEGS = pParms(1).Value
        p_BSUINAMES = pParms(2).Value
        p_BSUCURRENCY = pParms(4).Value
        getBsuSegmentSplit = pParms(3).Value
    End Function

    Public Shared Function GetData(ByVal vData As Object) As Object
        If vData Is DBNull.Value Then
            Return ""
        Else
            Return vData
        End If
    End Function

    Public Shared Function IsFutureDate(ByVal FromDate As String, Optional ByVal ToDate As String = "") As Boolean
        If ToDate = "" Then
            If Not IsDate(FromDate) Then
                Return True
            End If
            If Not HttpContext.Current.Session("sBusper") Then
                If CDate(FromDate) > Now.Date Then
                    Return True
                End If
            End If
        Else
            If Not IsDate(FromDate) Or Not IsDate(ToDate) Then
                Return True
            End If
            If CDate(FromDate) > CDate(ToDate) Then
                Return True
            End If
            If Not HttpContext.Current.Session("sBusper") Then
                If CDate(FromDate) > Now.Date Or CDate(ToDate) > Now.Date Then
                    Return True
                End If
            End If
        End If
        Return False
    End Function

    Public Shared Function GetBsuTree(ByVal bSuper As Boolean, ByVal USER As String, _
        ByVal ParentId As String) As DataTable
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@bSuper", SqlDbType.Bit)
        pParms(0).Value = bSuper
        pParms(1) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(1).Value = USER
        pParms(2) = New SqlClient.SqlParameter("@ParentId", SqlDbType.VarChar)
        pParms(2).Value = ParentId
        'pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        'pParms(3).Direction = ParameterDirection.ReturnValue
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "GetBsuTree", pParms)
        Return dsData.Tables(0)
    End Function

    Public Shared Function GetBsuTree_WithRights(ByVal bSuper As Boolean, ByVal USER As String, _
        ByVal ParentId As String, ByVal MenuID As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@bSuper", SqlDbType.Bit)
        pParms(0).Value = bSuper
        pParms(1) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(1).Value = USER
        pParms(2) = New SqlClient.SqlParameter("@ParentId", SqlDbType.VarChar)
        pParms(2).Value = ParentId
        pParms(3) = New SqlClient.SqlParameter("@MenuID", SqlDbType.VarChar)
        pParms(3).Value = MenuID
        'pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        'pParms(3).Direction = ParameterDirection.ReturnValue
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "GetBsuTree_WithRights", pParms)
        Return dsData.Tables(0)
    End Function

    Public Shared Function VerifyandUpdatePassword(ByVal Old_Password As String, ByVal New_Password As String, _
        ByVal username As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_OldPASSWORD", Old_Password)
            pParms(1) = New SqlClient.SqlParameter("@USR_NewPASSWORD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            pParms(3) = New SqlClient.SqlParameter("@USR_NAME", username)
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "VerifyandUpdatePassword", pParms)
            Return pParms(2).Value
        End Using
    End Function

    Public Shared Function CheckUSerAccessRights(ByVal BSU_ID As String, ByVal MNU_ID As String, _
           ByVal MODULEid As String, ByVal USER As String, ByVal bSUPER As String) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Using objConn
                Dim pParms(5) As SqlClient.SqlParameter
                ''''@BSU_ID varchar(20),@MNU_ID varchar(20),@MODULE varchar(10),@USER  varchar(50)
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                pParms(1) = New SqlClient.SqlParameter("@MNU_ID", MNU_ID)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                pParms(3) = New SqlClient.SqlParameter("@MODULE", MODULEid)
                pParms(4) = New SqlClient.SqlParameter("@USER", USER)
                pParms(5) = New SqlClient.SqlParameter("@bSUPER", bSUPER)
                SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "CheckUSerAccessRights", pParms)
                Return pParms(2).Value
            End Using
        Catch ex As Exception
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Function
    Public Shared Function GetGeneratedQuery(ByVal dbCommand As SqlCommand) As String
        Dim query = dbCommand.CommandText
        For Each parameter In dbCommand.Parameters
            query = query.Replace(parameter.ParameterName, parameter.Value.ToString)
        Next
        Return query
    End Function

    Public Shared Function ParameterValueForSQL(ByVal sp As SqlParameter) As String
        Dim retval As String = ""
        Try
            Select Case sp.SqlDbType
                Case SqlDbType.Char, SqlDbType.NChar, SqlDbType.NText, SqlDbType.NVarChar, SqlDbType.Text, SqlDbType.Time, SqlDbType.VarChar, SqlDbType.Xml, SqlDbType.Date, SqlDbType.DateTime, SqlDbType.DateTime2, SqlDbType.DateTimeOffset
                    retval = "'" & sp.Value.ToString().Replace("'", "''") & "'"

                Case SqlDbType.Bit
                    retval = If(CBool(sp.Value) = False, "0", "1")

                Case Else
                    retval = sp.Value.ToString().Replace("'", "''")
            End Select
        Catch ex As Exception
            retval = ""
        End Try

        Return retval
    End Function

    Public Shared Function CommandAsSql(ByVal sc As SqlCommand) As String
        Dim sql As New StringBuilder()
        Dim FirstParam As Boolean = True

        sql.AppendLine("use " & sc.Connection.Database & ";")
        Select Case sc.CommandType
            Case CommandType.StoredProcedure
                sql.AppendLine("declare @return_value int;")
                For Each sp As SqlParameter In sc.Parameters
                    If (sp.Direction = ParameterDirection.InputOutput) OrElse (sp.Direction = ParameterDirection.Output) Then
                        sql.Append("declare " & sp.ParameterName & ControlChars.Tab & sp.SqlDbType.ToString() & ControlChars.Tab & "= ")

                        sql.AppendLine((If(sp.Direction = ParameterDirection.Output, "null", ParameterValueForSQL(sp))) & ";")

                    End If
                Next sp

                sql.AppendLine("exec [" & sc.CommandText & "]")

                For Each sp As SqlParameter In sc.Parameters
                    If sp.Direction <> ParameterDirection.ReturnValue Then
                        sql.Append(If(FirstParam, ControlChars.Tab, ControlChars.Tab & ", "))

                        If FirstParam Then
                            FirstParam = False
                        End If

                        If sp.Direction = ParameterDirection.Input Then
                            sql.AppendLine(sp.ParameterName & " = " & ParameterValueForSQL(sp))
                        Else

                            sql.AppendLine(sp.ParameterName & " = " & sp.ParameterName & " output")
                        End If
                    End If
                Next sp
                sql.AppendLine(";")

                sql.AppendLine("select 'Return Value' = convert(varchar, @return_value);")

                For Each sp As SqlParameter In sc.Parameters
                    If (sp.Direction = ParameterDirection.InputOutput) OrElse (sp.Direction = ParameterDirection.Output) Then
                        sql.AppendLine("select '" & sp.ParameterName & "' = convert(varchar, " & sp.ParameterName & ");")
                    End If
                Next sp
            Case CommandType.Text
                sql.AppendLine(sc.CommandText)
        End Select

        Return sql.ToString()
    End Function

 Public Shared Function GET_APPLICATION_NAME() As String
        GET_APPLICATION_NAME = String.Empty
        Try
            Dim ctrl As New System.Web.UI.Control
            Dim APP_NAME As String = ""
            APP_NAME = HttpContext.Current.Request.ApplicationPath.Replace("/", "").Trim
            If APP_NAME <> "" Then
                APP_NAME &= "$|$" & APP_NAME & "$|$"
            Else
                APP_NAME = "$|$"
            End If
            GET_APPLICATION_NAME = APP_NAME
        Catch ex As Exception

        End Try
    End Function
End Class


Public Class MisParameters
    Dim vBSU_IDs As String
    Dim vCurrency As String
    Dim vFromDate As String
    Dim vToDate As String
    Dim vExpressedIn As String
    Dim vBSUShortnames As String
    Dim vBSUSegments As String
    Dim vRSSPARENT As String
    Dim vExcludeSpecialAccounts As Boolean

    Public Property ExcludeSpecialAccounts() As Boolean
        Get
            Return vExcludeSpecialAccounts
        End Get
        Set(ByVal value As Boolean)
            vExcludeSpecialAccounts = value
        End Set
    End Property

    Public Property BSU_IDs() As String
        Get
            Return vBSU_IDs
        End Get
        Set(ByVal value As String)
            vBSU_IDs = value
        End Set
    End Property

    Public Property Currency() As String
        Get
            Return vCurrency
        End Get
        Set(ByVal value As String)
            vCurrency = value
        End Set
    End Property

    Public Property FromDate() As String
        Get
            Return vFromDate
        End Get
        Set(ByVal value As String)
            vFromDate = value
        End Set
    End Property

    Public Property ToDate() As String
        Get
            Return vToDate
        End Get
        Set(ByVal value As String)
            vToDate = value
        End Set
    End Property

    Public Property ExpressedIn() As String
        Get
            Return vExpressedIn
        End Get
        Set(ByVal value As String)
            vExpressedIn = value
        End Set
    End Property

    Public Property BSUShortnames() As String
        Get
            Return vBSUShortnames
        End Get
        Set(ByVal value As String)
            vBSUShortnames = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BSUSegments() As String
        Get
            Return vBSUSegments
        End Get
        Set(ByVal value As String)
            vBSUSegments = value
        End Set
    End Property

    Public Property RSSPARENT() As String
        Get
            Return vRSSPARENT
        End Get
        Set(ByVal value As String)
            vRSSPARENT = value
        End Set
    End Property

End Class
