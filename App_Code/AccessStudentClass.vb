Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class AccessStudentClass
    Private var_GName As String

    Private Var_GValue As String


    Public Property GName() As String
        Get
            Return var_GName
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property GValue() As String
        Get
            Return Var_GValue
        End Get
        Set(ByVal value As String)

        End Set
    End Property
    Public Sub New(ByVal Gn As String, ByVal Gval As String)

        var_GName = Gn
        Var_GValue = Gval

    End Sub
    Public Shared Function GetYear_DESCR3(ByVal Bsu_id As String, ByVal CLM As String, ByVal ACY_ID As Integer) As SqlDataReader
        'Author(--Lijo)
        'Date   --17/JAN/2008
        'Purpose--Get Grade data from  ACADEMICYEAR_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetYear_DESCR As String = ""


        sqlGetYear_DESCR = "Select ACD_ID,Y_DESCR from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
                     " ACADEMICYEAR_D.ACD_CLM_ID AS CLM_ID, ACADEMICYEAR_D.ACD_ID as ACD_ID FROM ACADEMICYEAR_D INNER JOIN " & _
                     " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID WHERE ACADEMICYEAR_M.ACY_ID='" & ACY_ID - 1 & "')a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"



        '"Select ACY_ID,Y_DESCR from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
        '                      " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN " & _
        '                      " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetYear_DESCR, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

#Region " All SAVE(Update/Insert) function"






    Public Shared Function UPDATE_FEE_SPONSOR(ByVal TYPE As String, ByVal TEMP_FEESPONSOR As String, ByVal TEMP_FCOMPANY As String, _
 ByVal TEMP_MCOMPANY As String, ByVal TEMP_GCOMPANY As String, ByVal TEMP_F_COMP_ID As String, ByVal TEMP_M_COMP_ID As String, ByVal TEMP_G_COMP_ID As String, ByVal TEMP_ID As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TYPE", TYPE)
                pParms(1) = New SqlClient.SqlParameter("@TEMP_FEESPONSOR", TEMP_FEESPONSOR)
                pParms(2) = New SqlClient.SqlParameter("@TEMP_FCOMPANY", TEMP_FCOMPANY)
                pParms(3) = New SqlClient.SqlParameter("@TEMP_MCOMPANY", TEMP_MCOMPANY)
                pParms(4) = New SqlClient.SqlParameter("@TEMP_GCOMPANY", TEMP_GCOMPANY)
                pParms(5) = New SqlClient.SqlParameter("@TEMP_F_COMP_ID", TEMP_F_COMP_ID)
                pParms(6) = New SqlClient.SqlParameter("@TEMP_M_COMP_ID", TEMP_M_COMP_ID)
                pParms(7) = New SqlClient.SqlParameter("@TEMP_G_COMP_ID", TEMP_G_COMP_ID)
                pParms(8) = New SqlClient.SqlParameter("@TEMP_ID", TEMP_ID)
                pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SAVE_FEE_SPONSOR", pParms)
                Dim ReturnFlag As Integer = pParms(9).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function


    Public Shared Function UPDATEStudSTUDENT_D(ByVal STS_STU_ID As String, ByVal STS_FIRSTNAME As String, ByVal STS_MIDNAME As String, ByVal STS_LASTNAME As String, ByVal STS_COMPOBOX As String, ByVal STS_COMCITY As String, ByVal STS_MOBILE As String, ByVal STS_EMAIL As String, ByVal STS_OCC As String, ByVal PARENTID As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(20) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STS_FIRSTNAME", STS_FIRSTNAME)
                pParms(2) = New SqlClient.SqlParameter("@STS_MIDDLENAME", STS_MIDNAME)
                pParms(3) = New SqlClient.SqlParameter("@STS_LASTNAME", STS_LASTNAME)
                pParms(4) = New SqlClient.SqlParameter("@STS_COMPOBOX", STS_COMPOBOX)
                pParms(5) = New SqlClient.SqlParameter("@STS_COMCITY", STS_COMCITY)
                pParms(6) = New SqlClient.SqlParameter("@STS_MOBILE", STS_MOBILE)
                pParms(7) = New SqlClient.SqlParameter("@STS_EMAIL", STS_EMAIL)
                pParms(8) = New SqlClient.SqlParameter("@STS_OCC", STS_OCC)
                pParms(9) = New SqlClient.SqlParameter("@PARENTID", PARENTID)

                pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(10).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UPDATESTUDENT_D", pParms)
                Dim ReturnFlag As Integer = pParms(10).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using

    End Function


    Public Shared Function UPDATE_STUDENT_PROMO_S(ByVal STS_STU_ID As String, ByVal STP_ACD_ID As Integer, ByVal STP_BBSLNO As String, ByVal STP_RESULT As String, ByVal STP_MINLIST As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(20) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STP_BBSLNO", STP_BBSLNO)
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", STS_STU_ID)
                pParms(2) = New SqlClient.SqlParameter("@ACD_ID", STP_ACD_ID)
                If STP_RESULT = "" Then
                    pParms(3) = New SqlClient.SqlParameter("@STP_RESULT", System.DBNull.Value)
                Else

                    pParms(3) = New SqlClient.SqlParameter("@STP_RESULT", STP_RESULT)
                End If
                pParms(4) = New SqlClient.SqlParameter("@STP_MINLIST", STP_MINLIST)

                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UPDATE_STUDENT_PROMO_S", pParms)
                Dim ReturnFlag As Integer = pParms(5).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function

    Public Shared Function UPDATEStudSTUDENT_M(ByVal STU_ID As String, ByVal MINDOJ As String, ByVal STU_TFRTYPE As String, ByVal STU_PASPRTNAME As String, ByVal STU_FIRSTNAMEARABIC As String, ByVal STU_RLG_ID As String, ByVal STU_NATIONALITY As String, ByVal STU_DOB As String, ByVal STU_POB As String, ByVal STU_COB As String, ByVal STU_PREVSCHI As String, ByVal STU_PREVSCHI_COUNTRY As String, ByVal STU_PREVSCHI_CLM As String, ByVal STU_PREVSCHI_LASTATTDATE As String, ByVal Blue_Id As String, ByVal STU_GENDER As String, ByVal STU_GRD_ID_JOIN As String, ByVal STU_PRIMARYCONTACT As String, ByVal stu_bb_sct_id As String, ByVal STU_MINLIST As String, ByVal STU_BB_Remarks As String, ByVal STU_LEAVE_REMARKS As String, ByVal STU_JOIN_RESULT As String, ByVal trans As SqlTransaction) As Integer


        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(66) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
                '' pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)

                pParms(1) = New SqlClient.SqlParameter("@MINDOJ", MINDOJ)
                pParms(2) = New SqlClient.SqlParameter("@STU_GRD_ID_JOIN", STU_GRD_ID_JOIN)
                pParms(3) = New SqlClient.SqlParameter("@TFR_TYPE", STU_TFRTYPE)

                pParms(4) = New SqlClient.SqlParameter("@PASPRTNAME", STU_PASPRTNAME)
                pParms(5) = New SqlClient.SqlParameter("@FIRSTNAMEARABIC", STU_FIRSTNAMEARABIC)
                pParms(6) = New SqlClient.SqlParameter("@RLG_ID", STU_RLG_ID)
                pParms(7) = New SqlClient.SqlParameter("@NATIONALITY", STU_NATIONALITY)
                pParms(8) = New SqlClient.SqlParameter("@DOB", STU_DOB)
                pParms(9) = New SqlClient.SqlParameter("@POB", STU_POB)
                pParms(10) = New SqlClient.SqlParameter("@COB", STU_COB)
                pParms(11) = New SqlClient.SqlParameter("@PREVSCHI", STU_PREVSCHI)
                pParms(12) = New SqlClient.SqlParameter("@PREVSCHI_COUNTRY", STU_PREVSCHI_COUNTRY)
                pParms(13) = New SqlClient.SqlParameter("@PREVSCHI_CLM", STU_PREVSCHI_CLM)
                If STU_PREVSCHI_LASTATTDATE.Trim = "" Then
                    pParms(14) = New SqlClient.SqlParameter("@PREVSCHI_LASTATTDATE", System.DBNull.Value)
                Else
                    pParms(14) = New SqlClient.SqlParameter("@PREVSCHI_LASTATTDATE", STU_PREVSCHI_LASTATTDATE)
                End If
                pParms(15) = New SqlClient.SqlParameter("@STU_BLUEID", Blue_Id)
                pParms(16) = New SqlClient.SqlParameter("@STU_GENDER", STU_GENDER)
                If stu_bb_sct_id.Trim = "" Then
                    pParms(17) = New SqlClient.SqlParameter("@STU_BB_SCT_DES", System.DBNull.Value)
                Else
                    pParms(17) = New SqlClient.SqlParameter("@STU_BB_SCT_DES", stu_bb_sct_id)

                End If


                pParms(18) = New SqlClient.SqlParameter("@STU_PRIMARYCONTACT", STU_PRIMARYCONTACT)
                pParms(19) = New SqlClient.SqlParameter("@STU_MINLIST", STU_MINLIST)

                pParms(20) = New SqlClient.SqlParameter("@STU_BB_REMARKS", STU_BB_Remarks)
                pParms(21) = New SqlClient.SqlParameter("@STU_LEAVE_REMARKS", STU_LEAVE_REMARKS)
                pParms(22) = New SqlClient.SqlParameter("@STU_JOIN_RESULT", STU_JOIN_RESULT)

                pParms(23) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(23).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UPDATESTUDENT_M", pParms)
                Dim ReturnFlag As Integer = pParms(23).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using

    End Function
    Public Shared Function SAVESTUD_M_EDUSHIELD_XML(ByVal ACD_ID As String, ByVal STU_PLY_ID As String, ByVal STU_SUB_FDATE As String, ByVal STR_XML As String, ByRef STU_NO As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --11/Jun/2009
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@STU_PLY_ID", STU_PLY_ID)
                pParms(2) = New SqlClient.SqlParameter("@STU_SUB_FDATE", STU_SUB_FDATE)
                pParms(3) = New SqlClient.SqlParameter("@STR_XML", STR_XML)
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue
                pParms(5) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 900)
                pParms(5).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "EDU.SAVESTUD_M_EDUSHIELD_XML", pParms)
                Dim ReturnFlag As Integer = pParms(4).Value
                If ReturnFlag = 0 Then
                    STU_NO = IIf(TypeOf (pParms(5).Value) Is DBNull, String.Empty, pParms(5).Value)
                End If


                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Public Shared Function SAVESTU_COMM_DETAIL_PUSR(ByVal STU_IDS As String, ByVal COMSTREET As String, ByVal COMAREA As String, _
      ByVal COMBLDG As String, ByVal COMAPARTNO As String, ByVal COMPOBOX As String, ByVal COMCITY As String, ByVal COMSTATE As String, _
      ByVal COMCOUNTRY As String, ByVal OFFPHONE As String, ByVal RESPHONE As String, ByVal FAX As String, ByVal Mobile As String, _
      ByVal PRMADDR1 As String, ByVal PRMADDR2 As String, ByVal PRMPOBOX As String, ByVal PRMCITY As String, ByVal PRMCOUNTRY As String, _
      ByVal PRMPHONE As String, ByVal EMAIL As String, ByVal EMIR As String, ByVal transaction As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(21) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_IDS", STU_IDS)
            pParms(1) = New SqlClient.SqlParameter("@COMSTREET", COMSTREET)
            pParms(2) = New SqlClient.SqlParameter("@COMAREA", COMAREA)
            pParms(3) = New SqlClient.SqlParameter("@COMBLDG", COMBLDG)
            pParms(4) = New SqlClient.SqlParameter("@COMAPARTNO", COMAPARTNO)
            pParms(5) = New SqlClient.SqlParameter("@COMPOBOX", COMPOBOX)
            pParms(6) = New SqlClient.SqlParameter("@COMCITY", COMCITY)
            pParms(7) = New SqlClient.SqlParameter("@COMSTATE", COMSTATE)
            pParms(8) = New SqlClient.SqlParameter("@COMCOUNTRY", COMCOUNTRY)
            pParms(9) = New SqlClient.SqlParameter("@OFFPHONE", OFFPHONE)
            pParms(10) = New SqlClient.SqlParameter("@RESPHONE", RESPHONE)
            pParms(11) = New SqlClient.SqlParameter("@FAX", FAX)
            pParms(12) = New SqlClient.SqlParameter("@MOBILE", Mobile)
            pParms(13) = New SqlClient.SqlParameter("@PRMADDR1", PRMADDR1)
            pParms(14) = New SqlClient.SqlParameter("@PRMADDR2", PRMADDR2)
            pParms(15) = New SqlClient.SqlParameter("@PRMPOBOX", PRMPOBOX)
            pParms(16) = New SqlClient.SqlParameter("@PRMCITY", PRMCITY)
            pParms(17) = New SqlClient.SqlParameter("@PRMCOUNTRY", PRMCOUNTRY)
            pParms(18) = New SqlClient.SqlParameter("@PRMPHONE", PRMPHONE)
            pParms(19) = New SqlClient.SqlParameter("@EMAIL", EMAIL)
            pParms(20) = New SqlClient.SqlParameter("@EMIR", EMIR)
            pParms(21) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(21).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVESTU_COMM_DETAIL_PUSR", pParms)
            Dim ReturnFlag As Integer = pParms(21).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function SAVEUserMember_Details(ByVal USR_NAME As String, ByVal OLD_USR_PASSWORD As String, ByVal NEW_USR_PASSWORD As String, ByVal ENTER_PASSWORD As String, ByVal STS_STU_ID As Integer, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", USR_NAME)
            pParms(1) = New SqlClient.SqlParameter("@OLD_USR_PASSWORD", OLD_USR_PASSWORD)
            pParms(2) = New SqlClient.SqlParameter("@NEW_USR_PASSWORD", NEW_USR_PASSWORD)
            pParms(3) = New SqlClient.SqlParameter("@ENTER_PASSWORD", ENTER_PASSWORD)
            pParms(4) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SAVEUserMember_Details", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function SaveBSU_APPLIC_LETTER(ByVal BAL_ID As String, ByVal BAL_BSU_ID As String, ByVal BAL_SUB As String, _
                       ByVal BAL_MAT As String, ByVal BAL_REM As String, ByVal BAL_SIGN As String, ByVal BAL_ACK As String, ByVal BAL_bDefault As Boolean, _
         ByVal BAL_APL_ID As String, ByVal BAL_MAT_BLP_ID As String, ByVal BAL_REM_BLP_ID As String, ByVal BAL_ACK_BLP_ID As String, ByVal bEdit As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --18/NOV/2008
        'Purpose--To save BSU_APPL_LETTER data based 
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(13) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BAL_ID", BAL_ID)
                pParms(1) = New SqlClient.SqlParameter("@BAL_BSU_ID", BAL_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@BAL_SUB", BAL_SUB)
                pParms(3) = New SqlClient.SqlParameter("@BAL_MAT", BAL_MAT)
                pParms(4) = New SqlClient.SqlParameter("@BAL_REM", BAL_REM)
                pParms(5) = New SqlClient.SqlParameter("@BAL_SIGN", BAL_SIGN)
                pParms(6) = New SqlClient.SqlParameter("@BAL_ACK", BAL_ACK)
                pParms(7) = New SqlClient.SqlParameter("@BAL_bDefault", BAL_bDefault)
                pParms(8) = New SqlClient.SqlParameter("@BAL_APL_ID", BAL_APL_ID)

                pParms(9) = New SqlClient.SqlParameter("@BAL_MAT_BLP_ID", BAL_MAT_BLP_ID)
                pParms(10) = New SqlClient.SqlParameter("@BAL_REM_BLP_ID", BAL_REM_BLP_ID)
                pParms(11) = New SqlClient.SqlParameter("@BAL_ACK_BLP_ID", BAL_ACK_BLP_ID)

                pParms(12) = New SqlClient.SqlParameter("@bEdit", bEdit)
                pParms(13) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(13).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SaveBSU_APPL_LETTER", pParms)

                Dim ReturnFlag As Integer = pParms(13).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function

    Public Shared Function SaveLOT_ALLOTMENT_UPDATE(ByVal EQS_LOT_STATUS As String, ByVal EQS_EQM_ENQIDs As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --03/Nov/2008
        'Purpose--To save LOT_ALLOTMENT data
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_LOT_STATUS", EQS_LOT_STATUS)
                pParms(1) = New SqlClient.SqlParameter("@EQS_EQM_ENQIDs", EQS_EQM_ENQIDs)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.SaveLOT_ALLOTMENT_UPDATE", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function

    Public Shared Function SaveLOT_ALLOTMENT(ByVal EQS_ACD_ID As String, ByVal EQS_GRD_ID As String, _
                      ByVal EQS_SHF_ID As String, ByVal EQS_LOT_STATUS As String, ByVal EQS_LOT_DATE As String, _
                      ByVal EQS_EQM_ENQIDs As String, ByVal EQS_LOT_ALLOT_FOR As String, ByRef NEW_LOTNO As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --03/Nov/2008
        'Purpose--To save LOT_ALLOTMENT data
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(8) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_ACD_ID", EQS_ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
                pParms(2) = New SqlClient.SqlParameter("@EQS_SHF_ID", EQS_SHF_ID)
                pParms(3) = New SqlClient.SqlParameter("@EQS_LOT_STATUS", EQS_LOT_STATUS)
                pParms(4) = New SqlClient.SqlParameter("@EQS_LOT_DATE", EQS_LOT_DATE)
                pParms(5) = New SqlClient.SqlParameter("@EQS_EQM_ENQIDs", EQS_EQM_ENQIDs)
                pParms(6) = New SqlClient.SqlParameter("@EQS_LOT_ALLOT_FOR", EQS_LOT_ALLOT_FOR)
                pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(7).Direction = ParameterDirection.ReturnValue
                pParms(8) = New SqlClient.SqlParameter("@NEW_LOTNO", SqlDbType.Int)
                pParms(8).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.SaveLOT_ALLOTMENT", pParms)
                Dim ReturnFlag As Integer = pParms(7).Value
                If ReturnFlag = 0 Then
                    NEW_LOTNO = IIf(TypeOf (pParms(8).Value) Is DBNull, String.Empty, pParms(8).Value)
                End If
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function



    Public Shared Function SaveSaveENQUIRY_ACK_SETTING(ByVal EAS_ID As String, ByVal EAS_ACD_ID As String, ByVal EAS_CLM_ID As String, _
                   ByVal EAS_GRD_ID As String, ByVal EAS_BSU_ID As String, ByVal EAS_TEXT_MAIN As String, ByVal EAS_MAIN_bSHOW As Boolean, _
     ByVal EAS_SUB_bSHOW As Boolean, ByRef New_EAS_ID As String, ByVal EAS_bBULLET_POINTS As Boolean, ByVal bEdit As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To save ENQUIRY_ACK_SETTING data based 
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(11) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EAS_ID", EAS_ID)
                pParms(1) = New SqlClient.SqlParameter("@EAS_ACD_ID", EAS_ACD_ID)
                pParms(2) = New SqlClient.SqlParameter("@EAS_CLM_ID", EAS_CLM_ID)
                pParms(3) = New SqlClient.SqlParameter("@EAS_GRD_ID", EAS_GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@EAS_BSU_ID", EAS_BSU_ID)
                pParms(5) = New SqlClient.SqlParameter("@EAS_TEXT_MAIN", EAS_TEXT_MAIN)
                pParms(6) = New SqlClient.SqlParameter("@EAS_MAIN_bSHOW", EAS_MAIN_bSHOW)
                pParms(7) = New SqlClient.SqlParameter("@EAS_SUB_bSHOW", EAS_SUB_bSHOW)
                pParms(8) = New SqlClient.SqlParameter("@EAS_bBULLET_POINTS", EAS_bBULLET_POINTS)

                pParms(9) = New SqlClient.SqlParameter("@bEdit", bEdit)
                pParms(10) = New SqlClient.SqlParameter("@New_EAS_ID", SqlDbType.Int)
                pParms(10).Direction = ParameterDirection.Output
                pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(11).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.SaveENQUIRY_ACK_SETTING", pParms)
                New_EAS_ID = pParms(10).Value.ToString()
                Dim ReturnFlag As Integer = pParms(11).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function



    Public Shared Function SaveENQUIRY_ACK_DOC(ByVal EAD_EAS_ID As String, ByVal EAD_TEXT As String, _
                       ByVal EAD_DISPLAY_ORDER As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To save ENQUIRY_ACK_DOC data based 
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EAD_EAS_ID", EAD_EAS_ID)
                pParms(1) = New SqlClient.SqlParameter("@EAD_TEXT", EAD_TEXT)
                pParms(2) = New SqlClient.SqlParameter("@EAD_DISPLAY_ORDER", EAD_DISPLAY_ORDER)
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.SaveENQUIRY_ACK_DOC", pParms)
                Dim ReturnFlag As Integer = pParms(3).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function

    Public Shared Function SaveENQUIRY_SETTINGS(ByVal EQS_ID As String, ByVal EQS_ACD_ID As String, ByVal EQS_CLM_ID As String, ByVal EQS_GRD_ID As String, ByVal EQS_BSU_ID As String, _
             ByVal EQS_FROMDT As String, ByVal EQS_TODT As String, ByVal EQS_EQO_ID As String, ByVal EQS_BSU_IDS As String, ByVal bEdit As Boolean, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --23/SEP/2008
        'Purpose--To save ENQUIRY_SETTINGS 
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_ID", EQS_ID)
                pParms(1) = New SqlClient.SqlParameter("@EQS_ACD_ID", EQS_ACD_ID)
                pParms(2) = New SqlClient.SqlParameter("@EQS_CLM_ID", EQS_CLM_ID)
                pParms(3) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@EQS_BSU_ID", EQS_BSU_ID)
                pParms(5) = New SqlClient.SqlParameter("@EQS_FROMDT", EQS_FROMDT)
                pParms(6) = New SqlClient.SqlParameter("@EQS_TODT", EQS_TODT)
                pParms(7) = New SqlClient.SqlParameter("@EQS_EQO_ID", EQS_EQO_ID)
                pParms(8) = New SqlClient.SqlParameter("@EQS_BSU_IDS", EQS_BSU_IDS)
                pParms(9) = New SqlClient.SqlParameter("@bEdit", bEdit)
                pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(10).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveENQUIRY_SETTINGS", pParms)
                Dim ReturnFlag As Integer = pParms(10).Value
                Return ReturnFlag

            End Using

        Catch ex As Exception
            Return 1
        End Try


    End Function

    Public Shared Function Transport_SaveStudent(ByVal STU_ID As Integer, ByVal STU_BSU_ID As String, _
             ByVal STU_ACD_ID As Integer, _
             ByVal STU_GRD_ID As String, ByVal STU_SHF_ID As Integer, ByVal STU_SCT_ID As Integer, _
             ByVal STU_FIRSTNAME As String, ByVal STU_MIDNAME As String, ByVal STU_LASTNAME As String, _
             ByVal STU_DOJ As String, ByVal STU_FEE_ID As String, ByVal STU_DOB As String, _
             ByVal STU_GENDER As String, ByVal STU_RLG_ID As String, ByVal STU_NATIONALITY As String, _
            ByVal F_FIRSTNAME As String, ByVal F_MIDNAME As String, ByVal F_LASTNAME As String, _
            ByVal F_POBox As String, ByVal F_EMirate As String, ByVal F_Mobile As String, ByVal F_Email As String, _
            ByVal STU_GEMS_BSU_ID As String, ByVal STU_GEMS_STU_ID As Long, ByVal STU_SVC_EAV_ID As Integer, _
            ByVal STU_SVC_FROMDT As String, ByVal STU_SVC_TODT As String, _
             ByVal TranType As String, ByVal STU_NONGEMS_BSU_ID As String, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --05/Mar/2008
        'Purpose--To save STUDENT_M data based on the datamode (studEnqForm.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISTransportConnection()
            Try
                Dim pParms(62) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@STU_ACD_ID", STU_ACD_ID)
                pParms(3) = New SqlClient.SqlParameter("@STU_GRD_ID", STU_GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@STU_SHF_ID", STU_SHF_ID)
                pParms(5) = New SqlClient.SqlParameter("@STU_SCT_ID", STU_SCT_ID)
                pParms(6) = New SqlClient.SqlParameter("@STU_DOJ", STU_DOJ)
                pParms(7) = New SqlClient.SqlParameter("@STU_FEE_ID", STU_FEE_ID)
                pParms(8) = New SqlClient.SqlParameter("@STU_FIRSTNAME", STU_FIRSTNAME)
                pParms(9) = New SqlClient.SqlParameter("@STU_MIDNAME", STU_MIDNAME)
                pParms(10) = New SqlClient.SqlParameter("@STU_LASTNAME", STU_LASTNAME)
                pParms(11) = New SqlClient.SqlParameter("@STU_GENDER", STU_GENDER)
                pParms(12) = New SqlClient.SqlParameter("@STU_RLG_ID", STU_RLG_ID)
                pParms(13) = New SqlClient.SqlParameter("@STU_DOB", STU_DOB)
                pParms(14) = New SqlClient.SqlParameter("@STU_Nationality", STU_NATIONALITY)
                pParms(15) = New SqlClient.SqlParameter("@F_FIRSTNAME", F_FIRSTNAME)
                pParms(16) = New SqlClient.SqlParameter("@F_MIDNAME", F_MIDNAME)
                pParms(17) = New SqlClient.SqlParameter("@F_LASTNAME", F_LASTNAME)
                pParms(18) = New SqlClient.SqlParameter("@F_POBOX", F_POBox)
                pParms(19) = New SqlClient.SqlParameter("@F_EMIRATE", F_EMirate)
                pParms(20) = New SqlClient.SqlParameter("@F_MOBILE", F_Mobile)
                pParms(21) = New SqlClient.SqlParameter("@F_EMAIL", F_Email)

                pParms(22) = New SqlClient.SqlParameter("@STU_GEMS_BSU_ID", STU_GEMS_BSU_ID)
                pParms(23) = New SqlClient.SqlParameter("@STU_GEMS_STU_ID", STU_GEMS_STU_ID)
                pParms(24) = New SqlClient.SqlParameter("@STU_SVC_EAV_ID", STU_SVC_EAV_ID)
                pParms(25) = New SqlClient.SqlParameter("@STU_SVC_FROMDT", STU_SVC_FROMDT)
                pParms(26) = New SqlClient.SqlParameter("@STU_SVC_TODT", STU_SVC_TODT)
                pParms(27) = New SqlClient.SqlParameter("@TranType", TranType)
                pParms(28) = New SqlClient.SqlParameter("@STU_NONGEMS_BSU_ID", STU_NONGEMS_BSU_ID)
                pParms(29) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(29).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "TptSaveSTUDENT_M", pParms)
                Dim ReturnFlag As Integer = pParms(29).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using

    End Function

    Public Shared Function SaveCOM_CER_QUERY(ByVal CCQ_NAME As String, ByVal CCQ_BSU_ID As String, ByVal CCQ_QUERY As String, _
               ByVal CCQ_CREATE_DT As String, ByVal CCQ_TYPE As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --08/SEP/2008
        'Purpose--To save COM_CER_QUERY data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CCQ_NAME", CCQ_NAME)
            pParms(1) = New SqlClient.SqlParameter("@CCQ_BSU_ID", CCQ_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@CCQ_QUERY", CCQ_QUERY)
            If CCQ_CREATE_DT = "" Then
                pParms(3) = New SqlClient.SqlParameter("@CCQ_CREATE_DT", DBNull.Value)
            Else
                pParms(3) = New SqlClient.SqlParameter("@CCQ_CREATE_DT", CCQ_CREATE_DT)
            End If

            pParms(4) = New SqlClient.SqlParameter("@CCQ_TYPE", CCQ_TYPE)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveCOM_CER_QUERY", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag

        End Using
    End Function

    Public Shared Function SAVEComm_Detail(ByVal Stu_ID As String, ByVal Pob As String, ByVal Emirate As String, _
             ByVal Res_No As String, ByVal Mob_No As String, ByVal Email As String, ByVal SPARENT As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --24/Aug/2008
        'Purpose--To save Parent Communication Details data based 
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(7) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@Stu_ID", Stu_ID)
                pParms(1) = New SqlClient.SqlParameter("@Pob", Pob)
                pParms(2) = New SqlClient.SqlParameter("@Emirate", Emirate)
                pParms(3) = New SqlClient.SqlParameter("@Res_No", Res_No)
                pParms(4) = New SqlClient.SqlParameter("@Mob_No", Mob_No)
                pParms(5) = New SqlClient.SqlParameter("@Email", Email)
                pParms(6) = New SqlClient.SqlParameter("@SPARENT", SPARENT)
                pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(7).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVEComm_Detail", pParms)
                Dim ReturnFlag As Integer = pParms(7).Value
                Return ReturnFlag

            End Using

        Catch ex As Exception
            Return 1
        End Try


    End Function



    Public Shared Function SavePolicy_View(ByVal PLY_ID As String, ByVal PLY_DESCR As String, ByVal PLY_FROMDT As String, ByVal PLY_TODT As String, _
                ByVal PLY_CURRENT As Boolean, ByVal PLY_bSUBSCR_DATE As Boolean, ByVal bEdit As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --14/Jun/2009
        'Purpose--To save Attendance_M data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PLY_ID", PLY_ID)
            pParms(1) = New SqlClient.SqlParameter("@PLY_DESCR", PLY_DESCR)
            pParms(2) = New SqlClient.SqlParameter("@PLY_FROMDT", PLY_FROMDT)
            If PLY_TODT = "" Then
                pParms(3) = New SqlClient.SqlParameter("@PLY_TODT", DBNull.Value)
            Else
                pParms(3) = New SqlClient.SqlParameter("@PLY_TODT", PLY_TODT)
            End If
            pParms(4) = New SqlClient.SqlParameter("@PLY_CURRENT", PLY_CURRENT)
            pParms(5) = New SqlClient.SqlParameter("@PLY_bSUBSCR_DATE", PLY_bSUBSCR_DATE)
            pParms(6) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "edu.SavePolicy_M", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            Return ReturnFlag

        End Using
    End Function



    Public Shared Function SaveEnquiry_Validation_D(ByVal EVD_BSU_ID As String, ByVal EVD_EQV_ID As String, ByVal EVD_TYPE As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EVD_BSU_ID", EVD_BSU_ID)
            pParms(1) = New SqlClient.SqlParameter("@EVD_EQV_ID", EVD_EQV_ID)
            pParms(2) = New SqlClient.SqlParameter("@EVD_TYPE", EVD_TYPE)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ENQ.SaveEnquiry_Validation_D", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
        End Using
    End Function




    Public Shared Function SaveStudTCM_M(ByVal TCM_BSU_ID As String, ByVal TCM_DOCDATE As String, ByVal TCM_DOCNO As String, ByVal TCM_ACD_ID As String, _
           ByVal TCM_STU_ID As String, ByVal TCM_REFNO As String, ByVal TCM_ISSUEDATE As String, ByVal TCM_LEAVEDATE As String, _
           ByVal TCM_LASTATTDATE As String, ByVal TCM_REASON As String, ByVal TCM_DAYST As String, ByVal TCM_DAYSP As String, ByVal TCM_TCSO As String, _
           ByVal TCM_TCTYPE As String, ByVal TCM_bTFRGEMS As Boolean, ByVal TCM_GEMS_UNIT As String, ByVal TCM_TFRSCHOOL As String, ByVal TCM_TFRCOUNTRY As String, _
          ByVal TCM_TFRGRADE As String, ByVal TCM_bCANCELLED As Boolean, ByVal TCM_RESULT As String, ByVal TCM_REMARKS As String, ByVal TCM_SUBJECTS As String, ByVal TCM_TFRZONE As String, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --05/Mar/2008
        'Purpose--To save TCM_M data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(25) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TCM_DOCDATE", TCM_DOCDATE)
                pParms(1) = New SqlClient.SqlParameter("@TCM_DOCNO", TCM_DOCNO)
                pParms(2) = New SqlClient.SqlParameter("@TCM_ACD_ID", TCM_ACD_ID)
                pParms(3) = New SqlClient.SqlParameter("@TCM_STU_ID", TCM_STU_ID)
                pParms(4) = New SqlClient.SqlParameter("@TCM_REFNO", TCM_REFNO)
                pParms(5) = New SqlClient.SqlParameter("@TCM_APPLYDATE", TCM_DOCDATE)
                pParms(6) = New SqlClient.SqlParameter("@TCM_ISSUEDATE", TCM_ISSUEDATE)
                pParms(7) = New SqlClient.SqlParameter("@TCM_LEAVEDATE", TCM_LEAVEDATE)
                pParms(8) = New SqlClient.SqlParameter("@TCM_LASTATTDATE", TCM_LASTATTDATE)
                pParms(9) = New SqlClient.SqlParameter("@TCM_REASON", TCM_REASON)
                pParms(10) = New SqlClient.SqlParameter("@TCM_DAYST", TCM_DAYST)
                pParms(11) = New SqlClient.SqlParameter("@TCM_DAYSP", TCM_DAYSP)
                pParms(12) = New SqlClient.SqlParameter("@TCM_TCSO", TCM_TCSO)
                pParms(13) = New SqlClient.SqlParameter("@TCM_TCTYPE", TCM_TCTYPE)
                pParms(14) = New SqlClient.SqlParameter("@TCM_bTFRGEMS", TCM_bTFRGEMS)
                pParms(15) = New SqlClient.SqlParameter("@TCM_GEMS_UNIT", TCM_GEMS_UNIT)
                pParms(16) = New SqlClient.SqlParameter("@TCM_TFRSCHOOL", TCM_TFRSCHOOL)
                pParms(17) = New SqlClient.SqlParameter("@TCM_TFRCOUNTRY", TCM_TFRCOUNTRY)
                pParms(18) = New SqlClient.SqlParameter("@TCM_TFRGRADE", TCM_TFRGRADE)
                pParms(19) = New SqlClient.SqlParameter("@TCM_bCANCELLED", TCM_bCANCELLED)
                pParms(20) = New SqlClient.SqlParameter("@TCM_BSU_ID", TCM_BSU_ID)
                pParms(21) = New SqlClient.SqlParameter("@TCM_RESULT", TCM_RESULT)
                pParms(22) = New SqlClient.SqlParameter("@TCM_REMARKS", TCM_REMARKS)
                pParms(23) = New SqlClient.SqlParameter("@TCM_SUBJECTS", TCM_SUBJECTS)
                pParms(24) = New SqlClient.SqlParameter("@TCM_TFRZONE", TCM_TFRZONE)
                pParms(25) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(25).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudTCM_M", pParms)
                Dim ReturnFlag As Integer = pParms(25).Value
                Return ReturnFlag
            Catch ex As Exception
                Return -1
            End Try
        End Using
    End Function

    Public Shared Function studSAVESTUDSERVICES(ByVal SSV_BSU_ID As String, ByVal SSV_ACD_ID As String, ByVal SSV_STU_ID As String, ByVal SSV_SVC_ID As String, ByVal SSV_FROMDATE As String, ByVal trans As SqlTransaction)
        'Author(--Lijo)
        'Date   --05/Mar/2008
        'Purpose--To save STUDENT_SERVICES_D data based on the datamode 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@SSV_BSU_ID", SSV_BSU_ID)
                pParms(1) = New SqlClient.SqlParameter("@SSV_ACD_ID", SSV_ACD_ID)
                pParms(2) = New SqlClient.SqlParameter("@SSV_STU_ID", SSV_STU_ID)
                pParms(3) = New SqlClient.SqlParameter("@SSV_SVC_ID", SSV_SVC_ID)
                pParms(4) = New SqlClient.SqlParameter("@SSV_FROMDATE", SSV_FROMDATE)
                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "studSAVESTUDSERVICES", pParms)
                Dim ReturnFlag As Integer = pParms(5).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function

    'Public Shared Function SaveStudSTUDENT_M(ByVal STU_ID As String, ByVal STU_BSU_ID As String, ByVal STU_TFRTYPE As String, _
    'ByVal STU_BLUEID As String, ByVal STU_FIRSTNAME As String, ByVal STU_MIDNAME As String, ByVal STU_LASTNAME As String, _
    'ByVal STU_FIRSTNAMEARABIC As String, ByVal STU_MIDNAMEARABIC As String, ByVal STU_LASTNAMEARABIC As String, ByVal STU_DOB As String, _
    'ByVal STU_GENDER As String, ByVal STU_RLG_ID As String, ByVal STU_NATIONALITY As String, ByVal STU_POB As String, ByVal STU_COB As String, _
    'ByVal STU_HOUSE As String, ByVal STU_MINLIST As String, ByVal STU_MINLISTTYPE As String, ByVal STU_FEESPONSOR As String, _
    'ByVal STU_EMGCONTACT As String, ByVal STU_HEALTH As String, _
    'ByVal STU_PHYSICAL As String, ByVal STU_bRCVSPMEDICATION As Boolean, ByVal STU_SPMEDICATION As String, ByVal STU_PASPRTNAME As String, _
    'ByVal STU_PASPRTNO As String, ByVal STU_PASPRTISSPLACE As String, ByVal STU_PASPRTISSDATE As String, ByVal STU_PASPRTEXPDATE As String, _
    'ByVal STU_VISANO As String, ByVal STU_VISAISSPLACE As String, ByVal STU_VISAISSDATE As String, ByVal STU_VISAEXPDATE As String, _
    'ByVal STU_VISAISSAUTH As String, ByVal STU_BLOODGROUP As String, ByVal STU_HCNO As String, ByVal STU_PRIMARYCONTACT As String, _
    'ByVal STU_PREFCONTACT As String, ByVal STU_PHOTOPATH As String, ByVal STU_bRCVSMS As Boolean, ByVal STU_bRCVMAIL As Boolean, ByVal STU_SIBLING_ID As String, ByVal trans As SqlTransaction) As Integer
    '    'Author(--Lijo)
    '    'Date   --05/Mar/2008
    '    'Purpose--To save STUDENT_M data based on the datamode (studEnqForm.aspx)
    '    Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
    '        Try
    '            Dim pParms(43) As SqlClient.SqlParameter
    '            pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
    '            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)
    '            pParms(2) = New SqlClient.SqlParameter("@STU_TFRTYPE", STU_TFRTYPE)
    '            pParms(3) = New SqlClient.SqlParameter("@STU_BLUEID", STU_BLUEID)
    '            pParms(4) = New SqlClient.SqlParameter("@STU_FIRSTNAME", STU_FIRSTNAME)
    '            pParms(5) = New SqlClient.SqlParameter("@STU_MIDNAME", STU_MIDNAME)
    '            pParms(6) = New SqlClient.SqlParameter("@STU_LASTNAME", STU_LASTNAME)
    '            pParms(7) = New SqlClient.SqlParameter("@STU_FIRSTNAMEARABIC", STU_FIRSTNAMEARABIC)
    '            pParms(8) = New SqlClient.SqlParameter("@STU_MIDNAMEARABIC", STU_MIDNAMEARABIC)
    '            pParms(9) = New SqlClient.SqlParameter("@STU_LASTNAMEARABIC", STU_LASTNAMEARABIC)
    '            pParms(10) = New SqlClient.SqlParameter("@STU_DOB", STU_DOB)
    '            pParms(11) = New SqlClient.SqlParameter("@STU_GENDER", STU_GENDER)
    '            pParms(12) = New SqlClient.SqlParameter("@STU_RLG_ID", STU_RLG_ID)
    '            pParms(13) = New SqlClient.SqlParameter("@STU_NATIONALITY", STU_NATIONALITY)
    '            pParms(14) = New SqlClient.SqlParameter("@STU_POB", STU_POB)
    '            pParms(15) = New SqlClient.SqlParameter("@STU_COB", STU_COB)
    '            pParms(16) = New SqlClient.SqlParameter("@STU_HOUSE_ID", STU_HOUSE)
    '            pParms(17) = New SqlClient.SqlParameter("@STU_MINLIST", STU_MINLIST)
    '            pParms(18) = New SqlClient.SqlParameter("@STU_MINLISTTYPE", STU_MINLISTTYPE)
    '            pParms(19) = New SqlClient.SqlParameter("@STU_FEESPONSOR", STU_FEESPONSOR)
    '            pParms(20) = New SqlClient.SqlParameter("@STU_EMGCONTACT", STU_EMGCONTACT)
    '            pParms(21) = New SqlClient.SqlParameter("@STU_HEALTH", STU_HEALTH)
    '            pParms(22) = New SqlClient.SqlParameter("@STU_PHYSICAL", STU_PHYSICAL)
    '            pParms(23) = New SqlClient.SqlParameter("@STU_bRCVSPMEDICATION", STU_bRCVSPMEDICATION)
    '            pParms(24) = New SqlClient.SqlParameter("@STU_SPMEDICATION", STU_SPMEDICATION)
    '            pParms(25) = New SqlClient.SqlParameter("@STU_PASPRTNAME", STU_PASPRTNAME)
    '            pParms(26) = New SqlClient.SqlParameter("@STU_PASPRTNO", STU_PASPRTNO)
    '            pParms(27) = New SqlClient.SqlParameter("@STU_PASPRTISSPLACE", STU_PASPRTISSPLACE)
    '            pParms(28) = New SqlClient.SqlParameter("@STU_PASPRTISSDATE", STU_PASPRTISSDATE)
    '            pParms(29) = New SqlClient.SqlParameter("@STU_PASPRTEXPDATE", STU_PASPRTEXPDATE)
    '            pParms(30) = New SqlClient.SqlParameter("@STU_VISANO", STU_VISANO)
    '            pParms(31) = New SqlClient.SqlParameter("@STU_VISAISSPLACE", STU_VISAISSPLACE)
    '            pParms(32) = New SqlClient.SqlParameter("@STU_VISAISSDATE", STU_VISAISSDATE)
    '            pParms(33) = New SqlClient.SqlParameter("@STU_VISAEXPDATE", STU_VISAEXPDATE)
    '            pParms(34) = New SqlClient.SqlParameter("@STU_VISAISSAUTH", STU_VISAISSAUTH)
    '            pParms(35) = New SqlClient.SqlParameter("@STU_BLOODGROUP", STU_BLOODGROUP)
    '            pParms(36) = New SqlClient.SqlParameter("@STU_HCNO", STU_HCNO)
    '            pParms(37) = New SqlClient.SqlParameter("@STU_PRIMARYCONTACT", STU_PRIMARYCONTACT)
    '            pParms(38) = New SqlClient.SqlParameter("@STU_PREFCONTACT", STU_PREFCONTACT)
    '            pParms(39) = New SqlClient.SqlParameter("@STU_PHOTOPATH", STU_PHOTOPATH)
    '            pParms(40) = New SqlClient.SqlParameter("@STU_bRCVSMS", STU_bRCVSMS)
    '            pParms(41) = New SqlClient.SqlParameter("@STU_bRCVMAIL", STU_bRCVMAIL)
    '            pParms(42) = New SqlClient.SqlParameter("@STU_SIBLING_ID", STU_SIBLING_ID)
    '            pParms(43) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '            pParms(43).Direction = ParameterDirection.ReturnValue
    '            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDENT_M", pParms)
    '            Dim ReturnFlag As Integer = pParms(43).Value
    '            Return ReturnFlag
    '        Catch ex As Exception
    '            Return 1000
    '        End Try
    '    End Using

    'End Function


    'Public Shared Function SaveStudSTUDENT_D(ByVal STS_STU_ID As String, ByVal STS_FFIRSTNAME As String, ByVal STS_FMIDNAME As String, _
    'ByVal STS_FLASTNAME As String, ByVal STS_FNATIONALITY As String, ByVal STS_FNATIONALITY2 As String, ByVal STS_FCOMADDR1 As String, _
    'ByVal STS_FCOMADDR2 As String, ByVal STS_FCOMPOBOX As String, ByVal STS_FCOMCITY As String, ByVal STS_FCOMSTATE As String, _
    'ByVal STS_FCOMCOUNTRY As String, ByVal STS_FOFFPHONE As String, ByVal STS_FRESPHONE As String, ByVal STS_FFAX As String, _
    'ByVal STS_FMOBILE As String, ByVal STS_FPRMADDR1 As String, ByVal STS_FPRMADDR2 As String, ByVal STS_FPRMPOBOX As String, _
    'ByVal STS_FPRMCITY As String, ByVal STS_FPRMCOUNTRY As String, ByVal STS_FPRMPHONE As String, ByVal STS_FOCC As String, _
    'ByVal STS_FCOMPANY As String, ByVal STS_bFGEMSEMP As Boolean, ByVal STS_F_BSU_ID As String, ByVal STS_MFIRSTNAME As String, _
    'ByVal STS_MMIDNAME As String, ByVal STS_MLASTNAME As String, ByVal STS_MNATIONALITY As String, ByVal STS_MNATIONALITY2 As String, _
    'ByVal STS_MCOMADDR1 As String, ByVal STS_MCOMADDR2 As String, ByVal STS_MCOMPOBOX As String, ByVal STS_MCOMCITY As String, _
    'ByVal STS_MCOMSTATE As String, ByVal STS_MCOMCOUNTRY As String, ByVal STS_MOFFPHONE As String, ByVal STS_MRESPHONE As String, _
    'ByVal STS_MFAX As String, ByVal STS_MMOBILE As String, ByVal STS_MPRMADDR1 As String, ByVal STS_MPRMADDR2 As String, _
    'ByVal STS_MPRMPOBOX As String, ByVal STS_MPRMCITY As String, ByVal STS_MPRMCOUNTRY As String, ByVal STS_MPRMPHONE As String, _
    'ByVal STS_MOCC As String, ByVal STS_MCOMPANY As String, ByVal STS_bMGEMSEMP As Boolean, ByVal STS_M_BSU_ID As String, _
    'ByVal STS_GFIRSTNAME As String, ByVal STS_GMIDNAME As String, ByVal STS_GLASTNAME As String, ByVal STS_GNATIONALITY As String, _
    'ByVal STS_GNATIONALITY2 As String, ByVal STS_GCOMADDR1 As String, ByVal STS_GCOMADDR2 As String, ByVal STS_GCOMPOBOX As String, _
    'ByVal STS_GCOMCITY As String, ByVal STS_GCOMSTATE As String, ByVal STS_GCOMCOUNTRY As String, ByVal STS_GOFFPHONE As String, _
    'ByVal STS_GRESPHONE As String, ByVal STS_GFAX As String, ByVal STS_GMOBILE As String, ByVal STS_GPRMADDR1 As String, _
    'ByVal STS_GPRMADDR2 As String, ByVal STS_GPRMPOBOX As String, ByVal STS_GPRMCITY As String, ByVal STS_GPRMCOUNTRY As String, _
    'ByVal STS_GPRMPHONE As String, ByVal STS_GOCC As String, ByVal STS_GCOMPANY As String, ByVal STS_PREVSCHI As String, _
    'ByVal STS_PREVSCHI_CITY As String, ByVal STS_PREVSCHI_COUNTRY As String, ByVal STS_PREVSCHI_CLM As String, ByVal STS_PREVSCHI_GRADE As String, _
    'ByVal STS_PREVSCHI_LASTATTDATE As String, ByVal STS_PREVSCHII As String, ByVal STS_PREVSCHII_CITY As String, ByVal STS_PREVSCHII_COUNTRY As String, _
    'ByVal STS_PREVSCHII_CLM As String, ByVal STS_PREVSCHII_GRADE As String, ByVal STS_PREVSCHII_LASTATTDATE As String, ByVal STS_PREVSCHIII As String, _
    'ByVal STS_PREVSCHIII_CITY As String, ByVal STS_PREVSCHIII_COUNTRY As String, ByVal STS_PREVSCHIII_CLM As String, ByVal STS_PREVSCHIII_GRADE As String, _
    'ByVal STS_PREVSCHIII_LASTATTDATE As String, _
    'ByVal STS_FCOMPANY_ADDR As String, ByVal STS_FEMAIL As String, ByVal STS_MCOMPANY_ADDR As String, ByVal STS_MEMAIL As String, ByVal STS_GCOMPANY_ADDR As String, ByVal STS_GEMAIL As String, _
    'ByVal STS_F_COMP_ID As String, ByVal STS_M_COMP_ID As String, ByVal STS_G_COMP_ID As String, ByVal trans As SqlTransaction) As Integer
    '    'Author(--Lijo)
    '    'Date   --05/Mar/2008
    '    'Purpose--To save STUDENT_D data based on the datamode (StudRecordEdit.aspx)
    '    Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
    '        Try
    '            Dim pParms(101) As SqlClient.SqlParameter
    '            pParms(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
    '            pParms(1) = New SqlClient.SqlParameter("@STS_FFIRSTNAME", STS_FFIRSTNAME)
    '            pParms(2) = New SqlClient.SqlParameter("@STS_FMIDNAME", STS_FMIDNAME)
    '            pParms(3) = New SqlClient.SqlParameter("@STS_FLASTNAME", STS_FLASTNAME)
    '            pParms(4) = New SqlClient.SqlParameter("@STS_FNATIONALITY", STS_FNATIONALITY)
    '            pParms(5) = New SqlClient.SqlParameter("@STS_FNATIONALITY2", STS_FNATIONALITY2)
    '            pParms(6) = New SqlClient.SqlParameter("@STS_FCOMADDR1", STS_FCOMADDR1)
    '            pParms(7) = New SqlClient.SqlParameter("@STS_FCOMADDR2", STS_FCOMADDR2)
    '            pParms(8) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", STS_FCOMPOBOX)
    '            pParms(9) = New SqlClient.SqlParameter("@STS_FCOMCITY", STS_FCOMCITY)
    '            pParms(10) = New SqlClient.SqlParameter("@STS_FCOMSTATE", STS_FCOMSTATE)
    '            pParms(11) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", STS_FCOMCOUNTRY)
    '            pParms(12) = New SqlClient.SqlParameter("@STS_FOFFPHONE", STS_FOFFPHONE)
    '            pParms(13) = New SqlClient.SqlParameter("@STS_FRESPHONE", STS_FRESPHONE)
    '            pParms(14) = New SqlClient.SqlParameter("@STS_FFAX", STS_FFAX)
    '            pParms(15) = New SqlClient.SqlParameter("@STS_FMOBILE", STS_FMOBILE)
    '            pParms(16) = New SqlClient.SqlParameter("@STS_FPRMADDR1", STS_FPRMADDR1)
    '            pParms(17) = New SqlClient.SqlParameter("@STS_FPRMADDR2", STS_FPRMADDR2)
    '            pParms(18) = New SqlClient.SqlParameter("@STS_FPRMPOBOX", STS_FPRMPOBOX)
    '            pParms(19) = New SqlClient.SqlParameter("@STS_FPRMCITY", STS_FPRMCITY)
    '            pParms(20) = New SqlClient.SqlParameter("@STS_FPRMCOUNTRY", STS_FPRMCOUNTRY)
    '            pParms(21) = New SqlClient.SqlParameter("@STS_FPRMPHONE", STS_FPRMPHONE)
    '            pParms(22) = New SqlClient.SqlParameter("@STS_FOCC", STS_FOCC)
    '            pParms(23) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
    '            pParms(24) = New SqlClient.SqlParameter("@STS_bFGEMSEMP", STS_bFGEMSEMP)
    '            pParms(25) = New SqlClient.SqlParameter("@STS_F_BSU_ID", STS_F_BSU_ID)
    '            pParms(26) = New SqlClient.SqlParameter("@STS_MFIRSTNAME", STS_MFIRSTNAME)
    '            pParms(27) = New SqlClient.SqlParameter("@STS_MMIDNAME", STS_MMIDNAME)
    '            pParms(28) = New SqlClient.SqlParameter("@STS_MLASTNAME", STS_MLASTNAME)
    '            pParms(29) = New SqlClient.SqlParameter("@STS_MNATIONALITY", STS_MNATIONALITY)
    '            pParms(30) = New SqlClient.SqlParameter("@STS_MNATIONALITY2", STS_MNATIONALITY2)
    '            pParms(31) = New SqlClient.SqlParameter("@STS_MCOMADDR1", STS_MCOMADDR1)
    '            pParms(32) = New SqlClient.SqlParameter("@STS_MCOMADDR2", STS_MCOMADDR2)
    '            pParms(33) = New SqlClient.SqlParameter("@STS_MCOMPOBOX", STS_MCOMPOBOX)
    '            pParms(34) = New SqlClient.SqlParameter("@STS_MCOMCITY", STS_MCOMCITY)
    '            pParms(35) = New SqlClient.SqlParameter("@STS_MCOMSTATE", STS_MCOMSTATE)
    '            pParms(36) = New SqlClient.SqlParameter("@STS_MCOMCOUNTRY", STS_MCOMCOUNTRY)
    '            pParms(37) = New SqlClient.SqlParameter("@STS_MOFFPHONE", STS_MOFFPHONE)
    '            pParms(38) = New SqlClient.SqlParameter("@STS_MRESPHONE", STS_MRESPHONE)
    '            pParms(39) = New SqlClient.SqlParameter("@STS_MFAX", STS_MFAX)
    '            pParms(40) = New SqlClient.SqlParameter("@STS_MMOBILE", STS_MMOBILE)
    '            pParms(41) = New SqlClient.SqlParameter("@STS_MPRMADDR1", STS_MPRMADDR1)
    '            pParms(42) = New SqlClient.SqlParameter("@STS_MPRMADDR2", STS_MPRMADDR2)
    '            pParms(43) = New SqlClient.SqlParameter("@STS_MPRMPOBOX", STS_MPRMPOBOX)
    '            pParms(44) = New SqlClient.SqlParameter("@STS_MPRMCITY", STS_MPRMCITY)
    '            pParms(45) = New SqlClient.SqlParameter("@STS_MPRMCOUNTRY", STS_MPRMCOUNTRY)
    '            pParms(46) = New SqlClient.SqlParameter("@STS_MPRMPHONE", STS_MPRMPHONE)
    '            pParms(47) = New SqlClient.SqlParameter("@STS_MOCC", STS_MOCC)
    '            pParms(48) = New SqlClient.SqlParameter("@STS_MCOMPANY", STS_MCOMPANY)
    '            pParms(49) = New SqlClient.SqlParameter("@STS_bMGEMSEMP", STS_bMGEMSEMP)
    '            pParms(50) = New SqlClient.SqlParameter("@STS_M_BSU_ID", STS_M_BSU_ID)
    '            pParms(51) = New SqlClient.SqlParameter("@STS_GFIRSTNAME", STS_GFIRSTNAME)
    '            pParms(52) = New SqlClient.SqlParameter("@STS_GMIDNAME", STS_GMIDNAME)
    '            pParms(53) = New SqlClient.SqlParameter("@STS_GLASTNAME", STS_GLASTNAME)
    '            pParms(54) = New SqlClient.SqlParameter("@STS_GNATIONALITY", STS_GNATIONALITY)
    '            pParms(55) = New SqlClient.SqlParameter("@STS_GNATIONALITY2", STS_GNATIONALITY2)
    '            pParms(56) = New SqlClient.SqlParameter("@STS_GCOMADDR1", STS_GCOMADDR1)
    '            pParms(57) = New SqlClient.SqlParameter("@STS_GCOMADDR2", STS_GCOMADDR2)
    '            pParms(58) = New SqlClient.SqlParameter("@STS_GCOMPOBOX", STS_GCOMPOBOX)
    '            pParms(59) = New SqlClient.SqlParameter("@STS_GCOMCITY", STS_GCOMCITY)
    '            pParms(60) = New SqlClient.SqlParameter("@STS_GCOMSTATE", STS_GCOMSTATE)
    '            pParms(61) = New SqlClient.SqlParameter("@STS_GCOMCOUNTRY", STS_GCOMCOUNTRY)
    '            pParms(62) = New SqlClient.SqlParameter("@STS_GOFFPHONE", STS_GOFFPHONE)
    '            pParms(63) = New SqlClient.SqlParameter("@STS_GRESPHONE", STS_GRESPHONE)
    '            pParms(64) = New SqlClient.SqlParameter("@STS_GFAX", STS_GFAX)
    '            pParms(65) = New SqlClient.SqlParameter("@STS_GMOBILE", STS_GMOBILE)
    '            pParms(66) = New SqlClient.SqlParameter("@STS_GPRMADDR1", STS_GPRMADDR1)
    '            pParms(67) = New SqlClient.SqlParameter("@STS_GPRMADDR2", STS_GPRMADDR2)

    '            pParms(68) = New SqlClient.SqlParameter("@STS_GPRMPOBOX", STS_GPRMPOBOX)
    '            pParms(69) = New SqlClient.SqlParameter("@STS_GPRMCITY", STS_GPRMCITY)
    '            pParms(70) = New SqlClient.SqlParameter("@STS_GPRMCOUNTRY", STS_GPRMCOUNTRY)
    '            pParms(71) = New SqlClient.SqlParameter("@STS_GPRMPHONE", STS_GPRMPHONE)
    '            pParms(72) = New SqlClient.SqlParameter("@STS_GOCC", STS_GOCC)
    '            pParms(73) = New SqlClient.SqlParameter("@STS_GCOMPANY", STS_GCOMPANY)
    '            pParms(74) = New SqlClient.SqlParameter("@STS_PREVSCHI", STS_PREVSCHI)
    '            pParms(75) = New SqlClient.SqlParameter("@STS_PREVSCHI_CITY", STS_PREVSCHI_CITY)
    '            pParms(76) = New SqlClient.SqlParameter("@STS_PREVSCHI_COUNTRY", STS_PREVSCHI_COUNTRY)
    '            pParms(77) = New SqlClient.SqlParameter("@STS_PREVSCHI_CLM", STS_PREVSCHI_CLM)
    '            pParms(78) = New SqlClient.SqlParameter("@STS_PREVSCHI_GRADE", STS_PREVSCHI_GRADE)

    '            If STS_PREVSCHI_LASTATTDATE.Trim = "" Then
    '                pParms(79) = New SqlClient.SqlParameter("@STS_PREVSCHI_LASTATTDATE", System.DBNull.Value)
    '            Else
    '                pParms(79) = New SqlClient.SqlParameter("@STS_PREVSCHI_LASTATTDATE", STS_PREVSCHI_LASTATTDATE)
    '            End If

    '            pParms(80) = New SqlClient.SqlParameter("@STS_PREVSCHII", STS_PREVSCHII)
    '            pParms(81) = New SqlClient.SqlParameter("@STS_PREVSCHII_CITY", STS_PREVSCHII_CITY)
    '            pParms(82) = New SqlClient.SqlParameter("@STS_PREVSCHII_COUNTRY", STS_PREVSCHII_COUNTRY)
    '            pParms(83) = New SqlClient.SqlParameter("@STS_PREVSCHII_CLM", STS_PREVSCHII_CLM)
    '            pParms(84) = New SqlClient.SqlParameter("@STS_PREVSCHII_GRADE", STS_PREVSCHII_GRADE)

    '            If STS_PREVSCHII_LASTATTDATE.Trim = "" Then
    '                pParms(85) = New SqlClient.SqlParameter("@STS_PREVSCHII_LASTATTDATE", System.DBNull.Value)
    '            Else
    '                pParms(85) = New SqlClient.SqlParameter("@STS_PREVSCHII_LASTATTDATE", STS_PREVSCHII_LASTATTDATE)
    '            End If
    '            pParms(86) = New SqlClient.SqlParameter("@STS_PREVSCHIII", STS_PREVSCHIII)
    '            pParms(87) = New SqlClient.SqlParameter("@STS_PREVSCHIII_CITY", STS_PREVSCHIII_CITY)
    '            pParms(88) = New SqlClient.SqlParameter("@STS_PREVSCHIII_COUNTRY", STS_PREVSCHIII_COUNTRY)
    '            pParms(89) = New SqlClient.SqlParameter("@STS_PREVSCHIII_CLM", STS_PREVSCHIII_CLM)
    '            pParms(90) = New SqlClient.SqlParameter("@STS_PREVSCHIII_GRADE", STS_PREVSCHIII_GRADE)
    '            If STS_PREVSCHIII_LASTATTDATE.Trim = "" Then
    '                pParms(91) = New SqlClient.SqlParameter("@STS_PREVSCHIII_LASTATTDATE", System.DBNull.Value)
    '            Else
    '                pParms(91) = New SqlClient.SqlParameter("@STS_PREVSCHIII_LASTATTDATE", STS_PREVSCHIII_LASTATTDATE)
    '            End If
    '            pParms(92) = New SqlClient.SqlParameter("@STS_FCOMPANY_ADDR", STS_FCOMPANY_ADDR)
    '            pParms(93) = New SqlClient.SqlParameter("@STS_FEMAIL", STS_FEMAIL)
    '            pParms(94) = New SqlClient.SqlParameter("@STS_MCOMPANY_ADDR", STS_MCOMPANY_ADDR)
    '            pParms(95) = New SqlClient.SqlParameter("@STS_MEMAIL", STS_MEMAIL)
    '            pParms(96) = New SqlClient.SqlParameter("@STS_GCOMPANY_ADDR", STS_GCOMPANY_ADDR)
    '            pParms(97) = New SqlClient.SqlParameter("@STS_GEMAIL", STS_GEMAIL)
    '            pParms(98) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
    '            pParms(99) = New SqlClient.SqlParameter("@STS_M_COMP_ID", STS_M_COMP_ID)
    '            pParms(100) = New SqlClient.SqlParameter("@STS_G_COMP_ID", STS_G_COMP_ID)

    '            pParms(101) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '            pParms(101).Direction = ParameterDirection.ReturnValue
    '            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDENT_D", pParms)
    '            Dim ReturnFlag As Integer = pParms(101).Value
    '            Return ReturnFlag
    '        Catch ex As Exception
    '            Return 1000
    '        End Try
    '    End Using

    'End Function

    'CODE ADDED BY DHANYA 28 APR  08

    Public Shared Function SaveStudSTUDENT_M(ByVal STU_ID As String, ByVal STU_BSU_ID As String, ByVal STU_TFRTYPE As String, _
   ByVal STU_BLUEID As String, ByVal STU_FIRSTNAME As String, ByVal STU_MIDNAME As String, ByVal STU_LASTNAME As String, _
   ByVal STU_FIRSTNAMEARABIC As String, ByVal STU_MIDNAMEARABIC As String, ByVal STU_LASTNAMEARABIC As String, ByVal STU_DOB As String, _
   ByVal STU_GENDER As String, ByVal STU_RLG_ID As String, ByVal STU_NATIONALITY As String, ByVal STU_POB As String, ByVal STU_COB As String, _
   ByVal STU_HOUSE As String, ByVal STU_MINLIST As String, ByVal STU_MINLISTTYPE As String, ByVal STU_FEESPONSOR As String, _
   ByVal STU_EMGCONTACT As String, ByVal STU_HEALTH As String, _
   ByVal STU_PHYSICAL As String, ByVal STU_bRCVSPMEDICATION As Boolean, ByVal STU_SPMEDICATION As String, ByVal STU_PASPRTNAME As String, _
   ByVal STU_PASPRTNO As String, ByVal STU_PASPRTISSPLACE As String, ByVal STU_PASPRTISSDATE As String, ByVal STU_PASPRTEXPDATE As String, _
   ByVal STU_VISANO As String, ByVal STU_VISAISSPLACE As String, ByVal STU_VISAISSDATE As String, ByVal STU_VISAEXPDATE As String, _
   ByVal STU_VISAISSAUTH As String, ByVal STU_BLOODGROUP As String, ByVal STU_HCNO As String, ByVal STU_PRIMARYCONTACT As String, _
   ByVal STU_PREFCONTACT As String, ByVal STU_PHOTOPATH As String, ByVal STU_bRCVSMS As Boolean, ByVal STU_bRCVMAIL As Boolean, ByVal STU_SIBLING_ID As String, _
   ByVal STU_PREVSCHI As String, _
   ByVal STU_PREVSCHI_CITY As String, ByVal STU_PREVSCHI_COUNTRY As String, ByVal STU_PREVSCHI_CLM As String, ByVal STU_PREVSCHI_GRADE As String, _
   ByVal STU_PREVSCHI_LASTATTDATE As String, ByVal STU_PREVSCHII As String, ByVal STU_PREVSCHII_CITY As String, ByVal STU_PREVSCHII_COUNTRY As String, _
   ByVal STU_PREVSCHII_CLM As String, ByVal STU_PREVSCHII_GRADE As String, ByVal STU_PREVSCHII_LASTATTDATE As String, ByVal STU_PREVSCHIII As String, _
   ByVal STU_PREVSCHIII_CITY As String, ByVal STU_PREVSCHIII_COUNTRY As String, ByVal STU_PREVSCHIII_CLM As String, ByVal STU_PREVSCHIII_GRADE As String, _
   ByVal STU_PREVSCHIII_LASTATTDATE As String, _
  ByVal STU_PREVSCHI_MEDIUM As String, ByVal STU_PREVSCHII_MEDIUM As String, ByVal STU_PREVSCHIII_MEDIUM As String, ByVal STU_MINDOJ As String, ByVal SESS_USER As String, _
  ByVal STU_COMMENT As String, ByVal STU_KNOWNNAME As String, ByVal STU_REMARKS As String, _
  ByVal STU_bSEN As Boolean, ByVal STU_bEAL As Boolean, ByVal STU_SEN_REMARK As String, ByVal STU_EAL_REMARK As String, ByVal STU_FIRSTLANG As String, ByVal STU_OTHLANG As String, ByVal STU_bRCVPUBL As Boolean, ByVal trans As SqlTransaction) As Integer
        'CODE MODIFIED BY LIJO ON 07/JUN/2009
        'Author(--Lijo)
        'Date   --05/Mar/2008
        'Purpose--To save STUDENT_M data based on the datamode (studEnqForm.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(84) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@STU_TFRTYPE", STU_TFRTYPE)
                pParms(3) = New SqlClient.SqlParameter("@STU_BLUEID", STU_BLUEID)
                pParms(4) = New SqlClient.SqlParameter("@STU_FIRSTNAME", STU_FIRSTNAME)
                pParms(5) = New SqlClient.SqlParameter("@STU_MIDNAME", STU_MIDNAME)
                pParms(6) = New SqlClient.SqlParameter("@STU_LASTNAME", STU_LASTNAME)
                pParms(7) = New SqlClient.SqlParameter("@STU_FIRSTNAMEARABIC", STU_FIRSTNAMEARABIC)
                pParms(8) = New SqlClient.SqlParameter("@STU_MIDNAMEARABIC", STU_MIDNAMEARABIC)
                pParms(9) = New SqlClient.SqlParameter("@STU_LASTNAMEARABIC", STU_LASTNAMEARABIC)
                pParms(10) = New SqlClient.SqlParameter("@STU_DOB", STU_DOB)
                pParms(11) = New SqlClient.SqlParameter("@STU_GENDER", STU_GENDER)
                pParms(12) = New SqlClient.SqlParameter("@STU_RLG_ID", STU_RLG_ID)
                pParms(13) = New SqlClient.SqlParameter("@STU_NATIONALITY", STU_NATIONALITY)
                pParms(14) = New SqlClient.SqlParameter("@STU_POB", STU_POB)
                pParms(15) = New SqlClient.SqlParameter("@STU_COB", STU_COB)
                pParms(16) = New SqlClient.SqlParameter("@STU_HOUSE_ID", STU_HOUSE)
                pParms(17) = New SqlClient.SqlParameter("@STU_MINLIST", STU_MINLIST)
                pParms(18) = New SqlClient.SqlParameter("@STU_MINLISTTYPE", STU_MINLISTTYPE)
                pParms(19) = New SqlClient.SqlParameter("@STU_FEESPONSOR", STU_FEESPONSOR)
                pParms(20) = New SqlClient.SqlParameter("@STU_EMGCONTACT", STU_EMGCONTACT)
                pParms(21) = New SqlClient.SqlParameter("@STU_HEALTH", STU_HEALTH)
                pParms(22) = New SqlClient.SqlParameter("@STU_PHYSICAL", STU_PHYSICAL)
                pParms(23) = New SqlClient.SqlParameter("@STU_bRCVSPMEDICATION", STU_bRCVSPMEDICATION)
                pParms(24) = New SqlClient.SqlParameter("@STU_SPMEDICATION", STU_SPMEDICATION)
                pParms(25) = New SqlClient.SqlParameter("@STU_PASPRTNAME", STU_PASPRTNAME)
                pParms(26) = New SqlClient.SqlParameter("@STU_PASPRTNO", STU_PASPRTNO)
                pParms(27) = New SqlClient.SqlParameter("@STU_PASPRTISSPLACE", STU_PASPRTISSPLACE)
                pParms(28) = New SqlClient.SqlParameter("@STU_PASPRTISSDATE", STU_PASPRTISSDATE)
                pParms(29) = New SqlClient.SqlParameter("@STU_PASPRTEXPDATE", STU_PASPRTEXPDATE)
                pParms(30) = New SqlClient.SqlParameter("@STU_VISANO", STU_VISANO)
                pParms(31) = New SqlClient.SqlParameter("@STU_VISAISSPLACE", STU_VISAISSPLACE)
                pParms(32) = New SqlClient.SqlParameter("@STU_VISAISSDATE", STU_VISAISSDATE)
                pParms(33) = New SqlClient.SqlParameter("@STU_VISAEXPDATE", STU_VISAEXPDATE)
                pParms(34) = New SqlClient.SqlParameter("@STU_VISAISSAUTH", STU_VISAISSAUTH)
                pParms(35) = New SqlClient.SqlParameter("@STU_BLOODGROUP", STU_BLOODGROUP)
                pParms(36) = New SqlClient.SqlParameter("@STU_HCNO", STU_HCNO)
                pParms(37) = New SqlClient.SqlParameter("@STU_PRIMARYCONTACT", STU_PRIMARYCONTACT)
                pParms(38) = New SqlClient.SqlParameter("@STU_PREFCONTACT", STU_PREFCONTACT)
                pParms(39) = New SqlClient.SqlParameter("@STU_PHOTOPATH", STU_PHOTOPATH)
                pParms(40) = New SqlClient.SqlParameter("@STU_bRCVSMS", STU_bRCVSMS)
                pParms(41) = New SqlClient.SqlParameter("@STU_bRCVMAIL", STU_bRCVMAIL)
                pParms(42) = New SqlClient.SqlParameter("@STU_SIBLING_ID", STU_SIBLING_ID)
                pParms(43) = New SqlClient.SqlParameter("@STU_PREVSCHI", STU_PREVSCHI)
                pParms(44) = New SqlClient.SqlParameter("@STU_PREVSCHI_CITY", STU_PREVSCHI_CITY)
                pParms(45) = New SqlClient.SqlParameter("@STU_PREVSCHI_COUNTRY", STU_PREVSCHI_COUNTRY)
                pParms(46) = New SqlClient.SqlParameter("@STU_PREVSCHI_CLM", STU_PREVSCHI_CLM)
                pParms(47) = New SqlClient.SqlParameter("@STU_PREVSCHI_GRADE", STU_PREVSCHI_GRADE)

                If STU_PREVSCHI_LASTATTDATE.Trim = "" Then
                    pParms(48) = New SqlClient.SqlParameter("@STU_PREVSCHI_LASTATTDATE", System.DBNull.Value)
                Else
                    pParms(48) = New SqlClient.SqlParameter("@STU_PREVSCHI_LASTATTDATE", STU_PREVSCHI_LASTATTDATE)
                End If

                pParms(49) = New SqlClient.SqlParameter("@STU_PREVSCHII", STU_PREVSCHII)
                pParms(50) = New SqlClient.SqlParameter("@STU_PREVSCHII_CITY", STU_PREVSCHII_CITY)
                pParms(51) = New SqlClient.SqlParameter("@STU_PREVSCHII_COUNTRY", STU_PREVSCHII_COUNTRY)
                pParms(52) = New SqlClient.SqlParameter("@STU_PREVSCHII_CLM", STU_PREVSCHII_CLM)
                pParms(53) = New SqlClient.SqlParameter("@STU_PREVSCHII_GRADE", STU_PREVSCHII_GRADE)

                If STU_PREVSCHII_LASTATTDATE.Trim = "" Then
                    pParms(54) = New SqlClient.SqlParameter("@STU_PREVSCHII_LASTATTDATE", System.DBNull.Value)
                Else
                    pParms(54) = New SqlClient.SqlParameter("@STU_PREVSCHII_LASTATTDATE", STU_PREVSCHII_LASTATTDATE)
                End If
                pParms(55) = New SqlClient.SqlParameter("@STU_PREVSCHIII", STU_PREVSCHIII)
                pParms(56) = New SqlClient.SqlParameter("@STU_PREVSCHIII_CITY", STU_PREVSCHIII_CITY)
                pParms(57) = New SqlClient.SqlParameter("@STU_PREVSCHIII_COUNTRY", STU_PREVSCHIII_COUNTRY)
                pParms(58) = New SqlClient.SqlParameter("@STU_PREVSCHIII_CLM", STU_PREVSCHIII_CLM)
                pParms(59) = New SqlClient.SqlParameter("@STU_PREVSCHIII_GRADE", STU_PREVSCHIII_GRADE)
                If STU_PREVSCHIII_LASTATTDATE.Trim = "" Then
                    pParms(60) = New SqlClient.SqlParameter("@STU_PREVSCHIII_LASTATTDATE", System.DBNull.Value)
                Else
                    pParms(60) = New SqlClient.SqlParameter("@STU_PREVSCHIII_LASTATTDATE", STU_PREVSCHIII_LASTATTDATE)
                End If

                pParms(61) = New SqlClient.SqlParameter("@STU_PREVSCHI_MEDIUM", STU_PREVSCHI_MEDIUM)
                pParms(62) = New SqlClient.SqlParameter("@STU_PREVSCHII_MEDIUM", STU_PREVSCHII_MEDIUM)
                pParms(63) = New SqlClient.SqlParameter("@STU_PREVSCHIII_MEDIUM", STU_PREVSCHIII_MEDIUM)

                If STU_MINDOJ.Trim = "" Then
                    pParms(64) = New SqlClient.SqlParameter("@STU_MINDOJ", System.DBNull.Value)
                Else
                    pParms(64) = New SqlClient.SqlParameter("@STU_MINDOJ", STU_MINDOJ)
                End If

                pParms(65) = New SqlClient.SqlParameter("@user", SESS_USER)
                pParms(66) = New SqlClient.SqlParameter("@STU_COMMENT", STU_COMMENT)
                pParms(67) = New SqlClient.SqlParameter("@STU_KNOWNNAME", STU_KNOWNNAME)
                pParms(68) = New SqlClient.SqlParameter("@STU_REMARKS", STU_REMARKS)

                pParms(69) = New SqlClient.SqlParameter("@STU_bSEN", STU_bSEN)
                pParms(70) = New SqlClient.SqlParameter("@STU_bEAL", STU_bEAL)
                pParms(71) = New SqlClient.SqlParameter("@STU_SEN_REMARK", STU_SEN_REMARK)
                pParms(72) = New SqlClient.SqlParameter("@STU_EAL_REMARK", STU_EAL_REMARK)
                pParms(73) = New SqlClient.SqlParameter("@STU_FIRSTLANG", STU_FIRSTLANG)
                pParms(74) = New SqlClient.SqlParameter("@STU_OTHLANG", STU_OTHLANG)
                pParms(75) = New SqlClient.SqlParameter("@STU_bRCVPUBL", STU_bRCVPUBL)

                pParms(76) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(76).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDENT_M", pParms)
                Dim ReturnFlag As Integer = pParms(76).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using

    End Function
    Public Shared Function SaveStudSTUDENT_D(ByVal STS_STU_ID As String, ByVal STS_FFIRSTNAME As String, ByVal STS_FMIDNAME As String, _
   ByVal STS_FLASTNAME As String, ByVal STS_FNATIONALITY As String, ByVal STS_FNATIONALITY2 As String, ByVal STS_FCOMADDR1 As String, _
   ByVal STS_FCOMADDR2 As String, ByVal STS_FCOMPOBOX As String, ByVal STS_FCOMCITY As String, ByVal STS_FCOMSTATE As String, _
   ByVal STS_FCOMCOUNTRY As String, ByVal STS_FOFFPHONE As String, ByVal STS_FRESPHONE As String, ByVal STS_FFAX As String, _
   ByVal STS_FMOBILE As String, ByVal STS_FPRMADDR1 As String, ByVal STS_FPRMADDR2 As String, ByVal STS_FPRMPOBOX As String, _
   ByVal STS_FPRMCITY As String, ByVal STS_FPRMCOUNTRY As String, ByVal STS_FPRMPHONE As String, ByVal STS_FOCC As String, _
   ByVal STS_FCOMPANY As String, ByVal STS_bFGEMSEMP As Boolean, ByVal STS_F_BSU_ID As String, ByVal STS_MFIRSTNAME As String, _
   ByVal STS_MMIDNAME As String, ByVal STS_MLASTNAME As String, ByVal STS_MNATIONALITY As String, ByVal STS_MNATIONALITY2 As String, _
   ByVal STS_MCOMADDR1 As String, ByVal STS_MCOMADDR2 As String, ByVal STS_MCOMPOBOX As String, ByVal STS_MCOMCITY As String, _
   ByVal STS_MCOMSTATE As String, ByVal STS_MCOMCOUNTRY As String, ByVal STS_MOFFPHONE As String, ByVal STS_MRESPHONE As String, _
   ByVal STS_MFAX As String, ByVal STS_MMOBILE As String, ByVal STS_MPRMADDR1 As String, ByVal STS_MPRMADDR2 As String, _
   ByVal STS_MPRMPOBOX As String, ByVal STS_MPRMCITY As String, ByVal STS_MPRMCOUNTRY As String, ByVal STS_MPRMPHONE As String, _
   ByVal STS_MOCC As String, ByVal STS_MCOMPANY As String, ByVal STS_bMGEMSEMP As Boolean, ByVal STS_M_BSU_ID As String, _
   ByVal STS_GFIRSTNAME As String, ByVal STS_GMIDNAME As String, ByVal STS_GLASTNAME As String, ByVal STS_GNATIONALITY As String, _
   ByVal STS_GNATIONALITY2 As String, ByVal STS_GCOMADDR1 As String, ByVal STS_GCOMADDR2 As String, ByVal STS_GCOMPOBOX As String, _
   ByVal STS_GCOMCITY As String, ByVal STS_GCOMSTATE As String, ByVal STS_GCOMCOUNTRY As String, ByVal STS_GOFFPHONE As String, _
   ByVal STS_GRESPHONE As String, ByVal STS_GFAX As String, ByVal STS_GMOBILE As String, ByVal STS_GPRMADDR1 As String, _
   ByVal STS_GPRMADDR2 As String, ByVal STS_GPRMPOBOX As String, ByVal STS_GPRMCITY As String, ByVal STS_GPRMCOUNTRY As String, _
   ByVal STS_GPRMPHONE As String, ByVal STS_GOCC As String, ByVal STS_GCOMPANY As String, _
   ByVal STS_FCOMPANY_ADDR As String, ByVal STS_FEMAIL As String, ByVal STS_MCOMPANY_ADDR As String, ByVal STS_MEMAIL As String, ByVal STS_GCOMPANY_ADDR As String, ByVal STS_GEMAIL As String, _
   ByVal STS_F_COMP_ID As String, ByVal STS_M_COMP_ID As String, ByVal STS_G_COMP_ID As String, _
   ByVal STS_FCOMSTREET As String, ByVal STS_FCOMAREA As String, ByVal STS_FCOMBLDG As String, ByVal STS_FCOMAPARTNO As String, _
   ByVal STS_MCOMSTREET As String, ByVal STS_MCOMAREA As String, ByVal STS_MCOMBLDG As String, ByVal STS_MCOMAPARTNO As String, _
   ByVal STS_GCOMSTREET As String, ByVal STS_GCOMAREA As String, ByVal STS_GCOMBLDG As String, ByVal STS_GCOMAPARTNO As String, _
   ByVal STS_FEMIR As String, ByVal STS_MEMIR As String, ByVal STS_GEMIR As String, _
   ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --05/Mar/2008
        'Purpose--To save STUDENT_D data based on the datamode (StudRecordEdit.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(99) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STS_FFIRSTNAME", STS_FFIRSTNAME)
                pParms(2) = New SqlClient.SqlParameter("@STS_FMIDNAME", STS_FMIDNAME)
                pParms(3) = New SqlClient.SqlParameter("@STS_FLASTNAME", STS_FLASTNAME)
                pParms(4) = New SqlClient.SqlParameter("@STS_FNATIONALITY", STS_FNATIONALITY)
                pParms(5) = New SqlClient.SqlParameter("@STS_FNATIONALITY2", STS_FNATIONALITY2)
                pParms(6) = New SqlClient.SqlParameter("@STS_FCOMADDR1", STS_FCOMADDR1)
                pParms(7) = New SqlClient.SqlParameter("@STS_FCOMADDR2", STS_FCOMADDR2)
                pParms(8) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", STS_FCOMPOBOX)
                pParms(9) = New SqlClient.SqlParameter("@STS_FCOMCITY", STS_FCOMCITY)
                pParms(10) = New SqlClient.SqlParameter("@STS_FCOMSTATE", STS_FCOMSTATE)
                pParms(11) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", STS_FCOMCOUNTRY)
                pParms(12) = New SqlClient.SqlParameter("@STS_FOFFPHONE", STS_FOFFPHONE)
                pParms(13) = New SqlClient.SqlParameter("@STS_FRESPHONE", STS_FRESPHONE)
                pParms(14) = New SqlClient.SqlParameter("@STS_FFAX", STS_FFAX)
                pParms(15) = New SqlClient.SqlParameter("@STS_FMOBILE", STS_FMOBILE)
                pParms(16) = New SqlClient.SqlParameter("@STS_FPRMADDR1", STS_FPRMADDR1)
                pParms(17) = New SqlClient.SqlParameter("@STS_FPRMADDR2", STS_FPRMADDR2)
                pParms(18) = New SqlClient.SqlParameter("@STS_FPRMPOBOX", STS_FPRMPOBOX)
                pParms(19) = New SqlClient.SqlParameter("@STS_FPRMCITY", STS_FPRMCITY)
                pParms(20) = New SqlClient.SqlParameter("@STS_FPRMCOUNTRY", STS_FPRMCOUNTRY)
                pParms(21) = New SqlClient.SqlParameter("@STS_FPRMPHONE", STS_FPRMPHONE)
                pParms(22) = New SqlClient.SqlParameter("@STS_FOCC", STS_FOCC)
                pParms(23) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
                pParms(24) = New SqlClient.SqlParameter("@STS_bFGEMSEMP", STS_bFGEMSEMP)
                pParms(25) = New SqlClient.SqlParameter("@STS_F_BSU_ID", STS_F_BSU_ID)
                pParms(26) = New SqlClient.SqlParameter("@STS_MFIRSTNAME", STS_MFIRSTNAME)
                pParms(27) = New SqlClient.SqlParameter("@STS_MMIDNAME", STS_MMIDNAME)
                pParms(28) = New SqlClient.SqlParameter("@STS_MLASTNAME", STS_MLASTNAME)
                pParms(29) = New SqlClient.SqlParameter("@STS_MNATIONALITY", STS_MNATIONALITY)
                pParms(30) = New SqlClient.SqlParameter("@STS_MNATIONALITY2", STS_MNATIONALITY2)
                pParms(31) = New SqlClient.SqlParameter("@STS_MCOMADDR1", STS_MCOMADDR1)
                pParms(32) = New SqlClient.SqlParameter("@STS_MCOMADDR2", STS_MCOMADDR2)
                pParms(33) = New SqlClient.SqlParameter("@STS_MCOMPOBOX", STS_MCOMPOBOX)
                pParms(34) = New SqlClient.SqlParameter("@STS_MCOMCITY", STS_MCOMCITY)
                pParms(35) = New SqlClient.SqlParameter("@STS_MCOMSTATE", STS_MCOMSTATE)
                pParms(36) = New SqlClient.SqlParameter("@STS_MCOMCOUNTRY", STS_MCOMCOUNTRY)
                pParms(37) = New SqlClient.SqlParameter("@STS_MOFFPHONE", STS_MOFFPHONE)
                pParms(38) = New SqlClient.SqlParameter("@STS_MRESPHONE", STS_MRESPHONE)
                pParms(39) = New SqlClient.SqlParameter("@STS_MFAX", STS_MFAX)
                pParms(40) = New SqlClient.SqlParameter("@STS_MMOBILE", STS_MMOBILE)
                pParms(41) = New SqlClient.SqlParameter("@STS_MPRMADDR1", STS_MPRMADDR1)
                pParms(42) = New SqlClient.SqlParameter("@STS_MPRMADDR2", STS_MPRMADDR2)
                pParms(43) = New SqlClient.SqlParameter("@STS_MPRMPOBOX", STS_MPRMPOBOX)
                pParms(44) = New SqlClient.SqlParameter("@STS_MPRMCITY", STS_MPRMCITY)
                pParms(45) = New SqlClient.SqlParameter("@STS_MPRMCOUNTRY", STS_MPRMCOUNTRY)
                pParms(46) = New SqlClient.SqlParameter("@STS_MPRMPHONE", STS_MPRMPHONE)
                pParms(47) = New SqlClient.SqlParameter("@STS_MOCC", STS_MOCC)
                pParms(48) = New SqlClient.SqlParameter("@STS_MCOMPANY", STS_MCOMPANY)
                pParms(49) = New SqlClient.SqlParameter("@STS_bMGEMSEMP", STS_bMGEMSEMP)
                pParms(50) = New SqlClient.SqlParameter("@STS_M_BSU_ID", STS_M_BSU_ID)
                pParms(51) = New SqlClient.SqlParameter("@STS_GFIRSTNAME", STS_GFIRSTNAME)
                pParms(52) = New SqlClient.SqlParameter("@STS_GMIDNAME", STS_GMIDNAME)
                pParms(53) = New SqlClient.SqlParameter("@STS_GLASTNAME", STS_GLASTNAME)
                pParms(54) = New SqlClient.SqlParameter("@STS_GNATIONALITY", STS_GNATIONALITY)
                pParms(55) = New SqlClient.SqlParameter("@STS_GNATIONALITY2", STS_GNATIONALITY2)
                pParms(56) = New SqlClient.SqlParameter("@STS_GCOMADDR1", STS_GCOMADDR1)
                pParms(57) = New SqlClient.SqlParameter("@STS_GCOMADDR2", STS_GCOMADDR2)
                pParms(58) = New SqlClient.SqlParameter("@STS_GCOMPOBOX", STS_GCOMPOBOX)
                pParms(59) = New SqlClient.SqlParameter("@STS_GCOMCITY", STS_GCOMCITY)
                pParms(60) = New SqlClient.SqlParameter("@STS_GCOMSTATE", STS_GCOMSTATE)
                pParms(61) = New SqlClient.SqlParameter("@STS_GCOMCOUNTRY", STS_GCOMCOUNTRY)
                pParms(62) = New SqlClient.SqlParameter("@STS_GOFFPHONE", STS_GOFFPHONE)
                pParms(63) = New SqlClient.SqlParameter("@STS_GRESPHONE", STS_GRESPHONE)
                pParms(64) = New SqlClient.SqlParameter("@STS_GFAX", STS_GFAX)
                pParms(65) = New SqlClient.SqlParameter("@STS_GMOBILE", STS_GMOBILE)
                pParms(66) = New SqlClient.SqlParameter("@STS_GPRMADDR1", STS_GPRMADDR1)
                pParms(67) = New SqlClient.SqlParameter("@STS_GPRMADDR2", STS_GPRMADDR2)

                pParms(68) = New SqlClient.SqlParameter("@STS_GPRMPOBOX", STS_GPRMPOBOX)
                pParms(69) = New SqlClient.SqlParameter("@STS_GPRMCITY", STS_GPRMCITY)
                pParms(70) = New SqlClient.SqlParameter("@STS_GPRMCOUNTRY", STS_GPRMCOUNTRY)
                pParms(71) = New SqlClient.SqlParameter("@STS_GPRMPHONE", STS_GPRMPHONE)
                pParms(72) = New SqlClient.SqlParameter("@STS_GOCC", STS_GOCC)
                pParms(73) = New SqlClient.SqlParameter("@STS_GCOMPANY", STS_GCOMPANY)
                pParms(74) = New SqlClient.SqlParameter("@STS_FCOMPANY_ADDR", STS_FCOMPANY_ADDR)
                pParms(75) = New SqlClient.SqlParameter("@STS_FEMAIL", STS_FEMAIL)
                pParms(76) = New SqlClient.SqlParameter("@STS_MCOMPANY_ADDR", STS_MCOMPANY_ADDR)
                pParms(77) = New SqlClient.SqlParameter("@STS_MEMAIL", STS_MEMAIL)
                pParms(78) = New SqlClient.SqlParameter("@STS_GCOMPANY_ADDR", STS_GCOMPANY_ADDR)
                pParms(79) = New SqlClient.SqlParameter("@STS_GEMAIL", STS_GEMAIL)
                pParms(80) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
                pParms(81) = New SqlClient.SqlParameter("@STS_M_COMP_ID", STS_M_COMP_ID)
                pParms(82) = New SqlClient.SqlParameter("@STS_G_COMP_ID", STS_G_COMP_ID)

                pParms(83) = New SqlClient.SqlParameter("@STS_FCOMSTREET", STS_FCOMSTREET)
                pParms(84) = New SqlClient.SqlParameter("@STS_FCOMAREA", STS_FCOMAREA)
                pParms(85) = New SqlClient.SqlParameter("@STS_FCOMBLDG", STS_FCOMBLDG)
                pParms(86) = New SqlClient.SqlParameter("@STS_FCOMAPARTNO", STS_FCOMAPARTNO)


                pParms(87) = New SqlClient.SqlParameter("@STS_MCOMSTREET", STS_MCOMSTREET)
                pParms(88) = New SqlClient.SqlParameter("@STS_MCOMAREA", STS_MCOMAREA)
                pParms(89) = New SqlClient.SqlParameter("@STS_MCOMBLDG", STS_MCOMBLDG)
                pParms(90) = New SqlClient.SqlParameter("@STS_MCOMAPARTNO", STS_MCOMAPARTNO)

                pParms(91) = New SqlClient.SqlParameter("@STS_GCOMSTREET", STS_GCOMSTREET)
                pParms(92) = New SqlClient.SqlParameter("@STS_GCOMAREA", STS_GCOMAREA)
                pParms(93) = New SqlClient.SqlParameter("@STS_GCOMBLDG", STS_GCOMBLDG)
                pParms(94) = New SqlClient.SqlParameter("@STS_GCOMAPARTNO", STS_GCOMAPARTNO)

                pParms(95) = New SqlClient.SqlParameter("@STS_FEMIR", STS_FEMIR)
                pParms(96) = New SqlClient.SqlParameter("@STS_MEMIR", STS_MEMIR)
                pParms(97) = New SqlClient.SqlParameter("@STS_GEMIR", STS_GEMIR)

                pParms(98) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(98).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDENT_D", pParms)
                Dim ReturnFlag As Integer = pParms(98).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using

    End Function

    Public Shared Function SaveStudSTUDENT_SERVICES_D(ByVal SSV_SVC_ID As Integer, ByVal SSV_BSU_ID As String, ByVal SSV_ACD_ID As Integer, ByVal SSV_STU_ID As Integer, ByVal mode As String, ByVal trans As SqlTransaction)
        Dim pParms(5) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@SSV_SVC_ID", SSV_SVC_ID)
            pParms(1) = New SqlClient.SqlParameter("@SSV_BSU_ID", SSV_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@SSV_ACD_ID", SSV_ACD_ID)
            pParms(3) = New SqlClient.SqlParameter("@SSV_STU_ID", SSV_STU_ID)
            pParms(4) = New SqlClient.SqlParameter("@mode", mode)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDENT_SERVICES_D", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Public Shared Function studQUICKENROLL(ByVal EQS_XML As String, ByVal DOJ As String, ByVal MINDOJ As String, ByVal STU_TFRTYPE As String, ByVal BSU_ID As String, ByVal USR As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_XML", EQS_XML)
                pParms(1) = New SqlClient.SqlParameter("@STU_DOJ", DOJ)
                pParms(2) = New SqlClient.SqlParameter("@STU_MINDOJ", MINDOJ)
                pParms(3) = New SqlClient.SqlParameter("@STU_TFRTYPE", STU_TFRTYPE)
                pParms(4) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                pParms(5) = New SqlClient.SqlParameter("@USR", USR)
                pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(6).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "studQUICKENROLL", pParms)
                Dim ReturnFlag As Integer = pParms(6).Value
                Return ReturnFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return -1
            End Try
        End Using

    End Function
    Public Shared Function SaveEnqQuick_Reg(ByVal EQM_APPLFIRSTNAME As String, ByVal EQM_APPLMIDNAME As String, ByVal EQM_APPLLASTNAME As String, _
              ByVal EQM_APPLDOB As Date, ByVal EQM_APPLGENDER As String, ByVal EQM_REL_ID As String, ByVal EQM_APPLNATIONALITY As String, _
               ByVal EQM_APPLPASPRTNO As String, ByVal ENQ_APPLPASSPORTNAME As String, ByVal EQM_PRIMARYCONTACT As String, _
               ByVal EQM_ENQTYPE As String, ByVal EQM_STATUS As String, ByVal EQM_EMGCONTACT As String, _
 ByVal EQP_FFIRSTNAME As String, ByVal EQP_FMIDNAME As String, ByVal EQP_FLASTNAME As String, _
ByVal EQP_FEMAIL As String, ByVal EQP_FMOBILECODE As String, ByVal EQP_FMOBILE As String, ByVal EQS_ACY_ID As String, _
ByVal EQS_BSU_ID As String, ByVal EQS_GRD_ID As String, ByVal EQS_SHF_ID As String, ByVal EQS_STATUS As String, _
ByVal EQS_STM_ID As String, ByVal EQS_CLM_ID As String, ByVal EQM_TRM_ID As String, ByVal EQS_REGNDATE As String, ByRef TEMP_ApplNo As String, _
ByRef TEMP_EQS_ID As String, ByRef TEMP_STU_NO As String, ByRef TEMP_EQM_ENQID As String, ByVal trans As SqlTransaction) As Integer


        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try


                Dim pParms(32) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQM_APPLFIRSTNAME", EQM_APPLFIRSTNAME)
                pParms(1) = New SqlClient.SqlParameter("@EQM_APPLMIDNAME", EQM_APPLMIDNAME)
                pParms(2) = New SqlClient.SqlParameter("@EQM_APPLLASTNAME", EQM_APPLLASTNAME)
                'If EQM_APPLDOB <> Date.MinValue Then
                pParms(3) = New SqlClient.SqlParameter("@EQM_APPLDOB", EQM_APPLDOB)
                ' Else
                'pParms(3) = New SqlClient.SqlParameter("@EQM_APPLDOB", DBNull.Value)
                'End If
                pParms(4) = New SqlClient.SqlParameter("@EQM_APPLGENDER", EQM_APPLGENDER)
                pParms(5) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", EQM_PRIMARYCONTACT)
                pParms(6) = New SqlClient.SqlParameter("@EQM_APPLPASPRTNO", EQM_APPLPASPRTNO)
                pParms(7) = New SqlClient.SqlParameter("@ENQ_APPLPASSPORTNAME", ENQ_APPLPASSPORTNAME)

                pParms(8) = New SqlClient.SqlParameter("@EQM_ENQTYPE", EQM_ENQTYPE)
                pParms(9) = New SqlClient.SqlParameter("@EQM_STATUS", EQM_STATUS)
                pParms(10) = New SqlClient.SqlParameter("@EQM_EMGCONTACT", EQM_EMGCONTACT)
                pParms(11) = New SqlClient.SqlParameter("@EQM_REL_ID", EQM_REL_ID)
                pParms(12) = New SqlClient.SqlParameter("@EQM_APPLNATIONALITY", EQM_APPLNATIONALITY)
                pParms(13) = New SqlClient.SqlParameter("@EQP_FFIRSTNAME", EQP_FFIRSTNAME)
                pParms(14) = New SqlClient.SqlParameter("@EQP_FMIDNAME", EQP_FMIDNAME)
                pParms(15) = New SqlClient.SqlParameter("@EQP_FLASTNAME", EQP_FLASTNAME)
                pParms(16) = New SqlClient.SqlParameter("@EQP_FEMAIL", EQP_FEMAIL)
                pParms(17) = New SqlClient.SqlParameter("@EQP_FMOBILECODE", EQP_FMOBILECODE)
                pParms(18) = New SqlClient.SqlParameter("@EQP_FMOBILE", EQP_FMOBILE)
                pParms(19) = New SqlClient.SqlParameter("@EQS_ACY_ID", EQS_ACY_ID)
                pParms(20) = New SqlClient.SqlParameter("@EQS_BSU_ID", EQS_BSU_ID)
                pParms(21) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
                pParms(22) = New SqlClient.SqlParameter("@EQS_SHF_ID", EQS_SHF_ID)
                pParms(23) = New SqlClient.SqlParameter("@EQS_STATUS", EQS_STATUS)
                pParms(24) = New SqlClient.SqlParameter("@EQS_STM_ID", EQS_STM_ID)
                pParms(25) = New SqlClient.SqlParameter("@EQS_CLM_ID", EQS_CLM_ID)
                pParms(26) = New SqlClient.SqlParameter("@EQM_TRM_ID", EQM_TRM_ID)
                pParms(27) = New SqlClient.SqlParameter("@EQS_REGNDATE", EQS_REGNDATE)

                pParms(28) = New SqlClient.SqlParameter("@TEMP_ApplNo", SqlDbType.VarChar, 20)
                pParms(28).Direction = ParameterDirection.Output
                pParms(29) = New SqlClient.SqlParameter("@TEMP_EQS_ID", SqlDbType.Int)
                pParms(29).Direction = ParameterDirection.Output
                pParms(30) = New SqlClient.SqlParameter("@EQS_STU_NO", SqlDbType.VarChar, 15)
                pParms(30).Direction = ParameterDirection.Output
                pParms(31) = New SqlClient.SqlParameter("@EQM_ENQID_OUT", SqlDbType.Int)
                pParms(31).Direction = ParameterDirection.Output
                pParms(32) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(32).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEnqQuick_Reg", pParms)
                Dim ReturnFlag As Integer = pParms(32).Value
                If ReturnFlag = 0 Then
                    TEMP_ApplNo = pParms(28).Value
                    TEMP_EQS_ID = pParms(29).Value
                    TEMP_STU_NO = pParms(30).Value
                    TEMP_EQM_ENQID = pParms(31).Value
                End If

                Return ReturnFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return -1
            End Try
        End Using

    End Function


    Public Shared Function SaveEnqQuick_Reg_Stage(ByVal EQS_ID As String, ByRef Accno As String, ByVal trans As SqlTransaction) As Integer


        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try


                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_ID", EQS_ID)
                pParms(1) = New SqlClient.SqlParameter("@EQS_ACCNO", SqlDbType.VarChar, 50)
                pParms(1).Direction = ParameterDirection.Output
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEnqQuick_Reg_Stage", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value

                If ReturnFlag = 0 Then
                    Accno = pParms(1).Value

                End If
                Return ReturnFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return -1
            End Try
        End Using

    End Function


    Public Shared Function SaveStudENQUIRY_M(ByVal EQM_ENQID As String, ByVal EQM_ENQDATE As Date, _
            ByVal EQM_APPLFIRSTNAME As String, ByVal EQM_APPLMIDNAME As String, ByVal EQM_APPLLASTNAME As String, ByVal ENQ_APPLPASSPORTNAME As String, _
            ByVal EQM_APPLDOB As Date, ByVal EQM_APPLGENDER As String, ByVal EQM_REL_ID As String, ByVal EQM_APPLNATIONALITY As String, _
            ByVal EQM_APPLPOB As String, ByVal EQM_APPLCOB As String, ByVal EQM_APPLPASPRTNO As String, ByVal EQM_APPLPASPRTISSDATE As Date, _
            ByVal EQM_APPLPASPRTEXPDATE As Date, ByVal EQM_APPLPASPRTISSPLACE As String, _
            ByVal EQM_APPLVISANO As String, ByVal EQM_APPLVISAISSDATE As Date, ByVal EQM_APPLVISAEXPDATE As Date, _
            ByVal EQM_APPLVISAISSPLACE As String, ByVal EQM_APPLVISAISSAUTH As String, ByVal EQM_bPREVSCHOOLGEMS As Boolean, _
            ByVal EQM_PREVSCHOOL_BSU_ID As String, ByVal EQM_PREVSCHOOL_NURSERY As String, _
            ByVal EQM_PREVSCHOOL_REG_ID As String, ByVal EQM_PREVSCHOOL As String, _
            ByVal EQM_PREVSCHOOL_CITY As String, ByVal EQM_PREVSCHOOL_CTY_ID As String, ByVal EQM_PREVSCHOOL_GRD_ID As String, _
            ByVal EQM_PREVSCHOOL_CLM_ID As String, ByVal EQM_PREVSCHOOL_MEDIUM As String, ByVal EQM_PREVSCHOOL_LASTATTDATE As Date, _
            ByVal EQM_bAPPLSIBLING As Boolean, ByVal EQM_SIBLINGFEEID As String, _
            ByVal EQM_SIBLINGSCHOOL As String, ByVal EQM_bEXSTUDENT As Boolean, _
            ByVal EQM_EXUNIT As String, ByVal EQM_EX_STU_ID As String, _
            ByVal EQM_bSTAFFGEMS As Boolean, ByVal EQM_STAFFUNIT As String, _
            ByVal EQM_STAFF_EMP_ID As String, ByVal EQM_bRCVSPMEDICATION As Boolean, _
            ByVal EQM_SPMEDICN As String, ByVal EQM_REMARKS As String, _
            ByVal EQM_EQP_ID As String, ByVal EQM_bRCVSMS As Boolean, _
            ByVal EQM_bRCVMAIL As Boolean, ByVal EQM_PRIMARYCONTACT As String, _
            ByVal EQM_PREFCONTACT As String, ByVal EQM_MODE_ID As String, _
            ByVal EQM_ENQTYPE As String, ByVal EQM_STATUS As String, ByVal EQM_EMGCONTACT As String, ByVal EQM_TRM_ID As String, _
            ByVal EQM_FILLED_BY As String, ByVal EQM_AGENT_NAME As String, ByVal EQM_AGENT_MOB As String, ByVal EQM_FIRSTLANG As String, ByVal EQM_OTHLANG As String, ByVal EQM_bRCVPUBL As String, ByVal EQM_SAUDI_ID As String, _
            ByRef TEMP_EQM_ENQID As Integer, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        ''Date   --14/FEB/2008
        'Modified-2/MAR/2008
        ''Purpose--To save ENQUIRY_M data based on the datamode (studEnqForm.aspx)

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try


                Dim pParms(75) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQM_ENQID", EQM_ENQID)
                pParms(1) = New SqlClient.SqlParameter("@EQM_ENQDATE", EQM_ENQDATE)

                pParms(2) = New SqlClient.SqlParameter("@EQM_APPLFIRSTNAME", EQM_APPLFIRSTNAME)
                pParms(3) = New SqlClient.SqlParameter("@EQM_APPLMIDNAME", EQM_APPLMIDNAME)
                pParms(4) = New SqlClient.SqlParameter("@EQM_APPLLASTNAME", EQM_APPLLASTNAME)
                pParms(5) = New SqlClient.SqlParameter("@ENQ_APPLPASSPORTNAME", ENQ_APPLPASSPORTNAME)
                pParms(6) = New SqlClient.SqlParameter("@EQM_APPLDOB", EQM_APPLDOB)
                pParms(7) = New SqlClient.SqlParameter("@EQM_APPLGENDER", EQM_APPLGENDER)
                pParms(8) = New SqlClient.SqlParameter("@EQM_REL_ID", EQM_REL_ID)
                pParms(9) = New SqlClient.SqlParameter("@EQM_APPLNATIONALITY", EQM_APPLNATIONALITY)
                pParms(10) = New SqlClient.SqlParameter("@EQM_APPLPOB", EQM_APPLPOB)
                pParms(11) = New SqlClient.SqlParameter("@EQM_APPLCOB", EQM_APPLCOB)
                pParms(12) = New SqlClient.SqlParameter("@EQM_APPLPASPRTNO", EQM_APPLPASPRTNO)

                If EQM_APPLPASPRTISSDATE <> Date.MinValue Then
                    pParms(13) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSDATE", EQM_APPLPASPRTISSDATE)
                Else
                    pParms(13) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSDATE", DBNull.Value)
                End If

                If EQM_APPLPASPRTEXPDATE <> Date.MinValue Then
                    pParms(14) = New SqlClient.SqlParameter("@EQM_APPLPASPRTEXPDATE", EQM_APPLPASPRTEXPDATE)
                Else
                    pParms(14) = New SqlClient.SqlParameter("@EQM_APPLPASPRTEXPDATE", DBNull.Value)
                End If

                pParms(15) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSPLACE", EQM_APPLPASPRTISSPLACE)
                pParms(16) = New SqlClient.SqlParameter("@EQM_APPLVISANO", EQM_APPLVISANO)
                If EQM_APPLVISAISSDATE <> Date.MinValue Then
                    pParms(17) = New SqlClient.SqlParameter("@EQM_APPLVISAISSDATE", EQM_APPLVISAISSDATE)
                Else
                    pParms(17) = New SqlClient.SqlParameter("@EQM_APPLVISAISSDATE", DBNull.Value)
                End If


                If EQM_APPLVISAEXPDATE <> Date.MinValue Then
                    pParms(18) = New SqlClient.SqlParameter("@EQM_APPLVISAEXPDATE", EQM_APPLVISAEXPDATE)
                Else
                    pParms(18) = New SqlClient.SqlParameter("@EQM_APPLVISAEXPDATE", DBNull.Value)
                End If

                pParms(19) = New SqlClient.SqlParameter("@EQM_APPLVISAISSPLACE", EQM_APPLVISAISSPLACE)
                pParms(20) = New SqlClient.SqlParameter("@EQM_APPLVISAISSAUTH", EQM_APPLVISAISSAUTH)
                pParms(21) = New SqlClient.SqlParameter("@EQM_bPREVSCHOOLGEMS", EQM_bPREVSCHOOLGEMS)
                pParms(22) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_BSU_ID", EQM_PREVSCHOOL_BSU_ID)
                pParms(23) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_NURSERY", EQM_PREVSCHOOL_NURSERY)
                pParms(24) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_REG_ID", EQM_PREVSCHOOL_REG_ID)

                pParms(25) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL", EQM_PREVSCHOOL)
                pParms(26) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CITY", EQM_PREVSCHOOL_CITY)
                pParms(27) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_GRD_ID", EQM_PREVSCHOOL_GRD_ID)

                pParms(28) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CTY_ID", EQM_PREVSCHOOL_CTY_ID)
                pParms(29) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CLM_ID", EQM_PREVSCHOOL_CLM_ID)
                pParms(30) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_MEDIUM", EQM_PREVSCHOOL_MEDIUM)


                If EQM_PREVSCHOOL_LASTATTDATE <> Date.MinValue Then
                    pParms(31) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_LASTATTDATE", EQM_PREVSCHOOL_LASTATTDATE)
                Else
                    pParms(31) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_LASTATTDATE", DBNull.Value)
                End If

                pParms(32) = New SqlClient.SqlParameter("@EQM_bAPPLSIBLING", EQM_bAPPLSIBLING)
                pParms(33) = New SqlClient.SqlParameter("@EQM_SIBLINGFEEID", EQM_SIBLINGFEEID)
                pParms(34) = New SqlClient.SqlParameter("@EQM_SIBLINGSCHOOL", EQM_SIBLINGSCHOOL)
                pParms(35) = New SqlClient.SqlParameter("@EQM_bEXSTUDENT", EQM_bEXSTUDENT)
                pParms(36) = New SqlClient.SqlParameter("@EQM_EXUNIT", EQM_EXUNIT)
                pParms(37) = New SqlClient.SqlParameter("@EQM_EX_STU_ID", EQM_EX_STU_ID)
                pParms(38) = New SqlClient.SqlParameter("@EQM_bSTAFFGEMS", EQM_bSTAFFGEMS)
                pParms(39) = New SqlClient.SqlParameter("@EQM_STAFFUNIT", EQM_STAFFUNIT)
                pParms(40) = New SqlClient.SqlParameter("@EQM_STAFF_EMP_ID", EQM_STAFF_EMP_ID)
                pParms(41) = New SqlClient.SqlParameter("@EQM_bRCVSPMEDICATION", EQM_bRCVSPMEDICATION)
                pParms(42) = New SqlClient.SqlParameter("@EQM_SPMEDICN", EQM_SPMEDICN)
                pParms(43) = New SqlClient.SqlParameter("@EQM_REMARKS", EQM_REMARKS)
                pParms(44) = New SqlClient.SqlParameter("@EQM_EQP_ID", EQM_EQP_ID)
                pParms(45) = New SqlClient.SqlParameter("@EQM_bRCVSMS", EQM_bRCVSMS)
                pParms(46) = New SqlClient.SqlParameter("@EQM_bRCVMAIL", EQM_bRCVMAIL)
                pParms(47) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", EQM_PRIMARYCONTACT)
                pParms(48) = New SqlClient.SqlParameter("@EQM_PREFCONTACT", EQM_PREFCONTACT)
                pParms(49) = New SqlClient.SqlParameter("@EQM_MODE_ID", EQM_MODE_ID)
                pParms(50) = New SqlClient.SqlParameter("@EQM_ENQTYPE", EQM_ENQTYPE)
                pParms(51) = New SqlClient.SqlParameter("@EQM_STATUS", EQM_STATUS)
                pParms(52) = New SqlClient.SqlParameter("@EQM_EMGCONTACT", EQM_EMGCONTACT)
                pParms(53) = New SqlClient.SqlParameter("@EQM_TRM_ID", EQM_TRM_ID)


                pParms(54) = New SqlClient.SqlParameter("@EQM_FILLED_BY", EQM_FILLED_BY)
                pParms(55) = New SqlClient.SqlParameter("@EQM_AGENT_NAME", EQM_AGENT_NAME)
                pParms(56) = New SqlClient.SqlParameter("@EQM_AGENT_MOB", EQM_AGENT_MOB)

                pParms(57) = New SqlClient.SqlParameter("@EQM_FIRSTLANG", EQM_FIRSTLANG)
                pParms(58) = New SqlClient.SqlParameter("@EQM_OTHLANG", EQM_OTHLANG)
                pParms(59) = New SqlClient.SqlParameter("@EQM_bRCVPUBL", EQM_bRCVPUBL)
                pParms(60) = New SqlClient.SqlParameter("@EQM_SAUDI_ID", EQM_SAUDI_ID)


                pParms(61) = New SqlClient.SqlParameter("@TEMP_EQM_ENQID", SqlDbType.Int)
                pParms(61).Direction = ParameterDirection.Output
                pParms(62) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(62).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudENQUIRY_M", pParms)
                Dim ReturnFlag As Integer = pParms(62).Value
                If ReturnFlag = 0 Then
                    TEMP_EQM_ENQID = pParms(61).Value
                End If

                Return ReturnFlag
            Catch ex As Exception
                Return -1
            End Try
        End Using






    End Function
    Public Shared Function SaveStudENQUIRY_M_NEW(ByVal EQM_ENQID As String, ByVal EQM_ENQDATE As Date, _
          ByVal EQM_APPLFIRSTNAME As String, ByVal EQM_APPLMIDNAME As String, ByVal EQM_APPLLASTNAME As String, ByVal ENQ_APPLPASSPORTNAME As String, _
          ByVal EQM_APPLDOB As Date, ByVal EQM_APPLGENDER As String, ByVal EQM_REL_ID As String, ByVal EQM_APPLNATIONALITY As String, _
          ByVal EQM_APPLPOB As String, ByVal EQM_APPLCOB As String, ByVal EQM_APPLPASPRTNO As String, ByVal EQM_APPLPASPRTISSDATE As Date, _
          ByVal EQM_APPLPASPRTEXPDATE As Date, ByVal EQM_APPLPASPRTISSPLACE As String, _
          ByVal EQM_APPLVISANO As String, ByVal EQM_APPLVISAISSDATE As Date, ByVal EQM_APPLVISAEXPDATE As Date, _
          ByVal EQM_APPLVISAISSPLACE As String, ByVal EQM_APPLVISAISSAUTH As String, ByVal EQM_bPREVSCHOOLGEMS As Boolean, _
          ByVal EQM_PREVSCHOOL_BSU_ID As String, ByVal EQM_PREVSCHOOL_NURSERY As String, _
          ByVal EQM_PREVSCHOOL_REG_ID As String, ByVal EQM_PREVSCHOOL As String, _
          ByVal EQM_PREVSCHOOL_CITY As String, ByVal EQM_PREVSCHOOL_CTY_ID As String, ByVal EQM_PREVSCHOOL_GRD_ID As String, _
          ByVal EQM_PREVSCHOOL_CLM_ID As String, ByVal EQM_PREVSCHOOL_MEDIUM As String, ByVal EQM_PREVSCHOOL_LASTATTDATE As Date, _
          ByVal EQM_bAPPLSIBLING As Boolean, ByVal EQM_SIBLINGFEEID As String, _
          ByVal EQM_SIBLINGSCHOOL As String, ByVal EQM_bEXSTUDENT As Boolean, _
          ByVal EQM_EXUNIT As String, ByVal EQM_EX_STU_ID As String, _
          ByVal EQM_bSTAFFGEMS As Boolean, ByVal EQM_STAFFUNIT As String, _
          ByVal EQM_STAFF_EMP_ID As String, ByVal EQM_bRCVSPMEDICATION As Boolean, _
          ByVal EQM_SPMEDICN As String, ByVal EQM_REMARKS As String, _
          ByVal EQM_EQP_ID As String, ByVal EQM_bRCVSMS As Boolean, _
          ByVal EQM_bRCVMAIL As Boolean, ByVal EQM_PRIMARYCONTACT As String, _
          ByVal EQM_PREFCONTACT As String, ByVal EQM_MODE_ID As String, _
          ByVal EQM_ENQTYPE As String, ByVal EQM_STATUS As String, ByVal EQM_EMGCONTACT As String, ByVal EQM_TRM_ID As String, _
          ByVal EQM_FILLED_BY As String, ByVal EQM_AGENT_NAME As String, ByVal EQM_AGENT_MOB As String, _
          ByVal EQM_FIRSTLANG As String, ByVal EQM_OTHLANG As String, ByVal EQM_bRCVPUBL As String, _
          ByVal EQM_SAUDI_ID As String, ByVal EQM_BLOODGROUP As String, ByVal EQM_HEALTHCARDNO As String, ByVal EQM_FEE_SPONSOR As String, _
          ByVal EQM_bMEAL As Boolean, ByVal EQM_APPLEMIRATES_ID As String, ByVal EQM_MODE_NOTE As String, ByRef TEMP_EQM_ENQID As Integer, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        ''Date   --14/FEB/2008
        'Modified-2/MAR/2008
        ''Purpose--To save ENQUIRY_M data based on the datamode (studEnqForm.aspx)

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try


                Dim pParms(85) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQM_ENQID", EQM_ENQID)
                pParms(1) = New SqlClient.SqlParameter("@EQM_ENQDATE", EQM_ENQDATE)

                pParms(2) = New SqlClient.SqlParameter("@EQM_APPLFIRSTNAME", EQM_APPLFIRSTNAME)
                pParms(3) = New SqlClient.SqlParameter("@EQM_APPLMIDNAME", EQM_APPLMIDNAME)
                pParms(4) = New SqlClient.SqlParameter("@EQM_APPLLASTNAME", EQM_APPLLASTNAME)
                pParms(5) = New SqlClient.SqlParameter("@ENQ_APPLPASSPORTNAME", ENQ_APPLPASSPORTNAME)
                pParms(6) = New SqlClient.SqlParameter("@EQM_APPLDOB", EQM_APPLDOB)
                pParms(7) = New SqlClient.SqlParameter("@EQM_APPLGENDER", EQM_APPLGENDER)
                pParms(8) = New SqlClient.SqlParameter("@EQM_REL_ID", EQM_REL_ID)
                pParms(9) = New SqlClient.SqlParameter("@EQM_APPLNATIONALITY", EQM_APPLNATIONALITY)
                pParms(10) = New SqlClient.SqlParameter("@EQM_APPLPOB", EQM_APPLPOB)
                pParms(11) = New SqlClient.SqlParameter("@EQM_APPLCOB", EQM_APPLCOB)
                pParms(12) = New SqlClient.SqlParameter("@EQM_APPLPASPRTNO", EQM_APPLPASPRTNO)

                If EQM_APPLPASPRTISSDATE <> Date.MinValue Then
                    pParms(13) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSDATE", EQM_APPLPASPRTISSDATE)
                Else
                    pParms(13) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSDATE", DBNull.Value)
                End If

                If EQM_APPLPASPRTEXPDATE <> Date.MinValue Then
                    pParms(14) = New SqlClient.SqlParameter("@EQM_APPLPASPRTEXPDATE", EQM_APPLPASPRTEXPDATE)
                Else
                    pParms(14) = New SqlClient.SqlParameter("@EQM_APPLPASPRTEXPDATE", DBNull.Value)
                End If

                pParms(15) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSPLACE", EQM_APPLPASPRTISSPLACE)
                pParms(16) = New SqlClient.SqlParameter("@EQM_APPLVISANO", EQM_APPLVISANO)
                If EQM_APPLVISAISSDATE <> Date.MinValue Then
                    pParms(17) = New SqlClient.SqlParameter("@EQM_APPLVISAISSDATE", EQM_APPLVISAISSDATE)
                Else
                    pParms(17) = New SqlClient.SqlParameter("@EQM_APPLVISAISSDATE", DBNull.Value)
                End If


                If EQM_APPLVISAEXPDATE <> Date.MinValue Then
                    pParms(18) = New SqlClient.SqlParameter("@EQM_APPLVISAEXPDATE", EQM_APPLVISAEXPDATE)
                Else
                    pParms(18) = New SqlClient.SqlParameter("@EQM_APPLVISAEXPDATE", DBNull.Value)
                End If

                pParms(19) = New SqlClient.SqlParameter("@EQM_APPLVISAISSPLACE", EQM_APPLVISAISSPLACE)
                pParms(20) = New SqlClient.SqlParameter("@EQM_APPLVISAISSAUTH", EQM_APPLVISAISSAUTH)
                pParms(21) = New SqlClient.SqlParameter("@EQM_bPREVSCHOOLGEMS", EQM_bPREVSCHOOLGEMS)
                pParms(22) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_BSU_ID", EQM_PREVSCHOOL_BSU_ID)
                pParms(23) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_NURSERY", EQM_PREVSCHOOL_NURSERY)
                pParms(24) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_REG_ID", EQM_PREVSCHOOL_REG_ID)

                pParms(25) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL", EQM_PREVSCHOOL)
                pParms(26) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CITY", EQM_PREVSCHOOL_CITY)
                pParms(27) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_GRD_ID", EQM_PREVSCHOOL_GRD_ID)

                pParms(28) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CTY_ID", EQM_PREVSCHOOL_CTY_ID)
                pParms(29) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CLM_ID", EQM_PREVSCHOOL_CLM_ID)
                pParms(30) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_MEDIUM", EQM_PREVSCHOOL_MEDIUM)


                If EQM_PREVSCHOOL_LASTATTDATE <> Date.MinValue Then
                    pParms(31) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_LASTATTDATE", EQM_PREVSCHOOL_LASTATTDATE)
                Else
                    pParms(31) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_LASTATTDATE", DBNull.Value)
                End If

                pParms(32) = New SqlClient.SqlParameter("@EQM_bAPPLSIBLING", EQM_bAPPLSIBLING)
                pParms(33) = New SqlClient.SqlParameter("@EQM_SIBLINGFEEID", EQM_SIBLINGFEEID)
                pParms(34) = New SqlClient.SqlParameter("@EQM_SIBLINGSCHOOL", EQM_SIBLINGSCHOOL)
                pParms(35) = New SqlClient.SqlParameter("@EQM_bEXSTUDENT", EQM_bEXSTUDENT)
                pParms(36) = New SqlClient.SqlParameter("@EQM_EXUNIT", EQM_EXUNIT)
                pParms(37) = New SqlClient.SqlParameter("@EQM_EX_STU_ID", EQM_EX_STU_ID)
                pParms(38) = New SqlClient.SqlParameter("@EQM_bSTAFFGEMS", EQM_bSTAFFGEMS)
                pParms(39) = New SqlClient.SqlParameter("@EQM_STAFFUNIT", EQM_STAFFUNIT)
                pParms(40) = New SqlClient.SqlParameter("@EQM_STAFF_EMP_ID", EQM_STAFF_EMP_ID)
                pParms(41) = New SqlClient.SqlParameter("@EQM_bRCVSPMEDICATION", EQM_bRCVSPMEDICATION)
                pParms(42) = New SqlClient.SqlParameter("@EQM_SPMEDICN", EQM_SPMEDICN)
                pParms(43) = New SqlClient.SqlParameter("@EQM_REMARKS", EQM_REMARKS)
                pParms(44) = New SqlClient.SqlParameter("@EQM_EQP_ID", EQM_EQP_ID)
                pParms(45) = New SqlClient.SqlParameter("@EQM_bRCVSMS", EQM_bRCVSMS)
                pParms(46) = New SqlClient.SqlParameter("@EQM_bRCVMAIL", EQM_bRCVMAIL)
                pParms(47) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", EQM_PRIMARYCONTACT)
                pParms(48) = New SqlClient.SqlParameter("@EQM_PREFCONTACT", EQM_PREFCONTACT)
                pParms(49) = New SqlClient.SqlParameter("@EQM_MODE_ID", EQM_MODE_ID)
                pParms(50) = New SqlClient.SqlParameter("@EQM_ENQTYPE", EQM_ENQTYPE)
                pParms(51) = New SqlClient.SqlParameter("@EQM_STATUS", EQM_STATUS)
                pParms(52) = New SqlClient.SqlParameter("@EQM_EMGCONTACT", EQM_EMGCONTACT)
                pParms(53) = New SqlClient.SqlParameter("@EQM_TRM_ID", EQM_TRM_ID)


                pParms(54) = New SqlClient.SqlParameter("@EQM_FILLED_BY", EQM_FILLED_BY)
                pParms(55) = New SqlClient.SqlParameter("@EQM_AGENT_NAME", EQM_AGENT_NAME)
                pParms(56) = New SqlClient.SqlParameter("@EQM_AGENT_MOB", EQM_AGENT_MOB)

                pParms(57) = New SqlClient.SqlParameter("@EQM_FIRSTLANG", EQM_FIRSTLANG)
                pParms(58) = New SqlClient.SqlParameter("@EQM_OTHLANG", EQM_OTHLANG)
                pParms(59) = New SqlClient.SqlParameter("@EQM_bRCVPUBL", EQM_bRCVPUBL)
                pParms(60) = New SqlClient.SqlParameter("@EQM_SAUDI_ID", EQM_SAUDI_ID)

                pParms(61) = New SqlClient.SqlParameter("@EQM_BLOODGROUP", EQM_BLOODGROUP)

                pParms(62) = New SqlClient.SqlParameter("@EQM_HEALTHCARDNO", EQM_HEALTHCARDNO)
                pParms(63) = New SqlClient.SqlParameter("@EQM_FEE_SPONSOR", EQM_FEE_SPONSOR)

                pParms(64) = New SqlClient.SqlParameter("@EQM_bMEAL", EQM_bMEAL)
                pParms(65) = New SqlClient.SqlParameter("@EQM_APPLEMIRATES_ID", EQM_APPLEMIRATES_ID)
                pParms(66) = New SqlClient.SqlParameter("@EQM_MODE_NOTE", EQM_MODE_NOTE)

                pParms(67) = New SqlClient.SqlParameter("@TEMP_EQM_ENQID", SqlDbType.Int)
                pParms(67).Direction = ParameterDirection.Output
                pParms(68) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(68).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudENQUIRY_M_NEW", pParms)
                Dim ReturnFlag As Integer = pParms(68).Value
                If ReturnFlag = 0 Then
                    TEMP_EQM_ENQID = pParms(67).Value
                End If

                Return ReturnFlag
            Catch ex As Exception
                Return -1
            End Try
        End Using






    End Function
    Public Shared Function SaveStudENQUIRY_PARENT_M_NEW(ByVal EQP_EQM_ENQID As Integer, ByVal EQP_FFIRSTNAME As String, ByVal EQP_FMIDNAME As String, ByVal EQP_FLASTNAME As String, ByVal EQP_FNATIONALITY As String, ByVal EQP_FNATIONALITY2 As String, _
               ByVal EQP_FCOMSTREET As String, ByVal EQP_FCOMAREA As String, ByVal EQP_FCOMBLDG As String, ByVal EQP_FCOMAPARTNO As String, ByVal EQP_FCOMPOBOX As String, ByVal EQP_FCOMCITY As String, _
               ByVal EQP_FCOMSTATE As String, ByVal EQP_FCOMCOUNTRY As String, ByVal EQP_FOFFPHONECODE As String, ByVal EQP_FOFFPHONE As String, _
               ByVal EQP_FRESPHONECODE As String, ByVal EQP_FRESPHONE As String, ByVal EQP_FFAXCODE As String, ByVal EQP_FFAX As String, _
               ByVal EQP_FMOBILECODE As String, ByVal EQP_FMOBILE As String, ByVal EQP_FPRMADDR1 As String, ByVal EQP_FPRMADDR2 As String, ByVal EQP_FPRMPOBOX As String, _
               ByVal EQP_FPRMCITY As String, ByVal EQP_FPRMCOUNTRY As String, ByVal EQP_FPRMPHONE As String, ByVal EQP_FOCC As String, _
               ByVal EQP_FCOMPANY As String, ByVal EQP_FEMAIL As String, ByVal EQP_bFGEMSSTAFF As Boolean, ByVal EQP_BSU_ID_STAFF As String, ByVal EQP_FACD_YEAR As String, _
               ByVal EQP_MFIRSTNAME As String, ByVal EQP_MMIDNAME As String, ByVal EQP_MLASTNAME As String, ByVal EQP_MNATIONALITY As String, _
               ByVal EQP_MNATIONALITY2 As String, ByVal EQP_MCOMSTREET As String, ByVal EQP_MCOMAREA As String, ByVal EQP_MCOMBLDG As String, ByVal EQP_MCOMAPARTNO As String, ByVal EQP_MCOMPOBOX As String, _
               ByVal EQP_MCOMCITY As String, ByVal EQP_MCOMSTATE As String, ByVal EQP_MCOMCOUNTRY As String, ByVal EQP_MOFFPHONECODE As String, _
               ByVal EQP_MOFFPHONE As String, ByVal EQP_MRESPHONECODE As String, ByVal EQP_MRESPHONE As String, _
               ByVal EQP_MFAXCODE As String, ByVal EQP_MFAX As String, ByVal EQP_MMOBILECODE As String, _
               ByVal EQP_MMOBILE As String, ByVal EQP_MPRMADDR1 As String, ByVal EQP_MPRMADDR2 As String, ByVal EQP_MPRMPOBOX As String, _
               ByVal EQP_MPRMCITY As String, ByVal EQP_MPRMCOUNTRY As String, ByVal EQP_MPRMPHONE As String, _
               ByVal EQP_MOCC As String, ByVal EQP_MCOMPANY As String, ByVal EQP_bMGEMSSTAFF As Boolean, _
               ByVal EQP_MBSU_ID As String, ByVal EQP_MACD_YEAR As String, ByVal EQP_GFIRSTNAME As String, ByVal EQP_GMIDNAME As String, _
               ByVal EQP_GLASTNAME As String, ByVal EQP_GNATIONALITY As String, ByVal EQP_GNATIONALITY2 As String, _
               ByVal EQP_GCOMSTREET As String, ByVal EQP_GCOMAREA As String, ByVal EQP_GCOMBLDG As String, ByVal EQP_GCOMAPARTNO As String, ByVal EQP_GCOMPOBOX As String, ByVal EQP_GCOMCITY As String, _
               ByVal EQP_GCOMSTATE As String, ByVal EQP_GCOMCOUNTRY As String, ByVal EQP_GOFFPHONECODE As String, _
               ByVal EQP_GOFFPHONE As String, ByVal EQP_GRESPHONECODE As String, ByVal EQP_GRESPHONE As String, ByVal EQP_GFAXCODE As String, _
               ByVal EQP_GFAX As String, ByVal EQP_GMOBILECODE As String, ByVal EQP_GMOBILE As String, ByVal EQP_GPRMADDR1 As String, _
               ByVal EQP_GPRMADDR2 As String, ByVal EQP_GPRMPOBOX As String, ByVal EQP_GPRMCITY As String, _
               ByVal EQP_GPRMCOUNTRY As String, ByVal EQP_GPRMPHONE As String, _
               ByVal EQP_GOCC As String, ByVal EQP_GCOMPANY As String, ByVal EQP_bGGEMSSTAFF As Boolean, _
               ByVal EQP_GBSU_ID As String, ByVal EQP_GACD_YEAR As String, ByVal EQP_GEMAIL As String, ByVal EQP_MEMAIL As String, ByVal EQP_FCOMP_ID As String, ByVal EQP_MCOMP_ID As String, ByVal EQP_GCOMP_ID As String, ByVal EQP_FCOMPOBOX_EMIR As String, _
               ByVal EQP_MCOMPOBOX_EMIR As String, ByVal EQP_GCOMPOBOX_EMIR As String, ByVal EQP_FMODE_ID As String, ByVal EQP_MMODE_ID As String, ByVal EQP_GMODE_ID As String, ByVal EQP_FAMILY_NOTE As String, _
ByVal EQP_FSALUT As String, ByVal EQP_MSALUT As String, ByVal EQP_GSALUT As String, _
ByVal EQP_FAMILY_CHK As String, ByVal EQP_FAMILY_LEGAL As String, ByVal EQP_FAMILY_LIVING As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try

                Dim pParms(139) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQP_EQM_ENQID", EQP_EQM_ENQID)
                pParms(1) = New SqlClient.SqlParameter("@EQP_FFIRSTNAME", EQP_FFIRSTNAME)
                pParms(2) = New SqlClient.SqlParameter("@EQP_FMIDNAME", EQP_FMIDNAME)
                pParms(3) = New SqlClient.SqlParameter("@EQP_FLASTNAME", EQP_FLASTNAME)
                pParms(4) = New SqlClient.SqlParameter("@EQP_FNATIONALITY", EQP_FNATIONALITY)
                pParms(5) = New SqlClient.SqlParameter("@EQP_FNATIONALITY2", EQP_FNATIONALITY2)

                pParms(6) = New SqlClient.SqlParameter("@EQP_FCOMSTREET", EQP_FCOMSTREET)
                pParms(7) = New SqlClient.SqlParameter("@EQP_FCOMAREA", EQP_FCOMAREA)
                pParms(8) = New SqlClient.SqlParameter("@EQP_FCOMBLDG", EQP_FCOMBLDG)
                pParms(9) = New SqlClient.SqlParameter("@EQP_FCOMAPARTNO", EQP_FCOMAPARTNO)


                pParms(10) = New SqlClient.SqlParameter("@EQP_FCOMPOBOX", EQP_FCOMPOBOX)
                pParms(11) = New SqlClient.SqlParameter("@EQP_FCOMCITY", EQP_FCOMCITY)
                pParms(12) = New SqlClient.SqlParameter("@EQP_FCOMSTATE", EQP_FCOMSTATE)
                pParms(13) = New SqlClient.SqlParameter("@EQP_FCOMCOUNTRY", EQP_FCOMCOUNTRY)
                pParms(14) = New SqlClient.SqlParameter("@EQP_FOFFPHONECODE", EQP_FOFFPHONECODE)
                pParms(15) = New SqlClient.SqlParameter("@EQP_FOFFPHONE", EQP_FOFFPHONE)
                pParms(16) = New SqlClient.SqlParameter("@EQP_FRESPHONECODE", EQP_FRESPHONECODE)
                pParms(17) = New SqlClient.SqlParameter("@EQP_FRESPHONE", EQP_FRESPHONE)
                pParms(18) = New SqlClient.SqlParameter("@EQP_FFAXCODE", EQP_FFAXCODE)
                pParms(19) = New SqlClient.SqlParameter("@EQP_FFAX", EQP_FFAX)
                pParms(20) = New SqlClient.SqlParameter("@EQP_FMOBILECODE", EQP_FMOBILECODE)
                pParms(21) = New SqlClient.SqlParameter("@EQP_FMOBILE", EQP_FMOBILE)
                pParms(22) = New SqlClient.SqlParameter("@EQP_FPRMADDR1", EQP_FPRMADDR1)
                pParms(23) = New SqlClient.SqlParameter("@EQP_FPRMADDR2", EQP_FPRMADDR2)
                pParms(24) = New SqlClient.SqlParameter("@EQP_FPRMPOBOX", EQP_FPRMPOBOX)
                pParms(25) = New SqlClient.SqlParameter("@EQP_FPRMCITY", EQP_FPRMCITY)
                pParms(26) = New SqlClient.SqlParameter("@EQP_FPRMCOUNTRY", EQP_FPRMCOUNTRY)
                pParms(27) = New SqlClient.SqlParameter("@EQP_FPRMPHONE", EQP_FPRMPHONE)
                pParms(28) = New SqlClient.SqlParameter("@EQP_FOCC", EQP_FOCC)
                pParms(29) = New SqlClient.SqlParameter("@EQP_FCOMPANY", EQP_FCOMPANY)
                pParms(30) = New SqlClient.SqlParameter("@EQP_FEMAIL", EQP_FEMAIL)
                pParms(31) = New SqlClient.SqlParameter("@EQP_bFGEMSSTAFF", EQP_bFGEMSSTAFF)

                pParms(32) = New SqlClient.SqlParameter("@EQP_FACD_YEAR", EQP_FACD_YEAR)

                pParms(33) = New SqlClient.SqlParameter("@EQP_BSU_ID_STAFF", EQP_BSU_ID_STAFF)
                pParms(34) = New SqlClient.SqlParameter("@EQP_MFIRSTNAME", EQP_MFIRSTNAME)
                pParms(35) = New SqlClient.SqlParameter("@EQP_MMIDNAME", EQP_MMIDNAME)
                pParms(36) = New SqlClient.SqlParameter("@EQP_MLASTNAME", EQP_MLASTNAME)
                pParms(37) = New SqlClient.SqlParameter("@EQP_MNATIONALITY", EQP_MNATIONALITY)
                pParms(38) = New SqlClient.SqlParameter("@EQP_MNATIONALITY2", EQP_MNATIONALITY2)

                pParms(39) = New SqlClient.SqlParameter("@EQP_MCOMSTREET", EQP_MCOMSTREET)
                pParms(40) = New SqlClient.SqlParameter("@EQP_MCOMAREA", EQP_MCOMAREA)
                pParms(41) = New SqlClient.SqlParameter("@EQP_MCOMBLDG", EQP_MCOMBLDG)
                pParms(42) = New SqlClient.SqlParameter("@EQP_MCOMAPARTNO", EQP_MCOMAPARTNO)


                pParms(43) = New SqlClient.SqlParameter("@EQP_MCOMPOBOX", EQP_MCOMPOBOX)
                pParms(44) = New SqlClient.SqlParameter("@EQP_MCOMCITY", EQP_MCOMCITY)
                pParms(45) = New SqlClient.SqlParameter("@EQP_MCOMSTATE", EQP_MCOMSTATE)
                pParms(46) = New SqlClient.SqlParameter("@EQP_MCOMCOUNTRY", EQP_MCOMCOUNTRY)
                pParms(47) = New SqlClient.SqlParameter("@EQP_MOFFPHONECODE", EQP_MOFFPHONECODE)
                pParms(48) = New SqlClient.SqlParameter("@EQP_MOFFPHONE", EQP_MOFFPHONE)
                pParms(49) = New SqlClient.SqlParameter("@EQP_MRESPHONECODE", EQP_MRESPHONECODE)
                pParms(50) = New SqlClient.SqlParameter("@EQP_MRESPHONE", EQP_MRESPHONE)
                pParms(51) = New SqlClient.SqlParameter("@EQP_MFAXCODE", EQP_MFAXCODE)
                pParms(52) = New SqlClient.SqlParameter("@EQP_MFAX", EQP_MFAX)
                pParms(53) = New SqlClient.SqlParameter("@EQP_MMOBILECODE", EQP_MMOBILECODE)
                pParms(54) = New SqlClient.SqlParameter("@EQP_MMOBILE", EQP_MMOBILE)
                pParms(55) = New SqlClient.SqlParameter("@EQP_MPRMADDR1", EQP_MPRMADDR1)
                pParms(56) = New SqlClient.SqlParameter("@EQP_MPRMADDR2", EQP_MPRMADDR2)
                pParms(57) = New SqlClient.SqlParameter("@EQP_MPRMPOBOX", EQP_MPRMPOBOX)
                pParms(58) = New SqlClient.SqlParameter("@EQP_MPRMCITY", EQP_MPRMCITY)
                pParms(59) = New SqlClient.SqlParameter("@EQP_MPRMCOUNTRY", EQP_MPRMCOUNTRY)
                pParms(60) = New SqlClient.SqlParameter("@EQP_MPRMPHONE", EQP_MPRMPHONE)
                pParms(61) = New SqlClient.SqlParameter("@EQP_MOCC", EQP_MOCC)
                pParms(62) = New SqlClient.SqlParameter("@EQP_MCOMPANY", EQP_MCOMPANY)
                pParms(63) = New SqlClient.SqlParameter("@EQP_bMGEMSSTAFF", EQP_bMGEMSSTAFF)
                pParms(64) = New SqlClient.SqlParameter("@EQP_MBSU_ID", EQP_MBSU_ID)

                pParms(65) = New SqlClient.SqlParameter("@EQP_MACD_YEAR", EQP_MACD_YEAR)


                pParms(66) = New SqlClient.SqlParameter("@EQP_GFIRSTNAME", EQP_GFIRSTNAME)
                pParms(67) = New SqlClient.SqlParameter("@EQP_GMIDNAME", EQP_GMIDNAME)
                pParms(68) = New SqlClient.SqlParameter("@EQP_GLASTNAME", EQP_GLASTNAME)
                pParms(69) = New SqlClient.SqlParameter("@EQP_GNATIONALITY", EQP_GNATIONALITY)
                pParms(70) = New SqlClient.SqlParameter("@EQP_GNATIONALITY2", EQP_GNATIONALITY2)

                pParms(71) = New SqlClient.SqlParameter("@EQP_GCOMSTREET", EQP_GCOMSTREET)
                pParms(72) = New SqlClient.SqlParameter("@EQP_GCOMAREA", EQP_GCOMAREA)
                pParms(73) = New SqlClient.SqlParameter("@EQP_GCOMBLDG", EQP_GCOMBLDG)
                pParms(74) = New SqlClient.SqlParameter("@EQP_GCOMAPARTNO", EQP_GCOMAPARTNO)




                pParms(75) = New SqlClient.SqlParameter("@EQP_GCOMPOBOX", EQP_GCOMPOBOX)
                pParms(76) = New SqlClient.SqlParameter("@EQP_GCOMCITY", EQP_GCOMCITY)
                pParms(77) = New SqlClient.SqlParameter("@EQP_GCOMSTATE", EQP_GCOMSTATE)
                pParms(78) = New SqlClient.SqlParameter("@EQP_GCOMCOUNTRY", EQP_GCOMCOUNTRY)
                pParms(79) = New SqlClient.SqlParameter("@EQP_GOFFPHONECODE", EQP_GOFFPHONECODE)
                pParms(80) = New SqlClient.SqlParameter("@EQP_GOFFPHONE", EQP_GOFFPHONE)
                pParms(81) = New SqlClient.SqlParameter("@EQP_GRESPHONECODE", EQP_GRESPHONECODE)
                pParms(82) = New SqlClient.SqlParameter("@EQP_GRESPHONE", EQP_GRESPHONE)
                pParms(83) = New SqlClient.SqlParameter("@EQP_GFAXCODE", EQP_GFAXCODE)
                pParms(84) = New SqlClient.SqlParameter("@EQP_GFAX", EQP_GFAX)
                pParms(85) = New SqlClient.SqlParameter("@EQP_GMOBILECODE", EQP_GMOBILECODE)
                pParms(86) = New SqlClient.SqlParameter("@EQP_GMOBILE", EQP_GMOBILE)
                pParms(87) = New SqlClient.SqlParameter("@EQP_GPRMADDR1", EQP_GPRMADDR1)
                pParms(88) = New SqlClient.SqlParameter("@EQP_GPRMADDR2", EQP_GPRMADDR2)
                pParms(89) = New SqlClient.SqlParameter("@EQP_GPRMPOBOX", EQP_GPRMPOBOX)
                pParms(90) = New SqlClient.SqlParameter("@EQP_GPRMCITY", EQP_GPRMCITY)
                pParms(91) = New SqlClient.SqlParameter("@EQP_GPRMCOUNTRY", EQP_GPRMCOUNTRY)
                pParms(92) = New SqlClient.SqlParameter("@EQP_GPRMPHONE", EQP_GPRMPHONE)
                pParms(93) = New SqlClient.SqlParameter("@EQP_GOCC", EQP_GOCC)
                pParms(94) = New SqlClient.SqlParameter("@EQP_GCOMPANY", EQP_GCOMPANY)
                pParms(95) = New SqlClient.SqlParameter("@EQP_bGGEMSSTAFF", EQP_bGGEMSSTAFF)
                pParms(96) = New SqlClient.SqlParameter("@EQP_GBSU_ID", EQP_GBSU_ID)

                pParms(97) = New SqlClient.SqlParameter("@EQP_GACD_YEAR", EQP_GACD_YEAR)

                pParms(98) = New SqlClient.SqlParameter("@EQP_MEMAIL", EQP_MEMAIL)
                pParms(99) = New SqlClient.SqlParameter("@EQP_GEMAIL", EQP_GEMAIL)

                pParms(100) = New SqlClient.SqlParameter("@EQP_FCOMP_ID", EQP_FCOMP_ID)
                pParms(101) = New SqlClient.SqlParameter("@EQP_MCOMP_ID", EQP_MCOMP_ID)
                pParms(102) = New SqlClient.SqlParameter("@EQP_GCOMP_ID", EQP_GCOMP_ID)

                pParms(103) = New SqlClient.SqlParameter("@EQP_FCOMPOBOX_EMIR", EQP_FCOMPOBOX_EMIR)
                pParms(104) = New SqlClient.SqlParameter("@EQP_MCOMPOBOX_EMIR", EQP_MCOMPOBOX_EMIR)
                pParms(105) = New SqlClient.SqlParameter("@EQP_GCOMPOBOX_EMIR", EQP_GCOMPOBOX_EMIR)
                pParms(106) = New SqlClient.SqlParameter("@EQP_FMODE_ID", EQP_FMODE_ID)
                pParms(107) = New SqlClient.SqlParameter("@EQP_MMODE_ID", EQP_MMODE_ID)
                pParms(108) = New SqlClient.SqlParameter("@EQP_GMODE_ID", EQP_GMODE_ID)
                pParms(109) = New SqlClient.SqlParameter("@EQP_FAMILY_NOTE", EQP_FAMILY_NOTE)

                pParms(110) = New SqlClient.SqlParameter("@EQP_FSALUT", EQP_FSALUT)
                pParms(111) = New SqlClient.SqlParameter("@EQP_MSALUT", EQP_MSALUT)
                pParms(112) = New SqlClient.SqlParameter("@EQP_GSALUT", EQP_GSALUT)

                pParms(113) = New SqlClient.SqlParameter("@EQP_FAMILY_CHK", EQP_FAMILY_CHK)
                pParms(114) = New SqlClient.SqlParameter("@EQP_FAMILY_LEGAL", EQP_FAMILY_LEGAL)
                pParms(115) = New SqlClient.SqlParameter("@EQP_FAMILY_LIVING", EQP_FAMILY_LIVING)



                pParms(116) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(116).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudENQUIRY_PARENT_M_NEW", pParms)
                Dim ReturnFlag As Integer = pParms(116).Value
                Return ReturnFlag
            Catch ex As Exception
                Return -1
            End Try
        End Using
    End Function


    Public Shared Function SaveStudENQUIRY_PARENT_M(ByVal EQP_EQM_ENQID As Integer, ByVal EQP_FFIRSTNAME As String, ByVal EQP_FMIDNAME As String, ByVal EQP_FLASTNAME As String, ByVal EQP_FNATIONALITY As String, ByVal EQP_FNATIONALITY2 As String, _
               ByVal EQP_FCOMSTREET As String, ByVal EQP_FCOMAREA As String, ByVal EQP_FCOMBLDG As String, ByVal EQP_FCOMAPARTNO As String, ByVal EQP_FCOMPOBOX As String, ByVal EQP_FCOMCITY As String, _
               ByVal EQP_FCOMSTATE As String, ByVal EQP_FCOMCOUNTRY As String, ByVal EQP_FOFFPHONECODE As String, ByVal EQP_FOFFPHONE As String, _
               ByVal EQP_FRESPHONECODE As String, ByVal EQP_FRESPHONE As String, ByVal EQP_FFAXCODE As String, ByVal EQP_FFAX As String, _
               ByVal EQP_FMOBILECODE As String, ByVal EQP_FMOBILE As String, ByVal EQP_FPRMADDR1 As String, ByVal EQP_FPRMADDR2 As String, ByVal EQP_FPRMPOBOX As String, _
               ByVal EQP_FPRMCITY As String, ByVal EQP_FPRMCOUNTRY As String, ByVal EQP_FPRMPHONE As String, ByVal EQP_FOCC As String, _
               ByVal EQP_FCOMPANY As String, ByVal EQP_FEMAIL As String, ByVal EQP_bFGEMSSTAFF As Boolean, ByVal EQP_BSU_ID_STAFF As String, ByVal EQP_FACD_YEAR As String, _
               ByVal EQP_MFIRSTNAME As String, ByVal EQP_MMIDNAME As String, ByVal EQP_MLASTNAME As String, ByVal EQP_MNATIONALITY As String, _
               ByVal EQP_MNATIONALITY2 As String, ByVal EQP_MCOMSTREET As String, ByVal EQP_MCOMAREA As String, ByVal EQP_MCOMBLDG As String, ByVal EQP_MCOMAPARTNO As String, ByVal EQP_MCOMPOBOX As String, _
               ByVal EQP_MCOMCITY As String, ByVal EQP_MCOMSTATE As String, ByVal EQP_MCOMCOUNTRY As String, ByVal EQP_MOFFPHONECODE As String, _
               ByVal EQP_MOFFPHONE As String, ByVal EQP_MRESPHONECODE As String, ByVal EQP_MRESPHONE As String, _
               ByVal EQP_MFAXCODE As String, ByVal EQP_MFAX As String, ByVal EQP_MMOBILECODE As String, _
               ByVal EQP_MMOBILE As String, ByVal EQP_MPRMADDR1 As String, ByVal EQP_MPRMADDR2 As String, ByVal EQP_MPRMPOBOX As String, _
               ByVal EQP_MPRMCITY As String, ByVal EQP_MPRMCOUNTRY As String, ByVal EQP_MPRMPHONE As String, _
               ByVal EQP_MOCC As String, ByVal EQP_MCOMPANY As String, ByVal EQP_bMGEMSSTAFF As Boolean, _
               ByVal EQP_MBSU_ID As String, ByVal EQP_MACD_YEAR As String, ByVal EQP_GFIRSTNAME As String, ByVal EQP_GMIDNAME As String, _
               ByVal EQP_GLASTNAME As String, ByVal EQP_GNATIONALITY As String, ByVal EQP_GNATIONALITY2 As String, _
               ByVal EQP_GCOMSTREET As String, ByVal EQP_GCOMAREA As String, ByVal EQP_GCOMBLDG As String, ByVal EQP_GCOMAPARTNO As String, ByVal EQP_GCOMPOBOX As String, ByVal EQP_GCOMCITY As String, _
               ByVal EQP_GCOMSTATE As String, ByVal EQP_GCOMCOUNTRY As String, ByVal EQP_GOFFPHONECODE As String, _
               ByVal EQP_GOFFPHONE As String, ByVal EQP_GRESPHONECODE As String, ByVal EQP_GRESPHONE As String, ByVal EQP_GFAXCODE As String, _
               ByVal EQP_GFAX As String, ByVal EQP_GMOBILECODE As String, ByVal EQP_GMOBILE As String, ByVal EQP_GPRMADDR1 As String, _
               ByVal EQP_GPRMADDR2 As String, ByVal EQP_GPRMPOBOX As String, ByVal EQP_GPRMCITY As String, _
               ByVal EQP_GPRMCOUNTRY As String, ByVal EQP_GPRMPHONE As String, _
               ByVal EQP_GOCC As String, ByVal EQP_GCOMPANY As String, ByVal EQP_bGGEMSSTAFF As Boolean, _
               ByVal EQP_GBSU_ID As String, ByVal EQP_GACD_YEAR As String, ByVal EQP_GEMAIL As String, ByVal EQP_MEMAIL As String, ByVal EQP_FCOMP_ID As String, ByVal EQP_MCOMP_ID As String, ByVal EQP_GCOMP_ID As String, ByVal EQP_FCOMPOBOX_EMIR As String, _
               ByVal EQP_MCOMPOBOX_EMIR As String, ByVal EQP_GCOMPOBOX_EMIR As String, ByVal EQP_FMODE_ID As String, ByVal EQP_MMODE_ID As String, ByVal EQP_GMODE_ID As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try

                Dim pParms(109) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQP_EQM_ENQID", EQP_EQM_ENQID)
                pParms(1) = New SqlClient.SqlParameter("@EQP_FFIRSTNAME", EQP_FFIRSTNAME)
                pParms(2) = New SqlClient.SqlParameter("@EQP_FMIDNAME", EQP_FMIDNAME)
                pParms(3) = New SqlClient.SqlParameter("@EQP_FLASTNAME", EQP_FLASTNAME)
                pParms(4) = New SqlClient.SqlParameter("@EQP_FNATIONALITY", EQP_FNATIONALITY)
                pParms(5) = New SqlClient.SqlParameter("@EQP_FNATIONALITY2", EQP_FNATIONALITY2)

                pParms(6) = New SqlClient.SqlParameter("@EQP_FCOMSTREET", EQP_FCOMSTREET)
                pParms(7) = New SqlClient.SqlParameter("@EQP_FCOMAREA", EQP_FCOMAREA)
                pParms(8) = New SqlClient.SqlParameter("@EQP_FCOMBLDG", EQP_FCOMBLDG)
                pParms(9) = New SqlClient.SqlParameter("@EQP_FCOMAPARTNO", EQP_FCOMAPARTNO)


                pParms(10) = New SqlClient.SqlParameter("@EQP_FCOMPOBOX", EQP_FCOMPOBOX)
                pParms(11) = New SqlClient.SqlParameter("@EQP_FCOMCITY", EQP_FCOMCITY)
                pParms(12) = New SqlClient.SqlParameter("@EQP_FCOMSTATE", EQP_FCOMSTATE)
                pParms(13) = New SqlClient.SqlParameter("@EQP_FCOMCOUNTRY", EQP_FCOMCOUNTRY)
                pParms(14) = New SqlClient.SqlParameter("@EQP_FOFFPHONECODE", EQP_FOFFPHONECODE)
                pParms(15) = New SqlClient.SqlParameter("@EQP_FOFFPHONE", EQP_FOFFPHONE)
                pParms(16) = New SqlClient.SqlParameter("@EQP_FRESPHONECODE", EQP_FRESPHONECODE)
                pParms(17) = New SqlClient.SqlParameter("@EQP_FRESPHONE", EQP_FRESPHONE)
                pParms(18) = New SqlClient.SqlParameter("@EQP_FFAXCODE", EQP_FFAXCODE)
                pParms(19) = New SqlClient.SqlParameter("@EQP_FFAX", EQP_FFAX)
                pParms(20) = New SqlClient.SqlParameter("@EQP_FMOBILECODE", EQP_FMOBILECODE)
                pParms(21) = New SqlClient.SqlParameter("@EQP_FMOBILE", EQP_FMOBILE)
                pParms(22) = New SqlClient.SqlParameter("@EQP_FPRMADDR1", EQP_FPRMADDR1)
                pParms(23) = New SqlClient.SqlParameter("@EQP_FPRMADDR2", EQP_FPRMADDR2)
                pParms(24) = New SqlClient.SqlParameter("@EQP_FPRMPOBOX", EQP_FPRMPOBOX)
                pParms(25) = New SqlClient.SqlParameter("@EQP_FPRMCITY", EQP_FPRMCITY)
                pParms(26) = New SqlClient.SqlParameter("@EQP_FPRMCOUNTRY", EQP_FPRMCOUNTRY)
                pParms(27) = New SqlClient.SqlParameter("@EQP_FPRMPHONE", EQP_FPRMPHONE)
                pParms(28) = New SqlClient.SqlParameter("@EQP_FOCC", EQP_FOCC)
                pParms(29) = New SqlClient.SqlParameter("@EQP_FCOMPANY", EQP_FCOMPANY)
                pParms(30) = New SqlClient.SqlParameter("@EQP_FEMAIL", EQP_FEMAIL)
                pParms(31) = New SqlClient.SqlParameter("@EQP_bFGEMSSTAFF", EQP_bFGEMSSTAFF)

                pParms(32) = New SqlClient.SqlParameter("@EQP_FACD_YEAR", EQP_FACD_YEAR)

                pParms(33) = New SqlClient.SqlParameter("@EQP_BSU_ID_STAFF", EQP_BSU_ID_STAFF)
                pParms(34) = New SqlClient.SqlParameter("@EQP_MFIRSTNAME", EQP_MFIRSTNAME)
                pParms(35) = New SqlClient.SqlParameter("@EQP_MMIDNAME", EQP_MMIDNAME)
                pParms(36) = New SqlClient.SqlParameter("@EQP_MLASTNAME", EQP_MLASTNAME)
                pParms(37) = New SqlClient.SqlParameter("@EQP_MNATIONALITY", EQP_MNATIONALITY)
                pParms(38) = New SqlClient.SqlParameter("@EQP_MNATIONALITY2", EQP_MNATIONALITY2)

                pParms(39) = New SqlClient.SqlParameter("@EQP_MCOMSTREET", EQP_MCOMSTREET)
                pParms(40) = New SqlClient.SqlParameter("@EQP_MCOMAREA", EQP_MCOMAREA)
                pParms(41) = New SqlClient.SqlParameter("@EQP_MCOMBLDG", EQP_MCOMBLDG)
                pParms(42) = New SqlClient.SqlParameter("@EQP_MCOMAPARTNO", EQP_MCOMAPARTNO)


                pParms(43) = New SqlClient.SqlParameter("@EQP_MCOMPOBOX", EQP_MCOMPOBOX)
                pParms(44) = New SqlClient.SqlParameter("@EQP_MCOMCITY", EQP_MCOMCITY)
                pParms(45) = New SqlClient.SqlParameter("@EQP_MCOMSTATE", EQP_MCOMSTATE)
                pParms(46) = New SqlClient.SqlParameter("@EQP_MCOMCOUNTRY", EQP_MCOMCOUNTRY)
                pParms(47) = New SqlClient.SqlParameter("@EQP_MOFFPHONECODE", EQP_MOFFPHONECODE)
                pParms(48) = New SqlClient.SqlParameter("@EQP_MOFFPHONE", EQP_MOFFPHONE)
                pParms(49) = New SqlClient.SqlParameter("@EQP_MRESPHONECODE", EQP_MRESPHONECODE)
                pParms(50) = New SqlClient.SqlParameter("@EQP_MRESPHONE", EQP_MRESPHONE)
                pParms(51) = New SqlClient.SqlParameter("@EQP_MFAXCODE", EQP_MFAXCODE)
                pParms(52) = New SqlClient.SqlParameter("@EQP_MFAX", EQP_MFAX)
                pParms(53) = New SqlClient.SqlParameter("@EQP_MMOBILECODE", EQP_MMOBILECODE)
                pParms(54) = New SqlClient.SqlParameter("@EQP_MMOBILE", EQP_MMOBILE)
                pParms(55) = New SqlClient.SqlParameter("@EQP_MPRMADDR1", EQP_MPRMADDR1)
                pParms(56) = New SqlClient.SqlParameter("@EQP_MPRMADDR2", EQP_MPRMADDR2)
                pParms(57) = New SqlClient.SqlParameter("@EQP_MPRMPOBOX", EQP_MPRMPOBOX)
                pParms(58) = New SqlClient.SqlParameter("@EQP_MPRMCITY", EQP_MPRMCITY)
                pParms(59) = New SqlClient.SqlParameter("@EQP_MPRMCOUNTRY", EQP_MPRMCOUNTRY)
                pParms(60) = New SqlClient.SqlParameter("@EQP_MPRMPHONE", EQP_MPRMPHONE)
                pParms(61) = New SqlClient.SqlParameter("@EQP_MOCC", EQP_MOCC)
                pParms(62) = New SqlClient.SqlParameter("@EQP_MCOMPANY", EQP_MCOMPANY)
                pParms(63) = New SqlClient.SqlParameter("@EQP_bMGEMSSTAFF", EQP_bMGEMSSTAFF)
                pParms(64) = New SqlClient.SqlParameter("@EQP_MBSU_ID", EQP_MBSU_ID)

                pParms(65) = New SqlClient.SqlParameter("@EQP_MACD_YEAR", EQP_MACD_YEAR)


                pParms(66) = New SqlClient.SqlParameter("@EQP_GFIRSTNAME", EQP_GFIRSTNAME)
                pParms(67) = New SqlClient.SqlParameter("@EQP_GMIDNAME", EQP_GMIDNAME)
                pParms(68) = New SqlClient.SqlParameter("@EQP_GLASTNAME", EQP_GLASTNAME)
                pParms(69) = New SqlClient.SqlParameter("@EQP_GNATIONALITY", EQP_GNATIONALITY)
                pParms(70) = New SqlClient.SqlParameter("@EQP_GNATIONALITY2", EQP_GNATIONALITY2)

                pParms(71) = New SqlClient.SqlParameter("@EQP_GCOMSTREET", EQP_GCOMSTREET)
                pParms(72) = New SqlClient.SqlParameter("@EQP_GCOMAREA", EQP_GCOMAREA)
                pParms(73) = New SqlClient.SqlParameter("@EQP_GCOMBLDG", EQP_GCOMBLDG)
                pParms(74) = New SqlClient.SqlParameter("@EQP_GCOMAPARTNO", EQP_GCOMAPARTNO)




                pParms(75) = New SqlClient.SqlParameter("@EQP_GCOMPOBOX", EQP_GCOMPOBOX)
                pParms(76) = New SqlClient.SqlParameter("@EQP_GCOMCITY", EQP_GCOMCITY)
                pParms(77) = New SqlClient.SqlParameter("@EQP_GCOMSTATE", EQP_GCOMSTATE)
                pParms(78) = New SqlClient.SqlParameter("@EQP_GCOMCOUNTRY", EQP_GCOMCOUNTRY)
                pParms(79) = New SqlClient.SqlParameter("@EQP_GOFFPHONECODE", EQP_GOFFPHONECODE)
                pParms(80) = New SqlClient.SqlParameter("@EQP_GOFFPHONE", EQP_GOFFPHONE)
                pParms(81) = New SqlClient.SqlParameter("@EQP_GRESPHONECODE", EQP_GRESPHONECODE)
                pParms(82) = New SqlClient.SqlParameter("@EQP_GRESPHONE", EQP_GRESPHONE)
                pParms(83) = New SqlClient.SqlParameter("@EQP_GFAXCODE", EQP_GFAXCODE)
                pParms(84) = New SqlClient.SqlParameter("@EQP_GFAX", EQP_GFAX)
                pParms(85) = New SqlClient.SqlParameter("@EQP_GMOBILECODE", EQP_GMOBILECODE)
                pParms(86) = New SqlClient.SqlParameter("@EQP_GMOBILE", EQP_GMOBILE)
                pParms(87) = New SqlClient.SqlParameter("@EQP_GPRMADDR1", EQP_GPRMADDR1)
                pParms(88) = New SqlClient.SqlParameter("@EQP_GPRMADDR2", EQP_GPRMADDR2)
                pParms(89) = New SqlClient.SqlParameter("@EQP_GPRMPOBOX", EQP_GPRMPOBOX)
                pParms(90) = New SqlClient.SqlParameter("@EQP_GPRMCITY", EQP_GPRMCITY)
                pParms(91) = New SqlClient.SqlParameter("@EQP_GPRMCOUNTRY", EQP_GPRMCOUNTRY)
                pParms(92) = New SqlClient.SqlParameter("@EQP_GPRMPHONE", EQP_GPRMPHONE)
                pParms(93) = New SqlClient.SqlParameter("@EQP_GOCC", EQP_GOCC)
                pParms(94) = New SqlClient.SqlParameter("@EQP_GCOMPANY", EQP_GCOMPANY)
                pParms(95) = New SqlClient.SqlParameter("@EQP_bGGEMSSTAFF", EQP_bGGEMSSTAFF)
                pParms(96) = New SqlClient.SqlParameter("@EQP_GBSU_ID", EQP_GBSU_ID)

                pParms(97) = New SqlClient.SqlParameter("@EQP_GACD_YEAR", EQP_GACD_YEAR)

                pParms(98) = New SqlClient.SqlParameter("@EQP_MEMAIL", EQP_MEMAIL)
                pParms(99) = New SqlClient.SqlParameter("@EQP_GEMAIL", EQP_GEMAIL)

                pParms(100) = New SqlClient.SqlParameter("@EQP_FCOMP_ID", EQP_FCOMP_ID)
                pParms(101) = New SqlClient.SqlParameter("@EQP_MCOMP_ID", EQP_MCOMP_ID)
                pParms(102) = New SqlClient.SqlParameter("@EQP_GCOMP_ID", EQP_GCOMP_ID)

                pParms(103) = New SqlClient.SqlParameter("@EQP_FCOMPOBOX_EMIR", EQP_FCOMPOBOX_EMIR)
                pParms(104) = New SqlClient.SqlParameter("@EQP_MCOMPOBOX_EMIR", EQP_MCOMPOBOX_EMIR)
                pParms(105) = New SqlClient.SqlParameter("@EQP_GCOMPOBOX_EMIR", EQP_GCOMPOBOX_EMIR)
                pParms(106) = New SqlClient.SqlParameter("@EQP_FMODE_ID", EQP_FMODE_ID)
                pParms(107) = New SqlClient.SqlParameter("@EQP_MMODE_ID", EQP_MMODE_ID)
                pParms(108) = New SqlClient.SqlParameter("@EQP_GMODE_ID", EQP_GMODE_ID)

                pParms(109) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(109).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudENQUIRY_PARENT_M", pParms)
                Dim ReturnFlag As Integer = pParms(109).Value
                Return ReturnFlag
            Catch ex As Exception
                Return -1
            End Try
        End Using
    End Function

    '    Public Shared Function SaveStudENQUIRY_SCHOOLPRIO_S(ByVal EQS_EQM_ENQID As String, ByVal EQS_ACY_ID As String, _
    '      ByVal EQS_BSU_ID As String, ByVal EQS_GRD_ID As String, ByVal EQS_SHF_ID As String, _
    '      ByVal EQS_bTPTREQD As Boolean, ByVal EQS_LOC_ID As String, ByVal EQS_SBL_ID As String, ByVal EQS_PNT_ID As String, _
    'ByVal EQS_STM_ID As String, ByVal EQS_bAPPLSIBLING As Boolean, ByVal EQS_SIBLINGFEEID As String, ByVal EQS_SIBLINGSCHOOL As String, _
    'ByVal EQS_TPTREMARKS As String, ByVal EQS_STATUS As String, ByRef TEMP_ApplNo As String, ByVal trans As SqlTransaction) As Integer
    '        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
    '            Try


    '                Dim pParms(16) As SqlClient.SqlParameter
    '                pParms(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", EQS_EQM_ENQID)
    '                pParms(1) = New SqlClient.SqlParameter("@EQS_ACY_ID", EQS_ACY_ID)
    '                pParms(2) = New SqlClient.SqlParameter("@EQS_BSU_ID", EQS_BSU_ID)
    '                pParms(3) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
    '                pParms(4) = New SqlClient.SqlParameter("@EQS_SHF_ID", EQS_SHF_ID)
    '                pParms(5) = New SqlClient.SqlParameter("@EQS_bTPTREQD", EQS_bTPTREQD)
    '                pParms(6) = New SqlClient.SqlParameter("@EQS_LOC_ID", EQS_LOC_ID)
    '                pParms(7) = New SqlClient.SqlParameter("@EQS_SBL_ID", EQS_SBL_ID)
    '                pParms(8) = New SqlClient.SqlParameter("@EQS_PNT_ID", EQS_PNT_ID)
    '                pParms(9) = New SqlClient.SqlParameter("@EQS_STM_ID", EQS_STM_ID)
    '                pParms(10) = New SqlClient.SqlParameter("@EQS_bAPPLSIBLING", EQS_bAPPLSIBLING)
    '                pParms(11) = New SqlClient.SqlParameter("@EQS_SIBLINGFEEID", EQS_SIBLINGFEEID)
    '                pParms(12) = New SqlClient.SqlParameter("@EQS_SIBLINGSCHOOL", EQS_SIBLINGSCHOOL)
    '                pParms(13) = New SqlClient.SqlParameter("@EQS_TPTREMARKS", EQS_TPTREMARKS)
    '                pParms(14) = New SqlClient.SqlParameter("@EQS_STATUS", EQS_STATUS)
    '                pParms(15) = New SqlClient.SqlParameter("@TEMP_ApplNo", SqlDbType.VarChar, 20)
    '                pParms(15).Direction = ParameterDirection.Output
    '                pParms(16) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '                pParms(16).Direction = ParameterDirection.ReturnValue
    '                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudENQUIRY_SCHOOLPRIO_S", pParms)
    '                Dim ReturnFlag As Integer = pParms(16).Value

    '                If ReturnFlag = 0 Then
    '                    TEMP_ApplNo = pParms(15).Value
    '                End If
    '                Return ReturnFlag
    '            Catch ex As Exception
    '                Return -1
    '            End Try
    '        End Using



    '    End Function
    Public Shared Function SaveStudENQUIRY_SCHOOLPRIO_S_NEW(ByVal EQS_EQM_ENQID As String, ByVal EQS_ACY_ID As String, _
       ByVal EQS_BSU_ID As String, ByVal EQS_GRD_ID As String, ByVal EQS_SHF_ID As String, _
       ByVal EQS_bTPTREQD As Boolean, ByVal EQS_LOC_ID As String, ByVal EQS_SBL_ID As String, ByVal EQS_PNT_ID As String, _
 ByVal EQS_STM_ID As String, ByVal EQS_bAPPLSIBLING As Boolean, ByVal EQS_SIBLINGFEEID As String, ByVal EQS_SIBLINGSCHOOL As String, _
 ByVal EQS_TPTREMARKS As String, ByVal EQS_STATUS As String, _
ByVal EQM_REMARKS As String, ByVal EQS_CLM_ID As Integer, ByVal EQS_DOJ As String, ByVal EQS_RFS_ID As String, _
ByVal EQS_bALLERGIES As Boolean, ByVal EQS_ALLERGIES As String, ByVal EQS_bRCVSPMEDICATION As Boolean, ByVal EQS_SPMEDICN As String, _
 ByVal EQS_bPRESTRICTIONS As Boolean, ByVal EQS_PRESTRICTIONS As String, ByVal EQS_bHRESTRICTIONS As Boolean, _
 ByVal EQS_HRESTRICTIONS As String, ByVal EQS_bTHERAPHY As Boolean, ByVal EQS_THERAPHY As String, ByVal EQS_bSPEDUCATION As Boolean, _
 ByVal EQS_SPEDUCATION As String, ByVal EQS_bEAL As Boolean, ByVal EQS_EAL As String, ByVal EQS_bMUSICAL As Boolean, _
ByVal EQS_MUSICAL As String, ByVal EQS_bENRICH As Boolean, ByVal EQS_ENRICH As String, ByVal EQS_bBEHAVIOUR As Boolean, _
ByVal EQS_BEHAVIOUR As String, ByVal EQS_bSPORTS As Boolean, ByVal EQS_SPORTS As String, ByVal EQS_ETHNICITY As String, _
 ByVal EQS_ETHNICITY_OTH As String, ByVal EQS_ENG_READING As String, ByVal EQS_ENG_WRITING As String, ByVal EQS_ENG_SPEAKING As String, _
ByRef TEMP_ApplNo As String, ByVal EQS_bREP_GRD As Boolean, ByVal EQS_REP_GRD As String, _
ByVal EQS_bCommInt As Boolean, ByVal EQS_CommInt As String, ByVal EQS_bDisabled As Boolean, ByVal EQS_Disabled As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(80) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", EQS_EQM_ENQID)
                pParms(1) = New SqlClient.SqlParameter("@EQS_ACY_ID", EQS_ACY_ID)
                pParms(2) = New SqlClient.SqlParameter("@EQS_BSU_ID", EQS_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@EQS_SHF_ID", EQS_SHF_ID)
                pParms(5) = New SqlClient.SqlParameter("@EQS_bTPTREQD", EQS_bTPTREQD)
                pParms(6) = New SqlClient.SqlParameter("@EQS_LOC_ID", EQS_LOC_ID)
                pParms(7) = New SqlClient.SqlParameter("@EQS_SBL_ID", EQS_SBL_ID)
                pParms(8) = New SqlClient.SqlParameter("@EQS_PNT_ID", EQS_PNT_ID)
                pParms(9) = New SqlClient.SqlParameter("@EQS_STM_ID", EQS_STM_ID)
                pParms(10) = New SqlClient.SqlParameter("@EQS_bAPPLSIBLING", EQS_bAPPLSIBLING)
                pParms(11) = New SqlClient.SqlParameter("@EQS_SIBLINGFEEID", EQS_SIBLINGFEEID)
                pParms(12) = New SqlClient.SqlParameter("@EQS_SIBLINGSCHOOL", EQS_SIBLINGSCHOOL)
                pParms(13) = New SqlClient.SqlParameter("@EQS_TPTREMARKS", EQS_TPTREMARKS)
                pParms(14) = New SqlClient.SqlParameter("@EQS_STATUS", EQS_STATUS)
                pParms(15) = New SqlClient.SqlParameter("@EQS_CLM_ID", EQS_CLM_ID)
                pParms(16) = New SqlClient.SqlParameter("@EQM_REMARKS", EQM_REMARKS)
                pParms(17) = New SqlClient.SqlParameter("@EQS_DOJ", EQS_DOJ)
                pParms(18) = New SqlClient.SqlParameter("@EQS_RFS_ID", EQS_RFS_ID)
                pParms(19) = New SqlClient.SqlParameter("@EQS_SIB_APPL", HttpContext.Current.Session("SIB_APPLICANT"))
                pParms(20) = New SqlClient.SqlParameter("@EQS_bALLERGIES", EQS_bALLERGIES)
                pParms(21) = New SqlClient.SqlParameter("@EQS_ALLERGIES", EQS_ALLERGIES)
                pParms(22) = New SqlClient.SqlParameter("@EQS_bRCVSPMEDICATION", EQS_bRCVSPMEDICATION)
                pParms(23) = New SqlClient.SqlParameter("@EQS_SPMEDICN", EQS_SPMEDICN)
                pParms(24) = New SqlClient.SqlParameter("@EQS_bPRESTRICTIONS", EQS_bPRESTRICTIONS)
                pParms(25) = New SqlClient.SqlParameter("@EQS_PRESTRICTIONS", EQS_PRESTRICTIONS)
                pParms(26) = New SqlClient.SqlParameter("@EQS_bHRESTRICTIONS", EQS_bHRESTRICTIONS)
                pParms(27) = New SqlClient.SqlParameter("@EQS_HRESTRICTIONS", EQS_HRESTRICTIONS)
                pParms(28) = New SqlClient.SqlParameter("@EQS_bTHERAPHY", EQS_bTHERAPHY)
                pParms(29) = New SqlClient.SqlParameter("@EQS_THERAPHY", EQS_THERAPHY)
                pParms(30) = New SqlClient.SqlParameter("@EQS_bSPEDUCATION", EQS_bSPEDUCATION)
                pParms(31) = New SqlClient.SqlParameter("@EQS_SPEDUCATION", EQS_SPEDUCATION)
                pParms(32) = New SqlClient.SqlParameter("@EQS_bEAL", EQS_bEAL)
                pParms(33) = New SqlClient.SqlParameter("@EQS_EAL", EQS_EAL)
                pParms(34) = New SqlClient.SqlParameter("@EQS_bMUSICAL", EQS_bMUSICAL)
                pParms(35) = New SqlClient.SqlParameter("@EQS_MUSICAL", EQS_MUSICAL)
                pParms(36) = New SqlClient.SqlParameter("@EQS_bENRICH", EQS_bENRICH)
                pParms(37) = New SqlClient.SqlParameter("@EQS_ENRICH", EQS_ENRICH)
                pParms(38) = New SqlClient.SqlParameter("@EQS_bBEHAVIOUR", EQS_bBEHAVIOUR)
                pParms(39) = New SqlClient.SqlParameter("@EQS_BEHAVIOUR", EQS_BEHAVIOUR)
                pParms(40) = New SqlClient.SqlParameter("@EQS_bSPORTS", EQS_bSPORTS)
                pParms(41) = New SqlClient.SqlParameter("@EQS_SPORTS", EQS_SPORTS)
                pParms(42) = New SqlClient.SqlParameter("@EQS_ETHNICITY", EQS_ETHNICITY)
                pParms(43) = New SqlClient.SqlParameter("@EQS_ETHNICITY_OTH", EQS_ETHNICITY_OTH)
                pParms(44) = New SqlClient.SqlParameter("@EQS_ENG_READING", EQS_ENG_READING)
                pParms(45) = New SqlClient.SqlParameter("@EQS_ENG_WRITING", EQS_ENG_WRITING)
                pParms(46) = New SqlClient.SqlParameter("@EQS_ENG_SPEAKING", EQS_ENG_SPEAKING)
                pParms(47) = New SqlClient.SqlParameter("@EQS_bREP_GRD", EQS_bREP_GRD)
                pParms(48) = New SqlClient.SqlParameter("@EQS_REP_GRD", EQS_REP_GRD)

                pParms(49) = New SqlClient.SqlParameter("@EQS_bCommInt", EQS_bCommInt)
                pParms(50) = New SqlClient.SqlParameter("@EQS_CommInt", EQS_CommInt)

                pParms(51) = New SqlClient.SqlParameter("@EQS_bDisabled", EQS_bDisabled)
                pParms(52) = New SqlClient.SqlParameter("@EQS_Disabled", EQS_Disabled)



                pParms(53) = New SqlClient.SqlParameter("@TEMP_ApplNo", SqlDbType.VarChar, 20)
                pParms(53).Direction = ParameterDirection.Output
                pParms(54) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(54).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudENQUIRY_SCHOOLPRIO_S_NEW", pParms)
                Dim ReturnFlag As Integer = pParms(54).Value
                If ReturnFlag = 0 Then
                    TEMP_ApplNo = pParms(53).Value
                End If
                Return ReturnFlag
            Catch ex As Exception
                Return -1
            End Try
        End Using



    End Function


    Public Shared Function SaveStudENQUIRY_SCHOOLPRIO_S(ByVal EQS_EQM_ENQID As String, ByVal EQS_ACY_ID As String, _
       ByVal EQS_BSU_ID As String, ByVal EQS_GRD_ID As String, ByVal EQS_SHF_ID As String, _
       ByVal EQS_bTPTREQD As Boolean, ByVal EQS_LOC_ID As String, ByVal EQS_SBL_ID As String, ByVal EQS_PNT_ID As String, _
 ByVal EQS_STM_ID As String, ByVal EQS_bAPPLSIBLING As Boolean, ByVal EQS_SIBLINGFEEID As String, ByVal EQS_SIBLINGSCHOOL As String, _
 ByVal EQS_TPTREMARKS As String, ByVal EQS_STATUS As String, ByVal EQM_bRCVSPMEDICATION As Boolean, ByVal EQM_SPMEDICN As String, _
ByVal EQM_REMARKS As String, ByVal EQS_CLM_ID As Integer, ByVal EQS_DOJ As String, ByVal EQS_RFS_ID As String, ByRef TEMP_ApplNo As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try


                Dim pParms(30) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", EQS_EQM_ENQID)
                pParms(1) = New SqlClient.SqlParameter("@EQS_ACY_ID", EQS_ACY_ID)
                pParms(2) = New SqlClient.SqlParameter("@EQS_BSU_ID", EQS_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@EQS_SHF_ID", EQS_SHF_ID)
                pParms(5) = New SqlClient.SqlParameter("@EQS_bTPTREQD", EQS_bTPTREQD)
                pParms(6) = New SqlClient.SqlParameter("@EQS_LOC_ID", EQS_LOC_ID)
                pParms(7) = New SqlClient.SqlParameter("@EQS_SBL_ID", EQS_SBL_ID)
                pParms(8) = New SqlClient.SqlParameter("@EQS_PNT_ID", EQS_PNT_ID)
                pParms(9) = New SqlClient.SqlParameter("@EQS_STM_ID", EQS_STM_ID)
                pParms(10) = New SqlClient.SqlParameter("@EQS_bAPPLSIBLING", EQS_bAPPLSIBLING)
                pParms(11) = New SqlClient.SqlParameter("@EQS_SIBLINGFEEID", EQS_SIBLINGFEEID)
                pParms(12) = New SqlClient.SqlParameter("@EQS_SIBLINGSCHOOL", EQS_SIBLINGSCHOOL)
                pParms(13) = New SqlClient.SqlParameter("@EQS_TPTREMARKS", EQS_TPTREMARKS)
                pParms(14) = New SqlClient.SqlParameter("@EQS_STATUS", EQS_STATUS)
                pParms(15) = New SqlClient.SqlParameter("@EQM_bRCVSPMEDICATION", EQM_bRCVSPMEDICATION)
                pParms(16) = New SqlClient.SqlParameter("@EQM_SPMEDICN", EQM_SPMEDICN)
                pParms(17) = New SqlClient.SqlParameter("@EQS_CLM_ID", EQS_CLM_ID)
                pParms(18) = New SqlClient.SqlParameter("@EQM_REMARKS", EQM_REMARKS)
                pParms(19) = New SqlClient.SqlParameter("@EQS_DOJ", EQS_DOJ)
                pParms(20) = New SqlClient.SqlParameter("@EQS_RFS_ID", EQS_RFS_ID)

                pParms(21) = New SqlClient.SqlParameter("@EQS_SIB_APPL", HttpContext.Current.Session("SIB_APPLICANT"))
                pParms(22) = New SqlClient.SqlParameter("@TEMP_ApplNo", SqlDbType.VarChar, 20)
                pParms(22).Direction = ParameterDirection.Output
                pParms(23) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(23).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveStudENQUIRY_SCHOOLPRIO_S", pParms)
                Dim ReturnFlag As Integer = pParms(23).Value

                If ReturnFlag = 0 Then
                    TEMP_ApplNo = pParms(22).Value
                End If
                Return ReturnFlag
            Catch ex As Exception
                Return -1
            End Try
        End Using



    End Function

    Public Shared Function SaveGRADE_BSU_M(ByVal GRM_ACD_ID As String, ByVal GRM_BSU_ID As String, ByVal GRM_GRD_ID As String, _
                       ByVal GRM_DESCR As String, ByVal GRM_DISPLAY As String, ByVal GRM_bAVAILABLE As Boolean, ByVal GRM_OPENONLINE As Boolean, _
                       ByVal GRM_MAXCAPACITY As String, ByVal GRM_CURRCAPACITY As String, ByVal GRM_bWAITLIST As Boolean, _
                       ByVal GRM_bSUBJOPTIONAL As Boolean, ByVal GRM_SUBOPTIONAL As String, ByVal GRM_bSCREENINGREQD As Boolean, _
                       ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)

        'Date   --27/JAN/2008
        'Purpose--To save GRADE_BSU_M data based on the datamode (studGrade_M.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(14) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@GRM_ACD_ID", GRM_ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@GRM_BSU_ID", GRM_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@GRM_GRD_ID", GRM_GRD_ID)
            pParms(3) = New SqlClient.SqlParameter("@GRM_DESCR", GRM_DESCR)
            pParms(4) = New SqlClient.SqlParameter("@GRM_DISPLAY", GRM_DISPLAY)
            pParms(5) = New SqlClient.SqlParameter("@GRM_bAVAILABLE", GRM_bAVAILABLE)
            pParms(6) = New SqlClient.SqlParameter("@GRM_OPENONLINE", GRM_OPENONLINE)
            pParms(7) = New SqlClient.SqlParameter("@GRM_MAXCAPACITY", GRM_MAXCAPACITY)
            pParms(8) = New SqlClient.SqlParameter("@GRM_CURRCAPACITY", GRM_CURRCAPACITY)
            pParms(9) = New SqlClient.SqlParameter("@GRM_bWAITLIST", GRM_bWAITLIST)
            pParms(10) = New SqlClient.SqlParameter("@GRM_bSUBJOPTIONAL", GRM_bSUBJOPTIONAL)
            pParms(11) = New SqlClient.SqlParameter("@GRM_SUBOPTIONAL", GRM_SUBOPTIONAL)
            pParms(12) = New SqlClient.SqlParameter("@GRM_bSCREENINGREQD", GRM_bSCREENINGREQD)
            pParms(13) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(14) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(14).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDGRADE_BSU_M", pParms)
            Dim ReturnFlag As Integer = pParms(14).Value
            Return ReturnFlag
        End Using

    End Function


    Public Shared Function SaveSTUDDOCRQD_GRADE_M(ByVal DRQ_ID As Integer, ByVal DRQ_ACD_ID As String, ByVal DRQ_BSU_ID As String, _
                    ByVal DRQ_GRM_GRD_ID As String, ByVal DRQ_DOC_ID As String, ByVal DRQ_TYPE As String, ByVal DRQ_COPY As String, ByVal DRQ_APPLY As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --16/JAN/2008
        'Purpose--To save DOCRQD_GRADE_M data based on the datamode (studJoinDetails.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@DRQ_ID", DRQ_ID)
            pParms(1) = New SqlClient.SqlParameter("@DRQ_ACD_ID", DRQ_ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@DRQ_BSU_ID", DRQ_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@DRQ_GRM_GRD_ID", DRQ_GRM_GRD_ID)

            pParms(4) = New SqlClient.SqlParameter("@DRQ_DOC_ID", DRQ_DOC_ID)

            pParms(5) = New SqlClient.SqlParameter("@DRQ_TYPE", DRQ_TYPE)
            pParms(6) = New SqlClient.SqlParameter("@DRQ_COPY", DRQ_COPY)

            pParms(7) = New SqlClient.SqlParameter("@DRQ_APPLY", DRQ_APPLY)

            pParms(8) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDDOCRQD_GRADE_M", pParms)
            Dim ReturnFlag As Integer = pParms(9).Value
            Return ReturnFlag
        End Using

    End Function
    Public Shared Function SAVEComm_Detail_XML(ByVal STR_XML As String, ByVal SPARENT As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --06/MAY/2009
        'Purpose--To save Parent Communication Details data based 
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STR_XML", STR_XML)
                pParms(1) = New SqlClient.SqlParameter("@SPARENT", SPARENT)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVEComm_Detail_XML", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value
                Return ReturnFlag

            End Using

        Catch ex As Exception
            Return 1
        End Try


    End Function



    Public Shared Function SaveSTUDHouse_M(ByVal HOUSE_ID As Integer, ByVal HOUSE_BSU_ID As String, ByVal HOUSE_DESCRIPTION As String, _
                ByVal HOUSE_COLOR As String, ByVal E_type As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --09/JAN/2008
        'Purpose--To save TRM_M data based on the datamode (studSubject_M.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@HOUSE_ID", HOUSE_ID)
            pParms(1) = New SqlClient.SqlParameter("@HOUSE_BSU_ID", HOUSE_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@HOUSE_DESCRIPTION", HOUSE_DESCRIPTION)
            pParms(3) = New SqlClient.SqlParameter("@HOUSE_COLOR", HOUSE_COLOR)
            pParms(4) = New SqlClient.SqlParameter("@E_type", E_type)
            pParms(5) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDHouse_M", pParms)
            Dim ReturnFlag As Integer = pParms(6).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function SaveSTUDSubject_M(ByVal SBM_ID As String, ByVal SBM_DESCR As String, ByVal SMB_LANG As Boolean, _
                    ByVal E_type As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer

        'Author(--Lijo)
        'Date   --09/JAN/2008
        'Purpose--To save TRM_M data based on the datamode (studSubject_M.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SBM_ID", SBM_ID)
            pParms(1) = New SqlClient.SqlParameter("@SBM_DESCR", SBM_DESCR)
            pParms(2) = New SqlClient.SqlParameter("@SMB_LANG", SMB_LANG)
            pParms(3) = New SqlClient.SqlParameter("@E_type", E_type)
            pParms(4) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDSubject_M", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function SaveTRM_M(ByVal TRM_ID As Integer, ByVal TRM_ACD_ID As Integer, ByVal TRM_BSU_ID As String, ByVal TRM_DESCRIPTION As String, _
             ByVal TRM_STARTDATE As Date, ByVal TRM_ENDDATE As Date, ByVal weeksCount As Integer, ByVal bEdit As Boolean, ByRef NEW_TRM_ID As Integer, ByVal trans As SqlTransaction, ByVal TRM_ACTUALSTARTDATE As DateTime) As Integer

        'Author(--Lijo)
        'Date   --07/JAN/2008
        'Purpose--To save TRM_M data based on the datamode (studTRM_M.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TRM_ID", TRM_ID)
            pParms(1) = New SqlClient.SqlParameter("@TRM_ACD_ID", TRM_ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@TRM_BSU_ID", TRM_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@TRM_DESCRIPTION", TRM_DESCRIPTION)
            pParms(4) = New SqlClient.SqlParameter("@TRM_STARTDATE", TRM_STARTDATE)
            pParms(5) = New SqlClient.SqlParameter("@TRM_ENDDATE", TRM_ENDDATE)
            pParms(6) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            pParms(8) = New SqlClient.SqlParameter("@NewTRM_ID", NEW_TRM_ID)
            pParms(8).Direction = ParameterDirection.Output
            pParms(9) = New SqlClient.SqlParameter("@TRM_WEEKS", weeksCount)
            pParms(10) = New SqlClient.SqlParameter("@TRM_ACTUALSTARTDATE", TRM_ACTUALSTARTDATE)

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDTRM_M", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            If Not pParms(8).Value Is DBNull.Value Then
                NEW_TRM_ID = pParms(8).Value
            End If
            If NEW_TRM_ID = 0 Then NEW_TRM_ID = TRM_ID
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function F_UPDATEFEESPONSOR(ByVal STS_STU_ID As String, ByVal STS_FEESPONSOR As String, _
    ByVal STS_F_COMP_ID As String, ByVal STS_M_COMP_ID As String, ByVal STS_G_COMP_ID As String, _
    ByVal STS_FCOMPANY As String, ByVal STS_MCOMPANY As String, ByVal STS_GCOMPANY As String, _
    ByVal trans As SqlTransaction) As Integer
        'Author(--GURU)
        'Date   --05/Mar/2008
        'Purpose--To save STUDENT_D data based on the datamode (StudRecordEdit.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STS_FEESPONSOR", STS_FEESPONSOR)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                pParms(3) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
                pParms(4) = New SqlClient.SqlParameter("@STS_M_COMP_ID", STS_M_COMP_ID)
                pParms(5) = New SqlClient.SqlParameter("@STS_G_COMP_ID", STS_G_COMP_ID)

                pParms(6) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
                pParms(7) = New SqlClient.SqlParameter("@STS_MCOMPANY", STS_MCOMPANY)
                pParms(8) = New SqlClient.SqlParameter("@STS_GCOMPANY", STS_GCOMPANY)

                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.F_UPDATEFEESPONSOR", pParms)
                Return pParms(2).Value
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return 1000
            End Try
        End Using

    End Function

    ''' <summary>
    ''' The Term Month Details
    ''' </summary>
    ''' <param name="TRM_ID">Term ID</param>
    ''' <returns>The DataTable contains the Month Details</returns>
    ''' <remarks></remarks>
    ''' 

    Public Shared Function Fill_TRM_DETAILS(ByVal TRM_ID As Integer) As DataSet
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sqlTRM_DETAILS As String = ""

            sqlTRM_DETAILS = "SELECT AMS_ID , AMS_ACD_ID, AMS_MONTH, " & _
            "AMS_TRM_ID, AMS_ORDER, AMS_DTFROM, AMS_DTTO " & _
            "FROM ACADEMIC_MonthS_S WHERE AMS_TRM_ID=" & TRM_ID & _
            " ORDER BY AMS_ORDER"
            Dim command As SqlCommand = New SqlCommand(sqlTRM_DETAILS, connection)
            command.CommandType = CommandType.Text
            Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.Text, sqlTRM_DETAILS)
            Return ds
        End Using
    End Function

    ''' <summary>
    ''' The Term Month Details
    ''' </summary>
    ''' <param name="TRM_ID">Term ID</param>
    ''' <returns>The DataTable contains the Month Details</returns>
    ''' <remarks></remarks>
    Public Shared Function Fill_TRM_WEEKS(ByVal TRM_ID As Integer) As DataSet
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sqlTRM_DETAILS As String = ""

            sqlTRM_DETAILS = "SELECT AWS_ID, AWS_TRM_ID, AWS_DTFROM, AWS_DTTO " & _
            "FROM ACADEMIC_WEEKS_S WHERE AWS_TRM_ID=" & TRM_ID & _
            " ORDER BY AWS_DTFROM"
            Dim command As SqlCommand = New SqlCommand(sqlTRM_DETAILS, connection)
            command.CommandType = CommandType.Text
            Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.Text, sqlTRM_DETAILS)
            Return ds
        End Using
    End Function

    Public Shared Function DeleteTermWeeks(ByVal TRM_ID As Integer, _
    ByVal TRM_ACD_ID As Integer, ByVal trans As SqlTransaction, ByVal MNU_CODE As String, _
    ByVal user As String) As Integer
        Dim str_AuditData As String = String.Empty
        Dim i As Integer = 1
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@AWS_TRM_ID", TRM_ID)
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.[F_DeleteACADEMIC_WEEKS_S]", pParms)
        pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim ReturnFlag As Integer = pParms(1).Value
        If ReturnFlag > 0 Then
            Return ReturnFlag
        End If
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(MNU_CODE, TRM_ID, "Delete", user)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        Return 0
    End Function


    ''' <summary>
    ''' Saves the Terms in Descriptive mode.
    ''' 
    ''' </summary>
    ''' <param name="TRM_ID">Term ID </param>
    ''' <param name="TRM_ACD_ID"> Academic Year ID</param>
    ''' <param name="trans">SQLTransaction</param>
    ''' <param name="bEdit">If it is in editable mode or not</param>
    ''' <returns>The Error No.If the transaction is successed return 0</returns>
    ''' <remarks>Added to Get the Months count for Fees</remarks>
    Public Shared Function SaveTermWeeks(ByVal TRM_ID As Integer, _
    ByVal TRM_ACD_ID As Integer, ByVal dtTerm As DataTable, _
    ByVal trans As SqlTransaction, ByVal MNU_CODE As String, _
    ByVal user As String, Optional ByVal bEdit As Boolean = False) As Integer
        If dtTerm Is Nothing OrElse dtTerm.Rows.Count <= 0 Then
            Return 0
        End If
        Dim dv As New DataView(dtTerm)
        dv.Sort = "FromDate"
        dtTerm = dv.ToTable()

        Dim str_AuditData As String = String.Empty
        Dim i As Integer = 1
        'Dim pParms(2) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@AWS_TRM_ID", TRM_ID)
        'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.[F_DeleteACADEMIC_WEEKS_S]", pParms)
        'pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        'pParms(1).Direction = ParameterDirection.ReturnValue
        'Dim ReturnFlag As Integer = pParms(1).Value

        Dim ReturnFlag As Integer
        Dim pParms(6) As SqlClient.SqlParameter

        str_AuditData = "ACD_ID = " & TRM_ACD_ID & "; TRM_ID = " & TRM_ID & ";"
        'If ReturnFlag > 0 Then
        '    Return ReturnFlag
        'End If
        Dim tempbEdit As Boolean
        For Each dr As DataRow In dtTerm.Rows
            If IIf(dr("bDelete") Is Nothing, False, dr("bDelete")) = False Then
                If dr("FromDate") > dr("ToDate") Then
                    Return 782
                End If
            End If
            ReDim pParms(6)
            pParms(0) = New SqlClient.SqlParameter("@AWS_ID", dr("UniqueID"))
            pParms(1) = New SqlClient.SqlParameter("@AWS_TRM_ID", TRM_ID)
            pParms(2) = New SqlClient.SqlParameter("@AWS_DTFROM", dr("FromDate"))
            pParms(3) = New SqlClient.SqlParameter("@AWS_DTTO", dr("ToDate"))
            tempbEdit = IIf(dr("bEdit") Is DBNull.Value, False, dr("bEdit"))
            pParms(4) = New SqlClient.SqlParameter("@bEdit", tempbEdit)
            'If tempbEdit Then
            pParms(5) = New SqlClient.SqlParameter("@bDelete", IIf(dr("bDelete") Is DBNull.Value, False, dr("bDelete")))
            'Else
            '    pParms(5) = New SqlClient.SqlParameter("@bDelete", False)
            'End If
            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.F_SaveACADEMIC_WEEKS_S", pParms)
            ReturnFlag = pParms(6).Value
            If ReturnFlag > 0 Then
                Return ReturnFlag
            End If
            i = i + 1
            str_AuditData += "WEEK_ID = " & dr("UniqueID") & "; FROM_DATE = " & _
            Format(dr("FromDate"), OASISConstants.DateFormat) & _
            "; TO_DATE = " & Format(dr("ToDate"), OASISConstants.DateFormat)
        Next
        Dim str_Status As String = IIf(bEdit, "Edited", "Insert")
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(MNU_CODE, TRM_ID, str_Status, user)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        Return 0
    End Function

    Public Shared Function DeleteTermMonths(ByVal TRM_ID As Integer, ByVal TRM_ACD_ID As Integer, _
    ByVal trans As SqlTransaction, ByVal MNU_CODE As String, _
    ByVal user As String) As Integer
        Dim i As Integer = 1
        Dim ReturnFlag As Integer
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@AMS_TRM_ID", TRM_ID)
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.[F_DeleteACADEMIC_MonthS_S]", pParms)
        pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        ReturnFlag = pParms(1).Value
        If ReturnFlag > 0 Then
            Return ReturnFlag
        End If
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(MNU_CODE, TRM_ID, "Delete", user)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        Return 0
    End Function

    ''' <summary>
    ''' Saves the Terms in Descriptive mode.
    ''' 
    ''' </summary>
    ''' <param name="TRM_ID">Term ID </param>
    ''' <param name="TRM_ACD_ID"> Academic Year ID</param>
    ''' <param name="trans">SQLTransaction</param>
    ''' <param name="bEdit">If it is in editable mode or not</param>
    ''' <returns>The Error No.If the transaction is successed return 0</returns>
    ''' <remarks>Added to Get the Months count for Fees</remarks>
    Public Shared Function SaveTermMonths(ByVal TRM_ID As Integer, ByVal TRM_ACD_ID As Integer, _
    ByVal dtTerm As DataTable, ByVal trans As SqlTransaction, ByVal MNU_CODE As String, _
    ByVal user As String, Optional ByVal bEdit As Boolean = False) As Integer
        If dtTerm Is Nothing OrElse dtTerm.Rows.Count <= 0 Then
            Return 0
        End If
        Dim dv As New DataView(dtTerm)
        dv.Sort = "FromDate"
        dtTerm = dv.ToTable()
        Dim str_AuditData As String = String.Empty
        Dim i As Integer = 1
        Dim ReturnFlag As Integer
        'Dim pParms(2) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@AMS_TRM_ID", TRM_ID)
        'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.[F_DeleteACADEMIC_MonthS_S]", pParms)
        'pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        'pParms(1).Direction = ParameterDirection.ReturnValue
        'Dim ReturnFlag As Integer = pParms(1).Value
        'If ReturnFlag > 0 Then
        '    Return ReturnFlag
        'End If
        str_AuditData = "ACD_ID = " & TRM_ACD_ID & "; TRM_ID = " & TRM_ID & ";"
        For Each dr As DataRow In dtTerm.Rows
            If IIf(dr("bDelete") Is DBNull.Value, False, dr("bDelete")) = False Then
                If dr("FromDate") > dr("ToDate") Then
                    Return 782
                End If
            End If
            Dim vMonth_ID As Integer = dr("MONTH_ID")
            If CDate(dr("FromDate")).Month <> vMonth_ID Or CDate(dr("ToDate")).Month <> vMonth_ID Then
                'Return 783'Commented on 30/aug/2015 by jacob
            End If
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@AMS_ID", dr("UniqueID"))
            pParms(1) = New SqlClient.SqlParameter("@AMS_ACD_ID", TRM_ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@AMS_MONTH", CDate(dr("FromDate")).Month)
            pParms(3) = New SqlClient.SqlParameter("@AMS_TRM_ID", TRM_ID)
            pParms(4) = New SqlClient.SqlParameter("@AMS_ORDER", i)
            pParms(5) = New SqlClient.SqlParameter("@AMS_DTFROM", dr("FromDate"))
            pParms(6) = New SqlClient.SqlParameter("@AMS_DTTO", dr("ToDate"))
            pParms(7) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue
            pParms(9) = New SqlClient.SqlParameter("@bDelete", IIf(dr("bDelete") Is DBNull.Value, False, dr("bDelete")))
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.F_SaveACADEMIC_MonthS_S", pParms)
            str_AuditData += "MONTH_ID = " & CDate(dr("FromDate")).Month & "; FROM_DATE = " & _
            Format(dr("FromDate"), OASISConstants.DateFormat) & "; TO_DATE = " & Format(dr("ToDate"), OASISConstants.DateFormat)
            ReturnFlag = pParms(8).Value
            If ReturnFlag > 0 Then
                Return ReturnFlag
            End If
            i = i + 1
        Next
        Dim str_Status As String = IIf(bEdit, "Edit Month", "Insert")

        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(MNU_CODE, TRM_ID, str_Status, user, Nothing, str_AuditData)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        Return 0
    End Function

    Public Shared Function CheckEditingAllowed(ByVal TRM_ID As String) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TRMID", TRM_ID)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "CanDeleteTErm", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            If ReturnFlag = 800 Then
                Return 1
            ElseIf ReturnFlag <> 0 Then
                Return 1000
            Else
                Return 0
            End If
        End Using
    End Function
    Public Shared Function SaveSTU_BSU_RELIGION(ByVal SBR_ID As Integer, ByVal SBR_RLG_ID As String, ByVal SBR_BSU_ID As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --12/AUG/2008
        'Purpose--To save STU_BSU_RELIGION data based on the datamode (STU_BSU_RELIGION)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SBR_ID", SBR_ID)
            pParms(1) = New SqlClient.SqlParameter("@SBR_RLG_ID", SBR_RLG_ID)
            pParms(2) = New SqlClient.SqlParameter("@SBR_BSU_ID", SBR_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTU_BSU_RELIGION", pParms)
            Dim ReturnFlag As Integer = pParms(4).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function SaveACADEMICYEAR_D(ByVal ACD_ID As Integer, ByVal ACD_ACY_ID As Integer, ByVal ACD_BSU_ID As String, ByVal ACD_CLM_ID As Integer, _
             ByVal ACD_STARTDT As Date, ByVal ACD_ENDDT As Date, ByVal ACD_CURRENT As Boolean, ByVal ACD_OPENONLINE As Boolean, ByVal ACD_AFFLNO As String, ByVal ACD_MOEAFFLNO As String, ByVal ACD_LICENSE As String, ByVal ACD_ATT_CLOSINGDATE As Date, ByVal ACD_BROWNBOOKDATE As Date, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer

        'Author(--Lijo)
        'Date   --07/JAN/2008
        'Purpose--To save ACADEMICYEAR_D data based on the datamode (studAcademicyear_D.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(14) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@ACD_ACY_ID", ACD_ACY_ID)
            pParms(2) = New SqlClient.SqlParameter("@ACD_BSU_ID", ACD_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@ACD_CLM_ID", ACD_CLM_ID)
            pParms(4) = New SqlClient.SqlParameter("@ACD_STARTDT", ACD_STARTDT)
            pParms(5) = New SqlClient.SqlParameter("@ACD_ENDDT", ACD_ENDDT)
            pParms(6) = New SqlClient.SqlParameter("@ACD_CURRENT", ACD_CURRENT)
            pParms(7) = New SqlClient.SqlParameter("@ACD_OPENONLINE", ACD_OPENONLINE)
            ' pParms(8) = New SqlClient.SqlParameter("@ACD_AFFL", ACD_AFFL)
            pParms(8) = New SqlClient.SqlParameter("@ACD_AFFLNO", ACD_AFFLNO)
            pParms(9) = New SqlClient.SqlParameter("@ACD_MOEAFFLNO", ACD_MOEAFFLNO)
            pParms(10) = New SqlClient.SqlParameter("@ACD_LICENSE", ACD_LICENSE)
            Try
                Dim c_date As String
                c_date = ACD_ATT_CLOSINGDATE.ToString("dd/MM/yyyy")
                If c_date = "01/01/0001" Then
                    pParms(11) = New SqlClient.SqlParameter("@ACD_ATT_CLOSINGDATE", Nothing)
                Else
                    pParms(11) = New SqlClient.SqlParameter("@ACD_ATT_CLOSINGDATE", ACD_ATT_CLOSINGDATE)
                End If
            Catch ex As Exception
                pParms(11) = New SqlClient.SqlParameter("@ACD_ATT_CLOSINGDATE", Nothing)
            End Try

            Try
                Dim c_date As String
                c_date = ACD_BROWNBOOKDATE.ToString("dd/MM/yyyy")
                If c_date = "01/01/0001" Then
                    pParms(12) = New SqlClient.SqlParameter("@ACD_BROWNBOOKDATE", Nothing)
                Else
                    pParms(12) = New SqlClient.SqlParameter("@ACD_BROWNBOOKDATE", ACD_BROWNBOOKDATE)
                End If
            Catch ex As Exception
                pParms(12) = New SqlClient.SqlParameter("@ACD_BROWNBOOKDATE", Nothing)
            End Try


            pParms(13) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(14) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(14).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDACADEMICYEAR_D", pParms)
            Dim ReturnFlag As Integer = pParms(14).Value
            Return ReturnFlag
        End Using

    End Function


    Public Shared Function SaveSTUDDOCREQD_M(ByVal Doc_ID As Integer, ByVal DOC_DOCTYPE_ID As String, ByVal DOC_DESCR As String, _
             ByVal E_type As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --151/JAN/2008
        'Purpose--To save DOCREQD_M data based on the datamode (studSubject_M.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Doc_ID", Doc_ID)
            pParms(1) = New SqlClient.SqlParameter("@DOC_DOCTYPE_ID", DOC_DOCTYPE_ID)
            pParms(2) = New SqlClient.SqlParameter("@DOC_DESCR", DOC_DESCR)
            pParms(3) = New SqlClient.SqlParameter("@E_type", E_type)
            pParms(4) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDDOCREQD_M", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        End Using

    End Function





    Public Shared Function SaveSTUDCOMP_FEECOMPO_S(ByVal CIS_ID As String, ByVal CIS_COMP_ID As String, ByVal CIS_FEE_ID As String, _
                    ByVal CIS_bPreformaInvoice As Boolean, ByVal CIS_DiscPerct As String, ByVal RecordStatus As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --04/Jun/2008
        'Purpose--To save STUDCOMP_FEECOMPO_S data based on the datamode 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CIS_ID", CIS_ID)
            pParms(1) = New SqlClient.SqlParameter("@CIS_COMP_ID", CIS_COMP_ID)
            pParms(2) = New SqlClient.SqlParameter("@CIS_FEE_ID", CIS_FEE_ID)
            pParms(3) = New SqlClient.SqlParameter("@CIS_bPreformaInvoice", CIS_bPreformaInvoice)
            pParms(4) = New SqlClient.SqlParameter("@CIS_DiscPerct", CIS_DiscPerct)
            pParms(5) = New SqlClient.SqlParameter("@RecordStatus", RecordStatus)
            pParms(6) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDCOMP_FEECOMPO_S", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            Return ReturnFlag
        End Using

    End Function


    Public Shared Function SaveSTUDCOMP_LISTED_M(ByVal comp_ID As String, ByVal Comp_Name As String, ByVal Addr1 As String, _
                 ByVal Addr2 As String, ByVal City_Name As String, ByVal country_ID As String, ByVal pobox As String, ByVal Emirate_ID As String, _
                 ByVal offPhone As String, ByVal faxno As String, ByVal MobPhone As String, ByVal Email As String, _
                 ByVal Comp_URL As String, ByVal ContactPer As String, ByRef NEW_COMP_ID As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --03/Jun/2008
        'Purpose--To save STUDCOMP_LISTED_M data based on the datamode 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(16) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@COMP_ID", comp_ID)
            pParms(1) = New SqlClient.SqlParameter("@COMP_NAME", Comp_Name)
            pParms(2) = New SqlClient.SqlParameter("@COMP_ADDR1", Addr1)
            pParms(3) = New SqlClient.SqlParameter("@COMP_ADDR2", Addr2)
            pParms(4) = New SqlClient.SqlParameter("@COMP_CITY", City_Name)
            pParms(5) = New SqlClient.SqlParameter("@COMP_COUNTRY", country_ID)
            pParms(6) = New SqlClient.SqlParameter("@COMP_POBOX", pobox)
            pParms(7) = New SqlClient.SqlParameter("@COMP_EMIRATE", Emirate_ID)
            pParms(8) = New SqlClient.SqlParameter("@COMP_PHONE", offPhone)
            pParms(9) = New SqlClient.SqlParameter("@COMP_FAX", faxno)
            pParms(10) = New SqlClient.SqlParameter("@COMP_EMAIL", Email)
            pParms(11) = New SqlClient.SqlParameter("@COMP_URL", Comp_URL)
            pParms(12) = New SqlClient.SqlParameter("@COMP_CONTACT", ContactPer)
            pParms(13) = New SqlClient.SqlParameter("@COMP_CONTACT_MOBILE", MobPhone)
            pParms(14) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(15) = New SqlClient.SqlParameter("@NEW_COMP_ID", SqlDbType.Int)
            pParms(15).Direction = ParameterDirection.Output
            pParms(16) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(16).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDCOMP_LISTED_M", pParms)
            NEW_COMP_ID = pParms(15).Value
            Dim ReturnFlag As Integer = pParms(16).Value
            Return ReturnFlag
        End Using

    End Function
#End Region
#Region "used for form Registation Function"

    Public Shared Function GetSTU_COMM_DETAIL_USR(ByVal STU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To get GETSTU_COMM_DETAIL data based 


        Try


            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
            Dim pParms(0) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GETSTU_COMM_DETAIL", pParms)

            Return reader






        Catch ex As Exception
            Return Nothing
        End Try
    End Function



    Public Shared Function GetUserDetails(ByVal p_USR_NAME As String, ByVal p_USR_PASSWORD As String, ByVal ENTER_PASSWORD As String, _
       ByVal Activate_USR As String, ByVal p_AUD_WINUSER As String, ByRef dtUserData As DataTable, _
        ByRef USR_TRYCOUNT As Integer) As Integer
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        'EXEC	@return_value = [dbo].[GetUserDetails]
        '@USR_NAME = N'guru',
        '@USR_PASSWORD = N'wr92h2HbWq4=',
        '@USR_TRYCOUNT = @USR_TRYCOUNT OUTPUT,
        '@AUD_HOST = N'x',
        '@AUD_WINUSER = N'xx'

        Try

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
            pParms(0).Value = p_USR_NAME
            pParms(1) = New SqlClient.SqlParameter("@USR_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_USR_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@ENTER_PASSWORD", SqlDbType.VarChar, 100)
            pParms(2).Value = ENTER_PASSWORD

            pParms(3) = New SqlClient.SqlParameter("@USR_TRYCOUNT", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.Output
            pParms(4) = New SqlClient.SqlParameter("@Activate_USR", SqlDbType.VarChar, 100)
            pParms(4).Value = Activate_USR
            pParms(5) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(5).Value = p_AUD_WINUSER
            pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetUserMember_Details", pParms)

            If pParms(6).Value = "0" Then
                If ds.Tables.Count > 0 Then
                    dtUserData = ds.Tables(0)
                    USR_TRYCOUNT = pParms(3).Value
                End If
            ElseIf pParms(6).Value = "-888" Then
                If ds.Tables.Count > 0 Then
                    dtUserData = ds.Tables(0)
                    USR_TRYCOUNT = pParms(3).Value
                End If
            End If
            USR_TRYCOUNT = pParms(3).Value
            GetUserDetails = pParms(6).Value
            SqlConnection.ClearPool(connection)
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
            GetUserDetails = "1000"

        End Try
    End Function

    Public Shared Function GetUserDetails_Password(ByVal p_USR_NAME As String, ByRef dt As DataTable) As Integer
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
            pParms(0).Value = p_USR_NAME

            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetUserMember_Password", pParms)
            If pParms(1).Value = "0" Then
                If ds.Tables.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If
            GetUserDetails_Password = pParms(1).Value
        Catch ex As Exception
            GetUserDetails_Password = "-1"
        End Try
    End Function

    Public Shared Function getuser_Comm_EmailID(ByVal BSU_ID As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetMain_Location As String


        sqlGetMain_Location = "SELECT     BSC_HOST, BSC_USERNAME, BSC_PASSWORD, BSC_PORT, BSC_TYPE, BSC_FROMEMAIL " & _
        " FROM BSU_COMMUNICATION_M WHERE  BSC_TYPE = 'PARENT_LOG' AND BSC_BSU_ID = '" + BSU_ID + "'"

        Dim command As SqlCommand = New SqlCommand(sqlGetMain_Location, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader

    End Function
    Public Shared Function GetTermDate(ByVal trm_ID As String) As String
        Dim sqlString As String = "select replace(convert(varchar(11), trm_startdate, 106), ' ', '/') from trm_M where trm_ID='" & trm_ID & "'"
        Dim sdate As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            sdate = command.ExecuteScalar
            SqlConnection.ClearPool(connection)

        End Using
        Return CStr(sdate)
    End Function

    Public Shared Function getMain_Location(ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --06/MAR/2008--modified
        'Purpose--Get the Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetMain_Location As String

        'query modified by dhanya 29-07-08
        sqlGetMain_Location = "SELECT [LOC_ID],[LOC_DESCRIPTION]  FROM [TRANSPORT].[VV_LOCATION_M] WHERE LOC_BSU_ID='" + BSU_ID + "' order by LOC_DESCRIPTION"

        Dim command As SqlCommand = New SqlCommand(sqlGetMain_Location, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    Public Shared Function getSubMain_Location(ByVal BSU_ID As String, ByVal LOC_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --09/MAR/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetSubMain_Location As String

        'query modified by dhanya 29-07-08
        '  sqlGetSubMain_Location = "SELECT [SBL_ID]  ,[SBL_DESCRIPTION] FROM .[dbo].[TPTSUBLOCATION_M] where [SBL_BSU_ID]='" & BSU_ID & "' and [SBL_LOC_ID]='" & LOC_ID & "' order by SBL_DESCRIPTION"
        sqlGetSubMain_Location = "SELECT [SBL_ID]  ,[SBL_DESCRIPTION] FROM [TRANSPORT].[VV_SUBLOCATION_M] where [SBL_BSU_ID]='" & BSU_ID & "' and [SBL_LOC_ID]='" & LOC_ID & "' order by SBL_DESCRIPTION"
        Dim command As SqlCommand = New SqlCommand(sqlGetSubMain_Location, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    Public Shared Function getPickUP_Points(ByVal BSU_ID As String, ByVal SBL_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --09/MAR/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPickUP_Points As String
        'query modified by dhanya 29-07-08
        ' sqlGetPickUP_Points = "SELECT [PNT_ID],[PNT_DESCRIPTION]  FROM [OASIS].[dbo].[TPTPICKUPPOINTS_M] where [PNT_BSU_ID]='" & BSU_ID & "' and [PNT_SBL_ID]='" & SBL_ID & "' order by PNT_DESCRIPTION"
        sqlGetPickUP_Points = "SELECT [PNT_ID],[PNT_DESCRIPTION]  FROM [TRANSPORT].[VV_PICKUPOINTS_M] where [PNT_BSU_ID]='" & BSU_ID & "' and [PNT_SBL_ID]='" & SBL_ID & "' order by PNT_DESCRIPTION"

        Dim command As SqlCommand = New SqlCommand(sqlGetPickUP_Points, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function getPick_Stream_Only_Registrar(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String, ByVal SHF_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --20/Apr/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPick_Stream As String

        sqlGetPick_Stream = " SELECT  distinct   STREAM_M.STM_DESCR, GRADE_BSU_M.GRM_STM_ID AS STM_ID " & _
                               " FROM  GRADE_BSU_M INNER JOIN  ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
                              " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                              " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                              " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID WHERE (ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "') " & _
                              " AND (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "' and GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "') ORDER BY STREAM_M.STM_DESCR "



        Dim command As SqlCommand = New SqlCommand(sqlGetPick_Stream, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function getPick_Stream_Only(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String, ByVal SHF_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --20/Apr/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPick_Stream As String

        sqlGetPick_Stream = " SELECT  distinct   STREAM_M.STM_DESCR, GRADE_BSU_M.GRM_STM_ID AS STM_ID " & _
                               " FROM  GRADE_BSU_M INNER JOIN  ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
                              " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                              " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                              " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID WHERE (ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "') " & _
                              " AND (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "') AND  (GRADE_BSU_M.GRM_OPENONLINE = 1) AND (ACADEMICYEAR_D.ACD_OPENONLINE = 1) AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "' and GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "') ORDER BY STREAM_M.STM_DESCR "



        Dim command As SqlCommand = New SqlCommand(sqlGetPick_Stream, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function getPick_Stream_Only_Registrar(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --20/Apr/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPick_Stream As String

        sqlGetPick_Stream = " SELECT  distinct   STREAM_M.STM_DESCR, GRADE_BSU_M.GRM_STM_ID AS STM_ID " & _
                               " FROM  GRADE_BSU_M INNER JOIN  ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
                              " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                              " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                              " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID WHERE (ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "')  and ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "') " & _
                              "  ORDER BY STREAM_M.STM_DESCR "



        Dim command As SqlCommand = New SqlCommand(sqlGetPick_Stream, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function getPick_Stream_Registrar(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String, ByVal SHF_ID As String, ByVal STM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --09/MAR/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPick_Stream As String

        sqlGetPick_Stream = " SELECT DISTINCT STREAM_M.STM_DESCR, GRADE_BSU_M.GRM_STM_ID AS STM_ID " & _
                            " FROM GRADE_BSU_M INNER JOIN ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
                      " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                      " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                     " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID WHERE (ACADEMICYEAR_D.ACD_ACY_ID =  '" & ACY_ID & "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "')" & _
                     " AND (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "' and GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "' and  STREAM_M.STM_ID='" & STM_ID & "' ) ORDER BY STREAM_M.STM_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetPick_Stream, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function getPick_Stream_Registrar(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --09/MAR/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPick_Stream As String

        sqlGetPick_Stream = " SELECT DISTINCT STREAM_M.STM_DESCR, GRADE_BSU_M.GRM_STM_ID AS STM_ID " & _
                            " FROM GRADE_BSU_M INNER JOIN ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
                      " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                      " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                     " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID WHERE (ACADEMICYEAR_D.ACD_ACY_ID =  '" & ACY_ID & "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "')" & _
                     " ORDER BY STREAM_M.STM_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetPick_Stream, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function getPick_Stream(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String, ByVal SHF_ID As String, ByVal STM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --09/MAR/2008--modified
        'Purpose--Get the Sub Main location
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPick_Stream As String

        sqlGetPick_Stream = " SELECT DISTINCT STREAM_M.STM_DESCR, GRADE_BSU_M.GRM_STM_ID AS STM_ID " & _
                            " FROM GRADE_BSU_M INNER JOIN ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
                      " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                      " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                     " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID WHERE (ACADEMICYEAR_D.ACD_ACY_ID =  '" & ACY_ID & "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "')" & _
                     " AND (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "') AND  (GRADE_BSU_M.GRM_OPENONLINE = 1) AND (ACADEMICYEAR_D.ACD_OPENONLINE = 1) AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "' and GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "' and  STREAM_M.STM_ID='" & STM_ID & "' ) ORDER BY STREAM_M.STM_DESCR"





        '" SELECT  distinct   STREAM_M.STM_DESCR, GRADE_BSU_M.GRM_STM_ID AS STM_ID " & _
        '                       " FROM  GRADE_BSU_M INNER JOIN  ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
        '                      " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
        '                      " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
        '                      " STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID WHERE (ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "') " & _
        '                      " AND (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "') AND  (GRADE_BSU_M.GRM_OPENONLINE = 1) AND (ACADEMICYEAR_D.ACD_OPENONLINE = 1) AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "' and GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "') ORDER BY STREAM_M.STM_DESCR "


        '"SELECT distinct STREAM_M.STM_DESCR as STM_DESCR, GRADE_BSU_M.GRM_STM_ID as STM_ID " & _
        '                     " FROM  GRADE_BSU_M INNER JOIN  ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND " & _
        '                     " GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
        '                     " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID INNER JOIN  STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID " & _
        '                    " WHERE (ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM_ID & "') AND (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "') AND " & _
        '                    " (GRADE_BSU_M.GRM_OPENONLINE = 1) AND (ACADEMICYEAR_D.ACD_OPENONLINE = 1) AND (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "' and GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "') ORDER BY STREAM_M.STM_DESCR"


        Dim command As SqlCommand = New SqlCommand(sqlGetPick_Stream, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetTerm_CheckDate(ByVal ACD_ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal TRM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --08/jun/2008--modified
        'Purpose--Get the terms for that BSU which is currently open based on ACD_ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTerm_CheckDate As String = ""

        sqlGetTerm_CheckDate = " select Trm_startDate,trm_endDate FROM term_master " & _
 " WHERE TRM_ID='" & TRM_ID & "' and TRM_ACD_ID=(SELECT  ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_BSU_ID = '" & BSU_ID & "') AND " & _
" (ACD_ACY_ID = '" & ACD_ACY_ID & "') AND (ACD_CLM_ID = '" & CLM_ID & "'))"

        Dim command As SqlCommand = New SqlCommand(sqlGetTerm_CheckDate, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function



    Public Shared Function GetTerm_4ACD(ByVal ACD_ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --08/jun/2008--modified
        'Purpose--Get the terms for that BSU which is currently open based on ACD_ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTerm_4ACD As String = ""


        sqlGetTerm_4ACD = " select TRM_ID,TRM_DESCRIPTION+'('+replace(convert(varchar(11), Trm_startDate, 106), ' ', '/')+' To '+replace(convert(varchar(11), trm_endDate, 106), ' ', '/')+')' as TDescr,Trm_startDate,trm_endDate FROM term_master " & _
 " WHERE TRM_ACD_ID=(SELECT  ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_BSU_ID = '" & BSU_ID & "') AND " & _
" (ACD_ACY_ID = '" & ACD_ACY_ID & "') AND (ACD_CLM_ID = '" & CLM_ID & "')) AND cast(convert( varchar(12),trm_endDate ,106) as datetime)>=cast(convert( varchar(12),getdate() ,106) as datetime)"

        '       "sqlGetTerm_4ACD = "SELECT distinct TRM_M.TRM_DESCRIPTION as TDescr, TRM_M.TRM_ID as TRM_ID FROM ACADEMICYEAR_D INNER JOIN   GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
        '" TRM_M ON ACADEMICYEAR_D.ACD_ID = TRM_M.TRM_ACD_ID WHERE (ACADEMICYEAR_D.ACD_CURRENT=1) and (GRADE_BSU_M.GRM_OPENONLINE = 1) AND   (ACADEMICYEAR_D.ACD_OPENONLINE= 1) AND (GRADE_BSU_M.GRM_GRD_ID = '" & Grades & "') AND  (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "')"


        ' "SELECT TRM_ID , TRM_DESCRIPTION as TDescr FROM TRM_M where TRM_ACD_ID='" & ACD_ID & "' and  TRM_BSU_ID='" & BSU_ID & "' order by TDescr"
        Dim command As SqlCommand = New SqlCommand(sqlGetTerm_4ACD, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function GetOpen_BSU_Staff(ByVal BSU_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlStaffID_BSU As String = ""
        Dim ds1 As DataSet
        sqlStaffID_BSU = " select BSU_ID,BSU_NAME from BUSINESSUNIT_M where BSU_ID in (" & BSU_ID & ") order by BSU_NAME"

        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, sqlStaffID_BSU)
        SqlConnection.ClearPool(connection)
        Return ds1

    End Function
    Public Shared Function GetOpen_BSU_Sib(ByVal BSU_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSibID_BSU As String = ""
        Dim ds1 As DataSet
        sqlSibID_BSU = " select BSU_ID,BSU_NAME from BUSINESSUNIT_M where BSU_ID in (" & BSU_ID & ") order by BSU_NAME"

        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, sqlSibID_BSU)
        SqlConnection.ClearPool(connection)
        Return ds1



    End Function
    Public Shared Function CheckStaffID_BSU(ByVal BSU_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlStaffID_BSU As String = ""

        sqlStaffID_BSU = " select BSU_bCheck_StaffID from BUSINESSUNIT_M where BSU_ID= '" & BSU_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlStaffID_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader

    End Function
    Public Shared Function getValid_staff(ByVal StaffId As String, ByVal Bsu_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetValid_Staff As String = ""

        sqlGetValid_Staff = "SELECT EMP_ID FROM EMPLOYEE_M WHERE EMP_STAFFNO = '" & StaffId & "' AND EMP_BSU_ID = '" & Bsu_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetValid_Staff, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader

    End Function

    Public Shared Function GetValid_Sibl_virtual(ByVal FeesID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --12/Feb/2008
        'Purpose--To validate Sibling ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim sqlGetValid_Sibl As String = ""

        sqlGetValid_Sibl = "SELECT  STU_ID  FROM STUDENT_M WHERE (STU_FEE_ID = '" & FeesID & "')"


        Dim command As SqlCommand = New SqlCommand(sqlGetValid_Sibl, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetValid_Sibl(ByVal FeesID As String, ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --12/Feb/2008
        'Purpose--To validate Sibling ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim sqlGetValid_Sibl As String = ""

        sqlGetValid_Sibl = "SELECT  STU_ID  FROM STUDENT_M WHERE (STU_BSU_ID = '" & BSU_ID & "') AND ((STU_FEE_ID = '" & FeesID & "') OR (STU_NO = '" & FeesID & "'))"


        Dim command As SqlCommand = New SqlCommand(sqlGetValid_Sibl, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetCurrent_BsuShift(ByVal BSU_ID As String, ByVal ACD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the all the shift for the current academic year
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlCurrent_BsuShift As String = "SELECT [SHF_ID],[SHF_DESCR]  FROM [SHIFTS_M]where SHF_ID in(SELECT distinct [GRM_SHF_ID]  FROM [GRADE_BSU_M] where GRM_BSU_ID='" & BSU_ID & "' and GRM_ACD_ID='" & ACD_ID & "') order by SHF_DESCR "

        Dim command As SqlCommand = New SqlCommand(sqlCurrent_BsuShift, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader


    End Function
    Public Shared Function GetCurrent_Grades(ByVal BSU_ID As String, ByVal ACD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlCurrent_Grades As String = ""

        sqlCurrent_Grades = " SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID, GRADE_BSU_M.GRM_DISPLAY, GRADE_M.GRD_DISPLAYORDER " & _
                            " FROM GRADE_BSU_M INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_BSU_ID='" & BSU_ID & "' and GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER"



        Dim command As SqlCommand = New SqlCommand(sqlCurrent_Grades, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetGrade_Section(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Section As String = ""
        If SHF_ID = "" Then
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "') order by SCT_DESCR"
        Else
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID & "' ) order by SCT_DESCR"
        End If

        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetGrade_Section_TEMP(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Section As String = ""
        If SHF_ID = "" Then
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "') and sct_descr<>'TEMP' order by SCT_DESCR"
        Else
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID & "' )  and sct_descr<>'TEMP' order by SCT_DESCR"
        End If

        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetGrade_Section_LIB(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As New SqlCommand("GetGrade_Section_LIB", connection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add(New SqlClient.SqlParameter("@BSU_ID", BSU_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@ACD_ID", ACD_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@GRD_ID", GRD_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@SHF_ID", SHF_ID))



        Dim reader As SqlDataReader = cmd.ExecuteReader()
        SqlConnection.ClearPool(connection)
        Return reader









    End Function


    Public Shared Function GetActive_BsuAndCutOff(ByVal BSU_ID As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCurr_Off As String = ""

        sqlGetCurr_Off = "SELECT ACD_ID,ACD_ACY_ID,ACD_AGE_CUTOFF  FROM [ACADEMICYEAR_D] where ACD_CURRENT=1 and ACD_BSU_ID='" & BSU_ID & "' and ACD_CLM_ID='" & CLM_ID & "' "
        Dim command As SqlCommand = New SqlCommand(sqlGetCurr_Off, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function



    Public Shared Function getAllShift_4_Grade_Registrar(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --06/Mar/2008
        'Purpose--To collect the shift 4 the grade
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Shift As String = ""

        sqlGrade_Shift = "SELECT distinct GRADE_BSU_M.GRM_SHF_ID as SHF_ID , SHIFTS_M.SHF_DESCR as SHF_DESCR,ISNULL(GRADE_BSU_M.GRM_APPL_AGE_CUTOFF,ACADEMICYEAR_D.ACD_APPL_AGE_CUTOFF) AS CUTOFF FROM  GRADE_BSU_M INNER JOIN " & _
                     "ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                     " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID where (ACD_ACY_ID = '" & ACY_ID & "' and ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "' and ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "'  and GRADE_BSU_M.GRM_GRD_ID='" & GRD_ID & "') order by SHIFTS_M.SHF_DESCR"


        Dim command As SqlCommand = New SqlCommand(sqlGrade_Shift, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader


    End Function
    Public Shared Function getAllShift_4_Grade(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --06/Mar/2008
        'Purpose--To collect the shift 4 the grade
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Shift As String = ""

        sqlGrade_Shift = "SELECT distinct GRADE_BSU_M.GRM_SHF_ID as SHF_ID , SHIFTS_M.SHF_DESCR as SHF_DESCR,ISNULL(GRADE_BSU_M.GRM_APPL_AGE_CUTOFF,ACADEMICYEAR_D.ACD_APPL_AGE_CUTOFF) AS CUTOFF FROM  GRADE_BSU_M INNER JOIN " & _
                     "ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
                     " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID where (ACD_ACY_ID = '" & ACY_ID & "' and ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "' and ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' and GRADE_BSU_M.GRM_OPENONLINE = 1 and ACADEMICYEAR_D.ACD_OPENONLINE=1 and GRADE_BSU_M.GRM_GRD_ID='" & GRD_ID & "') order by SHIFTS_M.SHF_DESCR"


        Dim command As SqlCommand = New SqlCommand(sqlGrade_Shift, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader


    End Function
    Public Shared Function GetAllGrade_Bsu_Registrar(ByVal ACY_ID As String, ByVal BSU_ID As String, ByVal CLM As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Feb/2008
        'Purpose--Get the Grade for the currently open academic year
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetAllGrade_Bsu As String = ""

        sqlGetAllGrade_Bsu = "SELECT  distinct GRADE_BSU_M.GRM_DISPLAY AS GRM_Display, GRADE_M.GRD_DISPLAYORDER, GRADE_BSU_M.GRM_GRD_ID as GRD_ID,ISNULL(GRADE_BSU_M.GRM_AGE,'') AS GRM_AGE " & _
 " FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
 " GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
 " WHERE (ACADEMICYEAR_D.ACD_CLM_ID='" & CLM & "') and (ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "') AND " & _
" (ACADEMICYEAR_D.ACD_ID in (select ACD_ID from ACADEMICYEAR_D where ACADEMICYEAR_D.ACD_CLM_ID='" & CLM & "' and ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' and ACD_ACY_ID = '" & ACY_ID & "' )) ORDER BY GRADE_M.GRD_DISPLAYORDER "


        Dim command As SqlCommand = New SqlCommand(sqlGetAllGrade_Bsu, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    Public Shared Function GetAllGrade_Bsu_Registrar(ByVal ACY_ID As String, ByVal BSU_ID As String, ByVal CLM As String, ByVal strmId As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Feb/2008
        'Purpose--Get the Grade for the currently open academic year
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetAllGrade_Bsu As String = ""

        sqlGetAllGrade_Bsu = "SELECT  distinct GRADE_BSU_M.GRM_DISPLAY AS GRM_Display, GRADE_M.GRD_DISPLAYORDER, GRADE_BSU_M.GRM_GRD_ID as GRD_ID,ISNULL(GRADE_BSU_M.GRM_AGE,'') AS GRM_AGE " & _
 " FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
 " GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
 " WHERE (ACADEMICYEAR_D.ACD_CLM_ID='" & CLM & "') and (ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "') AND GRM_STM_ID='" & strmId & "' " & _
" AND (ACADEMICYEAR_D.ACD_ID in (select ACD_ID from ACADEMICYEAR_D where ACADEMICYEAR_D.ACD_CLM_ID='" & CLM & "' and ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' and ACD_ACY_ID = '" & ACY_ID & "' )) ORDER BY GRADE_M.GRD_DISPLAYORDER "


        Dim command As SqlCommand = New SqlCommand(sqlGetAllGrade_Bsu, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    Public Shared Function GetAllGrade_Bsu(ByVal ACY_ID As String, ByVal BSU_ID As String, ByVal CLM As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Feb/2008
        'Purpose--Get the Grade for the currently open academic year
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetAllGrade_Bsu As String = ""

        sqlGetAllGrade_Bsu = "SELECT  distinct GRADE_BSU_M.GRM_DISPLAY AS GRM_Display, GRADE_M.GRD_DISPLAYORDER, GRADE_BSU_M.GRM_GRD_ID as GRD_ID, ISNULL(GRADE_BSU_M.GRM_AGE,'') AS GRM_AGE " & _
 " FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
 " GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
 " WHERE (ACADEMICYEAR_D.ACD_CLM_ID='" & CLM & "') and (ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "')and (GRADE_BSU_M.GRM_OPENONLINE = 1) and (ACADEMICYEAR_D.ACD_OPENONLINE=1) AND " & _
" (ACADEMICYEAR_D.ACD_ID in (select ACD_ID from ACADEMICYEAR_D where ACADEMICYEAR_D.ACD_CLM_ID='" & CLM & "' and ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' and ACD_ACY_ID = '" & ACY_ID & "' and ACD_OPENONLINE=1 )) ORDER BY GRADE_M.GRD_DISPLAYORDER "


        Dim command As SqlCommand = New SqlCommand(sqlGetAllGrade_Bsu, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetActive_BSU_ACD_ID(ByVal BSU_ID As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Feb/2008--modified
        'Purpose--Get the Academic year for the active BSU
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetOpen_BSU As String = ""

        sqlGetOpen_BSU = "SELECT  distinct   ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR ,ACADEMICYEAR_D.ACD_ACY_ID as ACD_ACY_ID,ACADEMICYEAR_D.ACD_ID as ACD_ID " & _
" FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
  " GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID" & _
 " WHERE  (ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "') and (ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "') order by ACADEMICYEAR_D.ACD_ACY_ID "
        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetActive_BSU_Year_Registrar(ByVal BSU_ID As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Feb/2008--modified
        'Purpose--Get the Academic year for the active BSU
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetOpen_BSU As String = ""

        sqlGetOpen_BSU = "SELECT  distinct   ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR ,ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID " & _
" FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
  " GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID" & _
 " WHERE  (ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "') and (ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "') order by ACADEMICYEAR_D.ACD_ACY_ID "
        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function




    Public Shared Function GetActive_BSU_Year(ByVal BSU_ID As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Feb/2008--modified
        'Purpose--Get the Academic year for the active BSU
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetOpen_BSU As String = ""

        sqlGetOpen_BSU = "SELECT  distinct   ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR ,ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID " & _
" FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
  " GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID" & _
 " WHERE (GRADE_BSU_M.GRM_OPENONLINE = 1) and (ACADEMICYEAR_D.ACD_OPENONLINE=1)and (ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "') and (ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "') order by ACADEMICYEAR_D.ACD_ACY_ID "
        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetAll_BSU() As SqlDataReader
        'Author(--Lijo)
        'Date   --29/JAN/2008
        'Purpose--Get the all the BSU --not in use
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetAll_BSU As String = ""

        sqlGetAll_BSU = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M order by BSU_NAME"
        Dim command As SqlCommand = New SqlCommand(sqlGetAll_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function





    Public Shared Function GetOpen_BSU() As SqlDataReader
        'Author(--Lijo)
        'Date   --29/JAN/2008
        'Purpose--Get the BSU which is currently open
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetOpen_BSU As String = ""

        sqlGetOpen_BSU = "SELECT  ACADEMICYEAR_D.ACD_BSU_ID as BSU_ID, BUSINESSUNIT_M.BSU_NAME as BSU_NAME " & _
                        " FROM ACADEMICYEAR_D INNER JOIN  BUSINESSUNIT_M ON ACADEMICYEAR_D.ACD_BSU_ID = BUSINESSUNIT_M.BSU_ID where ACADEMICYEAR_D.ACD_OPENONLINE=1 order by BUSINESSUNIT_M.BSU_NAME"

        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
#End Region

#Region "All GET Function"


    Public Shared Function GetEnquiry_Validation(ByVal BSU_ID As String, ByVal EVD_TYPE As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --21/MAr/2009

        Dim sqlEnquiry_Validation As String = " SELECT ENQUIRY_VALIDATION_M.EQV_CODE as EQV_CODE, ENQUIRY_VALIDATION_M.EQV_ID as EQV_ID, isnull(ENQUIRY_VALIDATION_M.EQV_CONTROL_ID,'') as CONTROL_ID,isnull(ENQUIRY_VALIDATION_M.EQV_STAR_ID,'') as STAR_ID " & _
" FROM ENQUIRY_VALIDATION_M INNER JOIN ENQUIRY_VALIDATION_D ON ENQUIRY_VALIDATION_M.EQV_ID = ENQUIRY_VALIDATION_D.EVD_EQV_ID " & _
" where ENQUIRY_VALIDATION_D.EVD_bNOT_TO_VALIDATE=1 AND ENQUIRY_VALIDATION_D.EVD_TYPE='" & EVD_TYPE & "' AND ENQUIRY_VALIDATION_D.EVD_BSU_ID='" & BSU_ID & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlEnquiry_Validation, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetOffer_LetterDATA(ByVal BAL_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --19/nov/2008
        'Purpose--Get PATTERN data from BSU_APPL_LETTER
        Dim sqlOffer_LetterDATA As String = "SELECT     BAL_APL_ID,  BAL_MAT, BAL_REM, BAL_MAT_BLP_ID, BAL_REM_BLP_ID, BAL_SIGN, BAL_ACK, BAL_ACK_BLP_ID, BAL_bDefault, " & _
        " BAL_SUB FROM BSU_APPL_LETTER WHERE BAL_ID='" & BAL_ID & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlOffer_LetterDATA, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function

    Public Shared Function GetOffer_LetterList(ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --19/nov/2008
        'Purpose--Get PATTERN data from BSU_APPL_LETTER_PATTERN
        Dim sqlLetter_Pattern As String = "select BAL_ID,BAL_DESCR,BAL_bDefault from BSU_APPL_LETTER where BAL_BSU_ID ='" & BSU_ID & "' order by BAL_DESCR"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlLetter_Pattern, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetAPPLIC_ENQNO(ByVal ENQ_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --19/nov/2008
        'Purpose--Get Application Number from ENQUIRY_SCHOOLPRIO_S
        Dim sqlAPPLNO As String = "select distinct EQS_APPLNO from ENQUIRY_SCHOOLPRIO_S where EQS_EQM_ENQID ='" & ENQ_ID & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlAPPLNO, connection)
        command.CommandType = CommandType.Text
        Dim readerAPPLNO As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerAPPLNO
    End Function


    Public Shared Function GetMatter_Pattern() As SqlDataReader
        'Author(--Lijo)
        'Date   --19/nov/2008
        'Purpose--Get PATTERN data from BSU_APPL_LETTER_PATTERN
        Dim sqlMatter_Pattern As String = "SELECT [BLP_DESC] ,[BLP_PATTERN] FROM [BSU_APPL_LETTER_PATTERN]  where BLP_bBODY_TAG=0 order by [BLP_DISP_ORDER]"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlMatter_Pattern, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetLetter_Pattern() As SqlDataReader
        'Author(--Lijo)
        'Date   --19/nov/2008
        'Purpose--Get PATTERN data from BSU_APPL_LETTER_PATTERN
        Dim sqlLetter_Pattern As String = "SELECT [BLP_DESC] ,[BLP_PATTERN] FROM [BSU_APPL_LETTER_PATTERN] where [BLP_bFORMAT]=1 and BLP_bBODY_TAG=0 order by [BLP_DISP_ORDER]"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlLetter_Pattern, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetBody_Tag() As SqlDataReader
        'Author(--Lijo)
        'Date   --19/nov/2008
        'Purpose--Get PATTERN data from BSU_APPL_LETTER_PATTERN
        Dim sqlLetter_Pattern As String = "SELECT [BLP_DESC] ,[BLP_ID] FROM [BSU_APPL_LETTER_PATTERN] where [BLP_bFORMAT]=1 and BLP_bBODY_TAG=1 order by [BLP_DISP_ORDER]"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlLetter_Pattern, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetEMAIL_HOST(ByVal BSU_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim EMAIL_HOST As String = String.Empty
        EMAIL_HOST = "  SELECT [BSC_HOST],[BSC_USERNAME] ,[BSC_PASSWORD],[BSC_PORT],[BSC_TYPE]" & _
  " FROM [BSU_COMMUNICATION_M] where bsc_type='ENQUIRY'  AND [BSC_BSU_ID]='" & BSU_ID & "'"
        Dim command As SqlCommand = New SqlCommand(EMAIL_HOST, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetPOLICY_VIEW(ByVal PLY_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim POLICY_VIEW As String = String.Empty
        POLICY_VIEW = " SELECT PLY_ID, PLY_DESCR, PLY_FROMDT, PLY_TODT, ISNULL(PLY_CURRENT,0) AS PLY_CURRENT,ISNULL(PLY_bSUBSCR_DATE,0) AS PLY_bSUBSCR_DATE FROM POLICY_M_EDUSHIELD WHERE PLY_ID ='" & PLY_ID & "' "

        Dim command As SqlCommand = New SqlCommand(POLICY_VIEW, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function



    Public Shared Function GetACK_MAIN_VIEW(ByVal EAS_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ACK_MAIN As String = String.Empty
        ACK_MAIN = " SELECT distinct EAS_ACD_ID,[EAS_GRD_ID],[EAS_TEXT_MAIN] ,[EAS_MAIN_bSHOW],[EAS_SUB_bSHOW],isnull([EAS_bBULLET_POINTS],'false')  as EAS_bBULLET_POINTS FROM [ENQUIRY_ACK_SETTING]where [EAS_ID] ='" & EAS_ID & "' "

        Dim command As SqlCommand = New SqlCommand(ACK_MAIN, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function







    Public Shared Function GetACK_SUB_VIEW(ByVal EAS_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim ACK_SUB As String = String.Empty

        ACK_SUB = "SELECT ROW_NUMBER() OVER (ORDER BY EAD_DISPLAY_ORDER ASC) AS ID,[EAD_ID] ,[EAD_TEXT] as DOCLIST,[EAD_DISPLAY_ORDER] as DIS_ORDER " & _
  " FROM [ENQUIRY_ACK_DOC] where [EAD_EAS_ID]='" & EAS_ID & "' Order by [EAD_DISPLAY_ORDER]"

        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, ACK_SUB)
        SqlConnection.ClearPool(connection)
        Return ds1


    End Function



    Public Shared Function getBSU_CLM(ByVal BSU_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim BSU_CLM As String = String.Empty

        BSU_CLM = " SELECT distinct    CURRICULUM_M.CLM_DESCR as CLM_DESCR, CURRICULUM_M.CLM_ID as CLM_ID " & _
      " FROM ACADEMICYEAR_D INNER JOIN CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID " & _
       " and  ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "'"

        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, BSU_CLM)
        SqlConnection.ClearPool(connection)
        Return ds1


    End Function
    Public Shared Function GetACD_Year_Date(ByVal ACD_ID As String, ByVal CLM_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ACD_Date As String = String.Empty
        ACD_Date = "select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "' and  ACD_CLM_ID='" & CLM_ID & "'"

        Dim command As SqlCommand = New SqlCommand(ACD_Date, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Public Shared Function GetACY_Year_Date(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ACD_Date As String = String.Empty
        ACD_Date = "select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ACY_ID='" & ACY_ID & "' and  ACD_CLM_ID='" & CLM_ID & "' and  ACD_BSU_ID='" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(ACD_Date, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetBSU_M_AcadStart(ByVal BSU_ID As String, ByVal ACD_ACY_ID As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get BSU_M data from BUSINESSUNIT_M excluding Dummy Business Unit
        Dim sqlAcadStart As String = "select ACD_STARTDT From AcademicYear_D where ACD_BSU_ID='" & BSU_ID & "' AND ACD_ACY_ID='" & ACD_ACY_ID & "' AND ACD_CLM_ID='" & CLM_ID & "' "
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlAcadStart, connection)
        command.CommandType = CommandType.Text
        Dim readerAcadStart As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerAcadStart
    End Function
    Public Shared Function GetBSU_Option(ByVal Enq_Option As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim BSU_Option As String = String.Empty
        If Enq_Option = "1" Then
            BSU_Option = "select bsu_id,bsu_name from businessunit_m where (BSU_ID<>'XXXXXX') order by bsu_name"
        ElseIf Enq_Option = "2" Then
            BSU_Option = " select bsu_id,bsu_name from businessunit_m where (BUS_BSG_ID<>'4')  order by bsu_name"
        ElseIf Enq_Option = "3" Then
            BSU_Option = " Select SCH_CODE as bsu_id,SCH_DESCR as bsu_name from PRENURSERY_M where SCH_CODE not in('0','4') order by SCH_DESCR "
        ElseIf Enq_Option = "4" Then
            BSU_Option = " select bsu_id,bsu_name from businessunit_m where (BUS_BSG_ID<>'4')  order by bsu_name"
        Else
            BSU_Option = "dbo.GetSchool_list"
        End If
        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, BSU_Option)
        SqlConnection.ClearPool(connection)
        Return ds1

    End Function


    Public Shared Function GetACD_Year_Enquiry(ByVal BSU_ID As String, ByVal CLM_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim Acd_Year As String = " SELECT  distinct ACADEMICYEAR_M.ACY_DESCR, ACADEMICYEAR_D.ACD_ID FROM ACADEMICYEAR_D INNER JOIN " & _
 " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
" WHERE ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "' AND ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' AND  " & _
" ACADEMICYEAR_D.ACD_ACY_ID>=(SELECT ACD_ACY_ID from ACADEMICYEAR_D where ACD_BSU_ID='" & BSU_ID & "' AND ACD_CURRENT=1 and ACD_CLM_ID='" & CLM_ID & "')"
        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, Acd_Year)
        SqlConnection.ClearPool(connection)
        Return ds1
    End Function
    Public Shared Function GetComm_Detail(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, _
      ByVal SPARENT As String, ByVal FEE_ID As String, ByVal SNAME As String, ByVal Current_DT As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --22/aug/2008
        'Purpose--Get communitation Details
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlComm_Detail As String = ""
        If SPARENT = "F" Then
            sqlComm_Detail = " SELECT * FROM(SELECT     STUDENT_M.STU_ID AS STU_ID,  STUDENT_M.STU_FEE_ID AS FEE_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME), " & _
                             "   '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS SNAME, STUDENT_D.STS_FCOMPOBOX AS POB, STUDENT_D.STS_FCOMCITY AS EMIRATE, " & _
                              "  STUDENT_D.STS_FRESPHONE AS RESPHONE, STUDENT_D.STS_FMOBILE AS MOBILE, STUDENT_D.STS_FEMAIL AS EMAIL FROM   STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID " & _
                              " WHERE  (STUDENT_M.STU_ACD_ID = '" & ACD_ID & "') AND (STUDENT_M.STU_GRD_ID =  '" & GRD_ID & "') AND (STUDENT_M.STU_SCT_ID = '" & SCT_ID & "') AND " & _
                         " (('" & Current_DT & "' <= STUDENT_M.STU_LEAVEDATE) OR  (CONVERT(datetime, " & _
                         " STUDENT_M.STU_LEAVEDATE) IS NULL)))A  WHERE STU_ID<>'' " & FEE_ID & SNAME & " ORDER BY SNAME"

        ElseIf SPARENT = "M" Then
            sqlComm_Detail = "SELECT * FROM(SELECT     STUDENT_M.STU_ID as STU_ID, STUDENT_M.STU_FEE_ID AS FEE_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME),  " & _
           " '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS SNAME, STUDENT_D.STS_MCOMPOBOX AS POB, STUDENT_D.STS_MCOMCITY AS EMIRATE,  " & _
                    "  STUDENT_D.STS_MRESPHONE AS RESPHONE, STUDENT_D.STS_MMOBILE AS MOBILE, STUDENT_D.STS_MEMAIL AS EMAIL " & _
            " FROM   STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID  " & _
            " WHERE     (STUDENT_M.STU_ACD_ID = '" & ACD_ID & "') AND (STUDENT_M.STU_GRD_ID = '" & GRD_ID & "') AND (STUDENT_M.STU_SCT_ID = '" & SCT_ID & "') AND  " & _
                     " (('" & Current_DT & "' <= STUDENT_M.STU_LEAVEDATE)  or (CONVERT(datetime,  " & _
                      " STUDENT_M.STU_LEAVEDATE) IS NULL)))A  WHERE STU_ID<>'' " & FEE_ID & SNAME & "ORDER BY SNAME"

        ElseIf SPARENT = "G" Then
            sqlComm_Detail = " SELECT * FROM(SELECT     STUDENT_M.STU_ID as STU_ID,  STUDENT_M.STU_FEE_ID AS FEE_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME),  " & _
           " '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS SNAME, STUDENT_D.STS_GCOMPOBOX AS POB, STUDENT_D.STS_GCOMCITY AS EMIRATE,  " & _
                      " STUDENT_D.STS_GRESPHONE AS RESPHONE, STUDENT_D.STS_GMOBILE AS MOBILE, STUDENT_D.STS_GEMAIL AS EMAIL  " & _
            " FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID " & _
            " WHERE     (STUDENT_M.STU_ACD_ID = '" & ACD_ID & "') AND (STUDENT_M.STU_GRD_ID = '" & GRD_ID & "') AND (STUDENT_M.STU_SCT_ID = '" & SCT_ID & "') AND  " & _
                      " (('" & Current_DT & "' <= STUDENT_M.STU_LEAVEDATE)  or (CONVERT(datetime,  " & _
                     " STUDENT_M.STU_LEAVEDATE) IS NULL)))A  WHERE STU_ID<>''" & FEE_ID & SNAME & " ORDER BY SNAME"

        End If



        Dim command As SqlCommand = New SqlCommand(sqlComm_Detail, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function







    Public Shared Function GETGRADE_SECTION_Report(ByVal ACD As String, ByVal GRD As String, ByVal SCT As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGETGRADE_SECTION As String = ""

        sqlGETGRADE_SECTION = "SELECT DISTINCT SECTION_M.SCT_DESCR AS SCT, GRADE_BSU_M.GRM_DISPLAY AS GRD FROM GRADE_BSU_M INNER JOIN " & _
    " SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID" & _
    " where GRADE_BSU_M.GRM_ACD_ID='" & ACD & "' and GRADE_BSU_M.GRM_GRD_ID='" & GRD & "' AND SECTION_M.SCT_ID='" & SCT & "'"

        Dim command As SqlCommand = New SqlCommand(sqlGETGRADE_SECTION, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function



    Public Shared Function PopulateRole_User(ByVal BSU_ID As String) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = " declare  @temptable_rol table(ROL_DESCR varchar(100), USR_NAME varchar(100), ROL_ID varchar(10), USR_ID varchar(20)) " & _
 " insert @temptable_rol (ROL_DESCR,USR_NAME, ROL_ID, USR_ID) " & _
" select  ROLES_M.ROL_DESCR, USERS_M.USR_NAME,ROLES_M.ROL_ID , USERS_M.USR_ID " & _
 " FROM ROLES_M INNER JOIN USERS_M ON ROLES_M.ROL_ID = USERS_M.USR_ROL_ID where USERS_M.USR_BSU_ID='" & BSU_ID & "'" & _
" order by ROLES_M.ROL_DESCR, USERS_M.USR_NAME " & _
" SELECT ROL_ID,ROL_DESCR, USR_ID,USR_NAME FROM   @temptable_ROL ORDER BY ROL_DESCR, USR_NAME "

            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function



    Public Shared Function PopulateGrade_Section(ByVal ACD_ID As String) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "declare  @temptable table(GRD_DESCR varchar(120), SCT_DESCR varchar(120), GRD_ID varchar(100), SCT_ID varchar(10),DISPLAYORDER int) " & _
  " insert @temptable (GRD_DESCR, SCT_DESCR, GRD_ID, SCT_ID,DISPLAYORDER) " & _
" SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR, " & _
"  GRADE_BSU_M.GRM_GRD_ID as  GRD_ID,SECTION_M.SCT_ID as SCT_ID,GRADE_M.GRD_DISPLAYORDER as DISPLAYORDER" & _
" FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND " & _
" GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
" where  GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' AND SECTION_M.SCT_DESCR<>'TEMP' order by GRADE_M.GRD_DISPLAYORDER  ASC" & _
" SELECT GRD_ID,GRD_DESCR, SCT_ID,SCT_DESCR FROM   @temptable ORDER BY DISPLAYORDER "

            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function





    Public Shared Function GetLOT_ALLOTMENT(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SHF_ID As String, _
                        ByVal EQS_STAGE As String, ByVal LOT_ALLOC_FOR As String, ByVal LOT_CAP As String, ByVal LOT_GENDER As String) As DataSet
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To get LOT_ALLOTMENT data based 
        Dim dsData As DataSet = Nothing
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(15) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
                pParms(2) = New SqlClient.SqlParameter("@SHF_ID", SHF_ID)
                pParms(3) = New SqlClient.SqlParameter("@EQS_STAGE", EQS_STAGE)
                pParms(4) = New SqlClient.SqlParameter("@LOT_CAP", LOT_CAP)
                pParms(5) = New SqlClient.SqlParameter("@LOT_ALLOC_FOR", LOT_ALLOC_FOR)
                pParms(6) = New SqlClient.SqlParameter("@LOT_GENDER", LOT_GENDER)

                dsData = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "ENQ.LOT_ALLOTMENT", pParms)

            End Using

            Return dsData

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetENQ_SETTING_DATA(ByVal EQS_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Jun/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlENQ_SETTING_DATA As String = ""

        sqlENQ_SETTING_DATA = "SELECT [EQS_ACD_ID],[EQS_CLM_ID],[EQS_GRD_ID],[EQS_FROMDT] ,[EQS_TODT]  ,[EQS_EQO_ID]," & _
" [EQS_BSU_IDS]  FROM [ENQUIRY_SETTINGS]where [EQS_ID] ='" & EQS_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlENQ_SETTING_DATA, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function






    Public Shared Function GetCOMP_FEE_TYPE(ByVal CIS_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Jun/2008
        'Purpose--Get COMP_FeeType
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlCOMP_FEE_TYPE As String = ""

        sqlCOMP_FEE_TYPE = "SELECT  COMP_FEECOMPO_S.CIS_DiscPerct, COMP_LISTED_M.COMP_ID, COMP_FEECOMPO_S.CIS_FEE_ID, " & _
       " COMP_FEECOMPO_S.CIS_bPreformaInvoice FROM  COMP_LISTED_M INNER JOIN  COMP_FEECOMPO_S ON COMP_LISTED_M.COMP_ID = COMP_FEECOMPO_S.CIS_COMP_ID where COMP_FEECOMPO_S.CIS_ID='" & CIS_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlCOMP_FEE_TYPE, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetCOMP_LISTED_M(ByVal COMP_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/Jun/2008
        'Purpose--Get COMP_LISTED_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlCOMP_LISTED_M As String = ""

        sqlCOMP_LISTED_M = "SELECT      COMP_NAME, COMP_ADDR1, COMP_ADDR2, COMP_CITY, COMP_COUNTRY, COMP_POBOX, COMP_EMIRATE, COMP_PHONE, COMP_FAX," & _
                      " COMP_EMAIL, COMP_URL, COMP_CONTACT, COMP_CONTACT_MOBILE  FROM  COMP_LISTED_M where COMP_ID='" & COMP_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlCOMP_LISTED_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function getStudent_Metric(ByVal BSU_ID As String, ByVal ACD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --29/May/2008
        'Purpose--Get Student  Metric
        'modified leave date 11/08/2008
        'Modified Bisuness unit wise filter excluded in Corporate Users
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlStudent_Metric As String = ""
        'Convert.ToDateTime(txtToDate.Text).ToString("dd/MMM/yyyy")
        Dim Firstday_Month As String = Convert.ToDateTime(DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, DateTime.Now), Date.MinValue)).ToString("dd/MMM/yyyy")
        Dim Current_Date As String = Convert.ToDateTime(DateTime.Now).ToString("dd/MMM/yyyy")
        Dim CurrentDayMinus1 As String = Convert.ToDateTime(DateAdd(DateInterval.Day, -1, DateTime.Now)).ToString("dd/MMM/yyyy")

        Dim strBsuFilter As String = " AND (STU_BSU_ID =  '" & BSU_ID & "') "
        If BSU_ID = "999998" Or BSU_ID = "999999" Then
            strBsuFilter = " "
            sqlStudent_Metric = "  select (a.Beg_Date+a.Add_Date-a.Canceled_Date-a.Delete_Date) as Today_Date,* from " & _
                " (select distinct " & _
                " (Select count(STU_ID) from Student_M INNER JOIN ACADEMICYEAR_D ON ACD_ID=stu_acd_ID where (STU_DOJ< '" & Firstday_Month & "' and ACD_CURRENT=1) and " & _
                " (STU_CURRSTATUS NOT IN ('CN','TF') or (STU_CURRSTATUS ='CN' AND STU_CANCELDATE>='" & Firstday_Month & "')) " & _
                " And (convert(datetime,STU_LEAVEDATE) >='" & Firstday_Month & "' or convert(datetime,STU_LEAVEDATE) is null)) as Beg_Date," & _
                " (SELECT count(STU_ID) FROM STUDENT_M INNER JOIN ACADEMICYEAR_D ON ACD_ID=stu_acd_ID WHERE (STU_DOJ BETWEEN '" & Firstday_Month & "' AND '" & Current_Date & "') " & _
                "  " & strBsuFilter & " AND (ACD_CURRENT=1))  AS Add_Date, " & _
                " (SELECT count(STU_ID) FROM  STUDENT_M INNER JOIN ACADEMICYEAR_D ON ACD_ID=stu_acd_ID WHERE " & _
                "  (STU_CURRSTATUS = 'CN') " & strBsuFilter & " AND (ACD_CURRENT=1)  " & _
                " and (STU_CANCELDATE BETWEEN  '" & Firstday_Month & "' AND '" & Current_Date & "'))AS Canceled_Date, " & _
                "  (SELECT count(STU_ID) FROM  STUDENT_M INNER JOIN ACADEMICYEAR_D ON ACD_ID=stu_acd_ID WHERE  (convert(datetime,STU_LEAVEDATE) between '" & Firstday_Month & "' AND '" & Current_Date & "')  " & _
                " " & strBsuFilter & " AND (ACD_CURRENT=1) AND  (STU_CURRSTATUS <> 'CN'))   AS Delete_Date, " & _
                "  (SELECT count(STU_ID) FROM STUDENT_M INNER JOIN ACADEMICYEAR_D ON ACD_ID=stu_acd_ID WHERE  (STU_CURRSTATUS = 'EN') AND (STU_DOJ > '" & Current_Date & "') " & strBsuFilter & " " & _
                "  AND (ACD_CURRENT=1)) AS Future_Date,0 AS Next_date  " & _
                "  from student_M ) a "
        ElseIf BSU_ID = "900201" Then
            sqlStudent_Metric = "  select (a.Beg_Date+a.Add_Date-a.Canceled_Date-a.Delete_Date) as Today_Date,* from " & _
                " (select distinct " & _
                " (Select count(STU_ID) from Student_M where (STU_DOJ< '" & Firstday_Month & "' and stu_acd_ID='" & ACD_ID & "') and " & _
                " (STU_CURRSTATUS NOT IN ('CN','TF') or (STU_CURRSTATUS ='CN' AND STU_CANCELDATE>='" & Firstday_Month & "')) " & _
                " And (convert(datetime,STU_LEAVEDATE) >='" & Firstday_Month & "' or convert(datetime,STU_LEAVEDATE) is null)) as Beg_Date," & _
                " (SELECT count(STU_ID) FROM STUDENT_M WHERE (STU_DOJ BETWEEN '" & Firstday_Month & "' AND '" & Current_Date & "') " & _
                "  " & strBsuFilter & " AND (STU_ACD_ID='" & ACD_ID & "'))  AS Add_Date, " & _
                " (SELECT count(STU_ID) FROM  STUDENT_M WHERE " & _
" (STU_ACD_ID='" & ACD_ID & "')  " & strBsuFilter & " AND " & _
" ((STU_DOJ BETWEEN '" & Firstday_Month & "' AND '" & Current_Date & "' AND (STU_CURRSTATUS = 'CN') OR " & _
" (STU_DOJ<'" & Firstday_Month & "' AND STU_CANCELDATE BETWEEN  '" & Firstday_Month & "' AND '" & Current_Date & "')) " & _
                " )) AS Canceled_Date, " & _
                "  (SELECT count(STU_ID) FROM  STUDENT_M WHERE  (convert(datetime,STU_LEAVEDATE) between '" & Firstday_Month & "' AND '" & CurrentDayMinus1 & "')  " & _
                " " & strBsuFilter & " AND (STU_ACD_ID='" & ACD_ID & "') AND  (STU_CURRSTATUS <> 'CN'))   AS Delete_Date, " & _
                "  (SELECT count(STU_ID) FROM STUDENT_M WHERE  (STU_CURRSTATUS = 'EN') AND (STU_DOJ > '" & Current_Date & "') " & strBsuFilter & " " & _
                "  AND (stu_acd_ID='" & ACD_ID & "')) AS Future_Date , " & _
                "  (SELECT count(STU_ID) FROM STUDENT_M WHERE  (STU_CURRSTATUS = 'EN') AND (STU_DOJ > '" & Current_Date & "') " & strBsuFilter & " " & _
                "  AND (stu_acd_ID=( select acd_id from ACADEMICYEAR_D where  acd_acy_id=(" & _
                " select acd_acy_id from ACADEMICYEAR_D where acd_bsu_id='" & BSU_ID & "' and acd_id=" & ACD_ID & ")+1 and " & _
                " acd_bsu_id='" & BSU_ID & "'))) AS Next_date  " & _
                "  from student_M ) a "
        Else
            sqlStudent_Metric = "  select (a.Beg_Date+a.Add_Date-a.Canceled_Date-a.Delete_Date) as Today_Date,* from " & _
                " (select distinct " & _
                " (Select count(STU_ID) from Student_M where (STU_DOJ< '" & Firstday_Month & "' and stu_acd_ID='" & ACD_ID & "') and " & _
                " (STU_CURRSTATUS NOT IN ('CN','TF') or (STU_CURRSTATUS ='CN' AND STU_CANCELDATE>='" & Firstday_Month & "')) " & _
                " And (convert(datetime,STU_LEAVEDATE) >='" & Firstday_Month & "' or convert(datetime,STU_LEAVEDATE) is null)) as Beg_Date," & _
                " (SELECT count(STU_ID) FROM STUDENT_M WHERE (STU_DOJ BETWEEN '" & Firstday_Month & "' AND '" & Current_Date & "') " & _
                "  " & strBsuFilter & " AND (STU_ACD_ID='" & ACD_ID & "'))  AS Add_Date, " & _
                " (SELECT count(STU_ID) FROM  STUDENT_M WHERE " & _
" (STU_ACD_ID='" & ACD_ID & "')  " & strBsuFilter & " AND " & _
" ((STU_DOJ BETWEEN '" & Firstday_Month & "' AND '" & Current_Date & "' AND (STU_CURRSTATUS = 'CN') OR " & _
" (STU_DOJ<'" & Firstday_Month & "' AND STU_CANCELDATE BETWEEN  '" & Firstday_Month & "' AND '" & Current_Date & "')) " & _
                " )) AS Canceled_Date, " & _
                "  (SELECT count(STU_ID) FROM  STUDENT_M WHERE  (convert(datetime,STU_LEAVEDATE) between '" & Firstday_Month & "' AND '" & CurrentDayMinus1 & "')  " & _
                " " & strBsuFilter & " AND (STU_ACD_ID='" & ACD_ID & "') AND  (STU_CURRSTATUS <> 'CN'))   AS Delete_Date, " & _
                "  (SELECT count(STU_ID) FROM STUDENT_M WHERE  (STU_CURRSTATUS = 'EN') AND (STU_DOJ > '" & Current_Date & "') " & strBsuFilter & " " & _
                "  AND (stu_acd_ID='" & ACD_ID & "')) AS Future_Date,0 AS Next_date " & _
                           "  from student_M ) a "
        End If

        '        sqlStudent_Metric = " select (a.today_date+a.delete_date+a.canceled_date-a.add_date) as Beg_Date,* from (select distinct " & _
        '" (SELECT count(STU_CURRSTATUS) FROM STUDENT_M WHERE(STU_DOJ > '" & Firstday_Month & "') AND (STU_DOJ <= '" & Current_Date & "') AND (STU_CURRSTATUS = 'EN') AND (STU_BSU_ID = '" & BSU_ID & "') and (stu_acd_ID='" & ACD_ID & "'))  AS Add_Date," & _
        '" (SELECT count(STU_CURRSTATUS) FROM  STUDENT_M WHERE(STU_DOJ >= '" & Firstday_Month & "') AND (STU_CURRSTATUS = 'CN') AND (STU_DOJ <= '" & Current_Date & "')AND (STU_BSU_ID = '" & BSU_ID & "') and (stu_acd_ID='" & ACD_ID & "'))AS Canceled_Date," & _
        '" (SELECT count(STU_CURRSTATUS) FROM  STUDENT_M WHERE(STU_DOJ > ='" & Firstday_Month & "') AND (STU_CURRSTATUS = 'TC')or (STU_CURRSTATUS = 'SO') AND (STU_DOJ <= '" & Current_Date & "')AND (STU_BSU_ID = '" & BSU_ID & "')and (stu_acd_ID='" & ACD_ID & "'))AS Delete_Date," & _
        '"(SELECT count(STU_CURRSTATUS) FROM STUDENT_M WHERE (STU_CURRSTATUS = 'EN') AND (STU_DOJ <= '" & Current_Date & "')AND (STU_BSU_ID = '" & BSU_ID & "')and (stu_acd_ID='" & ACD_ID & "'))AS Today_Date, " & _
        '"(SELECT count(STU_CURRSTATUS) FROM STUDENT_M WHERE (STU_CURRSTATUS = 'EN') AND (STU_DOJ > '" & Current_Date & "')AND (STU_BSU_ID = '" & BSU_ID & "') and (stu_acd_ID='" & ACD_ID & "')) AS Future_Date " & _
        '" from student_M) a"

        Dim command As SqlCommand = New SqlCommand(sqlStudent_Metric, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function getTC_TFRTYPE_M() As SqlDataReader
        'Author(--Lijo)
        'Date   --31/Mar/2008
        'Purpose--Get transfer details based on TC_TFRTYPE_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTC_TFRTYPE_M As String = ""

        sqlGetTC_TFRTYPE_M = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [TC_TFRTYPE_M] order by [TCT_DESCR]"
        Dim command As SqlCommand = New SqlCommand(sqlGetTC_TFRTYPE_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function getTC_REASONS_M() As SqlDataReader
        'Author(--Lijo)
        'Date   --31/Mar/2008
        'Purpose--Get transfer details based on TC_REASONS_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTC_TFRTYPE_M As String = ""

        sqlGetTC_TFRTYPE_M = "SELECT [TCR_CODE],[TCR_DESCR]  FROM [TC_REASONS_M] order by [TCR_DESCR]"
        Dim command As SqlCommand = New SqlCommand(sqlGetTC_TFRTYPE_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function


    Public Shared Function getTCSO_STU_ID(ByVal STU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --30/Mar/2008
        'Purpose--Get student dtails based on STU_ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTCSO_STU_ID As String = ""
        sqlGetTCSO_STU_ID = " SELECT  STUDENT_M.STU_ACD_ID, STUDENT_M.STU_GRD_ID, STUDENT_M.STU_SCT_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') " & _
                      " AS SFIRSTNAME, ISNULL(UPPER(STUDENT_M.STU_MIDNAME), '') AS SMIDNAME, ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS SLASTNAME, " & _
                     "  ISNULL(GRADE_BSU_M.GRM_DISPLAY, '') AS Stu_Grade, ISNULL(SECTION_M.SCT_DESCR, '') AS Stu_Section, " & _
                      " CASE STUDENT_M.STU_CURRSTATUS WHEN 'EN' THEN 'ENROLL' WHEN 'TC' THEN 'TC' WHEN 'SO' THEN 'Strike Off' END AS Stu_Status, " & _
                      " STUDENT_M.STU_FEE_ID AS Fee_ID, STUDENT_M.STU_TCISSUEDATE AS TCISSUEDATE, STUDENT_M.STU_TCDATE AS TCDATE, " & _
                      " STUDENT_M.STU_LASTATTDATE AS LASTATTDATE, STUDENT_M.STU_LEAVEDATE AS LEAVEDATE, STUDENT_M.STU_SODATE AS SODATE, " & _
                      " STUDENT_M.STU_DOJ AS DOJ, ACADEMICYEAR_D.ACD_STARTDT, ACADEMICYEAR_D.ACD_ENDDT " & _
                      "  FROM  STUDENT_M INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID INNER JOIN " & _
                      " ACADEMICYEAR_D ON STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
                      " GRADE_BSU_M ON STUDENT_M.STU_GRM_ID = GRADE_BSU_M.GRM_ID WHERE STUDENT_M.STU_ID='" & STU_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlGetTCSO_STU_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function getNEXT_CURR_PREV(ByVal ACD_BSU_ID As String, ByVal ACD_CLM_ID As String, ByVal ACD_ACY_ID As Integer) As SqlDataReader
        'Author(--Lijo)
        'Date   --27/Mar/2008
        'Purpose--Get the next,current,previous ACD_ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetACD_ID As String = ""
        Dim nextACD_ACY_ID As Integer = ACD_ACY_ID + 1
        Dim PrevACD_ACY_ID As Integer = ACD_ACY_ID - 1


        sqlGetACD_ID = "select (select ACD_ID  from ACADEMICYEAR_D where ACD_BSU_ID='" & ACD_BSU_ID & "' and ACD_CLM_ID='" & ACD_CLM_ID & "' and ACD_ACY_ID='" & PrevACD_ACY_ID & "') as prev_ACD_ID," & _
              " (select  ACD_ID  from ACADEMICYEAR_D where ACD_BSU_ID='" & ACD_BSU_ID & "' and ACD_CLM_ID='" & ACD_CLM_ID & "' and ACD_ACY_ID='" & nextACD_ACY_ID & "')as next_ACD_ID," & _
              " (select ACD_ID  from ACADEMICYEAR_D where ACD_BSU_ID='" & ACD_BSU_ID & "' and ACD_CLM_ID='" & ACD_CLM_ID & "' and ACD_ACY_ID='" & ACD_ACY_ID & "')as curr_ACD_ID"

        Dim command As SqlCommand = New SqlCommand(sqlGetACD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)

        Return reader
    End Function
    Public Shared Function GetCompany_Name() As SqlDataReader
        'Author(--Lijo)
        'Date   --25/May/2008
        'Purpose--Get Company Name
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetComp_Name As String = ""

        sqlGetComp_Name = "Select comp_ID,comp_Name from comp_Listed_M order by comp_Name"


        Dim command As SqlCommand = New SqlCommand(sqlGetComp_Name, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)

        Return reader

    End Function

    Public Shared Function GetEnquiry_open_for(ByVal GRD_ID As String, ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String) As SqlDataReader 'new code for on line enqury
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim CurrDate As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Dim sqlGetEnquiry_open_for As String = ""

        sqlGetEnquiry_open_for = "  SELECT * FROM(SELECT  ENQUIRY_SETTINGS.EQS_EQO_ID, ENQUIRY_OPTION.EQO_ID,   ENQUIRY_OPTION.EQO_DESC, ENQUIRY_SETTINGS.EQS_ACD_ID, ENQUIRY_SETTINGS.EQS_GRD_ID, ENQUIRY_SETTINGS.EQS_FROMDT, " & _
 " ENQUIRY_SETTINGS.EQS_TODT, ENQUIRY_SETTINGS.EQS_BSU_IDS,ENQUIRY_SETTINGS.EQS_MSG1,ENQUIRY_SETTINGS.EQS_MSG2 FROM  ENQUIRY_OPTION INNER JOIN " & _
 "  ENQUIRY_SETTINGS ON ENQUIRY_OPTION.EQO_ID = ENQUIRY_SETTINGS.EQS_EQO_ID)A WHERE   A.EQS_GRD_ID='" & GRD_ID & "' " & _
 " AND CONVERT(DATETIME,'" & CurrDate & "') BETWEEN A.EQS_FROMDT AND A.EQS_TODT and A.EQS_ACD_ID=(select " & _
 " distinct ACD_ID FROM ACADEMICYEAR_D WHERE ACD_ACY_ID='" & ACY_ID & "' AND ACD_CLM_ID='" & CLM_ID & "' AND ACD_BSU_ID='" & BSU_ID & "') "

        Dim command As SqlCommand = New SqlCommand(sqlGetEnquiry_open_for, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Public Shared Function GetEnquiry_ApplAck(ByVal APPLNO As String, ByVal ENQID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/Mar/2008
        'Purpose--Get student details from Student_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEnquiry_ApplAck As String = ""

        sqlGetEnquiry_ApplAck = "SELECT    ENQUIRY_SCHOOLPRIO_S.EQS_ID as  EQS_ID, ENQUIRY_M.EQM_ENQID AS ENQID, ISNULL(REPLACE(CONVERT(VARCHAR(11), ENQUIRY_M.EQM_APPLDOB, 106), ' ', '/'),'')  AS  EQS_DOJ,ENQUIRY_M.ENQ_APPLPASSPORTNAME AS APPLPASSPORTNAME, " & _
                      " ISNULL(vw_OSO_STUDENQ_PARENT.EQP_FFIRSTNAME, '') + ' ' + ISNULL(vw_OSO_STUDENQ_PARENT.EQP_FMIDNAME, '') " & _
                      " + ' ' + ISNULL(vw_OSO_STUDENQ_PARENT.EQP_FLASTNAME, '') AS FNAME, vw_OSO_STUDENQ_PARENT.EQP_FCOMSTREET AS EQP_FCOMSTREET, " & _
                     "  vw_OSO_STUDENQ_PARENT.EQP_FCOMAREA AS EQP_FCOMAREA,vw_OSO_STUDENQ_PARENT.EQP_FCOMBLDG AS EQP_FCOMBLDG,vw_OSO_STUDENQ_PARENT.EQP_FCOMAPARTNO AS EQP_FCOMAPARTNO, vw_OSO_STUDENQ_PARENT.FMOBILE, " & _
                     "  vw_OSO_STUDENQ_PARENT.EQP_FEMAIL AS FEMAIL, vw_OSO_STUDENQ_PARENT.City, vw_OSO_STUDENQ_PARENT.Country, " & _
                     "  vw_OSO_STUDENQ_PARENT.COMPOBOX, BUSINESSUNIT_M.BSU_NAME, SHIFTS_M.SHF_DESCR, STREAM_M.STM_DESCR, " & _
                     " ACADEMICYEAR_M.ACY_DESCR, GRADE_BSU_M.GRM_DISPLAY, CURRICULUM_M.CLM_DESCR,BUSINESSUNIT_M.BSU_ID " & _
                     " FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
                      " CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID INNER JOIN " & _
                      " ENQUIRY_M INNER JOIN ENQUIRY_SCHOOLPRIO_S ON ENQUIRY_M.EQM_ENQID = ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID INNER JOIN" & _
                      " vw_OSO_STUDENQ_PARENT ON ENQUIRY_M.EQM_ENQID = vw_OSO_STUDENQ_PARENT.EQP_EQM_ENQID AND ENQUIRY_M.EQM_PRIMARYCONTACT=vw_OSO_STUDENQ_PARENT.FType INNER JOIN " & _
                      " BUSINESSUNIT_M ON ENQUIRY_SCHOOLPRIO_S.EQS_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN " & _
                      " STREAM_M ON ENQUIRY_SCHOOLPRIO_S.EQS_STM_ID = STREAM_M.STM_ID ON " & _
                      " ACADEMICYEAR_D.ACD_ID = ENQUIRY_SCHOOLPRIO_S.EQS_ACD_ID INNER JOIN " & _
                      " GRADE_BSU_M ON ENQUIRY_SCHOOLPRIO_S.EQS_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN " & _
                      " SHIFTS_M ON ENQUIRY_SCHOOLPRIO_S.EQS_SHF_ID = SHIFTS_M.SHF_ID " & _
                     " where ENQUIRY_SCHOOLPRIO_S.EQS_APPLNO ='" & APPLNO & "' and ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID='" & ENQID & "'"



        Dim command As SqlCommand = New SqlCommand(sqlGetEnquiry_ApplAck, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetEnq_Ack_MAIN(ByVal APPLNO As String, ByVal ENQID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --27/OCT/2008
        'Purpose--Get student details from Student_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEnq_Ack_MAIN As String = ""

        sqlGetEnq_Ack_MAIN = " select distinct EAS_TEXT_MAIN,EAS_FOOTER_MAIN,EAS_FOOTER_MAIN2 from ENQUIRY_ACK_SETTING where EAS_MAIN_bSHOW=1 and " & _
 " EAS_ACD_ID=(select distinct EQS_ACD_ID from  ENQUIRY_SCHOOLPRIO_S where EQS_EQM_ENQID='" & ENQID & "'   and  EQS_APPLNO='" & APPLNO & "' )" & _
" and  EAS_GRD_ID =(select distinct EQS_GRD_ID from ENQUIRY_SCHOOLPRIO_S where EQS_EQM_ENQID='" & ENQID & "'   and  EQS_APPLNO='" & APPLNO & "' )"



        Dim command As SqlCommand = New SqlCommand(sqlGetEnq_Ack_MAIN, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function





    Public Shared Function GetEnq_Ack_SUB(ByVal APPLNO As String, ByVal ENQID As String) As DataSet
        'Author(--Lijo)
        'Date   --27/OCT/2008
        'Purpose--Get student details from Student_D
        Dim ds1 As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEnq_Ack_SUB As String = ""

        sqlGetEnq_Ack_SUB = " SELECT  ENQUIRY_ACK_DOC.EAD_TEXT,isnull(ENQUIRY_ACK_SETTING.EAS_bBULLET_POINTS,'false') as EAS_bBULLET_POINTS  FROM  ENQUIRY_ACK_SETTING INNER JOIN " & _
" ENQUIRY_ACK_DOC ON ENQUIRY_ACK_SETTING.EAS_ID = ENQUIRY_ACK_DOC.EAD_EAS_ID where ENQUIRY_ACK_SETTING.EAS_SUB_bSHOW=1 " & _
" and ENQUIRY_ACK_SETTING.EAS_ACD_ID=(select distinct EQS_ACD_ID FROM ENQUIRY_SCHOOLPRIO_S where EQS_EQM_ENQID='" & ENQID & "'   and  EQS_APPLNO='" & APPLNO & "' )" & _
" and ENQUIRY_ACK_SETTING.EAS_GRD_ID=(select distinct EQS_GRD_ID  FROM ENQUIRY_SCHOOLPRIO_S where EQS_EQM_ENQID ='" & ENQID & "'  and  EQS_APPLNO='" & APPLNO & "' ) " & _
" order by ENQUIRY_ACK_DOC.EAD_DISPLAY_ORDER"

        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, sqlGetEnq_Ack_SUB)
        SqlConnection.ClearPool(connection)

        Return ds1
    End Function
    Public Shared Function GetDuplicate_Enquiry_M(ByVal APPLPASPRTNO As String, ByVal SHF_ID As String, ByVal STM_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String, ByVal ACY_ID As String, ByVal CLM_ID As String) As Integer
        ' Author(--Lijo)
        'Date   --11/Mar/2008
        'Purpose--Checking 4 duplicate entry in the Enquiry

        Dim sqlString As String = " SELECT Count(*)  FROM  ENQUIRY_M INNER JOIN ENQUIRY_SCHOOLPRIO_S ON " & _
" ENQUIRY_M.EQM_ENQID = ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID where ENQUIRY_M.EQM_APPLPASPRTNO='" & APPLPASPRTNO & "' and  " & _
" ENQUIRY_SCHOOLPRIO_S.EQS_BSU_ID ='" & BSU_ID & "' and ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID='" & GRD_ID & "' and ENQUIRY_SCHOOLPRIO_S.EQS_STM_ID='" & STM_ID & "' " & _
" and  ENQUIRY_SCHOOLPRIO_S.EQS_ACD_ID=(SELECT distinct [ACD_ID]FROM [ACADEMICYEAR_D] " & _
" where ACD_BSU_ID='" & BSU_ID & "' and ACD_CLM_ID='" & CLM_ID & "' and ACD_ACY_ID='" & ACY_ID & "')"





        '"SELECT Count(*) " & _
        '" FROM  ENQUIRY_M INNER JOIN ENQUIRY_SCHOOLPRIO_S ON ENQUIRY_M.EQM_ENQID = ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID where ENQUIRY_M.EQM_APPLPASPRTNO='" & APPLPASPRTNO & "' and " & _
        '" ENQUIRY_SCHOOLPRIO_S.EQS_BSU_ID ='" & BSU_ID & "' and ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID='" & GRD_ID & "' and ENQUIRY_SCHOOLPRIO_S.EQS_STM_ID='" & STM_ID & "' and " & _
        '" ENQUIRY_SCHOOLPRIO_S.EQS_ACD_ID=(SELECT  distinct GRM_ACD_ID FROM [GRADE_BSU_M] where [GRM_SHF_ID]= '" & SHF_ID & "'  and GRM_BSU_ID ='" & BSU_ID & "' and GRM_ACY_ID= '" & ACY_ID & "' and GRM_GRD_ID='" & GRD_ID & "') " & _
        '" and '" & CLM_ID & "'=(SELECT [ACD_CLM_ID]FROM [ACADEMICYEAR_D] where " & _
        '" [ACD_ID]=(SELECT  distinct GRM_ACD_ID FROM [GRADE_BSU_M] where [GRM_SHF_ID]= '" & SHF_ID & "' and GRM_BSU_ID ='" & BSU_ID & "' and GRM_ACY_ID= '" & ACY_ID & "' and GRM_GRD_ID='" & GRD_ID & "'))"





        '"select max(*) from Enquiry_M where EQM_APPLPASPRTNO='" & EQM_APPLPASPRTNO & "'"
        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CInt(result)
    End Function

    Public Shared Function Enquiry_Parent_Details(ByVal ENQ_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2009
        'Purpose--Get student details from Student_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEQS_Detail As String = ""

        sqlGetEQS_Detail = " SELECT     EQP_ID, EQP_EQM_ENQID, EQP_FCOMP_ID, EQP_FCOMPANY, EQP_MCOMP_ID, EQP_MCOMPANY," & _
 " EQP_GCOMP_ID, EQP_GCOMPANY, EQP_FEESPONSOR FROM ENQUIRY_PARENT_M WHERE EQP_EQM_ENQID='" & ENQ_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetEQS_Detail, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetStudent_D(ByVal SSIBLING_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/Mar/2008
        'Purpose--Get student details from Student_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetStudent_D As String = ""

        'Query Modified by dhanya 13-07-08 (moved previousschool information from student_d to student_m)

        'sqlGetStudent_D = " SELECT  STS_FFIRSTNAME  , STS_FMIDNAME , STS_FLASTNAME , STS_FNATIONALITY  , STS_FNATIONALITY2, STS_FCOMADDR1 ,   " & _
        '              " STS_FCOMADDR2 ,STS_FCOMPOBOX , STS_FCOMCITY STS_FCOMCITY, STS_FCOMSTATE ,STS_FCOMCOUNTRY  , STS_FOFFPHONE , STS_FRESPHONE,STS_FFAX, " & _
        '              " STS_FMOBILE , STS_FPRMADDR1 , STS_FPRMADDR2 , STS_FPRMPOBOX , STS_FPRMCITY , STS_FPRMCOUNTRY , STS_FPRMPHONE , STS_FOCC , " & _
        '              " STS_FEMAIL,STS_FCOMPANY_ADDR, STS_bFGEMSEMP, STS_F_BSU_ID , STS_MFIRSTNAME , STS_MMIDNAME , STS_MLASTNAME , STS_MNATIONALITY ,   " & _
        '              " STS_MNATIONALITY2 , STS_MCOMADDR1, STS_MCOMADDR2 , STS_MCOMPOBOX , STS_MCOMCITY , STS_MCOMSTATE , STS_MCOMCOUNTRY ,  " & _
        '              " STS_MOFFPHONE  , STS_MRESPHONE , STS_MFAX , STS_MMOBILE, STS_MPRMADDR1 , STS_MPRMADDR2 , STS_MPRMPOBOX , STS_MPRMCITY ,   " & _
        '              " STS_MPRMCOUNTRY , STS_MPRMPHONE , STS_MOCC , STS_MCOMPANY ,STS_MEMAIL,STS_MCOMPANY_ADDR, STS_bMGEMSEMP, STS_M_BSU_ID , STS_GFIRSTNAME , STS_GMIDNAME ," & _
        '              " STS_GLASTNAME , STS_GNATIONALITY , STS_GNATIONALITY2 , STS_GCOMADDR1 , STS_GCOMADDR2 ,STS_GCOMPOBOX , STS_GCOMCITY ," & _
        '              " STS_GCOMSTATE , STS_GCOMCOUNTRY , STS_GOFFPHONE , STS_GRESPHONE , STS_GFAX , STS_GMOBILE , STS_GPRMADDR1 , STS_GPRMADDR2 ,  " & _
        '              " STS_GPRMPOBOX , STS_GPRMCITY , STS_GPRMCOUNTRY , STS_GPRMPHONE , STS_GOCC , STS_GCOMPANY ,STS_GEMAIL,STS_GCOMPANY_ADDR, STS_PREVSCHI , STS_PREVSCHI_CITY , " & _
        '              " STS_PREVSCHI_COUNTRY , STS_PREVSCHI_CLM , STS_PREVSCHI_GRADE , STS_PREVSCHI_LASTATTDATE , STS_PREVSCHII , STS_PREVSCHII_CITY ,  " & _
        '              " STS_PREVSCHII_COUNTRY  , STS_PREVSCHII_CLM  , STS_PREVSCHII_GRADE , STS_PREVSCHII_LASTATTDATE , STS_PREVSCHIII ,  " & _
        '              " STS_PREVSCHIII_CITY , STS_PREVSCHIII_COUNTRY  , STS_PREVSCHIII_CLM , STS_PREVSCHIII_GRADE , STS_PREVSCHIII_LASTATTDATE," & _
        '              " STS_FCOMPANY,STS_F_COMP_ID, STS_M_COMP_ID, STS_MCOMPANY,STS_G_COMP_ID, STS_GCOMPANY   FROM   STUDENT_D STUDENT_D where STS_STU_ID='" & SSIBLING_ID & "'"




        sqlGetStudent_D = " SELECT  STS_FFIRSTNAME  , STS_FMIDNAME , STS_FLASTNAME , STS_FNATIONALITY  , STS_FNATIONALITY2, STS_FCOMADDR1 ,   " & _
                         " STS_FCOMADDR2 ,STS_FCOMPOBOX , STS_FCOMCITY STS_FCOMCITY, STS_FCOMSTATE ,STS_FCOMCOUNTRY  , STS_FOFFPHONE , STS_FRESPHONE,STS_FFAX, " & _
                         " STS_FMOBILE , STS_FPRMADDR1 , STS_FPRMADDR2 , STS_FPRMPOBOX , STS_FPRMCITY , STS_FPRMCOUNTRY , STS_FPRMPHONE , STS_FOCC , " & _
                         " STS_FEMAIL,STS_FCOMPANY_ADDR, STS_bFGEMSEMP, STS_F_BSU_ID , STS_MFIRSTNAME , STS_MMIDNAME , STS_MLASTNAME , STS_MNATIONALITY ,   " & _
                         " STS_MNATIONALITY2 , STS_MCOMADDR1, STS_MCOMADDR2 , STS_MCOMPOBOX , STS_MCOMCITY , STS_MCOMSTATE , STS_MCOMCOUNTRY ,  " & _
                         " STS_MOFFPHONE  , STS_MRESPHONE , STS_MFAX , STS_MMOBILE, STS_MPRMADDR1 , STS_MPRMADDR2 , STS_MPRMPOBOX , STS_MPRMCITY ,   " & _
                         " STS_MPRMCOUNTRY , STS_MPRMPHONE , STS_MOCC , STS_MCOMPANY ,STS_MEMAIL,STS_MCOMPANY_ADDR, STS_bMGEMSEMP, STS_M_BSU_ID , STS_GFIRSTNAME , STS_GMIDNAME ," & _
                         " STS_GLASTNAME , STS_GNATIONALITY , STS_GNATIONALITY2 , STS_GCOMADDR1 , STS_GCOMADDR2 ,STS_GCOMPOBOX , STS_GCOMCITY ," & _
                         " STS_GCOMSTATE , STS_GCOMCOUNTRY , STS_GOFFPHONE , STS_GRESPHONE , STS_GFAX , STS_GMOBILE , STS_GPRMADDR1 , STS_GPRMADDR2 ,  " & _
                         " STS_GPRMPOBOX , STS_GPRMCITY , STS_GPRMCOUNTRY , STS_GPRMPHONE , STS_GOCC , STS_GCOMPANY ,STS_GEMAIL,STS_GCOMPANY_ADDR, " & _
                         " STS_FCOMPANY,STS_F_COMP_ID, STS_M_COMP_ID, STS_MCOMPANY,STS_G_COMP_ID, STS_GCOMPANY, " & _
                         " STS_FCOMSTREET,STS_FCOMAREA,STS_FCOMBLDG,STS_FCOMAPARTNO,STS_MCOMSTREET,STS_MCOMAREA,STS_MCOMBLDG,STS_MCOMAPARTNO, " & _
                         " STS_GCOMSTREET,STS_GCOMAREA,STS_GCOMBLDG,STS_GCOMAPARTNO,STS_FEMIR,STS_MEMIR,STS_GEMIR,STS_FEESPONSOR " & _
                         "   FROM   STUDENT_D STUDENT_D  where STS_STU_ID='" & SSIBLING_ID & "'"







        Dim command As SqlCommand = New SqlCommand(sqlGetStudent_D, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetStudentID_Transport(ByVal STU_ID As String, ByVal ACD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --27/Feb/2008
        'Purpose--Get student details from Student_M and Student_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetStudentID_Transport As String = ""

        '*****************************************************
        'sqlGetStudentID_Transport = "SELECT STUDENT_SERVICES_D.SSV_STU_ID, STUDENT_SERVICES_D.SSV_SVC_ID AS SVC_ID, STUDENT_SERVICES_D.SSV_ACD_ID, " & _
        '              " STUDENT_SERVICES_D.SSV_REMARKS, STUDENT_SERVICES_D.SSV_TODATE, TPTLOCATION_M.LOC_DESCRIPTION AS LOC_DESC, " & _
        '              "  TPTSUBLOCATION_M.SBL_DESCRIPTION AS SBL_DESC, TPTPICKUPPOINTS_M.PNT_DESCRIPTION AS PNT_DESC, " & _
        '              " STUDENT_M.STU_bUSETPT as STU_bUSETPT fROM   TPTSUBLOCATION_M RIGHT OUTER JOIN " & _
        '              " STUDENT_SERVICES_D INNER JOIN  TPTLOCATION_M ON STUDENT_SERVICES_D.SSV_LOC_ID = TPTLOCATION_M.LOC_ID INNER JOIN " & _
        '              " SERVICES_SYS_M ON STUDENT_SERVICES_D.SSV_SVC_ID = SERVICES_SYS_M.SVC_ID INNER JOIN " & _
        '              " STUDENT_M ON STUDENT_SERVICES_D.SSV_STU_ID = STUDENT_M.STU_ID LEFT OUTER JOIN " & _
        '              " TPTPICKUPPOINTS_M ON STUDENT_SERVICES_D.SSV_SBL_ID = TPTPICKUPPOINTS_M.PNT_SBL_ID AND  " & _
        '              " STUDENT_SERVICES_D.SSV_BSU_ID = TPTPICKUPPOINTS_M.PNT_BSU_ID AND " & _
        '              " STUDENT_SERVICES_D.SSV_PICKUP = TPTPICKUPPOINTS_M.PNT_ID ON " & _
        '              " TPTSUBLOCATION_M.SBL_BSU_ID = STUDENT_SERVICES_D.SSV_BSU_ID AND " & _
        '              " TPTSUBLOCATION_M.SBL_LOC_ID = STUDENT_SERVICES_D.SSV_LOC_ID And TPTSUBLOCATION_M.SBL_ID = STUDENT_SERVICES_D.SSV_SBL_ID " & _
        '              " where STUDENT_SERVICES_D.SSV_TODATE is  null and  STUDENT_SERVICES_D.SSV_ACD_ID='" & ACD_ID & "' and STUDENT_SERVICES_D.SSV_STU_ID='" & STU_ID & "'"

        '************************************************************
        'query modified by dhanya

        sqlGetStudentID_Transport = "SELECT  ISNULL(B.LOC_DESCRIPTION,'') AS PLOC,ISNULL(D.SBL_DESCRIPTION,'') PSBL,ISNULL(C.PNT_DESCRIPTION,'') AS PPICKUP," _
                                 & "ISNULL(E.LOC_DESCRIPTION,'') AS DLOC,ISNULL(F.SBL_DESCRIPTION,'') AS DSBL,ISNULL(G.PNT_DESCRIPTION,'') AS DPICKUP," _
                                 & " ISNULL(H.SSV_SVC_ID,0) AS SVC_ID,ISNULL(A.STU_bUSETPT,'FALSE') AS STU_bUSETPT,ISNULL(H.SSV_REMARKS,'') AS REMARKS" _
                                 & " FROM  STUDENT_M AS A " _
                                 & " INNER JOIN TRANSPORT.VV_PICKUPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
                                 & " INNER JOIN TRANSPORT.VV_SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " INNER JOIN TRANSPORT.VV_LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
                                 & " INNER JOIN TRANSPORT.VV_PICKUPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
                                 & " INNER JOIN TRANSPORT.VV_SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " INNER JOIN  TRANSPORT.VV_LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
                                 & " INNER JOIN STUDENT_SERVICES_D AS H ON H.SSV_STU_ID=A.STU_ID" _
                                 & " WHERE STU_ID =" & STU_ID & " AND SSV_ACD_ID=" & ACD_ID _
                                 & " AND SSV_TODATE IS NULL"



        Dim command As SqlCommand = New SqlCommand(sqlGetStudentID_Transport, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetStudent_M_DDetails(ByVal Stud_No As String, ByVal BSU_ID As String) As SqlDataReader

        'Purpose--Get student details from Student_M and Student_D


        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetStudent_Details As String = ""

        sqlGetStudent_Details = " SELECT distinct    STUDENT_M.STU_ID, STUDENT_M.STU_NO,STUDENT_M.STU_KNOWNNAME, STUDENT_M.STU_FIRSTNAME AS SFIRSTNAME_E, STUDENT_M.STU_MIDNAME AS SMIDNAME_E, " & _
                        "  STUDENT_M.STU_LASTNAME AS SLASTNAME_E, STUDENT_M.STU_FIRSTNAMEARABIC AS SFIRSTNAME_A, " & _
                        "  STUDENT_M.STU_MIDNAMEARABIC AS SMIDNAME_A, STUDENT_M.STU_LASTNAMEARABIC AS SLASTNAME_A, ISNULL " & _
                        "   ((SELECT     EQS_APPLNO FROM ENQUIRY_SCHOOLPRIO_S  WHERE (EQS_EQM_ENQID = STUDENT_M.STU_EQM_ENQID) AND (EQS_BSU_ID = '" + BSU_ID + "')), 0) AS EQS_APPLNO, " & _
                        "  STUDENT_M.STU_FEE_ID AS FEE_ID, STUDENT_M.STU_BLUEID AS BLUEID, STUDENT_M.STU_REGNDATE AS REGNDATE, " & _
                        "  STUDENT_M.STU_DOJ AS SDOJ, STUDENT_M.STU_MINDOJ AS MINDOJ, STUDENT_M.STU_DOB AS DOB, STUDENT_M.STU_GENDER AS GENDER, " & _
                        "  STUDENT_M.STU_RLG_ID AS RLG_ID, STUDENT_M.STU_NATIONALITY AS SNATIONALITY, STUDENT_M.STU_POB AS POB, " & _
                        "   STUDENT_M.STU_COB AS COB, STUDENT_M.STU_HOUSE_ID AS HOUSE, STUDENT_M.STU_MINLIST AS MINLIST, " & _
                        "   STUDENT_M.STU_MINLISTTYPE AS SMINLISTTYPE, STUDENT_M.STU_FEESPONSOR AS SFEESPONSOR, " & _
                        "   STUDENT_M.STU_EMGCONTACT AS SEMGCONTACT, STUDENT_M.STU_HEALTH AS SHEALTH, STUDENT_M.STU_PHYSICAL AS SPHYSICAL, " & _
                        "   STUDENT_M.STU_bRCVSPMEDICATION AS SbRCVSPMEDICATION, STUDENT_M.STU_SPMEDICATION AS SPMEDICATION, " & _
                        "   STUDENT_M.STU_bUSETPT AS SbUSETPT, STUDENT_M.STU_PICKUP_BUSNO AS PICKUP_BUSNO, " & _
                        "   STUDENT_M.STU_DROPOFF_BUSNO AS DROPOFF_BUSNO, STUDENT_M.STU_PICKUP AS SPICKUP, STUDENT_M.STU_DROPOFF AS SDROPOFF, " & _
                       "    STUDENT_M.STU_PASPRTNAME AS SPASPRTNAME, STUDENT_M.STU_PASPRTNO AS SPASPRTNO, " & _
                       "    STUDENT_M.STU_PASPRTISSPLACE AS SPASPRTISSPLACE, CASE upper(STUDENT_M.STU_CURRSTATUS) WHEN NULL " & _
                       "    THEN '' WHEN 'EN' THEN 'ENROLL' WHEN 'TC' THEN 'TC' WHEN 'OS' THEN 'STRIKE OFF' ELSE '' END AS S_CURRSTATUS, " & _
                       "    STUDENT_M.STU_SIBLING_ID AS SSIBLING_ID, STUDENT_M.STU_SPMEDICATION, SECTION_M.SCT_DESCR AS SCT_ID, " & _
                       "    ACADEMICYEAR_M.ACY_DESCR AS ACD_ID_Y, GRADE_BSU_M.GRM_DISPLAY AS GRD_ID, " & _
                       "    SHIFTS_M.SHF_DESCR, STREAM_M.STM_DESCR, ACADEMICYEAR_D.ACD_ID, " & _
                        "   STUDENT_M.STU_PASPRTISSPLACE_ARBIC, STUDENT_M.STU_PASPRTISSDATE, STUDENT_M.STU_PASPRTEXPDATE, STUDENT_M.STU_VISANO, " & _
                        "   STUDENT_M.STU_VISAISSPLACE, STUDENT_M.STU_VISAISSDATE, STUDENT_M.STU_VISAEXPDATE, STUDENT_M.STU_VISAISSAUTH, " & _
                        "   STUDENT_M.STU_BLOODGROUP, STUDENT_M.STU_HCNO, RTrim(STUDENT_M.STU_PRIMARYCONTACT) as STU_PRIMARYCONTACT, STUDENT_M.STU_PREFCONTACT, " & _
                        "   isnull(STUDENT_M.STU_PHOTOPATH,'') as STU_PHOTOPATH, STUDENT_M.STU_bRCVSMS, STUDENT_M.STU_bRCVMAIL, STUDENT_M.STU_bActive, " & _
                        "   STUDENT_M.STU_TFRTYPE AS STFRTYPE, TFRTYPE_M.TFR_DESCR, ISNULL(STUDENT_M.STU_PREVSCHI, '') AS STU_PREVSCHI, " & _
                        "   ISNULL(STUDENT_M.STU_PREVSCHI_CITY, '') AS STU_PREVSCHI_CITY, ISNULL(STUDENT_M.STU_PREVSCHI_COUNTRY, 0) " & _
                        "   AS STU_PREVSCHI_COUNTRY, ISNULL(STUDENT_M.STU_PREVSCHI_CLM, 0) AS STU_PREVSCHI_CLM, " & _
                        "   ISNULL(STUDENT_M.STU_PREVSCHI_GRADE, '') AS STU_PREVSCHI_GRADE, ISNULL(STUDENT_M.STU_PREVSCHI_LASTATTDATE, '01/01/1900') " & _
                        "   AS STU_PREVSCHI_LASTATTDATE, ISNULL(STUDENT_M.STU_PREVSCHII, '') AS STU_PREVSCHII, ISNULL(STUDENT_M.STU_PREVSCHII_CITY, '') " & _
                        "   AS STU_PREVSCHII_CITY, ISNULL(STUDENT_M.STU_PREVSCHII_COUNTRY, 0) AS STU_PREVSCHII_COUNTRY, " & _
                        "   ISNULL(STUDENT_M.STU_PREVSCHII_CLM, 0) AS STU_PREVSCHII_CLM, ISNULL(STUDENT_M.STU_PREVSCHII_GRADE, '') " & _
                        "   AS STU_PREVSCHII_GRADE, ISNULL(STUDENT_M.STU_PREVSCHII_LASTATTDATE, '01/01/1900') AS STU_PREVSCHII_LASTATTDATE, " & _
                        "   ISNULL(STUDENT_M.STU_PREVSCHIII, '') AS STU_PREVSCHIII, ISNULL(STUDENT_M.STU_PREVSCHIII_CITY, '') AS STU_PREVSCHIII_CITY, " & _
                        "   ISNULL(STUDENT_M.STU_PREVSCHIII_COUNTRY, 0) AS STU_PREVSCHIII_COUNTRY, ISNULL(STUDENT_M.STU_PREVSCHIII_CLM, 0) " & _
                        "   AS STU_PREVSCHIII_CLM, ISNULL(STUDENT_M.STU_PREVSCHIII_GRADE, '') AS STU_PREVSCHIII_GRADE, " & _
                        "   ISNULL(STUDENT_M.STU_PREVSCHIII_LASTATTDATE, '01/01/1900') AS STU_PREVSCHIII_LASTATTDATE, " & _
                        "  STUDENT_M.STU_ACD_ID_JOIN AS ACD_ID_JOIN_Y, STUDENT_M.STU_GRD_ID_JOIN AS GRD_ID_JOIN, STUDENT_M.STU_SCT_ID_JOIN as SCT_DESCR_JOIN, " & _
                        "  STUDENT_M.STU_STM_ID_JOIN AS STM_DESCR_JOIN, STUDENT_M.STU_SHF_JOIN AS SHF_DESCR_JOIN, " & _
                        "  ISNULL(STU_PREVSCHI_MEDIUM,'') AS STU_PREVSCHI_MEDIUM,ISNULL(STU_PREVSCHII_MEDIUM,'') AS STU_PREVSCHII_MEDIUM,ISNULL(STU_PREVSCHIII_MEDIUM,'') AS STU_PREVSCHIII_MEDIUM,ACADEMICYEAR_M.ACY_ID,STU_GRD_ID,STUDENT_M.STU_COMMENT,isnull(STU_REMARKS,'') as STU_REMARKS " & _
                        " ,ISNULL(STUDENT_M.STU_bSEN,0) as STU_bSEN,isnull(STUDENT_M.STU_SEN_REMARK,'') as STU_SEN_REMARK,isnull(STUDENT_M.STU_bEAL,0) as STU_bEAL,isnull(STUDENT_M.STU_EAL_REMARK,'') as STU_EAL_REMARK ,isnull(STUDENT_M.STU_FIRSTLANG,'') as STU_FIRSTLANG, isnull(STUDENT_M.STU_OTHLANG,'')as STU_OTHLANG ,ISNULL(STU_bRCVPUBL,1) AS STU_bRCVPUBL,STU_BB_REMARKS,STU_BB_LEAVEREASON,STU_BB_SECTION_DES, isNULL(STU_JOIN_RESULT,'NEW') as STU_JOIN_RESULT" & _
                         "   FROM  ACADEMICYEAR_M AS ACADEMICYEAR_M_JOIN CROSS JOIN  TFRTYPE_M RIGHT OUTER JOIN " & _
                        "   SECTION_M AS SECTION_M RIGHT OUTER JOIN ACADEMICYEAR_M INNER JOIN " & _
                        "   ACADEMICYEAR_D AS ACADEMICYEAR_D ON ACADEMICYEAR_M.ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID INNER JOIN " & _
                        "   STUDENT_M ON ACADEMICYEAR_D.ACD_ID = STUDENT_M.STU_ACD_ID LEFT OUTER JOIN " & _
                        "   GRADE_BSU_M AS GRADE_BSU_M ON STUDENT_M.STU_GRM_ID = GRADE_BSU_M.GRM_ID LEFT OUTER JOIN " & _
                        "   SHIFTS_M ON STUDENT_M.STU_SHF_ID = SHIFTS_M.SHF_ID LEFT OUTER JOIN " & _
                        " STREAM_M ON STUDENT_M.STU_STM_ID = STREAM_M.STM_ID ON SECTION_M.SCT_ID = STUDENT_M.STU_SCT_ID ON " & _
                        " TFRTYPE_M.TFR_CODE = STUDENT_M.STU_TFRTYPE WHERE STUDENT_M.STU_ID ='" & Stud_No & "'"




        Dim command As SqlCommand = New SqlCommand(sqlGetStudent_Details, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetApplic_info(ByVal EQM_ENQID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlApplic_info As String = String.Empty
        sqlApplic_info = " SELECT  ENQUIRY_M.EQM_STAFF_EMP_ID, ENQUIRY_M.EQM_FILLED_BY, ENQUIRY_M.EQM_AGENT_NAME, ENQUIRY_M.EQM_PREFCONTACT,ENQUIRY_M.EQM_AGENT_MOB, " & _
                      " ENQUIRY_M.EQM_PRIMARYCONTACT, ENQUIRY_PARENT_M.EQP_ID, ENQUIRY_PARENT_M.EQP_EQM_ENQID, " & _
                      " ENQUIRY_PARENT_M.EQP_FFIRSTNAME, ENQUIRY_PARENT_M.EQP_FMIDNAME, ENQUIRY_PARENT_M.EQP_FLASTNAME," & _
                      " ENQUIRY_PARENT_M.EQP_FNATIONALITY, ENQUIRY_PARENT_M.EQP_FNATIONALITY2, ENQUIRY_PARENT_M.EQP_FCOMADDR1, " & _
                      " ENQUIRY_PARENT_M.EQP_FCOMADDR2, ENQUIRY_PARENT_M.EQP_FCOMSTREET, ENQUIRY_PARENT_M.EQP_FCOMAREA, " & _
                      " ENQUIRY_PARENT_M.EQP_FCOMBLDG, ENQUIRY_PARENT_M.EQP_FCOMAPARTNO, ENQUIRY_PARENT_M.EQP_FCOMPOBOX, " & _
                      " ENQUIRY_PARENT_M.EQP_FCOMCITY, ENQUIRY_PARENT_M.EQP_FCOMSTATE, ENQUIRY_PARENT_M.EQP_FCOMCOUNTRY, " & _
                      " ENQUIRY_PARENT_M.EQP_FOFFPHONECODE, ENQUIRY_PARENT_M.EQP_FOFFPHONE, ENQUIRY_PARENT_M.EQP_FRESPHONECODE, " & _
                      " ENQUIRY_PARENT_M.EQP_FRESPHONE, ENQUIRY_PARENT_M.EQP_FFAXCODE, ENQUIRY_PARENT_M.EQP_FFAX, " & _
                      " ENQUIRY_PARENT_M.EQP_FMOBILECODE, ENQUIRY_PARENT_M.EQP_FMOBILE, ENQUIRY_PARENT_M.EQP_FPRMADDR1, " & _
                      " ENQUIRY_PARENT_M.EQP_FPRMADDR2, ENQUIRY_PARENT_M.EQP_FPRMPOBOX, ENQUIRY_PARENT_M.EQP_FPRMCITY," & _
                      " ENQUIRY_PARENT_M.EQP_FPRMCOUNTRY, ENQUIRY_PARENT_M.EQP_FPRMPHONE, ENQUIRY_PARENT_M.EQP_FOCC, " & _
                      " ENQUIRY_PARENT_M.EQP_FCOMP_ID, ENQUIRY_PARENT_M.EQP_FCOMPANY, ENQUIRY_PARENT_M.EQP_FEMAIL, " & _
                      " ENQUIRY_PARENT_M.EQP_bFGEMSSTAFF, ENQUIRY_PARENT_M.EQP_FBSU_ID, ENQUIRY_PARENT_M.EQP_BSU_ID_STAFF, " & _
                      " ENQUIRY_PARENT_M.EQP_FACD_YEAR, ENQUIRY_PARENT_M.EQP_FMODE_ID, ENQUIRY_PARENT_M.EQP_MFIRSTNAME, " & _
                      " ENQUIRY_PARENT_M.EQP_MMIDNAME, ENQUIRY_PARENT_M.EQP_MLASTNAME, ENQUIRY_PARENT_M.EQP_MNATIONALITY, " & _
                      " ENQUIRY_PARENT_M.EQP_MNATIONALITY2, ENQUIRY_PARENT_M.EQP_MCOMADDR1, ENQUIRY_PARENT_M.EQP_MCOMADDR2, " & _
                      " ENQUIRY_PARENT_M.EQP_MCOMSTREET, ENQUIRY_PARENT_M.EQP_MCOMAREA, ENQUIRY_PARENT_M.EQP_MCOMBLDG, " & _
                      " ENQUIRY_PARENT_M.EQP_MCOMAPARTNO, ENQUIRY_PARENT_M.EQP_MCOMPOBOX, ENQUIRY_PARENT_M.EQP_MCOMCITY, " & _
                      " ENQUIRY_PARENT_M.EQP_MCOMSTATE, ENQUIRY_PARENT_M.EQP_MCOMCOUNTRY, ENQUIRY_PARENT_M.EQP_MOFFPHONECODE, " & _
                      " ENQUIRY_PARENT_M.EQP_MOFFPHONE, ENQUIRY_PARENT_M.EQP_MRESPHONECODE, ENQUIRY_PARENT_M.EQP_MRESPHONE, " & _
                      " ENQUIRY_PARENT_M.EQP_MFAXCODE, ENQUIRY_PARENT_M.EQP_MFAX, ENQUIRY_PARENT_M.EQP_MMOBILECODE, " & _
                      " ENQUIRY_PARENT_M.EQP_MMOBILE, ENQUIRY_PARENT_M.EQP_MPRMADDR1, ENQUIRY_PARENT_M.EQP_MPRMADDR2, " & _
                      " ENQUIRY_PARENT_M.EQP_MPRMPOBOX, ENQUIRY_PARENT_M.EQP_MPRMCITY, ENQUIRY_PARENT_M.EQP_MPRMCOUNTRY, " & _
                      " ENQUIRY_PARENT_M.EQP_MPRMPHONE, ENQUIRY_PARENT_M.EQP_MOCC, ENQUIRY_PARENT_M.EQP_MCOMP_ID, " & _
                      " ENQUIRY_PARENT_M.EQP_MCOMPANY, ENQUIRY_PARENT_M.EQP_MEMAIL, ENQUIRY_PARENT_M.EQP_bMGEMSSTAFF, " & _
                      " ENQUIRY_PARENT_M.EQP_MBSU_ID, ENQUIRY_PARENT_M.EQP_MACD_YEAR, ENQUIRY_PARENT_M.EQP_MMODE_ID, " & _
                      " ENQUIRY_PARENT_M.EQP_GFIRSTNAME, ENQUIRY_PARENT_M.EQP_GMIDNAME, ENQUIRY_PARENT_M.EQP_GLASTNAME, " & _
                      " ENQUIRY_PARENT_M.EQP_GNATIONALITY, ENQUIRY_PARENT_M.EQP_GNATIONALITY2, ENQUIRY_PARENT_M.EQP_GCOMADDR1, " & _
                      " ENQUIRY_PARENT_M.EQP_GCOMADDR2, ENQUIRY_PARENT_M.EQP_GCOMSTREET, ENQUIRY_PARENT_M.EQP_GCOMAREA, " & _
                      " ENQUIRY_PARENT_M.EQP_GCOMBLDG, ENQUIRY_PARENT_M.EQP_GCOMAPARTNO, ENQUIRY_PARENT_M.EQP_GCOMPOBOX, " & _
                      " ENQUIRY_PARENT_M.EQP_GCOMCITY, ENQUIRY_PARENT_M.EQP_GCOMSTATE, ENQUIRY_PARENT_M.EQP_GCOMCOUNTRY, " & _
                      " ENQUIRY_PARENT_M.EQP_GOFFPHONECODE, ENQUIRY_PARENT_M.EQP_GOFFPHONE, ENQUIRY_PARENT_M.EQP_GRESPHONECODE, " & _
                      " ENQUIRY_PARENT_M.EQP_GRESPHONE, ENQUIRY_PARENT_M.EQP_GFAXCODE, ENQUIRY_PARENT_M.EQP_GFAX, " & _
                      " ENQUIRY_PARENT_M.EQP_GMOBILECODE, ENQUIRY_PARENT_M.EQP_GMOBILE, ENQUIRY_PARENT_M.EQP_GPRMADDR1, " & _
                      " ENQUIRY_PARENT_M.EQP_GPRMADDR2, ENQUIRY_PARENT_M.EQP_GPRMPOBOX, ENQUIRY_PARENT_M.EQP_GPRMCITY, " & _
                      " ENQUIRY_PARENT_M.EQP_GPRMCOUNTRY, ENQUIRY_PARENT_M.EQP_GPRMPHONE, ENQUIRY_PARENT_M.EQP_GOCC, " & _
                      " ENQUIRY_PARENT_M.EQP_GCOMP_ID, ENQUIRY_PARENT_M.EQP_GCOMPANY, ENQUIRY_PARENT_M.EQP_GEMAIL, " & _
                      " ENQUIRY_PARENT_M.EQP_bGGEMSSTAFF, ENQUIRY_PARENT_M.EQP_GBSU_ID, ENQUIRY_PARENT_M.EQP_GACD_YEAR, " & _
                      " ENQUIRY_PARENT_M.EQP_GMODE_ID, ENQUIRY_PARENT_M.EQP_FCOMPOBOX_EMIR, ENQUIRY_PARENT_M.EQP_MCOMPOBOX_EMIR, " & _
                      " ENQUIRY_PARENT_M.EQP_GCOMPOBOX_EMIR, ENQUIRY_PARENT_M.EQP_SCHOOL, ENQUIRY_PARENT_M.EQP_OLDENQID, " & _
                      " ENQUIRY_PARENT_M.EQP_ENQSL, ENQUIRY_M.EQM_bEXSTUDENT, ENQUIRY_M.EQM_EXUNIT, ENQUIRY_M.EQM_EX_STU_ID, " & _
                      " ENQUIRY_M.EQM_bSTAFFGEMS, ENQUIRY_M.EQM_STAFFUNIT, ENQUIRY_M.EQM_EQP_ID, ENQUIRY_M.EQM_MODE_ID " & _
                      " FROM  ENQUIRY_M INNER JOIN  ENQUIRY_PARENT_M ON ENQUIRY_M.EQM_ENQID = ENQUIRY_PARENT_M.EQP_EQM_ENQID where ENQUIRY_M.EQM_ENQID ='" & EQM_ENQID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlApplic_info, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function




    'CODE ADDED BY DHANYA 13 08 07
    Public Shared Function GetSiblingDetails(ByVal SiblingFeeId As String, ByVal BSU_ID As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim sqlSibling_Details As String = ""
        sqlSibling_Details = "SELECT STU_PRIMARYCONTACT,STU_PREFCONTACT,ISNULL(STS_FFIRSTNAME,'') AS STS_FFIRSTNAME, " _
                            & " ISNULL(STS_FMIDNAME,'') AS STS_FMIDNAME,ISNULL(STS_FLASTNAME,'') AS STS_FLASTNAME," _
                            & " ISNULL(STS_FNATIONALITY,0) AS STS_FNATIONALITY,ISNULL(STS_FNATIONALITY2,'') AS STS_FNATIONALITY2," _
                            & " ISNULL(STS_FCOMADDR1,'') AS  STS_FCOMADDR1,ISNULL(STS_FCOMADDR2,'') AS STS_FCOMADDR2, " _
                            & " ISNULL(STS_FCOMPOBOX,'') AS STS_FCOMPOBOX,ISNULL(STS_FCOMCITY,0) AS STS_FCOMCITY, " _
                            & " ISNULL(STS_FCOMSTATE,'') AS STS_FCOMSTATE,ISNULL(STS_FCOMCOUNTRY,0) AS STS_FCOMCOUNTRY, " _
                            & " ISNULL(STS_FOFFPHONE,'') AS STS_FOFFPHONE,ISNULL(STS_FRESPHONE,'') AS STS_FRESPHONE," _
                            & " ISNULL(STS_FFAX,'') AS STS_FFAX,ISNULL(STS_FMOBILE,'') AS STS_FMOBILE," _
                            & " ISNULL(STS_FPRMADDR1,'') AS STS_FPRMADDR1,ISNULL(STS_FPRMADDR2,'') AS STS_FPRMADDR2," _
                            & " ISNULL(STS_FPRMPOBOX,'') AS STS_FPRMPOBOX,ISNULL(STS_FPRMCITY,0) AS STS_FPRMCITY, " _
                            & " ISNULL(STS_FPRMCOUNTRY,'') AS STS_FPRMCOUNTRY,ISNULL(STS_FPRMPHONE,'') AS STS_FPRMPHONE , " _
                            & " ISNULL(STS_FOCC,'') AS STS_FOCC,ISNULL(STS_F_COMP_ID,0) AS STS_F_COMP_ID," _
                            & " ISNULL(STS_FCOMPANY,'') AS STS_FCOMPANY,ISNULL(STS_FCOMPANY_ADDR,'') AS STS_FCOMPANY_ADDR," _
                            & " ISNULL(STS_FEMAIL,'') AS STS_FEMAIL,ISNULL(STS_MFIRSTNAME,'') AS STS_MFIRSTNAME," _
                            & " ISNULL(STS_MMIDNAME,'') AS STS_MMIDNAME,ISNULL(STS_MLASTNAME,'') AS STS_MLASTNAME," _
                            & " ISNULL(STS_MNATIONALITY,0) AS STS_MNATIONALITY, ISNULL(STS_MNATIONALITY2,0) AS STS_MNATIONALITY2," _
                            & " ISNULL(STS_MCOMADDR1,'') AS STS_MCOMADDR1,ISNULL(STS_MCOMADDR2,'') AS STS_MCOMADDR2, " _
                            & " ISNULL(STS_MCOMPOBOX,'') AS STS_MCOMPOBOX,ISNULL(STS_MCOMCITY,0) AS STS_MCOMCITY," _
                            & " ISNULL(STS_MCOMSTATE,'') AS STS_MCOMSTATE,ISNULL(STS_MCOMCOUNTRY,0) AS STS_MCOMCOUNTRY," _
                            & " ISNULL(STS_MOFFPHONE,'') AS STS_MOFFPHONE,ISNULL(STS_MRESPHONE,'') AS STS_MRESPHONE," _
                            & " ISNULL(STS_MFAX,'') AS STS_MFAX,ISNULL(STS_MMOBILE,'') AS STS_MMOBILE," _
                            & " ISNULL(STS_MPRMADDR1,'') AS STS_MPRMADDR1,ISNULL(STS_MPRMADDR2,'') AS STS_MPRMADDR2," _
                            & " ISNULL(STS_MPRMPOBOX,'') AS STS_MPRMPOBOX,ISNULL(STS_MPRMCITY,'') AS STS_MPRMCITY," _
                            & " ISNULL(STS_MPRMCOUNTRY,'') AS STS_MPRMCOUNTRY,ISNULL(STS_MPRMPHONE,'') AS STS_MPRMPHONE ," _
                            & " ISNULL(STS_MOCC,'') AS STS_MOCC,ISNULL(STS_M_COMP_ID,0) AS STS_M_COMP_ID," _
                            & " ISNULL(STS_MCOMPANY,'') AS STS_MCOMPANY,ISNULL(STS_MCOMPANY_ADDR,'') AS STS_MCOMPANY_ADDR," _
                            & " ISNULL(STS_MEMAIL,'') AS STS_MEMAIL,ISNULL(STS_GFIRSTNAME,'') AS STS_GFIRSTNAME," _
                            & " ISNULL(STS_GMIDNAME,'') AS STS_GMIDNAME,ISNULL(STS_GLASTNAME,'') AS STS_GLASTNAME," _
                            & " ISNULL(STS_GNATIONALITY,0) AS STS_GNATIONALITY,ISNULL(STS_GNATIONALITY2,0) AS STS_GNATIONALITY2," _
                            & " ISNULL(STS_GCOMADDR1,'') AS STS_GCOMADDR1,ISNULL(STS_GCOMADDR2,'') AS STS_GCOMADDR2," _
                            & " ISNULL(STS_GCOMPOBOX,'') AS STS_GCOMPOBOX,ISNULL(STS_GCOMCITY,'') AS STS_GCOMCITY," _
                            & " ISNULL(STS_GCOMSTATE,'') AS STS_GCOMSTATE,ISNULL(STS_GCOMCOUNTRY,'') AS STS_GCOMCOUNTRY," _
                            & " ISNULL(STS_GOFFPHONE,'') AS STS_GOFFPHONE,ISNULL(STS_GRESPHONE,'') AS STS_GRESPHONE," _
                            & " ISNULL(STS_GFAX,'') AS STS_GFAX ,ISNULL(STS_GMOBILE,'') AS STS_GMOBILE,ISNULL(STS_GPRMADDR1,'') AS STS_GPRMADDR1," _
                            & " ISNULL(STS_GPRMADDR2,'') AS STS_GPRMADDR2,ISNULL(STS_GPRMPOBOX,'') AS STS_GPRMPOBOX,ISNULL(STS_GPRMCITY,'') AS STS_GPRMCITY," _
                            & " ISNULL(STS_GPRMCOUNTRY,'') AS STS_GPRMCOUNTRY,ISNULL(STS_GPRMPHONE,'') AS STS_GPRMPHONE,ISNULL(STS_GOCC,'') AS STS_GOCC," _
                            & " ISNULL(STS_G_COMP_ID,0) AS STS_G_COMP_ID,ISNULL(STS_GCOMPANY,'') AS STS_GCOMPANY,ISNULL(STS_GCOMPANY_ADDR,'') AS STS_GCOMPANY_ADDR," _
                            & " ISNULL(STS_GEMAIL,'') AS STS_GEMAIL,ISNULL(STS_PNAME_ARABIC,'') AS STS_PNAME_ARABIC," _
                            & " ISNULL(STS_POCC_ARABIC,'') AS STS_POCC_ARABIC,ISNULL(STS_PADDR_ARABIC,'') AS STS_PADDR_ARABIC," _
                            & " ISNULL(STS_POFFCADDR_ARABIC,'') AS STS_POFFCADDR_ARABIC FROM STUDENT_M AS A INNER JOIN STUDENT_D AS" _
                            & " B ON A.STU_SIBLING_ID=B.STS_STU_ID WHERE ((STU_FEE_ID='" + SiblingFeeId + "') OR (STU_NO='" + SiblingFeeId + "'))  AND STU_BSU_ID='" + BSU_ID + "'"

        Dim command As SqlCommand = New SqlCommand(sqlSibling_Details, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetSiblingDetails_Virtual(ByVal SiblingFeeId As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim sqlSibling_Details As String = ""
        sqlSibling_Details = "SELECT STU_PRIMARYCONTACT,ISNULL(STS_FFIRSTNAME,'') AS STS_FFIRSTNAME, " _
                            & " ISNULL(STS_FMIDNAME,'') AS STS_FMIDNAME,ISNULL(STS_FLASTNAME,'') AS STS_FLASTNAME," _
                            & " ISNULL(STS_FNATIONALITY,0) AS STS_FNATIONALITY,ISNULL(STS_FNATIONALITY2,'') AS STS_FNATIONALITY2," _
                            & " ISNULL(STS_FCOMADDR1,'') AS  STS_FCOMADDR1,ISNULL(STS_FCOMADDR2,'') AS STS_FCOMADDR2, " _
                            & " ISNULL(STS_FCOMPOBOX,'') AS STS_FCOMPOBOX,ISNULL(STS_FCOMCITY,0) AS STS_FCOMCITY, " _
                            & " ISNULL(STS_FCOMSTATE,'') AS STS_FCOMSTATE,ISNULL(STS_FCOMCOUNTRY,0) AS STS_FCOMCOUNTRY, " _
                            & " ISNULL(STS_FOFFPHONE,'') AS STS_FOFFPHONE,ISNULL(STS_FRESPHONE,'') AS STS_FRESPHONE," _
                            & " ISNULL(STS_FFAX,'') AS STS_FFAX,ISNULL(STS_FMOBILE,'') AS STS_FMOBILE," _
                            & " ISNULL(STS_FPRMADDR1,'') AS STS_FPRMADDR1,ISNULL(STS_FPRMADDR2,'') AS STS_FPRMADDR2," _
                            & " ISNULL(STS_FPRMPOBOX,'') AS STS_FPRMPOBOX,ISNULL(STS_FPRMCITY,0) AS STS_FPRMCITY, " _
                            & " ISNULL(STS_FPRMCOUNTRY,'') AS STS_FPRMCOUNTRY,ISNULL(STS_FPRMPHONE,'') AS STS_FPRMPHONE , " _
                            & " ISNULL(STS_FOCC,'') AS STS_FOCC,ISNULL(STS_F_COMP_ID,0) AS STS_F_COMP_ID," _
                            & " ISNULL(STS_FCOMPANY,'') AS STS_FCOMPANY,ISNULL(STS_FCOMPANY_ADDR,'') AS STS_FCOMPANY_ADDR," _
                            & " ISNULL(STS_FEMAIL,'') AS STS_FEMAIL,ISNULL(STS_MFIRSTNAME,'') AS STS_MFIRSTNAME," _
                            & " ISNULL(STS_MMIDNAME,'') AS STS_MMIDNAME,ISNULL(STS_MLASTNAME,'') AS STS_MLASTNAME," _
                            & " ISNULL(STS_MNATIONALITY,0) AS STS_MNATIONALITY, ISNULL(STS_MNATIONALITY2,0) AS STS_MNATIONALITY2," _
                            & " ISNULL(STS_MCOMADDR1,'') AS STS_MCOMADDR1,ISNULL(STS_MCOMADDR2,'') AS STS_MCOMADDR2, " _
                            & " ISNULL(STS_MCOMPOBOX,'') AS STS_MCOMPOBOX,ISNULL(STS_MCOMCITY,0) AS STS_MCOMCITY," _
                            & " ISNULL(STS_MCOMSTATE,'') AS STS_MCOMSTATE,ISNULL(STS_MCOMCOUNTRY,0) AS STS_MCOMCOUNTRY," _
                            & " ISNULL(STS_MOFFPHONE,'') AS STS_MOFFPHONE,ISNULL(STS_MRESPHONE,'') AS STS_MRESPHONE," _
                            & " ISNULL(STS_MFAX,'') AS STS_MFAX,ISNULL(STS_MMOBILE,'') AS STS_MMOBILE," _
                            & " ISNULL(STS_MPRMADDR1,'') AS STS_MPRMADDR1,ISNULL(STS_MPRMADDR2,'') AS STS_MPRMADDR2," _
                            & " ISNULL(STS_MPRMPOBOX,'') AS STS_MPRMPOBOX,ISNULL(STS_MPRMCITY,'') AS STS_MPRMCITY," _
                            & " ISNULL(STS_MPRMCOUNTRY,'') AS STS_MPRMCOUNTRY,ISNULL(STS_MPRMPHONE,'') AS STS_MPRMPHONE ," _
                            & " ISNULL(STS_MOCC,'') AS STS_MOCC,ISNULL(STS_M_COMP_ID,0) AS STS_M_COMP_ID," _
                            & " ISNULL(STS_MCOMPANY,'') AS STS_MCOMPANY,ISNULL(STS_MCOMPANY_ADDR,'') AS STS_MCOMPANY_ADDR," _
                            & " ISNULL(STS_MEMAIL,'') AS STS_MEMAIL,ISNULL(STS_GFIRSTNAME,'') AS STS_GFIRSTNAME," _
                            & " ISNULL(STS_GMIDNAME,'') AS STS_GMIDNAME,ISNULL(STS_GLASTNAME,'') AS STS_GLASTNAME," _
                            & " ISNULL(STS_GNATIONALITY,0) AS STS_GNATIONALITY,ISNULL(STS_GNATIONALITY2,0) AS STS_GNATIONALITY2," _
                            & " ISNULL(STS_GCOMADDR1,'') AS STS_GCOMADDR1,ISNULL(STS_GCOMADDR2,'') AS STS_GCOMADDR2," _
                            & " ISNULL(STS_GCOMPOBOX,'') AS STS_GCOMPOBOX,ISNULL(STS_GCOMCITY,'') AS STS_GCOMCITY," _
                            & " ISNULL(STS_GCOMSTATE,'') AS STS_GCOMSTATE,ISNULL(STS_GCOMCOUNTRY,'') AS STS_GCOMCOUNTRY," _
                            & " ISNULL(STS_GOFFPHONE,'') AS STS_GOFFPHONE,ISNULL(STS_GRESPHONE,'') AS STS_GRESPHONE," _
                            & " ISNULL(STS_GFAX,'') AS STS_GFAX ,ISNULL(STS_GMOBILE,'') AS STS_GMOBILE,ISNULL(STS_GPRMADDR1,'') AS STS_GPRMADDR1," _
                            & " ISNULL(STS_GPRMADDR2,'') AS STS_GPRMADDR2,ISNULL(STS_GPRMPOBOX,'') AS STS_GPRMPOBOX,ISNULL(STS_GPRMCITY,'') AS STS_GPRMCITY," _
                            & " ISNULL(STS_GPRMCOUNTRY,'') AS STS_GPRMCOUNTRY,ISNULL(STS_GPRMPHONE,'') AS STS_GPRMPHONE,ISNULL(STS_GOCC,'') AS STS_GOCC," _
                            & " ISNULL(STS_G_COMP_ID,0) AS STS_G_COMP_ID,ISNULL(STS_GCOMPANY,'') AS STS_GCOMPANY,ISNULL(STS_GCOMPANY_ADDR,'') AS STS_GCOMPANY_ADDR," _
                            & " ISNULL(STS_GEMAIL,'') AS STS_GEMAIL,ISNULL(STS_PNAME_ARABIC,'') AS STS_PNAME_ARABIC," _
                            & " ISNULL(STS_POCC_ARABIC,'') AS STS_POCC_ARABIC,ISNULL(STS_PADDR_ARABIC,'') AS STS_PADDR_ARABIC," _
                            & " ISNULL(STS_POFFCADDR_ARABIC,'') AS STS_POFFCADDR_ARABIC FROM STUDENT_M AS A INNER JOIN STUDENT_D AS" _
                            & " B ON A.STU_SIBLING_ID=B.STS_STU_ID WHERE STU_FEE_ID=" + SiblingFeeId + ""

        Dim command As SqlCommand = New SqlCommand(sqlSibling_Details, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetDetailsTERM_M(ByVal TRM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --19/JAN/2008
        'Purpose--Get Grade data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTERM_M As String = ""

        sqlGetTERM_M = " Select TRM_ID,ACD_ID,Y_DESCR,TRM_DESCR,STARTDT,ENDDT,TRM_ACTUALSTARTDATE from(SELECT TRM_M.TRM_DESCRIPTION AS TRM_DESCR, TRM_M.TRM_STARTDATE AS STARTDT, TRM_M.TRM_ENDDATE AS ENDDT, " & _
                      " TRM_M.TRM_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, TRM_M.TRM_ACD_ID AS ACD_ID, TRM_M.TRM_ID as TRM_ID,ISNULL(TRM_M.TRM_ACTUALSTARTDATE,TRM_M.TRM_STARTDATE) TRM_ACTUALSTARTDATE " & _
                    " FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
                     " TRM_M ON ACADEMICYEAR_D.ACD_ID = TRM_M.TRM_ACD_ID)a where a.TRM_ID='" & TRM_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetTERM_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetDetailsDRQ_ID(ByVal DRQ_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --19/JAN/2008
        'Purpose--Get Grade data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetDRQ_ID As String = ""

        sqlGetDRQ_ID = "SELECT DRQ_ID,ACD_ID,ACY_DESCR,GRD_ID,GRM_DISPLAY,DOC_ID,DOC_DESCR,DRQ_TYPE,DRQ_APPLY,DRQ_COPY from(SELECT DOCRQD_GRADE_M.DRQ_ID as DRQ_ID, DOCRQD_GRADE_M.DRQ_ACD_ID as ACD_ID, ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, DOCRQD_GRADE_M.DRQ_BSU_ID as BSU_ID, " & _
            " DOCRQD_GRADE_M.DRQ_GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY, DOCRQD_GRADE_M.DRQ_DOC_ID as DOC_ID, DOCREQD_M.DOC_DESCR as DOC_DESCR, " & _
             " DOCRQD_GRADE_M.DRQ_TYPE as DRQ_TYPE , DOCRQD_GRADE_M.DRQ_COPY as DRQ_COPY, " & _
           " DOCRQD_GRADE_M.DRQ_APPLY as DRQ_APPLY   FROM GRADE_BSU_M INNER JOIN ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
               " DOCRQD_GRADE_M ON DOCRQD_GRADE_M.DRQ_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_GRD_ID = DOCRQD_GRADE_M.DRQ_GRM_GRD_ID INNER JOIN " & _
                 " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN  DOCREQD_M ON DOCRQD_GRADE_M.DRQ_DOC_ID = DOCREQD_M.DOC_ID)a where a.DRQ_ID='" & DRQ_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlGetDRQ_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetYear_DESCR(ByVal Bsu_id As String, ByVal CLM As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --17/JAN/2008
        'Purpose--Get Grade data from  ACADEMICYEAR_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetYear_DESCR As String = ""

        sqlGetYear_DESCR = "Select  ACY_ID,ACD_ID,Y_DESCR,ACD_CURRENT from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
                     " ACADEMICYEAR_D.ACD_CLM_ID AS CLM_ID, ACADEMICYEAR_D.ACD_ID as ACD_ID,ACD_CURRENT FROM ACADEMICYEAR_D INNER JOIN " & _
                     " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"





        '"Select ACY_ID,Y_DESCR from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
        '                      " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN " & _
        '                      " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetYear_DESCR, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetGRM_GRD_ID2(ByVal GRD_ID As String, ByVal CLM As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --16/JAN/2008
        'Purpose--Get DRQ_GRM_GRD_ID data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGRM_GRD_ID As String = ""

        sqlGetGRM_GRD_ID = "SELECT Distinct GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY " & _
" FROM ACADEMICYEAR_D INNER JOIN  GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & GRD_ID & "' AND GRADE_BSU_M.GRM_GRD_ID IN ('10','12') "


        Dim command As SqlCommand = New SqlCommand(sqlGetGRM_GRD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetPRENURSERY_M() As SqlDataReader
        'Author(--Lijo)
        'Date   --24/FEB/2008
        'Purpose--Get all the Nursery
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPRENURSERY As String = ""

        sqlGetPRENURSERY = "Select SCH_CODE,SCH_DESCR from PRENURSERY_M order by SCH_DESCR"


        '"Select ACY_ID,Y_DESCR from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
        '                      " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN " & _
        '                      " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetPRENURSERY, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetActive_ACD_4_Grade(ByVal Bsu_id As String, ByVal CLM As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/JAN/2008
        'Purpose--Get Active_ACD_4_Grade
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection

        Dim sqlGetActive_ACD As String = ""

        sqlGetActive_ACD = " Select ACD_ID,Y_DESCR from(SELECT ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_BSU_ID as BSU_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR,ACADEMICYEAR_D.ACD_Current as ACD_Current, " & _
                                    " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.BSU_ID='" & Bsu_id & "'  and a.CLM_ID='" & CLM & "' and a.ACD_Current=1 order by a.Y_DESCR desc"



        Dim command As SqlCommand = New SqlCommand(sqlGetActive_ACD, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetAll_Grade_4_ACD(ByVal Bsu_id As String, ByVal CLM As String, ByVal ACD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/JAN/2008
        'Purpose--Get Active_ACD_4_Grade
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection

        Dim sqlGetActive_ACD As String = ""

        sqlGetActive_ACD = " Select ACD_ID,Y_DESCR from(SELECT ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_BSU_ID as BSU_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR,ACADEMICYEAR_D.ACD_Current as ACD_Current, " & _
                                    " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.BSU_ID='" & Bsu_id & "'  and a.CLM_ID='" & CLM & "' and a.ACD_Current=1 order by a.Y_DESCR desc"



        Dim command As SqlCommand = New SqlCommand(sqlGetActive_ACD, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetACD_Year(ByVal ACD As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/JAN/2008
        'Purpose--Get selected ACD Year
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetACD_Year As String = ""

        sqlGetACD_Year = "SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR " & _
            " FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID where ACADEMICYEAR_D.ACD_ID='" & ACD & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetACD_Year, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetYear_DESCR2(ByVal Bsu_id As String, ByVal CLM As String, ByVal ACY_ID As Integer) As SqlDataReader
        'Author(--Lijo)
        'Date   --17/JAN/2008
        'Purpose--Get Grade data from  ACADEMICYEAR_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetYear_DESCR As String = ""


        sqlGetYear_DESCR = "Select ACD_ID,Y_DESCR from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
                     " ACADEMICYEAR_D.ACD_CLM_ID AS CLM_ID, ACADEMICYEAR_D.ACD_ID as ACD_ID FROM ACADEMICYEAR_D INNER JOIN " & _
                     " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID WHERE ACADEMICYEAR_M.ACY_ID<'" & ACY_ID & "')a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"



        '"Select ACY_ID,Y_DESCR from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
        '                      " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN " & _
        '                      " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.CLM_ID='" & CLM & "' and a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetYear_DESCR, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCURRICULUM_M_ID(ByVal BSU_ID As String) As SqlDataReader

        Dim sqlCURRICULUM As String = "select BSU_CLM_ID from BUSINESSUNIT_M where BSU_ID='" & BSU_ID & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlCURRICULUM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetActive_Year(ByVal Bsu_id As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --21/JAN/2008
        'Purpose--Get active year from  ACADEMICYEAR_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetActive_Year As String = ""

        sqlGetActive_Year = "SELECT   ACADEMICYEAR_M.ACY_ID as ACY_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR , ACADEMICYEAR_D.ACD_ID as ACD_ID " & _
" FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
 " where ACADEMICYEAR_D.ACD_BSU_ID='" & Bsu_id & "'  and ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "' and ACADEMICYEAR_D.ACD_CURRENT=1 "


        '" Select top 1  ACY_ID,Y_DESCR from(SELECT ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID as BSU_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR,ACADEMICYEAR_D.ACD_Current as ACD_Current, " & _
        '                            " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.BSU_ID='" & Bsu_id & "'  and a.CLM_ID=1 and a.ACD_Current=1 order by a.Y_DESCR desc"



        Dim command As SqlCommand = New SqlCommand(sqlGetActive_Year, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetAppl_To() As SqlDataReader
        'Author(--Lijo)
        'Date   --17/JAN/2008
        'Purpose--Get Grade data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetAppl_To As String = ""

        sqlGetAppl_To = "select distinct case DRQ_APPLY when 'ALL' then 'All' when 'EXP' then 'Expats' when 'NAT' then 'Nationals' end DRQ_APPLY from DOCRQD_GRADE_M order by DRQ_APPLY"

        Dim command As SqlCommand = New SqlCommand(sqlGetAppl_To, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCurrent_Grade_Display(ByVal Bsu_id As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --24/Mar/2008
        'Purpose--Get Grade data from GRADE_BSU_M 4 current academic year

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGrade As String = ""

        sqlGetGrade = "SELECT DISTINCT GRADE_BSU_M.GRM_GRD_ID as, GRADE_BSU_M.GRM_DISPLAY, GRADE_M.GRD_DISPLAYORDER, ACADEMICYEAR_D.ACD_CURRENT " & _
" FROM  GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID " & _
" WHERE  (GRADE_BSU_M.GRM_BSU_ID ='" & Bsu_id & "' and ACADEMICYEAR_D.ACD_CURRENT=1)ORDER BY GRADE_M.GRD_DISPLAYORDER"

        Dim command As SqlCommand = New SqlCommand(sqlGetGrade, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function



    Public Shared Function GetGrade_Display(ByVal Bsu_id As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --17/JAN/2008
        'Purpose--Get Grade data from GRADE_BSU_M
        'Modified on --24/Mar/2008
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGrade As String = ""

        sqlGetGrade = "SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID as GRM_GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY, GRADE_M.GRD_DISPLAYORDER " & _
                      " FROM   GRADE_BSU_M INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_BSU_ID='" & Bsu_id & "' order by GRADE_M.GRD_DISPLAYORDER"


        Dim command As SqlCommand = New SqlCommand(sqlGetGrade, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function




    Public Shared Function GetGrade_Display_BSU(ByVal Bsu_id As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --17/JAN/2008
        'Purpose--Get Grade data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGrade_BSU As String = ""

        sqlGetGrade_BSU = "SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY, GRADE_M.GRD_DISPLAYORDER, GRADE_BSU_M.GRM_BSU_ID " & _
         " FROM  GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_BSU_ID='" & Bsu_id & "' order by GRADE_M.GRD_DISPLAYORDER "

        Dim command As SqlCommand = New SqlCommand(sqlGetGrade_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetDOC_ID() As SqlDataReader
        'Author(--Lijo)
        'Date   --16/JAN/2008
        'Purpose--Get DOC_ID data from DOCREQD_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetDOC_ID As String = ""

        sqlGetDOC_ID = "SELECT  DOC_ID,DOC_DESCR FROM DOCREQD_M order by DOC_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetDOC_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    'Public Shared Function GetOFF_STUD_GRD(ByVal BSU_ID As String, ByVal CURR As String) As SqlDataReader
    '    'Author(--Lijo)
    '    'Date   --16/JAN/2008
    '    'Purpose--Get DOC_ID data from DOCREQD_M
    '    Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
    '    Dim sqlGetDOC_ID As String = ""

    '    sqlGetDOC_ID = "SELECT  distinct    tmp_fld2   FROM   SURV_OFFLINE where BSU_ID='" & BSU_ID & "' AND tmp_fld1='" & CURR & "'"

    '    Dim command As SqlCommand = New SqlCommand(sqlGetDOC_ID, connection)
    '    command.CommandType = CommandType.Text
    '    Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
    '    SqlConnection.ClearPool(connection)
    '    Return reader
    'End Function

    'Public Shared Function GetOFF_STUD_GRD_SECT(ByVal BSU_ID As String, ByVal CURR As String, ByVal GRD As String) As SqlDataReader
    '    'Author(--Lijo)
    '    'Date   --16/JAN/2008
    '    'Purpose--Get DOC_ID data from DOCREQD_M
    '    Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
    '    Dim sqlGetDOC_ID As String = ""

    '    sqlGetDOC_ID = "SELECT  distinct    tmp_fld3   FROM   SURV_OFFLINE where BSU_ID='" & BSU_ID & "' AND tmp_fld1='" & CURR & "' AND tmp_fld2='" & GRD & "'"

    '    Dim command As SqlCommand = New SqlCommand(sqlGetDOC_ID, connection)
    '    command.CommandType = CommandType.Text
    '    Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
    '    SqlConnection.ClearPool(connection)
    '    Return reader
    'End Function



    Public Shared Function GetGRM_GRD_ID(ByVal ACD_ID As String, ByVal CLM As String) As SqlDataReader
        ''''Check the related Function references


        'Author(--Lijo)
        'Date   --16/JAN/2008
        'Purpose--Get DRQ_GRM_GRD_ID data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGRM_GRD_ID As String = ""

        sqlGetGRM_GRD_ID = "SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER as GRD_DISPLAYORDER " & _
                        " FROM GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID left OUTER JOIN " & _
                      " ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND " & _
                     " GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "'  order by GRADE_M.GRD_DISPLAYORDER"



        '"SELECT GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY " & _
        '" FROM ACADEMICYEAR_D INNER JOIN  GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
        '                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER"


        Dim command As SqlCommand = New SqlCommand(sqlGetGRM_GRD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetGRM_GRD_ID(ByVal ACD_ID As String, ByVal CLM As String, ByVal SHF_ID As String) As SqlDataReader
        ''''Check the related Function references


        'modified Arun.g
        'Purpose--Get DRQ_GRM_GRD_ID data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGRM_GRD_ID As String = ""

        sqlGetGRM_GRD_ID = "SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER as GRD_DISPLAYORDER " & _
                        " FROM GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID left OUTER JOIN " & _
                      " ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND " & _
                     " GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "'  And GRM_SHF_ID='" & SHF_ID & "'order by GRADE_M.GRD_DISPLAYORDER"



        '"SELECT GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY " & _
        '" FROM ACADEMICYEAR_D INNER JOIN  GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
        '                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER"


        Dim command As SqlCommand = New SqlCommand(sqlGetGRM_GRD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetGRM_GRD_ID_LIB(ByVal ACD_ID As String, ByVal CLM As String, ByVal SHF_ID As String) As SqlDataReader
        ''''Check the related Function references

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As New SqlCommand("GETGRM_GRD_ID_LIB", connection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add(New SqlClient.SqlParameter("@ACD_ID", ACD_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@CLM", CLM))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@SHF_ID", SHF_ID))

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        SqlConnection.ClearPool(connection)
        Return reader

    End Function



    Public Shared Function GetDOCREQD_M(ByVal DOC_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --15/JAN/2008
        'Purpose--Get DOC_ID data from DOCREQD_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetDOCREQD_M As String = ""

        sqlGetDOCREQD_M = "Select ID,DType,Descr ,Doc_ID from(SELECT DOCREQD_M.DOC_ID as ID, DOCREQD_TYPE_M.DOCTYPE_DESCR as DType,DOCREQD_M.DOC_DESCR as Descr,  DOCREQD_M.DOC_DOCTYPE_ID as Doc_ID " & _
                            " FROM  DOCREQD_M INNER JOIN  DOCREQD_TYPE_M ON DOCREQD_M.DOC_DOCTYPE_ID = DOCREQD_TYPE_M.DOCTYPE_ID)a where a.id='" & DOC_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetDOCREQD_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetSubject_M(ByVal SubjectID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --13/JAN/2008
        'Purpose--Get Subject data from Subject_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetSubject_M As String = ""

        sqlGetSubject_M = "Select ID,Descr,bFlag from(select SBM_ID as ID,SBM_DESCR as Descr,SMB_LANG as bFlag from SUBJECT_M)a where  a.ID='" & SubjectID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlGetSubject_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetHouse_M(ByVal HouseID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --13/JAN/2008
        'Purpose--Get House data from House_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetHouse_M As String = ""

        sqlGetHouse_M = "Select Descr,HColor from(select House_ID as ID,House_Description as Descr,House_color as HColor from HOUSE_M)a where  a.ID='" & HouseID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlGetHouse_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetTransfer()
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTransfer As String = ""

        sqlGetTransfer = " SELECT TFR_CODE, TFR_DESCR FROM TFRTYPE_M order by TFR_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetTransfer, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetBSU_House(ByVal BSUID As String)
        'Author(--Lijo)
        'Date   --09/JAN/2008
        'Purpose--Get Color data from Color_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetBSU_House As String = ""

        sqlGetBSU_House = "Select HideID,ID,HColor from(SELECT HOUSE_M.HOUSE_ID as HideID,HOUSE_M.HOUSE_DESCRIPTION as ID,HOUSE_M.HOUSE_BSU_ID as BSU_ID,House_Color as HColor FROM  HOUSE_M)a where a.BSU_ID='" & BSUID & "' AND ID<>'-' "

        Dim command As SqlCommand = New SqlCommand(sqlGetBSU_House, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetColor() As SqlDataReader
        'Author(--Lijo)
        'Date   --09/JAN/2008
        'Purpose--Get Color data from Color_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetColor As String = ""

        sqlGetColor = "SELECT COLOR_DESCR,COLOR_HEX  FROM Color_M order by COLOR_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetColor, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetSTU_BSU_RELIGION(ByVal SBR_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --12/AUG/2008
        'Purpose--Get STU_BSU_RELIGION data based on the SBR_ID 

        Dim sqlStringID As String = "SELECT RELIGION_M.RLG_DESCR as R_DESCR, STU_BSU_RELIGION.SBR_RLG_ID as R_ID, STU_BSU_RELIGION.SBR_BSU_ID as R_BSU_ID" & _
                    " FROM  RELIGION_M INNER JOIN STU_BSU_RELIGION ON RELIGION_M.RLG_ID = STU_BSU_RELIGION.SBR_RLG_ID where STU_BSU_RELIGION.SBR_ID='" & SBR_ID & "'"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlStringID, connection)
        command.CommandType = CommandType.Text
        Dim readerSTU_BSU_RELIGION As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerSTU_BSU_RELIGION
    End Function
    Public Shared Function GetACADEMICYEAR_D(ByVal ACD_ID As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --08/JAN/2008
        'Purpose--Get ACADEMICYEAR_D data based on the ACD_ID (studAcademicyear_D.aspx)

        Dim sqlStringID As String = "SELECT ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR,ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID, CURRICULUM_M.CLM_DESCR as CLM_DESCR, ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID, ACADEMICYEAR_D.ACD_STARTDT as STARTDT, ACADEMICYEAR_D.ACD_ENDDT as ENDDT, " & _
                     " ACADEMICYEAR_D.ACD_CURRENT as ACD_CURRENT, ACADEMICYEAR_D.ACD_OPENONLINE as OPENONLINE, ACADEMICYEAR_D.ACD_AFFLNO as AFFLNO  , ACADEMICYEAR_D.ACD_MOEAFFLNO as MOEAFFLNO, ACADEMICYEAR_D.ACD_LICENSE as LICENSE,ACD_BROWNBOOKDATE,ACD_ATT_CLOSINGDATE FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
        " CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID where ACADEMICYEAR_D.ACD_ID='" & ACD_ID & "'"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlStringID, connection)
        command.CommandType = CommandType.Text
        Dim readerACADEMICYEAR_D As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerACADEMICYEAR_D

    End Function


    Public Shared Function GetStud_BSU_Religion(ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --12/AUG/2008
        'Purpose--Get Religion data from Color_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetReligion As String = ""

        sqlGetReligion = "SELECT   RELIGION_M.RLG_DESCR as RLG_DESCR,STU_BSU_RELIGION.SBR_RLG_ID as RLG_ID  FROM RELIGION_M INNER JOIN STU_BSU_RELIGION ON RELIGION_M.RLG_ID = STU_BSU_RELIGION.SBR_RLG_ID  where STU_BSU_RELIGION.SBR_BSU_ID='" & BSU_ID & "'  order by RELIGION_M.RLG_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetReligion, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetReligion() As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get Religion data from Color_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetReligion As String = ""

        sqlGetReligion = "SELECT RLG_ID, RLG_DESCR FROM RELIGION_M order by RLG_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetReligion, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetComp_Name() As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Jun/2008
        'Purpose--Get Company Name
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetComp_Name As String = ""

        sqlGetComp_Name = "SELECT COMP_ID,COMP_NAME  FROM COMP_LISTED_M  where COMP_bACTIVE=1 order by COMP_NAME"

        Dim command As SqlCommand = New SqlCommand(sqlGetComp_Name, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetFee_Type() As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Jun/2008
        'Purpose--Get Fee TYPE
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetFeeTYPE As String = ""

        sqlGetFeeTYPE = "SELECT FEE_ID,FEE_DESCR  FROM FEES.FEES_M order by FEE_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetFeeTYPE, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetEmirate(Optional ByVal ENQ_GROUP As Integer = 1) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get Emirate data from Emirate_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEmirate As String = ""
        If HttpContext.Current.Session("sBSUID") = "315001" Then
            sqlGetEmirate = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='4' order by EMR_DESCR"
        ElseIf HttpContext.Current.Session("sBSUID") = "800017" Then
            sqlGetEmirate = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='3' order by EMR_DESCR"
        Else
            sqlGetEmirate = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='" & ENQ_GROUP & "' order by EMR_DESCR"
        End If


        Dim command As SqlCommand = New SqlCommand(sqlGetEmirate, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetTELPHONECODE_M(ByVal S_Mobile As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get TELPHONECODE data from TELPHONECODE_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTELPHONECODE As String = ""
        If S_Mobile = "M" Then

            sqlGetTELPHONECODE = " Select  PHO_CTY_ID,PHO_CODE FROM TELPHONECODE_M  where PHO_bMOBILE =1 order by PHO_CODE"
        Else
            sqlGetTELPHONECODE = " Select  PHO_CTY_ID,PHO_CODE FROM TELPHONECODE_M  where PHO_bMOBILE =0 order by PHO_CODE"
        End If
        Dim command As SqlCommand = New SqlCommand(sqlGetTELPHONECODE, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCURRICULUM_BSU_Current(ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get CURRICULUM_BSU data from CURRICULUM_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCURRICULUM As String = ""
        sqlGetCURRICULUM = " SELECT  distinct  CURRICULUM_M.CLM_ID as CLM_ID, CURRICULUM_M.CLM_DESCR as CLM_DESCR " & _
" FROM  ACADEMICYEAR_D INNER JOIN  CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID where " & _
"  ACADEMICYEAR_D.ACD_CURRENT =1 AND ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' order by CURRICULUM_M.CLM_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetCURRICULUM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetCURRICULUM_BSU_Current(ByVal BSU_ID As String, ByVal acd_ID As String) As SqlDataReader
        'Author(--Arun.g)
        'Date   --22/april/2009
        'Purpose--Get CURRICULUM_BSU data from CURRICULUM_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCURRICULUM As String = ""
        sqlGetCURRICULUM = " SELECT  distinct  CURRICULUM_M.CLM_ID as CLM_ID, CURRICULUM_M.CLM_DESCR as CLM_DESCR " & _
" FROM  ACADEMICYEAR_D INNER JOIN  CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID where " & _
"  ACADEMICYEAR_D.acd_acy_id in (" & acd_ID & ") and ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' order by CURRICULUM_M.CLM_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetCURRICULUM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function




    Public Shared Function GetCURRICULUM_BSU(ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get CURRICULUM_BSU data from CURRICULUM_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCURRICULUM As String = ""
        sqlGetCURRICULUM = " SELECT  distinct  CURRICULUM_M.CLM_ID as CLM_ID, CURRICULUM_M.CLM_DESCR as CLM_DESCR " & _
" FROM  ACADEMICYEAR_D INNER JOIN  CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID where " & _
" ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' order by CURRICULUM_M.CLM_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetCURRICULUM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCurriculum() As SqlDataReader
        'Author(--Lijo)
        'Date   --24/aug/2008
        'Purpose--Get CURRICULUM_BSU data from CURRICULUM_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCURRICULUM As String = ""
        sqlGetCURRICULUM = " SELECT     CLM_ID, CLM_DESCR  FROM CURRICULUM_M WHERE CLM_DESCR <> '--' order by CLM_DESCR"
        Dim command As SqlCommand = New SqlCommand(sqlGetCURRICULUM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetCURRICULUM_M() As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get CURRICULUM data from CURRICULUM_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCURRICULUM As String = ""


        sqlGetCURRICULUM = " SELECT CLM_ID,CLM_DESCR FROM CURRICULUM_M order by CLM_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetCURRICULUM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetGrade_M() As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get Grade data from Grade_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGrade As String = ""

        sqlGetGrade = "SELECT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM GRADE_M order by GRD_DISPLAYORDER"

        Dim command As SqlCommand = New SqlCommand(sqlGetGrade, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetBSU_M_form_staff() As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get BSU_M data from BUSINESSUNIT_M excluding Dummy Business Unit
        Dim sqlBusinessUnit As String = "select bsu_id,bsu_name from businessunit_m where bsu_id not in('XXXXXX') order by bsu_name"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetBSU_M_form_Student(Optional ByVal bsu_enq_group As Integer = 1) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get BSU_M data from BUSINESSUNIT_M excluding Dummy Business Unit
        Dim sqlBusinessUnit As String = "select bsu_id,bsu_name from businessunit_m where   (BUS_BSG_ID NOT IN (4,5, 6, 8, 10, 9) and BSU_ENQ_GROUP='" & bsu_enq_group & "') order by bsu_name"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetBSU_M_form_Student_Enq(Optional ByVal bsu_enq_group As Integer = 1) As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get BSU_M data from BUSINESSUNIT_M excluding Dummy Business Unit
        Dim sqlBusinessUnit As String = "select bsu_id,bsu_name from businessunit_m where   (BUS_BSG_ID NOT IN (4,5, 6, 8, 10, 9) and BSU_ENQ_GROUP='" & bsu_enq_group & "') UNION ALL Select bsu_id,bsu_name From businessunit_m WHERE BSU_ID='300001' order by bsu_name"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetNational() As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetNational As String = ""

        sqlGetNational = "Select CTY_ID,case CTY_ID when '5' then 'Not Available' else isnull(CTY_NATIONALITY,'') end CTY_NATIONALITY from Country_m  order by CTY_NATIONALITY"

        Dim command As SqlCommand = New SqlCommand(sqlGetNational, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetBlue_BSU_TEACH(ByVal SCT_ID As String, ByVal grd_id As String, ByVal acd_id As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetBlue_BSU_TEACH As String = ""
        '******************************************************
        'sqlGetBlue_BSU_TEACH = " SELECT DISTINCT   SECTION_M.SCT_DESCR, CURRICULUM_M.CLM_DESCR, ACADEMICYEAR_D.ACD_ID, ACADEMICYEAR_M.ACY_DESCR, " & _
        '              " ISNULL(EMPLOYEE_M.EMP_FNAME, '') + '  ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '') + '  ' + ISNULL(EMPLOYEE_M.EMP_LNAME, ' ') AS Emp_Name," & _
        '              " GRADE_BSU_M.GRM_DISPLAY AS GRM_DISP, EMPLOYEE_M.EMP_ID, EMPQUALIFICATION_M.QLF_DESCR, BUSINESSUNIT_M.BSU_BB_LOGO as BB_Logo," & _
        '              " BUSINESSUNIT_M.BSU_MOE_LOGO  as MOE_Logo FROM  EMPLOYEE_M INNER JOIN  SECTION_M ON EMPLOYEE_M.EMP_ID = SECTION_M.SCT_EMP_ID INNER JOIN " & _
        '             " STUDENT_M INNER JOIN   GRADE_BSU_M ON STUDENT_M.STU_GRM_ID = GRADE_BSU_M.GRM_ID ON SECTION_M.SCT_ID = STUDENT_M.STU_SCT_ID INNER JOIN " & _
        '             " ACADEMICYEAR_M INNER JOIN  ACADEMICYEAR_D ON ACADEMICYEAR_M.ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID INNER JOIN " & _
        '             " CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID ON  STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
        '             " EMPQUALIFICATION_M ON EMPLOYEE_M.EMP_QLF_ID = EMPQUALIFICATION_M.QLF_ID INNER JOIN " & _
        '             " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID where STUDENT_M.STU_ACD_ID='" & acd_id & "' and STUDENT_M.STU_SCT_ID= '" & SCT_ID & "' and STUDENT_M.STU_GRD_ID='" & grd_id & "'"
        '*******************************************************************
        'query modified by dhanya 28-04-08 (left outer joint given for empqualification_m
        sqlGetBlue_BSU_TEACH = " SELECT DISTINCT   SECTION_M.SCT_DESCR, CURRICULUM_M.CLM_DESCR, ACADEMICYEAR_D.ACD_ID, ACADEMICYEAR_M.ACY_DESCR, " & _
                    " ISNULL(EMPLOYEE_M.EMP_FNAME, '') + '  ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '') + '  ' + ISNULL(EMPLOYEE_M.EMP_LNAME, ' ') AS Emp_Name," & _
                    " GRADE_BSU_M.GRM_DISPLAY AS GRM_DISP, EMPLOYEE_M.EMP_ID, EMPQUALIFICATION_M.QLF_DESCR, BUSINESSUNIT_M.BSU_BB_LOGO as BB_Logo," & _
                    " BUSINESSUNIT_M.BSU_MOE_LOGO  as MOE_Logo FROM  EMPLOYEE_M INNER JOIN  SECTION_M ON EMPLOYEE_M.EMP_ID = SECTION_M.SCT_EMP_ID INNER JOIN " & _
                   " STUDENT_M INNER JOIN   GRADE_BSU_M ON STUDENT_M.STU_GRM_ID = GRADE_BSU_M.GRM_ID ON SECTION_M.SCT_ID = STUDENT_M.STU_SCT_ID INNER JOIN " & _
                   " ACADEMICYEAR_M INNER JOIN  ACADEMICYEAR_D ON ACADEMICYEAR_M.ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID INNER JOIN " & _
                   " CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID ON  STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
                   " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID " & _
                   " LEFT OUTER JOIN EMPQUALIFICATION_M ON EMPLOYEE_M.EMP_QLF_ID = EMPQUALIFICATION_M.QLF_ID " & _
                   "  where STUDENT_M.STU_ACD_ID='" & acd_id & "' and STUDENT_M.STU_SCT_ID= '" & SCT_ID & "' and STUDENT_M.STU_GRD_ID='" & grd_id & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetBlue_BSU_TEACH, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Public Shared Function GetBlue_TotStud(ByVal SCT_ID As String, ByVal grd_id As String, ByVal acd_id As String, ByVal bsu_Id As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetBlue_TOTStud As String = ""
        '**********************************************
        '        sqlGetBlue_TOTStud = "select m.stu_grd_id,m.stu_sct_id,m.stu_acd_id,m.stu_bsu_id " & _
        '" ,(select count(case mm.stu_rlg_id when 'CHR' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Christian " & _
        '" ,(select count(case mm.stu_rlg_id when 'ISL' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Islam " & _
        '" ,(select count(case mm.stu_rlg_id when 'HIN' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Hin " & _
        '" ,(select count(case mm.stu_rlg_id when 'OTH' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as OTHE " & _
        '" ,(select count(case mm.stu_nationality when '172' then 1 end) from student_m mm  where mm.stu_nationality='172' and mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as local " & _
        '" ,(select count(*) from student_m mm  where mm.stu_nationality != '172' and mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Nonlocal " & _
        '" from student_m m inner join student_promo_s on m.stu_acd_id=student_promo_s.stp_acd_id " & _
        '" inner join RELIGION_M on m.stu_rlg_id = RELIGION_M.rlg_id " & _
        '" inner join grade_bsu_m gb on m.stu_grm_id = gb.grm_id and gb.grm_acd_id=m.stu_acd_id and gb.grm_bsu_id = m.stu_bsu_id " & _
        '" inner join section_m on section_m.sct_bsu_id=m.stu_bsu_id and section_m.sct_acd_id=m.stu_acd_id and m.stu_sct_id=section_m.sct_id " & _
        '" and m.stu_sct_id like coalesce('" & SCT_ID & "',m.stu_sct_id) and m.stu_grd_id like coalesce('" & grd_id & "',m.stu_grd_id)" & _
        '" and m.stu_bsu_id like coalesce('" & bsu_Id & "',m.stu_bsu_id)and m.stu_acd_id like coalesce('" & acd_id & "',m.stu_acd_id)" & _
        '" group by m.stu_grd_id,m.stu_sct_id,m.stu_acd_id,m.stu_bsu_id"
        '****************************************************************

        'query modified by dhanya 28 apr 08(male and female count also taken)
        sqlGetBlue_TOTStud = "select m.stu_grd_id,m.stu_sct_id,m.stu_acd_id,m.stu_bsu_id " & _
" ,(select count(case mm.stu_rlg_id when 'CHR' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Christian " & _
" ,(select count(case mm.stu_rlg_id when 'ISL' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Islam " & _
" ,(select count(case mm.stu_rlg_id when 'HIN' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Hin " & _
" ,(select count(case mm.stu_rlg_id when 'OTH' then 1 end) from student_m mm where mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as OTHE " & _
" ,(select count(case mm.stu_nationality when '172' then 1 end) from student_m mm  where mm.stu_nationality='172' and mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as local " & _
" ,(select count(*) from student_m mm  where mm.stu_nationality != '172' and mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Nonlocal " & _
" ,(select count(*) from student_m mm  where mm.stu_nationality != '172' and mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Nonlocal " & _
" ,(select count(*) from student_m mm  where mm.stu_gender = 'M' and mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Male " & _
" ,(select count(*) from student_m mm  where mm.stu_gender = 'F' and mm.stu_sct_id=m.stu_sct_id and mm.stu_grd_id=m.stu_grd_id and mm.stu_acd_id= m.stu_acd_id and mm.stu_bsu_id =m.stu_bsu_id) as Female " & _
" from student_m m inner join student_promo_s on m.stu_acd_id=student_promo_s.stp_acd_id " & _
" inner join RELIGION_M on m.stu_rlg_id = RELIGION_M.rlg_id " & _
" inner join grade_bsu_m gb on m.stu_grm_id = gb.grm_id and gb.grm_acd_id=m.stu_acd_id and gb.grm_bsu_id = m.stu_bsu_id " & _
" inner join section_m on section_m.sct_bsu_id=m.stu_bsu_id and section_m.sct_acd_id=m.stu_acd_id and m.stu_sct_id=section_m.sct_id " & _
" and m.stu_sct_id like coalesce('" & SCT_ID & "',m.stu_sct_id) and m.stu_grd_id like coalesce('" & grd_id & "',m.stu_grd_id)" & _
" and m.stu_bsu_id like coalesce('" & bsu_Id & "',m.stu_bsu_id)and m.stu_acd_id like coalesce('" & acd_id & "',m.stu_acd_id)" & _
" group by m.stu_grd_id,m.stu_sct_id,m.stu_acd_id,m.stu_bsu_id"

        Dim command As SqlCommand = New SqlCommand(sqlGetBlue_TOTStud, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetBlue_BookDisplay(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal Prev_ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetBlue_BookDisplay As String = ""
        sqlGetBlue_BookDisplay = " SELECT     STUDENT_M.STU_BLUEID AS BLUEID,(SELECT     GRADE_BSU_M.GRM_DISPLAY + SECTION_M.SCT_DESCR AS LastGrade " & _
                               "  FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN " & _
                     "  SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID" & _
                     " WHERE (STUDENT_PROMO_S.STP_ACD_ID ='" & Prev_ACD_ID & "') AND (STUDENT_PROMO_S.STP_STU_ID = STUDENT_M.STU_ID)) AS LastGrade,  STUDENT_M.STU_PASPRTNAME, STUDENT_M.STU_FIRSTNAME, STUDENT_D.STS_FFIRSTNAME, " & _
                      " STUDENT_M.STU_FIRSTNAMEARABIC, STUDENT_M.STU_GENDER, COUNTRY_M.CTY_SHORT AS STU_NATIONALITY, STUDENT_M.STU_RLG_ID, " & _
                     "  STUDENT_M.STU_POB, COUNTRY_M_1.CTY_DESCR AS COB, STUDENT_M.STU_DOB, STUDENT_M.STU_EMGCONTACT, " & _
                      " STUDENT_M.STU_PRIMARYCONTACT,CASE STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' THEN STUDENT_D.STS_FCOMPOBOX WHEN 'M' THEN STUDENT_D.STS_MCOMPOBOX WHEN 'G'" & _
                      "  THEN STUDENT_D.STS_GCOMPOBOX END AS STS_POBOX," & _
                      " CASE rtrim(upper(STUDENT_M.STU_TFRTYPE)) WHEN 'INT' THEN 'I' WHEN 'O' THEN 'O' WHEN 'NEW' THEN '' END AS STU_TFRTYPE, " & _
                     "  STUDENT_D.STS_PREVSCHI, STUDENT_D.STS_PREVSCHI_CLM, CURRICULUM_M.CLM_DESCR AS STS_PREVSCHI_CLM_DESCR, " & _
                     "  STUDENT_D.STS_PREVSCHI_LASTATTDATE, STUDENT_D.STS_PREVSCHI_CITY, STUDENT_M.STU_DOJ, STUDENT_M.STU_LEAVEDATE, " & _
                      " GRADE_BSU_M_1.GRM_DISPLAY AS STU_GRD_ID, STUDENT_M.STU_CURRSTATUS, STUDENT_M.STU_ID, STUDENT_M.STU_BSU_ID " & _
                      " FROM  STUDENT_M LEFT OUTER JOIN  GRADE_BSU_M AS GRADE_BSU_M_1 ON STUDENT_M.STU_GRM_ID_JOIN = GRADE_BSU_M_1.GRM_ID LEFT OUTER JOIN " & _
                      " SECTION_M AS SECTION_M_1 ON STUDENT_M.STU_SCT_ID = SECTION_M_1.SCT_ID LEFT OUTER JOIN " & _
                      " STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID LEFT OUTER JOIN " & _
                      " COUNTRY_M AS COUNTRY_M_1 ON STUDENT_M.STU_COB = COUNTRY_M_1.CTY_ID LEFT OUTER JOIN " & _
                      " CURRICULUM_M ON STUDENT_D.STS_PREVSCHI_CLM = CURRICULUM_M.CLM_ID LEFT OUTER JOIN " & _
                      " COUNTRY_M ON STUDENT_M.STU_NATIONALITY = COUNTRY_M.CTY_ID where STUDENT_M.STU_TFRTYPE<>'O' and STUDENT_M.stu_BSU_ID='" & BSU_ID & "' and STUDENT_M.stu_ACD_ID='" & ACD_ID & "' and STUDENT_M.stu_GRD_ID='" & GRD_ID & "' and STUDENT_M.STU_SCT_ID='" & SCT_ID & "' order by Student_M.STU_PASPRTNAME,Student_M.STU_FIRSTNAME"







        '" SELECT DISTINCT STUDENT_M.STU_BLUEID as BLUEID,(SELECT GRADE_BSU_M.GRM_DISPLAY + SECTION_M.SCT_DESCR AS LastGrade " & _
        '                            " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND " & _
        '                            " STUDENT_PROMO_S.STP_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND  STUDENT_PROMO_S.STP_BSU_ID = GRADE_BSU_M.GRM_BSU_ID INNER JOIN " & _
        '                             " SECTION_M ON STUDENT_PROMO_S.STP_BSU_ID = SECTION_M.SCT_BSU_ID AND  STUDENT_PROMO_S.STP_ACD_ID = SECTION_M.SCT_ACD_ID AND STUDENT_PROMO_S.STP_GRM_ID = SECTION_M.SCT_GRM_ID AND " & _
        '                        " STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID  WHERE (STUDENT_PROMO_S.STP_ACD_ID = '" & Prev_ACD_ID & "') AND (STUDENT_PROMO_S.STP_STU_ID = STUDENT_M.STU_ID)) AS LastGrade, " & _
        '                      " STUDENT_M.STU_PASPRTNAME as STU_PASPRTNAME, STUDENT_M.STU_FIRSTNAME as STU_FIRSTNAME, STUDENT_D.STS_FFIRSTNAME as STS_FFIRSTNAME, STUDENT_M.STU_FIRSTNAMEARABIC as STU_FIRSTNAMEARABIC, " & _
        '                     " STUDENT_M.STU_GENDER as STU_GENDER, COUNTRY_M.CTY_SHORT AS STU_NATIONALITY, STUDENT_M.STU_RLG_ID as STU_RLG_ID, STUDENT_M.STU_POB as STU_POB, " & _
        '                     " COUNTRY_M_1.CTY_DESCR AS COB, STUDENT_M.STU_DOB as STU_DOB, STUDENT_M.STU_EMGCONTACT as STU_EMGCONTACT, STUDENT_M.STU_PRIMARYCONTACT, " & _
        '                     " CASE STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' THEN STUDENT_D.STS_FCOMPOBOX WHEN 'M' THEN STUDENT_D.STS_MCOMPOBOX WHEN 'G' " & _
        '                      " THEN STUDENT_D.STS_GCOMPOBOX END AS STS_POBOX, " & _
        '                     " CASE STUDENT_M.STU_TFRTYPE WHEN 'INT' THEN 'I' WHEN 'OVR' THEN 'O' WHEN 'NEW' THEN '' END AS STU_TFRTYPE, STUDENT_D.STS_PREVSCHI, " & _
        '                     " STUDENT_D.STS_PREVSCHI_CLM, CURRICULUM_M.CLM_DESCR AS STS_PREVSCHI_CLM, STUDENT_D.STS_PREVSCHI_LASTATTDATE as STS_PREVSCHI_LASTATTDATE, " & _
        '                     " STUDENT_D.STS_PREVSCHI_CITY, STUDENT_M.STU_DOJ as STU_DOJ, STUDENT_M.STU_LEAVEDATE as STU_LEAVEDATE, GRADE_BSU_M_1.GRM_DISPLAY as STU_GRD_ID, " & _
        '                     " STUDENT_M.STU_CURRSTATUS as STU_CURRSTATUS, STUDENT_M.STU_ID , STUDENT_M.STU_BSU_ID " & _
        '                     " FROM STUDENT_M LEFT OUTER JOIN  SECTION_M AS SECTION_M_1 ON STUDENT_M.STU_SCT_ID = SECTION_M_1.SCT_ID AND " & _
        '                    "  STUDENT_M.STU_ACD_ID = SECTION_M_1.SCT_ACD_ID AND STUDENT_M.STU_BSU_ID = SECTION_M_1.SCT_BSU_ID LEFT OUTER JOIN " & _
        '                     " GRADE_BSU_M AS GRADE_BSU_M_1 ON STUDENT_M.STU_GRD_ID_JOIN = GRADE_BSU_M_1.GRM_GRD_ID AND " & _
        '                    "  STUDENT_M.STU_ACD_ID_JOIN = GRADE_BSU_M_1.GRM_ACD_ID LEFT OUTER JOIN " & _
        '                     " STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID LEFT OUTER JOIN " & _
        '                     " COUNTRY_M AS COUNTRY_M_1 ON STUDENT_M.STU_COB = COUNTRY_M_1.CTY_ID LEFT OUTER JOIN " & _
        '                     " CURRICULUM_M ON STUDENT_D.STS_PREVSCHI_CLM = CURRICULUM_M.CLM_ID LEFT OUTER JOIN " & _
        '                     " COUNTRY_M ON STUDENT_M.STU_NATIONALITY = COUNTRY_M.CTY_ID where STUDENT_M.stu_BSU_ID='" & BSU_ID & "' and STUDENT_M.stu_ACD_ID='" & ACD_ID & "' and STUDENT_M.stu_GRD_ID='" & GRD_ID & "' and STUDENT_M.STU_SCT_ID='" & SCT_ID & "' order by Student_M.STU_PASPRTNAME,Student_M.STU_FIRSTNAME"

        Dim command As SqlCommand = New SqlCommand(sqlGetBlue_BookDisplay, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

#End Region

#Region " All Delete function"
    Public Shared Function DeleteBSU_APPL_LETTER(ByVal BAL_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BAL_ID", BAL_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "STU.DeleteBSU_APPL_LETTER", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function DeleteENQ_ACK(ByVal EAS_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EAS_ID", EAS_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ENQ.DEL_ENQUIRY_ACK", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function


    Public Shared Function DeleteENQ_ACK_LIST(ByVal EAD_EAS_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EAD_EAS_ID", EAD_EAS_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ENQ.DEL_ENQUIRY_ACK_DOC", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function


    Public Shared Function DeleteENQ_SETTINGs(ByVal EQS_ID As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EQS_ID", EQS_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ENQ.DEL_ENQUIRY_SETTINGS", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using

        'Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
        '    Dim strDelete As String = "delete from ENQUIRY_SETTINGS where [EQS_ID]='" & EQS_ID & "'"
        '    Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(trans, CommandType.Text, strDelete)
        '    Return ReturnFlag
        'End Using
    End Function



    Public Shared Function DeleteSTUDCOMP_FEECOMPO_S(ByVal CIS_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CIS_ID", CIS_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSTUDCOMP_FEECOMPO_S", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function DeleteSTUDCOMP_LISTED_M(ByVal COMP_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@COMP_ID", COMP_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSTUDCOMP_LISTED_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function DeleteACD_GRADE(ByVal GRM_ACD_ID As String, ByVal GRM_BSU_ID As String, ByVal GRM_GRD_ID As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@GRM_ACD_ID", GRM_ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@GRM_BSU_ID", GRM_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@GRM_GRD_ID", GRM_GRD_ID)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            'SP not yet created with trigger to be considered
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSTUDACD_GRADE", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
        End Using
    End Function


    Public Shared Function DeleteACADEMICYEAR_D(ByVal ACD_ID As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)

            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            'SP not yet created with trigger to be considered
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSTUDACADEMICYEAR_D", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            SqlConnection.ClearPool(connection)
            Return ReturnFlag
        End Using
    End Function


#End Region

#Region "ATTENDANCE  CODE"
#Region "ALL GET DATA"
    Public Shared Function GetSTAFF_AUTHORIZED_LEAVE(ByVal SAL_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Jun/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTAFF_AUTHORIZED_LEAVE As String = ""

        sqlSTAFF_AUTHORIZED_LEAVE = "SELECT [SAL_ACD_ID],[SAL_GRD_ID],[SAL_BSU_ID],[SAL_EMP_ID],[SAL_FROMDT],[SAL_TODT],[SAL_NDAYS]  FROM [STAFF_AUTHORIZED_LEAVE] where SAL_ID='" & SAL_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlSTAFF_AUTHORIZED_LEAVE, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetSTAFF_AUTHORIZED(ByVal SAD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Jun/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTAFF_AUTHORIZED As String = ""

        sqlSTAFF_AUTHORIZED = "SELECT  SAD_ID, SAD_ACD_ID, SAD_GRD_ID, SAD_SCT_ID, SAD_STM_ID, SAD_SHF_ID, " & _
       "  SAD_BSU_ID, SAD_EMP_ID, SAD_FROMDT, SAD_TODT FROM STAFF_AUTHORIZED_ATT where SAD_ID='" & SAD_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlSTAFF_AUTHORIZED, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetPENDING_ATTENDANCE(ByVal ACD_ID As String, ByVal GRD_ID As String, _
                      ByVal ATT_DATE As String, ByVal ATT_TYPE As String) As DataSet
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To save ENQUIRY_ACK_DOC data based 
        Dim dsData As DataSet = Nothing
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
                pParms(2) = New SqlClient.SqlParameter("@ATT_DATE", ATT_DATE)
                pParms(3) = New SqlClient.SqlParameter("@ATT_TYPE", ATT_TYPE)
                dsData = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "STU.PENDING_ATTENDANCE", pParms)

            End Using
            If Not dsData Is Nothing Then
                Return dsData
            Else
                Return dsData
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Shared Function GetRAL_StudentID_ROOMADD(ByVal ACD_ID As String, ByVal SGR_ID As String, ByVal ASONDATE As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlALG_StudentID As String = ""

        sqlALG_StudentID = "SELECT  ALS_STU_ID,CAST(ALS_APD_ID AS VARCHAR)+'|'+'0' AS ALS_APD_ID,ALS_REMARKS  FROM  ATTENDANCE_LOG_STUDENT AS S INNER JOIN " & _
" ATTENDANCE_LOG_GRADE AS G ON S.ALS_ALG_ID=G.ALG_ID AND ALG_ATTDT='" & ASONDATE & "'  AND  " & _
" ALS_STU_ID IN(SELECT SSD_STU_ID from  STUDENT_GROUPS_S where SSD_SGR_ID='" & SGR_ID & "'  and SSD_ACD_ID='" & ACD_ID & "') "
        Dim command As SqlCommand = New SqlCommand(sqlALG_StudentID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetRAL_StudentID_ROOM(ByVal RAL_ID As String, ByVal ACD_ID As String, ByVal PERIOD_ID As String, ByVal SGR_ID As String, ByVal ASONDATE As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlALG_StudentID As String = ""

        sqlALG_StudentID = "SELECT  RAS_STU_ID,CAST(RAS_PAR_ID AS VARCHAR)+ '|'+ CAST(RAS_bROOM_PARAM AS VARCHAR) AS RAS_PAR_ID,RAS_REMARKS  FROM  ATT.ROOM_ATTENDANCE_STUDENT AS S INNER JOIN " & _
" ATT.ROOM_ATTENDANCE_LOG AS G ON S.RAS_RAL_ID=G.RAL_ID AND RAL_ID='" & RAL_ID & "' AND RAL_PERIOD_ID='" & PERIOD_ID & "' AND RAL_ATTDT='" & ASONDATE & "'  AND RAL_SGR_ID ='" & SGR_ID & "'" & _
" AND RAS_STU_ID IN(SELECT SSD_STU_ID " & _
 " from  STUDENT_GROUPS_S where SSD_SGR_ID='" & SGR_ID & "'  and SSD_ACD_ID='" & ACD_ID & "')"
        Dim command As SqlCommand = New SqlCommand(sqlALG_StudentID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetALG_StudentID_GROUP(ByVal AVG_ID As String, ByVal ASONDATE As String, ByVal ATT_TYPE As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlALG_StudentID As String = ""

        sqlALG_StudentID = " SELECT  DISTINCT ALS_STU_ID,ALS_APD_ID,ALS_REMARKS  FROM  ATTENDANCE_LOG_STUDENT AS S INNER JOIN  " & _
 " ATTENDANCE_LOG_GRADE ON ALS_ALG_ID=ALG_ID INNER JOIN STUDENT_M ON STU_ID=ALS_STU_ID  INNER JOIN  " & _
" ATT.ATTENDANCE_VSTUDENT_GROUP ON AVSG_STU_ID=STU_ID AND STU_SCT_ID=ALG_SCT_ID WHERE AVSG_bREMOVE=0 " & _
" AND ALG_ATTDT='" & ASONDATE & "'  AND AVSG_AVG_ID='" & AVG_ID & "'  AND ALG_ATT_TYPE='" & ATT_TYPE & "'"

        '"SELECT  ALS_STU_ID,ALS_APD_ID,ALS_REMARKS  FROM  ATTENDANCE_LOG_STUDENT AS S INNER JOIN " & _
        '" ATTENDANCE_LOG_GRADE AS G ON S.ALS_ALG_ID=G.ALG_ID AND ALG_ATTDT='" & ASONDATE & "'  AND ALG_SCT_ID IN( " & _
        '" SELECT DISTINCT STU_SCT_ID FROM STUDENT_M WHERE STU_ID IN(SELECT AVSG_STU_ID " & _
        ' " from  ATT.ATTENDANCE_VSTUDENT_GROUP where AVSG_AVG_ID='" & AVG_ID & "'  and AVSG_bREMOVE=0)) AND " & _
        '" ALS_STU_ID IN(SELECT AVSG_STU_ID from  ATT.ATTENDANCE_VSTUDENT_GROUP where AVSG_AVG_ID='" & AVG_ID & "' and AVSG_bREMOVE=0)"
        Dim command As SqlCommand = New SqlCommand(sqlALG_StudentID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetALG_StudentID(ByVal ALG_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlALG_StudentID As String = ""

        sqlALG_StudentID = "SELECT  ALS_STU_ID,ALS_APD_ID,ALS_REMARKS  FROM  ATTENDANCE_LOG_STUDENT where ALS_ALG_ID='" & ALG_ID & "' "

        Dim command As SqlCommand = New SqlCommand(sqlALG_StudentID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetHoliday_GradeSection(ByVal Code_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --14/Jul/2008
        'Purpose--Get Holiday Details
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlHoliday_GradeSection As String = ""

        sqlHoliday_GradeSection = " SELECT distinct  GRADE_BSU_M.GRM_DISPLAY AS GRD, SECTION_M.SCT_DESCR AS SCT,GRADE_M.GRD_DISPLAYORDER" & _
" FROM SCHOOL_HOLIDAY_GRADE INNER JOIN  GRADE_BSU_M ON SCHOOL_HOLIDAY_GRADE.SHG_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
" SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN SECTION_M ON SCHOOL_HOLIDAY_GRADE.SHG_SCT_ID = SECTION_M.SCT_ID AND " & _
" SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID = SECTION_M.SCT_ACD_ID AND SCHOOL_HOLIDAY_GRADE.SHG_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN " & _
" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where SCHOOL_HOLIDAY_GRADE.SHG_SCH_ID='" & Code_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

        Dim command As SqlCommand = New SqlCommand(sqlHoliday_GradeSection, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetSCHOOL_HOLIDAY_M(ByVal SCH_ID As String) As SqlDataReader
        ' Author(--Lijo)
        'Date   --16/Jul/2008
        'Purpose--Get Holiday Details
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSCHOOL_HOLIDAY_M As String = ""

        sqlSCHOOL_HOLIDAY_M = "SELECT [SCH_ACD_ID],[SCH_DTFROM],[SCH_DTTO],[SCH_REMARKS],[SCH_TYPE]" & _
      " ,[SCH_DAY_SL],[SCH_bDELETED],[SCH_bGRADEALL],[SCH_WEEKEND1_WORK]" & _
     " ,[SCH_bWEEKEND1_LOG_BOOK] ,[SCH_WEEKEND2_WORK],[SCH_bWEEKEND2_LOG_BOOK]" & _
     " FROM [SCHOOL_HOLIDAY_M] where  [SCH_ID]='" & SCH_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlSCHOOL_HOLIDAY_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GetSCHOOL_HOLIDAY_GRADE(ByVal SCH_ID As String) As SqlDataReader
        ' Author(--Lijo)
        'Date   --16/Jul/2008
        'Purpose--Get SCHOOL_HOLIDAY_GRADE
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSCHOOL_HOLIDAY_GRADE As String = ""

        sqlSCHOOL_HOLIDAY_GRADE = "SELECT [SHG_GRD_ID],[SHG_SCT_ID]" & _
  " FROM [SCHOOL_HOLIDAY_GRADE] where [SHG_SCH_ID]='" & SCH_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlSCHOOL_HOLIDAY_GRADE, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetHoliday_details(ByVal Code_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --14/Jul/2008
        'Purpose--Get Holiday Details
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlHoliday_D As String = ""

        sqlHoliday_D = "SELECT     SCH_ID, DATEDIFF(dd, SCH_DTFROM, SCH_DTTO) + 1 AS totDay, SCH_DTFROM, SCH_DTTO, SCH_REMARKS, SCH_TYPE " & _
       " FROM SCHOOL_HOLIDAY_M WHERE  SCH_ID = '" & Code_ID & "' "

        Dim command As SqlCommand = New SqlCommand(sqlHoliday_D, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetSTUD_Leave_Approval_ROOM(ByVal ACD_ID As String, ByVal SGR_ID As String, ByVal GRD_ID As String, ByVal ApprDate As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/Jun/2008
        'Purpose--Get STUD_Leave_Approval
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_Leave_Approval As String = ""

        sqlSTUD_Leave_Approval = " SELECT [SLA_STU_ID]  ,[SLA_REMARKS],[SLA_APPRLEAVE],isnull(SLA_APD_ID,0) as SLA_APD_ID  FROM [STUDENT_LEAVE_APPROVAL] " & _
" where [SLA_ACD_ID]='" & ACD_ID & "' AND [SLA_STU_ID] IN(SELECT DISTINCT SSD_STU_ID from   STUDENT_GROUPS_S " & _
" where SSD_GRD_ID='" & GRD_ID & "' and SSD_ACD_ID='" & ACD_ID & "' and SSD_SGR_ID='" & SGR_ID & "') AND '" & ApprDate & "' between [SLA_FROMDT] and isnull([SLA_TODT],'" & ApprDate & "') " & _
" and ([SLA_APPRLEAVE]='APPROVED' or [SLA_APPRLEAVE]='NOT APPROVED') "


        Dim command As SqlCommand = New SqlCommand(sqlSTUD_Leave_Approval, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GetSTUD_Leave_Approval_Group(ByVal ACD_ID As String, ByVal AVG_ID As String, ByVal ApprDate As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/Jun/2008
        'Purpose--Get STUD_Leave_Approval
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_Leave_Approval As String = ""

        sqlSTUD_Leave_Approval = " SELECT [SLA_STU_ID]  ,[SLA_REMARKS],[SLA_APPRLEAVE],isnull(SLA_APD_ID,0) as SLA_APD_ID  FROM [STUDENT_LEAVE_APPROVAL] " & _
" where [SLA_ACD_ID]='" & ACD_ID & "' AND [SLA_STU_ID] IN(SELECT AVSG_STU_ID from  ATT.ATTENDANCE_VSTUDENT_GROUP " & _
" where AVSG_AVG_ID='" & AVG_ID & "' and AVSG_bREMOVE=0) AND '" & ApprDate & "' between [SLA_FROMDT] and isnull([SLA_TODT],'" & ApprDate & "') " & _
" and ([SLA_APPRLEAVE]='APPROVED' or [SLA_APPRLEAVE]='NOT APPROVED') "


        Dim command As SqlCommand = New SqlCommand(sqlSTUD_Leave_Approval, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetSTUD_Leave_Approval(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, _
       ByVal STM_ID As String, ByVal SHF_ID As String, ByVal ApprDate As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/Jun/2008
        'Purpose--Get STUD_Leave_Approval
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_Leave_Approval As String = ""

        sqlSTUD_Leave_Approval = " SELECT [SLA_STU_ID]  ,[SLA_REMARKS],[SLA_APPRLEAVE],isnull(SLA_APD_ID,0) as SLA_APD_ID   FROM [STUDENT_LEAVE_APPROVAL] where [SLA_ACD_ID]='" & ACD_ID & "'" & _
    " and  [SLA_GRD_ID]='" & GRD_ID & "' and  [SLA_SCT_ID]='" & SCT_ID & "' and [SLA_STM_ID]='" & STM_ID & "' and [SLA_SHF_ID]='" & SHF_ID & "'" & _
    " and '" & ApprDate & "' between [SLA_FROMDT] and isnull([SLA_TODT],'" & ApprDate & "')  and ([SLA_APPRLEAVE]='APPROVED' or [SLA_APPRLEAVE]='NOT APPROVED')"

        Dim command As SqlCommand = New SqlCommand(sqlSTUD_Leave_Approval, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetAtt_type(ByVal ACD_ID As String, ByVal CheckDate As String) As SqlDataReader


        'Author(--Lijo)
        'Date   --29/Jun/2008
        'Purpose--Get Attendance_Type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlAtt_type As String = ""

        sqlAtt_type = " SELECT distinct [ATT_TYPE]  FROM [ATTENDANCE_M] where '" & CheckDate & "' between ATT_FROMDT and ATT_TODT and [ATT_ACD_ID]='" & ACD_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlAtt_type, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetAtt_SESSION_COUNT(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal FMONTH As String) As SqlDataReader


        'Author(--Lijo)
        'Date   --29/Jun/2008
        'Purpose--Get Attendance_Type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlAtt_SESSION As String = ""

        sqlAtt_SESSION = "SELECT SES_LIST FROM [STU].[fn_SessionCheck]('" & ACD_ID & "','" & GRD_ID & "','" & FMONTH & "')"

        Dim command As SqlCommand = New SqlCommand(sqlAtt_SESSION, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GetAtt_type_NEW(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal ChkDate As String) As SqlDataReader


        'Author(--Lijo)
        'Date   --29/Jun/2008
        'Purpose--Get Attendance_Type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlAtt_type As String = ""

        sqlAtt_type = " SELECT distinct [ATT_TYPE]  FROM [ATTENDANCE_M] where [ATT_ACD_ID]='" & ACD_ID & "' AND ATT_GRD_ID='" & GRD_ID & "' AND '" & ChkDate & "' BETWEEN ATT_FROMDT AND ATT_TODT"

        Dim command As SqlCommand = New SqlCommand(sqlAtt_type, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetAttendance_Param_NEW(ByVal ACD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/Jun/2008
        'Purpose--Get STUD_Leave_Approval
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlAttendance_Param As String = ""

        sqlAttendance_Param = " SELECT ATTENDANCE_PARAM_D.APD_PARAM_DESCR AS ARP_DESCR, ATTENDANCE_REPORT_PARAM.ARP_DISP AS ARP_DISP  FROM  ATTENDANCE_PARAM_D INNER JOIN " & _
 " ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID " & _
" where ATTENDANCE_PARAM_D.APD_PARAM_DESCR in ('SESSION1','SESSION2','PRESENT','PRESENT2') AND ATTENDANCE_REPORT_PARAM.ARP_ACD_ID='" & ACD_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlAttendance_Param, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function



    Public Shared Function GetAttendance_Param(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/Jun/2008
        'Purpose--Get STUD_Leave_Approval
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlAttendance_Param As String = ""

        sqlAttendance_Param = " SELECT  distinct [ARS_PRE_DESC_S1] ,[ARS_PRE_DISP_S1] ,[ARS_PRE_DESC_S2] " & _
     " ,[ARS_PRE_DISP_S2],[ARS_ABS_DESC],[ARS_ABS_DISP] " & _
     " ,[ARS_UNMARK_DESC],[ARS_UNMARK_DISP] ,[ARS_LATE_DESC] " & _
     " ,[ARS_LATE_DISP],[ARS_LEAVE_DESC],[ARS_LEAVE_DISP] " & _
     " ,[ARS_S1_DESC],[ARS_S1_DISP],[ARS_S2_DESC] ,[ARS_S2_DISP] " & _
 " FROM [ATTENDANCE_REPORT_SETTING] where " & _
" [ARS_ACD_ID]='" & ACD_ID & "' and [ARS_GRD_ID]='" & GRD_ID & "' and [ARS_BSU_ID]='" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlAttendance_Param, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GetAttendance_M(ByVal ATT_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --28/Jun/2008
        'Purpose--Get STUD_Leave_Approval
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlAttendance_M As String = ""

        sqlAttendance_M = "SELECT [ATT_ACD_ID] ,[ATT_BSU_ID] ,[ATT_GRD_ID],[ATT_FROMDT],[ATT_TODT],[ATT_TYPE]  FROM [ATTENDANCE_M] where ATT_ID='" & ATT_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlAttendance_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GetSTUD_GRADE_SECTION_HOLIDAY(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, ByVal TDATE As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --20/Jul/2008
        'modified tcissue date  between clause
        'Purpose--Get Att Type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_GRADE_SECTION_HOLIDAY As String = ""

        sqlSTUD_GRADE_SECTION_HOLIDAY = " SELECT SCHOOL_HOLIDAY_M.SCH_TYPE as STYPE,SCHOOL_HOLIDAY_M.SCH_WEEKEND1_WORK as WEEKEND1," & _
 " SCHOOL_HOLIDAY_M.SCH_bWEEKEND1_LOG_BOOK as bWEEKEND1,SCHOOL_HOLIDAY_M.SCH_WEEKEND2_WORK as WEEKEND2, SCHOOL_HOLIDAY_M.SCH_bWEEKEND2_LOG_BOOK as bWEEKEND2 " & _
" FROM SCHOOL_HOLIDAY_GRADE INNER JOIN SCHOOL_HOLIDAY_M ON SCHOOL_HOLIDAY_GRADE.SHG_SCH_ID = SCHOOL_HOLIDAY_M.SCH_ID AND " & _
"  SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID = SCHOOL_HOLIDAY_M.SCH_ACD_ID where SCHOOL_HOLIDAY_M.SCH_bDELETED=0 and SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID='" & ACD_ID & "' " & _
"  and SCHOOL_HOLIDAY_GRADE.SHG_GRD_ID='" & GRD_ID & "' and SCHOOL_HOLIDAY_GRADE.SHG_SCT_ID='" & SCT_ID & "' and '" & TDATE & "' between SCHOOL_HOLIDAY_M.SCH_DTFROM and SCHOOL_HOLIDAY_M.SCH_DTTO "

        Dim command As SqlCommand = New SqlCommand(sqlSTUD_GRADE_SECTION_HOLIDAY, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetSTUD_GRADE_SECTION_HOLIDAY_GROUP(ByVal AVG_ID As String, ByVal TDATE As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --20/Jul/2008
        'modified tcissue date  between clause
        'Purpose--Get Att Type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_GRADE_SECTION_HOLIDAY As String = ""

        sqlSTUD_GRADE_SECTION_HOLIDAY = " declare @temp_acd_id int,@temp_grd_id varchar(10),@temp_sct_id int; " & _
" SELECT     @temp_grd_id=STUDENT_M.STU_GRD_ID, @temp_sct_id=STUDENT_M.STU_SCT_ID,@temp_acd_id= STUDENT_M.STU_ACD_ID " & _
" FROM         ATT.ATTENDANCE_VSTUDENT_GROUP INNER JOIN STUDENT_M ON ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_STU_ID = STUDENT_M.STU_ID AND " & _
" ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_ACD_ID = STUDENT_M.STU_ACD_ID WHERE (ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_AVG_ID ='" & AVG_ID & "' ) " & _
" AND (ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_bREMOVE = 0); " & _
" SELECT SCHOOL_HOLIDAY_M.SCH_TYPE as STYPE,SCHOOL_HOLIDAY_M.SCH_WEEKEND1_WORK as WEEKEND1," & _
" SCHOOL_HOLIDAY_M.SCH_bWEEKEND1_LOG_BOOK as bWEEKEND1,SCHOOL_HOLIDAY_M.SCH_WEEKEND2_WORK as WEEKEND2, " & _
" SCHOOL_HOLIDAY_M.SCH_bWEEKEND2_LOG_BOOK as bWEEKEND2 FROM SCHOOL_HOLIDAY_GRADE INNER JOIN SCHOOL_HOLIDAY_M ON " & _
" SCHOOL_HOLIDAY_GRADE.SHG_SCH_ID = SCHOOL_HOLIDAY_M.SCH_ID And SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID = SCHOOL_HOLIDAY_M.SCH_ACD_ID " & _
" where SCHOOL_HOLIDAY_M.SCH_bDELETED=0 and SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID=@temp_acd_id " & _
" and SCHOOL_HOLIDAY_GRADE.SHG_GRD_ID=@temp_grd_id and SCHOOL_HOLIDAY_GRADE.SHG_SCT_ID=@temp_sct_id " & _
" and '" & TDATE & "' between SCHOOL_HOLIDAY_M.SCH_DTFROM and SCHOOL_HOLIDAY_M.SCH_DTTO  "

        Dim command As SqlCommand = New SqlCommand(sqlSTUD_GRADE_SECTION_HOLIDAY, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetSTUD_ROOM_HOLIDAY_GROUP(ByVal ACD_ID As String, ByVal SGR_ID As String, ByVal TDATE As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --20/Jul/2008
        'modified tcissue date  between clause
        'Purpose--Get Att Type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_GRADE_SECTION_HOLIDAY As String = ""

        sqlSTUD_GRADE_SECTION_HOLIDAY = " declare @temp_grd_id varchar(10),@temp_sct_id int; " & _
" SELECT @temp_grd_id=STU_GRD_ID,@temp_sct_id=STU_SCT_ID FROM STUDENT_M INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID  " & _
" INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID WHERE STU_ACD_ID=  '" & ACD_ID & "'  AND SGR_ID= '" & SGR_ID & "'   AND STU_CURRSTATUS <> 'CN' AND " & _
" (convert(datetime,stu_leavedate) > convert(datetime,'" & TDATE & "') or convert(datetime,stu_leavedate) is null ) " & _
" and  STU_DOJ <= convert(datetime,'" & TDATE & "'); " & _
" SELECT SCHOOL_HOLIDAY_M.SCH_TYPE as STYPE,SCHOOL_HOLIDAY_M.SCH_WEEKEND1_WORK as WEEKEND1, " & _
" SCHOOL_HOLIDAY_M.SCH_bWEEKEND1_LOG_BOOK as bWEEKEND1,SCHOOL_HOLIDAY_M.SCH_WEEKEND2_WORK as WEEKEND2, " & _
" SCHOOL_HOLIDAY_M.SCH_bWEEKEND2_LOG_BOOK as bWEEKEND2 FROM SCHOOL_HOLIDAY_GRADE INNER JOIN SCHOOL_HOLIDAY_M ON  " & _
" SCHOOL_HOLIDAY_GRADE.SHG_SCH_ID = SCHOOL_HOLIDAY_M.SCH_ID And SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID = SCHOOL_HOLIDAY_M.SCH_ACD_ID  " & _
" where SCHOOL_HOLIDAY_M.SCH_bDELETED=0 and SCHOOL_HOLIDAY_GRADE.SHG_ACD_ID='" & ACD_ID & "' " & _
" and SCHOOL_HOLIDAY_GRADE.SHG_GRD_ID=@temp_grd_id and SCHOOL_HOLIDAY_GRADE.SHG_SCT_ID=@temp_sct_id  " & _
" and '" & TDATE & "' between SCHOOL_HOLIDAY_M.SCH_DTFROM and SCHOOL_HOLIDAY_M.SCH_DTTO "

        Dim command As SqlCommand = New SqlCommand(sqlSTUD_GRADE_SECTION_HOLIDAY, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GETSTUDATT_REGIST_GROUP(ByVal ACD_ID As String, ByVal AVSG_AVG_ID As String, _
         ByVal ASONDATE As String, ByVal STARTDATE As String, ByVal ENDDATE As String, ByVal WEEKEND1 As String, _
   ByVal WEEKEND2 As String, ByVal SessionType As String) As SqlDataReader

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(15) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
        pParms(1) = New SqlClient.SqlParameter("@AVSG_AVG_ID", AVSG_AVG_ID)
        pParms(2) = New SqlClient.SqlParameter("@ASONDATE", ASONDATE)
        pParms(3) = New SqlClient.SqlParameter("@STARTDATE", STARTDATE)
        pParms(4) = New SqlClient.SqlParameter("@ENDDATE", ENDDATE)
        pParms(5) = New SqlClient.SqlParameter("@WEEKEND1", WEEKEND1)
        pParms(6) = New SqlClient.SqlParameter("@WEEKEND2", WEEKEND2)
        pParms(7) = New SqlClient.SqlParameter("@Session", SessionType)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.GETSTUDATT_REGIST_GROUP", pParms)
        Return reader

    End Function

    Public Shared Function GETSTUDATT_REGIST_ROOMGROUP(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SGR_ID As String, _
            ByVal ASONDATE As String, ByVal PERIOD_ID As String, ByVal STARTDATE As String, ByVal ENDDATE As String, ByVal WEEKEND1 As String, _
      ByVal WEEKEND2 As String) As SqlDataReader

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(15) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
        pParms(1) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
        pParms(2) = New SqlClient.SqlParameter("@SGR_ID", SGR_ID)
        pParms(3) = New SqlClient.SqlParameter("@ASONDATE", ASONDATE)
        pParms(4) = New SqlClient.SqlParameter("@PERIOD_ID", PERIOD_ID)
        pParms(5) = New SqlClient.SqlParameter("@STARTDATE", STARTDATE)
        pParms(6) = New SqlClient.SqlParameter("@ENDDATE", ENDDATE)
        pParms(7) = New SqlClient.SqlParameter("@WEEKEND1", WEEKEND1)
        pParms(8) = New SqlClient.SqlParameter("@WEEKEND2", WEEKEND2)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.GETSTUDATT_REGIST_ROOMGROUP", pParms)
        Return reader

    End Function




    Public Shared Function GetSTUD_ATT_ADD(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, _
      ByVal ASONDATE As String, ByVal STARTDATE As String, ByVal ENDDATE As String, ByVal WEEKEND1 As String, _
ByVal WEEKEND2 As String, ByVal SESSION As String) As SqlDataReader



        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(15) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
        pParms(1) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
        pParms(2) = New SqlClient.SqlParameter("@SCT_ID", SCT_ID)
        pParms(3) = New SqlClient.SqlParameter("@ASONDATE", ASONDATE)
        pParms(4) = New SqlClient.SqlParameter("@STARTDATE", STARTDATE)
        pParms(5) = New SqlClient.SqlParameter("@ENDDATE", ENDDATE)
        pParms(6) = New SqlClient.SqlParameter("@WEEKEND1", WEEKEND1)
        pParms(7) = New SqlClient.SqlParameter("@WEEKEND2", WEEKEND2)
        pParms(8) = New SqlClient.SqlParameter("@SESSION", SESSION)

        'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.GETSTUDATT_REGIST", pParms)
        Return reader


        '        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        '        Dim sqlSTUD_ATT_ADD As String = ""

        '        sqlSTUD_ATT_ADD = " SELECT distinct  STUDENT_M.STU_MINLIST AS STU_MINLIST,   STUDENT_M.STU_ID AS STU_ID, STUDENT_M.STU_NO AS STU_NO, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME),'') " & _
        '                     " + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME),'') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME),'') AS STUDNAME , STUDENT_M.STU_GENDER as SGENDER" & _
        '              " FROM  STUDENT_M INNER JOIN STAFF_AUTHORIZED_ATT ON STUDENT_M.STU_ACD_ID = STAFF_AUTHORIZED_ATT.SAD_ACD_ID AND " & _
        '                    "  STUDENT_M.STU_GRD_ID = STAFF_AUTHORIZED_ATT.SAD_GRD_ID AND STUDENT_M.STU_SCT_ID = STAFF_AUTHORIZED_ATT.SAD_SCT_ID AND " & _
        '     " STUDENT_M.STU_STM_ID = STAFF_AUTHORIZED_ATT.SAD_STM_ID And STUDENT_M.STU_SHF_ID = STAFF_AUTHORIZED_ATT.SAD_SHF_ID " & _
        '" where STU_CURRSTATUS <> 'CN' AND STUDENT_M.STU_ACD_ID='" & ACD_ID & "' and STUDENT_M.STU_GRD_ID='" & GRD_ID & "' and STUDENT_M.STU_SCT_ID='" & SCT_ID & "' and  STUDENT_M.STU_STM_ID='" & STM_ID & "' and STUDENT_M.STU_SHF_ID='" & SHF_ID & "' " & _
        '" and  STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & EMP_ID & "'  and  (convert(datetime,'" & lastAttDate & "')<STU_LEAVEDATE  " & _
        '" OR convert(datetime,STU_LEAVEDATE) is null) AND (convert(datetime,'" & lastAttDate & "')  between SAD_FROMDT  AND ISNULL(SAD_TODT,convert(datetime,'" & lastAttDate & "'))) and STUDENT_M.STU_DOJ <= convert(datetime,'" & lastAttDate & "') order by STUDNAME"

        '        Dim command As SqlCommand = New SqlCommand(sqlSTUD_ATT_ADD, connection)
        '        command.CommandType = CommandType.Text
        '        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        '        SqlConnection.ClearPool(connection)
        '        Return reader


    End Function

    Public Shared Function GetGrade_Report_Para(ByVal ACD_ID As String, ByVal GRD_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --25/oct/2008

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Report As String = ""

        sqlGrade_Report = " SELECT  [ARS_PRE_DESC_S1] ,[ARS_PRE_DISP_S1] ,[ARS_PRE_DESC_S2],[ARS_PRE_DISP_S2] " & _
      " ,[ARS_ABS_DESC],[ARS_ABS_DISP],[ARS_UNMARK_DESC],[ARS_UNMARK_DISP] ,[ARS_LATE_DESC]" & _
      " ,[ARS_LATE_DISP],[ARS_LEAVE_DESC],[ARS_LEAVE_DISP]  ,[ARS_S1_DESC] " & _
      " ,[ARS_S1_DISP] ,[ARS_S2_DESC] ,[ARS_S2_DISP] " & _
      "  FROM ATTENDANCE_REPORT_SETTING  where ARS_ACD_ID='" & ACD_ID & "' AND ARS_GRD_ID='" & GRD_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlGrade_Report, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetStudLeavesApproval_edit(ByVal SLA_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --02/Jun/2008
        'Purpose--Get Student ID Detail
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_Leave_edit As String = ""

        sqlSTUD_Leave_edit = " SELECT    STUDENT_M.STU_NO AS STUD_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') " & _
                      " + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS SNAME, " & _
                      " STUDENT_LEAVE_APPROVAL.SLA_FROMDT AS FROMDT, STUDENT_LEAVE_APPROVAL.SLA_TODT AS TODT, " & _
                      " STUDENT_LEAVE_APPROVAL.SLA_REMARKS AS REMARKS, STUDENT_LEAVE_APPROVAL.SLA_APPRLEAVE AS STATUS, " & _
                      " GRADE_BSU_M.GRM_DISPLAY AS GRM_DIS, SECTION_M.SCT_DESCR, ACADEMICYEAR_M.ACY_DESCR,STUDENT_M.STU_ACD_ID AS ACD_ID,isnull(STUDENT_LEAVE_APPROVAL.SLA_APD_ID,0) as SLA_APD_ID " & _
                      " FROM  ACADEMICYEAR_M INNER JOIN  ACADEMICYEAR_D ON ACADEMICYEAR_M.ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID INNER JOIN " & _
                      " STUDENT_LEAVE_APPROVAL INNER JOIN STUDENT_M ON STUDENT_LEAVE_APPROVAL.SLA_STU_ID = STUDENT_M.STU_ID INNER JOIN " & _
                      " GRADE_BSU_M ON STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID INNER JOIN " & _
                      " SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID AND STUDENT_M.STU_GRD_ID = SECTION_M.SCT_GRD_ID AND " & _
                      " STUDENT_M.STU_ACD_ID = SECTION_M.SCT_ACD_ID ON ACADEMICYEAR_D.ACD_ID = STUDENT_M.STU_ACD_ID " & _
                      " WHERE  STUDENT_LEAVE_APPROVAL.SLA_ID ='" & SLA_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlSTUD_Leave_edit, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetStudent_Detail_Leaves(ByVal STU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --02/Jun/2008
        'Purpose--Get Student ID Detail
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_Leave_Entry As String = ""

        sqlSTUD_Leave_Entry = " SELECT     ACADEMICYEAR_M.ACY_DESCR as AcdYear, GRADE_BSU_M.GRM_DISPLAY as GRD_DISP, SECTION_M.SCT_DESCR as SCT_DESCR, SHIFTS_M.SHF_DESCR as SHF_DESCR, " & _
                     " STREAM_M.STM_DESCR as STM_DESCR, STUDENT_M.STU_FEE_ID as FEE_ID, STUDENT_M.STU_BLUEID as BLUEID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') " & _
                     " + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS STUDNAME ," & _
                     " STUDENT_M.STU_ID as STU_ID, STUDENT_M.STU_ACD_ID as ACD_ID, STUDENT_M.STU_GRD_ID as GRD_ID, STUDENT_M.STU_SCT_ID as SCT_ID, STUDENT_M.STU_STM_ID as STM_ID, STUDENT_M.STU_SHF_ID as SHF_ID " & _
" FROM  STREAM_M INNER JOIN   STUDENT_M INNER JOIN   ACADEMICYEAR_M INNER JOIN   ACADEMICYEAR_D ON ACADEMICYEAR_M.ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID ON " & _
                      " STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN  GRADE_BSU_M ON STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                     " STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN   SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID AND " & _
 " STUDENT_M.STU_ACD_ID = SECTION_M.SCT_ACD_ID INNER JOIN  SHIFTS_M ON STUDENT_M.STU_SHF_ID = SHIFTS_M.SHF_ID ON STREAM_M.STM_ID = STUDENT_M.STU_STM_ID " & _
" where STUDENT_M.STU_ID='" & STU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlSTUD_Leave_Entry, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function




    Public Shared Function GetSTUD_Leave_Entry(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, _
       ByVal STM_ID As String, ByVal SHF_ID As String, ByVal EMP_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --02/Jun/2008
        'Purpose--Get STUDENT_M ,STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlSTUD_Leave_Entry As String = ""

        sqlSTUD_Leave_Entry = " SELECT ROW_NUMBER() OVER (ORDER BY STUDENT_M.STU_FIRSTNAME)  AS SRNO,    STUDENT_M.STU_ID AS STU_ID, STUDENT_M.STU_NO AS STU_NO, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME),'') " & _
                     " + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME),'') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME),'') AS STUDNAME " & _
              " FROM  STUDENT_M INNER JOIN STAFF_AUTHORIZED_ATT ON STUDENT_M.STU_ACD_ID = STAFF_AUTHORIZED_ATT.SAD_ACD_ID AND " & _
                    "  STUDENT_M.STU_GRD_ID = STAFF_AUTHORIZED_ATT.SAD_GRD_ID AND STUDENT_M.STU_SCT_ID = STAFF_AUTHORIZED_ATT.SAD_SCT_ID AND " & _
     " STUDENT_M.STU_STM_ID = STAFF_AUTHORIZED_ATT.SAD_STM_ID And STUDENT_M.STU_SHF_ID = STAFF_AUTHORIZED_ATT.SAD_SHF_ID " & _
" where STUDENT_M.STU_ACD_ID='" & ACD_ID & "' and STUDENT_M.STU_GRD_ID='" & GRD_ID & "' and STUDENT_M.STU_SCT_ID='" & SCT_ID & "' and  STUDENT_M.STU_STM_ID='" & STM_ID & "' and STUDENT_M.STU_SHF_ID='" & SHF_ID & "' " & _
" and  STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & EMP_ID & "'"

        Dim command As SqlCommand = New SqlCommand(sqlSTUD_Leave_Entry, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function
    Public Shared Function GetRAL_ID(ByVal ACD_ID As String, ByVal SGR_ID As String, ByVal PERIOD_ID As String, ByVal AttDate As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--GET THE GROUP MARKED FOR ATTENDANCE
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetALG_ID As String = ""

        sqlGetALG_ID = "SELECT RAL_ID FROM ATT.ROOM_ATTENDANCE_LOG WHERE RAL_PERIOD_ID='" & PERIOD_ID & "' AND RAL_SGR_ID='" & SGR_ID & "' AND RAL_ACD_ID='" & ACD_ID & "' AND RAL_ATTDT=convert(datetime,'" & AttDate & "')"
        Dim command As SqlCommand = New SqlCommand(sqlGetALG_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetALVG_ID(ByVal AVG_ID As String, ByVal lastAttDate As String, ByVal ATT_TYPE As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--GET THE GROUP MARKED FOR ATTENDANCE
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetALG_ID As String = ""

        sqlGetALG_ID = "SELECT ALVG_ID FROM ATT.ATTENDANCE_LOG_VGROUP WHERE ALVG_AVG_ID='" & AVG_ID & "' AND ALVG_ATTDT=convert(datetime,'" & lastAttDate & "') and ALVG_ATT_TYPE='" & ATT_TYPE & "'"
        Dim command As SqlCommand = New SqlCommand(sqlGetALG_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetALG_ID(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, _
    ByVal STM_ID As String, ByVal SHF_ID As String, ByVal ATT_TYPE As String, ByVal lastAttDate As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--Get STAFF_AUTHORIZED_ATT
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetALG_ID As String = ""

        sqlGetALG_ID = "SELECT  ALG_ID  FROM  ATTENDANCE_LOG_GRADE where " & _
  " ALG_ACD_ID='" & ACD_ID & "' and ALG_GRD_ID ='" & GRD_ID & "' and ALG_SCT_ID='" & SCT_ID & "' and ALG_SHF_ID='" & SHF_ID & "'" & _
 " and ALG_STM_ID='" & STM_ID & "' and ALG_ATT_TYPE ='" & ATT_TYPE & "' and ALG_ATTDT=convert(datetime,'" & lastAttDate & "')"

        Dim command As SqlCommand = New SqlCommand(sqlGetALG_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GETSTUATT_YEAR_STU_ID_SESSION(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, _
                   ByVal STARTDATE As String, ByVal ENDDATE As String, ByVal STU_ID As String, ByVal ATT_TYPE As String, _
                   ByVal WEEKEND1 As String, ByVal WEEKEND2 As String, ByVal TILL_DATE As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --12/AUG/2008
        'Purpose--To save GETSTUATT_YEAR_STU_ID_SESSION data based 
        '@ACD_ID INT='80',
        '@GRD_ID varchar(50)='KG1',
        '@SCT_ID BIGINT='6958',
        '@STARTDATE datetime='01/JUN/2008',
        '@ENDDATE datetime='31/MAY/2009',
        '@STU_ID bigint ='61421',
        '@ATT_TYPE VARCHAR(15)='Session1',
        '@WEEKEND1 varchar(12)='FRIDAY',
        '@WEEKEND2 varchar(12)='SATURDAY',
        '@TILL_DATE datetime='12/JUL/2008'


        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As New SqlCommand("GETSTUATT_YEAR_STU_ID_SESSION", connection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add(New SqlClient.SqlParameter("@ACD_ID", ACD_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@GRD_ID", GRD_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@SCT_ID", SCT_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@STARTDATE", STARTDATE))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@ENDDATE", ENDDATE))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@STU_ID", STU_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@ATT_TYPE", ATT_TYPE))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@WEEKEND1", WEEKEND1))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@WEEKEND2", WEEKEND2))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@TILL_DATE", TILL_DATE))

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GETREPORT_YEAR_ACD(ByVal ACD As String, ByVal GRD As String, ByVal SCT As Integer, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String, ByVal FMONTH As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetREPORT_YEAR_ACD As String = ""

        sqlGetREPORT_YEAR_ACD = "select DISTINCT YEAR(WDATE) as TYEAR from fn_TWORKINGDAYS('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "') where RTRIM(UPPER(substring(DATENAME(m,WDATE),1,3)))=UPPER(rtrim('" & FMONTH & "'))"

        Dim command As SqlCommand = New SqlCommand(sqlGetREPORT_YEAR_ACD, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetATT_PARAM() As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim Acd_Year As String = " SELECT  distinct APM_ID, APM_DESCR FROM ATTENDANCE_PARAM_M ORDER BY APM_ID"
        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, Acd_Year)
        SqlConnection.ClearPool(connection)
        Return ds1
    End Function
    Public Shared Function GetAtt_PARAM_DETAILS(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim Att_PARAM_DETAILS As String = String.Empty

        Att_PARAM_DETAILS = " SELECT    ATTENDANCE_PARAM_M.APM_DESCR AS ATT_CAT_DESCR, ATTENDANCE_PARAM_D.APD_PARAM_DESCR AS ATT_PARAM_DESCR, ATTENDANCE_PARAM_D.APD_ID AS ATT_PARAM_ID,ATTENDANCE_PARAM_D.APD_APM_ID AS ATT_CAT_ID, ATTENDANCE_PARAM_D.APD_bShow AS ATT_APD_bSHOW, 'NORMAL' as STATUS " & _
       " ,CASE WHEN ATTENDANCE_PARAM_D.APD_bPHY_ABS IS NULL THEN CASE WHEN ATTENDANCE_PARAM_D.APD_APM_ID=1 THEN 0 ELSE 1 END ELSE ATTENDANCE_PARAM_D.APD_bPHY_ABS END AS APD_bPHY_ABS " & _
 " FROM ATTENDANCE_PARAM_M INNER JOIN  ATTENDANCE_PARAM_D ON ATTENDANCE_PARAM_M.APM_ID = ATTENDANCE_PARAM_D.APD_APM_ID " & _
" WHERE ATTENDANCE_PARAM_D.APD_ACD_ID='" & ACD_ID & "' AND  ATTENDANCE_PARAM_D.APD_BSU_ID='" & BSU_ID & "' ORDER BY ATTENDANCE_PARAM_D.APD_ID"


        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, Att_PARAM_DETAILS)
        SqlConnection.ClearPool(connection)
        Return ds1


    End Function

    Public Shared Function GetAtt_ROOMPARAM_DETAILS(ByVal BSU_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim Att_PARAM_DETAILS As String = String.Empty

        Att_PARAM_DETAILS = "SELECT    R.RAP_ID, R.RAP_PARAM_DESCR , R.RAP_DISP, M.APM_DESCR,M.APM_ID, 'NORMAL' as STATUS " & _
" FROM ATT.ROOM_ATTENDANCE_PARAM_D R INNER JOIN   ATTENDANCE_PARAM_M M ON " & _
" R.RAP_APM_ID = M.APM_ID WHERE R.RAP_BSU_ID='" & BSU_ID & "' ORDER BY R.RAP_PARAM_DESCR"


        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, Att_PARAM_DETAILS)
        SqlConnection.ClearPool(connection)
        Return ds1


    End Function

    Public Shared Function GetAtt_PARAM_DISPLAY(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim Att_PARAM_DISPLAY As String = String.Empty

        Att_PARAM_DISPLAY = " SELECT  DISTINCT   ATTENDANCE_PARAM_D.APD_ID AS APD_ID, ATTENDANCE_PARAM_D.APD_PARAM_DESCR AS PARAM_DESCR, ATTENDANCE_REPORT_PARAM.ARP_DESCR AS ARP_DESCR, " & _
                     " ATTENDANCE_REPORT_PARAM.ARP_DISP AS ARP_DISP FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  " & _
                    "  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID  " & _
                    " WHERE  (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') ORDER BY ATTENDANCE_PARAM_D.APD_ID "



        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, Att_PARAM_DISPLAY)
        SqlConnection.ClearPool(connection)
        Return ds1


    End Function
    Public Shared Function GetATTENDANCE_ROOM_PARAM_D(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim ATT_PARAM_D As String = String.Empty



        ATT_PARAM_D = " SELECT R1,APD_ID,APD_PARAM_DESCR FROM ( " & _
" SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_PARAM_DESCR) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR]   " & _
 " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID    " & _
 " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION  " & _
" SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')'  " & _
"  AS [APD_PARAM_DESCR]   FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A ORDER BY R1"


        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, ATT_PARAM_D)
        SqlConnection.ClearPool(connection)
        Return ds1


    End Function



    Public Shared Function GetATTENDANCE_PARAM_D(ByVal ACD_ID As String, ByVal BSU_ID As String, ByVal SESS_TYPE As String) As DataSet
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As DataSet
        Dim ATT_PARAM_D As String = String.Empty


        If UCase(Trim(SESS_TYPE)) = "SESSION1" Then
            ATT_PARAM_D = "SELECT ATTENDANCE_PARAM_D.APD_ID AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR] " & _
                 " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID  " & _
               "  WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "' ) AND " & _
               " (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) ORDER BY isnull(ATTENDANCE_PARAM_D.APD_DISPLAYORDER,4) asc "
        ElseIf UCase(Trim(SESS_TYPE)) = "SESSION2" Then
            ATT_PARAM_D = " SELECT ATTENDANCE_PARAM_D.APD_ID AS [APD_ID],case when  upper(ATTENDANCE_PARAM_D.APD_PARAM_DESCR)='PRESENT' " & _
            " then (select  ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ ISNULL(P.ARP_DISP,'') +')'  FROM ATTENDANCE_PARAM_D as D INNER JOIN  ATTENDANCE_REPORT_PARAM as P ON D.APD_ID =P.ARP_APD_ID where D.APD_ID =ATTENDANCE_PARAM_D.APD_ID+1) " & _
            " else ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ ISNULL(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' end as [APD_PARAM_DESCR] " & _
            " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID  " & _
             "  WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "' ) AND " & _
            " (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) ORDER BY isnull(ATTENDANCE_PARAM_D.APD_DISPLAYORDER,4) asc "
        End If



        ds1 = SqlHelper.ExecuteDataset(connection, CommandType.Text, ATT_PARAM_D)
        SqlConnection.ClearPool(connection)
        Return ds1


    End Function
#End Region
#Region "ALL THE SAVE STATEMENT"
    Public Shared Function UPDATE_ROOM_ATT_PERIOD_GRADES(ByVal GRD_IDS As String, ByVal BSU_ID As String, ByVal PERIODS As String, ByVal b_Edit As Boolean, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@GRD_IDS", GRD_IDS)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@PERIODS", PERIODS)
                pParms(3) = New SqlClient.SqlParameter("@b_Edit", b_Edit)
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ATT.SAVEROOM_ATT_PERIOD_GRADES", pParms)
                Dim ReturnFlag As Integer = pParms(4).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function
    Public Shared Function SAVEATTENDANCE_REPORT_SETTING(ByVal ARS_ACD_ID As String, ByVal ARS_GRD_ID As String, _
                          ByVal ARS_BSU_ID As String, ByVal ARS_PRE_DESC_S1 As String, ByVal ARS_PRE_DISP_S1 As String, ByVal ARS_PRE_DESC_S2 As String, ByVal ARS_PRE_DISP_S2 As String, _
                          ByVal ARS_ABS_DESC As String, ByVal ARS_ABS_DISP As String, ByVal ARS_UNMARK_DESC As String, _
                          ByVal ARS_UNMARK_DISP As String, ByVal ARS_LATE_DESC As String, ByVal ARS_LATE_DISP As String, _
                          ByVal ARS_LEAVE_DESC As String, ByVal ARS_LEAVE_DISP As String, ByVal ARS_S1_DESC As String, _
                          ByVal ARS_S1_DISP As String, ByVal ARS_S2_DESC As String, ByVal ARS_S2_DISP As String, _
                          ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To save ATTENDANCE_REPORT_SETTING data based 
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(19) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@ARS_ACD_ID", ARS_ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@ARS_GRD_ID", ARS_GRD_ID)
                pParms(2) = New SqlClient.SqlParameter("@ARS_BSU_ID", ARS_BSU_ID)


                pParms(3) = New SqlClient.SqlParameter("@ARS_PRE_DESC_S1", ARS_PRE_DESC_S1)
                pParms(4) = New SqlClient.SqlParameter("@ARS_PRE_DISP_S1", ARS_PRE_DISP_S1)
                pParms(5) = New SqlClient.SqlParameter("@ARS_PRE_DESC_S2", ARS_PRE_DESC_S2)
                pParms(6) = New SqlClient.SqlParameter("@ARS_PRE_DISP_S2", ARS_PRE_DISP_S2)

                pParms(7) = New SqlClient.SqlParameter("@ARS_ABS_DESC", ARS_ABS_DESC)
                pParms(8) = New SqlClient.SqlParameter("@ARS_ABS_DISP", ARS_ABS_DISP)
                pParms(9) = New SqlClient.SqlParameter("@ARS_UNMARK_DESC", ARS_UNMARK_DESC)
                pParms(10) = New SqlClient.SqlParameter("@ARS_UNMARK_DISP", ARS_UNMARK_DISP)


                pParms(11) = New SqlClient.SqlParameter("@ARS_LATE_DESC", ARS_LATE_DESC)
                pParms(12) = New SqlClient.SqlParameter("@ARS_LATE_DISP", ARS_LATE_DISP)
                pParms(13) = New SqlClient.SqlParameter("@ARS_LEAVE_DESC", ARS_LEAVE_DESC)

                pParms(14) = New SqlClient.SqlParameter("@ARS_LEAVE_DISP", ARS_LEAVE_DISP)
                pParms(15) = New SqlClient.SqlParameter("@ARS_S1_DESC", ARS_S1_DESC)
                pParms(16) = New SqlClient.SqlParameter("@ARS_S1_DISP", ARS_S1_DISP)
                pParms(17) = New SqlClient.SqlParameter("@ARS_S2_DESC", ARS_S2_DESC)
                pParms(18) = New SqlClient.SqlParameter("@ARS_S2_DISP", ARS_S2_DISP)

                pParms(19) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(19).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SAVEATTENDANCE_REPORT_SETTING", pParms)
                Dim ReturnFlag As Integer = pParms(19).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function
    Public Shared Function SAVE_ROOMATT_PARAM_SET(ByVal RAP_ID As String, ByVal RAP_BSU_ID As String, ByVal RAP_PARAM_DESCR As String, _
                             ByVal RAP_APM_ID As String, ByVal RAP_DISP As String, ByVal Status As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To save ATTENDANCE_PARAM_D data based 
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RAP_ID", RAP_ID)
                pParms(1) = New SqlClient.SqlParameter("@RAP_BSU_ID", RAP_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@RAP_PARAM_DESCR", RAP_PARAM_DESCR)
                pParms(3) = New SqlClient.SqlParameter("@RAP_APM_ID", RAP_APM_ID)
                pParms(4) = New SqlClient.SqlParameter("@RAP_DISP", RAP_DISP)
                pParms(5) = New SqlClient.SqlParameter("@STATUS", Status)

                pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(6).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ATT.SAVE_ROOMATT_PARAM_SET", pParms)
                Dim ReturnFlag As Integer = pParms(6).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function



    Public Shared Function SAVE_ATT_PARAM_SET(ByVal APD_ID As String, ByVal APD_BSU_ID As String, ByVal APD_ACD_ID As String, _
                          ByVal APD_PARAM_DESCR As String, ByVal APD_APM_ID As String, ByVal APD_bSHOW As Boolean, ByVal Status As String, ByVal APD_bPHY_ABS As Boolean, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To save ATTENDANCE_PARAM_D data based 
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@APD_ID", APD_ID)
                pParms(1) = New SqlClient.SqlParameter("@APD_BSU_ID", APD_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@APD_ACD_ID", APD_ACD_ID)
                pParms(3) = New SqlClient.SqlParameter("@APD_PARAM_DESCR", APD_PARAM_DESCR)
                pParms(4) = New SqlClient.SqlParameter("@APD_APM_ID", APD_APM_ID)
                pParms(5) = New SqlClient.SqlParameter("@APD_bSHOW", APD_bSHOW)
                pParms(6) = New SqlClient.SqlParameter("@STATUS", Status)
                pParms(7) = New SqlClient.SqlParameter("@APD_bPHY_ABS", APD_bPHY_ABS)
                pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ATT.SAVE_ATT_PARAM_SET", pParms)
                Dim ReturnFlag As Integer = pParms(8).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return 1
        End Try
    End Function
    Public Shared Function SAVESTUDENT_LEAVE_APPROVAL(ByVal SLA_ID As String, ByVal SLA_EMP_ID_APPR As String, ByVal SLA_APPRLEAVE As String, _
         ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --06/Jul/2008
        'Purpose--To save STUDENT_LEAVE_APPROVAL data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SLA_ID", SLA_ID)
            pParms(1) = New SqlClient.SqlParameter("@SLA_EMP_ID_APPR", SLA_EMP_ID_APPR)
            pParms(2) = New SqlClient.SqlParameter("@SLA_APPRLEAVE", SLA_APPRLEAVE)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVESTUDENT_LEAVE_APPROVAL", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag

        End Using
    End Function

    Public Shared Function SAVESTUD_LEAVE_APPROVAL_EDIT(ByVal SLA_ID As String, ByVal SLA_EMP_ID_APPR As String, _
                    ByVal SLA_FROMDT As String, ByVal SLA_TODT As String, ByVal SLA_REMARKS As String, ByVal SLA_APPRLEAVE As String, ByVal SLA_APD_ID As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --03/Jul/2008
        'Purpose--To save STUDENT_LEAVE_APPROVAL data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SLA_ID", SLA_ID)
            pParms(1) = New SqlClient.SqlParameter("@SLA_EMP_ID_APPR", SLA_EMP_ID_APPR)
            pParms(2) = New SqlClient.SqlParameter("@SLA_FROMDT", SLA_FROMDT)
            pParms(3) = New SqlClient.SqlParameter("@SLA_TODT", SLA_TODT)
            pParms(4) = New SqlClient.SqlParameter("@SLA_REMARKS", SLA_REMARKS)
            pParms(5) = New SqlClient.SqlParameter("@SLA_APPRLEAVE", SLA_APPRLEAVE)
            pParms(6) = New SqlClient.SqlParameter("@SLA_APD_ID", SLA_APD_ID)
            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVESTUD_LEAVE_APPROVAL_EDIT", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function SaveSTUDLEAVE_ENTRY(ByVal SLA_ID As String, ByVal SLA_EMP_ID_AUTH As String, ByVal SLA_STU_ID As String, _
                   ByVal SLA_ACD_ID As String, ByVal SLA_GRD_ID As String, ByVal SLA_SCT_ID As String, ByVal SLA_STM_ID As String, _
     ByVal SLA_SHF_ID As String, ByVal SLA_FROMDT As String, ByVal SLA_TODT As String, ByVal SLA_REMARKS As String, ByVal SLA_APD_ID As String, ByVal SLA_APPRLEAVE As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --03/Jul/2008
        'Purpose--To save STUDENT_LEAVE_APPROVAL data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(16) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SLA_ID", SLA_ID)
            pParms(1) = New SqlClient.SqlParameter("@SLA_EMP_ID_AUTH", SLA_EMP_ID_AUTH)
            pParms(2) = New SqlClient.SqlParameter("@SLA_STU_ID", SLA_STU_ID)
            pParms(3) = New SqlClient.SqlParameter("@SLA_ACD_ID", SLA_ACD_ID)
            pParms(4) = New SqlClient.SqlParameter("@SLA_GRD_ID", SLA_GRD_ID)
            pParms(5) = New SqlClient.SqlParameter("@SLA_SCT_ID", SLA_SCT_ID)
            pParms(6) = New SqlClient.SqlParameter("@SLA_STM_ID", SLA_STM_ID)
            pParms(7) = New SqlClient.SqlParameter("@SLA_SHF_ID", SLA_SHF_ID)
            pParms(8) = New SqlClient.SqlParameter("@SLA_FROMDT", SLA_FROMDT)
            pParms(9) = New SqlClient.SqlParameter("@SLA_TODT", SLA_TODT)
            pParms(10) = New SqlClient.SqlParameter("@SLA_REMARKS", SLA_REMARKS)
            pParms(11) = New SqlClient.SqlParameter("@SLA_APD_ID", SLA_APD_ID)
            pParms(12) = New SqlClient.SqlParameter("@SLA_APPRLEAVE", SLA_APPRLEAVE)
            pParms(13) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(13).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveSTUDLEAVE_ENTRY", pParms)
            Dim ReturnFlag As Integer = pParms(13).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function SAVEATT_LOG_VGROUP(ByVal STR_XML As String, ByVal ACD_ID As String, ByVal ALG_ATTDT As String, ByVal SESS_TYPE As String, _
               ByVal ALVG_AVG_ID As String, ByVal ALVG_ID As String, ByVal EMP_ID As String, ByVal ALG_BSU_ID As String, ByVal ALG_CREATEDDT As String, _
               ByVal ALG_CREATEDTM As String, ByVal ALG_MODIFIEDDT As String, ByVal ALG_MODIFIEDTM As String, _
                 ByVal DATAMODE As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --30/NOV/2009
        'Purpose--To save Attendance_Log_Grade data based 


        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(16) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STR_XML", STR_XML)
            pParms(1) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@ALG_ATTDT", ALG_ATTDT)
            pParms(3) = New SqlClient.SqlParameter("@SESS_TYPE", SESS_TYPE)
            pParms(4) = New SqlClient.SqlParameter("@ALVG_AVG_ID", ALVG_AVG_ID)
            pParms(5) = New SqlClient.SqlParameter("@ALVG_ID", ALVG_ID)
            pParms(6) = New SqlClient.SqlParameter("@EMP_ID", EMP_ID)
            pParms(7) = New SqlClient.SqlParameter("@ALG_BSU_ID", ALG_BSU_ID)
            pParms(8) = New SqlClient.SqlParameter("@ALG_CREATEDDT", ALG_CREATEDDT)
            pParms(9) = New SqlClient.SqlParameter("@ALG_CREATEDTM", ALG_CREATEDTM)
            pParms(10) = New SqlClient.SqlParameter("@ALG_MODIFIEDDT", ALG_MODIFIEDDT)
            pParms(11) = New SqlClient.SqlParameter("@ALG_MODIFIEDTM", ALG_MODIFIEDTM)
            pParms(12) = New SqlClient.SqlParameter("@DATAMODE", DATAMODE)
            pParms(13) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(13).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVEATT_LOG_VGROUP]", pParms)
            Dim ReturnFlag As Integer = pParms(13).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function SAVEATT_LOG_ROOM(ByVal STR_XML As String, ByVal ACD_ID As String, ByVal RAL_ATTDT As String, _
                   ByVal RAL_SGR_ID As String, ByVal RAL_ID As String, ByVal EMP_ID As String, ByVal RAL_BSU_ID As String, ByVal RAL_CREATEDDT As String, _
                   ByVal RAL_CREATEDTM As String, ByVal RAL_MODIFIEDDT As String, ByVal RAL_MODIFIEDTM As String, ByVal RAL_PERIOD_ID As String, _
                     ByVal DATAMODE As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --30/NOV/2009
        'Purpose--To save Attendance_Log_Grade data based 


        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(16) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STR_XML", STR_XML)
            pParms(1) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@RAL_ATTDT", RAL_ATTDT)
            pParms(3) = New SqlClient.SqlParameter("@RAL_SGR_ID", RAL_SGR_ID)
            pParms(4) = New SqlClient.SqlParameter("@RAL_ID", RAL_ID)
            pParms(5) = New SqlClient.SqlParameter("@EMP_ID", EMP_ID)
            pParms(6) = New SqlClient.SqlParameter("@RAL_BSU_ID", RAL_BSU_ID)
            pParms(7) = New SqlClient.SqlParameter("@RAL_CREATEDDT", RAL_CREATEDDT)
            pParms(8) = New SqlClient.SqlParameter("@RAL_CREATEDTM", RAL_CREATEDTM)
            pParms(9) = New SqlClient.SqlParameter("@RAL_MODIFIEDDT", RAL_MODIFIEDDT)
            pParms(10) = New SqlClient.SqlParameter("@RAL_MODIFIEDTM", RAL_MODIFIEDTM)
            pParms(11) = New SqlClient.SqlParameter("@RAL_PERIOD_ID", RAL_PERIOD_ID)
            pParms(12) = New SqlClient.SqlParameter("@DATAMODE", DATAMODE)
            pParms(13) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(13).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVEATT_LOG_ROOM]", pParms)
            Dim ReturnFlag As Integer = pParms(13).Value
            Return ReturnFlag
        End Using
    End Function


    Public Shared Function SaveAttendance_Log_Grade(ByVal ALG_ID As String, ByVal ALG_CREATEDDT As String, ByVal ALG_CREATEDTM As String, _
               ByVal ALG_MODIFIEDDT As String, ByVal ALG_MODIFIEDTM As String, ByVal ALG_ATTDT As String, ByVal ALG_EMP_ID As String, _
 ByVal ALG_BSU_ID As String, ByVal ALG_ACD_ID As String, ByVal ALG_GRD_ID As String, ByVal ALG_SCT_ID As String, _
 ByVal ALG_SHF_ID As String, ByVal ALG_STM_ID As String, ByVal ALG_ATT_TYPE As String, ByRef New_ALG_ID As String, ByVal bEdit As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--To save Attendance_Log_Grade data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(16) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ALG_ID", ALG_ID)
            pParms(1) = New SqlClient.SqlParameter("@ALG_CREATEDDT", ALG_CREATEDDT)
            pParms(2) = New SqlClient.SqlParameter("@ALG_CREATEDTM", ALG_CREATEDTM)
            pParms(3) = New SqlClient.SqlParameter("@ALG_MODIFIEDDT", ALG_MODIFIEDDT)
            pParms(4) = New SqlClient.SqlParameter("@ALG_MODIFIEDTM", ALG_MODIFIEDTM)
            pParms(5) = New SqlClient.SqlParameter("@ALG_ATTDT", ALG_ATTDT)
            pParms(6) = New SqlClient.SqlParameter("@ALG_EMP_ID", ALG_EMP_ID)
            pParms(7) = New SqlClient.SqlParameter("@ALG_BSU_ID", ALG_BSU_ID)
            pParms(8) = New SqlClient.SqlParameter("@ALG_ACD_ID", ALG_ACD_ID)
            pParms(9) = New SqlClient.SqlParameter("@ALG_GRD_ID", ALG_GRD_ID)
            pParms(10) = New SqlClient.SqlParameter("@ALG_SCT_ID", ALG_SCT_ID)
            pParms(11) = New SqlClient.SqlParameter("@ALG_SHF_ID", ALG_SHF_ID)
            pParms(12) = New SqlClient.SqlParameter("@ALG_STM_ID", ALG_STM_ID)
            pParms(13) = New SqlClient.SqlParameter("@ALG_ATT_TYPE", ALG_ATT_TYPE)
            pParms(14) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(15) = New SqlClient.SqlParameter("@New_ALG_ID", SqlDbType.Int)
            pParms(15).Direction = ParameterDirection.Output
            pParms(16) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(16).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveAttendance_Log_Grade", pParms)
            New_ALG_ID = pParms(15).Value.ToString()
            Dim ReturnFlag As Integer = pParms(16).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function SaveATTENDANCE_REPORT_PARAM(ByVal ARP_ACD_ID As String, ByVal ARP_BSU_ID As String, ByVal ARP_APD_ID As String, _
         ByVal ARP_DESCR As String, ByVal ARP_DISP As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --10/NOV/2008
        'Purpose--To Save ATTENDANCE_REPORT_PARAM data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ARP_ACD_ID", ARP_ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@ARP_BSU_ID", ARP_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@ARP_APD_ID", ARP_APD_ID)
            pParms(3) = New SqlClient.SqlParameter("@ARP_DESCR", ARP_DESCR)
            pParms(4) = New SqlClient.SqlParameter("@ARP_DISP", ARP_DISP)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ATT.SaveATTENDANCE_REPORT_PARAM", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function SaveAttendance_Log_Student(ByVal ALS_ALG_ID As String, ByVal ALS_STU_ID As String, ByVal ALS_APD_ID As String, _
     ByVal ALS_REMARKS As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --01/Jul/2008
        'Purpose--To Save Attendance_Log_Student data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ALS_ALG_ID", ALS_ALG_ID)
            pParms(1) = New SqlClient.SqlParameter("@ALS_STU_ID", ALS_STU_ID)
            pParms(2) = New SqlClient.SqlParameter("@ALS_APD_ID", ALS_APD_ID)
            pParms(3) = New SqlClient.SqlParameter("@ALS_REMARKS", ALS_REMARKS)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveAttendance_Log_Student", pParms)
            Dim ReturnFlag As Integer = pParms(4).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function SaveAttendance_M(ByVal ATT_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal BSU_ID As String, _
           ByVal FromDT As String, ByVal ToDT As String, ByVal ATT_TYPE As String, ByVal bEdit As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --29/Jun/2008
        'Purpose--To save Attendance_M data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ATT_ID", ATT_ID)
            pParms(1) = New SqlClient.SqlParameter("@ATT_ACD_ID", ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@ATT_GRD_ID", GRD_ID)
            pParms(3) = New SqlClient.SqlParameter("@ATT_BSU_ID", BSU_ID)
            pParms(4) = New SqlClient.SqlParameter("@ATT_FROMDT", FromDT)
            If ToDT = "" Then
                pParms(5) = New SqlClient.SqlParameter("@ATT_TODT", DBNull.Value)
            Else
                pParms(5) = New SqlClient.SqlParameter("@ATT_TODT", ToDT)
            End If

            pParms(6) = New SqlClient.SqlParameter("@ATT_TYPE", ATT_TYPE)
            pParms(7) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveAttendance_M", pParms)
            Dim ReturnFlag As Integer = pParms(8).Value
            Return ReturnFlag

        End Using
    End Function

    Public Shared Function SaveSTAFF_AUTHORIZED_LEAVE(ByVal SAL_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, _
       ByVal BSU_ID As String, ByVal Emp_ID As String, ByVal FromDT As String, ByVal ToDT As String, ByVal SAL_NDAYS As String, ByVal bEdit As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --17/Jun/2008
        'Purpose--To save STAFF_AUTHORIZED_ATT data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SAL_ID", SAL_ID)
            pParms(1) = New SqlClient.SqlParameter("@SAL_ACD_ID", ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@SAL_GRD_ID", GRD_ID)
            pParms(3) = New SqlClient.SqlParameter("@SAL_BSU_ID", BSU_ID)
            pParms(4) = New SqlClient.SqlParameter("@SAL_EMP_ID", Emp_ID)
            If FromDT = "" Then
                pParms(5) = New SqlClient.SqlParameter("@SAL_FROMDT", DBNull.Value)
            Else
                pParms(5) = New SqlClient.SqlParameter("@SAL_FROMDT", FromDT)
            End If
            If ToDT = "" Then
                pParms(6) = New SqlClient.SqlParameter("@SAL_TODT", DBNull.Value)
            Else
                pParms(6) = New SqlClient.SqlParameter("@SAL_TODT", ToDT)
            End If

            pParms(7) = New SqlClient.SqlParameter("@SAL_NDAYS", SAL_NDAYS)
            pParms(8) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveSTAFF_AUTHORIZED_LEAVE", pParms)
            Dim ReturnFlag As Integer = pParms(9).Value
            Return ReturnFlag

        End Using
    End Function

    Public Shared Function SaveSTAFF_AUTHORIZED_ATTENDANCE(ByVal SAD_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal SCT_ID As String, ByVal STM_ID As String, ByVal SHF_ID As String, _
    ByVal BSU_ID As String, ByVal Emp_ID As String, ByVal FromDT As String, ByVal ToDT As String, ByVal bEdit As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --17/Jun/2008
        'Purpose--To save STAFF_AUTHORIZED_ATT data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(11) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SAD_ID", SAD_ID)
            pParms(1) = New SqlClient.SqlParameter("@SAD_ACD_ID", ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@SAD_GRD_ID", GRD_ID)
            pParms(3) = New SqlClient.SqlParameter("@SAD_SCT_ID", SCT_ID)
            pParms(4) = New SqlClient.SqlParameter("@SAD_STM_ID", STM_ID)
            pParms(5) = New SqlClient.SqlParameter("@SAD_SHF_ID", SHF_ID)
            pParms(6) = New SqlClient.SqlParameter("@SAD_BSU_ID", BSU_ID)
            pParms(7) = New SqlClient.SqlParameter("@SAD_EMP_ID", Emp_ID)

            If FromDT = "" Then
                pParms(8) = New SqlClient.SqlParameter("@SAD_FROMDT", DBNull.Value)
            Else
                pParms(8) = New SqlClient.SqlParameter("@SAD_FROMDT", FromDT)
            End If
            If ToDT = "" Then
                pParms(9) = New SqlClient.SqlParameter("@SAD_TODT", DBNull.Value)
            Else
                pParms(9) = New SqlClient.SqlParameter("@SAD_TODT", ToDT)
            End If

            pParms(10) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(11).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveSTAFF_AUTHORIZED_ATTENDANCE", pParms)
            Dim ReturnFlag As Integer = pParms(11).Value
            Return ReturnFlag

        End Using
    End Function
    Public Shared Function SaveSTAFF_AUTHORIZED_M(ByVal BSU_ID As String, ByVal ATC_DES_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ATC_BSU_ID", BSU_ID)
            pParms(1) = New SqlClient.SqlParameter("@ATC_DES_ID", ATC_DES_ID)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SavestudAUTHORIZED_CATEGORY", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function SaveSTUDSCHOOL_HOLIDAY_M(ByVal SCH_ID As String, ByVal SCH_DATE As Date, ByVal BSU_ID As String, _
                       ByVal SCH_ACD_ID As String, ByVal SCH_DTFROM As Date, ByVal SCH_DTTO As Date, ByVal SCH_REMARKS As String, _
                        ByVal SCH_TYPE As String, ByVal SCH_DAY_SL As String, ByVal SCH_bDELETED As Boolean, ByVal SCH_bGRADEALL As Boolean, _
                    ByVal SCH_WEEKEND1_WORK As String, ByVal SCH_bWEEKEND1_LOG_BOOK As Boolean, ByVal SCH_WEEKEND2_WORK As String, _
                    ByVal SCH_bWEEKEND2_LOG_BOOK As Boolean, ByRef New_SCH_ID As String, ByVal bEdit As String, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --15/Jun/2008
        'Purpose--To save SCHOOL_HOLIDAY_M data based on the datamode 


        'code modified on 15/jul/2008
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(17) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SCH_ID", SCH_ID)
            pParms(1) = New SqlClient.SqlParameter("@SCH_DATE", SCH_DATE)
            pParms(2) = New SqlClient.SqlParameter("@SCH_BSU_ID", BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@SCH_ACD_ID", SCH_ACD_ID)
            pParms(4) = New SqlClient.SqlParameter("@SCH_DTFROM", SCH_DTFROM)
            pParms(5) = New SqlClient.SqlParameter("@SCH_DTTO", SCH_DTTO)
            pParms(6) = New SqlClient.SqlParameter("@SCH_REMARKS", SCH_REMARKS)
            pParms(7) = New SqlClient.SqlParameter("@SCH_TYPE", SCH_TYPE)
            pParms(8) = New SqlClient.SqlParameter("@SCH_DAY_SL", SCH_DAY_SL)
            pParms(9) = New SqlClient.SqlParameter("@SCH_bDeleted", SCH_bDELETED)
            pParms(10) = New SqlClient.SqlParameter("@SCH_bGRADEALL", SCH_bGRADEALL)
            pParms(11) = New SqlClient.SqlParameter("@SCH_WEEKEND1_WORK", SCH_WEEKEND1_WORK)
            pParms(12) = New SqlClient.SqlParameter("@SCH_bWEEKEND1_LOG_BOOK", SCH_bWEEKEND1_LOG_BOOK)
            pParms(13) = New SqlClient.SqlParameter("@SCH_WEEKEND2_WORK", SCH_WEEKEND2_WORK)
            pParms(14) = New SqlClient.SqlParameter("@SCH_bWEEKEND2_LOG_BOOK", SCH_bWEEKEND2_LOG_BOOK)
            pParms(15) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(16) = New SqlClient.SqlParameter("@New_SCH_ID", SqlDbType.Int)
            pParms(16).Direction = ParameterDirection.Output
            pParms(17) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(17).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDSCHOOL_HOLIDAY_M", pParms)

            If Not pParms(16).Value Is DBNull.Value Then
                New_SCH_ID = pParms(16).Value
            End If

            Dim ReturnFlag As Integer = pParms(17).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function SaveSCHOOL_HOLIDAY_GRADE(ByVal SHG_SCH_ID As String, ByVal SHG_ACD_ID As String, ByVal SHG_GRD_ID As String, _
                 ByVal SHG_SCT_ID As String, ByVal SAL_FROMDT As String, ByVal SAL_TODT As String, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --15/JUL/2008
        'Purpose--To save SCHOOL_HOLIDAY grade & section wise 
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SHG_SCH_ID", SHG_SCH_ID)
            pParms(1) = New SqlClient.SqlParameter("@SHG_ACD_ID", SHG_ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@SHG_GRD_ID", SHG_GRD_ID)
            pParms(3) = New SqlClient.SqlParameter("@SHG_SCT_ID", SHG_SCT_ID)
            pParms(4) = New SqlClient.SqlParameter("@SAL_FROMDT", SAL_FROMDT)
            pParms(5) = New SqlClient.SqlParameter("@SAL_TODT", SAL_TODT)
            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSCHOOL_HOLIDAY_GRADE", pParms)
            Dim ReturnFlag As Integer = pParms(6).Value
            Return ReturnFlag
        End Using

    End Function

#End Region
#Region "DELETE REGION"
    Public Shared Function DeleteAttendance_M(ByVal ATT_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ATT_ID", ATT_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteAttendance_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function DeleteSCHOOL_HOILDAY_GRADE(ByVal SHG_SCH_ID As String, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --16/Jul/2008
        'Purpose--Delete SCHOOL_HOILDAY_GRADE
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SHG_SCH_ID", SHG_SCH_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSCHOOL_HOILDAY_GRADE", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function




    Public Shared Function DeleteSCHOOL_HOLIDAY_M(ByVal SCH_ID As String, ByVal trans As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --16/Jul/2008
        'Purpose--Delete SCHOOL_HOLIDAY_M by updating SCH_bDELETED=true
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SCH_ID", SCH_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSCHOOL_HOLIDAY_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function



    Public Shared Function DeleteSTAFF_AUTHORIZED_LEAVE(ByVal SAL_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SAL_ID", SAL_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSTAFF_AUTHORIZED_LEAVE", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function DeleteSTAFF_AUTHORIZED_ATTENDANCE(ByVal SAD_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SAD_ID", SAD_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteSTAFF_AUTHORIZED_ATTENDANCE", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function DeleteATTENDANCE_REPORT_PARAM(ByVal ARP_ACD_ID As String, ByVal ARP_BSU_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ARP_ACD_ID", ARP_ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@ARP_BSU_ID", ARP_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ATT.DeleteATTENDANCE_REPORT_PARAM", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function DeleteStaffAuth_BSU(ByVal BSU_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ATC_BSU_ID", BSU_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteAUTHORIZED_CATEGORY", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            SqlConnection.ClearPool(connection)
            Return ReturnFlag
        End Using

    End Function
#End Region
#End Region


#Region "ESPORTS"
    Public Shared Sub PopulateBSU(ByVal ddlBSU As DropDownList)
        Dim dsBSU As DataSet
        Dim qry As String
        qry = "SELECT BSU_ID,BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE ISNULL(BSU_Bschool,0)=1 ORDER BY BSU_NAME"

        dsBSU = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        ddlBSU.DataTextField = "BSU_NAME"
        ddlBSU.DataValueField = "BSU_ID"
        ddlBSU.DataSource = dsBSU
        ddlBSU.DataBind()
    End Sub
    Public Shared Sub PopulateVenue(ByVal ddlBSU As DropDownList, ByVal BSU_ID As String)
        Dim dsVENUE As DataSet
        Dim qry As String
        qry = "SELECT DISTINCT EAV_ID,EAV_DESCR FROM TRANSPORT.EXTRA_ACTIVITY_VENUE_M INNER JOIN " & _
              "TRANSPORT.EXTRA_ACTIVITY_SERVICES_VENUE_D ON EAV_ID=EAD_EAV_ID WHERE EAD_STU_BSU_ID = '" & BSU_ID & "'"

        dsVENUE = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, qry)
        ddlBSU.DataTextField = "EAV_DESCR"
        ddlBSU.DataValueField = "EAV_ID"
        ddlBSU.DataSource = dsVENUE
        ddlBSU.DataBind()
    End Sub
    Public Shared Function GET_STUDENT_DETIALS(ByVal STU_ID As Long) As DataSet
        Try
            Dim QRY As String = "SELECT STU_NO,ISNULL(STU_SIBLING_ID,STU_ID) STU_SIBLING_ID,ISNULL(STU_FIRSTNAME,'')STU_FIRSTNAME,ISNULL(STU_MIDNAME,'')STU_MIDNAME,ISNULL(STU_LASTNAME,'')STU_LASTNAME,STU_DOJ,STU_DOB,ISNULL(STU_NATIONALITY,'0')STU_NATIONALITY,ISNULL(STU_RLG_ID,'0')STU_RLG_ID,STU_GENDER FROM dbo.STUDENT_M WITH(NOLOCK) WHERE STU_ID=" & STU_ID & ""
            GET_STUDENT_DETIALS = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, QRY)
        Catch ex As Exception
            GET_STUDENT_DETIALS = Nothing
        End Try
    End Function
    Public Shared Function GET_PARENT_DETIALS(ByVal STU_ID As Long) As DataSet
        Try
            Dim QRY As String = "SELECT ISNULL(STS_FFIRSTNAME,'') STS_FFIRSTNAME,ISNULL(STS_FMIDNAME,'') STS_FMIDNAME ,ISNULL(STS_FLASTNAME,'') STS_FLASTNAME, " & _
                                "ISNULL(STS_FCOMPOBOX,'') STS_FCOMPOBOX ,ISNULL(STS_FEMIR,'') STS_FEMIR ,ISNULL(STS_FMOBILE,'') STS_FMOBILE, " & _
                                "ISNULL(STS_FEmail,'') STS_FEmail FROM dbo.STUDENT_D WITH(NOLOCK) WHERE STS_STU_ID=" & STU_ID & ""
            GET_PARENT_DETIALS = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, QRY)
        Catch ex As Exception
            GET_PARENT_DETIALS = Nothing
        End Try
    End Function
#End Region

    Public Shared Function Transport_UpdateStudent(ByVal STU_ID As Integer, ByVal STU_BSU_ID As String, _
             ByVal STU_ACD_ID As Integer, _
             ByVal STU_GRD_ID As String, ByVal STU_SHF_ID As Integer, ByVal STU_SCT_ID As Integer, _
             ByVal STU_FIRSTNAME As String, ByVal STU_MIDNAME As String, ByVal STU_LASTNAME As String, _
             ByVal STU_DOJ As String, ByVal STU_FEE_ID As String, ByVal STU_DOB As String, _
             ByVal STU_GENDER As String, ByVal STU_RLG_ID As String, ByVal STU_NATIONALITY As String, _
            ByVal F_FIRSTNAME As String, ByVal F_MIDNAME As String, ByVal F_LASTNAME As String, _
            ByVal F_POBox As String, ByVal F_EMirate As String, ByVal F_Mobile As String, ByVal F_Email As String, _
            ByVal STU_GEMS_BSU_ID As String, ByVal STU_GEMS_STU_ID As Long, ByVal STU_SVC_EAV_ID As Integer, _
            ByVal STU_SVC_FROMDT As String, ByVal STU_SVC_TODT As String, _
             ByVal TranType As String, ByVal STU_NONGEMS_BSU_ID As String, ByVal trans As SqlTransaction) As Integer
        'Author(--Jacob)
        'Date   --10/May/2016
        'Purpose--To update STUDENT_M data , particulary for Al Nabooda transport
        Using connection As SqlConnection = ConnectionManger.GetOASISTransportConnection()
            Try
                Dim pParms(62) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@STU_ACD_ID", STU_ACD_ID)
                pParms(3) = New SqlClient.SqlParameter("@STU_GRD_ID", STU_GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@STU_SHF_ID", STU_SHF_ID)
                pParms(5) = New SqlClient.SqlParameter("@STU_SCT_ID", STU_SCT_ID)
                pParms(6) = New SqlClient.SqlParameter("@STU_DOJ", STU_DOJ)
                pParms(7) = New SqlClient.SqlParameter("@STU_FEE_ID", STU_FEE_ID)
                pParms(8) = New SqlClient.SqlParameter("@STU_FIRSTNAME", STU_FIRSTNAME)
                pParms(9) = New SqlClient.SqlParameter("@STU_MIDNAME", STU_MIDNAME)
                pParms(10) = New SqlClient.SqlParameter("@STU_LASTNAME", STU_LASTNAME)
                pParms(11) = New SqlClient.SqlParameter("@STU_GENDER", STU_GENDER)
                pParms(12) = New SqlClient.SqlParameter("@STU_RLG_ID", STU_RLG_ID)
                pParms(13) = New SqlClient.SqlParameter("@STU_DOB", STU_DOB)
                pParms(14) = New SqlClient.SqlParameter("@STU_Nationality", STU_NATIONALITY)
                pParms(15) = New SqlClient.SqlParameter("@F_FIRSTNAME", F_FIRSTNAME)
                pParms(16) = New SqlClient.SqlParameter("@F_MIDNAME", F_MIDNAME)
                pParms(17) = New SqlClient.SqlParameter("@F_LASTNAME", F_LASTNAME)
                pParms(18) = New SqlClient.SqlParameter("@F_POBOX", F_POBox)
                pParms(19) = New SqlClient.SqlParameter("@F_EMIRATE", F_EMirate)
                pParms(20) = New SqlClient.SqlParameter("@F_MOBILE", F_Mobile)
                pParms(21) = New SqlClient.SqlParameter("@F_EMAIL", F_Email)

                pParms(22) = New SqlClient.SqlParameter("@STU_GEMS_BSU_ID", STU_GEMS_BSU_ID)
                pParms(23) = New SqlClient.SqlParameter("@STU_GEMS_STU_ID", STU_GEMS_STU_ID)
                pParms(24) = New SqlClient.SqlParameter("@STU_SVC_EAV_ID", STU_SVC_EAV_ID)
                pParms(25) = New SqlClient.SqlParameter("@STU_SVC_FROMDT", STU_SVC_FROMDT)
                pParms(26) = New SqlClient.SqlParameter("@STU_SVC_TODT", STU_SVC_TODT)
                pParms(27) = New SqlClient.SqlParameter("@TranType", TranType)
                pParms(28) = New SqlClient.SqlParameter("@STU_NONGEMS_BSU_ID", STU_NONGEMS_BSU_ID)
                pParms(29) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(29).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "dbo.TPT_UPDATE_STUDENT_M", pParms)
                Dim ReturnFlag As Integer = pParms(29).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using

    End Function

    Public Shared Function GetDuplicate_Enquiry_ByMultipleFields(ByVal EQP_FFIRSTNAME As String, ByVal EQM_APPLMIDNAME As String, ByVal EQM_APPLLASTNAME As String,
                                                ByVal EQM_APPLDOB As String, ByVal EQP_FEMAIL As String, ByVal EQS_BSU_ID As String,
                                                ByVal EQS_GRD_ID As String, ByVal ACD_CLM_ID As String, ByVal ACD_ACY_ID As String) As Integer
        ' Author(--Ahmed)
        'Date   --3/july/2019
        'Purpose--Checking 4 duplicate entry in the Enquiry
        Dim ReturnCount As Int32 = -1
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(14) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQP_FFIRSTNAME", EQP_FFIRSTNAME)
        pParms(1) = New SqlClient.SqlParameter("@EQM_APPLMIDNAME", EQM_APPLMIDNAME)
        pParms(2) = New SqlClient.SqlParameter("@EQM_APPLLASTNAME", EQM_APPLLASTNAME)
        pParms(3) = New SqlClient.SqlParameter("@EQM_APPLDOB", EQM_APPLDOB)
        pParms(4) = New SqlClient.SqlParameter("@EQP_FEMAIL", EQP_FEMAIL)
        pParms(5) = New SqlClient.SqlParameter("@EQS_BSU_ID", EQS_BSU_ID)
        pParms(6) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
        pParms(8) = New SqlClient.SqlParameter("@ACD_CLM_ID", ACD_CLM_ID)
        pParms(9) = New SqlClient.SqlParameter("@ACD_ACY_ID", ACD_ACY_ID)

        pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "GetDuplicate_Enquiry_M", pParms)

        ReturnCount = pParms(10).Value
        Return ReturnCount
    End Function
    Public Shared Function GetGrade_Section_STM_ID(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal STM_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim GRM_ID_Select = "SELECT grm_id FROM grade_bsu_m,stream_m WHERE" _
                                     & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ACD_ID + " and grm_grd_id='" + GRD_ID + "' and grm_shf_id='" + SHF_ID + "' and STM_ID= '" + STM_ID + "'"
        Dim sqlGrade_Section As String = ""
        If SHF_ID = "" Then
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID &
                "' and  [SCT_GRM_ID] in (" + GRM_ID_Select + ") ) order by SCT_DESCR"
        Else
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID &
               "' and  [SCT_GRM_ID] in (" + GRM_ID_Select + ") ) order by SCT_DESCR"
        End If

        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    
    Public Shared Function GetGrade_Section_STM(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal STM_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Section As String = ""
        If SHF_ID = "" Then
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "' AND GRM_STM_ID = '" + STM_ID + "') order by SCT_DESCR"
        Else
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "' AND GRM_STM_ID = '" + STM_ID + "'  AND [GRM_SHF_ID]='" & SHF_ID & "' ) order by SCT_DESCR"
        End If

        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function GetGrade_Section_TEMP_STM(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal STM_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Section As String = ""
        If SHF_ID = "" Then
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND GRM_STM_ID = '" + STM_ID + "' ) and sct_descr<>'TEMP' order by SCT_DESCR"
        Else
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND GRM_STM_ID = '" + STM_ID + "'  AND [GRM_SHF_ID]='" & SHF_ID & "' )  and sct_descr<>'TEMP' order by SCT_DESCR"
        End If

        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    Public Shared Function GetGRM_GRD_ID_STM_ID(ByVal ACD_ID As String, ByVal CLM As String, ByVal STM_ID As String, ByVal SHF_ID As String) As SqlDataReader
        ''''Check the related Function references


        'modified Arun.g
        'Purpose--Get DRQ_GRM_GRD_ID data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGRM_GRD_ID As String = ""

        sqlGetGRM_GRD_ID = "SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER as GRD_DISPLAYORDER " &
                        " FROM GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID left OUTER JOIN " &
                      " ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND " &
                     " GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "'And GRADE_BSU_M.GRM_STM_ID='" & STM_ID & "'  And GRM_SHF_ID='" & SHF_ID & "'order by GRADE_M.GRD_DISPLAYORDER"



        '"SELECT GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY " & _
        '" FROM ACADEMICYEAR_D INNER JOIN  GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
        '                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER"


        Dim command As SqlCommand = New SqlCommand(sqlGetGRM_GRD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

   

End Class
