﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class GetMessageStatusV2
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function


    <WebMethod()> _
    Public Function GetEmailNormalStatus(ByVal Sending_id As String) As XmlDocument

        Dim ds As New DataSet()
        Dim Xdoc As New XmlDocument
        Try

            Dim strConn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim condition = "SELECT TOTAL_DATA,SUCCESS,FAILED,ISNULL(START_DATE_TIME,'--')START_DATE_TIME,ISNULL(END_DATE_TIME,'--')END_DATE_TIME, CASE  WHEN TOTAL_DATA>0 THEN  CEILING(convert(decimal,(SUCCESS+FAILED))/TOTAL_DATA *100) ELSE 0 END  AS PERCENTAGE FROM dbo.MESSAGE_SEND_REPORT A WHERE SEND_ID='" & Sending_id & "'"

            ds = SqlHelper.ExecuteDataset(strConn, CommandType.Text, condition)
            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)

        Catch ex As Exception

        End Try

        Return Xdoc

    End Function

End Class