﻿Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Security.Cryptography
Imports System.IO

Public Class SHA256EncrDecr
    Private ReadOnly SALT As Byte() = New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
   &H65, &H64, &H76, &H65, &H64, &H65, _
   &H76}
    Private Const SHA256_key As String = "12341544566467678798791237868947"
    Private key64() As Byte = {}
    Private SEncryptionKey As String = "!#$a54?W"
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
    Public Function Encrypt_SHA256(textToEncrypt As String) As String
        ' Validations
        'if (textToEncrypt == null || textToEncrypt.Length <= 0) throw new ArgumentNullException(nameof(textToEncrypt));
        'if (key == null || key.Length <= 0) throw new ArgumentNullException(nameof(key));

        Try
            Dim dataBytes As Byte() = Encoding.UTF8.GetBytes(textToEncrypt)
            Dim key__1 = Encoding.ASCII.GetBytes(SHA256_key)
            Dim pdb As New Rfc2898DeriveBytes(key__1, SALT, 10)
            Dim initVector As Byte() = pdb.GetBytes(16)

            Dim encryptedData As Byte() = Nothing
            Using encryptor As Aes = Aes.Create()
                encryptor.Key = key__1
                encryptor.IV = initVector
                Using ms As New MemoryStream()
                    Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                        cs.Write(dataBytes, 0, dataBytes.Length)
                        cs.Close()
                    End Using

                    encryptedData = ms.ToArray()
                End Using
            End Using

            If encryptedData IsNot Nothing AndAlso encryptedData.Length > 0 Then
                ' The result will have first 16 bytes as the initVector, the remaining will be data
                Dim resultBytes As Byte() = New Byte(initVector.Length + (encryptedData.Length - 1)) {}
                Array.Copy(initVector, 0, resultBytes, 0, initVector.Length)
                Array.Copy(encryptedData, 0, resultBytes, initVector.Length, encryptedData.Length)
                Dim result As String = Convert.ToBase64String(resultBytes)
                Dim urlEncoded As String = WebUtility.UrlEncode(result)
                Dim finalresult As String = urlEncoded.Replace("%", "_")
                Return finalresult
            End If
        Catch
            Throw
        End Try

        Return String.Empty
    End Function


    Public Function Decrypt_SHA256(cipherText As String) As String
        ' Validations
        'if (cipherText == null || cipherText.Length <= 0) throw new ArgumentNullException(nameof(cipherText));
        'if (key == null || key.Length <= 0) throw new ArgumentNullException(nameof(key));

        Try
            Dim key__1 = Encoding.ASCII.GetBytes(SHA256_key)

            Dim cleanedText As String = cipherText.Replace("_", "%")
            Dim urlDecoded As String = WebUtility.UrlDecode(cleanedText)

            Dim cipherBytes As Byte() = Convert.FromBase64String(urlDecoded)

            ' in the CipherBytes, first 16 elements contains the IV and the rest contains the data.
            Dim initVector = New Byte(15) {}
            Dim encryptedData As Byte() = New Byte(cipherBytes.Length - 17) {}

            Array.Copy(cipherBytes, 0, initVector, 0, initVector.Length)
            Array.Copy(cipherBytes, 16, encryptedData, 0, encryptedData.Length)

            Using encryptor As Aes = Aes.Create()
                encryptor.Key = key__1
                encryptor.IV = initVector

                Using ms As New MemoryStream()
                    Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                        cs.Write(encryptedData, 0, encryptedData.Length)
                        cs.Close()
                    End Using
                    Dim result As String = Encoding.UTF8.GetString(ms.ToArray())
                    Return result
                End Using
            End Using
        Catch
            Throw
        End Try
    End Function
End Class
