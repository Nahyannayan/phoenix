﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports UtilityObj
Public Class BasePage
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If HttpContext.Current.Session("sUsr_name") & "" = "" Then
                HttpContext.Current.Response.Redirect("~/login.aspx", False)
                Exit Sub
            End If
            CheckForSQLInjection(Me.Page)
        Catch ex As Exception
        End Try
    End Sub
    Private Function CheckForSQLInjection(ByVal myParentObj As Object) As Boolean
        Try
            If Request.Url.PathAndQuery.ToString.Contains("'") Then
                Response.Redirect(Request.Url.PathAndQuery.ToString.Replace("'", ""), True)
            End If

            Dim mObj As Object
            For Each mObj In myParentObj.Controls
                Try
                    If mObj.HasControls Then
                        CheckForSQLInjection(mObj)
                    End If
                Catch ex As Exception

                End Try
                If TypeOf (mObj) Is TextBox Then
                    If TypeOf (mObj.Parent) Is GridView Or TypeOf (mObj.Parent) Is DataControlFieldHeaderCell Then
                        mObj.Text = Mainclass.cleanString(mObj.Text)
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
    End Function
   

End Class
