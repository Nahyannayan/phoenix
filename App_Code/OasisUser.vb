Imports Microsoft.VisualBasic

Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Namespace Oasis_User

    Public Class OasisUser

        Public Shared Function SaveApplicantForm(ByVal title As String, ByVal firstname As String, ByVal middlename As String, ByVal surname As String, ByVal address As String, ByVal town As String, ByVal postalcode As String, ByVal countryofresidence As Integer, ByVal countryissuepassport As Integer, ByVal homephone As String, ByVal mobile As String, ByVal officephone As String, ByVal email As String, ByVal dob As String, ByVal availabledate As String, ByVal qualifiedteacher As Boolean, ByVal qualifiedyear As String, ByVal experience As String, ByVal relocate As Boolean, ByVal additionalinfo As String, ByVal maritalstatus As String, ByVal children_no As Integer, ByVal NewLetters As Boolean, ByVal MobileAllerts As Boolean, ByVal Country As String, ByVal Gender As String, ByVal PassportNumber As String, ByVal PassportIssuePlace As String, ByVal PassportIssueDate As String, ByVal PassportExpiryDate As String, ByVal VisaNo As String, ByVal VisaIssuePlace As String, ByVal VisaIssueCountry As Integer, ByVal VisaIssueDate As String, ByVal VisaExpiryDate As String, ByVal PassportName As String) As String
            Dim returnstring = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(36) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TITLE", title)
            pParms(1) = New SqlClient.SqlParameter("@FIRST_NAME", firstname)
            pParms(2) = New SqlClient.SqlParameter("@MIDDLE_NAME", middlename)
            pParms(3) = New SqlClient.SqlParameter("@SUR_NAME", surname)
            pParms(4) = New SqlClient.SqlParameter("@ADDRESS", address)
            pParms(5) = New SqlClient.SqlParameter("@TOWN", town)
            pParms(6) = New SqlClient.SqlParameter("@POSTAL_CODE", postalcode)
            pParms(7) = New SqlClient.SqlParameter("@COUNTRY_OF_RESIDENCE", Convert.ToInt32(countryofresidence))
            If Convert.ToString(countryissuepassport).Trim() <> "" Then
                pParms(8) = New SqlClient.SqlParameter("@COUNTRY_ISSUE_PASSPORT", Convert.ToInt32(countryissuepassport))
            End If
            pParms(9) = New SqlClient.SqlParameter("@HOME_PHONE", homephone)
            pParms(10) = New SqlClient.SqlParameter("@MOBILE", mobile)
            pParms(11) = New SqlClient.SqlParameter("@OFFICE_PHONE", officephone)
            pParms(12) = New SqlClient.SqlParameter("@EMAIL", email)
            pParms(13) = New SqlClient.SqlParameter("@DATE_OF_BIRTH", dob)
            pParms(14) = New SqlClient.SqlParameter("@AVAILABLE_DATE", availabledate)
            pParms(15) = New SqlClient.SqlParameter("@QUALIFIED_TEACHER", qualifiedteacher)
            pParms(16) = New SqlClient.SqlParameter("@QUALIFIED_YEAR", qualifiedyear)
            If experience <> Nothing Then
                pParms(17) = New SqlClient.SqlParameter("@EXPERIENCE", Convert.ToInt32(experience))
            End If
            
            pParms(18) = New SqlClient.SqlParameter("@RE_ALLOCATION", relocate)
            pParms(19) = New SqlClient.SqlParameter("@ADDITIONAL_INFORMATION", additionalinfo)
            pParms(20) = New SqlClient.SqlParameter("@MARITAL_STATUS", maritalstatus)
            If Convert.ToString(children_no).Trim() <> "" Then
                pParms(21) = New SqlClient.SqlParameter("@NO_OF_CHILDREN", Convert.ToInt32(children_no))
            End If
            pParms(22) = New SqlClient.SqlParameter("@NEWS_LETTERS", NewLetters)
            pParms(23) = New SqlClient.SqlParameter("@MOBILE_ALERTS", MobileAllerts)
            pParms(24) = New SqlClient.SqlParameter("@COUNTRY", Convert.ToInt32(Country))
            pParms(25) = New SqlClient.SqlParameter("@GENDER", Gender)
            pParms(26) = New SqlClient.SqlParameter("@PASSPORT_NUMBER", PassportNumber)
            pParms(27) = New SqlClient.SqlParameter("@PASSPORT_ISSUE_PLACE", PassportIssuePlace)
            pParms(28) = New SqlClient.SqlParameter("@PASSPORT_ISSUE_DATE", PassportIssueDate)
            pParms(29) = New SqlClient.SqlParameter("@PASSPORT_EXPIRY_DATE", PassportExpiryDate)
            pParms(30) = New SqlClient.SqlParameter("@VISA_NO", VisaNo)
            pParms(31) = New SqlClient.SqlParameter("@VISA_ISSUE_PLACE", VisaIssuePlace)
            If Convert.ToString(VisaIssueCountry).Trim() <> "" Then
                pParms(32) = New SqlClient.SqlParameter("@VISA_ISSUE_COUNTRY", VisaIssueCountry)
            End If
            pParms(33) = New SqlClient.SqlParameter("@VISA_ISSUE_DATE", VisaIssueDate)
            pParms(34) = New SqlClient.SqlParameter("@VISA_EXPIRY_DATE", VisaExpiryDate)
            pParms(35) = New SqlClient.SqlParameter("@PASSPORT_NAME", PassportName)

            returnstring = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_APPLICATION_MASTER", pParms)
            Return returnstring
        End Function

        Public Shared Sub UpdateApplicantForm(ByVal title As String, ByVal firstname As String, ByVal middlename As String, ByVal surname As String, ByVal address As String, ByVal town As String, ByVal postalcode As String, ByVal countryofresidence As Integer, ByVal countryissuepassport As Integer, ByVal homephone As String, ByVal mobile As String, ByVal officephone As String, ByVal email As String, ByVal dob As String, ByVal availabledate As String, ByVal qualifiedteacher As Boolean, ByVal qualifiedyear As String, ByVal experience As String, ByVal relocate As Boolean, ByVal additionalinfo As String, ByVal maritalstatus As String, ByVal children_no As Integer, ByVal NewLetters As Boolean, ByVal MobileAllerts As Boolean, ByVal Country As String, ByVal Gender As String, ByVal PassportNumber As String, ByVal PassportIssuePlace As String, ByVal PassportIssueDate As String, ByVal PassportExpiryDate As String, ByVal VisaNo As String, ByVal VisaIssuePlace As String, ByVal VisaIssueCountry As Integer, ByVal VisaIssueDate As String, ByVal VisaExpiryDate As String, ByVal PassportName As String, ByVal Application_no As String, ByVal Ratings As String)
            Dim returnstring = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(38) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TITLE", title)
            pParms(1) = New SqlClient.SqlParameter("@FIRST_NAME", firstname)
            pParms(2) = New SqlClient.SqlParameter("@MIDDLE_NAME", middlename)
            pParms(3) = New SqlClient.SqlParameter("@SUR_NAME", surname)
            pParms(4) = New SqlClient.SqlParameter("@ADDRESS", address)
            pParms(5) = New SqlClient.SqlParameter("@TOWN", town)
            pParms(6) = New SqlClient.SqlParameter("@POSTAL_CODE", postalcode)
            pParms(7) = New SqlClient.SqlParameter("@COUNTRY_OF_RESIDENCE", Convert.ToInt32(countryofresidence))
            If Convert.ToString(countryissuepassport).Trim() <> "" Then
                pParms(8) = New SqlClient.SqlParameter("@COUNTRY_ISSUE_PASSPORT", Convert.ToInt32(countryissuepassport))
            End If
            pParms(9) = New SqlClient.SqlParameter("@HOME_PHONE", homephone)
            pParms(10) = New SqlClient.SqlParameter("@MOBILE", mobile)
            pParms(11) = New SqlClient.SqlParameter("@OFFICE_PHONE", officephone)
            pParms(12) = New SqlClient.SqlParameter("@EMAIL", email)
            pParms(13) = New SqlClient.SqlParameter("@DATE_OF_BIRTH", dob)
            pParms(14) = New SqlClient.SqlParameter("@AVAILABLE_DATE", availabledate)
            pParms(15) = New SqlClient.SqlParameter("@QUALIFIED_TEACHER", qualifiedteacher)
            pParms(16) = New SqlClient.SqlParameter("@QUALIFIED_YEAR", qualifiedyear)
            If experience <> Nothing Then
                pParms(17) = New SqlClient.SqlParameter("@EXPERIENCE", Convert.ToInt32(experience))
            End If

            pParms(18) = New SqlClient.SqlParameter("@RE_ALLOCATION", relocate)
            pParms(19) = New SqlClient.SqlParameter("@ADDITIONAL_INFORMATION", additionalinfo)
            pParms(20) = New SqlClient.SqlParameter("@MARITAL_STATUS", maritalstatus)
            If Convert.ToString(children_no).Trim() <> "" Then
                pParms(21) = New SqlClient.SqlParameter("@NO_OF_CHILDREN", Convert.ToInt32(children_no))
            End If
            pParms(22) = New SqlClient.SqlParameter("@NEWS_LETTERS", NewLetters)
            pParms(23) = New SqlClient.SqlParameter("@MOBILE_ALERTS", MobileAllerts)
            pParms(24) = New SqlClient.SqlParameter("@COUNTRY", Convert.ToInt32(Country))
            pParms(25) = New SqlClient.SqlParameter("@GENDER", Gender)
            pParms(26) = New SqlClient.SqlParameter("@PASSPORT_NUMBER", PassportNumber)
            pParms(27) = New SqlClient.SqlParameter("@PASSPORT_ISSUE_PLACE", PassportIssuePlace)
            pParms(28) = New SqlClient.SqlParameter("@PASSPORT_ISSUE_DATE", PassportIssueDate)
            pParms(29) = New SqlClient.SqlParameter("@PASSPORT_EXPIRY_DATE", PassportExpiryDate)
            pParms(30) = New SqlClient.SqlParameter("@VISA_NO", VisaNo)
            pParms(31) = New SqlClient.SqlParameter("@VISA_ISSUE_PLACE", VisaIssuePlace)
            If Convert.ToString(VisaIssueCountry).Trim() <> "" Then
                pParms(32) = New SqlClient.SqlParameter("@VISA_ISSUE_COUNTRY", VisaIssueCountry)
            End If
            pParms(33) = New SqlClient.SqlParameter("@VISA_ISSUE_DATE", VisaIssueDate)
            pParms(34) = New SqlClient.SqlParameter("@VISA_EXPIRY_DATE", VisaExpiryDate)
            pParms(35) = New SqlClient.SqlParameter("@PASSPORT_NAME", PassportName)
            pParms(36) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_no)
            pParms(37) = New SqlClient.SqlParameter("@RATINGS", Ratings)

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "UPDATE_APPLICANT_MASTER", pParms)

        End Sub

        Public Shared Sub SaveCVFileName(ByVal FileName As String, ByVal FilePath As String, ByVal Application_No As String)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "UPDATE APPLICATION_MASTER SET CV_FILE_NAME='" & FileName & "', CV_FILE_PATH='" & FilePath & "' WHERE APPLICATION_NO='" & Application_No & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End Sub
        Public Shared Sub SavePhotoFileName(ByVal FileName As String, ByVal FilePath As String, ByVal Application_No As String)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "UPDATE APPLICATION_MASTER SET PHOTO_FILE_NAME='" & FileName & "', PHOTO_FILE_PATH='" & FilePath & "' WHERE APPLICATION_NO='" & Application_No & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End Sub

        Public Shared Sub SaveSource(ByVal SourceId As Integer, ByVal Application_No As Integer)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "INSERT INTO APPLICATION_SOURCE (RECORD_ID,APPLICATION_NO,SOURCE_ID) VALUES(NEWID(),'" & Application_No & "','" & SourceId & "')"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End Sub

        Public Shared Sub SaveInterviewCity(ByVal CityId As Integer, ByVal Application_No As Integer, ByVal Priority As Integer)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "INSERT INTO APPLICATION_INTERVIEW_CITY (RECORD_ID,APPLICATION_NO,CITY_ID,PRIORITY) VALUES(NEWID(),'" & Application_No & "','" & CityId & "','" & Priority & "')"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End Sub

        Public Shared Sub SaveCountriesWilling(ByVal Countryid As Integer, ByVal Application_No As Integer)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "INSERT INTO APPLICATION_COUNTRY_WILLING (RECORD_ID,APPLICATION_NO,COUNTRY_ID) VALUES(NEWID(),'" & Application_No & "','" & Countryid & "')"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        End Sub
        Public Shared Sub SaveSubjects(ByVal SubjectId As Integer, ByVal Application_No As Integer, ByVal Status As String)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "INSERT INTO APPLICATION_SUBJECT (RECORD_ID,APPLICATION_NO,SUBJECT_ID,STATUS) VALUES(NEWID(),'" & Application_No & "','" & SubjectId & "','" & Status & "')"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End Sub
        Public Shared Sub SaveBSU(ByVal BSUID As Integer, ByVal Application_No As Integer)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "INSERT INTO APPLICATION_BSU_APPLIED (RECORD_ID,APPLICATION_NO,APPLICANT_BSU_APPLIED) VALUES(NEWID(),'" & Application_No & "','" & BSUID & "')"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End Sub
        Public Shared Sub SaveReferences(ByVal Application_no As Integer, ByVal name As String, ByVal position As String, ByVal address As String, ByVal telephone As String, ByVal email As String, ByVal datesworked As String, ByVal Company_Name As String, ByVal req_ref As Boolean)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_no)
            pParms(1) = New SqlClient.SqlParameter("@NAME", name)
            pParms(2) = New SqlClient.SqlParameter("@POSITION", position)
            pParms(3) = New SqlClient.SqlParameter("@ADDRESS", address)
            pParms(4) = New SqlClient.SqlParameter("@TELEPHONE", telephone)
            pParms(5) = New SqlClient.SqlParameter("@EMAIL", email)
            pParms(6) = New SqlClient.SqlParameter("@DAYS_WORKED", datesworked)
            pParms(7) = New SqlClient.SqlParameter("@COMPANY_NAME", Company_Name)
            pParms(8) = New SqlClient.SqlParameter("@REQUEST_REFERENCE", req_ref)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_APPLICATION_REFERENCES", pParms)
        End Sub

        Public Shared Sub SaveEducation(ByVal Application_no As Integer, ByVal qualification As String, ByVal institution As String, ByVal year As String, ByVal result As String, ByVal spec As String)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_no)
            pParms(1) = New SqlClient.SqlParameter("@QUALIFICATION", qualification)
            pParms(2) = New SqlClient.SqlParameter("@INSTITUTE", institution)
            pParms(3) = New SqlClient.SqlParameter("@YEARATTENDED", year)
            pParms(4) = New SqlClient.SqlParameter("@RESULT", result)
            pParms(5) = New SqlClient.SqlParameter("@SPECILIZATION", spec)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_APPLICATION_EDUCATION", pParms)

        End Sub
        Public Shared Sub SaveCategory(ByVal Application_no As Integer, ByVal Category_ID As Integer)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_no)
            pParms(1) = New SqlClient.SqlParameter("@CATEGORY_ID", Category_ID)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_APPLICATION_CATEGORY", pParms)

        End Sub
        Public Shared Sub SaveWorkExperience(ByVal Application_no As Integer, ByVal Name As String, ByVal Address As String, ByVal Postbox As String, ByVal Country_id As String, ByVal is_school As Boolean, ByVal Curriculum As String, ByVal SchoolType As String)
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", Application_no)
            pParms(1) = New SqlClient.SqlParameter("@NAME", Name)
            pParms(2) = New SqlClient.SqlParameter("@ADDRESS", Address)
            pParms(3) = New SqlClient.SqlParameter("@POBOX", Postbox)
            pParms(4) = New SqlClient.SqlParameter("@COUNTRY_ID", Country_id)
            pParms(5) = New SqlClient.SqlParameter("@IS_SCHOOL", is_school)
            pParms(6) = New SqlClient.SqlParameter("@CLM", Curriculum)
            pParms(7) = New SqlClient.SqlParameter("@SCHOOL_TYPE", SchoolType)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_APPLICATION_WORK_EXPERIENCE", pParms)

        End Sub
        Public Shared Function CheckTeaching(ByVal Category_Id As String) As Boolean
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim str_query = "SELECT TEACHING_MODE FROM CATEGORY WHERE CATEGORY_ID='" & Category_Id & "'"
            Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If val = "True" Then
                Return True
            Else
                Return False
            End If
        End Function

    End Class
End Namespace