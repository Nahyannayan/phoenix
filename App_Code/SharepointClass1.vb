﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
'Imports System.Web.UI.WebControls
Imports Microsoft.SharePoint.Client
Imports System.Net
Imports System.IO
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class SharepointClass1

    Public Property SHAREPOINT_URL As String
    Public Property SHAREPOINT_USER As String
    Public Property SHAREPOINT_PASSWORD As String
    Public Property SHAREPOINT_DOMAIN As String
    Public Property RA_BEFORE As Double
    Public Function LoginToSharepoint(ByVal DDT_ID As Int64) As ClientContext
        LoginToSharepoint = Nothing
        Try
            Dim strConn As String = ""
            Dim Qry As New StringBuilder
            Qry.Append("SELECT ISNULL(DDT_SHAREPOINT_URL,'')SHAREPOINT_URL,ISNULL(DDT_SHAREPOINT_USER,'')SHAREPOINT_USER,")
            Qry.Append("ISNULL(DDT_SHAREPOINT_PASSWORD,'')SHAREPOINT_PASSWORD,ISNULL(DDT_SHAREPOINT_DOMAIN,'')SHAREPOINT_DOMAIN ")
            Qry.Append("FROM OASIS_CLM.dbo.DOC_DEPARTMENT WITH(NOLOCK) WHERE DDT_ID=@DDT_ID")
            strConn = ConnectionManger.GetOASISConnectionString
            Using conn As New SqlConnection()
                conn.ConnectionString = strConn
                Using cmd As New SqlCommand()
                    cmd.CommandText = Qry.ToString
                    cmd.Parameters.AddWithValue("@DDT_ID", DDT_ID)

                    cmd.Connection = conn
                    conn.Open()
                    Dim sql As String = UtilityObj.CommandAsSql(cmd)
                    Using sdr As SqlDataReader = cmd.ExecuteReader()
                        Dim encr As New SHA256EncrDecr
                        While sdr.Read()
                            SHAREPOINT_URL = encr.Decrypt_SHA256(sdr("SHAREPOINT_URL").ToString)
                            SHAREPOINT_USER = encr.Decrypt_SHA256(sdr("SHAREPOINT_USER").ToString)
                            SHAREPOINT_PASSWORD = encr.Decrypt_SHA256(sdr("SHAREPOINT_PASSWORD").ToString)
                            SHAREPOINT_DOMAIN = encr.Decrypt_SHA256(sdr("SHAREPOINT_DOMAIN").ToString)
                        End While
                    End Using
                    conn.Close()
                End Using
            End Using
            'Dim dsCredential As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "")
            Dim cxt As New ClientContext(SHAREPOINT_URL)
            cxt.Credentials = New NetworkCredential(SHAREPOINT_USER, SHAREPOINT_PASSWORD, SHAREPOINT_DOMAIN)
            cxt.ExecuteQuery()
            LoginToSharepoint = cxt
        Catch ex As Exception
            UtilityObj.Errorlog("LoginToSharepoint() - " & ex.Message, "SharepointClass")
        End Try
    End Function
    Public Shared Function CreateFolder(ByRef cxt As ClientContext, ByRef list As List, ByVal FolderNAme As String) As Boolean
        CreateFolder = False
        'Enable Folder creation for the list
        list.EnableFolderCreation = True
        list.Update()
        cxt.ExecuteQuery()
        Try
            Dim info As New ListItemCreationInformation()
            info.UnderlyingObjectType = FileSystemObjectType.Folder
            info.LeafName = FolderNAme.Trim() 'Trim for spaces.Just extra check
            Dim newItem As ListItem = list.AddItem(info)
            newItem("Title") = FolderNAme
            newItem.Update()
            cxt.ExecuteQuery()
            CreateFolder = True
        Catch ex As Exception
            UtilityObj.Errorlog("CreateFolder() - " & ex.Message, "SharepointClass")
        End Try
    End Function

    Public Shared Function CreateLibrary(ByRef cxt As ClientContext, ByVal LibraryName As String) As Boolean
        CreateLibrary = False
        Try
            Dim lci As New ListCreationInformation()
            lci.Description = LibraryName
            lci.Title = LibraryName
            lci.TemplateType = CInt(ListTemplateType.DocumentLibrary)
            Dim newLib As List = cxt.Web.Lists.Add(lci)
            cxt.Load(newLib)
            cxt.ExecuteQuery()
            CreateLibrary = True
        Catch ex As Exception
            UtilityObj.Errorlog("CreateLibrary() - " & ex.Message, "SharepointClass")
        End Try
    End Function

    Public Shared Sub GetAllLibraries(ByRef cxt As ClientContext, ByRef ddl As DropDownList)
        ddl.Items.Clear()
        Try
            Dim web As Web = cxt.Web
            cxt.Load(web.Lists, Function(lists) lists.Include(Function(list) list.Title, Function(list) list.Id)) ' For each list, retrieve Title and Id.
            cxt.ExecuteQuery()
            ' Enumerate the web.Lists. 
            For Each list As List In web.Lists
                Dim LI As System.Web.UI.WebControls.ListItem = New System.Web.UI.WebControls.ListItem(list.Title, cxt.Url & "/" & list.Title)
                ddl.Items.Add(LI)
            Next list
        Catch ex As Exception
            UtilityObj.Errorlog("GetAllLibraries() - " & ex.Message, "SharepointClass")
        End Try
    End Sub

    Public Shared Sub GetAllFolders(ByRef cxt As ClientContext, ByVal LibraryName As String, ByRef ddl As DropDownList)
        ddl.Items.Clear()
        Try
            Dim List As List = cxt.Web.Lists.GetByTitle(LibraryName)
            cxt.Load(List)
            cxt.Load(List.RootFolder)
            cxt.Load(List.RootFolder.Folders)
            cxt.ExecuteQuery()

            Dim folders As FolderCollection = List.RootFolder.Folders
            For Each fldr As Folder In folders
                Dim LI As System.Web.UI.WebControls.ListItem = New System.Web.UI.WebControls.ListItem(fldr.Name, cxt.Url & "/" & LibraryName & "/" & fldr.Name)
                ddl.Items.Add(LI)
            Next
        Catch ex As Exception
            UtilityObj.Errorlog("GetAllFolders() - " & ex.Message, "SharepointClass")
        End Try

    End Sub

    Public Shared Sub GetAllFiles(ByRef cxt As ClientContext, ByVal LibraryName As String, ByVal FolderName As String, ByRef ddl As DropDownList)
        ddl.Items.Clear()
        Try
            Dim List As List = cxt.Web.Lists.GetByTitle(LibraryName)
            cxt.Load(List)
            cxt.Load(List.RootFolder)
            cxt.Load(List.RootFolder.Folders)
            cxt.ExecuteQuery()
            Dim folders As FolderCollection = List.RootFolder.Folders
            For Each fldr As Folder In folders
                If fldr.Name = FolderName Then
                    cxt.Load(fldr.Files)
                    cxt.ExecuteQuery()
                    Dim Fcoll As FileCollection = fldr.Files
                    For Each fil As Microsoft.SharePoint.Client.File In Fcoll
                        Dim url As String = New Uri(cxt.Url).GetLeftPart(UriPartial.Authority)
                        'Label1.Text &= url & "" & file.Name & "<br/>"
                        Dim LI As System.Web.UI.WebControls.ListItem = New System.Web.UI.WebControls.ListItem(fil.Name, url & "" & fil.ServerRelativeUrl)
                        ddl.Items.Add(LI)
                    Next
                    Exit For
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog("GetAllFiles() - " & ex.Message, "SharepointClass")
        End Try
    End Sub

    Public Shared Function IsLibraryExists(ByRef clientContext As ClientContext, ByVal listTitle As String) As Boolean
        IsLibraryExists = False
        Dim web As Web = clientContext.Web
        Dim lists As ListCollection = web.Lists
        clientContext.Load(lists)
        clientContext.ExecuteQuery()
        For Each L As List In lists
            If L.Title.Equals(listTitle, StringComparison.CurrentCultureIgnoreCase) Then
                IsLibraryExists = True
                Exit For
            End If
        Next
    End Function

    Public Shared Function UploadFile(ByRef clientContext As ClientContext, ByVal LibraryName As String, ByVal FolderName As String, ByVal FileName As String, ByRef SavedFilePath As String, ByVal BSU_Shortname As String, ByVal DOC_No As String) As Boolean
        UploadFile = False
        SavedFilePath = ""
        Dim bLibraryExists As Boolean = False
        Dim bFolderExists As Boolean = False
        UtilityObj.Errorlog("UploadFile() - " & "Starting the Function", "SharepointClass")
        Try
            If Not IsLibraryExists(clientContext, LibraryName) Then
                bLibraryExists = CreateLibrary(clientContext, LibraryName) 'Creates library if not exists
            Else
                bLibraryExists = True
            End If

            If bLibraryExists Then
                Dim List As List = clientContext.Web.Lists.GetByTitle(LibraryName)
                clientContext.Load(List)
                clientContext.Load(List.RootFolder)
                clientContext.Load(List.RootFolder.Folders)
                clientContext.ExecuteQuery()
                Dim folders As FolderCollection = List.RootFolder.Folders
                For Each fldr As Folder In folders
                    If fldr.Name.ToLower.Trim = FolderName.ToLower.Trim Then
                        bFolderExists = True
                        Exit For
                    End If
                Next
                UtilityObj.Errorlog("UploadFile() - " & bFolderExists, "SharepointClass")
                If Not bFolderExists Then
                    If CreateFolder(clientContext, List, FolderName) Then 'Create folder in library if not exists
                        bFolderExists = True
                    End If
                End If

                If bFolderExists Then
                    UtilityObj.Errorlog("UploadFile() - " & FileName, "SharepointClass")
                    Using fs = New FileStream(FileName, FileMode.Open) 'Upload file if library and folder exists
                        Dim fi = New FileInfo(FileName)
                        Dim DestinationFileName As String = "", ServerDomain As String = ""
                        ServerDomain = New Uri(clientContext.Url).GetLeftPart(UriPartial.Authority)
                        DestinationFileName = BSU_Shortname & "_" & DOC_No & "_" & fi.Name
                        Dim fileUrl = String.Format("{0}/{1}", ServerDomain & List.RootFolder.ServerRelativeUrl & "/" & FolderName, DestinationFileName)
                        While CheckFileExists(clientContext, fileUrl)
                            Dim random = New Random(Date.Now.Millisecond)
                            Dim randomNumber As Integer = random.Next(1, 500000)
                            DestinationFileName = randomNumber & "_" & DestinationFileName
                            fileUrl = String.Format("{0}/{1}", ServerDomain & List.RootFolder.ServerRelativeUrl & "/" & FolderName, DestinationFileName)
                        End While
                        If (clientContext.HasPendingRequest) Then
                            clientContext.ExecuteQuery()
                        End If
                        Try
                            Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl & "/" & FolderName & "/" & DestinationFileName, fs, False)
                            UploadFile = True
                            SavedFilePath = fileUrl 'New Uri(clientContext.Url).GetLeftPart(UriPartial.Authority) & fileUrl
                        Catch ex As Exception
                            UtilityObj.Errorlog("UploadFile() - " & ex.Message, "SharepointClass")
                        End Try

                    End Using
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("UploadFile() - " & ex.Message, "SharepointClass")
        End Try
    End Function

    Public Shared Sub DownloadFilesFromSharePoint(ByRef ctx As ClientContext, ByVal FileName As String, ByVal LocalFilePath As String, ByRef byteArray As Byte())
        Dim web As Web = ctx.Web
        ctx.Load(web)
        Dim url As String = New Uri(FileName).AbsolutePath
        Dim f As Microsoft.SharePoint.Client.File = web.GetFileByServerRelativeUrl(url)
        ctx.Load(f)
        ctx.ExecuteQuery()
        Dim fileInfo As FileInformation = Microsoft.SharePoint.Client.File.OpenBinaryDirect(ctx, f.ServerRelativeUrl)
        Dim imageArray() As Byte = Nothing
        Using mStream As New System.IO.MemoryStream()
            If fileInfo.Stream IsNot Nothing Then
                fileInfo.Stream.CopyTo(mStream)
                imageArray = mStream.ToArray()
            End If
        End Using
    End Sub

    Public Shared Function DeleteFile(ByRef ctx As ClientContext, ByVal FileName As String) As Boolean
        DeleteFile = False
        Try
            Dim web As Web = ctx.Web
            ctx.Load(web)
            Dim url As String = New Uri(FileName).AbsolutePath
            Dim f As Microsoft.SharePoint.Client.File = web.GetFileByServerRelativeUrl(url)
            ctx.Load(f)
            ctx.ExecuteQuery()
            If f.Exists Then
                f.DeleteObject()
                ctx.ExecuteQuery()
                DeleteFile = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("DeleteFile() - " & ex.Message, "SharepointClass")
        End Try
    End Function

    Public Shared Function CheckFileExists(ByRef ctx As ClientContext, ByVal FilePath As String) As Boolean
        CheckFileExists = False
        Try
            Dim web As Web = ctx.Web
            ctx.Load(web)
            Dim url As String = New Uri(FilePath).AbsolutePath
            Dim f As Microsoft.SharePoint.Client.File = web.GetFileByServerRelativeUrl(url)
            ctx.Load(f)
            ctx.ExecuteQuery()
            If f.Exists Then
                CheckFileExists = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("CheckFileExists() - " & ex.Message, "SharepointClass")
        End Try
    End Function
End Class
