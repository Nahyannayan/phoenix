﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data

Public Class clsInventory

    Public Shared Function GetItem(ByVal BSUID As String, ByVal prefix As String) As String()
        Dim items As New List(Of String)()
        Dim Qry As String = "dbo.GET_ITEM_LIST_FOR_PRICING @BSU_ID,@PREFIX_TEXT "

        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry
                cmd.Parameters.AddWithValue("@BSU_ID", BSUID)
                cmd.Parameters.AddWithValue("@PREFIX_TEXT", Trim(prefix))

                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        items.Add(String.Format("{0}$$${1}$$${2}", sdr("ITM_DESCR"), sdr("ITM_ISBN"), sdr("ITM_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return items.ToArray()
        End Using
    End Function

    Public Shared Function GET_SALES_DAYEND_AMOUNT(ByVal BSU_ID As String) As DataSet
        GET_SALES_DAYEND_AMOUNT = Nothing
        Try
            Dim Result As New DataTable
            Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            mSet = Mainclass.getDataSet("dbo.GET_SALES_DAYEND_AMOUNT", sqlParam, str_conn)
            If Not mSet Is Nothing AndAlso mSet.Tables(0).Rows.Count > 0 Then
                GET_SALES_DAYEND_AMOUNT = mSet
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "OASIS")
        End Try
    End Function

    Public Shared Function IsPostedFortheDay(ByVal BSU_ID As String) As Boolean
        IsPostedFortheDay = False
        Dim Qry As String = "SELECT COUNT(FOC_ID) AS bEXISTS FROM OASIS_FEES.FEES.FEEOTHCOLLECTION_H WITH(NOLOCK) INNER JOIN OASIS_FEES.FEES.FEEOTHCOLLSUB_D WITH(NOLOCK) ON FOD_FOC_ID = FOC_ID " & _
        "WHERE   FOC_BSU_ID = @BSU_ID AND FOC_EMP_ID = 'Auto' AND ISNULL(FOC_COLLECTION_SOURCE,'') = 'BOOKS' AND FOC_DATE = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0) GROUP BY FOC_RECNO"

        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry
                cmd.Parameters.AddWithValue("@BSU_ID", BSU_ID)
                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        If sdr("bEXISTS") > 0 Then
                            IsPostedFortheDay = True
                        End If
                    End While
                End Using
                conn.Close()
            End Using
        End Using
    End Function

End Class
