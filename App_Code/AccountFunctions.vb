Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj


Public Class AccountFunctions

    ''Public Shared Function GetNextDocId(ByVal p_Type As String, ByVal p_Bsu_id As String, ByVal p_Month As Integer, ByVal p_Year As Integer) As String
    ''    'select dbo.fN_ReturnNextNo('JV','SA','7','2007')
    ''    Try
    ''        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    ''        Dim ds As New DataSet
    ''        Dim str_Sql As String = "select dbo.fN_ReturnNextNo('" & p_Type & "','" & p_Bsu_id & "','" & p_Month & "','" & p_Year & "')"
    ''        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

    ''        If ds.Tables(0).Rows.Count > 0 Then
    ''            Return ds.Tables(0).Rows(0)(0) & ""
    ''        End If
    ''        Return ""
    ''    Catch ex As Exception
    ''        Return ""
    ''        Errorlog(ex.Message)
    ''    End Try

    ''End Function

    Public Shared Function GetPaymentTerm(ByVal vAccCode As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim strSql As String = "SELECT isnull(PAYMENTTERM_M.PTM_DESCR,'') FROM ACCOUNTS_M LEFT OUTER JOIN " & _
        " PAYMENTTERM_M ON ACCOUNTS_M.ACT_PTM_ID = PAYMENTTERM_M.PTM_ID " & _
        " WHERE ACCOUNTS_M.ACT_ID = '" & vAccCode & "'"
        Dim obj As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strSql)
        If obj Is DBNull.Value Then
            Return ""
        Else
            Return obj.ToString
        End If

    End Function

    Public Shared Function GetInvoiceDetails(ByVal vDocNo As String, ByVal tranType As String, ByVal vBSUID As String) As MyReportClass
        Dim repSourceSubRep As New MyReportClass
        Dim cmdPostDetails As New SqlCommand
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        cmdPostDetails.CommandText = "EXEC GetSettelemetInfoForVoucher '" & vBSUID & "' ,'" & tranType & "','" & vDocNo & "'   "
        cmdPostDetails.Connection = New SqlConnection(str_conn)
        cmdPostDetails.CommandType = CommandType.Text
        repSourceSubRep.Command = cmdPostDetails
        Return repSourceSubRep
    End Function

    Public Shared Function AddPostingDetails(ByVal vDocNo As String, ByVal tranType As String, ByVal vBSUID As String) As MyReportClass()
        Dim repSourceSubRep(0) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdPostDetails As New SqlCommand
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString

        Dim strFilter As String = String.Empty
        Dim strAND As String = String.Empty
        Dim strWhere As String = " WHERE "
        strFilter += strWhere + " JOURNAL_D.JNL_DOCNO ='" & vDocNo & "'" & " AND JNL_BSU_ID = '" & vBSUID & "' AND JNL_DOCTYPE = '" & tranType & "'"

        cmdPostDetails.CommandText = "SELECT ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, JOURNAL_D.JNL_CHQNO, JOURNAL_D.JNL_CHQDT," & _
        " JOURNAL_D.JNL_CREDIT, JOURNAL_D.JNL_DEBIT, vw_OSO_BUSINESSUNIT_M.BSU_NAME, JOURNAL_D.JNL_DOCTYPE, " & _
        "JOURNAL_D.JNL_DOCDT, JOURNAL_D.JNL_NARRATION, JOURNAL_H.JHD_DOCNO" & _
        " FROM JOURNAL_D RIGHT OUTER JOIN JOURNAL_H ON JOURNAL_D.JNL_BSU_ID = JOURNAL_H.JHD_BSU_ID AND " & _
        "JOURNAL_D.JNL_FYEAR = JOURNAL_H.JHD_FYEAR AND JOURNAL_D.JNL_DOCTYPE = JOURNAL_H.JHD_DOCTYPE AND " & _
        "JOURNAL_D.JNL_DOCNO = JOURNAL_H.JHD_DOCNO AND JOURNAL_D.JNL_SUB_ID = JOURNAL_H.JHD_SUB_ID LEFT OUTER JOIN " & _
        "ACCOUNTS_M ON JOURNAL_D.JNL_ACT_ID = ACCOUNTS_M.ACT_ID LEFT OUTER JOIN vw_OSO_BUSINESSUNIT_M ON " & _
        "JOURNAL_H.JHD_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID " & strFilter _
        & " ORDER BY JOURNAL_D.JNL_ID"

        cmdPostDetails.Connection = New SqlConnection(str_conn)
        cmdPostDetails.CommandType = CommandType.Text
        repSourceSubRep(0).Command = cmdPostDetails

        Select Case tranType
            Case "BP", "CP"
                ReDim Preserve repSourceSubRep(1)
                repSourceSubRep(1) = GetInvoiceDetails(vDocNo, tranType, vBSUID)
        End Select

        Return repSourceSubRep
    End Function

    Public Shared Function GetNextDocId(ByVal p_Type As String, ByVal p_Bsu_id As String, ByVal p_Month As Integer, ByVal p_Year As Integer) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        '@DOC_ID = N'dn',
        '		@BSU_ID = N'125016',
        '		@DOS_MONTH = 11,
        '		@DOS_YEAR = 2007,
        '		@NextNO = @NextNO OUTPUT
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = p_Type
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_Bsu_id
            pParms(2) = New SqlClient.SqlParameter("@DOS_MONTH", SqlDbType.Int)
            pParms(2).Value = p_Month
            pParms(3) = New SqlClient.SqlParameter("@DOS_YEAR", SqlDbType.Int)
            pParms(3).Value = p_Year
            pParms(4) = New SqlClient.SqlParameter("@NextNO", SqlDbType.VarChar, 20)
            pParms(4).Direction = ParameterDirection.Output

            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim retval As Integer

            retval = SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "ReturnNextNo", pParms)
            If pParms(5).Value = "0" Then

                GetNextDocId = pParms(4).Value & ""
            Else
                GetNextDocId = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            GetNextDocId = ""
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function

    Public Shared Function GetLedgerBalance(ByVal p_Bsu_id As String, ByVal p_Accountid As String, ByVal p_Docdate As String) As Double
        '@BSU_ID = N'125016',
        '@ACT_ID = N'06101002',
        '@DOCDT = N'2007-10-17 
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Dim objConn As New SqlConnection(str_conn) '
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
                pParms(0).Value = p_Bsu_id
                pParms(1) = New SqlClient.SqlParameter("@ACT_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = p_Accountid
                pParms(2) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime, 20)
                pParms(2).Value = p_Docdate
                GetLedgerBalance = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "GetLedgerBalance", pParms)
            End Using
        Catch ex As Exception
            Errorlog(ex.Message)
            GetLedgerBalance = 0
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function

    Public Shared Function GetReconclileBalance(ByVal p_Bsu_id As String, ByVal p_Accountid As String, ByVal p_Docdate As String) As Double
        '@RCD_BSU_ID = N'125016',
        '@RCD_JNL_BSU_ID = N'125016',
        '@RCD_JNL_ACT_ID = N'24601006',
        '@RCD_RECONDT = N'2007-10-27'
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RCD_BSU_ID", SqlDbType.VarChar, 50)
                pParms(0).Value = p_Bsu_id
                pParms(1) = New SqlClient.SqlParameter("@RCD_JNL_BSU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = p_Bsu_id
                pParms(2) = New SqlClient.SqlParameter("@RCD_JNL_ACT_ID", SqlDbType.VarChar, 20)
                pParms(2).Value = p_Accountid
                pParms(3) = New SqlClient.SqlParameter("@RCD_RECONDT", SqlDbType.DateTime)
                pParms(3).Value = p_Docdate

                GetReconclileBalance = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "GETRECONCILEDBALANCE", pParms)

            End Using

        Catch ex As Exception
            Errorlog(ex.Message)
            GetReconclileBalance = 0
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try


    End Function

    Public Shared Function SaveReconclile_D(ByVal p_Bsu_id As String, _
    ByVal p_Accountid As String, ByVal p_Docdate As String, ByVal p_DocNO As String, _
    ByVal p_Jnl_id As String, ByVal p_Slno As Integer, ByVal p_Recdate As String, ByVal p_Deposit As Decimal, ByVal p_Payment As Decimal, _
    ByVal stTrans As SqlTransaction, Optional ByVal p_temp As Boolean = False) As String
        '      @RCD_BSU_ID = N'125016',
        '@RCD_JNL_BSU_ID = N'125016',
        '@RCD_JNL_ACT_ID = N'24601006',
        '@RCD_RECONDT = N'2007-10-27'
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Try

            Dim str_Sql As String
            str_Sql = "SELECT     GUID, JNL_ID, JNL_SUB_ID, " _
            & " JNL_BSU_ID, JNL_FYEAR, JNL_DOCTYPE, " _
            & " JNL_DOCNO, JNL_CUR_ID, JNL_EXGRATE1," _
            & " JNL_EXGRATE2, JNL_DOCDT, JNL_ACT_ID," _
            & " JNL_DEBIT, JNL_CREDIT, JNL_NARRATION, " _
            & " JNL_RECONDT, ISNULL(JNL_CHQNO,' ') AS JNL_CHQNO, JNL_CHQDT, " _
            & " JNL_REFDOCTYPE, JNL_REFDOCNO, " _
            & " JNL_BDISCOUNTED, JNL_BDELETED," _
            & " JNL_SLNO, JNL_RSS_ID, JNL_OPP_ACT_ID, " _
            & " JNL_ACTVOUCHERDT, JNL_PARTY_ACT_ID" _
            & " FROM JOURNAL_D " _
            & " where JNL_ID='" & p_Jnl_id & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ' ''		@GUID = NULL,
                ' ''		@RCD_DOCNO = '123',
                ' ''        @RCD_RECONDT='2007-12-17',
                ' ''		@RCD_DOCDT = N'2007-12-17',
                ' ''		@RCD_BSU_ID = N'125016',
                Dim pParms(28) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@GUID", SqlDbType.VarChar, 50)
                pParms(0).Value = System.DBNull.Value
                pParms(1) = New SqlClient.SqlParameter("@RCD_DOCNO", SqlDbType.VarChar, 20)
                pParms(1).Value = p_DocNO
                pParms(2) = New SqlClient.SqlParameter("@RCD_DOCDT", SqlDbType.DateTime)
                pParms(2).Value = p_Docdate
                pParms(3) = New SqlClient.SqlParameter("@RCD_BSU_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = p_Bsu_id

                ' ''		@RCD_FYEAR = 2007,
                ' ''		@RCD_JNL_ID = 40470,
                ' ''		@RCD_JNL_SUB_ID = N'007',
                ' ''		@RCD_JNL_BSU_ID = N'125016',
                ' ''		@RCD_JNL_FYEAR = 2007,

                pParms(4) = New SqlClient.SqlParameter("@RCD_FYEAR", SqlDbType.Int)
                pParms(4).Value = ds.Tables(0).Rows(0)("JNL_FYEAR")
                pParms(5) = New SqlClient.SqlParameter("@RCD_JNL_ID", SqlDbType.VarChar, 20)
                pParms(5).Value = p_Jnl_id
                pParms(6) = New SqlClient.SqlParameter("@RCD_JNL_SUB_ID", SqlDbType.VarChar, 20)
                pParms(6).Value = ds.Tables(0).Rows(0)("JNL_SUB_ID")
                pParms(7) = New SqlClient.SqlParameter("@RCD_JNL_BSU_ID", SqlDbType.VarChar, 20)
                pParms(7).Value = p_Bsu_id
                pParms(8) = New SqlClient.SqlParameter("@RCD_JNL_FYEAR", SqlDbType.Int)
                pParms(8).Value = ds.Tables(0).Rows(0)("JNL_FYEAR")
                ' ''		@RCD_JNL_DOCTYPE = N'BP',
                ' ''		@RCD_JNL_DOCNO = N'0709BP-00100',
                ' ''		@RCD_JNL_DOCDT = N'2007-12-17',
                ' ''		@RCD_JNL_ACT_ID = N'24601006',
                ' ''		@RCD_JNL_DEBIT = 0,

                'JNL_ID JNL_BSU_ID JNL_SUB_ID JNL_FYEAR
                'JNL_DOCTYPE JNL_DOCNO JNL_CUR_ID
                'JNL_EXGRATE1 JNL_EXGRATE2 JNL_DOCDT
                'JNL_ACT_ID JNL_DEBIT JNL_CREDIT
                'JNL_NARRATION JNL_RECONDT JNL_CHQNO
                'JNL_CHQDT JNL_REFDOCTYPE JNL_REFDOCNO
                'JNL_BDISCOUNTED JNL_BDELETED JNL_ACTVOUCHERDT
                'JNL_SLNO JNL_RSS_ID JNL_OPP_ACT_ID    
                pParms(9) = New SqlClient.SqlParameter("@RCD_JNL_DOCTYPE", SqlDbType.VarChar, 10)
                pParms(9).Value = "REC"
                pParms(10) = New SqlClient.SqlParameter("@RCD_JNL_DOCNO", SqlDbType.VarChar, 20)
                pParms(10).Value = ds.Tables(0).Rows(0)("JNL_DOCNO")
                pParms(11) = New SqlClient.SqlParameter("@RCD_JNL_DOCDT", SqlDbType.DateTime)
                pParms(11).Value = ds.Tables(0).Rows(0)("JNL_DOCDT")
                pParms(12) = New SqlClient.SqlParameter("@RCD_JNL_ACT_ID", SqlDbType.VarChar, 20)
                pParms(12).Value = ds.Tables(0).Rows(0)("JNL_ACT_ID")
                pParms(13) = New SqlClient.SqlParameter("@RCD_JNL_DEBIT", SqlDbType.Decimal, 21)
                pParms(13).Value = p_Deposit 'ds.Tables(0).Rows(0)("JNL_DEBIT")

                ' ''		@RCD_JNL_CREDIT = 8160,
                ' ''		@RCD_JNL_CUR_ID = N'DHS',
                ' ''		@RCD_JNL_EXGRATE1 = 1,
                ' ''		@RCD_JNL_EXGRATE2 = 1,
                ' ''		@RCD_JNL_NARRATION = N'BOOKPLUS PDC',
                pParms(14) = New SqlClient.SqlParameter("@RCD_JNL_CREDIT", SqlDbType.Decimal, 21)
                pParms(14).Value = p_Payment 'ds.Tables(0).Rows(0)("JNL_CREDIT")
                pParms(15) = New SqlClient.SqlParameter("@RCD_JNL_CUR_ID", SqlDbType.VarChar, 20)
                pParms(15).Value = ds.Tables(0).Rows(0)("JNL_CUR_ID")
                pParms(16) = New SqlClient.SqlParameter("@RCD_JNL_EXGRATE1", SqlDbType.Decimal, 20)
                pParms(16).Value = ds.Tables(0).Rows(0)("JNL_EXGRATE1")
                pParms(17) = New SqlClient.SqlParameter("@RCD_JNL_EXGRATE2", SqlDbType.Decimal, 20)
                pParms(17).Value = ds.Tables(0).Rows(0)("JNL_EXGRATE2")
                pParms(18) = New SqlClient.SqlParameter("@RCD_JNL_NARRATION", SqlDbType.VarChar, 200)
                pParms(18).Value = ds.Tables(0).Rows(0)("JNL_NARRATION")

                ' ''		@RCD_JNL_CHQNO = N'19',
                ' ''		@RCD_JNL_CHQDT = N'2007-12-17',
                ' ''		@RCD_bTemp = 0,
                ' ''		@RCD_bDeleted = 0,
                ' ''		@RCD_SLNO = 0
                ''@RCD_NEWDOCNO
                pParms(19) = New SqlClient.SqlParameter("@RCD_JNL_CHQNO", SqlDbType.VarChar, 20)
                pParms(19).Value = ds.Tables(0).Rows(0)("JNL_CHQNO")
                pParms(20) = New SqlClient.SqlParameter("@RCD_JNL_CHQDT", SqlDbType.DateTime)
                pParms(20).Value = ds.Tables(0).Rows(0)("JNL_CHQDT")
                pParms(21) = New SqlClient.SqlParameter("@RCD_bTemp", SqlDbType.Bit)
                pParms(21).Value = p_temp
                pParms(22) = New SqlClient.SqlParameter("@RCD_bDeleted", SqlDbType.Bit)
                pParms(22).Value = False
                pParms(23) = New SqlClient.SqlParameter("@RCD_JNL_SLNO", SqlDbType.Int)
                pParms(23).Value = ds.Tables(0).Rows(0)("JNL_SLNO")
                pParms(24) = New SqlClient.SqlParameter("@RCD_SLNO", SqlDbType.Int)
                pParms(24).Value = p_Slno
                pParms(25) = New SqlClient.SqlParameter("@RCD_RECONDT", SqlDbType.DateTime)
                pParms(25).Value = p_Recdate
                pParms(26) = New SqlClient.SqlParameter("@RCD_NEWDOCNO", SqlDbType.VarChar, 20)
                pParms(26).Direction = ParameterDirection.Output

                pParms(27) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(27).Direction = ParameterDirection.ReturnValue
                'Dim stTrans As SqlTransaction = objConn.BeginTransaction

                SaveReconclile_D = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SaveRECONCILIATION_D", pParms)
                If pParms(27).Value = "0" Then

                    If p_Slno = 1 Then
                        SaveReconclile_D = pParms(26).Value & "|" & "0"
                    Else
                        SaveReconclile_D = 0
                    End If
                Else
                    SaveReconclile_D = pParms(27).Value
                End If
            Else
                'jnl not found
                SaveReconclile_D = "1000"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            SaveReconclile_D = 1000
        Finally

        End Try


    End Function

    Public Shared Function GetRandomString(Optional ByVal length As Integer = 10) As String
        'Get the GUID
        Dim guidResult As String = System.Guid.NewGuid().ToString()

        'Remove the hyphens
        guidResult = guidResult.Replace("-", String.Empty)

        'Make sure length is valid
        If length <= 0 OrElse length > guidResult.Length Then
            Throw New ArgumentException("Length must be between 1 and " & guidResult.Length)
        End If

        'Return the first length bytes
        Return guidResult.Substring(0, length)
    End Function

    Public Shared Function SaveSAVESETTLE_D(ByVal p_Sub_id As String, _
        ByVal p_Lineid As String, ByVal p_Type As String, _
        ByVal p_Exch1 As String, ByVal p_Exch2 As String, _
        ByVal p_Bsu_id As String, ByVal p_Year As Integer, ByVal p_DocNO As String, _
        ByVal p_Actid As String, ByVal p_Amount As String, _
        ByVal p_Curid As String, ByVal p_Date As String, ByVal p_RGUID As String, _
        ByVal p_RDocNO As String, ByVal p_RCurid As String, _
        ByVal stTrans As SqlTransaction) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Try
            ' ''@SET_SUB_ID = N'007',
            ' ''@SET_BSU_ID = N'125016',
            ' ''@SET_FYEAR = 2007,
            ' ''@SET_DOCTYPE = N'BP',
            ' ''@SET_DOCNO = N'0709BP-00100',
            Dim pParms(21) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SET_SUB_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_Sub_id
            pParms(1) = New SqlClient.SqlParameter("@SET_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_Bsu_id
            pParms(2) = New SqlClient.SqlParameter("@SET_FYEAR", SqlDbType.Int)
            pParms(2).Value = p_Year
            pParms(3) = New SqlClient.SqlParameter("@SET_DOCTYPE", SqlDbType.VarChar, 20)
            pParms(3).Value = p_Type
            pParms(4) = New SqlClient.SqlParameter("@SET_DOCNO", SqlDbType.VarChar, 20)
            pParms(4).Value = p_DocNO
            ' ''@SET_LINEID = 1,
            ' ''@SET_ACCTCODE = N'06101002',
            ' ''@SET_SETTLEDAMT = 10000,
            ' ''@SET_CURRENCY = N'DHS',
            ' ''@SET_EXGRATE1 = 1,
            ' ''@SET_EXGRATE2 = 2,
            pParms(5) = New SqlClient.SqlParameter("@SET_LINEID", SqlDbType.Int)
            pParms(5).Value = p_Lineid
            pParms(6) = New SqlClient.SqlParameter("@SET_ACCTCODE", SqlDbType.VarChar, 20)
            pParms(6).Value = p_Actid
            pParms(7) = New SqlClient.SqlParameter("@SET_SETTLEDAMT", SqlDbType.VarChar, 20)
            pParms(7).Value = p_Amount
            pParms(8) = New SqlClient.SqlParameter("@SET_CURRENCY", SqlDbType.VarChar, 20)
            pParms(8).Value = p_Curid
            pParms(9) = New SqlClient.SqlParameter("@SET_EXGRATE1", SqlDbType.Decimal, 21)
            pParms(9).Value = p_Exch1
            pParms(10) = New SqlClient.SqlParameter("@SET_EXGRATE2", SqlDbType.Decimal, 21)
            pParms(10).Value = p_Exch2
            ' ''@SET_REFDOCTYPE = N'CN',
            ' ''@SET_REFDOCNO = N'0709CN-00002',
            ' ''@SET_REFLINEID = 0,
            ' ''@SET_REFCURRENCY = N'DHS',
            ' ''@SET_REFEXGRATE1 = 3,
            ' ''@SET_REFEXGRATE2 = 4 
            Dim str_Sql As String

            Dim ds As New DataSet
            str_Sql = "SELECT    * FROM TRANHDR_D" _
                        & " WHERE GUID='" & p_RGUID & "'"
            ds.Tables.Clear()

            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count = 0 Then
                SaveSAVESETTLE_D = "526"
            End If
            pParms(11) = New SqlClient.SqlParameter("@SET_REFDOCTYPE", SqlDbType.VarChar, 20)
            pParms(11).Value = ds.Tables(0).Rows(0)("TRN_DOCTYPE")
            pParms(12) = New SqlClient.SqlParameter("@SET_REFDOCNO", SqlDbType.VarChar, 20)
            pParms(12).Value = p_RDocNO
            pParms(13) = New SqlClient.SqlParameter("@SET_REFLINEID", SqlDbType.Int)
            pParms(13).Value = ds.Tables(0).Rows(0)("TRN_LINEID")
            pParms(14) = New SqlClient.SqlParameter("@SET_REFCURRENCY", SqlDbType.VarChar, 20)
            pParms(14).Value = p_RCurid
            pParms(15) = New SqlClient.SqlParameter("@SET_REFEXGRATE1", SqlDbType.Decimal, 21)
            pParms(15).Value = ds.Tables(0).Rows(0)("TRN_EXGRATE1")
            pParms(16) = New SqlClient.SqlParameter("@SET_REFEXGRATE2", SqlDbType.Decimal, 21)
            pParms(16).Value = ds.Tables(0).Rows(0)("TRN_EXGRATE2")
            '@SET_DOCDT='15 sep 2007',
            '@SET_REFDOCDT='15 sep 2007',
            '@SET_REFYEAR
            pParms(17) = New SqlClient.SqlParameter("@SET_DOCDT", SqlDbType.DateTime)
            pParms(17).Value = p_Date
            pParms(18) = New SqlClient.SqlParameter("@SET_REFDOCDT", SqlDbType.DateTime)
            pParms(18).Value = ds.Tables(0).Rows(0)("TRN_DOCDT")
            pParms(19) = New SqlClient.SqlParameter("@SET_REFYEAR", SqlDbType.Int)
            pParms(19).Value = ds.Tables(0).Rows(0)("TRN_FYEAR")
            pParms(20) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(20).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVESETTLE_D", pParms)
            SaveSAVESETTLE_D = pParms(20).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            SaveSAVESETTLE_D = 1000
        End Try
    End Function

    Public Shared Function SAVESETTLEONLINE_D(ByVal p_Sub_id As String, _
            ByVal p_Lineid As String, ByVal p_Type As String, _
            ByVal p_Exch1 As String, ByVal p_Exch2 As String, _
            ByVal p_Bsu_id As String, ByVal p_Year As Integer, ByVal p_DocNO As String, _
            ByVal p_Actid As String, ByVal p_Amount As String, _
            ByVal p_Curid As String, ByVal p_Date As String, ByVal p_RGUID As String, _
              ByVal stTrans As SqlTransaction) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Try
            ' ''@SOL_SUB_ID = N'007',
            ' ''@SOL_BSU_ID = N'XXXXXX',
            ' ''@SOL_FYEAR = 2007 ,
            ' ''@SOL_DOCTYPE = N'BP',
            ' ''@SOL_DOCNO = N'D123',



            Dim pParms(21) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SOL_SUB_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_Sub_id
            pParms(1) = New SqlClient.SqlParameter("@SOL_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_Bsu_id
            pParms(2) = New SqlClient.SqlParameter("@SOL_FYEAR", SqlDbType.Int)
            pParms(2).Value = p_Year
            pParms(3) = New SqlClient.SqlParameter("@SOL_DOCTYPE", SqlDbType.VarChar, 20)
            pParms(3).Value = p_Type
            pParms(4) = New SqlClient.SqlParameter("@SOL_DOCNO", SqlDbType.VarChar, 20)
            pParms(4).Value = p_DocNO
            ' ''@SOL_DOCDT = N'22/SEP/2007',
            ' ''@SOL_LINEID = 1,
            ' ''@SOL_ACCTCODE = N'ACT333',
            ' ''@SOL_SETTLEDAMT = 3333,
            ' ''@SOL_CURRENCY = N'AED',
            ' ''@SOL_EXGRATE1 = 1,
            ' ''@SOL_EXGRATE2 = 2,

            pParms(5) = New SqlClient.SqlParameter("@SOL_LINEID", SqlDbType.Int)
            pParms(5).Value = p_Lineid
            pParms(6) = New SqlClient.SqlParameter("@SOL_ACCTCODE", SqlDbType.VarChar, 20)
            pParms(6).Value = p_Actid
            pParms(7) = New SqlClient.SqlParameter("@SOL_SETTLEDAMT", SqlDbType.VarChar, 20)
            pParms(7).Value = p_Amount
            pParms(8) = New SqlClient.SqlParameter("@SOL_CURRENCY", SqlDbType.VarChar, 20)
            pParms(8).Value = p_Curid
            pParms(9) = New SqlClient.SqlParameter("@SOL_EXGRATE1", SqlDbType.Decimal, 21)
            pParms(9).Value = p_Exch1
            pParms(10) = New SqlClient.SqlParameter("@SOL_EXGRATE2", SqlDbType.Decimal, 21)
            pParms(10).Value = p_Exch2
            ' ''@SOL_REFDOCTYPE = N'BR',
            ' ''@SOL_REFDOCNO = N'RD3333',
            ' ''@SOL_REFLINEID = 2,
            ' ''@SOL_REFCURRENCY = N'AED',
            ' ''@SOL_REFEXGRATE1 = 1,
            ' ''@SOL_REFEXGRATE2 = 2,
            ' ''@SOL_REFDOCDT = N'22/OCT/2007',
            ' ''@SOL_REFYEAR = 2007
            Dim str_Sql As String

            Dim ds As New DataSet
            str_Sql = "SELECT    * FROM TRANHDR_D" _
                        & " WHERE GUID='" & p_RGUID & "'"
            ds.Tables.Clear()

            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count = 0 Then
                SAVESETTLEONLINE_D = "526"
            End If
            pParms(11) = New SqlClient.SqlParameter("@SOL_REFDOCTYPE", SqlDbType.VarChar, 20)
            pParms(11).Value = ds.Tables(0).Rows(0)("TRN_DOCTYPE")
            pParms(12) = New SqlClient.SqlParameter("@SOL_REFDOCNO", SqlDbType.VarChar, 20)
            pParms(12).Value = ds.Tables(0).Rows(0)("TRN_DOCNO")
            pParms(13) = New SqlClient.SqlParameter("@SOL_REFLINEID", SqlDbType.Int)
            pParms(13).Value = ds.Tables(0).Rows(0)("TRN_LINEID")
            pParms(14) = New SqlClient.SqlParameter("@SOL_REFCURRENCY", SqlDbType.VarChar, 20)
            pParms(14).Value = ds.Tables(0).Rows(0)("TRN_CUR_ID")
            pParms(15) = New SqlClient.SqlParameter("@SOL_REFEXGRATE1", SqlDbType.Decimal, 21)
            pParms(15).Value = ds.Tables(0).Rows(0)("TRN_EXGRATE1")
            pParms(16) = New SqlClient.SqlParameter("@SOL_REFEXGRATE2", SqlDbType.Decimal, 21)
            pParms(16).Value = ds.Tables(0).Rows(0)("TRN_EXGRATE2")
            '@SET_DOCDT='15 sep 2007',
            '@SET_REFDOCDT='15 sep 2007',
            '@SET_REFYEAR
            pParms(17) = New SqlClient.SqlParameter("@SOL_DOCDT", SqlDbType.DateTime)
            pParms(17).Value = p_Date
            pParms(18) = New SqlClient.SqlParameter("@SOL_REFDOCDT", SqlDbType.DateTime)
            pParms(18).Value = ds.Tables(0).Rows(0)("TRN_DOCDT")
            pParms(19) = New SqlClient.SqlParameter("@SOL_REFYEAR", SqlDbType.Int)
            pParms(19).Value = ds.Tables(0).Rows(0)("TRN_FYEAR")


            pParms(20) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(20).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVESETTLEONLINE_D", pParms)
            SAVESETTLEONLINE_D = pParms(20).Value

        Catch ex As Exception
            Errorlog(ex.Message)
            SAVESETTLEONLINE_D = 1000
        End Try


    End Function

    Public Shared Function check_accounttype(ByVal p_accid As String, ByVal p_bsuid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID, ACT_FLAG" _
             & " FROM ACCOUNTS_M " _
             & " WHERE (ACT_Bctrlac = 'FALSE') " _
             & " AND (ACT_ID = '" & p_accid & "') " _
             & " AND (ACT_BSU_ID LIKE '%" & p_bsuid & "%')"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_FLAG")
            Else
                Return ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
    End Function

    Public Shared Function check_accountid(ByVal p_accid As String, ByVal p_bsuid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            'str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M" _
            '& " WHERE ACT_ID='" & p_accid & "'" _
            '& " AND ACT_Bctrlac='FALSE'"

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, PM.PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & p_accid & "'" _
            & " AND ACT_BSU_ID LIKE '%" & p_bsuid & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(1)
            Else
                Return ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try

    End Function

    Public Shared Function get_cost_center(ByVal p_ccsid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
               & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999' AND CCS_ID='" & p_ccsid & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("CCS_DESCR")
            End If
            Return p_ccsid
        Catch ex As Exception
            Errorlog(ex.Message)
            Return p_ccsid
        End Try

    End Function

    Public Shared Function CheckAccountBalance(ByVal p_Accountid As String, ByVal p_Bsu_id As String, ByVal p_Docdate As Date) As Double
        '@return_value = [dbo].[CheckAccountBalance]
        '@BSU_ID = N'125016',
        '@ACT_ID = N'24301002',
        '@DOCDT = N'2/sep/2009'
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
                pParms(0).Value = p_Bsu_id
                pParms(1) = New SqlClient.SqlParameter("@ACT_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = p_Accountid
                pParms(2) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime, 20)
                pParms(2).Value = p_Docdate

                CheckAccountBalance = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "GetLedgerBalance", pParms)
            End Using
        Catch ex As Exception
            Errorlog(ex.Message)
            CheckAccountBalance = 0
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function

    Public Shared Function SAVETRANHDRONLINE_D(ByVal p_GUID As String, ByVal p_Sub_id As String, _
        ByVal p_Lineid As String, ByVal p_Type As String, _
        ByVal p_Curid As String, _
        ByVal p_Exch1 As String, ByVal p_Exch2 As String, _
        ByVal p_Bsu_id As String, ByVal p_Year As Integer, ByVal p_DocNO As String, _
        ByVal p_Actid As String, ByVal p_DRCR As String, ByVal p_Amount As String, _
        ByVal p_DOCDate As String, ByVal p_INVno As String, _
        ByVal p_CHQDate As String, ByVal p_CHQno As String, _
        ByVal p_EDIT As Boolean, ByVal p_Narration As String, _
        ByVal stTrans As SqlTransaction) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Try
            ' ''@GUID = NULL,
            ' ''@TRN_SUB_ID = N'007',
            ' ''@TRN_BSU_ID = N'xxxxxx',
            ' ''@TRN_FYEAR = 2007,
            ' ''@TRN_DOCTYPE = N'BP',
            Dim pParms(21) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@GUID", SqlDbType.VarChar, 50)
            If p_GUID = "" Then
                pParms(0).Value = System.DBNull.Value
            Else
                pParms(0).Value = p_GUID
            End If
            pParms(1) = New SqlClient.SqlParameter("@TRN_SUB_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_Sub_id
            pParms(2) = New SqlClient.SqlParameter("@TRN_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_Bsu_id
            pParms(3) = New SqlClient.SqlParameter("@TRN_FYEAR", SqlDbType.Int)
            pParms(3).Value = p_Year
            pParms(4) = New SqlClient.SqlParameter("@TRN_DOCTYPE", SqlDbType.VarChar, 20)
            pParms(4).Value = p_Type
            ' ''@TRN_DOCNO = N'x123x',
            ' ''@TRN_LINEID = 1,
            ' ''@TRN_DOCDT = N'27/SEP/2007',
            ' ''@TRN_ACT_ID = N'06101002',
            ' ''@TRN_CUR_ID = N'AED',
            ' ''@TRN_EXGRATE1 = 1,
            ' ''@TRN_EXGRATE2 = 2,
            pParms(5) = New SqlClient.SqlParameter("@TRN_DOCNO", SqlDbType.VarChar, 20)
            pParms(5).Value = p_DocNO
            pParms(6) = New SqlClient.SqlParameter("@TRN_LINEID", SqlDbType.VarChar, 20)
            pParms(6).Value = p_Lineid
            pParms(7) = New SqlClient.SqlParameter("@TRN_DOCDT", SqlDbType.DateTime)
            pParms(7).Value = p_CHQDate
            pParms(8) = New SqlClient.SqlParameter("@TRN_ACT_ID", SqlDbType.VarChar, 20)
            pParms(8).Value = p_Actid
            pParms(9) = New SqlClient.SqlParameter("@TRN_CUR_ID", SqlDbType.VarChar, 20)
            pParms(9).Value = p_Curid
            pParms(10) = New SqlClient.SqlParameter("@TRN_EXGRATE1", SqlDbType.Decimal, 21)
            pParms(10).Value = p_Exch1
            pParms(11) = New SqlClient.SqlParameter("@TRN_EXGRATE2", SqlDbType.Decimal, 21)
            pParms(11).Value = p_Exch2
            ' ''@TRN_DEBIT = 1200,
            ' ''@TRN_CREDIT = 0,
            ' ''@TRN_ALLOCAMT = 0,
            ' ''@TRN_VOUCHDT = N'27/OCT/2007',
            ' ''@TRN_CHQNO = N'C12',
            ' ''@TRN_INVNO = N'I22',
            ' ''@TRN_NARRATION = N'VERUTHE',
            ' ''@bEdit = 0,
            ' ''@TRN_LOCALAMT = 111111222
            pParms(12) = New SqlClient.SqlParameter("@TRN_DEBIT", SqlDbType.Decimal, 21)
            If p_DRCR = "DR" Then
                pParms(12).Value = p_Amount
            Else
                pParms(12).Value = 0
            End If
            pParms(13) = New SqlClient.SqlParameter("@TRN_CREDIT", SqlDbType.Decimal, 21)
            If p_DRCR = "CR" Then
                pParms(13).Value = p_Amount
            Else
                pParms(13).Value = 0
            End If
            pParms(14) = New SqlClient.SqlParameter("@TRN_ALLOCAMT", SqlDbType.Decimal, 21)
            pParms(14).Value = 0
            pParms(15) = New SqlClient.SqlParameter("@TRN_VOUCHDT", SqlDbType.DateTime)
            pParms(15).Value = p_DOCDate
            pParms(16) = New SqlClient.SqlParameter("@TRN_CHQNO", SqlDbType.VarChar, 20)
            pParms(16).Value = p_CHQno
            pParms(17) = New SqlClient.SqlParameter("@TRN_INVNO", SqlDbType.VarChar, 20)
            If p_INVno = "" Then
                pParms(17).Value = System.DBNull.Value
            Else
                pParms(17).Value = p_INVno
            End If
            pParms(18) = New SqlClient.SqlParameter("@TRN_NARRATION", SqlDbType.VarChar, 20)
            pParms(18).Value = p_Narration
            pParms(19) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
            pParms(19).Value = p_EDIT
            pParms(20) = New SqlClient.SqlParameter("@TRN_LOCALAMT", SqlDbType.Decimal, 21)
            pParms(20).Value = p_Exch1 * p_Amount

            pParms(21) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(21).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVETRANHDRONLINE_D", pParms)
            SAVETRANHDRONLINE_D = pParms(21).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            SAVETRANHDRONLINE_D = 1000
        End Try
    End Function

    Public Shared Function Round(ByVal val As Object) As Object
        Try
            If IsNumeric(val) Then
                Return Math.Round(Convert.ToDecimal(val), HttpContext.Current.Session("BSU_ROUNDOFF"))
            Else
                Return 0
            End If
        Catch ex As Exception
            'UtilityObj.Errorlog("Error Message from Function AccountFunction.Round() :" & ex.Message)
            Return val
        End Try
    End Function

    Public Shared Function Round2(ByVal val As Object, ByVal roundby As Integer) As Object
        Try
            If IsNumeric(val) Then
                Return Math.Round(Convert.ToDecimal(val), roundby)
            Else
                Return 0
            End If
        Catch ex As Exception
            'UtilityObj.Errorlog("Error Message from Function AccountFunction.Round() :" & ex.Message)
            Return 0
        End Try
    End Function

    Public Shared Function get_CollectionType(ByVal p_accid As String, ByVal p_bsuid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     AM.ACT_ID, " _
            & " AM.ACT_COL_ID, CM.COL_DESCR  " _
            & " FROM ACCOUNTS_M AS AM  " _
            & " LEFT OUTER JOIN COLLECTION_M AS CM  " _
            & " ON AM.ACT_COL_ID = CM.COL_ID " _
            & " WHERE (AM.ACT_Bctrlac = 'FALSE') AND ACT_BACTIVE='TRUE'" _
            & " AND (AM.ACT_ID = '" & p_accid & "') " _
            & " AND (AM.ACT_BSU_ID LIKE '%" & p_bsuid & "%')"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_COL_ID") & "|" & ds.Tables(0).Rows(0)("COL_DESCR")
            Else
                Return ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
    End Function

    Public Shared Function Validate_Account(ByVal p_accid As String, ByVal p_bsuid As String, ByVal p_Mode As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql, str_filter As String
            str_filter = "AND "
            Select Case p_Mode
                Case "BANK"
                    str_filter = " AND ACT_BANKCASH='B'"
                Case "NOTCC"
                    str_filter = " AND ACT_BANKCASH<>'CC'"
                Case "CASHONLY"
                    str_filter = " AND ACT_BANKCASH='C'"
                Case "NORMAL"
                    str_filter = " AND ACT_BANKCASH='N'"
                Case "PARTY1"
                    str_filter = " AND ACT_FLAG='S'"
                Case "PARTY2"
                    str_filter = " AND ACT_TYPE in ('Liability','Asset')  and ACT_BANKCASH   in ('N') and ACT_BCTRLAC<>1"
                Case "DEBIT", "DEBIT_D"
                    str_filter = " AND ACT_BANKCASH='N'"
                    str_filter = str_filter & " AND ACT_FLAG = 'N'"
                Case "CHQISSAC"
                    str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("ChqissAC") & "'"
                Case "INTRAC"
                    str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("IntrAC") & "'"
                Case "ACRDAC"
                    str_filter = " AND ACT_CTRLACC='" & HttpContext.Current.Session("AcrdAc") & "'"
                Case "PREPDAC"
                    str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("PrepdAC") & "'"
                Case "CHQISSAC_PDC"
                    str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("ChqissAC") & "'"
                Case "CUSTSUPP"
                    str_filter = " AND (ACT_FLAG='S' OR ACT_FLAG='C')"
                Case "CUSTSUPPnIJV"
                    str_filter = " AND (( ACT_FLAG='S' OR  ACT_FLAG='C') or ( ACT_SGP_ID='0810')) "
                Case "INCOME"
                    str_filter = " AND (ACT_TYPE ='INCOME')"
                Case "CHARGE"
                    str_filter = " AND (ACT_TYPE ='Liability' AND ACT_FLAG<>'S')"
                Case "CONCESSION"
                    str_filter = " AND (ACT_TYPE ='EXPENSES')"
                Case "FEEDISC"
                    str_filter = " AND (ACT_TYPE ='EXPENSES')"
            End Select
            str_Sql = "SELECT ACT_NAME From ACCOUNTS_M " _
            & " WHERE (ACT_Bctrlac = 'FALSE') AND ACT_BACTIVE='TRUE' " _
            & " AND (ACT_ID = '" & p_accid & "') " _
            & " AND (ACT_BSU_ID LIKE '%" & p_bsuid & "%')" _
            & str_filter
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                Return ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
    End Function

    Public Shared Function get_CtrlAccount() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT isnull(SYS.SYS_SUPPLIER_CTRL_ACT_ID ,'')+'|'+isnull(AM.ACT_NAME,'') +'|'+isnull(AM.ACT_SGP_ID,'')" _
            & " FROM SYSINFO_S AS SYS INNER JOIN " _
            & " ACCOUNTS_M AS AM ON SYS.SYS_SUPPLIER_CTRL_ACT_ID = AM.ACT_ID"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0)
            Else
                Return ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
    End Function

    Public Shared Function CheckBAnkClearanceData(ByVal p_Type As String, ByVal p_Bsu_id As String, _
    ByVal P_DOC_ID As String, ByVal P_TRANS As SqlTransaction) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        '      @return_value = [dbo].[CheckBAnkClearanceData]
        '@DocType = N'CC',
        '@DOC_ID = N'0801BR-000001',
        '@BSU_ID = N'125003'
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@DocType", SqlDbType.VarChar, 50)
            pParms(0).Value = p_Type
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_Bsu_id
            pParms(2) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = P_DOC_ID
            pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(P_TRANS, CommandType.StoredProcedure, "CheckBAnkClearanceData", pParms)
            CheckBAnkClearanceData = pParms(3).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            CheckBAnkClearanceData = "1000"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Shared Function CheckBAnkClearanceData_DAX(ByVal p_Type As String, ByVal p_Bsu_id As String, _
   ByVal P_DOC_ID As String, ByVal P_TRANS As SqlTransaction) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
        '      @return_value = [dbo].[CheckBAnkClearanceData]
        '@DocType = N'CC',
        '@DOC_ID = N'0801BR-000001',
        '@BSU_ID = N'125003'
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@DocType", SqlDbType.VarChar, 50)
            pParms(0).Value = p_Type
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_Bsu_id
            pParms(2) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = P_DOC_ID
            pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(P_TRANS, CommandType.StoredProcedure, "FIN.CheckBAnkClearanceData", pParms)
            CheckBAnkClearanceData_DAX = pParms(3).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            CheckBAnkClearanceData_DAX = "1000"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Shared Function SaveRPTSETUP_S_ACC(ByVal p_RPA_RSS_TYP As String, _
        ByVal p_RPA_RSS_CODE As String, ByVal p_RPA_RSS_CODE1 As String, _
        ByVal p_RPA_ACC_ID As String, ByVal p_RPA_RSB_ID As String, ByVal p_stTrans As SqlTransaction) As String
        '     alter procedure 
        'SaveRPTSETUP_S_ACC() 
        '@RPA_RSS_TYP varchar(20)='MB',
        '@RPA_RSS_CODE varchar(20)='A032',
        '@RPA_RSS_CODE1 varchar(20)='A040', 
        '@RPA_ACC_ID varchar(20)='08101000' 
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@RPA_RSS_TYP", SqlDbType.VarChar, 20)
        pParms(0).Value = p_RPA_RSS_TYP
        pParms(1) = New SqlClient.SqlParameter("@RPA_RSS_CODE", SqlDbType.VarChar, 20)
        pParms(1).Value = p_RPA_RSS_CODE
        pParms(2) = New SqlClient.SqlParameter("@RPA_RSS_CODE1", SqlDbType.VarChar, 20)
        pParms(2).Value = p_RPA_RSS_CODE1
        pParms(3) = New SqlClient.SqlParameter("@RPA_ACC_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_RPA_ACC_ID
        pParms(4) = New SqlClient.SqlParameter("@RPA_RSB_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_RPA_RSB_ID
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveRPTSETUP_S_ACC", pParms)
        SaveRPTSETUP_S_ACC = pParms(5).Value
    End Function

    Public Shared Function getAccountname(ByVal p_accountid As String, ByVal P_BSU_ID As String) As String 'for verifying account nale during add -->returns accname|costcenter|mandatiry
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            ' str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M where ACT_ID='" & p_accountid & "' "
            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & p_accountid & "'"
            '& " AND ACT_BSU_ID LIKE '%" & P_BSU_ID & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_NAME") & "|" _
                & ds.Tables(0).Rows(0)("PLY_COSTCENTER") & "|" _
                & ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
    End Function

    Public Shared Function SaveVOUCHERSETUP_S(ByVal p_VHS_ID As String, _
        ByVal p_VHS_DOCTYPE As String, ByVal p_VHS_DESCRIPTION As String, _
        ByVal p_VHS_ACR_INT_ACT_ID As String, ByVal p_VHS_INT_ACT_ID As String, _
        ByVal p_VHS_PREP_EXP_ACT_ID As String, ByVal p_VHS_CHQ_ISS_ACT_ID As String, _
        ByVal p_stTrans As SqlTransaction) As String
        ' @return_value = [dbo].[SaveVOUCHERSETUP_S]
        '@VHS_ID = 0,
        '@VHS_DOCTYPE = N'PDC',
        '@VHS_DESCRIPTION = N'TEST',
        '@VHS_ACR_INT_ACT_ID = N'XC',
        '@VHS_INT_ACT_ID = N'XSX',
        '@VHS_PREP_EXP_ACT_ID = N'XSX',
        '@VHS_CHQ_ISS_ACT_ID = N'XSXSX'
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VHS_ID", SqlDbType.Int)
        pParms(0).Value = p_VHS_ID
        pParms(1) = New SqlClient.SqlParameter("@VHS_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(1).Value = p_VHS_DOCTYPE
        pParms(2) = New SqlClient.SqlParameter("@VHS_DESCRIPTION", SqlDbType.VarChar, 100)
        pParms(2).Value = p_VHS_DESCRIPTION
        pParms(3) = New SqlClient.SqlParameter("@VHS_ACR_INT_ACT_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_VHS_ACR_INT_ACT_ID
        pParms(4) = New SqlClient.SqlParameter("@VHS_INT_ACT_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_VHS_INT_ACT_ID
        pParms(5) = New SqlClient.SqlParameter("@VHS_PREP_EXP_ACT_ID", SqlDbType.VarChar, 20)
        pParms(5).Value = p_VHS_PREP_EXP_ACT_ID
        pParms(6) = New SqlClient.SqlParameter("@VHS_CHQ_ISS_ACT_ID", SqlDbType.VarChar, 20)
        pParms(6).Value = p_VHS_CHQ_ISS_ACT_ID
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveVOUCHERSETUP_S", pParms)
        SaveVOUCHERSETUP_S = pParms(7).Value
    End Function

    Public Shared Function get_CollectionAccount(ByVal p_ColId As String, ByVal p_bsuid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT COL.COL_ACT_ID, ACT.ACT_NAME" _
            & " FROM COLLECTION_M AS COL INNER JOIN " _
            & " ACCOUNTS_M AS ACT ON COL.COL_ACT_ID = ACT.ACT_ID  " _
            & " AND (ACT.ACT_BSU_ID LIKE '%" & p_bsuid & "%')" _
            & " WHERE     (COL.COL_ID = '" & p_ColId & "')  "
            
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("COL_ACT_ID") & "|" & ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                Return " | "
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try

    End Function

    Public Shared Function GetMatchingAccountList(ByVal ACT_NAME As String) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACT_NAME", SqlDbType.VarChar, 20)
        pParms(0).Value = ACT_NAME
        Dim dsData As DataSet
        dsData = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("mainDB").ConnectionString, _
        CommandType.StoredProcedure, "GetMatchingAccountList", pParms)
        Dim sql_query As String = "SELECT ACT_ID+ ' - ' + ACT_NAME AS ACCOUNT FROM ACCOUNTS_M " & _
            " WHERE ( replace(ACT_NAME,' ','') LIKE '%'+replace('" & ACT_NAME & "',' ','')+'%')"
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function SaveDayendJournalReverse(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
    ByVal p_docno As String, ByVal p_refdoctype As String, ByVal p_Subid As String, ByVal p_Bsuid As String, ByVal p_FYear As String, _
    ByVal p_Date As String, ByVal p_School_Bsuid As String) As String
        Dim iReturnvalue As Integer
        Dim cmd As New SqlCommand
        '      EXEC	@return_value = [dbo].[SaveDayendJournalReverse]
        '@JHD_SUB_ID = N'007',
        '@JHD_BSU_ID = N'900500',
        '@JHD_FYEAR = N'2008',
        '@REF_DOCTYPE = N'CR',
        '@REF_DOCNO = N'08CR-0000033',
        '@JHD_DOCDT = N'12-FEB-2009',
        '@SCHOOL_BSU_ID = N'125016',
        '@JHD_NEWDOCNO = @JHD_NEWDOCNO OUTPUT
        cmd.Dispose()
        cmd = New SqlCommand("SaveDayendJournalReverse", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@JHD_SUB_ID", p_Subid)
        cmd.Parameters.AddWithValue("@JHD_BSU_ID", p_Bsuid)
        cmd.Parameters.AddWithValue("@JHD_FYEAR", p_FYear)
        cmd.Parameters.AddWithValue("@REF_DOCTYPE", p_refdoctype)
        cmd.Parameters.AddWithValue("@REF_DOCNO", Trim(p_docno))
        cmd.Parameters.AddWithValue("@JHD_DOCDT", p_Date)
        cmd.Parameters.AddWithValue("@SCHOOL_BSU_ID", p_School_Bsuid)
        cmd.Parameters.Add("@JHD_NEWDOCNO", SqlDbType.VarChar, 20)
        cmd.Parameters("@JHD_NEWDOCNO").Direction = ParameterDirection.Output

        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
        cmd.ExecuteNonQuery()
        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
        Return iReturnvalue
    End Function

    Public Shared Function SaveDayendJournal(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
         ByVal p_JHD_DOCTYPE As String, ByVal p_Subid As String, ByVal p_Bsuid As String, _
         ByVal p_FYear As String, ByVal p_Date As String, ByVal p_JHD_CUR_ID As String, _
         ByVal p_JHD_EXGRATE1 As String, ByVal p_JHD_EXGRATE2 As String, ByVal p_JHD_NARRATION As String, _
         ByVal p_JHD_LOCK As String, ByVal p_JNL_ACT_ID_CR As String, ByVal p_JNL_ACT_ID_DR As String, _
         ByVal p_JNL_AMOUNT_DR As String, ByVal bChequeReverse As Boolean, ByVal p_Provider_BSU_ID As String, _
         ByVal bPostAftersave As Boolean) As String
        Dim iReturnvalue As Integer
        Dim cmd As New SqlCommand
        'EXEC @return_value = OASISFIN.DBO.SaveDayendJournal
        '@JHD_SUB_ID = @BSU_SUB_ID, @JHD_BSU_ID = @BSU_ID,
        '@JHD_FYEAR = @FYR_ID, @JHD_DOCTYPE = N'JV',
        '@JHD_DOCNO = N'NOTHING', @JHD_DOCDT = @Dt,

        '@JHD_CUR_ID = @BSU_CURRENCY, @JHD_EXGRATE1 = 1,
        '@JHD_EXGRATE2 = 1, @JHD_NARRATION = @VHH_NARRATION, 
        '@JHD_LOCK = @USER, @JNL_ACT_ID_CR =@SYS_FEERECEIVABLE_ACT_ID,
        '@JNL_ACT_ID_DR =@TRANSPORT_DAILYCOLLN_ACT_ID, @JNL_AMOUNT_DR =@CUR_FCA_AMOUNT,	 
        '@JHD_NEWDOCNO = @JHD_NEWDOCNO OUTPUT
        cmd.Dispose()
        cmd = New SqlCommand("SaveDayendJournal", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@JHD_SUB_ID", p_Subid)
        cmd.Parameters.AddWithValue("@JHD_BSU_ID", p_Bsuid)
        cmd.Parameters.AddWithValue("@JHD_FYEAR", p_FYear)
        cmd.Parameters.AddWithValue("@JHD_DOCTYPE", p_JHD_DOCTYPE)
        'cmd.Parameters.AddWithValue("@REF_DOCNO", Trim(p_JHD_DOCNO))
        cmd.Parameters.AddWithValue("@JHD_DOCDT", p_Date)
        cmd.Parameters.AddWithValue("@JHD_EXGRATE1", p_JHD_EXGRATE1)
        cmd.Parameters.AddWithValue("@JHD_EXGRATE2", p_JHD_EXGRATE2)
        cmd.Parameters.AddWithValue("@JHD_NARRATION", p_JHD_NARRATION)
        cmd.Parameters.AddWithValue("@JHD_LOCK", p_JHD_LOCK)
        cmd.Parameters.AddWithValue("@JNL_ACT_ID_CR", p_JNL_ACT_ID_CR)
        cmd.Parameters.AddWithValue("@JNL_ACT_ID_DR", p_JNL_ACT_ID_DR)
        cmd.Parameters.AddWithValue("@JNL_AMOUNT_DR", p_JNL_AMOUNT_DR)
        cmd.Parameters.AddWithValue("@bChequeReverse", bChequeReverse)
        cmd.Parameters.AddWithValue("@bPostAftersave", bPostAftersave)
        cmd.Parameters.AddWithValue("@Provider_BSU_ID", p_Provider_BSU_ID)
        cmd.Parameters.AddWithValue("@JHD_DOCNO", "")
        cmd.Parameters.AddWithValue("@JHD_CUR_ID", p_JHD_CUR_ID)

        cmd.Parameters.Add("@JHD_NEWDOCNO", SqlDbType.VarChar, 20)
        cmd.Parameters("@JHD_NEWDOCNO").Direction = ParameterDirection.Output

        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
        cmd.ExecuteNonQuery()
        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
        Return iReturnvalue
    End Function

    Public Shared Function GetCostCenter() As DataTable
        Dim sql_query As String = "SELECT CCS_ID,CCS_DESCR FROM COSTCENTER_S WHERE CCS_CCT_ID='9999' "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("mainDB").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function SubReports(ByVal vDocNo As String, ByVal tranType As String, ByVal vBSUID As String, ByVal IsCost As Boolean, ByVal Narr As Boolean) As MyReportClass()

        Dim x As Integer = 0
        Dim repSourceSubRep(0) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        '--------------------CR,CP,BP,JV,PJ,CN,DN,BR,IJV,PP,CC,IC
        If IsCost = True Then
            'ReDim Preserve repSourceSubRep(x)
            repSourceSubRep(x) = CostcenterSubRpt(vDocNo, tranType, vBSUID, HttpContext.Current.Session("Sub_ID"), HttpContext.Current.Session("F_YEAR"))
            x = x + 1
        End If
        '--------------------CR,CP,BP,JV,PJ,CN,DN,BR,CC,IC
        If Narr = True Then
            ReDim Preserve repSourceSubRep(x)
            repSourceSubRep(x) = VoucherNarration(vDocNo, tranType, vBSUID, HttpContext.Current.Session("Sub_ID"), HttpContext.Current.Session("F_YEAR"))
            x = x + 1
        End If
        Return repSourceSubRep
    End Function

    Public Shared Function CostcenterSubRpt(ByVal vDocNo As String, ByVal tranType As String, ByVal vBSUID As String, ByVal vSubId As String, ByVal vFYear As Integer) As MyReportClass
        Dim repSubRep As New MyReportClass
        Dim cmd As New SqlCommand("CostCenter_SubRpt")
        Dim str_conn As String = ConnectionManger.GetOASISFINConnectionString

        cmd.Parameters.AddWithValue("@VDS_SUB_ID", vSubId)
        cmd.Parameters.AddWithValue("@VDS_BSU_ID", vBSUID)
        cmd.Parameters.AddWithValue("@VDS_FYEAR", vFYear)
        cmd.Parameters.AddWithValue("@VDS_DOCTYPE", tranType)
        cmd.Parameters.AddWithValue("@VDS_DOCNO", Trim(vDocNo))

        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        repSubRep.Command = cmd
        Return repSubRep
    End Function

    Public Shared Function VoucherNarration(ByVal vDocNo As String, ByVal tranType As String, ByVal vBSUID As String, ByVal vSubId As String, ByVal vFYear As Integer) As MyReportClass
        Dim repSubRep As New MyReportClass
        Dim cmd As New SqlCommand("Vouchr_Narration")
        Dim str_conn As String = ConnectionManger.GetOASISFINConnectionString

        cmd.Parameters.AddWithValue("@VDS_SUB_ID", vSubId)
        cmd.Parameters.AddWithValue("@VDS_BSU_ID", vBSUID)
        cmd.Parameters.AddWithValue("@VDS_FYEAR", vFYear)
        cmd.Parameters.AddWithValue("@VDS_DOCTYPE", tranType)
        cmd.Parameters.AddWithValue("@VDS_DOCNO", Trim(vDocNo))

        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        repSubRep.Command = cmd
        Return repSubRep
    End Function
    

End Class
