Imports Microsoft.VisualBasic
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient
Imports UtilityObj

Public Class AccountsDetails
    Shared str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    Shared str_Sql As String
    Shared encrData As New Encryption64
    Shared MainMnu_code As String
    Shared datamode As String

    Shared Sub New()
        str_conn = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        datamode = "none"
    End Sub

    Shared Sub SetOpeningBalance(ByRef hyp1 As HyperLink)
        MainMnu_code = encrData.Encrypt("A200045")
        hyp1.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/accobPostOpeningBalance.aspx", MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT  count(*)  FROM  OPENING_H" _
        & " WHERE (OPENING_H.OPH_SUB_ID = '" & HttpContext.Current.Session("Sub_ID") & "')" _
        & " AND (OPENING_H.OPH_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "') AND OPH_bPOSTED=0" _
        & " AND OPH_DOCYEAR = " & HttpContext.Current.Session("F_YEAR")
        hyp1.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetDebitNote(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200020")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/acccnPostDebitNote.aspx", MainMnu_code, datamode)
        str_Sql = "SELECT  count(*)  FROM  VOUCHER_H" _
               & " where VHH_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
               & " VHH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR") & " and " _
               & " VHH_bDELETED=0 AND VHH_bPOSTED=0 AND VHH_DOCTYPE='DN'"
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetCreditNote(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200025")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/acccnPostDebitNote.aspx", MainMnu_code, datamode)
        Dim str_Sql As String
        str_Sql = "SELECT  count(*)  FROM  VOUCHER_H" _
               & " where VHH_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
               & " VHH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and" _
               & " VHH_bDELETED=0 AND VHH_bPOSTED=0 AND VHH_DOCTYPE='CN' AND VHH_bPDC=0 " _
               & " and VHH_FYEAR =" & HttpContext.Current.Session("F_YEAR")
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetPDCs(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200013")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/AccPOstBanktran.aspx", MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT  count(*)  FROM  VOUCHER_H" _
               & " where VHH_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
               & " VHH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and" _
               & " VHH_bDELETED=0 AND VHH_bPOSTED=0 AND VHH_DOCTYPE='BP' AND VHH_bPDC=1 " _
               & " AND VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetRecurringJournal(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200016")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/AccPOstBanktran.aspx", MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT  count(*) FROM  RJOURNAL_H" _
        & " where RJH_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
        & " RJH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and" _
        & " RJH_bDELETED=0 AND RJH_bPOSTED=0 " _
        & " AND RJH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetPurchaseJournal(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200017")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/AccPOstBanktran.aspx", MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT  count(*) FROM  PURCHASE_H" _
        & " where PUH_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
        & " PUH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and" _
        & " PUH_bDELETED=0 AND PUH_bPOSTED=0 " _
        & " AND PUH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetPrepaymentVoucher(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200030")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/accPrePaymentsPost.aspx", MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT  count(*)  FROM  PREPAYMENTS_H" _
               & " where PRP_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
               & " PRP_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and" _
               & " PRP_bDeleted =0 AND PRP_bPosted=0 " _
               & " AND PRP_FYEAR = " & HttpContext.Current.Session("F_YEAR")

        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetIUJournalVoucher(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200014")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/accIUPostIUJournalVoucher.aspx", MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = " SELECT  count(*) FROM  IJOURNAL_H IJH INNER JOIN IJOURNAL_D AS IJD ON " & _
        " IJH.IJH_FYEAR = IJD.IJL_FYEAR  AND IJH.IJH_DOCTYPE = IJD.IJL_DOCTYPE AND IJH.IJH_DOCNO = IJD.IJL_DOCNO WHERE " & _
        " IJH_FYEAR = '" & HttpContext.Current.Session("F_YEAR") & "' AND IJH_bDELETED = 0 " & _
        " AND ((IJH.IJH_bDELETED = 0 AND IJH.IJH_CR_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "' and IJH.IJH_bPOSTEDCRBSU = 0 ) " & _
        " OR (IJH.IJH_bDELETED = 0 AND IJH.IJH_DR_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & _
        "' AND IJH.IJH_FYEAR = " & HttpContext.Current.Session("F_YEAR") & " AND IJH.IJH_bPOSTEDDRBSU = 0)) " & _
        " AND (IJH_ISS_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' OR IJH.IJH_bPOSTEDDRBSU = 1 OR IJH.IJH_bPOSTEDCRBSU=1)"

        '& " AND (CASE WHEN IJH.IJH_CR_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "' AND " _
        '& " IJH.IJH_bPOSTEDCRBSU = 1 THEN 1 ELSE CASE WHEN IJH.IJH_DR_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "' AND  " _
        '& " IJH.IJH_bPOSTEDDRBSU = 1 THEN 1 ELSE 0 END END ) = 0 " _
        '& " AND (IJH.IJH_CR_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "' or IJH.IJH_DR_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "') "
        'str_Sql = " SELECT COUNT(*)" _
        '& " FROM IJOURNAL_H WHERE " & _
        '" IJH_bDELETED = 0 and  (IJH_CR_BSU_ID = '" & _
        'HttpContext.Current.Session("sBsuid") & "' and IJH_bPOSTEDCRBSU = 0  ) OR (IJH_DR_BSU_ID = '" & _
        'HttpContext.Current.Session("sBsuid") & "' and IJH_bPOSTEDDRBSU = 0)" & _
        '" AND IJH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub


    Shared Sub SetCashPayment(ByRef hyp As HyperLink, Optional ByVal bFromAccounts As Boolean = False, Optional ByVal tillDate As String = "")

        Dim strPath As String = IIf(bFromAccounts, "acccpPostCashPayment.aspx", "Accounts/acccpPostCashPayment.aspx")
        MainMnu_code = encrData.Encrypt("A200010")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", strPath, MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT COUNT(*)" _
        & " FROM VOUCHER_H" _
        & " WHERE  (VOUCHER_H.VHH_SUB_ID = '" & HttpContext.Current.Session("Sub_ID") & "') " _
        & " AND (VOUCHER_H.VHH_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "') " _
        & " AND (VOUCHER_H.VHH_bDELETED = 0) AND (VOUCHER_H.VHH_DOCTYPE = 'CP')" _
        & " AND VOUCHER_H.VHH_bPOSTED=0 " _
        & " AND VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        If bFromAccounts Then
            str_Sql += " AND VHH_DOCDT <= '" & tillDate & "'"
        End If

        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetJournalVoucher(ByRef hyp As HyperLink)
        MainMnu_code = encrData.Encrypt("A200015")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", "Accounts/accjvPostJournalVoucher.aspx", MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = " SELECT COUNT(*)" _
       & " FROM JOURNAL_H where JHD_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "'" _
       & "  AND JHD_bDELETED=0 " _
       & " AND JHD_DOCTYPE='JV' AND JHD_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' " _
       & " AND  JHD_bPOSTED=0 AND JHD_bDELETED=0 " _
       & " AND JHD_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetBankPayment(ByRef hyp As HyperLink, Optional ByVal bFromAccounts As Boolean = False, Optional ByVal tillDate As String = "")
        Dim strPath As String = IIf(bFromAccounts, "AccPOstBanktran.aspx", "Accounts/AccPOstBanktran.aspx")
        MainMnu_code = encrData.Encrypt("A200011")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", strPath, MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT  count(*)  FROM  VOUCHER_H" _
               & " where VHH_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
               & " VHH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and" _
               & " VHH_bDELETED=0 AND VHH_bPOSTED=0 AND VHH_DOCTYPE='BP' AND VHH_bPDC=0 " _
               & " AND VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        If bFromAccounts Then
            str_Sql += " AND VHH_DOCDT <= '" & tillDate & "'"
        End If
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetCreditCardReceipt(ByRef hyp As HyperLink, Optional ByVal bFromAccounts As Boolean = False, Optional ByVal tillDate As String = "")
        Dim strPath As String = IIf(bFromAccounts, "accccPostCreditcardReceipt.aspx", "Accounts/accccPostCreditcardReceipt.aspx")
        MainMnu_code = encrData.Encrypt(OASISConstants.AccPostCreditCardReceipt)
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", strPath, MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT COUNT(*)" _
        & " FROM VOUCHER_H" _
        & " WHERE  (VOUCHER_H.VHH_SUB_ID = '" & HttpContext.Current.Session("Sub_ID") & "') " _
        & " AND (VOUCHER_H.VHH_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "') " _
        & " AND (VOUCHER_H.VHH_bDELETED = 0)  AND (VOUCHER_H.VHH_DOCTYPE = 'CC')" _
        & " AND VOUCHER_H.VHH_bPOSTED=0 " _
        & " AND VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        If bFromAccounts Then
            str_Sql += " AND VHH_DOCDT <= '" & tillDate & "'"
        End If
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetCashReceipt(ByRef hyp As HyperLink, Optional ByVal bFromAccounts As Boolean = False, Optional ByVal tillDate As String = "")
        Dim strPath As String = IIf(bFromAccounts, "acccrPostCashReceipt.aspx", "Accounts/acccrPostCashReceipt.aspx")
        MainMnu_code = encrData.Encrypt("A200005")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", strPath, MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT COUNT(*)" _
        & " FROM VOUCHER_H" _
        & " WHERE  (VOUCHER_H.VHH_SUB_ID = '" & HttpContext.Current.Session("Sub_ID") & "') " _
        & " AND (VOUCHER_H.VHH_BSU_ID = '" & HttpContext.Current.Session("sBsuid") & "') " _
        & " AND (VOUCHER_H.VHH_bDELETED = 0) AND (VOUCHER_H.VHH_DOCTYPE = 'CR')" _
        & " AND VOUCHER_H.VHH_bPOSTED=0" _
        & " AND VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        If bFromAccounts Then
            str_Sql += " AND VHH_DOCDT <= '" & tillDate & "'"
        End If
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetBankReceipt(ByRef hyp1 As HyperLink, Optional ByVal bFromAccounts As Boolean = False, Optional ByVal tillDate As String = "")
        Dim strPath As String = IIf(bFromAccounts, "AccPostBankTran.aspx", "Accounts/AccPostBankTran.aspx")
        MainMnu_code = encrData.Encrypt("A200012")
        hyp1.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", strPath, MainMnu_code, datamode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT  count(*)  FROM  VOUCHER_H" _
           & " where VHH_SUB_ID='" & HttpContext.Current.Session("Sub_ID") & "' and " _
           & " VHH_BSU_ID='" & HttpContext.Current.Session("sBsuid") & "' and" _
           & " VHH_bDELETED=0 AND VHH_bPOSTED=0 AND VHH_DOCTYPE='BR' AND VHH_bPDC=0" _
           & " AND VHH_FYEAR = " & HttpContext.Current.Session("F_YEAR")
        If bFromAccounts Then
            str_Sql += " AND VHH_DOCDT <= '" & tillDate & "'"
        End If
        hyp1.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub

    Shared Sub SetTreasuryTransfer(ByRef hyp As HyperLink, Optional ByVal bFromAccounts As Boolean = False, Optional ByVal tillDate As String = "")
        Dim strPath As String = IIf(bFromAccounts, "AccTreasuryTransferApprove.aspx", "Accounts/AccTreasuryTransferApprove.aspx")
        MainMnu_code = encrData.Encrypt("A200351")
        hyp.NavigateUrl = String.Format("{0}?MainMnu_code={1}&datamode={2}", strPath, MainMnu_code, datamode)
        str_Sql = "SELECT  COUNT(*) FROM INTERUNIT_TRANSFER  " _
        & " WHERE ITF_FYEAR = '" & HttpContext.Current.Session("F_YEAR") & "' AND ITF_bPOSTED = 1 AND ITF_bDELETED = 0  " _
        & " AND (( ITF_DRBSU_ID = '" & HttpContext.Current.Session("sBsuid") & "'   AND  ITF_bPOSTEDDRBSU = 0)" _
        & " OR  (ITF_CRBSU_ID = '" & HttpContext.Current.Session("sBsuid") & "' AND ITF_bPOSTEDCRBSU = 0 ))"
        If bFromAccounts Then
            str_Sql += " AND ITF_DATE <= '" & tillDate & "'"
        End If
        hyp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
    End Sub


    Public Shared Function SaveMonthlyData_D(ByVal vDOCTYPE As String, ByVal vEDD_TYPE As String, _
    ByVal vBSU_ID As String, ByVal vDOC_NO As String, ByVal vSUB_ID As String, ByVal vCUR_ID As String, _
    ByVal vF_YEAR As Integer, ByVal vDOC_DATE As Date, ByVal conn As SqlConnection, ByVal trans_OASISFin As SqlTransaction) As Integer
        Dim str_sql As String = ""
        str_sql = "SELECT  VDS_AMOUNT,VOUCHER_D_S.VDS_ERN_ID, VOUCHER_D_S.VDS_CODE" & _
        " FROM VOUCHER_D_S INNER JOIN VOUCHER_H " & _
        "ON VOUCHER_D_S.VDS_SUB_ID = VOUCHER_H.VHH_SUB_ID  " & _
        "AND VOUCHER_D_S.VDS_BSU_ID = VOUCHER_H.VHH_BSU_ID  " & _
        "AND VOUCHER_D_S.VDS_FYEAR = VOUCHER_H.VHH_FYEAR  " & _
        "AND VOUCHER_D_S.VDS_DOCTYPE = VOUCHER_H.VHH_DOCTYPE  " & _
        "AND VOUCHER_D_S.VDS_DOCNO = VOUCHER_H.VHH_DOCNO " & _
        "WHERE VDS_CCS_ID = 'EMP' " & _
        "AND VOUCHER_D_S.VDS_DOCNO ='" & vDOC_NO & "' AND VOUCHER_D_S.VDS_DOCTYPE='" & vDOCTYPE & _
        "' AND VOUCHER_D_S.VDS_SUB_ID = '" & vSUB_ID & "' AND VOUCHER_D_S.VDS_FYEAR =" & vF_YEAR

        Dim iretVal As Integer = 0
        Dim ds As DataSet = SqlHelper.ExecuteDataset(trans_OASISFin, CommandType.Text, str_sql)
        If ds Is Nothing OrElse ds.Tables.Count <= 0 Then
            Return 0
        End If
        For Each drReader As DataRow In ds.Tables(0).Rows
            Dim vVDS_ERN_ID As String = drReader("VDS_ERN_ID")
            Dim vVDS_AMOUNT As Double = drReader("VDS_AMOUNT")
            Dim vVDS_CODE As String = drReader("VDS_CODE")
            If iretVal = 0 Then iretVal = SaveMonthlyData_Details(vDOCTYPE, vEDD_TYPE, vBSU_ID, vDOC_NO, vVDS_CODE, vDOC_DATE.Month, vDOC_DATE.Year, vCUR_ID, vVDS_ERN_ID, vVDS_AMOUNT, conn, trans_OASISFin)
            If iretVal <> 0 Then Exit For
        Next
        Return iretVal
    End Function

    Private Shared Function SaveMonthlyData_Details(ByVal vDOCTYPE As String, ByVal vEDD_TYPE As String, ByVal vBSU_ID As String, ByVal REF_DOC_NO As String, ByVal EMP_ID As Integer, ByVal PAY_MONTH As Integer, _
    ByVal PAY_YEAR As Integer, ByVal CUR_ID As String, ByVal ERN_ID As String, ByVal AMOUNT As Double, _
    ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

        Dim cmd As SqlCommand
        cmd = New SqlCommand("OASIS..SaveEMPMONTHLYDATA_D", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpEDD_EMP_ID As New SqlParameter("@EDD_EMP_ID", SqlDbType.Int)
        sqlpEDD_EMP_ID.Value = EMP_ID 'EmployeeID
        cmd.Parameters.Add(sqlpEDD_EMP_ID)

        Dim sqlpEDD_BSU_ID As New SqlParameter("@EDD_BSU_ID", SqlDbType.VarChar, 10)
        sqlpEDD_BSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpEDD_BSU_ID)

        Dim sqlpEDD_PAYMONTH As New SqlParameter("@EDD_PAYMONTH", SqlDbType.TinyInt)
        sqlpEDD_PAYMONTH.Value = PAY_MONTH 'PAYMONTH
        cmd.Parameters.Add(sqlpEDD_PAYMONTH)

        Dim sqlpEDD_PAYYEAR As New SqlParameter("@EDD_PAYYEAR", SqlDbType.Int)
        sqlpEDD_PAYYEAR.Value = PAY_YEAR 'PAYYEAR
        cmd.Parameters.Add(sqlpEDD_PAYYEAR)

        Dim sqlpEDD_CUR_ID As New SqlParameter("@EDD_CUR_ID", SqlDbType.VarChar, 10)
        sqlpEDD_CUR_ID.Value = CUR_ID
        cmd.Parameters.Add(sqlpEDD_CUR_ID)

        Dim sqlpEDD_ERNCODE As New SqlParameter("@EDD_ERNCODE", SqlDbType.VarChar, 10)
        sqlpEDD_ERNCODE.Value = ERN_ID
        cmd.Parameters.Add(sqlpEDD_ERNCODE)

        Dim sqlpEDD_TYPE As New SqlParameter("@EDD_TYPE", SqlDbType.VarChar, 1)
        sqlpEDD_TYPE.Value = vEDD_TYPE
        cmd.Parameters.Add(sqlpEDD_TYPE)

        Dim sqlpEDD_AMOUNT As New SqlParameter("@EDD_AMOUNT", SqlDbType.Decimal)
        sqlpEDD_AMOUNT.Value = AMOUNT
        cmd.Parameters.Add(sqlpEDD_AMOUNT)

        Dim sqlpEDD_REMARKS As New SqlParameter("@EDD_REMARKS", SqlDbType.VarChar, 100)
        sqlpEDD_REMARKS.Value = vDOCTYPE
        cmd.Parameters.Add(sqlpEDD_REMARKS)

        Dim sqlpEDD_REFDOCNO As New SqlParameter("@EDD_REFDOCNO", SqlDbType.VarChar, 20)
        sqlpEDD_REFDOCNO.Value = REF_DOC_NO
        cmd.Parameters.Add(sqlpEDD_REFDOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        Return retValParam.Value
    End Function

End Class
