Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
Imports System.IO
Imports System.Data

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class LibraryData
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function TopViewItems(ByVal library_division_id As String, ByVal bsu_id As String) As XmlDocument
        Dim ds As DataSet
        Dim LastCount As String = "0"
        Dim Xdoc As New XmlDocument

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", bsu_id)
        pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", library_division_id)


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "LCD_LIBRARY_TOP_ITEM_VIEW", pParms)
        Dim strXdoc As String
        strXdoc = ds.GetXml
        Xdoc.LoadXml(strXdoc)

        Return Xdoc

    End Function

    Public Shared Function GetStockIDForAccessonNo(ByVal Accession_No As String, ByVal Bsu_id As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "Select STOCK_ID from LIBRARY_ITEMS_QUANTITY WHERE ACCESSION_NO='" & Accession_No & "' AND PRODUCT_BSU_ID='" & Bsu_id & "' "
        Dim stock_id = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Return stock_id
    End Function

    Public Shared Sub isOffLine(ByVal bsuper As String)

        If bsuper = "False" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim str_query = "Select OFFLINE from LIBRARY_A_SETTINGS WHERE ID=1"
            Dim offline = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If offline = "True" Then
                HttpContext.Current.Response.Redirect("~/Library/libraryOffline.aspx")
            End If
        End If



    End Sub



End Class
