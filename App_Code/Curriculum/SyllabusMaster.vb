Imports Microsoft.VisualBasic
Imports System.Data
Imports UtilityObj



Public Class SyllabusMaster
    Dim vSYL_ID As Integer
    Dim vSYL_ACD_ID As Integer
    Dim vSYL_GRD_ID As String
    Dim vSYL_DESC As String
    Dim vSYL_SUB_ID As Integer
    Dim vSYL_TRM_ID As Integer
    Dim vSYL_DETAILS() As SyllabusMaster
    Dim bDelete As Boolean
    Dim bEdit As Boolean
    Public Property NEWLY_ADDED() As Boolean
        Get
            Return bEdit
        End Get
        Set(ByVal value As Boolean)
            bEdit = value
        End Set
    End Property
    'Public Property Delete() As Boolean
    '    Get
    '        Return bDelete
    '    End Get
    '    Set(ByVal value As Boolean)
    '        bDelete = value
    '    End Set
    'End Property
    Public Property Delete() As Boolean
        Get
            Return bDelete
        End Get
        Set(ByVal value As Boolean)
            bDelete = value
        End Set
    End Property
    '    Public Property SYL_DETAILS() As SyllabusMaster
    '        Get
    '            Return vSYL_DETAILS
    '        End Get
    '        Set(ByVal value As SyllabusMaster())
    '            vSYL_DETAILS = value
    '        End Set
    '    End Property
    '    Public Property GSD_ID() As Integer
    '        Get
    '            Return vSYL_ID
    '        End Get
    '        Set(ByVal value As Integer)
    '            vSYL_ID = value
    '        End Set
    '    End Property
    'Public Property AcdId() As String
    '    Get
    '        Return vSYL_ID
    '    End Get
    '    Set(ByVal value As Integer)
    '        vSYL_ID = value
    '    End Set
    'End Property

    'Author(--AJITH)
    'Date   --1/APRIL/2009--created
    'Purpose--TO CREATE A DYNAMIC TABLE FOR SYLLABUS_M
    Public Shared Function CreateDataTableSyllabus() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim AcdId As New DataColumn("AcdId", System.Type.GetType("System.String"))
            Dim TrmId As New DataColumn("TrmId", System.Type.GetType("System.String"))
            Dim GrdId As New DataColumn("GrdId", System.Type.GetType("System.String"))
            Dim SubjId As New DataColumn("SubjId", System.Type.GetType("System.String"))
            Dim AcdYear As New DataColumn("AcdYear", System.Type.GetType("System.String"))
            Dim Term As New DataColumn("Term", System.Type.GetType("System.String"))
            Dim Grade As New DataColumn("Grade", System.Type.GetType("System.String"))
            Dim Subject As New DataColumn("Subject", System.Type.GetType("System.String"))
            Dim hide As New DataColumn("hide", System.Type.GetType("System.String"))
            Dim tempview As New DataColumn("tempview", System.Type.GetType("System.String"))
            Dim Syllabus As New DataColumn("Syllabus", System.Type.GetType("System.String"))
            Dim SylId As New DataColumn("SylId", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(AcdId)
            dtDt.Columns.Add(TrmId)
            dtDt.Columns.Add(GrdId)

            dtDt.Columns.Add(SubjId)
            dtDt.Columns.Add(AcdYear)
            dtDt.Columns.Add(Term)
            dtDt.Columns.Add(Grade)

            dtDt.Columns.Add(Subject)
            dtDt.Columns.Add(hide)
            dtDt.Columns.Add(tempview)
            dtDt.Columns.Add(Syllabus)
            dtDt.Columns.Add(SylId)
            dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
    'Author(--AJITH)
    'Date   --1/APRIL/2009--created
    'Purpose--TO CREATE A DYNAMIC TABLE FOR SYLLABUS_D

    Public Shared Function CreateDTSyllabusDetails() As DataTable
        Dim dtSyllebusDet As DataTable
        dtSyllebusDet = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim SUBID As New DataColumn("SUBID", System.Type.GetType("System.String"))
            Dim SYBDESC As New DataColumn("SYBDESC", System.Type.GetType("System.String"))
            Dim SYBPARENTID As New DataColumn("SYBPARENTID", System.Type.GetType("System.String"))
            Dim SYBPARENTDESC As New DataColumn("SYBPARENTDESC", System.Type.GetType("System.String"))
            Dim SYBSTARTDT As New DataColumn("SYBSTARTDT", System.Type.GetType("System.String"))
            Dim SYBENDDT As New DataColumn("SYBENDDT", System.Type.GetType("System.String"))
            Dim SYBTOTHRS As New DataColumn("SYBTOTHRS", System.Type.GetType("System.String"))
            Dim SYBOBJECTIVE As New DataColumn("SYBOBJECTIVE", System.Type.GetType("System.String"))
            Dim SYBORDER As New DataColumn("SYBORDER", System.Type.GetType("System.String"))
            Dim SydId As New DataColumn("SydId", System.Type.GetType("System.String"))
            Dim SYBRESOURCE As New DataColumn("SYBRESOURCE", System.Type.GetType("System.String"))

            dtSyllebusDet.Columns.Add(ID)
            dtSyllebusDet.Columns.Add(SUBID)
            dtSyllebusDet.Columns.Add(SYBDESC)
            dtSyllebusDet.Columns.Add(SYBPARENTDESC)
            dtSyllebusDet.Columns.Add(SYBPARENTID)
            dtSyllebusDet.Columns.Add(SYBSTARTDT)
            dtSyllebusDet.Columns.Add(SYBENDDT)
            dtSyllebusDet.Columns.Add(SYBTOTHRS)
            dtSyllebusDet.Columns.Add(SYBOBJECTIVE)
            dtSyllebusDet.Columns.Add(SYBORDER)
            dtSyllebusDet.Columns.Add(SydId)
            dtSyllebusDet.Columns.Add(SYBRESOURCE)
            '
            Return dtSyllebusDet
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtSyllebusDet
        End Try
    End Function
    'Author(--arun.g)
    'Date   --1/APRIL/2009--created
    'Purpose--TO CREATE A DYNAMIC TABLE FOR SYLLABUS_TEACHER

    Public Shared Function CreateDT_SYLLABUS_TEACHER_M() As DataTable
        Dim dtLessonTeac As DataTable
        dtLessonTeac = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim TOPICID As New DataColumn("TOPICID", System.Type.GetType("System.String"))
            Dim TOPICNAME As New DataColumn("TOPICNAME", System.Type.GetType("System.String"))
            'Dim SYLLABUSID As New DataColumn("SYLLABUSID", System.Type.GetType("System.String"))
            Dim EMPID As New DataColumn("EMPID", System.Type.GetType("System.String"))
            Dim EMPNAME As New DataColumn("EMPNAME", System.Type.GetType("System.String"))
            'Dim GRADEID As New DataColumn("GRADEID", System.Type.GetType("System.String"))
            Dim GROUPID As New DataColumn("GROUPID", System.Type.GetType("System.String"))
            Dim LESFROMDT As New DataColumn("LESFROMDT", System.Type.GetType("System.String"))
            Dim LESTODT As New DataColumn("LESTODT", System.Type.GetType("System.String"))
            Dim LESHRSTK As New DataColumn("LESHRSTK", System.Type.GetType("System.String"))
            Dim LESOBJECTIVE As New DataColumn("LESOBJECTIVE", System.Type.GetType("System.String"))
            'Dim LESRESOURCE As New DataColumn("LESRESOURCE", System.Type.GetType("System.String"))
            Dim LESDESCRIBTION As New DataColumn("LESDESCRIBTION", System.Type.GetType("System.String"))
            Dim LESPlanBreakup As New DataColumn("LESPlanBreakup", System.Type.GetType("System.String"))
            Dim LesSYTId As New DataColumn("LesSYTId", System.Type.GetType("System.String"))
            'Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            dtLessonTeac.Columns.Add(Id)
            dtLessonTeac.Columns.Add(TOPICID)
            dtLessonTeac.Columns.Add(TOPICNAME)
            'dtLessonTeac.Columns.Add(SYLLABUSID)
            dtLessonTeac.Columns.Add(EMPID)
            dtLessonTeac.Columns.Add(EMPNAME)
            'dtLessonTeac.Columns.Add(GRADEID)
            dtLessonTeac.Columns.Add(GROUPID)
            dtLessonTeac.Columns.Add(LESFROMDT)
            dtLessonTeac.Columns.Add(LESTODT)
            dtLessonTeac.Columns.Add(LESHRSTK)
            dtLessonTeac.Columns.Add(LESOBJECTIVE)
            'dtLessonTeac.Columns.Add(LESRESOURCE)
            dtLessonTeac.Columns.Add(LESPlanBreakup)
            dtLessonTeac.Columns.Add(LesSYTId)

            Return dtLessonTeac
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtLessonTeac
        End Try
    End Function
    
End Class
