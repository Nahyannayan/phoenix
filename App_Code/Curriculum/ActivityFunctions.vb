Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Collections
Imports System.Reflection
Imports System.Collections.Generic

Public Class ActivityFunctions
    Public Shared Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String)
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + bsuid + "' AND ACD_CURRENT=1 AND ACD_CLM_ID=" + clm _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then
            Dim li As New ListItem
            li.Text = ds.Tables(0).Rows(0).Item(0)
            li.Value = ds.Tables(0).Rows(0).Item(1)
            ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        End If
        Return ddlAcademicYear
    End Function

    Public Shared Function GetBSU_ACD_YEAR(ByVal BSU_ID As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --04/Feb/2008--modified
        'Purpose--Get the Academic year for the active BSU
        Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim sqlGetOpen_BSU As String = ""

        sqlGetOpen_BSU = " SELECT     VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
" FROM  vw_ACADEMICYEAR_D INNER JOIN   VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
" WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_BSU, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetTERM_ACD_YR(ByVal BSU_ID As String, ByVal ACD_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim sqlGetOpen_TRM As String = ""
        sqlGetOpen_TRM = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID='" & ACD_ID & "' AND TRM_BSU_ID='" & BSU_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_TRM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetGrade_ACD_YR(ByVal BSU_ID As String, ByVal ACD_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim sqlGetOpen_GRM As String = ""
        sqlGetOpen_GRM = "SELECT     VW_GRADE_M.GRD_ID, VW_GRADE_M.GRD_DISPLAY " _
                    & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' and VW_GRADE_BSU_M.GRM_BSU_ID='" & BSU_ID & "' " _
                    & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_GRM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetSUBJECT_ACD_YR(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim sqlGetOpen_SBG As String = ""
        sqlGetOpen_SBG = "SELECT SBG_ID, SBG_DESCR " _
              & " FROM " _
              & " SUBJECTS_GRADE_S WHERE SBG_BSU_ID='" & BSU_ID & "' and SBG_ACD_ID='" & ACD_ID & "' AND SBG_GRD_ID='" & GRD_ID & "'"
        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_SBG, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function GetSyllabus_ACD_YR(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal TRM_ID As String, ByVal BSU_ID As String) As SqlDataReader

        'Author(--arun.g)
        'Date   --31/march/2009--created
        'Purpose--Get the syllabus for the active acadamic year

        Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
        Dim sqlGetOpen_SYM As String = ""
        sqlGetOpen_SYM = "SELECT SYM_ID, SYM_DESCR FROM SYL.SYLLABUS_M WHERE (SYM_ACD_ID = '" & ACD_ID & "') AND (SYM_TRM_ID = '" & TRM_ID & "') AND (SYM_GRD_ID = '" & GRD_ID & "')and (SYM_BSU_ID='" & BSU_ID & "')"

        Dim command As SqlCommand = New SqlCommand(sqlGetOpen_SYM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

End Class
