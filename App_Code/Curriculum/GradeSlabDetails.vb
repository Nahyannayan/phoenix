Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Namespace CURRICULUM

    ''' <summary>
    ''' The class Created for handling Grade Slab Master
    ''' 
    ''' Author : SHIJIN C A
    ''' Date : 25-Mar-2009
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Class GRADESLABDETAILS

        ''' <summary>
        ''' Variables used to Store the 
        ''' </summary>
        ''' <remarks></remarks>
        Dim vGSD_GSM_SLAB_ID As Integer
        Dim vGSD_ID As Integer
        Dim vGSD_DESC As String
        Dim vGSD_MIN_MARK As Double
        Dim vGSD_MAX_MARK As Double
        Dim vSUB_DETALS() As GRADESLABDETAILS
        Dim bDelete As Boolean
        Dim bEdit As Boolean

        Public Property NEWLY_ADDED() As Boolean
            Get
                Return bEdit
            End Get
            Set(ByVal value As Boolean)
                bEdit = value
            End Set
        End Property

        Public Property Delete() As Boolean
            Get
                Return bDelete
            End Get
            Set(ByVal value As Boolean)
                bDelete = value
            End Set
        End Property

        Public Property SUB_DETAILS() As GRADESLABDETAILS()
            Get
                Return vSUB_DETALS
            End Get
            Set(ByVal value As GRADESLABDETAILS())
                vSUB_DETALS = value
            End Set
        End Property

        Public Property GSD_ID() As Integer
            Get
                Return vGSD_ID
            End Get
            Set(ByVal value As Integer)
                vGSD_ID = value
            End Set
        End Property

        Public Property GSD_GSM_SLAB_ID() As Integer
            Get
                Return vGSD_GSM_SLAB_ID
            End Get
            Set(ByVal value As Integer)
                vGSD_GSM_SLAB_ID = value
            End Set
        End Property

        Public Property GSD_DESC() As String
            Get
                Return vGSD_DESC
            End Get
            Set(ByVal value As String)
                vGSD_DESC = value
            End Set
        End Property
 
        Public Property MIN_MARK() As Double
            Get
                Return vGSD_MIN_MARK
            End Get
            Set(ByVal value As Double)
                vGSD_MIN_MARK = value
            End Set
        End Property

        Public Property MAX_MARK() As Double
            Get
                Return vGSD_MAX_MARK
            End Get
            Set(ByVal value As Double)
                vGSD_MAX_MARK = value
            End Set
        End Property

        Public Shared Function GetNextID() As Integer
            Dim strSQL As String = "SELECT ISNULL(MAX(GSD_ID),0)+1 FROM ACT.GRADING_SLAB_D"
            Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, strSQL)
        End Function

        Public Shared Function PopulateGradeSlabMaster(ByVal vBSU_ID As String) As DataTable
            Dim strSQL As String = "SELECT GSM_DESC, CAST(GSM_SLAB_ID AS varchar)  + '__' + CAST(GSM_TOT_MARK AS varchar) AS SLB_MARK " & _
            " FROM ACT.GRADING_SLAB_M WHERE GSM_BSU_ID = '" & vBSU_ID & "'"
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, strSQL).Tables(0)
        End Function

        Public Shared Function GETGRADESLABDETAILS(ByVal vGSD_GSM_SLAB_ID As Integer) As Hashtable
            Dim str_Sql As String = String.Empty
            Dim tempvGRD_SLB_DET As New GRADESLABDETAILS
            Dim htGRDSLAB As New Hashtable
            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                str_Sql = "SELECT GSD_ID, GSD_GSM_SLAB_ID, GSD_MIN_MARK, GSD_MAX_MARK, GSD_DESC " & _
                " FROM ACT.GRADING_SLAB_D where GSD_GSM_SLAB_ID = " & vGSD_GSM_SLAB_ID
                Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
                While (dReader.Read())
                    tempvGRD_SLB_DET = New GRADESLABDETAILS
                    tempvGRD_SLB_DET.GSD_ID = dReader("GSD_ID")
                    tempvGRD_SLB_DET.GSD_GSM_SLAB_ID = dReader("GSD_GSM_SLAB_ID")
                    tempvGRD_SLB_DET.GSD_DESC = dReader("GSD_DESC")
                    tempvGRD_SLB_DET.MIN_MARK = dReader("GSD_MIN_MARK")
                    tempvGRD_SLB_DET.MAX_MARK = dReader("GSD_MAX_MARK")
                    htGRDSLAB(dReader("GSD_ID")) = tempvGRD_SLB_DET
                End While
            End Using
            Return htGRDSLAB
        End Function

        Public Shared Function GetSubDetailsAsDataTable(ByVal htDetails As Hashtable) As DataTable
            If htDetails Is Nothing Then
                Return Nothing
            End If
            Dim vGRD_SLB_DET As GRADESLABDETAILS
            Dim ienum As IDictionaryEnumerator = htDetails.GetEnumerator
            Dim dt As DataTable = CreateDataTable()
            Dim dr As DataRow
            While (ienum.MoveNext())
                vGRD_SLB_DET = ienum.Value
                If vGRD_SLB_DET.bDelete Then Continue While
                dr = dt.NewRow
                dr("GSD_ID") = vGRD_SLB_DET.GSD_ID
                dr("GSD_DESC") = vGRD_SLB_DET.GSD_DESC
                dr("GSD_MIN_MARK") = vGRD_SLB_DET.MIN_MARK
                dr("GSD_MAX_MARK") = vGRD_SLB_DET.MAX_MARK
                dt.Rows.Add(dr)
            End While
            Return dt
        End Function

        Private Shared Function CreateDataTable() As DataTable
            Dim dtDt As New DataTable
            Try
                Dim cGSD_ID As New DataColumn("GSD_ID", System.Type.GetType("System.Int32"))
                Dim cGSD_MIN_MARK As New DataColumn("GSD_MIN_MARK", System.Type.GetType("System.String"))
                Dim cGSD_MAX_MARK As New DataColumn("GSD_MAX_MARK", System.Type.GetType("System.String"))
                Dim cGSD_DESC As New DataColumn("GSD_DESC", System.Type.GetType("System.String"))

                dtDt.Columns.Add(cGSD_ID)
                dtDt.Columns.Add(cGSD_MIN_MARK)
                dtDt.Columns.Add(cGSD_MAX_MARK)
                dtDt.Columns.Add(cGSD_DESC)
                Return dtDt
            Catch ex As Exception
                Return dtDt
            End Try
            Return Nothing
        End Function

        Public Shared Function SaveDetails(ByVal vHTGRD_SLB_DET As Hashtable, ByVal SLAB_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

            If vHTGRD_SLB_DET Is Nothing Then
                Return -1
            End If
            Dim iReturnvalue As Integer
            Dim cmd As SqlCommand
            '@FCM_BSU_ID	varchar(20),
            '@FCM_DESCR	varchar(50),
            '@userID varchar(20),
            Dim vSLB_DET As GRADESLABDETAILS
            Dim ienum As IDictionaryEnumerator = vHTGRD_SLB_DET.GetEnumerator
            While (ienum.MoveNext())
                vSLB_DET = ienum.Value
                cmd = New SqlCommand("ACT.[SaveGRADING_SLAB_D]", conn, trans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpGSM_BSU_ID As New SqlParameter("@GSD_GSM_SLAB_ID", SqlDbType.Int)
                sqlpGSM_BSU_ID.Value = SLAB_ID
                cmd.Parameters.Add(sqlpGSM_BSU_ID)

                Dim sqlpGSD_DESC As New SqlParameter("@GSD_DESC", SqlDbType.VarChar)
                sqlpGSD_DESC.Value = vSLB_DET.GSD_DESC
                cmd.Parameters.Add(sqlpGSD_DESC)

                Dim sqlpGSD_MIN_MARK As New SqlParameter("@GSD_MIN_MARK", SqlDbType.Decimal)
                sqlpGSD_MIN_MARK.Value = vSLB_DET.MIN_MARK
                cmd.Parameters.Add(sqlpGSD_MIN_MARK)

                Dim sqlpGSD_MAX_MARK As New SqlParameter("@GSD_MAX_MARK", SqlDbType.Decimal)
                sqlpGSD_MAX_MARK.Value = vSLB_DET.MAX_MARK
                cmd.Parameters.Add(sqlpGSD_MAX_MARK)


                Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
                sqlpbDelete.Value = vSLB_DET.Delete
                cmd.Parameters.Add(sqlpbDelete)

                If vSLB_DET.NEWLY_ADDED = False And vSLB_DET.GSD_ID <> 0 Then

                    Dim sqlpGSD_ID As New SqlParameter("@GSD_ID", SqlDbType.BigInt)
                    sqlpGSD_ID.Value = vSLB_DET.GSD_ID
                    cmd.Parameters.Add(sqlpGSD_ID)
                End If

                Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retSValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retSValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retSValParam.Value
                If iReturnvalue <> 0 Then Return iReturnvalue
            End While
            Return iReturnvalue

        End Function

        Public Shared Function GetDetails(ByVal vGSM_SLAB_ID As Integer) As GRADESLABMASTER
            Dim cmd As SqlCommand
            Dim vGRADE_SLAB As New GRADESLABMASTER
            Dim str_sql As String
            Dim conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            str_sql = "SELECT GSM_SLAB_ID, GSM_DESC," & _
            " GSM_BSU_ID, GSM_TOT_MARK FROM ACT.GRADING_SLAB_M " & _
            " WHERE GSM_SLAB_ID = " & vGSM_SLAB_ID
            cmd = New SqlCommand(str_sql, conn)
            cmd.CommandType = CommandType.Text
            Try
                conn.Close()
                conn.Open()
                Dim sqlReader As SqlDataReader = cmd.ExecuteReader()
                While (sqlReader.Read())
                    vGRADE_SLAB.BSU_ID = sqlReader("GSM_BSU_ID")
                    vGRADE_SLAB.GRADE_SLAB_DESCR = sqlReader("GSM_DESC")
                    vGRADE_SLAB.TOTAL_MARK = sqlReader("GSM_TOT_MARK")
                    vGRADE_SLAB.GRADE_SLAB_ID = sqlReader("GSM_SLAB_ID")
                    Exit While
                End While
            Catch ex As Exception
                Return Nothing
            Finally
                conn.Close()
            End Try
            Return vGRADE_SLAB
        End Function
    End Class

End Namespace