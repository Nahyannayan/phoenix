Imports Microsoft.VisualBasic

Namespace CURRICULUM

    Public NotInheritable Class CURR_CONSTANTS

#Region "CURRICULUM MENUS"
        Public Const MNU_GRADESLAB_MASTER As String = "C100125"
        Public Const MNU_GRADESLAB_DETAILS As String = "C300025"
        Public Const MNU_ACTIVITY_SCHEDULE As String = "C312005"
        Public Const MNU_PROCESSING_REPORT As String = "C330008"
        Public Const MNU_PROCESSING_REPORT_BACKEND As String = "C330007"
#End Region

#Region "CURRICULUM REPORT MENUS"
        Public Const MNU_RPT_MONTHLY_PROGRESS_REPORT As String = "C300005"
        Public Const MNU_RPT_ASSESSMENT_BY_TEACHER As String = "C300015"
        Public Const MNU_RPT_SUBJECTLIST_BY_TEACHER As String = "C300035"
        Public Const MNU_RPT_SUBJECTLIST_BY_STUDENT As String = "C300045"
        Public Const MNU_RPT_SUBJECTLIST_BY_GRADE As String = "C300055"
        Public Const MNU_RPT_SUBJECTLIST_BY_DEPARTMENT As String = "C300065"
        Public Const MNU_RPT_FORMTUTOR_COMMENTS As String = "C300009"
#End Region

    End Class

End Namespace