Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CURRICULUM

Namespace SKILLSCHEDULE

    ''' <summary>
    ''' The class Created for handling Skill Schedule Derived From 'AS'
    ''' 
    ''' Author : FACTS
    ''' Date : 25-Mar-2018
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Class SKILLSCHEDULE


#Region "  Variables Defination "

        Dim vACD_ID As Integer
        Dim vTRM_ID As Integer
        Dim vCAD_ID As Integer
        Dim vGRD_IDs As Hashtable
        'SUBJECT MASTER ID
        Dim vSBM_ID As Integer
        Dim vSBM_DESCR As String
        Dim vSGR_IDs As Hashtable
        'GRADE SLAB ID
        Dim vGSD_ID As Integer
        Dim vCAS_ID As Integer
        Dim vPARENT_CAS_ID As Integer
        Dim vACT_DATE As Date
        Dim vACT_TO_DATE As Date
        Dim vACT_TIME As DateTime
        Dim vMIN_MARK As Double
        Dim vMAX_MARK As Double
        Dim vMAX_MARK_AOL_KU As Double
        Dim vMAX_MARK_AOL_APP As Double
        Dim vMAX_MARK_AOL_COMM As Double
        Dim vMAX_MARK_AOL_HOTS As Double
        Dim vMAX_MARK_AOL_WS As Double
        Dim vDURATION As Double
        Dim vCAS_DESCR As String
        Dim vCAS_REMARKS As String
        Dim vbATT_COMPULSARY As Boolean
        Dim vbHasAOLMARKS As Boolean
        Dim vbWithoutSkills As Boolean
        Dim vbSkills As Boolean
        Dim vbAUTO_ALLOC As Boolean
        Dim vCORE_EXT As String
        Dim bDelete As Boolean
        Dim vSkills As Hashtable
        Dim vCountSkills As Integer
        Dim vSKL_IDs As DataTable
        Dim vSKILL_IDs As Hashtable
        Dim vSkillsMaxMarksDt As DataTable
        Dim vSKILLIDs As String
        Dim vGrade As String
#End Region


#Region "  Property Defination "
        Public Property Delete() As Boolean
            Get
                Return bDelete
            End Get
            Set(ByVal value As Boolean)
                bDelete = value
            End Set
        End Property

        Public Property HasAOLMARKS() As Boolean
            Get
                Return vbHasAOLMARKS
            End Get
            Set(ByVal value As Boolean)
                vbHasAOLMARKS = value
            End Set
        End Property

        Public Property WithoutSkills() As Boolean
            Get
                Return vbWithoutSkills
            End Get
            Set(ByVal value As Boolean)
                vbWithoutSkills = value
            End Set
        End Property
        Public Property WithSkills() As Boolean
            Get
                Return vbSkills
            End Get
            Set(ByVal value As Boolean)
                vbSkills = value
            End Set
        End Property

        Public Property GRADE_SLAB_ID() As Integer
            Get
                Return vGSD_ID
            End Get
            Set(ByVal value As Integer)
                vGSD_ID = value
            End Set
        End Property

        Public Property PARENT_ID() As Integer
            Get
                Return vPARENT_CAS_ID
            End Get
            Set(ByVal value As Integer)
                vPARENT_CAS_ID = value
            End Set
        End Property

        Public Property ACD_ID() As Integer
            Get
                Return vACD_ID
            End Get
            Set(ByVal value As Integer)
                vACD_ID = value
            End Set
        End Property

        Public Property TRM_ID() As Integer
            Get
                Return vTRM_ID
            End Get
            Set(ByVal value As Integer)
                vTRM_ID = value
            End Set
        End Property

        Public Property ACTIVITY_ID() As Integer
            Get
                Return vCAD_ID
            End Get
            Set(ByVal value As Integer)
                vCAD_ID = value
            End Set
        End Property

        Public Property GRADE_ID() As Hashtable
            Get
                Return vGRD_IDs
            End Get
            Set(ByVal value As Hashtable)
                vGRD_IDs = value
            End Set
        End Property

        Public Property SUBJECT_ID() As Integer
            Get
                Return vSBM_ID
            End Get
            Set(ByVal value As Integer)
                vSBM_ID = value
            End Set
        End Property

        Public Property SUBJECT() As String
            Get
                Return vSBM_DESCR
            End Get
            Set(ByVal value As String)
                vSBM_DESCR = value
            End Set
        End Property
        Public Property SKILLIDs() As String
            Get
                Return vSKILLIDs
            End Get
            Set(ByVal value As String)
                vSKILLIDs = value
            End Set
        End Property

        Public Property SUBJECT_GROUP_ID() As Hashtable
            Get
                Return vSGR_IDs
            End Get
            Set(ByVal value As Hashtable)
                vSGR_IDs = value
            End Set
        End Property

        Public Property ACTIVTY_DATE() As Date
            Get
                Return vACT_DATE
            End Get
            Set(ByVal value As Date)
                vACT_DATE = value
            End Set
        End Property

        Public Property ACTIVTY_TO_DATE() As Date
            Get
                Return vACT_TO_DATE
            End Get
            Set(ByVal value As Date)
                vACT_TO_DATE = value
            End Set
        End Property

        Public Property ACTIVITY_TIME() As DateTime
            Get
                Return vACT_TIME
            End Get
            Set(ByVal value As DateTime)
                vACT_TIME = value
            End Set
        End Property

        Public Property DURATION() As Double
            Get
                Return vDURATION
            End Get
            Set(ByVal value As Double)
                vDURATION = value
            End Set
        End Property

        Public Property MIN_MARK() As Double
            Get
                Return vMIN_MARK
            End Get
            Set(ByVal value As Double)
                vMIN_MARK = value
            End Set
        End Property

        Public Property MAX_MARK() As Double
            Get
                Return vMAX_MARK
            End Get
            Set(ByVal value As Double)
                vMAX_MARK = value
            End Set
        End Property

        Public Property MAX_MARK_AOL_KU() As Double
            Get
                Return vMAX_MARK_AOL_KU
            End Get
            Set(ByVal value As Double)
                vMAX_MARK_AOL_KU = value
            End Set
        End Property
        Public Property MAX_MARK_AOL_APP() As Double
            Get
                Return vMAX_MARK_AOL_APP
            End Get
            Set(ByVal value As Double)
                vMAX_MARK_AOL_APP = value
            End Set
        End Property
        Public Property MAX_MARK_AOL_COMM() As Double
            Get
                Return vMAX_MARK_AOL_COMM
            End Get
            Set(ByVal value As Double)
                vMAX_MARK_AOL_COMM = value
            End Set
        End Property
        Public Property MAX_MARK_AOL_HOTS() As Double
            Get
                Return vMAX_MARK_AOL_HOTS
            End Get
            Set(ByVal value As Double)
                vMAX_MARK_AOL_HOTS = value
            End Set
        End Property
        Public Property MAX_MARK_AOL_WITHOUTSKILLS() As Double
            Get
                Return vMAX_MARK_AOL_WS
            End Get
            Set(ByVal value As Double)
                vMAX_MARK_AOL_WS = value
            End Set
        End Property
        Public Property DESCRIPTION() As String
            Get
                Return vCAS_DESCR
            End Get
            Set(ByVal value As String)
                vCAS_DESCR = value
            End Set
        End Property
        Public Property Grade() As String
            Get
                Return vGrade
            End Get
            Set(ByVal value As String)
                vGrade = value
            End Set
        End Property
        Public Property REMARKS() As String
            Get
                Return vCAS_REMARKS
            End Get
            Set(ByVal value As String)
                vCAS_REMARKS = value
            End Set
        End Property
        Public Property CAS_ID() As Integer
            Get
                Return vCAS_ID
            End Get
            Set(ByVal value As Integer)
                vCAS_ID = value
            End Set
        End Property

        Public Property bATTEND_COMPULSORY() As Boolean
            Get
                Return vbATT_COMPULSARY
            End Get
            Set(ByVal value As Boolean)
                vbATT_COMPULSARY = value
            End Set
        End Property

        Public Property bAUTOALLOCATE() As Boolean
            Get
                Return vbAUTO_ALLOC
            End Get
            Set(ByVal value As Boolean)
                vbAUTO_ALLOC = value
            End Set
        End Property

        Public Property TYPE_LEVEL() As String
            Get
                Return vCORE_EXT
            End Get
            Set(ByVal value As String)
                vCORE_EXT = value
            End Set
        End Property

        Public Property Skills() As Hashtable
            Get
                Return vSkills
            End Get
            Set(ByVal value As Hashtable)
                vSkills = value
            End Set
        End Property

        Public Property CountSkills() As Integer
            Get
                Return vCountSkills
            End Get
            Set(ByVal value As Integer)
                vCountSkills = value
            End Set
        End Property
        Public Property SKL_IDs() As DataTable
            Get
                Return vSKL_IDs
            End Get
            Set(ByVal value As DataTable)
                vSKL_IDs = value
            End Set
        End Property
        Public Property SKILL_IDs() As Hashtable
            Get
                Return vSKILL_IDs
            End Get
            Set(ByVal value As Hashtable)
                vSKILL_IDs = value
            End Set
        End Property
        Public Property SkillsMaxMarksDt() As DataTable
            Get
                Return vSkillsMaxMarksDt
            End Get
            Set(ByVal value As DataTable)
                vSkillsMaxMarksDt = value
            End Set
        End Property
#End Region


#Region "  Custom Shared Functions "
        Public Shared Function GetScheduleDetails(ByVal pGRDID As String, ByVal pSBG_ID As String, ByVal pCAD_ID As String) As SKILLSCHEDULE()
            Dim vACT_SCH() As SKILLSCHEDULE
            Dim str_sql As String = "SELECT CAS_ID FROM ACT.ACTIVITY_SCHEDULE INNER JOIN" & _
            " GROUPS_M ON ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID = GROUPS_M.SGR_ID  " & _
            " where ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID =" & pCAD_ID & _
            " AND GROUPS_M.SGR_GRD_ID = '" & pGRDID & "' AND ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID = " & pSBG_ID
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
            Dim i As Integer = 0
            While (dReader.Read())
                ReDim Preserve vACT_SCH(i)
                vACT_SCH(i) = GetScheduleDetails(dReader("CAS_ID"))
                i = i + 1
            End While
            Return vACT_SCH
        End Function

        Public Shared Function GetScheduleDetails(ByVal vVIEW_ID As Integer) As SKILLSCHEDULE
            Dim GetSkillColumn = String.Empty
            Dim str_sql As String = "SELECT CAS_ID, CAS_CAD_ID, CAS_SGR_ID, CAS_SBG_ID, CAS_DATE, CAS_TO_DATE," & _
            " CAS_TIME, CAS_DUR_OF_EXAM, CAS_MIN_MARK, CAS_MAX_MARK, CAS_DESC, " & _
            " CAS_REMARK, CAS_bPROCESSED, CAS_COUNT_ATTEMPT, " & _
            " CAS_bATT_ENTERED, CAS_bATT_MANDATORY, CAS_PARENT_ID, " & _
            " CAS_GSM_SLAB_ID, CAS_TYPE_LEVEL, CAD_ACD_ID, CAD_TRM_ID, " & _
            " SBG_DESCR, SGR_GRD_ID, SBG_SBM_ID , " & _
            " ISNULL(CAS_bHasAOLEXAM,0) CAS_bHasAOLEXAM ,ISNULL(CAS_bWITHOUTSKILLS,0) CAS_bWITHOUTSKILLS, ISNULL(CAD_bSKILLS,0) CAS_bSKILLS, " & _
            " ISNULL(CAS_AOL_KU_MAXMARK, 0) CAS_AOL_KU_MAXMARK, ISNULL(CAS_AOL_APP_MAXMARK,0) CAS_AOL_APP_MAXMARK, " & _
            " ISNULL(CAS_AOL_COMM_MAXMARK,0) CAS_AOL_COMM_MAXMARK, ISNULL(CAS_AOL_HOTS_MAXMARK, 0) CAS_AOL_HOTS_MAXMARK, " & _
            " ISNULL(CAS_AOL_SKILL1_MAXMARK,0) CAS_AOL_SKILL1_MAXMARK, ISNULL(CAS_AOL_SKILL2_MAXMARK, 0) CAS_AOL_SKILL2_MAXMARK, " & _
            " ISNULL(CAS_AOL_SKILL3_MAXMARK,0) CAS_AOL_SKILL3_MAXMARK, ISNULL(CAS_AOL_SKILL4_MAXMARK, 0) CAS_AOL_SKILL4_MAXMARK, " & _
            " ISNULL(CAS_AOL_SKILL5_MAXMARK,0) CAS_AOL_SKILL5_MAXMARK, ISNULL(CAS_AOL_SKILL6_MAXMARK, 0) CAS_AOL_SKILL6_MAXMARK, " & _
            " ISNULL(CAS_AOL_SKILL7_MAXMARK,0) CAS_AOL_SKILL7_MAXMARK, ISNULL(CAS_AOL_SKILL8_MAXMARK, 0) CAS_AOL_SKILL8_MAXMARK, " & _
            " ISNULL(CAS_AOL_SKILL9_MAXMARK,0) CAS_AOL_SKILL9_MAXMARK, ISNULL(CAS_AOL_SKILL10_MAXMARK, 0) CAS_AOL_SKILL10_MAXMARK, " & _
            " ISNULL(CAS_AOL_WITHOUTSKILLS_MAXMARK, 0) CAS_AOL_WITHOUTSKILLS_MAXMARK, CAS_SKL_ID " & _
            " FROM ACT.ACTIVITY_SCHEDULE INNER JOIN ACT.ACTIVITY_D " & _
            " ON ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID = ACT.ACTIVITY_D.CAD_ID " & _
            " INNER JOIN SUBJECTS_GRADE_S ON ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " & _
            " INNER JOIN GROUPS_M ON ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID = GROUPS_M.SGR_ID " & _
            " WHERE CAS_ID =" & vVIEW_ID
            Dim vACT_DETAILS As New SKILLSCHEDULE
            vACT_DETAILS.SkillsMaxMarksDt = New DataTable()
            vACT_DETAILS.SkillsMaxMarksDt.Columns.Add("MaxMarks", Type.GetType("System.Double"))
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
            While (drReader.Read())
                vACT_DETAILS.CAS_ID = drReader("CAS_ID")
                vACT_DETAILS.ACD_ID = drReader("CAD_ACD_ID")
                vACT_DETAILS.TRM_ID = drReader("CAD_TRM_ID")
                vACT_DETAILS.ACTIVITY_ID = drReader("CAS_CAD_ID")
                vACT_DETAILS.GRADE_ID = GETGRADE_FROM_GROUP(drReader("SGR_GRD_ID"))
                vACT_DETAILS.SUBJECT_ID = drReader("CAS_SBG_ID")
                vACT_DETAILS.SUBJECT = drReader("SBG_DESCR")
                vACT_DETAILS.SUBJECT_GROUP_ID = GETGRADE_FROM_GROUP(drReader("CAS_SGR_ID"))
                vACT_DETAILS.ACTIVTY_DATE = drReader("CAS_DATE")
                vACT_DETAILS.ACTIVTY_TO_DATE = drReader("CAS_TO_DATE")
                vACT_DETAILS.ACTIVITY_TIME = drReader("CAS_TIME")
                vACT_DETAILS.DURATION = drReader("CAS_DUR_OF_EXAM")
                vACT_DETAILS.GRADE_SLAB_ID = drReader("CAS_GSM_SLAB_ID")
                vACT_DETAILS.TYPE_LEVEL = drReader("CAS_TYPE_LEVEL")
                vACT_DETAILS.MIN_MARK = drReader("CAS_MIN_MARK")
                vACT_DETAILS.MAX_MARK = drReader("CAS_MAX_MARK")
                vACT_DETAILS.DESCRIPTION = drReader("CAS_DESC")
                vACT_DETAILS.REMARKS = drReader("CAS_REMARK")
                vACT_DETAILS.bATTEND_COMPULSORY = drReader("CAS_bATT_MANDATORY")
                'vACT_DETAILS.bAUTOALLOCATE = drReader("CAS_bATT_MANDATORY")
                vACT_DETAILS.vPARENT_CAS_ID = drReader("CAS_PARENT_ID")
                vACT_DETAILS.HasAOLMARKS = drReader("CAS_bHasAOLEXAM")
                vACT_DETAILS.MAX_MARK_AOL_KU = drReader("CAS_AOL_KU_MAXMARK")
                vACT_DETAILS.MAX_MARK_AOL_APP = drReader("CAS_AOL_APP_MAXMARK")
                vACT_DETAILS.MAX_MARK_AOL_COMM = drReader("CAS_AOL_COMM_MAXMARK")
                vACT_DETAILS.MAX_MARK_AOL_HOTS = drReader("CAS_AOL_HOTS_MAXMARK")
                vACT_DETAILS.WithoutSkills = drReader("CAS_bWITHOUTSKILLS")
                vACT_DETAILS.WithSkills = drReader("CAS_bSKILLS")
                vACT_DETAILS.MAX_MARK_AOL_WITHOUTSKILLS = drReader("CAS_AOL_WITHOUTSKILLS_MAXMARK")
                '            vACT_DETAILS.SkillsMaxMarksDt.Rows(0).Item(0) = drReader("CAS_AOL_SKILL1_MAXMARK")
                For i As Integer = 0 To 9
                    GetSkillColumn = "CAS_AOL_SKILL" + CType(i + 1, String) + "_MAXMARK"
                    Dim dr As DataRow = vACT_DETAILS.SkillsMaxMarksDt.NewRow
                    dr("MaxMarks") = drReader(GetSkillColumn)
                    vACT_DETAILS.SkillsMaxMarksDt.Rows.Add(dr)
                Next
                vACT_DETAILS.SKILLIDs = drReader("CAS_SKL_ID")
                vACT_DETAILS.Grade = drReader("SGR_GRD_ID")
                Exit While
            End While
            Return vACT_DETAILS
        End Function

        Private Shared Function GETGRADE_FROM_GROUP(ByVal GRP_ID As String) As Hashtable
            Dim ht As New Hashtable
            ht(GRP_ID) = GRP_ID
            Return ht
        End Function

        Private Shared Function GETSUBJECTID_FROM_SUBJECTGROUP(ByVal GRP_ID As Integer) As Integer
            Return GRP_ID
        End Function
        Public Shared Function SaveDetails(ByVal vACT_SCH_VAL() As SKILLSCHEDULE, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
            Dim retVal As Integer = 0
            For Each vACT_SCH As SKILLSCHEDULE In vACT_SCH_VAL
                retVal = SaveDetails(vACT_SCH, conn, trans)
                If retVal <> 0 Then
                    Exit For
                End If
            Next
            Return retVal
        End Function

        Public Shared Function SaveDetails(ByVal vACT_SCH As SKILLSCHEDULE, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

            If vACT_SCH Is Nothing Then
                Return -1
            End If
            Dim iReturnvalue As Integer
            Dim cmd As SqlCommand
            '@FCM_BSU_ID	varchar(20),
            '@FCM_DESCR	varchar(50),
            '@userID varchar(20),

            cmd = New SqlCommand("[ACT].[SaveSKILL_SCHEDULE]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpCAS_CAD_ID As New SqlParameter("@CAS_CAD_ID", SqlDbType.Int)
            sqlpCAS_CAD_ID.Value = vACT_SCH.vCAD_ID
            cmd.Parameters.Add(sqlpCAS_CAD_ID)

            Dim sqlpCAS_SGR_ID As New SqlParameter("@CAS_SGR_ID", SqlDbType.VarChar)
            sqlpCAS_SGR_ID.Value = GET_SGR_IDs(vACT_SCH.vSGR_IDs)
            cmd.Parameters.Add(sqlpCAS_SGR_ID)

            Dim sqlpCAS_SBG_ID As New SqlParameter("@CAS_SBG_ID", SqlDbType.Int)
            sqlpCAS_SBG_ID.Value = vACT_SCH.vSBM_ID
            cmd.Parameters.Add(sqlpCAS_SBG_ID)

            Dim sqlpCAS_TIME As New SqlParameter("@CAS_TIME", SqlDbType.DateTime)
            sqlpCAS_TIME.Value = vACT_SCH.vACT_TIME
            cmd.Parameters.Add(sqlpCAS_TIME)

            Dim sqlpCAS_DATE As New SqlParameter("@CAS_DATE", SqlDbType.DateTime)
            sqlpCAS_DATE.Value = vACT_SCH.vACT_DATE
            cmd.Parameters.Add(sqlpCAS_DATE)

            Dim sqlpCAS_TO_DATE As New SqlParameter("@CAS_TO_DATE", SqlDbType.DateTime)
            sqlpCAS_TO_DATE.Value = vACT_SCH.vACT_TO_DATE
            cmd.Parameters.Add(sqlpCAS_TO_DATE)

            Dim sqlpCAS_DUR_OF_EXAM As New SqlParameter("@CAS_DUR_OF_EXAM", SqlDbType.Decimal)
            sqlpCAS_DUR_OF_EXAM.Value = vACT_SCH.vDURATION
            cmd.Parameters.Add(sqlpCAS_DUR_OF_EXAM)

            Dim sqlpCAS_MIN_MARK As New SqlParameter("@CAS_MIN_MARK", SqlDbType.Decimal)
            sqlpCAS_MIN_MARK.Value = vACT_SCH.vMIN_MARK
            cmd.Parameters.Add(sqlpCAS_MIN_MARK)

            Dim sqlpCAS_MAX_MARK As New SqlParameter("@CAS_MAX_MARK", SqlDbType.Decimal)
            sqlpCAS_MAX_MARK.Value = vACT_SCH.vMAX_MARK
            cmd.Parameters.Add(sqlpCAS_MAX_MARK)

            Dim sqlpCAS_bHasAOLEXAM As New SqlParameter("@CAS_bHasAOLEXAM", SqlDbType.Bit)
            sqlpCAS_bHasAOLEXAM.Value = vACT_SCH.HasAOLMARKS
            cmd.Parameters.Add(sqlpCAS_bHasAOLEXAM)
            '------------------------------------------------------------------------------------DYNAMIC---------------------------------------------------------------------------
            Dim sqlpSkill = String.Empty
            For i As Integer = 0 To 9
                sqlpSkill = "@CAS_AOL_SKILL" + CType(i + 1, String) + "_MAXMARK"
                cmd.Parameters.AddWithValue(sqlpSkill, IIf(vACT_SCH.vSkills.Count - 1 >= i, IIf(vACT_SCH.Skills(i) = "", DBNull.Value, vACT_SCH.Skills(i)), DBNull.Value))
            Next

            Dim str = String.Empty
            Dim dt As DataTable = HttpContext.Current.Session("SkillIds")
            'For i As Integer = 0 To vACT_SCH.vSkills.Count - 1
            For i As Integer = 0 To dt.Rows.Count - 1
                str += CType(dt.Rows(i).Item(1), String) + "|"
            Next
            cmd.Parameters.AddWithValue("@CAS_SKL_ID", str.TrimEnd(CChar("|")))

            '---------------------------------------------------------------------END DYNAMIC---------------------------------------------------------------------------

            Dim sqlpCAS_DESC As New SqlParameter("@CAS_DESC", SqlDbType.VarChar)
            sqlpCAS_DESC.Value = vACT_SCH.vCAS_DESCR
            cmd.Parameters.Add(sqlpCAS_DESC)

            Dim sqlpCAS_REMARK As New SqlParameter("@CAS_REMARK", SqlDbType.VarChar)
            sqlpCAS_REMARK.Value = vACT_SCH.vCAS_REMARKS
            cmd.Parameters.Add(sqlpCAS_REMARK)

            'Dim sqlpCAS_bMARKENTERED As New SqlParameter("@CAS_bMARKENTERED", SqlDbType.Bit)
            'sqlpCAS_bMARKENTERED.Value = False
            'cmd.Parameters.Add(sqlpCAS_bMARKENTERED)

            Dim sqlpCAS_bPROCESSED As New SqlParameter("@CAS_bPROCESSED", SqlDbType.Bit)
            sqlpCAS_bPROCESSED.Value = False
            cmd.Parameters.Add(sqlpCAS_bPROCESSED)

            Dim sqlpCAS_COUNT_ATTEMPT As New SqlParameter("@CAS_COUNT_ATTEMPT", SqlDbType.Int)
            sqlpCAS_COUNT_ATTEMPT.Value = 0
            cmd.Parameters.Add(sqlpCAS_COUNT_ATTEMPT)

            Dim sqlpCAS_bATT_ENTERED As New SqlParameter("@CAS_bATT_ENTERED", SqlDbType.Bit)
            sqlpCAS_bATT_ENTERED.Value = False
            cmd.Parameters.Add(sqlpCAS_bATT_ENTERED)

            Dim sqlpCAS_bATT_MANDATORY As New SqlParameter("@CAS_bATT_MANDATORY", SqlDbType.Bit)
            sqlpCAS_bATT_MANDATORY.Value = vACT_SCH.vbATT_COMPULSARY
            cmd.Parameters.Add(sqlpCAS_bATT_MANDATORY)

            Dim sqlpbAUTOALLOCATE As New SqlParameter("@bAUTOALLOCATE", SqlDbType.Bit)
            sqlpbAUTOALLOCATE.Value = vACT_SCH.vbAUTO_ALLOC
            cmd.Parameters.Add(sqlpbAUTOALLOCATE)

            Dim sqlpCAS_PARENT_ID As New SqlParameter("@CAS_PARENT_ID", SqlDbType.BigInt)
            sqlpCAS_PARENT_ID.Value = vACT_SCH.vPARENT_CAS_ID
            cmd.Parameters.Add(sqlpCAS_PARENT_ID)

            Dim sqlpCAS_GSM_SLAB_ID As New SqlParameter("@CAS_GSM_SLAB_ID", SqlDbType.BigInt)
            sqlpCAS_GSM_SLAB_ID.Value = vACT_SCH.GRADE_SLAB_ID
            cmd.Parameters.Add(sqlpCAS_GSM_SLAB_ID)

            Dim sqlpCAS_TYPE_LEVEL As New SqlParameter("@CAS_TYPE_LEVEL", SqlDbType.VarChar)
            sqlpCAS_TYPE_LEVEL.Value = vACT_SCH.vCORE_EXT
            cmd.Parameters.Add(sqlpCAS_TYPE_LEVEL)

            Dim sqlpCAS_bWITHOUTSKILLS As New SqlParameter("@CAS_bWITHOUTSKILLS", SqlDbType.Bit)
            sqlpCAS_bWITHOUTSKILLS.Value = vACT_SCH.WithoutSkills
            cmd.Parameters.Add(sqlpCAS_bWITHOUTSKILLS)


            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = vACT_SCH.Delete
            cmd.Parameters.Add(sqlpbDelete)

            Dim sqlpCAS_AOL_WS_MAXMARK As New SqlParameter("@CAS_AOL_WITHOUTSKILLS_MAXMARK", SqlDbType.Decimal)
            sqlpCAS_AOL_WS_MAXMARK.Value = vACT_SCH.MAX_MARK_AOL_WITHOUTSKILLS
            cmd.Parameters.Add(sqlpCAS_AOL_WS_MAXMARK)


            If vACT_SCH.CAS_ID <> 0 Then
                Dim sqlpCAS_ID As New SqlParameter("@CAS_ID", SqlDbType.BigInt)
                sqlpCAS_ID.Value = vACT_SCH.CAS_ID
                cmd.Parameters.Add(sqlpCAS_ID)
            End If

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            Return iReturnvalue
        End Function

        Public Shared Function SaveGLGDATA(ByVal vACT_SCH As SKILLSCHEDULE, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

            If vACT_SCH Is Nothing Then
                Return -1
            End If
            Dim iReturnvalue As Integer
            Dim cmd As SqlCommand
            '@FCM_BSU_ID	varchar(20),
            '@FCM_DESCR	varchar(50),
            '@userID varchar(20),

            cmd = New SqlCommand("ACT.[SaveACTIVITY_SCHEDULE]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpCAS_CAD_ID As New SqlParameter("@CAS_CAD_ID", SqlDbType.Int)
            sqlpCAS_CAD_ID.Value = vACT_SCH.vCAD_ID
            cmd.Parameters.Add(sqlpCAS_CAD_ID)

            Dim sqlpCAS_SGR_ID As New SqlParameter("@CAS_SGR_ID", SqlDbType.VarChar)
            sqlpCAS_SGR_ID.Value = GET_SGR_IDs(vACT_SCH.vSGR_IDs)
            cmd.Parameters.Add(sqlpCAS_SGR_ID)

            Dim sqlpCAS_SBG_ID As New SqlParameter("@CAS_SBG_ID", SqlDbType.Int)
            sqlpCAS_SBG_ID.Value = vACT_SCH.vSBM_ID
            cmd.Parameters.Add(sqlpCAS_SBG_ID)

            Dim sqlpCAS_TIME As New SqlParameter("@CAS_TIME", SqlDbType.DateTime)
            sqlpCAS_TIME.Value = vACT_SCH.vACT_TIME
            cmd.Parameters.Add(sqlpCAS_TIME)

            Dim sqlpCAS_DATE As New SqlParameter("@CAS_DATE", SqlDbType.DateTime)
            sqlpCAS_DATE.Value = vACT_SCH.vACT_DATE
            cmd.Parameters.Add(sqlpCAS_DATE)

            Dim sqlpCAS_TO_DATE As New SqlParameter("@CAS_TO_DATE", SqlDbType.DateTime)
            sqlpCAS_TO_DATE.Value = vACT_SCH.vACT_TO_DATE
            cmd.Parameters.Add(sqlpCAS_TO_DATE)

            Dim sqlpCAS_DUR_OF_EXAM As New SqlParameter("@CAS_DUR_OF_EXAM", SqlDbType.Decimal)
            sqlpCAS_DUR_OF_EXAM.Value = vACT_SCH.vDURATION
            cmd.Parameters.Add(sqlpCAS_DUR_OF_EXAM)

            Dim sqlpCAS_MIN_MARK As New SqlParameter("@CAS_MIN_MARK", SqlDbType.Decimal)
            sqlpCAS_MIN_MARK.Value = vACT_SCH.vMIN_MARK
            cmd.Parameters.Add(sqlpCAS_MIN_MARK)

            Dim sqlpCAS_MAX_MARK As New SqlParameter("@CAS_MAX_MARK", SqlDbType.Decimal)
            sqlpCAS_MAX_MARK.Value = vACT_SCH.vMAX_MARK
            cmd.Parameters.Add(sqlpCAS_MAX_MARK)

            Dim sqlpCAS_DESC As New SqlParameter("@CAS_DESC", SqlDbType.VarChar)
            sqlpCAS_DESC.Value = vACT_SCH.vCAS_DESCR
            cmd.Parameters.Add(sqlpCAS_DESC)

            Dim sqlpCAS_REMARK As New SqlParameter("@CAS_REMARK", SqlDbType.VarChar)
            sqlpCAS_REMARK.Value = vACT_SCH.vCAS_REMARKS
            cmd.Parameters.Add(sqlpCAS_REMARK)

            'Dim sqlpCAS_bMARKENTERED As New SqlParameter("@CAS_bMARKENTERED", SqlDbType.Bit)
            'sqlpCAS_bMARKENTERED.Value = False
            'cmd.Parameters.Add(sqlpCAS_bMARKENTERED)

            Dim sqlpCAS_bPROCESSED As New SqlParameter("@CAS_bPROCESSED", SqlDbType.Bit)
            sqlpCAS_bPROCESSED.Value = False
            cmd.Parameters.Add(sqlpCAS_bPROCESSED)

            Dim sqlpCAS_COUNT_ATTEMPT As New SqlParameter("@CAS_COUNT_ATTEMPT", SqlDbType.Int)
            sqlpCAS_COUNT_ATTEMPT.Value = 0
            cmd.Parameters.Add(sqlpCAS_COUNT_ATTEMPT)

            Dim sqlpCAS_bATT_ENTERED As New SqlParameter("@CAS_bATT_ENTERED", SqlDbType.Bit)
            sqlpCAS_bATT_ENTERED.Value = False
            cmd.Parameters.Add(sqlpCAS_bATT_ENTERED)

            Dim sqlpCAS_bATT_MANDATORY As New SqlParameter("@CAS_bATT_MANDATORY", SqlDbType.Bit)
            sqlpCAS_bATT_MANDATORY.Value = vACT_SCH.vbATT_COMPULSARY
            cmd.Parameters.Add(sqlpCAS_bATT_MANDATORY)

            Dim sqlpbAUTOALLOCATE As New SqlParameter("@bAUTOALLOCATE", SqlDbType.Bit)
            sqlpbAUTOALLOCATE.Value = vACT_SCH.vbAUTO_ALLOC
            cmd.Parameters.Add(sqlpbAUTOALLOCATE)

            Dim sqlpCAS_PARENT_ID As New SqlParameter("@CAS_PARENT_ID", SqlDbType.BigInt)
            sqlpCAS_PARENT_ID.Value = vACT_SCH.vPARENT_CAS_ID
            cmd.Parameters.Add(sqlpCAS_PARENT_ID)

            Dim sqlpCAS_GSM_SLAB_ID As New SqlParameter("@CAS_GSM_SLAB_ID", SqlDbType.BigInt)
            sqlpCAS_GSM_SLAB_ID.Value = vACT_SCH.GRADE_SLAB_ID
            cmd.Parameters.Add(sqlpCAS_GSM_SLAB_ID)

            Dim sqlpCAS_TYPE_LEVEL As New SqlParameter("@CAS_TYPE_LEVEL", SqlDbType.VarChar)
            sqlpCAS_TYPE_LEVEL.Value = vACT_SCH.vCORE_EXT
            cmd.Parameters.Add(sqlpCAS_TYPE_LEVEL)

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = vACT_SCH.Delete
            cmd.Parameters.Add(sqlpbDelete)

            If vACT_SCH.CAS_ID <> 0 Then
                Dim sqlpCAS_ID As New SqlParameter("@CAS_ID", SqlDbType.BigInt)
                sqlpCAS_ID.Value = vACT_SCH.CAS_ID
                cmd.Parameters.Add(sqlpCAS_ID)
            End If

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception

            End Try

            iReturnvalue = retSValParam.Value
            Return iReturnvalue
        End Function

        Public Shared Function DeleteBulkDetails(ByVal pParent_id As String, ByVal pCAD_ID As String, _
        ByVal pGRD_ID As String, ByVal pSBG_ID As String, ByVal pUSR_NAME As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
            Dim str_sql As String = " SELECT ISNULL(CAS_ID, '') CAS_ID FROM ACT.ACTIVITY_SCHEDULE INNER JOIN " & _
            " GROUPS_M ON ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID = GROUPS_M.SGR_ID " & _
            " WHERE CAS_CAD_ID =" & pCAD_ID & " AND CAS_SBG_ID = " & pSBG_ID & _
            " AND CAS_PARENT_ID = " & pParent_id & " AND SGR_GRD_ID = '" & pGRD_ID & "'"

            Dim iReturnvalue As Integer
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CURRICULUMConnectionString, _
             CommandType.Text, str_sql)
            While (drReader.Read())
                iReturnvalue = DeleteDetails(drReader("CAS_ID"), pUSR_NAME, conn, trans)
                If iReturnvalue <> 0 Then Exit While
            End While
            'If pCAS_ID = "" Then
            '    Return -1
            'End If
            Return iReturnvalue
        End Function

        Public Shared Function DeleteDetails(ByVal pCAS_ID As String, ByVal pUSR_NAME As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

            If pCAS_ID = "" Then
                Return -1
            End If
            Dim iReturnvalue As Integer
            Dim cmd As SqlCommand

            cmd = New SqlCommand("ACT.[DELETEACTIVITY_SCHEDULE]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpCAS_ID As New SqlParameter("@CAS_ID", SqlDbType.Int)
            sqlpCAS_ID.Value = pCAS_ID
            cmd.Parameters.Add(sqlpCAS_ID)

            Dim sqlpUSR_NAME As New SqlParameter("@USR_NAME", SqlDbType.VarChar)
            sqlpUSR_NAME.Value = pUSR_NAME
            cmd.Parameters.Add(sqlpUSR_NAME)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            Return iReturnvalue
        End Function

        Private Shared Function GET_SGR_IDs(ByVal htTab As Hashtable) As String
            Dim ienum As IDictionaryEnumerator = htTab.GetEnumerator
            Dim strSGR_IDs As String = String.Empty
            Dim strSeperator As String = String.Empty
            While ienum.MoveNext
                strSGR_IDs += strSeperator & ienum.Value
                strSeperator = "|"
            End While
            Return strSGR_IDs
        End Function

        Public Shared Function GetDetails(ByVal vGSM_SLAB_ID As Integer) As GRADESLABMASTER
            Dim cmd As SqlCommand
            Dim vGRADE_SLAB As New GRADESLABMASTER
            Dim str_sql As String
            Dim conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            str_sql = "SELECT GSM_SLAB_ID, GSM_DESC," & _
            " GSM_BSU_ID, GSM_TOT_MARK FROM ACT.GRADING_SLAB_M " & _
            " WHERE GSM_SLAB_ID = " & vGSM_SLAB_ID
            cmd = New SqlCommand(str_sql, conn)
            cmd.CommandType = CommandType.Text
            Try
                conn.Close()
                conn.Open()
                Dim sqlReader As SqlDataReader = cmd.ExecuteReader()
                While (sqlReader.Read())
                    vGRADE_SLAB.BSU_ID = sqlReader("GSM_BSU_ID")
                    vGRADE_SLAB.GRADE_SLAB_DESCR = sqlReader("GSM_DESC")
                    vGRADE_SLAB.TOTAL_MARK = sqlReader("GSM_TOT_MARK")
                    vGRADE_SLAB.GRADE_SLAB_ID = sqlReader("GSM_SLAB_ID")
                    Exit While
                End While
            Catch ex As Exception
                Return Nothing
            Finally
                conn.Close()
            End Try
            Return vGRADE_SLAB
        End Function

        Public Shared Function GetGradeSlab(ByVal vBSU_ID As String) As DataSet
            Dim str_sql As String = String.Empty
            str_sql = "SELECT GSM_DESC , convert(varchar,GSM_SLAB_ID)+ '___' + convert(varchar,GSM_TOT_MARK) GSM_SLAB_ID " & _
            " FROM ACT.GRADING_SLAB_M WHERE GSM_BSU_ID ='" & vBSU_ID & "' AND ISNULL(GSM_bDISPLAY,'TRUE')='TRUE'"
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function HasPeriod(ByVal vCAM_ID As String) As Boolean
            Dim str_sql As String = String.Empty
            str_sql = "SELECT ISNULL(CAM_bHasPeriod, 0) FROM " & _
            " ACT.ACTIVITY_D INNER JOIN ACT.ACTIVITY_M ON ACT.ACTIVITY_D.CAD_CAM_ID = ACT.ACTIVITY_M.CAM_ID" & _
            " WHERE CAD_ID =" & vCAM_ID
            Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetActivityDetails(ByVal vACD_ID As String, ByVal vTRM_ID As String, Optional ByVal vCAM_ID As String = "") As DataSet
            Dim str_sql As String = String.Empty
            str_sql = "SELECT CAD_ID, CAD_DESC FROM ACT.ACTIVITY_D " & _
            " WHERE CAD_ACD_ID= '" & vACD_ID & "' AND  CAD_TRM_ID=" & vTRM_ID
            If vCAM_ID <> "" Then
                str_sql += " AND CAD_CAM_ID = " & vCAM_ID
            End If
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetAssessmentType(ByVal pCAD_ID As String) As ASSESSMENT_TYPE
            Dim str_sql As String = String.Empty
            str_sql = "SELECT  ISNULL( ACT.ACTIVITY_M.CAM_bHasPeriod, 0) CAM_bHasPeriod," & _
            " ISNULL(ACT.ACTIVITY_M.CAM_bHASTIME_DURATION, 0) CAM_bHASTIME_DURATION " & _
            " FROM ACT.ACTIVITY_D INNER JOIN ACT.ACTIVITY_M ON " & _
            " ACT.ACTIVITY_D.CAD_CAM_ID = ACT.ACTIVITY_M.CAM_ID " & _
            " WHERE CAD_ID= '" & pCAD_ID & "'"
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
            While (dr.Read())
                If dr("CAM_bHasPeriod") = True Then
                    Return ASSESSMENT_TYPE.HASPERIODS
                ElseIf dr("CAM_bHASTIME_DURATION") = True Then
                    Return ASSESSMENT_TYPE.HASTIME_DURATION
                Else
                    Return ASSESSMENT_TYPE.NO_TIME_DURATION
                End If
            End While
        End Function

        Public Shared Function GetGRADE(ByVal vBSU_ID As String, ByVal vACD_ID As Integer) As DataSet
            Dim str_sql As String = String.Empty
            str_sql = "select DISTINCT GRM_GRD_ID, GRM_DISPLAY, GRD_DISPLAYORDER from " & _
            " VW_GRADE_BSU_M INNER JOIN VW_GRADE_M " & _
            " ON GRM_GRD_ID = GRD_ID WHERE GRM_BSU_ID = '" & _
            vBSU_ID & "'" & " AND GRM_ACD_ID = " & vACD_ID & _
            " ORDER BY GRD_DISPLAYORDER "
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetSubjectGroups(ByVal vBSU_ID As String, ByVal vACD_ID As Integer, ByVal vGRD_ID As String, ByVal vSBG_ID As String, ByVal vCURR_SUPERUSER As String, ByVal vEMP_ID As String) As DataSet
            Dim str_sql As String = String.Empty
            'str_sql = "SELECT SGR_ID,SGR_DESCR from GROUPS_M WHERE SGR_BSU_ID = '" & vBSU_ID & "'" & _
            '"AND SGR_SBG_ID = " & vSBG_ID & " AND SGR_ACD_ID = " & vACD_ID & " ORDER BY SGR_DESCR"  '& " AND SGR_GRD_ID = '" & vGRD_ID & "'"

            If (vEMP_ID <> "") And (vCURR_SUPERUSER <> "Y") Then
                str_sql = "SELECT  DISTINCT SGR_ID, SGR_DESCR,  SBM_DESCR DESCR1 FROM GROUPS_M " & _
                  " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
                  " WHERE SGR_BSU_ID='" & vBSU_ID & "' AND (SGS_TODATE IS NULL) AND SGR_ACD_ID='" & vACD_ID & "' AND SGR_SBG_ID = '" & vSBG_ID.ToString + "'"
                str_sql += " AND SGS_EMP_ID=" & vEMP_ID & ""
            Else
                str_sql = "SELECT SGR_ID,SGR_DESCR from GROUPS_M WHERE SGR_BSU_ID = '" & vBSU_ID & "'" & _
               "AND SGR_SBG_ID = '" & vSBG_ID & "' AND SGR_ACD_ID = " & vACD_ID & " ORDER BY SGR_DESCR"  '& " AND SGR_GRD_ID = '" & vGRD_ID & "'"
            End If

            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function FillMaxMarkDetails(ByVal vGRADE_SLAB_ID As String, ByVal vBSU_ID As String) As String
            Dim str_sql As String = String.Empty
            str_sql = "select CAST(GSM_TOT_MARK AS decimal(8,2)) from ACT.GRADING_SLAB_M where GSM_SLAB_ID = '" & vGRADE_SLAB_ID & "' AND GSM_BSU_ID = '" & vBSU_ID & "'"
            Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

#End Region

    End Class

End Namespace