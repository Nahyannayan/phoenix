Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Namespace CURRICULUM

    Public Class ACTIVITYMASTER
#Region " All SAVE(Update/Insert) function"
        Public Shared Function SaveREPORT_STUDENT_PROCESS_BACKEND(ByVal ACD_ID As String, ByVal GRD_IDS As String, ByVal RPF_ID As String, _
     ByVal RSD_IDS As String, ByVal TRM_ID As String, ByVal RSM_ID As String, ByVal trans As SqlTransaction) As Integer
            Try

                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@GRD_IDS", GRD_IDS)
                pParms(2) = New SqlClient.SqlParameter("@RPF_ID", RPF_ID)
                pParms(3) = New SqlClient.SqlParameter("@RSD_IDS", RSD_IDS)
                pParms(4) = New SqlClient.SqlParameter("@TRM_ID", TRM_ID)
                pParms(5) = New SqlClient.SqlParameter("@RSM_ID", RSM_ID)
                pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(6).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[RPT].[SaveREPORT_STUDENT_PROCESS_BACKEND]", pParms)
                Dim ReturnFlag As Integer = pParms(6).Value
                Return ReturnFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "SaveREPORT_STUDENT_PROCESS_BACKEND")
                Return 1
            End Try
        End Function

        Public Shared Function SAVEACTIVITY_M(ByVal CAM_ID As String, ByVal CAM_DESC As String, ByVal CAM_BSU_ID As String, ByVal bHasPeriod As Boolean, ByVal bHasTime_Duration As Boolean, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CAM_ID", CAM_ID)
            pParms(1) = New SqlClient.SqlParameter("@CAM_DESC", CAM_DESC)
            pParms(2) = New SqlClient.SqlParameter("@CAM_bHASPERIOD", bHasPeriod)
            pParms(3) = New SqlClient.SqlParameter("@CAM_bHASTIME_DURATION", bHasTime_Duration)
            pParms(4) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(5) = New SqlClient.SqlParameter("@CAM_BSU_ID", CAM_BSU_ID)
            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[ACT].[SaveACTIVITY_M]", pParms)
            Dim ReturnFlag As Integer = pParms(6).Value
            Return ReturnFlag

        End Function

        Public Shared Function SAVEACTIVITY_D(ByVal ACT_XML As String, ByVal USR_ID As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
            Try
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACT_XML", ACT_XML)
                pParms(1) = New SqlClient.SqlParameter("@USR_ID", USR_ID)
                pParms(2) = New SqlClient.SqlParameter("@bEdit", bEdit)
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[ACT].[SaveACTIVITY_D]", pParms)
                Dim ReturnFlag As Integer = pParms(3).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1
            End Try
        End Function

        Public Shared Function SaveREPORT_STUDENT_S(ByVal RPT_XML As String, ByVal RPF_ID As String, ByVal ACD_ID As String, _
       ByVal GRD_ID As String, ByVal SGR_ID As String, ByVal SBG_ID As String, ByVal RSD_ID As String, ByVal trans As SqlTransaction) As Integer
            Try


                Dim pParms(7) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RPT_XML", RPT_XML)
                pParms(1) = New SqlClient.SqlParameter("@RPF_ID", RPF_ID)
                pParms(2) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(3) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@SGR_ID", SGR_ID)
                pParms(5) = New SqlClient.SqlParameter("@SBG_ID", SBG_ID)
                pParms(6) = New SqlClient.SqlParameter("@RSD_ID", RSD_ID)
                pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(7).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[RPT].[SaveREPORT_STUDENT_PROCESS]", pParms)
                Dim ReturnFlag As Integer = pParms(7).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1
            End Try
        End Function
        Public Shared Function SaveSTUDENT_ACTIVITY(ByVal STU_IDs As String, ByVal STA_ACD_ID As String, ByVal STA_SGR_ID As String, ByVal STA_SBG_ID As String, ByVal STA_CAS_ID As String, ByRef STU_NO As String, ByVal transaction As SqlTransaction) As Integer
            'Author(--Lijo)
            'Date   --03/Nov/2008
            'Purpose--To save LOT_ALLOTMENT data
            Try

                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_IDs", STU_IDs)
                pParms(1) = New SqlClient.SqlParameter("@STA_ACD_ID", STA_ACD_ID)
                pParms(2) = New SqlClient.SqlParameter("@STA_SGR_ID", STA_SGR_ID)
                pParms(3) = New SqlClient.SqlParameter("@STA_SBG_ID", STA_SBG_ID)
                pParms(4) = New SqlClient.SqlParameter("@STA_CAS_ID", STA_CAS_ID)
                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue
                pParms(6) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 900)
                pParms(6).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ACT.SaveSTUDENT_ACTIVITY", pParms)
                Dim ReturnFlag As Integer = pParms(5).Value
                If ReturnFlag = 0 Then
                    STU_NO = IIf(TypeOf (pParms(6).Value) Is DBNull, String.Empty, pParms(6).Value)
                End If
                Return ReturnFlag

            Catch ex As Exception
                Return 1
            End Try
        End Function
#End Region
#Region " All Select function"
        Public Shared Function GetACTIVITY_D(Optional ByVal CAD_ID As String = "") As SqlDataReader
            'Author(--Lijo)
            'Date   --02/Jun/2008
            'Purpose--Get Student ID Detail
            Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            Dim sqlACTIVITY_D As String = ""

            sqlACTIVITY_D = "SELECT   CAD_CAM_ID, CAD_ACD_ID, CAD_TRM_ID, CAD_DESC, ISNULL(CAD_bAOL,0) CAD_bAOL,ISNULL(CAD_bWITHOUTSKILLS,0) CAD_bWITHOUTSKILLS,ISNULL(CAD_bSKILLS,0) CAD_bSKILLS FROM ACT.ACTIVITY_D WHERE CAD_ID = '" & CAD_ID & "'"


            Dim command As SqlCommand = New SqlCommand(sqlACTIVITY_D, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
            SqlConnection.ClearPool(connection)
            Return reader

        End Function

        Public Shared Function GetACTIVITY_M(ByVal vBSU_ID As String, Optional ByVal CAM_ID As String = "") As SqlDataReader
            'Author(--Lijo)
            'Date   --02/Jun/2008
            'Purpose--Get Student ID Detail
            Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            Dim sqlACTIVITY_M As String = ""

            sqlACTIVITY_M = "SELECT CAM_ID, CAM_DESC, isnull(CAM_bHASPERIOD,0) CAM_bHASPERIOD, " & _
            "isnull(CAM_bHASTIME_DURATION,0) CAM_bHASTIME_DURATION FROM ACT.ACTIVITY_M " & _
            " WHERE CAM_BSU_ID = '" & vBSU_ID & "' "
            If CAM_ID <> "" Then
                sqlACTIVITY_M += " AND CAM_ID='" & CAM_ID & "'"
            End If

            Dim command As SqlCommand = New SqlCommand(sqlACTIVITY_M, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
            SqlConnection.ClearPool(connection)
            Return reader

        End Function


        Public Shared Function GetACT_SCHEDULE(ByVal CAS_ID As String) As SqlDataReader
            'Author(--Lijo)
            'Date   --02/Jun/2008
            'Purpose--Get Student ID Detail
            Dim connection As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            Dim sqlStud_ACT As String = ""

            sqlStud_ACT = " SELECT * FROM(SELECT     ACT.ACTIVITY_SCHEDULE.CAS_ID, ACT.ACTIVITY_D.CAD_TRM_ID AS TRM_ID, ACT.ACTIVITY_D.CAD_ACD_ID AS ACD_ID, ACT.ACTIVITY_D.CAD_DESC, " & _
                      " GROUPS_M.SGR_DESCR AS SGR_DESC, VW_GRADE_BSU_M.GRM_DESCR AS GRD_DESC, SUBJECTS_GRADE_S.SBG_DESCR AS SBG_DESC, " & _
                      " ACT.ACTIVITY_SCHEDULE.CAS_TYPE_LEVEL AS TYPE_LEVEL, VW_ACADEMICYEAR_M.ACY_DESCR AS ACY_DESC , VW_TRM_M.TRM_DESCRIPTION AS TRM_DESC, " & _
                      " ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID, ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID FROM  ACT.ACTIVITY_D INNER JOIN " & _
                      " ACT.ACTIVITY_SCHEDULE ON ACT.ACTIVITY_D.CAD_ID = ACT.ACTIVITY_SCHEDULE.CAS_CAD_ID INNER JOIN " & _
                      " GROUPS_M ON ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID = GROUPS_M.SGR_ID INNER JOIN " & _
                      " VW_GRADE_BSU_M ON GROUPS_M.SGR_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID AND " & _
                      " GROUPS_M.SGR_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                      " VW_TRM_M ON ACT.ACTIVITY_D.CAD_TRM_ID = VW_TRM_M.TRM_ID AND ACT.ACTIVITY_D.CAD_ACD_ID = VW_TRM_M.TRM_ACD_ID INNER JOIN " & _
                      " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
                      " SUBJECTS_GRADE_S ON ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID = SUBJECTS_GRADE_S.SBG_ID INNER JOIN " & _
                      " vw_ACADEMICYEAR_D ON ACT.ACTIVITY_D.CAD_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
                      " VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID)A WHERE  a.CAS_ID='" & CAS_ID & "'"

            Dim command As SqlCommand = New SqlCommand(sqlStud_ACT, connection)
            command.CommandType = CommandType.Text
            Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
            SqlConnection.ClearPool(connection)
            Return reader


        End Function
#End Region
#Region " All Delete function"

#End Region
    End Class
End Namespace