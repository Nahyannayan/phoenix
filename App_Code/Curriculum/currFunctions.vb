Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Public Class currFunctions
    Function PopulateGroupsByTeacher(ByVal emp_id As String, ByVal ddlGroup As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID " _
                                & " WHERE SGR_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                & " AND SGS_TODATE IS NULL"
        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID=" + sbg_id
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + grd_id + "'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        Return ddlGroup
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "")
        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If grd_id <> "" Then
            str_query += " AND SBG_GRD_ID='" + grd_id + "'"

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateActivityMaster(ByVal ddlActivity As DropDownList, ByVal BSUID As String)
        ddlActivity.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CAM_ID,CAM_DESC FROM ACT.ACTIVITY_M WHERE CAM_BSU_ID='" + BSUID + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "CAM_DESC"
        ddlActivity.DataValueField = "CAM_ID"
        ddlActivity.DataBind()
        Return ddlActivity
    End Function

    Function PopulateGrades(ByVal ddlGrade As DropDownList)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_ID,GRD_DISPLY FROM VW.GRADE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        Return ddlGrade
    End Function

    Public Function PopulateGradeSubjects(ByVal ddlSubjects As DropDownList, ByVal ddlGrade As DropDownList, ByVal sBsuId As String)
        ddlSubjects.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "" ' "SELECT SBG_ID,SBG_DESCR FROM VW.SUBJECTS_GRADE_S WHERE SBG_BSU_ID='" & sBsuId & "'"

        str_query = " SELECT SBG_ID AS ID, " & _
                    " GRD_DISPLAY DESCR1,  SBM_DESCR DESCR2 FROM SUBJECTS_GRADE_S " & _
                    " INNER JOIN SUBJECT_M ON SBG_SBM_ID = SBM_ID INNER JOIN VW_GRADE_M ON SBG_GRD_ID = GRD_ID AND GRD_ID='" & ddlGrade.SelectedValue & "' "
        str_query += " ORDER BY GRD_DISPLAY "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubjects.DataSource = ds
        ddlSubjects.DataTextField = "DESCR2"
        ddlSubjects.DataValueField = "ID"
        ddlSubjects.DataBind()
        Return ddlSubjects
    End Function
    Public Function PopulateGradeSubjects(ByVal ddlSubjects As CheckBoxList, ByVal ddlGrade As DropDownList, ByVal sBsuId As String)
        ddlSubjects.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "" ' "SELECT SBG_ID,SBG_DESCR FROM VW.SUBJECTS_GRADE_S WHERE SBG_BSU_ID='" & sBsuId & "'"

        str_query = " SELECT DISTINCT(SBG_ID) AS ID, " & _
                    " GRD_DISPLAY DESCR1,  SBM_DESCR DESCR2 FROM SUBJECTS_GRADE_S " & _
                    " INNER JOIN SUBJECT_M ON SBG_SBM_ID = SBM_ID INNER JOIN VW_GRADE_M ON SBG_GRD_ID = GRD_ID AND GRD_ID='" & ddlGrade.SelectedValue & "' AND SBG_BSU_ID='" & sBsuId & "'"
        str_query += " ORDER BY GRD_DISPLAY "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubjects.DataSource = ds
        ddlSubjects.DataTextField = "DESCR2"
        ddlSubjects.DataValueField = "ID"
        ddlSubjects.DataBind()
        Return ddlSubjects
    End Function
End Class
