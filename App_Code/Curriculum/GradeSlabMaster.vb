Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Namespace CURRICULUM

    ''' <summary>
    ''' The class Created for handling Grade Slab Master
    ''' 
    ''' Author : SHIJIN C A
    ''' Date : 25-Mar-2009
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Class GRADESLABMASTER

        ''' <summary>
        ''' Variables used to Store the 
        ''' </summary>
        ''' <remarks></remarks>
        Dim vGSM_SLAB_ID As Integer
        Dim vGSM_DESC As String
        Dim vGSM_BSU_ID As String
        Dim vGSM_TOT_MARK As Double
        Dim vBSU_NAME As String
        Dim bDelete As Boolean

        Public Property Delete() As Boolean
            Get
                Return bDelete
            End Get
            Set(ByVal value As Boolean)
                bDelete = value
            End Set
        End Property

        Public Property GRADE_SLAB_ID() As Integer
            Get
                Return vGSM_SLAB_ID
            End Get
            Set(ByVal value As Integer)
                vGSM_SLAB_ID = value
            End Set
        End Property

        Public Property GRADE_SLAB_DESCR() As String
            Get
                Return vGSM_DESC
            End Get
            Set(ByVal value As String)
                vGSM_DESC = value
            End Set
        End Property

        Public Property BSU_ID() As String
            Get
                Return vGSM_BSU_ID
            End Get
            Set(ByVal value As String)
                vGSM_BSU_ID = value
            End Set
        End Property

        Public Property BSU_NAME() As String
            Get
                Return vBSU_NAME
            End Get
            Set(ByVal value As String)
                vBSU_NAME = value
            End Set
        End Property

        Public Property TOTAL_MARK() As Double
            Get
                Return vGSM_TOT_MARK
            End Get
            Set(ByVal value As Double)
                vGSM_TOT_MARK = value
            End Set
        End Property

        Public Shared Function SaveDetails(ByVal vGRADE_SLAB As GRADESLABMASTER, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

            If vGRADE_SLAB Is Nothing Then
                Return -1
            End If
            Dim iReturnvalue As Integer
            Dim cmd As SqlCommand
            '@FCM_BSU_ID	varchar(20),
            '@FCM_DESCR	varchar(50),
            '@userID varchar(20),

            cmd = New SqlCommand("ACT.[SaveGRADING_SLAB_M]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpGSM_BSU_ID As New SqlParameter("@GSM_BSU_ID", SqlDbType.VarChar, 20)
            sqlpGSM_BSU_ID.Value = vGRADE_SLAB.BSU_ID
            cmd.Parameters.Add(sqlpGSM_BSU_ID)

            Dim sqlpGSM_DESC As New SqlParameter("@GSM_DESC", SqlDbType.VarChar)
            sqlpGSM_DESC.Value = vGRADE_SLAB.GRADE_SLAB_DESCR
            cmd.Parameters.Add(sqlpGSM_DESC)

            Dim sqlpGSM_TOT_MARK As New SqlParameter("@GSM_TOT_MARK", SqlDbType.Decimal)
            sqlpGSM_TOT_MARK.Value = vGRADE_SLAB.TOTAL_MARK
            cmd.Parameters.Add(sqlpGSM_TOT_MARK)


            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = vGRADE_SLAB.Delete
            cmd.Parameters.Add(sqlpbDelete)

            If vGRADE_SLAB.GRADE_SLAB_ID <> 0 Then

                Dim sqlpGSM_SLAB_ID As New SqlParameter("@GSM_SLAB_ID", SqlDbType.BigInt)
                sqlpGSM_SLAB_ID.Value = vGRADE_SLAB.GRADE_SLAB_ID
                cmd.Parameters.Add(sqlpGSM_SLAB_ID)
            End If

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
        End Function

        Public Shared Function GetDetails(ByVal vGSM_SLAB_ID As Integer) As GRADESLABMASTER
            Dim cmd As SqlCommand
            Dim vGRADE_SLAB As New GRADESLABMASTER
            Dim str_sql As String
            Dim conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            str_sql = "SELECT GSM_SLAB_ID, GSM_DESC," & _
            " GSM_BSU_ID, GSM_TOT_MARK FROM ACT.GRADING_SLAB_M " & _
            " WHERE GSM_SLAB_ID = " & vGSM_SLAB_ID
            cmd = New SqlCommand(str_sql, conn)
            cmd.CommandType = CommandType.Text
            Try
                conn.Close()
                conn.Open()
                Dim sqlReader As SqlDataReader = cmd.ExecuteReader()
                While (sqlReader.Read())
                    vGRADE_SLAB.BSU_ID = sqlReader("GSM_BSU_ID")
                    vGRADE_SLAB.GRADE_SLAB_DESCR = sqlReader("GSM_DESC")
                    vGRADE_SLAB.TOTAL_MARK = sqlReader("GSM_TOT_MARK")
                    vGRADE_SLAB.GRADE_SLAB_ID = sqlReader("GSM_SLAB_ID")
                    Exit While
                End While
            Catch ex As Exception
                Return Nothing
            Finally
                conn.Close()
            End Try
            Return vGRADE_SLAB
        End Function
    End Class

End Namespace