Imports Microsoft.VisualBasic
'Version        Author          Date             Change  
'1.1            Swapna          29/mar/2011      LEAVESETTLED  added in XMLTYPE
Public Enum DateType
    VoucherDate = 0
    ChequeDate = 1
End Enum

Public Enum Status
    ALL = 0
    Open = 1
    Posted = 2
    Deleted = 3
End Enum

Public Enum ProcessType
    DAYEND = 0
    MONTHEND = 1
    YEAREND = 2
End Enum

Public Enum XMLType
    BSUName
    ACTName
    EMPName
    AMOUNT
    AIRFARE
    LEAVESETTLED 'V1.1
    STUDENT
    ConcessionType
    Grade
    Bus
    CheckList
End Enum

Public Enum ReminderType
    FIRST = 1
    SECOND = 2
    THIRD = 3
End Enum

Public Enum AccountFilter

    BANK = 1
    CASHONLY = 2
    NORMAL = 3
    PARTY1 = 4
    PARTY2 = 5
    DEBIT = 7
    DEBIT_D = 8
    CHQISSAC = 9
    INTRAC = 10
    ACRDAC = 11
    PREPDAC = 12
    CHQISSAC_PDC = 13
    CUSTSUPP = 14
    CUSTSUPPnIJV = 15
    NOTCC = 16
    INCOME = 17
    CHARGE = 18
    CONCESSION = 19
    FEEDISC = 20
End Enum

Public Enum STUDENTTYPE
    STUDENT = 1
    ENQUIRY = 2
End Enum

Public Enum SELECTEDPROFORMADURATION
    TERMS = 1
    MONTHS = 0
End Enum

Public Enum ConcessionType
    Sibling = 1
    Staff = 2
    Management = 3
    Merit = 4
    Scholarships = 5
    Discount = 6
End Enum

Public Enum COLLECTIONTYPE
    CASH = 1
    CHEQUES = 2
    CREDIT_CARD = 3
    OTHER_MODES = 4
    INTERNET = 5
    VOUCHER_REDEMPTION = 6
    REWARDS_REDEMPTION = 7
End Enum

Public Enum PayrollFilterType
    DEPARTMENT = 1
    CATEGORY = 2
    DESIGNATION = 3
End Enum

Public Enum OASISPhotoType
    STUDENT_PHOTO = 1
    EMPLOYEE_PHOTO = 2
End Enum

Public Enum ASSESSMENT_TYPE
    HASPERIODS
    HASTIME_DURATION
    NO_TIME_DURATION
    NORMAL
End Enum



Public Enum ASSET_TRANSFER_TYPE
    PERMANENT_TRANSFER_OUT
    PERMANENT_TRANSFER_IN
    TEMPORARY_LEASE_OUT
    TEMPORARY_LEASE_IN
    SENDTOSUPPLIER
    RECEIVEFROMSUPPLIER
    RECEIVETOSTORE
    ALLOCATE
    RECEIVEAFTERLEASE
End Enum
