Imports System
Imports System.Drawing 
Imports System.IO 

''' <summary> 
''' Summary description for ImageCreationLibrary 
''' </summary> 
Public Class ImageCreationLibrary
    Private _baseImagePath As String
    Public Property BaseImagePath() As String
        Get
            Return Me._baseImagePath
        End Get
        Set(ByVal value As String)
            Me._baseImagePath = value
        End Set
    End Property

    Private _graphics As Graphics

    Public Sub New(ByVal stBasePath As String)
        Me._baseImagePath = stBasePath
        Dim i As System.Drawing.Image = DirectCast(New Bitmap(1000, 1000), Image)
        _graphics = Graphics.FromImage(i)
    End Sub

    Public Function CreateVerticalTextImage(ByVal stText As String, ByVal f As Font) As String
        ' Dim stFileName As String = Toolbox.GetMD5String(stText)
        Dim stFileName As String

        If checkUpper(stText) = True Then
            stFileName = stText.Replace("/", "_") + "_UPPERCASE"
        Else
            stFileName = stText.Replace("/", "_")
        End If

        Dim stFullFilePath As String = Me._baseImagePath + stFileName & ".jpg"
        If Not File.Exists(stFullFilePath) Then
            Dim stringFormat As New StringFormat()
            stringFormat.FormatFlags = StringFormatFlags.DirectionVertical

            Dim imageSize As SizeF = _graphics.MeasureString(stText, f, 25, stringFormat)
            Dim i As System.Drawing.Image = DirectCast(New Bitmap(CInt(imageSize.Width), CInt(imageSize.Height)), Image)
            Dim g As Graphics = Graphics.FromImage(i)
            g.FillRectangle(Brushes.White, 0, 0, i.Width, i.Height)

            'flip the image 180 degrees 
            g.TranslateTransform(i.Width, i.Height)
            g.RotateTransform(180.0F)

            g.DrawString(stText, f, Brushes.Black, 0, 0, stringFormat)

            i.Save(stFullFilePath, System.Drawing.Imaging.ImageFormat.Jpeg)
        End If
        Return stFullFilePath
    End Function

    Public Function checkUpper(ByVal str As String) As Boolean
        If Strings.StrComp(str, Strings.UCase(str)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class

