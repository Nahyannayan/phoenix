Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Imports System.Data.Oledb
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class CounterDisplay
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function DisplayCounter(ByVal Bsuid As String) As XmlDocument
        Dim Encr_decrData As New Encryption64
        Dim myDataset As New DataSet()
        Dim Xdoc As New XmlDocument
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringCounter").ConnectionString

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsuid)
            myDataset = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_DATA_LCD_DISPLAY", pParms)

            Dim strXdoc As String
            strXdoc = myDataset.GetXml
            Xdoc.LoadXml(strXdoc)

        Catch ex As Exception

        End Try

        Return Xdoc
    End Function

End Class
