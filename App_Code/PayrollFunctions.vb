Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
 
Public Class PayrollFunctions
    'Version        Date            Author            Change
    '1.1            20-Feb-2011     Swapna          To add logged in username in EMPLEAVEAPP table
    '1.2            10-Mar-2011     Swapna          To add work handover text in EMPLEAVEAPP table

    Public Shared Function SaveEMPTRANTYPE_TRN(ByVal p_bsu_id As String, _
        ByVal p_EMP_ID As String, ByVal p_EST_TTP_ID As String, ByVal p_EST_CODE As String, _
         ByVal p_EST_DTFROM As String, ByVal p_EST_DTTO As String, ByVal p_EST_REMARKS As String, _
          ByVal p_EST_OLDCODE As String, ByVal p_bFromMAsterForm As Boolean, _
           ByVal p_stTrans As SqlTransaction) As String
        '@return_value = [dbo].[SaveEMPTRANTYPE_TRN]
        '@EST_EMP_ID = 999,
        '@EST_BSU_ID = N'XXXXXX',
        '@EST_TTP_ID = 1,

        '@EST_CODE = N'1',
        '@EST_DTFROM = N'12-DEC-2007',
        '@EST_DTTO = NULL,
        '@EST_REMARKS = N'VERUTHE',
        '@EST_OLDCODE = N'2',
        '@bFromMAsterForm = 0

        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EST_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_bsu_id
        pParms(1) = New SqlClient.SqlParameter("@EST_EMP_ID", SqlDbType.Int)
        pParms(1).Value = p_EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@EST_TTP_ID", SqlDbType.Int)
        pParms(2).Value = p_EST_TTP_ID
        pParms(3) = New SqlClient.SqlParameter("@EST_CODE", SqlDbType.VarChar, 20)
        pParms(3).Value = p_EST_CODE
        pParms(4) = New SqlClient.SqlParameter("@EST_DTFROM", SqlDbType.DateTime)
        pParms(4).Value = p_EST_DTFROM

        'Passing employee last working day value
        pParms(5) = New SqlClient.SqlParameter("@EST_DTTO", SqlDbType.DateTime)
        If Not p_EST_DTTO Is Nothing And p_EST_DTTO <> "" Then
            pParms(5).Value = p_EST_DTTO 'System.DBNull.Value '@EST_DTTO
        Else
            pParms(5).Value = System.DBNull.Value '@EST_DTTO
        End If

        pParms(6) = New SqlClient.SqlParameter("@EST_REMARKS", SqlDbType.VarChar, 100)
        pParms(6).Value = p_EST_REMARKS
        pParms(7) = New SqlClient.SqlParameter("@EST_OLDCODE", SqlDbType.VarChar, 20)
        pParms(7).Value = p_EST_OLDCODE
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        pParms(9) = New SqlClient.SqlParameter("@bFromMAsterForm", SqlDbType.Bit)
        pParms(9).Value = p_bFromMAsterForm
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer

        If HttpContext.Current.Session("BSU_IsHROnDAX") = "1" AndAlso p_EST_TTP_ID = "18" Then
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPTRANTYPE_TRN_AX", pParms)
        Else
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPTRANTYPE_TRN", pParms)
        End If


        SaveEMPTRANTYPE_TRN = pParms(8).Value


    End Function


    Public Shared Function SaveEMPAIRFARE_S(ByVal p_bsu_id As String, _
        ByVal p_EAF_ID As String, ByVal p_EAF_CIT_ID As String, ByVal p_EAF_CLASS As String, _
         ByVal p_EST_DTFROM As String, ByVal p_EAF_AGEGROUP As String, _
          ByVal p_EAF_AMOUNT As Decimal, ByVal p_bEdit As Boolean, _
           ByVal p_stTrans As SqlTransaction) As String
        '     @return_value = [dbo].[SaveEMPAIRFARE_S]
        '@EAF_BSU_ID = N'XXXXXX',
        '@EAF_ID = 0,
        '@EAF_CIT_ID = N'DXB',
        '@EAF_CLASS = N'E',
        '@EAF_DTFROM = N'12-JAN-2008',
        '@EAF_DTTO = NULL,

        '@EAF_AGEGROUP = N'Adult',
        '@EAF_AMOUNT = 3000, 
        '@bEdit = 0


        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EAF_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_bsu_id
        pParms(1) = New SqlClient.SqlParameter("@EAF_ID", SqlDbType.Int)
        pParms(1).Value = p_EAF_ID
        pParms(2) = New SqlClient.SqlParameter("@EAF_CIT_ID", SqlDbType.VarChar, 5)
        pParms(2).Value = p_EAF_CIT_ID
        pParms(3) = New SqlClient.SqlParameter("@EAF_CLASS", SqlDbType.VarChar, 20)
        pParms(3).Value = p_EAF_CLASS
        pParms(4) = New SqlClient.SqlParameter("@EAF_DTFROM", SqlDbType.DateTime)
        pParms(4).Value = p_EST_DTFROM
        pParms(5) = New SqlClient.SqlParameter("@EAF_DTTO", SqlDbType.DateTime)
        pParms(5).Value = System.DBNull.Value
        pParms(6) = New SqlClient.SqlParameter("@EAF_AGEGROUP", SqlDbType.VarChar, 10)
        pParms(6).Value = p_EAF_AGEGROUP
        pParms(7) = New SqlClient.SqlParameter("@EAF_AMOUNT", SqlDbType.Decimal, 21)
        pParms(7).Value = p_EAF_AMOUNT
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        pParms(9) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(9).Value = p_bEdit
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPAIRFARE_S", pParms)
        SaveEMPAIRFARE_S = pParms(8).Value
    End Function


    Public Shared Function SaveBSU_LEAVESLAB_S(ByVal p_BLS_ID As Integer, ByVal p_bsu_id As String, _
    ByVal p_EMP_ID As Integer, ByVal p_BLS_ELT_ID As String, ByVal p_BLS_DESCRIPTION As String, _
    ByVal p_BLS_DTFROM As String, ByVal p_BLS_DTTO As String, ByVal p_BLS_ECT_ID As String, _
    ByVal p_BLS_Days As Integer, ByVal p_BLS_DaysPerMonth As Integer, ByVal p_BLS_FULLPAYDAYS As Integer, _
    ByVal p_BLS_HALFPAYDAYS As Integer, ByVal p_BLS_MinimumLeaveDays As Integer, _
    ByVal p_BLS_ALLOWEDPERIODFROM As String, ByVal p_BLS_ALLOWEDPERIODTO As String, _
    ByVal p_BLS_bCheckLeavePeriod As Boolean, ByVal p_BLS_bAccLeavetyp As Boolean, _
    ByVal p_BLS_MaxCarryFWD As Integer, ByVal p_BLS_MaxEnCashDays As Integer, ByVal p_BLS_bHolidaysAreLeave As Boolean, _
    ByVal p_BLS_maxLOPDays As Integer, ByVal p_EARNCODES As String, ByVal p_edit As Boolean, _
    ByRef NEW_BLS_ID As Integer, ByVal p_BLS_EligMinMonths As Integer, ByVal p_BLS_bLeaveSalary As Boolean, _
    ByVal p_BLS_SplitMonths As String, ByVal p_BLS_bPAID As Boolean, ByVal p_BLS_EOB As Boolean, ByVal p_stTrans As SqlTransaction) As String


        Dim pParms(30) As SqlClient.SqlParameter
        '@return_value = [dbo].[SaveBSU_LEAVESLAB_S]
        '@BLS_ID = 0,
        '@BLS_BSU_ID = N'XXXXXX',
        '@BLS_EMP_ID = NULL,
        '@BLS_ELT_ID = 'LOP',
        '@BLS_DESCRIPTION = 'TEST DATA',
        pParms(0) = New SqlClient.SqlParameter("@BLS_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_bsu_id
        pParms(1) = New SqlClient.SqlParameter("@BLS_EMP_ID", SqlDbType.Int)
        pParms(1).Value = p_EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@BLS_ELT_ID", SqlDbType.VarChar, 10)
        pParms(2).Value = p_BLS_ELT_ID
        pParms(3) = New SqlClient.SqlParameter("@BLS_DESCRIPTION", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BLS_DESCRIPTION
        '@BLS_DTFROM = N'1-JAN-2007',
        '@BLS_DTTO = NULL,
        '@BLS_ECT_ID = 2,
        '@BLS_Days = 30,
        '@BLS_DaysPerMonth = 2,
        '@BLS_FULLPAYDAYS = 3,
        '@BLS_HALFPAYDAYS = 4,
        pParms(4) = New SqlClient.SqlParameter("@BLS_DTFROM", SqlDbType.VarChar, 11)
        pParms(4).Value = p_BLS_DTFROM
        pParms(5) = New SqlClient.SqlParameter("@BLS_DTTO", SqlDbType.VarChar, 11)
        pParms(5).Value = p_BLS_DTTO
        pParms(6) = New SqlClient.SqlParameter("@BLS_ECT_ID", SqlDbType.Int)
        pParms(6).Value = p_BLS_ECT_ID
        pParms(7) = New SqlClient.SqlParameter("@BLS_Days", SqlDbType.Int)
        pParms(7).Value = p_BLS_Days
        pParms(8) = New SqlClient.SqlParameter("@BLS_DaysPerMonth", SqlDbType.Int)
        pParms(8).Value = p_BLS_DaysPerMonth
        pParms(9) = New SqlClient.SqlParameter("@BLS_FULLPAYDAYS", SqlDbType.Int)
        pParms(9).Value = p_BLS_FULLPAYDAYS
        pParms(10) = New SqlClient.SqlParameter("@BLS_HALFPAYDAYS", SqlDbType.Int)
        pParms(10).Value = p_BLS_HALFPAYDAYS

        '@BLS_MinimumLeaveDays = NULL,
        '@BLS_ALLOWEDPERIODFROM = NULL,
        '@BLS_ALLOWEDPERIODTO = NULL,
        '@BLS_bCheckLeavePeriod = 0,

        pParms(11) = New SqlClient.SqlParameter("@BLS_MinimumLeaveDays", SqlDbType.Int)
        If p_BLS_MinimumLeaveDays = 0 Then
            pParms(11).Value = System.DBNull.Value
        Else
            pParms(11).Value = p_BLS_MinimumLeaveDays
        End If
        pParms(11).Value = p_BLS_MinimumLeaveDays
        pParms(12) = New SqlClient.SqlParameter("@BLS_ALLOWEDPERIODFROM", SqlDbType.VarChar, 11)
        If p_BLS_ALLOWEDPERIODFROM = "" Then
            pParms(12).Value = System.DBNull.Value
        Else
            pParms(12).Value = p_BLS_ALLOWEDPERIODFROM
        End If
        pParms(13) = New SqlClient.SqlParameter("@BLS_ALLOWEDPERIODTO", SqlDbType.VarChar, 11)
        If p_BLS_ALLOWEDPERIODFROM = "" Then
            pParms(13).Value = System.DBNull.Value
        Else
            pParms(13).Value = p_BLS_ALLOWEDPERIODTO
        End If
        pParms(14) = New SqlClient.SqlParameter("@BLS_bCheckLeavePeriod", SqlDbType.Bit)
        pParms(14).Value = p_BLS_bCheckLeavePeriod

        '@BLS_bAccLeavetyp = 0,
        '@BLS_MaxCarryFWD = 5,
        '@BLS_MaxEnCashDays = 5,
        '@BLS_bHolidaysAreLeave = 5,
        '@bEdit = 0,@BLS_bLeaveSalary

        pParms(15) = New SqlClient.SqlParameter("@BLS_bAccLeavetyp", SqlDbType.Bit)
        pParms(15).Value = p_BLS_bAccLeavetyp
        pParms(16) = New SqlClient.SqlParameter("@BLS_MaxCarryFWD", SqlDbType.Int)
        pParms(16).Value = p_BLS_MaxCarryFWD
        pParms(17) = New SqlClient.SqlParameter("@BLS_MaxEnCashDays", SqlDbType.Int)
        pParms(17).Value = p_BLS_MaxEnCashDays
        pParms(18) = New SqlClient.SqlParameter("@BLS_bHolidaysAreLeave", SqlDbType.Bit)
        pParms(18).Value = p_BLS_bHolidaysAreLeave
        pParms(19) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(19).Value = p_edit
        pParms(20) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(20).Direction = ParameterDirection.ReturnValue
        pParms(21) = New SqlClient.SqlParameter("@BLS_ID", SqlDbType.Bit)
        pParms(21).Value = p_BLS_ID
        pParms(22) = New SqlClient.SqlParameter("@BLS_maxLOPDays", SqlDbType.Int)
        pParms(22).Value = p_BLS_maxLOPDays
        pParms(23) = New SqlClient.SqlParameter("@EARNCODES", SqlDbType.VarChar, 100)
        pParms(23).Value = p_EARNCODES
        pParms(24) = New SqlClient.SqlParameter("@NEW_BLS_ID", SqlDbType.Int)
        pParms(24).Direction = ParameterDirection.Output
        pParms(25) = New SqlClient.SqlParameter("@BLS_bLeaveSalary", SqlDbType.Bit)
        pParms(25).Value = p_BLS_bLeaveSalary
        pParms(26) = New SqlClient.SqlParameter("@BLS_EligMinMonths", SqlDbType.Int)
        pParms(26).Value = p_BLS_EligMinMonths
        pParms(27) = New SqlClient.SqlParameter("@BLS_SplitMonths", SqlDbType.VarChar, 20)
        pParms(27).Value = p_BLS_SplitMonths
        '@BLS_bPAID bit
        'v@BLS_EOB bit
        pParms(28) = New SqlClient.SqlParameter("@BLS_bPAID", SqlDbType.Bit)
        pParms(28).Value = p_BLS_bPAID
        pParms(29) = New SqlClient.SqlParameter("@BLS_EOB", SqlDbType.Bit)
        pParms(29).Value = p_BLS_EOB
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveBSU_LEAVESLAB_S", pParms)
        If pParms(20).Value = "0" Then
            SaveBSU_LEAVESLAB_S = pParms(20).Value
            NEW_BLS_ID = pParms(24).Value
        Else
            SaveBSU_LEAVESLAB_S = "1000"
        End If


    End Function


    Public Shared Function SaveBSU_GRATUITYSLAB_S(ByVal p_BGS_ID As Integer, ByVal p_bsu_id As String, _
     ByVal p_BGS_DESCRIPTION As String, _
    ByVal p_BGS_DTFROM As String, ByVal p_BGS_DTTO As String, ByVal p_BGS_ECT_ID As Integer, _
    ByVal p_BGS_CONTRACTTYPE As String, ByVal p_BGS_TRANTYPE As String, ByVal p_BGS_SLABFROM As Integer, _
    ByVal p_BGS_SLABTO As Integer, ByVal p_BGS_ACTUALDAYS As Integer, _
    ByVal p_BGS_PROVISIONDAYS As Integer, ByVal p_EARNCODES As String, _
    ByVal p_edit As Boolean, ByVal p_stTrans As SqlTransaction) As String


        Dim pParms(15) As SqlClient.SqlParameter
        '@return_value = [dbo].[SaveBSU_GRATUITYSLAB_S]

        '@BGS_BSU_ID = N'XXXXXX',
        '@BGS_ID = 0,
        '@BGS_DESCRIPTION = N'TEST GRATU',  
        pParms(0) = New SqlClient.SqlParameter("@BGS_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_bsu_id
        pParms(1) = New SqlClient.SqlParameter("@BGS_ID", SqlDbType.Int)
        pParms(1).Value = p_BGS_ID
        pParms(2) = New SqlClient.SqlParameter("@BGS_DESCRIPTION", SqlDbType.VarChar, 100)
        pParms(2).Value = p_BGS_DESCRIPTION
        '@BGS_DTFROM = N'1-JAN-2007',
        '@BGS_DTTO = N'31-DEC-2007',
        '@BGS_CONTRACTTYPE = N'Limited',
        '@BGS_ECT_ID = 1,
        '@BGS_TRANTYPE = N'Termination ', 
        pParms(3) = New SqlClient.SqlParameter("@BGS_DTFROM", SqlDbType.DateTime)
        pParms(3).Value = p_BGS_DTFROM
        pParms(4) = New SqlClient.SqlParameter("@BGS_DTTO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_BGS_DTTO
        pParms(5) = New SqlClient.SqlParameter("@BGS_CONTRACTTYPE", SqlDbType.VarChar, 10)
        pParms(5).Value = p_BGS_CONTRACTTYPE
        pParms(6) = New SqlClient.SqlParameter("@BGS_ECT_ID", SqlDbType.Int)
        pParms(6).Value = p_BGS_ECT_ID
        pParms(7) = New SqlClient.SqlParameter("@BGS_TRANTYPE", SqlDbType.VarChar, 20)
        pParms(7).Value = p_BGS_TRANTYPE
        '@BGS_SLABFROM = 0,
        '@BGS_SLABTO = 364,
        '@BGS_PROVISIONDAYS = 180,
        '@BGS_ACTUALDAYS = 90 
        '@EARNCODES = N'11|22|33',
        '@bEdit = 0, 
        pParms(8) = New SqlClient.SqlParameter("@BGS_SLABFROM", SqlDbType.Int)
        pParms(8).Value = p_BGS_SLABFROM
        pParms(9) = New SqlClient.SqlParameter("@BGS_SLABTO", SqlDbType.Int)
        pParms(9).Value = p_BGS_SLABTO
        pParms(10) = New SqlClient.SqlParameter("@BGS_PROVISIONDAYS", SqlDbType.Int)
        pParms(10).Value = p_BGS_PROVISIONDAYS
        pParms(11) = New SqlClient.SqlParameter("@BGS_ACTUALDAYS", SqlDbType.Int)
        pParms(11).Value = p_BGS_ACTUALDAYS

        pParms(12) = New SqlClient.SqlParameter("@EARNCODES", SqlDbType.VarChar, 200)
        pParms(12).Value = p_EARNCODES
        pParms(13) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(13).Value = p_edit
        pParms(14) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(14).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveBSU_GRATUITYSLAB_S", pParms)

        SaveBSU_GRATUITYSLAB_S = pParms(14).Value
    End Function


    Public Shared Function ProcessSalary(ByVal p_BSU_ID As String, _
         ByVal p_PAYMONTH As Integer, ByVal p_PAYYEAR As Integer, ByVal p_EMPID As String, _
         ByVal p_stTrans As SqlTransaction) As String
        '@return_value = [dbo].[ProcessSalary]
        '@BSUID = N'XXXXXX',
        '@PAYMONTH = 9,
        '@PAYYEAR = 2007,
        '@EMPID = N'999'


        Dim pParms(16) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@PAYMONTH", SqlDbType.Int)
        pParms(1).Value = p_PAYMONTH
        pParms(2) = New SqlClient.SqlParameter("@PAYYEAR", SqlDbType.Int)
        pParms(2).Value = p_PAYYEAR
        pParms(3) = New SqlClient.SqlParameter("@xEMPID", SqlDbType.VarChar)
        pParms(3).Value = p_EMPID

        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "ProcessSalary", pParms)

        ProcessSalary = pParms(4).Value
 
    End Function


    Public Shared Function ProcessSalaryWithVaccation(ByVal p_BSU_ID As String, _
        ByVal p_PAYMONTH As Integer, ByVal p_PAYYEAR As Integer, ByVal p_EMPID As String, _
        ByVal p_conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        '@return_value = [dbo].[ProcessSalary]
        '@BSUID = N'XXXXXX',
        '@PAYMONTH = 9,
        '@PAYYEAR = 2007,
        '@EMPID = N'999'

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@PAYMONTH", SqlDbType.Int)
        pParms(1).Value = p_PAYMONTH
        pParms(2) = New SqlClient.SqlParameter("@PAYYEAR", SqlDbType.Int)
        pParms(2).Value = p_PAYYEAR
        pParms(3) = New SqlClient.SqlParameter("@xEMPID", SqlDbType.VarChar)
        pParms(3).Value = p_EMPID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue

        Dim cmd As New SqlCommand
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))

        cmd.Connection = p_conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "ProcessSalaryWithVaccation"
        cmd.CommandTimeout = 0
        Dim retval As Integer
        retval = cmd.ExecuteNonQuery
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "ProcessSalaryWithVaccation", pParms)
        ProcessSalaryWithVaccation = pParms(4).Value
    End Function


    Public Shared Function SaveEMPSALARYTRF_D(ByVal p_EBT_BNK_ID As Integer, _
           ByVal p_EBT_OURBANK_ACT_ID As String, ByVal p_EBT_CHD_CHB_ID As String, _
           ByVal p_EBT_CHQ_NO As String, ByVal p_EBT_DATE As String, ByVal p_EBT_AMOUNT As Decimal, _
           ByVal p_EBT_REFDOCNO As String, ByVal p_EBT_NARRATION As String, ByVal p_bEdit As Boolean, _
           ByRef EBT_ID As Integer, ByVal p_EBT_DOCTYPE As String, ByVal p_stTrans As SqlTransaction, Optional ByVal p_BsuID As String = "", _
           Optional ByVal pEBT_PURPOSE As Integer = 0, Optional ByVal pValueDate As String = "") As String
        '@return_value = [dbo].[SaveEMPSALARYTRF_D]
        '@EBT_BNK_ID = 1,
        '@EBT_OURBANK_ACT_ID = N'XXX',
        '@EBT_CHD_CHB_ID = 1,
        '@EBT_CHQ_NO = N'22',
        '@EBT_DATE = N'12-JAN-2008',
        '@EBT_AMOUNT = 1200,
        '@EBT_REFDOCNO = N'D12',
        '@EBT_NARRATION = N'VERRRR',
        '@bEdit = 0,
        '@EBT_ID = @EBT_ID OUTPUT @EBT_DOCTYPE varchar(20), 

        Dim pParms(14) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EBT_BNK_ID", SqlDbType.Int)
        pParms(0).Value = p_EBT_BNK_ID
        pParms(1) = New SqlClient.SqlParameter("@EBT_OURBANK_ACT_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_EBT_OURBANK_ACT_ID
        pParms(2) = New SqlClient.SqlParameter("@EBT_CHD_CHB_ID", SqlDbType.VarChar, 10)
        If p_EBT_CHD_CHB_ID = "" Then
            pParms(2).Value = System.DBNull.Value
        Else
            pParms(2).Value = p_EBT_CHD_CHB_ID
        End If

        pParms(3) = New SqlClient.SqlParameter("@EBT_CHQ_NO", SqlDbType.VarChar, 10)
        If p_EBT_CHQ_NO = "" Then
            pParms(3).Value = System.DBNull.Value
        Else
            pParms(3).Value = p_EBT_CHQ_NO
        End If
        pParms(4) = New SqlClient.SqlParameter("@EBT_DATE", SqlDbType.DateTime)
        pParms(4).Value = p_EBT_DATE
        pParms(5) = New SqlClient.SqlParameter("@EBT_AMOUNT", SqlDbType.Decimal)
        pParms(5).Value = p_EBT_AMOUNT
        pParms(6) = New SqlClient.SqlParameter("@EBT_REFDOCNO", SqlDbType.VarChar, 20)
        pParms(6).Value = p_EBT_REFDOCNO
        pParms(7) = New SqlClient.SqlParameter("@EBT_NARRATION", SqlDbType.VarChar, 200)
        pParms(7).Value = p_EBT_NARRATION
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        pParms(9) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(9).Value = p_bEdit
        pParms(10) = New SqlClient.SqlParameter("@EBT_ID", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@EBT_DOCTYPE", SqlDbType.VarChar, 20)
        pParms(11).Value = p_EBT_DOCTYPE
        pParms(12) = New SqlClient.SqlParameter("@EBT_BSU_ID", SqlDbType.VarChar, 20)
        pParms(12).Value = p_BsuID

        pParms(13) = New SqlClient.SqlParameter("@EBT_PURPOSE", SqlDbType.VarChar, 20)

        pParms(13).Value = pEBT_PURPOSE

        If pValueDate = "" Then
            pValueDate = System.DBNull.Value.ToString
        End If
        pParms(14) = New SqlClient.SqlParameter("@EBT_ValueDate", SqlDbType.VarChar, 20)
        pParms(14).Value = pValueDate

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, _
        OASISConstants.dbPayroll & ".dbo.SaveEMPSALARYTRF_D", pParms)
        EBT_ID = pParms(10).Value
        SaveEMPSALARYTRF_D = pParms(8).Value
    End Function


    Public Shared Function SaveEMPSALHOLD_D(ByVal p_EHD_ID As Integer, _
            ByVal p_EHD_EMP_ID As String, ByVal p_EHD_PAYMONTH As Integer, _
            ByVal p_EHD_PAYYEAR As Integer, ByVal p_EHD_ESD_ID As String, ByVal p_EHD_bHold As Boolean, _
            ByVal p_EHD_Remarks As String, ByRef IsRelease As Boolean, ByRef IsUnhold As Boolean, ByVal p_stTrans As SqlTransaction, Optional ByVal IsPaidOutside As Boolean = False) As String
        ''EXEC	@return_value = [dbo].[SaveEMPSALHOLD_D]
        ''@EHD_ID = 0,
        ''@EHD_EMP_ID = 999,
        ''@EHD_PAYMONTH = 9,
        ''@EHD_PAYYEAR = 2007,
        ''@EHD_ESD_ID = 12,
        ''@EHD_bHold = 1,
        ''@EHD_Remarks = N'xxxver',
        ''@bEdit = 0

        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EHD_ID", SqlDbType.Int)
        pParms(0).Value = p_EHD_ID
        pParms(1) = New SqlClient.SqlParameter("@EHD_EMP_ID", SqlDbType.Int)
        pParms(1).Value = p_EHD_EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@EHD_PAYMONTH", SqlDbType.Int)
        pParms(2).Value = p_EHD_PAYMONTH
        pParms(3) = New SqlClient.SqlParameter("@EHD_PAYYEAR", SqlDbType.Int)
        pParms(3).Value = p_EHD_PAYYEAR
        pParms(4) = New SqlClient.SqlParameter("@EHD_ESD_ID", SqlDbType.VarChar, 10)
        pParms(4).Value = p_EHD_ESD_ID
        pParms(5) = New SqlClient.SqlParameter("@EHD_bHold", SqlDbType.Bit)
        pParms(5).Value = p_EHD_bHold
        pParms(6) = New SqlClient.SqlParameter("@EHD_Remarks", SqlDbType.VarChar, 200)
        pParms(6).Value = p_EHD_Remarks
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@EHD_bUpdateStatus", SqlDbType.Bit)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@IsRelease", SqlDbType.Bit)
        pParms(9).Value = IsRelease
        pParms(10) = New SqlClient.SqlParameter("@EHD_IsPaidOutsideSystem", SqlDbType.Bit) ' Hold payout
        pParms(10).Value = IsPaidOutside
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPSALHOLD_D", pParms)

        If pParms(7).Value = "0" Then
            IsUnhold = pParms(8).Value
        Else
            IsUnhold = False
        End If
        SaveEMPSALHOLD_D = pParms(7).Value
    End Function


    Public Shared Function PostPayroll(ByVal p_BSU_ID As String, _
             ByVal p_PAYMONTH As Integer, ByVal p_PAYYEAR As Integer, ByVal p_DOCDT As String, _
             ByVal p_stTrans As SqlTransaction, Optional ByVal p_IsFFS As Boolean = 0) As String  'Swapna added for separating FFS JVs
        ''@return_value = [dbo].[PostPayroll]
        ''@BSUID = N'125003',

        ''@PAyMonth = 12,
        ''@Payyear = 2007,

        ''@DOCDT = N'12-DEC-2007'

        Dim pParms(16) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@PAYMONTH", SqlDbType.Int)
        pParms(1).Value = p_PAYMONTH
        pParms(2) = New SqlClient.SqlParameter("@PAYYEAR", SqlDbType.Int)
        pParms(2).Value = p_PAYYEAR
        pParms(3) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(3).Value = p_DOCDT

        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue

        pParms(5) = New SqlClient.SqlParameter("@IsFFS", SqlDbType.Bit)
        pParms(5).Value = p_IsFFS

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "PostPayroll", pParms)

        PostPayroll = pParms(4).Value
    End Function
     

    Public Shared Function SaveEMPLOAN_H(ByVal p_ELH_ID As Integer, ByVal p_ELH_EMP_ID As Integer, ByVal p_ELH_PURPOSE As String, _
     ByVal p_ELH_BSU_ID As String, ByVal p_ELH_ERN_ID As String, ByVal p_ELH_DATE As Date, _
     ByVal p_ELH_INSTNO As Integer, ByVal p_ELH_CUR_ID As String, ByVal p_ELH_AMOUNT As Decimal, _
     ByVal p_ELH_BALANCE As Decimal, ByVal p_ELH_bPERSONAL As Boolean, ByVal p_bEdit As Boolean, _
     ByVal p_ELH_bPosted As Boolean, ByRef p_NEW_ELH_ID As Integer, ByVal p_ELH_DOCTYPE As String, _
     ByVal p_ELH_PAYREFNO As String, ByRef p_ELH_REMARKS As String, ByVal p_stTrans As SqlTransaction, Optional ByVal p_IsAXPosting As Boolean = False) As String '-- AX posting changes
        '       @return_value = [dbo].[SaveEMPLOAN_H]
        '@ELH_ID =1		, 
        '@ELH_EMP_ID = 1006,
        '@ELH_PURPOSE = N'veruthe',
        '@ELH_BSU_ID = N'XXXXXX',
        '@ELH_ERN_ID = N'123',
        '@ELH_DATE = N'12/DEC/2007',
        '@ELH_INSTNO = 3,
        '@ELH_CUR_ID = N'AED',
        '@ELH_AMOUNT = 1000,
        '@ELH_BALANCE = 1000,
        '@ELH_bPERSONAL = 1,
        '@bEdit = 0,
        '@ELH_bPosted = 0,
        '@NEW_ELH_ID = @NEW_ELH_ID OUTPUT,  @ELH_REMARKS

        Dim pParms(19) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ELH_ID", SqlDbType.Int)
        pParms(0).Value = p_ELH_ID
        pParms(1) = New SqlClient.SqlParameter("@ELH_EMP_ID", SqlDbType.Int)
        pParms(1).Value = p_ELH_EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@ELH_PURPOSE", SqlDbType.VarChar, 100)
        pParms(2).Value = p_ELH_PURPOSE
        pParms(3) = New SqlClient.SqlParameter("@ELH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_ELH_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@ELH_ERN_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_ELH_ERN_ID
        pParms(5) = New SqlClient.SqlParameter("@ELH_bPERSONAL", SqlDbType.Bit)
        pParms(5).Value = p_ELH_bPERSONAL

        pParms(6) = New SqlClient.SqlParameter("@ELH_DATE", SqlDbType.DateTime)
        pParms(6).Value = p_ELH_DATE
        pParms(7) = New SqlClient.SqlParameter("@ELH_INSTNO", SqlDbType.Int)
        pParms(7).Value = p_ELH_INSTNO
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        pParms(9) = New SqlClient.SqlParameter("@ELH_CUR_ID", SqlDbType.VarChar, 10)
        pParms(9).Value = p_ELH_CUR_ID
        pParms(10) = New SqlClient.SqlParameter("@ELH_AMOUNT", SqlDbType.Decimal)
        pParms(10).Value = p_ELH_AMOUNT
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction 
        '@ELA_ADDRESS,@ELA_PHONE ,@ELA_MOBILE ,@ELA_PIN )@APS_bFORWARD''' @ELH_DOCTYPE varchar(20),@ELH_PAYREFNO varchar(20)
        pParms(11) = New SqlClient.SqlParameter("@ELH_BALANCE", SqlDbType.Decimal)
        pParms(11).Value = p_ELH_BALANCE
        pParms(12) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(12).Value = p_bEdit
        pParms(13) = New SqlClient.SqlParameter("@ELH_bPosted", SqlDbType.Bit)
        pParms(13).Value = p_ELH_bPosted

        pParms(14) = New SqlClient.SqlParameter("@NEW_ELH_ID", SqlDbType.Int)
        pParms(14).Direction = ParameterDirection.Output

        pParms(15) = New SqlClient.SqlParameter("@ELH_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(15).Value = p_ELH_DOCTYPE
        pParms(16) = New SqlClient.SqlParameter("@ELH_PAYREFNO", SqlDbType.VarChar, 20)
        pParms(16).Value = p_ELH_PAYREFNO

        pParms(17) = New SqlClient.SqlParameter("@ELH_REMARKS", SqlDbType.VarChar, 200)
        pParms(17).Value = p_ELH_REMARKS

        pParms(18) = New SqlClient.SqlParameter("@ELH_IsAXPosting", SqlDbType.Bit)
        pParms(18).Value = p_IsAXPosting


        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.SaveEMPLOAN_H", pParms)

        SaveEMPLOAN_H = pParms(8).Value
        p_NEW_ELH_ID = pParms(14).Value
    End Function


    Public Shared Function SaveEMPLOAN_D(ByVal p_ELD_ID As Integer, ByVal p_ELD_ELH_ID As Integer, _
        ByVal p_ELH_INSTNO As Integer, _
        ByVal p_ELD_DATE As Date, ByVal p_ELD_AMOUNT As String, ByVal p_ELD_PAIDAMT As Decimal, _
        ByVal p_ELD_ESD_ID As Integer, ByVal p_bEdit As Boolean, ByVal p_stTrans As SqlTransaction) As String
        '       @return_value = [dbo].[SaveEMPLOAN_D]
        '		@ELD_ID = 1,
        '		@ELD_ELH_ID = 4,
        '		@ELH_INSTNO = 1,
        '		@ELD_DATE = N'12/dec/2007',
        '		@ELD_AMOUNT = 1200,
        '		@ELD_PAIDAMT = 244,
        '		@ELD_ESD_ID = 1223,
        '       @bEdit =0

        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ELD_ID", SqlDbType.Int)
        pParms(0).Value = p_ELD_ID
        pParms(1) = New SqlClient.SqlParameter("@ELD_ELH_ID", SqlDbType.Int)
        pParms(1).Value = p_ELD_ELH_ID
        pParms(2) = New SqlClient.SqlParameter("@ELH_INSTNO", SqlDbType.Int)
        pParms(2).Value = p_ELH_INSTNO
        pParms(3) = New SqlClient.SqlParameter("@ELD_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_ELD_DATE
        pParms(4) = New SqlClient.SqlParameter("@ELD_AMOUNT", SqlDbType.Decimal)
        pParms(4).Value = p_ELD_AMOUNT
        pParms(5) = New SqlClient.SqlParameter("@ELD_PAIDAMT", SqlDbType.Decimal)
        pParms(5).Value = p_ELD_PAIDAMT

        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@ELD_ESD_ID", SqlDbType.Int)
        pParms(7).Value = p_ELD_ESD_ID
        pParms(8) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Decimal)
        pParms(8).Value = p_bEdit

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.SaveEMPLOAN_D", pParms)

        SaveEMPLOAN_D = pParms(6).Value
        
    End Function


    Public Shared Function DeleteEMPLOAN_D(ByVal p_ELD_ID As Integer, ByVal p_stTrans As SqlTransaction) As String


        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ELD_ID", SqlDbType.Int)
        pParms(0).Value = p_ELD_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.DeleteEMPLOAN_D", pParms)

        DeleteEMPLOAN_D = pParms(1).Value
      

    End Function
 

    Public Shared Function SaveEMPSALCOMPO_M(ByVal ID As String, ByVal Descr As String, _
    ByVal Etype As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction, _
     ByVal ERN_TYP As Boolean, ByVal ERN_Flag As Integer, ByVal ERN_ACC_ID As String, _
     ByVal bLOP As Boolean, ByVal bIncludeLeaveSal As Boolean, _
     ByVal p_ERN_PAYABLE_ACT_ID As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(9) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@ERN_ID", ID)
            pParms(1) = New SqlClient.SqlParameter("@ERN_DESCR", Descr)
            ' pParms(2) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(2) = New SqlClient.SqlParameter("@ERN_ACC_ID", ERN_ACC_ID)

            pParms(3) = New SqlClient.SqlParameter("@ERN_Flag", ERN_Flag)
            pParms(4) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(5) = New SqlClient.SqlParameter("@ERN_TYP", ERN_TYP)
            pParms(6) = New SqlClient.SqlParameter("@ERN_bLOP", bLOP)
            pParms(7) = New SqlClient.SqlParameter("@ERN_BIncludeInLeaveSal", bIncludeLeaveSal)

            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue

            pParms(9) = New SqlClient.SqlParameter("@ERN_PAYABLE_ACT_ID", p_ERN_PAYABLE_ACT_ID)

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEMPSALCOMPO_M", pParms)
            Dim ReturnFlag As Integer = pParms(8).Value
            Return ReturnFlag
        End Using

    End Function


    Public Shared Function SaveEMPSALCOMPO_D(ByVal p_ERD_ERN_ID As String, ByVal p_ERD_ECT_ID As String, _
    ByVal p_ERD_ACC_ID As String, ByVal p_ERD_PAYABLE_ACT_ID As String, _
    ByVal p_ERD_CALCTYP As Integer, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(6) As SqlClient.SqlParameter
            'EXEC	@return_value = [dbo].[SaveEMPSALCOMPO_D]
            '		@ERD_ERN_ID = N'BASIC',
            '		@ERD_ECT_ID = 5 ,
            '		@ERD_ACC_ID = N'60101201',@ERD_PAYABLE_ACT_ID varchar(20)
            ' @ERD_CALCTYP tinyint=1 


            pParms(0) = New SqlClient.SqlParameter("@ERD_ERN_ID", p_ERD_ERN_ID)
            pParms(1) = New SqlClient.SqlParameter("@ERD_ECT_ID", p_ERD_ECT_ID)
            pParms(2) = New SqlClient.SqlParameter("@ERD_ACC_ID", p_ERD_ACC_ID)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            pParms(4) = New SqlClient.SqlParameter("@ERD_PAYABLE_ACT_ID", p_ERD_PAYABLE_ACT_ID)
            pParms(5) = New SqlClient.SqlParameter("@ERD_CALCTYP", p_ERD_CALCTYP)
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEMPSALCOMPO_D", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
        End Using

    End Function


    Public Shared Function GetEMPSAL_ID(ByVal ID As String, ByVal Etype As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEmpType As String = ""
        If Etype = "E" Then
            'sqlGetEmpType = "Select id,descr,paid,eob,Acc_ID,includeSal, bLOP from (SELECT EMPSALCOMPO_M.ERN_ID as id, EMPSALCOMPO_M.ERN_DESCR as descr,  vw_OSF_ACCOUNTS_M.ACT_NAME as paid, EMPSALCOMPO_M.ERN_BIncludeInLeaveSal as includeSal, EMPSALCOMPO_M.ERN_bLOP as bLOP, EMPSALCOMPO_M.ERN_Flag as eob,EMPSALCOMPO_M.ERN_TYP as ERN_TYP, EMPSALCOMPO_M.ERN_ACC_ID as Acc_ID FROM  vw_OSF_ACCOUNTS_M INNER JOIN  EMPSALCOMPO_M ON vw_OSF_ACCOUNTS_M.ACT_ID = EMPSALCOMPO_M.ERN_ACC_ID)a where a.ERN_TYP=1 and  a.ID='" & ID & "'"

            sqlGetEmpType = "Select ID,DESCR,PAID,EOB,ACC_ID,INCLUDESAL, bLOP,CACC_ID,CDESCR from (" _
                & " SELECT     ERN.ERN_ID AS ID, ERN.ERN_DESCR AS DESCR, AMD.ACT_NAME AS PAID, " _
                & " ERN.ERN_BIncludeInLeaveSal AS INCLUDESAL, ERN.ERN_bLOP AS bLOP, " _
                & " ERN.ERN_Flag AS EOB, ERN.ERN_TYP, ERN.ERN_ACC_ID AS ACC_ID, ERN.ERN_PAYABLE_ACT_ID AS CACC_ID, AMC.ACT_NAME AS CDESCR" _
                & " FROM         vw_OSF_ACCOUNTS_M AS AMC RIGHT OUTER JOIN" _
                & " EMPSALCOMPO_M AS ERN ON AMC.ACT_ID = ERN.ERN_PAYABLE_ACT_ID LEFT OUTER JOIN" _
                & " vw_OSF_ACCOUNTS_M AS AMD ON ERN.ERN_ACC_ID = AMD.ACT_ID" _
                & " )a where a.ERN_TYP=1 and  a.ID='" & ID & "'"


        ElseIf Etype = "D" Then
            ' sqlGetEmpType = "Select id,descr,paid,eob,Acc_ID from (SELECT EMPSALCOMPO_M.ERN_ID as id, EMPSALCOMPO_M.ERN_DESCR as descr,  vw_OSF_ACCOUNTS_M.ACT_NAME as paid, EMPSALCOMPO_M.ERN_Flag as eob,EMPSALCOMPO_M.ERN_TYP as ERN_TYP,EMPSALCOMPO_M.ERN_ACC_ID as Acc_ID FROM  vw_OSF_ACCOUNTS_M INNER JOIN  EMPSALCOMPO_M ON vw_OSF_ACCOUNTS_M.ACT_ID = EMPSALCOMPO_M.ERN_ACC_ID)a where a.ERN_TYP=0 and  a.ID='" & ID & "'"
            sqlGetEmpType = "Select ID,DESCR,PAID,EOB,ACC_ID,INCLUDESAL, bLOP,CACC_ID,CDESCR from (" _
                          & " SELECT     ERN.ERN_ID AS ID, ERN.ERN_DESCR AS DESCR, AMD.ACT_NAME AS PAID, " _
                          & " ERN.ERN_BIncludeInLeaveSal AS INCLUDESAL, ERN.ERN_bLOP AS bLOP, " _
                          & " ERN.ERN_Flag AS EOB, ERN.ERN_TYP, ERN.ERN_ACC_ID AS ACC_ID, ERN.ERN_PAYABLE_ACT_ID AS CACC_ID, AMC.ACT_NAME AS CDESCR" _
                          & " FROM         vw_OSF_ACCOUNTS_M AS AMC RIGHT OUTER JOIN" _
                          & " EMPSALCOMPO_M AS ERN ON AMC.ACT_ID = ERN.ERN_PAYABLE_ACT_ID LEFT OUTER JOIN" _
                          & " vw_OSF_ACCOUNTS_M AS AMD ON ERN.ERN_ACC_ID = AMD.ACT_ID" _
                          & " )a where a.ERN_TYP=0 and  a.ID='" & ID & "'"

        End If
        Dim command As SqlCommand = New SqlCommand(sqlGetEmpType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    Public Shared Function DoMonthendProcess(ByVal p_BSUID As String, _
    ByVal p_DOCDT As String, ByVal p_Type As String, ByRef JHD_NEWDOCNO As String, ByVal p_stTrans As SqlTransaction, ByVal conn As SqlConnection) As String
        'EXEC	@return_value = [dbo].[DoMonthendProcess]
        '		@BSUID = N'125003',
        '		@DOCDT = N'12-feb-2008',
        '		@Type = N'A'
        Dim cmd As New SqlCommand("DoMonthendProcess", conn, p_stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSUID
        cmd.Parameters.Add(pParms(0))

        pParms(1) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(1).Value = p_DOCDT
        cmd.Parameters.Add(pParms(1))

        pParms(2) = New SqlClient.SqlParameter("@Type", SqlDbType.Char)
        pParms(2).Value = p_Type
        cmd.Parameters.Add(pParms(2))

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(pParms(3))

        pParms(4) = New SqlClient.SqlParameter("@JHD_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(4).Direction = ParameterDirection.Output
        cmd.Parameters.Add(pParms(4))

        Dim retval As Integer
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DoMonthendProcess", pParms)
        cmd.CommandTimeout = 0
        retval = cmd.ExecuteNonQuery()


        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "ProcessVacation", pParms)


        If pParms(3).Value = 0 Then
            JHD_NEWDOCNO = pParms(4).Value
        Else
            JHD_NEWDOCNO = ""
        End If
        DoMonthendProcess = pParms(3).Value
    End Function


    Public Shared Function SaveBSU_LEAVESLAB_S_D(ByVal p_BLD_ID As String, _
        ByVal p_BLD_BLS_ID As String, ByVal p_BLD_SLABFROM As String, ByVal p_BLD_SLABTO As String, _
          ByVal p_BLD_FACTOR As Decimal, ByVal p_stTrans As SqlTransaction) As String
        '     [dbo].[SaveBSU_LEAVESLAB_S_D]
        '@BLD_ID = 0,
        '@BLD_BLS_ID = 12,
        '@BLD_SLABFROM = 0,
        '@BLD_SLABTO = 364,
        '	@BLD_FACTOR = 1.7


        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BLD_ID", SqlDbType.Int)
        pParms(0).Value = p_BLD_ID
        pParms(1) = New SqlClient.SqlParameter("@BLD_BLS_ID", SqlDbType.Int)
        pParms(1).Value = p_BLD_BLS_ID
        pParms(2) = New SqlClient.SqlParameter("@BLD_SLABFROM", SqlDbType.Int)
        pParms(2).Value = p_BLD_SLABFROM
        pParms(3) = New SqlClient.SqlParameter("@BLD_SLABTO", SqlDbType.Int)
        pParms(3).Value = p_BLD_SLABTO
        pParms(4) = New SqlClient.SqlParameter("@BLD_FACTOR", SqlDbType.Decimal)
        pParms(4).Value = p_BLD_FACTOR

        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue


        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveBSU_LEAVESLAB_S_D", pParms)

        SaveBSU_LEAVESLAB_S_D = pParms(5).Value
    End Function


    Public Shared Function SaveEMPLEAVE_D(ByVal p_ELV_ID As Integer, ByVal p_ELV_BSU_ID As String, _
    ByVal p_ELV_EMP_ID As Integer, ByVal p_ELV_ELT_ID As String, ByVal p_ELV_DTFROM As DateTime, _
     ByVal p_ELV_DTTO As DateTime, ByVal p_ELV_REMARKS As String, ByVal p_ELV_SALARY As Decimal, _
      ByVal p_ELV_LVSALARY As Decimal, ByVal p_ELV_ARREARS As Decimal, ByVal p_ELV_AIRTICKET As Decimal, _
      ByVal p_ELV_DEDUCTIONS As Decimal, ByVal p_ELV_bOpening As Boolean, _
      ByVal p_ELV_CARRYFORWARD As Integer, ByVal p_edit As Boolean, ByVal p_stTrans As SqlTransaction) As String

        '@return_value = [dbo].[SaveEMPLEAVE_D]
        '@ELV_ID = 0,
        '@ELV_BSU_ID = N'XXXXXX',
        '@ELV_EMP_ID = NULL,
        '@ELV_ELT_ID = N'AL',
        '@ELV_ELA_ID = NULL,
        '@ELV_DTFROM = N'12-DEC-2006',
        '@ELV_DTTO = N'12-DEC-2007',
        '@ELV_REMARKS = N'REMAAAA',
        '@ELV_SALARY = 0,
        '@ELV_LVSALARY = 0,
        '@ELV_ARREARS = 0,
        '@ELV_AIRTICKET = 0,
        '@ELV_DEDUCTIONS = 0,
        '@ELV_REJOINDT = NULL,
        '@ELV_bOpening = 1
        '@ELV_CARRYFORWARD

        Dim pParms(16) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ELV_ID", SqlDbType.Int)
        pParms(0).Value = p_ELV_ID
        pParms(1) = New SqlClient.SqlParameter("@ELV_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_ELV_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ELV_EMP_ID", SqlDbType.Int)
        pParms(2).Value = p_ELV_EMP_ID
        pParms(3) = New SqlClient.SqlParameter("@ELV_ELT_ID", SqlDbType.VarChar, 10)
        pParms(3).Value = p_ELV_ELT_ID
        pParms(4) = New SqlClient.SqlParameter("@ELV_ELA_ID", SqlDbType.Int)
        pParms(4).Value = System.DBNull.Value
        pParms(5) = New SqlClient.SqlParameter("@ELV_DTFROM", SqlDbType.DateTime)
        pParms(5).Value = p_ELV_DTFROM
        pParms(6) = New SqlClient.SqlParameter("@ELV_DTTO", SqlDbType.DateTime)
        pParms(6).Value = p_ELV_DTTO
        pParms(7) = New SqlClient.SqlParameter("@ELV_REMARKS", SqlDbType.VarChar, 200)
        pParms(7).Value = p_ELV_REMARKS

        pParms(8) = New SqlClient.SqlParameter("@ELV_SALARY", SqlDbType.Decimal, 21)
        pParms(8).Value = p_ELV_SALARY
        pParms(9) = New SqlClient.SqlParameter("@ELV_LVSALARY", SqlDbType.Decimal, 21)
        pParms(9).Value = p_ELV_LVSALARY
        pParms(10) = New SqlClient.SqlParameter("@ELV_ARREARS", SqlDbType.Decimal, 21)
        pParms(10).Value = p_ELV_ARREARS
        pParms(11) = New SqlClient.SqlParameter("@ELV_AIRTICKET", SqlDbType.Decimal, 21)
        pParms(11).Value = p_ELV_AIRTICKET
        pParms(12) = New SqlClient.SqlParameter("@ELV_DEDUCTIONS", SqlDbType.Decimal, 21)
        pParms(12).Value = p_ELV_DEDUCTIONS
        pParms(13) = New SqlClient.SqlParameter("@ELV_REJOINDT", SqlDbType.DateTime, 200)
        pParms(13).Value = System.DBNull.Value
        pParms(14) = New SqlClient.SqlParameter("@ELV_bOpening", SqlDbType.Bit)
        pParms(14).Value = p_ELV_bOpening
        pParms(15) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(15).Direction = ParameterDirection.ReturnValue
        pParms(16) = New SqlClient.SqlParameter("@ELV_CARRYFORWARD", SqlDbType.Int)
        pParms(16).Value = p_ELV_CARRYFORWARD
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPLEAVE_D", pParms)
        If pParms(8).Value = "0" Then
            SaveEMPLEAVE_D = pParms(8).Value
        Else
            SaveEMPLEAVE_D = "1000"
        End If
    End Function


    Public Shared Function SaveEMPATTENDANCE(ByVal p_eat_id As String, ByVal p_bsu_id As String, _
        ByVal p_EMP_ID As String, ByVal p_EAT_ELT_ID As String, ByVal p_EAT_DT As String, _
          ByVal p_EAT_REMARKS As String, ByVal p_EAT_DAYS As String, _
          ByVal p_edit As Boolean, ByVal p_stTrans As SqlTransaction) As String
        '@EAT_BSU_ID = N'xxxxxx',
        '		@EAT_EMP_ID = N'xxx',
        '		@EAT_ELT_ID = N'xxxxx',
        '		@EAT_DT = N'12 dec 2007',
        '		@EAT_ELV_ID = 1,
        '		@EAT_FLAG = 1,
        '		@bEdit = 0,
        '		@EAT_ID = 32 @EAT_REMARKS @EAT_DAYS


        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EAT_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_bsu_id
        pParms(1) = New SqlClient.SqlParameter("@EAT_EMP_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@EAT_ELT_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_EAT_ELT_ID
        pParms(3) = New SqlClient.SqlParameter("@EAT_DT", SqlDbType.DateTime)
        pParms(3).Value = p_EAT_DT
        pParms(4) = New SqlClient.SqlParameter("@EAT_ELV_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = System.DBNull.Value
        pParms(5) = New SqlClient.SqlParameter("@EAT_FLAG", SqlDbType.Bit)
        pParms(5).Value = 0
        pParms(6) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(6).Value = p_edit
        pParms(7) = New SqlClient.SqlParameter("@EAT_ID", SqlDbType.Int)
        pParms(7).Value = p_eat_id
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        pParms(9) = New SqlClient.SqlParameter("@EAT_REMARKS", SqlDbType.VarChar, 200)
        pParms(9).Value = p_EAT_REMARKS
        pParms(10) = New SqlClient.SqlParameter("@EAT_DAYS", SqlDbType.VarChar, 200)
        pParms(10).Value = p_EAT_DAYS
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPATTENDANCE", pParms)
        If pParms(8).Value = "0" Then
            SaveEMPATTENDANCE = pParms(8).Value
        Else
            SaveEMPATTENDANCE = "1000"
        End If

    End Function


    Public Shared Function DeleteEMPATTENDANCE(ByVal p_eat_id As String, ByVal p_bsu_id As String, _
    ByVal p_stTrans As SqlTransaction) As String

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EAT_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_bsu_id

        pParms(1) = New SqlClient.SqlParameter("@EAT_ID", SqlDbType.Int)
        pParms(1).Value = p_eat_id
        pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue

        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DeleteEMPATTENDANCE", pParms)

        DeleteEMPATTENDANCE = pParms(2).Value

    End Function


    Public Shared Function ProcessVacation(ByVal p_BSU_ID As String, ByVal p_LEAVE_FDATE As String, _
    ByVal p_LEAVE_TDATE As String, ByVal p_xEMPIDnAirfare As String, ByVal p_ELV_ELA_ID As String, _
    ByVal P_EMP_AIRFARE As String, ByVal p_stTrans As SqlTransaction, ByVal conn As SqlConnection) As String

        'Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As New SqlCommand("ProcessVacation", conn, p_stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        cmd.Parameters.Add(pParms(0))

        pParms(1) = New SqlClient.SqlParameter("@LEAVE_FDATE", SqlDbType.DateTime)
        pParms(1).Value = p_LEAVE_FDATE
        cmd.Parameters.Add(pParms(1))

        pParms(2) = New SqlClient.SqlParameter("@LEAVE_TDATE", SqlDbType.DateTime)
        pParms(2).Value = p_LEAVE_TDATE
        cmd.Parameters.Add(pParms(2))

        pParms(3) = New SqlClient.SqlParameter("@xEMPIDnAirfare", SqlDbType.Xml)
        pParms(3).Value = p_xEMPIDnAirfare
        cmd.Parameters.Add(pParms(3))

        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(pParms(4))

        pParms(5) = New SqlClient.SqlParameter("@ELV_ELA_ID", SqlDbType.Int)
        If p_ELV_ELA_ID = "" Then
            pParms(5).Value = System.DBNull.Value
        Else
            pParms(5).Value = p_ELV_ELA_ID
        End If
        cmd.Parameters.Add(pParms(5))

        pParms(6) = New SqlClient.SqlParameter("@EMP_AIRFARE", SqlDbType.VarChar, 100)
        pParms(6).Value = P_EMP_AIRFARE
        cmd.Parameters.Add(pParms(6))
        '@BSU_ID = N'125003',
        '@LEAVE_FDATE = N'12-FEBB-2008',
        '@LEAVE_TDATE = N'12-MARR-2008',
        '@xEMPIDnAirfare = N'12-FEBB-2008'
        Dim retval As Integer
        cmd.CommandTimeout = 0
        retval = cmd.ExecuteNonQuery()


        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "ProcessVacation", pParms)

        ProcessVacation = pParms(4).Value

    End Function


    Public Shared Function UpdateLeaveStatus(ByVal p_ELV_ID As String, ByVal p_ELV_REJOINDT As String, _
     ByVal p_stTrans As SqlTransaction, ByVal retMsg As String) As String

        Dim pParms(4) As SqlClient.SqlParameter   'V1.2
        pParms(0) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_ELV_ID

        pParms(1) = New SqlClient.SqlParameter("@ELA_REJOINDT", SqlDbType.DateTime)
        pParms(1).Value = p_ELV_REJOINDT

        pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue

        pParms(3) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
        pParms(3).Direction = ParameterDirection.Output

        '@BSU_ID = N'125003',
        '@LEAVE_FDATE = N'12-FEBB-2008',
        '@LEAVE_TDATE = N'12-MARR-2008',
        '@xEMPIDnAirfare = N'12-FEBB-2008'
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "UpdateLeaveStatus", pParms)
        retMsg = pParms(3).Value
        'retMsg = retMsg + "||" + pParms(2).Value.ToString
        Return retMsg
        UpdateLeaveStatus = pParms(2).Value
    End Function


    Public Shared Function DeleteEMPLEAVE_D(ByVal p_ELD_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ELV_ID", SqlDbType.Int)
        pParms(0).Value = p_ELD_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DeleteEMPLEAVE_D", pParms)
        DeleteEMPLEAVE_D = pParms(1).Value
    End Function


    Public Shared Function SaveAPPROVALPOLICY_H(ByVal p_LPS_ID As Integer, ByVal p_LPS_DOCTYPE As String, _
            ByVal p_LPS_DESCRIPTION As String, _
            ByVal p_LPS_BSU_ID As String, ByVal p_LPS_bActive As Boolean, ByVal p_LPS_bCheckAmount As Boolean, _
            ByRef NEW_LPS_ID As Integer, ByVal p_bEdit As Boolean, ByVal p_stTrans As SqlTransaction) As String
        '    	@LPS_ID = 0,
        '@LPS_DOCTYPE = N'LEAVE',
        '@LPS_DESCRIPTION = N'VET',
        '@LPS_BSU_ID = N'125003',
        '@LPS_bActive = 1,
        '@LPS_bCheckAmount = 0,
        '@NEW_LPS_ID = @NEW_LPS_ID OUTPUT
        '@bEdit = 0,

        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@LPS_ID", SqlDbType.Int)
        pParms(0).Value = p_LPS_ID
        pParms(1) = New SqlClient.SqlParameter("@LPS_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(1).Value = p_LPS_DOCTYPE
        pParms(2) = New SqlClient.SqlParameter("@LPS_DESCRIPTION", SqlDbType.VarChar, 100)
        pParms(2).Value = p_LPS_DESCRIPTION
        pParms(3) = New SqlClient.SqlParameter("@LPS_BSU_ID", SqlDbType.VarChar, 10)
        pParms(3).Value = p_LPS_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@LPS_bActive", SqlDbType.Bit)
        pParms(4).Value = p_LPS_bActive

        pParms(5) = New SqlClient.SqlParameter("@LPS_bCheckAmount", SqlDbType.Bit)
        pParms(5).Value = p_LPS_bCheckAmount
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@NEW_LPS_ID", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.Output
        pParms(8) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(8).Value = p_bEdit

        Dim retval As Integer = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.SaveAPPROVALPOLICY_H", pParms)
        If pParms(8).Value = 0 Then
            NEW_LPS_ID = pParms(7).Value
        End If
        SaveAPPROVALPOLICY_H = pParms(6).Value

    End Function


    Public Shared Function SaveAPPROVALPOLICY_D(ByVal p_LPD_ID As Integer, ByVal p_LPD_LPS_ID As Integer, _
                ByVal p_LPS_ORDERID As Integer, ByVal P_LPS_AMOUNT As Decimal, _
                ByVal p_LPD_DES_ID As Integer, ByVal p_LPD_BSU_ID As String, ByVal p_LPS_bREqPREVAPPROVAL As Boolean, _
                ByVal p_LPS_BREgHigherApproval As Integer, ByVal p_bEdit As Boolean, ByVal p_stTrans As SqlTransaction) As String
        '@return_value = [dbo].[SaveAPPROVALPOLICY_D]
        '@LPD_ID = 0 ,
        '@LPD_LPS_ID = 5,
        '@LPS_ORDERID = 1,
        '@LPD_DES_ID = 234,
        '@LPD_BSU_ID = N'125003',
        '@LPS_bREqPREVAPPROVAL = 0,
        '@LPS_BREgHigherApproval = 1,
        '@LPS_AMOUNT = 0,
        '@bEdit = 0
        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@LPD_ID", SqlDbType.Int)
        pParms(0).Value = p_LPD_ID
        pParms(1) = New SqlClient.SqlParameter("@LPD_LPS_ID", SqlDbType.Int)
        pParms(1).Value = p_LPD_LPS_ID
        pParms(2) = New SqlClient.SqlParameter("@LPS_ORDERID", SqlDbType.Int)
        pParms(2).Value = p_LPS_ORDERID
        pParms(3) = New SqlClient.SqlParameter("@LPD_DES_ID", SqlDbType.Int)
        pParms(3).Value = p_LPD_DES_ID
        pParms(4) = New SqlClient.SqlParameter("@LPD_BSU_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_LPD_BSU_ID

        pParms(5) = New SqlClient.SqlParameter("@LPS_bREqPREVAPPROVAL", SqlDbType.Bit)
        pParms(5).Value = p_LPS_bREqPREVAPPROVAL
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@LPS_BREgHigherApproval", SqlDbType.Int)
        pParms(7).Value = p_LPS_BREgHigherApproval
        pParms(8) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(8).Value = p_bEdit
        pParms(9) = New SqlClient.SqlParameter("@LPS_AMOUNT", SqlDbType.Decimal)
        pParms(9).Value = P_LPS_AMOUNT
        Dim retval As Integer = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.SaveAPPROVALPOLICY_D", pParms)

        SaveAPPROVALPOLICY_D = pParms(6).Value

    End Function

    Public Shared Function ProcessSalaryForTermination(ByVal p_BSUID As String, _
        ByVal p_PAYMONTH As Integer, ByVal p_PAYYEAR As Integer, ByVal p_EMPID As Integer, _
        ByVal p_ESD_bVaccation As Boolean, ByVal p_ESD_LEAVESALARY As Decimal, _
        ByVal p_ESD_AIRFARE As Decimal, ByVal p_ASONDT As DateTime, ByVal p_ESD_ERD_ID As Integer, _
        ByVal p_stTrans As SqlTransaction) As String
        '@return_value = [dbo].[ProcessSalaryForTermination]
        '@BSUID = N'125003',
        '@PAYMONTH = 9,
        '@PAYYEAR = 2008,
        '@EMPID = 1234 ,
        '@ESD_bVaccation = 0,
        '@ESD_LEAVESALARY = 300,
        '@ESD_AIRFARE = 100,
        '@ASONDT = N'12-mar-2008',
        '@ESD_ERD_ID = -1 
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSUID
        pParms(1) = New SqlClient.SqlParameter("@PAYMONTH", SqlDbType.Int)
        pParms(1).Value = p_PAYMONTH
        pParms(2) = New SqlClient.SqlParameter("@PAYYEAR", SqlDbType.Int)
        pParms(2).Value = p_PAYYEAR
        pParms(3) = New SqlClient.SqlParameter("@EMPID", SqlDbType.Int)
        pParms(3).Value = p_EMPID
        pParms(4) = New SqlClient.SqlParameter("@ESD_bVaccation", SqlDbType.Bit)
        pParms(4).Value = p_ESD_bVaccation
        pParms(5) = New SqlClient.SqlParameter("@ESD_LEAVESALARY", SqlDbType.Decimal)
        pParms(5).Value = p_ESD_LEAVESALARY
        pParms(6) = New SqlClient.SqlParameter("@ESD_AIRFARE", SqlDbType.Decimal)
        pParms(6).Value = p_ESD_AIRFARE
        pParms(7) = New SqlClient.SqlParameter("@ASONDT", SqlDbType.DateTime)
        pParms(7).Value = p_ASONDT
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        pParms(9) = New SqlClient.SqlParameter("@ESD_ERD_ID", SqlDbType.Int)
        pParms(9).Value = p_ESD_ERD_ID
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "ProcessSalaryForTermination", pParms)
        ProcessSalaryForTermination = pParms(8).Value
    End Function

    Public Shared Function SaveEMPTERMINATION_D_s(ByVal p_EDD_ID As Integer, ByVal p_EDD_ERD_ID As Integer, _
            ByVal p_EDD_ERN_ID As String, _
            ByVal p_EDD_DESCRIPTION As String, ByVal p_EDD_BDEDUCTION As Boolean, _
            ByVal p_EDD_AMOUNT As Decimal, ByVal p_EREG_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        '      [dbo].[SaveEMPTERMINATION_D_s] 
        '@EDD_ID = 12 ,
        '@EDD_ERD_ID = 3,
        '@EDD_ERN_ID = N'AL',
        '@EDD_DESCRIPTION = N'TEST DATA',
        '@EDD_BDEDUCTION = 1,
        '@EDD_AMOUNT = 3200

        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EDD_ID", SqlDbType.Int)
        pParms(0).Value = p_EDD_ID
        pParms(1) = New SqlClient.SqlParameter("@EDD_ERD_ID", SqlDbType.Int)
        pParms(1).Value = p_EDD_ERD_ID
        pParms(2) = New SqlClient.SqlParameter("@EDD_ERN_ID", SqlDbType.VarChar, 10)
        pParms(2).Value = p_EDD_ERN_ID
        pParms(3) = New SqlClient.SqlParameter("@EDD_DESCRIPTION", SqlDbType.VarChar, 200)
        pParms(3).Value = p_EDD_DESCRIPTION
        pParms(4) = New SqlClient.SqlParameter("@EDD_BDEDUCTION", SqlDbType.Bit)
        pParms(4).Value = p_EDD_BDEDUCTION

        pParms(5) = New SqlClient.SqlParameter("@EDD_AMOUNT", SqlDbType.Decimal)
        pParms(5).Value = p_EDD_AMOUNT

        pParms(6) = New SqlClient.SqlParameter("@EREG_ID", SqlDbType.Decimal)
        pParms(6).Value = p_EREG_ID

        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.SaveEMPTERMINATION_D_s", pParms)

        SaveEMPTERMINATION_D_s = pParms(7).Value

    End Function


    Public Shared Function SaveEMPTERMINATION_D(ByVal p_ERD_ID As Integer, ByVal p_ERD_BSU_ID As String, _
        ByVal p_ERD_EMP_ID As Integer, ByVal p_ERD_BRESIGN As Boolean, _
        ByVal p_ERD_DTFROM As String, ByVal p_ERD_REMARKS As String, _
        ByVal p_ERD_REMARKS1 As String, ByVal p_ERD_SALARY As Decimal, _
        ByVal p_ERD_LVSALARY As Decimal, ByVal p_ERD_ARREARS As Decimal, _
        ByVal p_ERD_GRATUITY As Decimal, ByVal p_ERD_AIRTICKET As Decimal, _
        ByVal p_ERD_TKTCOUNT As Integer, ByVal p_ERD_DEDUCTIONS As Decimal, _
        ByVal p_ERD_BASSETSRETURN As Boolean, ByVal p_ERD_BADVANCESSETTLED As Boolean, _
       ByRef NEW_ERD_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        ' @return_value = [dbo].[SaveEMPTERMINATION_D]
        '@ERD_ID = 0X,
        '@ERD_BSU_ID = N'125003',
        '@ERD_EMP_ID = 123,
        '@ERD_BRESIGN = 1,
        '@ERD_DTFROM = N'12-MAR-2008',

        Dim pParms(18) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ERD_ID", SqlDbType.Int)
        pParms(0).Value = p_ERD_ID
        pParms(1) = New SqlClient.SqlParameter("@ERD_BSU_ID", SqlDbType.VarChar, 10)
        pParms(1).Value = p_ERD_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ERD_EMP_ID", SqlDbType.Int)
        pParms(2).Value = p_ERD_EMP_ID
        pParms(3) = New SqlClient.SqlParameter("@ERD_BRESIGN", SqlDbType.Bit)
        pParms(3).Value = p_ERD_BRESIGN
        pParms(4) = New SqlClient.SqlParameter("@ERD_DTFROM", SqlDbType.DateTime)
        pParms(4).Value = p_ERD_DTFROM

        '@ERD_REMARKS = N'TEST',
        '@ERD_REMARKS1 = N'TEST1',
        '@ERD_SALARY = 1000,
        '@ERD_LVSALARY = 1500,
        '@ERD_ARREARS = 200,
        '@ERD_GRATUITY = 4000,

        pParms(5) = New SqlClient.SqlParameter("@ERD_REMARKS", SqlDbType.VarChar, 200)
        pParms(5).Value = p_ERD_REMARKS
        pParms(6) = New SqlClient.SqlParameter("@ERD_REMARKS1", SqlDbType.VarChar, 200)
        pParms(6).Value = p_ERD_REMARKS1
        pParms(7) = New SqlClient.SqlParameter("@ERD_SALARY", SqlDbType.Decimal)
        pParms(7).Value = p_ERD_SALARY
        pParms(8) = New SqlClient.SqlParameter("@ERD_LVSALARY", SqlDbType.Decimal)
        pParms(8).Value = p_ERD_LVSALARY
        pParms(9) = New SqlClient.SqlParameter("@ERD_ARREARS", SqlDbType.Decimal)
        pParms(9).Value = p_ERD_ARREARS
        pParms(10) = New SqlClient.SqlParameter("@ERD_GRATUITY", SqlDbType.Decimal)
        pParms(10).Value = p_ERD_GRATUITY

        '@ERD_AIRTICKET = 100,
        '@ERD_TKTCOUNT = 3,
        '@ERD_DEDUCTIONS = 12,
        '@ERD_BASSETSRETURN = 1,
        '@ERD_BADVANCESSETTLED = 1,
        '@NEW_ERD_ID = @NEW_ERD_ID OUTPUT

        pParms(11) = New SqlClient.SqlParameter("@ERD_AIRTICKET", SqlDbType.Decimal)
        pParms(11).Value = p_ERD_AIRTICKET
        pParms(12) = New SqlClient.SqlParameter("@ERD_TKTCOUNT", SqlDbType.Int)
        pParms(12).Value = p_ERD_TKTCOUNT
        pParms(13) = New SqlClient.SqlParameter("@ERD_DEDUCTIONS", SqlDbType.Decimal)
        pParms(13).Value = p_ERD_DEDUCTIONS
        pParms(14) = New SqlClient.SqlParameter("@ERD_BASSETSRETURN", SqlDbType.Bit)
        pParms(14).Value = p_ERD_BASSETSRETURN
        pParms(15) = New SqlClient.SqlParameter("@ERD_BADVANCESSETTLED", SqlDbType.Bit)
        pParms(15).Value = p_ERD_BADVANCESSETTLED

        pParms(16) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(16).Direction = ParameterDirection.ReturnValue
        pParms(17) = New SqlClient.SqlParameter("@NEW_LPS_ID", SqlDbType.Int)
        pParms(17).Direction = ParameterDirection.Output


        Dim retval As Integer = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.SaveEMPTERMINATION_D", pParms)
        If pParms(16).Value = 0 Then
            NEW_ERD_ID = pParms(17).Value
        End If
        SaveEMPTERMINATION_D = pParms(16).Value

    End Function

    Public Shared Function SaveEMPLEAVEAPP(ByVal p_ELA_ID As String, ByVal p_ELA_BSU_ID As String, ByVal p_ELA_EMP_ID As String, _
         ByVal p_ELA_ELT_ID As String, ByVal p_ELA_DTFROM As Date, ByVal p_ELA_DTTO As Date, _
         ByVal p_ELA_REMARKS As String, ByVal p_ELA_APPRSTATUS As String, ByVal p_ELA_STATUS As Integer, _
         ByVal p_ELA_ADDRESS As String, ByVal p_ELA_PHONE As String, ByVal p_ELA_MOBILE As String, ByVal p_ELA_Pobox As String, _
         ByVal p_APS_bFORWARD As Boolean, ByVal p_Edit As Boolean, ByVal p_stTrans As SqlTransaction, ByVal p_user As String, ByVal p_handovertxt As String) As String
        '  @return_value = [dbo].[SaveEMPLEAVEAPP]
        '@ELA_ID = 0,
        '@ELA_BSU_ID = N'XXXXXX',
        '@ELA_EMP_ID = N'123',
        '@ELA_ELT_ID = N'6',
        '@ELA_DTFROM = N'12/12/2007',
        '@ELA_DTTO = N'12/12/2007',
        '@ELA_REMARKS = N'veruthe',
        '@ELA_APPRSTATUS = 0,
        '@ELA_STATUS = 0,
        '@bEdit = 0

        ' Dim pParms(16) As SqlClient.SqlParameter ''V1.1 comment
        ' Dim pParms(17) As SqlClient.SqlParameter 'V1.1  V1.2 comment
        Dim pParms(19) As SqlClient.SqlParameter 'V1.2 ,V1.3
        pParms(0) = New SqlClient.SqlParameter("@ELA_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_ELA_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ELA_EMP_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_ELA_EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@ELA_ELT_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_ELA_ELT_ID
        pParms(3) = New SqlClient.SqlParameter("@ELA_DTFROM", SqlDbType.DateTime)
        pParms(3).Value = p_ELA_DTFROM
        pParms(4) = New SqlClient.SqlParameter("@ELA_DTTO", SqlDbType.DateTime)
        pParms(4).Value = p_ELA_DTTO
        pParms(5) = New SqlClient.SqlParameter("@ELA_APPRSTATUS", SqlDbType.VarChar, 1)
        pParms(5).Value = p_ELA_APPRSTATUS
        pParms(6) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
        pParms(6).Value = p_Edit
        pParms(7) = New SqlClient.SqlParameter("@ELA_STATUS", SqlDbType.Int)
        pParms(7).Value = p_ELA_STATUS
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        pParms(9) = New SqlClient.SqlParameter("@ELA_REMARKS", SqlDbType.VarChar, 200)
        pParms(9).Value = p_ELA_REMARKS
        pParms(10) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.Int)
        pParms(10).Value = p_ELA_ID
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction 
        '@ELA_ADDRESS,@ELA_PHONE ,@ELA_MOBILE ,@ELA_PIN )@APS_bFORWARD
        pParms(11) = New SqlClient.SqlParameter("@ELA_ADDRESS", SqlDbType.VarChar, 200)
        'pParms(11).Value = p_ELA_REMARKS   'V1.1  Bug fix
        pParms(11).Value = p_ELA_ADDRESS
        pParms(12) = New SqlClient.SqlParameter("@ELA_PHONE", SqlDbType.VarChar, 50)
        pParms(12).Value = p_ELA_PHONE
        pParms(13) = New SqlClient.SqlParameter("@ELA_MOBILE", SqlDbType.VarChar, 50)
        pParms(13).Value = p_ELA_MOBILE
        pParms(14) = New SqlClient.SqlParameter("@ELA_POBOX", SqlDbType.VarChar, 50)
        pParms(14).Value = p_ELA_Pobox
        pParms(15) = New SqlClient.SqlParameter("@APS_bFORWARD", SqlDbType.Bit)
        pParms(15).Value = p_APS_bFORWARD

        pParms(16) = New SqlClient.SqlParameter("@UserName", SqlDbType.VarChar, 20)
        pParms(16).Value = p_user
        pParms(17) = New SqlClient.SqlParameter("@Handovertxt", SqlDbType.VarChar, 200)
        pParms(17).Value = p_handovertxt
        pParms(18) = New SqlClient.SqlParameter("@RetMsg", SqlDbType.VarChar, 100)
        pParms(18).Direction = ParameterDirection.Output
        pParms(18).Value = ""
        Dim retval As String
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPLEAVEAPP", pParms)

        'SaveEMPLEAVEAPP = pParms(8).Value

        SaveEMPLEAVEAPP = pParms(8).Value.ToString + "||" + pParms(18).Value.ToString

    End Function
     


    Public Shared Function EmployeeTermination(ByVal p_BSU_ID As String, ByVal p_TERM_DATE As String, _
                  ByVal p_EMP_ID As Integer, ByVal p_ERD_REMARKS As String, ByVal p_ERD_REMARKS1 As String, _
                  ByVal p_ERD_DEDUCTIONS As Decimal, ByVal p_ERD_BASSETSRETURN As Boolean, _
                  ByVal p_ERD_BADVANCESSETTLED As Boolean, ByVal p_bIStermination As Boolean, _
                  ByRef NEW_ERD_ID As Integer, ByVal p_ERD_OTHEARNINGS As Decimal, ByVal p_EREG_ID As Integer, ByVal p_GratuityDetail As String, ByVal AlsoApprove As Boolean, ByVal p_stTrans As SqlTransaction) As String
        'EXEC	@return_value = [dbo].[EmployeeTermination]
        '@BSU_ID = N'125003',
        '@TERM_DATE = N'12-mar-2008',
        '@EMP_ID = 2 ,
        '@ERD_REMARKS = N'test',
        '@ERD_REMARKS1 = N'test11',
        '@ERD_DEDUCTIONS = 2000,
        '@ERD_BASSETSRETURN = 1,
        '@ERD_BADVANCESSETTLED = 1,
        '@bIStermination = 1,
        '@NEW_ERD_ID = @NEW_ERD_ID OUTPUT
        '@ERD_OTHEARNINGS NUMERIC(18,3),


        Dim pParms(14) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 10)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@TERM_DATE", SqlDbType.DateTime)
        pParms(1).Value = p_TERM_DATE
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
        pParms(2).Value = p_EMP_ID
        pParms(3) = New SqlClient.SqlParameter("@ERD_REMARKS", SqlDbType.VarChar, 200)
        pParms(3).Value = p_ERD_REMARKS
        pParms(4) = New SqlClient.SqlParameter("@ERD_REMARKS1", SqlDbType.VarChar, 200)
        pParms(4).Value = p_ERD_REMARKS1

        pParms(5) = New SqlClient.SqlParameter("@ERD_DEDUCTIONS", SqlDbType.Decimal)
        pParms(5).Value = p_ERD_DEDUCTIONS
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@NEW_ERD_ID", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.Output
        pParms(8) = New SqlClient.SqlParameter("@ERD_BASSETSRETURN", SqlDbType.Bit)
        pParms(8).Value = p_ERD_BASSETSRETURN
        pParms(9) = New SqlClient.SqlParameter("@ERD_BADVANCESSETTLED", SqlDbType.Bit)
        pParms(9).Value = p_ERD_BADVANCESSETTLED
        pParms(10) = New SqlClient.SqlParameter("@bIStermination", SqlDbType.Bit)
        pParms(10).Value = p_bIStermination
        pParms(11) = New SqlClient.SqlParameter("@ERD_OTHEARNINGS", SqlDbType.Decimal)
        pParms(11).Value = p_ERD_OTHEARNINGS
        pParms(12) = New SqlClient.SqlParameter("@EREG_ID", SqlDbType.Int)
        pParms(12).Value = p_EREG_ID
        pParms(13) = New SqlClient.SqlParameter("@Approve", SqlDbType.Bit)
        pParms(13).Value = AlsoApprove
        pParms(14) = New SqlClient.SqlParameter("@GratuityDetail", SqlDbType.VarChar)
        pParms(14).Value = p_GratuityDetail
        Dim retval As Integer = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbPayroll & ".dbo.EmployeeTermination", pParms)
        If pParms(6).Value = 0 Then
            NEW_ERD_ID = pParms(7).Value
        End If
        EmployeeTermination = pParms(6).Value

    End Function


    Public Shared Function FindLeaveStatus(ByVal p_BSU_ID As String, ByVal p_ESD_ID As String, _
    ByVal p_PAYMONTH As Integer, ByVal p_PAYYEAR As Integer, ByVal p_EMPID As String, ByVal p_ASONDT As String) As String

        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ESD_ID", SqlDbType.Int)
        pParms(1).Value = p_ESD_ID
        pParms(2) = New SqlClient.SqlParameter("@PAYMONTH", SqlDbType.Int)
        pParms(2).Value = p_PAYMONTH
        pParms(3) = New SqlClient.SqlParameter("@PAYYEAR", SqlDbType.Int)
        pParms(3).Value = p_PAYYEAR
        pParms(4) = New SqlClient.SqlParameter("@ASONDT", SqlDbType.DateTime)
        pParms(4).Value = p_ASONDT
        pParms(5) = New SqlClient.SqlParameter("@EMPID", SqlDbType.Int)
        pParms(5).Value = p_PAYYEAR
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue

        'EXEC	@return_value = [dbo].[FindLeaveStatus]
        '@BSU_ID = N'125003',
        '@ESD_ID = 0,
        '@PAYMONTH = 1,
        '@PAYYEAR = 2007,
        '@EMPID = 12,
        '@ASONDT = N'1-mar-2008'
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString, _
        CommandType.StoredProcedure, "FindLeaveStatus", pParms)

        FindLeaveStatus = pParms(6).Value
    End Function


    Public Shared Function DeleteEMPSALARYDATA(ByVal p_ELD_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        'EXEC	@return_value = [dbo].[DeleteEMPSALARYDATA]
        '		@ESD_ID = 192
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ESD_ID", SqlDbType.Int)
        pParms(0).Value = p_ELD_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DeleteEMPSALARYDATA", pParms)
        DeleteEMPSALARYDATA = pParms(1).Value
    End Function

    Public Shared Function SaveBANK_M(ByVal p_BNK_ID As String, _
        ByVal p_BNK_DESCRIPTION As String, ByVal p_BNK_SALTRFNARRATION As String, _
        ByVal p_BNK_SHORT As String, ByVal p_BNK_SHORTNAME As String, _
        ByVal p_BNK_HEADOFFICE As String, ByVal p_stTrans As SqlTransaction) As String
        '     EXEC	@return_value = [dbo].[SaveBANK_M]
        '@BNK_ID = 0,
        '@BNK_DESCRIPTION = N'test',
        '@BNK_SALTRFNARRATION = N'bankil itto',
        '@BNK_SHORT = N'bnt',
        '@BNK_SHORTNAME = N'bt',
        '@BNK_HEADOFFICE = N'karamaa'

        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BNK_ID", SqlDbType.Int)
        pParms(0).Value = p_BNK_ID
        pParms(1) = New SqlClient.SqlParameter("@BNK_DESCRIPTION", SqlDbType.VarChar, 100)
        pParms(1).Value = p_BNK_DESCRIPTION
        pParms(2) = New SqlClient.SqlParameter("@BNK_SALTRFNARRATION", SqlDbType.VarChar, 400)
        pParms(2).Value = p_BNK_SALTRFNARRATION
        pParms(3) = New SqlClient.SqlParameter("@BNK_SHORT", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BNK_SHORT
        pParms(4) = New SqlClient.SqlParameter("@BNK_SHORTNAME", SqlDbType.VarChar, 100)
        pParms(4).Value = p_BNK_SHORTNAME
        pParms(5) = New SqlClient.SqlParameter("@BNK_HEADOFFICE", SqlDbType.VarChar, 100)
        pParms(5).Value = p_BNK_HEADOFFICE

        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue

        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveBANK_M", pParms)

        SaveBANK_M = pParms(6).Value


    End Function


    Public Shared Function DeleteBANK_M(ByVal p_BNK_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        'EXEC	@return_value = [dbo].[DeleteEMPSALARYDATA]
        '		@ESD_ID = 192
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BNK_ID", SqlDbType.Int)
        pParms(0).Value = p_BNK_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DeleteBANK_M", pParms)
        DeleteBANK_M = pParms(1).Value
    End Function


End Class
