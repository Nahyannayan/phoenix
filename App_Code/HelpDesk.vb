Imports Microsoft.VisualBasic
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports EmailService
Imports SmsService
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail

Public Class HelpDesk

    Public Shared Function BindTaskSource(ByVal ddSource As DropDownList) As DropDownList
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  TASK_SOURCE_MASTER order by TASK_SOURCE_ID"

        ddSource.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddSource.DataTextField = "TASK_SOURCE_DESCRIPTION"
        ddSource.DataValueField = "TASK_SOURCE_ID"
        ddSource.DataBind()

        Return ddSource

    End Function


    Public Shared Function BindPriority(ByVal ddPriority As DropDownList) As DropDownList
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "SELECT RECORD_ID,PRIORITY_DESCRIPTION FROM TASK_PRIORITY_MASTER"

        ddPriority.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddPriority.DataTextField = "PRIORITY_DESCRIPTION"
        ddPriority.DataValueField = "RECORD_ID"
        ddPriority.DataBind()

        Return ddPriority

    End Function

    Public Shared Function BindBsu(ByVal dsBsu As DropDownList) As DropDownList
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"

        dsBsu.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        dsBsu.DataTextField = "BSU_NAME"
        dsBsu.DataValueField = "BSU_ID"
        dsBsu.DataBind()

        Return dsBsu

    End Function

    Public Shared Function BindTaskCategory(ByVal Sub_Tab_Id As String, ByVal TreeJobCategory As TreeView, ByVal BindGeneralCategory As Boolean, ByVal collapse As Boolean) As TreeView
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select TASK_CATEGORY_ID,CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION from  TASK_CATEGORY_MASTER A INNER JOIN dbo.TASK_CATEGORY B ON B.ID=A.CATEGORY_ID where PARENT_TASK_CATEGORY_ID=0 AND SUB_TAB_ID='" & Sub_Tab_Id & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_DESCRIPTION").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                TreeJobCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeJobCategory.Nodes
            BindChildNodes(Sub_Tab_Id, node)
        Next

        If BindGeneralCategory Then
            Dim GNode As New TreeNode
            GNode.Text = "General Category"
            GNode.Value = "-1"
            TreeJobCategory.Nodes.Add(GNode)
        End If

        If collapse Then
            TreeJobCategory.CollapseAll()
        End If

        Return TreeJobCategory

    End Function

    Public Shared Sub BindChildNodes(ByVal Sub_Tab_Id As String, ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select TASK_CATEGORY_ID,CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION from  TASK_CATEGORY_MASTER A INNER JOIN dbo.TASK_CATEGORY B ON B.ID=A.CATEGORY_ID where PARENT_TASK_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & Sub_Tab_Id & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_DESCRIPTION").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(Sub_Tab_Id, node)
        Next


    End Sub

    Public Shared Function BindEmp(ByVal bsu As String, ByVal ddEmp As DropDownList) As DropDownList
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select EMP_ID, isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) as EMP_NAME,emp_displayname from dbo.EMPLOYEE_M " & _
                        " where EMP_BSU_ID='" & bsu & "' and EMP_STATUS IN (1,2)  order by EMP_FNAME  "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddEmp.DataSource = ds
            ddEmp.DataTextField = "EMP_NAME"
            ddEmp.DataValueField = "EMP_ID"
            ddEmp.DataBind()
        End If

        Return ddEmp

    End Function

    Public Shared Function GetTreeNodeValue(ByVal TreeTaskCategory As TreeView) As String
        Dim val = ""

        For Each node As TreeNode In TreeTaskCategory.Nodes
            If node.Checked Then
                val = node.Value
                node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)
                If val <> "" Then
                    Exit For
                End If
            End If
        Next

        Return val

    End Function

    Public Shared Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Public Shared Function GetTreeNodeText(ByVal TreeNodeValue As String) As String

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim jcatogory = ""
        If TreeNodeValue = "" Or TreeNodeValue = "-1" Then

            jcatogory = "General Category"
        Else

            Dim Sql_Query = "Select CATEGORY_DES from  TASK_CATEGORY_MASTER A INNER JOIN dbo.TASK_CATEGORY B ON B.ID=A.CATEGORY_ID where TASK_CATEGORY_ID='" & TreeNodeValue & "'"
            jcatogory = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)

        End If

        Return jcatogory

    End Function

    Public Shared Function GetOwnerAssignedTaskCategory(ByVal Main_Tab_id As String, ByVal Sub_Tab_id As String, ByVal bsu_id As String) As DataSet
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim str_query = " select DISTINCT C.TASK_CATEGORY_ID,CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION from dbo.SUB_TAB_MASTER A " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER B ON A.MAIN_TAB_ID= B.MAIN_TAB_ID " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER C ON C.SUB_TAB_ID = A.SUB_TAB_ID " & _
                        " INNER JOIN TASK_CATEGORY C1 ON C1.ID=C.CATEGORY_ID " & _
                        " INNER JOIN dbo.TASK_ROOTING_MASTER D ON A.MAIN_TAB_ID =D.MAIN_TAB_ID " & _
                        " AND C.SUB_TAB_ID=D.SUB_TAB_ID AND C.TASK_CATEGORY_ID=D.TASK_CATEGORY_ID " & _
                        " WHERE TASK_CATEGORY_OWNER='True' AND " & _
                        " D.MAIN_TAB_ID ='" & Main_Tab_id & "' AND " & _
                        " D.SUB_TAB_ID='" & Sub_Tab_id & "' AND " & _
                        " EMP_TASK_BSU_ID='" & bsu_id & "' "

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Return ds

    End Function

    Public Shared Sub SaveEmailNotificationLog(ByVal MAIN_TAB_ID As String, ByVal SUB_TAB_ID As String, ByVal Task_list_id As String, ByVal emp_id As String, ByVal status As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", Task_list_id)
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
        pParms(2) = New SqlClient.SqlParameter("@STATUS", status)
        pParms(3) = New SqlClient.SqlParameter("@SUB_TAB_ID", SUB_TAB_ID)
        pParms(4) = New SqlClient.SqlParameter("@MAIN_TAB_ID", MAIN_TAB_ID) '' 1 Internal , 2 External
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_TASK_EMAIL_NOTIFICATION_LOG", pParms)

    End Sub

    Public Shared Sub SaveSmsNotificationLog(ByVal MAIN_TAB_ID As String, ByVal SUB_TAB_ID As String, ByVal Task_list_id As String, ByVal emp_id As String, ByVal status As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", Task_list_id)
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
        pParms(2) = New SqlClient.SqlParameter("@STATUS", status)
        pParms(3) = New SqlClient.SqlParameter("@SUB_TAB_ID", SUB_TAB_ID)
        pParms(4) = New SqlClient.SqlParameter("@MAIN_TAB_ID", MAIN_TAB_ID) '' 1 Internal , 2 External
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_TASK_SMS_NOTIFICATION_LOG", pParms)

    End Sub


    Public Shared Function GetCommunicationSettings() As DataSet


        Dim ds As DataSet

        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query =  "select * from COMMUNICATION_MASTER where ID='1'"

        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)

        Return ds

    End Function
    Public Shared Sub SendEmailSmsNotification(ByVal MAIN_TAB_ID As String, ByVal SUB_TAB_ID As String, ByVal comBsuid As String, ByVal rootBsuId As String, ByVal rootBsuText As String, ByVal ReportedEmpText As String, ByVal TASK_LIST_ID As String, ByVal TASK_CATEGORY_ID As String, ByVal TASK_CATEGORY As String, ByVal PRIORITY As String, ByVal REQ_DATE As String, ByVal TASK_DESCRIPTION As String, ByVal Title As String)

        Try

            Dim ds As DataSet
            Dim Sql_Query = ""
            ds = GetCommunicationSettings()

            Dim username = ""
            Dim password = ""
            Dim port = ""
            Dim host = ""
            Dim fromemailid = ""

            If ds.Tables(0).Rows.Count > 0 Then
                username = ds.Tables(0).Rows(0).Item("USERNAME").ToString()
                password = ds.Tables(0).Rows(0).Item("PASSWORD").ToString()
                port = ds.Tables(0).Rows(0).Item("PORT").ToString()
                host = ds.Tables(0).Rows(0).Item("HOST").ToString()
                fromemailid = ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString()
            End If

            Dim ds1 As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

            Sql_Query = "select DISTINCT A.EMP_ID,EMD_CUR_MOBILE,EMD_PERM_MOBILE, ISNULL(EMAIL_NOTIFY,'False')EMAIL_NOTIFY, ISNULL(SMS_NOTIFY,'False')SMS_NOTIFY, EMD_EMAIL " & _
                            " from dbo.EMPLOYEE_M  A " & _
                            " inner join OASIS_HELP_DESK.dbo.TASK_ROOTING_MASTER B on A.EMP_ID=B.EMP_ID " & _
                            " inner join dbo.EMPLOYEE_D C on A.EMP_ID = C.EMD_EMP_ID " & _
                            " where TASK_CATEGORY_OWNER='True' AND TASK_CATEGORY_ID='" & TASK_CATEGORY_ID & "' AND (EMAIL_NOTIFY ='True' or  SMS_NOTIFY='True') " & _
                            " AND EMP_bACTIVE='True' AND MAIN_TAB_ID='" & MAIN_TAB_ID & "' AND SUB_TAB_ID='" & SUB_TAB_ID & "' AND EMP_TASK_BSU_ID='" & rootBsuId & "' "

            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            If ds1.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds1.Tables(0).Rows.Count - 1

                    Dim ToEmailId = ds1.Tables(0).Rows(i).Item("EMD_EMAIL").ToString()
                    Dim emp_id = ds1.Tables(0).Rows(i).Item("EMP_ID").ToString()
                    Dim email = ds1.Tables(0).Rows(i).Item("EMAIL_NOTIFY")
                    Dim sms = ds1.Tables(0).Rows(i).Item("SMS_NOTIFY")
                    Dim mobileNumber = ds1.Tables(0).Rows(i).Item("EMD_CUR_MOBILE").ToString()


                    If ds.Tables(0).Rows.Count > 0 Then
                        If email Then

                            str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
                            Dim sb As New Object
                            Dim pParms(3) As SqlClient.SqlParameter
                            pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", TASK_LIST_ID)
                            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
                            pParms(2) = New SqlClient.SqlParameter("@OPTION", 2)
                            sb = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms)

                            Dim status = SendPlainTextEmails(fromemailid, ToEmailId, TASK_LIST_ID & " - " & TASK_CATEGORY & " - " & Title, sb.ToString(), username, password, host, port, GetuploadIds(TASK_LIST_ID, 1))
                            SaveEmailNotificationLog(MAIN_TAB_ID, SUB_TAB_ID, TASK_LIST_ID, emp_id, status)

                        End If
                    End If


                    If sms Then

                        str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
                        Dim qry = "SELECT TASK_TITLE FROM TASK_LIST_MASTER WHERE TASK_LIST_ID='" & TASK_LIST_ID & "'"


                        Dim message = "New Task List ID : " & TASK_LIST_ID & " , Category : " & TASK_CATEGORY & _
                                      ", Reported By : " & ReportedEmpText & _
                                      ", Subject : " & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, qry) & _
                                      ", Priority :" & PRIORITY & _
                                      ". GEMS-Helpdesk"
                        Dim status = SmsService.sms.SendMessage(mobileNumber, message, "Helpdesk", Web.Configuration.WebConfigurationManager.AppSettings("smsUsername").ToString(), Web.Configuration.WebConfigurationManager.AppSettings("smspwd").ToString())
                        SaveSmsNotificationLog(MAIN_TAB_ID, SUB_TAB_ID, TASK_LIST_ID, emp_id, status)

                    End If


                Next

            End If


            '' Send Task Reference No to Client
            SendClientEmail(TASK_LIST_ID)


        Catch ex As Exception



        End Try

    End Sub

    Public Shared Sub SendClientEmail(ByVal task_list_id As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " select A.TASK_ID,A.TASK_LIST_ID,TASK_TITLE,TASK_DESCRIPTION,TASK_TRAGET_DATE,A.UPLOAD_IDS,PRIORITY_DESCRIPTION, " & _
                          " ISNULL(EMD_EMAIL,B.EMAIL)EMAIL,CATEGORY_DES " & _
                          " from dbo.TASK_LIST_MASTER A " & _
                          " INNER JOIN dbo.TASK_CONTACT_MASTER B ON A.TASK_ID= B.TASK_ID " & _
                          " INNER JOIN dbo.TASK_PRIORITY_MASTER C ON A.TASK_PRIORITY= C.RECORD_ID " & _
                          " LEFT JOIN dbo.TASK_CATEGORY_MASTER D ON D.TASK_CATEGORY_ID=A.TASK_CATEGORY " & _
                          " LEFT JOIN dbo.TASK_CATEGORY E ON E.ID=D.CATEGORY_ID " & _
                          " LEFT JOIN OASIS.dbo.EMPLOYEE_M F ON F.EMP_ID= B.REPORTED_EMP_ID " & _
                          " LEFT JOIN OASIS.dbo.EMPLOYEE_D G ON G.EMD_EMP_ID=F.EMP_ID " & _
                          " WHERE A.TASK_LIST_ID='" & task_list_id & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim refernecid As String = ds.Tables(0).Rows(0).Item("TASK_LIST_ID").ToString()
            Dim title As String = ds.Tables(0).Rows(0).Item("TASK_TITLE").ToString()
            Dim email As String = ds.Tables(0).Rows(0).Item("EMAIL").ToString()
            Dim uploadid As String = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            Dim category_des As String = ds.Tables(0).Rows(0).Item("CATEGORY_DES").ToString()


            If email <> "" Then
                Dim sb As New StringBuilder

                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", task_list_id)
                pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)
                sb.Append(SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms))


                ds = GetCommunicationSettings()

                SendPlainTextEmails(ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString(), email, task_list_id & " - " & category_des & " - " & title, sb.ToString(), ds.Tables(0).Rows(0).Item("USERNAME").ToString(), ds.Tables(0).Rows(0).Item("PASSWORD").ToString(), ds.Tables(0).Rows(0).Item("HOST").ToString(), ds.Tables(0).Rows(0).Item("PORT").ToString(), uploadid)

            End If

        End If

    End Sub

    Public Shared Sub SendClosingReopeningEmails(ByVal task_list_id As String, ByVal des As String, ByVal status As String, ByVal reportedempid As String, ByVal logger As String, ByVal loggerid As String, ByVal suploadids As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " select A.TASK_ID,A.TASK_LIST_ID,TASK_TITLE,CATEGORY_DES,TASK_DESCRIPTION,TASK_TRAGET_DATE,A.UPLOAD_IDS,PRIORITY_DESCRIPTION, " & _
                        " REPORTED_EMP_ID,EMAIL,CLAIM_EMP_ID,B.TASK_DATE_TIME,ENTRY_EMP_ID " & _
                        " from dbo.TASK_LIST_MASTER A " & _
                        " INNER JOIN dbo.TASK_CONTACT_MASTER B ON A.TASK_ID= B.TASK_ID " & _
                        " INNER JOIN dbo.TASK_PRIORITY_MASTER C ON A.TASK_PRIORITY= C.RECORD_ID " & _
                        " LEFT JOIN dbo.TASK_CATEGORY_MASTER D ON D.TASK_CATEGORY_ID=A.TASK_CATEGORY " & _
                        " LEFT JOIN dbo.TASK_CATEGORY E ON E.ID=D.CATEGORY_ID " & _
                        " WHERE A.TASK_LIST_ID='" & task_list_id & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim reportemailid = GetEmployeeEmailId(ds.Tables(0).Rows(0).Item("REPORTED_EMP_ID").ToString()).ToString()
            Dim owneremailid = GetEmployeeEmailId(ds.Tables(0).Rows(0).Item("CLAIM_EMP_ID").ToString()).ToString()
            Dim loggeremailid = GetEmployeeEmailId(ds.Tables(0).Rows(0).Item("ENTRY_EMP_ID").ToString()).ToString()
            Dim tempemail = ds.Tables(0).Rows(0).Item("EMAIL").ToString()
            Dim email = ""


            If reportedempid = "" Then
                email = tempemail
            Else
                email = reportemailid
            End If





            If owneremailid = "" Then
                email = email & "," & owneremailid
            End If

            If loggeremailid <> reportemailid Then
                email = email & "," & loggeremailid
            End If


            Dim refernecid As String = ds.Tables(0).Rows(0).Item("TASK_LIST_ID").ToString()
            Dim title As String = ds.Tables(0).Rows(0).Item("TASK_TITLE").ToString()
            Dim uploadid As String = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()



            If email <> "" Then
                Dim sb As New Object
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", task_list_id)
                pParms(1) = New SqlClient.SqlParameter("@MESSAGE_ID", loggerid)
                pParms(2) = New SqlClient.SqlParameter("@OPTION", 8)
                sb = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms)


                ds = GetCommunicationSettings()

                Dim username = ""
                Dim password = ""
                Dim port = ""
                Dim host = ""
                Dim fromemailid = ""

                If ds.Tables(0).Rows.Count > 0 Then
                    username = ds.Tables(0).Rows(0).Item("USERNAME").ToString()
                    password = ds.Tables(0).Rows(0).Item("PASSWORD").ToString()
                    port = ds.Tables(0).Rows(0).Item("PORT").ToString()
                    host = ds.Tables(0).Rows(0).Item("HOST").ToString()
                    fromemailid = ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString()
                End If

                SendPlainTextEmails(fromemailid, email, task_list_id & " - " & title, sb.ToString(), username, password, host, port, suploadids)

            End If




        End If

    End Sub

    Public Shared Function GetuploadIds(ByVal id, ByVal optionNo) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim rval As String = ""
        Dim ds As DataSet
        Dim Sql_Query = ""
        If optionNo = 1 Then '' 1- TASK_LIST_MASTER-Table
            Sql_Query = "select ISNULL(UPLOAD_IDS,'') UPLOAD_IDS from TASK_LIST_MASTER where TASK_LIST_ID='" & id & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                rval = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            End If

        End If
        If optionNo = 2 Then '' 2- TASK_ASSIGNED_LOG-Table
            Sql_Query = "select ISNULL(UPLOAD_IDS,'') UPLOAD_IDS from TASK_ASSIGNED_LOG where TASK_ASSIGN_ID='" & id & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                rval = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            End If
        End If
        If optionNo = 3 Then '' 3- MESSAGE_LOG-Table
            Sql_Query = "select ISNULL(UPLOAD_IDS,'') UPLOAD_IDS from MESSAGE_LOG where MESSAGE_ID='" & id & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                rval = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            End If
        End If
        If optionNo = 4 Then '' 4- FOLLOW_UP_MESSAGE_LOG-Table
            Sql_Query = "select ISNULL(UPLOAD_IDS,'') UPLOAD_IDS from FOLLOW_UP_MESSAGE_LOG where RECORD_ID='" & id & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                rval = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            End If
        End If


        Return rval
    End Function


    Public Shared Function GetEmployeeName(ByVal Emp_id As String) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = " select EMP_ID,EMP_FNAME, isnull(EMP_FNAME,'') + ' ' +  isnull(EMP_MNAME ,'') + ' ' + isnull(EMP_LNAME ,'') as EMP_NAME from dbo.EMPLOYEE_M " & _
                        " where EMP_ID='" & Emp_id & "'  "

        Dim ds As DataSet
        Dim empname As String = ""
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            empname = ds.Tables(0).Rows(0).Item("EMP_NAME").ToString().ToString()
        End If

        Return empname

    End Function

    Public Shared Function GetEmployeeEmailId(ByVal Emp_id As String) As String

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select ISNULL(EMD_EMAIL,'') EMD_EMAIL from EMPLOYEE_M A INNER JOIN EMPLOYEE_D B ON A.EMP_ID= B.EMD_EMP_ID WHERE A.EMP_ID='" & Emp_id & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim emailid As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            emailid = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query).ToString()
        End If

        Return emailid

    End Function

    Public Shared Function GetEmployeeMobileNo(ByVal Emp_id As String) As String

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select ISNULL(EMD_CUR_MOBILE,'') EMD_CUR_MOBILE  from EMPLOYEE_M A INNER JOIN EMPLOYEE_D B ON A.EMP_ID= B.EMD_EMP_ID WHERE A.EMP_ID='" & Emp_id & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim mobno As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            mobno = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query).ToString()

        End If

        Return mobno

    End Function




    Public Shared Function GetBsuName(ByVal bsu_id As String) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "SELECT  BSU_NAME  FROM BUSINESSUNIT_M WHERE BSU_ID='" & bsu_id & "' "

        Dim ds As DataSet
        Dim empname As String = ""
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            empname = ds.Tables(0).Rows(0).Item("BSU_NAME").ToString()
        End If

        Return empname

    End Function

    Public Shared Function ReplytoEmailid(ByVal from_empid As String, ByVal to_emp_id As String) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim rval As String = ""
        Dim from_emp_bsu_id = ""
        Dim to_emp_bsu_id = ""
        Dim from_emp_email_id = ""
        Dim to_emp_email_id = ""

        Dim query = " SELECT EMD_CUR_MOBILE,EMD_EMAIL,EMP_BSU_ID FROM OASIS.dbo.EMPLOYEE_M A " & _
                       " INNER JOIN OASIS.dbo.EMPLOYEE_D B ON B.EMD_EMP_ID = A.EMP_ID " & _
                       " WHERE A.EMP_ID ='" & from_empid & "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        If ds.Tables(0).Rows.Count > 0 Then
            from_emp_bsu_id = ds.Tables(0).Rows(0).Item("EMP_BSU_ID").ToString()
            from_emp_email_id = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString()
        End If

        query = " SELECT EMD_CUR_MOBILE,EMD_EMAIL,EMP_BSU_ID FROM OASIS.dbo.EMPLOYEE_M A " & _
                    " INNER JOIN OASIS.dbo.EMPLOYEE_D B ON B.EMD_EMP_ID = A.EMP_ID " & _
                    " WHERE A.EMP_ID ='" & to_emp_id & "' "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        If ds.Tables(0).Rows.Count > 0 Then
            to_emp_bsu_id = ds.Tables(0).Rows(0).Item("EMP_BSU_ID").ToString()
            to_emp_email_id = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString()
        End If

        If to_emp_bsu_id = "888881" Then
            rval = from_emp_email_id
        End If


        Return rval
    End Function

    Public Shared Function SendPlainTextEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal HasAttachments As String, Optional ByVal ReplytoEmailId As String = "") As String
        Dim RetutnValue = ""
        Try

            Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

            msg.Subject = Subject

            msg.Body = MailBody

            msg.Priority = Net.Mail.MailPriority.High

            msg.IsBodyHtml = True

            If ReplytoEmailId <> "" Then
                Dim rto As New System.Net.Mail.MailAddress(ReplytoEmailId)
                'msg.ReplyTo = rto
                msg.From = rto
            End If



            '' If Attachments.

            Dim Afiles As String()
            Afiles = HasAttachments.Split(",")
            Dim i = 0
            Dim serverpath As String = WebConfigurationManager.AppSettings("HelpdeskAttachments").ToString
            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Sql_Query = ""

            For i = 0 To Afiles.Length - 1

                Dim filename = Afiles(i).ToString()
                Sql_Query = "select FILE_NAME from FILE_UPLOAD_MASTER where UPLOAD_ID='" & filename & "'"
                Dim orgfile = SqlHelper.ExecuteScalar(str_conn2, CommandType.Text, Sql_Query)

                Dim fileN = filename + "." + GetExtension(orgfile)
                If File.Exists(serverpath + fileN) Then
                    Dim attach As New System.Net.Mail.Attachment(serverpath + fileN)
                    attach.Name = orgfile
                    msg.Attachments.Add(attach)
                End If


            Next

            Dim client As New System.Net.Mail.SmtpClient(Host, Port)

            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            client.Send(msg)

            RetutnValue = "Successfully sent"

        Catch ex As Exception
            RetutnValue = "Error : " & ex.Message
        End Try

        Return RetutnValue

    End Function


    Private Shared Function GetExtension(ByVal FileName As String) As String

        Dim Extension As String = ""

        If FileName <> "" Then
            Dim split As String() = FileName.Split(".")
            If split.Length > 0 Then
                Extension = split(split.Length - 1)
            End If

        End If

        Return Extension


    End Function

End Class
