Imports System
Imports System.Drawing
Imports System.IO

Namespace SurveyChart
    ''' <summary> 
    ''' Takes care of drawing piechart from pieChartData 
    ''' </summary> 
    Public Class pieChart
        Private imgFormat As System.Drawing.Imaging.ImageFormat() = New System.Drawing.Imaging.ImageFormat(2) {}

        ''' <summary> 
        ''' Constructor for pieChart 
        ''' </summary> 
        Public Sub New()
            imgFormat(0) = System.Drawing.Imaging.ImageFormat.Gif
            imgFormat(1) = System.Drawing.Imaging.ImageFormat.Jpeg
            imgFormat(2) = System.Drawing.Imaging.ImageFormat.Bmp
        End Sub

        ''' <summary> 
        ''' method for getting chart image in stream format. 
        ''' </summary> 
        ''' <param name="mychartdata">Instance of PieChartData</param> 
        ''' <returns>Stream Obj that contains imagedata</returns> 
        Public Function getPieChart(ByRef mychartdata As pieChartData) As Stream
            Dim mystream As Stream = New MemoryStream()

            ' create Brushes and Pen to draw on image 

            Dim mybrush As Brush = New SolidBrush(Color.Black)
            Dim lightbrush As Brush = New SolidBrush(Color.FromArgb(204, 204, 204))
            Dim mypen As New Pen(Color.FromArgb(204, 204, 204), 1)
            '******************************************************************* 

            ' Get Data from PieChartdata into local variables 

            '******************************************************************* 

            Dim piedia As UShort = mychartdata.pieDia
            Dim pieRatio As UShort = mychartdata.pieRatio

            Dim myfont As Font = mychartdata.chartFont
            '******************************************************************* 

            ' get data from DrawData into local variables 

            '******************************************************************* 

            Dim drawdata As pieDrawData = mychartdata.getDrawData()
            Dim imgHeight As UShort = drawdata.imageHeight
            Dim imgWidth As UShort = drawdata.imageWidth
            Dim piewidth As UShort = drawdata.piewidth
            Dim maxvalswidth As UShort = drawdata.valWidth
            Dim maxnameswidth As UShort = drawdata.legendWidth
            Dim maxPerWidth As UShort = drawdata.PercentWidth
            ' create new bitmap Image and graphics object to draw chart 

            Dim memImg As New Bitmap(imgWidth, imgHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            Dim g As Graphics = Graphics.FromImage(memImg)
            g.Clear(Color.White)
            ' draw rectangle around Legends 

            Dim legendrect As New Rectangle(0, 0, 0, 0)
            legendrect.X = CInt((mychartdata.leftMargin + piewidth + drawdata.fontHeight * 0.5))
            legendrect.Y = mychartdata.topMargin
            legendrect.Width = CInt((drawdata.fontHeight * 2.2 + maxvalswidth + maxnameswidth + maxPerWidth + 10))
            legendrect.Height = mychartdata.elements * drawdata.fontHeight + 7
            g.FillRectangle(lightbrush, New Rectangle(legendrect.X + 5, legendrect.Y + 5, legendrect.Width, legendrect.Height))

            g.FillRectangle(New SolidBrush(Color.White), legendrect)
            g.DrawRectangle(mypen, legendrect)

            ' Draw Pies in loop to produce 3D Effect 

            Dim r1 As Rectangle
            For j As Integer = CInt((piedia * mychartdata.pie3dRatio * 0.01F)) To 0 Step -1
                For i As Integer = 0 To mychartdata.elements - 1
                    r1 = New Rectangle(drawdata.pieRect(i).X, drawdata.pieRect(i).Y + j, drawdata.pieRect(i).Width, drawdata.pieRect(i).Height)
                    g.FillPie(New System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.Percent50, mychartdata.colorVal(i)), r1, mychartdata.startAngle(i), mychartdata.span(i))
                Next
            Next
            ' Loop through each elements to draw pies, Legends, LegendSquare 
            For i As Integer = 0 To mychartdata.elements - 1
                g.FillPie(New SolidBrush(mychartdata.colorVal(i)), drawdata.pieRect(i), mychartdata.startAngle(i), mychartdata.span(i))

                ' draws Names and Vals of chart 
                g.DrawString("(" + mychartdata.percentVal(i).ToString + "%)", myfont, New SolidBrush(Color.Blue), CInt((piewidth + drawdata.fontHeight * 2 + mychartdata.leftMargin + maxvalswidth + maxnameswidth)), (i * drawdata.fontHeight) + 4 + mychartdata.topMargin)
                g.DrawString(mychartdata.Vals(i).ToString(), myfont, mybrush, CInt((piewidth + drawdata.fontHeight * 1.5 + mychartdata.leftMargin)), (i * drawdata.fontHeight) + 4 + mychartdata.topMargin)
                g.DrawString(mychartdata.Legends(i), myfont, mybrush, CInt((piewidth + drawdata.fontHeight * 1.75 + maxvalswidth + mychartdata.leftMargin)), (i * drawdata.fontHeight) + 4 + mychartdata.topMargin)
                g.FillRectangle(New SolidBrush(mychartdata.colorVal(i)), New Rectangle(CInt((piewidth + drawdata.fontHeight * 0.75 + mychartdata.leftMargin)), (i * drawdata.fontHeight) + (drawdata.fontHeight) / 5 + 4 + mychartdata.topMargin, CInt((drawdata.fontHeight * 0.7)), CInt((drawdata.fontHeight * 0.7))))
                g.DrawRectangle(New Pen(Color.Black), New Rectangle(CInt((piewidth + drawdata.fontHeight * 0.75 + mychartdata.leftMargin)), (i * drawdata.fontHeight) + (drawdata.fontHeight) / 5 + 4 + mychartdata.topMargin, CInt((drawdata.fontHeight * 0.7)), CInt((drawdata.fontHeight * 0.7))))
            Next
            g.DrawRectangle(mypen, New Rectangle(0, 0, imgWidth - 1, imgHeight - 1))
            g.DrawString(" ", New Font("serif", 7), lightbrush, 5, memImg.Height - 15)
            memImg.Save(mystream, imgFormat(CInt((mychartdata.imageFormat))))
            Return mystream

        End Function
    End Class
End Namespace