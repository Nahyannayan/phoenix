Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing

Namespace SurveyChart

    Public Enum imageFormat
        'Corresponds to "System.Drawing.Imaging.ImageFormat.Gif"
        Gif = 0
        'Corresponds to "System.Drawing.Imaging.ImageFormat.Jpeg"
        Jpeg = 1
        'Corresponds to "System.Drawing.Imaging.ImageFormat.Bmp"
        Bmp = 2
    End Enum

    Public Class ColorData

        ' Public ReadOnly colorval() As Color = {System.Drawing.Color.FromArgb(255, 0, 0), System.Drawing.Color.FromArgb(153, 153, 204), System.Drawing.Color.FromArgb(204, 153, 153), System.Drawing.Color.FromArgb(51, 204, 204), System.Drawing.Color.FromArgb(204, 204, 51), System.Drawing.Color.FromArgb(204, 51, 204), System.Drawing.Color.FromArgb(0, 153, 153), System.Drawing.Color.FromArgb(153, 153, 0), System.Drawing.Color.FromArgb(153, 0, 153), System.Drawing.Color.FromArgb(0, 0, 153), System.Drawing.Color.FromArgb(153, 0, 0), System.Drawing.Color.FromArgb(0, 153, 0), System.Drawing.Color.FromArgb(0, 204, 204), System.Drawing.Color.FromArgb(204, 204, 0), System.Drawing.Color.FromArgb(204, 0, 204), System.Drawing.Color.FromArgb(0, 0, 204), System.Drawing.Color.FromArgb(204, 0, 0), System.Drawing.Color.FromArgb(0, 204, 0), System.Drawing.Color.FromArgb(204, 204, 204), System.Drawing.Color.FromArgb(0, 102, 102), System.Drawing.Color.FromArgb(102, 102, 0), System.Drawing.Color.FromArgb(102, 0, 102), System.Drawing.Color.FromArgb(0, 0, 102), System.Drawing.Color.FromArgb(102, 0, 0), System.Drawing.Color.FromArgb(0, 102, 0), System.Drawing.Color.FromArgb(0, 255, 0), System.Drawing.Color.FromArgb(255, 0, 0), System.Drawing.Color.FromArgb(0, 0, 255), System.Drawing.Color.FromArgb(255, 0, 255), System.Drawing.Color.FromArgb(0, 255, 255), System.Drawing.Color.FromArgb(255, 255, 0)}
        Public ReadOnly colorval() As Color = {System.Drawing.Color.FromArgb(0, 128, 0), System.Drawing.Color.FromArgb(50, 205, 50), System.Drawing.Color.FromArgb(65, 105, 255), System.Drawing.Color.FromArgb(255, 0, 0), System.Drawing.Color.FromArgb(255, 215, 0), System.Drawing.Color.FromArgb(128, 128, 0), System.Drawing.Color.FromArgb(255, 255, 0), System.Drawing.Color.FromArgb(128, 128, 128), System.Drawing.Color.FromArgb(0, 255, 127), System.Drawing.Color.FromArgb(153, 0, 0), System.Drawing.Color.FromArgb(153, 0, 0), System.Drawing.Color.FromArgb(0, 153, 0), System.Drawing.Color.FromArgb(0, 204, 204), System.Drawing.Color.FromArgb(204, 204, 0), System.Drawing.Color.FromArgb(204, 0, 204), System.Drawing.Color.FromArgb(0, 0, 204), System.Drawing.Color.FromArgb(204, 0, 0), System.Drawing.Color.FromArgb(0, 204, 0), System.Drawing.Color.FromArgb(204, 204, 204), System.Drawing.Color.FromArgb(0, 102, 102), System.Drawing.Color.FromArgb(102, 102, 0), System.Drawing.Color.FromArgb(102, 0, 102), System.Drawing.Color.FromArgb(0, 0, 102), System.Drawing.Color.FromArgb(102, 0, 0), System.Drawing.Color.FromArgb(0, 102, 0), System.Drawing.Color.FromArgb(0, 255, 0), System.Drawing.Color.FromArgb(255, 0, 0), System.Drawing.Color.FromArgb(0, 0, 255), System.Drawing.Color.FromArgb(255, 0, 255), System.Drawing.Color.FromArgb(0, 255, 255), System.Drawing.Color.FromArgb(255, 255, 0)}
        '153, 204, 153
    End Class

End Namespace
