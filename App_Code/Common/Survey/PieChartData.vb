Imports System
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports SurveyChart

Namespace SurveyChart
    ''' <summary> 
    ''' Describes all data required to draw chart 
    ''' </summary> 
    Public Class pieChartData
        '******************************************************************** 

        ' Define Internal Members of Class 

        '******************************************************************** 

        Private m_Vals As Single(), m_PercentVal As Single(), m_StartAngle As Single(), m_Span As Single()
        Private m_Legends As String(), m_Links As String()
        Private m_Explode As Boolean()
        Private m_TopMargin As UShort, m_BottomMargin As UShort, m_RightMargin As UShort, m_LeftMargin As UShort
        Private m_Pie3dRatio As Byte, m_pieRatio As Byte, m_ExploadOffset As Byte
        Private m_chartFont As Font
        Private m_elements As UShort
        Private m_pieDia As UShort
        Private m_imageFormat As imageFormat
        Private m_colorVal As Color()
        Private m_QuestionId As Integer
        Private m_SurveyId As Integer
        '******************************************************************** 


        '******************************************************************** 

        ' Public Members of Class 

        '******************************************************************** 


        ''' <summary> 
        ''' Describes Image Format to be used to generate chart image. 
        ''' </summary> 
        Public Property imageFormat() As imageFormat
            Get
                Return m_imageFormat
            End Get
            Set(ByVal value As imageFormat)
                m_imageFormat = value
            End Set
        End Property
        ''' <summary> 
        ''' Array Of Colors Used for each corresponding Values( by Default 
        ''' this colors will be generated from colorData Class ). 
        ''' </summary> 
        Public Property colorVal() As Color()
            Get
                Return m_colorVal
            End Get
            Set(ByVal value As Color())
                m_colorVal = value
            End Set
        End Property
        ''' <summary> 
        ''' Array of Chart Values. 
        ''' </summary> 
        Public Property Vals() As Single()
            Get
                Return m_Vals
            End Get
            Set(ByVal value As Single())
                m_Vals = value
            End Set
        End Property
        ''' <summary> 
        ''' Array Of Chart Legends. 
        ''' </summary> 
        Public Property Legends() As String()
            Get
                Return m_Legends
            End Get
            Set(ByVal value As String())
                m_Legends = value
            End Set
        End Property
        ''' <summary> 
        ''' Array Of Chart Links(Url for Click Through). 
        ''' </summary> 
        Public Property Links() As String()
            Get
                Return m_Links
            End Get
            Set(ByVal value As String())
                m_Links = value
            End Set
        End Property
        ''' <summary> 
        ''' Array of bool(if true it explode's Pie by Default its False). 
        ''' </summary> 
        Public Property Explode() As Boolean()
            Get
                Return m_Explode
            End Get
            Set(ByVal value As Boolean())
                m_Explode = value
            End Set
        End Property
        ''' <summary> 
        ''' Array Of Chart Vals converted in Percent Format. 
        ''' </summary> 
        Public ReadOnly Property percentVal() As Single()
            Get
                Return m_PercentVal
            End Get
        End Property
        'set{m_PercentVal= value;} 
        ''' <summary> 
        ''' Array Of Span in Degree 
        ''' (Deacribes span of pie to be drawn) 
        ''' </summary> 
        Public Property span() As Single()
            Get
                Return m_Span
            End Get
            Set(ByVal value As Single())
                m_Span = value
            End Set
        End Property
        ''' <summary> 
        ''' Array of StartAngle in Degree 
        ''' (Describes starting angle for drawing pie) 
        ''' </summary> 
        Public Property startAngle() As Single()
            Get
                Return m_StartAngle
            End Get
            Set(ByVal value As Single())
                m_StartAngle = value
            End Set
        End Property
        ''' <summary> 
        ''' Number of Elements in array of Legends and Vals 
        ''' </summary> 
        Public Property elements() As UShort
            Get
                Return m_elements
            End Get
            Set(ByVal value As UShort)
                m_elements = value
            End Set
        End Property
        ''' <summary> 
        ''' Percentage of Pie Height to Pie Width(Default is 80) 
        ''' </summary> 
        Public Property pieRatio() As Byte
            Get
                Return m_pieRatio
            End Get
            Set(ByVal value As Byte)
                m_pieRatio = value
            End Set
        End Property
        ''' <summary> 
        ''' Diameter of Pie to be drawn in Pixels(Default is 150) 
        ''' </summary> 
        Public Property pieDia() As UShort
            Get
                Return m_pieDia
            End Get
            Set(ByVal value As UShort)
                m_pieDia = value
            End Set
        End Property
        ''' <summary> 
        ''' Ratio of Pie Thickness to Pie Widht use Zero to make 
        ''' 2Dimentional Pie Chart ( Default is 0.08 ) 
        ''' </summary> 
        Public Property pie3dRatio() As Byte
            Get
                Return m_Pie3dRatio
            End Get
            Set(ByVal value As Byte)
                m_Pie3dRatio = value
            End Set
        End Property
        ''' <summary> 
        ''' Font Used in Chart to Draw Legends. 
        ''' </summary> 
        Public Property chartFont() As Font
            Get
                Return m_chartFont
            End Get
            Set(ByVal value As Font)
                m_chartFont = value
            End Set
        End Property
        ''' <summary> 
        ''' Distance as Percentage of pieDia from center for Exploaded pie. 
        ''' (default value is 25) 
        ''' </summary> 
        Public Property ExploadOffset() As Byte
            Get
                Return m_ExploadOffset
            End Get
            Set(ByVal value As Byte)
                m_ExploadOffset = value
            End Set
        End Property
        ''' <summary> 
        ''' Extra Space on Right Side of Image. 
        ''' (default is 10) 
        ''' </summary> 
        Public Property rightMargin() As UShort
            Get
                Return m_RightMargin
            End Get
            Set(ByVal value As UShort)
                m_RightMargin = value
            End Set
        End Property
        ''' <summary> 
        ''' Extra Space on Left Side of Image. 
        ''' (default is 10) 
        ''' </summary> 
        Public Property leftMargin() As UShort
            Get
                Return m_LeftMargin
            End Get
            Set(ByVal value As UShort)
                m_LeftMargin = value
            End Set
        End Property
        ''' <summary> 
        ''' Extra Space on Top Side of Image. 
        ''' (default is 10) 
        ''' </summary> 
        Public Property topMargin() As UShort
            Get
                Return m_TopMargin
            End Get
            Set(ByVal value As UShort)
                m_TopMargin = value
            End Set
        End Property
        ''' <summary> 
        ''' Extra Space on Bottom Side of Image. 
        ''' (default is 10) 
        ''' </summary> 
        Public Property bottomMargin() As UShort
            Get
                Return m_BottomMargin
            End Get
            Set(ByVal value As UShort)
                m_BottomMargin = value
            End Set
        End Property
        'Newly added property for Survey Information --- added by Sajesh
        Public Property QuestionId() As Integer
            Get
                Return m_QuestionId
            End Get
            Set(ByVal value As Integer)
                m_QuestionId = value
            End Set
        End Property 'm_SurveyId

        Public Property SurveyId() As Integer
            Get
                Return m_SurveyId
            End Get
            Set(ByVal value As Integer)
                m_SurveyId = value
            End Set
        End Property '
        '************************************************************** 


        ''' <summary> 
        ''' Creates instance of data for creating pie chart 
        ''' </summary> 
        ''' <param name="Vals">Array of Values for Chart</param> 
        ''' <param name="Legends">Array of Legends for Chart</param> 
        ''' <param name="Explode">Array of bool for chart 
        ''' "True if exploaded" 
        ''' </param> 
        Public Sub New(ByVal Vals As Single(), ByVal Legends As String(), ByVal Explode As Boolean())
            m_pieChartData(Vals, Legends, Explode)
        End Sub
        ''' <summary> 
        ''' Creates instance of data for creating pie chart 
        ''' </summary> 
        ''' <param name="Vals">Array of Values for Chart</param> 
        ''' <param name="Legends">Array of Legends for Chart</param> 
        Public Sub New(ByVal Vals As Single(), ByVal Legends As String())
            Dim arrsize As Integer = Vals.Length '- 1
            Dim m_Expload As Boolean() = New Boolean(arrsize - 1) {}
            For i As Integer = 0 To arrsize - 1
                m_Expload(i) = False
            Next

            m_pieChartData(Vals, Legends, m_Expload)
        End Sub
        Private Sub m_pieChartData(ByVal Vals As Single(), ByVal Legends As String(), ByVal Explode As Boolean())

            '******************************************************************* 

            ' initilize array with size = arrsize 

            '******************************************************************* 

            Dim arrsize As Integer = Vals.Length ' - 1
            m_PercentVal = New Single(arrsize - 1) {}
            m_StartAngle = New Single(arrsize - 1) {}
            m_Span = New Single(arrsize - 1) {}
            m_Links = New String(arrsize - 1) {}
            m_colorVal = New Color(arrsize - 1) {}
            '******************************************************************* 


            '******************************************************************* 

            ' Set Default values for other Properties 

            '******************************************************************* 

            Me.m_LeftMargin = 10
            Me.m_RightMargin = 10
            Me.m_TopMargin = 10
            Me.m_BottomMargin = 10
            Me.m_ExploadOffset = 25
            Me.m_Pie3dRatio = 8
            Me.m_pieRatio = 80
            Me.m_pieDia = 150
            Me.m_Vals = Vals
            Me.m_Legends = Legends
            Me.m_elements = CUShort(arrsize)
            Me.m_Explode = Explode
            Me.m_imageFormat = imageFormat.Gif
            Me.m_chartFont = New System.Drawing.Font("Verdana", 8.0F, System.Drawing.FontStyle.Bold)
            'new Font("verdana",8,FontStyle.Bold); 
            '******************************************************************* 



            ' this loop gets total of all values in array (vals) 

            Dim totalval As Single = 0
            For i As Integer = 0 To arrsize - 1
                totalval = totalval + Vals(i)
            Next
            ' this loop gets StartAngle,Span,PercentVal,ColorVals 

            Dim total As Single = 0
            Dim t As Integer = 0
            Dim mycolordata As New colordata()
            For i As Integer = 0 To arrsize - 1
                m_StartAngle(i) = total
                m_Span(i) = Vals(i) * 360 / totalval
                Try
                    m_PercentVal(i) = CSng(CInt((Vals(i) * 10000 / totalval))) / 100
                Catch ex As Exception
                    m_PercentVal(i) = 0.0
                End Try

                total = total + m_Vals(i) * 360 / totalval

                m_colorVal(i) = mycolordata.colorval(t)
                If t + 1 >= mycolordata.colorval.Length Then
                    t = 0
                Else
                    t = t + 1
                End If
            Next
        End Sub
        ''' <summary> 
        ''' Creates data for drawing of Chart 
        ''' </summary> 
        ''' <returns>obj pieDrawdata for Drawing Chart</returns> 
        Public Function getDrawData() As pieDrawData

            '******************************************************************* 

            ' Define Internal Members of method 

            '******************************************************************* 

            Dim imgHeight As UShort, imgWidth As UShort, fontHeight As UShort, pieWidth As UShort
            Dim Legendmap As Rectangle() = New Rectangle(m_elements - 1) {}
            fontHeight = CUShort(m_chartFont.GetHeight())
            '******************************************************************* 

            ' this part handles chart Exploading 

            '******************************************************************* 

            Dim xShift As Single() = New Single(m_elements - 1) {}
            Dim yShift As Single() = New Single(m_elements - 1) {}
            Dim centerangle As Single
            Dim maxXS As Single, maxYS As Single, minXS As Single, minYS As Single
            Dim pieRect As Rectangle() = New Rectangle(m_elements - 1) {}

            maxXS = 0
            maxYS = 0
            minXS = 0
            minYS = 0

            For i As Integer = 0 To m_elements - 1
                If m_Explode(i) = True Then
                    centerangle = (m_StartAngle(i) + (m_Span(i) / 2)) * CSng(Math.PI) / 180
                    xShift(i) = CSng(Math.Cos(centerangle)) * m_pieDia * m_ExploadOffset * 0.5F * 0.01F
                    yShift(i) = CSng(Math.Sin(centerangle)) * m_pieDia * m_pieRatio * m_ExploadOffset * 0.5F * 0.01F * 0.01F

                    If xShift(i) > maxXS Then
                        maxXS = xShift(i)
                    End If
                    If yShift(i) > maxYS Then
                        maxYS = yShift(i)
                    End If
                    If xShift(i) < minXS Then
                        minXS = xShift(i)
                    End If
                    If yShift(i) < minYS Then
                        minYS = yShift(i)
                    End If

                End If
            Next

            For i As Integer = 0 To m_elements - 1
                If m_Explode(i) = True Then
                    centerangle = (m_StartAngle(i) + (m_Span(i) / 2)) * CSng(Math.PI) / 180
                    xShift(i) = CSng(Math.Cos(centerangle)) * m_pieDia * m_ExploadOffset * 0.5F * 0.01F
                    yShift(i) = CSng(Math.Sin(centerangle)) * m_pieDia * m_pieRatio * m_ExploadOffset * 0.5F * 0.01F * 0.01F
                Else
                    xShift(i) = 0
                    yShift(i) = 0
                End If
                Try
                    pieRect(i) = New Rectangle(CSng((m_LeftMargin + xShift(i) - minXS)), CSng((m_TopMargin + yShift(i) - minYS)), m_pieDia, CInt((m_pieDia * m_pieRatio * 0.01F)))
                Catch ex As Exception
                    pieRect(i) = New Rectangle(CSng((m_LeftMargin + CInt(-1) - minXS)), CSng((m_TopMargin + CInt(-1) - minYS)), m_pieDia, CInt((m_pieDia * m_pieRatio * 0.01F)))
                End Try

            Next
            ' get dimension of the image to be poduced 'Change by Sajesh --CInt

            imgHeight = CUShort((m_pieDia * (m_pieRatio * 0.01F) + maxYS - minYS + m_TopMargin + m_BottomMargin + m_pieDia * m_Pie3dRatio * 0.01F + 15))

            If m_elements * fontHeight + m_TopMargin + m_BottomMargin + 15 > imgHeight Then
                imgHeight = CUShort((m_elements * fontHeight + m_TopMargin + m_BottomMargin + 15))
            End If

            ' we will generate graphics object to get width of string 

            Dim gtemp As Graphics = Graphics.FromImage(New Bitmap(1, 1))

            Dim maxNamesWidth As UShort = 0
            Dim maxValsWidth As UShort = 0
            Dim maxPerWidth As UShort = 0

            Dim tempNamesWidth As UShort, tempValsWidth As UShort, tempPerWidth As UShort
            imgWidth = 200
            For i As Integer = 0 To m_elements - 1
                tempValsWidth = CUShort(gtemp.MeasureString(m_Vals(i).ToString(), m_chartFont).Width)
                tempNamesWidth = CUShort(gtemp.MeasureString(m_Legends(i), m_chartFont).Width)
                tempPerWidth = CUShort(gtemp.MeasureString(m_PercentVal(i).ToString() + "()", m_chartFont).Width)


                If tempNamesWidth > maxNamesWidth Then
                    maxNamesWidth = tempNamesWidth
                End If
                If tempValsWidth > maxValsWidth Then
                    maxValsWidth = tempValsWidth
                End If
                If tempPerWidth > maxPerWidth Then
                    maxPerWidth = tempPerWidth

                End If
            Next
            ' We will get image width 

            pieWidth = CUShort((m_pieDia + maxXS - minXS))
            imgWidth = CUShort((m_LeftMargin + pieWidth + fontHeight * 2.75 + 12 + maxValsWidth + maxNamesWidth + maxPerWidth + m_RightMargin))

            gtemp.Dispose()
            '******************************************************************* 


            For i As Integer = 0 To m_elements - 1
                Legendmap(i).X = CInt((pieWidth + fontHeight * 0.75 + m_LeftMargin))
                Legendmap(i).Y = (i * fontHeight) + (fontHeight) / 4 + 4 + m_TopMargin
                Legendmap(i).Width = fontHeight * 1 + maxValsWidth + maxNamesWidth + maxPerWidth
                Legendmap(i).Height = fontHeight - 1
            Next

            Dim DrawData As New pieDrawData()
            DrawData.legendWidth = maxNamesWidth
            DrawData.valWidth = maxValsWidth
            DrawData.imageHeight = imgHeight
            DrawData.pieRect = pieRect
            DrawData.imageWidth = imgWidth
            DrawData.piewidth = pieWidth
            DrawData.fontHeight = fontHeight
            DrawData.PercentWidth = maxPerWidth
            DrawData.LegandMap = Legendmap

            Return DrawData
        End Function
        ''' <summary> 
        ''' Method To get imagemap for chart 
        ''' </summary> 
        ''' <returns>html String that contains image maping information</returns> 
        Public Function getImageMap(ByVal mapName As String) As String
            Dim DrawData As pieDrawData = getDrawData()

            Dim startAngle As Single, endAngle As Single
            Dim CenterPoint As Point() = New Point(m_elements - 1) {}
            Dim point0 As Point() = New Point(m_elements - 1) {}
            Dim Point1 As Point() = New Point(m_elements - 1) {}
            Dim Point2 As Point() = New Point(m_elements - 1) {}
            Dim Point3 As Point() = New Point(m_elements - 1) {}
            Dim myPieRect As Rectangle() = DrawData.pieRect
            Dim m_imageMap As String = ""

            For i As Integer = 0 To m_elements - 1
                'Try
                On Error Resume Next

                CenterPoint(i).X = myPieRect(i).X + myPieRect(i).Width / 2
                CenterPoint(i).Y = myPieRect(i).Y + myPieRect(i).Height / 2
                startAngle = CSng(((m_StartAngle(i) + 1) * Math.PI / 180))
                endAngle = CSng(((m_StartAngle(i) + m_Span(i) - 1) * Math.PI / 180))

                point0(i).X = CInt((CenterPoint(i).X + Math.Cos((m_StartAngle(i) + m_Span(i) / 2) * Math.PI / 180) * m_pieDia * 0.05))
                point0(i).Y = CInt((CenterPoint(i).Y + Math.Sin((m_StartAngle(i) + m_Span(i) / 2) * Math.PI / 180) * m_pieDia * m_pieRatio * 0.05 * 0.01F))

                Point1(i).X = CInt((CenterPoint(i).X + Math.Cos(startAngle) * m_pieDia / 2))
                Point1(i).Y = CInt((CenterPoint(i).Y + Math.Sin(startAngle) * m_pieDia * m_pieRatio * 0.01F / 2))

                Point2(i).X = CInt((CenterPoint(i).X + Math.Cos((m_StartAngle(i) + m_Span(i) / 2) * Math.PI / 180) * m_pieDia / 2))
                Point2(i).Y = CInt((CenterPoint(i).Y + Math.Sin((m_StartAngle(i) + m_Span(i) / 2) * Math.PI / 180) * m_pieRatio * 0.01F * m_pieDia / 2))

                Point3(i).X = CInt((CenterPoint(i).X + Math.Cos(endAngle) * m_pieDia / 2))
                Point3(i).Y = CInt((CenterPoint(i).Y + Math.Sin(endAngle) * m_pieDia * m_pieRatio * 0.01F / 2))
                'Catch ex As Exception

                'End Try
            Next
            Dim r As Rectangle() = DrawData.LegandMap
            m_imageMap = "<map name='" + mapName + "'>" + vbLf
            Dim str1 As String
            Dim str2 As String
            str1 = Me.SurveyId.ToString
            str2 = Me.QuestionId.ToString
            For i As Integer = 0 To m_elements - 1
                'm_imageMap = m_imageMap + "<area href='" + m_Links(i) + "' shape='polygon' coords='" + point0(i).X.ToString() + "," + point0(i).Y.ToString() + "," + Point1(i).X.ToString() + "," + Point1(i).Y.ToString() + "," + Point2(i).X.ToString() + "," + Point2(i).Y.ToString() + "," + Point3(i).X.ToString() + "," + Point3(i).Y.ToString() + "' alt='" + m_Legends(i) + "'>" + vbLf
                m_imageMap = m_imageMap + "<area href='#' onclick=GetResult(" & QuestionId & "," & SurveyId & ",'" & m_Legends(i).Replace(" ", ".") & "') shape='polygon' coords='" + point0(i).X.ToString() + "," + point0(i).Y.ToString() + "," + Point1(i).X.ToString() + "," + Point1(i).Y.ToString() + "," + Point2(i).X.ToString() + "," + Point2(i).Y.ToString() + "," + Point3(i).X.ToString() + "," + Point3(i).Y.ToString() + "' alt='" + m_Legends(i) + "'>" + vbLf
                'm_imageMap = m_imageMap + "<area href='" + m_Links(i).ToString + "' shape='rect' coords='" + r(i).X.ToString() + "," + r(i).Y.ToString + "," + (r(i).X + r(i).Width).ToString + "," + (r(i).Y + r(i).Height).ToString + "' alt='" + m_Legends(i).ToString + "'>" + vbLf
                m_imageMap = m_imageMap + "<area href='#' onclick=GetResult(" & QuestionId & "," & SurveyId & ",'" & m_Legends(i).Replace(" ", ".") & "') shape='rect' coords='" + r(i).X.ToString() + "," + r(i).Y.ToString + "," + (r(i).X + r(i).Width).ToString + "," + (r(i).Y + r(i).Height).ToString + "' alt='" + m_Legends(i).ToString + "'>" + vbLf
            Next '

            'm_imageMap = m_imageMap + "<area href='www.gemseducation.com' shape='rect' coords='10," + (DrawData.imageHeight - 20).ToString + ",200," + (DrawData.imageHeight - 5).ToString + "' alt='Gems Education'>" + vbLf
            m_imageMap = m_imageMap + "</map>"
            Return m_imageMap
        End Function
    End Class
End Namespace