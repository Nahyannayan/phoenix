﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

''' <summary>
''' EventArgs class to report when progress has changed, during a 
''' long-running process.
''' </summary>
Public Class ProgressChangedEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' Creates a new instance of this type.
    ''' </summary>
    ''' <param name="percentComplete">The percent complete of the 
    ''' long-running process.</param>
    ''' <param name="isComplete">Whether the long-running process 
    ''' is complete.</param>
    ''' <param name="workDescription">A brief description of the 
    ''' work that is currently being executed, to let the user 
    ''' know what is going on.</param>
    Public Sub New(ByVal percentComplete As Double, ByVal isComplete As Boolean, ByVal workDescription As String)
        MyBase.New()
        ' No argument validation - OK if these are null or invalid.

        Me.PercentComplete = percentComplete
        Me.IsComplete = isComplete
        Me.WorkDescription = workDescription
    End Sub

    ''' <summary>
    ''' Gets the percent complete of the long-running process.
    ''' </summary>
    Private privatePercentComplete As Double
    Public Property PercentComplete() As Double
        Get
            Return privatePercentComplete
        End Get
        Protected Set(ByVal value As Double)
            privatePercentComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is complete.
    ''' </summary>
    Private privateIsComplete As Boolean
    Public Property IsComplete() As Boolean
        Get
            Return privateIsComplete
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a brief description of the work that is currently being 
    ''' executed, to let the user know what is going on.
    ''' </summary>
    Private privateWorkDescription As String
    Public Property WorkDescription() As String
        Get
            Return privateWorkDescription
        End Get
        Protected Set(ByVal value As String)
            privateWorkDescription = value
        End Set
    End Property
End Class
