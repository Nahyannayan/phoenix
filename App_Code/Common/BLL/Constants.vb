Imports Microsoft.VisualBasic

Public NotInheritable Class OASISConstants
    Private Sub New()

    End Sub

    Public Const SEPARATOR_PIPE As String = "|"
    Public Const DateFormat As String = "dd/MMM/yyyy"
    Public Const DataBaseDateFormat As String = "dd/MMM/yyyy"
    Public Const Gemstitle As String = "PHOENIXBETA"
    'Public Const Transporttitle As String = ">>>Bright Bus Transport - Online Transport Management System<<<"
    Public Const dbFinance As String = "OASISFIN"
    Public Const dbPayroll As String = "OASIS"
    Public Const dbOasis As String = "OASIS"
    Public Const dbFees As String = "OASIS_FEES"
    Public Const MAX_PASSWORDTRY As Integer = 10
    Public Const TransportBSU_BrightBus As String = "900500"
    Public Const TransportBSU_Sts As String = "900501"
    Public Const TransportFEEID As Integer = 6
    Public Const ERRORMSG_UNEXPECTED As Integer = 1000
    Public Const ERRORMSG_NOERRORS As Integer = 0
    Public Const ERRORMSG_SESSIONDOESNOTMATCH As String = "Sessions not matching..."
    Public Const CostCenter_STUDENT As String = "0004"
    Public Const MNU_STU_SET_SERVICE_DET As String = "S050015"



#Region "Menu Accounts"
    Public Const AccountsYearEnd As String = "A200065"
    Public Const AccPostCreditCardReceipt As String = "A200040"
    Public Const AccPostInternetCollection As String = "A200055"
    Public Const FEECreditCardMachine As String = "F100135"
    Public Const MNU_CREDITCARD_DEPOSIT As String = "A200019"
    Public Const MNU_CASH_DEPOSIT As String = "A200018"
    Public Const MNU_CHEQUE_PAYMENT As String = "A150017"
    'Reports
    Public Const ReportMISDailyCashFlow As String = "A753035" 
    Public Const ReportMISPnL As String = "A753005"
    Public Const ReportMISBalanceSheet As String = "A753015"
    Public Const ReportMISBalanceSheetSchedule As String = "A753020"
    Public Const ReportMISCashFlow As String = "A753030"
    Public Const ReportMISBudgetvsActual As String = "A753040"
    Public Const MNU_ACC_REP_PROCESSCHECKLIST As String = "A350045"
    Public Const ReportMISPnLColumnar As String = "A753045"
    Public Const ReportMISBalanceSheetColumnar As String = "A753050"
    Public Const ReportAccountBalanceColumnar As String = "A753060"
    Public Const ReportBvsActualColumnar As String = "A753055"
    Public Const ReportCashFlowColumnar As String = "A753065"
    Public Const ReportCostcenterAnalysis As String = "A350035"
    Public Const ReportSubLedgerAnalysis As String = "A350042"
#End Region


#Region "Menu Details"
    Public Const MenuEMPSUBJECT_M As String = "P050080"
    Public Const MenuEMPQUALIFICATION_CAT_M As String = "P050085"
    Public Const MenuEMPQUALIFICATION_M As String = "P050075"
    Public Const MenuEMPSCALE_GRADE As String = "P050100"
    Public Const MenuEMPGRADESALARY As String = "P050095"
    Public Const MenuEMPSALARYREVISION As String = "P130145"
    Public Const MenuEMPSALARYBULKREVISION As String = "P130146"
    Public Const MenuEMPSALARYDEDUCTION As String = "P130185"
    Public Const MenuREPORTSJV As String = "A250032"
    Public Const mnuREP_DOC_MOVEMENT As String = "P153025"
#End Region


#Region "Fee Menu Details"
    Public Const MNU_FEE_PROCESS_FLOW As String = "F100120"
    Public Const MNU_FEE_CONCESSION As String = "F100125"
    Public Const MNU_FEE_CONCESSION_TRANSPORT As String = "F100135"
	Public Const MNU_FEE_CONCESSION_CANCEL As String = "F300110"
    Public Const MNU_FEE_PERFORMAINVOICE As String = "F100130"
    Public Const MNU_FEE_CONCESSION_TRANS_TRANSPORT As String = "F300160"
    Public Const MNU_FEE_CONCESSION_TRANS As String = "F300140"
    Public Const MNU_FEE_ADJUSTMENTS As String = "F300145"
    Public Const MNU_FEE_ADJUSTMENTS_REQ As String = "F300146"
    Public Const MNU_FEE_ADJUSTMENTS_REQ_NEW As String = "F300148"

    Public Const MNU_FEE_ADJUSTMENTS_APP As String = "F300147"
    Public Const MNU_FEE_ADJUSTMENTS_TRANSPORT As String = "F300170"
    Public Const MNU_FEE_ADJUSTMENTS_REQ_TRANSPORT As String = "F300186"
    Public Const MNU_FEE_ADJUSTMENTS_APP_TRANSPORT As String = "F300188"
    Public Const MNU_FEE_ADJUSTMENTS_HEADTOHEAD_TRANSPORT As String = "F300225"
    Public Const MNU_FEE_STUDENT_LEDGER As String = "F703005"

    Public Const MNU_FEE_STUDENT_MASTER As String = "F100045"
 
    Public Const MNU_FEE_COUNTER As String = "F100035"
    Public Const MNU_FEE_ADJUSTMENTS_HEADTOHEAD As String = "F300195"
    Public Const MNU_FEE_REMINDER As String = "F300200"
    Public Const MNU_FEE_REMINDER_TEMPLATE As String = "F300205"
    Public Const MNU_FEE_REFUND_REQUEST As String = "F300210"
    Public Const MNU_FEE_CHEQUE_BOUNCE As String = "F300215"
    Public Const MNU_FEE_CHEQUE_BOUNCE_POST As String = "F351070"
    Public Const MNU_FEE_RECEIPT_CANCELLATION As String = "F300222"

    'APPPROVAL FEE
    Public Const MNU_FEE_REVENUE_RECOGNITION As String = "F351035"
    Public Const MNU_FEE_CHARGING As String = "F351030"
    Public Const MNU_FEE_POST_CHARGING As String = "F351045"
    Public Const MNU_FEE_CONCESSION_APPROVAL As String = "F351050"

    Public Const MNU_FEE_TRANSPORT_REVENUE_RECOGNITION As String = "F352005"
    Public Const MNU_FEE_TRANSPORT_DO_MONTHEND As String = "F352010"
    Public Const MNU_FEE_TRANSPORT_POST_CHARGING As String = "F352015"
    Public Const MNU_FEE_TRANSPORT_ADJTO_FINANCE As String = "F352020"

    Public Const MNU_FEE_ADJTO_FINANCE As String = "F351055"
    Public Const MNU_FEE_CHEQUE_CLEARANCE As String = "F351060"
    Public Const MNU_FEE_REFUND_FEE_APPROVAL As String = "F351065"
    Public Const MNU_FEE_CONCESSION_CANCEL_APPROVAL As String = "F351075"
    Public Const MNU_FEE_DAYEND_PROCESS As String = "F300150"
    Public Const MNU_FEE_FEE_SETTLEMENT As String = "F351062"

    'Fee Reports
    Public Const MNU_FEE_SERVICEFEE_DETAILS As String = "F725075"
    Public Const MNU_FEE_ADJUSTMENT_DETAILS As String = "F725085"
    Public Const MNU_FEE_CHEQUEBOUNCE_DETAILS As String = "F725135"


    'APPPROVAL TARNSPORT
    Public Const MNU_FEE_DAYEND_PROCESS_TRANSPORT As String = "F300405"
    Public Const MNU_FEE_CASH_DEPOSIT_TRANSPORT As String = "F300410"
    Public Const MNU_FEE_BANK_DEPOSIT_TRANSPORT As String = "F300415"
    Public Const MNU_FEE_BANK_DEPOSIT_VOUCHER As String = "F300120"
    Public Const MNU_FEE_BANK_DEPOSIT_VOUCHER_TRANSPORT As String = "F300420"
    Public Const MNU_FEE_CHEQUE_CLEARANCE_TRANSPORT As String = "F300425"
    Public Const MNU_FEE_CREDITCARD_DEPOSIT_TRANSPORT As String = "F300430"
    Public Const MNU_FEE_CHEQUE_COLLECTION As String = "F300115"
    Public Const MNU_FEE_CHEQUE_COLLECTION_TRANSPORT As String = "F300415"

    'fee transport
    Public Const MNU_FEE_TRANSPORT_FEE_COLLECTION As String = "F300155"
    Public Const MNU_FEE_TRANSPORT_FEE_COLLECTION_NEW As String = "F300165"
    Public Const MNU_FEE_TRANSPORT_FEE_CONCESSION_APPROVAL As String = "F300182"
    Public Const MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL_APPROVAL As String = "F300187"

    'fee ECA 
    Public Const MNU_FEE_ECA_FEE_COLLECTION As String = "F300168"

    ''AdjustmentECA
    Public Const MNU_FEE_ECA_ADJUSTMENT_COLLECTION As String = "F300169"
    Public Const MNU_FEE_ECA_ADJUSTMENT_APPROVAL As String = "F300197"

    Public Const MNU_FEE_CHEQUE_BOUNCE_TRANSPORT As String = "F300235"
    Public Const MNU_FEE_CHEQUE_BOUNCE_TRANSPORT_POST As String = "F300189"
    'Transport Fee Collection Menus
    Public Const MNU_FEE_REP_SUMMARY As String = "F715005"
    Public Const MNU_FEE_REP_COLLECTION_DET As String = "F715010"
    Public Const MNU_FEE_REP_COLLECTION_SUMMARY As String = "F715015"
    Public Const MNU_FEE_REP_COLL_SUMMARY_SCH As String = "F715020"
    Public Const MNU_FEE_REP_COLL_SUMMARY_EMP As String = "F715025"
    Public Const MNU_FEE_REP_COLL_SUMMARY_EMP_SCH As String = "F715030"
    Public Const MNU_FEE_REP_CHQ_COLL_SUMMARY As String = "F715035"
    Public Const MNU_FEE_REP_CREDIT_CARD_COLL_SUMMARY As String = "F715070"
    Public Const MNU_FEE_REP_CREDITCARDDAILY As String = "F725067"

    Public Const MNU_FEE_TRANS_REP_CHARGE_OVERALL As String = "F730316"
    Public Const MNU_FEE_TRANS_COLL_PDC_SCH As String = "F715040"
    Public Const MNU_FEE_TRAN_COLL_AREAWISE As String = "F715045"
    Public Const MNU_FEE_TRAN_COLL_STUDENTS As String = "F715050"
    Public Const MNU_FEE_TRAN_COLL_CREDIT_CRD As String = "F715055"
    Public Const MNU_FEE_REP_CHQ_CLEARANCE As String = "F715060"
    Public Const MNU_FEE_TRANS_COLL_VIEW_ALL As String = "F715065"
    Public Const MNU_FEE_TRANSPORT_FEE_CONCESSION As String = "F300178"
    Public Const MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL As String = "F300180"
    Public Const MNU_FEE_TRANSPORT_FEE_REMINDER As String = "F300192"
    Public Const MNU_FEE_TRANSPORT_REMINDER_TEMPLATE As String = "F300230"
    Public Const MNU_FEE_TRANSPORT_REMINDER_EXCLUDE As String = "F300193"
    Public Const MNU_FEE_REMINDEREXCLUDE As String = "F300202"
    Public Const MNU_FEE_REMINDERNEW As String = "F300203"

    'ECA FEE reports 
    Public Const MNU_ECA_FEE_STUDENT_LEDGER As String = "F703098"
    'ECa collection reports 
    Public Const MNU_ECA_FEE_COLLECTION_DETAILS As String = "F720098"


    'Transport reports
    Public Const MNU_TRAN_REP_STUDENT_COUNT_CHART As String = "F700055"
    Public Const MNU_TRAN_REP_COLLECTION_SUMMARY As String = "F700050"
    Public Const MNU_FEE_TRANSPORT_STUDENT_LEDGER As String = "F730310"
    Public Const MNU_FEE_TRANSPORT_STATEMENT_REPORT As String = "F730311"
    Public Const MNU_FEE_TRANSPORTFEE_AGING As String = "F730305"
    Public Const MNU_TRANSPORT_CHEQUE_BOUNCE As String = "725125"
    'Other Fee Collection Details
    Public Const MNU_FEE_COLLECTION_DETAILS As String = "F720005"
    Public Const MNU_NORM_FEE_SPLITUP_DETAILS As String = "F720010"
    Public Const MNU_FEE_COLLECTION As String = "F300135"
    Public Const MNU_FEE_COLLECTION_OTHER As String = "F300143"

    Public Const MNU_ONLINE_FEECOLLECTION_VIEW As String = "F300220"
    'Online Fee Collection Details
    Public Const MNU_FEE_COLLECTION_ONLINE As String = "I000010" 
    Public Const MNU_ONLINE_FEECOLLECTION_HISTORY As String = "I000020"
    Public Const MNU_ONLINE_FEECOLLECTION_CHARGEDETAIL As String = "I000025"
    Public Const MNU_ONLINE_FEECOLLECTION_CHANGE_PWD As String = "I000015"
    Public Const MNU_ONLINE_FEECOLLECTION_AGEING As String = "I000030"



    'Normal Fee Collection Reports 
    Public Const MNU_FEE_REP_NORM_FEERECONCILIATION As String = "F725090"
    Public Const MNU_FEE_REP_NORM_FEEWISERECONCILIATION As String = "F725130"

    Public Const MNU_FEE_REP_NORM_FEEOUTSTANDING As String = "F725080"
    Public Const MNU_FEE_REP_NORM_FEECHARGEDETAILS As String = "F725095"
    Public Const MNU_FEE_REP_NORM_SUMMARY As String = "F725005"
    Public Const MNU_FEE_REP_NORM_COLLECTION_DET As String = "F725010"
    Public Const MNU_FEE_REP_NORM_COLLECTION_SUMMARY As String = "F725015"
    Public Const MNU_FEE_REP_NORM_COLL_SUMMARY_SCH As String = "F725020"
    Public Const MNU_FEE_REP_NORM_COLL_SUMMARY_EMP As String = "F725025"
    Public Const MNU_FEE_REP_NORM_COLL_SUMMARY_EMP_SCH As String = "F725030"
    Public Const MNU_FEE_REP_NORM_CHQ_COLL_SUMMARY As String = "F725035"
    Public Const MNU_FEE_NORM_COLL_PDC_SCH As String = "F725040"
    Public Const MNU_FEE_NORM_COLL_PDC_SCH_FEE As String = "F725045"
    Public Const MNU_FEE_NORM_COLL_STUDENTS As String = "F725050"
    Public Const MNU_FEE_NORM_COLL_CREDIT_CRD As String = "F725055"
    Public Const MNU_FEE_NORM_CHQ_CLEARANCE As String = "F725060"
    Public Const MNU_FEE_NORM_DAILY_CHQ_COLLECTION As String = "F725065"
    Public Const MNU_FEE_REP_NORM_FEE_BALANCELIST As String = "F725115"
    Public Const MNU_FEE_REP_NORM_FEESETUP As String = "F725120"
    Public Const MNU_FEE_REP_NORM_HEADVSCOLLECTION As String = "F725069"
    Public Const MNU_FEE_FEERECEIPTCANCELLATION_FEES As String = "F726065"

    Public Const MNU_FEE_REP_NORM_FEERECONCILIATION_WITHFO As String = "F725100"
    Public Const MNU_FEE_REP_NORM_MONTHLY_ADVANCESUMMARY As String = "F725105"
    Public Const MNU_FEE_STUDENT_AGING As String = "F725110"
    Public Const MNU_FEE_STUDENT_AGING_SUMMARY As String = "F725117"

    Public Const MNU_FEE_REP_TRANSPORT_FEECHARGEDETAILS As String = "F730315"
    Public Const MNU_FEE_REP_TRANSPORT_FEERECONCILIATION As String = "F730320"
    Public Const MNU_FEE_REP_TRANSPORT_FEERECONCILIATION_WITHFO As String = "F730325"
    Public Const MNU_FEE_REP_TRANSPORT_FEEOUTSTANDING As String = "F730335"
    Public Const MNU_FEE_REP_TRANSPORT_MONTHLY_ADVANCESUMMARY As String = "F730340"
    Public Const MNU_FEE_REP_TRANSPORT_FEE_ADJUSTMENT As String = "F730345"
    Public Const MNU_FEE_REP_TRANSPORT_FEECHARGEREVERSAL As String = "F730355"
    Public Const MNU_FEE_REP_TRANSPORT_FEERECONCILIATION_CONSOLIDATED As String = "F733030"

    Public Const MNU_FEE_REP_TRANSPORT_FEESERVICECHARGE_BUSWISE As String = "T100325"

    Public Const MNU_FEE_REP_TRANSPORT_FEEOUTSTANDING_CONSOLIDATED As String = "F733010"
    Public Const MNU_FEE_REP_TRANSPORT_FEEADVANCE_CONSOLIDATED As String = "F733015"
    Public Const MNU_FEE_REP_TRANSPORT_FEEADJUSTMENT_CONSOLIDATED As String = "F733020"
    Public Const MNU_FEE_REP_TRANSPORT_FEECONCESSION_CONSOLIDATED As String = "F733025"
#End Region

#Region "Transport Menu"
    Public Const MNU_TRAN_CHEQUSTATUS As String = "F715060"
    Public Const MNU_TRAN_CHEQUHAND As String = "F715061"
    Public Const MNU_TRANSPORT_REFUND_REQUEST As String = "F300167"
    Public Const MNU_TRANSPORT_REFUND_FEE_APPROVAL As String = "F351066"
#End Region

#Region "Budget Menu"
    Public Const MNU_BUDGET_CREATE_BUDGET As String = "B100205"
    Public Const MNU_BUDGET_STUDENTORSTAFF As String = "B100210"

#End Region

#Region "Trans_Type"
    Public Const TRANS_TYPE_Designation As Integer = 1
    Public Const TRANS_TYPE_Grade As Integer = 2
    Public Const TRANS_TYPE_Category As Integer = 3
    Public Const TRANS_TYPE_Mode_of_pay As Integer = 6
    Public Const TRANS_TYPE_Accomodation As Integer = 7
    Public Const TRANS_TYPE_Company_Transportation As Integer = 8
    Public Const TRANS_TYPE_Department As Integer = 9
    Public Const TRANS_TYPE_Employee_Account As Integer = 24
    Public Const TRANS_TYPE_Employee_Bank As Integer = 25
#End Region


End Class
