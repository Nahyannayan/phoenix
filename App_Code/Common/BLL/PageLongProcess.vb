Imports System
Imports System.Web
Imports System.Web.Configuration
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls 
Imports System.Data
Imports System.Configuration 
Imports System.Web.Security
Imports System.Web.UI 
Imports System.Web.UI.WebControls.WebParts 
Imports System.Runtime.Remoting.Messaging
Imports System.Threading

''' <summary> 
''' Used as the base class for all ASP.NET Web Forms in the application 
''' </summary> 
Public Class PageLongProcess
    Inherits System.Web.UI.Page
    Implements ICallbackEventHandler

    Public Delegate Function LongRun(ByVal name_NotUsing As String) As String

    Public Sub New()
        MyBase.New() 
        'AddHandler Me.PreInit, AddressOf Me.BasePage_PreInit
    End Sub

    Public Sub RaiseCallbackEvent(ByVal args As String) _
          Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
    End Sub

    Public Sub CallBackMethod(ByVal ar As IAsyncResult)
        Dim Result As AsyncResult = DirectCast(ar, AsyncResult)
        Dim LongRunProcess As LongRun = DirectCast(Result.AsyncDelegate, LongRun)
        Session("message") = LongRunProcess.EndInvoke(Result)
    End Sub

    Public Function GetCallbackResult() As String _
     Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        If Session("result") IsNot Nothing Then
            Dim res As IAsyncResult = DirectCast(Session("result"), IAsyncResult)
            If res.IsCompleted Then
                'Checking whether process is over or not
                Return "SUCCESS"
            End If
        End If
        Return "FAIL"
    End Function


    'Private Sub LongRunningMethod(ByVal name As String)
    '    'This will run for 5 mins
    '    Thread.Sleep(60000 * 5)
    'End Sub


    'Private Function DoSomthing() As IAsyncResult
    '    Dim [Long] As New LongRun(AddressOf LongRunningMethod)
    '    ' Delegate will call LongRunningMethod
    '    Dim ar As IAsyncResult = [Long].BeginInvoke("CHECK", New AsyncCallback(AddressOf CallBackMethod), Nothing)
    '    '�CHECK� will go as function parameter to LongRunningMethod
    '    'Once LongRunningMethod get over CallBackMethod method will be invoked.
    '    Return ar
    'End Function 

    'Protected Sub Button1_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Response.Write("Long running process is started")
    '    Session("result") = Nothing
    '    Dim res As IAsyncResult = DoSomthing()
    '    'You can redirect to some other page, prcoess will continue..
    '    'since we are using session variable it will be available on another page also
    '    'and you just need to check for process completion as we did in GetCallbackResult().
    '    'On that page also we will have to use ICallback for priodic check.
    '    Session("result") = res
    'End Sub

End Class

 
