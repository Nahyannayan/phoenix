﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Threading.Tasks

''' <summary>
''' Fictional utility class that executes a long-running process.
''' </summary>
Public Class WorkProcessor
    ''' <summary>
    ''' Non-blocking call that starts running the long-running process.
    ''' </summary>
    Public Sub StartProcessing()
        ' Reset the properties that report status.
        IsComplete = False
        IsRunning = True
        PercentComplete = 0

        ' Kick off the actual, private long-running process in a new Task
        task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                StartLongRunningProcess()
                                                            End Sub)
    End Sub

    ''' <summary>
    ''' Gets the percent complete for the long-running process.
    ''' </summary>
    Private privatePercentComplete As Double
    Public Property PercentComplete() As Double
        Get
            Return privatePercentComplete
        End Get
        Protected Set(ByVal value As Double)
            privatePercentComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is still executing.
    ''' </summary>
    Private privateIsRunning As Boolean
    Public Property IsRunning() As Boolean
        Get
            Return privateIsRunning
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsRunning = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is done running.
    ''' </summary>
    Private privateIsComplete As Boolean
    Public Property IsComplete() As Boolean
        Get
            Return privateIsComplete
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a brief description of what is currently being worked on, to update the end-user.
    ''' </summary>
    Public Property WorkDescription() As String

    ''' <summary>
    ''' Event for when progress has changed.
    ''' </summary>
    Public Event ProgressChanged As EventHandler(Of ProgressChangedEventArgs)

    ''' <summary>
    ''' Event for the the long-running process is complete.
    ''' </summary>
    Public Event ProcessComplete As EventHandler

#Region "Private Support Members"
    ''' <summary>
    ''' Private handle to the task that is currently execute the long-running process.
    ''' </summary>
    Private task As Task

    ''' <summary>
    ''' Private, blocking method that actually runs the long-running process.
    ''' </summary>
    Private Sub StartLongRunningProcess()
        IsRunning = True

        ' SIMULATE LONG-RUNNING PROCESS
        ' TODO: Your real work would be put here.
        For counter As Integer = 0 To 24
            UpdatePercent(counter, "Starting...")
            Thread.Sleep(75)
        Next counter

        For counter As Integer = 25 To 49
            UpdatePercent(counter, "Doing some stuff.")
            Thread.Sleep(200)
        Next counter

        For counter As Integer = 50 To 74
            UpdatePercent(counter, "Other stuff being done...")
            Thread.Sleep(100)
        Next counter

        For counter As Integer = 75 To 99
            UpdatePercent(counter, "New stuff...")
            Thread.Sleep(80)
        Next counter

        UpdatePercent(100, "Done.")
        Thread.Sleep(1000)

        RaiseEvent ProcessComplete(Me, New EventArgs())
        IsRunning = False
    End Sub

    Private Sub UpdatePercent(ByVal newPercentComplete As Double, ByVal workDescription As String)
        ' Update the properties for any callers casually checking the status.
        Me.PercentComplete = newPercentComplete
        Me.WorkDescription = workDescription
        If newPercentComplete = 100 Then
            IsComplete = True
        End If

        ' Fire the event for any callers who actively want to be notifed.
        RaiseEvent ProgressChanged(Me, New ProgressChangedEventArgs(newPercentComplete, IsComplete, workDescription))
    End Sub
#End Region
End Class
