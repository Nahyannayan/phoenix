Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class BusinessUnitSelection

    Shared Function BindFinancialYear() As DataTable
        Dim str_Sql As String = "SELECT FYR_ID, FYR_DESCR, bDefault FROM  FINANCIALYEAR_S  " _
            & " ORDER BY bDefault DESC, FYR_ID DESC"
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql).Tables(0)
    End Function

    Shared Function BindBusinessUnit(ByVal sUsr_name As String) As DataTable
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select * from [fn_GetBusinessUnits] " _
        & " ('" & sUsr_name & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME").Tables(0)
    End Function

End Class
