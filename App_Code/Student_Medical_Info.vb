﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic

Public Class Student_Medical_Info

    Private _STUID As String
    Private _STUNO As String
    Private _STUNAME As String
    Private _STUDOB As String
    Private _STUAGE As String
    Private _STUGENDER As String
    Private _STUPHOTO As String
    Private _STURELIGION As String
    Private _STUGRD As String

    Private _STURNAME As String
    Private _STURELATION As String
    Private _STU_RNationality As String
    Private _STU_RAddress As String
    Private _STU_RCity As String
    Private _STU_RCountry As String
    Private _STU_REmail As String
    Private _STU_RContactNumber As String
    Private _STU_ROccupation As String
    Private _STU_RCompany As String

    Private _STU_ECAddress As String
    Private _STU_ECCity As String
    Private _STU_ECCountry As String
    Private _STU_ECEmail As String
    Private _STU_ECContactNumber As String
    Private _STU_ECOccupation As String
    Private _STU_ECCompany As String
    Private _STU_ECContactName As String


    Private _STU_bALLERGIES As String
    Private _STU_ALLERGIES As String
    Private _STU_bRCVSPMEDICATION As String
    Private _STU_SPMEDICATION As String
    Private _STU_bPHYSICAL As String
    Private _STU_PHYSICAL As String
    Private _STU_bHEALTH As String
    Private _STU_HEALTH As String
    Private _STU_bDisabled As String
    Private _STU_Disabled As String
    Private _STU_bSEN As String
    Private _STU_SEN_REMARK As String
    Private _StudIncidentDetail As List(Of StudIncidentDetail)
    Private _StudMedicationDetail As List(Of StudMedicationDetail)


    Public Property STU_ID() As String
        Get
            Return _STUID
        End Get
        Set(ByVal value As String)
            _STUID = value
        End Set
    End Property

    Public Property STU_NO() As String
        Get
            Return _STUNO
        End Get
        Set(ByVal value As String)
            _STUNO = value
        End Set
    End Property

    Public Property STU_DOB() As String
        Get
            Return _STUDOB
        End Get
        Set(ByVal value As String)
            _STUDOB = value
        End Set
    End Property


    Public Property STU_AGE() As String
        Get
            Return _STUAGE
        End Get
        Set(ByVal value As String)
            _STUAGE = value
        End Set
    End Property


    Public Property STU_GENDER() As String
        Get
            Return _STUGENDER
        End Get
        Set(ByVal value As String)
            _STUGENDER = value
        End Set
    End Property


    Public Property STU_NAME() As String
        Get
            Return _STUNAME
        End Get
        Set(ByVal value As String)
            _STUNAME = value
        End Set
    End Property



    Public Property STU_PHOTO() As String
        Get
            Return _STUPHOTO
        End Get
        Set(ByVal value As String)
            _STUPHOTO = value
        End Set
    End Property

    Public Property STU_RELIGION() As String
        Get
            Return _STURELIGION
        End Get
        Set(ByVal value As String)
            _STURELIGION = value
        End Set
    End Property

    Public Property STU_GRD() As String
        Get
            Return _STUGRD
        End Get
        Set(ByVal value As String)
            _STUGRD = value
        End Set
    End Property



    Public Property STU_RNAME() As String
        Get
            Return _STURNAME
        End Get
        Set(ByVal value As String)
            _STURNAME = value
        End Set
    End Property

    Public Property STU_RELATION() As String
        Get
            Return _STURELATION
        End Get
        Set(ByVal value As String)
            _STURELATION = value
        End Set
    End Property


    Public Property STU_RNationality() As String
        Get
            Return _STU_RNationality
        End Get
        Set(ByVal value As String)
            _STU_RNationality = value
        End Set
    End Property
    Public Property STU_RAddress() As String
        Get
            Return _STU_RAddress
        End Get
        Set(ByVal value As String)
            _STU_RAddress = value
        End Set
    End Property

    Public Property STU_RCity() As String
        Get
            Return _STU_RCity
        End Get
        Set(ByVal value As String)
            _STU_RCity = value
        End Set
    End Property



    Public Property STU_RCountry() As String
        Get
            Return _STU_RCountry
        End Get
        Set(ByVal value As String)
            _STU_RCountry = value
        End Set
    End Property



    Public Property STU_REmail() As String
        Get
            Return _STU_REmail
        End Get
        Set(ByVal value As String)
            _STU_REmail = value
        End Set
    End Property


    Public Property STU_RContactNumber() As String
        Get
            Return _STU_RContactNumber
        End Get
        Set(ByVal value As String)
            _STU_RContactNumber = value
        End Set
    End Property

    Public Property STU_ROccupation() As String
        Get
            Return _STU_ROccupation
        End Get
        Set(ByVal value As String)
            _STU_ROccupation = value
        End Set
    End Property

    Public Property STU_RCompany() As String
        Get
            Return _STU_RCompany
        End Get
        Set(ByVal value As String)
            _STU_RCompany = value
        End Set
    End Property


    Public Property STU_ECAddress() As String
        Get
            Return _STU_ECAddress
        End Get
        Set(ByVal value As String)
            _STU_ECAddress = value
        End Set
    End Property



    Public Property STU_ECCity() As String
        Get
            Return _STU_ECCity
        End Get
        Set(ByVal value As String)
            _STU_ECCity = value
        End Set
    End Property

    Public Property STU_ECCountry() As String
        Get
            Return _STU_ECCountry
        End Get
        Set(ByVal value As String)
            _STU_ECCountry = value
        End Set
    End Property

    Public Property STU_ECEmail() As String
        Get
            Return _STU_ECEmail
        End Get
        Set(ByVal value As String)
            _STU_ECEmail = value
        End Set
    End Property

    Public Property STU_ECContactNumber() As String
        Get
            Return _STU_ECContactNumber
        End Get
        Set(ByVal value As String)
            _STU_ECContactNumber = value
        End Set
    End Property


    Public Property STU_ECContactName() As String
        Get
            Return _STU_ECContactName
        End Get
        Set(ByVal value As String)
            _STU_ECContactName = value
        End Set
    End Property

    Public Property STU_ECOccupation() As String
        Get
            Return _STU_ECOccupation
        End Get
        Set(ByVal value As String)
            _STU_ECOccupation = value
        End Set
    End Property

    Public Property STU_ECCompany() As String
        Get
            Return _STU_ECCompany
        End Get
        Set(ByVal value As String)
            _STU_ECCompany = value
        End Set
    End Property


    Public Property STU_bALLERGIES() As String
        Get
            Return _STU_bALLERGIES
        End Get
        Set(ByVal value As String)
            _STU_bALLERGIES = value
        End Set
    End Property


    Public Property STU_ALLERGIES() As String
        Get
            Return _STU_ALLERGIES
        End Get
        Set(ByVal value As String)
            _STU_ALLERGIES = value
        End Set
    End Property

    Public Property STU_bRCVSPMEDICATION() As String
        Get
            Return _STU_bRCVSPMEDICATION
        End Get
        Set(ByVal value As String)
            _STU_bRCVSPMEDICATION = value
        End Set
    End Property


    Public Property STU_SPMEDICATION() As String
        Get
            Return _STU_SPMEDICATION
        End Get
        Set(ByVal value As String)
            _STU_SPMEDICATION = value
        End Set
    End Property

    Public Property STU_bPHYSICAL() As String
        Get
            Return _STU_bPHYSICAL
        End Get
        Set(ByVal value As String)
            _STU_bPHYSICAL = value
        End Set
    End Property


    Public Property STU_PHYSICAL() As String
        Get
            Return _STU_PHYSICAL
        End Get
        Set(ByVal value As String)
            _STU_PHYSICAL = value
        End Set
    End Property

    Public Property STU_bHEALTH() As String
        Get
            Return _STU_bHEALTH
        End Get
        Set(ByVal value As String)
            _STU_bHEALTH = value
        End Set
    End Property


    Public Property STU_HEALTH() As String
        Get
            Return _STU_HEALTH
        End Get
        Set(ByVal value As String)
            _STU_HEALTH = value
        End Set
    End Property


    Public Property STU_bDisabled() As String
        Get
            Return _STU_bDisabled
        End Get
        Set(ByVal value As String)
            _STU_bDisabled = value
        End Set
    End Property


    Public Property STU_Disabled() As String
        Get
            Return _STU_Disabled
        End Get
        Set(ByVal value As String)
            _STU_Disabled = value
        End Set
    End Property

    Public Property STU_bSEN() As String
        Get
            Return _STU_bSEN
        End Get
        Set(ByVal value As String)
            _STU_bSEN = value
        End Set
    End Property


    Public Property STU_SEN_REMARK() As String
        Get
            Return _STU_SEN_REMARK
        End Get
        Set(ByVal value As String)
            _STU_SEN_REMARK = value
        End Set
    End Property



    Public Property StudIncidentDetail() As List(Of StudIncidentDetail)
        Get
            Return _StudIncidentDetail
        End Get
        Set(ByVal value As List(Of StudIncidentDetail))
            _StudIncidentDetail = value
        End Set
    End Property

    Public Property StudMedicationDetail() As List(Of StudMedicationDetail)
        Get
            Return _StudMedicationDetail
        End Get
        Set(ByVal value As List(Of StudMedicationDetail))
            _StudMedicationDetail = value
        End Set
    End Property

End Class
