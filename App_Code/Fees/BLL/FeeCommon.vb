Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data 
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class FeeCommon

    Public Shared Function GetAcademicYear(ByVal BSU_ID As String, ByVal Mode As String) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar)
        pParms(1).Value = Mode
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetAcademicYear", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetBSUAcademicYear(ByVal BSU_ID As String) As DataTable
        Dim sql_query As String = " EXEC FEES.GetBSUAcademicYear '" & BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function GetAcademicYear_CURRENT_NEXT(ByVal BSU_ID As String) As DataTable
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
      
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "[FEES].[GET_ACADEMICYEAR_CURR_NEXT]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetBSUAcademicYear(ByVal BSU_ID As String, ByVal Normal As Boolean) As DataTable
        Dim sql_query As String = " SELECT ACY.ACY_DESCR  AS ACY_DESCR, ACD.ACD_CURRENT, ACD.ACD_ID " _
        & " FROM ACADEMICYEAR_D AS ACD INNER JOIN ACADEMICYEAR_M AS ACY ON ACD.ACD_ACY_ID = ACY.ACY_ID" _
        & " INNER JOIN CURRICULUM_M AS CLM ON ACD.ACD_CLM_ID = CLM.CLM_ID" _
        & " WHERE     (ACD.ACD_BSU_ID = '" & BSU_ID & "') AND (ISNULL(ACD.ACD_bCLOSE, 0) = 0) " _
        & " ORDER BY ACD.ACD_STARTDT"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function getFeeTypes(ByVal BSU_ID As String, Optional ByVal ACD_ID As Integer = 0) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@SVB_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[FEE_GETFEESFORCOLLECTION]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function DisplayGrade(ByVal vGRD_ID As String, ByVal BsuID As String) As String
        Dim str_data As String = String.Empty
        Dim str_sql As String = String.Empty
        str_sql = "select GRM_DISPLAY from GRADE_BSU_M WHERE GRM_GRD_ID = '" & vGRD_ID & "' AND GRM_BSU_ID = '" & BsuID & "'"
        str_data = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, Data.CommandType.Text, str_sql)
        Return str_data
    End Function

    Public Shared Function GetCurrentAcademicYear(ByVal BSU_ID As String) As String
        Dim sql_query As String = "Select ACADEMICYEAR_D.ACD_ID as ACD_ID FROM ACADEMICYEAR_D " & _
        " where ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "'and isnull(ACADEMICYEAR_D.ACD_bCLOSE,0)=0 " & _
        " AND ISNULL(ACD_CURRENT, 0) = 1 "
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
    End Function

    Public Shared Function GetPayDateforTransport(ByVal BSU_ID As String) As String
        Dim sql_query As String = "SELECT BSU_FEE_PAY_DATE FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"
        Dim feePayDate As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, _
        CommandType.Text, sql_query)
        If Not feePayDate Is Nothing AndAlso Not feePayDate Is DBNull.Value Then
            Return Format(feePayDate, OASISConstants.DateFormat)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetNewInvoiceNo(ByVal DocID As String, ByVal dt As DateTime, ByVal vBSU_ID As String, ByVal ACD_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[ReturnNextNo]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpDOC_ID As New SqlParameter("@DOC_ID", SqlDbType.VarChar)
        sqlpDOC_ID.Value = DocID
        cmd.Parameters.Add(sqlpDOC_ID)

        Dim sqlpFPH_BSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_BSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpDOS_MONTH As New SqlParameter("@DOS_MONTH", SqlDbType.TinyInt)
        sqlpDOS_MONTH.Value = dt.Month
        cmd.Parameters.Add(sqlpDOS_MONTH)

        Dim sqlpDOS_YEAR As New SqlParameter("@DOS_YEAR", SqlDbType.Int)
        sqlpDOS_YEAR.Value = dt.Year
        cmd.Parameters.Add(sqlpDOS_YEAR)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NEW_INV_NO As New SqlParameter("@NextNO", SqlDbType.VarChar, 20)
        NEW_INV_NO.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NEW_INV_NO)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return ""
        Else
            Return NEW_INV_NO.Value
        End If

    End Function

    Public Shared Function CheckFeeclose(ByVal vBSU_ID As String, ByVal dt As DateTime, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[CheckFeeCloseforCollection]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFPH_BSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_BSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpDATE As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDATE.Value = dt
        cmd.Parameters.Add(sqlpDATE)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        Return iReturnvalue.ToString()
    End Function

    Public Shared Function CheckFeeclose(ByVal BSU_ID As String, ByVal dt As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(2).Value = Convert.ToDateTime(dt)
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[FEES].[CheckFeeCloseforCollection]", pParms)
        CheckFeeclose = pParms(1).Value
    End Function

    Public Shared Function CheckDayEnd(ByVal BSU_ID As String, ByVal dt As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(2).Value = Convert.ToDateTime(dt)
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[FEES].[CheckDayEnd]", pParms)
        CheckDayEnd = pParms(1).Value
    End Function

    Public Shared Function UpdateNextInvoiceNo(ByVal DocID As String, ByVal vInvoiceNo As String, ByVal dt As DateTime, ByVal vBSU_ID As String, ByVal ACD_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("OASIS.dbo.UpdateNextNo", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpDOC_ID As New SqlParameter("@DOC_ID", SqlDbType.VarChar)
        sqlpDOC_ID.Value = DocID
        cmd.Parameters.Add(sqlpDOC_ID)

        Dim sqlpFPH_BSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_BSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpDOS_MONTH As New SqlParameter("@DOS_MONTH", SqlDbType.TinyInt)
        sqlpDOS_MONTH.Value = dt.Month
        cmd.Parameters.Add(sqlpDOS_MONTH)

        Dim sqlpDOS_YEAR As New SqlParameter("@DOS_YEAR", SqlDbType.Int)
        sqlpDOS_YEAR.Value = dt.Year
        cmd.Parameters.Add(sqlpDOS_YEAR)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        Return iReturnvalue
    End Function

    Public Shared Function GetSERVICES_SYS(ByVal BSU_ID As String) As DataTable
        Dim sql_query As String = "SELECT     SVB.SVB_ID,   SVC.SVC_DESCRIPTION" _
            & " FROM         SERVICES_BSU_M AS SVB INNER JOIN" _
            & " SERVICES_SYS_M AS SVC ON SVB.SVB_SVC_ID = SVC.SVC_ID" _
            & " WHERE  SVB.SVB_BSU_ID='" & BSU_ID & "'AND SVB.SVB_bAVAILABLE=1"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetTPTPICKUPPOINTS_M(ByVal BSU_ID As String) As DataTable
        Dim sql_query As String = " SELECT PNT_ID , PNT_DESCRIPTION FROM TPTPICKUPPOINTS_M" _
        & " WHERE PNT_BSU_ID='" & BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFEETYPE_M() As DataTable 
        Dim sql_query As String = " SELECT FTP_ID , FTP_DESCR  FROM  FEES.FEETYPE_M "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetSCHEDULE_M(ByVal bisCollection As Boolean) As DataTable
        Dim sql_query As String
        If bisCollection Then
            sql_query = "SELECT SCH_ID, SCH_DESCR FROM FEES.SCHEDULE_M where SCH_DESCR not like '%Quarterly%'"
        Else
            sql_query = "SELECT SCH_ID, SCH_DESCR FROM FEES.SCHEDULE_M"
        End If
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetSCHEDULE_PAYMENTPLAN() As DataTable
        Dim sql_query As String
        sql_query = "SELECT SCH_ID, SCH_DESCR FROM FEES.SCHEDULE_M WHERE SCH_ID IN(0,2,4)"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Sub GetTermStartDate(ByVal p_date As DateTime, ByVal vACD_ID As Integer, ByVal vBSU_ID As String, ByRef fromDT As String, ByRef toDate As String)
        Dim drReader As SqlDataReader
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT TRM_STARTDATE, TRM_ENDDATE FROM TRM_M WHERE TRM_BSU_ID  = '" & vBSU_ID & "' AND TRM_ACD_ID = " & vACD_ID
            drReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                fromDT = Format(drReader("TRM_STARTDATE"), OASISConstants.DateFormat)
                toDate = Format(drReader("TRM_ENDDATE"), OASISConstants.DateFormat)
                Exit While
            End While
        End Using
    End Sub

    Public Shared Function GetSERVICES_SYS_M() As DataTable
        Dim sql_query As String = "SELECT SVC_ID,  SVC_DESCRIPTION FROM SERVICES_SYS_M union select 0,'None'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetGRADE_M(ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim sql_query As String = "SELECT GRM.GRM_GRD_ID, GRM.GRM_DISPLAY FROM GRADE_BSU_M AS GRM INNER JOIN " _
        & " GRADE_M AS GRD ON GRM.GRM_GRD_ID = GRD.GRD_ID  WHERE GRM.GRM_BSU_ID = '" & BSU_ID & "' AND GRM.GRM_ACD_ID = '" & ACD_ID & "'" _
        & " ORDER BY GRD.GRD_DISPLAYORDER "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetSTREAM_M(ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim sql_query As String = "SELECT  STM_ID, STM_DESCR FROM STREAM_M " _
        & " WHERE  STM_ID IN (SELECT GRM_STM_ID " _
        & " FROM GRADE_BSU_M WHERE (GRM_BSU_ID = '" & BSU_ID & "') AND (GRM_ACD_ID = '" & ACD_ID & "') )"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFEES_M(ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim JoQuery = ""
        Dim sql_query As String = " SELECT FEES.FEES_M.FEE_ID, FEES.FEES_M.FEE_DESCR " _
        & " FROM FEES.FEES_M    where  FEE_ID NOT IN ( " _
        & " SELECT FEES.FEES_M.FEE_ID FROM  FEES.FEES_M INNER JOIN " _
        & " oasis..SERVICES_BSU_M AS SVB ON FEES.FEES_M.FEE_SVC_ID = SVB.SVB_SVC_ID" _
        & " AND SVB.SVB_PROVIDER_BSU_ID<> SVB.SVB_BSU_ID AND SVB.SVB_BSU_ID='" & BSU_ID & "'" _
        & " AND SVB_ACD_ID='" & ACD_ID & "' ) AND  FEE_BReverseCharge='False' OR FEE_BReverseCharge IS NULL  ORDER BY FEES.FEES_M.FEE_ORDER  "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFEES_M_FOR_CONCESSION() As DataTable
        Dim sql_query As String = "SELECT FEE_ID , FEE_DESCR FROM FEES.FEES_M WHERE isnull(FEE_bConcession, 0) = 1 ORDER BY FEE_ORDER"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFEES_M_TRANSPORT() As DataTable
        Dim sql_query As String = "SELECT FEE_ID , FEE_DESCR FROM FEES.FEES_M where FEE_SVC_ID=1"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetACDStartDate(ByVal p_ACD_ID As Integer) As String
        Dim sql_query As String = " SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE (ACD_ID = '" & p_ACD_ID & "') "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return Format(dsData.Tables(0).Rows(0)(0), OASISConstants.DateFormat)
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetBSUPrintReceiptsCnt(ByVal BSU_ID As String) As Integer
        Dim sql_query As String = " SELECT ISNULL(BSU_ReceiptCopies,0) FROm BUSINESSUNIT_M WHERE BSU_ID='" & BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return CInt(dsData.Tables(0).Rows(0).Item(0))
        Else
            Return 0
        End If
    End Function

    Public Shared Function SERVICES_BSU_M(ByVal USR_ID As String, ByVal BSU_ID As String) As DataTable
        Dim sql_query As String = "EXEC gETUNITSFORTRANSPORTFEES '" & USR_ID & "' ,  '" & BSU_ID & "'  "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetACADEMIC_MonthorTerm(ByVal ACD_ID As Integer, ByVal bTerm As Boolean) As DataTable
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(0).Value = ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@bTerm", SqlDbType.Bit)
        pParms(1).Value = bTerm
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetACADEMIC_MonthorTerm", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFeeSCH_ID_ForConcession(ByVal p_STU_NO As String, ByVal p_FEE_ID As String, _
        ByVal p_ACD_ID As String, ByVal p_BSU_ID As String, ByRef TOTALAMOUNT As Decimal) As String
        ' FEES.GetFeeSCH_ID_ForConcession
        '@STU_ID BIGINT,
        '@FEE_ID INT,
        '@ACD_ID BIGINT,
        '@BSU_ID VARCHAR(20) 
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_STU_NO
        pParms(1) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(1).Value = p_FEE_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetFeeSCH_ID_ForConcession", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            If IsNumeric(dsData.Tables(0).Rows(0)(1)) Then
                TOTALAMOUNT = dsData.Tables(0).Rows(0)(1)
            End If
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If 
    End Function

    Public Shared Function GetFeeSCH_ID_ForConcession_Transport(ByVal p_STU_NO As String, ByVal p_FEE_ID As String, _
            ByVal p_ACD_ID As String, ByVal p_BSU_ID As String, ByVal p_SBL_ID As String, ByRef TOTALAMOUNT As Decimal, _
            ByVal p_DT As String) As String
        ' FEES.GetFeeSCH_ID_ForConcession
        '@STU_ID BIGINT,
        '@FEE_ID INT,
        '@ACD_ID BIGINT,
        '@BSU_ID VARCHAR(20) 
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_STU_NO
        pParms(1) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(1).Value = p_FEE_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.Int)
        pParms(5).Value = p_SBL_ID
        pParms(6) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(6).Value = p_DT

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
          CommandType.StoredProcedure, "FEES.GetFeeSCH_ID_ForConcession", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            If IsNumeric(dsData.Tables(0).Rows(0)(1)) Then
                TOTALAMOUNT = dsData.Tables(0).Rows(0)(1)
            End If
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetFee_MonthorTerm(ByVal p_FEE_ID As String, ByVal p_ACD_ID As String, _
    ByVal p_BSU_ID As String, ByVal p_bTerm As Boolean, ByVal p_STU_ID As String, ByVal p_DT As String) As DataTable
        ' (FEES.GetFee_MonthorTerm), @ACD_ID BIGINT,,@BSU_ID VARCHAR(20), @FEE_ID INT, @bTerm BIT 
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@bTerm", SqlDbType.Bit)
        pParms(0).Value = p_bTerm
        pParms(1) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(1).Value = p_FEE_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(5).Value = p_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(6).Value = p_DT
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetFee_MonthorTerm", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFee_MonthorTerm_OASIS(ByVal p_FEE_ID As String, ByVal p_ACD_ID As String, _
    ByVal p_BSU_ID As String, ByVal p_STU_ID As String, ByVal p_DT As String, ByVal p_TODT As String, _
    ByRef TOTALAMOUNT As String, ByRef SCH_ID As String) As DataTable
        ' @ACD_ID BIGINT,@BSU_ID VARCHAR(20),@FEE_ID INT, @STU_ID BIGINT,@DT datetime,@TODT DATETIME,@TOTALAMOUNT NUMERIC (18,3) OUTPUT
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TOTALAMOUNT", SqlDbType.Decimal)
        pParms(0).Direction = ParameterDirection.Output
        pParms(1) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(1).Value = p_FEE_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(5).Value = p_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(6).Value = p_DT
        pParms(7) = New SqlClient.SqlParameter("@TODT", SqlDbType.DateTime)
        pParms(7).Value = p_TODT
        pParms(8) = New SqlClient.SqlParameter("@SCH_ID", SqlDbType.Decimal)
        pParms(8).Direction = ParameterDirection.Output

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetFee_MonthorTerm_OASIS", pParms)
        If Not dsData Is Nothing Then
            If Not pParms(0).Value Is System.DBNull.Value And _
            Not pParms(8).Value Is System.DBNull.Value  Then
                TOTALAMOUNT = pParms(0).Value
                SCH_ID = pParms(8).Value
            End If
            Return dsData.Tables(0)
        Else
            TOTALAMOUNT = 0
            SCH_ID = 0
            Return Nothing
        End If
    End Function

    Public Shared Function GetFee_MonthorTerm_Transport(ByVal p_FEE_ID As String, ByVal p_ACD_ID As String, _
    ByVal p_BSU_ID As String, ByVal p_bTerm As Boolean, ByVal p_STU_ID As String, ByVal p_SBL_ID As String) As DataTable
        ' (FEES.GetFee_MonthorTerm) 
        '@ACD_ID BIGINT, @BSU_ID VARCHAR(20), @FEE_ID INT,  @bTerm BIT ,@SBL_ID 
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@bTerm", SqlDbType.Bit)
        pParms(0).Value = p_bTerm
        pParms(1) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(1).Value = p_FEE_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(5).Value = p_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.BigInt)
        pParms(6).Value = p_SBL_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
          CommandType.StoredProcedure, "FEES.GetFee_MonthorTerm", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFee_MonthorTermforConcession_Transport(ByVal p_FEE_ID As String, ByVal p_ACD_ID As String, _
        ByVal p_BSU_ID As String, ByVal p_bTerm As Boolean, ByVal p_STU_ID As String, ByVal p_SBL_ID As String, ByVal p_DT As String) As DataTable
        ' (FEES.GetFee_MonthorTerm) 
        '@ACD_ID BIGINT, @BSU_ID VARCHAR(20), @FEE_ID INT,  @bTerm BIT ,@SBL_ID 
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@bTerm", SqlDbType.Bit)
        pParms(0).Value = p_bTerm
        pParms(1) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(1).Value = p_FEE_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(5).Value = p_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.BigInt)
        pParms(6).Value = p_SBL_ID
        pParms(7) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(7).Value = p_DT
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTransportConnection, _
          CommandType.StoredProcedure, "FEES.GetFee_MonthorTermforConcession", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function AcademicYearStartEndDate(ByRef vFromDT As String, ByRef vToDT As String, ByVal ACD_ID As Integer, ByVal BSU_ID As String) As Boolean
        Dim drReader As SqlDataReader
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT ACD_STARTDT, ACD_ENDDT FROM ACADEMICYEAR_D WHERE ACD_BSU_ID = '" & BSU_ID & "' AND ACD_ID = " & ACD_ID
            drReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                vFromDT = Format(drReader("ACD_STARTDT"), OASISConstants.DateFormat)
                vToDT = Format(drReader("ACD_ENDDT"), OASISConstants.DateFormat)
                Exit While
            End While
        End Using
    End Function

    Public Shared Function GetStudentJoinDate(ByVal p_BSU_ID As String, ByVal p_STU_ID As String, ByVal p_IsTransport As Boolean) As String
        Dim str_conn As String
        If p_IsTransport Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Else
            str_conn = ConnectionManger.GetOASISConnectionString
        End If
        Dim sql_query As String = " SELECT STU_DOJ FROM STUDENT_M " _
        & " WHERE STU_BSU_ID='" & p_BSU_ID & "' AND STU_ID='" & p_STU_ID & "' "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(str_conn, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            If IsDate(dsData.Tables(0).Rows(0)(0)) Then
                Return Format(CDate(dsData.Tables(0).Rows(0)(0)), OASISConstants.DateFormat)
            End If
        End If
        Return ""
    End Function

    Public Shared Function GetStudentName(ByVal vSTUD_ID As String, ByVal bEnquiry As Boolean) As String
        Dim objName As Object
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String
            If bEnquiry Then
                sql_query = "SELECT ISNULL(dbo.ENQUIRY_M.EQM_APPLFIRSTNAME, '') + ' ' + " & _
                " ISNULL(dbo.ENQUIRY_M.EQM_APPLMIDNAME, '') + ' ' +  " & _
                " ISNULL(dbo.ENQUIRY_M.EQM_APPLLASTNAME, '') AS STUD_NAME  " & _
                " FROM dbo.ENQUIRY_M INNER JOIN " & _
                " ENQUIRY_SCHOOLPRIO_S ON ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID = ENQUIRY_M.EQM_ENQID  " & _
                " WHERE ENQUIRY_SCHOOLPRIO_S.EQS_ID = " & vSTUD_ID
            Else
                sql_query = "SELECT isnull(STU_FIRSTNAME,'') + ' ' + isnull(STU_MIDNAME,'') + ' ' + isnull(STU_LASTNAME,'')  STUD_NAME from dbo.STUDENT_M  WHERE STU_ID = " & vSTUD_ID
            End If
            objName = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
        End Using
        If objName Is DBNull.Value Or objName Is Nothing Then
            Return ""
        End If
        Return objName.ToString
    End Function

    Public Shared Function GetTransportStudentID(ByVal STU_NO As String, ByVal BSU_ID As String, _
    ByVal bEnquiry As Boolean, ByVal p_IsTransport As Boolean) As String
        Dim str_data, str_sql, str_conn As String
        If p_IsTransport Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Else
            str_conn = ConnectionManger.GetOASISConnectionString
        End If
        Dim iStdnolength As Integer = STU_NO.Length
        If bEnquiry Then
            str_sql = "SELECT STU_ID  FROM FEES.vw_OSO_ENQUIRY_COMP" _
             & " WHERE     (STU_BSU_ID = '" & BSU_ID & "') AND  STU_NO='" & STU_NO & "'"
            str_data = GetDataFromSQL(str_sql,str_conn)
        Else
            If iStdnolength < 9 Then
                If STU_NO.Trim.Length < 8 Then
                    For i As Integer = iStdnolength + 1 To 8
                        STU_NO = "0" & STU_NO
                    Next
                End If
                STU_NO = BSU_ID & STU_NO
            End If
            str_sql = "SELECT STU_ID  FROM VW_OSO_STUDENT_M" _
             & " WHERE (STU_BSU_ID = '" & BSU_ID & "') AND  STU_NO='" & STU_NO & "'"
            str_data = GetDataFromSQL(str_sql, str_conn)
        End If
        If str_data <> "--" Then
            Return str_data
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetStudentNo(ByVal vSTUD_ID As String, ByVal p_IsTransport As Boolean) As String
        Dim objName As Object
        Dim sql_query As String = "SELECT STU_NO from dbo.STUDENT_M  WHERE STU_ID = " & vSTUD_ID
        If p_IsTransport Then
            objName = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, _
                    CommandType.Text, sql_query)
        Else
            objName = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, _
                    CommandType.Text, sql_query)
        End If
        If objName Is DBNull.Value Or objName Is Nothing Then
            Return ""
        End If
        Return objName.ToString
    End Function

    Public Shared Function GETBSUFORUSER(ByVal USR_ID As String) As DataTable
        Dim sql_query As String = "EXEC GETBSUFORUSER '" & USR_ID & "'   "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function getADJREASON(ByVal bShow As Boolean) As DataTable
        Dim sql_query As String = "EXEC getADJ_REASON " & bShow & "   "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetStudentWithLocation_Transport(ByRef STU_ID As String, ByRef STU_NO As String, _
    ByRef STU_NAME As String, ByVal p_BSU_ID As String, ByVal p_IsEnquiry As Boolean, ByRef SBL_ID As String, _
    ByRef SBL_DESCR As String, ByVal p_DT As String, ByVal p_ACD_ID As String) As Boolean
        ' GetStudentWithLocation @STU_ID VARCHAR(30),
        '@STU_NO VARCHAR(30),
        '@BSU_ID VARCHAR(20),
        '@bENQUIRY BIT  
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 30)
        pParms(0).Value = STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 30)
        pParms(1).Value = STU_NO
        pParms(2) = New SqlClient.SqlParameter("@bENQUIRY", SqlDbType.Bit)
        pParms(2).Value = p_IsEnquiry
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(5).Value = p_DT
        pParms(6) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(6).Value = p_ACD_ID
        Dim dsData As New DataSet
        dsData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
         CommandType.StoredProcedure, "FEES.GetStudentWithLocation", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            STU_ID = dsData.Tables(0).Rows(0)("STU_ID").ToString
            STU_NO = dsData.Tables(0).Rows(0)("STU_NO").ToString
            SBL_ID = dsData.Tables(0).Rows(0)("SBL_ID").ToString
            STU_NAME = dsData.Tables(0).Rows(0)("STU_NAME").ToString
            SBL_DESCR = dsData.Tables(0).Rows(0)("SBL_DESCR").ToString
            Return True
        Else
            STU_ID = ""
            STU_NAME = ""
            SBL_ID = ""
            SBL_DESCR = ""
            Return False
        End If
    End Function

    Public Shared Function GetStudentWithLocation_ForPerforma(ByRef STU_ID As String, ByRef STU_NO As String, _
        ByRef STU_NAME As String, ByVal p_BSU_ID As String, ByVal p_IsEnquiry As Boolean, ByRef SBL_ID As String, _
        ByRef SBL_DESCR As String, ByVal p_DT As String, ByVal p_ACD_ID As String) As Boolean
        ' GetStudentWithLocation @STU_ID VARCHAR(30),
        '@STU_NO VARCHAR(30),
        '@BSU_ID VARCHAR(20),
        '@bENQUIRY BIT  
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 30)
        pParms(0).Value = STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 30)
        pParms(1).Value = STU_NO
        pParms(2) = New SqlClient.SqlParameter("@bENQUIRY", SqlDbType.Bit)
        pParms(2).Value = p_IsEnquiry
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(5).Value = p_DT
        pParms(6) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(6).Value = p_ACD_ID
        Dim dsData As New DataSet
        dsData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
         CommandType.StoredProcedure, "FEES.GetStudentWithLocation_ForPerforma", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            STU_ID = dsData.Tables(0).Rows(0)("STU_ID").ToString
            STU_NO = dsData.Tables(0).Rows(0)("STU_NO").ToString
            SBL_ID = dsData.Tables(0).Rows(0)("SBL_ID").ToString
            STU_NAME = dsData.Tables(0).Rows(0)("STU_NAME").ToString
            SBL_DESCR = dsData.Tables(0).Rows(0)("SBL_DESCR").ToString
            Return True
        Else
            STU_ID = ""
            STU_NAME = ""
            SBL_ID = ""
            SBL_DESCR = ""
            Return False
        End If
    End Function

    Public Shared Function GetBankName(ByVal p_BankShort As String, ByRef BankID As String, ByRef HeadOffice As String) As String
        Dim str_bank As String = ""
        Dim STR_SQL As String = ""
        Dim ds As New DataSet
        If p_BankShort <> "" Then
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = HttpContext.Current.Session("sBsuId").ToString()
            pParms(1) = New SqlClient.SqlParameter("@BNK_SEARCH_STR", SqlDbType.VarChar, 100)
            pParms(1).Value = p_BankShort
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_BANK_DETAILS", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                BankID = ds.Tables(0).Rows(0)("BNK_ID").ToString
                str_bank = ds.Tables(0).Rows(0)("BNK_DESCRIPTION").ToString
                HeadOffice = ds.Tables(0).Rows(0)("BNK_HEADOFFICE").ToString
            End If
            'STR_SQL = "SELECT BNK_ID, BNK_DESCRIPTION, BNK_SHORT,  BNK_HEADOFFICE " _
            '        & " FROM         BANK_M where BNK_SHORT='" & p_BankShort & "'"
            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            'CommandType.Text, STR_SQL)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    BankID = ds.Tables(0).Rows(0)("BNK_ID")
            '    str_bank = ds.Tables(0).Rows(0)("BNK_DESCRIPTION").ToString
            '    HeadOffice = ds.Tables(0).Rows(0)("BNK_HEADOFFICE").ToString
            'Else
            '    STR_SQL = "SELECT BNK_ID, BNK_DESCRIPTION, BNK_SHORT,  BNK_HEADOFFICE " _
            '                      & " FROM         BANK_M where BNK_DESCRIPTION='" & p_BankShort & "'"
            '    ds.Tables.Clear()
            '    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            '    CommandType.Text, STR_SQL)
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        BankID = ds.Tables(0).Rows(0)("BNK_ID")
            '        str_bank = ds.Tables(0).Rows(0)("BNK_DESCRIPTION").ToString
            '        HeadOffice = ds.Tables(0).Rows(0)("BNK_HEADOFFICE").ToString
            '    End If
            'End If
        End If
        Return str_bank
    End Function

    Public Shared Function DIS_SelectDiscount(ByVal p_DT As String, ByVal p_STU_ID As String, _
        ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal STM_ID As String, ByVal bAll As Boolean) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(0).Value = p_DT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@STM_ID", SqlDbType.VarChar, 20)
        If STM_ID = "" Then
            pParms(4).Value = System.DBNull.Value
        Else
            pParms(4).Value = STM_ID
        End If
        pParms(5) = New SqlClient.SqlParameter("@bAll", SqlDbType.VarChar, 20)
        pParms(5).Value = bAll
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.DIS_SelectDiscount", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetTransportProviders(ByVal USR_ID As String) As DataTable
        Dim sql_query As String = "EXEC [Transport].[GetTransportProviders]  '" & USR_ID & "' "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function FEE_GETFEESFORCOLLECTION(ByVal BSU_ID As String, Optional ByVal ACD_ID As Integer = 0) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@SVB_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
          CommandType.StoredProcedure, "[TRANSPORT].[FEE_GETFEESFORCOLLECTION]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function Get_Events_and_Activities(ByVal BSU_ID As String, ByVal COL_ID As Integer, ByVal ACT_ID As String, ByVal DT As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@COL_ID", SqlDbType.Int)
        pParms(1).Value = COL_ID
        pParms(2) = New SqlClient.SqlParameter("@ACT_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = ACT_ID
        pParms(3) = New SqlClient.SqlParameter("@DT", SqlDbType.VarChar, 20)
        pParms(3).Value = DT
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_EVENT_DIMENSIONS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function Get_CashFlowList(ByVal BSU_ID As String, ByVal COL_ID As Integer, ByVal ACT_ID As String, ByVal DT As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@COL_ID", SqlDbType.Int)
        pParms(1).Value = COL_ID
        pParms(2) = New SqlClient.SqlParameter("@ACT_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = ACT_ID
        pParms(3) = New SqlClient.SqlParameter("@DT", SqlDbType.VarChar, 20)
        pParms(3).Value = DT
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_CASHFLOW_DIMENSIONS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function GET_FEE_COLLECTION_SCHEDULE_ID(ByVal BSU_ID As String, ByVal ACD_ID As Integer, ByVal FEE_ID As Integer) As Integer
        GET_FEE_COLLECTION_SCHEDULE_ID = 0
        Try
            Dim qry As String = "SELECT ISNULL(MAX(FSP_Collection_SCH_ID),-1) AS SCH_ID FROM FEES.FEESETUP_S WITH(NOLOCK) WHERE FSP_BSU_ID='" & BSU_ID & "' AND FSP_ACD_ID=" & ACD_ID & " AND FSP_FEE_ID=" & FEE_ID & ""
            GET_FEE_COLLECTION_SCHEDULE_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function bFeesTaxable(ByVal BSU_ID As String) As Boolean
        bFeesTaxable = False
        Try
            Dim QRY = "SELECT ISNULL(BUS_bFEE_TAXABLE,0) AS bFEE_TAXABLE FROM dbo.BUSINESSUNIT_SUB WITH(NOLOCK) WHERE BUS_BSU_ID='" & BSU_ID & "'"
            bFeesTaxable = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, QRY)
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function bFeesMulticurrency(ByVal BSU_ID As String) As Boolean
        Try
            Dim QRY = "SELECT ISNULL(BSU_bFeesMulticurrency,0) FROM BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & BSU_ID & "'"
            bFeesMulticurrency = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, QRY)
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Shared Function GetGradesinAcademicYear(ByVal ACD_ID As Integer, ByVal BSU_ID As String) As DataTable
        Try
            Dim conn_str As String = ConnectionManger.GetOASISConnectionString
            Dim sql_query As New StringBuilder
            sql_query.Append("SELECT GRM_DISPLAY, GRM_GRD_ID FROM GRADE_BSU_M WITH(NOLOCK) ")
            sql_query.Append("INNER JOIN GRADE_M WITH(NOLOCK) ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID WHERE GRM_BSU_ID ='" & BSU_ID & "' ")
            sql_query.Append("AND GRM_ACD_ID = " & ACD_ID & "")
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query.ToString)
            If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
                Dim dr As DataRow = dsData.Tables(0).NewRow
                dr(0) = "ALL"
                dr(1) = "-1"
                dsData.Tables(0).Rows.Add(dr)
            End If
            Return dsData.Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class
