Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class FeeTransportDayendProcess

    Public Shared Function GetDayEndHistory(ByVal p_ACD_ID As String, ByVal p_BSU_ID As String, _
    ByVal p_DATE As Date, ByVal p_IsTransport As Boolean) As DataTable
        ' FEES.GetDayEndHistory 
        '@CURDATE DATETIME,
        '@BSU_ID VARCHAR(20),
        '@ACD_ID BIGINT  
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        If p_IsTransport Then
            str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        End If
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(0).Value = p_ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@CURDATE", SqlDbType.DateTime)
        pParms(1).Value = p_DATE
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_BSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(str_conn, _
          CommandType.StoredProcedure, "FEES.GetDayEndHistory", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_POSTFEECHARGE(ByVal p_BSU_ID As String, _
            ByVal p_Dt As String, ByVal p_USER As String, ByVal p_ACD_ID As String, ByRef NEW_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_POSTFEECHARGE]
        '@BSU_ID = N'125016',
        '@ACD_ID = 80XX,
        '@Dt = N'12-SEP-2008',
        '@USER = N'GURU'
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
        pParms(2).Value = p_USER
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
        pParms(5).Direction = ParameterDirection.Output

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTFEECHARGE", pParms)
        NEW_DOCNO = pParms(5).Value
        F_POSTFEECHARGE = pParms(3).Value
    End Function

    Public Shared Function F_POSTFEESADJUSTMENT_MONTHLY(ByVal p_BSU_ID As String, _
                ByVal p_Dt As String, ByRef p_NewDOCNo As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(4) As SqlClient.SqlParameter
        'EXEC  @return_value = [FEES].[F_POSTFEESADJUSTMENT_MONTHLY]
        '@BSU_ID = N'125016',
        '@Dt = '31/aug/2008',
        '@NewDOCNo = @NewDOCNo OUTPUT
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@NewDOCNo", SqlDbType.VarChar, 20)
        pParms(2).Direction = ParameterDirection.Output

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTFEESADJUSTMENT_MONTHLY", pParms)
        If pParms(3).Value = 0 Then
            p_NewDOCNo = pParms(2).Value
        Else
            p_NewDOCNo = ""
        End If
        F_POSTFEESADJUSTMENT_MONTHLY = pParms(3).Value
    End Function

    Public Shared Function F_POSTFEE_REVENUE(ByVal p_BSU_ID As String, _
            ByVal p_Dt As String, ByVal p_ACD_ID As String, ByRef DOC_NO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_POSTFEE_REVENUE]
        '		@BSU_ID = N'125016',
        '		@Dt = N'12-SEP-2008',
        '		@ACD_ID = XXCD
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = IIf(IsNumeric(p_ACD_ID), p_ACD_ID, 0)
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Direction = ParameterDirection.Output
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTFEE_REVENUE", pParms)
        F_POSTFEE_REVENUE = pParms(3).Value
        If Not pParms(4).Value Is DBNull.Value Then
            DOC_NO = pParms(4).Value
        End If
    End Function

    Public Shared Function F_DOMONTHEND(ByVal p_BSU_ID As String, _
        ByVal p_Dt As String, ByVal p_ACD_ID As String, ByVal p_What As String, _
        ByVal p_User As String, ByVal p_bHardClose As Boolean, _
        ByVal p_conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        '      EXEC	@return_value = [FEES].[F_DOMONTHEND]
        '@BSUID = N'125016',
        '@ACD_ID = 80,
        '@DT = N'12-jan-2008',
        '@What = 12,
        '@User = N'guru'
        Dim cmd As New SqlCommand
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = IIf(IsNumeric(p_ACD_ID), p_ACD_ID, 0)
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@User", SqlDbType.VarChar, 50)
        pParms(4).Value = p_User
        pParms(5) = New SqlClient.SqlParameter("@What", SqlDbType.BigInt)
        pParms(5).Value = p_What

        pParms(6) = New SqlClient.SqlParameter("@bHardClose", SqlDbType.Bit)
        pParms(6).Value = p_bHardClose

        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))
        cmd.Parameters.Add(pParms(6))

        cmd.Connection = p_conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FEES.F_DOMONTHEND"
        cmd.CommandTimeout = 0
        Dim retval As Integer
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_DOMONTHEND", pParms)
        retval = cmd.ExecuteNonQuery()
        F_DOMONTHEND = pParms(3).Value
    End Function

    Public Shared Function F_DOMONTHEND_STUDENTWISE(ByVal p_BSU_ID As String, _
    ByVal p_Dt As String, ByVal p_ACD_ID As String, ByVal p_STU_IDS As String, _
    ByVal p_Conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        ' ALTER procedure [FEES].[F_DOMONTHEND_STUDENTWISE]
        '@BSU_ID VARCHAR(20)='125016',
        '@STU_IDS XML= '<STU_DETAILS><STU_DETAIL><STU_ID>61034</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60889</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60896</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60898</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60902</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61320</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61413</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61501</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61527</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61538</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61544</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61295</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61304</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61308</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60880</STU_ID></STU_DETAIL></STU_DETAILS>',
        '@ACD_ID BIGINT=80, 
        '@Dt DATETIME ='1-JUN-2008' 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = IIf(IsNumeric(p_ACD_ID), p_ACD_ID, 0)
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@STU_IDS", SqlDbType.Xml)
        pParms(4).Value = p_STU_IDS

        Dim cmd As New SqlCommand()
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))

        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.CommandText = "FEES.F_DOMONTHEND_STUDENTWISE"
        cmd.CommandTimeout = 0
        cmd.Connection = p_Conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        Dim retval As Integer
        ' retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_DOMONTHEND_STUDENTWISE", pParms)
        retval = cmd.ExecuteNonQuery
        F_DOMONTHEND_STUDENTWISE = pParms(3).Value
    End Function

    Public Shared Function F_POSTCONCESSION(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_FCH_ID As String, _
    ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '   ALTER procedure [FEES].[F_POSTCONCESSION] 
        '(@BSU_ID varchar(20),
        '@Dt datetime,
        '@FCH_ID BIGINT 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@FCH_ID", SqlDbType.BigInt)
        pParms(2).Value = p_FCH_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTCONCESSION", pParms)

        F_POSTCONCESSION = pParms(3).Value
    End Function

    Public Shared Function F_POSTCONCESSION_Transoport(ByVal p_BSU_ID As String, ByVal p_Dt As String, _
    ByVal p_FCH_ID As String, ByVal p_STU_BSU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '   ALTER procedure [FEES].[F_POSTCONCESSION] 
        '(@BSU_ID varchar(20),
        '@Dt datetime,
        '@FCH_ID BIGINT 

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@FCH_ID", SqlDbType.BigInt)
        pParms(2).Value = p_FCH_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_STU_BSU_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTCONCESSION", pParms)

        F_POSTCONCESSION_Transoport = pParms(3).Value
    End Function

    Public Shared Function F_GenerateCollectionVoucher_Transport(ByVal p_BSU_ID As String, _
    ByVal p_Dt As String, ByVal p_USER As String, ByVal p_ACD_ID As String, _
    ByVal p_PDCIncludeDays As String, ByVal p_STU_BSU_ID As String, _
    ByVal p_CASHREFNO As String, ByVal p_BANKREFNO As String, ByVal p_conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(8) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_GenerateCollectionVoucher]
        '@BSU_ID = N'125016',
        '@Dt = N'12-may-2008',
        '@USER = N'guru' @ACD_ID BIGINT  
        '@PDCIncludeDays 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
        pParms(2).Value = p_USER
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@PDCIncludeDays", SqlDbType.Int)
        pParms(5).Value = p_PDCIncludeDays
        pParms(6) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(6).Value = p_STU_BSU_ID
        pParms(7) = New SqlClient.SqlParameter("@CASHREFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_CASHREFNO
        pParms(8) = New SqlClient.SqlParameter("@BANKREFNO", SqlDbType.VarChar, 20)
        pParms(8).Value = p_BANKREFNO
        '@CASHREFNO VARCHAR(20),@BANKREFNO VARCHAR(20),
        Dim cmd As New SqlCommand
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))
        cmd.Parameters.Add(pParms(6))
        cmd.Parameters.Add(pParms(7))
        cmd.Parameters.Add(pParms(8))
        cmd.Connection = p_conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FEES.F_GenerateCollectionVoucher"
        cmd.CommandTimeout = 0
        Dim retval As Integer
        retval = cmd.ExecuteNonQuery
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_GenerateCollectionVoucher", pParms)
        F_GenerateCollectionVoucher_Transport = pParms(3).Value
    End Function

End Class
