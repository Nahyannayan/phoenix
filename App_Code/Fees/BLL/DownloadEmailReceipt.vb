Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Public Class DownloadEmailReceipt
    Shared reportHeader As String
    Public Shared Function DownloadOrEMailReceipt(ByVal Action As String, ByVal p_Receiptno As String, ByVal BSUID As String, ByVal ForceEmail As Boolean) As String
        DownloadOrEMailReceipt = ""
        Try

            Dim str_Sql, strFilter As String
            strFilter = " FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & BSUID & "' "
            str_Sql = " SELECT * FROM [FEES].[VW_OSO_FEES_EMAIL_FEERECEIPT] WHERE " & strFilter
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim ds As New DataSet
            Dim repSource As New MyReportClass
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            repSource.ReportUniqueName = BSUID & "_" & p_Receiptno
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)

                Dim params As New Hashtable
                params("UserName") = "SYSTEM"
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = False
                '''''''''''''''
                Dim repSourceSubRep(2) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                repSourceSubRep(1) = New MyReportClass
                repSourceSubRep(2) = New MyReportClass

                Dim cmdHeader As New SqlCommand("getBsuInFoWithImage", New SqlConnection(ConnectionManger.GetOASISConnectionString))
                cmdHeader.CommandType = CommandType.StoredProcedure
                cmdHeader.Parameters.AddWithValue("@IMG_BSU_ID", BSUID)
                cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
                repSourceSubRep(1).Command = cmdHeader

                Dim cmdSubEarn As New SqlCommand
                cmdSubEarn.CommandText = "EXEC FEES.GetReceiptPrint_Online @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & BSUID & "'"
                cmdSubEarn.Connection = New SqlConnection(str_conn)
                cmdSubEarn.CommandType = CommandType.Text
                repSourceSubRep(0).Command = cmdSubEarn

                Dim cmdSubPayment As New SqlCommand
                cmdSubPayment.CommandText = "FEES.GetReceiptPrint_Online"
                cmdSubPayment.CommandType = CommandType.StoredProcedure
                cmdSubPayment.Parameters.AddWithValue("@FCL_BSU_ID", BSUID)
                cmdSubPayment.Parameters.AddWithValue("@FCL_RECNO", p_Receiptno)
                cmdSubPayment.Parameters.AddWithValue("@bPATMENT_DETAIL", True)
                cmdSubPayment.Connection = New SqlConnection(str_conn)
                repSourceSubRep(2).Command = cmdSubPayment
                repSource.SubReport = repSourceSubRep

                'repSource.ResourceName = "~/Reports/RPT/rptFeeReceipt.rpt"
                Dim bBSUTaxable As Boolean, RPTName As String

                bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & BSUID & "'"), Boolean)
                If bBSUTaxable Then
                    repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT_EMAIL/rptFeeReceipt_Tax.rpt")
                    'RPTName = "rptFeeReceipt_Tax.rpt"
                Else
                    'RPTName = "rptFeeReceipt.rpt"
                    repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT_EMAIL/rptFeeReceipt.rpt")
                End If
                'Dim RootBackArr As String(), AppPath As String, i As Int16, RPTPath As String
                'AppPath = HttpContext.Current.Request.Url.AbsolutePath
                'RootBackArr = AppPath.ToString.Split("/")C:\Applications\VS2013\OASIS_VSS\Fees\Reports\RPT_EMAIL\rptFeeReceipt.rpt
                'RPTPath = ""
                'For i = 1 To RootBackArr.Length - 3
                '    RPTPath = RPTPath & "../"
                'Next

                'repSource.ResourceName = RPTPath & "Fees/Reports/RPT/" & RPTName
            End If
            If Not repSource Is Nothing Then
                Dim repClassVal As New RepClass

                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName
                repClassVal.SetDataSource(ds.Tables(0))
                If repSource.IncludeBSUImage Then
                    If repSource.HeaderBSUID Is Nothing Then
                        IncludeBSUmage(repClassVal, BSUID)
                    Else
                        IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                    End If

                End If

                SubReport(repSource, repSource, repClassVal)

                Dim pdfFilePath As String
                Dim pdfFileName As String
                If Action.ToUpper = "DOWNLOAD" Or Action.ToUpper = "" Then
                    pdfFileName = "OASIS_Fee Receipt-" & repSource.ReportUniqueName & ".pdf"
                Else
                    pdfFileName = "OASIS_Fee Receipt-" & repSource.ReportUniqueName & ".pdf"
                End If
                pdfFilePath = WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
                pdfFilePath += pdfFileName

                repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                End While
                Try
                    If System.IO.File.Exists(pdfFilePath) Then
                        System.IO.File.Delete(pdfFilePath)
                    End If
                Catch ex As Exception

                End Try

                repClassVal.ExportToDisk(ExportFormatType.PortableDocFormat, pdfFilePath)
                If Action.ToUpper = "DOWNLOAD" Or Action.ToUpper = "" Then
                    Dim BytesData As Byte()
                    BytesData = ConvertFiletoBytes(pdfFilePath)
                    Dim ContentType, Extension As String
                    ContentType = "application/pdf"
                    Extension = GetFileExtension(ContentType)
                    Dim Title As String = "Receipt"
                    If repSource.ReportUniqueName.ToString.Split("_").Length > 1 Then
                        Title = repSource.ReportUniqueName.ToString.Split("_")(1)
                    End If
                    DownloadFile(System.Web.HttpContext.Current, BytesData, ContentType, Extension, Title)
                    repClassVal.Close()
                    repClassVal.Dispose()
                    repSource = Nothing
                    repClassVal = Nothing
                ElseIf Action.ToUpper = "EMAIL" Then
                    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
                      "EXEC  [ONLINE].[GetFeeReceiptEmailDetails_copy] '" & BSUID & "','" & p_Receiptno & "'")
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Dim subject As String = "Fee Payment Receipt"
                        Dim ContactName, BSU_Name, StudName, EMailID As String, Emailed As Boolean
                        ContactName = ds.Tables(0).Rows(0).Item("TO_EMAIL_NAME").ToString
                        BSU_Name = ds.Tables(0).Rows(0).Item("BSU_Name").ToString
                        StudName = ds.Tables(0).Rows(0).Item("STU_NAME").ToString
                        EMailID = ds.Tables(0).Rows(0).Item("TO_EMAIL_ID").ToString
                        Emailed = ds.Tables(0).Rows(0).Item("Emailed")
                        If ForceEmail Or Not Emailed Then
                            Dim sb As New StringBuilder
                            sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                            sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & ContactName & ", </td></tr>")
                            sb.Append("<tr><td ><br />GREETINGS from the " & BSU_Name & "<br /></td></tr>")
                            sb.Append("<tr><td ><br />Please find the attached fee receipt for your child " & StudName & ".<br /></td></tr>")
                            sb.Append("<tr><td ><br />Thanks & regards </td></tr>")
                            sb.Append("<tr><td >Accounts </td></tr>")
                            sb.Append("<tr><td >" & BSU_Name & "<br /></td></tr>")
                            sb.Append("<tr><td></td></tr>")
                            sb.Append("<tr></tr><tr></tr>")
                            sb.Append("</table>")
                            Dim EmailStatus As String
                            EmailStatus = eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, EMailID, _
                                         subject, sb.ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, ds.Tables(0).Rows(0)("SYS_PASSWORD_FEEONLINE").ToString, _
                                                ds.Tables(0).Rows(0)("SYS_EMAIL_HOST").ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_PORT").ToString, pdfFilePath)
                            If EmailStatus.ToString.ToUpper.Contains("SUCCESS") Then
                                DownloadOrEMailReceipt = "Receipt successfully emailed to " & EMailID
                                Dim sqlStr As String
                                sqlStr = "UPDATE FEES.FEECOLLECTION_H SET FCL_bEMAILED=1 WHERE FCL_BSU_ID='" & BSUID & "' and FCL_RECNO='" & p_Receiptno & "'"
                                SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, sqlStr)
                            Else
                                DownloadOrEMailReceipt = "Email Sending Failed"
                            End If
                            repClassVal.Close()
                            repClassVal.Dispose()
                            repSource = Nothing
                            repClassVal = Nothing
                        End If
                        Try
                            If System.IO.File.Exists(pdfFilePath) Then
                                System.IO.File.Delete(pdfFilePath)
                            End If
                        Catch ex As Exception

                        End Try
                    End If

                End If
            End If
        Catch ex As Exception
            DownloadOrEMailReceipt = ex.Message
        End Try
    End Function
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & Title & Extension)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.End()
    End Sub
    Private Shared Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Shared Sub SubReport(ByVal repSource As MyReportClass, ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(repSource, subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            reportHeader = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Shared Sub IncludeBSUmage(ByRef repClassVal As RepClass, ByVal BSUID As String)
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = BSUID
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub
End Class
