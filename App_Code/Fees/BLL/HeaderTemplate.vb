﻿Imports Microsoft.VisualBasic

Public Class HeaderTemplate
    Implements ITemplate

    Private header As String
    Public Sub New(ByVal header As String)
        Me.header = header
    End Sub
    Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
        Dim lable As New Label()
        lable.ID = "LblHeader"
        lable.Text = header
        container.Controls.Add(lable)
    End Sub

End Class
