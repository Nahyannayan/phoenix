﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Net

Public Class ClsPayment
    Public Property FCO_FCO_ID() As Long
    Public Property CPS_ID() As Integer
    Public Property ORDER_INFO() As String
    Public Property TOTAL_AMOUNT() As Double
    Public Property RESULT_PAGE() As String
    Public Property STU_BSU_ID() As String
    Public Property OLU_ID() As Integer
    Public Property PROVIDER_BSU_ID() As String
    Public Property MERCHANT_ID() As String
    Public Property CURRENCY() As String
    Public Property MERCHANT_NAME() As String
    Public Property GATEWAY_TYPE() As String
    Public Property API_PASSWORD() As String
    Public Property API_URL() As String

    Public Property MPGS_SESSION_ID() As String
    Public Property MPGS_SESSION_VERSION() As String
    Public Property MPGS_SESSION_UPDATE_STATUS() As String
    Public Property MPGS_SUCCESS_INDICATOR() As String
    Public Property MERCHANT_TXN_REF_ID() As String
    Public Property COPYRIGHT_OWNER() As String
    Public Property GEMS_PRIVATE_KEY() As String
    Public Property STU_NOS() As String
    Public Property PARENT_EMAIL() As String
    Public Property PARENT_MOBILE() As String
    Public Property PARENT_NAME() As String
    Public Property SCHOOL_CITY() As String
    Public Property PAY_MODE() As String
    Public Property WALLET_TOKEN() As String
    Public Property DEVICE_MODEL() As String

    Public Property BSU_MERCHANTCODE() As String
    Public Property BSU_QUERYDRUSER() As String
    Public Property BSU_QUERYDRPASSWORD() As String
    Public Property QUERYDR_REFUND_URL() As String
    Public Property QUERYDR_QUERY_URL() As String
    Public Property MERCHANT_CODE() As String

    Public Sub GET_PAYMENT_REDIRECT(ByVal pSRC As String, ByVal pFCO_FCO_ID As Long, ByVal pBSU_ID As String)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = pFCO_FCO_ID

        pParms(1) = New SqlClient.SqlParameter("@SRC", SqlDbType.VarChar, 20)
        pParms(1).Value = pSRC
        pParms(2) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = pBSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
          CommandType.StoredProcedure, "API.GET_PAYMENT_PROVIDER_PARAMETERS", pParms)

        If Not dsData Is Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
            FCO_FCO_ID = pFCO_FCO_ID
            CPS_ID = dsData.Tables(0).Rows(0)("CPS_ID")
            ORDER_INFO = dsData.Tables(0).Rows(0)("ORDER_INFO").ToString
            TOTAL_AMOUNT = dsData.Tables(0).Rows(0)("TOTAL_AMOUNT")
            RESULT_PAGE = dsData.Tables(0).Rows(0)("RESULT_PAGE").ToString
            STU_BSU_ID = dsData.Tables(0).Rows(0)("STU_BSU_ID").ToString
            OLU_ID = dsData.Tables(0).Rows(0)("OLU_ID")
            PROVIDER_BSU_ID = dsData.Tables(0).Rows(0)("PROVIDER_BSU_ID").ToString
            MERCHANT_ID = dsData.Tables(0).Rows(0)("MERCHANT_ID").ToString
            CURRENCY = dsData.Tables(0).Rows(0)("CURRENCY").ToString
            MERCHANT_NAME = dsData.Tables(0).Rows(0)("MERCHANT_NAME").ToString
            GATEWAY_TYPE = dsData.Tables(0).Rows(0)("GATEWAY_TYPE").ToString
            API_PASSWORD = dsData.Tables(0).Rows(0)("API_PASSWORD").ToString
            API_URL = dsData.Tables(0).Rows(0)("API_URL").ToString
            MERCHANT_TXN_REF_ID = dsData.Tables(0).Rows(0)("MERCHANT_TXN_REF_ID").ToString
            COPYRIGHT_OWNER = dsData.Tables(0).Rows(0)("COPYRIGHT_OWNER").ToString
            GEMS_PRIVATE_KEY = dsData.Tables(0).Rows(0)("GEMS_PRIVATE_KEY").ToString
            STU_NOS = dsData.Tables(0).Rows(0)("STU_NOS").ToString
            PARENT_EMAIL = dsData.Tables(0).Rows(0)("PARENT_EMAIL").ToString
            PARENT_MOBILE = dsData.Tables(0).Rows(0)("PARENT_MOBILE").ToString
            PARENT_NAME = dsData.Tables(0).Rows(0)("PARENT_NAME").ToString
            SCHOOL_CITY = dsData.Tables(0).Rows(0)("SCHOOL_CITY").ToString
            PAY_MODE = dsData.Tables(0).Rows(0)("PAY_MODE").ToString
            QUERYDR_QUERY_URL = dsData.Tables(0).Rows(0)("QUERYDR_QUERY_URL").ToString
            BSU_QUERYDRUSER = dsData.Tables(0).Rows(0)("CPS_QUERYDRUSER").ToString
            BSU_QUERYDRPASSWORD = dsData.Tables(0).Rows(0)("CPS_QUERYDRPASSWORD").ToString
            MERCHANT_CODE = dsData.Tables(0).Rows(0)("MERCHANT_CODE").ToString
        Else
            FCO_FCO_ID = pFCO_FCO_ID
            CPS_ID = 0 : ORDER_INFO = "" : TOTAL_AMOUNT = 0 : RESULT_PAGE = "" : STU_BSU_ID = "" : OLU_ID = 0
            PROVIDER_BSU_ID = "" : MERCHANT_ID = "" : CURRENCY = "" : MERCHANT_NAME = "" : GATEWAY_TYPE = ""
            API_PASSWORD = "" : API_URL = "" : MERCHANT_TXN_REF_ID = "" : COPYRIGHT_OWNER = "" : GEMS_PRIVATE_KEY = ""
            STU_NOS = "" : PARENT_EMAIL = "" : PARENT_MOBILE = "" : PARENT_NAME = "" : SCHOOL_CITY = "" : PAY_MODE = ""
            QUERYDR_QUERY_URL = "" : MERCHANT_CODE = "" : BSU_QUERYDRUSER = "" : BSU_QUERYDRPASSWORD = ""
        End If
    End Sub

    Public Sub GET_MIGS_QUERY_DR_INFO(ByVal pFCO_FCO_ID As Long, ByVal pBSU_ID As String)
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = pFCO_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = pBSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString,
          CommandType.StoredProcedure, "FEES.GET_MIGS_QUERY_DR_INFO", pParms)

        If Not dsData Is Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
            FCO_FCO_ID = pFCO_FCO_ID
            BSU_MERCHANTCODE = dsData.Tables(0).Rows(0)("BSU_MERCHANTCODE").ToString
            BSU_QUERYDRUSER = dsData.Tables(0).Rows(0)("BSU_QUERYDRUSER").ToString
            BSU_QUERYDRPASSWORD = dsData.Tables(0).Rows(0)("BSU_QUERYDRPASSWORD").ToString
            MERCHANT_ID = dsData.Tables(0).Rows(0)("BSU_MERCHANTID").ToString
        Else
            FCO_FCO_ID = pFCO_FCO_ID
            BSU_MERCHANTCODE = "" : BSU_QUERYDRPASSWORD = "" : BSU_QUERYDRUSER = ""
        End If
    End Sub

    Public Shared Function UPDATE_MPGS_FETCH_ORDER_API_RESPONSE_FOR_FEEREFUND(ByVal pSOURCE As String, ByVal pFCO_FCO_ID As Long,
    ByVal pFCOD_RESPONSE As String, ByVal p_stTrans As SqlTransaction) As Integer
        Dim ReturnFlag As Integer = 1000
        Try
            Dim pParms(4) As SqlParameter

            pParms(0) = New SqlParameter("@SOURCE", SqlDbType.VarChar, 20)
            pParms(0).Value = pSOURCE
            pParms(1) = New SqlParameter("@FCO_FCO_ID", SqlDbType.BigInt)
            pParms(1).Value = pFCO_FCO_ID
            pParms(2) = New SqlParameter("@API_RESPONSE", SqlDbType.NVarChar)
            pParms(2).Value = pFCOD_RESPONSE

            pParms(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbOasis & ".FEES.UPDATE_MPGS_FETCH_ORDER_API_DATA", pParms)

            ReturnFlag = pParms(3).Value
        Catch ex As Exception

        End Try
        Return ReturnFlag
    End Function

    Public Sub GET_REFUND_API_INFO(ByVal pSRC As String, ByVal pFRH_ID As Long, ByVal pBSU_ID As String)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.BigInt)
        pParms(0).Value = pFRH_ID

        pParms(1) = New SqlClient.SqlParameter("@SRC", SqlDbType.VarChar, 20)
        pParms(1).Value = pSRC
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = pBSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
          CommandType.StoredProcedure, "API.GET_REFUND_API_INFO", pParms)

        If Not dsData Is Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
            FCO_FCO_ID = pFRH_ID
            CPS_ID = dsData.Tables(0).Rows(0)("CPS_ID")
            ORDER_INFO = dsData.Tables(0).Rows(0)("ORDER_INFO").ToString
            TOTAL_AMOUNT = dsData.Tables(0).Rows(0)("TOTAL_AMOUNT")
            RESULT_PAGE = dsData.Tables(0).Rows(0)("RESULT_PAGE").ToString
            STU_BSU_ID = dsData.Tables(0).Rows(0)("STU_BSU_ID").ToString
            OLU_ID = dsData.Tables(0).Rows(0)("OLU_ID")
            PROVIDER_BSU_ID = dsData.Tables(0).Rows(0)("PROVIDER_BSU_ID").ToString
            MERCHANT_ID = dsData.Tables(0).Rows(0)("MERCHANT_ID").ToString
            CURRENCY = dsData.Tables(0).Rows(0)("CURRENCY").ToString
            MERCHANT_NAME = dsData.Tables(0).Rows(0)("MERCHANT_NAME").ToString
            GATEWAY_TYPE = dsData.Tables(0).Rows(0)("GATEWAY_TYPE").ToString
            API_PASSWORD = dsData.Tables(0).Rows(0)("API_PASSWORD").ToString
            API_URL = dsData.Tables(0).Rows(0)("API_URL").ToString
            MERCHANT_TXN_REF_ID = dsData.Tables(0).Rows(0)("MERCHANT_TXN_REF_ID").ToString
            COPYRIGHT_OWNER = dsData.Tables(0).Rows(0)("COPYRIGHT_OWNER").ToString
            GEMS_PRIVATE_KEY = dsData.Tables(0).Rows(0)("GEMS_PRIVATE_KEY").ToString
            STU_NOS = dsData.Tables(0).Rows(0)("STU_NOS").ToString
            PARENT_EMAIL = dsData.Tables(0).Rows(0)("PARENT_EMAIL").ToString
            PARENT_MOBILE = dsData.Tables(0).Rows(0)("PARENT_MOBILE").ToString
            PARENT_NAME = dsData.Tables(0).Rows(0)("PARENT_NAME").ToString
            SCHOOL_CITY = dsData.Tables(0).Rows(0)("SCHOOL_CITY").ToString
            PAY_MODE = dsData.Tables(0).Rows(0)("PAY_MODE").ToString
            QUERYDR_REFUND_URL = dsData.Tables(0).Rows(0)("QUERYDR_REFUND_URL").ToString
        Else
            FCO_FCO_ID = pFRH_ID
            CPS_ID = 0 : ORDER_INFO = "" : TOTAL_AMOUNT = 0 : RESULT_PAGE = "" : STU_BSU_ID = "" : OLU_ID = 0
            PROVIDER_BSU_ID = "" : MERCHANT_ID = "" : CURRENCY = "" : MERCHANT_NAME = "" : GATEWAY_TYPE = ""
            API_PASSWORD = "" : API_URL = "" : MERCHANT_TXN_REF_ID = "" : COPYRIGHT_OWNER = "" : GEMS_PRIVATE_KEY = ""
            STU_NOS = "" : PARENT_EMAIL = "" : PARENT_MOBILE = "" : PARENT_NAME = "" : SCHOOL_CITY = "" : PAY_MODE = "" : QUERYDR_REFUND_URL = ""
        End If
    End Sub


    Public Shared Function UPDATE_FEE_REFUND_API_RESPONSE_DATA(ByVal pSOURCE As String, ByVal pFRH_ID As Long,
       ByVal pFCOD_RESPONSE As String, ByVal p_stTrans As SqlTransaction) As Integer
        Dim ReturnFlag As Integer = 1000
        Try
            Dim pParms(4) As SqlParameter

            pParms(0) = New SqlParameter("@SOURCE", SqlDbType.VarChar, 20)
            pParms(0).Value = pSOURCE
            pParms(1) = New SqlParameter("@FRH_ID", SqlDbType.BigInt)
            pParms(1).Value = pFRH_ID
            pParms(2) = New SqlParameter("@API_RESPONSE", SqlDbType.NVarChar)
            pParms(2).Value = pFCOD_RESPONSE

            pParms(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFees & ".FEES.UPDATE_FEE_REFUND_API_RESPONSE_DATA", pParms)

            ReturnFlag = pParms(3).Value
        Catch ex As Exception

        End Try
        Return ReturnFlag
    End Function


End Class
Public Class clsInvoice
    Public Property inv_no As String
    Public Property request_id As String
End Class
