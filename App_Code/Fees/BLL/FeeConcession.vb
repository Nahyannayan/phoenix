Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
''' The class Created for handling the Concession Type
''' 
''' Author : SHIJIN C A
''' Date : 03-MAY-2008 
''' </summary>
''' <remarks></remarks>
Public Class FeeConcession

    ''' <summary>
    ''' Variables used to Store the 
    ''' </summary>
    ''' <remarks></remarks>
    Dim vFCM_ID As Integer
    Dim vFCM_FCT_ID As Integer
    Dim vFCT_DESCR As String
    Dim vFCM_DESCR As String
    Dim vFCM_DEBIT_ACT_ID As String
    Dim vFCM_BSU_ID As String
    Dim vFCM_BSU_DESCR As String
    Dim vFCM_DEBIT_ACT_DESCR As String
    Dim bEdited As Boolean
    Dim bDeleted As Boolean

    ''' <summary>
    ''' Gets or Sets the FCM_ID for the current Object
    ''' This is the primary Key of the object and the 
    ''' Unique Identifier for the object created
    ''' </summary>
    ''' <value>Integer</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FCM_ID() As Integer
        Get
            Return vFCM_ID
        End Get
        Set(ByVal value As Integer)
            vFCM_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the FCM_FCT_ID for the current Object
    ''' It Sets the Concession Type from FEE_CONCESSIONTYPE_M Table
    ''' </summary>
    ''' <value>Integer</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FCT_ID() As Integer
        Get
            Return vFCM_FCT_ID
        End Get
        Set(ByVal value As Integer)
            vFCM_FCT_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the FCT_DESCR for the current Object
    ''' It Sets the Concession Type Description from FEE_CONCESSIONTYPE_M Table
    ''' </summary>
    ''' <value>String</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FCT_DESCR() As String
        Get
            Return vFCT_DESCR
        End Get
        Set(ByVal value As String)
            vFCT_DESCR = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the FCM_DESCR for the current Object
    ''' It is the Description of the concession that is 
    ''' created
    ''' </summary>
    ''' <value>String</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FCM_DESCR() As String
        Get
            Return vFCM_DESCR
        End Get
        Set(ByVal value As String)
            vFCM_DESCR = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Debit Account ID for the current Object
    ''' This is the account where the cocession to be debited
    ''' 
    ''' </summary>
    ''' <value>String</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FCM_DEBIT_ACT_ID() As String
        Get
            Return vFCM_DEBIT_ACT_ID
        End Get
        Set(ByVal value As String)
            vFCM_DEBIT_ACT_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Debit Account NAME for the current Object
    ''' This is the account where the cocession to be debited
    ''' 
    ''' </summary>
    ''' <value>String</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FCM_DEBIT_ACT_NAME() As String
        Get
            Return vFCM_DEBIT_ACT_DESCR
        End Get
        Set(ByVal value As String)
            vFCM_DEBIT_ACT_DESCR = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets Business Unit to link( only for staff concession)
    ''' So that each business unit will have a concession head and 
    ''' can be used for interunit voucher generation.
    ''' 
    ''' </summary>
    ''' <value>String</value>
    ''' <returns>Business Unit ID</returns>
    ''' <remarks></remarks>
    Public Property FCM_BSU_ID() As String
        Get
            Return vFCM_BSU_ID
        End Get
        Set(ByVal value As String)
            vFCM_BSU_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets Business Unit to link( only for staff concession)
    ''' So that each business unit will have a concession head and 
    ''' can be used for interunit voucher generation.
    ''' 
    ''' </summary>
    ''' <value>String</value>
    ''' <returns>Business Unit ID</returns>
    ''' <remarks></remarks>

    Public Property FCM_BSU_NAME() As String
        Get
            Return vFCM_BSU_DESCR
        End Get
        Set(ByVal value As String)
            vFCM_BSU_DESCR = value
        End Set
    End Property

    ''' <summary>
    ''' Function used to Populate the Concession Type Details to the 
    ''' Screen.It Returns a data table containing all Fee type 
    ''' from the Concession Type master Table FEE_CONCESSIONTYPE_M
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function PopulateConcessionType() As DataTable
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim sql_query As String = "select FCT_ID, FCT_DESCR from FEES.FEE_CONCESSIONTYPE_M ORDER BY FCT_DESCR "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Returns the Fee Concession detials for the passed FCM_ID
    ''' 
    ''' </summary>
    ''' <param name="FCM_ID"> The unique ID to get the Deails</param>
    ''' <returns> The FeeConcession object after filling all the data</returns>
    ''' <remarks></remarks>
    Public Shared Function GetFeeConcession(ByVal FCM_ID As Integer, ByVal conn As SqlConnection) As FeeConcession
        Dim sql_str As String
        sql_str = "SELECT FCM_ID, FCM_FCT_ID, FCM_DESCR, FCM_DEBIT_ACT_ID, " & _
        "FCM_BSU_ID, FCT_DESCR, BSU_NAME, ACT_NAME FROM vw_OSO_FEES_FEECONCESION " & _
        " WHERE FCM_ID =" & FCM_ID
        Dim dreader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_str)
        Dim vFeeConc As New FeeConcession
        While (dreader.Read())
            vFeeConc.FCM_ID = dreader("FCM_ID")
            vFeeConc.FCT_ID = dreader("FCM_FCT_ID")
            vFeeConc.FCM_DESCR = dreader("FCM_DESCR")
            vFeeConc.FCM_DEBIT_ACT_ID = dreader("FCM_DEBIT_ACT_ID")
            vFeeConc.FCM_BSU_ID = dreader("FCM_BSU_ID").ToString
            vFeeConc.FCT_DESCR = dreader("FCT_DESCR")
            vFeeConc.FCM_BSU_NAME = dreader("BSU_NAME").ToString
            vFeeConc.FCM_DEBIT_ACT_NAME = dreader("ACT_NAME").ToString
            vFeeConc.bEdited = True
        End While
        Return vFeeConc
    End Function


    ''' <summary>
    ''' Saves the Fee Concession details in the vSaveFeeConc variable to the 
    ''' database
    ''' </summary>
    ''' <param name="vSaveFeeConc">The object whos data needed to be saved to the database</param>
    ''' <param name="trans">The transaction object for the transaction</param>
    ''' <param name="conn">the SQL Connection object for the Connection </param>
    ''' <returns>Returns the Error No if there is any Error occurs.Returns 0 if the Transaction completed without Errors</returns>
    ''' <remarks></remarks>
    Public Shared Function SaveFeeConcession(ByVal vSaveFeeConc As FeeConcession, ByVal trans As SqlTransaction, ByVal conn As SqlConnection, Optional ByVal AudUser As String = "") As Integer
        If vSaveFeeConc Is Nothing Then
            Return -1
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("FEES.F_SaveFEE_CONCESSION_M", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpFCM_FCT_ID As New SqlParameter("@FCM_FCT_ID", SqlDbType.Int)
        sqlpFCM_FCT_ID.Value = vSaveFeeConc.FCT_ID
        cmd.Parameters.Add(sqlpFCM_FCT_ID)

        Dim sqlpFCM_DESCR As New SqlParameter("@FCM_DESCR", SqlDbType.VarChar, 100)
        sqlpFCM_DESCR.Value = vSaveFeeConc.FCM_DESCR
        cmd.Parameters.Add(sqlpFCM_DESCR)

        Dim sqlpFCM_DEBIT_ACT_ID As New SqlParameter("@FCM_DEBIT_ACT_ID", SqlDbType.VarChar, 20)
        sqlpFCM_DEBIT_ACT_ID.Value = vSaveFeeConc.FCM_DEBIT_ACT_ID
        cmd.Parameters.Add(sqlpFCM_DEBIT_ACT_ID)

        Dim sqlpFCM_BSU_ID As New SqlParameter("@FCM_BSU_ID", SqlDbType.VarChar, 20)
        If vSaveFeeConc.FCM_BSU_ID = "" Or vSaveFeeConc.FCM_BSU_ID = "-1" Then
            sqlpFCM_BSU_ID.Value = DBNull.Value
        Else
            sqlpFCM_BSU_ID.Value = vSaveFeeConc.FCM_BSU_ID
        End If
        cmd.Parameters.Add(sqlpFCM_BSU_ID)

        If vSaveFeeConc.bEdited Then
            Dim sqlpFPB_ID As New SqlParameter("@FCM_ID", SqlDbType.Int)
            sqlpFPB_ID.Value = vSaveFeeConc.FCM_ID
            cmd.Parameters.Add(sqlpFPB_ID)
        End If

        'Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        'sqlpbEdit.Value = vSaveFeeConc.bEdited
        'cmd.Parameters.Add(sqlpbEdit)

        'Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        'sqlpbDelete.Value = vSaveFeeConc.bDeleted
        'cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpAud_user As New SqlParameter("@Aud_user", SqlDbType.VarChar)
        sqlpAud_user.Value = AudUser
        cmd.Parameters.Add(sqlpAud_user)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function
End Class
