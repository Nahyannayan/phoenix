Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class FeeMaster

    Public Shared Function F_SaveFEES_M(ByVal p_FEE_ID As Integer, ByVal p_FEE_DESCR As String, _
    ByVal p_FEE_FTP_ID As Integer, ByVal p_FEE_bConcession As Boolean, _
    ByVal p_FEE_ChargedFor_SCH_ID As Integer, ByVal p_FEE_Collection_SCH_ID As Integer, _
    ByVal p_FEE_bFixed As Boolean, ByVal p_FEE_SVC_ID As Integer, ByVal p_FEE_COL_ID As Integer, _
    ByVal p_FEE_INC_ACT_ID As String, ByVal p_FEE_CHRG_INADV_ACT_ID As String, _
    ByVal p_FEE_CONCESSION_ACT_ID As String, ByVal p_FEE_DISC_ACT_ID As String, _
    ByVal p_FEE_ORDER As Integer, ByVal p_FEE_SETOFFORDER As Integer, _
    ByVal p_FEE_LATEAMTTYPE As Integer, ByVal p_FEE_LATEAMT As Decimal, _
    ByVal p_FEE_LATEDAYS As Integer, ByVal p_FEE_bREVENUE_SCH_ID As Boolean, _
    ByVal p_FEE_bJOINPRORATA As Boolean, ByVal p_FEE_bDiscontinuePRORATA As Boolean, _
    ByVal p_FEE_bSETUPBYGRADE As Boolean, ByVal p_PROC_ID As String,
     ByVal p_FEE_AX_INC_ACT_ID As String, ByVal p_FEE_AX_CHRG_INADV_ACT_ID As String, ByVal p_FEE_FGP_ID As Integer, ByVal p_FEE_EVT_ID As String, ByVal p_FEE_bShowInOtherColl As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(31) As SqlClient.SqlParameter
        '@return_value = [dbo].[F_SaveFEES_M]
        '		@FEE_ID = 0,
        '		@FEE_DESCR = N'test',
        '		@FEE_FTP_ID = 1,
        '		@FEE_bConcession = 0,
        '		@FEE_ChargedFor_SCH_ID = 12,
        pParms(0) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(0).Value = p_FEE_ID
        pParms(1) = New SqlClient.SqlParameter("@FEE_DESCR", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FEE_DESCR
        pParms(2) = New SqlClient.SqlParameter("@FEE_FTP_ID", SqlDbType.Int)
        pParms(2).Value = p_FEE_FTP_ID
        pParms(3) = New SqlClient.SqlParameter("@FEE_bConcession", SqlDbType.Bit)
        pParms(3).Value = p_FEE_bConcession
        pParms(4) = New SqlClient.SqlParameter("@FEE_ChargedFor_SCH_ID", SqlDbType.Int)
        pParms(4).Value = p_FEE_ChargedFor_SCH_ID
        '		@FEE_Collection_SCH_ID = 23,
        '		@FEE_bFixed = 0,
        '		@FEE_SVC_ID = 4,
        '		@FEE_COL_ID = 23,
        '		@FEE_INC_ACT_ID = N'23',
        pParms(5) = New SqlClient.SqlParameter("@FEE_Collection_SCH_ID", SqlDbType.Int)
        pParms(5).Value = p_FEE_Collection_SCH_ID
        pParms(6) = New SqlClient.SqlParameter("@FEE_bFixed", SqlDbType.Bit)
        pParms(6).Value = p_FEE_bFixed
        pParms(7) = New SqlClient.SqlParameter("@FEE_SVC_ID", SqlDbType.Int)
        pParms(7).Value = p_FEE_SVC_ID
        pParms(8) = New SqlClient.SqlParameter("@FEE_COL_ID", SqlDbType.Int)
        pParms(8).Value = p_FEE_COL_ID
        pParms(9) = New SqlClient.SqlParameter("@FEE_INC_ACT_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FEE_INC_ACT_ID
        '		@FEE_CHRG_INADV_ACT_ID = N'2323',
        '		@FEE_CONCESSION_ACT_ID = N's233',
        '		@FEE_DISC_ACT_ID = N'1234',
        '		@FEE_ORDER = 14,
        '		@FEE_SETOFFORDER = 13,
        pParms(10) = New SqlClient.SqlParameter("@FEE_CHRG_INADV_ACT_ID", SqlDbType.VarChar, 20)
        pParms(10).Value = p_FEE_CHRG_INADV_ACT_ID
        pParms(11) = New SqlClient.SqlParameter("@FEE_CONCESSION_ACT_ID", SqlDbType.VarChar, 20)
        pParms(11).Value = p_FEE_CONCESSION_ACT_ID
        pParms(12) = New SqlClient.SqlParameter("@FEE_DISC_ACT_ID", SqlDbType.VarChar, 20)
        pParms(12).Value = p_FEE_DISC_ACT_ID
        pParms(13) = New SqlClient.SqlParameter("@FEE_ORDER", SqlDbType.Int)
        pParms(13).Value = p_FEE_ORDER
        pParms(14) = New SqlClient.SqlParameter("@FEE_SETOFFORDER", SqlDbType.Int)
        pParms(14).Value = p_FEE_SETOFFORDER
        '		@FEE_LATEAMTTYPE = 12,
        '		@FEE_LATEAMT = 123.56,
        '		@FEE_LATEDAYS = 5 ,
        '@FEE_bREVENUE_SCH_ID =0 ,@FEE_bJOINPRORATA =1, @FEE_bDiscontinuePRORATA =0 
        pParms(15) = New SqlClient.SqlParameter("@FEE_LATEAMTTYPE", SqlDbType.Int)
        pParms(15).Value = p_FEE_LATEAMTTYPE
        pParms(16) = New SqlClient.SqlParameter("@FEE_LATEAMT", SqlDbType.Decimal)
        pParms(16).Value = p_FEE_LATEAMT
        pParms(17) = New SqlClient.SqlParameter("@FEE_LATEDAYS", SqlDbType.Int)
        pParms(17).Value = p_FEE_LATEDAYS
        pParms(18) = New SqlClient.SqlParameter("@FEE_bREVENUE_SCH_ID", SqlDbType.Bit)
        pParms(18).Value = p_FEE_bREVENUE_SCH_ID
        pParms(19) = New SqlClient.SqlParameter("@FEE_bJOINPRORATA", SqlDbType.Bit)
        pParms(19).Value = p_FEE_bJOINPRORATA
        pParms(20) = New SqlClient.SqlParameter("@FEE_bDiscontinuePRORATA", SqlDbType.Bit)
        pParms(20).Value = p_FEE_bDiscontinuePRORATA
        pParms(21) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(21).Direction = ParameterDirection.ReturnValue
        pParms(22) = New SqlClient.SqlParameter("@FEE_bSETUPBYGRADE", SqlDbType.Bit)
        pParms(22).Value = p_FEE_bSETUPBYGRADE
        pParms(23) = New SqlClient.SqlParameter("@FEE_PRO_ID", SqlDbType.Int)
        pParms(23).Value = p_PROC_ID

        pParms(24) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(24).Value = HttpContext.Current.Session("SBsuID")
        pParms(25) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(25).Value = HttpContext.Current.Session("sUsr_name")
        pParms(26) = New SqlClient.SqlParameter("@WINUSER", SqlDbType.VarChar)
        pParms(26).Value = HttpContext.Current.User.Identity.Name.ToString()


        pParms(27) = New SqlClient.SqlParameter("@FEE_AX_INC_ACT_ID", SqlDbType.VarChar, 20)
        pParms(27).Value = p_FEE_AX_INC_ACT_ID
        pParms(28) = New SqlClient.SqlParameter("@FEE_AX_CHRG_INADV_ACT_ID", SqlDbType.VarChar, 20)
        pParms(28).Value = p_FEE_AX_CHRG_INADV_ACT_ID
        pParms(29) = New SqlClient.SqlParameter("@FEE_FGP_ID", SqlDbType.Int)
        pParms(29).Value = p_FEE_FGP_ID
        pParms(30) = New SqlClient.SqlParameter("@FEE_EVT_ID", SqlDbType.VarChar, 20)
        pParms(30).Value = p_FEE_EVT_ID
        pParms(31) = New SqlClient.SqlParameter("@FEE_bShowInOtherColl", SqlDbType.Int)
        pParms(31).Value = p_FEE_bShowInOtherColl
        '@BSU_ID varchar(15) = '',@USER varchar(20)='',@WINUSER varchar(100)=''

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEES_M", pParms)

        F_SaveFEES_M = pParms(21).Value
    End Function

    Public Shared Function F_SaveFEESETUP_S(ByVal p_FSP_ID As Integer, ByVal p_FSP_BSU_ID As String, _
  ByVal p_FSP_ACD_ID As Integer, ByVal p_FSP_FEE_ID As Integer, ByVal p_FSP_GRD_ID As String, _
  ByVal p_FSP_AMOUNT As Decimal, ByVal p_FSP_ONEWAYAMT As Decimal, ByVal p_FSP_bActive As Boolean, ByVal p_FSP_FROMDT As String, _
  ByVal p_FSP_TODT As String, ByVal p_FSP_bJOINPRORATA As Boolean, ByVal p_FSP_bDiscontinuePRORATA As Boolean, _
  ByVal p_FSP_LATEAMTTYPE As Integer, ByVal p_FSP_LATEAMT As Decimal, ByVal p_FSP_LATEDAYS As Integer, _
  ByVal p_FSP_Collection_SCH_ID As Integer, ByVal p_FSP_STM_ID As String, ByVal p_FSP_bWEEKLY_CHARGE As Boolean, _
  ByVal p_FSP_JOIN_FPM_ID As Integer, ByVal p_FSP_DISCONTINUE_FPM_ID As Integer, ByVal p_FSP_SBL_ID As String, _
  ByVal p_RTm_FSM_ID As String, ByRef NEW_FSP_ID As String, ByRef NEW_RTM_ID As String, ByVal p_FSP_PRO_ID As Integer, _
  ByVal p_FSP_BRefundable As Boolean, ByVal FSP_FSM_ID As String, ByVal FSP_NEXT_SCH_ID As String, ByVal p_stTrans As SqlTransaction, _
  Optional ByVal FSP_bINV_DUEDATE As Boolean = False, Optional ByVal FSP_INV_DUEDATE As String = "", _
  Optional ByVal FSP_REVENUE_RECO_ID As Integer = 0) As String
        Dim pParms(31) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FSP_ID", SqlDbType.Int)
        pParms(0).Value = p_FSP_ID
        pParms(1) = New SqlClient.SqlParameter("@FSP_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FSP_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@FSP_ACD_ID", SqlDbType.Int)
        pParms(2).Value = p_FSP_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@FSP_FEE_ID", SqlDbType.Int)
        pParms(3).Value = p_FSP_FEE_ID
        pParms(4) = New SqlClient.SqlParameter("@FSP_GRD_ID", SqlDbType.VarChar, 10)
        If p_FSP_GRD_ID = "" Then
            pParms(4).Value = System.DBNull.Value
        Else
            pParms(4).Value = p_FSP_GRD_ID
        End If
        pParms(5) = New SqlClient.SqlParameter("@FSP_AMOUNT", SqlDbType.Decimal, 21)
        pParms(5).Value = p_FSP_AMOUNT
        pParms(6) = New SqlClient.SqlParameter("@FSP_bActive", SqlDbType.Bit)
        pParms(6).Value = p_FSP_bActive
        pParms(7) = New SqlClient.SqlParameter("@FSP_FROMDT", SqlDbType.DateTime)
        pParms(7).Value = p_FSP_FROMDT
        pParms(8) = New SqlClient.SqlParameter("@FSP_TODT", SqlDbType.DateTime)
        If p_FSP_TODT = "" Then
            pParms(8).Value = System.DBNull.Value
        Else
            If CDate(p_FSP_FROMDT) > CDate(p_FSP_TODT) Then
                Return "606"
            Else
                pParms(8).Value = p_FSP_TODT
            End If
        End If

        pParms(9) = New SqlClient.SqlParameter("@FSP_bJOINPRORATA", SqlDbType.Bit)
        pParms(9).Value = p_FSP_bJOINPRORATA
        pParms(10) = New SqlClient.SqlParameter("@FSP_bDiscontinuePRORATA", SqlDbType.Bit)
        pParms(10).Value = p_FSP_bDiscontinuePRORATA
        pParms(11) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(11).Direction = ParameterDirection.ReturnValue

        pParms(12) = New SqlClient.SqlParameter("@FSP_LATEAMTTYPE", SqlDbType.TinyInt)
        pParms(12).Value = p_FSP_LATEAMTTYPE
        pParms(13) = New SqlClient.SqlParameter("@FSP_LATEAMT", SqlDbType.Decimal, 21)
        pParms(13).Value = p_FSP_LATEAMT
        pParms(14) = New SqlClient.SqlParameter("@FSP_LATEDAYS", SqlDbType.Int)
        pParms(14).Value = p_FSP_LATEDAYS
        pParms(15) = New SqlClient.SqlParameter("@FSP_Collection_SCH_ID", SqlDbType.Int)
        pParms(15).Value = p_FSP_Collection_SCH_ID
        pParms(16) = New SqlClient.SqlParameter("@FSP_STM_ID", SqlDbType.Int)
        If p_FSP_STM_ID = "" Then
            pParms(16).Value = System.DBNull.Value
        Else
            pParms(16).Value = p_FSP_STM_ID
        End If
        pParms(17) = New SqlClient.SqlParameter("@FSP_bWEEKLY_CHARGE", SqlDbType.Bit)
        pParms(17).Value = p_FSP_bWEEKLY_CHARGE
        pParms(18) = New SqlClient.SqlParameter("@NEW_FSP_ID", SqlDbType.Int)
        pParms(18).Direction = ParameterDirection.Output
        pParms(19) = New SqlClient.SqlParameter("@FSP_JOIN_FPM_ID", SqlDbType.Int)
        pParms(19).Value = p_FSP_JOIN_FPM_ID
        pParms(20) = New SqlClient.SqlParameter("@FSP_DISCONTINUE_FPM_ID", SqlDbType.Int)
        pParms(20).Value = p_FSP_DISCONTINUE_FPM_ID
        pParms(21) = New SqlClient.SqlParameter("@FSP_SBL_ID", SqlDbType.Int)
        If p_FSP_SBL_ID = "" Then
            pParms(21).Value = System.DBNull.Value
        Else
            pParms(21).Value = p_FSP_SBL_ID
        End If
        pParms(22) = New SqlClient.SqlParameter("@NEW_RTM_ID", SqlDbType.Int)
        pParms(22).Direction = ParameterDirection.Output
        pParms(23) = New SqlClient.SqlParameter("@RTm_FSM_ID", SqlDbType.Int)
        If p_RTm_FSM_ID = "" Then
            pParms(23).Value = System.DBNull.Value
        Else
            pParms(23).Value = p_RTm_FSM_ID
        End If
        pParms(24) = New SqlClient.SqlParameter("@FSP_PRO_ID", SqlDbType.Int)
        pParms(24).Value = p_FSP_PRO_ID
        pParms(25) = New SqlClient.SqlParameter("@FSP_BRefundable", SqlDbType.Bit)
        pParms(25).Value = p_FSP_BRefundable
        pParms(26) = New SqlClient.SqlParameter("@FSP_FSM_ID", SqlDbType.BigInt)
        pParms(26).Value = FSP_FSM_ID
        pParms(27) = New SqlClient.SqlParameter("@FSP_NEXT_SCH_ID", SqlDbType.Int)
        pParms(27).Value = FSP_NEXT_SCH_ID
        pParms(28) = New SqlClient.SqlParameter("@FSP_ONEWAYAMT", SqlDbType.Decimal, 21)
        pParms(28).Value = p_FSP_ONEWAYAMT

        pParms(29) = New SqlClient.SqlParameter("@FSP_bINV_DUEDATE", SqlDbType.Bit)
        pParms(29).Value = FSP_bINV_DUEDATE
        pParms(30) = New SqlClient.SqlParameter("@FSP_INV_DUEDATE", SqlDbType.DateTime)
        If FSP_bINV_DUEDATE Then
            pParms(30).Value = FSP_INV_DUEDATE
        Else
            pParms(30).Value = System.DBNull.Value
        End If

        pParms(31) = New SqlClient.SqlParameter("@FSP_REVENUE_RECO_ID", SqlDbType.Int)
        pParms(31).Value = FSP_REVENUE_RECO_ID

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEESETUP_S", pParms)
        If pParms(11).Value = 0 Then
            NEW_FSP_ID = pParms(18).Value
        End If
        If Not pParms(22).Value Is DBNull.Value Then
            NEW_RTM_ID = pParms(22).Value
        End If
        Return pParms(11).Value
    End Function

    Public Shared Function F_SaveFEESETUP_M(ByVal p_FSM_ID As Integer, ByVal p_FSM_BSU_ID As String, _
        ByVal p_FSM_ACD_ID As Integer, ByVal p_FSM_FEE_ID As Integer, ByVal p_FSM_CHARGE_SCH_ID As String, _
        ByVal p_FSM_Collection_SCH_ID As Decimal, ByRef NEW_FSM_ID As String, ByVal p_stTrans As SqlTransaction, _
        ByVal FSM_DEF_SCH_ID As String, ByVal FSM_BFlexiblePlan As Boolean) As String
        Dim pParms(11) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FSM_ID", SqlDbType.Int)
        pParms(0).Value = p_FSM_ID
        pParms(1) = New SqlClient.SqlParameter("@FSM_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FSM_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@FSM_ACD_ID", SqlDbType.Int)
        pParms(2).Value = p_FSM_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@FSM_FEE_ID", SqlDbType.Int)
        pParms(3).Value = p_FSM_FEE_ID
        pParms(4) = New SqlClient.SqlParameter("@FSM_CHARGE_SCH_ID", SqlDbType.Int)
        pParms(4).Value = p_FSM_CHARGE_SCH_ID
        pParms(5) = New SqlClient.SqlParameter("@FSM_Collection_SCH_ID", SqlDbType.Int)
        pParms(5).Value = p_FSM_Collection_SCH_ID
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@NEW_FSM_ID", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.Output
        pParms(8) = New SqlClient.SqlParameter("@FSM_DEF_SCH_ID", SqlDbType.Int)
        If FSM_DEF_SCH_ID = "-1" Then
            pParms(8).Value = System.DBNull.Value
        Else
            pParms(8).Value = FSM_DEF_SCH_ID
        End If
        pParms(9) = New SqlClient.SqlParameter("@FSM_BFlexiblePlan", SqlDbType.Bit)
        pParms(9).Value = FSM_BFlexiblePlan

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEESETUP_M", pParms)
        If pParms(6).Value = 0 Then
            NEW_FSM_ID = pParms(7).Value
        End If
        Return pParms(6).Value
    End Function

    Public Shared Function F_SaveFEESETUPMONTHLY_D(ByVal p_FDD_ID As Integer, ByVal p_FDD_FSP_ID As String, _
       ByVal p_FDD_REF_ID As Integer, ByVal p_FDD_DATE As String, ByVal p_FDD_AMOUNT As Decimal, ByVal p_ONEWAY As Decimal, _
       ByVal p_FDD_FIRSTMEMODT As String, ByVal p_FDD_SECONDMEMODT As String, ByVal p_FDD_THIRDMEMODT As String, _
       ByVal p_BSU_ID As String, ByVal p_ACD_ID As String, ByVal p_RTS_RTM_ID As String, ByVal p_USER As String, _
       ByVal FDD_DUEDT As String, ByVal FDD_OTHCHARGE As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(16) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEESETUPMONTHLY_D]
        '@FDD_ID = 0,
        '@FDD_PRO_ID = 1,     
        '@FDD_FSP_ID = 1,
        '@FDD_REF_ID = 1,
        '@FDD_DATE = N'12-MAR-2008',
        '@FDD_AMOUNT = 1200,
        pParms(0) = New SqlClient.SqlParameter("@FDD_ID", SqlDbType.Int)
        pParms(0).Value = p_FDD_ID
        pParms(1) = New SqlClient.SqlParameter("@FDD_FSP_ID", SqlDbType.Int)
        pParms(1).Value = p_FDD_FSP_ID
        pParms(2) = New SqlClient.SqlParameter("@FDD_REF_ID", SqlDbType.Int)
        pParms(2).Value = p_FDD_REF_ID
        pParms(3) = New SqlClient.SqlParameter("@FDD_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FDD_DATE
        pParms(4) = New SqlClient.SqlParameter("@FDD_AMOUNT", SqlDbType.VarChar, 10)
        pParms(4).Value = p_FDD_AMOUNT
        '@FDD_FIRSTMEMODT = N'15-MAR-2008',
        '@FDD_SECONDMEMODT = N'18-MAR-2008',
        '@FDD_THIRDMEMODT = N'21-MAR-2008'
        pParms(5) = New SqlClient.SqlParameter("@FDD_FIRSTMEMODT", SqlDbType.DateTime)
        If p_FDD_FIRSTMEMODT = "" Then
            pParms(5).Value = System.DBNull.Value
        Else
            pParms(5).Value = p_FDD_FIRSTMEMODT
        End If
        pParms(6) = New SqlClient.SqlParameter("@FDD_SECONDMEMODT", SqlDbType.DateTime)
        If p_FDD_SECONDMEMODT = "" Then
            pParms(6).Value = System.DBNull.Value
        Else
            pParms(6).Value = p_FDD_SECONDMEMODT
        End If
        pParms(7) = New SqlClient.SqlParameter("@FDD_THIRDMEMODT", SqlDbType.DateTime)
        If p_FDD_THIRDMEMODT = "" Then
            pParms(7).Value = System.DBNull.Value
        Else
            pParms(7).Value = p_FDD_THIRDMEMODT
        End If
        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        '@BSU_ID VARCHAR(20),
        '@ACD_ID INT
        pParms(9) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_BSU_ID
        pParms(10) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(10).Value = p_ACD_ID
        pParms(11) = New SqlClient.SqlParameter("@RTS_RTM_ID", SqlDbType.BigInt)
        If p_RTS_RTM_ID = "" Then
            pParms(11).Value = System.DBNull.Value
        Else
            pParms(11).Value = p_RTS_RTM_ID
        End If
        pParms(12) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(12).Value = p_USER
        pParms(13) = New SqlClient.SqlParameter("@FDD_DUEDT", SqlDbType.DateTime)
        pParms(13).Value = FDD_DUEDT
        pParms(14) = New SqlClient.SqlParameter("@FDD_OTHCHARGE", SqlDbType.Decimal)
        pParms(14).Value = FDD_OTHCHARGE
        pParms(15) = New SqlClient.SqlParameter("@ONEWAYAMT", SqlDbType.Decimal)
        pParms(15).Value = p_ONEWAY
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEESETUPMONTHLY_D", pParms)
        F_SaveFEESETUPMONTHLY_D = pParms(8).Value
    End Function

    Public Shared Function F_SaveSERVICES_BSU_RATE_S(ByVal p_SBR_ID As Integer, ByVal p_SBR_BSU_ID As String, _
        ByVal p_SBR_SVB_ID As Integer, ByVal p_SBR_PNT_ID As Integer, ByVal p_SBR_RATE As String, _
         ByVal p_SBR_DTFROM As String, ByVal p_SBR_DTTO As String, ByVal p_SBR_GRD_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter
        '@return_value = [dbo].[F_SaveSERVICES_BSU_RATE_S]
        '@SBR_ID = 0 ,
        '@SBR_BSU_ID = N'125016',
        '@SBR_SVB_ID = 1,
        '@SBR_PNT_ID = 2,
        '@SBR_RATE = 23.54,
        '@SBR_DTFROM = N'12-MAR-2008',
        '@SBR_DTTO = NULL
        '@SBR_GRD_ID
        pParms(0) = New SqlClient.SqlParameter("@SBR_ID", SqlDbType.Int)
        pParms(0).Value = p_SBR_ID
        pParms(1) = New SqlClient.SqlParameter("@SBR_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_SBR_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@SBR_SVB_ID", SqlDbType.Int)
        pParms(2).Value = p_SBR_SVB_ID
        pParms(3) = New SqlClient.SqlParameter("@SBR_PNT_ID", SqlDbType.Int)
        pParms(3).Value = p_SBR_PNT_ID
        pParms(4) = New SqlClient.SqlParameter("@SBR_RATE", SqlDbType.Decimal)
        pParms(4).Value = p_SBR_RATE

        pParms(5) = New SqlClient.SqlParameter("@SBR_DTFROM", SqlDbType.DateTime)
        pParms(5).Value = p_SBR_DTFROM
        pParms(6) = New SqlClient.SqlParameter("@SBR_DTTO", SqlDbType.DateTime)
        If p_SBR_DTTO = "" Then
            pParms(6).Value = System.DBNull.Value
        Else
            pParms(6).Value = p_SBR_DTTO
        End If

        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@SBR_GRD_ID", SqlDbType.VarChar, 20)
        If p_SBR_GRD_ID = "" Then
            pParms(8).Value = System.DBNull.Value
        Else
            pParms(8).Value = p_SBR_GRD_ID
        End If
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveSERVICES_BSU_RATE_S", pParms)

        F_SaveSERVICES_BSU_RATE_S = pParms(7).Value
    End Function

    Public Shared Function CreateDataTableFeeSetup() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAcd_year As New DataColumn("Acd_year", System.Type.GetType("System.String"))
            Dim cAcd_id As New DataColumn("Acd_id", System.Type.GetType("System.Decimal"))
            Dim cFee_id As New DataColumn("Fee_id", System.Type.GetType("System.Decimal"))
            Dim cFee_descr As New DataColumn("Fee_descr", System.Type.GetType("System.String"))

            Dim cGrd_id As New DataColumn("Grd_id", System.Type.GetType("System.String"))
            Dim cFDate As New DataColumn("FDate", System.Type.GetType("System.DateTime"))
            Dim cTDate As New DataColumn("TDate", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))

            Dim cJoin As New DataColumn("Join", System.Type.GetType("System.Decimal"))
            Dim cDiscontinue As New DataColumn("Discontinue", System.Type.GetType("System.Decimal"))
            Dim cActive As New DataColumn("Active", System.Type.GetType("System.Boolean"))
            Dim cWeekly As New DataColumn("Weekly", System.Type.GetType("System.Boolean"))
            Dim cPercentage As New DataColumn("Percentage", System.Type.GetType("System.Boolean"))

            Dim cLateAmount As New DataColumn("LateAmount", System.Type.GetType("System.Decimal"))
            Dim cLateDays As New DataColumn("LateDays", System.Type.GetType("System.Decimal"))
            Dim cCollection As New DataColumn("Collection", System.Type.GetType("System.Decimal"))
            Dim cStream As New DataColumn("Stream", System.Type.GetType("System.Decimal"))

            Dim cLocationid As New DataColumn("Locationid", System.Type.GetType("System.String"))
            Dim cLocation As New DataColumn("Location", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            Dim cPRO_ID As New DataColumn("PRO_ID", System.Type.GetType("System.String"))
            Dim cPRO_DESCRIPTION As New DataColumn("PRO_DESCRIPTION", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAcd_year)
            dtDt.Columns.Add(cAcd_id)
            dtDt.Columns.Add(cFee_id)
            dtDt.Columns.Add(cFee_descr)

            dtDt.Columns.Add(cGrd_id)
            dtDt.Columns.Add(cFDate)
            dtDt.Columns.Add(cTDate)
            dtDt.Columns.Add(cAmount)

            dtDt.Columns.Add(cJoin)
            dtDt.Columns.Add(cDiscontinue)
            dtDt.Columns.Add(cActive)
            dtDt.Columns.Add(cWeekly)

            dtDt.Columns.Add(cPercentage)
            dtDt.Columns.Add(cLateAmount)
            dtDt.Columns.Add(cLateDays)
            dtDt.Columns.Add(cCollection)
            dtDt.Columns.Add(cStream)
            dtDt.Columns.Add(cLocationid)
            dtDt.Columns.Add(cLocation)

            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cPRO_ID)
            dtDt.Columns.Add(cPRO_DESCRIPTION)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTableServiceCharges() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cSVB_ID As New DataColumn("SVB_ID", System.Type.GetType("System.String"))
            Dim cPNT_ID As New DataColumn("PNT_ID", System.Type.GetType("System.String"))
            Dim cSVB_DESCR As New DataColumn("SVB_DESCR", System.Type.GetType("System.String"))
            Dim cPNT_DESCR As New DataColumn("PNT_DESCR", System.Type.GetType("System.String"))


            Dim cFDate As New DataColumn("FDate", System.Type.GetType("System.DateTime"))
            Dim cTDate As New DataColumn("TDate", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cGrade As New DataColumn("Grade", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            'SBR_SVB_ID, SBR_PNT_ID, 
            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cSVB_ID)
            dtDt.Columns.Add(cPNT_ID)
            dtDt.Columns.Add(cSVB_DESCR)
            dtDt.Columns.Add(cPNT_DESCR)
            dtDt.Columns.Add(cFDate)
            dtDt.Columns.Add(cTDate)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cGrade)

            dtDt.Columns.Add(cStatus)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function FeeMasterListingSql() As String
        Return "SELECT * FROM(SELECT FEE.FEE_ID,FEE.FEE_DESCR, FEE.FEE_FTP_ID, SVC.SVC_DESCRIPTION," _
            & " SCH_SET.SCH_DESCR AS SET_SCHEDULE, SCH_COL.SCH_DESCR AS COL_SCHEDULE, COL.COL_DESCR, FEE.FEE_bFixed,FEE.FEE_bSETUPBYGRADE" _
            & " FROM FEES.FEES_M AS FEE LEFT OUTER JOIN" _
            & " OASIS..SERVICES_SYS_M AS SVC ON FEE.FEE_SVC_ID = SVC.SVC_ID LEFT OUTER JOIN" _
            & " FEES.SCHEDULE_M AS SCH_SET ON FEE.FEE_ChargedFor_SCH_ID = SCH_SET.SCH_ID LEFT OUTER JOIN" _
            & " FEES.SCHEDULE_M AS SCH_COL ON FEE.FEE_Collection_SCH_ID = SCH_COL.SCH_ID LEFT OUTER JOIN" _
            & " vw_OSF_COLLECTION_M AS COL ON FEE.FEE_COL_ID = COL.COL_ID) AS DB WHERE 1=1 "
    End Function

    Public Shared Sub SetDefaultLinkToState(ByRef txtLinkDescr As TextBox, ByRef hfPRO_ID As HiddenField)
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
        "select PRO_ID, PRO_DESCRIPTION from PROCESSFO_SYS_M  WHERE PRO_ORDER = 1000")
        While (dr.Read())
            txtLinkDescr.Text = dr("PRO_DESCRIPTION")
            hfPRO_ID.Value = dr("PRO_ID")
        End While
    End Sub

    Public Shared Function CreateDataTableFeeSetupMonthly() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try ' FDD_DATE, FDD_AMOUNT, FDD_FIRSTMEMODT, FDD_SECONDMEMODT,FDD_THIRDMEMODT
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cDescr As New DataColumn("Descr", System.Type.GetType("System.String"))
            Dim cFDD_DATE As New DataColumn("FDD_DATE", System.Type.GetType("System.String"))
            Dim cFDD_AMOUNT As New DataColumn("FDD_AMOUNT", System.Type.GetType("System.String"))
            Dim cFDD_FIRSTMEMODT As New DataColumn("FDD_FIRSTMEMODT", System.Type.GetType("System.String"))
            Dim cFDD_SECONDMEMODT As New DataColumn("FDD_SECONDMEMODT", System.Type.GetType("System.String"))
            Dim cFDD_THIRDMEMODT As New DataColumn("FDD_THIRDMEMODT", System.Type.GetType("System.String"))
            Dim cFDD_REF_ID As New DataColumn("FDD_REF_ID", System.Type.GetType("System.String"))
            Dim cFDD_DUEDT As New DataColumn("FDD_DUEDT", System.Type.GetType("System.String"))
            Dim cFDD_OTHCHARGE As New DataColumn("FDD_OTHCHARGE", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cONEWAYAMT As New DataColumn("ONEWAYAMT", System.Type.GetType("System.String"))
            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cDescr)
            dtDt.Columns.Add(cFDD_DATE)
            dtDt.Columns.Add(cFDD_AMOUNT)
            dtDt.Columns.Add(cFDD_FIRSTMEMODT)
            dtDt.Columns.Add(cFDD_SECONDMEMODT)
            dtDt.Columns.Add(cFDD_THIRDMEMODT)
            dtDt.Columns.Add(cFDD_REF_ID)
            dtDt.Columns.Add(cFDD_DUEDT)
            dtDt.Columns.Add(cFDD_OTHCHARGE)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cONEWAYAMT)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function F_DeleteFeeSETUP_OASIS(ByVal p_FSP_ID As Integer, ByVal p_ACD_ID As String, _
        ByVal p_User As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        '@FSP_ID int,@ACD_ID int,@User varchar(50)
        pParms(0) = New SqlClient.SqlParameter("@FSP_ID", SqlDbType.Int)
        pParms(0).Value = p_FSP_ID
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = p_ACD_ID
        pParms(2) = New SqlClient.SqlParameter("@User", SqlDbType.VarChar, 100)
        pParms(2).Value = p_User
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_DeleteFeeSETUP_OASIS", pParms)
        F_DeleteFeeSETUP_OASIS = pParms(3).Value
    End Function

    Public Shared Function UpdateChangedFeesInConcession_OASIS(ByVal p_ACD_ID As Integer, ByVal p_BSU_ID As String, _
        ByVal p_GRD_ID As String, ByVal p_FEE_ID As Integer, ByVal p_STM_ID As String, _
        ByVal P_AUD_USER As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        'create procedure UpdateChangedFeesInConcession_OASIS(
        '@BSU_ID varchar(20),
        '@ACD_ID int,
        '@FEE_ID int,
        '@GRD_ID varchar(20),
        '@STM_ID int,
        '@AUD_USER varchar(50)) 
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(0).Value = p_ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@GRD_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_GRD_ID
        pParms(3) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(3).Value = p_FEE_ID
        pParms(4) = New SqlClient.SqlParameter("@STM_ID", SqlDbType.Int)
        pParms(4).Value = p_STM_ID
        pParms(5) = New SqlClient.SqlParameter("@AUD_USER", SqlDbType.VarChar)
        pParms(5).Value = P_AUD_USER
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.UpdateChangedFeesInConcession_OASIS", pParms)
        UpdateChangedFeesInConcession_OASIS = pParms(6).Value
    End Function

End Class
