﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class clsTaxFunctions

#Region "Variables"
    Private pSTU_ID As Long
    Public Property STU_ID() As Long
        Get
            Return pSTU_ID
        End Get
        Set(ByVal value As Long)
            pSTU_ID = value
        End Set
    End Property

    Private pSTU_BSU_ID As String
    Public Property STU_BSU_ID() As String
        Get
            Return pSTU_BSU_ID
        End Get
        Set(ByVal value As String)
            pSTU_BSU_ID = value
        End Set
    End Property
    Private pACD_ID As Integer
    Public Property ACD_ID() As Integer
        Get
            Return pACD_ID
        End Get
        Set(ByVal value As Integer)
            pACD_ID = value
        End Set
    End Property
    Private pPROVIDER_BSU_ID As String
    Public Property PROVIDER_BSU_ID() As String
        Get
            Return pPROVIDER_BSU_ID
        End Get
        Set(ByVal value As String)
            pPROVIDER_BSU_ID = value
        End Set
    End Property
    Private pSTU_NAME As String
    Public Property STU_NAME() As String
        Get
            Return pSTU_NAME
        End Get
        Set(ByVal value As String)
            pSTU_NAME = value
        End Set
    End Property

    Private pSTU_NO As String
    Public Property STU_NO() As String
        Get
            Return pSTU_NO
        End Get
        Set(ByVal value As String)
            pSTU_NO = value
        End Set
    End Property
    Private pSTU_TYPE As String
    Public Property STU_TYPE() As String
        Get
            Return pSTU_TYPE
        End Get
        Set(ByVal value As String)
            pSTU_TYPE = value
        End Set
    End Property
    Private pGRD_DISPLAY As String
    Public Property GRD_DISPLAY() As String
        Get
            Return pGRD_DISPLAY
        End Get
        Set(ByVal value As String)
            pGRD_DISPLAY = value
        End Set
    End Property
    Private pSTU_CURRSTATUS As String
    Public Property STU_CURRSTATUS() As String
        Get
            Return pSTU_CURRSTATUS
        End Get
        Set(ByVal value As String)
            pSTU_CURRSTATUS = value
        End Set
    End Property

    Private pSTU_DOJ As Date
    Public Property STU_DOJ() As Date
        Get
            Return pSTU_DOJ
        End Get
        Set(ByVal value As Date)
            pSTU_DOJ = value
        End Set
    End Property
    Private pSTU_LASTATTDATE As Date
    Public Property STU_LASTATTDATE() As Date
        Get
            Return pSTU_LASTATTDATE
        End Get
        Set(ByVal value As Date)
            pSTU_LASTATTDATE = value
        End Set
    End Property
    Private pSTU_LEAVEDATE As Date
    Public Property STU_LEAVEDATE() As Date
        Get
            Return pSTU_LEAVEDATE
        End Get
        Set(ByVal value As Date)
            pSTU_LEAVEDATE = value
        End Set
    End Property

    Private pFEE_ID As Integer
    Public Property FEE_ID() As Integer
        Get
            Return pFEE_ID
        End Get
        Set(ByVal value As Integer)
            pFEE_ID = value
        End Set
    End Property

    Private pTOTAL_DR As Double
    Public Property TOTAL_DR() As Double
        Get
            Return pTOTAL_DR
        End Get
        Set(ByVal value As Double)
            pTOTAL_DR = value
        End Set
    End Property

    Private pTOTAL_CR As Double
    Public Property TOTAL_CR() As Double
        Get
            Return pTOTAL_CR
        End Get
        Set(ByVal value As Double)
            pTOTAL_CR = value
        End Set
    End Property

    Private pBALANCE As Double
    Public Property BALANCE() As Double
        Get
            Return pBALANCE
        End Get
        Set(ByVal value As Double)
            pBALANCE = value
        End Set
    End Property

    Private pbSUMMARY As Boolean
    Public Property bSUMMARY() As Boolean
        Get
            Return pbSUMMARY
        End Get
        Set(ByVal value As Boolean)
            pbSUMMARY = value
        End Set
    End Property

    Private pUser As String
    Public Property User() As String
        Get
            Return pUser
        End Get
        Set(ByVal value As String)
            pUser = value
        End Set
    End Property
    Private pAIR_DATE As Date
    Public Property AIR_DATE() As Date
        Get
            Return pAIR_DATE
        End Get
        Set(ByVal value As Date)
            pAIR_DATE = value
        End Set
    End Property
    Private pAIR_FROM As String
    Public Property AIR_FROM() As String
        Get
            Return pAIR_FROM
        End Get
        Set(ByVal value As String)
            pAIR_FROM = value
        End Set
    End Property
    Private pAIR_AMOUNT As Double
    Public Property AIR_AMOUNT() As Double
        Get
            Return pAIR_AMOUNT
        End Get
        Set(ByVal value As Double)
            pAIR_AMOUNT = value
        End Set
    End Property
    Private pAIR_NARRATION As String
    Public Property AIR_NARRATION() As String
        Get
            Return pAIR_NARRATION
        End Get
        Set(ByVal value As String)
            pAIR_NARRATION = value
        End Set
    End Property

    Private pGRD_ID As String
    Public Property GRD_ID() As String
        Get
            Return pGRD_ID
        End Get
        Set(ByVal value As String)
            pGRD_ID = value
        End Set
    End Property
    Private pSTU_IDs As String
    Public Property STU_IDs() As String
        Get
            Return pSTU_IDs
        End Get
        Set(ByVal value As String)
            pSTU_IDs = value
        End Set
    End Property
    Private pPeriod As String
    Public Property Period() As String
        Get
            Return pPeriod
        End Get
        Set(ByVal value As String)
            pPeriod = value
        End Set
    End Property
    Private pBTerm As Boolean
    Public Property BTerm() As Boolean
        Get
            Return pBTerm
        End Get
        Set(ByVal value As Boolean)
            pBTerm = value
        End Set
    End Property
    Private pINV_AMOUNT As Double
    Public Property INV_AMOUNT() As Double
        Get
            Return pINV_AMOUNT
        End Get
        Set(ByVal value As Double)
            pINV_AMOUNT = value
        End Set
    End Property

    Private pMIT_DATE As Date
    Public Property MIT_DATE() As Date
        Get
            Return pMIT_DATE
        End Get
        Set(ByVal value As Date)
            pMIT_DATE = value
        End Set
    End Property
    Private pMIT_BATCH_NO As Integer
    Public Property MIT_BATCH_NO() As Integer
        Get
            Return pMIT_BATCH_NO
        End Get
        Set(ByVal value As Integer)
            pMIT_BATCH_NO = value
        End Set
    End Property
    Private pMIT_NARRATION As String
    Public Property MIT_NARRATION() As String
        Get
            Return pMIT_NARRATION
        End Get
        Set(ByVal value As String)
            pMIT_NARRATION = value
        End Set
    End Property
    Private pMIT_TERM_IDs As String
    Public Property MIT_TERM_IDs() As String
        Get
            Return pMIT_TERM_IDs
        End Get
        Set(ByVal value As String)
            pMIT_TERM_IDs = value
        End Set
    End Property
    Private pNEW_INV_NO As String
    Public Property NEW_INV_NO() As String
        Get
            Return pNEW_INV_NO
        End Get
        Set(ByVal value As String)
            pNEW_INV_NO = value
        End Set
    End Property
    Private pFROM_DATE As Date
    Public Property FROM_DATE() As Date
        Get
            Return pFROM_DATE
        End Get
        Set(ByVal value As Date)
            pFROM_DATE = value
        End Set
    End Property
    Private pTO_DATE As Date
    Public Property TO_DATE() As Date
        Get
            Return pTO_DATE
        End Get
        Set(ByVal value As Date)
            pTO_DATE = value
        End Set
    End Property
    Private pReversePrevInvoice As Boolean
    Public Property bReversePrevInvoice() As Boolean
        Get
            Return pReversePrevInvoice
        End Get
        Set(ByVal value As Boolean)
            pReversePrevInvoice = value
        End Set
    End Property
    '
#End Region
#Region "Methods"

    Public Shared Function GetBusinessUnits(ByVal usrName As String, Optional ByVal showActiveOnly As Boolean = False) As DataSet
        Dim str_sql As String = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetBusinessUnits] ('" & usrName & "') "
        If showActiveOnly Then
            str_sql = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetBusinessUnits] ('" & usrName & "') WHERE ISNULL(BSU_bSHOW, 0) = 1 "
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function GetServiceBusinessUnits(ByVal usrName As String, ByVal PROVIDER_BSU_ID As String, Optional ByVal showActiveOnly As Boolean = False) As DataSet
        Dim str_sql As String = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetServiceBusinessUnits] ('" & usrName & "','" & PROVIDER_BSU_ID & "') "
        If showActiveOnly Then
            str_sql = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetServiceBusinessUnits] ('" & usrName & "','" & PROVIDER_BSU_ID & "') WHERE ISNULL(BSU_bSHOW, 0) = 1 "
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function GetFeeType(ByVal BSU_ID As String) As DataSet
        Dim str_sql As String = "SELECT FEE_ID,FEE_DESCR FROM FEES.FEES_M WITH ( NOLOCK ) WHERE FEE_ID IN ( SELECT DISTINCT FSH_FEE_ID FROM FEES.FEESCHEDULE WITH ( NOLOCK ) WHERE FSH_BSU_ID = '" & BSU_ID & "' AND FSH_SOURCE='FEE ADVANCE INVOICE')ORDER BY FEE_DESCR"
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function GetStudent(ByVal BSUID As String, ByVal STU_TYPE As String, ByVal prefix As String) As String()
        Dim student As New List(Of String)()
        Dim strConn As String = ""
        Dim Qry As New StringBuilder
        If STU_TYPE = "S" Then
            Qry.Append("SELECT TOP 15 STU_ID,STU_NO,ISNULL(STU.STU_FIRSTNAME, '') + ' ' + ISNULL(STU.STU_MIDNAME, '') + ' ' + ISNULL(STU.STU_LASTNAME, '') AS STU_NAME FROM dbo.STUDENT_M AS STU WITH (NOLOCK) ")
            Qry.Append("WHERE STU_BSU_ID = @BSUID ")
            If IsNumeric(Trim(prefix)) Then
                Qry.Append(" AND (STU_NO LIKE '%' + @prefix + '%') ORDER BY STU_NO")
            Else
                Qry.Append(" AND (LTRIM(RTRIM(ISNULL(STU.STU_FIRSTNAME, ''))) + ' ' + LTRIM(RTRIM(ISNULL(STU.STU_MIDNAME, ''))) + ' ' + LTRIM(RTRIM(ISNULL(STU.STU_LASTNAME, ''))) LIKE '%' + @prefix + '%') ORDER BY LTRIM(RTRIM(ISNULL(STU.STU_FIRSTNAME, '')))")
            End If
            strConn = ConnectionManger.GetOASISConnectionString
        Else
            Qry.Append("SELECT TOP 15 A.EQS_ID AS STU_ID ,CAST(ISNULL(EQS_STU_NO, CAST(EQS_APPLNO AS VARCHAR(20)) + 'A') AS VARCHAR(20)) AS STU_NO, ")
            Qry.Append("ISNULL(B.EQM_APPLFIRSTNAME, '') + ' ' + ISNULL(B.EQM_APPLMIDNAME, '') + ' ' + ISNULL(B.EQM_APPLLASTNAME, '') AS STU_NAME ")
            Qry.Append("FROM dbo.ENQUIRY_SCHOOLPRIO_S AS A WITH(NOLOCK) LEFT OUTER JOIN dbo.ENQUIRY_M AS B WITH ( NOLOCK ) ON EQS_EQM_ENQID = B.EQM_ENQID ")
            Qry.Append("WHERE A.EQS_BSU_ID=@BSUID AND ISNULL(A.EQS_STATUS, '') <> 'ENR' ")
            If IsNumeric(Trim(prefix)) Then
                Qry.Append(" AND ((CAST(ISNULL(EQS_STU_NO, CAST(EQS_APPLNO AS VARCHAR(20))) AS VARCHAR(20))) LIKE '%' + @prefix + '%') ORDER BY CAST(ISNULL(EQS_STU_NO, CAST(EQS_APPLNO AS VARCHAR(20))) AS VARCHAR(20))")
            Else
                Qry.Append(" AND (LTRIM(RTRIM(ISNULL(B.EQM_APPLFIRSTNAME, ''))) + ' ' + LTRIM(RTRIM(ISNULL(B.EQM_APPLMIDNAME, ''))) + ' ' + LTRIM(RTRIM(ISNULL(B.EQM_APPLLASTNAME, ''))) LIKE '%' + @prefix + '%') ORDER BY LTRIM(RTRIM(ISNULL(B.EQM_APPLFIRSTNAME, ''))) ")
            End If

            strConn = ConnectionManger.GetOASISConnectionString
        End If

        Using conn As New SqlConnection()
            conn.ConnectionString = strConn
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                cmd.Parameters.AddWithValue("@BSUID", BSUID)
                cmd.Parameters.AddWithValue("@prefix", Trim(prefix))

                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        student.Add(String.Format("{0}-{1}-{2}", sdr("STU_NAME").ToString.Replace("-", " "), sdr("STU_NO"), sdr("STU_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return student.ToArray()
        End Using
    End Function
    Public Shared Function GetStudent(ByVal BSUID As String, ByVal STU_TYPE As String, ByVal prefix As String, ByVal COMPID As String) As String()
        Dim student As New List(Of String)()
        Dim strConn As String = ""
        Dim Qry As New StringBuilder
        'If STU_TYPE = "S" Then
        '    Qry.Append("SELECT TOP 15 STU_ID,STU_NO,ISNULL(STU.STU_FIRSTNAME, '') + ' ' + ISNULL(STU.STU_MIDNAME, '') + ' ' + ISNULL(STU.STU_LASTNAME, '') AS STU_NAME FROM OASIS.dbo.STUDENT_M AS STU WITH (NOLOCK) ")
        '    Qry.Append("LEFT JOIN dbo.STUDENT_D AS STD WITH (NOLOCK) ON STU.STU_SIBLING_ID = STD.STS_STU_ID WHERE STU_BSU_ID = @BSUID ")
        '    If COMPID <> -1 Then
        '        Qry.Append(" AND CASE ISNULL(STD.STS_FEESPONSOR, 0) WHEN 4 THEN ISNULL(STD.STS_F_COMP_ID, '') WHEN 5 THEN ISNULL(STD.STS_M_COMP_ID, '') WHEN 6 THEN ISNULL(STD.STS_G_COMP_ID, '') END = '" & COMPID & "'")
        '    End If
        '    If IsNumeric(Trim(prefix)) Then
        '        Qry.Append(" AND (STU_NO LIKE '%' + @prefix + '%') ORDER BY STU_NO")
        '    Else
        '        Qry.Append(" AND (LTRIM(RTRIM(ISNULL(STU.STU_FIRSTNAME, ''))) + ' ' + LTRIM(RTRIM(ISNULL(STU.STU_MIDNAME, ''))) + ' ' + LTRIM(RTRIM(ISNULL(STU.STU_LASTNAME, ''))) LIKE '%' + @prefix + '%')")
        '        Qry.Append(" ORDER BY LTRIM(RTRIM(ISNULL(STU.STU_FIRSTNAME, '')))")
        '    End If
        '    strConn = ConnectionManger.GetOASIS_FEESConnectionString
        'Else
        '    Qry.Append("SELECT TOP 15 A.EQS_ID AS STU_ID ,CAST(ISNULL(EQS_STU_NO, CAST(EQS_APPLNO AS VARCHAR(20)) + 'A') AS VARCHAR(20)) AS STU_NO, ")
        '    Qry.Append("ISNULL(B.EQM_APPLFIRSTNAME, '') + ' ' + ISNULL(B.EQM_APPLMIDNAME, '') + ' ' + ISNULL(B.EQM_APPLLASTNAME, '') AS STU_NAME ")
        '    Qry.Append("FROM dbo.ENQUIRY_SCHOOLPRIO_S AS A WITH(NOLOCK) LEFT OUTER JOIN dbo.ENQUIRY_M AS B WITH ( NOLOCK ) ON EQS_EQM_ENQID = B.EQM_ENQID ")
        '    Qry.Append("LEFT OUTER JOIN dbo.ENQUIRY_PARENT_M AS EPM WITH (NOLOCK) ON B.EQM_ENQID = EPM.EQP_EQM_ENQID ")
        '    Qry.Append("WHERE A.EQS_BSU_ID=@BSUID AND ISNULL(A.EQS_STATUS, '') <> 'ENR' ")
        '    If COMPID <> -1 Then
        '        Qry.Append(" AND CASE B.EQM_PRIMARYCONTACT WHEN 'F' THEN ISNULL(EPM.EQP_FCOMP_ID, '') WHEN 'M' THEN ISNULL(EPM.EQP_MCOMP_ID, '') WHEN 'G' THEN ISNULL(EPM.EQP_GCOMP_ID, '') END = '" & COMPID & "' ")
        '    End If
        '    If IsNumeric(Trim(prefix)) Then
        '        Qry.Append(" AND ((CAST(ISNULL(EQS_STU_NO, CAST(EQS_APPLNO AS VARCHAR(20))) AS VARCHAR(20))) LIKE '%' + @prefix + '%') ORDER BY CAST(ISNULL(EQS_STU_NO, CAST(EQS_APPLNO AS VARCHAR(20))) AS VARCHAR(20))")
        '    Else
        '        Qry.Append(" AND (ISNULL(B.EQM_APPLFIRSTNAME, '') + ' ' + ISNULL(B.EQM_APPLMIDNAME, '') + ' ' + ISNULL(B.EQM_APPLLASTNAME, '') LIKE '%' + @prefix + '%') ")
        '        Qry.Append(" ORDER BY LTRIM(RTRIM(ISNULL(B.EQM_APPLFIRSTNAME, '')))")
        '    End If
        '    strConn = ConnectionManger.GetOASISConnectionString
        'End If
        Qry.Append("EXEC dbo.GLOBAL_SEARCH @SEARCH_TERM = 'STUDENT', @STU_TYPE = '" & STU_TYPE & "', @BSU_ID = '" & BSUID & "',@username='',@modulekey='', @PREFIX_TEXT = '" & prefix.Trim & "', @bEXCLUDE_CSS_STYLE = 1")
        strConn = ConnectionManger.GetOASISConnectionString
        Using conn As New SqlConnection()
            conn.ConnectionString = strConn
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        student.Add(String.Format("{0}$${1}$${2}", sdr("STU_NAME").ToString.Replace("-", " "), sdr("STU_NO"), sdr("STU_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return student.ToArray()
        End Using
    End Function

    Public Function GET_STUDENT_DETAILS(ByVal STU_TYPE As String) As Boolean
        GET_STUDENT_DETAILS = False
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("SELECT STU_ID,STU_NO,STU_NAME,GRD_DISPLAY,STU_DOJ,CASE WHEN STU_TYPE='E' THEN ISNULL(STU_CURRSTATUS,EQS_STATUS) ELSE ISNULL(STU_CURRSTATUS,'') END STU_CURRSTATUS,STU_LASTATTDATE,STU_LEAVEDATE FROM ")
            Qry.Append("[dbo].[VW_OSO_STUDENT_ENQUIRY] WHERE STU_BSU_ID=@STU_BSU_ID AND STU_TYPE=@STU_TYPE AND STU_ID=@STU_ID ")

            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = STU_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(1).Value = STU_ID
            pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
            pParms(2).Value = STU_TYPE
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                STU_NO = ds.Tables(0).Rows(0)("STU_NO").ToString
                STU_NAME = ds.Tables(0).Rows(0)("STU_NAME").ToString
                GRD_DISPLAY = ds.Tables(0).Rows(0)("GRD_DISPLAY").ToString
                STU_CURRSTATUS = ds.Tables(0).Rows(0)("STU_CURRSTATUS").ToString
                If Not ds.Tables(0).Rows(0)("STU_LASTATTDATE") Is DBNull.Value AndAlso IsDate(ds.Tables(0).Rows(0)("STU_LASTATTDATE")) Then
                    STU_LASTATTDATE = ds.Tables(0).Rows(0)("STU_LASTATTDATE").ToString
                End If
                If Not ds.Tables(0).Rows(0)("STU_LEAVEDATE") Is DBNull.Value AndAlso IsDate(ds.Tables(0).Rows(0)("STU_LEAVEDATE")) Then
                    STU_LEAVEDATE = ds.Tables(0).Rows(0)("STU_LEAVEDATE").ToString
                End If

                GET_STUDENT_DETAILS = True
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Function GET_ADVANCE_INV_STATEMENT(ByVal STU_TYPE As String) As DataTable
        GET_ADVANCE_INV_STATEMENT = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC FEES.GET_STU_FEE_ADVANCE_TAX_INV_STATEMENT @STU_BSU_ID = @STU_BSU_ID,@bSUMMARY = @bSUMMARY, ")
            Qry.Append("@STU_TYPE = @STU_TYPE, @STU_ID = @STU_ID,@FEE_ID = @FEE_ID,@TOTAL_DEBIT = @TOT_DEBIT OUTPUT,@TOTAL_CREDIT = @TOT_CREDIT OUTPUT ,@BALANCE = @BAL OUTPUT  ")

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = STU_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
            pParms(1).Value = STU_TYPE
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.Int)
            pParms(2).Value = STU_ID
            pParms(3) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
            pParms(3).Value = FEE_ID
            pParms(4) = New SqlClient.SqlParameter("@TOT_DEBIT", SqlDbType.Decimal)
            pParms(4).Direction = ParameterDirection.Output
            pParms(5) = New SqlClient.SqlParameter("@TOT_CREDIT", SqlDbType.Decimal)
            pParms(5).Direction = ParameterDirection.Output
            pParms(6) = New SqlClient.SqlParameter("@BAL", SqlDbType.Decimal)
            pParms(6).Direction = ParameterDirection.Output
            pParms(7) = New SqlClient.SqlParameter("@bSUMMARY", SqlDbType.Bit)
            pParms(7).Value = bSUMMARY
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_ADVANCE_INV_STATEMENT = ds.Tables(0)
                TOTAL_DR = IIf(IsDBNull(pParms(4).Value), 0, pParms(4).Value)
                TOTAL_CR = IIf(IsDBNull(pParms(5).Value), 0, pParms(5).Value)
                BALANCE = IIf(IsDBNull(pParms(6).Value), 0, pParms(6).Value)
            End If
        Catch ex As Exception
            GET_ADVANCE_INV_STATEMENT = Nothing
        End Try
    End Function
    Public Function GET_ADVANCE_INV_STATEMENT_TRANSPORT(ByVal STU_TYPE As String) As DataTable
        GET_ADVANCE_INV_STATEMENT_TRANSPORT = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC FEES.GET_STU_FEE_ADVANCE_TAX_INV_STATEMENT @BSU_ID = @BSU_ID, @STU_BSU_ID = @STU_BSU_ID,@bSUMMARY = @bSUMMARY, ")
            Qry.Append("@STU_TYPE = @STU_TYPE, @STU_ID = @STU_ID,@FEE_ID = @FEE_ID,@TOTAL_DEBIT = @TOT_DEBIT OUTPUT,@TOTAL_CREDIT = @TOT_CREDIT OUTPUT ,@BALANCE = @BAL OUTPUT  ")

            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = PROVIDER_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = STU_BSU_ID
            pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
            pParms(2).Value = STU_TYPE
            pParms(3) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.Int)
            pParms(3).Value = STU_ID
            pParms(4) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
            pParms(4).Value = FEE_ID
            pParms(5) = New SqlClient.SqlParameter("@TOT_DEBIT", SqlDbType.Decimal)
            pParms(5).Direction = ParameterDirection.Output
            pParms(6) = New SqlClient.SqlParameter("@TOT_CREDIT", SqlDbType.Decimal)
            pParms(6).Direction = ParameterDirection.Output
            pParms(7) = New SqlClient.SqlParameter("@BAL", SqlDbType.Decimal)
            pParms(7).Direction = ParameterDirection.Output
            pParms(8) = New SqlClient.SqlParameter("@bSUMMARY", SqlDbType.Bit)
            pParms(8).Value = bSUMMARY
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_ADVANCE_INV_STATEMENT_TRANSPORT = ds.Tables(0)
                TOTAL_DR = IIf(IsDBNull(pParms(5).Value), 0, pParms(5).Value)
                TOTAL_CR = IIf(IsDBNull(pParms(6).Value), 0, pParms(6).Value)
                BALANCE = IIf(IsDBNull(pParms(7).Value), 0, pParms(7).Value)
            End If
        Catch ex As Exception
            GET_ADVANCE_INV_STATEMENT_TRANSPORT = Nothing
        End Try
    End Function

    Public Function SAVE_ADV_TAX_REVERSAL(ByRef RetMessage As String) As Integer
        SAVE_ADV_TAX_REVERSAL = 1
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        Try
            Dim trans As SqlTransaction
            conn.Open()
            trans = conn.BeginTransaction("ADV_REV")
            Dim cmd As SqlCommand
            Dim RetVal As Integer = 0

            cmd = New SqlCommand("TAX.REVERSE_ADV_TAXINVOICE", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@AIR_BSU_ID", SqlDbType.VarChar, 20)
            sqlp1.Value = PROVIDER_BSU_ID
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@AIR_STU_BSU_ID", SqlDbType.VarChar, 20)
            sqlp2.Value = STU_BSU_ID
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@AIR_DATE", SqlDbType.DateTime)
            sqlp3.Value = AIR_DATE
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@AIR_FROM", SqlDbType.VarChar, 15)
            sqlp4.Value = AIR_FROM
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@AIR_STU_ID", SqlDbType.BigInt)
            sqlp5.Value = STU_ID
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@AIR_STU_TYPE", SqlDbType.VarChar, 5)
            sqlp6.Value = STU_TYPE
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@AIR_FEE_ID", SqlDbType.Int)
            sqlp7.Value = FEE_ID
            cmd.Parameters.Add(sqlp7)

            Dim sqlp8 As New SqlParameter("@AIR_AMOUNT", SqlDbType.Decimal)
            sqlp8.Value = AIR_AMOUNT
            cmd.Parameters.Add(sqlp8)

            Dim sqlp9 As New SqlParameter("@AIR_NARRATION", SqlDbType.VarChar, 500)
            sqlp9.Value = AIR_NARRATION
            cmd.Parameters.Add(sqlp9)

            Dim sqlp10 As New SqlParameter("@AIR_LOG_USER", SqlDbType.VarChar, 20)
            sqlp10.Value = User
            cmd.Parameters.Add(sqlp10)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            RetVal = retSValParam.Value

            If RetVal <> 0 Then
                trans.Rollback()
                SAVE_ADV_TAX_REVERSAL = RetVal
            Else
                trans.Commit()
                SAVE_ADV_TAX_REVERSAL = 0
            End If
        Catch ex As Exception
            RetMessage = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Public Shared Function PopulateMonthsInAcademicYear(ByVal ACD_ID As Integer, ByVal BSU_ID As String) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT TM.TRM_ID ,AMS_ID ,TM.TRM_DESCRIPTION ,AMS_MONTH , " & _
            "DATENAME(month, ISNULL(AM.AMS_FEE_DTFROM, AM.AMS_DTFROM)) + '-' + ltrim(str(datepart(yyyy,ISNULL(AM.AMS_FEE_DTFROM, AM.AMS_DTFROM)))) " & _
            " AS MONTH_DESCR FROM ACADEMIC_MonthS_S AS AM WITH ( NOLOCK ) LEFT OUTER JOIN " & _
            " TRM_M AS TM WITH ( NOLOCK ) ON AM.AMS_TRM_ID = TM.TRM_ID WHERE AMS_ACD_ID = " & ACD_ID & _
            " AND TRM_BSU_ID = '" & BSU_ID & "' AND ISNULL(AM.AMS_FEE_DTFROM, AM.AMS_DTFROM) > OASIS.dbo.fnFormatDateToDisplay(GETDATE())"
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function PopulateTermsInAcademicYear(ByVal ACD_ID As Integer, ByVal BSU_ID As String) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT TRM_M.TRM_ID, TRM_M.TRM_DESCRIPTION FROM " & _
            " TRM_M WITH ( NOLOCK ) WHERE TRM_BSU_ID = '" & BSU_ID & "' AND TRM_ACD_ID = " & ACD_ID & _
            " AND TRM_STARTDATE > OASIS.dbo.fnFormatDateToDisplay(GETDATE())"
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function GET_FEE_TYPE_FOR_ADV_TAX_INVOICE(ByVal BSU_ID As String) As DataSet
        Dim ds As New DataSet
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_FEE_TYPES_FOR_TAX_INVOICE", pParms)
        Return ds
    End Function

    Public Function GET_FEE_SETUP_FOR_ADV_TAX_INVOICE(ByVal BSU_ID As String) As DataTable
        EXCLUDE_DUPLICATE_STUDENT()
        Dim ds As New DataSet
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_IDs", SqlDbType.VarChar, -1)
        pParms(2).Value = STU_IDs
        pParms(3) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(3).Value = STU_TYPE
        pParms(4) = New SqlClient.SqlParameter("@Period", SqlDbType.VarChar, 200)
        pParms(4).Value = Period
        pParms(5) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
        pParms(5).Value = FEE_ID
        pParms(6) = New SqlClient.SqlParameter("@BTerm", SqlDbType.Bit)
        pParms(6).Value = BTerm

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.F_GET_FEESETUP_TAX_INVOICE", pParms)
        If Not ds Is Nothing Then
            If Not HttpContext.Current.Session("gvStudents") Is Nothing Then
                Dim dt As DataTable = DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
                If dt.Rows.Count >= 0 Then
                    dt.Merge(ds.Tables(0))
                    HttpContext.Current.Session("gvStudents") = dt
                    REARRANGE_DATATABLE()
                End If
                Return dt
            Else
                Return ds.Tables(0)
            End If
        Else
            Return DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
        End If
    End Function

    Private Sub EXCLUDE_DUPLICATE_STUDENT()
        If Not HttpContext.Current.Session("gvStudents") Is Nothing Then
            Dim strSTUID() As String = STU_IDs.Split("||")
            If strSTUID.Length > 0 Then
                For i As Int16 = 0 To strSTUID.Length - 1
                    If strSTUID(i).ToString <> "" AndAlso IsNumeric(strSTUID(i).ToString) Then
                        Dim filter As String = "STU_ID='" & strSTUID(i).ToString & "'"
                        Dim view As New DataView(DirectCast(HttpContext.Current.Session("gvStudents"), DataTable))
                        view.RowFilter = filter
                        If Not view Is Nothing AndAlso view.ToTable().Rows.Count > 0 Then
                            STU_IDs = STU_IDs.Replace(strSTUID(i), "0")
                        End If
                    End If
                Next
            End If
        End If
    End Sub
    Public Shared Sub REARRANGE_DATATABLE()
        If Not HttpContext.Current.Session("gvStudents") Is Nothing Then
            Dim dt As DataTable = DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
            Dim rows As Integer = dt.Rows.Count
            For i As Integer = 1 To rows Step 1
                dt.Rows(i - 1)("ID") = i
                'i = i + 1
            Next
            HttpContext.Current.Session("gvStudents") = dt
        End If
    End Sub
    Public Function GENERATE_ADV_TAX_INVOICE(ByRef RetMessage As String) As Integer
        GENERATE_ADV_TAX_INVOICE = 1
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            Dim trans As SqlTransaction
            conn.Open()
            trans = conn.BeginTransaction("ADV_INV")
            Dim cmd As SqlCommand
            Dim RetVal As Integer = 0

            cmd = New SqlCommand("FEES.GENERATE_MANUAL_TAX_INVOICE", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@MIT_BSU_ID", SqlDbType.VarChar, 20)
            sqlp1.Value = STU_BSU_ID
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@MIT_ACD_ID", SqlDbType.Int)
            sqlp2.Value = ACD_ID
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@MIT_DATE", SqlDbType.Date)
            sqlp3.Value = MIT_DATE
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@MIT_BATCH_NO", SqlDbType.Int)
            sqlp4.Value = MIT_BATCH_NO
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@MIT_STU_BSU_ID", SqlDbType.VarChar, 20)
            sqlp5.Value = STU_BSU_ID
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@MIT_STU_TYPE", SqlDbType.VarChar, 5)
            sqlp6.Value = STU_TYPE
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@MIT_STU_ID", SqlDbType.BigInt)
            sqlp7.Value = STU_ID
            cmd.Parameters.Add(sqlp7)

            Dim sqlp8 As New SqlParameter("@MIT_FEE_ID", SqlDbType.Int)
            sqlp8.Value = FEE_ID
            cmd.Parameters.Add(sqlp8)

            Dim sqlp9 As New SqlParameter("@MIT_bTERM", SqlDbType.Bit)
            sqlp9.Value = BTerm
            cmd.Parameters.Add(sqlp9)

            Dim sqlp10 As New SqlParameter("@MIT_TERM_IDs", SqlDbType.VarChar, 500)
            sqlp10.Value = MIT_TERM_IDs
            cmd.Parameters.Add(sqlp10)

            Dim sqlp12 As New SqlParameter("@MIT_INV_AMOUNT", SqlDbType.Decimal)
            sqlp12.Value = INV_AMOUNT
            cmd.Parameters.Add(sqlp12)

            Dim sqlp13 As New SqlParameter("@MIT_NARRATION", SqlDbType.VarChar, 200)
            sqlp13.Value = MIT_NARRATION
            cmd.Parameters.Add(sqlp13)

            Dim sqlp14 As New SqlParameter("@MIT_LOG_USER", SqlDbType.VarChar, 20)
            sqlp14.Value = User
            cmd.Parameters.Add(sqlp14)

            Dim sqlp15 As New SqlParameter("@bREVERSE_PREV_ADV_INV", SqlDbType.Bit)
            sqlp15.Value = bReversePrevInvoice
            cmd.Parameters.Add(sqlp15)

            Dim sqlp16 As New SqlParameter("@PREV_ADV_INVOICE_BAL_AMT", SqlDbType.Decimal)
            sqlp16.Value = AIR_AMOUNT 'Previous advance invoice reversal amount balance
            cmd.Parameters.Add(sqlp16)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            RetVal = retSValParam.Value

            If RetVal <> 0 Then
                trans.Rollback()
                GENERATE_ADV_TAX_INVOICE = RetVal
                NEW_INV_NO = ""
            Else
                trans.Commit()
                GENERATE_ADV_TAX_INVOICE = 0
                'If bReversePrevInvoice Then
                '    RetVal = SAVE_ADV_TAX_REVERSAL(RetMessage, conn, trans, sqlp15.Value)
                '    If RetVal = 0 Then
                '        trans.Commit()
                '        GENERATE_ADV_TAX_INVOICE = 0
                '        NEW_INV_NO = sqlp15.Value
                '    Else
                '        trans.Rollback()
                '        GENERATE_ADV_TAX_INVOICE = RetVal
                '        NEW_INV_NO = ""
                '    End If
                'End If
            End If
        Catch ex As Exception
            RetMessage = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Public Shared Function GET_STU_ID_PADDED(ByVal BSU_ID As String, ByVal STU_NOs As String) As String
        GET_STU_ID_PADDED = ""
        Try
            GET_STU_ID_PADDED = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(dbo.fn_GET_STUDENT_ID_PADDED('" & BSU_ID & "','" & STU_NOs & "'),'') AS STU_ID")
        Catch ex As Exception

        End Try
    End Function
    Public Function GET_STU_LEDGER() As DataTable
        GET_STU_LEDGER = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC FEES.F_GEN_RPT_FEE_STUDENT_LEDGER @BSU_ID = @BSU_ID,@STU_TYPE = @STU_TYPE, @STU_ID = @STU_ID, ")
            Qry.Append("@FEE_ID = @FEE_ID,@FROMDT = @FROMDT,@TODT = @TODT ")

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = STU_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
            pParms(1).Value = STU_TYPE
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.Int)
            pParms(2).Value = STU_ID
            pParms(3) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
            pParms(3).Value = FEE_ID
            pParms(4) = New SqlClient.SqlParameter("@FROMDT", SqlDbType.Date)
            pParms(4).Value = FROM_DATE
            pParms(5) = New SqlClient.SqlParameter("@TODT", SqlDbType.Date)
            pParms(5).Value = TO_DATE

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_STU_LEDGER = ds.Tables(0)
            End If
        Catch ex As Exception
            GET_STU_LEDGER = Nothing
        End Try
    End Function
#End Region
End Class
