Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports Newtonsoft.Json.Linq

Public Class FeeCollection

#Region "Public Variables"

    Private pVOUCHER_NO As String
    Public Property VOUCHER_NO() As String
        Get
            Return pVOUCHER_NO
        End Get
        Set(ByVal value As String)
            pVOUCHER_NO = value
        End Set
    End Property

    Private pBSU_SHORT As String
    Public Property BSU_SHORT() As String
        Get
            Return pBSU_SHORT
        End Get
        Set(ByVal value As String)
            pBSU_SHORT = value
        End Set
    End Property

    Private pFCL_RECNO As String
    Public Property FCL_RECNO() As String
        Get
            Return pFCL_RECNO
        End Get
        Set(ByVal value As String)
            pFCL_RECNO = value
        End Set
    End Property

#End Region
    Public Shared Function GetFEES_M_Colection(ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim sql_query As String = "SELECT DISTINCT FEES.FEES_M.FEE_ID, FEES.FEES_M.FEE_DESCR, FEES.FEES_M.FEE_ORDER " _
        & " FROM FEES.FEES_M INNER JOIN FEES.FEESETUP_S ON FEE_ID=FSP_FEE_ID AND " _
        & " FSP_ACD_ID='" & ACD_ID & "' where  FEE_ID NOT IN (  SELECT FEES.FEES_M.FEE_ID FROM " _
        & " FEES.FEES_M INNER JOIN oasis..SERVICES_BSU_M AS SVB ON FEES.FEES_M.FEE_SVC_ID = SVB.SVB_SVC_ID " _
        & " AND SVB.SVB_PROVIDER_BSU_ID<> SVB.SVB_BSU_ID AND SVB.SVB_BSU_ID= '" & BSU_ID & "'  " _
        & " AND SVB_ACD_ID= '" & ACD_ID & "' ) ORDER BY FEES.FEES_M.FEE_ORDER  "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_GetFeeDetailsFOrCollection(ByVal p_DOCDT As String, ByVal p_STU_ID As String, _
    ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal ACD_ID As String, Optional ByVal NEXTACAYEARCHECKED As Boolean = False) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@NEXTACDYEAR", SqlDbType.Bit)
        pParms(5).Value = NEXTACAYEARCHECKED
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeDetailsFOrCollection", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_GetFeeTypes(ByVal BSU_ID As String, Optional ByVal ACD_ID As Integer = 0) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@SVB_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[FEE_GETFEESFORCOLLECTION]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_SaveFEECOLLSUB_Normal(ByVal p_FCS_ID As Integer, ByVal p_FCS_FCL_ID As Integer, _
ByVal p_FCS_FEE_ID As Integer, ByVal p_FCS_AMOUNT As Decimal, ByVal p_FCS_FSH_ID As String, _
ByVal p_FCS_ORG_AMOUNT As String, ByVal p_FCS_DISCOUNT As Decimal, ByVal p_FCS_CURRENCY_AMOUNT As Decimal, _
ByVal p_stTrans As SqlTransaction, Optional ByVal FeeAutoChg As Boolean = False, _
 Optional ByVal p_FCS_TAX_AMOUNT As Double = 0, Optional ByVal p_FCS_PAY_TYPE_IDs As String = "", Optional ByVal p_FCS_PAY_TYPE As String = "") As String
        Dim pParms(12) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCS_ID", SqlDbType.Int)
        pParms(0).Value = p_FCS_ID
        pParms(1) = New SqlClient.SqlParameter("@FCS_FCL_ID", SqlDbType.Int)
        pParms(1).Value = p_FCS_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCS_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FCS_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FCS_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCS_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@FCS_FSH_ID", SqlDbType.BigInt)
        pParms(5).Value = p_FCS_FSH_ID
        pParms(6) = New SqlClient.SqlParameter("@FCS_ORG_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCS_ORG_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FeeAutoChg", SqlDbType.Bit)
        pParms(7).Value = FeeAutoChg
        pParms(8) = New SqlClient.SqlParameter("@FCS_DISCOUNT", SqlDbType.Decimal, 21)
        pParms(8).Value = p_FCS_DISCOUNT
        pParms(9) = New SqlClient.SqlParameter("@FCS_CURRENCY_AMOUNT", SqlDbType.Decimal, 21)
        pParms(9).Value = p_FCS_CURRENCY_AMOUNT
        pParms(10) = New SqlClient.SqlParameter("@FCS_TAX_AMOUNT", SqlDbType.Decimal, 21)
        pParms(10).Value = p_FCS_TAX_AMOUNT
        pParms(11) = New SqlClient.SqlParameter("@FCS_PAY_TYPE_IDs", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCS_PAY_TYPE_IDs
        pParms(12) = New SqlClient.SqlParameter("@FCS_PAY_TYPE", SqlDbType.VarChar, 50)
        pParms(12).Value = p_FCS_PAY_TYPE
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB", pParms)
        F_SaveFEECOLLSUB_Normal = pParms(4).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_D_Normal(ByVal p_FCD_ID As Integer, ByVal p_FCD_FCL_ID As String, _
        ByVal p_FCD_CLT_ID As Integer, ByVal p_FCD_AMOUNT As String, ByVal p_FCD_CURRENCY_AMOUNT As String, ByVal p_FCD_REFNO As String, _
        ByVal p_FCD_DATE As String, ByVal p_FCD_STATUS As String, ByVal p_FCD_VHH_DOCNO As String, _
        ByVal p_FCD_REF_ID As String, ByVal p_FCD_EMR_ID As String, ByVal p_stTrans As SqlTransaction, _
        Optional ByVal p_FCD_UNIQCHQ_ID As String = "", Optional ByVal p_FCD_CHARGE_CLIENT As Double = 0, _
        Optional ByVal p_FCD_BANK_ACT_ID As String = "", Optional ByVal p_FCD_VTM_ID As Integer = 0, _
        Optional ByVal p_FCD_VOUCHER_REF_NO As String = "", Optional ByVal p_FCD_CARD_HOLDER_NAME As String = "", _
        Optional ByVal p_FCD_ISSUE_DATE As String = "", Optional ByVal p_FCD_EXPIRY_DATE As String = "") As String
        Dim pParms(19) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCD_ID", SqlDbType.Int)
        pParms(0).Value = p_FCD_ID
        pParms(1) = New SqlClient.SqlParameter("@FCD_FCL_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FCD_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCD_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FCD_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCD_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FCD_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FCD_REFNO
        pParms(5) = New SqlClient.SqlParameter("@FCD_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FCD_DATE
        pParms(6) = New SqlClient.SqlParameter("@FCD_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_FCD_STATUS
        pParms(7) = New SqlClient.SqlParameter("@FCD_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCD_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@FCD_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_FCD_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@FCD_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FCD_EMR_ID
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        pParms(11) = New SqlClient.SqlParameter("@FCD_UNIQCHQ_ID", SqlDbType.VarChar)
        pParms(11).Value = p_FCD_UNIQCHQ_ID
        pParms(12) = New SqlClient.SqlParameter("@FCD_CURRENCY_AMOUNT", SqlDbType.Decimal)
        pParms(12).Value = p_FCD_CURRENCY_AMOUNT
        pParms(13) = New SqlClient.SqlParameter("@FCD_CHARGE_CLIENT", SqlDbType.Decimal)
        pParms(13).Value = p_FCD_CHARGE_CLIENT
        pParms(14) = New SqlClient.SqlParameter("@FCD_BANK_ACT_ID", SqlDbType.VarChar, 20)
        pParms(14).Value = p_FCD_BANK_ACT_ID

        pParms(15) = New SqlClient.SqlParameter("@FCD_VTM_ID", SqlDbType.Int)
        pParms(15).Value = p_FCD_VTM_ID
        pParms(16) = New SqlClient.SqlParameter("@FCD_VOUCHER_REF_NO", SqlDbType.VarChar, 20)
        pParms(16).Value = p_FCD_VOUCHER_REF_NO
        pParms(17) = New SqlClient.SqlParameter("@FCD_CARD_HOLDER_NAME", SqlDbType.VarChar, 100)
        pParms(17).Value = p_FCD_CARD_HOLDER_NAME
        pParms(18) = New SqlClient.SqlParameter("@FCD_ISSUE_DATE", SqlDbType.DateTime)
        pParms(18).Value = IIf(p_FCD_ISSUE_DATE = "", DBNull.Value, p_FCD_ISSUE_DATE)
        pParms(19) = New SqlClient.SqlParameter("@FCD_EXPIRY_DATE", SqlDbType.DateTime)
        pParms(19).Value = IIf(p_FCD_EXPIRY_DATE = "", DBNull.Value, p_FCD_EXPIRY_DATE)

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D", pParms)
        F_SaveFEECOLLSUB_D_Normal = pParms(10).Value
    End Function

    Public Shared Function GETFEEPAYABLE(ByVal p_ACD_ID As Integer, ByVal p_BSU_ID As String, _
    ByVal p_Term As String, ByVal p_SBL_ID As Integer, ByVal p_FSM_Collection_SCH_ID As Integer, ByVal p_str_conn As String) As Decimal
        Dim pParms(6) As SqlClient.SqlParameter
        'EXEC     @return_value = [TRANSPORT].[GETFEEPAYABLE]
        '@ACD_ID = 79,    
        '@BSU_ID = N'125016',
        '@Term = N'41|42',
        '@SBL_ID = 1
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@Term", SqlDbType.VarChar, 200)
        pParms(2).Value = p_Term
        pParms(3) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.Int)
        pParms(3).Value = p_SBL_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@FSM_Collection_SCH_ID", SqlDbType.Int)
        pParms(5).Value = p_FSM_Collection_SCH_ID
        Dim retval
        retval = SqlHelper.ExecuteScalar(p_str_conn, CommandType.StoredProcedure, "TRANSPORT.GETFEEPAYABLE", pParms)
        If retval Is DBNull.Value Then
            GETFEEPAYABLE = 0
        Else
            GETFEEPAYABLE = CDbl(retval)
        End If
    End Function

    Public Shared Function F_SaveFEECOLLALLOCATION_D_Normal(ByVal p_BSU_ID As String, ByVal p_FCL_ID As String, _
    ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        '     EXEC      @return_value = [FEES].[F_SaveFEECOLLALLOCATION_D] 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
        pParms(2).Value = p_FCL_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLALLOCATION_D", pParms)
        F_SaveFEECOLLALLOCATION_D_Normal = pParms(1).Value
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_Normal(ByVal p_FCL_ID As Long, ByVal p_FCL_SOURCE As String, _
        ByVal p_FCL_DATE As DateTime, ByVal p_FCL_RECNO As String, ByVal p_FCL_ACD_ID As Integer, ByVal p_FCL_STU_ID As Integer, _
        ByVal p_FCL_AMOUNT As Decimal, ByVal p_FCL_EMP_ID As String, ByVal p_FCL_Bposted As Boolean, _
        ByRef p_newFCL_ID As Long, ByVal p_FCL_BSU_ID As String, ByVal p_FCL_NARRATION As String, _
        ByVal p_FCL_DRCR As String, ByRef p_NEW_FCL_RECNO As String, ByVal p_FCL_OUTSTANDING As String, _
        ByVal p_FCL_STU_BSU_ID As String, ByVal p_FCL_STU_TYPE As String, ByVal p_FCL_BALANCE As String, _
        ByVal p_FCL_CURRENCY As String, ByVal p_FCL_EXCHANGE1 As Decimal, _
        ByVal p_stTrans As SqlTransaction, Optional ByVal NACDPYMT As Boolean = False, Optional ByVal p_FCL_PAYMENT_BSU_ID As String = "") As String
        Dim pParms(23) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCL_ID
        pParms(1) = New SqlClient.SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCL_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCL_RECNO
        pParms(3) = New SqlClient.SqlParameter("@FCL_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCL_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCL_ACD_ID", SqlDbType.VarChar, 100)
        pParms(4).Value = p_FCL_ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@FCL_STU_ID", SqlDbType.BigInt)
        pParms(5).Value = p_FCL_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCL_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCL_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCL_EMP_ID", SqlDbType.VarChar, 100)
        pParms(7).Value = p_FCL_EMP_ID
        pParms(8) = New SqlClient.SqlParameter("@FCL_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCL_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCL_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCL_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCL_NARRATION", SqlDbType.VarChar, 500)
        pParms(12).Value = p_FCL_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCL_DRCR", SqlDbType.VarChar, 5)
        pParms(13).Value = p_FCL_DRCR
        pParms(14) = New SqlClient.SqlParameter("@NEW_FCL_RECNO", SqlDbType.VarChar, 30)
        pParms(14).Direction = ParameterDirection.Output
        pParms(15) = New SqlClient.SqlParameter("@FCL_OUTSTANDING", SqlDbType.Decimal)
        pParms(15).Value = p_FCL_OUTSTANDING
        pParms(16) = New SqlClient.SqlParameter("@FCL_STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(16).Value = p_FCL_STU_BSU_ID
        pParms(17) = New SqlClient.SqlParameter("@FCL_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(17).Value = p_FCL_STU_TYPE
        pParms(18) = New SqlClient.SqlParameter("@FCL_BALANCE", SqlDbType.Decimal, 21)
        pParms(18).Value = p_FCL_BALANCE
        pParms(19) = New SqlClient.SqlParameter("@FCL_CUR_ID", SqlDbType.VarChar, 21)
        pParms(19).Value = p_FCL_CURRENCY
        pParms(20) = New SqlClient.SqlParameter("@FCL_EXCHANGE1", SqlDbType.Decimal, 21)
        pParms(20).Value = p_FCL_EXCHANGE1
        pParms(21) = New SqlClient.SqlParameter("@FCL_NEXTACDPAYMENT", SqlDbType.Bit)
        pParms(21).Value = NACDPYMT
        pParms(22) = New SqlClient.SqlParameter("@FCL_PAYMENT_BSU_ID", SqlDbType.VarChar, 20)
        pParms(22).Value = p_FCL_PAYMENT_BSU_ID


        Dim cmdCollection As New SqlCommand
        cmdCollection.CommandType = CommandType.StoredProcedure
        cmdCollection.CommandText = "FEES.F_SaveFEECOLLECTION_H"
        cmdCollection.Transaction = p_stTrans
        cmdCollection.Connection = p_stTrans.Connection
        'cmdCollection.CommandTimeout = 0

        cmdCollection.Parameters.Add(pParms(0))
        cmdCollection.Parameters.Add(pParms(1))
        cmdCollection.Parameters.Add(pParms(2))
        cmdCollection.Parameters.Add(pParms(3))
        cmdCollection.Parameters.Add(pParms(4))
        cmdCollection.Parameters.Add(pParms(5))
        cmdCollection.Parameters.Add(pParms(6))
        cmdCollection.Parameters.Add(pParms(7))
        cmdCollection.Parameters.Add(pParms(8))
        cmdCollection.Parameters.Add(pParms(9))
        cmdCollection.Parameters.Add(pParms(10))
        cmdCollection.Parameters.Add(pParms(11))
        cmdCollection.Parameters.Add(pParms(12))
        cmdCollection.Parameters.Add(pParms(13))
        cmdCollection.Parameters.Add(pParms(14))
        cmdCollection.Parameters.Add(pParms(15))
        cmdCollection.Parameters.Add(pParms(16))
        cmdCollection.Parameters.Add(pParms(17))
        cmdCollection.Parameters.Add(pParms(18))
        cmdCollection.Parameters.Add(pParms(19))
        cmdCollection.Parameters.Add(pParms(20))
        cmdCollection.Parameters.Add(pParms(21))
        cmdCollection.Parameters.Add(pParms(22))
        Dim retval As Integer
        retval = cmdCollection.ExecuteNonQuery
        ' SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H", pParms)
        If pParms(9).Value = 0 Then
            p_newFCL_ID = pParms(10).Value
            p_NEW_FCL_RECNO = pParms(14).Value
        End If
        F_SaveFEECOLLECTION_H_Normal = pParms(9).Value
    End Function

    Public Shared Function F_SettleFee_Normal(ByVal p_BSU_ID As String, _
        ByVal p_Dt As String, ByVal p_STU_ID As String, ByVal p_isEnquiry As Boolean, _
        ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(4) As SqlClient.SqlParameter
        'procedure [FEES].[F_SettleFee](          
        '@BSU_ID varchar(20)='125016', 
        '@STU_ID bigint=93690,@Dt datetime)
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_STU_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@bEnquiry", SqlDbType.Bit)
        pParms(4).Value = p_isEnquiry
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SettleFee", pParms)
        F_SettleFee_Normal = pParms(3).Value
    End Function

    Public Shared Function F_SAVEFEEDISCOUNTTOADJUSTMENT_Normal(ByVal p_FCL_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCL_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SAVEFEEDISCOUNTTOADJUSTMENT", pParms)
        F_SAVEFEEDISCOUNTTOADJUSTMENT_Normal = pParms(1).Value
    End Function

    Public Shared Function F_GetFeeNarration(ByVal p_DOCDT As String, _
    ByVal p_ACD_ID As String, ByVal p_BSU_ID As String) As String
        '[FEES].[F_GetFeeNarration] 
        '@DOCDT varchar(12),
        '@BSU_ID varchar(20),
        '@ACD_ID bigint  
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function CreateFeeCollectionTransport() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cID As New DataColumn("ID", System.Type.GetType("System.Decimal"))
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.String"))
            Dim cFEE_DESCR As New DataColumn("FEE_DESCR", System.Type.GetType("System.String"))
            Dim cDue As New DataColumn("Due", System.Type.GetType("System.Decimal"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            'FEE_ID, FEE_DESCR, Opening, CurrentCharge, Net
            dtDt.Columns.Add(cID)
            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cFEE_DESCR)
            dtDt.Columns.Add(cDue)
            dtDt.Columns.Add(cAmount)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function F_SAVECHEQUEDATA_OASIS(ByVal p_BSU_ID As String, ByVal p_FCL_IDs As String, _
        ByVal p_FCL_EMP_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '   EXEC [FEES].[F_SAVECHEQUEDATA]
        '@BSU_ID  ='900500' ,
        '@FCL_IDS  ='1|2|3|345|4456777',
        '@STU_BSU_ID   ='125016'
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@FCL_IDs", SqlDbType.VarChar, 2000)
        pParms(2).Value = p_FCL_IDs
        pParms(3) = New SqlClient.SqlParameter("@FCL_EMP_ID", SqlDbType.VarChar, 50)
        pParms(3).Value = p_FCL_EMP_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SAVECHEQUEDATA_OASIS", pParms)
        F_SAVECHEQUEDATA_OASIS = pParms(1).Value
    End Function

    Public Shared Function GetPreviousACD(ByVal p_ACD_ID As Integer, ByVal p_BSU_ID As String, _
    ByVal p_str_conn As String) As String
        Dim pParms(6) As SqlClient.SqlParameter
        'EXEC     @return_value = [fees.GetPreviousACD
        '@ACD_ID = 79,    
        '@BSU_ID = N'125016', 
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        Dim retval
        retval = SqlHelper.ExecuteScalar(p_str_conn, CommandType.StoredProcedure, "fees.GetPreviousACD", pParms)
        If retval Is DBNull.Value Then
            Return String.Empty
        Else
            Return ""
        End If
    End Function

    Public Shared Sub SaveAUDIT_PRINTRECEIPT(ByVal p_APR_SOURCE As String, ByVal p_APR_RECNO As String, _
    ByVal p_APR_BSU_ID As String, ByVal p_APR_EMP_ID As String)
        Dim pParms(21) As SqlClient.SqlParameter
        '[SaveAUDIT_PRINTRECEIPT]
        '@APR_SOURCE = N'FEES',
        '@APR_RECNO = N'123',
        '@APR_BSU_ID = N'125016',
        '@APR_EMP_ID = N'GURU',
        '@APR_STU_BSU_ID = NULL
        pParms(0) = New SqlClient.SqlParameter("@APR_SOURCE", SqlDbType.VarChar, 15)
        pParms(0).Value = p_APR_SOURCE
        pParms(1) = New SqlClient.SqlParameter("@APR_RECNO", SqlDbType.VarChar, 20)
        pParms(1).Value = p_APR_RECNO
        pParms(2) = New SqlClient.SqlParameter("@APR_BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_APR_BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@APR_EMP_ID", SqlDbType.VarChar, 100)
        pParms(3).Value = p_APR_EMP_ID
        pParms(4) = New SqlClient.SqlParameter("@APR_STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = System.DBNull.Value
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, _
        CommandType.StoredProcedure, "FEES.SaveAUDIT_PRINTRECEIPT", pParms)
    End Sub

    Public Shared Function PrintReceipt(ByVal p_Receiptno As String, ByVal BSU_ID As String, _
    ByVal IsEnquiry As Boolean, ByVal USER_NAME As String, ByVal IsCollection As Boolean) As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmdFeeReceiptHead As New SqlCommand("FEES.GetFeeReceipt", New SqlConnection(str_conn))
        cmdFeeReceiptHead.Parameters.AddWithValue("@FCL_BSU_ID", BSU_ID)
        cmdFeeReceiptHead.Parameters.AddWithValue("@FCL_RECNO", p_Receiptno)
        cmdFeeReceiptHead.Parameters.AddWithValue("@bENQUIRY", IsEnquiry)
        cmdFeeReceiptHead.Parameters.AddWithValue("@bCOLLECTION", IsCollection)
        cmdFeeReceiptHead.CommandType = CommandType.StoredProcedure
        Dim strFilter As String = "  FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & BSU_ID & "' "
        Dim repSource As New MyReportClass
        cmdFeeReceiptHead.Connection = New SqlConnection(str_conn)
        Dim params As New Hashtable
        If IsEnquiry Then
            params("STU_TYPE") = "Enquiry ID"
        Else
            params("STU_TYPE") = "Student ID"
        End If
        'RcptCnt
        params("UserName") = USER_NAME
        repSource.Parameter = params
        repSource.Command = cmdFeeReceiptHead
        repSource.IncludeBSUImage = True

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        Dim cmdSubColln As New SqlCommand

        ''SUBREPORT1
        cmdSubColln.CommandText = "select * FROM FEES.VW_OSO_FEES_FEECOLLECTED where " & strFilter & " ORDER BY FEE_ORDER "
        cmdSubColln.Connection = New SqlConnection(str_conn)
        cmdSubColln.CommandType = CommandType.Text
        repSourceSubRep(0).Command = cmdSubColln

        ''''SUBREPORT2
        Dim cmdSubPayments As New SqlCommand
        cmdSubPayments.CommandText = "select * FROM   FEES.VW_OSO_FEES_PAYMENTS where " & strFilter
        cmdSubPayments.Connection = New SqlConnection(str_conn)
        cmdSubPayments.CommandType = CommandType.Text
        repSourceSubRep(1).Command = cmdSubPayments

        repSource.SubReport = repSourceSubRep
        repSource.ResourceName = "../../fees/Reports/RPT/rptFeeCollectionReceipt.rpt"
        Return repSource
    End Function

    Public Shared Function DIS_GetDiscount(ByVal p_FDS_ID As String, ByVal p_STU_ID As String, _
    ByVal p_STU_TYPE As String, ByVal p_DT As String) As DataTable
        '@FDS_ID int,@STU_ID bigint,@STU_TYPE  varchar(2), @DT datetime
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FDS_ID", p_FDS_ID)
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", p_STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", p_STU_TYPE)
        pParms(3) = New SqlClient.SqlParameter("@DT", p_DT)
        pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.StoredProcedure, "FEES.DIS_GetDiscount", pParms)
        Return ds.Tables(0)
    End Function

    Public Shared Function DIS_FindDiscount(ByVal p_STU_ID As String, ByVal p_STU_TYPE As String, _
        ByVal p_DT As String, ByVal p_BSU_ID As String, ByVal p_STM_ID As String, _
        ByVal p_SelectedAmt As Decimal, ByRef p_DiscountAmt As Decimal) As String
        Dim pParms(14) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = p_STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(1).Value = p_STU_TYPE
        pParms(2) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(2).Value = p_DT
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@STM_ID", SqlDbType.BigInt)
        If p_STM_ID = "" Then
            pParms(4).Value = System.DBNull.Value
        Else
            pParms(4).Value = p_STM_ID
        End If
        pParms(5) = New SqlClient.SqlParameter("@SelectedAmt", SqlDbType.Decimal, 21)
        pParms(5).Value = p_SelectedAmt
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@DiscountAmt", SqlDbType.Decimal, 21)
        pParms(7).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.DIS_FindDiscount", pParms)
        If pParms(6).Value = 0 Then
            If Not pParms(7).Value Is Nothing And pParms(7).Value Is System.DBNull.Value Then
                p_DiscountAmt = 0
            Else
                p_DiscountAmt = pParms(7).Value
            End If
        End If
        DIS_FindDiscount = pParms(6).Value
    End Function

    Public Shared Function F_SAVEFEE_DISCOUNT_DETAIL(ByVal p_FCL_ID As String, ByVal p_DISC_XML As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@DISC_XML", SqlDbType.VarChar)
        pParms(2).Value = p_DISC_XML
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.SAVE_DISCOUNT_DETAIL", pParms)
        F_SAVEFEE_DISCOUNT_DETAIL = pParms(1).Value
    End Function

    Public Function VALIDATE_VOUCHER_NUMBER() As String
        VALIDATE_VOUCHER_NUMBER = ""
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            Dim trans As SqlTransaction
            conn.Open()
            trans = conn.BeginTransaction("VOUCHER")
            Dim cmd As SqlCommand
            Dim RetVal As Integer = 0

            cmd = New SqlCommand("FEES.VALIDATE_REDEMPTION_VOUCHER", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@VOUCHER_NO", SqlDbType.VarChar, 20)
            sqlp1.Value = VOUCHER_NO
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@BSU_SHORT", SqlDbType.VarChar, 5)
            sqlp2.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            sqlp3.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlp3)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            RetVal = retSValParam.Value

            If RetVal <> 0 Then
                trans.Rollback()
                VALIDATE_VOUCHER_NUMBER = UtilityObj.getErrorMessage(RetVal).Replace("&&&&", sqlp2.Value).Replace("####", sqlp3.Value)
            Else
                trans.Commit()
                VALIDATE_VOUCHER_NUMBER = ""
            End If
        Catch ex As Exception
            VALIDATE_VOUCHER_NUMBER = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Public Shared Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Public Shared Function ValidateValue(ByVal parameter As JProperty) As String
        ValidateValue = ""
        Try
            Dim msgProperty = parameter
            If msgProperty IsNot Nothing Then
                ValidateValue = msgProperty.Value.ToString
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Shared Sub SAVE_EMAIL_LOG(ByVal BSU_ID As String, ByVal USERNAME As String, ByVal Receipt As String, ByVal FCL_ID As Integer, ByVal RetMessage As String)
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & BSU_ID & "','" & USERNAME & "','" & Receipt & "','" & FCL_ID & "','" & RetMessage & "' ")
    End Sub

    Public Shared Function IsBankTranferOrTT(ByVal BNK_ID As Integer) As Boolean
        IsBankTranferOrTT = False
        Try
            Dim bEXISTS As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT COUNT(BNK_ID) AS bTT FROM dbo.BANK_M WITH(NOLOCK) WHERE BNK_ID='" & BNK_ID & "' AND ISNULL(BNK_TYPE,'')='BANKTRF'")
            If bEXISTS > 0 Then
                IsBankTranferOrTT = True
            End If
        Catch ex As Exception

        End Try
    End Function

End Class