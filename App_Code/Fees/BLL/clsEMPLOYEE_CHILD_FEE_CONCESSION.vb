﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class clsEMPLOYEE_CHILD_FEE_CONCESSION

#Region "Public Variables"

    Private pSCR_ID As Integer
    Public Property SCR_ID() As Integer
        Get
            Return pSCR_ID
        End Get
        Set(ByVal value As Integer)
            pSCR_ID = value
        End Set
    End Property

    Private pUser As String
    Public Property User() As String
        Get
            Return pUser
        End Get
        Set(ByVal value As String)
            pUser = value
        End Set
    End Property

    Private pPROCESS As String
    Public Property PROCESS() As String
        Get
            Return pPROCESS
        End Get
        Set(ByVal value As String)
            pPROCESS = value
        End Set
    End Property

    Private pSCD_ID As Integer
    Public Property SCD_ID() As Integer
        Get
            Return pSCD_ID
        End Get
        Set(ByVal value As Integer)
            pSCD_ID = value
        End Set
    End Property

    Private pSCR_SCD_IDs As String
    Public Property SCR_SCD_IDs() As String
        Get
            Return pSCR_SCD_IDs
        End Get
        Set(ByVal value As String)
            pSCR_SCD_IDs = value
        End Set
    End Property

    Private pSCD_APPR_REMARKS As String
    Public Property SCD_APPR_REMARKS() As String
        Get
            Return pSCD_APPR_REMARKS
        End Get
        Set(ByVal value As String)
            pSCD_APPR_REMARKS = value
        End Set
    End Property

    Private pbTRANSPORT As Boolean
    Public Property bTRANSPORT() As Boolean
        Get
            Return pbTRANSPORT
        End Get
        Set(ByVal value As Boolean)
            pbTRANSPORT = value
        End Set
    End Property

    Private pSCD_CONCESSION_TODT As String
    Public Property SCD_CONCESSION_TODT() As String
        Get
            Return pSCD_CONCESSION_TODT
        End Get
        Set(ByVal value As String)
            pSCD_CONCESSION_TODT = value
        End Set
    End Property

    Private pSCD_CONCESSION_FROMDT As String
    Public Property SCD_CONCESSION_FROMDT() As String
        Get
            Return pSCD_CONCESSION_FROMDT
        End Get
        Set(ByVal value As String)
            pSCD_CONCESSION_FROMDT = value
        End Set
    End Property
#End Region

    Public Function Approve_Employee_Child_ConcessionRequest(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As String
        Approve_Employee_Child_ConcessionRequest = "0"
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.APPROVE_STUDENT_CONCESSION_REQUEST", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim pParms(7) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@SCD_IDs", SqlDbType.VarChar)
            pParms(0).Value = SCR_SCD_IDs
            cmd.Parameters.Add(pParms(0))

            pParms(1) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
            pParms(1).Value = User
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@SCD_APPR_STATUS", SqlDbType.VarChar)
            pParms(2).Value = PROCESS
            cmd.Parameters.Add(pParms(2))

            pParms(3) = New SqlClient.SqlParameter("@SCD_APPR_REMARKS", SqlDbType.VarChar)
            pParms(3).Value = SCD_APPR_REMARKS
            cmd.Parameters.Add(pParms(3))

            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(pParms(4))

            pParms(5) = New SqlClient.SqlParameter("@bTRANSPORT", SqlDbType.Bit)
            pParms(5).Value = bTRANSPORT
            cmd.Parameters.Add(pParms(5))

            If bTRANSPORT Then
                pParms(6) = New SqlClient.SqlParameter("@SCD_CONCESSION_FROMDT", SqlDbType.DateTime)
                pParms(6).Value = SCD_CONCESSION_FROMDT
                cmd.Parameters.Add(pParms(6))

                pParms(7) = New SqlClient.SqlParameter("@SCD_CONCESSION_TODT", SqlDbType.DateTime)
                pParms(7).Value = SCD_CONCESSION_TODT
                cmd.Parameters.Add(pParms(7))
            End If

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            Approve_Employee_Child_ConcessionRequest = pParms(4).Value

        Catch ex As Exception
            Approve_Employee_Child_ConcessionRequest = "1"
            'TRANS.Rollback()
        End Try
    End Function

    'Public Function Approve_Employee_Child_ConcessionRequest_Individual(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As String
    '    Approve_Employee_Child_ConcessionRequest_Individual = "0"
    '    Try
    '        Dim cmd As SqlCommand

    '        cmd = New SqlCommand("FEES.APPROVE_STUDENT_CONCESSION_REQUEST", objCon, TRANS)
    '        cmd.CommandType = CommandType.StoredProcedure

    '        Dim pParms(7) As SqlClient.SqlParameter

    '        pParms(0) = New SqlClient.SqlParameter("@SCD_ID", SqlDbType.Int)
    '        pParms(0).Value = SCD_ID
    '        cmd.Parameters.Add(pParms(0))

    '        pParms(1) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
    '        pParms(1).Value = User
    '        cmd.Parameters.Add(pParms(1))

    '        pParms(2) = New SqlClient.SqlParameter("@SCD_APPR_STATUS", SqlDbType.VarChar)
    '        pParms(2).Value = PROCESS
    '        cmd.Parameters.Add(pParms(2))

    '        pParms(3) = New SqlClient.SqlParameter("@SCD_APPR_REMARKS", SqlDbType.VarChar)
    '        pParms(3).Value = SCD_APPR_REMARKS
    '        cmd.Parameters.Add(pParms(3))

    '        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
    '        pParms(4).Direction = ParameterDirection.ReturnValue
    '        cmd.Parameters.Add(pParms(4))

    '        pParms(5) = New SqlClient.SqlParameter("@bTRANSPORT", SqlDbType.Bit)
    '        pParms(5).Value = bTRANSPORT
    '        cmd.Parameters.Add(pParms(5))

    '        pParms(6) = New SqlClient.SqlParameter("@SCD_CONCESSION_FROMDT", SqlDbType.DateTime)
    '        pParms(6).Value = SCD_CONCESSION_FROMDT
    '        cmd.Parameters.Add(pParms(6))

    '        pParms(7) = New SqlClient.SqlParameter("@SCD_CONCESSION_TODT", SqlDbType.DateTime)
    '        pParms(7).Value = SCD_CONCESSION_TODT
    '        cmd.Parameters.Add(pParms(7))

    '        cmd.CommandTimeout = 0
    '        cmd.ExecuteNonQuery()

    '        Approve_Employee_Child_ConcessionRequest_Individual = pParms(4).Value

    '    Catch ex As Exception
    '        Approve_Employee_Child_ConcessionRequest_Individual = "1"
    '        'TRANS.Rollback()
    '    End Try
    'End Function

    Public Function VALIDATE_CONCESSION_REQUEST(ByVal SCDID As Integer, ByRef RET_MESSAGE As String) As Boolean
        VALIDATE_CONCESSION_REQUEST = False
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("FEES.VALIDATE_TRANSPORT_CONCESSION_REQUEST", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SCD_ID", SqlDbType.Int)
            pParms(0).Value = SCDID
            cmd.Parameters.Add(pParms(0))

            pParms(1) = New SqlClient.SqlParameter("@RET_MESSAGE", SqlDbType.VarChar, 500)
            pParms(1).Direction = ParameterDirection.Output
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(pParms(2))

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            If pParms(2).Value = 0 Then
                VALIDATE_CONCESSION_REQUEST = True
            End If
            RET_MESSAGE = pParms(1).Value
        Catch ex As Exception
            RET_MESSAGE = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Sub SendPaymentConfirmationSMS(ByVal DOM_ID As Integer)
        Dim Response As String
        Try
            Dim sms As New SmsService.sms
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_DONATIONConnectionString").ConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@DOM_ID", 0)
            Dim DS As New DataSet
            Dim dt As New DataTable
            DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "DON.GET_PAYMENT_CONFIRMATION_SMS", param)
            If DS.Tables(0).Rows.Count > 0 Then
                dt = DS.Tables(0)
                If dt.Rows.Count > 0 Then
                    Dim username As String = System.Configuration.ConfigurationManager.AppSettings("smsUsernameforFees").ToString()
                    Dim password As String = System.Configuration.ConfigurationManager.AppSettings("smspwdforFees").ToString()

                    If DS.Tables(0).Rows.Count > 0 Then
                        Dim Message = dt.Rows(0).Item("LOG_MESSAGE").ToString()
                        Dim mobilenumber = dt.Rows(0).Item("MOBILE_NO").ToString()
                        Dim From = "VGF"
                        Response = sms.SendMessage(mobilenumber, Message, From, username, password)
                    End If
                End If
            End If

        Catch ex As Exception

        Finally

        End Try
    End Sub
End Class
