Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class FeeCollectionOnline


    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE(ByVal p_FCO_ID As Integer, ByVal p_FCO_SOURCE As String, _
        ByVal p_FCO_DATE As DateTime, ByVal p_FCO_ACD_ID As Integer, ByVal p_FCO_STU_ID As String, ByVal p_FCO_STU_TYPE As String, _
        ByVal p_FCO_AMOUNT As Decimal, ByVal p_FCO_Bposted As Boolean, ByRef p_newFCO_ID As Integer, _
        ByVal p_FCO_BSU_ID As String, ByVal p_FCO_NARRATION As String, ByVal p_FCO_DRCR As String, _
        ByVal p_FCO_REFNO As String, ByVal p_FCO_IP_ADDRESS As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(14) As SqlClient.SqlParameter
        'FEES.F_SaveFEECOLLECTION_H_ONLINE  
        ',@FCO_ID  VARCHAR(20)
        '@FCO_SOURCE VARCHAR(20)
        ',@FCO_RECNO VARCHAR(20)
        ',@FCO_DATE DATETIME 
        ',@FCO_ACD_ID BIGINT  
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCO_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCO_DRCR", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCO_DRCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCO_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCO_ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCO_ACD_ID
        ',@FCO_STU_ID BIGINT 
        ',@FCO_AMOUNT NUMERIC(18,3) 
        ',@FCO_Bposted BIT 
        pParms(5) = New SqlClient.SqlParameter("@FCO_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCO_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCO_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_REFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCO_REFNO
        pParms(8) = New SqlClient.SqlParameter("@FCO_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCO_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        ',@FCO_NARRATION VARCHAR(20)
        ',@FCO_DRCR VARCHAR(20)  
        ',@FCO_BSU_ID VARCHAR(20) 
        ',@NEW_FCO_ID INT OUTPUT
        ',@FCO_REFNO VARCHAR(20)
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCO_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCO_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCO_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FCO_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCO_IP_ADDRESS", SqlDbType.VarChar, 100)
        pParms(13).Value = p_FCO_IP_ADDRESS
        pParms(14) = New SqlClient.SqlParameter("@FCO_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FCO_STU_TYPE


        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H_ONLINE", pParms)
        If pParms(9).Value = 0 Then
            p_newFCO_ID = pParms(10).Value
        End If
        F_SaveFEECOLLECTION_H_ONLINE = pParms(9).Value
    End Function


    Public Shared Function F_SaveFEECOLLSUB_ONLINE(ByVal p_FSO_ID As Integer, ByVal p_FSO_FCO_ID As Integer, _
        ByVal p_FSO_FEE_ID As Integer, ByVal p_FSO_AMOUNT As Decimal, ByVal p_FSO_FSH_ID As String, _
        ByVal p_FSO_ORG_AMOUNT As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '[FEES].[F_SaveFEECOLLSUB_ONLINE] 
        '@FSO_ID int, 
        '@FSO_FCO_ID int, 
        '@FSO_FEE_ID int, 
        '@FSO_AMOUNT numeric (18,3),
        '@FSO_FSH_ID  int  ,
        '@FSO_ORG_AMOUNT  numeric (18,3)
        pParms(0) = New SqlClient.SqlParameter("@FSO_ID", SqlDbType.Int)
        pParms(0).Value = p_FSO_ID
        pParms(1) = New SqlClient.SqlParameter("@FSO_FCO_ID", SqlDbType.Int)
        pParms(1).Value = p_FSO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FSO_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FSO_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FSO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FSO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@FSO_FSH_ID", SqlDbType.BigInt)
        pParms(5).Value = p_FSO_FSH_ID
        pParms(6) = New SqlClient.SqlParameter("@FSO_ORG_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FSO_ORG_AMOUNT
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_ONLINE", pParms)
        F_SaveFEECOLLSUB_ONLINE = pParms(4).Value
    End Function


    Public Shared Function F_SaveFEECOLLSUB_D_ONLINE(ByVal p_FDO_ID As Integer, ByVal p_FDO_FCO_ID As String, _
        ByVal p_FDO_CLT_ID As Integer, ByVal p_FDO_AMOUNT As String, ByVal p_FDO_REFNO As String, _
         ByVal p_FDO_DATE As String, ByVal p_FDO_STATUS As String, ByVal p_FDO_VHH_DOCNO As String, _
        ByVal p_FDO_REF_ID As String, ByVal p_FDO_EMR_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '@FDO_ID = 0,
        '@FDO_FCO_ID = 1,
        '@FDO_CLT_ID = 23,
        '@FDO_AMOUNT = 700,
        '@FDO_REFNO = N'VOUC34',
        pParms(0) = New SqlClient.SqlParameter("@FDO_ID", SqlDbType.Int)
        pParms(0).Value = p_FDO_ID
        pParms(1) = New SqlClient.SqlParameter("@FDO_FCO_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FDO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FDO_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FDO_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FDO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FDO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FDO_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FDO_REFNO
        '@FDO_DATE = N'12-MAY-2008',
        '@FDO_STATUS = 1,
        '@FDO_VHH_DOCNO = N'VOUN5666'
        '@FDO_REF_ID = N'VOUN5666', 
        '@FDO_EMR_ID = N'VOUN5666'
        pParms(5) = New SqlClient.SqlParameter("@FDO_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FDO_DATE
        pParms(6) = New SqlClient.SqlParameter("@FDO_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_FDO_STATUS
        pParms(7) = New SqlClient.SqlParameter("@FDO_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FDO_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@FDO_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_FDO_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@FDO_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FDO_EMR_ID
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D_ONLINE", pParms)
        F_SaveFEECOLLSUB_D_ONLINE = pParms(10).Value
    End Function


    Public Shared Function GetUserForOnlinePayment(ByVal p_STU_NO As String, ByVal p_STU_PASSWORD As String, _
        ByVal p_AUD_HOST As String, ByVal p_AUD_WINUSER As String, ByVal p_STU_BSU_ID As String, _
        ByRef dtUserData As DataTable, ByVal p_str_conn As String) As Integer
        'DECLARE	@return_value int 
        'EXEC	@return_value = [FEES].[GetUserDetailsBB]
        '		@STU_NO = N'11100100005134',
        '		@STU_PASSWORD = N'123',
        '		@AUD_HOST = N'WWW',
        '		@AUD_WINUSER = N'WSE',
        '		@STU_BSU_ID='111001'
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(0).Value = p_STU_NO
            pParms(1) = New SqlClient.SqlParameter("@STU_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_STU_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_STU_BSU_ID
            pParms(3) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar, 100)
            pParms(3).Value = p_AUD_HOST
            pParms(4) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(4).Value = p_AUD_WINUSER
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(p_str_conn, CommandType.StoredProcedure, "FEES.GetUserForOnlinePayment", pParms)
            'retval = SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "GetUserDetails", pParms)
            If pParms(5).Value = "0" Then
                dtUserData = ds.Tables(0)
            End If
            GetUserForOnlinePayment = pParms(5).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            GetUserForOnlinePayment = "1000"
        End Try
    End Function

    
    Public Shared Function UpdateStudentPassword(ByVal p_STU_OLD_PASSWORD As String, ByVal p_STU_ID As String, _
     ByVal p_STU_PASSWORD As String, ByVal trans As SqlTransaction) As Integer
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_OLD_PASSWORD", p_STU_OLD_PASSWORD)
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", p_STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@STU_PASSWORD", p_STU_PASSWORD)
        pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.UpdateStudentPassword", pParms)
        Dim ReturnFlag As Integer = pParms(3).Value
        Return ReturnFlag
    End Function

End Class
