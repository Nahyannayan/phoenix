Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

''' <summary>
''' The class Created for handling Performa Invoice
''' 
''' Author : SHIJIN C A
''' Date : 03-MAY-2008 
''' </summary>
''' <remarks></remarks>
Public Class FEEReminder

    ''' <summary>
    ''' Internal Variables used in the class to Store poperties
    ''' </summary>
    ''' <remarks></remarks>
    Dim vFRH_ID As Integer
    Dim vFRH_BSU_ID As String
    Dim vFRH_ACY_DESCR As String
    Dim vFRH_ACD_ID As Integer
    Dim vFRH_REMARKS As String
    Dim vFRH_DT As DateTime
    Dim vFRH_ASONDATE As DateTime
    Dim vFRH_Level As Int16
    Dim vFRH_bIncludePDC As Boolean
    Public bDelete As Boolean
    Public bEdit As Boolean
    Dim vSTD_SUB_DET As ArrayList

    Public Property FRH_ID() As Integer
        Get
            Return vFRH_ID
        End Get
        Set(ByVal value As Integer)
            vFRH_ID = value
        End Set
    End Property

    Public Property FRH_BSU_ID() As String
        Get
            Return vFRH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFRH_BSU_ID = value
        End Set
    End Property

    Public Property FRH_ACY_DESCR() As String
        Get
            Return vFRH_ACY_DESCR
        End Get
        Set(ByVal value As String)
            vFRH_ACY_DESCR = value
        End Set
    End Property

    Public Property FRH_REMARKS() As String
        Get
            Return vFRH_REMARKS
        End Get
        Set(ByVal value As String)
            vFRH_REMARKS = value
        End Set
    End Property

    Public Property FRH_ACD_ID() As Integer
        Get
            Return vFRH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFRH_ACD_ID = value
        End Set
    End Property

    Public Property FRH_Level() As Int16
        Get
            Return vFRH_Level
        End Get
        Set(ByVal value As Int16)
            vFRH_Level = value
        End Set
    End Property

    Public Property FRH_DT() As DateTime
        Get
            Return vFRH_DT
        End Get
        Set(ByVal value As DateTime)
            vFRH_DT = value
        End Set
    End Property

    Public Property FRH_ASONDATE() As DateTime
        Get
            Return vFRH_ASONDATE
        End Get
        Set(ByVal value As DateTime)
            vFRH_ASONDATE = value
        End Set
    End Property

    Public Property FRH_bIncludePDC() As Boolean
        Get
            Return vFRH_bIncludePDC
        End Get
        Set(ByVal value As Boolean)
            vFRH_bIncludePDC = value
        End Set
    End Property

    Private vFRH_bARABIC As Boolean
    Public Property FRH_bARABIC() As Boolean
        Get
            Return vFRH_bARABIC
        End Get
        Set(value As Boolean)
            vFRH_bARABIC = value
        End Set
    End Property

    Public Function Delete() As Boolean
        bDelete = True
    End Function

    Private pSEND_REMINDER_TO As String
    Public Property SEND_REMINDER_TO() As String
        Get
            Return pSEND_REMINDER_TO
        End Get
        Set(ByVal value As String)
            pSEND_REMINDER_TO = value
        End Set
    End Property
    Public Property IsAutoReminder() As Boolean
    Public Property ReminderLevel() As String
    Private Shared Function GetSection(ByVal vSTU_ID As String) As Integer
        Dim str_Sql As String = "SELECT STU_SCT_ID FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID =" & vSTU_ID
        Dim objSCT_ID As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        If objSCT_ID Is DBNull.Value Then
            Return ""
        Else
            Return Convert.ToInt32(objSCT_ID)
        End If
    End Function

    Public Shared Function GetGradeForBSU(ByVal vBSU_ID As String, ByVal vACD_ID As String) As DataSet
        Dim str_Sql As String
        str_Sql = " SELECT DISTINCT GRM_GRD_ID AS GRD_ID, GRM_DISPLAY, GRD_DISPLAYORDER " & _
            " FROM GRADE_BSU_M WITH(NOLOCK) " & _
            " INNER JOIN GRADE_M WITH(NOLOCK) ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
            " WHERE  (GRM_ACD_ID = '" & vACD_ID & "') " & _
            " AND (GRM_BSU_ID = '" & vBSU_ID & "') " & _
            " ORDER BY GRD_DISPLAYORDER"
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
    End Function

    Public Shared Function GetSectionForGrade(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String)
        Dim str_Sql As String = String.Empty
        str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M WITH(NOLOCK) INNER JOIN " & _
            " SECTION_M WITH(NOLOCK) ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " & _
            " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
            " (GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "')  order by SECTION_M.SCT_DESCR "
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
    End Function

    Public Shared Function SaveFEEReminderDetails(ByVal vSTU_IDs As Hashtable, ByVal vAmt_limit As Double, ByVal vFEE_REM As FEEReminder, ByRef NEW_FRH_ID As Integer, ByVal bSMSafterSave As Boolean, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal strFEEIds As String = "", Optional ByVal ExcludePDC As Boolean = False) As Integer
        Dim iReturnvalue As Integer
        Dim bUpdateData As Boolean = False
        If vFEE_REM Is Nothing Then
            Return 1000
        End If
        Dim cmd As SqlCommand

        If vFEE_REM.bEdit = False AndAlso vFEE_REM.bDelete Then Return 0
        If vFEE_REM.bEdit AndAlso vFEE_REM.bDelete = False Then Return 0

        cmd = New SqlCommand("[FEES].[F_SAVEFEE_REMINDER_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRH_BSU_ID As New SqlParameter("@FRH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFRH_BSU_ID.Value = vFEE_REM.FRH_BSU_ID
        cmd.Parameters.Add(sqlpFRH_BSU_ID)

        Dim sqlpFRH_ACD_ID As New SqlParameter("@FRH_ACD_ID", SqlDbType.Int)
        sqlpFRH_ACD_ID.Value = vFEE_REM.FRH_ACD_ID
        cmd.Parameters.Add(sqlpFRH_ACD_ID)

        Dim sqlpFRH_DT As New SqlParameter("@FRH_DT", SqlDbType.DateTime)
        sqlpFRH_DT.Value = vFEE_REM.FRH_DT
        cmd.Parameters.Add(sqlpFRH_DT)

        Dim sqlpFRH_ASONDATE As New SqlParameter("@FRH_ASONDATE", SqlDbType.DateTime)
        sqlpFRH_ASONDATE.Value = vFEE_REM.FRH_ASONDATE
        cmd.Parameters.Add(sqlpFRH_ASONDATE)

        Dim sqlpFRH_Level As New SqlParameter("@FRH_Level", SqlDbType.TinyInt)
        sqlpFRH_Level.Value = vFEE_REM.vFRH_Level
        cmd.Parameters.Add(sqlpFRH_Level)

        Dim sqlpNarration As New SqlParameter("@Narration", SqlDbType.VarChar)
        sqlpNarration.Value = vFEE_REM.FRH_REMARKS
        cmd.Parameters.Add(sqlpNarration)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_REM.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpbExcludePDC As New SqlParameter("@EXCLUDEPDC", SqlDbType.Bit)
        sqlpbExcludePDC.Value = vFEE_REM.vFRH_bIncludePDC
        cmd.Parameters.Add(sqlpbExcludePDC)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim sqlpNEW_FRH_ID As New SqlParameter("@NEW_FRH_ID", SqlDbType.Int)
        sqlpNEW_FRH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEW_FRH_ID)
        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue = 0 Then
            NEW_FRH_ID = sqlpNEW_FRH_ID.Value
        End If
        If iReturnvalue = 0 Then iReturnvalue = SaveFeeReminderSubDetails(vSTU_IDs, vFEE_REM.FRH_ASONDATE, vFEE_REM.FRH_BSU_ID, vFEE_REM.FRH_ACD_ID, sqlpNEW_FRH_ID.Value, vFEE_REM.FRH_Level, vAmt_limit, bSMSafterSave, conn, trans, vFEE_REM.SEND_REMINDER_TO, strFEEIds, vFEE_REM.vFRH_bIncludePDC)

        Return iReturnvalue
    End Function

    Public Shared Function GetStudentDetailsXML(ByVal vSTU_IDs As Hashtable) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLSTU_ID As XmlElement
        Dim XMLSTU_DET As XmlElement
        Dim XMLbExclude As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vSTU_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLSTU_DET = xmlDoc.CreateElement("STU_DET")

            XMLSTU_ID = xmlDoc.CreateElement("STU_ID")
            XMLSTU_ID.InnerText = iDictEnum.Key
            XMLSTU_DET.AppendChild(XMLSTU_ID)

            XMLbExclude = xmlDoc.CreateElement("bExclude")
            XMLbExclude.InnerText = iDictEnum.Value
            XMLSTU_DET.AppendChild(XMLbExclude)

            xmlDoc.DocumentElement.InsertBefore(XMLSTU_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function

    Public Shared Function GetStudentDetailsXML(ByVal vSTU_IDs As Hashtable, ByVal vACD_ID As Integer, ByVal vASOnDate As Date, ByVal vBSU_ID As String, ByVal vLevel As ReminderType, ByVal trans As SqlTransaction) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLSTU_DET As XmlElement
        Dim XMLFEE_DET As XmlNode
        Dim XMLSTU_ID As XmlElement
        Dim XMLSCT_ID As XmlElement
        Dim XMLACD_ID As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vSTU_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLSTU_DET = xmlDoc.CreateElement("STU_DET")
            XMLSTU_ID = xmlDoc.CreateElement("STU_ID")
            XMLSTU_ID.InnerText = iDictEnum.Key
            XMLSTU_DET.AppendChild(XMLSTU_ID)

            Dim vSCT_ID As Integer = GetSection(iDictEnum.Key)

            XMLSCT_ID = xmlDoc.CreateElement("SCT_ID")
            XMLSCT_ID.InnerText = vSCT_ID
            XMLSTU_DET.AppendChild(XMLSCT_ID)

            XMLACD_ID = xmlDoc.CreateElement("ACD_ID")
            XMLACD_ID.InnerText = vACD_ID
            XMLSTU_DET.AppendChild(XMLACD_ID)

            'XMLFEE_DET = xmlDoc.CreateElement("FEE_DETAILS")

            XMLFEE_DET = GetDetails(xmlDoc, iDictEnum.Key, vASOnDate, vBSU_ID, vLevel, iDictEnum.Value, trans)
            XMLSTU_DET.AppendChild(XMLFEE_DET)

            xmlDoc.DocumentElement.InsertBefore(XMLSTU_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function

    Private Shared Sub GetACD_ID_SCT_ID(ByVal vSTU_ID As String, ByRef vACD_ID As Integer, ByRef vSCT_ID As Integer, ByVal trans As SqlTransaction)
        Dim str_Sql As String = "SELECT STU_SCT_ID, STU_ACD_ID FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID =" & vSTU_ID
        Dim drread As SqlDataReader = SqlHelper.ExecuteReader(trans, CommandType.Text, str_Sql)
        While (drread.Read)
            vACD_ID = drread("STU_ACD_ID")
            vSCT_ID = drread("STU_SCT_ID")
        End While
    End Sub

    Public Shared Function SaveFeeReminderSubDetails(ByVal vSTU_IDs As Hashtable, ByVal vASOnDate As DateTime, _
    ByVal vBSU_ID As String, ByVal vACD_ID As Integer, ByVal vFRH_ID As Integer, ByVal vLevel As ReminderType, _
    ByVal vAmtLimit As Double, ByVal bSMSafterSave As Boolean, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, _
    ByVal SEND_REMINDER_TO As String, _
    Optional ByVal strFEEIds As String = "", Optional ByVal bExcludePDC As Boolean = False) As Integer
        Dim iDictEnum As IDictionaryEnumerator = vSTU_IDs.GetEnumerator
        Dim cmd As SqlCommand
        Dim iReturnvalue As Integer

        Dim str_xml As String = GetStudentDetailsXML(vSTU_IDs)

        cmd = New SqlCommand("FEES.[F_SAVEFEE_REMINDER_D]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRD_FRH_ID As New SqlParameter("@FRD_FRH_ID", SqlDbType.Int)
        sqlpFRD_FRH_ID.Value = vFRH_ID
        cmd.Parameters.Add(sqlpFRD_FRH_ID)

        Dim sqlpAsOnDt As New SqlParameter("@AsOnDt", SqlDbType.DateTime)
        sqlpAsOnDt.Value = vASOnDate
        cmd.Parameters.Add(sqlpAsOnDt)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpLevel As New SqlParameter("@Level", SqlDbType.TinyInt)
        sqlpLevel.Value = vLevel
        cmd.Parameters.Add(sqlpLevel)

        Dim sqlpAmtLimit As New SqlParameter("@Amount_limit", SqlDbType.Decimal)
        sqlpAmtLimit.Value = vAmtLimit
        cmd.Parameters.Add(sqlpAmtLimit)

        Dim sqlpSTU_DET As New SqlParameter("@STU_IDs", SqlDbType.Xml)
        sqlpSTU_DET.Value = str_xml
        cmd.Parameters.Add(sqlpSTU_DET)

        Dim sqlpbSMSafterSave As New SqlParameter("@bSMSafterSave", SqlDbType.Bit)
        sqlpbSMSafterSave.Value = bSMSafterSave
        cmd.Parameters.Add(sqlpbSMSafterSave)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = False
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpFeeIDs As New SqlParameter("@FeeIDs", SqlDbType.VarChar)
        sqlpFeeIDs.Value = strFEEIds
        cmd.Parameters.Add(sqlpFeeIDs)

        Dim sqlpbExcludePDC As New SqlParameter("@EXCLUDEPDC", SqlDbType.Bit)
        sqlpbExcludePDC.Value = bExcludePDC
        cmd.Parameters.Add(sqlpbExcludePDC)

        Dim sqlpSEND_REMINDER_TO As New SqlParameter("@SEND_REMINDER_TO", SqlDbType.VarChar, 5)
        sqlpSEND_REMINDER_TO.Value = SEND_REMINDER_TO
        cmd.Parameters.Add(sqlpSEND_REMINDER_TO)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If

    End Function

    Private Shared Function GetDetails(ByVal xmlDoc As XmlDocument, ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal vRemindertyp As ReminderType, ByVal bExclude As Boolean, ByVal trans As SqlTransaction) As XmlNode
        Try
            Dim dt As DataTable = FEEReminder.GetDueAmountDataTable(stud_id, asOnDate, vBSU_ID, vRemindertyp, trans)
            Dim i As Integer = 0
            'Dim xmlDoc As New XmlDocument
            Dim XMLFEE_DET As XmlElement
            Dim XMLFEE_ID As XmlElement
            Dim XMLFSH_ID As XmlElement
            Dim XMLAMOUNT As XmlElement
            Dim XMLbExclude As XmlElement
            Dim XMLRoot As XmlNode
            XMLRoot = xmlDoc.CreateElement("FEE_DETAILS")
            'xmlDoc.AppendChild(XMLRoot)
            For Each drread As DataRow In dt.Rows
                XMLFEE_DET = xmlDoc.CreateElement("FEE_DET")
                XMLFEE_ID = xmlDoc.CreateElement("FEE_ID")
                XMLFEE_ID.InnerText = drread("FSH_FEE_ID")
                XMLFEE_DET.AppendChild(XMLFEE_ID)

                XMLFSH_ID = xmlDoc.CreateElement("FSH_ID")
                XMLFSH_ID.InnerText = drread("FSH_ID")
                XMLFEE_DET.AppendChild(XMLFSH_ID)

                XMLAMOUNT = xmlDoc.CreateElement("AMOUNT")
                XMLAMOUNT.InnerText = drread("DUE_AMT")
                XMLFEE_DET.AppendChild(XMLAMOUNT)

                XMLbExclude = xmlDoc.CreateElement("bExclude")
                XMLbExclude.InnerText = bExclude
                XMLFEE_DET.AppendChild(XMLbExclude)

                XMLRoot.AppendChild(XMLFEE_DET)
                'xmlDoc.DocumentElement.InsertBefore(XMLFEE_DET, xmlDoc.DocumentElement.LastChild)
            Next
            Return XMLRoot
            'drread.Close()
            'Return rem_sub
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetAdvancedAmount(ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal conn As SqlConnection) As SqlDataReader
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("[FEES].F_FEEReminder_GETADVAMOUNT")
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = vBSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
            sqlpAsOnDT.Value = asOnDate
            cmd.Parameters.Add(sqlpAsOnDT)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = stud_id
            cmd.Parameters.Add(sqlpSTU_ID)
            cmd.Connection = conn
            Return cmd.ExecuteReader
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDueAmount(ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal vAge As Integer) As SqlDataReader
        Try
            Dim sqlparams(4) As SqlParameter

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = vBSU_ID
            'cmd.Parameters.Add(sqlpBSU_ID)
            sqlparams(0) = sqlpBSU_ID

            Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
            sqlpAsOnDT.Value = asOnDate
            sqlparams(1) = sqlpAsOnDT
            'cmd.Parameters.Add(sqlpAsOnDT)

            Dim sqlpAge As New SqlParameter("@Age", SqlDbType.Int)
            sqlpAge.Value = vAge
            sqlparams(2) = sqlpAge
            'cmd.Parameters.Add(sqlpLevel)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = stud_id
            sqlparams(3) = sqlpSTU_ID
            'cmd.Parameters.Add(sqlpSTU_ID)
            'cmd.Connection = conn
            Return SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_FEESConnectionString, _
            CommandType.StoredProcedure, "[FEES].F_FEEReminder_GETDUEAMOUNT", sqlparams)
            'Return cmd.ExecuteReader
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetHeaderDetails(ByVal vFRH_ID As Integer) As FEEReminder
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = "SELECT FEES.FEE_REMINDER_H.FRH_DT, FEES.FEE_REMINDER_H.FRH_ASONDATE, " & _
        " FEES.FEE_REMINDER_H.FRH_Level, FEES.FEE_REMINDER_H.FRH_REMARKS, A.ACY_DESCR, FRH_ACD_ID, " & _
        " ISNULL(RMD_DESCR,'') RMD_DESCR, ISNULL(FRH_bAUTO_REMINDER, 0) FRH_bAUTO_REMINDER FROM OASIS..ACADEMICYEAR_M A WITH ( NOLOCK ) INNER JOIN " & _
        " OASIS..ACADEMICYEAR_D B WITH ( NOLOCK ) ON A.ACY_ID = B.ACD_ACY_ID INNER JOIN " & _
        " FEES.FEE_REMINDER_H WITH ( NOLOCK ) ON B.ACD_ID = FEES.FEE_REMINDER_H.FRH_ACD_ID " & _
        " INNER JOIN dbo.REMINDERTYPE_M AS RM WITH(NOLOCK) ON FRH_Level = RMD_ID " & _
        " WHERE FEES.FEE_REMINDER_H.FRH_ID = " & vFRH_ID & _
        " AND FEES.FEE_REMINDER_H.FRH_bDeleted = 0"
        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        Dim fee_Rem As New FEEReminder
        While (drReader.Read())
            fee_Rem.FRH_ASONDATE = drReader("FRH_ASONDATE")
            fee_Rem.FRH_DT = drReader("FRH_DT")
            fee_Rem.FRH_Level = drReader("FRH_Level")
            fee_Rem.FRH_ACY_DESCR = drReader("ACY_DESCR").ToString()
            fee_Rem.FRH_REMARKS = drReader("FRH_REMARKS").ToString()
            fee_Rem.FRH_ACD_ID = drReader("FRH_ACD_ID")
            fee_Rem.ReminderLevel = drReader("RMD_DESCR").ToString()
            fee_Rem.IsAutoReminder = Convert.ToBoolean(drReader("FRH_bAUTO_REMINDER"))
        End While
        Return fee_Rem
    End Function

    Public Shared Function GetTransportReminderHeaderDetails(ByVal vFRH_ID As Integer) As FEEReminder
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT FEES.FEE_REMINDER_H.FRH_DT, FEES.FEE_REMINDER_H.FRH_ASONDATE, " & _
        " FEES.FEE_REMINDER_H.FRH_Level, FEES.FEE_REMINDER_H.FRH_REMARKS, " & _
        " A.ACY_DESCR FROM OASIS..ACADEMICYEAR_M A INNER JOIN " & _
        " OASIS..ACADEMICYEAR_D B ON A.ACY_ID = B.ACD_ACY_ID INNER JOIN " & _
        " FEES.FEE_REMINDER_H ON B.ACD_ID = FEES.FEE_REMINDER_H.FRH_ACD_ID " & _
        " WHERE FEES.FEE_REMINDER_H.FRH_ID = " & vFRH_ID & _
        " AND FEES.FEE_REMINDER_H.FRH_bDeleted = 0"
        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        Dim fee_Rem As New FEEReminder
        While (drReader.Read())
            fee_Rem.FRH_ASONDATE = drReader("FRH_ASONDATE")
            fee_Rem.FRH_DT = drReader("FRH_DT")
            fee_Rem.FRH_Level = drReader("FRH_Level")
            fee_Rem.FRH_ACY_DESCR = drReader("ACY_DESCR")
            fee_Rem.FRH_REMARKS = drReader("FRH_REMARKS")
        End While
        Return fee_Rem
    End Function

    Public Shared Function GetDueAmountDataTable(ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal vLevel As Int16, ByVal trans As SqlTransaction) As DataTable
        Try
            Dim sqlparams(4) As SqlParameter

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = vBSU_ID
            'cmd.Parameters.Add(sqlpBSU_ID)
            sqlparams(0) = sqlpBSU_ID

            Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
            sqlpAsOnDT.Value = asOnDate
            sqlparams(1) = sqlpAsOnDT
            'cmd.Parameters.Add(sqlpAsOnDT)

            Dim sqlpLevel As New SqlParameter("@Level", SqlDbType.TinyInt)
            sqlpLevel.Value = vLevel
            sqlparams(2) = sqlpLevel
            'cmd.Parameters.Add(sqlpLevel)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = stud_id
            sqlparams(3) = sqlpSTU_ID
            'cmd.Parameters.Add(sqlpSTU_ID)
            'cmd.Connection = conn
            Dim ds As DataSet = SqlHelper.ExecuteDataset(trans, CommandType.StoredProcedure, "[FEES].F_FEEReminder_GETDUEAMOUNT", sqlparams)

            If ds Is Nothing Or ds.Tables.Count < 0 Then
                Return Nothing
            Else
                Return ds.Tables(0)
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function PrintReminder(ByVal vFRH_ID As Integer, ByVal Usr_name As String, Optional ByVal Fees As Boolean = True, Optional ByVal vSTU_IDs As String = "") As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        If Fees = False Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        End If

        Dim cmd As New SqlCommand("[FEES].[F_GETReminderLetter]", New SqlConnection(str_conn))
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRH_ID As New SqlParameter("@FRH_ID", SqlDbType.Int)
        sqlpFRH_ID.Value = vFRH_ID
        cmd.Parameters.Add(sqlpFRH_ID)

        Dim sqlpSTU_ID As New SqlParameter("@FRD_STU_ID", SqlDbType.VarChar)
        sqlpSTU_ID.Value = vSTU_IDs
        cmd.Parameters.Add(sqlpSTU_ID)

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass

        Dim cmdSubColln As New SqlCommand

        ''SUBREPORT1
        cmdSubColln.CommandText = "EXEC [FEES].F_GETReminderLetterDetail " & vFRH_ID & ",'" & vSTU_IDs & "'"
        cmdSubColln.Connection = New SqlConnection(str_conn)
        cmdSubColln.CommandType = CommandType.Text
        repSourceSubRep(0).Command = cmdSubColln

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("UserName") = Usr_name
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If Fees = False Then
            repSource.ResourceName = "../../fees/Reports/RPT/rptTransportFeeReminder.rpt"
        Else
            repSource.ResourceName = "../../fees/Reports/RPT/rptFeeReminder.rpt"
        End If

        Return repSource
    End Function

End Class

Public Class FEEReminder_SUB

    Dim vFRD_STU_ID As Integer
    Dim vFRD_STU_NAME As String
    Dim vFRD_ID As Integer
    Dim vFRD_FRH_ID As Integer
    Dim vFRD_ACD_ID As Integer
    Dim vFRD_ACADEMIC_YEAR As String
    Dim vFRD_FSH_ID As Integer
    Dim vFRD_FEE_ID As Integer
    Dim vFRD_FEE_DESCR As String
    Dim vFRD_AMOUNT As Double
    Dim vFRD_REMARKS As String
    Public bDelete As Boolean
    Public bEdit As Boolean
    Dim bExclude As Boolean

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRD_ID() As Integer
        Get
            Return vFRD_ID
        End Get
        Set(ByVal value As Integer)
            vFRD_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Business Unit ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRD_STU_NAME() As String
        Get
            Return vFRD_STU_NAME
        End Get
        Set(ByVal value As String)
            vFRD_STU_NAME = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Student ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRH_STU_ID() As Integer
        Get
            Return vFRD_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFRD_STU_ID = value
        End Set
    End Property

    Public Property FRD_ACD_ID() As Integer
        Get
            Return vFRD_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFRD_ACD_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Business Unit ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRD_ACADEMIC_YEAR() As String
        Get
            Return vFRD_ACADEMIC_YEAR
        End Get
        Set(ByVal value As String)
            vFRD_ACADEMIC_YEAR = value
        End Set
    End Property


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRD_FEE_ID() As Integer
        Get
            Return vFRD_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFRD_FEE_ID = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRD_AMOUNT() As Double
        Get
            Return vFRD_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFRD_AMOUNT = value
        End Set
    End Property

    Public Property FRD_FEE_DESCR() As String
        Get
            Return vFRD_FEE_DESCR
        End Get
        Set(ByVal value As String)
            vFRD_FEE_DESCR = value
        End Set
    End Property

    Public Property FRD_REMARKS() As String
        Get
            Return vFRD_REMARKS
        End Get
        Set(ByVal value As String)
            vFRD_REMARKS = value
        End Set
    End Property

    Public Property FRD_FRH_ID() As Integer
        Get
            Return vFRD_FRH_ID
        End Get
        Set(ByVal value As Integer)
            vFRD_FRH_ID = value
        End Set
    End Property

    Public Property FRD_FSH_ID() As Integer
        Get
            Return vFRD_FsH_ID
        End Get
        Set(ByVal value As Integer)
            vFRD_FsH_ID = value
        End Set
    End Property

    Public Property FRD_bExclude() As Boolean
        Get
            Return bExclude
        End Get
        Set(ByVal value As Boolean)
            bExclude = value
        End Set
    End Property

End Class