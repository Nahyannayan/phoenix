﻿Imports Microsoft.VisualBasic

Public Class DirecPayTransaction

End Class
Public Class RefundRequest
    Public Property MerchantID As Long
    Public Property CollaboratorID As String
    Public Property TransactionDetails As New TransactionDetails_
    Public Property Amount As New Amount
End Class

Public Class TransactionDetails_
    Public Property ReferenceID As Long
    Public Property MerchantOrderNumber As String
End Class
Public Class Amount
    Public Property RefundRequestID As String
    Public Property RefundAmount As Decimal
    Public Property TransactionAmount As Decimal

End Class

Public Class RefundResponse

    Public Property TransactionRefundDetails As New TransactionRefundDetails
    Public Property AmountRefund As New AmountRefund
    Public Property StatusBlock As New StatusBlock

End Class
Public Class TransactionRefundDetails
    Public Property ReferenceID As Long
    Public Property MerchantOrderNumber As String
    Public Property RefundReferenceNumber As String
End Class
Public Class AmountRefund
    Public Property TotalRefundedAmount As Decimal
    Public Property Amountavailableforrefund As Decimal
    Public Property Currency As String
End Class
Public Class StatusBlock
    Public Property StatusFlag As String
    Public Property ReasonCode As String
    Public Property ReasonDescription As String
End Class