Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Linq
Imports System.Threading
Imports System.Threading.Tasks

Public Class FeeDayendProcess

    Public Shared Function F_GenerateCollectionVoucher(ByVal p_BSU_ID As String, ByVal p_Dt As String, _
    ByVal p_USER As String, ByVal p_bHArdClose As String, ByVal p_PDCIncludeDays As String, _
     ByVal p_conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_GenerateCollectionVoucher]
        '@BSU_ID = N'125016',
        '@Dt = N'12-may-2008',
        '@USER = N'guru' @ACD_ID BIGINT  
        '@PDCIncludeDays 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
        pParms(2).Value = p_USER
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@bHArdClose", SqlDbType.Bit)
        pParms(4).Value = p_bHArdClose
        pParms(5) = New SqlClient.SqlParameter("@PDCIncludeDays", SqlDbType.Int)
        pParms(5).Value = p_PDCIncludeDays
        Dim cmd As New SqlCommand
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))
        cmd.Connection = p_conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FEES.F_GenerateCollectionVoucher"
        cmd.CommandTimeout = 0
        Dim retval As Integer
        retval = cmd.ExecuteNonQuery
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_GenerateCollectionVoucher", pParms)
        F_GenerateCollectionVoucher = pParms(3).Value
    End Function

    Public Shared Function POSTVOUCHER_CHQ(ByVal p_VCH_SUB_ID As String, ByVal p_VCH_BSU_ID As String, _
                  ByVal p_VCH_FYEAR As String, ByVal p_VCH_DOCTYPE As String, ByVal p_VCH_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '@return_value = [dbo].[POSTVOUCHER_CHQ]
        '@VCH_SUB_ID = N'007',
        '@VCH_BSU_ID = N'125016',
        '@VCH_FYEAR = 2008gh,
        '@VCH_DOCTYPE = N'damn',
        '@VCH_DOCNO = N'd0005' 
        pParms(0) = New SqlClient.SqlParameter("@VCH_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_VCH_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VCH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_VCH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VCH_FYEAR", SqlDbType.VarChar, 50)
        pParms(2).Value = p_VCH_FYEAR
        pParms(3) = New SqlClient.SqlParameter("@VCH_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_VCH_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCH_DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_VCH_DOCNO
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFinance & ".dbo.POSTVOUCHER_CHQ", pParms)
        POSTVOUCHER_CHQ = pParms(5).Value
    End Function

    Public Shared Function POSTVOUCHER_QR(ByVal p_VHH_SUB_ID As String, ByVal p_VHH_BSU_ID As String, _
              ByVal p_VHH_FYEAR As String, ByVal p_VHH_DOCTYPE As String, ByVal p_VHH_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '      EXEC	@return_value = [dbo].[POSTVOUCHER_QR]
        '@VHH_SUB_ID = N'007',
        '@VHH_BSU_ID = N'125016',
        '@VHH_FYEAR = 2008,
        '@VHH_DOCTYPE = N'12-AUG-2008',
        '@VHH_DOCNO = N'072C567' 
        pParms(0) = New SqlClient.SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_VHH_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_VHH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHH_FYEAR", SqlDbType.VarChar, 50)
        pParms(2).Value = p_VHH_FYEAR
        pParms(3) = New SqlClient.SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_VHH_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_VHH_DOCNO
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFinance & ".dbo.POSTVOUCHER_QR", pParms)
        POSTVOUCHER_QR = pParms(5).Value
    End Function

    Public Shared Function POSTVOUCHER_CHQ_REVERSE(ByVal p_VCH_SUB_ID As String, ByVal p_VCH_BSU_ID As String, _
      ByVal p_VCH_FYEAR As String, ByVal p_VCH_DOCTYPE As String, ByVal p_VCH_DOCNO As String, _
       ByVal p_DT As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '     EXEC	@return_value = [dbo].[POSTVOUCHER_CHQ_REVERSE]
        '@VCH_SUB_ID = N'007',
        '@VCH_BSU_ID = N'125016',
        '@VCH_FYEAR = 2008,
        '@VCH_DOCTYPE = N'QR',
        '@VCH_DOCNO = N'qerr',
        '@DT = N'12-sep-2008'
        pParms(0) = New SqlClient.SqlParameter("@VCH_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_VCH_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VCH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_VCH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VCH_FYEAR", SqlDbType.VarChar, 50)
        pParms(2).Value = p_VCH_FYEAR
        pParms(3) = New SqlClient.SqlParameter("@VCH_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_VCH_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCH_DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_VCH_DOCNO
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        pParms(6) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(6).Value = p_DT
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFinance & ".dbo.POSTVOUCHER_CHQ_REVERSE", pParms)
        POSTVOUCHER_CHQ_REVERSE = pParms(5).Value
    End Function

    Public Shared Function GenerateQRDetails(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_SUB_ID As String, _
        ByVal p_DOCTYPE As String, ByVal p_VCD_DET As String, ByVal p_VHH_NEWDOCNO As String, ByVal p_USER As String, _
        ByVal p_FYR_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(8) As SqlClient.SqlParameter
        'EXEC	@return_value = [dbo].[GenerateQRDetails]
        '@BSU_ID = N'125016',
        '@Dt = N'27-JUL-2008',
        '@SUB_ID = N'007',
        '@DOCTYPE = N'QR',
        '@VCD_IDS = N'61|62|63', @FYR_ID
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SUB_ID
        pParms(3) = New SqlClient.SqlParameter("@DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCD_DET", SqlDbType.Xml)
        pParms(4).Value = p_VCD_DET
        '@VHH_NEWDOCNO = N'XXX',
        '@USER = N'GURU'
        pParms(5) = New SqlClient.SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(5).Value = p_VHH_NEWDOCNO
        pParms(6) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(6).Value = p_USER
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@FYR_ID", SqlDbType.VarChar, 10)
        pParms(8).Value = p_FYR_ID

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "GenerateQRDetails", pParms)
        GenerateQRDetails = pParms(7).Value
    End Function

    Public Shared Function GenerateQRDetails_Transport(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_SUB_ID As String, _
            ByVal p_DOCTYPE As String, ByVal p_VCD_DET As String, ByVal p_VHH_NEWDOCNO As String, ByVal p_USER As String, _
            ByVal p_FYR_ID As String, ByVal p_STU_BSU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(9) As SqlClient.SqlParameter
        'EXEC	@return_value = [dbo].[GenerateQRDetails]
        '@BSU_ID = N'125016',
        '@Dt = N'27-JUL-2008',
        '@SUB_ID = N'007',
        '@DOCTYPE = N'QR',
        '@VCD_IDS = N'61|62|63', @FYR_ID
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SUB_ID
        pParms(3) = New SqlClient.SqlParameter("@DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCD_DET", SqlDbType.Xml)
        pParms(4).Value = p_VCD_DET
        '@VHH_NEWDOCNO = N'XXX',
        '@USER = N'GURU'
        pParms(5) = New SqlClient.SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(5).Value = p_VHH_NEWDOCNO
        pParms(6) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(6).Value = p_USER
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@FYR_ID", SqlDbType.VarChar, 10)
        pParms(8).Value = p_FYR_ID
        pParms(9) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_STU_BSU_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "GenerateQRDetails_Transport", pParms)
        GenerateQRDetails_Transport = pParms(7).Value
    End Function

    Public Shared Function GetDayEndHistory(ByVal p_bHardClose As Boolean, ByVal p_BSU_ID As String, _
    ByVal p_DATE As Date, ByVal p_IsTransport As Boolean) As DataSet
        ' FEES.GetDayEndHistory 
        '@CURDATE DATETIME,
        '@BSU_ID VARCHAR(20),
        '@ACD_ID BIGINT  
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        If p_IsTransport Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        End If
        Dim pParms(5) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@bHardClose", SqlDbType.Bit)
        'pParms(0).Value = p_bHardClose
        pParms(0) = New SqlClient.SqlParameter("@CURDATE", SqlDbType.DateTime)
        pParms(0).Value = p_DATE
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        Return SqlHelper.ExecuteDataset(str_conn, _
          CommandType.StoredProcedure, "FEES.GetDayEndHistory", pParms)
    End Function

    Public Shared Function UnpostedOthFeecharge(ByVal p_BSU_ID As String, ByVal p_DATE As Date, ByVal p_IsTransport As Boolean) As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(0).Value = p_DATE
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        Return SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.OtherFeecharge_Unposted", pParms)
    End Function

    Public Function F_POSTFEECHARGE(ByVal p_BSU_ID As String, _
            ByVal p_Dt As String, ByVal p_USER As String, ByVal p_ACD_ID As String, ByRef NEW_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_POSTFEECHARGE]
        '@BSU_ID = N'125016',
        '@ACD_ID = 80XX,
        '@Dt = N'12-SEP-2008',
        '@USER = N'GURU'
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
        pParms(2).Value = p_USER
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
        pParms(5).Direction = ParameterDirection.Output
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTFEECHARGE", pParms)
        If Not pParms(5).Value Is System.DBNull.Value Then
            NEW_DOCNO = pParms(5).Value
        End If
        F_POSTFEECHARGE = pParms(3).Value
    End Function

    Private pNEW_DOCNO As String
    Public Property pbNEW_DOCNO() As String
        Get
            Return pNEW_DOCNO
        End Get
        Protected Set(ByVal value As String)
            pNEW_DOCNO = value
        End Set
    End Property

    Private pSTATUS As String
    Public Property STATUS() As String
        Get
            Return pSTATUS
        End Get
        Protected Set(ByVal value As String)
            pSTATUS = value
        End Set
    End Property
    ''' <summary>
    ''' Gets the percent complete for the long-running process.
    ''' </summary>
    Private privatePercentComplete As Double
    Public Property PercentComplete() As Double
        Get
            Return privatePercentComplete
        End Get
        Protected Set(ByVal value As Double)
            privatePercentComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is still executing.
    ''' </summary>
    Private privateIsRunning As Boolean
    Public Property IsRunning() As Boolean
        Get
            Return privateIsRunning
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsRunning = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is done running.
    ''' </summary>
    Private privateIsComplete As Boolean
    Public Property IsComplete() As Boolean
        Get
            Return privateIsComplete
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsComplete = value
        End Set
    End Property
    Private privateIsSuccess As Boolean
    Public Property IsSuccess() As Boolean
        Get
            Return privateIsSuccess
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsSuccess = value
        End Set
    End Property

    Private privateBSUID As String
    Public Property BSUID() As String
        Get
            Return privateBSUID
        End Get
        Protected Set(ByVal value As String)
            privateBSUID = value
        End Set
    End Property
    Private privateSTBSUID As String
    Public Property STBSUID() As String
        Get
            Return privateSTBSUID
        End Get
        Protected Set(ByVal value As String)
            privateSTBSUID = value
        End Set
    End Property
    Private privateChDATE As String
    Public Property ChDATE() As String
        Get
            Return privateChDATE
        End Get
        Protected Set(ByVal value As String)
            privateChDATE = value
        End Set
    End Property
    Private privateIsTransport As Boolean
    Public Property IsTransport() As Boolean
        Get
            Return privateIsTransport
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsTransport = value
        End Set
    End Property
    ''' <summary>
    ''' Gets a brief description of what is currently being worked on, to update the end-user.
    ''' </summary>
    Public Property WorkDescription() As String

    ''' <summary>
    ''' Event for when progress has changed.
    ''' </summary>
    Public Event ProgressChanged As EventHandler(Of ProgressChangedEventArgs)

    ''' <summary>
    ''' Event for the the long-running process is complete.
    ''' </summary>
    Public Event ProcessComplete As EventHandler
    ''' <summary>
    ''' Private handle to the task that is currently execute the long-running process.
    ''' </summary>
    Private task As Task

    Public Sub UpdatePercent(ByVal newPercentComplete As Double, ByVal workDescription As String)
        ' Update the properties for any callers casually checking the status.
        Me.PercentComplete = newPercentComplete
        Me.WorkDescription = workDescription
        If newPercentComplete = 100 Then
            IsComplete = True
        End If
        ' Fire the event for any callers who actively want to be notifed.
        RaiseEvent ProgressChanged(Me, New ProgressChangedEventArgs(newPercentComplete, IsComplete, workDescription))
    End Sub

    Public Sub POSTFEECHARGE(ByVal p_BSU_ID As String, _
            ByVal p_Dt As String, ByVal p_USER As String, ByVal p_ACD_ID As String)
        Dim stTrans As SqlTransaction
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)

        ' Kick off the actual, private long-running process in a new Task
        task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                Dim objss As New AjaxServices
                                                                Try
                                                                    objss.StartProcess()
                                                                    objConn.Open()
                                                                    stTrans = objConn.BeginTransaction("SampleTransaction")
                                                                    STATUS = F_POSTFEECHARGE(p_BSU_ID, p_Dt, p_USER, p_ACD_ID, pNEW_DOCNO, stTrans)
                                                                    If STATUS <> 0 Then
                                                                        stTrans.Rollback()
                                                                        objss.EndProcess(False, UtilityObj.getErrorMessage(STATUS))
                                                                    Else
                                                                        stTrans.Commit()
                                                                        objss.EndProcess(True, "Charging completed successfully")
                                                                    End If
                                                                Catch ex As Exception
                                                                    objss.EndProcess(False, "Charging Failed")
                                                                Finally
                                                                    If objConn.State = ConnectionState.Open Then
                                                                        objConn.Close()
                                                                    End If
                                                                End Try
                                                            End Sub)
    End Sub

    Public Shared Function F_POSTFEESADJUSTMENT_MONTHLY(ByVal p_BSU_ID As String, _
                ByVal p_Dt As String, ByRef p_NewDOCNo As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(4) As SqlClient.SqlParameter
        'EXEC  @return_value = [FEES].[F_POSTFEESADJUSTMENT_MONTHLY]
        '@BSU_ID = N'125016',
        '@Dt = '31/aug/2008',
        '@NewDOCNo = @NewDOCNo OUTPUT
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@NewDOCNo", SqlDbType.VarChar, 20)
        pParms(2).Direction = ParameterDirection.Output

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTFEESADJUSTMENT_MONTHLY", pParms)
        If Not pParms(2).Value Is System.DBNull.Value Then
            p_NewDOCNo = pParms(2).Value
        End If
        F_POSTFEESADJUSTMENT_MONTHLY = pParms(3).Value
    End Function

    Public Shared Function F_POSTFEE_REVENUE(ByVal p_BSU_ID As String, _
            ByVal p_Dt As String, ByVal p_ACD_ID As String, ByRef DOC_NO As String, _
            ByRef p_BHardClose As Boolean, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_POSTFEE_REVENUE]
        '@BSU_ID = N'125016',
        '@Dt = N'12-SEP-2008',
        '@ACD_ID = XXCD
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Direction = ParameterDirection.Output
        pParms(5) = New SqlClient.SqlParameter("@BHardClose", SqlDbType.Bit)
        pParms(5).Value = p_BHardClose

        Dim cmd As New SqlCommand
        cmd.Transaction = p_stTrans
        cmd.CommandTimeout = 0
        cmd.CommandText = "FEES.F_POSTFEE_REVENUE"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = p_stTrans.Connection
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTFEE_REVENUE", pParms)
        cmd.ExecuteNonQuery()

        F_POSTFEE_REVENUE = pParms(3).Value
        If Not pParms(4).Value Is DBNull.Value Then
            DOC_NO = pParms(4).Value
        End If
    End Function

    Public Shared Function F_DOMONTHEND(ByVal p_BSU_ID As String, _
        ByVal p_Dt As String, ByVal p_ACD_ID As String, ByVal p_What As String, ByVal p_User As String, _
        ByVal bHardClose As Boolean, ByVal p_conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        '      EXEC	@return_value = [FEES].[F_DOMONTHEND]
        '@BSUID = N'125016',
        '@ACD_ID = 80,
        '@DT = N'12-jan-2008',
        '@What = 12,
        '@User = N'guru'
        Dim cmd As New SqlCommand
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@User", SqlDbType.VarChar, 50)
        pParms(4).Value = p_User
        pParms(5) = New SqlClient.SqlParameter("@What", SqlDbType.BigInt)
        pParms(5).Value = p_What
        pParms(6) = New SqlClient.SqlParameter("@bHardClose", SqlDbType.Bit)
        pParms(6).Value = bHardClose

        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))
        cmd.Parameters.Add(pParms(6))

        cmd.Connection = p_conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FEES.F_DOMONTHEND"
        cmd.CommandTimeout = 0
        Dim retval As Integer
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_DOMONTHEND", pParms)
        retval = cmd.ExecuteNonQuery()
        F_DOMONTHEND = pParms(3).Value
    End Function

    Public Sub StartProcess()
        IsComplete = False
        IsRunning = True
        IsSuccess = True
        PercentComplete = 0
        UpdatePercent(0, "Please wait..")
    End Sub
    Public Sub InitProgress(ByVal prbsuid As String, ByVal stbsuid As String, ByVal chDate As String, ByVal bTransport As Boolean)
        Me.BSUID = prbsuid
        Me.STBSUID = stbsuid
        Me.ChDATE = chDate
        Me.IsTransport = bTransport
    End Sub
    Public Sub GetProgress()
        Dim QRY As String = "EXEC dbo.GET_CHARGING_STATUS @PR_BSU_ID = '" & BSUID & "',@ST_BSU_ID = '" & STBSUID & "',@CH_DATE = '" & ChDATE & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, QRY)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim STU_COUNT As Integer = ds.Tables(0).Rows(0)("STU_COUNT").ToString
            Dim CHARGE_COUNT As Integer = ds.Tables(0).Rows(0)("CHARGE_COUNT").ToString
            Dim Percnt As Integer = (CHARGE_COUNT / STU_COUNT) * 100
            If Percnt < 100 Then
                UpdatePercent(Percnt, "processing")
            End If
        End If
    End Sub
    Public Sub EndProcess(ByVal status As Boolean, ByVal msg As String)
        UpdatePercent(100, msg)
        IsSuccess = status
        IsTransport = False
        Me.BSUID = ""
        Me.STBSUID = ""
        Me.ChDATE = ""
        Thread.Sleep(1200)
        RaiseEvent ProcessComplete(Me, New EventArgs())
        IsRunning = False
    End Sub
    Public Sub DOMONTHEND_STUDENTWISE(ByVal STUIDs As String, ByVal p_BSU_ID As String, _
    ByVal p_Dt As String, ByVal p_ACD_ID As String)
        ' Kick off the actual, private long-running process in a new Task
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            STATUS = 1000
            task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                    Dim objss As New AjaxServices
                                                                    Dim flag As Boolean = True
                                                                    Try
                                                                        objss.StartProcess()
                                                                        Dim i As Integer = 0
                                                                        If STUIDs.Trim <> "" Then
                                                                            objConn.Open()
                                                                            Dim STUID As String() = STUIDs.Split("|")
                                                                            For i = 0 To STUID.Length - 1
                                                                                transaction = objConn.BeginTransaction("SampleTransaction")

                                                                                STATUS = F_DOMONTHEND_STUDENTWISE(p_BSU_ID, p_Dt, p_ACD_ID, STUID(i), objConn, transaction)
                                                                                Dim Percnt As Integer = ((i + 1) / STUID.Length) * 100
                                                                                If Percnt < 100 Then
                                                                                    objss.UpdatePercentage(Percnt, "processing")
                                                                                End If
                                                                                If STATUS = 0 Then
                                                                                    transaction.Commit()
                                                                                Else
                                                                                    transaction.Rollback()
                                                                                    flag = False
                                                                                    Exit For
                                                                                End If
                                                                            Next i
                                                                        End If
                                                                        'UpdatePercent(i, "")
                                                                        If flag = False Then
                                                                            objss.EndProcess(flag, UtilityObj.getErrorMessage(STATUS))
                                                                        Else
                                                                            objss.EndProcess(flag, "Charging completed successfully")
                                                                        End If
                                                                    Catch ex As Exception
                                                                        objss.EndProcess(False, "Charging Failed")
                                                                    Finally
                                                                        If objConn.State = ConnectionState.Open Then
                                                                            objConn.Close()
                                                                        End If
                                                                    End Try
                                                                End Sub)
            If task.IsCompleted Then
                Dim a = task.Status
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function F_DOMONTHEND_STUDENTWISE(ByVal p_BSU_ID As String, _
    ByVal p_Dt As String, ByVal p_ACD_ID As String, ByVal p_STU_ID As String, _
    ByVal p_Conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        IsRunning = True
        Dim pParms(6) As SqlClient.SqlParameter
        ' ALTER procedure [FEES].[F_DOMONTHEND_STUDENTWISE]
        '@BSU_ID VARCHAR(20)='125016',
        '@STU_IDS XML= '<STU_DETAILS><STU_DETAIL><STU_ID>61034</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60889</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60896</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60898</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60902</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61320</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61413</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61501</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61527</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61538</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61544</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61295</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61304</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61308</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60880</STU_ID></STU_DETAIL></STU_DETAILS>',
        '@ACD_ID BIGINT=80, 
        '@Dt DATETIME ='1-JUN-2008' 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar)
        pParms(4).Value = p_STU_ID
        Dim cmd As New SqlCommand()
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))

        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.CommandText = "FEES.F_DOMONTHEND_STUDENTWISE"
        cmd.CommandTimeout = 0
        cmd.Connection = p_Conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        Dim retval As Integer
        retval = cmd.ExecuteNonQuery
        F_DOMONTHEND_STUDENTWISE = pParms(3).Value
    End Function
    Public Sub DOMONTHEND_STUDENTS(ByVal STUIDs As String, ByVal p_BSU_ID As String, _
   ByVal p_Dt As String, ByVal p_ACD_ID As String)
        ' Reset the properties that report status.


        ' Kick off the actual, private long-running process in a new Task
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            Dim flag As Boolean = True
            STATUS = 1000
            task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                    Dim objss As New AjaxServices
                                                                    Try
                                                                        objss.StartProcess()
                                                                        If STUIDs.Trim <> "" Then
                                                                            objConn.Open()
                                                                            transaction = objConn.BeginTransaction("SampleTransaction")
                                                                            STATUS = F_DOMONTHEND_STUDENTWISE(p_BSU_ID, p_Dt, p_ACD_ID, STUIDs, objConn, transaction)
                                                                        End If
                                                                        If STATUS <> 0 Then
                                                                            transaction.Rollback()
                                                                            objss.EndProcess(False, UtilityObj.getErrorMessage(STATUS))
                                                                        Else
                                                                            transaction.Commit()
                                                                            objss.EndProcess(True, "Charging completed successfully")
                                                                        End If
                                                                    Catch ex As Exception
                                                                        objss.EndProcess(False, "Charging Failed")
                                                                    Finally
                                                                        If objConn.State = ConnectionState.Open Then
                                                                            objConn.Close()
                                                                        End If
                                                                    End Try
                                                                End Sub)
            If task.IsCompleted Then
                Dim a = task.Status
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function F_DOMONTHEND_STUDENTS(ByVal p_BSU_ID As String, _
    ByVal p_Dt As String, ByVal p_ACD_ID As String, ByVal p_STU_IDs As String, _
    ByVal p_Conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        IsRunning = True
        Dim pParms(6) As SqlClient.SqlParameter
        ' ALTER procedure [FEES].[F_DOMONTHEND_STUDENTWISE]
        '@BSU_ID VARCHAR(20)='125016',
        '@STU_IDS XML= '<STU_DETAILS><STU_DETAIL><STU_ID>61034</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60889</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60896</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60898</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60902</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61320</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61413</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61501</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61527</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61538</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61544</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61295</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61304</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>61308</STU_ID></STU_DETAIL><STU_DETAIL><STU_ID>60880</STU_ID></STU_DETAIL></STU_DETAILS>',
        '@ACD_ID BIGINT=80, 
        '@Dt DATETIME ='1-JUN-2008' 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@STU_IDs", SqlDbType.VarChar)
        pParms(4).Value = p_STU_IDs
        Dim cmd As New SqlCommand()
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))

        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.CommandText = "FEES.F_DOMONTHEND_STUDENTS"
        cmd.CommandTimeout = 0
        cmd.Connection = p_Conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        Dim retval As Integer
        retval = cmd.ExecuteNonQuery
        F_DOMONTHEND_STUDENTS = pParms(3).Value
    End Function

    Public Shared Function F_POSTCONCESSION(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_FCH_ID As String, _
    ByVal p_stTrans As SqlTransaction, Optional ByVal p_FCH_USER As String = "") As String
        Dim pParms(6) As SqlClient.SqlParameter
        '   ALTER procedure [FEES].[F_POSTCONCESSION] 
        '(@BSU_ID varchar(20),
        '@Dt datetime,
        '@FCH_ID BIGINT 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@FCH_ID", SqlDbType.BigInt)
        pParms(2).Value = p_FCH_ID
        pParms(4) = New SqlClient.SqlParameter("@FCH_USER", SqlDbType.VarChar)
        pParms(4).Value = p_FCH_USER
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        'HttpContext.Current.Session("")
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTCONCESSION", pParms)

        F_POSTCONCESSION = pParms(3).Value
    End Function

    Public Shared Function REJECT_CONCESSION(ByVal p_FCH_ID As String, _
    ByVal p_stTrans As SqlTransaction, Optional ByVal p_FCH_USER As String = "") As String
        Dim pParms(6) As SqlClient.SqlParameter
        '   ALTER procedure [FEES].[F_POSTCONCESSION] 
        '(@BSU_ID varchar(20),
        '@Dt datetime,
        '@FCH_ID BIGINT 
        pParms(0) = New SqlClient.SqlParameter("@FCH_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCH_ID
        pParms(1) = New SqlClient.SqlParameter("@FCH_USER", SqlDbType.VarChar)
        pParms(1).Value = p_FCH_USER
        pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        'HttpContext.Current.Session("")
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[FEES].[REJECT_CONCESSION]", pParms)

        REJECT_CONCESSION = pParms(2).Value
    End Function

    Public Shared Function F_POSTCONCESSION_Transoport(ByVal p_BSU_ID As String, ByVal p_Dt As String, _
    ByVal p_FCH_ID As String, ByVal p_STU_BSU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '   ALTER procedure [FEES].[F_POSTCONCESSION] 
        '(@BSU_ID varchar(20),
        '@Dt datetime,
        '@FCH_ID BIGINT 

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@FCH_ID", SqlDbType.BigInt)
        pParms(2).Value = p_FCH_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_STU_BSU_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTCONCESSION", pParms)

        F_POSTCONCESSION_Transoport = pParms(3).Value
    End Function

    Public Shared Function REJECT_CONCESSION_Transport(ByVal p_FCH_ID As String, _
    ByVal p_stTrans As SqlTransaction, Optional ByVal p_FCH_USER As String = "") As String
        Dim pParms(6) As SqlClient.SqlParameter
        '   ALTER procedure [FEES].[F_POSTCONCESSION] 
        '(@BSU_ID varchar(20),
        '@Dt datetime,
        '@FCH_ID BIGINT 
        pParms(0) = New SqlClient.SqlParameter("@FCH_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCH_ID
        pParms(1) = New SqlClient.SqlParameter("@FCH_USER", SqlDbType.VarChar)
        pParms(1).Value = p_FCH_USER
        pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        'HttpContext.Current.Session("")
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[FEES].[REJECT_CONCESSION]", pParms)

        REJECT_CONCESSION_Transport = pParms(2).Value
    End Function

    Public Shared Function F_GenerateCollectionVoucher_Transport(ByVal p_BSU_ID As String, _
    ByVal p_Dt As String, ByVal p_USER As String, ByVal p_PDCIncludeDays As String, _
    ByVal p_STU_BSU_ID As String, ByVal p_bHardClose As Boolean, _
    ByVal p_conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(8) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_GenerateCollectionVoucher]
        '@BSU_ID = N'125016',
        '@Dt = N'12-may-2008',
        '@USER = N'guru' @ACD_ID BIGINT  
        '@PDCIncludeDays 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
        pParms(2).Value = p_USER
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@bHardClose", SqlDbType.Bit)
        pParms(4).Value = p_bHardClose
        pParms(5) = New SqlClient.SqlParameter("@PDCIncludeDays", SqlDbType.Int)
        pParms(5).Value = p_PDCIncludeDays
        pParms(6) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(6).Value = p_STU_BSU_ID

        Dim cmd As New SqlCommand
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))
        cmd.Parameters.Add(pParms(6))

        cmd.Connection = p_conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FEES.F_GenerateCollectionVoucher"
        cmd.CommandTimeout = 0
        Dim retval As Integer
        retval = cmd.ExecuteNonQuery
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_GenerateCollectionVoucher", pParms)
        F_GenerateCollectionVoucher_Transport = pParms(3).Value
    End Function

    Public Shared Function GETDAYENDSTATUS(ByVal p_BSU_ID As String, _
    ByVal p_DATE As Date, ByVal p_IsTransport As Boolean) As DataTable
        ' FEES.GetDayEndHistory 
        '@CURDATE DATETIME,
        '@BSU_ID VARCHAR(20),
        '@ACD_ID BIGINT  
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        If p_IsTransport Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        End If
        Dim pParms(5) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        'pParms(0).Value = p_ACD_ID
        pParms(0) = New SqlClient.SqlParameter("@CURDATE", SqlDbType.DateTime)
        pParms(0).Value = p_DATE
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(str_conn, _
          CommandType.StoredProcedure, "FEES.GETDAYENDSTATUS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GenerateBRFromQR(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_SUB_ID As String, _
        ByVal p_DOCTYPE As String, ByVal p_VHD_DET As String, ByVal p_VHH_NEWDOCNO As String, ByVal p_USER As String, _
        ByVal p_FYR_ID As String, ByVal p_NARRATION As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter
        'exec([dbo].[GenerateBRFromQR])
        '@BSU_ID ='123004',
        '@SUB_ID ='007',
        '@FYR_ID='2009',
        '@DOCTYPE='BR',
        '@VHH_NEWDOCNO ='',
        '@Dt   ='07/oct/2009',
        '@VHD_DET='',
        '@NARRATION= 'veruthe',
        '@USER= 'koppu'
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SUB_ID
        pParms(3) = New SqlClient.SqlParameter("@DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VHD_DET", SqlDbType.Xml)
        pParms(4).Value = p_VHD_DET
        pParms(5) = New SqlClient.SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(5).Value = p_VHH_NEWDOCNO
        pParms(6) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(6).Value = p_USER
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@FYR_ID", SqlDbType.VarChar)
        pParms(8).Value = p_FYR_ID
        pParms(9) = New SqlClient.SqlParameter("@NARRATION", SqlDbType.VarChar)
        pParms(9).Value = p_NARRATION
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "GenerateBRFromQR", pParms)
        GenerateBRFromQR = pParms(7).Value
    End Function

    Public Shared Function PrintDepostSlip(ByVal BSU_ID As String, ByVal SUB_ID As String, _
    ByVal FYear As String, ByVal DOC_NO As String, ByVal Usr_name As String) As MyReportClass
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
        ' GetBankDepositData 
        '@VHD_DOCTYPE varchar(10),
        '@VHD_BSU_ID  varchar(20),
        '@VHD_FYEAR  varchar(20),
        '@VHD_SUB_ID  varchar(20),
        '@VHD_DOCNO  varchar(20)  ViewState("VHD_DOCNO")  ViewState("FYear")
        Dim cmd As New SqlCommand("GetBankDepositData", New SqlConnection(str_conn))
        cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "QR")
        cmd.Parameters.AddWithValue("@VHD_BSU_ID", BSU_ID)
        cmd.Parameters.AddWithValue("@VHD_FYEAR", FYear)
        cmd.Parameters.AddWithValue("@VHD_SUB_ID", SUB_ID)
        cmd.Parameters.AddWithValue("@VHD_DOCNO", DOC_NO)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Usr_name
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../fees/Reports/RPT/rptFeeBankDepositSlip.rpt"
        Return repSource
    End Function

    Public Shared Function PrintQR(ByVal DocNo As String, ByVal DocDate As String, ByVal BSuId As String, ByVal FYear As String, Optional ByVal docType As String = "", Optional ByVal NdocNo As String = "") As MyReportClass
        Dim str_Sql, strFilter As String
        Dim IsCost As Boolean = True
        Dim Narr As Boolean = True
        If docType = "" Then
            docType = DocNo.Substring(2, 2)
        End If
        If DocDate = "" Then
            strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DocNo _
                    & "' and VHH_BSU_ID in('" & BSuId & "')"
        ElseIf NdocNo <> "" Then
            strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO  in ('" & NdocNo & "')" _
                          & " and VHH_DOCDT = '" & DocDate & "' and VHH_BSU_ID in('" & BSuId & "')"
        Else
            strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DocNo _
                   & "' and VHH_DOCDT = '" & String.Format("{0:dd-MMM-yyyy}", CDate(DocDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        End If
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim VochNamePara As String = "Cheque Deposit Voucher"
        Dim VochName As String = "Cheque Deposit Voucher"
        Dim RptName As String = "../../fees/Reports/RPT/rptFeeBankReceiptReport.rpt"
        DocNo = DocNo.Replace("'", "")
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, docType, DocDate)
            params("voucherName") = VochNamePara
            params("Summary") = False
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.VoucherName = VochName
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.SubReports(DocNo, docType, BSuId, IsCost, Narr)
            repSource.Command = cmd
            repSource.ResourceName = RptName
        End If
        Return repSource
    End Function

    Public Shared Function F_GenerateCollectionVoucher_Provider_Transport(ByVal p_BSU_ID As String, _
    ByVal p_Dt As String, ByVal p_USER As String, ByVal p_STU_BSU_ID As String, _
    ByVal p_bHardClose As Boolean, ByVal p_conn As SqlConnection, _
    ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(8) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_GenerateCollectionVoucher]
        '@BSU_ID = N'125016',
        '@Dt = N'12-may-2008',
        '@USER = N'guru' @ACD_ID BIGINT  
        '@PDCIncludeDays 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
        pParms(2).Value = p_USER
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@bHardClose", SqlDbType.Bit)
        pParms(4).Value = p_bHardClose
        pParms(5) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(5).Value = p_STU_BSU_ID

        Dim cmd As New SqlCommand
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))
        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))

        cmd.Connection = p_conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FEES.F_GenerateCollectionVoucher_Provider"
        cmd.CommandTimeout = 0
        Dim retval As Integer
        retval = cmd.ExecuteNonQuery
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_GenerateCollectionVoucher", pParms)
        F_GenerateCollectionVoucher_Provider_Transport = pParms(3).Value
    End Function

End Class
