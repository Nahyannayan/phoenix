Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration


Public Class FEEConcessionTransactionBB

    Dim vFCH_ID As Integer
    Dim vFCH_ACD_ID As Integer
    Dim vFCH_STU_ID As Integer
    Dim vFCH_FCM_ID As Integer
    Dim vFCH_REF_ID As Integer
    Dim vFCH_REF_NAME As String
    Dim vFCH_DT As Date
    Dim vFCH_DTFROM As Date
    Dim vFCH_DTTO As Date
    Dim vFCH_BSU_ID As String
    Dim vFCH_STU_BSU_ID As String
    Dim vSTU_NAME As String
    Dim vFCH_REMARKS As String
    Dim vFCH_DRCR As String
    Dim vFCH_FCH_ID As String
    Dim vFCH_SBL_ID As String
    Dim vFCH_SBL_DESCR As String
    Dim vFCH_bDELETE As Boolean
    Public bEdit As Boolean
    Dim arrSubList As ArrayList

    Public Property SUB_DETAILS() As ArrayList
        Get
            Return arrSubList
        End Get
        Set(ByVal value As ArrayList)
            arrSubList = value
        End Set
    End Property

    Public Property FCH_REMARKS() As String
        Get
            Return vFCH_REMARKS
        End Get
        Set(ByVal value As String)
            vFCH_REMARKS = value
        End Set
    End Property

    Public Property FCH_SBL_ID() As String
        Get
            Return vFCH_SBL_ID
        End Get
        Set(ByVal value As String)
            vFCH_SBL_ID = value
        End Set
    End Property

    Public Property FCH_SBL_DESCR() As String
        Get
            Return vFCH_SBL_DESCR
        End Get
        Set(ByVal value As String)
            vFCH_SBL_DESCR = value
        End Set
    End Property

    Public Property FCH_DRCR() As String
        Get
            Return vFCH_DRCR
        End Get
        Set(ByVal value As String)
            vFCH_DRCR = value
        End Set
    End Property

    Public Property FCH_FCH_ID() As String
        Get
            Return vFCH_FCH_ID
        End Get
        Set(ByVal value As String)
            vFCH_FCH_ID = value
        End Set
    End Property

    Public Property STU_NAME() As String
        Get
            Return vSTU_NAME
        End Get
        Set(ByVal value As String)
            vSTU_NAME = value
        End Set
    End Property

    Public Property FCH_BSU_ID() As String
        Get
            Return vFCH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFCH_BSU_ID = value
        End Set
    End Property

    Public Property FCH_STU_BSU_ID() As String
        Get
            Return vFCH_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            vFCH_STU_BSU_ID = value
        End Set
    End Property

    Public Property FCH_DTTO() As Date
        Get
            Return vFCH_DTTO
        End Get
        Set(ByVal value As Date)
            vFCH_DTTO = value
        End Set
    End Property

    Public Property FCH_DTFROM() As Date
        Get
            Return vFCH_DTFROM
        End Get
        Set(ByVal value As Date)
            vFCH_DTFROM = value
        End Set
    End Property

    Public Property FCH_DT() As Date
        Get
            Return vFCH_DT
        End Get
        Set(ByVal value As Date)
            vFCH_DT = value
        End Set
    End Property

    Public Property FCH_REF_ID() As Integer
        Get
            Return vFCH_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_REF_ID = value
        End Set
    End Property

    Public Property FCH_REF_NAME() As String
        Get
            Return vFCH_REF_NAME
        End Get
        Set(ByVal value As String)
            vFCH_REF_NAME = value
        End Set
    End Property

    Public Property FCH_FCM_ID() As Integer
        Get
            Return vFCH_FCM_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_FCM_ID = value
        End Set
    End Property

    Public Property FCH_STU_ID() As Integer
        Get
            Return vFCH_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_STU_ID = value
        End Set
    End Property

    Public Property FCH_ACD_ID() As Integer
        Get
            Return vFCH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_ACD_ID = value
        End Set
    End Property

    Public Property FCH_ID() As Integer
        Get
            Return vFCH_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_ID = value
        End Set
    End Property
    ''' <summary>
    ''' Function used to Populate the Concession Type Details to the 
    ''' Screen.It Returns a data table containing all Fee type 
    ''' from the Concession Type master Table FEE_CONCESSIONTYPE_M
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function PopulateConcessionType() As DataTable
        Dim dsData As DataSet
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "select FCT_ID, FCT_DESCR from FEES.FEE_CONCESSIONTYPE_M WITH(NOLOCK) ORDER BY FCT_DESCR"
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    ''' <summary>
    ''' Function used to Populate the FEE Types which is allowed for concession.
    '''
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function GetConcessionFEEType() As DataTable
        Dim dsData As DataSet
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT FEE_ID, FEE_DESCR FROM FEES.FEES_M WITH(NOLOCK) WHERE FEE_bConcession = 1 "
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetConcessionHistory(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String) As DataTable
        Dim sql_query As String = " SELECT  REPLACE(CONVERT(VARCHAR(11),FCH.FCH_DT, 113), ' ', '/') as FCH_DT , FCD.FCD_AMOUNT, FCM.FCM_DESCR, FEE.FEE_DESCR" _
            & " FROM FEES.FEE_CONCESSION_H AS FCH INNER JOIN" _
            & " FEES.FEE_CONCESSION_D AS FCD ON FCH.FCH_ID = FCD.FCD_FCH_ID INNER JOIN" _
            & " VW_OSO_FEE_CONCESSION_M AS FCM ON FCD.FCD_FCM_ID = FCM.FCM_ID INNER JOIN" _
            & " VW_OSO_FEES_M AS FEE ON FCD.FCD_FEE_ID = FEE.FEE_ID" _
            & " WHERE     (FCH.FCH_ACD_ID = '" & ACD_ID & "') AND (FCH.FCH_STU_BSU_ID = '" & BSU_ID & "') " _
            & " AND (FCH.FCH_STU_ID = '" & STU_ID & "')"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASIS_TransportConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function RefRepetition(ByVal refType As ConcessionType, ByVal REF_ID As Integer) As Boolean
        Dim sql_query As String
        Dim count As Integer = 0
        sql_query = "SELECT COUNT(*) FROM  FEES.FEE_CONCESSION_H WHERE FCH_FCM_ID=" & _
                    refType & " AND FCH_REF_ID =" & REF_ID
        count = SqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings("OASIS_TransportConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function GetEmployeeBSU(ByVal EMP_ID As String, ByVal STU_ID As String) As String
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT  EMP.EMP_BSU_ID, STS.STS_STU_ID " & _
            " FROM EMPLOYEE_M AS EMP WITH(NOLOCK) LEFT OUTER JOIN STUDENT_D AS STS WITH(NOLOCK) ON STS.STS_EMP_ID = EMP.EMP_ID" & _
            " WHERE EMP.EMP_ID ='" & EMP_ID & "' "
            Dim BSUID = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query).ToString
            Return BSUID.ToString
        End Using
    End Function

    Public Shared Function CheckPreviousConcession(ByVal ACD_ID As Integer, ByVal BSU_ID As String, _
    ByVal STU_ID As String, ByVal FCH_ID As String) As Boolean
        Dim sql_query As String
        Dim count As Integer = 0
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            sql_query = "SELECT COUNT(FCT.FCT_ID) FROM VW_OSO_FEE_CONCESSIONTYPE_M AS FCT WITH(NOLOCK) INNER JOIN " & _
            " VW_OSO_FEE_CONCESSION_M AS FCM WITH(NOLOCK) ON FCT.FCT_ID = FCM.FCM_FCT_ID INNER JOIN" & _
            " FEES.FEE_CONCESSION_H AS FCH WITH(NOLOCK) ON FCM.FCM_ID = FCH.FCH_FCM_ID " & _
            " WHERE FCH_ACD_ID='" & ACD_ID & "' and FCH_ID <> '" & FCH_ID & "' " & _
            " AND FCH_STU_BSU_ID ='" & BSU_ID & "' AND FCH_STU_ID='" & STU_ID & "'" & _
            " AND ISNULL(FCH_FCH_ID,0)=0 AND FCH_ID NOT IN (SELECT FCH_ID FROM FEES.FEE_CONCESSION_H)"
            count = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function
    ''' <summary>
    ''' This function checks whether entered ReferenceID is valid one or not.
    ''' It 
    ''' </summary>
    ''' <param name="refType"></param>
    ''' <param name="ID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ValidateReference(ByVal refType As ConcessionType, ByVal ID As String, ByVal BSU_ID As String) As Boolean
        Dim sql_query As String
        Dim count As Integer = 0
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Select Case refType
                Case ConcessionType.Sibling
                    sql_query = "SELECT count(STU_ID) FROM STUDENT_M WITH(NOLOCK) WHERE STU_bActive = 1 " & _
                    " AND (STU_ID='" & ID & "' OR STU_FEE_ID='" & ID & "') "
                    count = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
                Case ConcessionType.Staff
                    sql_query = "SELECT count(EMP_ID) FROM  oasis..EMPLOYEE_M WITH(NOLOCK) WHERE EMP_RESGDT is null" & _
                    " AND (EMP_ID='" & ID & "' OR EMPNO='" & ID & "') "
                    count = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
            End Select
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    Public Shared Function GetDetails(ByVal FCH_ID As Integer, ByVal type As Int32, ByVal BSU_ID As String, _
    ByVal STU_BSU_ID As String, ByVal bCancel As Boolean, ByVal DataMode As String) As FEEConcessionTransactionBB
        Dim vFEE_CON As New FEEConcessionTransactionBB
        'Dim sql_query As String
        Dim cmd As New SqlCommand
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            cmd = New SqlCommand("[FEES].[F_GETFEECONCESSIONTRANSVIEW]", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            sqlpSTU_BSU_ID.Value = STU_BSU_ID
            cmd.Parameters.Add(sqlpSTU_BSU_ID)

            Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
            sqlpFCH_ID.Value = FCH_ID
            cmd.Parameters.Add(sqlpFCH_ID)

            'Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
            'sqlpACD_ID.Value = ACD_ID
            'cmd.Parameters.Add(sqlpACD_ID)
            Dim sqlpGET_ALL As New SqlParameter("@GET_ALL", SqlDbType.Bit)
            Select Case type
                Case 1
                    sqlpGET_ALL.Value = False
                Case 2
                    sqlpGET_ALL.Value = True
            End Select
            cmd.Parameters.Add(sqlpGET_ALL)
            Dim drReader As SqlDataReader = cmd.ExecuteReader()
            'Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                vFEE_CON.FCH_ACD_ID = drReader("FCH_ACD_ID")
                vFEE_CON.FCH_BSU_ID = BSU_ID
                vFEE_CON.FCH_DT = drReader("FCH_DT")
                vFEE_CON.FCH_DTFROM = drReader("FCH_DTFROM")
                vFEE_CON.FCH_DTTO = drReader("FCH_DTTO")
                vFEE_CON.FCH_FCM_ID = drReader("FCH_FCM_ID")
                vFEE_CON.FCH_ID = drReader("FCH_ID")
                vFEE_CON.FCH_REF_ID = drReader("FCH_REF_ID")
                vFEE_CON.FCH_REF_NAME = drReader("REF_NAME").ToString
                vFEE_CON.FCH_REMARKS = drReader("FCH_REMARKS")
                vFEE_CON.FCH_STU_ID = drReader("STU_ID")
                vFEE_CON.STU_NAME = drReader("STU_NAME")
                vFEE_CON.SUB_DETAILS = PopulateSubDetails(drReader("FCH_ID"))
                vFEE_CON.FCH_SBL_ID = drReader("SBL_ID")
                vFEE_CON.FCH_SBL_DESCR = drReader("SBL_DESCRIPTION")
                'vFEE_CON.FCH_STU_ID = drReader("STU_NAME")
                If DataMode = "view" Then
                    vFEE_CON.FCH_FCH_ID = drReader("FCH_FCH_ID")
                End If

                For Each vFEE_CON_SUB As FEE_CONC_TRANC_SUB_BB In vFEE_CON.SUB_DETAILS
                    vFEE_CON_SUB.SubList_Monthly = PopulateSubDetails_MONTHLY(vFEE_CON_SUB.FCD_ID, bCancel)
                Next
            End While
        End Using
        Return vFEE_CON
    End Function

    Public Shared Function GetDetails(ByVal FCH_ID As Integer, ByVal type As Int32, ByVal BSU_ID As String, _
 ByVal STU_BSU_ID As String, ByVal bCancel As Boolean) As FEEConcessionTransactionBB
        Dim vFEE_CON As New FEEConcessionTransactionBB
        'Dim sql_query As String
        Dim cmd As New SqlCommand
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            cmd = New SqlCommand("[FEES].[F_GETFEECONCESSIONTRANSVIEW]", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            sqlpSTU_BSU_ID.Value = STU_BSU_ID
            cmd.Parameters.Add(sqlpSTU_BSU_ID)

            Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
            sqlpFCH_ID.Value = FCH_ID
            cmd.Parameters.Add(sqlpFCH_ID)

            'Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
            'sqlpACD_ID.Value = ACD_ID
            'cmd.Parameters.Add(sqlpACD_ID)
            Dim sqlpGET_ALL As New SqlParameter("@GET_ALL", SqlDbType.Bit)
            Select Case type
                Case 1
                    sqlpGET_ALL.Value = False
                Case 2
                    sqlpGET_ALL.Value = True
            End Select
            cmd.Parameters.Add(sqlpGET_ALL)
            Dim drReader As SqlDataReader = cmd.ExecuteReader()
            'Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                vFEE_CON.FCH_ACD_ID = drReader("FCH_ACD_ID")
                vFEE_CON.FCH_BSU_ID = BSU_ID
                vFEE_CON.FCH_DT = drReader("FCH_DT")
                vFEE_CON.FCH_DTFROM = drReader("FCH_DTFROM")
                vFEE_CON.FCH_DTTO = drReader("FCH_DTTO")
                vFEE_CON.FCH_FCM_ID = drReader("FCH_FCM_ID")
                vFEE_CON.FCH_ID = drReader("FCH_ID")

                vFEE_CON.FCH_REF_ID = drReader("FCH_REF_ID")
                vFEE_CON.FCH_REF_NAME = drReader("REF_NAME").ToString
                vFEE_CON.FCH_REMARKS = drReader("FCH_REMARKS")
                vFEE_CON.FCH_STU_ID = drReader("STU_ID")
                vFEE_CON.STU_NAME = drReader("STU_NAME")
                vFEE_CON.SUB_DETAILS = PopulateSubDetails(drReader("FCH_ID"))
                vFEE_CON.FCH_SBL_ID = drReader("SBL_ID")
                vFEE_CON.FCH_SBL_DESCR = drReader("SBL_DESCRIPTION")
                'vFEE_CON.FCH_STU_ID = drReader("STU_NAME")
                'If DataMode = "view" Then
                '    vFEE_CON.FCH_FCH_ID = drReader("FCH_FCH_ID")
                'End If

                For Each vFEE_CON_SUB As FEE_CONC_TRANC_SUB_BB In vFEE_CON.SUB_DETAILS
                    vFEE_CON_SUB.SubList_Monthly = PopulateSubDetails_MONTHLY(vFEE_CON_SUB.FCD_ID, bCancel)
                Next
            End While
        End Using
        Return vFEE_CON
    End Function



    Public Shared Function PopulateSubDetails_MONTHLY(ByVal FCD_ID As Integer, ByVal bCancel As Boolean) As ArrayList
        Dim arrDetList As New ArrayList
        Dim sql_query As String
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            sql_query = "SELECT FMD.FMD_ID, FMD.FMD_FCD_ID, FMD.FMD_REF_ID, REPLACE(CONVERT(VARCHAR(11), FMD.FMD_DATE, 113), ' ', '/') AS FMD_DATE," _
           & " FMD.FMD_AMOUNT, FMD.FMD_ORG_AMOUNT, FCD.FCD_REF_ID, DESCR.DESCR" _
           & " FROM FEES.FEE_CONCESSIONMONTHLY_D AS FMD " _
           & " INNER JOIN FEES.FEE_CONCESSION_D AS FCD ON FMD.FMD_FCD_ID = FCD.FCD_ID " _
           & " LEFT OUTER JOIN (SELECT TRM_ID AS ID, 2 AS SCH, TRM_DESCRIPTION AS DESCR" _
           & " FROM " & OASISConstants.dbOasis & ".dbo.TRM_M WITH(NOLOCK) UNION SELECT AMS_ID AS ID, 0 AS SCH, " _
           & " DATENAME(month, AMS_DTFROM) + '-' + LTRIM(STR(DATEPART(yyyy, AMS_DTFROM))) AS DESCR" _
           & " FROM " & OASISConstants.dbOasis & ".dbo.ACADEMIC_MonthS_S WITH(NOLOCK)) AS DESCR ON CASE FCD.FCD_SCH_ID WHEN 2 THEN 2 ELSE 0 END = DESCR.SCH AND FMD.FMD_REF_ID = DESCR.ID" _
           & " WHERE FMD.FMD_FCD_ID='" & FCD_ID & "'"
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                Dim vFEE_CON_SUB_MONTHLY As New FEE_CONC_TRANC_SUB_MONTHLY_BB
                If bCancel Then
                    vFEE_CON_SUB_MONTHLY.FMD_AMOUNT = drReader("FMD_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_ORG_AMOUNT = drReader("FMD_AMOUNT")
                Else
                    vFEE_CON_SUB_MONTHLY.FMD_ORG_AMOUNT = drReader("FMD_ORG_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_AMOUNT = drReader("FMD_AMOUNT")
                End If
                vFEE_CON_SUB_MONTHLY.FMD_DATE = drReader("FMD_DATE")
                vFEE_CON_SUB_MONTHLY.FMD_FCD_ID = FCD_ID
                vFEE_CON_SUB_MONTHLY.FMD_ID = drReader("FMD_ID")
                vFEE_CON_SUB_MONTHLY.FMD_REF_ID = drReader("FMD_REF_ID")
                vFEE_CON_SUB_MONTHLY.FMD_REF_NAME = drReader("DESCR")

                arrDetList.Add(vFEE_CON_SUB_MONTHLY)
            End While
        End Using
        Return arrDetList
    End Function

    Public Shared Function DuplicateFeeType(ByVal arrSubList As ArrayList, ByVal FEE_ID As Integer, ByVal S_FCD_ID As String) As Boolean
        Dim FCD_ID As Integer = 0
        If S_FCD_ID <> "" Then
            FCD_ID = CInt(S_FCD_ID)
        End If
        If arrSubList Is Nothing Then
            Return False
        End If
        Dim CON_SUB_LIST As FEE_CONC_TRANC_SUB_BB
        For i As Integer = 0 To arrSubList.Count - 1
            CON_SUB_LIST = arrSubList(i)
            If CON_SUB_LIST.FCD_FEE_ID = FEE_ID And FCD_ID <> CON_SUB_LIST.FCD_ID Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Shared Function PeriodBelongstoAcademicYear(ByVal FromDT As DateTime, ByVal ToDT As DateTime, ByVal ACD_ID As Integer, ByVal BSU_ID As String) As Boolean
        Dim drReader As SqlDataReader
        Dim vFromDT, vToDT As DateTime
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT ACD_STARTDT, ACD_ENDDT FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE ACD_BSU_ID = '" & BSU_ID & "' AND ACD_ID = " & ACD_ID
            drReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                vFromDT = drReader("ACD_STARTDT")
                vToDT = drReader("ACD_ENDDT")
                Exit While
            End While
        End Using
        If vFromDT <= FromDT AndAlso vToDT >= ToDT Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function PopulateSubDetails(ByVal FCH_ID As Integer) As ArrayList
        Dim arrDetList As New ArrayList
        Dim sql_query As String
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection

            sql_query = "SELECT FCD.FCD_ID, FCD.FCD_FCH_ID, FCD.FCD_FCM_ID, FCD.FCD_REF_ID, " _
           & " FCD.FCD_FEE_ID, FCD.FCD_AMTTYPE, FCD.FCD_AMOUNT, FEE.FEE_DESCR, FCM.FCM_DESCR, " _
           & " CASE FCD.FCD_FCM_ID WHEN 2 THEN ISNULL(EMP.EMP_FNAME, '') + ' ' + ISNULL(EMP.EMP_MNAME, '') " _
           & " + ' ' + ISNULL(EMP.EMP_LNAME, '') WHEN 1 THEN ISNULL(STU.STU_FIRSTNAME, '') " _
           & " + ' ' + ISNULL(STU.STU_MIDNAME, '') + ' ' + ISNULL(STU.STU_LASTNAME, '') END AS REF_NAME, " _
           & " FCD.FCD_SCH_ID FROM FEES.FEE_CONCESSION_D AS FCD LEFT OUTER JOIN " _
           & OASISConstants.dbFees & ".FEES.FEES_M AS FEE WITH(NOLOCK) ON FCD.FCD_FEE_ID = FEE.FEE_ID LEFT OUTER JOIN " _
           & OASISConstants.dbFees & ".FEES.FEE_CONCESSION_M AS FCM WITH(NOLOCK) ON FCD.FCD_FCM_ID = FCM.FCM_ID LEFT OUTER JOIN " _
           & OASISConstants.dbOasis & ".dbo.EMPLOYEE_M AS EMP WITH(NOLOCK) ON FCD.FCD_REF_ID = EMP.EMP_ID LEFT OUTER JOIN" _
           & " STUDENT_M AS STU WITH(NOLOCK) ON FCD.FCD_REF_ID = STU.STU_ID" _
           & " WHERE FCD.FCD_FCH_ID = " & FCH_ID
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB_BB
                vFEE_CON_SUB.FCD_AMOUNT = drReader("FCD_AMOUNT")
                vFEE_CON_SUB.FCD_AMTTYPE = drReader("FCD_AMTTYPE")
                vFEE_CON_SUB.FCD_FCH_ID = drReader("FCD_FCH_ID")
                vFEE_CON_SUB.FCD_FCM_ID = drReader("FCD_FCM_ID")
                vFEE_CON_SUB.FCD_FEE_ID = drReader("FCD_FEE_ID")
                vFEE_CON_SUB.FCD_ID = drReader("FCD_ID")
                vFEE_CON_SUB.FCD_REF_ID = drReader("FCD_REF_ID")
                vFEE_CON_SUB.FCD_REF_NAME = drReader("REF_NAME").ToString
                vFEE_CON_SUB.FCM_DESCR = drReader("FCM_DESCR").ToString
                vFEE_CON_SUB.FEE_DESCR = drReader("FEE_DESCR").ToString
                vFEE_CON_SUB.FCD_SCH_ID = drReader("FCD_SCH_ID").ToString
                arrDetList.Add(vFEE_CON_SUB)
            End While
        End Using
        Return arrDetList
    End Function

    Public Shared Function F_SaveFEE_CONCESSION_H(ByVal CONCESSION_DET As FEEConcessionTransactionBB, _
    ByRef NEW_FCH_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If CONCESSION_DET Is Nothing Then
            Return 426
        End If
        If CONCESSION_DET.SUB_DETAILS.Count = 0 Then
            Return 426
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSION_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFCH_ACD_ID As New SqlParameter("@FCH_ACD_ID", SqlDbType.VarChar, 20)
        sqlpFCH_ACD_ID.Value = CONCESSION_DET.FCH_ACD_ID
        cmd.Parameters.Add(sqlpFCH_ACD_ID)

        Dim sqlpFCH_BSU_ID As New SqlParameter("@FCH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCH_BSU_ID.Value = CONCESSION_DET.FCH_BSU_ID
        cmd.Parameters.Add(sqlpFCH_BSU_ID)

        Dim sqlpFCH_STU_BSU_ID As New SqlParameter("@FCH_STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCH_STU_BSU_ID.Value = CONCESSION_DET.FCH_STU_BSU_ID
        cmd.Parameters.Add(sqlpFCH_STU_BSU_ID)

        Dim sqlpFCH_STU_ID As New SqlParameter("@FCH_STU_ID", SqlDbType.BigInt)
        sqlpFCH_STU_ID.Value = CONCESSION_DET.FCH_STU_ID
        cmd.Parameters.Add(sqlpFCH_STU_ID)

        Dim sqlpFCH_DT As New SqlParameter("@FCH_DT", SqlDbType.DateTime)
        sqlpFCH_DT.Value = CONCESSION_DET.FCH_DT
        cmd.Parameters.Add(sqlpFCH_DT)

        Dim sqlpFCH_DTFROM As New SqlParameter("@FCH_DTFROM", SqlDbType.DateTime)
        sqlpFCH_DTFROM.Value = CONCESSION_DET.FCH_DTFROM
        cmd.Parameters.Add(sqlpFCH_DTFROM)

        Dim sqlpFCH_DTTO As New SqlParameter("@FCH_DTTO", SqlDbType.DateTime)
        sqlpFCH_DTTO.Value = CONCESSION_DET.FCH_DTTO
        cmd.Parameters.Add(sqlpFCH_DTTO)

        Dim sqlpFCH_FCM_ID As New SqlParameter("@FCH_FCM_ID", SqlDbType.Int)
        sqlpFCH_FCM_ID.Value = CONCESSION_DET.FCH_FCM_ID
        cmd.Parameters.Add(sqlpFCH_FCM_ID)

        Dim sqlpFCH_REF_ID As New SqlParameter("@FCH_REF_ID", SqlDbType.Int)
        sqlpFCH_REF_ID.Value = CONCESSION_DET.FCH_REF_ID
        cmd.Parameters.Add(sqlpFCH_REF_ID)

        Dim sqlpFCH_REMARKS As New SqlParameter("@FCH_REMARKS", SqlDbType.VarChar, 200)
        sqlpFCH_REMARKS.Value = CONCESSION_DET.FCH_REMARKS
        cmd.Parameters.Add(sqlpFCH_REMARKS)

        Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
        If CONCESSION_DET.bEdit Then
            sqlpFCH_ID.Value = CONCESSION_DET.FCH_ID
        Else
            sqlpFCH_ID.Value = 0
        End If

        cmd.Parameters.Add(sqlpFCH_ID)
        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = False
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpNEW_FCH_ID As New SqlParameter("@NEW_FCH_ID", SqlDbType.Int)
        sqlpNEW_FCH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEW_FCH_ID)

        Dim sqlpFCH_DRCR As New SqlParameter("@FCH_DRCR", SqlDbType.VarChar, 200)
        sqlpFCH_DRCR.Value = CONCESSION_DET.FCH_DRCR
        cmd.Parameters.Add(sqlpFCH_DRCR)

        Dim sqlpFCH_SBL_ID As New SqlParameter("@FCH_SBL_ID", SqlDbType.Int)
        sqlpFCH_SBL_ID.Value = CONCESSION_DET.FCH_SBL_ID
        cmd.Parameters.Add(sqlpFCH_SBL_ID)

        Dim sqlpFCH_FCH_ID As New SqlParameter("@FCH_FCH_ID", SqlDbType.VarChar, 200)
        If CONCESSION_DET.FCH_FCH_ID Is Nothing Then
            sqlpFCH_FCH_ID.Value = System.DBNull.Value
        Else
            sqlpFCH_FCH_ID.Value = CONCESSION_DET.FCH_FCH_ID
        End If
        cmd.Parameters.Add(sqlpFCH_FCH_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        NEW_FCH_ID = sqlpNEW_FCH_ID.Value
        iReturnvalue = F_SaveFEE_CONCESSION_D(CONCESSION_DET.SUB_DETAILS, NEW_FCH_ID, CONCESSION_DET.FCH_SBL_ID, conn, trans, False) ' CONCESSION_DET.bEdit)---modified by jacob on 19/Mar/2020 to handle editing
        If iReturnvalue = 0 Then
            iReturnvalue = FEEConcessionTransactionBB.UpdateConcessionActualAmount(NEW_FCH_ID, conn, trans)
        End If
        Return iReturnvalue
    End Function

    Private Shared Function F_SaveFEE_CONCESSION_D(ByVal arrSUBList As ArrayList, ByVal FCH_ID As Integer, _
    ByVal p_SBL_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal bEdit As Boolean = False) As Integer
        If arrSUBList Is Nothing Then
            Return 426
        End If
        If arrSUBList.Count = 0 Then
            Return 426
        End If
        Dim CON_SUB_LIST As FEE_CONC_TRANC_SUB_BB
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        For i As Integer = 0 To arrSUBList.Count - 1
            CON_SUB_LIST = arrSUBList(i)

            cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSION_D]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFCD_FCH_ID As New SqlParameter("@FCD_FCH_ID", SqlDbType.Int)
            sqlpFCD_FCH_ID.Value = FCH_ID
            cmd.Parameters.Add(sqlpFCD_FCH_ID)

            Dim sqlpFCD_FCM_ID As New SqlParameter("@FCD_FCM_ID", SqlDbType.Int)
            sqlpFCD_FCM_ID.Value = CON_SUB_LIST.FCD_FCM_ID
            cmd.Parameters.Add(sqlpFCD_FCM_ID)

            Dim sqlpFCD_REF_ID As New SqlParameter("@FCD_REF_ID", SqlDbType.Int)
            sqlpFCD_REF_ID.Value = CON_SUB_LIST.FCD_REF_ID
            cmd.Parameters.Add(sqlpFCD_REF_ID)

            Dim sqlpFCD_FEE_ID As New SqlParameter("@FCD_FEE_ID", SqlDbType.Int)
            sqlpFCD_FEE_ID.Value = CON_SUB_LIST.FCD_FEE_ID
            cmd.Parameters.Add(sqlpFCD_FEE_ID)

            Dim sqlpFCD_AMTTYPE As New SqlParameter("@FCD_AMTTYPE", SqlDbType.SmallInt)
            sqlpFCD_AMTTYPE.Value = CON_SUB_LIST.FCD_AMTTYPE
            cmd.Parameters.Add(sqlpFCD_AMTTYPE)

            Dim sqlpFCD_AMOUNT As New SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFCD_AMOUNT.Value = CON_SUB_LIST.FCD_AMOUNT
            cmd.Parameters.Add(sqlpFCD_AMOUNT)

            Dim sqlpFCD_ACTUALAMT As New SqlParameter("@FCD_ACTUALAMT", SqlDbType.Decimal, 21)
            sqlpFCD_ACTUALAMT.Value = CON_SUB_LIST.FCD_ACTUALAMT
            cmd.Parameters.Add(sqlpFCD_ACTUALAMT)

            Dim sqlpFCD_SCH_ID As New SqlParameter("@FCD_SCH_ID", SqlDbType.Int, 21)
            sqlpFCD_SCH_ID.Value = CON_SUB_LIST.FCD_SCH_ID
            cmd.Parameters.Add(sqlpFCD_SCH_ID)

            Dim sqlpFCD_REF_BSU_ID As New SqlParameter("@FCD_REF_BSU_ID", SqlDbType.VarChar, 20)
            If CON_SUB_LIST.FCD_REF_BSU_ID Is Nothing Then
                sqlpFCD_REF_BSU_ID.Value = ""
            Else
                sqlpFCD_REF_BSU_ID.Value = CON_SUB_LIST.FCD_REF_BSU_ID
            End If
            cmd.Parameters.Add(sqlpFCD_REF_BSU_ID)

            Dim sqlpFCD_ID As New SqlParameter("@FCD_ID", SqlDbType.Int)
            If bEdit Then
                sqlpFCD_ID.Value = CON_SUB_LIST.FCD_ID
            Else
                sqlpFCD_ID.Value = 0
            End If


            cmd.Parameters.Add(sqlpFCD_ID)

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = False
            cmd.Parameters.Add(sqlpbDelete)

            Dim sqlpFCH_SBL_ID As New SqlParameter("@FCD_SBL_ID", SqlDbType.Int)
            sqlpFCH_SBL_ID.Value = p_SBL_ID
            cmd.Parameters.Add(sqlpFCH_SBL_ID)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            'Get the New FCD ID (change the S.P so that it returns the New FCD_ID
            Dim sqlpNEW_FCD_ID As New SqlParameter("@NEW_FCD_ID", SqlDbType.Int)
            sqlpNEW_FCD_ID.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpNEW_FCD_ID)
            Dim vNew_FCD_ID As Integer

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue = 0 Then
                vNew_FCD_ID = sqlpNEW_FCD_ID.Value
                iReturnvalue = F_SaveFEE_CONCESSIONMONTHLY_D(CON_SUB_LIST.SubList_Monthly, vNew_FCD_ID, conn, trans, bEdit)
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                End If
            Else
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function F_SaveFEE_CONCESSIONMONTHLY_D(ByVal arrSUBList As ArrayList, _
    ByVal FCD_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal bEdit As Boolean = False) As Integer
        If arrSUBList Is Nothing Then
            Return 426
        End If
        If arrSUBList.Count = 0 Then
            Return 426
        End If
        Dim CON_SUB_LIST_MONTHLY As FEE_CONC_TRANC_SUB_MONTHLY_BB
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        For i As Integer = 0 To arrSUBList.Count - 1
            CON_SUB_LIST_MONTHLY = arrSUBList(i)
            cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSIONMONTHLY_D]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFMD_FCD_ID As New SqlParameter("@FMD_FCD_ID", SqlDbType.Int)
            sqlpFMD_FCD_ID.Value = FCD_ID
            cmd.Parameters.Add(sqlpFMD_FCD_ID)

            Dim sqlpFMD_REF_ID As New SqlParameter("@FMD_REF_ID", SqlDbType.Int)
            sqlpFMD_REF_ID.Value = CON_SUB_LIST_MONTHLY.FMD_REF_ID
            cmd.Parameters.Add(sqlpFMD_REF_ID)

            Dim sqlpFMD_AMOUNT As New SqlParameter("@FMD_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_AMOUNT.Value = CON_SUB_LIST_MONTHLY.FMD_AMOUNT
            cmd.Parameters.Add(sqlpFMD_AMOUNT)

            Dim sqlpFMD_ORG_AMOUNT As New SqlParameter("@FMD_ORG_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_ORG_AMOUNT.Value = CON_SUB_LIST_MONTHLY.FMD_ORG_AMOUNT
            cmd.Parameters.Add(sqlpFMD_ORG_AMOUNT)

            Dim sqlpFMD_DATE As New SqlParameter("@FMD_DATE", SqlDbType.DateTime)
            sqlpFMD_DATE.Value = CON_SUB_LIST_MONTHLY.FMD_DATE
            cmd.Parameters.Add(sqlpFMD_DATE)

            Dim sqlpFMD_ID As New SqlParameter("@FMD_ID", SqlDbType.Int)
            If bEdit Then
                sqlpFMD_ID.Value = CON_SUB_LIST_MONTHLY.FMD_ID
            Else
                sqlpFMD_ID.Value = 0
            End If

            cmd.Parameters.Add(sqlpFMD_ID)

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = False
            cmd.Parameters.Add(sqlpbDelete)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function UpdateConcessionActualAmount(ByVal FCH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[UpdateConcessionActualAmount]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFMD_FCD_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
        sqlpFMD_FCD_ID.Value = FCH_ID
        cmd.Parameters.Add(sqlpFMD_FCD_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        Return retSValParam.Value
    End Function

    Public Shared Function GetReference(ByVal refType As ConcessionType, ByVal ID As String, ByVal BSU_ID As String) As String
        Dim sql_query As String
        Dim objName As Object
        Select Case refType
            Case ConcessionType.Sibling
                sql_query = "SELECT ISNULL(STU_FIRSTNAME, '')+' ' + ISNULL(STU_MIDNAME, '')+' ' + ISNULL(STU_LASTNAME, '') AS STU_NAME FROM STUDENT_M WITH(NOLOCK) WHERE ISNULL(STU_bActive, 0) = 1 " & _
                " AND (STU_ID='" & ID & "' ) "
                objName = SqlHelper.ExecuteScalar(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
                CommandType.Text, sql_query)
            Case ConcessionType.Staff
                sql_query = "SELECT ISNULL(EMP_FNAME, '')+' ' + ISNULL(EMP_MNAME, '')+' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME FROM EMPLOYEE_M WITH(NOLOCK) WHERE EMP_RESGDT is null" & _
                " AND (EMP_ID='" & ID & "' ) "
                objName = SqlHelper.ExecuteScalar(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, CommandType.Text, sql_query)
            Case Else
                Return ""
        End Select
        If objName Is Nothing Then
            Return ""
        End If
        Return objName.ToString
    End Function

    Public Shared Function PrintConcession(ByVal IntFCH_ID As Integer, ByVal BSU_ID As String, _
    ByVal USR_NAME As String) As MyReportClass
        Dim strSql As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim ds As DataSet
        Dim cmd As New SqlCommand
        Dim strCurID As String = ""
        Dim strCurDesc As String = ""
        Dim strCurDeno As String = ""
        Dim strSTU_BSUID As String
        Dim strBSUName As String
        Dim repSource As New MyReportClass
        strSql = "SELECT * FROM VW_OST_CONCESSION WHERE FCH_ID=" & IntFCH_ID & ""
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            Dim params As New Hashtable
            strSTU_BSUID = ds.Tables(0).Rows(0)("FCH_STU_BSU_ID")
            strBSUName = GetStudentBSU(strSTU_BSUID)
            params("STU_BSU") = strBSUName
            params("UserName") = USR_NAME
            params("ReportName") = "TRANSPORT FEE CONCESSION VOUCHER"
            repSource.Parameter = params
            cmd.CommandText = strSql
            repSource.Command = cmd

            Dim repSourceSubRep(0) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            ''''
            Dim cmdHeader As New SqlCommand("getBsuInFoWithImage_Provider", New SqlConnection(ConnectionManger.GetOASISConnectionString))
            cmdHeader.CommandType = CommandType.StoredProcedure
            cmdHeader.Parameters.AddWithValue("@IMG_STU_BSU_ID", strSTU_BSUID)
            cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
            cmdHeader.Parameters.AddWithValue("@IMG_PROVIDER_BSU_ID", BSU_ID)
            repSourceSubRep(0).Command = cmdHeader
            repSource.SubReport = repSourceSubRep
            If BSU_ID <> "" Then
                Dim str_sql As String
                Dim dsCur As DataSet
                Dim strCon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                str_sql = "SELECT vw_OSO_BUSINESSUNIT_M.BSU_ID, CURRENCY_M.CUR_ID, " _
                & " CURRENCY_M.CUR_DESCR, CURRENCY_M.CUR_DENOMINATION FROM CURRENCY_M " _
                & " CROSS JOIN vw_OSO_BUSINESSUNIT_M where bsu_id='" & BSU_ID & "' AND CUR_ID='AED'"
                dsCur = SqlHelper.ExecuteDataset(strCon, CommandType.Text, str_sql)
                If dsCur.Tables(0).Rows.Count > 0 Then
                    strCurID = dsCur.Tables(0).Rows(0)("CUR_ID")
                    strCurDesc = dsCur.Tables(0).Rows(0)("CUR_DESCR")
                    strCurDeno = dsCur.Tables(0).Rows(0)("CUR_DENOMINATION")
                End If
            End If
            params("CurrID") = strCurID
            params("CurrDesc") = strCurDesc
            params("CurrDeno") = strCurDeno
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTransportFeeConcessionReport.rpt"
            repSource.IncludeBSUImage = False
        End If
        Return repSource
    End Function
    Public Shared Function PrintConcessionCancel(ByVal IntFCH_ID As Integer, ByVal BSU_ID As String, _
   ByVal USR_NAME As String) As MyReportClass
        Dim strSql As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim ds As DataSet
        Dim cmd As New SqlCommand
        Dim strCurID As String = ""
        Dim strCurDesc As String = ""
        Dim strCurDeno As String = ""
        Dim strSTU_BSUID As String
        Dim strBSUName As String
        Dim repSource As New MyReportClass
        strSql = "SELECT * FROM VW_OST_CONCESSION WHERE FCH_ID=" & IntFCH_ID & ""
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            Dim params As New Hashtable
            strSTU_BSUID = ds.Tables(0).Rows(0)("FCH_STU_BSU_ID")
            strBSUName = GetStudentBSU(strSTU_BSUID)
            params("STU_BSU") = strBSUName
            params("UserName") = USR_NAME
            params("ReportName") = "TRANSPORT FEE CONCESSION VOUCHER"
            repSource.Parameter = params
            cmd.CommandText = strSql
            repSource.Command = cmd

            Dim repSourceSubRep(0) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            ''''
            Dim cmdHeader As New SqlCommand("getBsuInFoWithImage_Provider", New SqlConnection(ConnectionManger.GetOASISConnectionString))
            cmdHeader.CommandType = CommandType.StoredProcedure
            cmdHeader.Parameters.AddWithValue("@IMG_STU_BSU_ID", strSTU_BSUID)
            cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
            cmdHeader.Parameters.AddWithValue("@IMG_PROVIDER_BSU_ID", BSU_ID)
            repSourceSubRep(0).Command = cmdHeader
            repSource.SubReport = repSourceSubRep
            If BSU_ID <> "" Then
                Dim str_sql As String
                Dim dsCur As DataSet
                Dim strCon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                str_sql = "SELECT vw_OSO_BUSINESSUNIT_M.BSU_ID, CURRENCY_M.CUR_ID, " _
                & " CURRENCY_M.CUR_DESCR, CURRENCY_M.CUR_DENOMINATION FROM CURRENCY_M " _
                & " CROSS JOIN vw_OSO_BUSINESSUNIT_M where bsu_id='" & BSU_ID & "' AND CUR_ID='AED'"
                dsCur = SqlHelper.ExecuteDataset(strCon, CommandType.Text, str_sql)
                If dsCur.Tables(0).Rows.Count > 0 Then
                    strCurID = dsCur.Tables(0).Rows(0)("CUR_ID")
                    strCurDesc = dsCur.Tables(0).Rows(0)("CUR_DESCR")
                    strCurDeno = dsCur.Tables(0).Rows(0)("CUR_DENOMINATION")
                End If
            End If
            params("CurrID") = strCurID
            params("CurrDesc") = strCurDesc
            params("CurrDeno") = strCurDeno
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTransportFeeConcessionReportCancel.rpt"
            repSource.IncludeBSUImage = False
        End If
        Return repSource
    End Function
    Public Shared Function GetStudentBSU(ByVal STU_BSU As String) As String
        Dim str_query As String
        Dim dsBSU As DataSet
        Dim strBSU As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        str_query = "SELECT BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & STU_BSU & "'"
        dsBSU = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If dsBSU.Tables(0).Rows.Count > 0 Then
            strBSU = dsBSU.Tables(0).Rows(0)("BSU_NAME")
        End If
        Return strBSU
    End Function

    Public Shared Function DELETEFEE_CONCESSION_D_TRANSPORT(ByVal p_FCH_ID As String, _
    ByVal p_FCH_BSU_ID As String, ByVal p_User As String, _
    ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String
        Dim cmd As New SqlCommand("FEES.DELETEFEE_CONCESSION_D_TRANSPORT", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure
        ' @FCH_ID INT ,
        '@FCH_BSU_ID VARCHAR(20) ,
        '@USER VARCHAR(20) 
        Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
        sqlpFCH_ID.Value = p_FCH_ID
        cmd.Parameters.Add(sqlpFCH_ID)

        Dim sqlpFCH_BSU_ID As New SqlParameter("@FCH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCH_BSU_ID.Value = p_FCH_BSU_ID
        cmd.Parameters.Add(sqlpFCH_BSU_ID)

        Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 200)
        sqlpUSER.Value = p_User
        cmd.Parameters.Add(sqlpUSER)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)
        cmd.ExecuteNonQuery()
        Return retValParam.Value
    End Function

End Class

Public Class FEE_CONC_TRANC_SUB_BB

    Dim vFCD_ID As Integer
    Dim vFCD_FCH_ID As Integer
    Dim vFCD_FCM_ID As Integer
    Dim vFCD_REF_ID As Integer
    Dim vFCD_FEE_ID As Integer
    Dim vFCD_REF_NAME As String
    Dim vFEE_DESCR As String
    Dim vFCM_DESCR As String
    Dim vFCD_AMTTYPE As Int32
    Dim vFCD_AMOUNT As Decimal
    Dim vFCD_ACTUALAMT As Decimal
    Dim vFCD_SCH_ID As Integer
    Dim vFCD_REF_BSU_ID As String
    Dim arrSubList_Monthly As ArrayList

    Public Property SubList_Monthly() As ArrayList
        Get
            Return arrSubList_Monthly
        End Get
        Set(ByVal value As ArrayList)
            arrSubList_Monthly = value
        End Set
    End Property

    Public Property FCD_REF_BSU_ID() As String
        Get
            Return vFCD_REF_BSU_ID
        End Get
        Set(ByVal value As String)
            vFCD_REF_BSU_ID = value
        End Set
    End Property

    Public Property FCD_SCH_ID() As Integer
        Get
            Return vFCD_SCH_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_SCH_ID = value
        End Set
    End Property

    Public Property FCD_ACTUALAMT() As Decimal
        Get
            Return vFCD_ACTUALAMT
        End Get
        Set(ByVal value As Decimal)
            vFCD_ACTUALAMT = value
        End Set
    End Property

    Public Property FCD_AMOUNT() As Decimal
        Get
            Return vFCD_AMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFCD_AMOUNT = value
        End Set
    End Property

    Public Property FCD_AMTTYPE() As Int32
        Get
            Return vFCD_AMTTYPE
        End Get
        Set(ByVal value As Int32)
            vFCD_AMTTYPE = value
        End Set
    End Property

    Public Property FCM_DESCR() As String
        Get
            Return vFCM_DESCR
        End Get
        Set(ByVal value As String)
            vFCM_DESCR = value
        End Set
    End Property

    Public Property FEE_DESCR() As String
        Get
            Return vFEE_DESCR
        End Get
        Set(ByVal value As String)
            vFEE_DESCR = value
        End Set
    End Property

    Public Property FCD_FEE_ID() As Integer
        Get
            Return vFCD_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_FEE_ID = value
        End Set
    End Property

    Public Property FCD_REF_ID() As Integer
        Get
            Return vFCD_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_REF_ID = value
        End Set
    End Property

    Public Property FCD_REF_NAME() As String
        Get
            Return vFCD_REF_NAME
        End Get
        Set(ByVal value As String)
            vFCD_REF_NAME = value
        End Set
    End Property

    Public Property FCD_FCM_ID() As Integer
        Get
            Return vFCD_FCM_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_FCM_ID = value
        End Set
    End Property

    Public Property FCD_FCH_ID() As Integer
        Get
            Return vFCD_FCH_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_FCH_ID = value
        End Set
    End Property

    Public Property FCD_ID() As Integer
        Get
            Return vFCD_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_ID = value
        End Set
    End Property

End Class

Public Class FEE_CONC_TRANC_SUB_MONTHLY_BB
    Dim vFMD_ID As Integer
    Dim vFMD_FCD_ID As Integer
    Dim vFMD_REF_ID As Integer
    Dim vFMD_REF_NAME As String
    Dim vFMD_DATE As String
    Dim vFMD_AMOUNT As Decimal
    Dim vFMD_ORG_AMOUNT As Decimal

    Public Property FMD_ID() As Integer
        Get
            Return vFMD_ID
        End Get
        Set(ByVal value As Integer)
            vFMD_ID = value
        End Set
    End Property

    Public Property FMD_FCD_ID() As Integer
        Get
            Return vFMD_FCD_ID
        End Get
        Set(ByVal value As Integer)
            vFMD_FCD_ID = value
        End Set
    End Property

    Public Property FMD_REF_ID() As Integer
        Get
            Return vFMD_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFMD_REF_ID = value
        End Set
    End Property

    Public Property FMD_REF_NAME() As String
        Get
            Return vFMD_REF_NAME
        End Get
        Set(ByVal value As String)
            vFMD_REF_NAME = value
        End Set
    End Property

    Public Property FMD_AMOUNT() As Decimal
        Get
            Return vFMD_AMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFMD_AMOUNT = value
        End Set
    End Property

    Public Property FMD_DATE() As String
        Get
            Return vFMD_DATE
        End Get
        Set(ByVal value As String)
            vFMD_DATE = value
        End Set
    End Property

    Public Property FMD_ORG_AMOUNT() As Decimal
        Get
            Return vFMD_ORG_AMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFMD_ORG_AMOUNT = value
        End Set
    End Property

    Public Shared Function CreateDataTableFeeConcessionMonthly() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        ' FDD_DATE, FDD_AMOUNT, FDD_FIRSTMEMODT, FDD_SECONDMEMODT,FDD_THIRDMEMODT
        Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
        Dim cREF_Id As New DataColumn("REF_ID", System.Type.GetType("System.String"))
        Dim cFDD_DATE As New DataColumn("FDD_DATE", System.Type.GetType("System.String"))
        Dim cdescr As New DataColumn("DESCR", System.Type.GetType("System.String"))
        Dim cFDD_AMOUNT As New DataColumn("FDD_AMOUNT", System.Type.GetType("System.String"))
        Dim cCUR_AMOUNT As New DataColumn("CUR_AMOUNT", System.Type.GetType("System.String"))
        Dim cTAX_AMOUNT As New DataColumn("TAX_AMOUNT", System.Type.GetType("System.String"))
        Dim cNET_AMOUNT As New DataColumn("NET_AMOUNT", System.Type.GetType("System.String"))
        Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
        dtDt.Columns.Add(cId)
        dtDt.Columns.Add(cREF_Id)
        dtDt.Columns.Add(cFDD_DATE)
        dtDt.Columns.Add(cdescr)
        dtDt.Columns.Add(cFDD_AMOUNT)
        dtDt.Columns.Add(cCUR_AMOUNT)
        dtDt.Columns.Add(cTAX_AMOUNT)
        dtDt.Columns.Add(cNET_AMOUNT)
        dtDt.Columns.Add(cStatus)
        Return dtDt
    End Function

End Class