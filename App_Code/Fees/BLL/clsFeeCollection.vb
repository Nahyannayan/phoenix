﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Public Class clsFeeCollection

    Public Property STU_ID() As Long
    Public Property STU_TYPE() As String
    Public Property CRR_ID() As Integer
    Public Property CPM_ID() As Integer
    Public Property CLT_ID() As Integer
    Public Property FEE_ID() As Integer
    Public Property AMOUNT() As Double
    Public Property DISCOUNT() As Double
    Public Property ADMISSION_FEE() As Double
    Public Property OTHER_CHARGE() As Double
    Public Property DISCOUNT_XML() As Object
    Public Property DISCOUNT_DT() As DataTable
    Public Property BSU_ID() As String
    Public Property bOTH_DISC() As Boolean
    Public Property bGSTEnabled() As Boolean
    Public Property bFeesMulticurrency() As Boolean
    Public Property hfCobrandValue() As String
    Public Property DefaultCity() As String
    Public Property bFeeProtectionEnabled() As Boolean

    Public Sub GetDiscountedFeesPayable()
        Dim pParms(14) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt) '
        pParms(0).Value = STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(1).Value = STU_TYPE
        pParms(2) = New SqlClient.SqlParameter("@CRR_ID", SqlDbType.Int)
        pParms(2).Value = CRR_ID
        pParms(3) = New SqlClient.SqlParameter("@CLT_ID", SqlDbType.Int)
        pParms(3).Value = CLT_ID
        pParms(4) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.BigInt)
        pParms(4).Value = FEE_ID
        pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal, 21)
        pParms(5).Value = AMOUNT
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@Discount", SqlDbType.Decimal, 21)
        pParms(7).Direction = ParameterDirection.Output
        pParms(8) = New SqlClient.SqlParameter("@NEW_ADMISSION_FEE", SqlDbType.Decimal, 21)
        pParms(8).Value = ADMISSION_FEE
        pParms(9) = New SqlClient.SqlParameter("@SPLITUP_XML", SqlDbType.Xml, 100000)
        pParms(9).Direction = ParameterDirection.Output
        pParms(10) = New SqlClient.SqlParameter("@OTHER_CHARGE", SqlDbType.Decimal, 21)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@CPM_ID", SqlDbType.Int)
        pParms(11).Value = CPM_ID
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "GetDiscountedFeesPayable_COBRAND_NEW", pParms)
        If pParms(6).Value = 0 Then
            If Not pParms(7).Value Is Nothing And pParms(7).Value Is System.DBNull.Value Then
                DISCOUNT = 0
            Else
                DISCOUNT = pParms(7).Value
            End If

            If Not pParms(10).Value Is Nothing And pParms(10).Value Is System.DBNull.Value Then
                OTHER_CHARGE = 0
            Else
                OTHER_CHARGE = pParms(10).Value
            End If
        End If
        If Not pParms(9).Value Is DBNull.Value Then
            'ViewState("DiscntXML") = pParms(9).Value
            DISCOUNT_XML = pParms(9).Value
            Dim readXML As StringReader = New StringReader(HttpUtility.HtmlDecode(DISCOUNT_XML))
            Dim dsDiscnt As New DataSet
            dsDiscnt.ReadXml(readXML)
            DISCOUNT_DT = dsDiscnt.Tables(0).Copy
        End If
    End Sub
    Public Shared Function GetBusinessUnitsForCollection() As DataTable
        GetBusinessUnitsForCollection = Nothing
        Dim pParms(2) As SqlClient.SqlParameter
        Dim ds As New DataSet
        pParms(0) = New SqlClient.SqlParameter("@COUNTRY_ID", SqlDbType.Int) '
        pParms(0).Value = HttpContext.Current.Session("BSU_COUNTRY_ID")
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20) '
        pParms(1).Value = HttpContext.Current.Session("sBsuId")

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "FEES.GET_BUSINESSUNITS_FOR_COLLECTION", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            GetBusinessUnitsForCollection = ds.Tables(0)
        End If
    End Function
    Public Function GetEmiratesOrStates() As DataTable
        GetEmiratesOrStates = Nothing
        Dim sqlStr = "SELECT EMR_CODE, EMR_DESCR FROM dbo.EMIRATE_M WITH(NOLOCK) WHERE EMR_CTY_ID = " & HttpContext.Current.Session("BSU_COUNTRY_ID") & ""
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlStr)
        If Not ds Is Nothing Then
            GetEmiratesOrStates = ds.Tables(0)
        End If
    End Function
    Public Sub GetBusinessunitParameters(ByVal pBsuId As String)
        Dim sqlStr = "SELECT ISNULL(BSU_FEE_OTH_DISC,0) AS bOTH_DISC, ISNULL(BSU_IsTAXEnabled, 0) AS bGSTEnabled, " & _
                     "ISNULL(BSU_bFeesMulticurrency,0) AS bFeesMulticurrency, ISNULL(BSU_CITY, '') BSU_CITY, " & _
                     "ISNULL(BSU_bFeeProtectionEnabled, 0) BSU_bFeeProtectionEnabled " & _
                     "FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & pBsuId & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlStr)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            bOTH_DISC = CBool(ds.Tables(0).Rows(0)("bOTH_DISC"))
            bGSTEnabled = CBool(ds.Tables(0).Rows(0)("bGSTEnabled"))
            bFeesMulticurrency = CBool(ds.Tables(0).Rows(0)("bFeesMulticurrency"))
            DefaultCity = ds.Tables(0).Rows(0)("BSU_CITY").ToString
            bFeeProtectionEnabled = CBool(ds.Tables(0).Rows(0)("BSU_bFeeProtectionEnabled"))
        End If
    End Sub
    Public Sub GetCobrandvalue()
        Dim qry As String = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM dbo.CREDITCARD_S WITH(NOLOCK) INNER JOIN " & _
                                " dbo.CREDITCARD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
                                " dbo.CREDITCARD_PROVD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
                                " WHERE (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
        hfCobrandValue = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)
    End Sub
    Public Function GetCreditCardListForCollection(ByVal pBsuId As String, ByVal pCollectFrom As String) As DataTable
        GetCreditCardListForCollection = Nothing
        Dim pParms(2) As SqlClient.SqlParameter
        Dim ds As New DataSet
        pParms(0) = New SqlClient.SqlParameter("@CollectFrom", SqlDbType.VarChar, 10) '
        pParms(0).Value = pCollectFrom
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20) '
        pParms(1).Value = pBsuId

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.GetCreditCardListForCollection", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            GetCreditCardListForCollection = ds.Tables(0)
        End If
    End Function
    Public Shared Sub GetFPSData(ByVal pBsuId As String, ByVal pStuId As Long, ByVal pStuAcdId As Integer, ByVal pStuType As String, ByRef pShow As Boolean, ByRef pMessage As String)
        Dim pParms(4) As SqlClient.SqlParameter
        Dim ds As New DataSet
        pShow = False
        pMessage = ""
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = pStuId
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = pBsuId
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        pParms(2).Value = pStuType
        pParms(3) = New SqlClient.SqlParameter("@STU_ACD_ID", SqlDbType.Int)
        pParms(3).Value = pStuAcdId

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "dbo.GET_NEXTACADEMICPAYMENT_LIST", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            pShow = ds.Tables(0).Rows(0)("SHOWCHECK")
            pMessage = ds.Tables(0).Rows(0)("DISPLAYMESSAGE")
        Else
            pShow = False
            pMessage = ""
        End If
    End Sub
    Public Shared Function GetActivityDetails(ByVal pBsuId As String, ByVal pStuId As Long) As DataTable
        GetActivityDetails = Nothing
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = pStuId
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = pBsuId

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "OASIS.GET_ACTIVITY_LIST_FOR_PAYMENT", pParms)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            GetActivityDetails = ds.Tables(0)
        End If
    End Function
    Public Shared Function GetTAXAmount(ByVal pBsuId As String, ByVal pStuId As Long,
                                        ByVal pStuAcdId As Integer, ByVal pFeeId As Integer,
                                        ByVal pDate As String, ByVal pAmount As Double,
                                        ByVal pStuType As String) As Double
        GetTAXAmount = 0
        Try
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = pStuId
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = pBsuId
            pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
            pParms(2).Value = pStuAcdId
            pParms(3) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
            pParms(3).Value = pFeeId
            pParms(4) = New SqlClient.SqlParameter("@DATE", SqlDbType.Date)
            pParms(4).Value = pDate
            pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal)
            pParms(5).Value = pAmount
            pParms(6) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
            pParms(6).Value = pStuType
            pParms(7) = New SqlClient.SqlParameter("@TAX", SqlDbType.Decimal)
            pParms(7).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_TAX_FEECOLLECTION", pParms)
            GetTAXAmount = pParms(7).Value
        Catch ex As Exception

        End Try
    End Function
End Class
