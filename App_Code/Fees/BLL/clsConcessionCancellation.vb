﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic

Public Class clsConcessionCancellation
#Region "Public Variables"
    Dim vFCH_ID As Integer
    Dim vFCH_ACD_ID As Integer
    Dim vFCH_STU_ID As Integer
    Dim vFCH_FCM_ID As Integer
    Dim vFCH_REF_ID As Integer
    Dim vFCH_REF_NAME As String
    Dim vFCH_DT As Date
    Dim vFCH_DTFROM As Date
    Dim vFCH_DTTO As Date
    Dim vFCH_BSU_ID As String
    Dim vSTU_NAME As String
    Dim vFCH_REMARKS As String
    Dim vFCH_DRCR As String
    Dim vFCH_EMP_ID As String
    Dim vFCH_FCH_ID As String
    Dim vFCH_GRM_DISPLAY As String
    Dim vFCH_bDELETE As Boolean
    Dim vFCH_CANC_RECNO As String
    Dim vNextAcd_Concession As Boolean
    Dim vFCH_USER As String
    Dim vFCH_bPOSTED As Boolean
    Public bEdit As Boolean
    Dim vFCM_DESCR As String
    Dim vFCM_FCT_ID As Integer
    Dim vSTU_GRD_ID As String
    Dim vREF_NAME As String
    Dim vSTU_NO As String
    Dim vCONCESSION_D As DataTable
    Dim vCONCESSIONMONTHLY_D As DataTable
    Public Property FCH_REMARKS() As String
        Get
            Return vFCH_REMARKS
        End Get
        Set(ByVal value As String)
            vFCH_REMARKS = value
        End Set
    End Property
    Public Property CANC_RECNO() As String
        Get
            Return vFCH_CANC_RECNO
        End Get
        Set(ByVal value As String)
            vFCH_CANC_RECNO = value
        End Set
    End Property

    Public Property FCH_DRCR() As String
        Get
            Return vFCH_DRCR
        End Get
        Set(ByVal value As String)
            vFCH_DRCR = value
        End Set
    End Property

    Public Property FCH_EMP_ID() As String
        Get
            Return vFCH_EMP_ID
        End Get
        Set(ByVal value As String)
            vFCH_EMP_ID = value
        End Set
    End Property

    Public Property FCH_FCH_ID() As String
        Get
            Return vFCH_FCH_ID
        End Get
        Set(ByVal value As String)
            vFCH_FCH_ID = value
        End Set
    End Property

    Public Property STU_NAME() As String
        Get
            Return vSTU_NAME
        End Get
        Set(ByVal value As String)
            vSTU_NAME = value
        End Set
    End Property

    Public Property NextAcd_Concession() As Boolean
        Get
            Return vNextAcd_Concession
        End Get
        Set(ByVal value As Boolean)
            vNextAcd_Concession = value
        End Set
    End Property

    Public Property FCH_BSU_ID() As String
        Get
            Return vFCH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFCH_BSU_ID = value
        End Set
    End Property
    Public Property FCH_DTTO() As Date
        Get
            Return vFCH_DTTO
        End Get
        Set(ByVal value As Date)
            vFCH_DTTO = value
        End Set
    End Property
    Public Property FCH_DTFROM() As Date
        Get
            Return vFCH_DTFROM
        End Get
        Set(ByVal value As Date)
            vFCH_DTFROM = value
        End Set
    End Property

    Public Property FCH_DT() As Date
        Get
            Return vFCH_DT
        End Get
        Set(ByVal value As Date)
            vFCH_DT = value
        End Set
    End Property

    Public Property FCH_REF_ID() As Integer
        Get
            Return vFCH_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_REF_ID = value
        End Set
    End Property

    Public Property FCH_REF_NAME() As String
        Get
            Return vFCH_REF_NAME
        End Get
        Set(ByVal value As String)
            vFCH_REF_NAME = value
        End Set
    End Property

    Public Property GRM_DISPLAY() As String
        Get
            Return vFCH_GRM_DISPLAY
        End Get
        Set(ByVal value As String)
            vFCH_GRM_DISPLAY = value
        End Set
    End Property

    Public Property FCH_FCM_ID() As Integer
        Get
            Return vFCH_FCM_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_FCM_ID = value
        End Set
    End Property

    Public Property FCH_STU_ID() As Integer
        Get
            Return vFCH_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_STU_ID = value
        End Set
    End Property

    Public Property FCH_ACD_ID() As Integer
        Get
            Return vFCH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_ACD_ID = value
        End Set
    End Property

    Public Property FCH_ID() As Integer
        Get
            Return vFCH_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_ID = value
        End Set
    End Property

    Public Property FCH_USER() As String
        Get
            Return vFCH_USER
        End Get
        Set(ByVal value As String)
            vFCH_USER = value
        End Set
    End Property

    Public Property FCH_bPOSTED() As Boolean
        Get
            Return vFCH_bPOSTED
        End Get
        Set(ByVal value As Boolean)
            vFCH_bPOSTED = value
        End Set
    End Property
    Public Property FCM_DESCR() As String
        Get
            Return vFCM_DESCR
        End Get
        Set(ByVal value As String)
            vFCM_DESCR = value
        End Set
    End Property
    Public Property FCM_FCT_ID() As Integer
        Get
            Return vFCM_FCT_ID
        End Get
        Set(ByVal value As Integer)
            vFCM_FCT_ID = value
        End Set
    End Property
    Public Property STU_GRD_ID() As String
        Get
            Return vSTU_GRD_ID
        End Get
        Set(ByVal value As String)
            vSTU_GRD_ID = value
        End Set
    End Property
    Public Property REF_NAME() As String
        Get
            Return vREF_NAME
        End Get
        Set(ByVal value As String)
            vREF_NAME = value
        End Set
    End Property
    Public Property STU_NO() As String
        Get
            Return vSTU_NO
        End Get
        Set(ByVal value As String)
            vSTU_NO = value
        End Set
    End Property

    Public Property CONCESSION_D() As DataTable
        Get
            Return vCONCESSION_D
        End Get
        Set(ByVal value As DataTable)
            vCONCESSION_D = value
        End Set
    End Property
    Public Property CONCESSIONMONTHLY_D() As DataTable
        Get
            Return vCONCESSIONMONTHLY_D
        End Get
        Set(ByVal value As DataTable)
            vCONCESSIONMONTHLY_D = value
        End Set
    End Property
#End Region

    Public Sub GetConcesionDetails(ByVal BSU_ID As String, ByVal FCH_ID As Integer, Optional ByVal bGET_ALL As Boolean = False)
        Dim vFEE_CON As New FEEConcessionTransaction
        'Dim sql_query As String
        Dim cmd As New SqlCommand
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            cmd = New SqlCommand("[FEES].[F_GETFEECONCESSIONTRANSVIEW]", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)
            Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
            sqlpFCH_ID.Value = FCH_ID
            cmd.Parameters.Add(sqlpFCH_ID)
            'Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
            'sqlpACD_ID.Value = ACD_ID
            'cmd.Parameters.Add(sqlpACD_ID)
            Dim sqlpGET_ALL As New SqlParameter("@GET_ALL", SqlDbType.Bit)
            sqlpGET_ALL.Value = bGET_ALL
            cmd.Parameters.Add(sqlpGET_ALL)
            Dim drReader As SqlDataReader = cmd.ExecuteReader()
            'Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                FCH_ACD_ID = drReader("FCH_ACD_ID")
                FCH_BSU_ID = BSU_ID
                FCH_DT = drReader("FCH_DT")
                FCH_DTFROM = drReader("FCH_DTFROM")
                FCH_DTTO = drReader("FCH_DTTO")
                FCH_FCM_ID = drReader("FCH_FCM_ID")
                FCM_DESCR = drReader("FCM_DESCR").ToString
                FCM_FCT_ID = drReader("FCM_FCT_ID")
                FCH_ID = drReader("FCH_ID")
                FCH_REF_ID = drReader("FCH_REF_ID").ToString
                FCH_REF_NAME = drReader("REF_NAME").ToString
                FCH_REMARKS = drReader("FCH_REMARKS").ToString
                FCH_STU_ID = drReader("STU_ID")
                STU_NAME = drReader("STU_NAME").ToString
                'SUB_DETAILS = PopulateSubDetails(drReader("FCH_ID"))
                GRM_DISPLAY = drReader("GRM_DISPLAY").ToString
                FCH_EMP_ID = drReader("EMP_ID")
                FCH_FCH_ID = drReader("FCH_FCH_ID")
                CANC_RECNO = drReader("CANC_RECNO").ToString
                FCH_bPOSTED = drReader("FCH_bPosted")
                STU_GRD_ID = drReader("STU_GRD_ID").ToString
                REF_NAME = drReader("FCH_REF_NAME").ToString
                STU_NO = drReader("STU_NO").ToString
                'For Each vFEE_CON_SUB As FEE_CONC_TRANC_SUB In vFEE_CON.SUB_DETAILS
                '    vFEE_CON_SUB.SubList_Monthly = PopulateSubDetails_MONTHLY(vFEE_CON_SUB.FCD_ID, bCancel)
                'Next
            End While
        End Using
    End Sub
    Public Shared Function GET_FEE_CONCESION_D(ByVal pFCH_ID As Integer, Optional ByVal pFCD_ID As Int32 = 0) As DataTable
        GET_FEE_CONCESION_D = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As String = " FEES.GET_FEE_CONCESION_D @FCH_ID = @FCHID, @FCD_ID = @FCDID "
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FCHID", SqlDbType.Int)
            pParms(0).Value = pFCH_ID
            pParms(1) = New SqlClient.SqlParameter("@FCDID", SqlDbType.Int)
            pParms(1).Value = pFCD_ID

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                GET_FEE_CONCESION_D = ds.Tables(0)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "OASIS")
        End Try
    End Function
    Public Shared Function GET_FEE_CONCESSIONMONTHLY_D(ByVal pFCD_ID As Integer) As DataTable
        GET_FEE_CONCESSIONMONTHLY_D = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As String = " FEES.GET_FEE_CONCESSIONMONTHLY_D @FCD_ID = @FCDID "
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FCDID", SqlDbType.Int)
            pParms(0).Value = pFCD_ID

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                GET_FEE_CONCESSIONMONTHLY_D = ds.Tables(0)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "OASIS")
        End Try
    End Function

    Public Shared Function StudentConcessions(ByVal BSU_ID As String, ByVal STU_ID As String, ByVal AcdId As String, Optional ByVal FCH_ID As Integer = 0) As DataTable
        StudentConcessions = Nothing
        Dim dtData As New DataTable
        Dim Qry As New StringBuilder
        Qry.Append("FEES.GET_CONCESSION_FOR_CANCELLATION @FCH_ID = @FCHID, @BSU_ID = @BSUID, @ACD_ID = @ACDID, @STU_ID = @STUID")
        Using conn As New SqlConnection '= ConnectionManger.GetOASIS_FEESConnection
            conn.ConnectionString = ConnectionManger.GetOASIS_FEESConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                cmd.Parameters.AddWithValue("@FCHID", FCH_ID)
                cmd.Parameters.AddWithValue("@BSUID", BSU_ID)
                cmd.Parameters.AddWithValue("@ACDID", AcdId)
                cmd.Parameters.AddWithValue("@STUID", STU_ID)

                cmd.Connection = conn
                conn.Open()
                Dim sqladap As New SqlDataAdapter ' = UtilityObj.CommandAsSql(cmd)
                sqladap.SelectCommand = cmd
                sqladap.Fill(dtData)
                If Not dtData Is Nothing Then
                    StudentConcessions = dtData
                End If
                conn.Close()
            End Using
        End Using
    End Function
    Public Shared Function GetStudent(ByVal BSUID As String, ByVal prefix As String) As String()
        Dim student As New List(Of String)()
        Dim strConn As String = ""
        Dim Qry As New StringBuilder

        Qry.Append("FEES.GET_STUDENTS_FOR_CONCESION @BSU_ID = @BSUID,@FILTER_TEXT = @prefix")
        strConn = ConnectionManger.GetOASIS_FEESConnectionString

        Using conn As New SqlConnection()
            conn.ConnectionString = strConn
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry.ToString
                cmd.Parameters.AddWithValue("@BSUID", BSUID)
                cmd.Parameters.AddWithValue("@prefix", Trim(prefix))

                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        student.Add(String.Format("{0}$${1}$${2}", sdr("STU_NAME").ToString.Replace("-", " "), sdr("STU_NO"), sdr("STU_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return student.ToArray()
        End Using
    End Function

    Public Function F_SaveFEE_CONCESSION_H(ByRef NEW_FCH_ID As String, ByRef conn As SqlConnection, ByRef trans As SqlTransaction, Optional ByVal bDELETE As Boolean = False) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSION_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFCH_ACD_ID As New SqlParameter("@FCH_ACD_ID", SqlDbType.VarChar, 20)
        sqlpFCH_ACD_ID.Value = FCH_ACD_ID
        cmd.Parameters.Add(sqlpFCH_ACD_ID)

        Dim sqlpFCH_BSU_ID As New SqlParameter("@FCH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCH_BSU_ID.Value = FCH_BSU_ID
        cmd.Parameters.Add(sqlpFCH_BSU_ID)

        Dim sqlpFCH_STU_ID As New SqlParameter("@FCH_STU_ID", SqlDbType.Int)
        sqlpFCH_STU_ID.Value = FCH_STU_ID
        cmd.Parameters.Add(sqlpFCH_STU_ID)

        Dim sqlpFCH_DT As New SqlParameter("@FCH_DT", SqlDbType.DateTime)
        sqlpFCH_DT.Value = FCH_DT
        cmd.Parameters.Add(sqlpFCH_DT)

        Dim sqlpFCH_DTFROM As New SqlParameter("@FCH_DTFROM", SqlDbType.DateTime)
        sqlpFCH_DTFROM.Value = FCH_DTFROM
        cmd.Parameters.Add(sqlpFCH_DTFROM)

        Dim sqlpFCH_DTTO As New SqlParameter("@FCH_DTTO", SqlDbType.DateTime)
        sqlpFCH_DTTO.Value = FCH_DTTO
        cmd.Parameters.Add(sqlpFCH_DTTO)

        Dim sqlpFCH_FCM_ID As New SqlParameter("@FCH_FCM_ID", SqlDbType.Int)
        sqlpFCH_FCM_ID.Value = FCH_FCM_ID
        cmd.Parameters.Add(sqlpFCH_FCM_ID)

        Dim sqlpFCH_REF_ID As New SqlParameter("@FCH_REF_ID", SqlDbType.Int)
        sqlpFCH_REF_ID.Value = FCH_REF_ID
        cmd.Parameters.Add(sqlpFCH_REF_ID)

        Dim sqlpFCH_REMARKS As New SqlParameter("@FCH_REMARKS", SqlDbType.VarChar, 200)
        sqlpFCH_REMARKS.Value = FCH_REMARKS
        cmd.Parameters.Add(sqlpFCH_REMARKS)

        Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
        sqlpFCH_ID.Value = FCH_ID
        cmd.Parameters.Add(sqlpFCH_ID)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = bDELETE
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpNEW_FCH_ID As New SqlParameter("@NEW_FCH_ID", SqlDbType.Int)
        sqlpNEW_FCH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEW_FCH_ID)

        Dim sqlpFCH_DRCR As New SqlParameter("@FCH_DRCR", SqlDbType.VarChar, 200)
        sqlpFCH_DRCR.Value = FCH_DRCR
        cmd.Parameters.Add(sqlpFCH_DRCR)

        Dim sqlpFCH_USER As New SqlParameter("@FCH_USER", SqlDbType.VarChar)
        sqlpFCH_USER.Value = FCH_USER
        cmd.Parameters.Add(sqlpFCH_USER)

        Dim sqlpFCH_FCH_ID As New SqlParameter("@FCH_FCH_ID", SqlDbType.Int)
        sqlpFCH_FCH_ID.Value = FCH_FCH_ID
        cmd.Parameters.Add(sqlpFCH_FCH_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim sqlpbNextAcd_Concession As New SqlParameter("@NextAcd_Concession", SqlDbType.Bit)
        sqlpbNextAcd_Concession.Value = False
        cmd.Parameters.Add(sqlpbNextAcd_Concession)

        cmd.CommandTimeout = 0
        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        ElseIf bDELETE = False Then
            NEW_FCH_ID = sqlpNEW_FCH_ID.Value
            iReturnvalue = F_SaveFEE_CONCESSION_D(NEW_FCH_ID, conn, trans)
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        End If

        Return iReturnvalue
    End Function

    Private Function F_SaveFEE_CONCESSION_D(ByVal pFCH_ID As Integer, _
    ByRef conn As SqlConnection, ByRef trans As SqlTransaction) As Integer

        Dim iReturnvalue As Integer = 0
        Dim cmd As SqlCommand

        For Each dr As DataRow In CONCESSION_D.Rows
            cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSION_D]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFCD_FCH_ID As New SqlParameter("@FCD_FCH_ID", SqlDbType.Int)
            sqlpFCD_FCH_ID.Value = pFCH_ID
            cmd.Parameters.Add(sqlpFCD_FCH_ID)

            Dim sqlpFCD_FCM_ID As New SqlParameter("@FCD_FCM_ID", SqlDbType.Int)
            sqlpFCD_FCM_ID.Value = FCH_FCM_ID
            cmd.Parameters.Add(sqlpFCD_FCM_ID)

            Dim sqlpFCD_REF_ID As New SqlParameter("@FCD_REF_ID", SqlDbType.Int)
            sqlpFCD_REF_ID.Value = FCH_REF_ID
            cmd.Parameters.Add(sqlpFCD_REF_ID)

            Dim sqlpFCD_FEE_ID As New SqlParameter("@FCD_FEE_ID", SqlDbType.Int)
            sqlpFCD_FEE_ID.Value = dr("FCD_FEE_ID")
            cmd.Parameters.Add(sqlpFCD_FEE_ID)

            Dim sqlpFCD_AMTTYPE As New SqlParameter("@FCD_AMTTYPE", SqlDbType.SmallInt)
            sqlpFCD_AMTTYPE.Value = dr("FCD_AMTTYPE")
            cmd.Parameters.Add(sqlpFCD_AMTTYPE)

            Dim sqlpFCD_AMOUNT As New SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFCD_AMOUNT.Value = dr("AMOUNT")
            cmd.Parameters.Add(sqlpFCD_AMOUNT)

            Dim sqlpFCD_TAXAMOUNT As New SqlParameter("@FCD_TAXAMOUNT", SqlDbType.Decimal, 21)
            sqlpFCD_TAXAMOUNT.Value = dr("FCD_TAX_AMOUNT")
            cmd.Parameters.Add(sqlpFCD_TAXAMOUNT)

            Dim sqlpFCD_NETAMOUNT As New SqlParameter("@FCD_NETAMOUNT", SqlDbType.Decimal, 21)
            sqlpFCD_NETAMOUNT.Value = dr("FCD_NET_AMOUNT")
            cmd.Parameters.Add(sqlpFCD_NETAMOUNT)

            Dim sqlpFCD_ISINCLUSIVE_TAX As New SqlParameter("@FCD_ISINCLUSIVE_TAX", SqlDbType.Bit)
            sqlpFCD_ISINCLUSIVE_TAX.Value = dr("FCD_ISINCLUSIVE_TAX")
            cmd.Parameters.Add(sqlpFCD_ISINCLUSIVE_TAX)

            Dim sqlpFCD_TAX_CODE As New SqlParameter("@FCD_TAX_CODE", SqlDbType.VarChar, 20)
            sqlpFCD_TAX_CODE.Value = dr("FCD_TAX_CODE")
            cmd.Parameters.Add(sqlpFCD_TAX_CODE)

            Dim sqlpFCD_ACTUALAMT As New SqlParameter("@FCD_ACTUALAMT", SqlDbType.Decimal, 21)
            sqlpFCD_ACTUALAMT.Value = dr("FCD_ACTUALAMT")
            cmd.Parameters.Add(sqlpFCD_ACTUALAMT)

            Dim sqlpFCD_SCH_ID As New SqlParameter("@FCD_SCH_ID", SqlDbType.Int, 21)
            sqlpFCD_SCH_ID.Value = dr("FCD_SCH_ID")
            cmd.Parameters.Add(sqlpFCD_SCH_ID)

            Dim sqlpFCD_REF_BSU_ID As New SqlParameter("@FCD_REF_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFCD_REF_BSU_ID.Value = dr("FCD_REF_BSU_ID")
            cmd.Parameters.Add(sqlpFCD_REF_BSU_ID)

            Dim sqlpFCD_ID As New SqlParameter("@FCD_ID", SqlDbType.Int)
            sqlpFCD_ID.Value = IIf(FCH_ID = 0, 0, dr("FCD_ID")) 'CON_SUB_LIST.FCD_ID
            cmd.Parameters.Add(sqlpFCD_ID)

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = CBool(dr("FCD_bDELETE"))
            cmd.Parameters.Add(sqlpbDelete)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            'Get the New FCD ID (change the S.P so that it returns the New FCD_ID
            Dim sqlpNEW_FCD_ID As New SqlParameter("@NEW_FCD_ID", SqlDbType.Int)
            sqlpNEW_FCD_ID.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpNEW_FCD_ID)
            Dim NEW_FCD_ID As Integer

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue = 0 Then
                NEW_FCD_ID = sqlpNEW_FCD_ID.Value
                iReturnvalue = F_SaveFEE_CONCESSIONMONTHLY_D(NEW_FCD_ID, dr("FCD_ID"), conn, trans)
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                End If
            Else
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function
    Private Function F_SaveFEE_CONCESSIONMONTHLY_D(ByVal pNEW_FCD_ID As Integer, ByVal pFilterFCD_ID As Integer, ByRef conn As SqlConnection, ByRef trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer = 0
        Dim cmd As SqlCommand
        Dim dvMontly As DataView = New DataView(CONCESSIONMONTHLY_D)
        dvMontly.RowFilter = "FMD_FCD_ID = " & pFilterFCD_ID

        For Each drr As DataRow In dvMontly.ToTable().Rows
            cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSIONMONTHLY_D]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFMD_FCD_ID As New SqlParameter("@FMD_FCD_ID", SqlDbType.Int)
            sqlpFMD_FCD_ID.Value = pNEW_FCD_ID
            cmd.Parameters.Add(sqlpFMD_FCD_ID)

            Dim sqlpFMD_REF_ID As New SqlParameter("@FMD_REF_ID", SqlDbType.Int)
            sqlpFMD_REF_ID.Value = drr("FMD_REF_ID").ToString
            cmd.Parameters.Add(sqlpFMD_REF_ID)

            Dim sqlpFMD_AMOUNT As New SqlParameter("@FMD_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_AMOUNT.Value = FeeCollection.GetDoubleVal(drr("FMD_AMOUNT"))
            cmd.Parameters.Add(sqlpFMD_AMOUNT)

            Dim sqlpFMD_TAXAMOUNT As New SqlParameter("@FMD_TAX_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_TAXAMOUNT.Value = FeeCollection.GetDoubleVal(drr("FMD_TAX_AMOUNT"))
            cmd.Parameters.Add(sqlpFMD_TAXAMOUNT)

            Dim sqlpFMD_NETAMOUNT As New SqlParameter("@FMD_NET_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_NETAMOUNT.Value = FeeCollection.GetDoubleVal(drr("FMD_NET_AMOUNT"))
            cmd.Parameters.Add(sqlpFMD_NETAMOUNT)

            Dim sqlpFMD_ORG_AMOUNT As New SqlParameter("@FMD_ORG_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_ORG_AMOUNT.Value = FeeCollection.GetDoubleVal(drr("FMD_ORG_AMOUNT"))
            cmd.Parameters.Add(sqlpFMD_ORG_AMOUNT)

            Dim sqlpFMD_DATE As New SqlParameter("@FMD_DATE", SqlDbType.DateTime)
            sqlpFMD_DATE.Value = drr("FMD_DATE").ToString
            cmd.Parameters.Add(sqlpFMD_DATE)

            Dim sqlpFMD_ID As New SqlParameter("@FMD_ID", SqlDbType.Int)
            sqlpFMD_ID.Value = IIf(FCH_ID = 0, 0, drr("FMD_ID")) 'CON_SUB_LIST_MONTHLY.FMD_ID
            cmd.Parameters.Add(sqlpFMD_ID)

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = CBool(drr("FMD_bDelete"))
            cmd.Parameters.Add(sqlpbDelete)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function
End Class
