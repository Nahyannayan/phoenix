Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Public Class FEE_MST_CC_MACHINE

    Dim vCCM_ID As Integer
    Dim vCCM_BANK_ID As String
    Dim vCCM_BANK_NAME As String
    Dim vCCM_DESCR As String
    Dim vCMM_BSU_ID As String
    Dim vCMM_ALLOC_BSU_ID As String
    Dim vCMM_USR_ID As String
    Dim bDelete As Boolean
    Public bEdit As Boolean

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Property CCM_ID() As Integer
        Get
            Return vCCM_ID
        End Get
        Set(ByVal value As Integer)
            vCCM_ID = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CCM_BANK() As String
        Get
            Return vCCM_BANK_ID
        End Get
        Set(ByVal value As String)
            vCCM_BANK_ID = value
        End Set
    End Property

    Public Property CCM_BANKNAME() As String
        Get
            Return vCCM_BANK_NAME
        End Get
        Set(ByVal value As String)
            vCCM_BANK_NAME = value
        End Set
    End Property

    Public Property CCM_DESCR() As String
        Get
            Return vCCM_DESCR
        End Get
        Set(ByVal value As String)
            vCCM_DESCR = value
        End Set
    End Property

    Public Property CMM_BSU_ID() As String
        Get
            Return vCMM_BSU_ID
        End Get
        Set(ByVal value As String)
            vCMM_BSU_ID = value
        End Set
    End Property

    Public Property CMM_ALLOC_BSU_ID() As String
        Get
            Return vCMM_ALLOC_BSU_ID
        End Get
        Set(ByVal value As String)
            vCMM_ALLOC_BSU_ID = value
        End Set
    End Property

    Public Property CMM_USR_ID() As String
        Get
            Return vCMM_USR_ID
        End Get
        Set(ByVal value As String)
            vCMM_USR_ID = value
        End Set
    End Property

    Public Shared Function GetDetails(ByVal pCCM_ID As Integer, ByVal vconnstr As String) As FEE_MST_CC_MACHINE
        Dim str_Sql = "SELECT CCARD_MACH_M.CCM_ID, CCARD_MACH_M.CCM_DESCR, CCARD_MACH_M.CCM_USR_ID, " & _
        " CCM_ALLOC_BSU_ID, CCARD_MACH_M.CCM_BANK , ACT_NAME " & _
        " FROM CCARD_MACH_M INNER JOIN " & _
        " OASISFIN..ACCOUNTS_M ON ACT_ID = CCARD_MACH_M.CCM_BANK " & _
        " WHERE CCM_ID = " & pCCM_ID
        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(vconnstr, CommandType.Text, str_Sql)
        Dim vFEE_CCM As New FEE_MST_CC_MACHINE
        While (drReader.Read())
            vFEE_CCM.CCM_BANK = drReader("CCM_BANK")
            vFEE_CCM.CCM_DESCR = drReader("CCM_DESCR")
            vFEE_CCM.CMM_ALLOC_BSU_ID = drReader("CCM_ALLOC_BSU_ID")
            vFEE_CCM.CCM_BANKNAME = drReader("ACT_NAME")
            vFEE_CCM.CCM_ID = drReader("CCM_ID")
            vFEE_CCM.CMM_USR_ID = drReader("CCM_USR_ID")
            Exit While
        End While
        Return vFEE_CCM
    End Function

    Public Shared Function SaveDetails(ByVal vFEE_CCM As FEE_MST_CC_MACHINE, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

        If vFEE_CCM Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVECCARD_MACH_M", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpCCM_BSU_ID As New SqlParameter("@CCM_BSU_ID", SqlDbType.VarChar, 20)
        sqlpCCM_BSU_ID.Value = vFEE_CCM.CMM_BSU_ID
        cmd.Parameters.Add(sqlpCCM_BSU_ID)

        Dim sqlpCCM_ALLOC_BSU_ID As New SqlParameter("@CCM_ALLOC_BSU_ID", SqlDbType.VarChar, 20)
        sqlpCCM_ALLOC_BSU_ID.Value = vFEE_CCM.CMM_ALLOC_BSU_ID
        cmd.Parameters.Add(sqlpCCM_ALLOC_BSU_ID)

        Dim sqlpCCM_BANK As New SqlParameter("@CCM_BANK", SqlDbType.VarChar, 100)
        sqlpCCM_BANK.Value = vFEE_CCM.CCM_BANK
        cmd.Parameters.Add(sqlpCCM_BANK)

        Dim sqlpCCM_USR_ID As New SqlParameter("@CCM_USR_ID", SqlDbType.VarChar, 20)
        sqlpCCM_USR_ID.Value = vFEE_CCM.CMM_USR_ID
        cmd.Parameters.Add(sqlpCCM_USR_ID)

        Dim sqlpCCM_DESCR As New SqlParameter("@CCM_DESCR", SqlDbType.VarChar, 20)
        sqlpCCM_DESCR.Value = vFEE_CCM.CCM_DESCR
        cmd.Parameters.Add(sqlpCCM_DESCR)

        If vFEE_CCM.bDelete OrElse vFEE_CCM.bEdit Then
            Dim sqlpCCM_ID As New SqlParameter("@CCM_ID", SqlDbType.Int)
            sqlpCCM_ID.Value = vFEE_CCM.CCM_ID
            cmd.Parameters.Add(sqlpCCM_ID)
        End If

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        Return 0
    End Function

End Class
