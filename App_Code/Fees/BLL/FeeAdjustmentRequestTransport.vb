Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class FeeAdjustmentRequestTransport
    Dim vFAH_ID As Integer
    Dim vFAH_STU_ID As Integer
    Dim vFAH_STU_TYPE As String
    Dim vFAH_STU_NAME As String
    Dim vFAH_ACD_ID As Integer
    Dim vFAH_DATE As DateTime
    Dim vFAH_REMARKS As String
    Dim vFAH_BSU_ID As String
    Dim vFAH_GRD_ID As String
    Public vFAH_GRD_DISPLAY As String
    Dim vFAH_DOC_NO As String
    Dim vADJ_TYPE As Integer
    Public bAllowEdit As Boolean
    Public bDelete As Boolean
    Public bEdit As Boolean
    Dim vFEE_ADJ_DET As Hashtable
    Dim vFAH_STU_ID_CR As Integer
    Dim vFAH_STU_TYPE_CR As String
    Dim vFAH_STU_NAME_CR As String
    Dim vFAH_GRD_ID_CR As String

    ''' <summary>
    ''' Constructor to initializze variables
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        vFEE_ADJ_DET = New Hashtable
    End Sub

    Public Property FEE_ADJ_DET() As Hashtable
        Get
            Return vFEE_ADJ_DET
        End Get
        Set(ByVal value As Hashtable)
            vFEE_ADJ_DET = value
        End Set
    End Property

    Public Property ADJUSTMENTTYPE() As Integer
        Get
            Return vADJ_TYPE
        End Get
        Set(ByVal value As Integer)
            vADJ_TYPE = value
        End Set
    End Property

    Public Property FAH_STU_TYPE() As String
        Get
            Return vFAH_STU_TYPE
        End Get
        Set(ByVal value As String)
            vFAH_STU_TYPE = value
        End Set
    End Property

    Public Property FAH_STU_NAME() As String
        Get
            Return vFAH_STU_NAME
        End Get
        Set(ByVal value As String)
            vFAH_STU_NAME = value
        End Set
    End Property

    Public Property FAH_STU_TYPE_CR() As String
        Get
            Return vFAH_STU_TYPE_CR
        End Get
        Set(ByVal value As String)
            vFAH_STU_TYPE_CR = value
        End Set
    End Property

    Public Property FAH_GRD_ID_CR() As String
        Get
            Return vFAH_GRD_ID_CR
        End Get
        Set(ByVal value As String)
            vFAH_GRD_ID_CR = value
        End Set
    End Property

    Public Property FAH_STU_NAME_CR() As String
        Get
            Return vFAH_STU_NAME_CR
        End Get
        Set(ByVal value As String)
            vFAH_STU_NAME_CR = value
        End Set
    End Property

    Public Property FAH_STU_ID_CR() As Integer
        Get
            Return vFAH_STU_ID_CR
        End Get
        Set(ByVal value As Integer)
            vFAH_STU_ID_CR = value
        End Set
    End Property

    Public Property FAH_DOC_NO() As String
        Get
            Return vFAH_DOC_NO
        End Get
        Set(ByVal value As String)
            vFAH_DOC_NO = value
        End Set
    End Property

    Public Property FAH_BSU_ID() As String
        Get
            Return vFAH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFAH_BSU_ID = value
        End Set
    End Property

    Public Property FAH_REMARKS() As String
        Get
            Return vFAH_REMARKS
        End Get
        Set(ByVal value As String)
            vFAH_REMARKS = value
        End Set
    End Property

    Public Property FAH_DATE() As DateTime
        Get
            Return vFAH_DATE
        End Get
        Set(ByVal value As DateTime)
            vFAH_DATE = value
        End Set
    End Property

    Public Property FAH_GRD_ID() As String
        Get
            Return vFAH_GRD_ID
        End Get
        Set(ByVal value As String)
            vFAH_GRD_ID = value
        End Set
    End Property

    Public Property FAH_ACD_ID() As Integer
        Get
            Return vFAH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFAH_ACD_ID = value
        End Set
    End Property

    Public Property FAH_STU_ID() As Integer
        Get
            Return vFAH_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFAH_STU_ID = value
        End Set
    End Property

    Public Property FAH_ID() As Integer
        Get
            Return vFAH_ID
        End Get
        Set(ByVal value As Integer)
            vFAH_ID = value
        End Set
    End Property

    Public Shared Function GetGrade(ByVal STUD_ID As String) As String
        Dim sql_query As String = "select STU_GRD_ID FROM STUDENT_M WHERE STU_ID =" & STUD_ID
        Dim connStr As String = ConnectionManger.GetOASISConnectionString
        Return SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql_query)
    End Function

    Public Shared Function GetGradeDetails(ByVal STUD_ID As String, ByRef GRD_DISPLAY As String) As String
        Dim sql_query As String = "SELECT STU_GRD_ID GRD_ID , GRD_DISPLAY FROM STUDENT_M " & _
        " INNER JOIN GRADE_M ON STUDENT_M.STU_GRD_ID = GRADE_M.GRD_ID WHERE STU_ID = " & STUD_ID
        Dim connStr As String = ConnectionManger.GetOASISConnectionString
        Dim sqlReader As SqlDataReader = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql_query)
        While (sqlReader.Read())
            GRD_DISPLAY = sqlReader("GRD_DISPLAY")
            Return sqlReader("GRD_ID")
        End While
        Return ""
    End Function

    Public Shared Function GetFeeAdjustments(ByVal FAH_ID As Integer) As FeeAdjustmentRequestTransport
        Dim str_Sql As String = String.Empty
        Dim tempvFEE_ADJ As New FeeAdjustmentRequestTransport
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            str_Sql = "select isnull(FAH_STU_TYPE, 'S') FROM FEES.FEEADJUSTMENT_H " & _
            " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            Dim vSTU_TYP As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, str_Sql)
            If vSTU_TYP = "S" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " ISNULL(FEES.FEEADJUSTMENT_H.FAH_DOCNO, '') FAH_DOCNO, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE, FEES.FEEADJUSTMENT_H.FAH_ACD_ID, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NAME, VW_OSO_STUDENT_M.STU_GRD_ID, " & _
                " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID, GRADE_M.GRD_DISPLAY " & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN VW_OSO_STUDENT_M " & _
                " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = VW_OSO_STUDENT_M.STU_ID" & _
                " INNER JOIN GRADE_M ON GRADE_M.GRD_ID = VW_OSO_STUDENT_M.STU_GRD_ID " & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            ElseIf vSTU_TYP = "E" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " ISNULL(FEES.FEEADJUSTMENT_H.FAH_DOCNO, '') FAH_DOCNO, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID, FEES.FEEADJUSTMENT_H.FAH_ACD_ID, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, FEES.vw_OSO_ENQUIRY_COMP.STU_GRD_ID, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID, GRADE_M.GRD_DISPLAY " & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP " & _
                " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID" & _
                " INNER JOIN GRADE_M ON GRADE_M.GRD_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_GRD_ID " & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            End If

            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ.FAH_ACD_ID = dReader("FAH_ACD_ID")
                tempvFEE_ADJ.FAH_DOC_NO = dReader("FAH_DOCNO").ToString()
                tempvFEE_ADJ.FAH_BSU_ID = dReader("FAH_BSU_ID")
                tempvFEE_ADJ.FAH_DATE = dReader("FAH_DATE")
                tempvFEE_ADJ.FAH_GRD_ID = dReader("STU_GRD_ID")
                tempvFEE_ADJ.vFAH_GRD_DISPLAY = dReader("GRD_DISPLAY")
                tempvFEE_ADJ.FAH_ID = dReader("FAH_ID")
                tempvFEE_ADJ.FAH_REMARKS = dReader("FAH_REMARKS")
                tempvFEE_ADJ.FAH_STU_ID = dReader("FAH_STU_ID")
                tempvFEE_ADJ.FAH_STU_TYPE = dReader("FAH_STU_TYPE")
                tempvFEE_ADJ.FAH_STU_NAME = dReader("STU_NAME")
                tempvFEE_ADJ.FEE_ADJ_DET = GetSubFEEAdjustmentSubDetais(FAH_ID)
                If dReader("FAH_bPosted") = True OrElse dReader("FAH_FAR_ID") <> 0 Then
                    tempvFEE_ADJ.bAllowEdit = False
                Else
                    tempvFEE_ADJ.bAllowEdit = True
                End If
                Exit While
            End While
        End Using
        Return tempvFEE_ADJ
    End Function

    Public Shared Function GetFeeAdjustmentsInternalTransfer(ByVal FAH_ID As Integer) As FeeAdjustmentRequestTransport
        Dim str_Sql As String = String.Empty
        Dim tempvFEE_ADJ As New FeeAdjustmentRequestTransport
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            str_Sql = "select isnull(FAH_STU_TYPE, 'S') FROM FEES.FEEADJUSTMENT_H " & _
            " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            Dim vSTU_TYP As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, str_Sql)
            If vSTU_TYP = "S" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FAH_STU_ID_CR, FAH_STU_ID) FAH_STU_ID_CR, " & _
                " isnull(FAH_STU_TYPE_CR, FAH_STU_TYPE) FAH_STU_TYPE_CR, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE, FEES.FEEADJUSTMENT_H.FAH_ACD_ID, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NAME, VW_OSO_STUDENT_M.STU_GRD_ID, " & _
                " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID" & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN VW_OSO_STUDENT_M " & _
                "ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = VW_OSO_STUDENT_M.STU_ID" & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            ElseIf vSTU_TYP = "E" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID, FEES.FEEADJUSTMENT_H.FAH_ACD_ID, " & _
                " isnull(FAH_STU_ID_CR, FAH_STU_ID) FAH_STU_ID_CR, " & _
                " isnull(FAH_STU_TYPE_CR, FAH_STU_TYPE) FAH_STU_TYPE_CR, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, FEES.vw_OSO_ENQUIRY_COMP.STU_GRD_ID, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID" & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP " & _
                " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID" & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            End If
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ.FAH_ACD_ID = dReader("FAH_ACD_ID")
                tempvFEE_ADJ.FAH_BSU_ID = dReader("FAH_BSU_ID")
                tempvFEE_ADJ.FAH_DATE = dReader("FAH_DATE")
                tempvFEE_ADJ.FAH_GRD_ID = dReader("STU_GRD_ID")
                tempvFEE_ADJ.FAH_STU_TYPE_CR = dReader("FAH_STU_TYPE_CR")
                tempvFEE_ADJ.FAH_STU_ID_CR = dReader("FAH_STU_ID_CR")
                If tempvFEE_ADJ.FAH_STU_TYPE_CR = "S" Then
                    tempvFEE_ADJ.FAH_STU_NAME_CR = GetStudentName(tempvFEE_ADJ.FAH_STU_ID_CR, False)
                ElseIf tempvFEE_ADJ.FAH_STU_TYPE_CR = "E" Then
                    tempvFEE_ADJ.FAH_STU_NAME_CR = GetStudentName(tempvFEE_ADJ.FAH_STU_ID_CR, True)
                End If
                tempvFEE_ADJ.FAH_ID = dReader("FAH_ID")
                tempvFEE_ADJ.FAH_REMARKS = dReader("FAH_REMARKS")
                tempvFEE_ADJ.FAH_STU_ID = dReader("FAH_STU_ID")
                tempvFEE_ADJ.FAH_STU_TYPE = dReader("FAH_STU_TYPE")
                tempvFEE_ADJ.FAH_STU_NAME = dReader("STU_NAME")
                tempvFEE_ADJ.FEE_ADJ_DET = GetSubFEEAdjustmentSubDetais(FAH_ID)
                If dReader("FAH_bPosted") = True OrElse dReader("FAH_FAR_ID") <> 0 Then
                    tempvFEE_ADJ.bAllowEdit = False
                Else
                    tempvFEE_ADJ.bAllowEdit = True
                End If
                Exit While
            End While
        End Using
        Return tempvFEE_ADJ
    End Function

    Public Shared Function GetBalanceAmountForFeeType(ByVal vBSU_ID As String, ByVal stud_typ As String, ByVal STU_ID As String, ByVal asondate As String, ByVal feeID As Integer) As Double
        If asondate = "" Then Return 0
        Dim str_sql As String
        str_sql = " SELECT SUM(CASE WHEN DRCR = 'DR' THEN 1 ELSE - 1 END * AMOUNT) AS AMOUNT " & _
        "FROM FEES.fn_STUDENTLEDGER('" & vBSU_ID & "', " & STU_ID & ", '" & stud_typ & "') " & _
        " WHERE (DOCDATE <= '" & asondate & "')" & " AND FEE_ID = " & feeID
        Dim obj As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        If obj Is DBNull.Value Then
            Return 0
        Else
            Return CDbl(obj)
        End If
    End Function

    Private Shared Function GetStudentName(ByVal STU_ID As String, ByVal bEnquiry As Boolean) As String
        Dim str_sql As String
        If bEnquiry Then
            str_sql = " SELECT STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP WHERE STU_ID = " & STU_ID
        Else
            str_sql = " SELECT VW_OSO_STUDENT_M.STU_NAME FROM VW_OSO_STUDENT_M WHERE STU_ID = " & STU_ID
        End If
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function GetSubFEEAdjustmentSubDetais(ByVal FAH_ID As Integer) As Hashtable
        Dim str_Sql As String = String.Empty
        Dim htFEE_ADJ_DET As New Hashtable
        Dim tempvFEE_ADJ_S As New FeeAdjustmentRequestTransport_S
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            str_Sql = "SELECT  FEES.FeeAdjustmentRequestTransport_S.FAD_ID, FEES.FeeAdjustmentRequestTransport_S.FAD_FAH_ID, " & _
            " FEES.FeeAdjustmentRequestTransport_S.FAD_FEE_ID, isnull(FEES.FeeAdjustmentRequestTransport_S.FAD_TO_FEE_ID, 0) FAD_TO_FEE_ID, " & _
            " FEES.FeeAdjustmentRequestTransport_S.FAD_AMOUNT, " & _
            " FEES.FeeAdjustmentRequestTransport_S.FAD_REMARKS, FEES.FEES_M.FEE_DESCR " & _
            " FROM FEES.FeeAdjustmentRequestTransport_S INNER JOIN " & _
            " FEES.FEES_M ON FEES.FeeAdjustmentRequestTransport_S.FAD_FEE_ID = FEES.FEES_M.FEE_ID" & _
            " WHERE FAD_FAH_ID =" & FAH_ID
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ_S = New FeeAdjustmentRequestTransport_S
                tempvFEE_ADJ_S.FAD_ID = dReader("FAD_ID")
                tempvFEE_ADJ_S.FAD_FEE_ID = dReader("FAD_FEE_ID")
                tempvFEE_ADJ_S.TO_FAD_FEE_ID = dReader("FAD_TO_FEE_ID")
                tempvFEE_ADJ_S.FAD_FAH_ID = dReader("FAD_FAH_ID")
                tempvFEE_ADJ_S.FAD_AMOUNT = dReader("FAD_AMOUNT")
                tempvFEE_ADJ_S.FAD_REMARKS = dReader("FAD_REMARKS")
                tempvFEE_ADJ_S.FEE_TYPE = dReader("FEE_DESCR")
                htFEE_ADJ_DET(tempvFEE_ADJ_S.FAD_FEE_ID) = tempvFEE_ADJ_S
            End While
        End Using
        Return htFEE_ADJ_DET
    End Function

    Public Shared Function GetSubDetailsAsDataTable(ByVal htDetails As Hashtable) As DataTable
        If htDetails Is Nothing Then
            Return Nothing
        End If
        Dim vFEE_DET As FeeAdjustmentRequestTransport_S
        Dim ienum As IDictionaryEnumerator = htDetails.GetEnumerator
        Dim dt As DataTable = CreateDataTable()
        Dim dr As DataRow
        While (ienum.MoveNext())
            vFEE_DET = ienum.Value
            If vFEE_DET.bDelete Then Continue While
            dr = dt.NewRow
            dr("FEE_ID") = vFEE_DET.FAD_FEE_ID
            dr("FEE_TYPE") = vFEE_DET.FEE_TYPE
            If vFEE_DET.AdjustmentType = -1 Then
                dr("ADJTYPE") = "Negtive"
            ElseIf vFEE_DET.AdjustmentType = 1 Then
                dr("ADJTYPE") = "Positive"
            End If
            dr("FEE_AMOUNT") = vFEE_DET.FAD_AMOUNT
            dr("FEE_REMARKS") = vFEE_DET.FAD_REMARKS
            dt.Rows.Add(dr)
        End While
        Return dt
    End Function

    Private Shared Function CreateDataTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.Int32"))
            Dim cFEE_TYPE As New DataColumn("FEE_TYPE", System.Type.GetType("System.String"))
            Dim cFEE_ADJ_TYPE As New DataColumn("ADJTYPE", System.Type.GetType("System.String"))
            Dim cFEE_AMOUNT As New DataColumn("FEE_AMOUNT", System.Type.GetType("System.Double"))
            Dim cFEE_REMARKS As New DataColumn("FEE_REMARKS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cFEE_ADJ_TYPE)
            dtDt.Columns.Add(cFEE_TYPE)
            dtDt.Columns.Add(cFEE_AMOUNT)
            dtDt.Columns.Add(cFEE_REMARKS)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Public Shared Function SaveDetailsInternal(ByVal FEE_ADJ_DET As FeeAdjustmentRequestTransport, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.[F_SAVEFEEADJUSTMENT_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAH_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAH_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpFAH_STU_TYPE As New SqlParameter("@FAH_STU_TYPE", SqlDbType.VarChar, 2)
        sqlpFAH_STU_TYPE.Value = FEE_ADJ_DET.FAH_STU_TYPE
        cmd.Parameters.Add(sqlpFAH_STU_TYPE)

        Dim sqlpFSH_STU_TYPE_CR As New SqlParameter("@FAH_STU_TYPE_CR", SqlDbType.VarChar, 4)
        sqlpFSH_STU_TYPE_CR.Value = FEE_ADJ_DET.FAH_STU_TYPE_CR
        cmd.Parameters.Add(sqlpFSH_STU_TYPE_CR)

        Dim sqlpFSH_STU_ID_CR As New SqlParameter("@FAH_STU_ID_CR", SqlDbType.VarChar, 15)
        sqlpFSH_STU_ID_CR.Value = FEE_ADJ_DET.FAH_STU_ID_CR
        cmd.Parameters.Add(sqlpFSH_STU_ID_CR)

        Dim sqlpFAH_DATE As New SqlParameter("@FAH_DATE", SqlDbType.DateTime)
        sqlpFAH_DATE.Value = FEE_ADJ_DET.FAH_DATE
        cmd.Parameters.Add(sqlpFAH_DATE)

        Dim sqlpFAH_bInter As New SqlParameter("@FAH_bInter", SqlDbType.Bit)
        sqlpFAH_bInter.Value = 1
        cmd.Parameters.Add(sqlpFAH_bInter)

        Dim sqlpFAH_ACD_ID As New SqlParameter("@FAH_ACD_ID", SqlDbType.Int)
        sqlpFAH_ACD_ID.Value = FEE_ADJ_DET.FAH_ACD_ID
        cmd.Parameters.Add(sqlpFAH_ACD_ID)

        Dim sqlpFAH_ADJTYPE As New SqlParameter("@FAH_ADJTYPE", SqlDbType.VarChar, 50)
        If FEE_ADJ_DET.ADJUSTMENTTYPE = 1 Then
            sqlpFAH_ADJTYPE.Value = "POSITIVE"
        ElseIf FEE_ADJ_DET.ADJUSTMENTTYPE = -1 Then
            sqlpFAH_ADJTYPE.Value = "NEGATIVE"
        End If
        cmd.Parameters.Add(sqlpFAH_ADJTYPE)

        Dim sqlpFAH_REMARKS As New SqlParameter("@FAH_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.FAH_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        If FEE_ADJ_DET.bDelete OrElse FEE_ADJ_DET.bEdit Then
            Dim sqlpFAH_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = FEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = FEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NewFAH_ID As New SqlParameter("@NEW_FAH_ID", SqlDbType.Int)
        NewFAH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFAH_ID)

        Dim NewFAH_DOCNO As New SqlParameter("@FAH_DOCNO", SqlDbType.VarChar, 20)
        NewFAH_DOCNO.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFAH_DOCNO)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue = 0 Then iReturnvalue = SaveFeeSubDetailsInternal(NewFAH_DOCNO.Value, FEE_ADJ_DET, NewFAH_ID.Value, conn, trans)
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        Return 0
    End Function

    Private Shared Function SaveFeeSubDetailsInternal(ByVal DOC_NO As String, ByVal vFEE_ADJ As FeeAdjustmentRequestTransport, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        For Each vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S In vFEE_ADJ.FEE_ADJ_DET.Values
            If vFEE_ADJ_DET Is Nothing Then
                Continue For
            End If
            vFEE_ADJ_DET.bEdit = vFEE_ADJ_DET.bEdit
            vFEE_ADJ_DET.bDelete = vFEE_ADJ_DET.bDelete
            If iReturnvalue = 0 Then iReturnvalue = SaveFeeAdjustmentDetailsInternal(vFEE_ADJ_DET, FAH_ID, conn, trans)
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEESCHEDULEInternal(DOC_NO, vFEE_ADJ_DET, FAH_ID, vFEE_ADJ.FAH_STU_ID, vFEE_ADJ.FAH_STU_TYPE, _
            vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_BSU_ID, vFEE_ADJ.FAH_GRD_ID, vFEE_ADJ.FAH_DATE, conn, trans)
            'Fee CR Entry
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEESCHEDULEInternal(DOC_NO, vFEE_ADJ_DET, FAH_ID, vFEE_ADJ.FAH_STU_ID_CR, vFEE_ADJ.FAH_STU_TYPE_CR, _
            vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_BSU_ID, vFEE_ADJ.FAH_GRD_ID_CR, vFEE_ADJ.FAH_DATE, conn, trans, True)
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function F_SAVEFEESCHEDULEInternal(ByVal DOC_NO As String, ByVal vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S, ByVal NEW_FAH_ID As Integer, _
    ByVal STU_ID As Integer, ByVal STU_TYPE As String, ByVal ACD_ID As Integer, ByVal BSU_ID As String, ByVal GRD_ID As String, _
    ByVal DT As Date, _
    ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal bCREntry As Boolean = False) As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEESCHEDULE", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFSH_ID As New SqlParameter("@FSH_ID", SqlDbType.Int)
        sqlpFSH_ID.Value = DBNull.Value
        cmd.Parameters.Add(sqlpFSH_ID)

        Dim sqlpFSH_FSP_ID As New SqlParameter("@FSH_FSP_ID", SqlDbType.Int)
        sqlpFSH_FSP_ID.Value = 0
        cmd.Parameters.Add(sqlpFSH_FSP_ID)

        Dim sqlpFSH_BSU_ID As New SqlParameter("@FSH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFSH_BSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpFSH_BSU_ID)

        Dim sqlpFSH_ACD_ID As New SqlParameter("@FSH_ACD_ID", SqlDbType.Int)
        sqlpFSH_ACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpFSH_ACD_ID)

        Dim sqlpFSH_GRD_ID As New SqlParameter("@FSH_GRD_ID", SqlDbType.VarChar, 10)
        sqlpFSH_GRD_ID.Value = GRD_ID
        cmd.Parameters.Add(sqlpFSH_GRD_ID)

        Dim sqlpFSH_DOCNO As New SqlParameter("@FSH_DOCNO", SqlDbType.VarChar, 20)
        sqlpFSH_DOCNO.Value = DOC_NO
        cmd.Parameters.Add(sqlpFSH_DOCNO)

        Dim sqlpFSH_STU_TYPE As New SqlParameter("@FSH_STU_TYPE", SqlDbType.VarChar, 4)
        sqlpFSH_STU_TYPE.Value = STU_TYPE
        cmd.Parameters.Add(sqlpFSH_STU_TYPE)

        Dim sqlpFSH_STU_ID As New SqlParameter("@FSH_STU_ID", SqlDbType.VarChar, 20)
        sqlpFSH_STU_ID.Value = STU_ID
        cmd.Parameters.Add(sqlpFSH_STU_ID)

        Dim sqlpFSH_FEE_ID As New SqlParameter("@FSH_FEE_ID", SqlDbType.Int)
        If bCREntry Then
            sqlpFSH_FEE_ID.Value = vFEE_ADJ_DET.TO_FAD_FEE_ID
        Else
            sqlpFSH_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        End If
        cmd.Parameters.Add(sqlpFSH_FEE_ID)

        Dim sqlpFSH_DATE As New SqlParameter("@FSH_DATE", SqlDbType.DateTime)
        sqlpFSH_DATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_DATE)

        Dim sqlpFSH_FDUEDATE As New SqlParameter("@FSH_FDUEDATE", SqlDbType.DateTime)
        sqlpFSH_FDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_FDUEDATE)

        Dim sqlpFSH_SDUEDATE As New SqlParameter("@FSH_SDUEDATE", SqlDbType.DateTime)
        sqlpFSH_SDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_SDUEDATE)

        Dim sqlpFSH_TDUEDATE As New SqlParameter("@FSH_TDUEDATE", SqlDbType.DateTime)
        sqlpFSH_TDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_TDUEDATE)

        Dim sqlpFSH_AMOUNT As New SqlParameter("@FSH_AMOUNT", SqlDbType.Decimal)
        sqlpFSH_AMOUNT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFSH_AMOUNT)

        Dim sqlpFSH_SETTLEAMT As New SqlParameter("@FSH_SETTLEAMT", SqlDbType.Decimal)
        sqlpFSH_SETTLEAMT.Value = 0
        cmd.Parameters.Add(sqlpFSH_SETTLEAMT)

        Dim sqlpFSH_SOURCE As New SqlParameter("@FSH_SOURCE", SqlDbType.VarChar, 50)
        sqlpFSH_SOURCE.Value = "FEE Adjustment"
        cmd.Parameters.Add(sqlpFSH_SOURCE)

        Dim sqlpFSH_REF_ID As New SqlParameter("@FSH_REF_ID", SqlDbType.Int)
        sqlpFSH_REF_ID.Value = NEW_FAH_ID
        cmd.Parameters.Add(sqlpFSH_REF_ID)

        Dim sqlpFSH_GRM_ID As New SqlParameter("@FSH_SCT_ID", SqlDbType.Int)
        sqlpFSH_GRM_ID.Value = GetSCTID(BSU_ID, ACD_ID, GRD_ID, STU_ID)
        cmd.Parameters.Add(sqlpFSH_GRM_ID)

        Dim sqlpFSH_DRCR As New SqlParameter("@FSH_DRCR", SqlDbType.VarChar, 2)
        If vFEE_ADJ_DET.FAD_AMOUNT < 0 OrElse bCREntry Then
            sqlpFSH_DRCR.Value = "CR"
        Else
            sqlpFSH_DRCR.Value = "DR"
        End If
        cmd.Parameters.Add(sqlpFSH_DRCR)

        Dim sqlpFSH_MONTHS As New SqlParameter("@FSH_MONTHS", SqlDbType.Int)
        sqlpFSH_MONTHS.Value = 1
        cmd.Parameters.Add(sqlpFSH_MONTHS)

        Dim sqlpFSH_ROUNDOFF As New SqlParameter("@FSH_ROUNDOFF", SqlDbType.Decimal)
        sqlpFSH_ROUNDOFF.Value = 0
        cmd.Parameters.Add(sqlpFSH_ROUNDOFF)

        '	@FSH_STU_TYPE varchar(4) 
        '	@FSH_MONTHS tinyint=1 ,@
        '   FSH_ROUNDOFF(numeric(18, 3) = 0)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        Return 0
        'iReturnvalue = retSValParam.Value
        'If iReturnvalue <> 0 Then
        '    Return iReturnvalue
        'End If
    End Function

    Private Shared Function SaveFeeAdjustmentDetailsInternal(ByVal vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFeeAdjustmentRequestTransport_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAD_FAH_ID As New SqlParameter("@FAD_FAH_ID", SqlDbType.Int)
        sqlpFAD_FAH_ID.Value = FAH_ID 'STU_SUB_DET.FPD_FPH_ID
        cmd.Parameters.Add(sqlpFAD_FAH_ID)

        Dim sqlpFAD_FEE_ID As New SqlParameter("@FAD_FEE_ID", SqlDbType.Int)
        sqlpFAD_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFAD_FEE_ID)

        Dim sqlpFAD_TO_FEE_ID As New SqlParameter("@FAD_TO_FEE_ID", SqlDbType.Int)
        sqlpFAD_TO_FEE_ID.Value = vFEE_ADJ_DET.TO_FAD_FEE_ID
        cmd.Parameters.Add(sqlpFAD_TO_FEE_ID)

        Dim sqlpFAD_AMOUNT As New SqlParameter("@FAD_AMOUNT", SqlDbType.Decimal)
        sqlpFAD_AMOUNT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFAD_AMOUNT)

        Dim sqlpFAD_REMARKS As New SqlParameter("@FAD_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAD_REMARKS.Value = "Internal FEE adjustment"
        cmd.Parameters.Add(sqlpFAD_REMARKS)

        If vFEE_ADJ_DET.bDelete OrElse vFEE_ADJ_DET.bEdit Then
            Dim sqlpFAD_ID As New SqlParameter("@FAD_ID", SqlDbType.Int)
            sqlpFAD_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFAD_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = vFEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function


    Public Shared Function SaveDetails(ByVal FEE_ADJ_DET As FeeAdjustmentRequestTransport, ByRef newretFAH_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.[F_SAVEFEEADJUSTMENT_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAH_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAH_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpFAH_STU_TYPE As New SqlParameter("@FAH_STU_TYPE", SqlDbType.VarChar, 2)
        sqlpFAH_STU_TYPE.Value = FEE_ADJ_DET.FAH_STU_TYPE
        cmd.Parameters.Add(sqlpFAH_STU_TYPE)

        Dim sqlpFAH_DATE As New SqlParameter("@FAH_DATE", SqlDbType.DateTime)
        sqlpFAH_DATE.Value = FEE_ADJ_DET.FAH_DATE
        cmd.Parameters.Add(sqlpFAH_DATE)

        Dim sqlpFAH_ACD_ID As New SqlParameter("@FAH_ACD_ID", SqlDbType.Int)
        sqlpFAH_ACD_ID.Value = FEE_ADJ_DET.FAH_ACD_ID
        cmd.Parameters.Add(sqlpFAH_ACD_ID)

        Dim sqlpFAH_ADJTYPE As New SqlParameter("@FAH_ADJTYPE", SqlDbType.VarChar, 50)
        If FEE_ADJ_DET.ADJUSTMENTTYPE = 1 Then
            sqlpFAH_ADJTYPE.Value = "POSITIVE"
        ElseIf FEE_ADJ_DET.ADJUSTMENTTYPE = -1 Then
            sqlpFAH_ADJTYPE.Value = "NEGATIVE"
        End If
        cmd.Parameters.Add(sqlpFAH_ADJTYPE)

        Dim sqlpFAH_REMARKS As New SqlParameter("@FAH_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.FAH_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        If FEE_ADJ_DET.bDelete OrElse FEE_ADJ_DET.bEdit Then
            Dim sqlpFAH_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = FEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = FEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpFAH_bInter As New SqlParameter("@FAH_bInter", SqlDbType.Bit)
        sqlpFAH_bInter.Value = 0
        cmd.Parameters.Add(sqlpFAH_bInter)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NewFAH_ID As New SqlParameter("@NEW_FAH_ID", SqlDbType.Int)
        NewFAH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFAH_ID)

        Dim NewFAH_DOCNO As New SqlParameter("@FAH_DOCNO", SqlDbType.VarChar, 20)
        NewFAH_DOCNO.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFAH_DOCNO)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If Not NewFAH_ID Is Nothing AndAlso Not NewFAH_ID.Value Is DBNull.Value Then
            newretFAH_ID = NewFAH_ID.Value
        End If
        If iReturnvalue = 0 Then iReturnvalue = SaveFeeSubDetails(NewFAH_DOCNO.Value, FEE_ADJ_DET, NewFAH_ID.Value, conn, trans)
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        Return 0
    End Function

    Private Shared Function SaveFeeSubDetails(ByVal DOC_NO As String, ByVal vFEE_ADJ As FeeAdjustmentRequestTransport, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        For Each vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S In vFEE_ADJ.FEE_ADJ_DET.Values
            If vFEE_ADJ_DET Is Nothing Then
                Continue For
            End If
            If iReturnvalue = 0 Then iReturnvalue = SaveFeeAdjustmentDetails(vFEE_ADJ_DET, FAH_ID, conn, trans)
            If iReturnvalue = 0 Then F_SAVEFEESCHEDULE(DOC_NO, vFEE_ADJ_DET, FAH_ID, vFEE_ADJ.FAH_STU_ID, vFEE_ADJ.FAH_STU_TYPE, _
            vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_BSU_ID, vFEE_ADJ.FAH_GRD_ID, vFEE_ADJ.FAH_DATE, conn, trans)
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function F_SAVEFEESCHEDULE(ByVal DOC_NO As String, ByVal vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S, ByVal NEW_FAH_ID As Integer, _
    ByVal STU_ID As Integer, ByVal STU_TYPE As String, ByVal ACD_ID As Integer, ByVal BSU_ID As String, ByVal GRD_ID As String, _
    ByVal DT As Date, _
    ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEESCHEDULE", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFSH_ID As New SqlParameter("@FSH_ID", SqlDbType.Int)
        sqlpFSH_ID.Value = DBNull.Value
        cmd.Parameters.Add(sqlpFSH_ID)

        Dim sqlpFSH_FSP_ID As New SqlParameter("@FSH_FSP_ID", SqlDbType.Int)
        sqlpFSH_FSP_ID.Value = 0
        cmd.Parameters.Add(sqlpFSH_FSP_ID)

        Dim sqlpFSH_BSU_ID As New SqlParameter("@FSH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFSH_BSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpFSH_BSU_ID)

        Dim sqlpFSH_ACD_ID As New SqlParameter("@FSH_ACD_ID", SqlDbType.Int)
        sqlpFSH_ACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpFSH_ACD_ID)

        Dim sqlpFSH_GRD_ID As New SqlParameter("@FSH_GRD_ID", SqlDbType.VarChar, 10)
        sqlpFSH_GRD_ID.Value = GRD_ID
        cmd.Parameters.Add(sqlpFSH_GRD_ID)

        Dim sqlpFSH_DOCNO As New SqlParameter("@FSH_DOCNO", SqlDbType.VarChar, 20)
        sqlpFSH_DOCNO.Value = DOC_NO
        cmd.Parameters.Add(sqlpFSH_DOCNO)

        Dim sqlpFSH_GRM_ID As New SqlParameter("@FSH_SCT_ID", SqlDbType.Int)
        sqlpFSH_GRM_ID.Value = GetSCTID(BSU_ID, ACD_ID, GRD_ID, STU_ID)
        cmd.Parameters.Add(sqlpFSH_GRM_ID)

        Dim sqlpFSH_STU_TYPE As New SqlParameter("@FSH_STU_TYPE", SqlDbType.VarChar, 4)
        sqlpFSH_STU_TYPE.Value = STU_TYPE
        cmd.Parameters.Add(sqlpFSH_STU_TYPE)

        Dim sqlpFSH_STU_ID As New SqlParameter("@FSH_STU_ID", SqlDbType.VarChar, 20)
        sqlpFSH_STU_ID.Value = STU_ID
        cmd.Parameters.Add(sqlpFSH_STU_ID)

        Dim sqlpFSH_FEE_ID As New SqlParameter("@FSH_FEE_ID", SqlDbType.Int)
        sqlpFSH_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFSH_FEE_ID)

        Dim sqlpFSH_DATE As New SqlParameter("@FSH_DATE", SqlDbType.DateTime)
        sqlpFSH_DATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_DATE)

        Dim sqlpFSH_FDUEDATE As New SqlParameter("@FSH_FDUEDATE", SqlDbType.DateTime)
        sqlpFSH_FDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_FDUEDATE)

        Dim sqlpFSH_SDUEDATE As New SqlParameter("@FSH_SDUEDATE", SqlDbType.DateTime)
        sqlpFSH_SDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_SDUEDATE)

        Dim sqlpFSH_TDUEDATE As New SqlParameter("@FSH_TDUEDATE", SqlDbType.DateTime)
        sqlpFSH_TDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_TDUEDATE)

        Dim sqlpFSH_AMOUNT As New SqlParameter("@FSH_AMOUNT", SqlDbType.Decimal)
        sqlpFSH_AMOUNT.Value = IIf(vFEE_ADJ_DET.FAD_AMOUNT > 0, 1, -1) * vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFSH_AMOUNT)

        Dim sqlpFSH_SETTLEAMT As New SqlParameter("@FSH_SETTLEAMT", SqlDbType.Decimal)
        sqlpFSH_SETTLEAMT.Value = 0
        'If vFEE_ADJ_DET.FAD_AMOUNT < 0 Then
        '    sqlpFSH_SETTLEAMT.Value = 0
        'Else
        '    sqlpFSH_SETTLEAMT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        'End If
        cmd.Parameters.Add(sqlpFSH_SETTLEAMT)

        Dim sqlpFSH_SOURCE As New SqlParameter("@FSH_SOURCE", SqlDbType.VarChar, 50)
        sqlpFSH_SOURCE.Value = "FEE Adjustment"
        cmd.Parameters.Add(sqlpFSH_SOURCE)

        Dim sqlpFSH_REF_ID As New SqlParameter("@FSH_REF_ID", SqlDbType.Int)
        sqlpFSH_REF_ID.Value = NEW_FAH_ID
        cmd.Parameters.Add(sqlpFSH_REF_ID)

        Dim sqlpFSH_DRCR As New SqlParameter("@FSH_DRCR", SqlDbType.VarChar, 2)
        If vFEE_ADJ_DET.FAD_AMOUNT < 0 Then
            sqlpFSH_DRCR.Value = "CR"
        Else
            sqlpFSH_DRCR.Value = "DR"
        End If
        cmd.Parameters.Add(sqlpFSH_DRCR)

        Dim sqlpFSH_MONTHS As New SqlParameter("@FSH_MONTHS", SqlDbType.Int)
        sqlpFSH_MONTHS.Value = 1
        cmd.Parameters.Add(sqlpFSH_MONTHS)

        Dim sqlpFSH_ROUNDOFF As New SqlParameter("@FSH_ROUNDOFF", SqlDbType.Decimal)
        sqlpFSH_ROUNDOFF.Value = 0
        cmd.Parameters.Add(sqlpFSH_ROUNDOFF)

        Dim sqlpFSH_NARRATION As New SqlParameter("@FSH_NARRATION", SqlDbType.VarChar, 100)
        sqlpFSH_NARRATION.Value = vFEE_ADJ_DET.FAD_REMARKS
        cmd.Parameters.Add(sqlpFSH_NARRATION)

        '	@FSH_STU_TYPE varchar(4) 
        '	@FSH_MONTHS tinyint=1 ,@
        '   FSH_ROUNDOFF(numeric(18, 3) = 0)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function

    Private Shared Function GetSCTID(ByVal vBSU_ID As String, ByVal vACD_ID As Integer, ByVal vGRD_ID As String, ByVal vSTU_ID As Integer) As Integer
        Dim str_sql As String
        str_sql = "SELECT ISNULL(STU_SCT_ID,0) FROM STUDENT_M WHERE STU_ID = " & vSTU_ID
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Private Shared Function SaveFeeAdjustmentDetails(ByVal vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFeeAdjustmentRequestTransport_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAD_FAH_ID As New SqlParameter("@FAD_FAH_ID", SqlDbType.Int)
        sqlpFAD_FAH_ID.Value = FAH_ID 'STU_SUB_DET.FPD_FPH_ID
        cmd.Parameters.Add(sqlpFAD_FAH_ID)

        Dim sqlpFAD_FEE_ID As New SqlParameter("@FAD_FEE_ID", SqlDbType.Int)
        sqlpFAD_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFAD_FEE_ID)

        Dim sqlpFAD_TO_FEE_ID As New SqlParameter("@FAD_TO_FEE_ID", SqlDbType.Int)
        sqlpFAD_TO_FEE_ID.Value = DBNull.Value
        cmd.Parameters.Add(sqlpFAD_TO_FEE_ID)

        Dim sqlpFAD_AMOUNT As New SqlParameter("@FAD_AMOUNT", SqlDbType.Decimal)
        sqlpFAD_AMOUNT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFAD_AMOUNT)

        Dim sqlpFAD_REMARKS As New SqlParameter("@FAD_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAD_REMARKS.Value = vFEE_ADJ_DET.FAD_REMARKS
        cmd.Parameters.Add(sqlpFAD_REMARKS)

        If vFEE_ADJ_DET.bDelete OrElse vFEE_ADJ_DET.bEdit Then
            Dim sqlpFAD_ID As New SqlParameter("@FAD_ID", SqlDbType.Int)
            sqlpFAD_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFAD_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = vFEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function

    Public Shared Function PopulateFeeMaster(ByVal vBSUID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String) As DataTable
        Dim connStr As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query As String = "SELECT FEES.FEES_M.FEE_ID, " & _
        " FEES.FEES_M.FEE_DESCR " & _
        " FROM FEES.FEESETUP_S INNER JOIN FEES.FEES_M " & _
        " ON FEES.FEESETUP_S.FSP_FEE_ID = FEES.FEES_M.FEE_ID " & _
        " WHERE FEES.FEESETUP_S.FSP_bActive = 1 AND " & _
        " FEES.FEESETUP_S.FSP_ACD_ID = '" & vACD_ID & "' AND " & _
        " FEES.FEESETUP_S.FSP_GRD_ID = '" & vGRD_ID & "' AND " & _
        " FEES.FEESETUP_S.FSP_BSU_ID ='" & vBSUID & "' "
        '" AND FEES.FEESETUPMONTHLY_D.FDD_REF_ID = '" & vTRM_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql_query)
        If Not dsData Is Nothing AndAlso Not dsData.Tables(0) Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

End Class

Public Class FeeAdjustmentRequestTransport_S
    Dim vFAD_ID As Integer
    Dim vFAD_FAH_ID As Integer
    Dim vFAD_FEE_ID As Integer
    Dim vFRS_TO_FEE_ID As Integer
    Dim vFAD_TO_FEE_ID As Integer
    Dim vTO_FEE_DESCR As String
    Dim vFAD_AMOUNT As Double

    Dim vFAD_TAX_CODE As String
    Dim vFAD_TAX_AMOUNT As Double
    Dim vFAD_NET_AMOUNT As Double

    Dim vFAD_REMARKS As String
    Dim vFEE_DESCR As String
    Dim vFPM_ID As Integer
    Dim vAdjType As Integer
    Dim vOtherCount As Integer
    Public bMonth As Boolean
    Public bDelete As Boolean
    Public bEdit As Boolean
    Dim vAPPR_AMOUNT As Double
    Dim vDurationDescription As String

    Public Property FEE_TYPE() As String
        Get
            Return vFEE_DESCR
        End Get
        Set(ByVal value As String)
            vFEE_DESCR = value
        End Set
    End Property

    Public Property TO_FEE_TYPE() As String
        Get
            Return vTO_FEE_DESCR
        End Get
        Set(ByVal value As String)
            vTO_FEE_DESCR = value
        End Set
    End Property

    Public Property TO_FAD_FEE_ID() As Integer
        Get
            Return vFAD_TO_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_TO_FEE_ID = value
        End Set
    End Property

    Public Property FAD_REMARKS() As String
        Get
            Return vFAD_REMARKS
        End Get
        Set(ByVal value As String)
            vFAD_REMARKS = value
        End Set
    End Property

    Public Property FAD_AMOUNT() As Double
        Get
            Return vFAD_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFAD_AMOUNT = value
        End Set
    End Property
    Public Property FAD_TAX_CODE() As String
        Get
            Return vFAD_TAX_CODE
        End Get
        Set(ByVal value As String)
            vFAD_TAX_CODE = value
        End Set
    End Property
    Public Property FAD_TAX_AMOUNT() As Double
        Get
            Return vFAD_TAX_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFAD_TAX_AMOUNT = value
        End Set
    End Property
    Public Property FAD_NET_AMOUNT() As Double
        Get
            Return vFAD_NET_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFAD_NET_AMOUNT = value
        End Set
    End Property
    Public Property AdjustmentType() As Integer
        Get
            Return vAdjType
        End Get
        Set(ByVal value As Integer)
            vAdjType = value
        End Set
    End Property

    Public Property DurationCount() As Integer
        Get
            Return vOtherCount
        End Get
        Set(ByVal value As Integer)
            vOtherCount = value
        End Set
    End Property

    Public Property DurationDescription() As String
        Get
            Return vDurationDescription
        End Get
        Set(ByVal value As String)
            vDurationDescription = value
        End Set
    End Property

    Public Property FAD_FEE_ID() As Integer
        Get
            Return vFAD_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_FEE_ID = value
        End Set
    End Property

    Public Property FRS_TO_FEE_ID() As Integer
        Get
            Return vFRS_TO_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFRS_TO_FEE_ID = value
        End Set
    End Property

    Public Property FPM_ID() As Integer
        Get
            Return vFPM_ID
        End Get
        Set(ByVal value As Integer)
            vFPM_ID = value
        End Set
    End Property

    Public Property FAD_FAH_ID() As Integer
        Get
            Return vFAD_FAH_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_FAH_ID = value
        End Set
    End Property

    Public Property FAD_ID() As Integer
        Get
            Return vFAD_ID
        End Get
        Set(ByVal value As Integer)
            vFAD_ID = value
        End Set
    End Property

    Public Property APPROVEDAMOUNT() As Double
        Get
            Return vAPPR_AMOUNT
        End Get
        Set(ByVal value As Double)
            vAPPR_AMOUNT = value
        End Set
    End Property

End Class