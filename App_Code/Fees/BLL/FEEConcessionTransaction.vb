Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Public Class FEEConcessionTransaction

    Dim vFCH_ID As Integer
    Dim vFCH_ACD_ID As Integer
    Dim vFCH_STU_ID As Integer
    Dim vFCH_FCM_ID As Integer
    Dim vFCH_REF_ID As Integer
    Dim vFCH_REF_NAME As String
    Dim vFCH_DT As Date
    Dim vFCH_DTFROM As Date
    Dim vFCH_DTTO As Date
    Dim vFCH_BSU_ID As String
    Dim vSTU_NAME As String
    Dim vFCH_REMARKS As String
    Dim vFCH_DRCR As String
    Dim vFCH_EMP_ID As String
    Dim vFCH_FCH_ID As String
    Dim vFCH_GRM_DISPLAY As String
    Dim vFCH_STU_SIBLING_ID As String
    Dim vFCH_IS_DIFFERENT_S_B As Boolean
    Dim vFCH_bDELETE As Boolean
    Dim vFCH_CANC_RECNO As String
    Dim vNextAcd_Concession As Boolean
    Dim vFCH_USER As String
    Dim vFCH_ROUNDOFF As Integer
    Dim vFCH_bPOSTED As Boolean
    'Dim vFCH_ISINCLUSIVE_TAX As Boolean
    Public bEdit As Boolean
    Dim arrSubList As ArrayList

    Public Property SUB_DETAILS() As ArrayList
        Get
            Return arrSubList
        End Get
        Set(ByVal value As ArrayList)
            arrSubList = value
        End Set
    End Property

    Public Property FCH_REMARKS() As String
        Get
            Return vFCH_REMARKS
        End Get
        Set(ByVal value As String)
            vFCH_REMARKS = value
        End Set
    End Property
    Public Property CANC_RECNO() As String
        Get
            Return vFCH_CANC_RECNO
        End Get
        Set(ByVal value As String)
            vFCH_CANC_RECNO = value
        End Set
    End Property

    Public Property FCH_DRCR() As String
        Get
            Return vFCH_DRCR
        End Get
        Set(ByVal value As String)
            vFCH_DRCR = value
        End Set
    End Property

    Public Property FCH_EMP_ID() As String
        Get
            Return vFCH_EMP_ID
        End Get
        Set(ByVal value As String)
            vFCH_EMP_ID = value
        End Set
    End Property

    Public Property FCH_FCH_ID() As String
        Get
            Return vFCH_FCH_ID
        End Get
        Set(ByVal value As String)
            vFCH_FCH_ID = value
        End Set
    End Property

    Public Property STU_NAME() As String
        Get
            Return vSTU_NAME
        End Get
        Set(ByVal value As String)
            vSTU_NAME = value
        End Set
    End Property

    Public Property NextAcd_Concession() As Boolean
        Get
            Return vNextAcd_Concession
        End Get
        Set(ByVal value As Boolean)
            vNextAcd_Concession = value
        End Set
    End Property

    Public Property FCH_BSU_ID() As String
        Get
            Return vFCH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFCH_BSU_ID = value
        End Set
    End Property


    Public Property FCH_DTTO() As Date
        Get
            Return vFCH_DTTO
        End Get
        Set(ByVal value As Date)
            vFCH_DTTO = value
        End Set
    End Property

    Public Property FCH_DTFROM() As Date
        Get
            Return vFCH_DTFROM
        End Get
        Set(ByVal value As Date)
            vFCH_DTFROM= value
        End Set
    End Property

    Public Property FCH_DT() As Date
        Get
            Return vFCH_DT
        End Get
        Set(ByVal value As Date)
            vFCH_DT = value
        End Set
    End Property

    Public Property FCH_REF_ID() As Integer
        Get
            Return vFCH_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_REF_ID = value
        End Set
    End Property

    Public Property FCH_REF_NAME() As String
        Get
            Return vFCH_REF_NAME
        End Get
        Set(ByVal value As String)
            vFCH_REF_NAME = value
        End Set
    End Property

    Public Property GRM_DISPLAY() As String
        Get
            Return vFCH_GRM_DISPLAY
        End Get
        Set(ByVal value As String)
            vFCH_GRM_DISPLAY = value
        End Set
    End Property

    Public Property FCH_FCM_ID() As Integer
        Get
            Return vFCH_FCM_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_FCM_ID = value
        End Set
    End Property

    Public Property FCH_STU_ID() As Integer
        Get
            Return vFCH_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_STU_ID = value
        End Set
    End Property

    Public Property FCH_STU_SIBLING_ID() As String
        Get
            Return vFCH_STU_SIBLING_ID
        End Get
        Set(ByVal value As String)
            vFCH_STU_SIBLING_ID = value
        End Set
    End Property
    Public Property FCH_IS_DIFFERENT_S_B() As String
        Get
            Return vFCH_IS_DIFFERENT_S_B
        End Get
        Set(ByVal value As String)
            vFCH_IS_DIFFERENT_S_B = value
        End Set
    End Property

    Public Property FCH_ACD_ID() As Integer
        Get
            Return vFCH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_ACD_ID = value
        End Set
    End Property

    Public Property FCH_ID() As Integer
        Get
            Return vFCH_ID
        End Get
        Set(ByVal value As Integer)
            vFCH_ID = value
        End Set
    End Property

    Public Property FCH_USER() As String
        Get
            Return vFCH_USER
        End Get
        Set(ByVal value As String)
            vFCH_USER = value
        End Set
    End Property

    Public Property FCH_ROUNDOFF() As Integer
        Get
            Return vFCH_ROUNDOFF
        End Get
        Set(ByVal value As Integer)
            vFCH_ROUNDOFF = value
        End Set
    End Property

    Public Property FCH_bPOSTED() As Boolean
        Get
            Return vFCH_bPOSTED
        End Get
        Set(ByVal value As Boolean)
            vFCH_bPOSTED = value
        End Set
    End Property

    'Public Property FCH_ISINCLUSIVE_TAX() As Boolean
    '    Get
    '        Return vFCH_ISINCLUSIVE_TAX
    '    End Get
    '    Set(ByVal value As Boolean)
    '        vFCH_ISINCLUSIVE_TAX = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Function used to Populate the Concession Type Details to the 
    ''' Screen.It Returns a data table containing all Fee type 
    ''' from the Concession Type master Table FEE_CONCESSIONTYPE_M
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function PopulateConcessionType() As DataTable
        Dim dsData As DataSet
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim sql_query As String = "select FCT_ID, FCT_DESCR from FEES.FEE_CONCESSIONTYPE_M WITH(NOLOCK) ORDER BY FCT_DESCR"
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    ''' <summary>
    ''' Function used to Populate the FEE Types which is allowed for concession.
    '''
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function GetConcessionFEEType(ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim dsData As DataSet
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim sql_query As String = "exec FEES.SP_GET_CONCESSION_FEE_TYPES @BSU_ID='" & BSU_ID.ToString & "',@ACD_ID = '" & ACD_ID.ToString & "'"
            ' Dim sql_query As String = " SELECT FEES.FEES_M.FEE_ID, FEES.FEES_M.FEE_DESCR " _
            '& " FROM FEES.FEES_M where FEE_bConcession = 1 and FEE_ID NOT IN ( " _
            '& " SELECT FEES.FEES_M.FEE_ID FROM  FEES.FEES_M INNER JOIN " _
            '& " OASIS..SERVICES_BSU_M AS SVB ON FEES.FEES_M.FEE_SVC_ID = SVB.SVB_SVC_ID" _
            '& " AND SVB.SVB_PROVIDER_BSU_ID<> SVB.SVB_BSU_ID AND SVB.SVB_BSU_ID='" & BSU_ID & "'" _
            '& " AND SVB_ACD_ID='" & ACD_ID & "') AND FEE_ID IN (SELECT FSP_FEE_ID " _
            '& " FROM FEES.FEESETUP_S where FSP_ACD_ID='" & ACD_ID & "' and FSP_BSU_ID='" & BSU_ID & "') " _
            '& " ORDER BY FEES.FEES_M.FEE_DESCR DESC "
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function StudentConcessions(ByVal BSU_ID As String, ByVal STU_ID As String, ByVal AcdId As String) As DataTable
        Dim dsData As DataSet
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim sql_query As String = " SELECT  FCH_ID, FCH_RECNO FROM FEES.FEE_CONCESSION_H WHERE isnull(FCH_FCH_ID,0)=0 AND FCH_ID NOT IN " _
            & " (SELECT FCH_FCH_ID FROM FEES.FEE_CONCESSION_H AS FEE_CONCESSION_H_1 WITH(NOLOCK) WHERE ISNULL(FCH_FCH_ID,0)<> 0 " _
            & " AND FCH_BSU_ID = '" & BSU_ID & "' AND ISNULL(FCH_bDeleted, 0) = 0 ) AND FCH_BSU_ID = '" & BSU_ID & "' AND ISNULL(FCH_bPosted,0) = 1 AND ISNULL(FCH_bDeleted, 0) = 0 " _
            & " AND FCH_ACD_ID  =" & AcdId & "  AND FCH_STU_ID =" & STU_ID & " AND FCH_DRCR ='CR' ORDER BY FCH_RECNO "

            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetConcessionHistory(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String) As DataTable
        Dim sql_query As String = " SELECT  REPLACE(CONVERT(VARCHAR(11),FCH.FCH_DT, 113), ' ', '/') as FCH_DT , FCD.FCD_AMOUNT, FCM.FCM_DESCR, FEE.FEE_DESCR" _
            & " FROM FEES.FEE_CONCESSION_H AS FCH INNER JOIN" _
            & " FEES.FEE_CONCESSION_D AS FCD ON FCH.FCH_ID = FCD.FCD_FCH_ID INNER JOIN" _
            & " FEES.FEE_CONCESSION_M AS FCM WITH(NOLOCK) ON FCD.FCD_FCM_ID = FCM.FCM_ID INNER JOIN" _
            & " FEES.FEES_M AS FEE WITH(NOLOCK) ON FCD.FCD_FEE_ID = FEE.FEE_ID" _
            & " WHERE     (FCH.FCH_ACD_ID = '" & ACD_ID & "') AND (FCH.FCH_BSU_ID = '" & BSU_ID & "') " _
            & " AND (FCH.FCH_STU_ID = '" & STU_ID & "')"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function RefRepetition(ByVal refType As ConcessionType, ByVal REF_ID As Integer) As Boolean
        Dim sql_query As String
        Dim count As Integer = 0
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            sql_query = "SELECT COUNT(1) FROM  FEES.FEE_CONCESSION_H WHERE FCH_FCM_ID=" & _
                    refType & " AND FCH_REF_ID =" & REF_ID
            count = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    Public Shared Function GetEmployeeBSU(ByVal EMP_ID As String, ByVal STU_ID As String) As String
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT EMP.EMP_BSU_ID FROM EMPLOYEE_M AS EMP WITH(NOLOCK) WHERE EMP.EMP_ID ='" & STU_ID & "' "
            Return SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query).ToString
        End Using
    End Function

    Public Shared Function CheckPreviousConcession(ByVal refType As ConcessionType, ByVal ACD_ID As Integer, _
        ByVal BSU_ID As String, ByVal STU_ID As String) As Boolean
        Dim sql_query As String
        Dim count As Integer = 0
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            sql_query = "SELECT COUNT(1) FROM FEES.FEE_CONCESSIONTYPE_M AS FCT WITH(NOLOCK) INNER JOIN" & _
            " FEES.FEE_CONCESSION_M AS FCM WITH(NOLOCK) ON FCT.FCT_ID = FCM.FCM_FCT_ID INNER JOIN" & _
            " FEES.FEE_CONCESSION_H AS FCH ON FCM.FCM_ID = FCH.FCH_FCM_ID " & _
            " WHERE FCT_ID='" & refType & "' AND FCH_ACD_ID='" & ACD_ID & "' " & _
            " AND FCH_BSU_ID ='" & BSU_ID & "' AND FCH_STU_ID='" & STU_ID & "'"
            count = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function
    ''' <summary>
    ''' This function checks whether entered ReferenceID is valid one or not.
    ''' It 
    ''' </summary>
    ''' <param name="refType"></param>
    ''' <param name="ID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ValidateReference(ByVal refType As ConcessionType, ByVal ID As String, ByVal BSU_ID As String) As Boolean
        Dim sql_query As String
        Dim count As Integer = 0
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Select Case refType
                Case ConcessionType.Sibling
                    'sql_query = "SELECT count(*) FROM STUDENT_M WHERE STU_bActive = 1 " & _ Req: 117356
                    sql_query = "SELECT count(1) FROM STUDENT_M WITH(NOLOCK) WHERE STU_CURRSTATUS='EN' " & _
                    " AND (STU_ID='" & ID & "' OR STU_FEE_ID='" & ID & "') "
                    count = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
                Case ConcessionType.Staff
                    sql_query = "SELECT count(1) FROM  EMPLOYEE_M WITH(NOLOCK) WHERE EMP_RESGDT is null" & _
                    " AND (EMP_ID='" & ID & "' OR EMPNO='" & ID & "') "
                    count = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
            End Select
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    Public Shared Function GetDetails(ByVal STU_ID As Integer, ByVal type As Int32, _
    ByVal ACD_ID As Integer, ByVal BSU_ID As String, ByVal bCancel As Boolean) As FEEConcessionTransaction
        Dim vFEE_CON As New FEEConcessionTransaction
        'Dim sql_query As String
        Dim cmd As New SqlCommand
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            cmd = New SqlCommand("[FEES].[F_GETFEECONCESSIONTRANSVIEW]", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
            sqlpFCH_ID.Value = STU_ID
            cmd.Parameters.Add(sqlpFCH_ID)

            Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
            sqlpACD_ID.Value = ACD_ID
            cmd.Parameters.Add(sqlpACD_ID)

            Dim sqlpGET_ALL As New SqlParameter("@GET_ALL", SqlDbType.Bit)
            Select Case type
                Case 1
                    sqlpGET_ALL.Value = False
                Case 2
                    sqlpGET_ALL.Value = True
            End Select
            cmd.Parameters.Add(sqlpGET_ALL)
            Dim drReader As SqlDataReader = cmd.ExecuteReader()
            'Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                vFEE_CON.FCH_ACD_ID = drReader("FCH_ACD_ID")
                vFEE_CON.FCH_BSU_ID = BSU_ID
                vFEE_CON.FCH_DT = drReader("FCH_DT")
                vFEE_CON.FCH_DTFROM = drReader("FCH_DTFROM")
                vFEE_CON.FCH_DTTO = drReader("FCH_DTTO")
                vFEE_CON.FCH_FCM_ID = drReader("FCH_FCM_ID")
                vFEE_CON.FCH_ID = drReader("FCH_ID")
                vFEE_CON.FCH_REF_ID = drReader("FCH_REF_ID")
                vFEE_CON.FCH_REF_NAME = drReader("REF_NAME").ToString
                vFEE_CON.FCH_REMARKS = drReader("FCH_REMARKS")
                vFEE_CON.FCH_STU_ID = drReader("STU_ID")
                vFEE_CON.STU_NAME = drReader("STU_NAME")
                vFEE_CON.SUB_DETAILS = PopulateSubDetails(drReader("FCH_ID"))
                vFEE_CON.GRM_DISPLAY = drReader("GRM_DISPLAY")
                vFEE_CON.FCH_EMP_ID = drReader("EMP_ID")
                vFEE_CON.FCH_FCH_ID = drReader("FCH_FCH_ID")
                vFEE_CON.CANC_RECNO = drReader("CANC_RECNO")
                vFEE_CON.FCH_bPOSTED = drReader("FCH_bPosted")

                'Added by Nikunj (14-June-2020)
                vFEE_CON.FCH_STU_SIBLING_ID = drReader("FCH_STU_SIBLING_ID")
                vFEE_CON.FCH_IS_DIFFERENT_S_B = drReader("FCH_IS_DIFFERENT_S_B")
                'vFEE_CON.FCH_ISINCLUSIVE_TAX = drReader("FCH_ISINCLUSIVE_TAX")
                For Each vFEE_CON_SUB As FEE_CONC_TRANC_SUB In vFEE_CON.SUB_DETAILS
                    vFEE_CON_SUB.SubList_Monthly = PopulateSubDetails_MONTHLY(vFEE_CON_SUB.FCD_ID, bCancel)
                Next
            End While
        End Using
        Return vFEE_CON
    End Function

    Public Shared Function PopulateSubDetails_MONTHLY(ByVal FCD_ID As Integer, ByVal bCancel As Boolean) As ArrayList
        Dim arrDetList As New ArrayList
        Dim sql_query As String
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            sql_query = "SELECT FMD_ID, FMD_FCD_ID, FMD_REF_ID, FMD_DATE, FMD_AMOUNT,FMD_ORG_AMOUNT " & _
            " FROM FEES.FEE_CONCESSIONMONTHLY_D WHERE FMD_FCD_ID='" & FCD_ID & "' "
            sql_query = "SELECT FMD.FMD_ID, FMD.FMD_FCD_ID, FMD.FMD_REF_ID, REPLACE(CONVERT(VARCHAR(11), FMD.FMD_DATE, 113), ' ', '/') AS FMD_DATE," _
           & " FMD.FMD_AMOUNT,ISNULL(FMD.FMD_TAX_AMOUNT,0) FMD_TAX_AMOUNT,ISNULL(FMD.FMD_NET_AMOUNT,0) FMD_NET_AMOUNT, FMD.FMD_ORG_AMOUNT, FCD.FCD_REF_ID, DESCR.DESCR" _
           & " FROM FEES.FEE_CONCESSIONMONTHLY_D AS FMD " _
           & " INNER JOIN FEES.FEE_CONCESSION_D AS FCD ON FMD.FMD_FCD_ID = FCD.FCD_ID " _
           & " LEFT OUTER JOIN (SELECT TRM_ID AS ID, 2 AS SCH, TRM_DESCRIPTION AS DESCR" _
           & " FROM OASIS..TRM_M WITH(NOLOCK) UNION SELECT AMS_ID AS ID, 0 AS SCH, " _
           & " DATENAME(month,  ISNULL(AMS_FEE_DTFROM, AMS_DTFROM)) + '-' + LTRIM(STR(DATEPART(yyyy, ISNULL(AMS_FEE_DTFROM ,AMS_DTFROM)))) AS DESCR" _
           & " FROM OASIS..ACADEMIC_MonthS_S WITH(NOLOCK)) AS DESCR ON CASE FCD.FCD_SCH_ID WHEN 2 THEN 2 ELSE 0 END = DESCR.SCH AND FMD.FMD_REF_ID = DESCR.ID" _
           & " WHERE FMD.FMD_FCD_ID='" & FCD_ID & "'"
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                Dim vFEE_CON_SUB_MONTHLY As New FEE_CONC_TRANC_SUB_MONTHLY
                If bCancel Then
                    vFEE_CON_SUB_MONTHLY.FMD_AMOUNT = drReader("FMD_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_ORG_AMOUNT = drReader("FMD_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_TAXAMOUNT = drReader("FMD_TAX_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_NETAMOUNT = drReader("FMD_NET_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_PRO_AMOUNT = drReader("FMD_ORG_AMOUNT")
                Else
                    vFEE_CON_SUB_MONTHLY.FMD_TAXAMOUNT = drReader("FMD_TAX_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_NETAMOUNT = drReader("FMD_NET_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_ORG_AMOUNT = drReader("FMD_ORG_AMOUNT")
                    vFEE_CON_SUB_MONTHLY.FMD_AMOUNT = drReader("FMD_AMOUNT")
                End If

                vFEE_CON_SUB_MONTHLY.FMD_DATE = drReader("FMD_DATE")
                vFEE_CON_SUB_MONTHLY.FMD_FCD_ID = FCD_ID
                vFEE_CON_SUB_MONTHLY.FMD_ID = drReader("FMD_ID")
                vFEE_CON_SUB_MONTHLY.FMD_REF_ID = drReader("FMD_REF_ID")
                vFEE_CON_SUB_MONTHLY.FMD_REF_NAME = drReader("DESCR")
                arrDetList.Add(vFEE_CON_SUB_MONTHLY)
            End While
        End Using
        Return arrDetList
    End Function

    Public Shared Function DuplicateFeeType(ByVal arrSubList As ArrayList, ByVal FEE_ID As Integer, ByVal S_FCD_ID As String) As Boolean
        Dim FCD_ID As Integer = 0
        If S_FCD_ID <> "" Then
            FCD_ID = CInt(S_FCD_ID)
        End If
        If arrSubList Is Nothing Then
            Return False
        End If
        Dim CON_SUB_LIST As FEE_CONC_TRANC_SUB
        For i As Integer = 0 To arrSubList.Count - 1
            CON_SUB_LIST = arrSubList(i)
            If CON_SUB_LIST.FCD_FEE_ID = FEE_ID And FCD_ID <> CON_SUB_LIST.FCD_ID Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Shared Function PeriodBelongstoAcademicYear(ByVal FromDT As DateTime, ByVal ToDT As DateTime, ByVal ACD_ID As Integer, ByVal BSU_ID As String) As Boolean
        Dim drReader As SqlDataReader
        Dim vFromDT, vToDT As DateTime
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT ACD_STARTDT, ACD_ENDDT FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE ACD_BSU_ID = '" & BSU_ID & "' AND ACD_ID = " & ACD_ID
            drReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                vFromDT = drReader("ACD_STARTDT")
                vToDT = drReader("ACD_ENDDT")
                Exit While
            End While
        End Using
        If vFromDT <= FromDT AndAlso vToDT >= ToDT Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function PopulateSubDetails(ByVal FCH_ID As Integer) As ArrayList
        Dim arrDetList As New ArrayList
        Dim sql_query As String
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection

            ' sql_query = "SELECT FCD.FCD_ID, FCD.FCD_FCH_ID, FCD.FCD_FCM_ID, FCD.FCD_REF_ID, " _
            '& " FCD.FCD_FEE_ID, FCD.FCD_AMTTYPE, FCD.FCD_AMOUNT,ISNULL(FCD.FCD_TAX_AMOUNT,0) FCD_TAX_AMOUNT,ISNULL(FCD.FCD_NET_AMOUNT,0) FCD_NET_AMOUNT, ISNULL(FCD.FCD_ISINCLUSIVE_TAX,0) FCD_ISINCLUSIVE_TAX, ISNULL(FCD.FCD_TAX_CODE,'NA') FCD_TAX_CODE, FCD.FCD_ACTUALAMT, FEE.FEE_DESCR, FCM.FCM_DESCR, " _
            '& " CASE FCD.FCD_FCM_ID WHEN 2 THEN ISNULL(EMP.EMP_FNAME, '') + ' ' + ISNULL(EMP.EMP_MNAME, '') " _
            '& " + ' ' + ISNULL(EMP.EMP_LNAME, '') WHEN 1 THEN ISNULL(STU.STU_FIRSTNAME, '') " _
            '& " + ' ' + ISNULL(STU.STU_MIDNAME, '') + ' ' + ISNULL(STU.STU_LASTNAME, '') END AS REF_NAME, " _
            '& " FCD.FCD_SCH_ID,FCD.FCD_REF_BSU_ID FROM FEES.FEE_CONCESSION_D AS FCD LEFT OUTER JOIN" _
            '& " FEES.FEES_M AS FEE WITH(NOLOCK) ON FCD.FCD_FEE_ID = FEE.FEE_ID LEFT OUTER JOIN" _
            '& " FEES.FEE_CONCESSION_M AS FCM WITH(NOLOCK) ON FCD.FCD_FCM_ID = FCM.FCM_ID LEFT OUTER JOIN" _
            '& " OASIS..EMPLOYEE_M AS EMP WITH(NOLOCK) ON FCD.FCD_REF_ID = EMP.EMP_ID LEFT OUTER JOIN" _
            '& " OASIS..STUDENT_M AS STU WITH(NOLOCK) ON FCD.FCD_REF_ID = STU.STU_ID" _
            '& " WHERE FCD.FCD_FCH_ID = " & FCH_ID

            sql_query = "SELECT FCD.FCD_ID, FCD.FCD_FCH_ID, FCD.FCD_FCM_ID, FCD.FCD_REF_ID, FCD.FCD_FEE_ID, FCD.FCD_AMTTYPE, FCD.FCD_AMOUNT, ISNULL(FCD.FCD_TAX_AMOUNT, 0) FCD_TAX_AMOUNT," _
            & "ISNULL(FCD.FCD_NET_AMOUNT, 0) FCD_NET_AMOUNT, ISNULL(FCD.FCD_ISINCLUSIVE_TAX, 0) FCD_ISINCLUSIVE_TAX, ISNULL(FCD.FCD_TAX_CODE, TAX_CODE) FCD_TAX_CODE, FCD.FCD_ACTUALAMT, FEE.FEE_DESCR," _
            & "FCM.FCM_DESCR, CASE FCD.FCD_FCM_ID WHEN 2 THEN " _
            & "ISNULL(EMP.EMP_FNAME, '') + ' ' + ISNULL(EMP.EMP_MNAME, '') + ' ' + ISNULL(EMP.EMP_LNAME, '')" _
            & "WHEN 1 THEN " _
            & "ISNULL(STU.STU_FIRSTNAME, '') + ' ' + ISNULL(STU.STU_MIDNAME, '') + ' ' + ISNULL(STU.STU_LASTNAME, '') END AS REF_NAME, FCD.FCD_SCH_ID, FCD.FCD_REF_BSU_ID " _
            & "FROM FEES.FEE_CONCESSION_H " _
            & "INNER JOIN FEES.FEE_CONCESSION_D AS FCD ON FCD.FCD_FCH_ID = FEE_CONCESSION_H.FCH_ID " _
            & "CROSS APPLY OASIS.TAX.GetTAXCodeAndAmount_EXT('FEES', FCH_BSU_ID, 'FEE', FCD_FEE_ID, FCH_DT, FCD_AMOUNT, '', 0) " _
            & "LEFT OUTER JOIN FEES.FEES_M AS FEE WITH (NOLOCK) ON FCD.FCD_FEE_ID = FEE.FEE_ID " _
            & "LEFT OUTER JOIN FEES.FEE_CONCESSION_M AS FCM WITH (NOLOCK) ON FCD.FCD_FCM_ID = FCM.FCM_ID " _
            & "LEFT OUTER JOIN OASIS..EMPLOYEE_M AS EMP WITH (NOLOCK) ON FCD.FCD_REF_ID = EMP.EMP_ID " _
            & "LEFT OUTER JOIN OASIS..STUDENT_M AS STU WITH (NOLOCK) ON FCD.FCD_REF_ID = STU.STU_ID " _
            & "WHERE FCD.FCD_FCH_ID = " & FCH_ID

            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB
                'If drReader("FCD_AMTTYPE") = 1 Then
                '    vFEE_CON_SUB.PERCENTAGE = drReader("FCD_AMOUNT")
                '    vFEE_CON_SUB.FCD_AMOUNT = drReader("FCD_AMOUNT")
                'Else
                '    vFEE_CON_SUB.PERCENTAGE = "N/A"
                '    vFEE_CON_SUB.FCD_AMOUNT = drReader("FCD_AMOUNT")
                'End If

                vFEE_CON_SUB.FCD_AMOUNT = drReader("FCD_AMOUNT")
                vFEE_CON_SUB.FCD_TAX_AMOUNT = drReader("FCD_TAX_AMOUNT")
                vFEE_CON_SUB.FCD_NET_AMOUNT = drReader("FCD_NET_AMOUNT")
                vFEE_CON_SUB.FCD_ISINCLUSIVE_TAX = drReader("FCD_ISINCLUSIVE_TAX")
                vFEE_CON_SUB.FCD_TAX_CODE = drReader("FCD_TAX_CODE")
                vFEE_CON_SUB.FCD_ACTUALAMT = drReader("FCD_ACTUALAMT")
                vFEE_CON_SUB.FCD_AMTTYPE = drReader("FCD_AMTTYPE")
                vFEE_CON_SUB.FCD_FCH_ID = drReader("FCD_FCH_ID")
                vFEE_CON_SUB.FCD_FCM_ID = drReader("FCD_FCM_ID")
                vFEE_CON_SUB.FCD_FEE_ID = drReader("FCD_FEE_ID")
                vFEE_CON_SUB.FCD_ID = drReader("FCD_ID")
                vFEE_CON_SUB.FCD_REF_ID = drReader("FCD_REF_ID")
                vFEE_CON_SUB.FCD_REF_NAME = drReader("REF_NAME").ToString
                vFEE_CON_SUB.FCM_DESCR = drReader("FCM_DESCR").ToString
                vFEE_CON_SUB.FEE_DESCR = drReader("FEE_DESCR").ToString
                vFEE_CON_SUB.FCD_SCH_ID = drReader("FCD_SCH_ID").ToString
                vFEE_CON_SUB.FCD_REF_BSU_ID = drReader("FCD_REF_BSU_ID").ToString
                arrDetList.Add(vFEE_CON_SUB)
            End While
        End Using
        Return arrDetList
    End Function

    Public Shared Function F_SaveFEE_CONCESSION_H(ByVal CONCESSION_DET As FEEConcessionTransaction, _
    ByRef NEW_FCH_ID As String, ByVal FCT_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If CONCESSION_DET Is Nothing Then
            Return 426
        End If
        If CONCESSION_DET.SUB_DETAILS.Count = 0 Then
            Return 426
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSION_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFCH_ACD_ID As New SqlParameter("@FCH_ACD_ID", SqlDbType.VarChar, 20)
        sqlpFCH_ACD_ID.Value = CONCESSION_DET.FCH_ACD_ID
        cmd.Parameters.Add(sqlpFCH_ACD_ID)

        Dim sqlpFCH_BSU_ID As New SqlParameter("@FCH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCH_BSU_ID.Value = CONCESSION_DET.FCH_BSU_ID
        cmd.Parameters.Add(sqlpFCH_BSU_ID)

        Dim sqlpFCH_STU_ID As New SqlParameter("@FCH_STU_ID", SqlDbType.Int)
        sqlpFCH_STU_ID.Value = CONCESSION_DET.FCH_STU_ID
        cmd.Parameters.Add(sqlpFCH_STU_ID)

        Dim sqlpFCH_DT As New SqlParameter("@FCH_DT", SqlDbType.DateTime)
        sqlpFCH_DT.Value = CONCESSION_DET.FCH_DT
        cmd.Parameters.Add(sqlpFCH_DT)

        Dim sqlpFCH_DTFROM As New SqlParameter("@FCH_DTFROM", SqlDbType.DateTime)
        sqlpFCH_DTFROM.Value = CONCESSION_DET.FCH_DTFROM
        cmd.Parameters.Add(sqlpFCH_DTFROM)

        Dim sqlpFCH_DTTO As New SqlParameter("@FCH_DTTO", SqlDbType.DateTime)
        sqlpFCH_DTTO.Value = CONCESSION_DET.FCH_DTTO
        cmd.Parameters.Add(sqlpFCH_DTTO)

        Dim sqlpFCH_FCM_ID As New SqlParameter("@FCH_FCM_ID", SqlDbType.Int)
        sqlpFCH_FCM_ID.Value = CONCESSION_DET.FCH_FCM_ID
        cmd.Parameters.Add(sqlpFCH_FCM_ID)

        Dim sqlpFCH_REF_ID As New SqlParameter("@FCH_REF_ID", SqlDbType.Int)
        sqlpFCH_REF_ID.Value = CONCESSION_DET.FCH_REF_ID
        cmd.Parameters.Add(sqlpFCH_REF_ID)

        Dim sqlpFCH_REMARKS As New SqlParameter("@FCH_REMARKS", SqlDbType.VarChar, 200)
        sqlpFCH_REMARKS.Value = CONCESSION_DET.FCH_REMARKS
        cmd.Parameters.Add(sqlpFCH_REMARKS)

        If CONCESSION_DET.bEdit Then
            Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
            sqlpFCH_ID.Value = CONCESSION_DET.FCH_ID
            cmd.Parameters.Add(sqlpFCH_ID)
        End If

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = False
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpNEW_FCH_ID As New SqlParameter("@NEW_FCH_ID", SqlDbType.Int)
        sqlpNEW_FCH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEW_FCH_ID)

        Dim sqlpFCH_DRCR As New SqlParameter("@FCH_DRCR", SqlDbType.VarChar, 200)
        sqlpFCH_DRCR.Value = CONCESSION_DET.FCH_DRCR
        cmd.Parameters.Add(sqlpFCH_DRCR)

        Dim sqlpFCH_USER As New SqlParameter("@FCH_USER", SqlDbType.VarChar)
        sqlpFCH_USER.Value = CONCESSION_DET.FCH_USER
        cmd.Parameters.Add(sqlpFCH_USER)

        Dim sqlpFCH_ROUNDOFF As New SqlParameter("@FCH_ROUNDOFF", SqlDbType.Int)
        sqlpFCH_ROUNDOFF.Value = CONCESSION_DET.FCH_ROUNDOFF
        cmd.Parameters.Add(sqlpFCH_ROUNDOFF)

        Dim sqlpFCH_FCH_ID As New SqlParameter("@FCH_FCH_ID", SqlDbType.VarChar, 200)
        If CONCESSION_DET.FCH_FCH_ID Is Nothing Then
            sqlpFCH_FCH_ID.Value = System.DBNull.Value
        Else
            sqlpFCH_FCH_ID.Value = CONCESSION_DET.FCH_FCH_ID
        End If
        cmd.Parameters.Add(sqlpFCH_FCH_ID)


        'Added by Nikunj (14-June-2020)
        Dim sqlpFCH_STU_SIBLING_ID As New SqlParameter("@FCH_STU_SIBLING_ID", SqlDbType.VarChar)
        sqlpFCH_STU_SIBLING_ID.Value = CONCESSION_DET.FCH_STU_SIBLING_ID
        cmd.Parameters.Add(sqlpFCH_STU_SIBLING_ID)

        Dim sqlpFCH_IS_DIFFERENT_S_B As New SqlParameter("@FCH_IS_DIFFERENT_S_B", SqlDbType.Bit)
        sqlpFCH_IS_DIFFERENT_S_B.Value = CONCESSION_DET.FCH_IS_DIFFERENT_S_B
        cmd.Parameters.Add(sqlpFCH_IS_DIFFERENT_S_B)

        'Dim sqlpFCH_ISINCLUSIVE_TAX As New SqlParameter("@FCH_ISINCLUSIVE_TAX", SqlDbType.VarChar)
        'sqlpFCH_ISINCLUSIVE_TAX.Value = CONCESSION_DET.FCH_ISINCLUSIVE_TAX
        'cmd.Parameters.Add(sqlpFCH_ISINCLUSIVE_TAX)


        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim sqlpbNextAcd_Concession As New SqlParameter("@NextAcd_Concession", SqlDbType.Bit)
        sqlpbNextAcd_Concession.Value = CONCESSION_DET.vNextAcd_Concession
        cmd.Parameters.Add(sqlpbNextAcd_Concession)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        NEW_FCH_ID = sqlpNEW_FCH_ID.Value
        Dim REF_BSU_ID As String = String.Empty
        If FCT_ID = ConcessionType.Staff Then
            REF_BSU_ID = FEEConcessionTransaction.GetEmployeeBSU(CONCESSION_DET.FCH_FCM_ID, CONCESSION_DET.FCH_REF_ID)
        End If

        iReturnvalue = F_SaveFEE_CONCESSION_D(CONCESSION_DET.SUB_DETAILS, NEW_FCH_ID, _
        CONCESSION_DET.FCH_FCM_ID, CONCESSION_DET.FCH_REF_ID, REF_BSU_ID, conn, trans)
        If iReturnvalue = 0 Then
            iReturnvalue = FEEConcessionTransaction.UpdateConcessionActualAmount(NEW_FCH_ID, conn, trans)
        End If
        Return iReturnvalue
    End Function

    Private Shared Function F_SaveFEE_CONCESSION_D(ByVal arrSUBList As ArrayList, ByVal FCH_ID As Integer, _
    ByVal FCM_ID As String, ByVal REF_ID As String, ByVal REF_BSU_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If arrSUBList Is Nothing Then
            Return 426
        End If
        If arrSUBList.Count = 0 Then
            Return 426
        End If
        Dim CON_SUB_LIST As FEE_CONC_TRANC_SUB
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        CON_SUB_LIST = arrSUBList(0)
        For i As Integer = 0 To arrSUBList.Count - 1
            CON_SUB_LIST = arrSUBList(i)

            cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSION_D]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFCD_FCH_ID As New SqlParameter("@FCD_FCH_ID", SqlDbType.Int)
            sqlpFCD_FCH_ID.Value = FCH_ID
            cmd.Parameters.Add(sqlpFCD_FCH_ID)

            Dim sqlpFCD_FCM_ID As New SqlParameter("@FCD_FCM_ID", SqlDbType.Int)
            sqlpFCD_FCM_ID.Value = FCM_ID
            cmd.Parameters.Add(sqlpFCD_FCM_ID)

            Dim sqlpFCD_REF_ID As New SqlParameter("@FCD_REF_ID", SqlDbType.Int)
            sqlpFCD_REF_ID.Value = REF_ID
            cmd.Parameters.Add(sqlpFCD_REF_ID)

            Dim sqlpFCD_FEE_ID As New SqlParameter("@FCD_FEE_ID", SqlDbType.Int)
            sqlpFCD_FEE_ID.Value = CON_SUB_LIST.FCD_FEE_ID
            cmd.Parameters.Add(sqlpFCD_FEE_ID)

            Dim sqlpFCD_AMTTYPE As New SqlParameter("@FCD_AMTTYPE", SqlDbType.SmallInt)
            sqlpFCD_AMTTYPE.Value = CON_SUB_LIST.FCD_AMTTYPE
            cmd.Parameters.Add(sqlpFCD_AMTTYPE)

            Dim sqlpFCD_AMOUNT As New SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFCD_AMOUNT.Value = CON_SUB_LIST.FCD_AMOUNT
            cmd.Parameters.Add(sqlpFCD_AMOUNT)

            Dim sqlpFCD_TAXAMOUNT As New SqlParameter("@FCD_TAXAMOUNT", SqlDbType.Decimal, 21)
            sqlpFCD_TAXAMOUNT.Value = CON_SUB_LIST.FCD_TAX_AMOUNT
            cmd.Parameters.Add(sqlpFCD_TAXAMOUNT)

            Dim sqlpFCD_NETAMOUNT As New SqlParameter("@FCD_NETAMOUNT", SqlDbType.Decimal, 21)
            sqlpFCD_NETAMOUNT.Value = CON_SUB_LIST.FCD_NET_AMOUNT
            cmd.Parameters.Add(sqlpFCD_NETAMOUNT)

            Dim sqlpFCD_ISINCLUSIVE_TAX As New SqlParameter("@FCD_ISINCLUSIVE_TAX", SqlDbType.Bit)
            sqlpFCD_ISINCLUSIVE_TAX.Value = CON_SUB_LIST.FCD_ISINCLUSIVE_TAX
            cmd.Parameters.Add(sqlpFCD_ISINCLUSIVE_TAX)

            Dim sqlpFCD_TAX_CODE As New SqlParameter("@FCD_TAX_CODE", SqlDbType.VarChar, 20)
            sqlpFCD_TAX_CODE.Value = CON_SUB_LIST.FCD_TAX_CODE
            cmd.Parameters.Add(sqlpFCD_TAX_CODE)

            Dim sqlpFCD_ACTUALAMT As New SqlParameter("@FCD_ACTUALAMT", SqlDbType.Decimal, 21)
            sqlpFCD_ACTUALAMT.Value = CON_SUB_LIST.FCD_ACTUALAMT
            cmd.Parameters.Add(sqlpFCD_ACTUALAMT)

            Dim sqlpFCD_SCH_ID As New SqlParameter("@FCD_SCH_ID", SqlDbType.Int, 21)
            sqlpFCD_SCH_ID.Value = CON_SUB_LIST.FCD_SCH_ID
            cmd.Parameters.Add(sqlpFCD_SCH_ID)

            Dim sqlpFCD_REF_BSU_ID As New SqlParameter("@FCD_REF_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFCD_REF_BSU_ID.Value = REF_BSU_ID
            cmd.Parameters.Add(sqlpFCD_REF_BSU_ID)

            Dim sqlpFCD_ID As New SqlParameter("@FCD_ID", SqlDbType.Int)
            sqlpFCD_ID.Value = 0 'CON_SUB_LIST.FCD_ID
            cmd.Parameters.Add(sqlpFCD_ID)

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = False
            cmd.Parameters.Add(sqlpbDelete)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            'Get the New FCD ID (change the S.P so that it returns the New FCD_ID
            Dim sqlpNEW_FCD_ID As New SqlParameter("@NEW_FCD_ID", SqlDbType.Int)
            sqlpNEW_FCD_ID.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpNEW_FCD_ID)
            Dim vNew_FCD_ID As Integer

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue = 0 Then
                vNew_FCD_ID = sqlpNEW_FCD_ID.Value
                iReturnvalue = F_SaveFEE_CONCESSIONMONTHLY_D(CON_SUB_LIST.SubList_Monthly, vNew_FCD_ID, conn, trans)
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                End If
            Else
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function F_SaveFEE_CONCESSIONMONTHLY_D(ByVal arrSUBList As ArrayList, _
    ByVal FCD_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        '  EXEC	@return_value = [FEES].[F_SaveFEE_CONCESSIONMONTHLY_D]
        '@FMD_ID = 0,
        '@FMD_FCD_ID = 1,
        '@FMD_REF_ID = 2,
        '@FMD_DATE = N'12-MAR-2008',
        '@FMD_AMOUNT = 1000,
        '@bDelete = 0
        If arrSUBList Is Nothing Then
            Return 426
        End If
        If arrSUBList.Count = 0 Then
            Return 426
        End If
        Dim CON_SUB_LIST_MONTHLY As FEE_CONC_TRANC_SUB_MONTHLY
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        For i As Integer = 0 To arrSUBList.Count - 1
            CON_SUB_LIST_MONTHLY = arrSUBList(i)

            cmd = New SqlCommand("[FEES].[F_SaveFEE_CONCESSIONMONTHLY_D]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFMD_FCD_ID As New SqlParameter("@FMD_FCD_ID", SqlDbType.Int)
            sqlpFMD_FCD_ID.Value = FCD_ID
            cmd.Parameters.Add(sqlpFMD_FCD_ID)

            Dim sqlpFMD_REF_ID As New SqlParameter("@FMD_REF_ID", SqlDbType.Int)
            sqlpFMD_REF_ID.Value = CON_SUB_LIST_MONTHLY.FMD_REF_ID
            cmd.Parameters.Add(sqlpFMD_REF_ID)

            Dim sqlpFMD_AMOUNT As New SqlParameter("@FMD_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_AMOUNT.Value = CON_SUB_LIST_MONTHLY.FMD_AMOUNT
            cmd.Parameters.Add(sqlpFMD_AMOUNT)

            Dim sqlpFMD_TAXAMOUNT As New SqlParameter("@FMD_TAX_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_TAXAMOUNT.Value = CON_SUB_LIST_MONTHLY.FMD_TAXAMOUNT
            cmd.Parameters.Add(sqlpFMD_TAXAMOUNT)

            Dim sqlpFMD_NETAMOUNT As New SqlParameter("@FMD_NET_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_NETAMOUNT.Value = CON_SUB_LIST_MONTHLY.FMD_NETAMOUNT
            cmd.Parameters.Add(sqlpFMD_NETAMOUNT)

            Dim sqlpFMD_ORG_AMOUNT As New SqlParameter("@FMD_ORG_AMOUNT", SqlDbType.Decimal, 21)
            sqlpFMD_ORG_AMOUNT.Value = CON_SUB_LIST_MONTHLY.FMD_ORG_AMOUNT
            cmd.Parameters.Add(sqlpFMD_ORG_AMOUNT)

            Dim sqlpFMD_DATE As New SqlParameter("@FMD_DATE", SqlDbType.DateTime)
            sqlpFMD_DATE.Value = CON_SUB_LIST_MONTHLY.FMD_DATE
            cmd.Parameters.Add(sqlpFMD_DATE)

            Dim sqlpFMD_ID As New SqlParameter("@FMD_ID", SqlDbType.Int)
            sqlpFMD_ID.Value = 0 'CON_SUB_LIST_MONTHLY.FMD_ID
            cmd.Parameters.Add(sqlpFMD_ID)

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            If CON_SUB_LIST_MONTHLY.FMD_bDelete = True Then
                sqlpbDelete.Value = 1
            Else
                sqlpbDelete.Value = 0
            End If
            cmd.Parameters.Add(sqlpbDelete)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function UpdateConcessionActualAmount(ByVal FCH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[UpdateConcessionActualAmount]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFMD_FCD_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
        sqlpFMD_FCD_ID.Value = FCH_ID
        cmd.Parameters.Add(sqlpFMD_FCD_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        Return retSValParam.Value

    End Function

    Public Shared Function GetReference(ByVal refType As ConcessionType, ByVal ID As String, ByVal BSU_ID As String) As String
        Dim sql_query As String
        Dim objName As Object
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Select Case refType
                Case ConcessionType.Sibling
                    sql_query = "SELECT ISNULL(STU_FIRSTNAME, '')+' ' + ISNULL(STU_MIDNAME, '')+' ' + ISNULL(STU_LASTNAME, '') AS STU_NAME ,STU_BSU_ID as REF_BSUID " &
                    " FROM STUDENT_M WITH(NOLOCK) WHERE  (STU_ID = '" & ID & "' ) "
                    objName = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
                Case ConcessionType.Staff
                    sql_query = "SELECT ISNULL(EMP_FNAME, '')+' ' + ISNULL(EMP_MNAME, '')+' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME,EMP_BSU_ID as REF_BSUID " & _
                    " FROM EMPLOYEE_M WITH(NOLOCK) WHERE (EMP_ID='" & ID & "') "
                    objName = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
                Case Else
                    Return ""
            End Select
            If objName Is DBNull.Value Or objName Is Nothing Then
                Return ""
            End If
            Return objName.ToString
        End Using
    End Function

    Public Shared Function DELETEFEE_CONCESSION_H(ByVal p_FCH_ID As String, _
    ByVal p_FCH_BSU_ID As String, ByVal p_User As String) As String
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim cmd As New SqlCommand("FEES.DELETEFEE_CONCESSION_H", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure  
        '@FCH_ID INT ,
        '@FCH_BSU_ID VARCHAR(20) ,
        '@USER VARCHAR(20) 
        Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
        sqlpFCH_ID.Value = p_FCH_ID
        cmd.Parameters.Add(sqlpFCH_ID)

        Dim sqlpFCH_BSU_ID As New SqlParameter("@FCH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCH_BSU_ID.Value = p_FCH_BSU_ID
        cmd.Parameters.Add(sqlpFCH_BSU_ID)

        Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 200)
        sqlpUSER.Value = p_User
        cmd.Parameters.Add(sqlpUSER)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)
        cmd.ExecuteNonQuery()
        If retValParam.Value <> 0 Then
            stTrans.Rollback()
        Else
            stTrans.Commit()
        End If
        Return retValParam.Value
    End Function

    Public Shared Function DELETEFEE_CONCESSION_D(ByVal p_FCH_ID As String, _
    ByVal p_FCH_BSU_ID As String, ByVal p_User As String, _
    ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String
        Dim cmd As New SqlCommand("FEES.DELETEFEE_CONCESSION_D", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure
        ' @FCH_ID INT ,
        '@FCH_BSU_ID VARCHAR(20) ,
        '@USER VARCHAR(20) 
        Dim sqlpFCH_ID As New SqlParameter("@FCH_ID", SqlDbType.Int)
        sqlpFCH_ID.Value = p_FCH_ID
        cmd.Parameters.Add(sqlpFCH_ID)

        Dim sqlpFCH_BSU_ID As New SqlParameter("@FCH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCH_BSU_ID.Value = p_FCH_BSU_ID
        cmd.Parameters.Add(sqlpFCH_BSU_ID)

        Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 200)
        sqlpUSER.Value = p_User
        cmd.Parameters.Add(sqlpUSER)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)
        cmd.ExecuteNonQuery()  
        Return retValParam.Value
    End Function

    Public Shared Function PrintConcession(ByVal IntFCH_ID As Integer, _
    ByVal BSU_ID As String, ByVal USR_NAME As String) As MyReportClass
        Dim strSql As String
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As DataSet
        Dim cmd As New SqlCommand
        Dim strCurID As String = ""
        Dim strCurDesc As String = ""
        Dim strCurDeno As String = ""
        Dim repSource As New MyReportClass
        strSql = "SELECT * FROM FEES.VW_FEE_CONCESSION_DETAILS_Next WHERE FCH_ID=" & IntFCH_ID & ""
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim params As New Hashtable
            params("UserName") = USR_NAME
            params("ReportName") = "FEE CONCESSION VOUCHER"
            repSource.Parameter = params
            cmd.CommandText = strSql
            repSource.Command = cmd
            If BSU_ID <> "" Then
                Dim str_sql As String
                Dim dsCur As DataSet
                Dim strCon As String = ConnectionManger.GetOASISFINConnectionString
                str_sql = "SELECT BSU_ID, CUR_ID, CUR_DESCR, CUR_DENOMINATION " _
                & " FROM dbo.CURRENCY_M WITH ( NOLOCK ) INNER JOIN " _
                & " OASIS.dbo.BUSINESSUNIT_M WITH ( NOLOCK ) ON LTRIM(RTRIM(CUR_ID)) = LTRIM(RTRIM(ISNULL(BSU_CURRENCY,''))) WHERE BSU_ID='" & BSU_ID & "'"
                dsCur = SqlHelper.ExecuteDataset(strCon, CommandType.Text, str_sql)
                If dsCur.Tables(0).Rows.Count > 0 Then
                    strCurID = dsCur.Tables(0).Rows(0)("CUR_ID")
                    strCurDesc = dsCur.Tables(0).Rows(0)("CUR_DESCR")
                    strCurDeno = dsCur.Tables(0).Rows(0)("CUR_DENOMINATION")
                End If
            End If
            params("CurrID") = strCurID
            params("CurrDesc") = strCurDesc
            params("CurrDeno") = strCurDeno
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeConcessionReport.rpt"
            repSource.IncludeBSUImage = True
        End If
        Return repSource
    End Function

    Public Shared Function PrintConcessionCancel(ByVal IntFCH_ID As Integer, _
     ByVal BSU_ID As String, ByVal USR_NAME As String) As MyReportClass
        Dim strSql As String
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As DataSet
        Dim cmd As New SqlCommand
        Dim strCurID As String = ""
        Dim strCurDesc As String = ""
        Dim strCurDeno As String = ""
        Dim repSource As New MyReportClass
        strSql = "SELECT * FROM FEES.VW_FEE_CONCESSION_DETAILS WHERE FCH_ID=" & IntFCH_ID & ""
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim params As New Hashtable
            params("UserName") = USR_NAME
            params("ReportName") = "FEE CONCESSION VOUCHER (REVISED)"
            repSource.Parameter = params
            cmd.CommandText = strSql
            repSource.Command = cmd
            If BSU_ID <> "" Then
                Dim str_sql As String
                Dim dsCur As DataSet
                Dim strCon As String = ConnectionManger.GetOASISFINConnectionString
                str_sql = "SELECT BSU_ID, CUR_ID, CUR_DESCR, CUR_DENOMINATION " _
                & " FROM dbo.CURRENCY_M WITH ( NOLOCK ) INNER JOIN " _
                & " OASIS.dbo.BUSINESSUNIT_M WITH ( NOLOCK ) ON LTRIM(RTRIM(CUR_ID)) = LTRIM(RTRIM(ISNULL(BSU_CURRENCY,''))) WHERE BSU_ID='" & BSU_ID & "'"
                dsCur = SqlHelper.ExecuteDataset(strCon, CommandType.Text, str_sql)
                If dsCur.Tables(0).Rows.Count > 0 Then
                    strCurID = dsCur.Tables(0).Rows(0)("CUR_ID")
                    strCurDesc = dsCur.Tables(0).Rows(0)("CUR_DESCR")
                    strCurDeno = dsCur.Tables(0).Rows(0)("CUR_DENOMINATION")
                End If
            End If
            params("CurrID") = strCurID
            params("CurrDesc") = strCurDesc
            params("CurrDeno") = strCurDeno
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeConcessionCancelReport.rpt"
            repSource.IncludeBSUImage = True
        End If
        Return repSource
    End Function

    Public Shared Function GetProrateChargeFee(ByVal BSU_ID As String, ByVal ACD_ID As String, _
    ByVal STU_ID As String, ByVal GRD_ID As String, ByVal REASON As Integer, ByVal asondate As String, _
    ByVal FEE_ID As Integer, ByVal ChargeAmount As String) As Decimal
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[GetProrateChargeFee]", ConnectionManger.GetOASIS_FEESConnection)
        cmd.CommandType = CommandType.StoredProcedure
        '[FEES].[GetProrateChargeFee]()
        '@BSU_ID = N'131001', @ACD_ID = 26,
        '@GRD_ID = N'df', @STU_ID = 23, @REASON = 1,
        '@date = N'12/apr/2009', @FEE_ID = 1,
        '@ProRataAmount = @ProRataAmount OUTPUT,
        '@ChargeAmount = 1212 
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.BigInt)
        sqlpACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.VarChar)
        sqlpSTU_ID.Value = STU_ID
        cmd.Parameters.Add(sqlpSTU_ID)

        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar)
        sqlpGRD_ID.Value = GRD_ID
        cmd.Parameters.Add(sqlpGRD_ID)

        Dim sqlpREASON As New SqlParameter("@REASON", SqlDbType.Int)
        sqlpREASON.Value = REASON
        cmd.Parameters.Add(sqlpREASON)

        Dim sqlpdate As New SqlParameter("@date", SqlDbType.DateTime)
        sqlpdate.Value = asondate
        cmd.Parameters.Add(sqlpdate)

        Dim sqlpFEE_ID As New SqlParameter("@FEE_ID", SqlDbType.Int)
        sqlpFEE_ID.Value = FEE_ID
        cmd.Parameters.Add(sqlpFEE_ID)

        Dim sqlpChargeAmount As New SqlParameter("@ChargeAmount", SqlDbType.VarChar, 200)
        sqlpChargeAmount.Value = ChargeAmount
        cmd.Parameters.Add(sqlpChargeAmount)

        Dim sqlpProRataAmount As New SqlParameter("@ProRataAmount", SqlDbType.Decimal)
        sqlpProRataAmount.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpProRataAmount)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value

        If Not sqlpProRataAmount.Value Is Nothing AndAlso Not sqlpProRataAmount.Value Is System.DBNull.Value Then
            Return sqlpProRataAmount.Value
        Else
            Return 0
        End If
    End Function

    Public Shared Function GetProrateChargeFee_CANCEL(ByVal BSU_ID As String, ByVal ACD_ID As String, _
        ByVal STU_ID As String, ByVal REASON As Integer, ByVal asondate As String, _
        ByVal FEE_ID As Integer, ByVal ChargeAmount As String, ByVal MonthlyDate As String) As Decimal
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[GetProrateChargeFee_Cancel]", ConnectionManger.GetOASIS_FEESConnection)
        cmd.CommandType = CommandType.StoredProcedure
        '[FEES].[GetProrateChargeFee]()
        '@BSU_ID = N'131001', @ACD_ID = 26,
        '@GRD_ID = N'df', @STU_ID = 23, @REASON = 1,
        '@date = N'12/apr/2009', @FEE_ID = 1,
        '@ProRataAmount = @ProRataAmount OUTPUT,
        '@ChargeAmount = 1212 
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.BigInt)
        sqlpACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.VarChar)
        sqlpSTU_ID.Value = STU_ID
        cmd.Parameters.Add(sqlpSTU_ID)

        Dim sqlpMonthlyDate As New SqlParameter("@MonthlyDate", SqlDbType.VarChar)
        sqlpMonthlyDate.Value = MonthlyDate
        cmd.Parameters.Add(sqlpMonthlyDate)

        Dim sqlpREASON As New SqlParameter("@REASON", SqlDbType.Int)
        sqlpREASON.Value = REASON
        cmd.Parameters.Add(sqlpREASON)

        Dim sqlpdate As New SqlParameter("@date", SqlDbType.DateTime)
        sqlpdate.Value = asondate
        cmd.Parameters.Add(sqlpdate)

        Dim sqlpFEE_ID As New SqlParameter("@FEE_ID", SqlDbType.Int)
        sqlpFEE_ID.Value = FEE_ID
        cmd.Parameters.Add(sqlpFEE_ID)

        Dim sqlpChargeAmount As New SqlParameter("@ChargeAmount", SqlDbType.VarChar, 200)
        sqlpChargeAmount.Value = ChargeAmount
        cmd.Parameters.Add(sqlpChargeAmount)

        Dim sqlpProRataAmount As New SqlParameter("@ProRataAmount", SqlDbType.Decimal)
        sqlpProRataAmount.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpProRataAmount)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value

        If Not sqlpProRataAmount.Value Is Nothing AndAlso Not sqlpProRataAmount.Value Is System.DBNull.Value Then
            Return sqlpProRataAmount.Value
        Else
            Return 0
        End If
    End Function

    Public Shared Function GetRefConcession(ByVal FCH_ID As Long) As DataSet
        GetRefConcession = Nothing
        Dim qry As String = "SELECT FCH_ID,FCH_RECNO FROM FEES.FEE_CONCESSION_H WITH(NOLOCK) WHERE FCH_ID=" & FCH_ID
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        Return ds
    End Function
    Public Shared Function GET_PREVIOUS_ACADEMIC_YEAR(ByVal ACD_ID As Long) As Long
        GET_PREVIOUS_ACADEMIC_YEAR = 0
        Try
            Dim QRY As String = "SELECT OLD_ACD.ACD_ID FROM dbo.ACADEMICYEAR_D OLD_ACD WITH(NOLOCK) INNER JOIN dbo.ACADEMICYEAR_D NEW_ACD WITH(NOLOCK) ON NEW_ACD.ACD_BSU_ID = OLD_ACD.ACD_BSU_ID " & _
                "AND NEW_ACD.ACD_CLM_ID = OLD_ACD.ACD_CLM_ID AND NEW_ACD.ACD_ACY_ID = OLD_ACD.ACD_ACY_ID + 1 WHERE NEW_ACD.ACD_ID = " & ACD_ID & " "
            GET_PREVIOUS_ACADEMIC_YEAR = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, QRY)
        Catch ex As Exception

        End Try
    End Function

End Class

Public Class FEE_CONC_TRANC_SUB

    Dim vFCD_ID As Integer
    Dim vFCD_FCH_ID As Integer
    Dim vFCD_FCM_ID As Integer
    Dim vFCD_REF_ID As Integer
    Dim vFCD_FEE_ID As Integer
    Dim vFCD_REF_NAME As String
    Dim vFEE_DESCR As String
    Dim vFCM_DESCR As String
    Dim vFCD_AMTTYPE As Int32
    Dim vFCD_AMOUNT As Decimal
    Dim vFCD_TAXAMOUNT As Decimal
    Dim vFCD_NETAMOUNT As Decimal
    Dim vFCD_ACTUALAMT As Decimal
    Dim vFCD_PERCENTAGE As String
    Dim vFCD_SCH_ID As Integer
    Dim vFCD_REF_BSU_ID As String
    Dim vFCD_ISINCLUSIVE_TAX As Boolean
    Dim vFCD_TAX_CODE As String
    Dim arrSubList_Monthly As ArrayList

    Public Property SubList_Monthly() As ArrayList
        Get
            Return arrSubList_Monthly
        End Get
        Set(ByVal value As ArrayList)
            arrSubList_Monthly = value
        End Set
    End Property

    Public Property FCD_REF_BSU_ID() As String
        Get
            Return vFCD_REF_BSU_ID
        End Get
        Set(ByVal value As String)
            vFCD_REF_BSU_ID = value
        End Set
    End Property

    Public Property PERCENTAGE() As String
        Get
            Return vFCD_PERCENTAGE
        End Get
        Set(ByVal value As String)
            vFCD_PERCENTAGE = value
        End Set
    End Property

    Public Property FCD_SCH_ID() As Integer
        Get
            Return vFCD_SCH_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_SCH_ID = value
        End Set
    End Property

    Public Property FCD_ACTUALAMT() As Decimal
        Get
            Return vFCD_ACTUALAMT
        End Get
        Set(ByVal value As Decimal)
            vFCD_ACTUALAMT = value
        End Set
    End Property

    Public Property FCD_AMOUNT() As Decimal
        Get
            Return vFCD_AMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFCD_AMOUNT = value
        End Set
    End Property

    Public Property FCD_TAX_AMOUNT() As Decimal
        Get
            Return vFCD_TAXAMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFCD_TAXAMOUNT = value
        End Set
    End Property

    Public Property FCD_NET_AMOUNT() As Decimal
        Get
            Return vFCD_NETAMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFCD_NETAMOUNT = value
        End Set
    End Property

    Public Property FCD_AMTTYPE() As Int32
        Get
            Return vFCD_AMTTYPE
        End Get
        Set(ByVal value As Int32)
            vFCD_AMTTYPE = value
        End Set
    End Property

    Public Property FCM_DESCR() As String
        Get
            Return vFCM_DESCR
        End Get
        Set(ByVal value As String)
            vFCM_DESCR = value
        End Set
    End Property

    Public Property FEE_DESCR() As String
        Get
            Return vFEE_DESCR
        End Get
        Set(ByVal value As String)
            vFEE_DESCR = value
        End Set
    End Property

    Public Property FCD_FEE_ID() As Integer
        Get
            Return vFCD_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_FEE_ID = value
        End Set
    End Property

    Public Property FCD_REF_ID() As Integer
        Get
            Return vFCD_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_REF_ID = value
        End Set
    End Property

    Public Property FCD_REF_NAME() As String
        Get
            Return vFCD_REF_NAME
        End Get
        Set(ByVal value As String)
            vFCD_REF_NAME = value
        End Set
    End Property

    Public Property FCD_FCM_ID() As Integer
        Get
            Return vFCD_FCM_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_FCM_ID = value
        End Set
    End Property

    Public Property FCD_FCH_ID() As Integer
        Get
            Return vFCD_FCH_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_FCH_ID = value
        End Set
    End Property

    Public Property FCD_ID() As Integer
        Get
            Return vFCD_ID
        End Get
        Set(ByVal value As Integer)
            vFCD_ID = value
        End Set
    End Property

    Public Property FCD_ISINCLUSIVE_TAX() As Boolean
        Get
            Return vFCD_ISINCLUSIVE_TAX
        End Get
        Set(ByVal value As Boolean)
            vFCD_ISINCLUSIVE_TAX = value
        End Set
    End Property

    Public Property FCD_TAX_CODE() As String
        Get
            Return vFCD_TAX_CODE
        End Get
        Set(ByVal value As String)
            vFCD_TAX_CODE = value
        End Set
    End Property
End Class

Public Class FEE_CONC_TRANC_SUB_MONTHLY
    Dim vFMD_ID As Integer
    Dim vFMD_FCD_ID As Integer
    Dim vFMD_REF_ID As Integer
    Dim vFMD_REF_NAME As String
    Dim vFMD_DATE As String
    Dim vFMD_AMOUNT As Decimal
    Dim vFMD_TAXAMOUNT As Decimal
    Dim vFMD_NETAMOUNT As Decimal
    Dim vFMD_ORG_AMOUNT As Decimal
    Dim vFMD_PRO_AMOUNT As Decimal
    Dim vFMD_bDelete As Boolean
    Public Property FMD_ID() As Integer
        Get
            Return vFMD_ID
        End Get
        Set(ByVal value As Integer)
            vFMD_ID = value
        End Set
    End Property

    Public Property FMD_FCD_ID() As Integer
        Get
            Return vFMD_FCD_ID
        End Get
        Set(ByVal value As Integer)
            vFMD_FCD_ID = value
        End Set
    End Property

    Public Property FMD_REF_ID() As Integer
        Get
            Return vFMD_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFMD_REF_ID = value
        End Set
    End Property

    Public Property FMD_REF_NAME() As String
        Get
            Return vFMD_REF_NAME
        End Get
        Set(ByVal value As String)
            vFMD_REF_NAME = value
        End Set
    End Property

    Public Property FMD_AMOUNT() As Decimal
        Get
            Return vFMD_AMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFMD_AMOUNT = value
        End Set
    End Property

    Public Property FMD_TAXAMOUNT() As Decimal
        Get
            Return vFMD_TAXAMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFMD_TAXAMOUNT = value
        End Set
    End Property

    Public Property FMD_NETAMOUNT() As Decimal
        Get
            Return vFMD_NETAMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFMD_NETAMOUNT = value
        End Set
    End Property

    Public Property FMD_DATE() As String
        Get
            Return vFMD_DATE
        End Get
        Set(ByVal value As String)
            vFMD_DATE = value
        End Set
    End Property

    Public Property FMD_ORG_AMOUNT() As Decimal
        Get
            Return vFMD_ORG_AMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFMD_ORG_AMOUNT = value
        End Set
    End Property

    Public Property FMD_PRO_AMOUNT() As Decimal
        Get
            Return vFMD_PRO_AMOUNT
        End Get
        Set(ByVal value As Decimal)
            vFMD_PRO_AMOUNT = value
        End Set
    End Property

    Public Property FMD_bDelete() As Boolean
        Get
            Return vFMD_bDelete
        End Get
        Set(ByVal value As Boolean)
            vFMD_bDelete = value        
        End Set
    End Property


    Public Shared Function CreateDataTableFeeConcessionMonthly() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        ' FDD_DATE, FDD_AMOUNT, FDD_FIRSTMEMODT, FDD_SECONDMEMODT,FDD_THIRDMEMODT
        Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
        Dim cREF_Id As New DataColumn("REF_ID", System.Type.GetType("System.String"))
        Dim cFDD_DATE As New DataColumn("FDD_DATE", System.Type.GetType("System.DateTime"))
        Dim cdescr As New DataColumn("DESCR", System.Type.GetType("System.String"))
        Dim cFDD_AMOUNT As New DataColumn("FDD_AMOUNT", System.Type.GetType("System.String"))
        Dim cCUR_AMOUNT As New DataColumn("CUR_AMOUNT", System.Type.GetType("System.String"))
        Dim cTAX_AMOUNT As New DataColumn("TAX_AMOUNT", System.Type.GetType("System.String"))
        Dim cNET_AMOUNT As New DataColumn("NET_AMOUNT", System.Type.GetType("System.String"))
        Dim cPRO_AMOUNT As New DataColumn("PRO_AMOUNT", System.Type.GetType("System.String"))
        Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
        Dim cDelete As New DataColumn("DFlag", System.Type.GetType("System.Boolean"))
        dtDt.Columns.Add(cId)
        dtDt.Columns.Add(cREF_Id)
        dtDt.Columns.Add(cFDD_DATE)
        dtDt.Columns.Add(cdescr)
        dtDt.Columns.Add(cFDD_AMOUNT)
        dtDt.Columns.Add(cCUR_AMOUNT)
        dtDt.Columns.Add(cTAX_AMOUNT)
        dtDt.Columns.Add(cNET_AMOUNT)
        dtDt.Columns.Add(cPRO_AMOUNT)
        dtDt.Columns.Add(cStatus)
        dtDt.Columns.Add(cDelete)
        Return dtDt
    End Function

End Class

