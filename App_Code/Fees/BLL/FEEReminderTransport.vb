Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

''' <summary>
''' The class Created for handling Performa Invoice
''' 
''' Author : SHIJIN C A
''' Date : 03-MAY-2008 
''' </summary>
''' <remarks></remarks>
Public Class FEEReminderTransport

    ''' <summary>
    ''' Internal Variables used in the class to Store poperties
    ''' </summary>
    ''' <remarks></remarks>
    Dim vFRH_ID As Integer
    Dim vFRH_BSU_ID As String
    Dim vFRH_STU_BSU_ID As String
    Dim vFRH_ACY_DESCR As String
    Dim vFRH_ACD_ID As Integer
    Dim vFRH_REMARKS As String
    Dim vFRH_DT As DateTime
    Dim vFRH_ASONDATE As DateTime
    Dim vFRH_Level As Int16
    Public bDelete As Boolean
    Public bEdit As Boolean
    Dim vSTD_SUB_DET As ArrayList

    ''' <summary>
    ''' Gets or Sets the Unique ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRH_ID() As Integer
        Get
            Return vFRH_ID
        End Get
        Set(ByVal value As Integer)
            vFRH_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Business Unit ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRH_BSU_ID() As String
        Get
            Return vFRH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFRH_BSU_ID = value
        End Set
    End Property

    Public Property FRH_STU_BSU_ID() As String
        Get
            Return vFRH_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            vFRH_STU_BSU_ID = value
        End Set
    End Property

    Public Property FRH_ACY_DESCR() As String
        Get
            Return vFRH_ACY_DESCR
        End Get
        Set(ByVal value As String)
            vFRH_ACY_DESCR = value
        End Set
    End Property

    Public Property FRH_REMARKS() As String
        Get
            Return vFRH_REMARKS
        End Get
        Set(ByVal value As String)
            vFRH_REMARKS = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Academic Year ID 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRH_ACD_ID() As Integer
        Get
            Return vFRH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFRH_ACD_ID = value
        End Set
    End Property

    Public Property FRH_Level() As ReminderType
        Get
            Return vFRH_Level
        End Get
        Set(ByVal value As ReminderType)
            vFRH_Level = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or Sets Date on which the Performa Invoice is saved
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRH_DT() As DateTime
        Get
            Return vFRH_DT
        End Get
        Set(ByVal value As DateTime)
            vFRH_DT = value
        End Set
    End Property

    Public Property FRH_ASONDATE() As DateTime
        Get
            Return vFRH_ASONDATE
        End Get
        Set(ByVal value As DateTime)
            vFRH_ASONDATE = value
        End Set
    End Property

    Public Function Delete() As Boolean
        bDelete = True
    End Function

    Private Shared Function GetSection(ByVal vSTU_ID As String) As Integer
        Dim str_Sql As String = "SELECT STU_SCT_ID FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID =" & vSTU_ID
        Dim objSCT_ID As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)
        If objSCT_ID Is DBNull.Value Then
            Return ""
        Else
            Return Convert.ToInt32(objSCT_ID)
        End If
    End Function

    Public Shared Function SaveFEEReminderDetails(ByVal vXml_IDs As String, ByVal vFEE_REM As FEEReminderTransport, ByRef NEW_FRH_ID As Integer, ByVal bGrade As Boolean, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim bUpdateData As Boolean = False

        If vFEE_REM Is Nothing Then
            Return 1000
        End If
        Dim cmd As SqlCommand

        If vFEE_REM.bEdit = False AndAlso vFEE_REM.bDelete Then Return 0
        If vFEE_REM.bEdit AndAlso vFEE_REM.bDelete = False Then Return 0

        cmd = New SqlCommand("[FEES].[F_SAVEFEE_REMINDER_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRH_BSU_ID As New SqlParameter("@FRH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFRH_BSU_ID.Value = vFEE_REM.FRH_BSU_ID
        cmd.Parameters.Add(sqlpFRH_BSU_ID)

        Dim sqlpFRH_STU_BSU_ID As New SqlParameter("@FRH_STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFRH_STU_BSU_ID.Value = vFEE_REM.FRH_STU_BSU_ID
        cmd.Parameters.Add(sqlpFRH_STU_BSU_ID)

        Dim sqlpFRH_ACD_ID As New SqlParameter("@FRH_ACD_ID", SqlDbType.Int)
        sqlpFRH_ACD_ID.Value = vFEE_REM.FRH_ACD_ID
        cmd.Parameters.Add(sqlpFRH_ACD_ID)

        Dim sqlpFRH_DT As New SqlParameter("@FRH_DT", SqlDbType.DateTime)
        sqlpFRH_DT.Value = vFEE_REM.FRH_DT
        cmd.Parameters.Add(sqlpFRH_DT)

        Dim sqlpFRH_ASONDATE As New SqlParameter("@FRH_ASONDATE", SqlDbType.DateTime)
        sqlpFRH_ASONDATE.Value = vFEE_REM.FRH_ASONDATE
        cmd.Parameters.Add(sqlpFRH_ASONDATE)

        Dim sqlpFRH_Level As New SqlParameter("@FRH_Level", SqlDbType.TinyInt)
        sqlpFRH_Level.Value = vFEE_REM.vFRH_Level
        cmd.Parameters.Add(sqlpFRH_Level)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_REM.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim sqlpNEW_FRH_ID As New SqlParameter("@NEW_FRH_ID", SqlDbType.Int)
        sqlpNEW_FRH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEW_FRH_ID)
        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue = 0 Then
            NEW_FRH_ID = sqlpNEW_FRH_ID.Value
        End If
        If iReturnvalue = 0 Then iReturnvalue = SaveFeeReminderSubDetails(vXml_IDs, vFEE_REM.FRH_ASONDATE, vFEE_REM.FRH_STU_BSU_ID, vFEE_REM.FRH_ACD_ID, sqlpNEW_FRH_ID.Value, vFEE_REM.FRH_Level, bGrade, conn, trans)

        Return iReturnvalue
    End Function

    Public Shared Function GetStudentDetailsXML(ByVal vSTU_IDs As Hashtable) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLSTU_ID As XmlElement
        Dim XMLSTU_DET As XmlElement
        Dim XMLbExclude As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vSTU_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLSTU_DET = xmlDoc.CreateElement("STU_DET")

            XMLSTU_ID = xmlDoc.CreateElement("STU_ID")
            XMLSTU_ID.InnerText = iDictEnum.Key
            XMLSTU_DET.AppendChild(XMLSTU_ID)

            XMLbExclude = xmlDoc.CreateElement("bExclude")
            XMLbExclude.InnerText = iDictEnum.Value
            XMLSTU_DET.AppendChild(XMLbExclude)

            xmlDoc.DocumentElement.InsertBefore(XMLSTU_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function


    Public Shared Function GetStudentDetailsXML(ByVal vSTU_IDs As Hashtable, ByVal vACD_ID As Integer, ByVal vASOnDate As Date, ByVal vBSU_ID As String, ByVal vLevel As ReminderType, ByVal trans As SqlTransaction) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLSTU_DET As XmlElement
        Dim XMLFEE_DET As XmlNode
        Dim XMLSTU_ID As XmlElement
        Dim XMLSCT_ID As XmlElement
        Dim XMLACD_ID As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vSTU_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLSTU_DET = xmlDoc.CreateElement("STU_DET")
            XMLSTU_ID = xmlDoc.CreateElement("STU_ID")
            XMLSTU_ID.InnerText = iDictEnum.Key
            XMLSTU_DET.AppendChild(XMLSTU_ID)

            Dim vSCT_ID As Integer = GetSection(iDictEnum.Key)

            XMLSCT_ID = xmlDoc.CreateElement("SCT_ID")
            XMLSCT_ID.InnerText = vSCT_ID
            XMLSTU_DET.AppendChild(XMLSCT_ID)

            XMLACD_ID = xmlDoc.CreateElement("ACD_ID")
            XMLACD_ID.InnerText = vACD_ID
            XMLSTU_DET.AppendChild(XMLACD_ID)

            'XMLFEE_DET = xmlDoc.CreateElement("FEE_DETAILS")

            XMLFEE_DET = GetDetails(xmlDoc, iDictEnum.Key, vASOnDate, vBSU_ID, vLevel, iDictEnum.Value, trans)
            XMLSTU_DET.AppendChild(XMLFEE_DET)

            xmlDoc.DocumentElement.InsertBefore(XMLSTU_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function

    Private Shared Sub GetACD_ID_SCT_ID(ByVal vSTU_ID As String, ByRef vACD_ID As Integer, ByRef vSCT_ID As Integer, ByVal trans As SqlTransaction)
        Dim str_Sql As String = "SELECT STU_SCT_ID, STU_ACD_ID FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID =" & vSTU_ID
        Dim drread As SqlDataReader = SqlHelper.ExecuteReader(trans, CommandType.Text, str_Sql)
        While (drread.Read)
            vACD_ID = drread("STU_ACD_ID")
            vSCT_ID = drread("STU_SCT_ID")
        End While
    End Sub


    Public Shared Function SaveFeeReminderSubDetails(ByVal vXML_IDs As String, ByVal vASOnDate As DateTime, ByVal vBSU_ID As String, ByVal vACD_ID As Integer, ByVal vFRH_ID As Integer, ByVal vLevel As ReminderType, ByVal bGrade As Boolean, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

        Dim cmd As SqlCommand
        Dim iReturnvalue As Integer
        ' Dim iDictEnum As IDictionaryEnumerator = vGrade_IDs.GetEnumerator
        'Dim str_Gradexml As String = GetGradeDetailsXML(vGrade_IDs)

        'Dim iDictEnum As IDictionaryEnumerator = vBus_IDs.GetEnumerator
        'Dim str_Busxml As String = GetBusDetailsXML(vBus_IDs)

        cmd = New SqlCommand("FEES.[F_SAVEFEE_REMINDER_D]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRD_FRH_ID As New SqlParameter("@FRD_FRH_ID", SqlDbType.Int)
        sqlpFRD_FRH_ID.Value = vFRH_ID
        cmd.Parameters.Add(sqlpFRD_FRH_ID)

        Dim sqlpAsOnDt As New SqlParameter("@AsOnDt", SqlDbType.DateTime)
        sqlpAsOnDt.Value = vASOnDate
        cmd.Parameters.Add(sqlpAsOnDt)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpLevel As New SqlParameter("@Level", SqlDbType.TinyInt)
        sqlpLevel.Value = vLevel
        cmd.Parameters.Add(sqlpLevel)

        Dim sqlpXML_IDs As New SqlParameter("@XML_IDs", SqlDbType.Xml)
        If bGrade Then
            sqlpXML_IDs.Value = UtilityObj.GenerateXML(vXML_IDs, XMLType.Grade)
        Else
            sqlpXML_IDs.Value = UtilityObj.GenerateXML(vXML_IDs, XMLType.Bus)
        End If
        cmd.Parameters.Add(sqlpXML_IDs)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = False
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpbGrade As New SqlParameter("@bGrade", SqlDbType.Bit)
        sqlpbGrade.Value = bGrade
        cmd.Parameters.Add(sqlpbGrade)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If

    End Function

    Private Shared Function GetDetails(ByVal xmlDoc As XmlDocument, ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal vRemindertyp As ReminderType, ByVal bExclude As Boolean, ByVal trans As SqlTransaction) As XmlNode
        'Dim rem_sub As FEEReminder_SUB()
        Try
            Dim dt As DataTable = FEEReminderTransport.GetDueAmountDataTable(stud_id, asOnDate, vBSU_ID, vRemindertyp, trans)
            Dim i As Integer = 0
            'Dim xmlDoc As New XmlDocument
            Dim XMLFEE_DET As XmlElement
            Dim XMLFEE_ID As XmlElement
            Dim XMLFSH_ID As XmlElement
            Dim XMLAMOUNT As XmlElement
            Dim XMLbExclude As XmlElement
            Dim XMLRoot As XmlNode
            XMLRoot = xmlDoc.CreateElement("FEE_DETAILS")
            'xmlDoc.AppendChild(XMLRoot)
            For Each drread As DataRow In dt.Rows
                XMLFEE_DET = xmlDoc.CreateElement("FEE_DET")
                XMLFEE_ID = xmlDoc.CreateElement("FEE_ID")
                XMLFEE_ID.InnerText = drread("FSH_FEE_ID")
                XMLFEE_DET.AppendChild(XMLFEE_ID)

                XMLFSH_ID = xmlDoc.CreateElement("FSH_ID")
                XMLFSH_ID.InnerText = drread("FSH_ID")
                XMLFEE_DET.AppendChild(XMLFSH_ID)

                XMLAMOUNT = xmlDoc.CreateElement("AMOUNT")
                XMLAMOUNT.InnerText = drread("DUE_AMT")
                XMLFEE_DET.AppendChild(XMLAMOUNT)

                XMLbExclude = xmlDoc.CreateElement("bExclude")
                XMLbExclude.InnerText = bExclude
                XMLFEE_DET.AppendChild(XMLbExclude)

                XMLRoot.AppendChild(XMLFEE_DET)
                'xmlDoc.DocumentElement.InsertBefore(XMLFEE_DET, xmlDoc.DocumentElement.LastChild)
            Next
            Return XMLRoot
            'drread.Close()
            'Return rem_sub
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetAdvancedAmount(ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal conn As SqlConnection) As SqlDataReader
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("[FEES].[F_Transport_FEEReminder_GETADVAMOUNT]")
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = vBSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
            sqlpAsOnDT.Value = asOnDate
            cmd.Parameters.Add(sqlpAsOnDT)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = stud_id
            cmd.Parameters.Add(sqlpSTU_ID)
            cmd.Connection = conn
            Return cmd.ExecuteReader
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDueAmount(ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal vLevel As Int16) As SqlDataReader
        Try
            'Dim cmd As SqlCommand

            ' cmd = New SqlCommand("[FEES].[F_Transport_FEEReminder_GETDUEAMOUNT]")
            'cmd.CommandType = CommandType.StoredProcedure
            Dim sqlparams(4) As SqlParameter

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = vBSU_ID
            'cmd.Parameters.Add(sqlpBSU_ID)
            sqlparams(0) = sqlpBSU_ID

            Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
            sqlpAsOnDT.Value = asOnDate
            sqlparams(1) = sqlpAsOnDT
            'cmd.Parameters.Add(sqlpAsOnDT)

            Dim sqlpLevel As New SqlParameter("@Level", SqlDbType.TinyInt)
            sqlpLevel.Value = vLevel
            sqlparams(2) = sqlpLevel
            'cmd.Parameters.Add(sqlpLevel)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = stud_id
            sqlparams(3) = sqlpSTU_ID
            'cmd.Parameters.Add(sqlpSTU_ID)
            'cmd.Connection = conn
            Return SqlHelper.ExecuteReader(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "[FEES].[F_Transport_FEEReminder_GETDUEAMOUNT]", sqlparams)
            'Return cmd.ExecuteReader
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetHeaderDetails(ByVal vFRH_ID As Integer) As FEEReminderTransport
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT FEES.FEE_REMINDER_H.FRH_DT, FEES.FEE_REMINDER_H.FRH_ASONDATE, " & _
        " FRH_STU_BSU_ID, FRH_ACD_ID, " & _
        " FEES.FEE_REMINDER_H.FRH_Level, FEES.FEE_REMINDER_H.FRH_REMARKS FROM " & _
        " FEES.FEE_REMINDER_H WHERE FEES.FEE_REMINDER_H.FRH_ID = " & vFRH_ID & _
        " AND FEES.FEE_REMINDER_H.FRH_bDeleted = 0 "
        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        Dim fee_Rem As New FEEReminderTransport
        While (drReader.Read())
            fee_Rem.FRH_ASONDATE = drReader("FRH_ASONDATE")
            fee_Rem.FRH_DT = drReader("FRH_DT")
            fee_Rem.FRH_Level = drReader("FRH_Level")
            fee_Rem.FRH_ACD_ID = drReader("FRH_ACD_ID")
            fee_Rem.FRH_BSU_ID = drReader("FRH_STU_BSU_ID")
        End While
        Return fee_Rem
    End Function
    Public Shared Function PrintReminder(ByVal vFRH_ID As Integer, ByVal Usr_name As String, ByVal BSU_ID As String, Optional ByVal vSTU_IDs As String = "") As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim cmd As New SqlCommand("[FEES].[F_GETReminderLetter]", New SqlConnection(str_conn))
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRH_ID As New SqlParameter("@FRH_ID", SqlDbType.Int)
        sqlpFRH_ID.Value = vFRH_ID
        cmd.Parameters.Add(sqlpFRH_ID)

        Dim sqlpSTU_ID As New SqlParameter("@FRD_STU_ID", SqlDbType.VarChar)
        sqlpSTU_ID.Value = vSTU_IDs
        cmd.Parameters.Add(sqlpSTU_ID)

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass

        Dim cmdSubColln As New SqlCommand

        ''SUBREPORT1
        cmdSubColln.CommandText = "EXEC [FEES].F_GETReminderLetterDetail " & vFRH_ID & ",'" & vSTU_IDs & "'"
        cmdSubColln.Connection = New SqlConnection(str_conn)
        cmdSubColln.CommandType = CommandType.Text
        repSourceSubRep(0).Command = cmdSubColln

        ''SUBREPORT2
        Dim cmdSubFooter As New SqlCommand
        cmdSubFooter.CommandText = "EXEC TRANSPORT.GetTransportFooter '" & BSU_ID & "'"
        cmdSubFooter.Connection = New SqlConnection(str_conn)
        cmdSubFooter.CommandType = CommandType.Text
        repSourceSubRep(1).Command = cmdSubFooter



        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("UserName") = Usr_name
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../fees/Reports/RPT/rptTransportFeeReminder.rpt"


        Return repSource
    End Function
    Public Shared Function GetDueAmountDataTable(ByVal stud_id As Integer, ByVal asOnDate As DateTime, ByVal vBSU_ID As String, ByVal vLevel As Int16, ByVal trans As SqlTransaction) As DataTable
        Try
            'Dim cmd As SqlCommand

            'cmd = New SqlCommand("[FEES].F_FEEReminder_GETDUEAMOUNT")
            'cmd.CommandType = CommandType.StoredProcedure
            Dim sqlparams(4) As SqlParameter

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = vBSU_ID
            'cmd.Parameters.Add(sqlpBSU_ID)
            sqlparams(0) = sqlpBSU_ID

            Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
            sqlpAsOnDT.Value = asOnDate
            sqlparams(1) = sqlpAsOnDT
            'cmd.Parameters.Add(sqlpAsOnDT)

            Dim sqlpLevel As New SqlParameter("@Level", SqlDbType.TinyInt)
            sqlpLevel.Value = vLevel
            sqlparams(2) = sqlpLevel
            'cmd.Parameters.Add(sqlpLevel)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = stud_id
            sqlparams(3) = sqlpSTU_ID
            'cmd.Parameters.Add(sqlpSTU_ID)
            'cmd.Connection = conn
            Dim ds As DataSet = SqlHelper.ExecuteDataset(trans, CommandType.StoredProcedure, "[FEES].F_FEEReminder_GETDUEAMOUNT", sqlparams)

            If ds Is Nothing Or ds.Tables.Count < 0 Then
                Return Nothing
            Else
                Return ds.Tables(0)
            End If

            'Return cmd.ExecuteReader
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Shared Function GetGradeDetailsXML(ByVal vGrade_IDs As Hashtable) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLGRD_ID As XmlElement
        Dim XMLGRD_DET As XmlElement
        'Dim XMLbExclude As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vGrade_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLGRD_DET = xmlDoc.CreateElement("GRM_DESCR")

            XMLGRD_ID = xmlDoc.CreateElement("GRM_GRD_ID")
            XMLGRD_ID.InnerText = iDictEnum.Key
            XMLGRD_DET.AppendChild(XMLGRD_ID)

            'XMLbExclude = xmlDoc.CreateElement("bExclude")
            'XMLbExclude.InnerText = iDictEnum.Value
            'XMLSTU_DET.AppendChild(XMLbExclude)

            xmlDoc.DocumentElement.InsertBefore(XMLGRD_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function
    Public Shared Function GetBusDetailsXML(ByVal vBus_IDs As Hashtable) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLBUS_ID As XmlElement
        Dim XMLBUS_DET As XmlElement
        'Dim XMLbExclude As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vBus_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLBUS_DET = xmlDoc.CreateElement("BNO_DESCR")

            XMLBUS_ID = xmlDoc.CreateElement("BNO_ID")
            XMLBUS_ID.InnerText = iDictEnum.Key
            XMLBUS_DET.AppendChild(XMLBUS_ID)

            'XMLbExclude = xmlDoc.CreateElement("bExclude")
            'XMLbExclude.InnerText = iDictEnum.Value
            'XMLSTU_DET.AppendChild(XMLbExclude)

            xmlDoc.DocumentElement.InsertBefore(XMLBUS_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function

End Class

