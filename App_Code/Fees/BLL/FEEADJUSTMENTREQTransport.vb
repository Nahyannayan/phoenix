Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class FEEADJUSTMENTREQTransport
    Dim vFAR_ID As Integer
    Dim vFAR_STU_ID As Integer
    Dim vFAR_STU_NAME As String
    Dim vSTU_TYP As String
    Dim vFAR_ACD_ID As Integer
    Dim vSUB_LOC As String
    Dim vFAR_DATE As DateTime
    Dim vFAR_REMARKS As String
    Dim vbMANUAL_ADJ As Boolean
    Dim vFAR_BSU_ID As String
    Dim vFAR_STU_BSU_ID As String
    Public FAR_STU_BSU_NAME As String
    Dim vFAR_EVENT As String
    Dim vFAR_GRD_ID As String
    Public vJOIN_GRD_ID As String
    Public vFAR_ACD_DISPLAY As String
    Public vJOIN_ACD_DISPLAY As String
    Public vJOIN_DATE As Date
    Public vSER_START_DATE As Date
    Public vSER_DISCONT_DATE As Date
    Public bDelete As Boolean
    Public bEdit As Boolean
    Dim vFEE_ADJ_DET As Hashtable

    Dim vAPPR_STATUS As String
    Dim vAPPR_REMARKS As String
    Dim vAPPR_DATE As Date
    Public vTOTALAMT As Double

    Public Sub New()
        vFEE_ADJ_DET = New Hashtable
    End Sub

    Public Property FEE_ADJ_DET() As Hashtable
        Get
            Return vFEE_ADJ_DET
        End Get
        Set(ByVal value As Hashtable)
            vFEE_ADJ_DET = value
        End Set
    End Property

    Public Property SUB_LOCATION() As String
        Get
            Return vSUB_LOC
        End Get
        Set(ByVal value As String)
            vSUB_LOC = value
        End Set
    End Property

    Public Property IsEnquiry() As Boolean
        Get
            If vSTU_TYP = "E" Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            If value Then
                vSTU_TYP = "E"
            Else
                vSTU_TYP = "S"
            End If
        End Set
    End Property

    Public Property APPR_STATUS() As String
        Get
            Return vAPPR_STATUS
        End Get
        Set(ByVal value As String)
            vAPPR_STATUS = value
        End Set
    End Property

    Public Property FAR_STU_NAME() As String
        Get
            Return vFAR_STU_NAME
        End Get
        Set(ByVal value As String)
            vFAR_STU_NAME = value
        End Set
    End Property

    Public Property FAR_EVENT() As String
        Get
            Return vFAR_EVENT
        End Get
        Set(ByVal value As String)
            vFAR_EVENT = value
        End Set
    End Property

    Public Property FAR_BSU_ID() As String
        Get
            Return vFAR_BSU_ID
        End Get
        Set(ByVal value As String)
            vFAR_BSU_ID = value
        End Set
    End Property

    Public Property FAR_STU_BSU_ID() As String
        Get
            Return vFAR_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            vFAR_STU_BSU_ID = value
        End Set
    End Property

    Public Property FAH_REMARKS() As String
        Get
            Return vFAR_REMARKS
        End Get
        Set(ByVal value As String)
            vFAR_REMARKS = value
        End Set
    End Property


    Public Property FAH_bMANUAL_ADJ() As Boolean
        Get
            Return vbMANUAL_ADJ
        End Get
        Set(ByVal value As Boolean)
            vbMANUAL_ADJ = value
        End Set
    End Property

    Public Property FAH_DATE() As DateTime
        Get
            Return vFAR_DATE
        End Get
        Set(ByVal value As DateTime)
            vFAR_DATE = value
        End Set
    End Property

    Public Property APPROVAL_REMARKS() As String
        Get
            Return vAPPR_REMARKS
        End Get
        Set(ByVal value As String)
            vAPPR_REMARKS = value
        End Set
    End Property

    Public Property APPR_DATE() As DateTime
        Get
            Return vAPPR_DATE
        End Get
        Set(ByVal value As DateTime)
            vAPPR_DATE = value
        End Set
    End Property

    Public Property FAH_GRD_ID() As String
        Get
            Return vFAR_GRD_ID
        End Get
        Set(ByVal value As String)
            vFAR_GRD_ID = value
        End Set
    End Property

    Public Property FAH_ACD_ID() As Integer
        Get
            Return vFAR_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFAR_ACD_ID = value
        End Set
    End Property

    Public Property FAH_STU_ID() As Integer
        Get
            Return vFAR_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFAR_STU_ID = value
        End Set
    End Property

    Public Property TotalAdjustmentAmount() As Double
        Get
            Return vTOTALAMT
        End Get
        Set(ByVal value As Double)
            vTOTALAMT = value
        End Set
    End Property

    Public Property FAH_ID() As Integer
        Get
            Return vFAR_ID
        End Get
        Set(ByVal value As Integer)
            vFAR_ID = value
        End Set
    End Property

    Public Shared Function GetTotalFeePaid(ByVal vSTU_ID As Integer) As DataTable
        Dim conn_str As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_query As String = "SELECT FEES.FEESCHEDULE.FSH_FEE_ID, OASIS.FEES.FEES_M.FEE_DESCR," & _
         " FEES.FEESCHEDULE.FSH_DATE, FEES.FEESCHEDULE.FSH_AMOUNT, " & _
         " FEES.FEESCHEDULE.FSH_SETTLEAMT, GRADE_M.GRD_DISPLAY, ACADEMICYEAR_M.ACY_DESCR " & _
         " FROM OASIS..ACADEMICYEAR_D ACADEMICYEAR_D INNER JOIN" & _
         " OASIS..ACADEMICYEAR_M ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID AND " & _
         " ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN" & _
         " FEES.FEESCHEDULE INNER JOIN OASIS..GRADE_M GRADE_M ON " & _
         " FEES.FEESCHEDULE.FSH_GRD_ID = GRADE_M.GRD_ID INNER JOIN" & _
         " OASIS.FEES.FEES_M ON FEES.FEESCHEDULE.FSH_FEE_ID = OASIS.FEES.FEES_M.FEE_ID ON " & _
         " ACADEMICYEAR_D.ACD_ID = FEES.FEESCHEDULE.FSH_ACD_ID " & _
         " WHERE FSH_STU_ID = " & vSTU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function IsRepeated(ByVal FRS_ID As Integer, ByVal vSTUD_ID As Integer, ByVal vACD_ID As Integer, ByVal vGRD_ID As String, ByVal vBSU_ID As String, ByVal vFEE_ID As Integer) As Boolean
        Dim conn_str As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_query As String = "SELECT count(*) FROM FEES.FEEADJREQUEST_S INNER JOIN" & _
        " FEES.FEEADJREQUEST_H ON FEES.FEEADJREQUEST_S.FRS_FAR_ID = " & _
        " FEES.FEEADJREQUEST_H.FAR_ID WHERE ISNULL(FAR_ApprStatus, '') = 'N' AND " & _
        " FEES.FEEADJREQUEST_S.FRS_FEE_ID = " & vFEE_ID & _
        " AND isnull(FEES.FEEADJREQUEST_H.FAR_bDeleted, 0) = 0 " & _
        " AND FEES.FEEADJREQUEST_H.FAR_ACD_ID =  " & vACD_ID & _
        " AND FEES.FEEADJREQUEST_H.FAR_GRD_ID = '" & vGRD_ID & _
        "' AND FEES.FEEADJREQUEST_H.FAR_STU_ID = " & vSTUD_ID & _
        " AND FEES.FEEADJREQUEST_H.FAR_BSU_ID = '" & vBSU_ID & "'"
        If FRS_ID > 0 Then
            sql_query += " AND FRS_ID <> " & FRS_ID
        End If
        Dim obj As Object = SqlHelper.ExecuteScalar(conn_str, CommandType.Text, sql_query)
        If obj Is DBNull.Value Then
            Return False
        ElseIf obj >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function GetServiceHistory(ByVal vSTU_ID As Integer) As DataTable
        Dim conn_str As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_query As String = "SELECT  OASIS.dbo.ACADEMICYEAR_M.ACY_DESCR, OASIS.dbo.BUSINESSUNIT_M.BSU_NAME, " & _
        " VW_OSO_STUDENT_SERVICES_D.SSV_FROMDATE, VW_OSO_STUDENT_SERVICES_D.SSV_TODATE, " _
        & " TRANSPORT.SUBLOCATION_M.SBL_DESCRIPTION " & _
        " FROM OASIS.dbo.ACADEMICYEAR_M INNER JOIN " & _
        " OASIS.dbo.ACADEMICYEAR_D ON OASIS.dbo.ACADEMICYEAR_M.ACY_ID = OASIS.dbo.ACADEMICYEAR_D.ACD_ACY_ID INNER JOIN " & _
        " VW_OSO_STUDENT_SERVICES_D ON OASIS.dbo.ACADEMICYEAR_D.ACD_ID = VW_OSO_STUDENT_SERVICES_D.SSV_ACD_ID INNER JOIN " & _
        " OASIS.dbo.BUSINESSUNIT_M ON VW_OSO_STUDENT_SERVICES_D.SSV_BSU_ID = OASIS.dbo.BUSINESSUNIT_M.BSU_ID INNER JOIN " & _
        " TRANSPORT.SUBLOCATION_M ON VW_OSO_STUDENT_SERVICES_D.SSV_SBL_ID = TRANSPORT.SUBLOCATION_M.SBL_ID " & _
        " WHERE SSV_STU_ID = " & vSTU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetFeePaidForCurrentAcademicYear(ByVal vSTU_ID As Integer, ByVal vACD_ID As Integer) As DataTable
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query As String = "SELECT FEES.FEESCHEDULE.FSH_FEE_ID, FEES.FEES_M.FEE_DESCR," & _
        " SUM(FEES.FEESCHEDULE.FSH_AMOUNT) FSH_AMOUNT, " & _
        " SUM(FEES.FEESCHEDULE.FSH_SETTLEAMT) FSH_SETTLEAMT, GRADE_M.GRD_DISPLAY" & _
        " FROM FEES.FEESCHEDULE INNER JOIN GRADE_M ON " & _
        " FEES.FEESCHEDULE.FSH_GRD_ID = GRADE_M.GRD_ID INNER JOIN" & _
        " FEES.FEES_M ON FEES.FEESCHEDULE.FSH_FEE_ID = FEES.FEES_M.FEE_ID" & _
        " WHERE FSH_STU_ID = " & vSTU_ID & " AND FEES.FEESCHEDULE.FSH_ACD_ID = " & vACD_ID & _
        " GROUP BY FEES.FEESCHEDULE.FSH_FEE_ID, FEES.FEES_M.FEE_DESCR," & _
        " GRADE_M.GRD_DISPLAY"

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function PopulateFeeMaster(ByVal vBSUID As String) As DataTable
        Dim sql_query As String = "SELECT FEE_ID , FEE_DESCR FROM vw_PopulateTransportFeeType "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
        ' Return FeeCommon.GetFEES_M_TRANSPORT
    End Function

    Public Shared Function GetFeeAdjustments(ByVal FAH_ID As Integer) As FEEADJUSTMENTREQTransport
        Dim str_Sql As String = String.Empty
        Dim tempvFEE_ADJ As New FEEADJUSTMENTREQTransport
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            str_Sql = "select isnull(FAR_STU_TYP, 'S') FROM FEES.FEEADJREQUEST_H " & _
            " WHERE FAR_ID =" & FAH_ID
            Dim vSTU_TYP As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, str_Sql)
            If vSTU_TYP = "S" Then
                str_Sql = "SELECT [FEES].[VW_OSO_STUDENT_ADJ_VIEW].STU_NAME, FAR_APPRSTATUS, SBL_DESCRIPTION, " & _
                " FEES.FEEADJREQUEST_H.FAR_STU_BSU_ID, OASIS.dbo.BUSINESSUNIT_M.BSU_NAME STU_BSU_NAME ," & _
                " JOIN_ACY_DESCR, JOIN_GRD_DISPLAY, STU_DOJ, OASIS.dbo.ACADEMICYEAR_M.ACY_DESCR ACY_DESCR, " & _
                " [FEES].[VW_OSO_STUDENT_ADJ_VIEW].STU_GRD_ID, FEES.FEEADJREQUEST_H.FAR_ID, " & _
                " FEES.FEEADJREQUEST_H.FAR_BSU_ID, FEES.FEEADJREQUEST_H.FAR_EVENT, " & _
                " FEES.FEEADJREQUEST_H.FAR_REMARKS," & _
                " isnull(FEES.FEEADJREQUEST_H.FAR_STU_TYP,'S') FAR_STU_TYP," & _
                " FEES.FEEADJREQUEST_H.FAR_DATE, " & _
                " FEES.FEEADJREQUEST_H.FAR_ACD_ID, FEES.FEEADJREQUEST_H.FAR_STU_ID, " & _
                " VW_OSO_STUDENT_SERVICES_D.SSV_FROMDATE, isnull(VW_OSO_STUDENT_SERVICES_D.SSV_TODATE , '1/1/1900') SSV_TODATE " & _
                " FROM [FEES].[VW_OSO_STUDENT_ADJ_VIEW] INNER JOIN FEES.FEEADJREQUEST_H ON " & _
                "[FEES].[VW_OSO_STUDENT_ADJ_VIEW].STU_ID = FEES.FEEADJREQUEST_H.FAR_STU_ID " & _
                " LEFT OUTER  JOIN TRANSPORT.SUBLOCATION_M ON  FEES.FEEADJREQUEST_H.FAR_SBL_ID = SBL_ID " & _
                " INNER JOIN OASIS.dbo.ACADEMICYEAR_M INNER JOIN OASIS.dbo.ACADEMICYEAR_D ON OASIS.dbo.ACADEMICYEAR_M.ACY_ID " & _
                " = OASIS.dbo.ACADEMICYEAR_D.ACD_ACY_ID ON FEES.FEEADJREQUEST_H.FAR_ACD_ID = OASIS.dbo.ACADEMICYEAR_D.ACD_ID" & _
                " INNER JOIN OASIS.dbo.BUSINESSUNIT_M ON OASIS.dbo.BUSINESSUNIT_M.BSU_ID = FEES.FEEADJREQUEST_H.FAR_STU_BSU_ID" & _
                " LEFT OUTER JOIN VW_OSO_STUDENT_SERVICES_D ON FEES.FEEADJREQUEST_H.FAR_SSV_ID = VW_OSO_STUDENT_SERVICES_D.SSV_ID " & _
                " WHERE FAR_ID =" & FAH_ID
            ElseIf vSTU_TYP = "E" Then
                str_Sql = "SELECT  STU_NAME, FEES.FEEADJREQUEST_H.FAR_STU_BSU_ID, OASIS.dbo.BUSINESSUNIT_M.BSU_NAME STU_BSU_NAME , " & _
                " ACY_DESCR JOIN_ACY_DESCR, GRD_DISPLAY JOIN_GRD_DISPLAY, '1/1/1900' STU_DOJ , ACY_DESCR, " & _
                " STU_GRD_ID, FEES.FEEADJREQUEST_H.FAR_ID, FAR_APPRSTATUS, " & _
                " FEES.FEEADJREQUEST_H.FAR_BSU_ID, FEES.FEEADJREQUEST_H.FAR_EVENT, " & _
                " FEES.FEEADJREQUEST_H.FAR_REMARKS," & _
                " isnull(FEES.FEEADJREQUEST_H.FAR_STU_TYP,'S') FAR_STU_TYP," & _
                " FEES.FEEADJREQUEST_H.FAR_DATE, " & _
                " FEES.FEEADJREQUEST_H.FAR_ACD_ID, FEES.FEEADJREQUEST_H.FAR_STU_ID " & _
                " FROM FEES.vw_OSO_ENQUIRY_COMP INNER JOIN FEES.FEEADJREQUEST_H ON " & _
                " FEES.vw_OSO_ENQUIRY_COMP.STU_ID = FEES.FEEADJREQUEST_H.FAR_STU_ID " & _
                " INNER JOIN OASIS.dbo.BUSINESSUNIT_M ON OASIS.dbo.BUSINESSUNIT_M.BSU_ID = FEES.FEEADJREQUEST_H.FAR_STU_BSU_ID" & _
                " WHERE FAR_ID =" & FAH_ID
            End If

            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ.APPR_STATUS = UtilityObj.GetData(dReader("FAR_APPRSTATUS"))
                tempvFEE_ADJ.FAH_ACD_ID = UtilityObj.GetData(dReader("FAR_ACD_ID"))
                tempvFEE_ADJ.FAR_BSU_ID = UtilityObj.GetData(dReader("FAR_BSU_ID"))
                tempvFEE_ADJ.FAH_DATE = UtilityObj.GetData(dReader("FAR_DATE"))
                tempvFEE_ADJ.FAH_GRD_ID = UtilityObj.GetData(dReader("STU_GRD_ID"))
                tempvFEE_ADJ.FAH_ID = UtilityObj.GetData(dReader("FAR_ID"))
                tempvFEE_ADJ.FAR_EVENT = UtilityObj.GetData(dReader("FAR_EVENT"))
                tempvFEE_ADJ.FAH_REMARKS = UtilityObj.GetData(dReader("FAR_REMARKS"))
                tempvFEE_ADJ.FAR_STU_BSU_ID = UtilityObj.GetData(dReader("FAR_STU_BSU_ID"))
                tempvFEE_ADJ.FAR_STU_BSU_NAME = UtilityObj.GetData(dReader("STU_BSU_NAME"))
                tempvFEE_ADJ.FAH_STU_ID = UtilityObj.GetData(dReader("FAR_STU_ID"))
                tempvFEE_ADJ.SUB_LOCATION = UtilityObj.GetData(dReader("SBL_DESCRIPTION"))
                tempvFEE_ADJ.vSTU_TYP = UtilityObj.GetData(dReader("FAR_STU_TYP"))
                tempvFEE_ADJ.FAR_STU_NAME = UtilityObj.GetData(dReader("STU_NAME"))
                tempvFEE_ADJ.TotalAdjustmentAmount = GetTotalAdjustmentAmount(FAH_ID)
                tempvFEE_ADJ.vJOIN_ACD_DISPLAY = UtilityObj.GetData(dReader("JOIN_ACY_DESCR")).ToString
                tempvFEE_ADJ.vJOIN_DATE = UtilityObj.GetData(dReader("STU_DOJ"))
                tempvFEE_ADJ.vJOIN_GRD_ID = UtilityObj.GetData(dReader("JOIN_GRD_DISPLAY"))
                tempvFEE_ADJ.vFAR_ACD_DISPLAY = UtilityObj.GetData(dReader("ACY_DESCR"))
                tempvFEE_ADJ.FEE_ADJ_DET = GetSubFEEAdjustmentSubDetais(FAH_ID)

                tempvFEE_ADJ.vSER_START_DATE = IIf(dReader("SSV_FROMDATE") Is DBNull.Value, DateTime.MinValue, dReader("SSV_FROMDATE"))
                tempvFEE_ADJ.vSER_DISCONT_DATE = IIf(dReader("SSV_TODATE") Is DBNull.Value, DateTime.MinValue, dReader("SSV_TODATE"))
                Exit While
            End While
        End Using
        Return tempvFEE_ADJ
    End Function

    Private Shared Function GetTotalAdjustmentAmount(ByVal vFAH_ID As Integer) As Double
        Dim strSql As String = "SELECT ISNULL(SUM(FRS_AMOUNT), 0) FROM FEES.FEEADJREQUEST_S WHERE FRS_FAR_ID =" & vFAH_ID
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, strSql)
    End Function

    Public Shared Function GetSubFEEAdjustmentSubDetais(ByVal FAH_ID As Integer) As Hashtable
        Dim str_Sql As String = String.Empty
        Dim htFEE_ADJ_DET As New Hashtable
        Dim tempvFEE_ADJ_S As New FeeAdjustmentRequestTransport_S
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            str_Sql = "SELECT OASIS.FEES.FEES_M.FEE_DESCR, FEES.FEEADJREQUEST_S.FRS_ID, " & _
            " FEES.FEEADJREQUEST_S.FRS_FEE_ID, FEES.FEEADJREQUEST_S.FRS_FAR_ID, " & _
            " FEES.FEEADJREQUEST_S.FRS_AMOUNT, FEES.FEEADJREQUEST_S.FRS_REMARKS, " & _
            " FEES.FEEADJREQUEST_S.FRS_bMonths, FEES.FEEADJREQUEST_S.FRS_Quantity, " & _
            " isnull(FEES.FEEADJREQUEST_S.FRS_TAX_CODE,'NA')FRS_TAX_CODE,isnull(FEES.FEEADJREQUEST_S.FRS_TAX_AMOUNT,0)FRS_TAX_AMOUNT," & _
            " isnull(FEES.FEEADJREQUEST_S.FRS_NET_AMOUNT,0)FRS_NET_AMOUNT" & _
            " FROM OASIS.FEES.FEES_M INNER JOIN FEES.FEEADJREQUEST_S ON " & _
            " OASIS.FEES.FEES_M.FEE_ID = FEES.FEEADJREQUEST_S.FRS_FEE_ID " & _
            " WHERE FRS_FAR_ID =" & FAH_ID
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ_S = New FeeAdjustmentRequestTransport_S
                tempvFEE_ADJ_S.FAD_ID = dReader("FRS_ID")
                tempvFEE_ADJ_S.FAD_FEE_ID = dReader("FRS_FEE_ID")
                tempvFEE_ADJ_S.FAD_FAH_ID = dReader("FRS_FAR_ID")
                tempvFEE_ADJ_S.FAD_AMOUNT = dReader("FRS_AMOUNT")

                tempvFEE_ADJ_S.FAD_TAX_CODE = dReader("FRS_TAX_CODE")
                tempvFEE_ADJ_S.FAD_TAX_AMOUNT = dReader("FRS_TAX_AMOUNT")
                tempvFEE_ADJ_S.FAD_NET_AMOUNT = dReader("FRS_NET_AMOUNT")


                tempvFEE_ADJ_S.bMonth = dReader("FRS_bMonths")
                tempvFEE_ADJ_S.DurationCount = dReader("FRS_Quantity")
                If dReader("FRS_bMonths") Then
                    tempvFEE_ADJ_S.DurationDescription = tempvFEE_ADJ_S.DurationCount & " Month(s)"
                Else
                    tempvFEE_ADJ_S.DurationDescription = tempvFEE_ADJ_S.DurationCount & " Week(s)"
                End If
                tempvFEE_ADJ_S.FAD_REMARKS = dReader("FRS_REMARKS")
                tempvFEE_ADJ_S.FEE_TYPE = dReader("FEE_DESCR")
                htFEE_ADJ_DET(tempvFEE_ADJ_S.FAD_FEE_ID) = tempvFEE_ADJ_S
            End While
        End Using
        Return htFEE_ADJ_DET
    End Function

    Public Shared Function GetSubDetailsAsDataTable(ByVal vFEE_ADJ As FEEADJUSTMENTREQTransport) As DataTable
        Dim htDetails As Hashtable = vFEE_ADJ.FEE_ADJ_DET
        If htDetails Is Nothing Then
            Return Nothing
        End If
        Dim vFEE_DET As FeeAdjustmentRequestTransport_S
        Dim ienum As IDictionaryEnumerator = htDetails.GetEnumerator
        Dim dt As DataTable = CreateDataTable()
        Dim dr As DataRow
        While (ienum.MoveNext())
            If ienum.Value Is Nothing Then Return dt
            vFEE_DET = ienum.Value
            If vFEE_DET.bDelete Then Continue While
            dr = dt.NewRow
            GetChargedCollectedDetails(vFEE_ADJ.FAR_BSU_ID, vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_GRD_ID, vFEE_ADJ.FAH_STU_ID, vFEE_DET.FAD_FEE_ID, dr)
            dr("FEE_ID") = vFEE_DET.FAD_FEE_ID
            dr("DURATION") = vFEE_DET.DurationDescription
            dr("FEE_TYPE") = vFEE_DET.FEE_TYPE
            dr("FEE_AMOUNT") = vFEE_DET.FAD_AMOUNT
            dr("FEE_REMARKS") = vFEE_DET.FAD_REMARKS
            dr("FEE_TAX_CODE") = vFEE_DET.FAD_TAX_CODE
            dr("FEE_TAX_AMOUNT") = vFEE_DET.FAD_TAX_AMOUNT
            dr("FEE_NET_AMOUNT") = vFEE_DET.FAD_NET_AMOUNT
            dt.Rows.Add(dr)
        End While
        Return dt
    End Function

    Private Shared Sub GetChargedCollectedDetails(ByVal vBSU_ID As String, ByVal vACD_ID As Integer, _
    ByVal vGRD_ID As String, ByVal vSTU_ID As Integer, ByVal vFEE_ID As Integer, ByRef dr As DataRow)
        Dim str_Sql As String = "SELECT SUM(CASE FSH_DRCR WHEN 'DR' THEN FSH_AMOUNT ELSE 0 END) " & _
        "- SUM(CASE FSH_DRCR WHEN 'CR' THEN FSH_AMOUNT ELSE 0 END) AS 'CHARGEDAMOUNT', " & _
        " SUM(CASE FSH_DRCR WHEN 'DR' THEN FSH_SETTLEAMT ELSE 0 END) " & _
        " - SUM(CASE FSH_DRCR WHEN 'CR' THEN FSH_SETTLEAMT ELSE 0 END) AS 'PAIDAMOUNT'" & _
        " FROM FEES.FEESCHEDULE  " & _
        " WHERE ( FSH_STU_ID = " & vSTU_ID & ") AND (FSH_ACD_ID = " & vACD_ID & ") " & _
        " AND (FSH_GRD_ID = '" & vGRD_ID & "') AND FSH_FEE_ID =" & vFEE_ID & _
        " GROUP BY FSH_FEE_ID "
        Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)
        While (dReader.Read())
            dr("CHARGEDAMOUNT") = dReader("CHARGEDAMOUNT")
            dr("PAIDAMOUNT") = dReader("PAIDAMOUNT")
        End While 
    End Sub

    Private Shared Function CreateDataTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.Int32"))
            Dim cFEE_TYPE As New DataColumn("FEE_TYPE", System.Type.GetType("System.String"))
            Dim cCHARGEDAMOUNT As New DataColumn("CHARGEDAMOUNT", System.Type.GetType("System.Double"))
            Dim cPAIDAMOUNT As New DataColumn("PAIDAMOUNT", System.Type.GetType("System.Double"))
            Dim cDuration As New DataColumn("DURATION", System.Type.GetType("System.String"))
            Dim cFEE_AMOUNT As New DataColumn("FEE_AMOUNT", System.Type.GetType("System.Double"))
            Dim cFEE_REMARKS As New DataColumn("FEE_REMARKS", System.Type.GetType("System.String"))

            Dim cFEE_TAX_CODE As New DataColumn("FEE_TAX_CODE", System.Type.GetType("System.String"))
            Dim cFEE_TAX_AMOUNT As New DataColumn("FEE_TAX_AMOUNT", System.Type.GetType("System.Double"))
            Dim cFEE_NET_AMOUNT As New DataColumn("FEE_NET_AMOUNT", System.Type.GetType("System.Double"))

            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cDuration)
            dtDt.Columns.Add(cFEE_TYPE)
            dtDt.Columns.Add(cCHARGEDAMOUNT)
            dtDt.Columns.Add(cPAIDAMOUNT)
            dtDt.Columns.Add(cFEE_AMOUNT)
            dtDt.Columns.Add(cFEE_REMARKS)
            dtDt.Columns.Add(cFEE_TAX_CODE)
            dtDt.Columns.Add(cFEE_TAX_AMOUNT)
            dtDt.Columns.Add(cFEE_NET_AMOUNT)

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Public Shared Function F_SAVEFEEADJREQUEST_H(ByRef NEW_FAR_ID As Int64, ByVal FEE_ADJ_DET As FEEADJUSTMENTREQTransport, _
    ByVal USER As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 1000
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.[F_SAVEFEEADJREQUEST_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAR_BSU_ID As New SqlParameter("@FAR_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAR_BSU_ID.Value = FEE_ADJ_DET.FAR_BSU_ID
        cmd.Parameters.Add(sqlpFAR_BSU_ID)

        Dim sqlpFAR_STU_BSU_ID As New SqlParameter("@FAR_STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAR_STU_BSU_ID.Value = FEE_ADJ_DET.FAR_STU_BSU_ID
        cmd.Parameters.Add(sqlpFAR_STU_BSU_ID)

        Dim sqlpFAR_STU_ID As New SqlParameter("@FAR_STU_ID", SqlDbType.BigInt)
        sqlpFAR_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAR_STU_ID)

        Dim sqlpFAR_STU_TYP As New SqlParameter("@FAR_STU_TYP", SqlDbType.VarChar, 1)
        sqlpFAR_STU_TYP.Value = FEE_ADJ_DET.vSTU_TYP
        cmd.Parameters.Add(sqlpFAR_STU_TYP)

        Dim sqlpFAR_GRD_ID As New SqlParameter("@FAR_GRD_ID", SqlDbType.VarChar, 10)
        sqlpFAR_GRD_ID.Value = FEE_ADJ_DET.FAH_GRD_ID
        cmd.Parameters.Add(sqlpFAR_GRD_ID)

        Dim sqlpFAH_DATE As New SqlParameter("@FAR_DATE", SqlDbType.DateTime)
        sqlpFAH_DATE.Value = FEE_ADJ_DET.FAH_DATE
        cmd.Parameters.Add(sqlpFAH_DATE)

        Dim sqlpFAH_ACD_ID As New SqlParameter("@FAR_ACD_ID", SqlDbType.Int)
        sqlpFAH_ACD_ID.Value = FEE_ADJ_DET.FAH_ACD_ID
        cmd.Parameters.Add(sqlpFAH_ACD_ID)

        Dim sqlpFAR_EVENT As New SqlParameter("@FAR_EVENT", SqlDbType.VarChar, 2)
        sqlpFAR_EVENT.Value = FEE_ADJ_DET.FAR_EVENT
        cmd.Parameters.Add(sqlpFAR_EVENT)

        Dim sqlpFAH_REMARKS As New SqlParameter("@FAR_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.FAH_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        If FEE_ADJ_DET.bDelete OrElse FEE_ADJ_DET.bEdit Then
            Dim sqlpFAH_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)
        End If

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = FEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = FEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim sqlpNewFAR_ID As New SqlParameter("@NEW_FAR_ID", SqlDbType.Int)
        sqlpNewFAR_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNewFAR_ID)

        Dim sqlpFAR_REQUESTED_BY As New SqlParameter("@FAR_REQUESTED_BY", SqlDbType.VarChar, 200)
        sqlpFAR_REQUESTED_BY.Value = USER
        cmd.Parameters.Add(sqlpFAR_REQUESTED_BY)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue = 0 Then iReturnvalue = SaveFeeSubDetails(FEE_ADJ_DET, sqlpNewFAR_ID.Value, conn, trans)
        NEW_FAR_ID = sqlpNewFAR_ID.Value
        Return iReturnvalue
    End Function

    Public Shared Function F_SAVEFEEADJAPPOVED_TO_FEEADJ(ByVal FEE_ADJ_DET As FEEADJUSTMENTREQTransport, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.[F_SAVEFEEADJAPPOVED_TO_FEEADJ]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAR_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAR_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
        sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
        cmd.Parameters.Add(sqlpFAH_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAR_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpAPPR_DATE As New SqlParameter("@APPR_DATE", SqlDbType.DateTime)
        sqlpAPPR_DATE.Value = FEE_ADJ_DET.APPR_DATE
        cmd.Parameters.Add(sqlpAPPR_DATE)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        Return iReturnvalue
    End Function

    Public Shared Function F_SAVEFEEADJAPPROVAL(ByVal FEE_ADJ_DET As FEEADJUSTMENTREQTransport, ByVal bApprove As Boolean, _
    ByRef FAH_ID As String, ByVal USER As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.[F_SAVEFEEADJAPPROVAL]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAR_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAR_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
        sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
        cmd.Parameters.Add(sqlpFAH_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAR_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpFAH_REMARKS As New SqlParameter("@APPR_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.APPROVAL_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        Dim sqlpAPPROV_CODE As New SqlParameter("@APPROV_CODE", SqlDbType.VarChar, 1)
        If bApprove Then
            sqlpAPPROV_CODE.Value = "A"
        Else
            sqlpAPPROV_CODE.Value = "R"
        End If
        cmd.Parameters.Add(sqlpAPPROV_CODE)

        Dim sqlpAPPR_DATE As New SqlParameter("@APPR_DATE", SqlDbType.DateTime)
        sqlpAPPR_DATE.Value = FEE_ADJ_DET.APPR_DATE
        cmd.Parameters.Add(sqlpAPPR_DATE)

        Dim sqlpNEW_FAH_ID As New SqlParameter("@NEW_FAH_ID", SqlDbType.Int)
        sqlpNEW_FAH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEW_FAH_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim sqlpFAR_APPROVED_BY As New SqlParameter("@FAR_APPROVED_BY", SqlDbType.VarChar, 200)
        sqlpFAR_APPROVED_BY.Value = USER
        cmd.Parameters.Add(sqlpFAR_APPROVED_BY)


        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        FAH_ID = sqlpNEW_FAH_ID.Value
        If bApprove Then
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEEADJAPPROVAL_S(FEE_ADJ_DET, sqlpNEW_FAH_ID.Value, FEE_ADJ_DET.FAH_ID, conn, trans)
        End If
        Return iReturnvalue
    End Function

    Private Shared Function F_SAVEFEEADJAPPROVAL_S(ByVal vFEE_ADJ_APPR As FEEADJUSTMENTREQTransport, ByVal FAH_ID As Integer, ByVal FAR_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        For Each vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S In vFEE_ADJ_APPR.FEE_ADJ_DET.Values
            If vFEE_ADJ_DET Is Nothing Then
                Continue For
            End If
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.F_SAVEFEEADJAPPROVAL_S", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFRS_FAR_ID As New SqlParameter("@FRS_FAR_ID", SqlDbType.Int)
            sqlpFRS_FAR_ID.Value = FAR_ID 'STU_SUB_DET.FPD_FPH_ID
            cmd.Parameters.Add(sqlpFRS_FAR_ID)

            Dim sqlpFAH_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)

            Dim sqlpFRS_ID As New SqlParameter("@FRS_ID", SqlDbType.Int)
            sqlpFRS_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFRS_ID)

            Dim sqlpFRS_FEE_ID As New SqlParameter("@FRS_FEE_ID", SqlDbType.Int)
            sqlpFRS_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
            cmd.Parameters.Add(sqlpFRS_FEE_ID)

            Dim sqlpFRS_AMOUNT As New SqlParameter("@APPR_AMOUNT", SqlDbType.Decimal)
            sqlpFRS_AMOUNT.Value = vFEE_ADJ_DET.APPROVEDAMOUNT
            cmd.Parameters.Add(sqlpFRS_AMOUNT)

            Dim sqlpFAD_TAX_CODE As New SqlParameter("@FAD_TAX_CODE", SqlDbType.VarChar)
            sqlpFAD_TAX_CODE.Value = vFEE_ADJ_DET.FAD_TAX_CODE
            cmd.Parameters.Add(sqlpFAD_TAX_CODE)

            Dim sqlpFAD_TAX_AMOUNT As New SqlParameter("@FAD_TAX_AMOUNT", SqlDbType.Decimal)
            sqlpFAD_TAX_AMOUNT.Value = vFEE_ADJ_DET.FAD_TAX_AMOUNT
            cmd.Parameters.Add(sqlpFAD_TAX_AMOUNT)

            Dim sqlpFAD_NET_AMOUNT As New SqlParameter("@FAD_NET_AMOUNT", SqlDbType.Decimal)
            sqlpFAD_NET_AMOUNT.Value = vFEE_ADJ_DET.FAD_NET_AMOUNT
            cmd.Parameters.Add(sqlpFAD_NET_AMOUNT)



            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return 0
    End Function

    Private Shared Function SaveFeeSubDetails(ByVal vFEE_ADJ As FEEADJUSTMENTREQTransport, ByVal FAR_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer = 0
        For Each vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S In vFEE_ADJ.FEE_ADJ_DET.Values
            If vFEE_ADJ_DET Is Nothing Then
                Continue For
            End If
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEEADJREQUEST_S(vFEE_ADJ_DET, FAR_ID, conn, trans)
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function GetSCTID(ByVal vBSU_ID As String, ByVal vSTU_ID As Integer) As Integer
        Dim str_sql As String
        str_sql = "SELECT STU_SCT_ID FROM STUDENT_M WHERE STU_ID = " & vSTU_ID
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
    End Function

    Private Shared Function F_SAVEFEEADJREQUEST_S(ByVal vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("FEES.F_SAVEFEEADJREQUEST_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRS_FAR_ID As New SqlParameter("@FRS_FAR_ID", SqlDbType.Int)
        sqlpFRS_FAR_ID.Value = FAH_ID 'STU_SUB_DET.FPD_FPH_ID
        cmd.Parameters.Add(sqlpFRS_FAR_ID)

        Dim sqlpFRS_FEE_ID As New SqlParameter("@FRS_FEE_ID", SqlDbType.Int)
        sqlpFRS_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFRS_FEE_ID)

        Dim sqlpFRS_AMOUNT As New SqlParameter("@FRS_AMOUNT", SqlDbType.Decimal)
        sqlpFRS_AMOUNT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFRS_AMOUNT)

        Dim sqlpFRS_Quantity As New SqlParameter("@FRS_Quantity", SqlDbType.Int)
        sqlpFRS_Quantity.Value = vFEE_ADJ_DET.DurationCount
        cmd.Parameters.Add(sqlpFRS_Quantity)

        Dim sqlpFRS_bMonths As New SqlParameter("@FRS_bMonths", SqlDbType.Bit)
        sqlpFRS_bMonths.Value = vFEE_ADJ_DET.bMonth
        cmd.Parameters.Add(sqlpFRS_bMonths)

        Dim sqlpFRS_REMARKS As New SqlParameter("@FRS_REMARKS", SqlDbType.VarChar, 200)
        sqlpFRS_REMARKS.Value = vFEE_ADJ_DET.FAD_REMARKS
        cmd.Parameters.Add(sqlpFRS_REMARKS)

        If vFEE_ADJ_DET.bDelete Or vFEE_ADJ_DET.bEdit Then
            Dim sqlpFRS_ID As New SqlParameter("@FRS_ID", SqlDbType.Int)
            sqlpFRS_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFRS_ID)
        End If

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = vFEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpFRS_TAX_CODE As New SqlParameter("@FRS_TAX_CODE", SqlDbType.VarChar, 20)
        sqlpFRS_TAX_CODE.Value = vFEE_ADJ_DET.FAD_TAX_CODE
        cmd.Parameters.Add(sqlpFRS_TAX_CODE)


        Dim sqlpFRS_TAX_AMOUNT As New SqlParameter("@FRS_TAX_AMOUNT", SqlDbType.Decimal)
        sqlpFRS_TAX_AMOUNT.Value = vFEE_ADJ_DET.FAD_TAX_AMOUNT
        cmd.Parameters.Add(sqlpFRS_TAX_AMOUNT)

        Dim sqlpFRS_FRS_NET_AMOUNT As New SqlParameter("@FRS_NET_AMOUNT", SqlDbType.Decimal)
        sqlpFRS_FRS_NET_AMOUNT.Value = vFEE_ADJ_DET.FAD_NET_AMOUNT
        cmd.Parameters.Add(sqlpFRS_FRS_NET_AMOUNT)



        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        Return retSValParam.Value
    End Function

End Class
