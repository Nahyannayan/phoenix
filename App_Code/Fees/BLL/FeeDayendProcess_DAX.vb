﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Linq
Imports System.Threading
Imports System.Threading.Tasks

Public Class FeeDayendProcess_DAX
    Public Shared Function POSTVOUCHER_CHQ(ByVal p_VCH_SUB_ID As String, ByVal p_VCH_BSU_ID As String, _
                  ByVal p_VCH_FYEAR As String, ByVal p_VCH_DOCTYPE As String, ByVal p_VCH_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '@return_value = [dbo].[POSTVOUCHER_CHQ]
        '@VCH_SUB_ID = N'007',
        '@VCH_BSU_ID = N'125016',
        '@VCH_FYEAR = 2008gh,
        '@VCH_DOCTYPE = N'damn',
        '@VCH_DOCNO = N'd0005' 
        pParms(0) = New SqlClient.SqlParameter("@VCH_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_VCH_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VCH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_VCH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VCH_FYEAR", SqlDbType.VarChar, 50)
        pParms(2).Value = p_VCH_FYEAR
        pParms(3) = New SqlClient.SqlParameter("@VCH_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_VCH_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCH_DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_VCH_DOCNO
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "OASIS_DAX.FIN.POSTVOUCHER_CHQ", pParms)
        POSTVOUCHER_CHQ = pParms(5).Value
    End Function

    Public Shared Function POSTVOUCHER_QR(ByVal p_VHH_SUB_ID As String, ByVal p_VHH_BSU_ID As String, _
              ByVal p_VHH_FYEAR As String, ByVal p_VHH_DOCTYPE As String, ByVal p_VHH_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '      EXEC	@return_value = [dbo].[POSTVOUCHER_QR]
        '@VHH_SUB_ID = N'007',
        '@VHH_BSU_ID = N'125016',
        '@VHH_FYEAR = 2008,
        '@VHH_DOCTYPE = N'12-AUG-2008',
        '@VHH_DOCNO = N'072C567' 
        pParms(0) = New SqlClient.SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_VHH_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_VHH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VHH_FYEAR", SqlDbType.VarChar, 50)
        pParms(2).Value = p_VHH_FYEAR
        pParms(3) = New SqlClient.SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_VHH_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_VHH_DOCNO
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "OASIS_DAX.FIN.POSTVOUCHER_QR", pParms)
        POSTVOUCHER_QR = pParms(5).Value
    End Function

    Public Shared Function POSTVOUCHER_CHQ_REVERSE(ByVal p_VCH_SUB_ID As String, ByVal p_VCH_BSU_ID As String, _
      ByVal p_VCH_FYEAR As String, ByVal p_VCH_DOCTYPE As String, ByVal p_VCH_DOCNO As String, _
       ByVal p_DT As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '     EXEC	@return_value = [dbo].[POSTVOUCHER_CHQ_REVERSE]
        '@VCH_SUB_ID = N'007',
        '@VCH_BSU_ID = N'125016',
        '@VCH_FYEAR = 2008,
        '@VCH_DOCTYPE = N'QR',
        '@VCH_DOCNO = N'qerr',
        '@DT = N'12-sep-2008'
        pParms(0) = New SqlClient.SqlParameter("@VCH_SUB_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_VCH_SUB_ID
        pParms(1) = New SqlClient.SqlParameter("@VCH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_VCH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@VCH_FYEAR", SqlDbType.VarChar, 50)
        pParms(2).Value = p_VCH_FYEAR
        pParms(3) = New SqlClient.SqlParameter("@VCH_DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_VCH_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCH_DOCNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_VCH_DOCNO
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        pParms(6) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(6).Value = p_DT
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "OASIS_DAX.FIN.POSTVOUCHER_CHQ_REVERSE", pParms)
        POSTVOUCHER_CHQ_REVERSE = pParms(5).Value
    End Function

    Public Shared Function GenerateQRDetails(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_SUB_ID As String, _
        ByVal p_DOCTYPE As String, ByVal p_VCD_DET As String, ByVal p_VHH_NEWDOCNO As String, ByVal p_USER As String, _
        ByVal p_FYR_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(8) As SqlClient.SqlParameter
        'EXEC	@return_value = [dbo].[GenerateQRDetails]
        '@BSU_ID = N'125016',
        '@Dt = N'27-JUL-2008',
        '@SUB_ID = N'007',
        '@DOCTYPE = N'QR',
        '@VCD_IDS = N'61|62|63', @FYR_ID
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SUB_ID
        pParms(3) = New SqlClient.SqlParameter("@DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCD_DET", SqlDbType.Xml)
        pParms(4).Value = p_VCD_DET
        '@VHH_NEWDOCNO = N'XXX',
        '@USER = N'GURU'
        pParms(5) = New SqlClient.SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(5).Value = p_VHH_NEWDOCNO
        pParms(6) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(6).Value = p_USER
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@FYR_ID", SqlDbType.VarChar, 10)
        pParms(8).Value = p_FYR_ID

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FIN.GenerateQRDetails", pParms)
        GenerateQRDetails = pParms(7).Value
    End Function

    Public Shared Function GenerateQRDetails_Transport(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_SUB_ID As String, _
            ByVal p_DOCTYPE As String, ByVal p_VCD_DET As String, ByVal p_VHH_NEWDOCNO As String, ByVal p_USER As String, _
            ByVal p_FYR_ID As String, ByVal p_STU_BSU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(9) As SqlClient.SqlParameter
        'EXEC	@return_value = [dbo].[GenerateQRDetails]
        '@BSU_ID = N'125016',
        '@Dt = N'27-JUL-2008',
        '@SUB_ID = N'007',
        '@DOCTYPE = N'QR',
        '@VCD_IDS = N'61|62|63', @FYR_ID
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SUB_ID
        pParms(3) = New SqlClient.SqlParameter("@DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VCD_DET", SqlDbType.Xml)
        pParms(4).Value = p_VCD_DET
        '@VHH_NEWDOCNO = N'XXX',
        '@USER = N'GURU'
        pParms(5) = New SqlClient.SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(5).Value = p_VHH_NEWDOCNO
        pParms(6) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(6).Value = p_USER
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@FYR_ID", SqlDbType.VarChar, 10)
        pParms(8).Value = p_FYR_ID
        pParms(9) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_STU_BSU_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FIN.GenerateQRDetails_Transport", pParms)
        GenerateQRDetails_Transport = pParms(7).Value
    End Function
    Public Shared Function CopyVouchersToDAXStaging(ByVal BSU_ID As String, ByVal TRN_TYPE As String, _
            ByVal SOURCE As String, ByVal STATUS_TYPE As String, ByVal TRN_SUB_TYPES As String, DOCNOS As String, ByVal p_stTrans As SqlTransaction, ByRef RETURN_STATUS As String, ByRef RETURN_MESSAGE As String) As String
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@TRN_TYPE", SqlDbType.VarChar, 20)
        pParms(1).Value = TRN_TYPE
        pParms(2) = New SqlClient.SqlParameter("@SOURCE", SqlDbType.VarChar, 50)
        pParms(2).Value = SOURCE
        pParms(3) = New SqlClient.SqlParameter("@STATUS_TYPE", SqlDbType.VarChar, 20)
        pParms(3).Value = STATUS_TYPE
        pParms(4) = New SqlClient.SqlParameter("@TRN_SUB_TYPES", SqlDbType.VarChar, 2000)
        pParms(4).Value = TRN_SUB_TYPES
        pParms(5) = New SqlClient.SqlParameter("@DOCNOS", SqlDbType.VarChar, 2000)
        pParms(5).Value = DOCNOS
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue

        pParms(7) = New SqlClient.SqlParameter("@RETURN_STATUS", SqlDbType.VarChar, 2000)
        pParms(7).Direction = ParameterDirection.Output


        pParms(8) = New SqlClient.SqlParameter("@RETURN_MESSAGE", SqlDbType.VarChar, 2000)
        pParms(8).Direction = ParameterDirection.Output

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "OASIS_DAX.[FEES].[SP_COPY_VOUCHER_TO_DAX_STAGING]", pParms)
        CopyVouchersToDAXStaging = pParms(6).Value

        RETURN_STATUS = pParms(7).Value
        RETURN_MESSAGE = pParms(8).Value

    End Function


    Private pNEW_DOCNO As String
    Public Property pbNEW_DOCNO() As String
        Get
            Return pNEW_DOCNO
        End Get
        Protected Set(ByVal value As String)
            pNEW_DOCNO = value
        End Set
    End Property

    Private pSTATUS As String
    Public Property STATUS() As String
        Get
            Return pSTATUS
        End Get
        Protected Set(ByVal value As String)
            pSTATUS = value
        End Set
    End Property
    ''' <summary>
    ''' Gets the percent complete for the long-running process.
    ''' </summary>
    Private privatePercentComplete As Double
    Public Property PercentComplete() As Double
        Get
            Return privatePercentComplete
        End Get
        Protected Set(ByVal value As Double)
            privatePercentComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is still executing.
    ''' </summary>
    Private privateIsRunning As Boolean
    Public Property IsRunning() As Boolean
        Get
            Return privateIsRunning
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsRunning = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is done running.
    ''' </summary>
    Private privateIsComplete As Boolean
    Public Property IsComplete() As Boolean
        Get
            Return privateIsComplete
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsComplete = value
        End Set
    End Property
    Private privateIsSuccess As Boolean
    Public Property IsSuccess() As Boolean
        Get
            Return privateIsSuccess
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsSuccess = value
        End Set
    End Property
    ''' <summary>
    ''' Gets a brief description of what is currently being worked on, to update the end-user.
    ''' </summary>
    Public Property WorkDescription() As String

    ''' <summary>
    ''' Event for when progress has changed.
    ''' </summary>
    Public Event ProgressChanged As EventHandler(Of ProgressChangedEventArgs)

    ''' <summary>
    ''' Event for the the long-running process is complete.
    ''' </summary>
    Public Event ProcessComplete As EventHandler
    ''' <summary>
    ''' Private handle to the task that is currently execute the long-running process.
    ''' </summary>
    Private task As Task

    Public Sub UpdatePercent(ByVal newPercentComplete As Double, ByVal workDescription As String)
        ' Update the properties for any callers casually checking the status.
        Me.PercentComplete = newPercentComplete
        Me.WorkDescription = workDescription
        If newPercentComplete = 100 Then
            IsComplete = True
        End If
        ' Fire the event for any callers who actively want to be notifed.
        RaiseEvent ProgressChanged(Me, New ProgressChangedEventArgs(newPercentComplete, IsComplete, workDescription))
    End Sub


    Public Sub StartProcess()
        IsComplete = False
        IsRunning = True
        IsSuccess = True
        PercentComplete = 0
        UpdatePercent(0, "Please wait..")
    End Sub
    Public Sub EndProcess(ByVal status As Boolean, ByVal msg As String)
        UpdatePercent(100, msg)
        IsSuccess = status
        Thread.Sleep(1200)
        RaiseEvent ProcessComplete(Me, New EventArgs())
        IsRunning = False
    End Sub
    Public Shared Function GenerateBRFromQR(ByVal p_BSU_ID As String, ByVal p_Dt As String, ByVal p_SUB_ID As String, _
        ByVal p_DOCTYPE As String, ByVal p_VHD_DET As String, ByVal p_VHH_NEWDOCNO As String, ByVal p_USER As String, _
        ByVal p_FYR_ID As String, ByVal p_NARRATION As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter
        'exec([dbo].[GenerateBRFromQR])
        '@BSU_ID ='123004',
        '@SUB_ID ='007',
        '@FYR_ID='2009',
        '@DOCTYPE='BR',
        '@VHH_NEWDOCNO ='',
        '@Dt   ='07/oct/2009',
        '@VHD_DET='',
        '@NARRATION= 'veruthe',
        '@USER= 'koppu'
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SUB_ID
        pParms(3) = New SqlClient.SqlParameter("@DOCTYPE", SqlDbType.VarChar, 10)
        pParms(3).Value = p_DOCTYPE
        pParms(4) = New SqlClient.SqlParameter("@VHD_DET", SqlDbType.Xml)
        pParms(4).Value = p_VHD_DET
        pParms(5) = New SqlClient.SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(5).Value = p_VHH_NEWDOCNO
        pParms(6) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(6).Value = p_USER
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@FYR_ID", SqlDbType.VarChar)
        pParms(8).Value = p_FYR_ID
        pParms(9) = New SqlClient.SqlParameter("@NARRATION", SqlDbType.VarChar)
        pParms(9).Value = p_NARRATION
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FIN.GenerateBRFromQR", pParms)
        GenerateBRFromQR = pParms(7).Value
    End Function

    Public Shared Function PrintDepostSlip(ByVal BSU_ID As String, ByVal SUB_ID As String, _
    ByVal FYear As String, ByVal DOC_NO As String, ByVal Usr_name As String) As MyReportClass
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
        ' GetBankDepositData 
        '@VHD_DOCTYPE varchar(10),
        '@VHD_BSU_ID  varchar(20),
        '@VHD_FYEAR  varchar(20),
        '@VHD_SUB_ID  varchar(20),
        '@VHD_DOCNO  varchar(20)  ViewState("VHD_DOCNO")  ViewState("FYear")
        Dim cmd As New SqlCommand("FIN.GetBankDepositData", New SqlConnection(str_conn))
        cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "QR")
        cmd.Parameters.AddWithValue("@VHD_BSU_ID", BSU_ID)
        cmd.Parameters.AddWithValue("@VHD_FYEAR", FYear)
        cmd.Parameters.AddWithValue("@VHD_SUB_ID", SUB_ID)
        cmd.Parameters.AddWithValue("@VHD_DOCNO", DOC_NO)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Usr_name
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../fees/Reports/RPT/rptFeeBankDepositSlip.rpt"
        Return repSource
    End Function

    Public Shared Function PrintQR(ByVal DocNo As String, ByVal DocDate As String, ByVal BSuId As String, ByVal FYear As String, Optional ByVal docType As String = "", Optional ByVal NdocNo As String = "") As MyReportClass
        Dim str_Sql, strFilter As String
        Dim IsCost As Boolean = True
        Dim Narr As Boolean = True
        If docType = "" Then
            docType = DocNo.Substring(2, 2)
        End If
        If DocDate = "" Then
            strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DocNo _
                    & "' and VHH_BSU_ID in('" & BSuId & "')"
        ElseIf NdocNo <> "" Then
            strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO  in ('" & NdocNo & "')" _
                          & " and VHH_DOCDT = '" & DocDate & "' and VHH_BSU_ID in('" & BSuId & "')"
        Else
            strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DocNo _
                   & "' and VHH_DOCDT = '" & String.Format("{0:dd-MMM-yyyy}", CDate(DocDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        End If
        str_Sql = "SELECT * FROM FIN.vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim VochNamePara As String = "Cheque Deposit Voucher"
        Dim VochName As String = "Cheque Deposit Voucher"
        Dim RptName As String = "../../fees/Reports/RPT/rptFeeBankReceiptReport.rpt"
        DocNo = DocNo.Replace("'", "")
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, docType, DocDate)
            params("voucherName") = VochNamePara
            params("Summary") = False
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.VoucherName = VochName
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.SubReports(DocNo, docType, BSuId, IsCost, Narr)
            repSource.Command = cmd
            repSource.ResourceName = RptName
        End If
        Return repSource
    End Function

   

End Class
