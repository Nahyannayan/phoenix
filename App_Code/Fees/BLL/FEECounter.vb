Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
''' The class Created for handling the Concession Type
''' 
''' Author : SHIJIN C A
''' Date : 12-JUNE-2008 
''' </summary>
''' <remarks></remarks>
''' 
Public Class FEECounter

    ''' <summary>
    ''' Variables used to Store the 
    ''' </summary>
    ''' <remarks></remarks>
    Dim vFCM_ID As Integer
    Dim vFCM_DESCR As String
    Dim vBSU_ID As String
    Dim vBSU_NAME As String
    Dim vUSR_ID As String
    Dim vUSR_NAME As String
    Dim bDelete As Boolean

    Public Property Delete() As Boolean
        Get
            Return bDelete
        End Get
        Set(ByVal value As Boolean)
            bDelete = value
        End Set
    End Property

    Public Property FEE_COUNTER_ID() As Integer
        Get
            Return vFCM_ID
        End Get
        Set(ByVal value As Integer)
            vFCM_ID = value
        End Set
    End Property

    Public Property FEE_COUNTER_DESCR() As String
        Get
            Return vFCM_DESCR
        End Get
        Set(ByVal value As String)
            vFCM_DESCR = value
        End Set
    End Property

    Public Property BSU_ID() As String
        Get
            Return vBSU_ID
        End Get
        Set(ByVal value As String)
            vBSU_ID = value
        End Set
    End Property

    Public Property BSU_NAME() As String
        Get
            Return vBSU_NAME
        End Get
        Set(ByVal value As String)
            vBSU_NAME = value
        End Set
    End Property

    Public Property USER_ID() As String
        Get
            Return vUSR_ID
        End Get
        Set(ByVal value As String)
            vUSR_ID = value
        End Set
    End Property

    Public Property USER_NAME() As String
        Get
            Return vUSR_NAME
        End Get
        Set(ByVal value As String)
            vUSR_NAME = value
        End Set
    End Property

    Public Shared Function SaveDetails(ByVal vFEE_COUNTER As FEECounter, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

        If vFEE_COUNTER Is Nothing Then
            Return -1
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        '@FCM_BSU_ID	varchar(20),
        '@FCM_DESCR	varchar(50),
        '@userID varchar(20),

        cmd = New SqlCommand("[FEES].[F_SaveFEE_COUNTER]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFCM_BSU_ID As New SqlParameter("@FCM_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFCM_BSU_ID.Value = vFEE_COUNTER.vBSU_ID
        cmd.Parameters.Add(sqlpFCM_BSU_ID)

        Dim sqlpFCM_DESCR As New SqlParameter("@FCM_DESCR", SqlDbType.VarChar, 50)
        sqlpFCM_DESCR.Value = vFEE_COUNTER.vFCM_DESCR
        cmd.Parameters.Add(sqlpFCM_DESCR)

        Dim sqlpuserID As New SqlParameter("@userID", SqlDbType.VarChar, 20)
        sqlpuserID.Value = vFEE_COUNTER.vUSR_ID
        cmd.Parameters.Add(sqlpuserID)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_COUNTER.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        If vFEE_COUNTER.FEE_COUNTER_ID <> 0 Then

            Dim sqlpFCM_ID As New SqlParameter("@FCM_ID", SqlDbType.Int)
            sqlpFCM_ID.Value = vFEE_COUNTER.FEE_COUNTER_ID
            cmd.Parameters.Add(sqlpFCM_ID)
        End If

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
    End Function

    Public Shared Function GetDetails(ByVal vFCM_ID As Integer) As FEECounter
        Dim cmd As SqlCommand
        Dim vfeeCounter As New FEECounter
        Dim str_sql As String
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        str_sql = "SELECT FEES.FEECOUNTER_M.FCM_ID, FEES.FEECOUNTER_M.FCM_BSU_ID, " & _
        " FEES.FEECOUNTER_M.FCM_DESCR, BUSINESSUNIT_M.BSU_NAME, " & _
        " isnull(USERS_M.USR_NAME,'') AS USR_NAME, isnull(USERS_M.USR_ID,0) AS USR_ID" & _
        " FROM FEES.FEECOUNTER_M LEFT OUTER JOIN" & _
        " OASIS..USERS_M ON FEES.FEECOUNTER_M.FCM_ID = USERS_M.USR_FCM_ID LEFT OUTER JOIN " & _
        " BUSINESSUNIT_M ON FEES.FEECOUNTER_M.FCM_BSU_ID = BUSINESSUNIT_M.BSU_ID " & _
        " WHERE FCM_ID = " & vFCM_ID
        cmd = New SqlCommand(str_sql, conn)
        cmd.CommandType = CommandType.Text
        Try
            conn.Close()
            conn.Open()
            Dim sqlReader As SqlDataReader = cmd.ExecuteReader()
            While (sqlReader.Read())
                vfeeCounter.BSU_ID = sqlReader("FCM_BSU_ID")
                vfeeCounter.BSU_NAME = sqlReader("BSU_NAME")
                vfeeCounter.FEE_COUNTER_DESCR = sqlReader("FCM_DESCR")
                vfeeCounter.FEE_COUNTER_ID = sqlReader("FCM_ID")
                vfeeCounter.USER_ID = sqlReader("USR_ID")
                vfeeCounter.USER_NAME = sqlReader("USR_NAME")
                Exit While
            End While
        Catch ex As Exception
            Return Nothing
        Finally
            conn.Close()
        End Try
        Return vfeeCounter
    End Function
End Class
