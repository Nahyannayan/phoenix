Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

Public Class FEEReminderExclude

    Dim vFRE_STU_ID As String
    Dim vFRE_BSU_ID As String
    Dim vFRE_STU_BSU_ID As String
    Dim vFRE_STU_ACD_ID As Integer
    Dim vFRE_FROMDT As DateTime
    Dim vFRE_TODT As DateTime
    Dim vFRE_bDELETE As Boolean
    Public bEdit As Boolean
    Public bDelete As Boolean

    'Public Property FRH_ID() As Integer
    '    Get
    '        Return vFRH_ID
    '    End Get
    '    Set(ByVal value As Integer)
    '        vFRH_ID = value
    '    End Set
    'End Property

  
    'Public Property FRE_STU_ID() As Int64
    '    Get
    '        Return vFRE_STU_ID
    '    End Get
    '    Set(ByVal value As Int64)
    '        vFRE_STU_ID = value
    '    End Set
    'End Property
    Public Property FRE_BSU_ID() As String
        Get
            Return vFRE_BSU_ID
        End Get
        Set(ByVal value As String)
            vFRE_BSU_ID = value
        End Set
    End Property

    Public Property FRE_STU_BSU_ID() As String
        Get
            Return vFRE_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            vFRE_STU_BSU_ID = value
        End Set
    End Property

    'Public Property FRH_ACY_DESCR() As String
    '    Get
    '        Return vFRH_ACY_DESCR
    '    End Get
    '    Set(ByVal value As String)
    '        vFRH_ACY_DESCR = value
    '    End Set
    'End Property

    'Public Property FRH_REMARKS() As String
    '    Get
    '        Return vFRH_REMARKS
    '    End Get
    '    Set(ByVal value As String)
    '        vFRH_REMARKS = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Gets or Sets the Academic Year ID 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRE_STU_ACD_ID() As Integer
        Get
            Return vFRE_STU_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFRE_STU_ACD_ID = value
        End Set
    End Property

    'Public Property FRH_Level() As ReminderType
    '    Get
    '        Return vFRH_Level
    '    End Get
    '    Set(ByVal value As ReminderType)
    '        vFRH_Level = value
    '    End Set
    'End Property



    ''' <summary>
    ''' Gets or Sets Date on which the Performa Invoice is saved
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FRE_FROMDT() As DateTime
        Get
            Return vFRE_FROMDT
        End Get
        Set(ByVal value As DateTime)
            vFRE_FROMDT = value
        End Set
    End Property

    Public Property FRE_TODT() As DateTime
        Get
            Return vFRE_TODT
        End Get
        Set(ByVal value As DateTime)
            vFRE_TODT = value
        End Set
    End Property
    Public Property FRE_bDELETE() As Boolean
        Get
            Return vFRE_bDELETE
        End Get
        Set(ByVal value As Boolean)
            vFRE_bDELETE = False
        End Set
    End Property
    Public Function Delete() As Boolean
        bDelete = True
    End Function

    Private Shared Function GetSection(ByVal vSTU_ID As String) As Integer
        Dim str_Sql As String = "SELECT STU_SCT_ID FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID =" & vSTU_ID
        Dim objSCT_ID As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)
        If objSCT_ID Is DBNull.Value Then
            Return ""
        Else
            Return Convert.ToInt32(objSCT_ID)
        End If
    End Function

    Public Shared Function SaveFEEReminderExclude(ByVal vSTU_IDs As Hashtable, ByVal vFEE_REM As FEEReminderExclude, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim bUpdateData As Boolean = False

        If vFEE_REM Is Nothing Then
            Return 1000
        End If
        Dim cmd As SqlCommand

        If vFEE_REM.bEdit = False AndAlso vFEE_REM.bDelete Then Return 0
        If vFEE_REM.bEdit AndAlso vFEE_REM.bDelete = False Then Return 0

        cmd = New SqlCommand("[FEES].[F_SAVEFEE_REMINDEREXCLUDE]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim str_xml As String = GetStudentDetailsXML(vSTU_IDs)

        Dim sqlpFRE_STU_ID As New SqlParameter("@FRE_STU_ID", SqlDbType.Xml)
        sqlpFRE_STU_ID.Value = str_xml
        cmd.Parameters.Add(sqlpFRE_STU_ID)

        Dim sqlpFRE_BSU_ID As New SqlParameter("@FRE_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFRE_BSU_ID.Value = vFEE_REM.FRE_BSU_ID
        cmd.Parameters.Add(sqlpFRE_BSU_ID)

        Dim sqlpFRE_STU_BSU_ID As New SqlParameter("@FRE_STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFRE_STU_BSU_ID.Value = vFEE_REM.FRE_STU_BSU_ID
        cmd.Parameters.Add(sqlpFRE_STU_BSU_ID)

        Dim sqlpFRE_STU_ACD_ID As New SqlParameter("@FRE_STU_ACD_ID", SqlDbType.Int)
        sqlpFRE_STU_ACD_ID.Value = vFEE_REM.FRE_STU_ACD_ID
        cmd.Parameters.Add(sqlpFRE_STU_ACD_ID)

        Dim sqlpFRE_FROMDT As New SqlParameter("@FRE_FROMDT", SqlDbType.DateTime)
        sqlpFRE_FROMDT.Value = vFEE_REM.vFRE_FROMDT
        cmd.Parameters.Add(sqlpFRE_FROMDT)

        Dim sqlpFRE_TODT As New SqlParameter("@FRE_TODT", SqlDbType.DateTime)
        sqlpFRE_TODT.Value = vFEE_REM.vFRE_TODT
        cmd.Parameters.Add(sqlpFRE_TODT)

        Dim sqlpFRE_bDELETE As New SqlParameter("@FRE_bDELETE", SqlDbType.TinyInt)
        sqlpFRE_bDELETE.Value = vFEE_REM.vFRE_bDELETE
        cmd.Parameters.Add(sqlpFRE_bDELETE)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_REM.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        'Dim sqlpNEW_FRH_ID As New SqlParameter("@NEW_FRH_ID", SqlDbType.Int)
        'sqlpNEW_FRH_ID.Direction = ParameterDirection.Output
        'cmd.Parameters.Add(sqlpNEW_FRH_ID)
        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        'If iReturnvalue = 0 Then
        '    NEW_FRH_ID = sqlpNEW_FRH_ID.Value
        'End If
        'If iReturnvalue = 0 Then iReturnvalue = SaveFeeReminderSubDetails(vSTU_IDs, vFEE_REM.FRH_ASONDATE, vFEE_REM.FRH_STU_BSU_ID, vFEE_REM.FRH_ACD_ID, sqlpNEW_FRH_ID.Value, vFEE_REM.FRH_Level, conn, trans)

        Return iReturnvalue
    End Function
    Public Shared Function GetStudentDetailsXML(ByVal vSTU_IDs As Hashtable) As String
        Dim xmlDoc As New XmlDocument
        Dim XMLSTU_ID As XmlElement
        Dim XMLSTU_DET As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        Dim iDictEnum As IDictionaryEnumerator = vSTU_IDs.GetEnumerator
        While (iDictEnum.MoveNext())
            XMLSTU_DET = xmlDoc.CreateElement("STU_DET")

            XMLSTU_ID = xmlDoc.CreateElement("STU_ID")
            XMLSTU_ID.InnerText = iDictEnum.Key
            XMLSTU_DET.AppendChild(XMLSTU_ID)

            'XMLbExclude = xmlDoc.CreateElement("bExclude")
            'XMLbExclude.InnerText = iDictEnum.Value
            'XMLSTU_DET.AppendChild(XMLbExclude)

            xmlDoc.DocumentElement.InsertBefore(XMLSTU_DET, xmlDoc.DocumentElement.LastChild)
        End While
        'drread.Close()
        'Return rem_sub
        Return xmlDoc.OuterXml
    End Function

End Class
