﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class clsAddVendor
#Region "Public Variables"

    Private pVDR_ID As Integer
    Public Property VDR_ID() As Integer
        Get
            Return pVDR_ID
        End Get
        Set(ByVal value As Integer)
            pVDR_ID = value
        End Set
    End Property

    Private pEMP_ID As Integer
    Public Property EMP_ID() As Integer
        Get
            Return pEMP_ID
        End Get
        Set(ByVal value As Integer)
            pEMP_ID = value
        End Set
    End Property

    Private pEMP_BSU_ID As String
    Public Property EMP_BSU_ID() As String
        Get
            Return pEMP_BSU_ID
        End Get
        Set(ByVal value As String)
            pEMP_BSU_ID = value
        End Set
    End Property

    Private pUser As String
    Public Property User() As String
        Get
            Return pUser
        End Get
        Set(ByVal value As String)
            pUser = value
        End Set
    End Property

    Private pVDB_BSU_ID As String
    Public Property VDB_BSU_ID() As String
        Get
            Return pVDB_BSU_ID
        End Get
        Set(ByVal value As String)
            pVDB_BSU_ID = value
        End Set
    End Property

    Private pVDR_REF_NO As String
    Public Property VDR_REF_NO() As String
        Get
            Return pVDR_REF_NO
        End Get
        Set(ByVal value As String)
            pVDR_REF_NO = value
        End Set
    End Property

    Private pEMP_NAME As String
    Public Property EMP_NAME() As String
        Get
            Return pEMP_NAME
        End Get
        Set(ByVal value As String)
            pEMP_NAME = value
        End Set
    End Property

    Private pVDR_CODE As String
    Public Property VDR_CODE() As String
        Get
            Return pVDR_CODE
        End Get
        Set(ByVal value As String)
            pVDR_CODE = value
        End Set
    End Property

    Private pEMP_NO As String
    Public Property EMP_NO() As String
        Get
            Return pEMP_NO
        End Get
        Set(ByVal value As String)
            pEMP_NO = value
        End Set
    End Property

    Private pDES_DESCR As String
    Public Property DESIGNATION() As String
        Get
            Return pDES_DESCR
        End Get
        Set(ByVal value As String)
            pDES_DESCR = value
        End Set
    End Property

    Private pBSU_NAME As String
    Public Property WORKING_UNIT() As String
        Get
            Return pBSU_NAME
        End Get
        Set(ByVal value As String)
            pBSU_NAME = value
        End Set
    End Property

    Private pDPT_DESCR As String
    Public Property DEPARTMENT() As String
        Get
            Return pDPT_DESCR
        End Get
        Set(ByVal value As String)
            pDPT_DESCR = value
        End Set
    End Property

    Private pEMP_IMAGE_URL As String
    Public Property EMP_IMAGE_URL() As String
        Get
            Return pEMP_IMAGE_URL
        End Get
        Set(ByVal value As String)
            pEMP_IMAGE_URL = value
        End Set
    End Property

    Private pBSU_SHORTNAME As String
    Public Property BSU_SHORTNAME() As String
        Get
            Return pBSU_SHORTNAME
        End Get
        Set(ByVal value As String)
            pBSU_SHORTNAME = value
        End Set
    End Property

    Private pbDELETE As Boolean = False
    Public Property bDELETE() As Boolean
        Get
            Return pbDELETE
        End Get
        Set(ByVal value As Boolean)
            pbDELETE = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Shared Function GetBusinessUnits(ByVal usrName As String, Optional ByVal showActiveOnly As Boolean = False) As DataSet
        Dim str_sql As String = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetBusinessUnits] ('" & usrName & "') "
        If showActiveOnly Then
            str_sql = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetBusinessUnits] ('" & usrName & "') WHERE ISNULL(BSU_bSHOW, 0) = 1 "
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function GET_BSU_SHORT_NAME(ByVal BSU_ID As String) As String
        Dim str_sql As String = "SELECT ISNULL(BSU_SHORTNAME ,'') BSU_SHORTNAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & BSU_ID & "' "
        
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql).ToString
    End Function
    Public Function GET_VENDOR_DETAILSBYID() As Boolean
        GET_VENDOR_DETAILSBYID = False
        Try
            Dim ds As New DataSet
            Dim Qry As String = "SELECT VDR_ID , BSU_SHORTNAME , VDR_REF_NO , EMP_NAME , DES_DESCR , VDR_CODE , VDB_BSU_ID , VDR_LOGDT , EMP_FNAME, EMP_ID, EMP_BSU_ID FROM DAX.VW_VENDOR_M WHERE VDR_ID = @VDR_ID "
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@VDR_ID", SqlDbType.Int)
            pParms(0).Value = VDR_ID
            ds = SqlHelper.ExecuteDataset(ConnectionManger.OASIS_DAX_ConnectionString, CommandType.Text, Qry, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                VDB_BSU_ID = ds.Tables(0).Rows(0)("VDB_BSU_ID").ToString
                VDR_REF_NO = ds.Tables(0).Rows(0)("VDR_REF_NO").ToString
                EMP_NAME = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                VDR_CODE = ds.Tables(0).Rows(0)("VDR_CODE").ToString
                EMP_ID = ds.Tables(0).Rows(0)("EMP_ID").ToString
                EMP_BSU_ID = ds.Tables(0).Rows(0)("EMP_BSU_ID").ToString
                GET_VENDOR_DETAILSBYID = True
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Function GET_VENDOR_CODES_FOR_EMPLOYEE() As DataTable
        GET_VENDOR_CODES_FOR_EMPLOYEE = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As String = "SELECT VDR_ID , BSU_SHORTNAME , VDR_REF_NO , EMP_NAME , DES_DESCR , VDR_CODE , VDB_BSU_ID , VDR_LOGDT , EMP_FNAME, EMP_ID FROM DAX.VW_VENDOR_M WHERE EMP_ID = @EMP_ID "
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(0).Value = EMP_ID
            ds = SqlHelper.ExecuteDataset(ConnectionManger.OASIS_DAX_ConnectionString, CommandType.Text, Qry, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                GET_VENDOR_CODES_FOR_EMPLOYEE = ds.Tables(0)
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetEmployee(ByVal BSUID As String, ByVal prefix As String) As String()
        Dim employee As New List(Of String)()
        Dim Qry As String = "SELECT TOP 15 EMP_ID,EMPNO,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME, E.DES_DESCR, C.BSU_NAME " & _
                            "FROM dbo.EMPLOYEE_M AS D WITH(NOLOCK) INNER JOIN OASIS.dbo.EMPDESIGNATION_M AS E WITH ( NOLOCK ) ON E.DES_ID = D.EMP_DES_ID " & _
                            "INNER JOIN OASIS.dbo.BUSINESSUNIT_M AS C WITH ( NOLOCK ) ON C.BSU_ID = D.EMP_BSU_ID WHERE EMP_BSU_ID= @BSUID  AND ISNULL(EMP_bACTIVE,0)=1 "

        If IsNumeric(Trim(prefix)) Then
            Qry = Qry & " AND (EMPNO LIKE '%' + @prefix + '%') ORDER BY EMPNO"
        Else
            Qry = Qry & " AND (ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') LIKE '%' + @prefix + '%') ORDER BY ISNULL(EMP_FNAME, '')"
        End If
        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = Qry
                cmd.Parameters.AddWithValue("@BSUID", BSUID)
                cmd.Parameters.AddWithValue("@prefix", Trim(prefix))

                cmd.Connection = conn
                conn.Open()
                Dim sql As String = UtilityObj.CommandAsSql(cmd)
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        employee.Add(String.Format("{0}-{1}-{2}", sdr("EMP_NAME"), sdr("EMPNO"), sdr("EMP_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return employee.ToArray()
        End Using
    End Function

    Public Function GET_EMPLOYEE_DETAILS() As Boolean
        GET_EMPLOYEE_DETAILS = False
        Try
            Dim ds As New DataSet
            Dim Qry As String = "SELECT EMP_ID,EMPNO,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME, " & _
                            "E.DES_DESCR, C.BSU_NAME, C.BSU_SHORTNAME, A.DPT_DESCR, ISNULL(EMD_PHOTO,'') EMD_PHOTO " & _
                            "FROM dbo.EMPLOYEE_M AS D WITH(NOLOCK) INNER JOIN dbo.EMPDESIGNATION_M AS E WITH ( NOLOCK ) ON E.DES_ID = D.EMP_DES_ID " & _
                            "INNER JOIN dbo.BUSINESSUNIT_M AS C WITH ( NOLOCK ) ON C.BSU_ID = D.EMP_BSU_ID " & _
                            "INNER JOIN dbo.DEPARTMENT_M AS A WITH ( NOLOCK ) ON D.EMP_DPT_ID = A.DPT_ID " & _
                            "LEFT JOIN dbo.EMPLOYEE_D AS F WITH(NOLOCK) ON F.EMD_EMP_ID = D.EMP_ID " & _
                            "WHERE EMP_BSU_ID= @BSU_ID  AND EMP_ID=@EMP_ID "
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms(0).Value = EMP_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(1).Value = EMP_ID
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                EMP_NO = ds.Tables(0).Rows(0)("EMPNO").ToString
                EMP_NAME = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                DESIGNATION = ds.Tables(0).Rows(0)("DES_DESCR").ToString
                WORKING_UNIT = ds.Tables(0).Rows(0)("BSU_NAME").ToString
                DEPARTMENT = ds.Tables(0).Rows(0)("DPT_DESCR").ToString
                BSU_SHORTNAME = ds.Tables(0).Rows(0)("BSU_SHORTNAME").ToString
                Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & "/" & ds.Tables(0).Rows(0)("EMD_PHOTO").ToString
                EMP_IMAGE_URL = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                GET_EMPLOYEE_DETAILS = True
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Function SAVE_VENDOR(ByRef RetMessage As String) As Boolean
        SAVE_VENDOR = False
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.OASIS_DAX_ConnectionString)
        Try
            Dim trans As SqlTransaction
            conn.Open()
            trans = conn.BeginTransaction("VENDOR")
            Dim cmd As SqlCommand
            Dim RetVal As Integer = 0

            cmd = New SqlCommand("DAX.SAVE_VENDOR", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpVDR_CODE As New SqlParameter("@VDR_CODE", SqlDbType.VarChar, 20)
            sqlpVDR_CODE.Value = VDR_CODE
            cmd.Parameters.Add(sqlpVDR_CODE)

            Dim sqlpVDR_NAME As New SqlParameter("@VDR_NAME", SqlDbType.VarChar, 200)
            sqlpVDR_NAME.Value = EMP_NAME
            cmd.Parameters.Add(sqlpVDR_NAME)

            Dim sqlpVDR_REF_NO As New SqlParameter("@VDR_REF_NO", SqlDbType.VarChar, 20)
            sqlpVDR_REF_NO.Value = VDR_REF_NO
            cmd.Parameters.Add(sqlpVDR_REF_NO)

            Dim sqlpBSU_SHORTNAME As New SqlParameter("@BSU_SHORTNAME", SqlDbType.VarChar, 20)
            sqlpBSU_SHORTNAME.Value = BSU_SHORTNAME
            cmd.Parameters.Add(sqlpBSU_SHORTNAME)

            Dim sqlpVDR_ID As New SqlParameter("@VDR_ID", SqlDbType.Int)
            sqlpVDR_ID.Direction = ParameterDirection.InputOutput
            sqlpVDR_ID.Value = VDR_ID
            cmd.Parameters.Add(sqlpVDR_ID)

            Dim sqlpUser As New SqlParameter("@USER", SqlDbType.VarChar, 50)
            sqlpUser.Value = User
            cmd.Parameters.Add(sqlpUser)

            Dim sqlpDELETE As New SqlParameter("@bDELETE", SqlDbType.Bit)
            sqlpDELETE.Value = bDELETE
            cmd.Parameters.Add(sqlpDELETE)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            RetVal = retSValParam.Value

            If RetVal <> 0 Then
                trans.Rollback()
                RetMessage = UtilityObj.getErrorMessage(RetVal)
            Else
                trans.Commit()
                VDR_ID = sqlpVDR_ID.Value
                SAVE_VENDOR = True
                RetMessage = ""
            End If


        Catch ex As Exception
            RetMessage = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

#End Region
End Class
