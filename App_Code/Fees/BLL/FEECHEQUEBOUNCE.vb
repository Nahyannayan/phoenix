Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class FEECHEQUEBOUNCE

    Private Shared Function GetChequeBounceDetails(ByVal FCQ_ID As Integer, ByVal trans As SqlTransaction) As SqlDataReader
        Dim str_sql As String = "SELECT VHH_DOCNO FCR_RECNO, FCQ_DATE FCR_DATE, " & _
        " FCL_BSU_ID FCR_BSU_ID, FCL_ACD_ID FCR_ACD_ID, FCL_STU_TYPE FCR_STU_TYPE ," & _
        " FCL_STU_ID FCR_STU_ID, FCL_GRM_ID FCR_GRM_ID, FCL_AMOUNT FCR_AMOUNT, " & _
        " FCL_SCT_ID FCR_SCT_ID, FCL_STU_BSU_ID FCR_STU_BSU_ID , FCQ_ID FCR_FCQ_ID, " & _
        " FCQ_AMOUNT,VHH_ACT_ID FROM FEES.vw_OSA_CHEQUEBOUNCELIST WHERE FCQ_ID = " & FCQ_ID
        Return SqlHelper.ExecuteReader(trans, CommandType.Text, str_sql)
    End Function

    Public Shared Function GetBouncedChequeDetails(ByVal vFCR_ID As Integer, ByVal str_conn As String) As SqlDataReader
        Dim str_sql As String = "SELECT  * FROM [FEES].[vw_OSA_BOUNCEDCHQDETAILS] WHERE FCR_ID = " & vFCR_ID
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
    End Function

    Public Shared Function GetBouncedChequesFeeDetails(ByVal vFCR_ID As Integer, ByVal str_conn As String) As SqlDataReader
        Dim str_sql As String = "SELECT OASIS.FEES.FEECHEQUERETURN_D.FRD_FEE_ID FEE_ID, OASIS.FEES.FEES_M.FEE_DESCR,  OASIS.FEES.FEECHEQUERETURN_D.FRD_AMOUNT AMOUNT" & _
        " FROM OASIS.FEES.FEECHEQUERETURN_D INNER JOIN OASIS.FEES.FEES_M " & _
        " ON OASIS.FEES.FEECHEQUERETURN_D.FRD_FEE_ID = OASIS.FEES.FEES_M.FEE_ID" & _
        " WHERE FRD_FCR_ID = " & vFCR_ID
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
    End Function

    Public Shared Function F_SAVEFEECHEQUERETURN_H(ByVal p_FCR_ID As Long, ByVal p_FCR_DATE As String, _
        ByVal p_FCR_BSU_ID As String, ByVal p_FCR_STU_TYPE As String, ByVal p_FCR_STU_ID As String, _
        ByVal p_FCR_BNK_CHARG As Decimal, ByVal p_FCR_NARRATION As String, ByVal p_FCR_Bposted As Boolean, _
        ByVal p_FCR_STU_BSU_ID As String, ByVal p_FCR_FCQ_ID As String, ByRef p_NEW_FCR_ID As String, _
        ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(12) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_SAVEFEECHEQUERETURN_H]
        '@FCR_ID = 0, 
        '@FCR_DATE = N'14/OCT/2009',
        '@FCR_BSU_ID = N'123004', 
        '@FCR_STU_TYPE =  'S',
        '@FCR_STU_ID = 95536,       
        pParms(0) = New SqlClient.SqlParameter("@FCR_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCR_ID
        pParms(1) = New SqlClient.SqlParameter("@FCR_DATE", SqlDbType.DateTime)
        pParms(1).Value = p_FCR_DATE
        pParms(2) = New SqlClient.SqlParameter("@FCR_BSU_ID", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCR_BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@FCR_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(3).Value = p_FCR_STU_TYPE
        pParms(4) = New SqlClient.SqlParameter("@FCR_STU_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCR_STU_ID
        '@FCR_BNK_CHARG = 18,		 
        '@FCR_Bposted = 0,
        '@FCR_NARRATION = N'1313',
        '@FCR_STU_BSU_ID = N'123004',        
        pParms(5) = New SqlClient.SqlParameter("@FCR_BNK_CHARG", SqlDbType.Decimal, 21)
        pParms(5).Value = p_FCR_BNK_CHARG
        pParms(6) = New SqlClient.SqlParameter("@FCR_Bposted", SqlDbType.Bit)
        pParms(6).Value = p_FCR_Bposted
        pParms(7) = New SqlClient.SqlParameter("@FCR_NARRATION", SqlDbType.VarChar)
        pParms(7).Value = p_FCR_NARRATION
        pParms(8) = New SqlClient.SqlParameter("@FCR_STU_BSU_ID", SqlDbType.VarChar)
        pParms(8).Value = p_FCR_STU_BSU_ID
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        '@FCR_FCQ_ID = 47743,
        '@NEW_FCR_ID = @NEW_FCR_ID OUTPUT 
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCR_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCR_FCQ_ID", SqlDbType.BigInt)
        pParms(11).Value = p_FCR_FCQ_ID

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SAVEFEECHEQUERETURN_H", pParms)
        If pParms(9).Value = 0 Then
            p_NEW_FCR_ID = pParms(10).Value
        End If
        F_SAVEFEECHEQUERETURN_H = pParms(9).Value
    End Function

End Class
