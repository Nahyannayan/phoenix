﻿Imports Microsoft.VisualBasic

Public Class clsDirecPay

    Public Shared Function getQueryResponse(ByVal formParams As String) As QueryResponse

        Dim respQueryDR As QueryResponse = New QueryResponse()
        'respQueryDR.ReferenceID = Convert.ToInt64(strRespParams(0))
        'respQueryDR.MerchantOrderNumber = strRespParams(1)

        Dim ResultValues As String() = formParams.Split(New String() {"||"}, StringSplitOptions.None)

        Dim index As Integer = 0
        'If ResultValues.Length <= 2 Then
        '    index = 4
        'End If

        Dim valid As String = ResultValues(0)
        For Each values As String In ResultValues

            If Not (values = "Null") Then
                If (values.Contains("FAILURE")) Then
                    values = "0000|" + values
                End If
                Dim subVal As String() = values.Split("|")
                Dim subIndex As Integer = 0

                For Each val As String In subVal
                    If Not val = "Null" Then
                        Select Case index
                            Case 1
                                If (valid(index - 1) = "1") Then
                                    Select Case subIndex
                                        Case 1
                                            respQueryDR.ReferenceID = Convert.ToInt64(val)
                                        Case 2
                                            respQueryDR.MerchantOrderNumber = val
                                    End Select
                                Else
                                    index = index + 1
                                End If

                            Case 2
                                If (valid(index - 1) = "1") Then
                                    Select Case subIndex
                                        Case 1
                                            respQueryDR.AmountBlock().Amount = Convert.ToDecimal(val)
                                        Case 2
                                            respQueryDR.AmountBlock().Currency = val
                                        Case 3
                                            respQueryDR.AmountBlock().Country = val

                                    End Select
                                Else
                                    index = index + 1
                                End If

                            Case 3
                                If (valid(index - 1) = "1") Then

                                    Select Case subIndex
                                        Case 1
                                            respQueryDR.TransactionResponseStatus().Querydescription = val
                                        Case 2
                                            respQueryDR.TransactionResponseStatus().StatusFlag = val
                                        Case 3
                                            respQueryDR.TransactionResponseStatus().ReasonCode = val
                                        Case 4
                                            respQueryDR.TransactionResponseStatus().ReasonDescription = val
                                    End Select
                                Else
                                    index = index + 1
                                End If

                            Case 4
                                If (valid(index - 1) = "1") Then
                                    Select Case subIndex
                                        Case 1
                                            respQueryDR.RefundDetails().RefundReferenceNumber = val
                                        Case 2
                                            respQueryDR.RefundDetails().Amount = Convert.ToDecimal(val)
                                        Case 3
                                            respQueryDR.RefundDetails().AmountAvailableForRefund = Convert.ToDecimal(val)
                                    End Select
                                Else
                                    index = index + 1
                                End If
                            Case 5
                                If (valid(index - 1) = "1") Then
                                    Select Case subIndex
                                        Case 1
                                            respQueryDR.AdditionalTransactionDetails().GatewayName = val
                                        Case 2
                                            respQueryDR.AdditionalTransactionDetails().GatewayIdentifier = val
                                        Case 3
                                            respQueryDR.AdditionalTransactionDetails().Paymode = val
                                        Case 4
                                            respQueryDR.AdditionalTransactionDetails().CardType = val
                                    End Select
                                End If
                            Case Else
                                Exit Select
                        End Select
                    Else
                        respQueryDR.TransactionResponseStatus() = Nothing
                    End If

                    subIndex = subIndex + 1
                Next
                'End If

            End If
            index = index + 1
        Next
        Return respQueryDR
    End Function
    Public Shared Function configureRequest(ByVal QueryRequest As QueryRequest) As String

        Dim fieldFlag As String = ""
        Dim paramFlag As String = ""
        Dim paramString As String = ""
        Dim requestString As String = "" '1110100||
        Dim paramObj As QueryRequest = QueryRequest
        If Not paramObj.TransactionDetails() Is Nothing Then
            fieldFlag = fieldFlag + "1"
            If Not paramObj.TransactionDetails().ReferenceID = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + Convert.ToString(paramObj.TransactionDetails().ReferenceID)
            Else
                paramFlag = paramFlag + "0"
            End If
            If Not paramObj.TransactionDetails().MerchantOrderNumber = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + paramObj.TransactionDetails().MerchantOrderNumber
            Else
                paramFlag = paramFlag + "0"
            End If
            If Not paramObj.TransactionDetails().TransactionType = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + paramObj.TransactionDetails().TransactionType
            Else
                paramFlag = paramFlag + "0"
            End If
            If Not paramObj.TransactionDetails().AdditionalDetails = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + paramObj.TransactionDetails().AdditionalDetails
            Else
                paramFlag = paramFlag + "0"
            End If

            requestString = paramFlag + paramString
        Else
            fieldFlag = fieldFlag + "0"
        End If
        Return fieldFlag + "||" + requestString
    End Function
End Class
Public Class QueryRequest
    Public Property MerchantID As Long
    Public Property CollaboratorID As String
    Public Property TransactionDetails As New TransactionDetails
End Class
Public Class TransactionDetails
    Public Property ReferenceID As Long
    Public Property MerchantOrderNumber As String
    Public Property TransactionType As String
    Public Property AdditionalDetails As String

End Class
Public Class QueryResponse
    Public Property ReferenceID As Long
    Public Property MerchantOrderNumber As String
    Public Property AmountBlock As New AmountBlock
    Public Property TransactionResponseStatus As New TransactionResponseStatus
    Public Property RefundDetails As New RefundDetails
    Public Property AdditionalTransactionDetails As New AdditionalTransactionDetails

End Class
Public Class AmountBlock
    Public Property Amount As Decimal
    Public Property Currency As String
    Public Property Country As String
End Class
Public Class TransactionResponseStatus
    Public Property Querydescription As String
    Public Property StatusFlag As String
    Public Property ReasonCode As String
    Public Property ReasonDescription As String
End Class
Public Class RefundDetails
    Public Property RefundReferenceNumber As String
    Public Property Amount As Decimal
    Public Property AmountAvailableForRefund As Decimal
End Class
Public Class AdditionalTransactionDetails
    Public Property GatewayName As String
    Public Property GatewayIdentifier As String
    Public Property Paymode As String
    Public Property CardType As String
End Class