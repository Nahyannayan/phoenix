﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Web
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Public Class FeePaymentPlanner
#Region "PublicProperties1"
    Private pBSU_ID As String
    Public Property BSU_ID() As String
        Get
            Return pBSU_ID
        End Get
        Set(ByVal value As String)
            pBSU_ID = value
        End Set
    End Property
    Private pACD_ID As Integer
    Public Property ACD_ID() As Integer
        Get
            Return pACD_ID
        End Get
        Set(ByVal value As Integer)
            pACD_ID = value
        End Set
    End Property
    Private pSTU_ID As Integer
    Public Property STU_ID() As Integer
        Get
            Return pSTU_ID
        End Get
        Set(ByVal value As Integer)
            pSTU_ID = value
        End Set
    End Property
    Private pSTU_TYPE As String
    Public Property STU_TYPE() As String
        Get
            Return pSTU_TYPE
        End Get
        Set(ByVal value As String)
            pSTU_TYPE = value
        End Set
    End Property
    Private pDOCDATE As String
    Public Property DOCDATE() As String
        Get
            Return pDOCDATE
        End Get
        Set(ByVal value As String)
            pDOCDATE = value
        End Set
    End Property
    Private pASONDATE As String
    Public Property ASONDATE() As String
        Get
            Return pASONDATE
        End Get
        Set(ByVal value As String)
            pASONDATE = value
        End Set
    End Property
    Private pFEE_IDs As String
    Public Property FEE_IDs() As String
        Get
            Return pFEE_IDs
        End Get
        Set(ByVal value As String)
            pFEE_IDs = value
        End Set
    End Property
    Private pUser As String
    Public Property User() As String
        Get
            Return pUser
        End Get
        Set(ByVal value As String)
            pUser = value
        End Set
    End Property
    Private pComments As String
    Public Property Comments() As String
        Get
            Return pComments
        End Get
        Set(ByVal value As String)
            pComments = value
        End Set
    End Property
    Private pCheckFeeCollection As Boolean = False
    Public Property CheckFeeCollection() As Boolean
        Get
            Return pCheckFeeCollection
        End Get
        Set(ByVal value As Boolean)
            pCheckFeeCollection = value
        End Set
    End Property

    Private pStatus As Integer
    Public Property Status() As Integer
        Get
            Return pStatus
        End Get
        Set(ByVal value As Integer)
            pStatus = value
        End Set
    End Property
    Private pPrincipalStatus As String
    Public Property PrincipalStatus() As String
        Get
            Return pPrincipalStatus
        End Get
        Set(ByVal value As String)
            pPrincipalStatus = value
        End Set
    End Property
    Private pIsAmountEditable As Integer
    Public Property IsAmountEditable() As Integer
        Get
            Return pIsAmountEditable
        End Get
        Set(ByVal value As Integer)
            pIsAmountEditable = value
        End Set
    End Property
    Private pIsPrincipalApprove As Integer
    Public Property IsPrincipalApprove() As Integer
        Get
            Return pIsPrincipalApprove
        End Get
        Set(ByVal value As Integer)
            pIsPrincipalApprove = value
        End Set
    End Property
    Private pFinanceComments As String
    Public Property FinanceComments() As String
        Get
            Return pFinanceComments
        End Get
        Set(ByVal value As String)
            pFinanceComments = value
        End Set
    End Property
    Private pPincipalComments As String
    Public Property PrincipalComments() As String
        Get
            Return pPincipalComments
        End Get
        Set(ByVal value As String)
            pPincipalComments = value
        End Set
    End Property
    Private pAging_XML As String
    Public Property Aging_XML() As String
        Get
            Return pAging_XML
        End Get
        Set(ByVal value As String)
            pAging_XML = value
        End Set
    End Property
    Private pBkt1 As String
    Public Property Bkt1() As String
        Get
            Return pBkt1
        End Get
        Set(ByVal value As String)
            pBkt1 = value
        End Set
    End Property
    Private pBkt2 As String
    Public Property Bkt2() As String
        Get
            Return pBkt2
        End Get
        Set(ByVal value As String)
            pBkt2 = value
        End Set
    End Property
    Private pBkt3 As String
    Public Property Bkt3() As String
        Get
            Return pBkt3
        End Get
        Set(ByVal value As String)
            pBkt3 = value
        End Set
    End Property
    Private pBkt4 As String
    Public Property Bkt4() As String
        Get
            Return pBkt4
        End Get
        Set(ByVal value As String)
            pBkt4 = value
        End Set
    End Property
    Private pFPP_ID As Integer
    Public Property FPP_ID() As Integer
        Get
            Return pFPP_ID
        End Get
        Set(ByVal value As Integer)
            pFPP_ID = value
        End Set
    End Property
    Private pFPP_GRD_ID As String
    Public Property FPP_GRD_ID() As String
        Get
            Return pFPP_GRD_ID
        End Get
        Set(ByVal value As String)
            pFPP_GRD_ID = value
        End Set
    End Property

    Private pContactName As String
    Public Property ContactName() As String
        Get
            Return pContactName
        End Get
        Set(ByVal value As String)
            pContactName = value
        End Set
    End Property
    Private pFPP_APPROVED As String
    Public Property FPP_APPROVED() As String
        Get
            Return pFPP_APPROVED
        End Get
        Set(ByVal value As String)
            pFPP_APPROVED = value
        End Set
    End Property

    Private pFPP_PLAN_TYPE As String
    Public Property FPP_PLAN_TYPE() As String
        Get
            Return pFPP_PLAN_TYPE
        End Get
        Set(ByVal value As String)
            pFPP_PLAN_TYPE = value
        End Set
    End Property

    Private pFPP_REQ_MODE_OF_PAY As String
    Public Property FPP_REQ_MODE_OF_PAY() As String
        Get
            Return pFPP_REQ_MODE_OF_PAY
        End Get
        Set(ByVal value As String)
            pFPP_REQ_MODE_OF_PAY = value
        End Set
    End Property

    Private pEmail As String
    Public Property Email() As String
        Get
            Return pEmail
        End Get
        Set(ByVal value As String)
            pEmail = value
        End Set
    End Property

    Private pMobileNo As String
    Public Property MobileNo() As String
        Get
            Return pMobileNo
        End Get
        Set(ByVal value As String)
            pMobileNo = value
        End Set
    End Property
    Private pResidenceNo As String
    Public Property ResidenceNo() As String
        Get
            Return pResidenceNo
        End Get
        Set(ByVal value As String)
            pResidenceNo = value
        End Set
    End Property
#End Region
    Public Function getFeeTypes() As DataTable

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.Text, "SELECT FEE_ID,FEE_DESCR FROM dbo.vFEETYPES_PAYPLAN")
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Function getCollectionTypes() As DataTable

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.Text, "SELECT CLT_ID, CLT_DESCR, CLT_BREQBANKREC, CLT_ORDER FROM COLLECTIONTYP_M WHERE CLT_ID IN (1,2)")
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Function GetStudentFeeAging() As DataTable
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ASONDATE", SqlDbType.VarChar)
        pParms(0).Value = pASONDATE
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = pSTU_ID
        pParms(2) = New SqlClient.SqlParameter("@FEE_IDs", SqlDbType.VarChar)
        pParms(2).Value = pFEE_IDs
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = pBSU_ID
        pParms(4) = New SqlClient.SqlParameter("@Bkt1", SqlDbType.VarChar, 5)
        pParms(4).Value = pBkt1
        pParms(5) = New SqlClient.SqlParameter("@Bkt2", SqlDbType.VarChar, 5)
        pParms(5).Value = pBkt2
        pParms(6) = New SqlClient.SqlParameter("@Bkt3", SqlDbType.VarChar, 5)
        pParms(6).Value = pBkt3
        pParms(7) = New SqlClient.SqlParameter("@Bkt4", SqlDbType.VarChar, 5)
        pParms(7).Value = pBkt4

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.STUDENT_FEE_AGING_PAYPLAN", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function SetStudentFeeAging() As Integer
        SetStudentFeeAging = 0
        Try
            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Try
                Dim cmd As SqlCommand

                Dim RetFlag As Boolean = False

                cmd = New SqlCommand("FEES.SET_STUDENT_FEE_AGING_PAYPLAN", sqlConn, sqlTrans)
                cmd.CommandType = CommandType.StoredProcedure


                Dim sqlpFPP_ASONDATE As New SqlParameter("@ASONDATE", SqlDbType.VarChar, 100)
                sqlpFPP_ASONDATE.Value = ASONDATE
                cmd.Parameters.Add(sqlpFPP_ASONDATE)
                Dim sqlpFPP_STU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
                sqlpFPP_STU_ID.Value = STU_ID
                cmd.Parameters.Add(sqlpFPP_STU_ID)
                Dim sqlpFPP_FEE_ID As New SqlParameter("@FEE_IDs", SqlDbType.VarChar, 500)
                sqlpFPP_FEE_ID.Value = FEE_IDs
                cmd.Parameters.Add(sqlpFPP_FEE_ID)
                Dim sqlpFPP_BSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                sqlpFPP_BSU_ID.Value = BSU_ID
                cmd.Parameters.Add(sqlpFPP_BSU_ID)
                Dim sqlpFPP_pBkt1 As New SqlParameter("@Bkt1", SqlDbType.VarChar)
                sqlpFPP_pBkt1.Value = pBkt1
                cmd.Parameters.Add(sqlpFPP_pBkt1)
                Dim sqlpFPP_pBkt2 As New SqlParameter("@Bkt2", SqlDbType.VarChar)
                sqlpFPP_pBkt2.Value = pBkt2
                cmd.Parameters.Add(sqlpFPP_pBkt2)
                Dim sqlpFPP_pBkt3 As New SqlParameter("@Bkt3", SqlDbType.VarChar)
                sqlpFPP_pBkt3.Value = pBkt3
                cmd.Parameters.Add(sqlpFPP_pBkt3)
                Dim sqlpFPP_pBkt4 As New SqlParameter("@Bkt4", SqlDbType.VarChar)
                sqlpFPP_pBkt4.Value = pBkt4
                cmd.Parameters.Add(sqlpFPP_pBkt4)

                'Dim pParms(8) As SqlClient.SqlParameter
                'pParms(0) = New SqlClient.SqlParameter("@ASONDATE", SqlDbType.VarChar)
                'pParms(0).Value = pASONDATE
                'pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
                'pParms(1).Value = pSTU_ID
                'pParms(2) = New SqlClient.SqlParameter("@FEE_IDs", SqlDbType.VarChar)
                'pParms(2).Value = pFEE_IDs
                'pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                'pParms(3).Value = pBSU_ID
                'pParms(4) = New SqlClient.SqlParameter("@Bkt1", SqlDbType.VarChar, 5)
                'pParms(4).Value = pBkt1
                'pParms(5) = New SqlClient.SqlParameter("@Bkt2", SqlDbType.VarChar, 5)
                'pParms(5).Value = pBkt2
                'pParms(6) = New SqlClient.SqlParameter("@Bkt3", SqlDbType.VarChar, 5)
                'pParms(6).Value = pBkt3
                'pParms(7) = New SqlClient.SqlParameter("@Bkt4", SqlDbType.VarChar, 5)
                'pParms(7).Value = pBkt4
                Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                retSValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retSValParam)

                'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
                '  CommandType.StoredProcedure, "FEES.SET_STUDENT_FEE_AGING_PAYPLAN", pParms)
                'If Not dsData Is Nothing Then
                '    Return dsData.Tables(0)
                'Else
                '    Return Nothing
                'End If
                cmd.CommandTimeout = 0
                cmd.ExecuteNonQuery()
                SetStudentFeeAging = retSValParam.Value

                If SetStudentFeeAging = 0 Then
                    sqlTrans.Commit()
                Else
                    sqlTrans.Rollback()
                End If

            Catch ex As Exception
                SetStudentFeeAging = 1
                sqlTrans.Rollback()
            Finally
                If sqlConn.State = ConnectionState.Open Then
                    sqlConn.Close()
                End If
            End Try
        Catch ex As Exception
            SetStudentFeeAging = 1
        End Try
    End Function

    Public Function CreateSchema() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ID", Type.GetType("System.Int32"))
        dt.Columns.Add("CLT_ID", Type.GetType("System.Int32"))
        dt.Columns.Add("PAYMODE", Type.GetType("System.String"))
        dt.Columns.Add("PAY_DATE", Type.GetType("System.String"))
        dt.Columns.Add("AMOUNT", Type.GetType("System.Double"))
        dt.Columns.Add("CHQNO", Type.GetType("System.String"))
        dt.Columns.Add("CHQDT", Type.GetType("System.String"))
        dt.Columns.Add("BANK_ID", Type.GetType("System.String"))
        dt.Columns.Add("CRR_ID", Type.GetType("System.String"))
        dt.Columns.Add("BANK", Type.GetType("System.String"))
        dt.Columns.Add("COMMENTS", Type.GetType("System.String"))


        CreateSchema = dt
    End Function

    Public Function SAVE_FEE_PAYMENT_PLAN_H(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        SAVE_FEE_PAYMENT_PLAN_H = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.SAVE_FEE_PAYMENTPLAN_H", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFPP_BSU_ID As New SqlParameter("@FPP_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFPP_BSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpFPP_BSU_ID)
            Dim sqlpFPP_ACD_ID As New SqlParameter("@FPP_ACD_ID", SqlDbType.VarChar, 20)
            sqlpFPP_ACD_ID.Value = ACD_ID
            cmd.Parameters.Add(sqlpFPP_ACD_ID)
            Dim sqlpFPP_STU_ID As New SqlParameter("@FPP_STU_ID", SqlDbType.VarChar, 20)
            sqlpFPP_STU_ID.Value = STU_ID
            cmd.Parameters.Add(sqlpFPP_STU_ID)

            Dim sqlpFPP_STU_TYPE As New SqlParameter("@FPP_STU_TYPE", SqlDbType.VarChar, 20)
            sqlpFPP_STU_TYPE.Value = STU_TYPE
            cmd.Parameters.Add(sqlpFPP_STU_TYPE)

            Dim sqlpFPP_FEE_IDs As New SqlParameter("@FPP_FEE_IDs", SqlDbType.VarChar)
            sqlpFPP_FEE_IDs.Value = FEE_IDs
            cmd.Parameters.Add(sqlpFPP_FEE_IDs)

            Dim sqlpFPP_DATE As New SqlParameter("@FPP_DATE", SqlDbType.VarChar)
            sqlpFPP_DATE.Value = DOCDATE
            cmd.Parameters.Add(sqlpFPP_DATE)

            Dim sqlpFPP_STU_AGING As New SqlParameter("@FPP_STU_AGING", SqlDbType.Xml)
            sqlpFPP_STU_AGING.Value = Aging_XML
            cmd.Parameters.Add(sqlpFPP_STU_AGING)

            Dim sqlpFPP_USER As New SqlParameter("@FPP_USER", SqlDbType.VarChar, 20)
            sqlpFPP_USER.Value = User
            cmd.Parameters.Add(sqlpFPP_USER)

            Dim sqlpFPP_COMMENTS As New SqlParameter("@FPP_COMMENTS", SqlDbType.VarChar, 500)
            sqlpFPP_COMMENTS.Value = Comments
            cmd.Parameters.Add(sqlpFPP_COMMENTS)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            Dim sqlpFPP_ID As New SqlParameter("@FPP_ID", SqlDbType.Int)
            sqlpFPP_ID.Direction = ParameterDirection.InputOutput
            sqlpFPP_ID.Value = FPP_ID
            cmd.Parameters.Add(sqlpFPP_ID)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            FPP_ID = sqlpFPP_ID.Value
            SAVE_FEE_PAYMENT_PLAN_H = retSValParam.Value


        Catch ex As Exception
            SAVE_FEE_PAYMENT_PLAN_H = 1000
            'TRANS.Rollback()
        End Try
    End Function

    Public Function APPROVE_FEE_PAYMENT_PLAN_H(ByVal Options As Integer, ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        APPROVE_FEE_PAYMENT_PLAN_H = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.APPROVE_FEE_PAYMENT_PLAN_H", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFPP_OPTIONS As New SqlParameter("@OPTIONS", SqlDbType.Int)
            sqlpFPP_OPTIONS.Value = Options
            cmd.Parameters.Add(sqlpFPP_OPTIONS)

            Dim sqlpFPP_BSU_ID As New SqlParameter("@FPP_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFPP_BSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpFPP_BSU_ID)

            Dim sqlpFPP_FPP_ID As New SqlParameter("@FPP_ID", SqlDbType.Int)
            sqlpFPP_FPP_ID.Value = FPP_ID
            cmd.Parameters.Add(sqlpFPP_FPP_ID)

            Dim sqlpFPP_USER As New SqlParameter("@FPP_USER", SqlDbType.VarChar, 20)
            sqlpFPP_USER.Value = User
            cmd.Parameters.Add(sqlpFPP_USER)

            Dim sqlpFPP_COMMENTS As New SqlParameter("@FPP_COMMENTS", SqlDbType.VarChar, 500)
            sqlpFPP_COMMENTS.Value = Comments
            cmd.Parameters.Add(sqlpFPP_COMMENTS)

            Dim Encr_decrData As New Encryption64
            Dim sqlpFPP_ENCR_FPP_ID As New SqlParameter("@FPP_REFERENCE1", SqlDbType.VarChar, 500)
            sqlpFPP_ENCR_FPP_ID.Value = Encr_decrData.Encrypt(FPP_ID)
            cmd.Parameters.Add(sqlpFPP_ENCR_FPP_ID)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            APPROVE_FEE_PAYMENT_PLAN_H = retSValParam.Value


        Catch ex As Exception
            APPROVE_FEE_PAYMENT_PLAN_H = -1
            'TRANS.Rollback()
        End Try
    End Function

    Public Function SUBMIT_FEE_PAYMENT_PLAN_H(ByVal Options As Integer, ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        SUBMIT_FEE_PAYMENT_PLAN_H = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.SUBMIT_FEE_PAYMENT_PLAN_H", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFPP_OPTIONS As New SqlParameter("@OPTIONS", SqlDbType.Int)
            sqlpFPP_OPTIONS.Value = Options
            cmd.Parameters.Add(sqlpFPP_OPTIONS)

            Dim sqlpFPP_BSU_ID As New SqlParameter("@FPP_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFPP_BSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpFPP_BSU_ID)

            Dim sqlpFPP_FPP_ID As New SqlParameter("@FPP_ID", SqlDbType.Int)
            sqlpFPP_FPP_ID.Value = FPP_ID
            cmd.Parameters.Add(sqlpFPP_FPP_ID)

            Dim sqlpFPP_USER As New SqlParameter("@FPP_USER", SqlDbType.VarChar, 20)
            sqlpFPP_USER.Value = User
            cmd.Parameters.Add(sqlpFPP_USER)

            Dim sqlpFPP_COMMENTS As New SqlParameter("@FPP_COMMENTS", SqlDbType.VarChar, 500)
            sqlpFPP_COMMENTS.Value = Comments
            cmd.Parameters.Add(sqlpFPP_COMMENTS)

            Dim sqlpFPP_CHKFEECOLLECTION As New SqlParameter("@FPP_CHKFEECOLLECTION", SqlDbType.Bit)
            sqlpFPP_CHKFEECOLLECTION.Value = CheckFeeCollection
            cmd.Parameters.Add(sqlpFPP_CHKFEECOLLECTION)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            SUBMIT_FEE_PAYMENT_PLAN_H = retSValParam.Value


        Catch ex As Exception
            SUBMIT_FEE_PAYMENT_PLAN_H = -1
            'TRANS.Rollback()
        End Try
    End Function


#Region "PublicProperties2"
    Private pFPD_ID As Integer
    Public Property FPD_ID() As Integer
        Get
            Return pFPD_ID
        End Get
        Set(ByVal value As Integer)
            pFPD_ID = value
        End Set
    End Property
    Private pFPD_CLT_ID As Integer
    Public Property FPD_CLT_ID() As Integer
        Get
            Return pFPD_CLT_ID
        End Get
        Set(ByVal value As Integer)
            pFPD_CLT_ID = value
        End Set
    End Property
    Private pFPD_BANK_ID As String
    Public Property FPD_BANK_ID() As String
        Get
            Return pFPD_BANK_ID
        End Get
        Set(ByVal value As String)
            pFPD_BANK_ID = value
        End Set
    End Property

    Private pFPD_AMOUNT As Double
    Public Property FPD_AMOUNT() As Double
        Get
            Return pFPD_AMOUNT
        End Get
        Set(ByVal value As Double)
            pFPD_AMOUNT = value
        End Set
    End Property
    Private pFPD_PAYMENT_DATE As String
    Public Property FPD_PAYMENT_DATE() As String
        Get
            Return pFPD_PAYMENT_DATE
        End Get
        Set(ByVal value As String)
            pFPD_PAYMENT_DATE = value
        End Set
    End Property
    Private pFPD_CHQNO As String
    Public Property FPD_CHQNO() As String
        Get
            Return pFPD_CHQNO
        End Get
        Set(ByVal value As String)
            pFPD_CHQNO = value
        End Set
    End Property
    Private pFPD_CHQDATE As String
    Public Property FPD_CHQDATE() As String
        Get
            Return pFPD_CHQDATE
        End Get
        Set(ByVal value As String)
            pFPD_CHQDATE = value
        End Set
    End Property
    Private pFPD_COMMENTS As String
    Public Property FPD_COMMENTS() As String
        Get
            Return pFPD_COMMENTS
        End Get
        Set(ByVal value As String)
            pFPD_COMMENTS = value
        End Set
    End Property
    Private pgvPayPlanDetail As DataTable
    Public Property gvPayPlanDetail() As DataTable
        Get
            Return pgvPayPlanDetail
        End Get
        Set(value As DataTable)
            pgvPayPlanDetail = value
        End Set
    End Property
#End Region
    Public Function CLEAR_FEE_PAYMENT_PLAN_D(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        CLEAR_FEE_PAYMENT_PLAN_D = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("DELETE FROM FEES.FEE_PAYMENTPLAN_D WHERE FPD_FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            CLEAR_FEE_PAYMENT_PLAN_D = True
        Catch ex As Exception
            CLEAR_FEE_PAYMENT_PLAN_D = False
            ' TRANS.Rollback()
        End Try

    End Function
    Public Function SAVE_FEE_PAYMENT_PLAN_D(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        SAVE_FEE_PAYMENT_PLAN_D = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.SAVE_FEE_PAYMENTPLAN_D", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFPD_CLT_ID As New SqlParameter("@FPD_CLT_ID", SqlDbType.Int)
            sqlpFPD_CLT_ID.Value = FPD_CLT_ID
            cmd.Parameters.Add(sqlpFPD_CLT_ID)

            Dim sqlpFPD_AMOUNT As New SqlParameter("@FPD_AMOUNT", SqlDbType.Decimal)
            sqlpFPD_AMOUNT.Value = FPD_AMOUNT
            cmd.Parameters.Add(sqlpFPD_AMOUNT)

            Dim sqlpFPD_PAYMENT_DATE As New SqlParameter("@FPD_PAYMENT_DATE", SqlDbType.VarChar)
            sqlpFPD_PAYMENT_DATE.Value = FPD_PAYMENT_DATE
            cmd.Parameters.Add(sqlpFPD_PAYMENT_DATE)

            Dim sqlpFPD_CHQNO As New SqlParameter("@FPD_CHQNO", SqlDbType.VarChar)
            sqlpFPD_CHQNO.Value = FPD_CHQNO
            cmd.Parameters.Add(sqlpFPD_CHQNO)

            Dim sqlpFPD_CHQDATE As New SqlParameter("@FPD_CHQDATE", SqlDbType.VarChar)
            sqlpFPD_CHQDATE.Value = FPD_CHQDATE
            cmd.Parameters.Add(sqlpFPD_CHQDATE)

            Dim sqlpFPD_BANK_ID As New SqlParameter("@FPD_BANK_ID", SqlDbType.VarChar)
            sqlpFPD_BANK_ID.Value = FPD_BANK_ID
            cmd.Parameters.Add(sqlpFPD_BANK_ID)

            Dim sqlpFPD_COMMENTS As New SqlParameter("@FPD_COMMENTS", SqlDbType.VarChar)
            sqlpFPD_COMMENTS.Value = FPD_COMMENTS
            cmd.Parameters.Add(sqlpFPD_COMMENTS)

            Dim sqlpFPP_ID As New SqlParameter("@FPP_ID", SqlDbType.Int)
            sqlpFPP_ID.Value = FPP_ID
            cmd.Parameters.Add(sqlpFPP_ID)

            Dim sqlpFPD_ID As New SqlParameter("@FPD_ID", SqlDbType.Int)
            sqlpFPD_ID.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlpFPD_ID)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            SAVE_FEE_PAYMENT_PLAN_D = retSValParam.Value
        Catch
            SAVE_FEE_PAYMENT_PLAN_D = 1
            'TRANS.Rollback()
        End Try
    End Function
    Public Function UPDATE_FEE_PAYMENT_PLAN_D(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        UPDATE_FEE_PAYMENT_PLAN_D = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_D SET FPD_CHQNO='" & FPD_CHQNO & "' , FPD_BANK_ID='" & FPD_BANK_ID & "' WHERE FPD_ID=" & FPD_ID & " AND  FPD_FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            UPDATE_FEE_PAYMENT_PLAN_D = True
        Catch ex As Exception
            UPDATE_FEE_PAYMENT_PLAN_D = False
        End Try
    End Function

    Public Function IS_CASHIER_EDITABLE(ByVal FPP_ID As Integer) As Boolean
        IS_CASHIER_EDITABLE = False
        If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT CASE WHEN ISNULL(FPP_LEVEL2_APPROVED,'')='PB' THEN 1 ELSE CASE WHEN ISNULL(FPP_APPROVED,'')='GR' THEN 1 ELSE CASE WHEN ISNULL(FPP_APPROVED,'')='FB' THEN 1 ELSE CASE WHEN ISNULL(FPP_LEVEL2_APPROVED,'')='PA' THEN 1 ELSE 0 END END END END AS STATUS FROM FEES.FEE_PAYMENTPLAN_H WITH (NOLOCK) WHERE 1=1 AND FPP_ID=" & FPP_ID & "") = 1 Then
            IS_CASHIER_EDITABLE = True
        End If
    End Function
    Public Function IS_CASHIER_DELETABLE(ByVal FPP_ID As Integer) As Boolean
        IS_CASHIER_DELETABLE = False
        If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT CASE WHEN ISNULL(FPP_LEVEL2_APPROVED,'')='PB' THEN 0 ELSE CASE WHEN ISNULL(FPP_APPROVED,'')='FB' THEN 1  ELSE 0 END END AS STATUS FROM FEES.FEE_PAYMENTPLAN_H WITH (NOLOCK) WHERE 1=1 AND FPP_ID=" & FPP_ID & "") = 1 Then
            IS_CASHIER_DELETABLE = True
        End If
    End Function
    Public Function bNEW_APPROVAL_PLAN(ByVal StudentID As Integer) As Boolean
        bNEW_APPROVAL_PLAN = False
        If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT COUNT(FPP_ID) FROM FEES.FEE_PAYMENTPLAN_H WITH (NOLOCK) WHERE (ISNULL(FPP_LEVEL2_APPROVED,'') NOT IN ('PA','PR')) AND FPP_STU_ID=" & StudentID & "") = 0 Then
            bNEW_APPROVAL_PLAN = True
        End If
    End Function
    Public Function bPLAN_APPROVED() As Boolean
        bPLAN_APPROVED = False
        If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(FPP_LEVEL2_APPROVED,'') AS APPROVED FROM FEES.FEE_PAYMENTPLAN_H WITH (NOLOCK) WHERE FPP_ID=" & FPP_ID & "") = "PA" Then
            bPLAN_APPROVED = True
        End If
    End Function
    Public Function bPLAN_APPROVED_OR_REJECTED() As Boolean
        bPLAN_APPROVED_OR_REJECTED = False
        If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT CASE WHEN ISNULL(FPP_LEVEL2_APPROVED,'') = 'PA' THEN 1 ELSE  CASE WHEN ISNULL(FPP_LEVEL2_APPROVED,'') = 'PR' THEN 1 ELSE 0 END END AS APPROVED FROM FEES.FEE_PAYMENTPLAN_H WITH (NOLOCK) WHERE FPP_ID=" & FPP_ID & "") = "1" Then
            bPLAN_APPROVED_OR_REJECTED = True
        End If
    End Function

    Public Function bPLAN_REJECTED() As Boolean
        bPLAN_REJECTED = False
        If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(FPP_LEVEL2_APPROVED,'') AS APPROVED FROM FEES.FEE_PAYMENTPLAN_H WITH (NOLOCK) WHERE FPP_ID=" & FPP_ID & "") = "PR" Then
            bPLAN_REJECTED = True
        End If
    End Function
    Public Shared Function GET_EMAIL_PREVIEW(ByVal FPP_ID As Long, ByVal BSU_ID As String,
                                                       ByVal EML_TYPE As String) As String
        GET_EMAIL_PREVIEW = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("SELECT  [DBO].GET_SCHEDULE_EMAIL_PREVIEW('" & FPP_ID & "','" & BSU_ID & "','" & EML_TYPE & "')")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString)
            If Not ds Is Nothing Then
                GET_EMAIL_PREVIEW = ds.Tables(0).Rows(0)(0).ToString
            End If
        Catch ex As Exception
            GET_EMAIL_PREVIEW = Nothing
        End Try
    End Function

    Public Function DELETE_FEE_PAYMENT_PLAN(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        DELETE_FEE_PAYMENT_PLAN = True
        If CLEAR_FEE_PAYMENT_PLAN_D(objCon, TRANS) Then
            Dim cmd As SqlCommand
            Try
                'cmd = New SqlCommand("DELETE FROM FEES.FEE_PAYMENTPLAN_H WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
                cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_bDELETED=1 WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
                cmd.CommandType = CommandType.Text
                cmd.CommandTimeout = 0
                cmd.ExecuteNonQuery()
                DELETE_FEE_PAYMENT_PLAN = True
            Catch ex As Exception
                DELETE_FEE_PAYMENT_PLAN = False
            End Try
        End If
    End Function
    Public Function SUBMIT_FEE_PAYMENT_PLAN(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        SUBMIT_FEE_PAYMENT_PLAN = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_STATUS=4,FPP_LEVEL0_COMMENTS='" & Comments & "' WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            SUBMIT_FEE_PAYMENT_PLAN = True
        Catch ex As Exception
            SUBMIT_FEE_PAYMENT_PLAN = False
        End Try
    End Function
    Public Function ONHOLD_FEE_PAYMENT_PLAN(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        ONHOLD_FEE_PAYMENT_PLAN = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L1',FPP_APPROVED='FO',FPP_STATUS=1,FPP_APPROVE_USER='" & User & "',FPP_APPROVE_COMMENTS='" & Comments & "',FPP_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            ONHOLD_FEE_PAYMENT_PLAN = True
        Catch ex As Exception
            ONHOLD_FEE_PAYMENT_PLAN = False
        End Try
    End Function
    Public Function REVERT_FEE_PAYMENT_PLAN(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        REVERT_FEE_PAYMENT_PLAN = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L1',FPP_APPROVED='FB',FPP_STATUS=0,FPP_APPROVE_USER='" & User & "',FPP_APPROVE_COMMENTS='" & Comments & "',FPP_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            REVERT_FEE_PAYMENT_PLAN = True
        Catch ex As Exception
            REVERT_FEE_PAYMENT_PLAN = False
        End Try
    End Function
    Public Function APPROVE_FEE_PAYMENT_PLAN(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        APPROVE_FEE_PAYMENT_PLAN = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L1',FPP_APPROVED='FA',FPP_STATUS=2,FPP_APPROVE_USER='" & User & "',FPP_APPROVE_COMMENTS='" & Comments & "',FPP_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            APPROVE_FEE_PAYMENT_PLAN = True
        Catch ex As Exception
            APPROVE_FEE_PAYMENT_PLAN = False
        End Try
    End Function

    Public Function REJECT_FEE_PAYMENT_PLAN(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        REJECT_FEE_PAYMENT_PLAN = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L1',FPP_APPROVED='FR',FPP_STATUS=2,FPP_APPROVE_USER='" & User & "',FPP_APPROVE_COMMENTS='" & Comments & "',FPP_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            REJECT_FEE_PAYMENT_PLAN = True
        Catch ex As Exception
            REJECT_FEE_PAYMENT_PLAN = False
        End Try
    End Function
    Public Function ONHOLD_FEE_PAYMENT_PLAN_LEVEL2(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        ONHOLD_FEE_PAYMENT_PLAN_LEVEL2 = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L2',FPP_LEVEL2_APPROVED='PO',FPP_STATUS=2,FPP_LEVEL2_USER='" & User & "',FPP_LEVEL2_COMMENTS='" & Comments & "',FPP_LEVEL2_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            ONHOLD_FEE_PAYMENT_PLAN_LEVEL2 = True
        Catch ex As Exception
            ONHOLD_FEE_PAYMENT_PLAN_LEVEL2 = False
        End Try
    End Function
    Public Function REVERT_FEE_PAYMENT_PLAN_LEVEL2(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        REVERT_FEE_PAYMENT_PLAN_LEVEL2 = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L2',FPP_LEVEL2_APPROVED='PB',FPP_STATUS=0,FPP_LEVEL2_USER='" & User & "',FPP_LEVEL2_COMMENTS='" & Comments & "',FPP_LEVEL2_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            REVERT_FEE_PAYMENT_PLAN_LEVEL2 = True
        Catch ex As Exception
            REVERT_FEE_PAYMENT_PLAN_LEVEL2 = False
        End Try
    End Function
    Public Function APPROVE_FEE_PAYMENT_PLAN_LEVEL2(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        APPROVE_FEE_PAYMENT_PLAN_LEVEL2 = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L2',FPP_LEVEL2_APPROVED='PA',FPP_STATUS=3,FPP_LEVEL2_USER='" & User & "',FPP_LEVEL2_COMMENTS='" & Comments & "',FPP_LEVEL2_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            APPROVE_FEE_PAYMENT_PLAN_LEVEL2 = True
        Catch ex As Exception
            APPROVE_FEE_PAYMENT_PLAN_LEVEL2 = False
        End Try
    End Function
    Public Function REJECT_FEE_PAYMENT_PLAN_LEVEL2(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Boolean
        REJECT_FEE_PAYMENT_PLAN_LEVEL2 = True
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("UPDATE FEES.FEE_PAYMENTPLAN_H SET FPP_APPROVE_LEVEL='L2',FPP_LEVEL2_APPROVED='PR',FPP_STATUS=2,FPP_LEVEL2_USER='" & User & "',FPP_LEVEL2_COMMENTS='" & Comments & "',FPP_LEVEL2_APPROVE_DATE=GETDATE() WHERE FPP_ID=" & FPP_ID & "", objCon, TRANS)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            REJECT_FEE_PAYMENT_PLAN_LEVEL2 = True
        Catch ex As Exception
            REJECT_FEE_PAYMENT_PLAN_LEVEL2 = False
        End Try
    End Function
    Public Sub GetContactDetails()
        Dim qry As String = "SELECT ISNULL(ParentName,'') ParentName , ISNULL(PARENT_EMAIL,'') PARENTEMAIL , ISNULL(PARENT_MOBILE_NO,'') MOBILENO, ISNULL(PARENT_RESIDENCE_PHONE,'') RESIDENCENO FROM OASIS.[dbo].[vw_PARENTDETAILS_LIST] WHERE STU_ID=" & STU_ID & ""
        Dim dsH As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        If Not dsH Is Nothing AndAlso dsH.Tables.Count > 0 Then
            ContactName = dsH.Tables(0).Rows(0)("ParentName")
            Email = dsH.Tables(0).Rows(0)("PARENTEMAIL")
            MobileNo = dsH.Tables(0).Rows(0)("MOBILENO")
            ResidenceNo = dsH.Tables(0).Rows(0)("RESIDENCENO")
        End If
    End Sub
    Public Shared Function GetROLEAccess(ByVal UserName As String, ByVal BSUID As String) As Integer
        GetROLEAccess = 0
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("SELECT  [DBO].GET_USER_ROLE_ACCESS('" & UserName & "','" & BSUID & "')")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString)
            If Not ds Is Nothing Then
                GetROLEAccess = ds.Tables(0).Rows(0)(0).ToString
            End If
        Catch ex As Exception
            GetROLEAccess = 0
        End Try
    End Function
    Public Sub GetSavedDetails()
        Dim qry As String = "SELECT FPP_ID,FPP_BSU_ID,FPP_ACD_ID,FPP_STU_ID,FPP_STU_TYPE,FPP_FEE_IDs, " & _
            "FPP_DATE, FPP_STU_AGING, FPP_APPROVE_USER, FPP_APPROVE_DATE, FPP_LOG_USER, FPP_LOG_DATE, FPP_STATUS, CASE WHEN FPP_STATUS=3 THEN 1 ELSE 0 END AS IS_PRINCIPAL_APPROVE, CASE WHEN FPP_STATUS=3 THEN 0 ELSE 1 END AS ISAMTEDITABLE, ISNULL(FPP_APPROVED,'') FPP_APPROVED, ISNULL(FPP_LEVEL2_APPROVED,'') FPP_LEVEL2_APPROVED, ISNULL(FPP_APPROVE_COMMENTS,'') FPP_APPROVE_COMMENTS, ISNULL(FPP_LEVEL2_COMMENTS,'') FPP_LEVEL2_COMMENTS, ISNULL(FPP_REQ_MODE_OF_PAY,0) FPP_REQ_MODE_OF_PAY, ISNULL(FPP_PLAN_TYPE,'') FPP_PLAN_TYPE " & _
            "FROM FEES.FEE_PAYMENTPLAN_H WHERE FPP_ID=" & FPP_ID & ""
        Dim dsH As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        If Not dsH Is Nothing AndAlso dsH.Tables.Count > 0 Then
            ACD_ID = dsH.Tables(0).Rows(0)("FPP_ACD_ID")
            STU_ID = dsH.Tables(0).Rows(0)("FPP_STU_ID")
            DOCDATE = dsH.Tables(0).Rows(0)("FPP_DATE")
            Aging_XML = dsH.Tables(0).Rows(0)("FPP_STU_AGING")
            FEE_IDs = dsH.Tables(0).Rows(0)("FPP_FEE_IDs")
            FinanceComments = dsH.Tables(0).Rows(0)("FPP_APPROVE_COMMENTS")
            PrincipalComments = dsH.Tables(0).Rows(0)("FPP_LEVEL2_COMMENTS")
            Status = dsH.Tables(0).Rows(0)("FPP_STATUS")
            PrincipalStatus = dsH.Tables(0).Rows(0)("FPP_LEVEL2_APPROVED")
            IsAmountEditable = dsH.Tables(0).Rows(0)("ISAMTEDITABLE")
            IsPrincipalApprove = dsH.Tables(0).Rows(0)("IS_PRINCIPAL_APPROVE")
            FPP_REQ_MODE_OF_PAY = dsH.Tables(0).Rows(0)("FPP_REQ_MODE_OF_PAY")
            FPP_APPROVED = dsH.Tables(0).Rows(0)("FPP_APPROVED")
            FPP_PLAN_TYPE = dsH.Tables(0).Rows(0)("FPP_PLAN_TYPE")
            BSU_ID = dsH.Tables(0).Rows(0)("FPP_BSU_ID")
            'ASONDATE = DOCDATE
            'STU_ID = STU_ID
            'Bkt1 = "30"
            'Bkt2 = "60"
            'Bkt3 = "90"
            'Bkt4 = "180"
            'FEE_IDs = FEE_IDs
            'Try
            '    SetStudentFeeAging()
            'Catch Ex As Exception
            'End Try

        End If
        qry = "SELECT FPD_ID AS ID,CLT_ID,PAYMODE,PAY_DATE,AMOUNT,CHQNO,CHQDT,BANK_ID,CRR_ID,BANK,COMMENTS FROM vwFEE_PAYMENTPLAN_D WHERE AMOUNT<>0 AND FPD_FPP_ID=" & FPP_ID & ""
        Dim dsD As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        If Not dsD Is Nothing AndAlso dsD.Tables.Count > 0 Then
            gvPayPlanDetail = dsD.Tables(0)
        End If
    End Sub

    Public Function PrintPayPlan(Optional ByVal bAttachAsEmail As Boolean = False) As MyReportClass
        Dim repSource As New MyReportClass

        Dim RptNameComp As String = "../../fees/Reports/RPT/rptFEE_PaymentPlan.rpt"
        Dim str_Sql As String

        str_Sql = "EXEC [dbo].[PaymentPlan] @FPP_ID=" & FPP_ID & ""
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            Dim params As New Hashtable

            params("ReportHead") = "FEE PAYMENT PLAN"

            params("UserName") = User
            params("bAttachasEmail") = bAttachAsEmail
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            Dim cmdSub1 As New SqlCommand

            cmdSub1.CommandText = "EXEC [dbo].[PaymentPlan_D] @FPD_FPP_ID=" & FPP_ID & ""
            cmdSub1.Connection = New SqlConnection(str_conn)
            cmdSub1.CommandType = CommandType.Text
            repSourceSubRep(0).Command = cmdSub1
            repSource.SubReport = repSourceSubRep

            repSource.ResourceName = RptNameComp

        End If
        Return repSource
    End Function
End Class
