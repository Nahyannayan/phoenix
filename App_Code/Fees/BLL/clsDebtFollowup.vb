﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class clsDebtFollowup

#Region "Variables"

    Public Property DFF_ID() As Long
    Public Property DFF_BSU_ID() As String
    Public Property DFF_DESC() As String
    Public Property DFF_STU_TYPE() As String
    Public Property DFF_STU_BSU_ID() As String
    Public Property DFF_STU_ACD_ID() As Integer
    Public Property DFF_STU_ID() As Long
    Public Property DFF_STU_MOBILENO() As String
    Public Property DFF_STU_EMAIL_ID() As String
    Public Property DFF_STU_OS() As Double
    Public Property DFF_STU_FEE_DUE_SINCE() As Double
    Public Property DFF_STU_GRD_IDs() As String
    Public Property DFF_STU_SCT_IDs() As String
    Public Property DFF_LOG_USER() As String
    Public Property DFC_ID() As Long
    Public Property DFC_BSU_ID() As String
    Public Property DFC_STU_ID() As Long
    Public Property DFC_COMMENTS() As String
    Public Property DFC_LOG_USER() As String
    Public Property DFC_FOLLOWUP_DATE() As String
    Public Property FOLLW_UP_ID() As Integer
    Public Property DFF_ALPHABETS() As String
    Public Property DFF_FILTER_FUNCTION() As String

    Public Property DFF_COVID_CONCESSION() As String

    Public Property DFF_PAY_TYPE() As String
    Public Property DFF_REASON() As String

    Public Property DFF_bPRIMARY_CONTACT() As Boolean

    Public Property DFF_bSECONDARY_CONTACT() As Boolean

    Public Property DFF_bSEND_EMAIL() As Boolean

    Public Property DFC_EMAIL_TEXT() As String

#End Region

#Region "Methods"

    Public Shared Function GET_SEARCH_FILTERS() As DataSet
        Dim str_sql As String = "SELECT DFF_ID,DFF_DESC FROM FEES.FEE_DEBT_FOLLOWUP_FILTER_M WITH(NOLOCK) WHERE DFF_BSU_ID = '" & HttpContext.Current.Session("sBsuId") & "' ORDER BY DFF_ID DESC"

        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function GET_DEBT_FOLLOW_UP_LIST() As DataSet
        Dim str_sql As String = "SELECT DFM_DAYS,DFM_DESC FROM FEES.DEBT_FOLLOW_UP_M ORDER BY DFM_DAYS"

        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_sql)
    End Function
    Public Sub GET_SEARCH_FILTER()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC FEES.GET_SAVED_SEARCH_FILTER @DFF_ID = @DFFID")

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@DFFID", SqlDbType.BigInt)
            pParms(0).Value = DFF_ID

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                DFF_STU_TYPE = ds.Tables(0).Rows(0)("DFF_STU_TYPE").ToString
                DFF_STU_BSU_ID = ds.Tables(0).Rows(0)("DFF_STU_BSU_ID").ToString
                DFF_STU_ACD_ID = ds.Tables(0).Rows(0)("DFF_STU_ACD_ID").ToString
                DFF_STU_GRD_IDs = ds.Tables(0).Rows(0)("DFF_STU_GRD_IDs").ToString
                DFF_STU_SCT_IDs = ds.Tables(0).Rows(0)("DFF_STU_SCT_IDs").ToString
                DFF_STU_MOBILENO = ds.Tables(0).Rows(0)("DFF_STU_MOBILENO").ToString
                DFF_STU_EMAIL_ID = ds.Tables(0).Rows(0)("DFF_STU_EMAIL_ID").ToString
                DFF_STU_OS = ds.Tables(0).Rows(0)("DFF_STU_OS").ToString
                DFF_STU_FEE_DUE_SINCE = ds.Tables(0).Rows(0)("DFF_STU_FEE_DUE_SINCE").ToString
                DFF_STU_ID = ds.Tables(0).Rows(0)("DFF_STU_ID").ToString
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function SAVE_DEBT_FOLLOWUP_FILTER_M(ByVal Retmessage As String) As Integer
        SAVE_DEBT_FOLLOWUP_FILTER_M = 1
        Retmessage = ""
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            Dim trans As SqlTransaction
            conn.Open()
            trans = conn.BeginTransaction()
            Dim cmd As SqlCommand
            Dim RetVal As Integer = 0

            cmd = New SqlCommand("FEES.SAVE_DEBT_FOLLOWUP_FILTER_M", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@DFF_ID", SqlDbType.BigInt)
            sqlp1.Value = DFF_ID
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@DFF_BSU_ID", SqlDbType.VarChar, 20)
            sqlp2.Value = DFF_BSU_ID
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@DFF_DESC", SqlDbType.VarChar, 250)
            sqlp3.Value = DFF_DESC
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@DFF_STU_TYPE", SqlDbType.VarChar, 5)
            sqlp4.Value = DFF_STU_TYPE
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@DFF_STU_BSU_ID", SqlDbType.VarChar, 20)
            sqlp5.Value = DFF_STU_BSU_ID
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@DFF_STU_ACD_ID", SqlDbType.Int)
            sqlp6.Value = DFF_STU_ACD_ID
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@DFF_STU_ID", SqlDbType.BigInt)
            sqlp7.Value = DFF_STU_ID
            cmd.Parameters.Add(sqlp7)

            Dim sqlp8 As New SqlParameter("@DFF_STU_MOBILENO", SqlDbType.VarChar, 15)
            sqlp8.Value = DFF_STU_MOBILENO
            cmd.Parameters.Add(sqlp8)

            Dim sqlp9 As New SqlParameter("@DFF_STU_EMAIL_ID", SqlDbType.VarChar, 50)
            sqlp9.Value = DFF_STU_EMAIL_ID
            cmd.Parameters.Add(sqlp9)

            Dim sqlp10 As New SqlParameter("@DFF_STU_OS", SqlDbType.Decimal)
            sqlp10.Value = DFF_STU_OS
            cmd.Parameters.Add(sqlp10)

            Dim sqlp11 As New SqlParameter("@DFF_STU_FEE_DUE_SINCE", SqlDbType.Decimal)
            sqlp11.Value = DFF_STU_FEE_DUE_SINCE
            cmd.Parameters.Add(sqlp11)

            Dim sqlp12 As New SqlParameter("@DFF_LOG_USER", SqlDbType.VarChar, 50)
            sqlp12.Value = DFF_LOG_USER
            cmd.Parameters.Add(sqlp12)

            Dim sqlp13 As New SqlParameter("@DFF_STU_GRD_IDs", SqlDbType.VarChar, 500)
            sqlp13.Value = DFF_STU_GRD_IDs
            cmd.Parameters.Add(sqlp13)

            Dim sqlp14 As New SqlParameter("@DFF_STU_SCT_IDs", SqlDbType.VarChar, 500)
            sqlp14.Value = DFF_STU_SCT_IDs
            cmd.Parameters.Add(sqlp14)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            RetVal = retSValParam.Value

            If RetVal <> 0 Then
                trans.Rollback()
                SAVE_DEBT_FOLLOWUP_FILTER_M = RetVal
            Else
                trans.Commit()
                SAVE_DEBT_FOLLOWUP_FILTER_M = 0
            End If
        Catch ex As Exception
            Retmessage = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Public Function SEARCH_STUDENTS_FOR_DEBT_FOLLOWUP(Optional ByRef RetVal As Integer = 0) As DataTable
        SEARCH_STUDENTS_FOR_DEBT_FOLLOWUP = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As String = "FEES.SEARCH_STUDENTS_FOR_DEBT_FOLLOWUP"
            Dim pParms(16) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
            pParms(0).Value = DFF_STU_TYPE
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = DFF_STU_BSU_ID
            pParms(2) = New SqlClient.SqlParameter("@STU_ACD_ID", SqlDbType.Int)
            pParms(2).Value = DFF_STU_ACD_ID
            pParms(3) = New SqlClient.SqlParameter("@STU_GRD_IDs", SqlDbType.VarChar, 500)
            pParms(3).Value = DFF_STU_GRD_IDs
            pParms(4) = New SqlClient.SqlParameter("@STU_SCT_IDs", SqlDbType.VarChar, 500)
            pParms(4).Value = DFF_STU_SCT_IDs
            pParms(5) = New SqlClient.SqlParameter("@MOBILE_NO", SqlDbType.VarChar, 20)
            pParms(5).Value = DFF_STU_MOBILENO
            pParms(6) = New SqlClient.SqlParameter("@EMAIL_ID", SqlDbType.VarChar, 50)
            pParms(6).Value = DFF_STU_EMAIL_ID
            pParms(7) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(7).Value = DFF_STU_ID
            pParms(8) = New SqlClient.SqlParameter("@STU_OS", SqlDbType.Decimal)
            pParms(8).Value = DFF_STU_OS
            pParms(9) = New SqlClient.SqlParameter("@DUE_SINCE", SqlDbType.Decimal)
            pParms(9).Value = DFF_STU_FEE_DUE_SINCE
            pParms(10) = New SqlClient.SqlParameter("@FOLLW_UP_ID", SqlDbType.Int)
            pParms(10).Value = FOLLW_UP_ID
            pParms(11) = New SqlClient.SqlParameter("@ALPHABETS", SqlDbType.VarChar, 100)
            pParms(11).Value = DFF_ALPHABETS
            pParms(12) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
            pParms(12).Value = DFF_LOG_USER
            pParms(13) = New SqlClient.SqlParameter("@DFF_FILTER_FUNCTION", SqlDbType.VarChar, 5)
            pParms(13).Value = DFF_FILTER_FUNCTION
            pParms(14) = New SqlClient.SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(14).Direction = ParameterDirection.ReturnValue
            pParms(15) = New SqlClient.SqlParameter("@bCOVID_CONCESSION", SqlDbType.VarChar, 5)
            pParms(15).Value = DFF_COVID_CONCESSION
            pParms(16) = New SqlClient.SqlParameter("@DFC_REASON", SqlDbType.VarChar, 5)
            pParms(16).Value = DFF_REASON

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, Qry, pParms)
            RetVal = pParms(14).Value
            If Not ds Is Nothing Then
                SEARCH_STUDENTS_FOR_DEBT_FOLLOWUP = ds.Tables(0)
            End If
        Catch ex As Exception
            SEARCH_STUDENTS_FOR_DEBT_FOLLOWUP = Nothing
        End Try
    End Function
    Public Shared Function GET_SAVED_DEBT_FOLLOW_UP_COMMENTS(ByVal STU_ID As Long) As DataTable
        GET_SAVED_DEBT_FOLLOW_UP_COMMENTS = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("FEES.GET_SAVED_DEBT_FOLLOW_UP_COMMENTS")

            Dim pParms(1) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = STU_ID

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_SAVED_DEBT_FOLLOW_UP_COMMENTS = ds.Tables(0)
            End If
        Catch ex As Exception
            GET_SAVED_DEBT_FOLLOW_UP_COMMENTS = Nothing
        End Try
    End Function
    Public Function SAVE_DEBT_FOLLOW_UP_COMMENTS(ByVal Retmessage As String, ByVal DTLS As DataTable) As Integer
        SAVE_DEBT_FOLLOW_UP_COMMENTS = 1
        Retmessage = ""
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            Dim trans As SqlTransaction
            conn.Open()
            trans = conn.BeginTransaction()
            Dim cmd As SqlCommand
            Dim RetVal As Integer = 0

            cmd = New SqlCommand("FEES.SAVE_DEBT_FOLLOW_UP_COMMENTS", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@DFC_ID", SqlDbType.BigInt)
            sqlp1.Value = DFC_ID
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@DFC_BSU_ID", SqlDbType.VarChar, 20)
            sqlp2.Value = DFC_BSU_ID
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@DFC_STU_ID", SqlDbType.BigInt)
            sqlp3.Value = DFC_STU_ID
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@DFC_COMMENTS", SqlDbType.VarChar, 750)
            sqlp4.Value = DFC_COMMENTS
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@DFC_LOG_USER", SqlDbType.VarChar, 50)
            sqlp5.Value = DFC_LOG_USER
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@DFC_FOLLOWUP_DATE", SqlDbType.Date)
            sqlp6.Value = DFC_FOLLOWUP_DATE
            cmd.Parameters.Add(sqlp6)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            Dim sqlp7 As New SqlClient.SqlParameter("@DT_XL", DTLS)
            sqlp7.Value = DTLS
            cmd.Parameters.Add(sqlp7)

            Dim sqlp8 As New SqlParameter("@DFC_PAY_TYPE", SqlDbType.VarChar, 50)
            sqlp8.Value = DFF_PAY_TYPE
            cmd.Parameters.Add(sqlp8)

            Dim sqlp9 As New SqlParameter("@DFC_REASON", SqlDbType.VarChar, 50)
            sqlp9.Value = DFF_REASON
            cmd.Parameters.Add(sqlp9)

            Dim sqlp10 As New SqlParameter("@DFC_bPRIMARY_CONTACT", SqlDbType.Bit)
            sqlp10.Value = DFF_bPRIMARY_CONTACT
            cmd.Parameters.Add(sqlp10)

            Dim sqlp11 As New SqlParameter("@DFC_bSECONDARY_CONTACT", SqlDbType.Bit)
            sqlp11.Value = DFF_bSECONDARY_CONTACT
            cmd.Parameters.Add(sqlp11)

            Dim sqlp12 As New SqlParameter("@DFC_bSEND_EMAIL", SqlDbType.Bit)
            sqlp12.Value = DFF_bSEND_EMAIL
            cmd.Parameters.Add(sqlp12)

            Dim sqlp13 As New SqlParameter("@DFC_EMAIL_TEXT", SqlDbType.VarChar)
            sqlp13.Value = DFC_EMAIL_TEXT
            cmd.Parameters.Add(sqlp13)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            RetVal = retSValParam.Value

            If RetVal <> 0 Then
                trans.Rollback()
                SAVE_DEBT_FOLLOW_UP_COMMENTS = RetVal
            Else
                trans.Commit()
                SAVE_DEBT_FOLLOW_UP_COMMENTS = 0
            End If
        Catch ex As Exception
            Retmessage = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Public Shared Function GET_STUDENT_DETAILS(ByVal STU_BSU_ID As String, ByVal STU_ID As Long) As DataTable
        GET_STUDENT_DETAILS = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("SELECT DISTINCT A.STU_NO, STU_NAME, GRD_DISPLAY, CASE WHEN ISNULL(B.RRH_STATUS, 0)=14 THEN 'Yes' ELSE 'No' END bCOVID_CONCESSION, STU_SIBLING_ID,")
            Qry.Append(" STU_PHOTO_PATH,CASE WHEN STU_CURRSTATUS<>'EN' THEN STU_CURRSTATUS + ' - LDA(' + OASIS.DBO.fnFormatDateToDisplay(STU_LASTATTDATE)+')' ELSE STU_CURRSTATUS END STU_CURRSTATUS ")
            Qry.Append(" FROM OASIS.dbo.VW_OSO_STUDENT_ENQUIRY A LEFT OUTER JOIN OASIS.COV.VW_COVID_RELIEF_DETAIL B ON RRD_STU_ID=A.STU_ID ")
            Qry.Append(" WHERE A.STU_BSU_ID = @STU_BSU_ID AND A.STU_ID=@STU_ID")

            'Qry.Append("SELECT STU_NO, STU_NAME, GRD_DISPLAY, 'YES'  bCOVID_CONCESSION, STU_SIBLING_ID, STU_PHOTO_PATH FROM OASIS.dbo.VW_OSO_STUDENT_ENQUIRY  WHERE STU_ID=@STU_ID")

            Dim pParms(2) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = STU_ID
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = STU_BSU_ID

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_STUDENT_DETAILS = ds.Tables(0)
            End If
        Catch ex As Exception
            GET_STUDENT_DETAILS = Nothing
        End Try
    End Function

    Public Shared Function GET_SIBLINGS(ByVal STU_BSU_ID As String, ByVal STU_SIBLING_ID As Long, ByVal STU_ID As Long) As DataTable
        GET_SIBLINGS = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("SELECT A.STU_ID,A.STU_NO + ' - ' + ISNULL(A.STU_PASPRTNAME,A.STU_FIRSTNAME) AS STU_NAME FROM OASIS.dbo.STUDENT_M AS A WITH(NOLOCK) RIGHT JOIN ")
            Qry.Append("OASIS.dbo.STUDENT_D AS B WITH(NOLOCK) ON A.STU_SIBLING_ID = B.STS_STU_ID WHERE A.STU_BSU_ID=@STU_BSU_ID AND STU_SIBLING_ID = @STU_SIBLING_ID ")
            Qry.Append("AND A.STU_ID<>@STU_ID")

            Dim pParms(3) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@STU_SIBLING_ID", SqlDbType.BigInt)
            pParms(0).Value = STU_SIBLING_ID
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = STU_BSU_ID
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(2).Value = STU_ID

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_SIBLINGS = ds.Tables(0)
            End If
        Catch ex As Exception
            GET_SIBLINGS = Nothing
        End Try
    End Function

    Public Shared Function GET_DEBT_FOLLOW_UP_REASONS(Optional ByVal bMandatory As Boolean = True) As DataTable
        GET_DEBT_FOLLOW_UP_REASONS = Nothing
        Try
            'Dim ds As New DataSet
            'Dim Qry As String = String.Empty
            'If Not bMandatory Then
            '    Qry = ("SELECT [DFR_ID],CASE WHEN DFR_ID = -1 THEN '' ELSE DFR_REASON END [DFR_REASON] FROM [OASIS_FEES].[FEES].[DEBT_FOLLOW_UP_REASONS] WITH(NOLOCK) WHERE ISNULL(DFR_bDELETED,0)=0 ORDER BY DFR_ID ASC ")
            'Else
            '    Qry = ("SELECT [DFR_ID],[DFR_REASON] FROM [OASIS_FEES].[FEES].[DEBT_FOLLOW_UP_REASONS] WITH(NOLOCK) WHERE ISNULL(DFR_bDELETED,0)=0 ORDER BY DFR_ID ASC ")
            'End If

            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry)
            'If Not ds Is Nothing Then
            '    GET_DEBT_FOLLOW_UP_REASONS = ds.Tables(0)
            'End If
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("FEES.GET_DEBT_FOLLOW_UP_REASONS")

            Dim pParms(1) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@bMandatory", SqlDbType.Int)
            pParms(0).Value = bMandatory

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_DEBT_FOLLOW_UP_REASONS = ds.Tables(0)
            End If
        Catch ex As Exception
            GET_DEBT_FOLLOW_UP_REASONS = Nothing
        End Try
    End Function

    Public Shared Function GET_FEE_HEADWISE_OUTSTANDING(ByVal STU_ID As Long, ByVal STU_BSU_ID As String,
                                                        Optional ByVal FEE_ID As Integer = 0, Optional ByVal IncludeFutureDate As Boolean = False) As DataTable
        GET_FEE_HEADWISE_OUTSTANDING = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("SELECT * FROM [FEES].FN_GET_FEE_HEADWISE_OUTSTANDING('" & STU_BSU_ID & "'," & STU_ID & "," & FEE_ID & ",'" & IncludeFutureDate & "') ORDER BY SORT_ORDER")

            'Dim pParms(4) As SqlClient.SqlParameter

            'pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            'pParms(0).Value = STU_ID
            'pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            'pParms(1).Value = STU_BSU_ID
            'pParms(2) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.Int)
            'pParms(2).Value = FEE_ID
            'pParms(3) = New SqlClient.SqlParameter("@bIncludeFutureDate", SqlDbType.Bit)
            'pParms(3).Value = IncludeFutureDate

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry.ToString)
            If Not ds Is Nothing Then
                GET_FEE_HEADWISE_OUTSTANDING = ds.Tables(0)
            End If
        Catch ex As Exception
            GET_FEE_HEADWISE_OUTSTANDING = Nothing
        End Try
    End Function
    Public Shared Function GET_PAYMENT_PLAN(ByVal STU_ID As Long) As DataTable
        GET_PAYMENT_PLAN = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("FEES.GET_DEBT_FOLLOWUP_PAY_PLAN")

            Dim pParms(1) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = STU_ID

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, Qry.ToString, pParms)
            If Not ds Is Nothing Then
                GET_PAYMENT_PLAN = ds.Tables(0)
            End If
        Catch ex As Exception
            GET_PAYMENT_PLAN = Nothing
        End Try
    End Function

    Public Shared Function GET_EMAIL_PREVIEW(ByVal STU_ID As Long, ByVal STU_BSU_ID As String,
                                                         ByVal EML_TYPE As String, ByVal COMPANY As String, Optional ByVal REFERENCE1 As String = "", Optional ByVal REFERENCE2 As String = "") As String
        GET_EMAIL_PREVIEW = Nothing
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("SELECT  [DBO].GET_EMAIL_PREVIEW('" & STU_ID & "','" & STU_BSU_ID & "','" & EML_TYPE & "','" & COMPANY & "','" & REFERENCE1 & "','" & REFERENCE2 & "')")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString)
            If Not ds Is Nothing Then
                GET_EMAIL_PREVIEW = ds.Tables(0).Rows(0)(0).ToString
            End If
        Catch ex As Exception
            GET_EMAIL_PREVIEW = Nothing
        End Try
    End Function

#End Region
End Class
