Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Imports System.Web.Configuration

Public Class FeeTranspotInvoice

    Public Shared Function F_SAVEFEE_PERFORMAINVOICE_H(ByVal p_FPH_ID As String, _
        ByVal p_FPH_BSU_ID As String, ByVal p_FPH_STU_BSU_ID As String, ByRef p_FPH_INOICENO As String, _
        ByVal p_FPH_DT As String, ByVal p_FPH_ACD_ID As String, ByVal p_FPD_STU_ID As String, _
        ByVal p_FPD_GRD_ID As String, ByVal p_FPH_STU_TYPE As String, ByRef NEW_FPH_ID As String, _
        ByVal p_bDelete As Boolean, ByVal p_FPH_SCH_ID As String, ByVal p_FPH_COMP_ID As String, _
        ByVal p_FPH_PrintCount As String, ByVal p_FPH_SBL_ID As String, ByVal p_FCL_NARRATION As String, _
        ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal bXcludeAdvFees As Boolean = False) As Integer
        'EXEC	@return_value = [FEES].[F_SAVEFEE_PERFORMAINVOICE_H]
        '@FPH_ID = 0, 
        '@FPH_BSU_ID = N'',
        '@FPH_STU_BSU_ID = N'121009',
        '@FPH_INOICENO = N'234',
        '@FPH_DT = N'12-mar-2009',
        '@FPH_ACD_ID = 28,
        '@FPD_STU_ID = 123,
        '@FPD_GRD_ID = N'01',
        '@FPH_STU_TYPE = N'0',
        '@FPH_COMP_ID = 0,
        '@FPH_PrintCount = 0,
        '@NEW_FPH_ID = @NEW_FPH_ID OUTPUT,
        '@bDelete = 0
        '@FPH_SCH_ID, FPH_SBL_ID,FCL_NARRATION

        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_H", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFPH_BSU_ID As New SqlParameter("@FPH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_BSU_ID.Value = p_FPH_BSU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpFPH_STU_BSU_ID As New SqlParameter("@FPH_STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_STU_BSU_ID.Value = p_FPH_STU_BSU_ID
        cmd.Parameters.Add(sqlpFPH_STU_BSU_ID)

        Dim sqlpFPH_INOICENO As New SqlParameter("@FPH_INOICENO", SqlDbType.VarChar, 20)
        sqlpFPH_INOICENO.Direction = ParameterDirection.InputOutput
        sqlpFPH_INOICENO.Value = p_FPH_INOICENO
        cmd.Parameters.Add(sqlpFPH_INOICENO)

        Dim sqlpFPH_DT As New SqlParameter("@FPH_DT", SqlDbType.DateTime)
        sqlpFPH_DT.Value = p_FPH_DT
        cmd.Parameters.Add(sqlpFPH_DT)

        Dim sqlpFPH_ACD_ID As New SqlParameter("@FPH_ACD_ID", SqlDbType.Int)
        sqlpFPH_ACD_ID.Value = p_FPH_ACD_ID
        cmd.Parameters.Add(sqlpFPH_ACD_ID)

        Dim sqlpFPD_STU_ID As New SqlParameter("@FPD_STU_ID", SqlDbType.BigInt)
        sqlpFPD_STU_ID.Value = p_FPD_STU_ID
        cmd.Parameters.Add(sqlpFPD_STU_ID)

        Dim sqlpFPD_GRD_ID As New SqlParameter("@FPD_GRD_ID", SqlDbType.VarChar, 10)
        sqlpFPD_GRD_ID.Value = p_FPD_GRD_ID
        cmd.Parameters.Add(sqlpFPD_GRD_ID)

        Dim sqlpFPH_STU_TYPE As New SqlParameter("@FPH_STU_TYPE", SqlDbType.VarChar, 4)
        sqlpFPH_STU_TYPE.Value = p_FPH_STU_TYPE
        cmd.Parameters.Add(sqlpFPH_STU_TYPE)

        Dim sqlpFPH_COMP_ID As New SqlParameter("@FPH_COMP_ID", SqlDbType.Int)
        If p_FPH_COMP_ID = "" Then
            sqlpFPH_COMP_ID.Value = System.DBNull.Value
        Else
            sqlpFPH_COMP_ID.Value = p_FPH_COMP_ID
        End If


        cmd.Parameters.Add(sqlpFPH_COMP_ID)

        Dim sqlpFPH_PrintCount As New SqlParameter("@FPH_PrintCount", SqlDbType.Int)
        sqlpFPH_PrintCount.Value = p_FPH_PrintCount
        cmd.Parameters.Add(sqlpFPH_PrintCount)

        Dim sqlpFPH_ID As New SqlParameter("@FPH_ID", SqlDbType.Int)
        sqlpFPH_ID.Value = p_FPH_ID
        cmd.Parameters.Add(sqlpFPH_ID)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = p_bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpFPH_SCH_ID As New SqlParameter("@FPH_SCH_ID", SqlDbType.Int)
        sqlpFPH_SCH_ID.Value = p_FPH_SCH_ID
        cmd.Parameters.Add(sqlpFPH_SCH_ID)
        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NewFPH_ID As New SqlParameter("@NEW_FPH_ID", SqlDbType.Int)
        NewFPH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFPH_ID)

        Dim FPH_SBL_ID As New SqlParameter("@FPH_SBL_ID", SqlDbType.Int)
        FPH_SBL_ID.Value = p_FPH_SBL_ID
        cmd.Parameters.Add(FPH_SBL_ID)

        Dim FCL_NARRATION As New SqlParameter("@FPH_NARRATION", SqlDbType.VarChar, 500)
        FCL_NARRATION.Value = p_FCL_NARRATION
        cmd.Parameters.Add(FCL_NARRATION)

        Dim SqlFPH_bXcludeAdvFees As New SqlParameter("@FPH_bXcludeAdvFees", SqlDbType.Bit)
        SqlFPH_bXcludeAdvFees.Value = bXcludeAdvFees
        cmd.Parameters.Add(SqlFPH_bXcludeAdvFees)

        cmd.ExecuteNonQuery()

        p_FPH_INOICENO = sqlpFPH_INOICENO.Value
        If retSValParam.Value = 0 Then
            NEW_FPH_ID = NewFPH_ID.Value
        Else
            NEW_FPH_ID = ""
        End If
        Return retSValParam.Value
    End Function

    Public Shared Function F_SAVEFEE_PERFORMAINVOICE_D(ByVal p_FPD_ID As Integer, _
    ByVal p_FPD_FPH_ID As String, ByVal p_FPD_FEE_ID As String, ByVal p_FPD_AMOUNT As String, _
    ByVal p_FPS_REF_ID As String, ByVal p_bDelete As Boolean, ByVal p_FPD_DESCR As String, _
    ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        'EXEC	@return_value = [FEES].[F_SAVEFEE_PERFORMAINVOICE_D]
        '@FPD_ID = 0,
        '@FPD_FPH_ID = 1,
        '@FPD_FEE_ID = 6,
        '@FPS_REF_ID = 6,
        '@FPD_AMOUNT = 1200,
        '@bDelete = 0
        '@FPD_DESCR
        Dim cmd As SqlCommand
        cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_D", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFPD_FPH_ID As New SqlParameter("@FPD_FPH_ID", SqlDbType.Int)
        sqlpFPD_FPH_ID.Value = p_FPD_FPH_ID
        cmd.Parameters.Add(sqlpFPD_FPH_ID)

        Dim sqlpFPD_FEE_ID As New SqlParameter("@FPD_FEE_ID", SqlDbType.Int)
        sqlpFPD_FEE_ID.Value = p_FPD_FEE_ID
        cmd.Parameters.Add(sqlpFPD_FEE_ID)

        Dim sqlpFPD_AMOUNT As New SqlParameter("@FPD_AMOUNT", SqlDbType.Decimal)
        sqlpFPD_AMOUNT.Value = p_FPD_AMOUNT
        cmd.Parameters.Add(sqlpFPD_AMOUNT)

        Dim sqlpFPD_ID As New SqlParameter("@FPD_ID", SqlDbType.Int)
        sqlpFPD_ID.Value = p_FPD_ID
        cmd.Parameters.Add(sqlpFPD_ID)

        Dim sqlpFPS_REF_ID As New SqlParameter("@FPS_REF_ID", SqlDbType.Int)
        sqlpFPS_REF_ID.Value = p_FPS_REF_ID
        cmd.Parameters.Add(sqlpFPS_REF_ID)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = p_bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpFPD_DESCR As New SqlParameter("@FPD_DESCR", SqlDbType.VarChar, 100)
        sqlpFPD_DESCR.Value = p_FPD_DESCR
        cmd.Parameters.Add(sqlpFPD_DESCR)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        Return retSValParam.Value

    End Function

    Public Shared Function CreateFeeTransportPI() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cID As New DataColumn("ID", System.Type.GetType("System.Decimal"))
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.String"))
            Dim cFEE_DESCR As New DataColumn("FEE_DESCR", System.Type.GetType("System.String"))
            Dim cFPH_ACD_ID As New DataColumn("FPH_ACD_ID", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cSTU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim cSTU_NAME As New DataColumn("STU_NAME", System.Type.GetType("System.String"))
            Dim cSTU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim cSTU_BSU_ID As New DataColumn("STU_BSU_ID", System.Type.GetType("System.String"))
            Dim cFPH_STU_TYPE As New DataColumn("FPH_STU_TYPE", System.Type.GetType("System.String"))
            Dim cFPH_COMP_ID As New DataColumn("FPH_COMP_ID", System.Type.GetType("System.String"))
            Dim cFPS_REF_ID As New DataColumn("FPS_REF_ID", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cID)
            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cFEE_DESCR)
            dtDt.Columns.Add(cFPH_ACD_ID)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cSTU_ID)
            dtDt.Columns.Add(cSTU_NAME)
            dtDt.Columns.Add(cSTU_NO)
            dtDt.Columns.Add(cSTU_BSU_ID)
            dtDt.Columns.Add(cFPH_STU_TYPE)
            dtDt.Columns.Add(cFPH_COMP_ID)
            dtDt.Columns.Add(cFPS_REF_ID)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "CreateFeeTransportPI")
            Return dtDt
        End Try
    End Function

    Public Shared Function GETFEEPAYABLEFORPERFORMA(ByVal p_ACD_ID As String, _
        ByVal p_BSU_ID As String, ByVal p_Term As String, ByVal p_SBL_ID As String, _
        ByVal p_FSM_Collection_SCH_ID As String, _
        ByVal str_conn As String) As DataSet
        '     EXEC	@return_value = [TRANSPORT].[GETFEEPAYABLEFORPERFORMA]
        '@ACD_ID = 28,
        '@BSU_ID = N'121009',
        '@Term = N'131500|131501|131502',
        '@SBL_ID = 77,
        '@FSM_Collection_SCH_ID = 2
        Dim pParms(6) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID

        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID

        pParms(2) = New SqlClient.SqlParameter("@Term", SqlDbType.VarChar, 200)
        pParms(2).Value = p_Term

        pParms(3) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_SBL_ID

        pParms(4) = New SqlClient.SqlParameter("@FSM_Collection_SCH_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FSM_Collection_SCH_ID
        'pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        'pParms(5).Direction = ParameterDirection.ReturnValue        

        Return SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRANSPORT.GETFEEPAYABLEFORPERFORMA", pParms)
    End Function

    Public Shared Function SaveReferenceDetails(ByVal FPS_ID As Integer, ByVal FPS_FPH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal FPS_DATE As DateTime, ByVal Isdelete As Boolean) As Integer

        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpFPS_ID As New SqlParameter("@FPS_ID", SqlDbType.Int)
        sqlpFPS_ID.Value = FPS_ID
        cmd.Parameters.Add(sqlpFPS_ID)

        Dim sqlpFPS_FPH_ID As New SqlParameter("@FPS_FPH_ID", SqlDbType.Int)
        sqlpFPS_FPH_ID.Value = FPS_FPH_ID
        cmd.Parameters.Add(sqlpFPS_FPH_ID)

        Dim sqlpFPS_DATE As New SqlParameter("@FPS_DATE", SqlDbType.DateTime)
        sqlpFPS_DATE.Value = FPS_DATE
        cmd.Parameters.Add(sqlpFPS_DATE)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = Isdelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        Return iReturnvalue

    End Function


    Public Shared Function GETFEEPAYABLEFORPERFORMA_GROUP(ByVal p_ACD_ID As String, _
        ByVal p_BSU_ID As String, ByVal p_Term As String, ByVal p_StuIdIS As String, _
        ByVal p_FSM_Collection_SCH_ID As String, ByVal p_IsDetailed As Boolean, _
        ByVal str_conn As String) As DataSet
        '     EXEC	@return_value = [TRANSPORT].[GETFEEPAYABLEFORPERFORMA_GROUP]
        '@ACD_ID = 28,
        '@BSU_ID = N'121009',
        '@Term = N'131500|131501|131502',
        '@SBL_ID = 77,
        '@FSM_Collection_SCH_ID = 2
        Dim pParms(6) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID

        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID

        pParms(2) = New SqlClient.SqlParameter("@Term", SqlDbType.VarChar)
        pParms(2).Value = p_Term

        pParms(3) = New SqlClient.SqlParameter("@StuIdIS", SqlDbType.VarChar)
        pParms(3).Value = p_StuIdIS

        pParms(4) = New SqlClient.SqlParameter("@FSM_Collection_SCH_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FSM_Collection_SCH_ID


        pParms(5) = New SqlClient.SqlParameter("@IsDetailed", SqlDbType.Bit)
        pParms(5).Value = p_IsDetailed


        Return SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRANSPORT.GETFEEPAYABLEFORPERFORMA_GROUP", pParms)
    End Function

    Public Shared Function PopulateMonthsInAcademicYearID(ByVal ACD_ID As Integer) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT TM.TRM_ID, TM.TRM_DESCRIPTION, AMS_ID, " & _
            "DATENAME(month, ISNULL(AMS_FEE_DTFROM, AMS_DTFROM)) + '-' + ltrim(str(datepart(yyyy,ISNULL(AMS_FEE_DTFROM, AMS_DTFROM)))) " & _
            " AS MONTH_DESCR FROM ACADEMIC_MonthS_S AS AM WITH ( NOLOCK ) LEFT OUTER JOIN " & _
            " TRM_M AS TM WITH ( NOLOCK ) ON AM.AMS_TRM_ID = TM.TRM_ID AND " & _
            " TM.TRM_ACD_ID = AM.AMS_ACD_ID  WHERE AMS_ACD_ID = " & ACD_ID
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GETFEEPAYABLE_ONLINE(ByVal p_ACD_ID As String, _
        ByVal p_BSU_ID As String, ByVal p_SBL_ID As String, ByVal str_conn As String) As DataTable
        'EXEC	@return_value =  TRANSPORT.GETFEEPAYABLE_ONLINE
        '@ACD_ID = 28,
        '@BSU_ID = N'121009' ,
        '@SBL_ID = 42 
        Dim DS As DataSet
        Dim pParms(3) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID

        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID

        pParms(2) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SBL_ID

        'pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        'pParms(5).Direction = ParameterDirection.ReturnValue        

        DS = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRANSPORT.GETFEEPAYABLE_ONLINE", pParms)
        Return DS.Tables(0)
    End Function
    Public Shared Function PrintReceiptComp(ByVal vINV_NO As String, ByVal BSU_ID As String, ByVal STU_BSU_ID As String, ByVal USR_NAME As String) As MyReportClass
        Dim repSource As New MyReportClass

        Dim RptNameComp As String = "../../fees/Reports/RPT/"
        Dim str_Sql, strFilter, str_Sql_sub As String
        strFilter = "  FPH_INOICENO ='" & vINV_NO & "' AND FPH_BSU_ID='" & BSU_ID & "' AND FPH_STU_BSU_ID='" & STU_BSU_ID & "' "

        str_Sql = "SELECT * FROM FEES.VW_OSO_FEES_PROFORMA_REPORT WHERE " & strFilter
        str_Sql_sub = "SELECT * FROM FEES.VW_OSO_FEES_PROFORMA_REPORT_SUB WHERE " & strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            RptNameComp &= ds.Tables(0).Rows(0)("RPT_NAME").ToString
            Dim params As New Hashtable

            params("RPT_CAPTION") = "Invoice"
            params("UserName") = USR_NAME
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            Dim cmdSubMain As New SqlCommand

            cmdSubMain.CommandText = str_Sql_sub
            cmdSubMain.Connection = New SqlConnection(str_conn)
            cmdSubMain.CommandType = CommandType.Text
            repSourceSubRep(0).Command = cmdSubMain
            repSource.SubReport = repSourceSubRep

            repSource.ResourceName = RptNameComp

        End If
        Return repSource
    End Function

    Public Shared Function PrintReceipt(ByVal p_new_fph_id As String, ByVal BSU_ID As String, _
     ByVal USR_NAME As String, ByVal STU_BSU_ID As String) As MyReportClass
        Dim str_Sql, strFilter, strFilter_sub As String, PVM_ID As Integer = 0, PVM_VER_VER_NO As Integer = 0
        strFilter = " FPH_ID IN(" & p_new_fph_id & ") AND FPH_BSU_ID='" & BSU_ID & "' "
        strFilter_sub = " FPD_FPH_ID IN(" & p_new_fph_id & ") "

        str_Sql = "SELECT * FROM TRANSPORT.VW_TRANSPORT_INVOICE_H WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim repSource As New MyReportClass
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        ' PVM_ID = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select FPH_PVM_ID from TRANSPORT.VW_TRANSPORT_INVOICE_H WHERE " + strFilter)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            PVM_ID = ds.Tables(0).Rows(0)("FPH_PVM_ID")
            PVM_VER_VER_NO = ds.Tables(0).Rows(0)("PVM_VER_VER_NO")
            cmd.Connection = New SqlConnection(str_conn)

            Dim params As New Hashtable
            params("UserName") = USR_NAME
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = False
            '''''''''''''''
            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            repSourceSubRep(1) = New MyReportClass

            Dim cmdHeader As New SqlCommand("getBsuInFoWithImage_Provider", New SqlConnection(ConnectionManger.GetOASISConnectionString))
            cmdHeader.CommandType = CommandType.StoredProcedure
            cmdHeader.Parameters.AddWithValue("@IMG_STU_BSU_ID", STU_BSU_ID)
            cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
            cmdHeader.Parameters.AddWithValue("@IMG_PROVIDER_BSU_ID", BSU_ID)
            repSourceSubRep(0).Command = cmdHeader

            Dim cmdSubEarn As New SqlCommand
            cmdSubEarn.CommandText = "[TRANSPORT].[PERFORMAINVOICE]"
            cmdSubEarn.Parameters.AddWithValue("@FPH_ID", p_new_fph_id)
            cmdSubEarn.Parameters.AddWithValue("@BSU_ID", BSU_ID)
            cmdSubEarn.Connection = New SqlConnection(str_conn)
            cmdSubEarn.CommandType = CommandType.StoredProcedure
            repSourceSubRep(1).Command = cmdSubEarn



            ''SUBREPORT2
            If PVM_ID <> 0 Then
                ReDim Preserve repSourceSubRep(2)
                repSourceSubRep(2) = New MyReportClass
                Dim cmdSubFooter As New SqlCommand
                cmdSubFooter.CommandText = "EXEC TRANSPORT.GetTransportFooter '" & BSU_ID & "'"
                cmdSubFooter.Connection = New SqlConnection(str_conn)
                cmdSubFooter.CommandType = CommandType.Text
                repSourceSubRep(2).Command = cmdSubFooter
            End If

            repSource.SubReport = repSourceSubRep

            ''''''''''
            If PVM_ID <> 0 And PVM_VER_VER_NO = 3 Then 'if TAX enabled
                Select Case BSU_ID
                    Case OASISConstants.TransportBSU_BrightBus
                        repSource.ResourceName = "../../fees/Reports/RPT/rptFeeTransportProformaTaxInvoice_bbt.rpt"
                    Case OASISConstants.TransportBSU_Sts
                        repSource.ResourceName = "../../fees/Reports/RPT/rptFeeTransportProformaTaxInvoice_sts.rpt"
                End Select
            ElseIf PVM_ID <> 0 Then
                repSource.ResourceName = "../../fees/Reports/RPT/rptFeeTransportInvoice_StudentNew.rpt"
            Else
                Select Case BSU_ID
                    Case OASISConstants.TransportBSU_BrightBus
                        repSource.ResourceName = "../../fees/Reports/RPT/rptFeeTransportInvoice_Student.rpt"
                    Case OASISConstants.TransportBSU_Sts
                        repSource.ResourceName = "../../fees/Reports/RPT/rptFeeTransportInvoice_Student_sts.rpt"
                End Select
            End If


        End If
        Return repSource
    End Function

End Class
