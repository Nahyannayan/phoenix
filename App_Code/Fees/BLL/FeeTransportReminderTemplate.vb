Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
''' The class Created for handling the Concession Type
''' 
''' Author : SHIJIN C A
''' Date : 03-MAY-2008 
''' </summary>
''' <remarks></remarks>
Public Class FeeTransportReminderTemplate

    ''' <summary>
    ''' Variables used to Store the 
    ''' </summary>
    ''' <remarks></remarks>
    Dim vFRM_ID As Integer
    Dim vFRM_Level As Int16
    Dim vFRM_SHORT_DESCR As String
    Dim vFRM_REMARKS As String
    Dim vFRM_Acknowledgement As String
    Dim vFRM_BSU_ID As String
    Dim vFRM_BSU_DESCR As String
    Dim vFRM_SIGNATORY As String
    Dim bEdited As Boolean
    Dim bDeleted As Boolean

    Public Property FRM_ID() As Integer
        Get
            Return vFRM_ID
        End Get
        Set(ByVal value As Integer)
            vFRM_ID = value
        End Set
    End Property

    Public Property FRM_Level() As Int16
        Get
            Return vFRM_Level
        End Get
        Set(ByVal value As Int16)
            vFRM_Level = value
        End Set
    End Property

    Public Property FRM_SHORT_DESCR() As String
        Get
            Return vFRM_SHORT_DESCR
        End Get
        Set(ByVal value As String)
            vFRM_SHORT_DESCR = value
        End Set
    End Property

    Public Property FRM_SIGNATORY() As String
        Get
            Return vFRM_SIGNATORY
        End Get
        Set(ByVal value As String)
            vFRM_SIGNATORY = value
        End Set
    End Property

    Public Property FRM_Acknowledgement() As String
        Get
            Return vFRM_Acknowledgement
        End Get
        Set(ByVal value As String)
            vFRM_Acknowledgement = value
        End Set
    End Property

    Public Property FRM_REMARKS() As String
        Get
            Return vFRM_REMARKS
        End Get
        Set(ByVal value As String)
            vFRM_REMARKS = value
        End Set
    End Property

    Public Property FRM_BSU_ID() As String
        Get
            Return vFRM_BSU_ID
        End Get
        Set(ByVal value As String)
            vFRM_BSU_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets Business Unit to link( only for staff concession)
    ''' So that each business unit will have a concession head and 
    ''' can be used for interunit voucher generation.
    ''' 
    ''' </summary>
    ''' <value>String</value>
    ''' <returns>Business Unit ID</returns>
    ''' <remarks></remarks>

    Public Property FRM_BSU_NAME() As String
        Get
            Return vFRM_BSU_DESCR
        End Get
        Set(ByVal value As String)
            vFRM_BSU_DESCR = value
        End Set
    End Property

    Public Shared Function GetFeeTransportReminderTemplate(ByVal FRM_ID As Integer, ByVal conn As SqlConnection) As FeeTransportReminderTemplate
        Dim sql_str As String
        sql_str = "SELECT FEES.FEE_REMINDER_M.FRM_ID, FEES.FEE_REMINDER_M.FRM_BSU_ID, " & _
        " FEES.FEE_REMINDER_M.FRM_SIGNATORY, FEES.FEE_REMINDER_M.FRM_SHORT_DESCR, FEES.FEE_REMINDER_M.FRM_ACKNOWLEDGEMENT, " & _
        " FEES.FEE_REMINDER_M.FRM_Level, FEES.FEE_REMINDER_M.FRM_REMARKS, " & _
        " BUSINESSUNIT_M.BSU_NAME FROM FEES.FEE_REMINDER_M INNER JOIN " & _
        " BUSINESSUNIT_M ON FEES.FEE_REMINDER_M.FRM_BSU_ID = BUSINESSUNIT_M.BSU_ID " & _
        " WHERE FRM_ID =" & FRM_ID
        Dim dreader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_str)
        Dim vFeeRem As New FeeTransportReminderTemplate
        While (dreader.Read())
            vFeeRem.FRM_ID = dreader("FRM_ID")
            vFeeRem.FRM_SHORT_DESCR = dreader("FRM_SHORT_DESCR")
            vFeeRem.FRM_Acknowledgement = dreader("FRM_ACKNOWLEDGEMENT").ToString()
            vFeeRem.FRM_SIGNATORY = dreader("FRM_SIGNATORY")
            vFeeRem.FRM_Level = dreader("FRM_Level")
            vFeeRem.FRM_BSU_ID = dreader("FRM_BSU_ID").ToString
            vFeeRem.FRM_REMARKS = dreader("FRM_REMARKS")
            vFeeRem.FRM_BSU_NAME = dreader("BSU_NAME").ToString
            vFeeRem.bEdited = True
        End While
        Return vFeeRem
    End Function


    Public Shared Function SaveFeeTransportReminderTemplate(ByVal vSaveFeeRem As FeeTransportReminderTemplate, ByVal trans As SqlTransaction, ByVal conn As SqlConnection) As Integer
        If vSaveFeeRem Is Nothing Then
            Return -1
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[F_SaveFEE_REMINDER_M]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRM_Level As New SqlParameter("@FRM_Level", SqlDbType.TinyInt)
        sqlpFRM_Level.Value = vSaveFeeRem.FRM_Level
        cmd.Parameters.Add(sqlpFRM_Level)

        Dim sqlpFRM_REMARKS As New SqlParameter("@FRM_REMARKS", SqlDbType.VarChar, 300)
        sqlpFRM_REMARKS.Value = vSaveFeeRem.FRM_REMARKS
        cmd.Parameters.Add(sqlpFRM_REMARKS)

        Dim sqlpFRM_Acknowledgement As New SqlParameter("@FRM_Acknowledgement", SqlDbType.VarChar, 500)
        sqlpFRM_Acknowledgement.Value = vSaveFeeRem.FRM_Acknowledgement
        cmd.Parameters.Add(sqlpFRM_Acknowledgement)

        Dim sqlpFRM_SHORT_DESCR As New SqlParameter("@FRM_SHORT_DESCR", SqlDbType.VarChar, 70)
        sqlpFRM_SHORT_DESCR.Value = vSaveFeeRem.FRM_SHORT_DESCR
        cmd.Parameters.Add(sqlpFRM_SHORT_DESCR)

        Dim sqlpFRM_BSU_ID As New SqlParameter("@FRM_BSU_ID", SqlDbType.VarChar, 20)
        If vSaveFeeRem.FRM_BSU_ID = "" Or vSaveFeeRem.FRM_BSU_ID = "-1" Then
            sqlpFRM_BSU_ID.Value = DBNull.Value
        Else
            sqlpFRM_BSU_ID.Value = vSaveFeeRem.FRM_BSU_ID
        End If
        cmd.Parameters.Add(sqlpFRM_BSU_ID)

        If vSaveFeeRem.bEdited Then
            Dim sqlpFPB_ID As New SqlParameter("@FRM_ID", SqlDbType.Int)
            sqlpFPB_ID.Value = vSaveFeeRem.FRM_ID
            cmd.Parameters.Add(sqlpFPB_ID)
        End If

        Dim sqlpFRM_SIGNATORY As New SqlParameter("@FRM_SIGNATORY", SqlDbType.VarChar, 100)
        sqlpFRM_SIGNATORY.Value = vSaveFeeRem.FRM_SIGNATORY
        cmd.Parameters.Add(sqlpFRM_SIGNATORY)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vSaveFeeRem.bDeleted
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function
End Class
