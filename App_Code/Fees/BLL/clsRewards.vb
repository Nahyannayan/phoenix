﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net
Imports Newtonsoft.Json.Linq
Imports RestSharp
Public Class clsRewards

#Region "public variables for POINTS_REDEMPTION_ONLINE"
    Private vPRO_ID As Long, vPRO_PRO_ID As Long, vPRO_BSU_ID As String, vPRO_ACD_ID As Integer
    Private vPRO_DATE As DateTime, vPRO_SOURCE As String, vPRO_STU_TYPE As String, vPRO_STU_ID As Long
    Private vPRO_GRM_ID As Integer, vPRO_SCT_ID As Integer, vPRO_DRCR As String, vPRO_AMOUNT As Double
    Private vPRO_NARRATION As String, vPRO_bPOSTED As Boolean, vPRO_STATUS As String, vPRO_LOG_USERNAME As String

    Public Property PRO_ID() As Long
        Get
            Return vPRO_ID
        End Get
        Set(ByVal value As Long)
            vPRO_ID = value
        End Set
    End Property
    Public Property PRO_BSU_ID() As String
        Get
            Return vPRO_BSU_ID
        End Get
        Set(ByVal value As String)
            vPRO_BSU_ID = value
        End Set
    End Property
    Public Property PRO_PRO_ID() As Long
        Get
            Return vPRO_PRO_ID
        End Get
        Set(ByVal value As Long)
            vPRO_PRO_ID = value
        End Set
    End Property
    Public Property PRO_ACD_ID() As Integer
        Get
            Return vPRO_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vPRO_ACD_ID = value
        End Set
    End Property
    Public Property PRO_DATE() As DateTime
        Get
            Return vPRO_DATE
        End Get
        Set(ByVal value As DateTime)
            vPRO_DATE = value
        End Set
    End Property
    Public Property PRO_SOURCE() As String
        Get
            Return vPRO_SOURCE
        End Get
        Set(ByVal value As String)
            vPRO_SOURCE = value
        End Set
    End Property
    Public Property PRO_STU_TYPE() As String
        Get
            Return vPRO_STU_TYPE
        End Get
        Set(ByVal value As String)
            vPRO_STU_TYPE = value
        End Set
    End Property
    Public Property PRO_STU_ID() As Long
        Get
            Return vPRO_STU_ID
        End Get
        Set(ByVal value As Long)
            vPRO_STU_ID = value
        End Set
    End Property
    Public Property PRO_GRM_ID() As Integer
        Get
            Return vPRO_GRM_ID
        End Get
        Set(ByVal value As Integer)
            vPRO_GRM_ID = value
        End Set
    End Property
    Public Property PRO_SCT_ID() As Integer
        Get
            Return vPRO_SCT_ID
        End Get
        Set(ByVal value As Integer)
            vPRO_SCT_ID = value
        End Set
    End Property
    Public Property PRO_DRCR() As String
        Get
            Return vPRO_DRCR
        End Get
        Set(ByVal value As String)
            vPRO_DRCR = value
        End Set
    End Property
    Public Property PRO_AMOUNT() As Double
        Get
            Return vPRO_AMOUNT
        End Get
        Set(ByVal value As Double)
            vPRO_AMOUNT = value
        End Set
    End Property
    Public Property PRO_NARRATION() As String
        Get
            Return vPRO_NARRATION
        End Get
        Set(ByVal value As String)
            vPRO_NARRATION = value
        End Set
    End Property
    Public Property PRO_bPOSTED() As Boolean
        Get
            Return vPRO_bPOSTED
        End Get
        Set(ByVal value As Boolean)
            vPRO_bPOSTED = value
        End Set
    End Property
    Public Property PRO_LOG_USERNAME() As String
        Get
            Return vPRO_LOG_USERNAME
        End Get
        Set(ByVal value As String)
            vPRO_LOG_USERNAME = value
        End Set
    End Property
    Public Property PRO_STATUS() As String
        Get
            Return vPRO_STATUS
        End Get
        Set(ByVal value As String)
            vPRO_STATUS = value
        End Set
    End Property

    Public Property PARENT_EMAIL As String
    Public Property PERSISTENT_ID As Long

    Public Property API_CMD As String
    Public Property API_URI As String
    Public Property API_USERNAME As String
    Public Property API_PASSWORD As String
    Public Property API_TOKEN As String
    Public Property API_METHOD As String

    Public Property PRO_API_SUCCESS As String
    Public Property PRO_API_MESSAGE As String
    Public Property PRO_API_HTTP_RESPONSE As String
    Public Property PRO_API_CODE As String
    Public Property PRO_API_DATA_CODE As String
    Public Property PRO_API_DATA_MESSAGE As String
    Public Property PRO_API_DATA_TRANSACTION_ID As String
    Public Property PRO_API_DATA_STATUS As String
    Public Property PRO_API_DATA_AVAILABLE_POINTS As Long
    Public Property PRO_API_DATA_REDEEMABLE_POINTS As Long
    Public Property PRO_API_DATA_REDEEMABLE_AMOUNT As Double
    Public Property bUpdate_Points As Boolean
    Public Property PRO_FCL_ID As Long
    Public Property CUSTOMER_ID As String

#End Region

    Public Sub FETCH_REWARDS_INFO(ByVal STU_ID As Int64)
        Try
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = STU_ID

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "FEES.GET_GEMS_REWARDS_CUSTOMER_INFO", pParms)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                PERSISTENT_ID = ds.Tables(0).Rows(0)("PERSISTENT_ID").ToString
                PARENT_EMAIL = ds.Tables(0).Rows(0)("PARENT_EMAIL").ToString
                CUSTOMER_ID = ds.Tables(0).Rows(0)("CUSTOMER_ID").ToString
            Else
                PERSISTENT_ID = 0
                PARENT_EMAIL = ""
                CUSTOMER_ID = 0
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function IS_UAE_SCHOOL(ByVal BSU_ID As String) As Boolean
        IS_UAE_SCHOOL = False
        Try
            Dim strConn As String = ""
            Dim Qry As New StringBuilder
            Qry.Append("SELECT COUNT(BSU_ID)AS EXIST FROM dbo.BUSINESSUNIT_M WITH (NOLOCK) WHERE BSU_ID = @BSU_ID AND ISNULL(BSU_COUNTRY_ID, 0) = 172 ")

            strConn = ConnectionManger.GetOASISConnectionString
            Using conn As New SqlConnection()
                conn.ConnectionString = strConn
                Using cmd As New SqlCommand()
                    cmd.CommandText = Qry.ToString
                    cmd.Parameters.AddWithValue("@BSU_ID", BSU_ID)
                    cmd.Connection = conn
                    conn.Open()
                    Dim sql As String = UtilityObj.CommandAsSql(cmd)
                    Using sdr As SqlDataReader = cmd.ExecuteReader()
                        While sdr.Read()
                            If FeeCollection.GetDoubleVal(sdr("EXIST")) > 0 Then
                                IS_UAE_SCHOOL = True
                            End If
                        End While
                    End Using
                    conn.Close()
                End Using
            End Using
        Catch ex As Exception

        End Try
    End Function

    Public Sub GET_API_CALL_PARAMETERS(ByVal API_COMMAND As String)
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@GRI_COMMAND", SqlDbType.VarChar, 50)
        pParms(0).Value = API_COMMAND

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "OASIS_LOYALTY.dbo.GET_ENTERTAINER_API_DATA", pParms)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            API_URI = ds.Tables(0).Rows(0)("API_URI").ToString
            API_USERNAME = ds.Tables(0).Rows(0)("API_USERNAME").ToString
            API_PASSWORD = ds.Tables(0).Rows(0)("API_PASSWORD").ToString
            API_TOKEN = ds.Tables(0).Rows(0)("API_TOKEN").ToString
            API_METHOD = ds.Tables(0).Rows(0)("API_METHOD").ToString
        Else
            API_URI = ""
            API_USERNAME = ""
            API_PASSWORD = ""
            API_TOKEN = ""
            API_METHOD = ""
        End If
    End Sub
    Public Shared Function SAVE_ENTERTAINER_API_CALL_LOG(ByRef ACL_ID As Long, ByVal ACL_BSU_ID As String, ByVal ACL_DATE As DateTime, _
                                                         ByVal ACL_ACTION As String, ByVal ACL_URL As String, ByVal ACL_METHOD As String, _
                                                         ByVal ACL_PERSISTENT_ID As String, ByVal ACL_USER As String) As String()
        Dim output As New List(Of String)()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim param(9) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACL_ID", SqlDbType.BigInt)
            param(0).Direction = ParameterDirection.InputOutput
            param(0).Value = ACL_ID
            param(1) = New SqlClient.SqlParameter("@ACL_BSU_ID", SqlDbType.VarChar, 20)
            param(1).Value = ACL_BSU_ID
            param(2) = New SqlClient.SqlParameter("@ACL_DATE", SqlDbType.DateTime)
            param(2).Value = ACL_DATE
            param(3) = New SqlClient.SqlParameter("@ACL_ACTION", SqlDbType.VarChar, 20)
            param(3).Value = ACL_ACTION
            param(4) = New SqlClient.SqlParameter("@ACL_URL", SqlDbType.VarChar, 150)
            param(4).Value = ACL_URL
            param(5) = New SqlClient.SqlParameter("@ACL_METHOD", SqlDbType.VarChar, 5)
            param(5).Value = ACL_METHOD
            param(6) = New SqlClient.SqlParameter("@ACL_PERSISTENT_ID", SqlDbType.BigInt)
            param(6).Value = ACL_PERSISTENT_ID
            param(7) = New SqlClient.SqlParameter("@ACL_USER", SqlDbType.VarChar, 20)
            param(7).Value = ACL_USER
            param(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(8).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.SAVE_ENTERTAINER_API_CALL_LOG", param)
            If param(8).Value = 0 Then
                stTrans.Commit()
                ACL_ID = param(0).Value
                output.Add(String.Format("{0}", ACL_ID))
            Else
                stTrans.Rollback()
                output.Add(String.Format("{0}", 0))
            End If
        Catch ex As Exception
            output.Add(String.Format("{0}", 0))
            Errorlog(ex.Message, "API_CALL_LOG")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            SAVE_ENTERTAINER_API_CALL_LOG = output.ToArray
        End Try
    End Function
    Public Shared Function UPDATE_ENTERTAINER_API_CALL_LOG(ByRef ACL_ID As Long, ByVal ACL_RESPONSE_DATA As String) As Boolean
        UPDATE_ENTERTAINER_API_CALL_LOG = False
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACL_ID", SqlDbType.BigInt)
            param(0).Value = ACL_ID
            param(1) = New SqlClient.SqlParameter("@ACL_RESPONSE_DATA", SqlDbType.VarChar, 2000)
            param(1).Value = ACL_RESPONSE_DATA

            param(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(3).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.UPDATE_ENTERTAINER_API_CALL_LOG", param)
            If param(3).Value = 0 Then
                stTrans.Commit()
                UPDATE_ENTERTAINER_API_CALL_LOG = True
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "API_CALL_LOG")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Function SAVE_POINTS_REDEMPTION_ONLINE(ByVal p_stTrans As SqlTransaction) As Boolean
        Try
            Dim pParms(20) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PRO_ID", SqlDbType.BigInt)
            pParms(0).Direction = ParameterDirection.InputOutput
            pParms(0).Value = PRO_ID
            pParms(1) = New SqlClient.SqlParameter("@PRO_PRO_ID", SqlDbType.BigInt)
            pParms(1).Value = PRO_PRO_ID
            pParms(2) = New SqlClient.SqlParameter("@PRO_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = PRO_BSU_ID
            pParms(3) = New SqlClient.SqlParameter("@PRO_ACD_ID", SqlDbType.Int)
            pParms(3).Value = PRO_ACD_ID
            pParms(4) = New SqlClient.SqlParameter("@PRO_DATE", SqlDbType.DateTime)
            pParms(4).Value = PRO_DATE
            pParms(5) = New SqlClient.SqlParameter("@PRO_SOURCE", SqlDbType.VarChar, 15)
            pParms(5).Value = PRO_SOURCE
            pParms(6) = New SqlClient.SqlParameter("@PRO_STU_TYPE", SqlDbType.VarChar, 2)
            pParms(6).Value = PRO_STU_TYPE
            pParms(7) = New SqlClient.SqlParameter("@PRO_STU_ID", SqlDbType.BigInt)
            pParms(7).Value = PRO_STU_ID
            pParms(8) = New SqlClient.SqlParameter("@PRO_DRCR", SqlDbType.VarChar, 2)
            pParms(8).Value = PRO_DRCR
            pParms(9) = New SqlClient.SqlParameter("@PRO_AMOUNT", SqlDbType.Decimal)
            pParms(9).Value = PRO_AMOUNT
            pParms(10) = New SqlClient.SqlParameter("@PRO_NARRATION", SqlDbType.VarChar, 500)
            pParms(10).Value = PRO_NARRATION
            pParms(11) = New SqlClient.SqlParameter("@PRO_bPOSTED", SqlDbType.Bit)
            pParms(11).Value = False 'PRO_bPOSTED
            pParms(12) = New SqlClient.SqlParameter("@PRO_STATUS", SqlDbType.VarChar, 10)
            pParms(12).Value = PRO_STATUS
            pParms(13) = New SqlClient.SqlParameter("@PRO_LOG_USERNAME", SqlDbType.VarChar, 20)
            pParms(13).Value = PRO_LOG_USERNAME

            pParms(14) = New SqlClient.SqlParameter("@PRO_API_CMD", SqlDbType.VarChar, 100)
            pParms(14).Value = API_CMD
            pParms(15) = New SqlClient.SqlParameter("@PRO_API_PERSISTENT_ID", SqlDbType.BigInt)
            pParms(15).Value = PERSISTENT_ID
            pParms(16) = New SqlClient.SqlParameter("@PRO_API_DATA_AVAILABLE_POINTS", SqlDbType.BigInt)
            pParms(16).Value = PRO_API_DATA_AVAILABLE_POINTS
            pParms(17) = New SqlClient.SqlParameter("@PRO_API_DATA_REDEEMABLE_POINTS", SqlDbType.BigInt)
            pParms(17).Value = PRO_API_DATA_REDEEMABLE_POINTS
            pParms(18) = New SqlClient.SqlParameter("@PRO_API_DATA_REDEEMABLE_AMOUNT", SqlDbType.Decimal)
            pParms(18).Value = PRO_API_DATA_REDEEMABLE_AMOUNT
            pParms(19) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(19).Direction = ParameterDirection.ReturnValue


            SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.SAVE_POINTS_REDEMPTION_ONLINE", pParms)
            If pParms(19).Value = 0 Then
                PRO_ID = pParms(0).Value
            Else
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function UPDATE_POINTS_REDEMPTION_ONLINE(ByRef stTrans As SqlTransaction) As Boolean
        UPDATE_POINTS_REDEMPTION_ONLINE = False
        Try
            Dim pParms(15) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PRO_ID", SqlDbType.BigInt)
            pParms(0).Value = PRO_ID
            pParms(1) = New SqlClient.SqlParameter("@PRO_API_CMD", SqlDbType.VarChar, 100)
            pParms(1).Value = API_CMD
            pParms(2) = New SqlClient.SqlParameter("@PRO_API_SUCCESS", SqlDbType.VarChar, 5)
            pParms(2).Value = PRO_API_SUCCESS
            pParms(3) = New SqlClient.SqlParameter("@PRO_API_MESSAGE", SqlDbType.VarChar, 300)
            pParms(3).Value = PRO_API_MESSAGE
            pParms(4) = New SqlClient.SqlParameter("@PRO_API_HTTP_RESPONSE", SqlDbType.VarChar, 50)
            pParms(4).Value = PRO_API_HTTP_RESPONSE
            pParms(5) = New SqlClient.SqlParameter("@PRO_API_CODE", SqlDbType.VarChar, 10)
            pParms(5).Value = PRO_API_CODE
            pParms(6) = New SqlClient.SqlParameter("@PRO_API_DATA_CODE", SqlDbType.VarChar, 10)
            pParms(6).Value = PRO_API_DATA_CODE
            pParms(7) = New SqlClient.SqlParameter("@PRO_API_DATA_MESSAGE", SqlDbType.VarChar, 300)
            pParms(7).Value = PRO_API_DATA_MESSAGE
            pParms(8) = New SqlClient.SqlParameter("@PRO_API_DATA_TRANSACTION_ID", SqlDbType.VarChar, 50)
            pParms(8).Value = PRO_API_DATA_TRANSACTION_ID
            pParms(9) = New SqlClient.SqlParameter("@PRO_API_DATA_STATUS", SqlDbType.VarChar, 50)
            pParms(9).Value = PRO_API_DATA_STATUS
            pParms(10) = New SqlClient.SqlParameter("@PRO_AVAILABLE_POINTS_AFTER_REDEEM", SqlDbType.BigInt)
            pParms(10).Value = PRO_API_DATA_AVAILABLE_POINTS
            pParms(11) = New SqlClient.SqlParameter("@PRO_REDEEMABLE_POINTS_AFTER_REDEEM", SqlDbType.BigInt)
            pParms(11).Value = PRO_API_DATA_REDEEMABLE_POINTS
            pParms(12) = New SqlClient.SqlParameter("@PRO_REDEEMABLE_AMOUNT_AFTER_REDEEM", SqlDbType.Decimal)
            pParms(12).Value = PRO_API_DATA_REDEEMABLE_AMOUNT
            pParms(13) = New SqlClient.SqlParameter("@bUPDATE_POINTS", SqlDbType.Bit)
            pParms(13).Value = bUpdate_Points
            pParms(14) = New SqlClient.SqlParameter("@PRO_FCL_ID", SqlDbType.BigInt)
            pParms(14).Value = PRO_FCL_ID
            pParms(15) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(15).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[UPDATE_POINTS_REDEMPTION_ONLINE]", pParms)
            If pParms(15).Value = 0 Then
                UPDATE_POINTS_REDEMPTION_ONLINE = True
            End If
        Catch ex As Exception
        End Try
    End Function

    Public Function GENERATE_ACCESS_TOKEN(ByRef pObjcls As clsRewards, ByRef pTOKEN As String) As Boolean
        GENERATE_ACCESS_TOKEN = False
        pTOKEN = "Failed to generate Token"
        Try
            pObjcls.GET_API_CALL_PARAMETERS("GETTOKEN") 'function gets the web api call parameters
            
            'Function to log the api call
            Dim ACL_ID As Long = 0
            Dim str = clsRewards.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, HttpContext.Current.Session("sBsuid"), DateTime.Now, "GETTOKEN", pObjcls.API_URI, pObjcls.API_METHOD, pObjcls.CUSTOMER_ID, HttpContext.Current.Session("username"))
            Dim Apiuri As Uri = New Uri(pObjcls.API_URI)
            Dim client = New RestClient(Apiuri.AbsoluteUri)
            Dim APIRequest = New RestRequest(clsRewards.GetAPIMethod(pObjcls.API_METHOD))
            APIRequest.AddHeader("Accept", "*/*")
            APIRequest.AddHeader("TP_APPLICATION_KEY", pObjcls.API_TOKEN)
            Dim Response As IRestResponse = client.Execute(APIRequest)
            Try
                Dim responseData As String = Response.Content
                Dim jsonGetTokenResponse = JObject.Parse(responseData)
                Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000)) '--------updating the API call response data

                If Not jsonGetTokenResponse("status") Is Nothing Then
                    If jsonGetTokenResponse("status").ToObject(Of String)().ToLower = "true" Then 'if web api return with status=true message
                        If Not jsonGetTokenResponse("values") Is Nothing Then
                            Dim jsonGetTokenResponse_values = JObject.Parse(jsonGetTokenResponse("values").ToString())
                            pTOKEN = FeeCollection.ValidateValue(jsonGetTokenResponse_values.Property("CC_TOKEN"))
                            GENERATE_ACCESS_TOKEN = True
                        Else
                            Errorlog("FeeCollection.aspx.REDEEM_POINTS_CALL, Error:Unable to find item values in json response", "PHOENIX")
                            pTOKEN = "Unable to find item values in json response"
                            Exit Function
                        End If
                    Else
                        Dim ErrorMessage As String = jsonGetTokenResponse("message").ToObject(Of String)()
                        pTOKEN = ErrorMessage
                        Errorlog("clsRewards.vb.GENERATE_ACCESS_TOKEN, Error:" & ErrorMessage, "PHOENIX")
                        Exit Function
                    End If
                End If
            Catch ex As Exception
                Errorlog("FeeCollection.aspx.REDEEM_POINTS_CALL, Error:" & ex.Message, "PHOENIX")
            End Try
        Catch ex As Exception
            Errorlog("FeeCollection.aspx.REDEEM_POINTS_CALL, Error:" & ex.Message, "PHOENIX")
        End Try
    End Function
    Public Shared Function GetAPIMethod(ByVal strMethod As String) As Method
        GetAPIMethod = Method.GET
        Select Case strMethod
            Case "GET"
                GetAPIMethod = Method.GET
            Case "POST"
                GetAPIMethod = Method.POST
            Case "PUT"
                GetAPIMethod = Method.PUT
            Case "DELETE"
                GetAPIMethod = Method.DELETE
        End Select
    End Function

#Region "Public variables for POINTS_REDEMPTION_SUB_ONLINE"
    Private vPSS_ID As Long, vPSS_FEE_ID As Integer, vPSS_ORG_AMOUNT As Double, vPSS_AMOUNT As Double, vPSS_CURRENCY_AMOUNT As Double, vPSS_TAX_AMOUNT As Double
    Private vPSS_CANCEL_AMOUNT As Double
    Public Property PSS_ID() As Long
        Get
            Return vPSS_ID
        End Get
        Set(ByVal value As Long)
            vPSS_ID = value
        End Set
    End Property
    Public Property PSS_FEE_ID() As Integer
        Get
            Return vPSS_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vPSS_FEE_ID = value
        End Set
    End Property
    Public Property PSS_ORG_AMOUNT() As Double
        Get
            Return vPSS_ORG_AMOUNT
        End Get
        Set(ByVal value As Double)
            vPSS_ORG_AMOUNT = value
        End Set
    End Property
    Public Property PSS_AMOUNT() As Double
        Get
            Return vPSS_AMOUNT
        End Get
        Set(ByVal value As Double)
            vPSS_AMOUNT = value
        End Set
    End Property
    Public Property PSS_CURRENCY_AMOUNT() As Double
        Get
            Return vPSS_CURRENCY_AMOUNT
        End Get
        Set(ByVal value As Double)
            vPSS_CURRENCY_AMOUNT = value
        End Set
    End Property
    Public Property PSS_TAX_AMOUNT() As Double
        Get
            Return vPSS_TAX_AMOUNT
        End Get
        Set(ByVal value As Double)
            vPSS_TAX_AMOUNT = value
        End Set
    End Property
    Public Property PSS_CANCEL_AMOUNT() As Double
        Get
            Return vPSS_CANCEL_AMOUNT
        End Get
        Set(ByVal value As Double)
            vPSS_CANCEL_AMOUNT = value
        End Set
    End Property
#End Region

    Public Function SAVE_POINTS_REDEMPTION_SUB_ONLINE(ByVal p_stTrans As SqlTransaction) As Boolean
        Try
            Dim pParms(10) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PSS_ID", SqlDbType.BigInt)
            pParms(0).Direction = ParameterDirection.InputOutput
            pParms(0).Value = PSS_ID
            pParms(1) = New SqlClient.SqlParameter("@PSS_PRO_ID", SqlDbType.BigInt)
            pParms(1).Value = PRO_ID
            pParms(2) = New SqlClient.SqlParameter("@PSS_FSH_ID", SqlDbType.BigInt)
            pParms(2).Value = 0
            pParms(3) = New SqlClient.SqlParameter("@PSS_FEE_ID", SqlDbType.Int)
            pParms(3).Value = PSS_FEE_ID
            pParms(4) = New SqlClient.SqlParameter("@PSS_ORG_AMOUNT", SqlDbType.Decimal)
            pParms(4).Value = PSS_ORG_AMOUNT
            pParms(5) = New SqlClient.SqlParameter("@PSS_AMOUNT", SqlDbType.Decimal)
            pParms(5).Value = PSS_AMOUNT
            pParms(6) = New SqlClient.SqlParameter("@PSS_CURRENCY_AMOUNT", SqlDbType.Decimal)
            pParms(6).Value = PSS_CURRENCY_AMOUNT
            pParms(7) = New SqlClient.SqlParameter("@PSS_TAX_AMOUNT", SqlDbType.Decimal)
            pParms(7).Value = PSS_TAX_AMOUNT
            pParms(8) = New SqlClient.SqlParameter("@PSS_CANCEL_AMOUNT", SqlDbType.Decimal)
            pParms(8).Value = PSS_CANCEL_AMOUNT

            pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue


            SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.SAVE_POINTS_REDEMPTION_SUB_ONLINE", pParms)
            If pParms(9).Value = 0 Then
                PSS_ID = pParms(0).Value
            Else
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

#Region "public variables for SaveFEECOLLECTION_H"

#End Region
    Public Function SAVE_FEECOLLECTION_H(ByVal p_stTrans As SqlTransaction) As Boolean
        Try
            Dim pParms(4) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PRO_ID", SqlDbType.BigInt)
            pParms(0).Value = PRO_ID
            pParms(1) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
            pParms(1).Direction = ParameterDirection.Output
            pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            pParms(2).Direction = ParameterDirection.Output
            pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.SAVE_FEECOLLECTION_H_REWARDS_REDEMPTION", pParms)
            If pParms(3).Value = 0 Then
                PRO_ID = pParms(0).Value
            Else
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Property AP_BEFORE As Integer
    Public Property RP_BEFORE As Integer
    Public Property AP_AFTER As Integer
    Public Property RP_AFTER As Integer
    Public Property RA_BEFORE As Double
    Public Property RA_AFTER As Double
    Public Property REDEEMED_POINTS As Integer
    Public Property REDEEMED_AMOUNT As Double
    Public Sub FETCH_POINTS_SUMMARY(ByVal PRO_ID As Int64)
        Try
            AP_BEFORE = 0 : AP_AFTER = 0 : RP_AFTER = 0 : RP_BEFORE = 0 : RA_AFTER = 0 : RA_BEFORE = 0
            Dim strConn As String = ""
            Dim Qry As New StringBuilder
            Qry.Append("SELECT ISNULL(PRO_API_DATA_AVAILABLE_POINTS, 0) AS AP_BEFORE, ISNULL(PRO_API_DATA_REDEEMABLE_POINTS, 0) AS RP_BEFORE,")
            Qry.Append("ISNULL(PRO_API_DATA_REDEEMABLE_AMOUNT, 0) AS RA_BEFORE, ISNULL(PRO_AVAILABLE_POINTS_AFTER_REDEEM, 0) AP_AFTER,")
            Qry.Append("ISNULL(PRO_REDEEMABLE_POINTS_AFTER_REDEEM, 0) RP_AFTER, ISNULL(PRO_REDEEMABLE_AMOUNT_AFTER_REDEEM, 0) RA_AFTER, ")
            Qry.Append("ISNULL(PRO_API_DATA_AVAILABLE_POINTS, 0) - ISNULL(PRO_AVAILABLE_POINTS_AFTER_REDEEM, 0) AS REDEEMED_POINTS, ISNULL(SUM(PRO_AMOUNT),0) AS REDEEMED_AMOUNT ")
            Qry.Append("FROM OASIS_FEES.FEES.POINTS_REDEMPTION_ONLINE WHERE ISNULL(PRO_PRO_ID, PRO_ID) = @PRO_ID ")
            Qry.Append("GROUP BY ISNULL(PRO_API_DATA_AVAILABLE_POINTS, 0), ISNULL(PRO_API_DATA_REDEEMABLE_POINTS, 0), ISNULL(PRO_API_DATA_REDEEMABLE_AMOUNT, 0), ")
            Qry.Append("ISNULL(PRO_AVAILABLE_POINTS_AFTER_REDEEM, 0), ISNULL(PRO_REDEEMABLE_POINTS_AFTER_REDEEM, 0), ISNULL(PRO_REDEEMABLE_AMOUNT_AFTER_REDEEM, 0) ")
            strConn = ConnectionManger.GetOASISConnectionString
            Using conn As New SqlConnection()
                conn.ConnectionString = strConn
                Using cmd As New SqlCommand()
                    cmd.CommandText = Qry.ToString
                    cmd.Parameters.AddWithValue("@PRO_ID", PRO_ID)

                    cmd.Connection = conn
                    conn.Open()
                    Dim sql As String = UtilityObj.CommandAsSql(cmd)
                    Using sdr As SqlDataReader = cmd.ExecuteReader()
                        While sdr.Read()
                            AP_BEFORE = FeeCollection.GetDoubleVal(sdr("AP_BEFORE").ToString)
                            RP_BEFORE = FeeCollection.GetDoubleVal(sdr("RP_BEFORE").ToString)
                            AP_AFTER = FeeCollection.GetDoubleVal(sdr("AP_AFTER").ToString)
                            RP_AFTER = FeeCollection.GetDoubleVal(sdr("RP_AFTER").ToString)
                            RA_BEFORE = FeeCollection.GetDoubleVal(sdr("RA_BEFORE").ToString)
                            RA_AFTER = FeeCollection.GetDoubleVal(sdr("RA_AFTER").ToString)
                            REDEEMED_POINTS = FeeCollection.GetDoubleVal(sdr("REDEEMED_POINTS").ToString)
                            REDEEMED_AMOUNT = FeeCollection.GetDoubleVal(sdr("REDEEMED_AMOUNT").ToString)
                        End While
                    End Using
                    conn.Close()
                End Using
            End Using
        Catch ex As Exception
            AP_BEFORE = 0 : AP_AFTER = 0 : RP_AFTER = 0 : RP_BEFORE = 0 : RA_AFTER = 0 : RA_BEFORE = 0
        End Try
    End Sub

End Class
