Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Threading
Imports System.Threading.Tasks
Public Class feeGenerateTransportFeeCharge

    Private privatePercentComplete As Double
    Public Property PercentComplete() As Double
        Get
            Return privatePercentComplete
        End Get
        Protected Set(ByVal value As Double)
            privatePercentComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is still executing.
    ''' </summary>
    Private privateIsRunning As Boolean
    Public Property IsRunning() As Boolean
        Get
            Return privateIsRunning
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsRunning = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is done running.
    ''' </summary>
    Private privateIsComplete As Boolean
    Public Property IsComplete() As Boolean
        Get
            Return privateIsComplete
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsComplete = value
        End Set
    End Property

    Private privateIsSuccess As Boolean
    Public Property IsSuccess() As Boolean
        Get
            Return privateIsSuccess
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsSuccess = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a brief description of what is currently being worked on, to update the end-user.
    ''' </summary>
    Public Property WorkDescription() As String

    ''' <summary>
    ''' Event for when progress has changed.
    ''' </summary>
    Public Event ProgressChanged As EventHandler(Of ProgressChangedEventArgs)

    ''' <summary>
    ''' Event for the the long-running process is complete.
    ''' </summary>
    Public Event ProcessComplete As EventHandler
    ''' <summary>
    ''' Private handle to the task that is currently execute the long-running process.
    ''' </summary>
    Private task As Task

    Public Sub UpdatePercent(ByVal newPercentComplete As Double, ByVal workDescription As String)
        ' Update the properties for any callers casually checking the status.
        Me.PercentComplete = newPercentComplete
        Me.WorkDescription = workDescription
        If newPercentComplete = 100 Then
            IsComplete = True
        End If
        ' Fire the event for any callers who actively want to be notifed.
        RaiseEvent ProgressChanged(Me, New ProgressChangedEventArgs(newPercentComplete, IsComplete, workDescription))
    End Sub

    Public Sub StartProcess()
        IsComplete = False
        IsRunning = True
        IsSuccess = True
        PercentComplete = 0
        UpdatePercent(0, "Please wait..")
    End Sub
    Public Sub EndProcess(ByVal status As Boolean, ByVal msg As String)
        UpdatePercent(100, msg)
        IsSuccess = status
        Thread.Sleep(1200)
        RaiseEvent ProcessComplete(Me, New EventArgs())
        IsRunning = False
    End Sub

    Public Sub GENERATE_TRANSPORT_FEE_CHARGE_FOR_BSU(ByVal p_BSU_ID As String, _
        ByVal p_ACD_ID As Integer, ByVal p_Dt As String, ByVal p_STUDENT_BSU_ID As String, _
        ByVal p_STUDENT_STU_IDS As String)

        ' Kick off the actual, private long-running process in a new Task
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Try
            Dim flag As Boolean = True
            Dim Status As Integer = 1000
            task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                    Dim objss As New AjaxServices
                                                                    Try
                                                                        objss.StartProcess()
                                                                        objss.InitProgress(p_BSU_ID, p_STUDENT_BSU_ID, p_Dt, True)
                                                                        objConn.Open()
                                                                        transaction = objConn.BeginTransaction("SampleTransaction")

                                                                        Status = F_GenerateTransportFeeCharge_For_BSU(p_BSU_ID, p_ACD_ID, p_Dt, p_STUDENT_BSU_ID, p_STUDENT_STU_IDS, objConn, transaction)

                                                                        If Status <> 0 Then
                                                                            transaction.Rollback()
                                                                            objss.EndProcess(False, UtilityObj.getErrorMessage(Status))
                                                                        Else
                                                                            transaction.Commit()
                                                                            objss.EndProcess(True, "Charging completed successfully")
                                                                        End If
                                                                    Catch ex As Exception
                                                                        objss.EndProcess(False, "Charging Failed")
                                                                    Finally
                                                                        If objConn.State = ConnectionState.Open Then
                                                                            objConn.Close()
                                                                        End If
                                                                    End Try
                                                                End Sub)
            If task.IsCompleted Then
                Dim a = task.Status
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Function F_GenerateTransportFeeCharge_For_BSU(ByVal p_BSU_ID As String, _
        ByVal p_ACD_ID As Integer, ByVal p_Dt As String, ByVal p_STUDENT_BSU_ID As String, _
        ByVal p_STUDENT_STU_IDS As String, ByVal p_Conn As SqlConnection, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(1).Value = p_ACD_ID
        pParms(2) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(2).Value = p_Dt
        pParms(3) = New SqlClient.SqlParameter("@STUDENT_BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_STUDENT_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@STUDENT_STU_IDS", SqlDbType.VarChar, 500)
        pParms(5).Value = p_STUDENT_STU_IDS
        Dim retval As Integer
        Dim cmd As New SqlCommand()
        cmd.Parameters.Add(pParms(0))
        cmd.Parameters.Add(pParms(1))
        cmd.Parameters.Add(pParms(2))

        cmd.Parameters.Add(pParms(3))
        cmd.Parameters.Add(pParms(4))
        cmd.Parameters.Add(pParms(5))
        cmd.CommandText = "FEES.F_GenerateTransportFeeCharge_For_BSU"
        cmd.CommandTimeout = 0
        cmd.connection = p_Conn
        cmd.Transaction = p_stTrans
        cmd.CommandType = CommandType.StoredProcedure
        'retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_GenerateTransportFeeCharge_For_BSU", pParms)
        retval = cmd.ExecuteNonQuery
        F_GenerateTransportFeeCharge_For_BSU = pParms(4).Value
    End Function


    Public Shared Function Get_BSU_Provider(ByVal p_BSU_ID As String, ByVal p_ACD_ID As String) As String
        Dim sql_query As String = " SELECT SVB_PROVIDER_BSU_ID FROM SERVICES_BSU_M " _
        & " WHERE SVB_BSU_ID = '" & p_BSU_ID & "' AND SVB_ACD_ID ='" & p_ACD_ID & "' and SVB_SVC_ID=1  "

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function


End Class
