Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
''' The class Created for handling Performa Invoice
''' 
''' Author : SHIJIN C A
''' Date : 03-MAY-2008 
''' </summary>
''' <remarks></remarks>
Public Class FEEPERFORMAINVOICE

    ''' <summary>
    ''' Internal Variables used in the class to Store poperties
    ''' </summary>
    ''' <remarks></remarks>
    Dim vFPH_INVOICENO As String
    Dim vFPH_REMARKS As String
    Dim vFPH_ID As Integer
    Dim vFPH_BSU_ID As String
    Dim vFPH_ACD_ID As Integer
    Dim vFPH_STU_ID As Integer
    Dim vFPH_STU_NO As String
    Dim vFPH_STU_NAME As String
    Dim vFPH_GRD_ID As String
    Dim vFPH_STUD_TYPE As STUDENTTYPE
    Dim vFPH_COMP_ID As Integer
    Dim vFPH_COMP_NAME As String
    Dim vFPH_DT As DateTime
    Dim vTOTAL_AMOUNT As Double
    Dim vFPH_PrintCount As Integer
    Public bDelete As Boolean
    Public FPH_bPrintYearly As Boolean
    Public bEdit As Boolean
    Dim vSTD_SUB_DET As ArrayList
    Dim vFEE_TYPES As ArrayList
    Dim vSelProformaDur As SELECTEDPROFORMADURATION
    Dim vFPH_INV_CAPTION As String
    Dim vFPH_IsServiceINV As Boolean
    Dim vFPH_IsNewStudent As Boolean
    Dim vFPH_bAccountStatus As Boolean
    Dim vFPH_COMP_DETAILS As String
    Dim vFPH_IsTuitionINV As Boolean
    Dim vFPH_CURRENCY As String

    Dim vFPH_FEE_TYPES As String
    Dim vFPH_IsEXCL_TC As Boolean
    Dim vFPH_EXL_TYPE As String
    Dim vFPH_IsMULTI_SEL As Boolean
    ''' <summary>
    ''' Stores the various FEE Types Selected
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FEE_TYPES() As ArrayList
        Get
            Return vFEE_TYPES
        End Get
        Set(ByVal value As ArrayList)
            vFEE_TYPES = value
        End Set
    End Property

    Public Property ProformaDuration() As SELECTEDPROFORMADURATION
        Get
            Return vSelProformaDur
        End Get
        Set(ByVal value As SELECTEDPROFORMADURATION)
            vSelProformaDur = value
        End Set
    End Property

    ''' <summary>
    ''' Stores the Split up details of the FEEs
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property STUDENT_SUBDETAILS() As ArrayList
        Get
            Return vSTD_SUB_DET
        End Get
        Set(ByVal value As ArrayList)
            vSTD_SUB_DET = value
        End Set
    End Property


    ''' <summary>
    ''' The Total Fee of the student
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TOTAL_AMOUNT() As Double
        Get
            Return vTOTAL_AMOUNT
        End Get
        Set(ByVal value As Double)
            vTOTAL_AMOUNT = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Student Type
    ''' Student - S
    ''' Enquiry - E
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_STUD_TYPE() As STUDENTTYPE
        Get
            Return vFPH_STUD_TYPE
        End Get
        Set(ByVal value As STUDENTTYPE)
            vFPH_STUD_TYPE = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Unique ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_ID() As Integer
        Get
            Return vFPH_ID
        End Get
        Set(ByVal value As Integer)
            vFPH_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Company ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_COMP_ID() As Integer
        Get
            Return vFPH_COMP_ID
        End Get
        Set(ByVal value As Integer)
            vFPH_COMP_ID = value
        End Set
    End Property
    Public Property FPH_IsServiceINV() As Boolean
        Get
            Return vFPH_IsServiceINV
        End Get
        Set(ByVal value As Boolean)
            vFPH_IsServiceINV = value
        End Set
    End Property
    Public Property FPH_IsTuitionINV() As Boolean
        Get
            Return vFPH_IsTuitionINV
        End Get
        Set(ByVal value As Boolean)
            vFPH_IsTuitionINV = value
        End Set
    End Property

    Public Property FPH_IsNewStudent() As Boolean
        Get
            Return vFPH_IsNewStudent
        End Get
        Set(ByVal value As Boolean)
            vFPH_IsNewStudent = value
        End Set
    End Property

    Public Property FPH_bAccountStatus() As Boolean
        Get
            Return vFPH_bAccountStatus
        End Get
        Set(ByVal value As Boolean)
            vFPH_bAccountStatus = value
        End Set
    End Property


    Public Property FPH_COMP_DETAILS() As String
        Get
            Return vFPH_COMP_DETAILS
        End Get
        Set(ByVal value As String)
            vFPH_COMP_DETAILS = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the COMPANY NAME
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_COMP_NAME() As String
        Get
            Return vFPH_COMP_NAME
        End Get
        Set(ByVal value As String)
            vFPH_COMP_NAME = value
        End Set
    End Property

    Public Property FPH_INVOICE_CAPTION() As String
        Get
            Return vFPH_INV_CAPTION
        End Get
        Set(ByVal value As String)
            vFPH_INV_CAPTION = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Business Unit ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_STU_NAME() As String
        Get
            Return vFPH_STU_NAME
        End Get
        Set(ByVal value As String)
            vFPH_STU_NAME = value
        End Set
    End Property

    Public Property FPH_STU_NO() As String
        Get
            Return vFPH_STU_NO
        End Get
        Set(ByVal value As String)
            vFPH_STU_NO = value
        End Set
    End Property
    ''' <summary>
    ''' Gets or Sets the Business Unit ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_BSU_ID() As String
        Get
            Return vFPH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFPH_BSU_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Grade ID of the Enquiry
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_GRD_ID() As String
        Get
            Return vFPH_GRD_ID
        End Get
        Set(ByVal value As String)
            vFPH_GRD_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Academic Year ID 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_ACD_ID() As Integer
        Get
            Return vFPH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFPH_ACD_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Student ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_STU_ID() As Integer
        Get
            Return vFPH_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFPH_STU_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets Date on which the Performa Invoice is saved
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_DT() As DateTime
        Get
            Return vFPH_DT
        End Get
        Set(ByVal value As DateTime)
            vFPH_DT = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the Print Count 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_PrintCount() As Integer
        Get
            Return vFPH_PrintCount
        End Get
        Set(ByVal value As Integer)
            vFPH_PrintCount = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the invoice number for the object created
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPH_INVOICENO() As String
        Get
            Return vFPH_INVOICENO
        End Get
        Set(ByVal value As String)
            vFPH_INVOICENO = value
        End Set
    End Property

    Public Property FPH_REMARKS() As String
        Get
            Return vFPH_REMARKS
        End Get
        Set(ByVal value As String)
            vFPH_REMARKS = value
        End Set
    End Property
    Private Shared pbTAXable As Boolean
    Public Shared Property bTAXable() As Boolean
        Get
            Return pbTAXable
        End Get
        Set(ByVal value As Boolean)
            pbTAXable = value
        End Set
    End Property
    Public Function Delete() As Boolean
        bDelete = True
        Delete = bDelete
    End Function

    Private pSTU_BSU_ID As String
    Public Property STU_BSU_ID() As String
        Get
            Return pSTU_BSU_ID
        End Get
        Set(ByVal value As String)
            pSTU_BSU_ID = value
        End Set
    End Property
    Private pSTU_ACD_ID As Integer
    Public Property STU_ACD_ID() As Integer
        Get
            Return pSTU_ACD_ID
        End Get
        Set(ByVal value As Integer)
            pSTU_ACD_ID = value
        End Set
    End Property
    Private pSTU_ID As Integer
    Public Property STU_ID() As Integer
        Get
            Return pSTU_ID
        End Get
        Set(ByVal value As Integer)
            pSTU_ID = value
        End Set
    End Property
    Private pSTU_TYPE As String
    Public Property STU_TYPE() As String
        Get
            Return pSTU_TYPE
        End Get
        Set(ByVal value As String)
            pSTU_TYPE = value
        End Set
    End Property
    Private pSTU_GRD_ID As String
    Public Property STU_GRD_ID() As String
        Get
            Return pSTU_GRD_ID
        End Get
        Set(ByVal value As String)
            pSTU_GRD_ID = value
        End Set
    End Property
    Private pSTU_NO As String
    Public Property STU_NO() As String
        Get
            Return pSTU_NO
        End Get
        Set(ByVal value As String)
            pSTU_NO = value
        End Set
    End Property
    Private pSTU_NAME As String
    Public Property STU_NAME() As String
        Get
            Return pSTU_NAME
        End Get
        Set(ByVal value As String)
            pSTU_NAME = value
        End Set
    End Property
    Public Property FPH_CURRENCY() As String
        Get
            Return vFPH_CURRENCY
        End Get
        Set(ByVal value As String)
            vFPH_CURRENCY = value
        End Set
    End Property

    Public Property FPH_FEE_TYPES() As String
        Get
            Return vFPH_FEE_TYPES
        End Get
        Set(ByVal value As String)
            vFPH_FEE_TYPES = value
        End Set
    End Property

    Public Property FPH_EXL_TYPE() As String
        Get
            Return vFPH_EXL_TYPE
        End Get
        Set(ByVal value As String)
            vFPH_EXL_TYPE = value
        End Set
    End Property
    Public Property FPH_IsEXCL_TC() As Boolean
        Get
            Return vFPH_IsEXCL_TC
        End Get
        Set(ByVal value As Boolean)
            vFPH_IsEXCL_TC = value
        End Set
    End Property

    Public Property FPH_IsMULTI_SEL() As Boolean
        Get
            Return vFPH_IsMULTI_SEL
        End Get
        Set(ByVal value As Boolean)
            vFPH_IsMULTI_SEL = value
        End Set
    End Property

    ''' <summary>
    ''' Function used to Populate the FEE Types which is allowed for concession.
    '''
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function GetAllFEEType(ByVal ACD_ID As String, ByVal BSU_ID As String, ByVal COMP_ID As Int16, Optional ByVal IsServiceType As Boolean = False, Optional ByVal IsOnlyFeeSetup As Boolean = False, Optional ByVal IsOnlyTuitionFee As Boolean = False) As DataTable
        Dim dsData As DataSet
        Dim pParms(5) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@ACD_ID", ACD_ID, SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@IsServiceType", IsServiceType, SqlDbType.Bit)
        pParms(3) = Mainclass.CreateSqlParameter("@IsOnlyFeeSetup", IsOnlyFeeSetup, SqlDbType.Bit)
        pParms(4) = Mainclass.CreateSqlParameter("@IsOnlyTuitionFee", IsOnlyTuitionFee, SqlDbType.Bit)
        pParms(5) = Mainclass.CreateSqlParameter("@COMP_ID", COMP_ID, SqlDbType.Int)

        dsData = Mainclass.getDataSet("FEES.SP_FEE_TYPES_FOR_PERFORMA", pParms, ConnectionManger.GetOASIS_FEESConnectionString)

        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Function used to Populate the FEE Types which is allowed for concession.
    '''
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function GetStudentName(ByVal vSTUD_ID As String, ByRef STU_NO As String, ByVal bEnquiry As Boolean) As String
        Dim objReader As SqlDataReader
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String
            If bEnquiry Then
                sql_query = "SELECT ISNULL(dbo.ENQUIRY_M.EQM_APPLFIRSTNAME, '') + ' ' + " & _
                " ISNULL(dbo.ENQUIRY_M.EQM_APPLMIDNAME, '') + ' ' +  " & _
                " ISNULL(dbo.ENQUIRY_M.EQM_APPLLASTNAME, '') AS STUD_NAME, EQS_ID as STU_NO  " & _
                " FROM dbo.ENQUIRY_M INNER JOIN " & _
                " ENQUIRY_SCHOOLPRIO_S ON ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID = ENQUIRY_M.EQM_ENQID  " & _
                " WHERE ENQUIRY_SCHOOLPRIO_S.EQS_ID = " & vSTUD_ID
            Else
                sql_query = "SELECT isnull(STU_FIRSTNAME,'') + ' ' + isnull(STU_MIDNAME,'') + ' ' + isnull(STU_LASTNAME,'') STUD_NAME, STU_NO FROM dbo.STUDENT_M WITH(NOLOCK) WHERE STU_ID = " & vSTUD_ID
            End If
            objReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (objReader.Read())
                STU_NO = objReader("STU_NO")
                Return objReader("STUD_NAME").ToString
            End While
        End Using
        If objReader Is DBNull.Value Then
            Return ""
        End If
        Return ""
    End Function
    ''' <summary>
    ''' Populates the FEE Types allowed in the performa invoice for the specified company.
    '''
    ''' </summary>
    ''' <returns>DataTable containing Concession Type Details</returns>
    Public Shared Function GetFEETypeForCompany(ByVal COMP_ID As String) As DataTable
        Dim dsData As DataSet
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT DISTINCT FEE_ORDER, " & _
            " CAST(COMP_FEECOMPO_S.CIS_FEE_ID AS VARCHAR(10)) + '___' + " & _
            " CAST(FEE_ORDER as VARCHAR(10)) AS FEE_ID, FEE_DESCR FEE_DESCR " & _
            " FROM COMP_FEECOMPO_S LEFT OUTER JOIN" & _
            " OASIS_FEES.FEES.FEES_M WITH(NOLOCK) ON COMP_FEECOMPO_S.CIS_FEE_ID = FEE_ID" & _
            " WHERE(ISNULL(COMP_FEECOMPO_S.CIS_bPreformaInvoice,0) = 1 AND " & _
            " COMP_FEECOMPO_S.CIS_COMP_ID = '" & COMP_ID & "') ORDER BY FEE_ORDER "
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetSelectedMonthsDetails(ByVal FPH_ID As Integer) As ArrayList
        Dim arrList As New ArrayList
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim sql_query As String
            sql_query = "SELECT FPS_REF_ID FROM FEES.FEE_PERFORMAINVOICE_S WHERE FPS_FPH_ID=" & FPH_ID
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
            While (drReader.Read())
                arrList.Add(drReader("FPS_REF_ID"))
            End While
        End Using
        Return arrList
    End Function

    Public Shared Function GetDetails(ByVal vINV_NO As String, ByVal bENQ As Boolean, ByRef FPH_ID As Integer, ByVal BSU_ID As String, ByVal ACD_ID As String) As Hashtable
        Dim htTab As New Hashtable
        'Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim dsData As New DataSet
        Dim pParms(5) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@FPH_BSU_ID", BSU_ID, SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@FPH_ACD_ID", ACD_ID, SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@FPH_INVOICE_NO", vINV_NO, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@bENQUIRY", bENQ, SqlDbType.Bit)
        pParms(4) = Mainclass.CreateSqlParameter("@FETCH_ITEM", "HEADER", SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@FPH_ID", 0, SqlDbType.Int)
        dsData = Mainclass.getDataSet("FEES.FETCH_SAVED_PERFORMA", pParms, ConnectionManger.GetOASIS_FEESConnectionString)

        If dsData Is Nothing OrElse dsData.Tables(0).Rows.Count <= 0 Then
            Return Nothing
        End If
        For Each dr As DataRow In dsData.Tables(0).Rows
            Dim vFPH_ID As Integer = dr("FPH_ID")
            Dim vFEE_COMP_DET As New FEEPERFORMAINVOICE
            Dim arrSUBList As New ArrayList
            Dim total_amt As Double = 0
            Dim dsSUB As DataSet = Nothing
            vFEE_COMP_DET.FPH_ACD_ID = dr("FPH_ACD_ID")
            vFEE_COMP_DET.FPH_BSU_ID = dr("BSU_ID")
            vFEE_COMP_DET.FPH_COMP_ID = dr("FPH_COMP_ID")
            vFEE_COMP_DET.FPH_DT = dr("DATE")
            vFEE_COMP_DET.FPH_GRD_ID = NullCheck(dr("GRD_ID"))
            vFEE_COMP_DET.FPH_ID = vFPH_ID
            vFEE_COMP_DET.FPH_INVOICENO = dr("FPH_INOICENO")
            vFEE_COMP_DET.FPH_STU_ID = dr("FPD_STU_ID")
            vFEE_COMP_DET.FPH_STU_NAME = dr("STUD_NAME")
            vFEE_COMP_DET.FPH_STU_NO = NullCheck(dr("STU_NO"))
            vFEE_COMP_DET.FPH_COMP_NAME = dr("COMP_NAME")
            vFEE_COMP_DET.FPH_INVOICE_CAPTION = dr("FPH_INVOICE_CAPTION")
            vFEE_COMP_DET.FPH_IsServiceINV = dr("FPH_IsServiceINV")
            If dr("FPH_STU_TYPE").ToString().ToUpper() = "S" Then
                vFEE_COMP_DET.FPH_STUD_TYPE = STUDENTTYPE.STUDENT
            Else
                vFEE_COMP_DET.FPH_STUD_TYPE = STUDENTTYPE.ENQUIRY
            End If
            vFEE_COMP_DET.FPH_REMARKS = NullCheck(dr("FPH_REMARKS"))
            'vFEE_COMP_DET.FPH_PrintCount = dr("FPH_ID")
            vFEE_COMP_DET.FPH_IsNewStudent = dr("FPH_IsNewStudent") 'swapna
            vFEE_COMP_DET.FPH_bAccountStatus = dr("FPH_bAccountStatus")
            vFEE_COMP_DET.vFPH_COMP_DETAILS = dr("FPH_COMP_DETAILS") 'jacob on 25jul13
            vFEE_COMP_DET.FPH_CURRENCY = NullCheck(dr("FPH_CURRENCY")) 'Added by Jacob on 24/Jul/2019 for multicurrency

            Dim pParm(5) As SqlParameter
            pParm(0) = Mainclass.CreateSqlParameter("@FPH_BSU_ID", BSU_ID, SqlDbType.VarChar)
            pParm(1) = Mainclass.CreateSqlParameter("@FPH_ACD_ID", ACD_ID, SqlDbType.Int)
            pParm(2) = Mainclass.CreateSqlParameter("@FPH_INVOICE_NO", vINV_NO, SqlDbType.VarChar)
            pParm(3) = Mainclass.CreateSqlParameter("@bENQUIRY", bENQ, SqlDbType.Bit)
            pParm(4) = Mainclass.CreateSqlParameter("@FETCH_ITEM", "SUB", SqlDbType.VarChar)
            pParm(5) = Mainclass.CreateSqlParameter("@FPH_ID", vFEE_COMP_DET.FPH_ID, SqlDbType.Int)
            dsSUB = Mainclass.getDataSet("FEES.FETCH_SAVED_PERFORMA", pParm, ConnectionManger.GetOASIS_FEESConnectionString)
            If Not dsData Is Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                For Each drr As DataRow In dsSUB.Tables(0).Rows
                    Dim vFEE_DET_SUB As New FEEPERFORMANCEREVIEW_SUB
                    vFEE_DET_SUB.FPD_FPH_ID = vFPH_ID
                    vFEE_DET_SUB.FPD_ID = drr("FPD_ID")
                    vFEE_DET_SUB.FPD_FEE_DESCR = drr("FEE_DESCR")
                    vFEE_DET_SUB.FPD_AMOUNT = drr("FPD_AMOUNT")
                    vFEE_DET_SUB.FPD_FEE_ID = drr("FPD_FEE_ID")
                    vFEE_DET_SUB.FPD_EXG_RATE = drr("FPD_EXG_RATE")
                    vFEE_DET_SUB.FPD_EXG_AMOUNT = drr("FPD_EXG_AMOUNT")
                    total_amt += vFEE_DET_SUB.FPD_AMOUNT
                    arrSUBList.Add(vFEE_DET_SUB)
                Next
            End If

            vFEE_COMP_DET.STUDENT_SUBDETAILS = arrSUBList
            vFEE_COMP_DET.TOTAL_AMOUNT = total_amt
            htTab(vFEE_COMP_DET.FPH_STU_ID) = vFEE_COMP_DET
        Next

        Return htTab
    End Function

    Private Shared Function NullCheck(ByVal objVal As Object) As Object
        If objVal Is Nothing OrElse objVal Is DBNull.Value Then
            Return ""
        Else
            Return objVal
        End If
    End Function

    Public Function GetSubDetails(ByVal vFEE_ID As Integer) As FEEPERFORMANCEREVIEW_SUB
        If vSTD_SUB_DET Is Nothing Then Return Nothing
        Dim ienum As IEnumerator = vSTD_SUB_DET.GetEnumerator
        Dim SUB_DET As FEEPERFORMANCEREVIEW_SUB
        While (ienum.MoveNext())
            SUB_DET = ienum.Current
            If SUB_DET Is Nothing Then Continue While
            If SUB_DET.FPD_FEE_ID = vFEE_ID Then
                Return SUB_DET
            End If
        End While
        Return Nothing
    End Function

    ''' <summary>
    ''' Function used to Populate the Months in the Academic Year provided 
    ''' It Returns a data table containing all Months for the ACD_ID provided
    ''' from Table ACADEMIC_MonthS_S
    ''' </summary>
    ''' <param name="ACD_ID"></param>
    ''' <returns>DataTable containing the Months</returns>
    Public Shared Function PopulateMonthsInAcademicYear(ByVal ACD_ID As Integer, ByVal BSU_ID As String) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT TM.TRM_ID ,AMS_ID ,TM.TRM_DESCRIPTION ,AMS_MONTH , " & _
            "DATENAME(month, ISNULL(AM.AMS_FEE_DTFROM, AM.AMS_DTFROM)) + '-' + ltrim(str(datepart(yyyy,ISNULL(AM.AMS_FEE_DTFROM, AM.AMS_DTFROM)))) " & _
            " AS MONTH_DESCR FROM ACADEMIC_MonthS_S AS AM WITH ( NOLOCK ) LEFT OUTER JOIN " & _
            " TRM_M AS TM WITH ( NOLOCK ) ON AM.AMS_TRM_ID = TM.TRM_ID WHERE AMS_ACD_ID = " & ACD_ID & _
            " AND TRM_BSU_ID = '" & BSU_ID & "'"
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function PopulateTermsInAcademicYear(ByVal ACD_ID As Integer, ByVal BSU_ID As String) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT TRM_M.TRM_ID, TRM_M.TRM_DESCRIPTION FROM " & _
            " dbo.TRM_M WITH ( NOLOCK ) WHERE TRM_ACD_ID = " & ACD_ID & _
            " AND TRM_BSU_ID = '" & BSU_ID & "'"
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function


    ''' <summary>
    ''' Gets the Total Fee to Be paid by the student 
    ''' including the Normal Fees and other Service Fees
    ''' </summary>
    ''' <param name="STUD_ID">Student ID whose Fee details need to be retrieved</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetTotalFees(ByVal STUD_ID As Integer, ByVal STU_TYPE As STUDENTTYPE, ByVal ACD_ID As Integer, _
                                        ByVal GRD_ID As String, ByVal BSU_ID As String, ByVal str_Months As String, _
                                        ByVal str_feeTypes As String, ByVal selProDur As SELECTEDPROFORMADURATION, _
                                        ByRef total As Double, ByVal NextAcademicYear As Boolean, ByVal pExgRate As Double) As ArrayList
        Dim dsTotal As New DataSet
        Dim arrList As New ArrayList
        Dim vFEE_SUBDET As FEEPERFORMANCEREVIEW_SUB
        Dim cmd As SqlCommand

        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            cmd = New SqlCommand("[FEES].[F_GetFEE_PERFORMA_INVOICE]", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
            sqlpACD_ID.Value = ACD_ID
            cmd.Parameters.Add(sqlpACD_ID)

            Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 10)
            sqlpGRD_ID.Value = GRD_ID
            cmd.Parameters.Add(sqlpGRD_ID)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = STUD_ID
            cmd.Parameters.Add(sqlpSTU_ID)

            '1 - STUDENT
            '2 - ENQUIRY
            Dim str_STU_TYPE As String = String.Empty
            Select Case STU_TYPE
                Case 1
                    str_STU_TYPE = "S"
                Case 2
                    str_STU_TYPE = "E"
            End Select
            Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar)
            sqlpSTU_TYPE.Value = str_STU_TYPE  'STU_TYPE
            cmd.Parameters.Add(sqlpSTU_TYPE)

            Dim sqlpBTerm As New SqlParameter("@BTerm", SqlDbType.Bit)
            sqlpBTerm.Value = selProDur 'STU_TYPE
            cmd.Parameters.Add(sqlpBTerm)

            Dim sqlpDates As New SqlParameter("@Period", SqlDbType.VarChar)
            sqlpDates.Value = str_Months
            cmd.Parameters.Add(sqlpDates)

            Dim sqlpTEMPDOCNO As New SqlParameter("@TEMPDOCNO", SqlDbType.VarChar)
            sqlpTEMPDOCNO.Value = ""
            cmd.Parameters.Add(sqlpTEMPDOCNO)

            Dim sqlpFEETYPES As New SqlParameter("@FEETYPES", SqlDbType.VarChar)
            sqlpFEETYPES.Value = str_feeTypes
            cmd.Parameters.Add(sqlpFEETYPES)

            Dim sqlpNextAcademicYear As New SqlParameter("@bNextAcademicYear", SqlDbType.Bit)
            sqlpNextAcademicYear.Value = NextAcademicYear
            cmd.Parameters.Add(sqlpNextAcademicYear)


            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(dsTotal)
        End Using
        If dsTotal Is Nothing OrElse dsTotal.Tables.Count = 0 OrElse dsTotal.Tables(0).Rows.Count = 0 Then
            Return Nothing
        End If
        total = 0
        For Each dr As DataRow In dsTotal.Tables(0).Rows
            vFEE_SUBDET = New FEEPERFORMANCEREVIEW_SUB
            vFEE_SUBDET.FPD_FEE_ID = 0
            vFEE_SUBDET.FPD_FEE_ID = dr("FEE_ID")
            vFEE_SUBDET.FPD_FEE_DESCR = dr("FEE_DESCR")
            vFEE_SUBDET.FPD_AMOUNT = dr("AMOUNT")
            vFEE_SUBDET.FPD_REF_ID = dr("REF_ID")
            vFEE_SUBDET.SORT_DATE = dr("sortdate")
            vFEE_SUBDET.FPD_DESCRIPTION = dr("Description")
            vFEE_SUBDET.FPD_EXG_RATE = pExgRate 'Added by Jacob on 24/Jul/2019 for multicurrency
            total += dr("AMOUNT")
            arrList.Add(vFEE_SUBDET)
        Next
        Return arrList
    End Function


    ''' <summary>
    ''' Gets the Normal Fee(Fees other than Services) for the student ID that is passed
    ''' </summary>
    ''' <param name="STUD_ID">Student ID whose Fee details need to be retrieved</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetNormalFees(ByVal STUD_ID As Integer) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim sql_query As String = "SELECT STU_ID, FSP_FEE_ID AS FEE_ID, FDD_AMOUNT AS AMOUNT, " & _
            "FEE_DESCR FROM vw_OSO_FEES_GETNORMALFEES WHERE " & _
            " STU_ID = " & STUD_ID
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            Dim dttable As New DataTable
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Gets the Service FEE details for the student ID that is passed
    ''' </summary>
    ''' <param name="STUD_ID">Student ID whose Fee details need to be retrieved</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetServiceFees(ByVal STUD_ID As Integer) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT SSV_STU_ID AS STU_ID, SBR_RATE AS AMOUNT, FEE_DESCR, FEE_ID " & _
            " FROM vw_OSO_FEES_GETSERVICEFEES " & _
            " WHERE SSV_STU_ID = " & STUD_ID
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            Dim dttable As New DataTable
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetNewInvoiceNo(ByVal dt As DateTime, ByVal vBSU_ID As String, ByVal ACD_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As String
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[OASIS].[FEES].[ReturnNextNo]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpDOC_ID As New SqlParameter("@DOC_ID", SqlDbType.VarChar)
        sqlpDOC_ID.Value = "PINV"
        cmd.Parameters.Add(sqlpDOC_ID)

        Dim sqlpFPH_BSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_BSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpDOS_MONTH As New SqlParameter("@DOS_MONTH", SqlDbType.TinyInt)
        sqlpDOS_MONTH.Value = dt.Month
        cmd.Parameters.Add(sqlpDOS_MONTH)

        Dim sqlpDOS_YEAR As New SqlParameter("@DOS_YEAR", SqlDbType.Int)
        sqlpDOS_YEAR.Value = dt.Year
        cmd.Parameters.Add(sqlpDOS_YEAR)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NEW_INV_NO As New SqlParameter("@NextNO", SqlDbType.VarChar, 20)
        NEW_INV_NO.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NEW_INV_NO)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return ""
        Else
            Return NEW_INV_NO.Value
        End If

    End Function

    ''' <summary>
    ''' Saves the edited or Inserted details to the database
    ''' The Function recieves the Array of Details 
    ''' </summary>
    ''' <param name="studDetails">Student Details Array</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SaveDetails(ByVal vInvoiceNo As String, ByVal bAnual As Boolean, ByVal invCaption As String, _
                                       ByVal studDetails As Hashtable, ByVal dtSelectedDates As DataTable, ByVal dt As Date, _
                                       ByVal vBSU_ID As String, ByVal ACD_ID As Integer, ByVal user As String, ByVal conn As SqlConnection, _
                                       ByVal trans As SqlTransaction, ByRef arrInvNos As ArrayList, ByVal Regenerate As Boolean, _
                                       ByVal Remarks As String, ByVal IsServiceInv As Boolean, ByVal IsNewStudent As Boolean, _
                                       ByVal bAccStatus As Boolean, ByVal pCurrency As String, ByVal pExgRate As Double, Optional ByVal bXcludeAdvFees As Boolean = False, _
                                       Optional ByVal CompanyDetails As String = "", Optional ByVal FeeIDs As String = "", _
                                       Optional ByVal NextAcademicYear As Boolean = False, Optional ByVal IsTuitionINV As Boolean = False, _
                                       Optional ByVal bXcludeDue As Boolean = False) As Integer
        Dim iReturnvalue As Integer
        Dim bUpdateData As Boolean = False
        Dim NewACD_ID As Int16
        NewACD_ID = ACD_ID
        If vInvoiceNo = "" Then
            Dim sql As String
            NewACD_ID = ACD_ID
            If NextAcademicYear Then
                sql = "SELECT NEW.ACD_ID FROM OASIS.dbo.ACADEMICYEAR_D NEW WITH(NOLOCK) INNER JOIN ACADEMICYEAR_D OLD ON NEW.ACD_BSU_ID=OLD.ACD_BSU_ID " & _
                      " AND NEW.ACD_CLM_ID=OLD.ACD_CLM_ID AND NEW.ACD_ACY_ID=OLD.ACD_ACY_ID+1 WHERE OLD.ACD_ID = " & ACD_ID.ToString
                NewACD_ID = Mainclass.getDataValue(sql, "OASIS_FEESConnectionString")
            End If
            vInvoiceNo = GetNewInvoiceNo(dt, vBSU_ID, NewACD_ID, conn, trans)
        Else
            bUpdateData = True
        End If
        If vInvoiceNo = "" Then Return 302
        arrInvNos = New ArrayList

        Dim HasAdmissionFee As Boolean = False


        For Each STU_DET As FEEPERFORMAINVOICE In studDetails.Values
            If STU_DET Is Nothing Then
                Continue For
            End If
            Dim cmd As SqlCommand

            If STU_DET.bEdit = False AndAlso STU_DET.bDelete Then Continue For

            If STU_DET.bEdit AndAlso STU_DET.bDelete = False Then
                If Regenerate = True Then

                    iReturnvalue = SavePerformaBalances(STU_DET.FPH_ID, dt, STU_DET.FPH_STU_ID, STU_DET.FPH_STUD_TYPE, _
                                    STU_DET.FPH_BSU_ID, conn, trans, IsNewStudent)
                    If iReturnvalue <> 0 Then
                        Exit For
                    End If
                End If
            End If
            If STU_DET.bEdit AndAlso STU_DET.bDelete = False Then Continue For

            If STU_DET.FPH_COMP_ID = "-1" And vInvoiceNo = "" Then
                vInvoiceNo = GetNewInvoiceNo(dt, vBSU_ID, NewACD_ID, conn, trans)
                If vInvoiceNo = "" Then Return -1
            End If

            If Not arrInvNos.Contains(vInvoiceNo) Then arrInvNos.Add(vInvoiceNo)

            cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_H", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFPH_BSU_ID As New SqlParameter("@FPH_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFPH_BSU_ID.Value = STU_DET.FPH_BSU_ID
            cmd.Parameters.Add(sqlpFPH_BSU_ID)

            Dim sqlpFPH_INOICENO As New SqlParameter("@FPH_INOICENO", SqlDbType.VarChar, 20)
            sqlpFPH_INOICENO.Value = vInvoiceNo 'STU_DET.FPH_INVOICENO
            cmd.Parameters.Add(sqlpFPH_INOICENO)

            Dim sqlpFPH_DT As New SqlParameter("@FPH_DT", SqlDbType.DateTime)
            sqlpFPH_DT.Value = dt
            cmd.Parameters.Add(sqlpFPH_DT)

            Dim sqlpFPH_ACD_ID As New SqlParameter("@FPH_ACD_ID", SqlDbType.Int)
            sqlpFPH_ACD_ID.Value = STU_DET.FPH_ACD_ID
            cmd.Parameters.Add(sqlpFPH_ACD_ID)

            Dim sqlpFPD_STU_ID As New SqlParameter("@FPD_STU_ID", SqlDbType.BigInt)
            sqlpFPD_STU_ID.Value = STU_DET.FPH_STU_ID
            cmd.Parameters.Add(sqlpFPD_STU_ID)

            Dim sqlpFPD_GRD_ID As New SqlParameter("@FPD_GRD_ID", SqlDbType.VarChar, 10)
            sqlpFPD_GRD_ID.Value = STU_DET.FPH_GRD_ID
            cmd.Parameters.Add(sqlpFPD_GRD_ID)

            Dim sqlpFPH_INVOICE_CAPTION As New SqlParameter("@FPH_INVOICE_CAPTION", SqlDbType.VarChar, 100)
            sqlpFPH_INVOICE_CAPTION.Value = invCaption
            cmd.Parameters.Add(sqlpFPH_INVOICE_CAPTION)

            Dim sqlpFPH_STU_TYPE As New SqlParameter("@FPH_STU_TYPE", SqlDbType.VarChar, 4)
            Select Case STU_DET.FPH_STUD_TYPE
                Case STUDENTTYPE.STUDENT
                    sqlpFPH_STU_TYPE.Value = "S"
                Case STUDENTTYPE.ENQUIRY
                    sqlpFPH_STU_TYPE.Value = "E"
            End Select
            cmd.Parameters.Add(sqlpFPH_STU_TYPE)

            Dim sqlpFPH_COMP_ID As New SqlParameter("@FPH_COMP_ID", SqlDbType.Int)
            sqlpFPH_COMP_ID.Value = STU_DET.FPH_COMP_ID
            cmd.Parameters.Add(sqlpFPH_COMP_ID)

            Dim sqlpFPH_SCH_ID As New SqlParameter("@FPH_SCH_ID", SqlDbType.Int)
            sqlpFPH_SCH_ID.Value = STU_DET.ProformaDuration
            cmd.Parameters.Add(sqlpFPH_SCH_ID)

            Dim sqlpuser As New SqlParameter("@user", SqlDbType.VarChar, 20)
            sqlpuser.Value = user
            cmd.Parameters.Add(sqlpuser)

            Dim sqlpFPH_PrintCount As New SqlParameter("@FPH_PrintCount", SqlDbType.Int)
            sqlpFPH_PrintCount.Value = STU_DET.FPH_PrintCount
            cmd.Parameters.Add(sqlpFPH_PrintCount)

            If STU_DET.bDelete Then
                Dim sqlpFPH_ID As New SqlParameter("@FPH_ID", SqlDbType.Int)
                sqlpFPH_ID.Value = STU_DET.FPH_ID
                cmd.Parameters.Add(sqlpFPH_ID)
            End If

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = STU_DET.bDelete
            cmd.Parameters.Add(sqlpbDelete)

            Dim sqlpFPH_bPrintYEarly As New SqlParameter("@FPH_bPrintYEarly", SqlDbType.Bit)
            sqlpFPH_bPrintYEarly.Value = bAnual
            cmd.Parameters.Add(sqlpFPH_bPrintYEarly)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            Dim NewFPH_ID As New SqlParameter("@NEW_FPH_ID", SqlDbType.Int)
            NewFPH_ID.Direction = ParameterDirection.Output
            cmd.Parameters.Add(NewFPH_ID)

            Dim SqlAdvBooking As New SqlParameter("@AdvBooking", SqlDbType.Bit)
            SqlAdvBooking.Value = False
            cmd.Parameters.Add(SqlAdvBooking)

            Dim SqlRemarks As New SqlParameter("@FPH_REMARKS", SqlDbType.VarChar)
            SqlRemarks.Value = Remarks
            cmd.Parameters.Add(SqlRemarks)

            Dim SqlIsServiceInv As New SqlParameter("@FPH_IsServiceINV", SqlDbType.Bit)
            SqlIsServiceInv.Value = IsServiceInv
            cmd.Parameters.Add(SqlIsServiceInv)

            Dim SqlIsNewStudent As New SqlParameter("@FPH_IsNewStudent", SqlDbType.Bit)
            SqlIsNewStudent.Value = IsNewStudent
            cmd.Parameters.Add(SqlIsNewStudent)

            Dim SqlFPH_bAccountStatus As New SqlParameter("@FPH_bAccountStatus", SqlDbType.Bit)
            SqlFPH_bAccountStatus.Value = bAccStatus
            cmd.Parameters.Add(SqlFPH_bAccountStatus)

            Dim SqlFPH_bXcludeAdvFees As New SqlParameter("@FPH_bXcludeAdvFees", SqlDbType.Bit)
            SqlFPH_bXcludeAdvFees.Value = bXcludeAdvFees
            cmd.Parameters.Add(SqlFPH_bXcludeAdvFees)

            Dim SqlCompanyDetails As New SqlParameter("@FPH_COMP_DETAILS", SqlDbType.VarChar, 300)
            SqlCompanyDetails.Value = CompanyDetails
            cmd.Parameters.Add(SqlCompanyDetails)

            Dim SqlFEEIDs As New SqlParameter("@FPH_FEE_IDS", SqlDbType.VarChar, 1000)
            SqlFEEIDs.Value = FeeIDs
            cmd.Parameters.Add(SqlFEEIDs)

            Dim sqlpNextAcademicYear As New SqlParameter("@bNextAcademicYear", SqlDbType.Bit)
            sqlpNextAcademicYear.Value = NextAcademicYear
            cmd.Parameters.Add(sqlpNextAcademicYear)

            Dim sqlpIsTuitionINV As New SqlParameter("@FPH_bTuitionINV", SqlDbType.Bit)
            sqlpIsTuitionINV.Value = IsTuitionINV
            cmd.Parameters.Add(sqlpIsTuitionINV)

            Dim sqlpbEXCLUDE_DUE As New SqlParameter("@FPH_bEXCLUDE_DUE", SqlDbType.Bit)
            sqlpbEXCLUDE_DUE.Value = bXcludeDue
            cmd.Parameters.Add(sqlpbEXCLUDE_DUE)

            Dim SqlCurrency As New SqlParameter("@FPH_CURRENCY", SqlDbType.VarChar, 5) 'Added by Jacob on 24/Jul/2019 for multicurrency
            SqlCurrency.Value = pCurrency
            cmd.Parameters.Add(SqlCurrency)

            Dim SqlExgRate As New SqlParameter("@FPH_EXG_RATE", SqlDbType.Decimal) 'Added by Jacob on 24/Jul/2019 for multicurrency
            SqlExgRate.Value = pExgRate
            cmd.Parameters.Add(SqlExgRate)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value

            If NewFPH_ID.Value Is System.DBNull.Value Then Continue For

            If IsNewStudent = True Then
                For Each STU_SUB_DET As FEEPERFORMANCEREVIEW_SUB In STU_DET.STUDENT_SUBDETAILS
                    If STU_SUB_DET.FPD_FEE_ID = 145 Then
                        HasAdmissionFee = True
                    End If
                Next
            End If

            If Not STU_DET.bDelete Then
                If iReturnvalue = 0 Then iReturnvalue = SaveFeeSubDetails(STU_DET.STUDENT_SUBDETAILS, NewFPH_ID.Value, conn, trans, HasAdmissionFee)
                If iReturnvalue = 0 Then iReturnvalue = SaveReferenceDetails(dtSelectedDates, NewFPH_ID.Value, conn, trans)

                If STU_DET.FPH_COMP_ID = "-1" AndAlso vInvoiceNo <> "" AndAlso Not bUpdateData Then
                    If iReturnvalue = 0 Then UpdateNextInvoiceNo(vInvoiceNo, dt, vBSU_ID, NewACD_ID, conn, trans)
                    vInvoiceNo = ""
                End If
            End If
            If iReturnvalue <> 0 Then
                Exit For
            Else ''Save For Balance
                If Not STU_DET.bDelete Then

                    iReturnvalue = SavePerformaBalances(NewFPH_ID.Value, dt, STU_DET.FPH_STU_ID, sqlpFPH_STU_TYPE.Value, _
                    STU_DET.FPH_BSU_ID, conn, trans)

                    If iReturnvalue <> 0 Then
                        Exit For
                    End If
                End If
            End If
        Next
        If vInvoiceNo <> "" And bUpdateData = False Then
            If iReturnvalue = 0 Then iReturnvalue = UpdateNextInvoiceNo(vInvoiceNo, dt, vBSU_ID, NewACD_ID, conn, trans)
        End If

        Return iReturnvalue
    End Function

    Public Shared Function SaveDetailsAdv(ByVal feeId As String, ByVal aMount As Double, ByVal disCription As String, ByVal stuType As String, ByVal vInvoiceNo As String, ByVal bAnual As Boolean, ByVal invCaption As String, ByVal StuId As String, ByVal dt As Date, ByVal vBSU_ID As String, ByVal ACD_ID As Integer, ByVal user As String, ByVal CompId As String, ByVal AdvBooking As Boolean, ByVal GrdId As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByRef arrInvNos As ArrayList, ByVal bDelete As Boolean) As Integer
        Dim iReturnvalue As Integer
        Dim bUpdateData As Boolean = False
        If vInvoiceNo = "" Then
            vInvoiceNo = GetNewInvoiceNo(dt, vBSU_ID, ACD_ID, conn, trans)
        Else
            bUpdateData = True
        End If
        If vInvoiceNo = "" Then Return 302
        Dim cmd As SqlCommand
        'If CompId = "-1" And vInvoiceNo = "" Then
        '    vInvoiceNo = GetNewInvoiceNo(dt, vBSU_ID, ACD_ID, conn, trans)
        '    If vInvoiceNo = "" Then Return -1
        'End If
        'arrInvNos = New ArrayList
        If Not arrInvNos.Contains(vInvoiceNo) Then arrInvNos.Add(vInvoiceNo)

        cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_H", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFPH_BSU_ID As New SqlParameter("@FPH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_BSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpFPH_INOICENO As New SqlParameter("@FPH_INOICENO", SqlDbType.VarChar, 20)
        sqlpFPH_INOICENO.Value = vInvoiceNo 'STU_DET.FPH_INVOICENO
        cmd.Parameters.Add(sqlpFPH_INOICENO)

        Dim sqlpFPH_DT As New SqlParameter("@FPH_DT", SqlDbType.DateTime)
        sqlpFPH_DT.Value = dt
        cmd.Parameters.Add(sqlpFPH_DT)

        Dim sqlpFPH_ACD_ID As New SqlParameter("@FPH_ACD_ID", SqlDbType.Int)
        sqlpFPH_ACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpFPH_ACD_ID)

        Dim sqlpFPD_STU_ID As New SqlParameter("@FPD_STU_ID", SqlDbType.BigInt)
        sqlpFPD_STU_ID.Value = StuId
        cmd.Parameters.Add(sqlpFPD_STU_ID)

        Dim sqlpFPD_GRD_ID As New SqlParameter("@FPD_GRD_ID", SqlDbType.VarChar, 10)
        sqlpFPD_GRD_ID.Value = GrdId
        cmd.Parameters.Add(sqlpFPD_GRD_ID)

        Dim sqlpFPH_INVOICE_CAPTION As New SqlParameter("@FPH_INVOICE_CAPTION", SqlDbType.VarChar, 100)
        sqlpFPH_INVOICE_CAPTION.Value = invCaption
        cmd.Parameters.Add(sqlpFPH_INVOICE_CAPTION)

        Dim sqlpFPH_STU_TYPE As New SqlParameter("@FPH_STU_TYPE", SqlDbType.VarChar, 4)
        sqlpFPH_STU_TYPE.Value = stuType
        cmd.Parameters.Add(sqlpFPH_STU_TYPE)

        Dim sqlpFPH_COMP_ID As New SqlParameter("@FPH_COMP_ID", SqlDbType.Int)
        sqlpFPH_COMP_ID.Value = CompId
        cmd.Parameters.Add(sqlpFPH_COMP_ID)

        Dim sqlpFPH_SCH_ID As New SqlParameter("@FPH_SCH_ID", SqlDbType.Int)
        sqlpFPH_SCH_ID.Value = "1"
        cmd.Parameters.Add(sqlpFPH_SCH_ID)

        Dim sqlpuser As New SqlParameter("@user", SqlDbType.VarChar, 20)
        sqlpuser.Value = user
        cmd.Parameters.Add(sqlpuser)

        Dim sqlpFPH_PrintCount As New SqlParameter("@FPH_PrintCount", SqlDbType.Int)
        sqlpFPH_PrintCount.Value = "0"
        cmd.Parameters.Add(sqlpFPH_PrintCount)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpFPH_bPrintYEarly As New SqlParameter("@FPH_bPrintYEarly", SqlDbType.Bit)
        sqlpFPH_bPrintYEarly.Value = bAnual
        cmd.Parameters.Add(sqlpFPH_bPrintYEarly)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NewFPH_ID As New SqlParameter("@NEW_FPH_ID", SqlDbType.Int)
        NewFPH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFPH_ID)

        Dim SqlAdvBooking As New SqlParameter("@AdvBooking", SqlDbType.Bit)
        SqlAdvBooking.Value = True
        cmd.Parameters.Add(SqlAdvBooking)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value

        If NewFPH_ID.Value Is System.DBNull.Value Then Return 792

        If iReturnvalue = 0 Then iReturnvalue = SaveFeeSubDetailsAdv(dt, disCription, aMount, feeId, NewFPH_ID.Value, conn, trans, bDelete)
        If iReturnvalue <> 0 Then
            SaveDetailsAdv = iReturnvalue
            Exit Function
        End If
        If vInvoiceNo <> "" And bUpdateData = False Then
            If iReturnvalue = 0 Then iReturnvalue = UpdateNextInvoiceNo(vInvoiceNo, dt, vBSU_ID, ACD_ID, conn, trans)
        End If
        Return iReturnvalue
    End Function

    Private Shared Function SavePerformaBalances(ByVal FAB_FPH_ID As String, ByVal DTFROM As DateTime, _
    ByVal STU_ID As String, ByVal STU_TYPE As String, ByVal BSU_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal IsNewStud As Boolean = 0) As Integer

        'procedure [FEES].[SavePerformaBalances] (@FAB_FPH_ID bigint,@STU_ID bigint,@STU_TYPE varchar(2),
        '@DTFROM datetime,@BSU_ID varchar(20))
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[SavePerformaBalances]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpDOC_ID As New SqlParameter("@FAB_FPH_ID", SqlDbType.BigInt)
        sqlpDOC_ID.Value = FAB_FPH_ID
        cmd.Parameters.Add(sqlpDOC_ID)

        Dim sqlpFPH_BSU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
        sqlpFPH_BSU_ID.Value = STU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpDOS_MONTH As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        sqlpDOS_MONTH.Value = STU_TYPE
        cmd.Parameters.Add(sqlpDOS_MONTH)

        Dim sqlpDOS_YEAR As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpDOS_YEAR.Value = DTFROM
        cmd.Parameters.Add(sqlpDOS_YEAR)

        Dim sqlpACD_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpACD_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpIsNewStud As New SqlParameter("@IsNewStud", SqlDbType.Bit)
        sqlpIsNewStud.Value = IsNewStud
        cmd.Parameters.Add(sqlpIsNewStud)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        Return iReturnvalue
    End Function


    Private Shared Function UpdateNextInvoiceNo(ByVal vInvoiceNo As String, ByVal dt As DateTime, ByVal vBSU_ID As String, ByVal ACD_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("OASIS.dbo.UpdateNextNo", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpDOC_ID As New SqlParameter("@DOC_ID", SqlDbType.VarChar)
        sqlpDOC_ID.Value = "PINV"
        cmd.Parameters.Add(sqlpDOC_ID)

        Dim sqlpFPH_BSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpFPH_BSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpFPH_BSU_ID)

        Dim sqlpDOS_MONTH As New SqlParameter("@DOS_MONTH", SqlDbType.TinyInt)
        sqlpDOS_MONTH.Value = dt.Month
        cmd.Parameters.Add(sqlpDOS_MONTH)

        Dim sqlpDOS_YEAR As New SqlParameter("@DOS_YEAR", SqlDbType.Int)
        sqlpDOS_YEAR.Value = dt.Year
        cmd.Parameters.Add(sqlpDOS_YEAR)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        Return iReturnvalue
    End Function

    Private Shared Function SaveReferenceDetails(ByVal dtSelectedDates As DataTable, ByVal FPH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        SaveReferenceDetails = 1000
        For Each drSTUD_DET As DataRow In dtSelectedDates.Rows
            If drSTUD_DET Is Nothing Then
                Continue For
            End If
            Dim iReturnvalue As Integer
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_S", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFPS_FPH_ID As New SqlParameter("@FPS_FPH_ID", SqlDbType.Int)
            sqlpFPS_FPH_ID.Value = FPH_ID
            cmd.Parameters.Add(sqlpFPS_FPH_ID)

            Dim sqlpFPS_REF_ID As New SqlParameter("@FPS_REF_ID", SqlDbType.Int)
            sqlpFPS_REF_ID.Value = drSTUD_DET("MonthID")
            cmd.Parameters.Add(sqlpFPS_REF_ID)

            'If STU_SUB_DET.bDelete Then
            '    Dim sqlpFPD_ID As New SqlParameter("@FPD_ID", SqlDbType.Int)
            '    sqlpFPD_ID.Value = STU_SUB_DET.FPD_ID
            '    cmd.Parameters.Add(sqlpFPD_ID)
            'End If

            Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
            sqlpbDelete.Value = False
            cmd.Parameters.Add(sqlpbDelete)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            SaveReferenceDetails = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
    End Function
    Public Shared Function SaveFeeSubDetailsAdv(ByVal shDate As Date, ByVal disCription As String, ByVal Amount As Double, ByVal FeeId As Integer, ByVal FPH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal bDelete As Boolean) As Integer
        SaveFeeSubDetailsAdv = 1000
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_D", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFPD_FPH_ID As New SqlParameter("@FPD_FPH_ID", SqlDbType.Int)
        sqlpFPD_FPH_ID.Value = FPH_ID 'STU_SUB_DET.FPD_FPH_ID
        cmd.Parameters.Add(sqlpFPD_FPH_ID)

        Dim sqlpFPD_FEE_ID As New SqlParameter("@FPD_FEE_ID", SqlDbType.Int)
        sqlpFPD_FEE_ID.Value = FeeId
        cmd.Parameters.Add(sqlpFPD_FEE_ID)

        Dim sqlpFPD_AMOUNT As New SqlParameter("@FPD_AMOUNT", SqlDbType.Decimal)
        sqlpFPD_AMOUNT.Value = Amount
        cmd.Parameters.Add(sqlpFPD_AMOUNT)

        Dim sqlpFPD_DESCRIPTION As New SqlParameter("@FPD_DESCRIPTION", SqlDbType.VarChar, 200)
        sqlpFPD_DESCRIPTION.Value = disCription
        cmd.Parameters.Add(sqlpFPD_DESCRIPTION)

        Dim sqlpFPD_bADD As New SqlParameter("@FPD_bADD", SqlDbType.Bit)
        sqlpFPD_bADD.Value = False
        cmd.Parameters.Add(sqlpFPD_bADD)

        Dim sqlpFPD_SORT_DATE As New SqlParameter("@FPD_SORT_DATE", SqlDbType.DateTime)
        sqlpFPD_SORT_DATE.Value = shDate
        cmd.Parameters.Add(sqlpFPD_SORT_DATE)


        Dim sqlpFPD_bManual As New SqlParameter("@FPD_bManual", SqlDbType.Bit)
        sqlpFPD_bManual.Value = False
        cmd.Parameters.Add(sqlpFPD_bManual)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        'Dim sqlExgRate As New SqlParameter("@FPD_EXG_RATE", SqlDbType.Decimal)
        'sqlExgRate.Value = 1
        'cmd.Parameters.Add(sqlExgRate)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        SaveFeeSubDetailsAdv = iReturnvalue
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If

    End Function

    Public Shared Function SaveFeeSubDetails(ByVal studSubDetails As ArrayList, ByVal FPH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal HasAdmissionFee As Boolean = False) As Integer
        Dim TuitionCount As Integer = 0
        SaveFeeSubDetails = 1000
        For Each STU_SUB_DET As FEEPERFORMANCEREVIEW_SUB In studSubDetails
            If STU_SUB_DET Is Nothing Then
                Continue For
            End If
            Dim iReturnvalue As Integer
            Dim cmd As SqlCommand

            If STU_SUB_DET.FPD_AMOUNT <> 0 Then
                cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_D", conn, trans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpFPD_FPH_ID As New SqlParameter("@FPD_FPH_ID", SqlDbType.Int)
                sqlpFPD_FPH_ID.Value = FPH_ID 'STU_SUB_DET.FPD_FPH_ID
                cmd.Parameters.Add(sqlpFPD_FPH_ID)

                Dim sqlpFPD_FEE_ID As New SqlParameter("@FPD_FEE_ID", SqlDbType.Int)
                sqlpFPD_FEE_ID.Value = STU_SUB_DET.FPD_FEE_ID
                cmd.Parameters.Add(sqlpFPD_FEE_ID)

                Dim sqlpFPD_AMOUNT As New SqlParameter("@FPD_AMOUNT", SqlDbType.Decimal)
                sqlpFPD_AMOUNT.Value = STU_SUB_DET.FPD_AMOUNT
                cmd.Parameters.Add(sqlpFPD_AMOUNT)

                Dim sqlpFPD_DESCRIPTION As New SqlParameter("@FPD_DESCRIPTION", SqlDbType.VarChar, 200)
                sqlpFPD_DESCRIPTION.Value = STU_SUB_DET.FPD_DESCRIPTION
                cmd.Parameters.Add(sqlpFPD_DESCRIPTION)

                Dim sqlpFPD_bADD As New SqlParameter("@FPD_bADD", SqlDbType.Bit)
                sqlpFPD_bADD.Value = False
                cmd.Parameters.Add(sqlpFPD_bADD)

                Dim sqlpFPD_SORT_DATE As New SqlParameter("@FPD_SORT_DATE", SqlDbType.DateTime)
                sqlpFPD_SORT_DATE.Value = STU_SUB_DET.SORT_DATE
                cmd.Parameters.Add(sqlpFPD_SORT_DATE)

                If STU_SUB_DET.bDelete Then
                    Dim sqlpFPD_ID As New SqlParameter("@FPD_ID", SqlDbType.Int)
                    sqlpFPD_ID.Value = STU_SUB_DET.FPD_ID
                    cmd.Parameters.Add(sqlpFPD_ID)
                End If

                Dim sqlpFPD_bManual As New SqlParameter("@FPD_bManual", SqlDbType.Bit)
                sqlpFPD_bManual.Value = False
                cmd.Parameters.Add(sqlpFPD_bManual)

                Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
                sqlpbDelete.Value = STU_SUB_DET.bDelete
                cmd.Parameters.Add(sqlpbDelete)

                Dim sqlpFPD_ded_FEE_ID As New SqlParameter("@FPD_DED_FEE_ID", SqlDbType.Int)
                If HasAdmissionFee = True And STU_SUB_DET.FPD_FEE_ID = 5 And TuitionCount = 0 Then
                    sqlpFPD_ded_FEE_ID.Value = 145
                    TuitionCount = 1
                Else
                    sqlpFPD_ded_FEE_ID.Value = 0
                End If
                cmd.Parameters.Add(sqlpFPD_ded_FEE_ID)

                Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retSValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retSValParam)

                'Dim sqlExgRate As New SqlParameter("@FPD_EXG_RATE", SqlDbType.Decimal) 'Added by Jacob on 24/Jul/2019 for multicurrency
                'sqlExgRate.Value = STU_SUB_DET.FPD_EXG_RATE
                'cmd.Parameters.Add(sqlExgRate)

                cmd.ExecuteNonQuery()
                iReturnvalue = retSValParam.Value
                SaveFeeSubDetails = iReturnvalue
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                End If
            End If
            If STU_SUB_DET.FPD_ADJ_AMOUNT <> 0 Then
                iReturnvalue = UpdateAdjustmentAmount(STU_SUB_DET, FPH_ID, conn, trans)
                SaveFeeSubDetails = iReturnvalue
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                End If
            End If
        Next
    End Function

    Public Shared Function UpdateAdjustmentAmount(ByVal STU_SUB_DET As FEEPERFORMANCEREVIEW_SUB, ByVal FPH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If STU_SUB_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_D", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFPD_FPH_ID As New SqlParameter("@FPD_FPH_ID", SqlDbType.Int)
        sqlpFPD_FPH_ID.Value = FPH_ID 'STU_SUB_DET.FPD_FPH_ID
        cmd.Parameters.Add(sqlpFPD_FPH_ID)

        Dim sqlpFPD_FEE_ID As New SqlParameter("@FPD_FEE_ID", SqlDbType.Int)
        sqlpFPD_FEE_ID.Value = STU_SUB_DET.FPD_FEE_ID
        cmd.Parameters.Add(sqlpFPD_FEE_ID)

        Dim sqlpFPD_AMOUNT As New SqlParameter("@FPD_AMOUNT", SqlDbType.Decimal)
        sqlpFPD_AMOUNT.Value = STU_SUB_DET.FPD_ADJ_AMOUNT
        cmd.Parameters.Add(sqlpFPD_AMOUNT)

        Dim sqlpFPD_DESCRIPTION As New SqlParameter("@FPD_DESCRIPTION", SqlDbType.VarChar, 200)
        sqlpFPD_DESCRIPTION.Value = STU_SUB_DET.FPD_ADJ_REMARKS
        cmd.Parameters.Add(sqlpFPD_DESCRIPTION)

        Dim sqlpFPD_SORT_DATE As New SqlParameter("@FPD_SORT_DATE", SqlDbType.DateTime)
        sqlpFPD_SORT_DATE.Value = STU_SUB_DET.SORT_DATE
        cmd.Parameters.Add(sqlpFPD_SORT_DATE)

        Dim sqlpFPD_bADD As New SqlParameter("@FPD_bADD", SqlDbType.Bit)
        If STU_SUB_DET.FPD_ADJ_AMOUNT > 0 Then
            sqlpFPD_bADD.Value = True
        Else
            sqlpFPD_bADD.Value = False
        End If
        cmd.Parameters.Add(sqlpFPD_bADD)

        Dim sqlpFPD_bManual As New SqlParameter("@FPD_bManual", SqlDbType.Bit)
        sqlpFPD_bManual.Value = True
        cmd.Parameters.Add(sqlpFPD_bManual)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = False
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        'Dim sqlExgRate As New SqlParameter("@FPD_EXG_RATE", SqlDbType.Decimal) 'Added by Jacob on 24/Jul/2019 for multicurrency
        'sqlExgRate.Value = STU_SUB_DET.FPD_EXG_RATE
        'cmd.Parameters.Add(sqlExgRate)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value

        Return iReturnvalue

    End Function

    Public Shared Function GetGradeDetails(ByVal STU_ID As Integer, ByVal bGetNxtGrade As Boolean, Optional ByVal STUD_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT) As String
        Dim obj As Object
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = ""
            Select Case STUD_TYP
                Case STUDENTTYPE.ENQUIRY
                    sql_query = "SELECT GRD_ID, GRD_DISPLAY " & _
                    " FROM GRADE_M WHERE GRD_DISPLAYORDER = " & _
                    " (SELECT GRADE_M.GRD_DISPLAYORDER  FROM GRADE_M INNER JOIN" & _
                    " ENQUIRY_SCHOOLPRIO_S ON GRADE_M.GRD_ID = ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID " & _
                    " WHERE EQS_ID = " & STU_ID & ") "
                Case STUDENTTYPE.STUDENT
                    If bGetNxtGrade Then
                        sql_query = "SELECT GRD_ID, GRD_DISPLAY " & _
                        " FROM GRADE_M WHERE (GRD_DISPLAYORDER -1) =" & _
                        " (SELECT GRADE_M.GRD_DISPLAYORDER FROM GRADE_M INNER JOIN" & _
                        " STUDENT_M ON GRADE_M.GRD_ID = STUDENT_M.STU_GRD_ID" & _
                        " WHERE STU_ID = " & STU_ID & ")"
                    Else
                        sql_query = "SELECT  GRD_ID, GRD_DISPLAY " & _
                        " FROM GRADE_M WHERE GRD_DISPLAYORDER = " & _
                        " (SELECT GRADE_M.GRD_DISPLAYORDER  FROM GRADE_M INNER JOIN" & _
                        " STUDENT_M ON GRADE_M.GRD_ID = STUDENT_M.STU_GRD_ID " & _
                        " WHERE STU_ID = " & STU_ID & ")  "
                    End If
            End Select

            obj = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
        End Using
        If obj Is DBNull.Value Then
            Return ""
        Else
            Return obj
        End If
    End Function

    Public Shared Function GetGradeDetails(ByVal STU_ID As Integer, ByVal STUD_TYP As STUDENTTYPE) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = ""
            Select Case STUD_TYP
                Case STUDENTTYPE.STUDENT
                    sql_query = "select GRD_DISPLAYORDER , STU_GRD_ID GRD_ID ,GRM_DISPLAY " & _
                    "from STUDENT_M INNER JOIN  GRADE_M ON " & _
                    "GRD_ID = STUDENT_M.STU_GRD_ID INNER JOIN GRADE_BSU_M  " & _
                    "ON GRADE_BSU_M.GRM_GRD_ID = STUDENT_M.STU_GRD_ID AND " & _
                    "GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    "GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID AND " & _
                    "GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID AND " & _
                    "STU_ID = " & STU_ID & " union " & _
                    "select GRD_DISPLAYORDER+1, (select  GRD_ID from   GRADE_M GRADE_M_1  " & _
                    "where  GRADE_M_1.GRD_DISPLAYORDER=GRADE_M.GRD_DISPLAYORDER+1), " & _
                    "(select max(GRM_DISPLAY) from GRADE_BSU_M,GRADE_M GRADE_M_1 " & _
                    "where  GRADE_M_1.GRD_DISPLAYORDER=GRADE_M.GRD_DISPLAYORDER+1 and " & _
                    "GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    "GRM_GRD_ID =GRADE_M_1.GRD_ID and GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID " & _
                    "AND GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID ) from STUDENT_M " & _
                    "INNER JOIN  GRADE_M ON GRD_ID = STUDENT_M.STU_GRD_ID INNER JOIN GRADE_BSU_M " & _
                    "ON GRADE_BSU_M.GRM_GRD_ID = STUDENT_M.STU_GRD_ID AND " & _
                    "GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    "GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID  AND " & _
                    "GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID AND STU_ID = " & STU_ID & _
                    " ORDER BY GRD_DISPLAYORDER"
                Case STUDENTTYPE.ENQUIRY
                    sql_query = "SELECT GRD_ID, GRD_DISPLAY GRM_DISPLAY " & _
                    " FROM GRADE_M WHERE GRD_DISPLAYORDER = " & _
                    " (SELECT GRADE_M.GRD_DISPLAYORDER  FROM GRADE_M INNER JOIN" & _
                    " ENQUIRY_SCHOOLPRIO_S ON GRADE_M.GRD_ID = ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID " & _
                    " WHERE EQS_ID = " & STU_ID & ") "
            End Select
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            Dim dttable As New DataTable
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Public Shared Function PrintGroup(ByVal InvNo As String, ByVal AdvBook As Boolean, ByVal BsuId As String, ByVal UserName As String) As MyReportClass
        Dim repSource As New MyReportClass
        Dim cmd As New SqlCommand("[FEES].[GroupPERFORMAINVOICE_Print]")
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString

        cmd.Parameters.AddWithValue("@FPH_INOICENO", InvNo)
        cmd.Parameters.AddWithValue("@FPH_bAdvanceBooking", AdvBook)
        cmd.Parameters.AddWithValue("@FPH_BSU_ID", BsuId)


        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim params As New Hashtable
        params("RPT_CAPTION") = "Proforma Invoice"
        params("UserName") = UserName
        repSource.IncludeBSUImage = True
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../../fees/Reports/RPT/ProformaInvoiceAdvGroup.rpt"
        Return repSource
    End Function
    Public Shared Function PrintReceipt(ByVal vINV_NO As String, ByVal bCompanyProforma As Boolean, _
          ByVal BSU_ID As String, ByVal bEnquiry As Boolean, ByVal USR_NAME As String, Optional ByVal AdvBooking As Boolean = False) As MyReportClass
        Dim VerEnabled As Boolean, VerNo As Integer, Ver_Ver_No As Integer, FPH_ID As Long
        Dim ds As DataSet
        Dim sqlStr As New StringBuilder
        Dim repSource As New MyReportClass
        'sqlStr = "SELECT isnull(BSU_bPINV_VersionEnabled,0) FROM BUSINESSUNIT_SUB WITH(NOLOCK) WHERE BUS_BSU_ID='" & BSU_ID & "'"
        'VerEnabled = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
        sqlStr.Append("SELECT FPH_ID,ISNULL(PVM_VERSION_NO,0) PVM_VERSION_NO,ISNULL(PVM_VER_VER_NO,0)PVM_VER_VER_NO,ISNULL(BSU_bPINV_VersionEnabled,0)BSU_bPINV_VersionEnabled,")
        sqlStr.Append("ISNULL(BUS_bFEE_TAXABLE,0)BUS_bFEE_TAXABLE FROM FEES.FEE_PERFORMAINVOICE_H WITH(NOLOCK) ")
        sqlStr.Append("LEFT OUTER JOIN FEES.PERFORMA_VERSION_M WITH(NOLOCK) ON FPH_PVM_ID=PVM_ID INNER JOIN OASIS.dbo.BUSINESSUNIT_M WITH(NOLOCK) ")
        sqlStr.Append("ON FPH_BSU_ID=BSU_ID INNER JOIN OASIS.dbo.BUSINESSUNIT_SUB WITH(NOLOCK) ON BUS_BSU_ID=BSU_ID ")
        sqlStr.Append("WHERE FPH_BSU_ID='" & BSU_ID & "' AND FPH_INOICENO IN (" & vINV_NO & ")")
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, sqlStr.ToString)
        'sqlStr = "SELECT isnull(BUS_bFEE_TAXABLE,0) FROM BUSINESSUNIT_SUB WITH(NOLOCK) WHERE BUS_BSU_ID='" & BSU_ID & "'"
        'bTAXable = Mainclass.getDataValue(sqlStr, "OASISConnectionString")

        If Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            FPH_ID = ds.Tables(0).Rows(0)("FPH_ID")
            VerEnabled = ds.Tables(0).Rows(0)("BSU_bPINV_VersionEnabled")
            bTAXable = ds.Tables(0).Rows(0)("BUS_bFEE_TAXABLE")
            VerNo = ds.Tables(0).Rows(0)("PVM_VERSION_NO")
            Ver_Ver_No = ds.Tables(0).Rows(0)("PVM_VER_VER_NO")
        End If
        SetTaxForGIP(bTAXable, vINV_NO, BSU_ID)
        'VerNo = Mainclass.getDataValue(sqlStr, "OASIS_FEESConnectionString")
        If Not VerEnabled Or VerNo = 0 Or AdvBooking Or bCompanyProforma Then
            repSource = PrintReceipt_Ver_0(vINV_NO, bCompanyProforma, BSU_ID, bEnquiry, USR_NAME, VerEnabled, AdvBooking)
        ElseIf Ver_Ver_No = 2 Then
            repSource = PrintReceipt_Ver_2(vINV_NO, bCompanyProforma, BSU_ID, bEnquiry, USR_NAME, AdvBooking)
        ElseIf Ver_Ver_No = 3 Then
            repSource = PrintReceipt_Ver_3(FPH_ID, bCompanyProforma, BSU_ID, USR_NAME)
        Else
            repSource = PrintReceipt_Ver_1(vINV_NO, bCompanyProforma, BSU_ID, bEnquiry, USR_NAME, AdvBooking)
        End If
        Return repSource
    End Function

    Private Shared Sub SetTaxForGIP(ByRef bTax As Boolean, ByVal vINV_NO As String, ByVal BSUID As String)
        Dim ACD_ID As Integer = 0
        Dim QRY As String = "SELECT FPH_ACD_ID FROM FEES.FEE_PERFORMAINVOICE_H WHERE FPH_BSU_ID='" & BSUID & "' AND FPH_INOICENO='" & vINV_NO & "'"
        'ACD_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QRY)
        If BSUID = "315888" Then
            bTax = False
        End If
    End Sub

    Public Shared Function PrintReceipt_Ver_0(ByVal vINV_NO As String, ByVal bCompanyProforma As Boolean, _
         ByVal BSU_ID As String, ByVal bEnquiry As Boolean, ByVal USR_NAME As String, Optional ByVal VerEnabled As Boolean = False, _
         Optional ByVal AdvBooking As Boolean = False) As MyReportClass
        Dim repSource As New MyReportClass

        Dim ShowNewPI As Boolean
        'Dim bTAXation As Boolean
        Dim sqlStr As String
        sqlStr = "select isnull(BSU_bNewProformaInv,0) from BUSINESSUNIT_SUB where BUS_BSU_ID='" & BSU_ID & "'"
        ShowNewPI = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
        'sqlStr = "select isnull(BUS_bFEE_TAXABLE,0) from BUSINESSUNIT_SUB where BUS_BSU_ID='" & BSU_ID & "'"
        'bTAXation = Mainclass.getDataValue(sqlStr, "OASISConnectionString")

        Dim RptNameComp As String = "../../fees/Reports/RPT/rptFeeProformaInvoiceCompany.rpt"
        Dim RptName As String
        If ShowNewPI Then
            RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceNew.rpt"
            If bCompanyProforma = False Then 'And bEnquiry = False Then
                RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceNewFormat.rpt"
            End If
        Else
            RptName = "../../fees/Reports/RPT/rptFeeProformaInvoice.rpt"
        End If
        If bTAXable Then
            RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceWithTAX.rpt"
            RptNameComp = IIf(VerEnabled, "../../fees/Reports/RPT/rptFeeProformaInvoiceCompanyWithTAX_Ver_1.rpt", "../../fees/Reports/RPT/rptFeeProformaInvoiceCompanyWithTAX.rpt")
        End If
        If AdvBooking = True Then
            RptNameComp = "../../fees/Reports/RPT/ProformaInvoiceCompanyAdv.rpt"
            RptName = "../../fees/Reports/RPT/ProformaInvoiceAdv.rpt"
        End If
        Dim str_Sql, strFilter, str_Sql_net As String
        strFilter = "  FPH_INOICENO in (" & vINV_NO & ") AND  BSU_ID='" & BSU_ID & "' "
        str_Sql_net = "select * from fees.VW_FEE_PERFORMAACCBALANACE  WHERE " & strFilter
        If bEnquiry Then
            str_Sql = "select * from FEES.VW_OSO_FEES_PROFORMA_REPORT_ENQ WHERE " & strFilter
        Else
            str_Sql = "select * from FEES.VW_OSO_FEES_PROFORMA_REPORT WHERE " & strFilter
        End If
        If bCompanyProforma Then
            str_Sql = "EXEC FEES.FEES_PERFORMA_SUBREPORT @FPH_INVOICENO=" & vINV_NO & ",@BSU_ID='" & BSU_ID & "'"
            If BSU_ID = "900201" Then
                str_Sql = "EXEC FEES.FEES_PERFORMA_SUBREPORT_GWS @FPH_INVOICENO=" & vINV_NO & ",@BSU_ID='" & BSU_ID & "'"
            End If
        End If
        'Dim cmd As New SqlCommand
        'cmd.CommandText = str_Sql
        'cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet

        Dim cmd As New SqlCommand
        cmd.CommandText = IIf(VerEnabled, "[FEES].[SP_OSO_FEES_PROFORMA_REPORT_VER_1]", "[FEES].[SP_OSO_FEES_PROFORMA_REPORT]")
        cmd.CommandType = Data.CommandType.StoredProcedure
        Dim param(2), cmdParam(2) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        param(1) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
        param(2) = Mainclass.CreateSqlParameter("@bEnquiry", bEnquiry, SqlDbType.Bit)

        cmd.Connection = New SqlConnection(str_conn)
        cmd.Parameters.Add(param(0))
        cmd.Parameters.Add(param(1))
        cmd.Parameters.Add(param(2))
        ' SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
        'cmd.Connection = New SqlConnection(str_conn)

        Dim params As New Hashtable

        params("RPT_CAPTION") = "Proforma Invoice"

        params("UserName") = USR_NAME
        'If RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceNewswap.rpt" Then
        '    params("BSuid") = "BSU_ID"
        '    params("docno") = "vINV_NO"
        'End If

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If bCompanyProforma Then
            If AdvBooking = False Then
                Dim repSourceSubRep(2) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubEarn As New SqlCommand

                cmdSubEarn.CommandText = str_Sql
                cmdSubEarn.Connection = New SqlConnection(str_conn)
                cmdSubEarn.CommandType = CommandType.Text
                repSourceSubRep(0).Command = cmdSubEarn
                repSource.SubReport = repSourceSubRep

                repSourceSubRep(1) = New MyReportClass
                Dim cmdNetPayable As New SqlCommand
                cmdNetPayable.CommandText = str_Sql_net
                cmdNetPayable.Connection = New SqlConnection(str_conn)
                cmdNetPayable.CommandType = CommandType.Text
                repSourceSubRep(1).Command = cmdNetPayable
                repSource.SubReport = repSourceSubRep

            End If
            repSource.ResourceName = RptNameComp
        Else

            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            Dim cmdNetPayable As New SqlCommand
            cmdNetPayable.CommandText = str_Sql_net
            cmdNetPayable.Connection = New SqlConnection(str_conn)
            cmdNetPayable.CommandType = CommandType.Text
            If AdvBooking = False Then
                repSourceSubRep(0).Command = cmdNetPayable
                repSource.SubReport = repSourceSubRep
            End If
            repSource.ResourceName = RptName
        End If
        '   End If
        'And bEnquiry  = False 
        If (bCompanyProforma = False And (RptName.EndsWith("rptFeeProformaInvoiceNewFormat.rpt") Or RptName.EndsWith("rptFeeProformaInvoiceWithTAX.rpt"))) Then

            Dim repSourceSubRep(2) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            repSourceSubRep(1) = New MyReportClass
            repSourceSubRep(2) = New MyReportClass
            Dim cmdNetPayable As New SqlCommand
            Dim cmdNetPayable1 As New SqlCommand
            Dim cmdNetPayableSub As New SqlCommand

            cmdNetPayable.CommandText = str_Sql_net
            cmdNetPayable.Connection = New SqlConnection(str_conn)
            cmdNetPayable.CommandType = CommandType.Text


            cmdNetPayable1.CommandText = str_Sql_net
            cmdNetPayable1.Connection = New SqlConnection(str_conn)
            cmdNetPayable1.CommandType = CommandType.Text

            ' cmdNetPayableSub.CommandText = "exec FEES.GEtPerformaDetails 0,'" & BSU_ID & "'," & vINV_NO

            cmdNetPayableSub.CommandText = "[FEES].[GEtPerformaDetails]"
            cmdNetPayableSub.CommandType = Data.CommandType.StoredProcedure
            Dim netPparam(3) As SqlParameter
            netPparam(0) = Mainclass.CreateSqlParameter("@FPH_ID", 0, SqlDbType.Int)
            netPparam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            netPparam(2) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
            netPparam(3) = Mainclass.CreateSqlParameter("@ReturnTotal", False, SqlDbType.Bit)


            cmdNetPayableSub.Parameters.Add(netPparam(0))
            cmdNetPayableSub.Parameters.Add(netPparam(1))
            cmdNetPayableSub.Parameters.Add(netPparam(2))
            cmdNetPayableSub.Parameters.Add(netPparam(3))

            cmdNetPayableSub.Connection = New SqlConnection(str_conn)
            If AdvBooking = False Then
                repSourceSubRep(0).Command = cmdNetPayable
                repSource.SubReport = repSourceSubRep

                repSourceSubRep(1).Command = cmdNetPayable1
                repSource.SubReport = repSourceSubRep

                repSourceSubRep(2).Command = cmdNetPayableSub
                repSource.SubReport = repSourceSubRep
            End If
            repSource.ResourceName = RptName
        End If
        Return repSource
    End Function

    Public Shared Function PrintReceipt_Ver_1(ByVal vINV_NO As String, ByVal bCompanyProforma As Boolean, _
         ByVal BSU_ID As String, ByVal bEnquiry As Boolean, ByVal USR_NAME As String, Optional ByVal AdvBooking As Boolean = False) As MyReportClass
        Dim repSource As New MyReportClass
        Dim RptName As String

        If BSU_ID = "800555" Then 'GWE
            RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceVer_GWE.rpt"
        Else
            RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceVer_1.rpt"
        End If

        Dim strFilter, str_Sql_net As String
        strFilter = "  FPH_INOICENO in (" & vINV_NO & ") AND  BSU_ID='" & BSU_ID & "' "
        str_Sql_net = "select * from fees.VW_FEE_PERFORMAACCBALANACE  WHERE " & strFilter

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        Dim Qry As String
        Qry = "select isnull(BUS_bFEE_TAXABLE,0) from BUSINESSUNIT_SUB where BUS_BSU_ID='" & BSU_ID & "'"
        'bTAXation = Mainclass.getDataValue(Qry, "OASISConnectionString")
        'SetTaxForGIP(bTAXation, vINV_NO, BSU_ID)
        If bTAXable Then
            RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceWithTax_Ver_1.rpt"
        End If
        Dim cmd As New SqlCommand
        cmd.CommandText = "[FEES].[SP_OSO_FEES_PROFORMA_REPORT_VER_1]"
        cmd.CommandType = Data.CommandType.StoredProcedure
        Dim param(2), cmdParam(2) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        param(1) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
        param(2) = Mainclass.CreateSqlParameter("@bEnquiry", bEnquiry, SqlDbType.Bit)

        cmd.Connection = New SqlConnection(str_conn)
        cmd.Parameters.Add(param(0))
        cmd.Parameters.Add(param(1))
        cmd.Parameters.Add(param(2))

        cmd.Connection = New SqlConnection(str_conn)
        Dim params As New Hashtable
        params("RPT_CAPTION") = "Proforma Invoice"
        params("UserName") = USR_NAME

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        Dim repSourceSubRep(3) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(2) = New MyReportClass
        If Not bTAXable Then
            repSourceSubRep(3) = New MyReportClass
        End If

        Dim cmdNetPayable As New SqlCommand
        Dim cmdNetPayable1 As New SqlCommand
        Dim cmdNetPayableSub As New SqlCommand
        Dim cmdSubNewHeader As New SqlCommand

        cmdNetPayable.CommandText = str_Sql_net
        cmdNetPayable.Connection = New SqlConnection(str_conn)
        cmdNetPayable.CommandType = CommandType.Text

        cmdNetPayable1.CommandText = str_Sql_net
        cmdNetPayable1.Connection = New SqlConnection(str_conn)
        cmdNetPayable1.CommandType = CommandType.Text

        cmdNetPayableSub.CommandText = "[FEES].[GEtPerformaDetails]"
        cmdNetPayableSub.CommandType = Data.CommandType.StoredProcedure
        Dim netPparam(3) As SqlParameter
        netPparam(0) = Mainclass.CreateSqlParameter("@FPH_ID", 0, SqlDbType.Int)
        netPparam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        netPparam(2) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
        netPparam(3) = Mainclass.CreateSqlParameter("@ReturnTotal", False, SqlDbType.Bit)

        cmdNetPayableSub.Parameters.Add(netPparam(0))
        cmdNetPayableSub.Parameters.Add(netPparam(1))
        cmdNetPayableSub.Parameters.Add(netPparam(2))
        cmdNetPayableSub.Parameters.Add(netPparam(3))
        cmdNetPayableSub.Connection = New SqlConnection(str_conn)

        cmdSubNewHeader.CommandText = "getBsuInFoWithImage"
        cmdSubNewHeader.CommandType = Data.CommandType.StoredProcedure
        Dim newHeaderParam(2) As SqlParameter
        newHeaderParam(0) = Mainclass.CreateSqlParameter("@IMG_BSU_ID", BSU_ID, SqlDbType.VarChar)
        newHeaderParam(1) = Mainclass.CreateSqlParameter("@IMG_TYPE", "LOGO", SqlDbType.VarChar)

        cmdSubNewHeader.Parameters.Add(newHeaderParam(0))
        cmdSubNewHeader.Parameters.Add(newHeaderParam(1))
        cmdSubNewHeader.Connection = ConnectionManger.GetOASISConnection


        repSourceSubRep(0).Command = cmdNetPayable
        'repSource.SubReport = repSourceSubRep
        repSourceSubRep(1).Command = cmdNetPayable1
        'repSource.SubReport = repSourceSubRep
        repSourceSubRep(2).Command = cmdNetPayableSub
        'repSource.SubReport = repSourceSubRep
        If Not bTAXable Then
            repSourceSubRep(3).Command = cmdSubNewHeader
            'repSource.SubReport = repSourceSubRep
        End If

        repSource.SubReport = repSourceSubRep
        repSource.ResourceName = RptName


        Return repSource

    End Function

    Public Shared Function PrintReceipt_Ver_2(ByVal vINV_NO As String, ByVal bCompanyProforma As Boolean, _
         ByVal BSU_ID As String, ByVal bEnquiry As Boolean, ByVal USR_NAME As String, Optional ByVal AdvBooking As Boolean = False) As MyReportClass
        Dim repSource As New MyReportClass
        Dim RptName As String

        RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceVer_2.rpt"

        Dim strFilter, str_Sql_net As String
        strFilter = "  FPH_INOICENO in (" & vINV_NO & ") AND  BSU_ID='" & BSU_ID & "' "
        str_Sql_net = "select * from fees.VW_FEE_PERFORMAACCBALANACE  WHERE " & strFilter

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        'Dim Qry As String
        'Qry = "select isnull(BUS_bFEE_TAXABLE,0) from BUSINESSUNIT_SUB where BUS_BSU_ID='" & BSU_ID & "'"
        'bTAXation = Mainclass.getDataValue(Qry, "OASISConnectionString")
        'SetTaxForGIP(bTAXation, vINV_NO, BSU_ID)
        If bTAXable Then
            RptName = "../../fees/Reports/RPT/rptFeeProformaInvoiceWithTax_Ver_1.rpt"
        End If
        Dim cmd As New SqlCommand
        cmd.CommandText = "[FEES].[SP_OSO_FEES_PROFORMA_REPORT_VER_2]"
        cmd.CommandType = Data.CommandType.StoredProcedure
        Dim param(2), cmdParam(2) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        param(1) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
        param(2) = Mainclass.CreateSqlParameter("@bEnquiry", bEnquiry, SqlDbType.Bit)

        cmd.Connection = New SqlConnection(str_conn)
        cmd.Parameters.Add(param(0))
        cmd.Parameters.Add(param(1))
        cmd.Parameters.Add(param(2))

        cmd.Connection = New SqlConnection(str_conn)
        Dim params As New Hashtable
        params("RPT_CAPTION") = "Proforma Invoice"
        params("UserName") = USR_NAME

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(2) = New MyReportClass
        Dim cmdNetPayable As New SqlCommand
        Dim cmdNetPayable1 As New SqlCommand
        Dim cmdNetPayableSub As New SqlCommand

        cmdNetPayable.CommandText = str_Sql_net
        cmdNetPayable.Connection = New SqlConnection(str_conn)
        cmdNetPayable.CommandType = CommandType.Text

        cmdNetPayable1.CommandText = str_Sql_net
        cmdNetPayable1.Connection = New SqlConnection(str_conn)
        cmdNetPayable1.CommandType = CommandType.Text

        cmdNetPayableSub.CommandText = "[FEES].[GEtPerformaDetails]"
        cmdNetPayableSub.CommandType = Data.CommandType.StoredProcedure
        cmdNetPayableSub.Connection = New SqlConnection(str_conn)
        Dim netPparam(3) As SqlParameter
        netPparam(0) = Mainclass.CreateSqlParameter("@FPH_ID", 0, SqlDbType.Int)
        netPparam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        netPparam(2) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
        netPparam(3) = Mainclass.CreateSqlParameter("@ReturnTotal", False, SqlDbType.Bit)

        cmdNetPayableSub.Parameters.Add(netPparam(0))
        cmdNetPayableSub.Parameters.Add(netPparam(1))
        cmdNetPayableSub.Parameters.Add(netPparam(2))
        cmdNetPayableSub.Parameters.Add(netPparam(3))

        repSourceSubRep(0).Command = cmdNetPayable
        repSourceSubRep(1).Command = cmdNetPayable1
        repSourceSubRep(2).Command = cmdNetPayableSub
        repSource.SubReport = repSourceSubRep

        repSource.ResourceName = RptName
        Return repSource

    End Function

    Public Shared Function PrintReceipt_Ver_3(ByVal FPH_ID As Long, ByVal bCompanyProforma As Boolean, _
         ByVal BSU_ID As String, ByVal USR_NAME As String) As MyReportClass
        Dim repSource As New MyReportClass
        Dim RptName As String

        RptName = "../../fees/Reports/RPT/rptFeeVAT_ProformaInvoice.rpt"

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet

        Dim cmd As New SqlCommand
        cmd.CommandText = "[FEES].[PRINT_PROFORMA_TAX_INVOICE]"
        cmd.CommandType = Data.CommandType.StoredProcedure
        cmd.Connection = New SqlConnection(str_conn)
        Dim param(2) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        param(1) = Mainclass.CreateSqlParameter("@FPH_ID", FPH_ID, SqlDbType.BigInt)
        cmd.Parameters.Add(param(0))
        cmd.Parameters.Add(param(1))

        Dim params As New Hashtable
        'params("RPT_CAPTION") = "Proforma Invoice"
        params("userName") = USR_NAME

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = BSU_ID

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        'repSourceSubRep(2) = New MyReportClass
        'Dim cmdHeader As New SqlCommand
        Dim cmdFeeVAT_PerformaSplitup As New SqlCommand
        Dim cmdNetPayableSub As New SqlCommand

        'cmdHeader.CommandText = "dbo.getBsuInFoWithImage"
        'cmdHeader.Connection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        'cmdHeader.CommandType = CommandType.StoredProcedure
        'Dim sqlpBSU_IDsub2 As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 20)
        'sqlpBSU_IDsub2.Value = BSU_ID
        'cmdHeader.Parameters.Add(sqlpBSU_IDsub2)
        'Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        'sqlpIMG_TYPE.Value = "LOGO"
        'cmdHeader.Parameters.Add(sqlpIMG_TYPE)

        cmdFeeVAT_PerformaSplitup.CommandText = "[FEES].[PRINT_PROFORMA_TAX_INVOICE_SPLITUP]"
        cmdFeeVAT_PerformaSplitup.CommandType = CommandType.StoredProcedure
        cmdFeeVAT_PerformaSplitup.Connection = New SqlConnection(str_conn)
        Dim SubcmdParam2 As New SqlParameter("@FPH_ID", SqlDbType.BigInt)
        SubcmdParam2.Value = FPH_ID
        cmdFeeVAT_PerformaSplitup.Parameters.Add(SubcmdParam2)

        cmdNetPayableSub.CommandText = "FEES.GET_FEE_PERFORMAACCBALANACE"
        cmdNetPayableSub.CommandType = Data.CommandType.StoredProcedure
        cmdNetPayableSub.Connection = New SqlConnection(str_conn)
        Dim netPparam(2) As SqlParameter
        netPparam(0) = Mainclass.CreateSqlParameter("@FPH_ID", FPH_ID, SqlDbType.BigInt)
        netPparam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        cmdNetPayableSub.Parameters.Add(netPparam(0))
        cmdNetPayableSub.Parameters.Add(netPparam(1))

        'repSourceSubRep(0).Command = cmdHeader
        repSourceSubRep(0).Command = cmdFeeVAT_PerformaSplitup
        repSourceSubRep(1).Command = cmdNetPayableSub
        repSource.SubReport = repSourceSubRep

        repSource.ResourceName = RptName
        Return repSource

    End Function

    Public Shared Sub F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(ByVal FPH_BSU_ID As String, _
        ByVal FPH_INOICENO As String, ByVal USR_NAME As String)
        Dim cmd As SqlCommand
        Dim conn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            conn.Open()
            cmd = New SqlCommand("FEES.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFPH_BSU_ID As New SqlParameter("@FPH_BSU_ID", SqlDbType.VarChar)
            sqlpFPH_BSU_ID.Value = FPH_BSU_ID
            cmd.Parameters.Add(sqlpFPH_BSU_ID)

            Dim sqlpFPH_INOICENO As New SqlParameter("@FPH_INOICENO", SqlDbType.VarChar)
            sqlpFPH_INOICENO.Value = FPH_INOICENO
            cmd.Parameters.Add(sqlpFPH_INOICENO)

            Dim sqlpFPD_AMOUNT As New SqlParameter("@USR_NAME", SqlDbType.VarChar)
            sqlpFPD_AMOUNT.Value = USR_NAME
            cmd.Parameters.Add(sqlpFPD_AMOUNT)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)
            cmd.ExecuteNonQuery()
            '            Return retSValParam.Value
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Public Shared Function GetFeeDetails(ByVal STUD_ID As Integer, ByVal STU_TYPE As STUDENTTYPE, ByVal ACD_ID As Integer, ByVal GRD_ID As String, ByVal BSU_ID As String, ByVal str_Months As String, ByVal str_feeTypes As String, ByVal selProDur As SELECTEDPROFORMADURATION, ByRef total As Double, ByVal NextAcademicYear As Boolean) As DataTable
        Dim dsTotal As New DataSet
        Dim cmd As SqlCommand

        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            cmd = New SqlCommand("[FEES].[F_GetFEE_PERFORMA_INVOICE]", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
            sqlpACD_ID.Value = ACD_ID
            cmd.Parameters.Add(sqlpACD_ID)

            Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 10)
            sqlpGRD_ID.Value = GRD_ID
            cmd.Parameters.Add(sqlpGRD_ID)

            Dim sqlpSTU_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTU_ID.Value = STUD_ID
            cmd.Parameters.Add(sqlpSTU_ID)

            '1 - STUDENT
            '2 - ENQUIRY
            Dim str_STU_TYPE As String = String.Empty
            Select Case STU_TYPE
                Case 1
                    str_STU_TYPE = "S"
                Case 2
                    str_STU_TYPE = "E"
            End Select
            Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar)
            sqlpSTU_TYPE.Value = str_STU_TYPE  'STU_TYPE
            cmd.Parameters.Add(sqlpSTU_TYPE)

            Dim sqlpBTerm As New SqlParameter("@BTerm", SqlDbType.Bit)
            sqlpBTerm.Value = selProDur 'STU_TYPE
            cmd.Parameters.Add(sqlpBTerm)

            Dim sqlpDates As New SqlParameter("@Period", SqlDbType.VarChar)
            sqlpDates.Value = str_Months
            cmd.Parameters.Add(sqlpDates)

            Dim sqlpTEMPDOCNO As New SqlParameter("@TEMPDOCNO", SqlDbType.VarChar)
            sqlpTEMPDOCNO.Value = ""
            cmd.Parameters.Add(sqlpTEMPDOCNO)

            Dim sqlpFEETYPES As New SqlParameter("@FEETYPES", SqlDbType.VarChar)
            sqlpFEETYPES.Value = str_feeTypes
            cmd.Parameters.Add(sqlpFEETYPES)

            Dim sqlpNextAcademicYear As New SqlParameter("@bNextAcademicYear", SqlDbType.Bit)
            sqlpNextAcademicYear.Value = NextAcademicYear
            cmd.Parameters.Add(sqlpNextAcademicYear)


            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(dsTotal)
        End Using
        If dsTotal Is Nothing OrElse dsTotal.Tables.Count = 0 OrElse dsTotal.Tables(0).Rows.Count = 0 Then
            Return Nothing
        Else
            total = dsTotal.Tables(0).Compute("Sum(AMOUNT)", "")
            Return dsTotal.Tables(0)
        End If
    End Function

    Public Sub GetStudentDetails(ByVal bNextAcademicYear As Boolean, Optional IsFromProfile As Boolean = False)
        Dim objReader As SqlDataReader
        Dim ds As DataSet
        Dim pParms(13) As SqlClient.SqlParameter
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String
            If STU_TYPE = "E" Then


                sql_query = "SELECT ISNULL(dbo.ENQUIRY_M.EQM_APPLFIRSTNAME, '') + ' ' + " & _
                " ISNULL(dbo.ENQUIRY_M.EQM_APPLMIDNAME, '') + ' ' +  " & _
                " ISNULL(dbo.ENQUIRY_M.EQM_APPLLASTNAME, '') AS STUD_NAME, EQS_ID as STU_NO, EQS_GRD_ID AS STU_GRD_ID,EQS_ACD_ID AS STU_ACD_ID  " & _
                " FROM dbo.ENQUIRY_M WITH(NOLOCK) INNER JOIN " & _
                " ENQUIRY_SCHOOLPRIO_S WITH(NOLOCK) ON ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID = ENQUIRY_M.EQM_ENQID  " & _
                " WHERE ENQUIRY_SCHOOLPRIO_S.EQS_ID = " & STU_ID
            Else
                sql_query = "SELECT isnull(STU_FIRSTNAME,'') + ' ' + isnull(STU_MIDNAME,'') + ' ' + isnull(STU_LASTNAME,'')  STUD_NAME, STU_NO, STU_ACD_ID, STU_GRD_ID FROM dbo.STUDENT_M WITH(NOLOCK)  WHERE STU_ID = " & STU_ID
                objReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
                While (objReader.Read())
                    STU_NO = objReader("STU_NO").ToString
                End While
                objReader.Close()
                If IsFromProfile Then
                    pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
                    pParms(0).Value = 1
                    pParms(1) = New SqlClient.SqlParameter("@BSU_CODE", SqlDbType.VarChar)
                    pParms(1).Value = FPH_BSU_ID
                    pParms(2) = New SqlClient.SqlParameter("@NewStudent", SqlDbType.VarChar)
                    pParms(2).Value = FPH_IsNewStudent
                    pParms(3) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.VarChar)
                    pParms(3).Value = FPH_ACD_ID
                    pParms(4) = New SqlClient.SqlParameter("@POP_ACD_ID", SqlDbType.VarChar)
                    pParms(4).Value = ""
                    pParms(5) = New SqlClient.SqlParameter("@IsService", SqlDbType.VarChar)
                    pParms(5).Value = FPH_IsServiceINV
                    pParms(6) = New SqlClient.SqlParameter("@FEE_TYPE", SqlDbType.VarChar)
                    pParms(6).Value = FPH_FEE_TYPES
                    pParms(7) = New SqlClient.SqlParameter("@EXL_TYPE", SqlDbType.VarChar)
                    pParms(7).Value = FPH_EXL_TYPE
                    pParms(8) = New SqlClient.SqlParameter("@EXCL_TC", SqlDbType.VarChar)
                    pParms(8).Value = FPH_IsEXCL_TC
                    pParms(9) = New SqlClient.SqlParameter("@COMP_ID", SqlDbType.VarChar)
                    pParms(9).Value = FPH_COMP_ID
                    pParms(10) = New SqlClient.SqlParameter("@STU_NO_FLTR", SqlDbType.VarChar)
                    pParms(10).Value = STU_NO
                    pParms(11) = New SqlClient.SqlParameter("@STU_NAME_FLTR", SqlDbType.VarChar)
                    pParms(11).Value = ""
                    pParms(12) = New SqlClient.SqlParameter("@STU_GRD_FLTR", SqlDbType.VarChar)
                    pParms(12).Value = ""
                    pParms(13) = New SqlClient.SqlParameter("@ENROLL_TYPE_FLTR", SqlDbType.VarChar)
                    pParms(13).Value = "ALL"
                    sql_query = "[OASIS].[DBO].[GET_STU_NEW_POPUP_LIST]"

                End If
            End If
            If IsFromProfile Then
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, sql_query, pParms)
                STU_NO = ds.Tables(0).Rows(0)("STU_NO").ToString 'objReader("STU_NO").ToString
                STU_NAME = ds.Tables(0).Rows(0)("STU_NAME").ToString
                STU_GRD_ID = ds.Tables(0).Rows(0)("STU_GRD_ID").ToString 'objReader("STU_GRD_ID").ToString
                STU_ACD_ID = ds.Tables(0).Rows(0)("STU_ACD_ID").ToString 'objReader("STU_ACD_ID").ToString
            Else
                objReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_query)
                While (objReader.Read())
                    STU_NO = objReader("STU_NO").ToString
                    STU_NAME = objReader("STUD_NAME").ToString
                    STU_GRD_ID = objReader("STU_GRD_ID").ToString
                    STU_ACD_ID = objReader("STU_ACD_ID").ToString
                End While
                objReader.Close()

            End If

        End Using
    End Sub
End Class

Public Class FEEPERFORMANCEREVIEW_SUB
    Dim vFPD_ID As Integer
    Dim vFPD_FPH_ID As Integer
    Dim vFPD_FEE_ID As Integer
    Dim vFPD_FEE_DESCR As String
    Dim vFPD_AMOUNT As Double
    Dim vFPD_ADJ_AMOUNT As Double
    Dim vFPD_ADJ_REMARKS As String
    Public bDelete As Boolean
    Public bEdit As Boolean
    Dim vFPD_DESCRIPTION As String
    Dim vFPD_REF_ID As Integer
    Dim bALLOWCONC As Boolean
    Dim vFPD_SORT_DATE As DateTime
    Dim vFEE_ApplyToAll As Boolean

    Dim vFPD_EXG_RATE As Double
    Dim vFPD_EXG_AMOUNT As Double
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPD_ID() As Integer
        Get
            Return vFPD_ID
        End Get
        Set(ByVal value As Integer)
            vFPD_ID = value
        End Set
    End Property

    Public Property AllowConcession() As Boolean
        Get
            Return bALLOWCONC
        End Get
        Set(ByVal value As Boolean)
            bALLOWCONC = value
        End Set
    End Property

    Public Property SORT_DATE() As DateTime
        Get
            Return vFPD_SORT_DATE
        End Get
        Set(ByVal value As DateTime)
            vFPD_SORT_DATE = value
        End Set
    End Property


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPD_FPH_ID() As Integer
        Get
            Return vFPD_FPH_ID
        End Get
        Set(ByVal value As Integer)
            vFPD_FPH_ID = value
        End Set
    End Property

    Public Property FPD_REF_ID() As Integer
        Get
            Return vFPD_REF_ID
        End Get
        Set(ByVal value As Integer)
            vFPD_REF_ID = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPD_FEE_ID() As Integer
        Get
            Return vFPD_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFPD_FEE_ID = value
        End Set
    End Property
    Public Property FEE_ApplyToAll() As Boolean
        Get
            Return vFEE_ApplyToAll
        End Get
        Set(ByVal value As Boolean)
            vFEE_ApplyToAll = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPD_AMOUNT() As Double
        Get
            Return vFPD_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFPD_AMOUNT = value
        End Set
    End Property

    Public Property FPD_FEE_DESCR() As String
        Get
            Return vFPD_FEE_DESCR
        End Get
        Set(ByVal value As String)
            vFPD_FEE_DESCR = value
        End Set
    End Property

    Public Property FPD_DESCRIPTION() As String
        Get
            Return vFPD_DESCRIPTION
        End Get
        Set(ByVal value As String)
            vFPD_DESCRIPTION = value
        End Set
    End Property

    Public Property FPD_ADJ_AMOUNT() As Double
        Get
            Return vFPD_ADJ_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFPD_ADJ_AMOUNT = value
        End Set
    End Property

    Public Property FPD_ADJ_REMARKS() As String
        Get
            Return vFPD_ADJ_REMARKS
        End Get
        Set(ByVal value As String)
            vFPD_ADJ_REMARKS = value
        End Set
    End Property

    Public Property FPD_EXG_RATE() As Double
        Get
            Return vFPD_EXG_RATE
        End Get
        Set(ByVal value As Double)
            vFPD_EXG_RATE = value
        End Set
    End Property
    Public Property FPD_EXG_AMOUNT() As Double
        Get
            Return vFPD_EXG_AMOUNT
        End Get
        Set(ByVal value As Double)
            vFPD_EXG_AMOUNT = value
        End Set
    End Property
End Class