Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
''' The class Created for handling the Concession Type
''' 
''' Author : Guru
''' Date : 03-MAY-2008 
''' </summary>
''' <remarks></remarks>
Public Class FeeCollectionOther

    Public Shared Function F_SaveFEEOTHCOLLECTION_H(ByVal FOC_ID As String, _
    ByVal FOC_REFNO As String, ByVal FOC_DATE As String, ByVal FOC_BSU_ID As String, _
    ByVal FOC_AMOUNT As Decimal, ByVal FOC_EMP_ID As String, ByVal FOC_CLT_ID As String, _
    ByVal FOC_Bposted As Boolean, ByVal FOC_NARRATION As String, ByVal FOC_DRCR As String, _
    ByVal FOC_bDELETED As Boolean, ByVal FOC_COL_ID As String, ByVal FOC_ACT_ID As String, _
    ByRef NEW_FOC_RECNO As String, ByRef NEW_FOC_ID As String, ByVal trans As SqlTransaction, _
    ByVal conn As SqlConnection, Optional ByVal FOC_CUR_ID As String = "0", _
    Optional ByVal FOC_EXCHANGE1 As Double = 0, Optional ByVal FOC_EVT_ID As String = "", _
    Optional ByVal FOC_CFA_ID As String = "", Optional ByVal FOC_REF_VDR_CODE As String = "", _
    Optional ByVal FOC_CUSTOMER_ACT_ID As String = "", Optional ByVal FOC_CUSTOMER_TAX_REG_NO As String = "", _
    Optional ByVal FOC_CUSTOMER_ADDRESS As String = "", Optional ByVal FOC_TAX_CODE As String = "", _
    Optional ByVal FOC_STU_ID As Long = 0, Optional ByVal FOC_STAFF_EMP_ID As Integer = 0,
    Optional ByVal FOC_BATCH_NO As Integer = 0, Optional ByVal FOC_BATCH_REMARKS As String = "",
    Optional ByVal FOC_INVOICE As String = "", Optional ByVal FOC_IsGenerateTaxInvoice As Integer = 0,
    Optional ByVal FOC_TAX_INVOICE_REF_NO As String = "", Optional ByVal FOC_FEE_ID As Integer = 0,
    Optional ByVal FOC_EVENT_TYPE_ID As Integer = 0, Optional ByVal FOC_COLLECTION_SOURCE As String = "",
    Optional ByVal FOC_PAYMENT_BSU_ID As String = "") As Integer
        Dim iReturnvalue As Integer

        Dim pParms(35) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FOC_ID", FOC_ID)
        pParms(1) = New SqlClient.SqlParameter("@FOC_REFNO", FOC_REFNO)
        pParms(2) = New SqlClient.SqlParameter("@FOC_DATE", FOC_DATE)
        pParms(3) = New SqlClient.SqlParameter("@FOC_BSU_ID", FOC_BSU_ID)
        pParms(4) = New SqlClient.SqlParameter("@FOC_AMOUNT", FOC_AMOUNT)
        pParms(5) = New SqlClient.SqlParameter("@FOC_EMP_ID", FOC_EMP_ID)
        pParms(6) = New SqlClient.SqlParameter("@FOC_CLT_ID", FOC_CLT_ID)
        pParms(7) = New SqlClient.SqlParameter("@FOC_Bposted", FOC_Bposted)
        pParms(8) = New SqlClient.SqlParameter("@FOC_NARRATION", FOC_NARRATION)
        pParms(9) = New SqlClient.SqlParameter("@FOC_DRCR", FOC_DRCR)
        pParms(10) = New SqlClient.SqlParameter("@FOC_COL_ID", FOC_COL_ID)
        pParms(11) = New SqlClient.SqlParameter("@FOC_bDELETED", FOC_bDELETED)
        pParms(12) = New SqlClient.SqlParameter("@NEW_FOC_RECNO", SqlDbType.VarChar, 30)
        pParms(12).Direction = ParameterDirection.Output
        pParms(13) = New SqlClient.SqlParameter("@NEW_FOC_ID", SqlDbType.BigInt)
        pParms(13).Direction = ParameterDirection.Output

        pParms(14) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(14).Direction = ParameterDirection.ReturnValue
        pParms(15) = New SqlClient.SqlParameter("@FOC_ACT_ID", FOC_ACT_ID) '@
        pParms(16) = New SqlClient.SqlParameter("@FOC_CUR_ID", FOC_CUR_ID)
        pParms(17) = New SqlClient.SqlParameter("@FOC_EXCHANGE1", FOC_EXCHANGE1)
        pParms(18) = New SqlClient.SqlParameter("@FOC_EVT_ID", FOC_EVT_ID) 'Event Id
        pParms(19) = New SqlClient.SqlParameter("@FOC_CFA_ID", FOC_CFA_ID) 'Cash Flow Id
        pParms(20) = New SqlClient.SqlParameter("@FOC_REF_VDR_CODE", FOC_REF_VDR_CODE) 'Ref Vendor Code
        pParms(21) = New SqlClient.SqlParameter("@FOC_CUSTOMER_ACT_ID", FOC_CUSTOMER_ACT_ID) 'ref customer
        pParms(22) = New SqlClient.SqlParameter("@FOC_CUSTOMER_TAX_REG_NO", FOC_CUSTOMER_TAX_REG_NO) 'ref customer tax reg no
        pParms(23) = New SqlClient.SqlParameter("@FOC_CUSTOMER_ADDRESS", FOC_CUSTOMER_ADDRESS) 'ref customer address
        pParms(24) = New SqlClient.SqlParameter("@FOC_TAX_CODE", FOC_TAX_CODE) 'tax code
        pParms(25) = New SqlClient.SqlParameter("@FOC_STU_ID", FOC_STU_ID) 'ref customer address
        pParms(26) = New SqlClient.SqlParameter("@FOC_STAFF_EMP_ID", FOC_STAFF_EMP_ID) 'ref customer address
        pParms(27) = New SqlClient.SqlParameter("@FOC_BATCH_NO", FOC_BATCH_NO) 'for Batch Invoice
        pParms(28) = New SqlClient.SqlParameter("@FOC_BATCH_REMARKS", FOC_BATCH_REMARKS) 'Batch Invoice Rema
        pParms(29) = New SqlClient.SqlParameter("@FOC_INVOICE", FOC_INVOICE)
        pParms(30) = New SqlClient.SqlParameter("@FOC_IsGenerateTaxInvoice", FOC_IsGenerateTaxInvoice) 'IsGenerateTaxInvoice
        pParms(31) = New SqlClient.SqlParameter("@FOC_TAX_INVOICE_REF_NO", FOC_TAX_INVOICE_REF_NO) 'Tax Invoice Reference No
        pParms(32) = New SqlClient.SqlParameter("@FOC_FEE_ID", FOC_FEE_ID) 'FEE ID
        pParms(33) = New SqlClient.SqlParameter("@FOC_EVENT_TYPE_ID", FOC_EVENT_TYPE_ID) 'EVENT TYPE ID
        pParms(34) = New SqlClient.SqlParameter("@FOC_COLLECTION_SOURCE", FOC_COLLECTION_SOURCE) 'FOC_COLLECTION_SOURCE
        pParms(35) = New SqlClient.SqlParameter("@FOC_PAYMENT_BSU_ID", FOC_PAYMENT_BSU_ID)

        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.F_SaveFEEOTHCOLLECTION_H", pParms)
        iReturnvalue = pParms(14).Value
        If iReturnvalue = 0 Then
            NEW_FOC_RECNO = pParms(12).Value
            NEW_FOC_ID = pParms(13).Value
        Else
            NEW_FOC_RECNO = ""
        End If
        Return iReturnvalue
    End Function

    Public Shared Function F_SaveFEEOTHCOLLSUB_D(ByVal FOD_ID As String, _
    ByVal FOD_FOC_ID As String, ByVal FOD_REF_ID As String, ByVal FOD_EMR_ID As String, _
    ByVal FOD_AMOUNT As String, ByVal FOD_REFNO As String, ByVal FOD_DATE As String, _
    ByVal FOD_STATUS As String, ByVal FOD_VHH_DOCNO As String, ByVal bDelete As Boolean, _
     ByVal trans As SqlTransaction, ByVal conn As SqlConnection, _
    Optional ByVal FOD_CURRENCY_AMOUNT As Double = 0, Optional ByVal FOD_CHARGE_CLIENT As Double = 0, _
    Optional ByVal FOD_BANK_ACT_ID As String = "", Optional ByVal FOC_TAX_CODE As String = "", Optional ByVal FOC_TAX_AMOUNT As Double = 0, Optional ByVal FOC_NET_AMOUNT As Double = 0) As Integer
        Dim iReturnvalue As Integer
        '@FOD_ID BIGINT,  @FOD_FOC_ID BIGINT,  @FOD_ACT_ID VARCHAR(20), 
        '@FOD_REF_ID VARCHAR(20),  @FOD_EMR_ID VARCHAR(20), @FOD_AMOUNT NUMERIC(18,3), 
        '@FOD_REFNO VARCHAR(20), @FOD_DATE DATETIME, @FOD_STATUS INT, 
        '@FOD_VHH_DOCNO VARCHAR(20), @FOD_UNIQCHQ_ID BIGINT, @bDelete bit=0
        Dim pParms(25) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FOD_ID", FOD_ID)
        pParms(1) = New SqlClient.SqlParameter("@FOD_FOC_ID", FOD_FOC_ID)
        pParms(2) = New SqlClient.SqlParameter("@FOD_REF_ID", FOD_REF_ID)
        pParms(3) = New SqlClient.SqlParameter("@FOD_EMR_ID", FOD_EMR_ID)
        pParms(4) = New SqlClient.SqlParameter("@FOD_AMOUNT", FOD_AMOUNT)
        pParms(5) = New SqlClient.SqlParameter("@FOD_REFNO", FOD_REFNO)
        If FOD_DATE = "" Then
            pParms(6) = New SqlClient.SqlParameter("@FOD_DATE", System.DBNull.Value)
        Else
            pParms(6) = New SqlClient.SqlParameter("@FOD_DATE", FOD_DATE)
        End If
        pParms(7) = New SqlClient.SqlParameter("@FOD_STATUS", FOD_STATUS)
        pParms(8) = New SqlClient.SqlParameter("@FOD_VHH_DOCNO", FOD_VHH_DOCNO)
        pParms(9) = New SqlClient.SqlParameter("@FOD_UNIQCHQ_ID", System.DBNull.Value)
        pParms(10) = New SqlClient.SqlParameter("@bDelete", bDelete)
        pParms(11) = New SqlClient.SqlParameter("@FOD_CURRENCY_AMOUNT", FOD_CURRENCY_AMOUNT)
        pParms(12) = New SqlClient.SqlParameter("@FOD_CHARGE_CLIENT", FOD_CHARGE_CLIENT)
        pParms(13) = New SqlClient.SqlParameter("@FOD_BANK_ACT_ID", FOD_BANK_ACT_ID)
        pParms(14) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(14).Direction = ParameterDirection.ReturnValue
        pParms(15) = New SqlClient.SqlParameter("@FOC_TAX_CODE", FOC_TAX_CODE)
        pParms(16) = New SqlClient.SqlParameter("@FOC_TAX_AMOUNT", FOC_TAX_AMOUNT)
        pParms(17) = New SqlClient.SqlParameter("@FOC_NET_AMOUNT", FOC_NET_AMOUNT)
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.F_SaveFEEOTHCOLLSUB_D", pParms)
        iReturnvalue = pParms(14).Value
        Return iReturnvalue
    End Function

    Public Shared Function CreateFeeCollectionOther() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable

        Dim cID As New DataColumn("ID", System.Type.GetType("System.Decimal"))
        Dim cPayMode As New DataColumn("PayMode", System.Type.GetType("System.Int16"))
        Dim cREF_ID As New DataColumn("REF_ID", System.Type.GetType("System.String"))
        Dim cREF_DESCR As New DataColumn("REF_DESCR", System.Type.GetType("System.String"))
        Dim cREF_NO As New DataColumn("REF_NO", System.Type.GetType("System.String"))
        Dim cEMR_ID As New DataColumn("EMR_ID", System.Type.GetType("System.String"))
        Dim cDocDate As New DataColumn("Date", System.Type.GetType("System.String"))
        Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
        Dim cCrAmount As New DataColumn("CrAmount", System.Type.GetType("System.Decimal"))
        Dim cCrCharge As New DataColumn("CrCharge", System.Type.GetType("System.Decimal"))
        Dim cCrTotal As New DataColumn("CrTotal", System.Type.GetType("System.Decimal"))
        Dim cFCAmount As New DataColumn("FCAmount", System.Type.GetType("System.Decimal"))
        Dim cBNK_ACT As New DataColumn("BNK_ACT", System.Type.GetType("System.String"))
        Dim cTAX_CODE As New DataColumn("TAX_CODE", System.Type.GetType("System.String"))
        Dim cTAX_AMOUNT As New DataColumn("TAX_AMOUNT", System.Type.GetType("System.Decimal"))
        Dim cTAX_NET_AMOUNT As New DataColumn("TAX_NET_AMOUNT", System.Type.GetType("System.Decimal"))
        'FEE_ID, FEE_DESCR, Opening, CurrentCharge, Net
        dtDt.Columns.Add(cID)
        dtDt.Columns.Add(cPayMode)
        dtDt.Columns.Add(cREF_ID)
        dtDt.Columns.Add(cREF_DESCR)
        dtDt.Columns.Add(cREF_NO)
        dtDt.Columns.Add(cEMR_ID)
        dtDt.Columns.Add(cDocDate)
        dtDt.Columns.Add(cAmount)
        dtDt.Columns.Add(cCrAmount)
        dtDt.Columns.Add(cCrCharge)
        dtDt.Columns.Add(cCrTotal)
        dtDt.Columns.Add(cFCAmount)
        dtDt.Columns.Add(cBNK_ACT)
        dtDt.Columns.Add(cTAX_AMOUNT)
        dtDt.Columns.Add(cTAX_CODE)
        dtDt.Columns.Add(cTAX_NET_AMOUNT)
        Return dtDt
    End Function

    Public Shared Function PrintReceipt(ByVal p_Receiptno As String, ByVal UserName As String, _
    ByVal BSU_ID As String, ByVal p_str_conn As String) As MyReportClass
        Dim str_Sql, strFilter As String
        Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT_OTHER"

        strFilter = "  FOC_RECNO='" & p_Receiptno & "' AND FOC_BSU_ID='" & BSU_ID & "' "
        str_Sql = "select * FROM " & STR_RECEIPT & " WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim repSource As New MyReportClass
        Dim ds As New DataSet
        SqlHelper.FillDataset(p_str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(p_str_conn)
            Dim params As New Hashtable
            params("UserName") = UserName
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True

            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            Dim cmdSubColln As New SqlCommand
            ''''SUBREPORT1
            Dim cmdSubPayments As New SqlCommand
            cmdSubPayments.CommandText = "select * FROM FEES.VW_OSO_FEES_PAYMENTS_OTHER where " & strFilter
            cmdSubPayments.Connection = New SqlConnection(p_str_conn)
            cmdSubPayments.CommandType = CommandType.Text
            repSourceSubRep(0).Command = cmdSubPayments

            repSource.SubReport = repSourceSubRep
            Dim FCurr As Boolean = IIf(SqlHelper.ExecuteScalar(p_str_conn, CommandType.Text, "select CASE WHEN  ltrim(rtrim(foc_cur_id))=ltrim(rtrim(BASECURRENCY)) THEN 1 ELSE 0 END FROM FEES.VW_OSO_FEES_RECEIPT_OTHER WHERE " + strFilter) = 1, False, True)

            If FCurr Then
                repSource.ResourceName = "../../fees/Reports/RPT/rptFeeOthCollectionReceiptFC.rpt"
            Else
                repSource.ResourceName = "../../fees/Reports/RPT/rptFeeOthCollectionReceipt.rpt"
            End If

        End If
        Return repSource
    End Function

    Public Shared Function PrintReceipt_DAX(ByVal p_Receiptno As String, ByVal UserName As String, _
   ByVal BSU_ID As String, ByVal p_str_conn As String, ByVal FC As Boolean) As MyReportClass
        Dim str_Sql, strFilter As String
        Dim STR_RECEIPT As String = "DAX.VW_OSO_FEES_RECEIPT_OTHER"

        strFilter = " FOC_RECNO='" & p_Receiptno & "' AND FOC_BSU_ID='" & BSU_ID & "' "
        str_Sql = "SELECT * FROM " & STR_RECEIPT & " WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim repSource As New MyReportClass
        Dim ds As New DataSet
        SqlHelper.FillDataset(p_str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(p_str_conn)
            Dim params As New Hashtable
            params("UserName") = UserName
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True

            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            Dim cmdSubColln As New SqlCommand
            ''''SUBREPORT1
            Dim cmdSubPayments As New SqlCommand
            cmdSubPayments.CommandText = "SELECT * FROM FEES.VW_OSO_FEES_PAYMENTS_OTHER WHERE " & strFilter
            cmdSubPayments.Connection = New SqlConnection(p_str_conn)
            cmdSubPayments.CommandType = CommandType.Text
            repSourceSubRep(0).Command = cmdSubPayments

            repSource.SubReport = repSourceSubRep

            If FC Then
                repSource.ResourceName = "../../fees/Reports/RPT/rptFeeOthCollectionReceipt_DAX_FC.rpt"
            Else
                repSource.ResourceName = "../../fees/Reports/RPT/rptFeeOthCollectionReceipt_DAX.rpt"
            End If

        End If
        Return repSource
    End Function

    Public Shared Function get_CollectionAccount(ByVal p_ColId As String, ByVal p_bsuid As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISFINConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT COL.COL_ACT_ID, ACT.ACT_NAME,ISNULL(OTH.OFM_ID ,0) OFM_ID FROM COLLECTION_M AS COL WITH(NOLOCK) INNER JOIN " _
            & " ACCOUNTS_M AS ACT WITH(NOLOCK) ON COL.COL_DEF_ACT_ID = ACT.ACT_ID  " _
            & " LEFT JOIN OASIS_FEES.DAX.OTH_FEECOLLECTION_MAPPING AS OTH WITH(NOLOCK) ON OTH.OFM_COL_ID=COL.COL_ID AND OTH.OFM_OASIS_ACT_CODE=COL.COL_DEF_ACT_ID " _
            & " WHERE (COL.COL_ID = '" & p_ColId & "')  "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("COL_ACT_ID") & "|" & ds.Tables(0).Rows(0)("ACT_NAME") & "|" & ds.Tables(0).Rows(0)("OFM_ID")
            Else
                Return " | "
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return ""
        End Try

    End Function
    Public Shared Function Validate_Account(ByVal p_accid As String, ByVal p_bsuid As String, ByVal p_CollId As String) As String
        Validate_Account = ""
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim str_Sql As String
            str_Sql = "EXEC FEES.GET_OTH_FEECOLLECTION_MAPPING_ACCOUNTS_FOR_AX @COL_ID = " & p_CollId & ",@BSU_ID = '" & p_bsuid & "',@DT = '" & Date.Now.ToShortDateString & "',@ACT_ID='" & p_accid & "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Validate_Account = ds.Tables(0).Rows(0)("ACT_NAME").ToString
            End If
        Catch ex As Exception
            Validate_Account = ""
        End Try
    End Function

    Public Shared Function operOnAudiTable(ByVal aud_form As String, ByVal aud_docno As String, ByVal aud_action As String, _
                                           ByVal trans As SqlTransaction, ByVal conn As SqlConnection, _
                                           Optional ByVal userInfo As String = "", Optional ByVal ocontrol As Control = Nothing, _
                                           Optional ByVal aud_remarks As String = "") As Integer
        Dim aud_remark As String = ""
        Dim Encr_decrData As New Encryption64
        Try
            If Trim(HttpContext.Current.Request.QueryString("MainMnu_code") + "") <> "" Then
                aud_form = Encr_decrData.Decrypt(HttpContext.Current.Request.QueryString("MainMnu_code").Replace(" ", "+"))
            End If
            aud_action = aud_action.ToLower()
            Select Case aud_action
                Case "insert"
                    aud_remark = "New Record inserted"
                    clearallResource()
                Case Else
                    aud_remark = aud_action
                    clearallResource()
            End Select

            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Aud_user", HttpContext.Current.Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@Aud_bsu_id", HttpContext.Current.Session("sBsuid"))
            pParms(2) = New SqlClient.SqlParameter("@Aud_module", HttpContext.Current.Session("sModule"))
            pParms(3) = New SqlClient.SqlParameter("@Aud_form", aud_form)

            pParms(4) = New SqlClient.SqlParameter("@Aud_docNo", aud_docno)
            pParms(5) = New SqlClient.SqlParameter("@Aud_action", aud_action)
            pParms(6) = New SqlClient.SqlParameter("@Aud_remarks", aud_remark & " " & aud_remarks)
            pParms(7) = New SqlClient.SqlParameter("@AUD_HOST", HttpContext.Current.Request.UserHostAddress.ToString()) 'host.HostName)
            pParms(8) = New SqlClient.SqlParameter("@AUD_WINUSER", userInfo)

            pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "dbo.SaveAudit", pParms)
            Dim ReturnFlag As Integer = pParms(9).Value
            Return ReturnFlag

        Catch ex As Exception
            Return -1
        End Try
    End Function
    Public Shared Sub clearallResource()
        Dim afterArraylist = IIf(Not HttpContext.Current.Session("afterArraylist") Is Nothing, HttpContext.Current.Session("afterArraylist"), Nothing)
        Dim beforeArrayList = IIf(Not HttpContext.Current.Session("beforeArrayList") Is Nothing, HttpContext.Current.Session("beforeArrayList"), Nothing)
        If Not beforeArrayList Is Nothing Then
            beforeArrayList.Clear()
            HttpContext.Current.Session("beforeArrayList") = beforeArrayList
        End If
        If Not afterArraylist Is Nothing Then
            afterArraylist.Clear()
            HttpContext.Current.Session("afterArraylist") = afterArraylist
        End If
    End Sub
End Class



