Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class FeeReminderTemplate

    Dim vFRM_ID As Integer
    Dim vFRM_Level As Int16
    Dim vFRM_SHORT_DESCR As String
    Dim vFRM_REMARKS As String
    Dim vFRM_Acknowledgement As String
    Dim vFRM_BSU_ID As String
    Dim vFRM_BSU_DESCR As String
    Dim vFRM_SIGNATORY As String
    Dim vFRM_TO_ADDRESS As String
    Dim vFRM_SMS As String
    Dim bEdited As Boolean
    Dim bDeleted As Boolean

    Public Property FRM_ID() As Integer
        Get
            Return vFRM_ID
        End Get
        Set(ByVal value As Integer)
            vFRM_ID = value
        End Set
    End Property

    Public Property FRM_Level() As Int16
        Get
            Return vFRM_Level
        End Get
        Set(ByVal value As Int16)
            vFRM_Level = value
        End Set
    End Property

    Public Property FRM_SHORT_DESCR() As String
        Get
            Return vFRM_SHORT_DESCR
        End Get
        Set(ByVal value As String)
            vFRM_SHORT_DESCR = value
        End Set
    End Property

    Public Property FRM_SIGNATORY() As String
        Get
            Return vFRM_SIGNATORY
        End Get
        Set(ByVal value As String)
            vFRM_SIGNATORY = value
        End Set
    End Property
    Public Property FRM_TO_ADDRESS() As String
        Get
            Return vFRM_TO_ADDRESS
        End Get
        Set(ByVal value As String)
            vFRM_TO_ADDRESS = value
        End Set
    End Property

    Public Property FRM_Acknowledgement() As String
        Get
            Return vFRM_Acknowledgement
        End Get
        Set(ByVal value As String)
            vFRM_Acknowledgement = value
        End Set
    End Property

    Public Property FRM_REMARKS() As String
        Get
            Return vFRM_REMARKS
        End Get
        Set(ByVal value As String)
            vFRM_REMARKS = value
        End Set
    End Property

    Public Property FRM_BSU_ID() As String
        Get
            Return vFRM_BSU_ID
        End Get
        Set(ByVal value As String)
            vFRM_BSU_ID = value
        End Set
    End Property

    Public Property FRM_BSU_NAME() As String
        Get
            Return vFRM_BSU_DESCR
        End Get
        Set(ByVal value As String)
            vFRM_BSU_DESCR = value
        End Set
    End Property
    Public Property FRM_SMS() As String
        Get
            Return vFRM_SMS
        End Get
        Set(ByVal value As String)
            vFRM_SMS = value
        End Set
    End Property

    Public Shared Function GetFeeReminderTemplate(ByVal FRM_ID As Integer, ByVal BSU_ID As String, ByVal conn As SqlConnection) As FeeReminderTemplate
        Dim sql_str As String
        sql_str = " EXECUTE FEES.GET_REMINDER_TEMPLATE @BSU_ID='" & BSU_ID & "',@FRM_ID=" & FRM_ID & ""
        Dim dreader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_str)
        Dim vFeeRem As New FeeReminderTemplate
        While (dreader.Read())
            vFeeRem.FRM_ID = dreader("FRM_ID")
            vFeeRem.FRM_SHORT_DESCR = dreader("FRM_SHORT_DESCR")
            vFeeRem.FRM_Acknowledgement = dreader("FRM_ACKNOWLEDGEMENT").ToString()
            vFeeRem.FRM_SIGNATORY = dreader("FRM_SIGNATORY")
            vFeeRem.FRM_TO_ADDRESS = dreader("FRM_TO_ADDRESS")
            vFeeRem.FRM_Level = dreader("FRM_Level")
            vFeeRem.FRM_BSU_ID = dreader("FRM_BSU_ID").ToString
            vFeeRem.FRM_REMARKS = dreader("FRM_REMARKS")
            vFeeRem.FRM_BSU_NAME = dreader("BSU_NAME").ToString
            vFeeRem.FRM_SMS = dreader("FRM_SMS_TEMPLATE").ToString
            vFeeRem.bEdited = True
        End While
        Return vFeeRem
    End Function



    Public Shared Function SaveFeeReminderTemplate(ByVal vSaveFeeRem As FeeReminderTemplate, ByVal trans As SqlTransaction, ByVal conn As SqlConnection) As Integer
        If vSaveFeeRem Is Nothing Then
            Return -1
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("[FEES].[F_SaveFEE_REMINDER_M]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRM_Level As New SqlParameter("@FRM_Level", SqlDbType.TinyInt)
        sqlpFRM_Level.Value = vSaveFeeRem.FRM_Level
        cmd.Parameters.Add(sqlpFRM_Level)

        Dim sqlpFRM_REMARKS As New SqlParameter("@FRM_REMARKS", SqlDbType.NVarChar)
        sqlpFRM_REMARKS.Value = vSaveFeeRem.FRM_REMARKS
        cmd.Parameters.Add(sqlpFRM_REMARKS)

        Dim sqlpFRM_Acknowledgement As New SqlParameter("@FRM_Acknowledgement", SqlDbType.NVarChar)
        sqlpFRM_Acknowledgement.Value = vSaveFeeRem.FRM_Acknowledgement
        cmd.Parameters.Add(sqlpFRM_Acknowledgement)

        Dim sqlpFRM_SHORT_DESCR As New SqlParameter("@FRM_SHORT_DESCR", SqlDbType.NVarChar)
        sqlpFRM_SHORT_DESCR.Value = vSaveFeeRem.FRM_SHORT_DESCR
        cmd.Parameters.Add(sqlpFRM_SHORT_DESCR)

        Dim sqlpFRM_BSU_ID As New SqlParameter("@FRM_BSU_ID", SqlDbType.VarChar, 20)
        If vSaveFeeRem.FRM_BSU_ID = "" Or vSaveFeeRem.FRM_BSU_ID = "-1" Then
            sqlpFRM_BSU_ID.Value = DBNull.Value
        Else
            sqlpFRM_BSU_ID.Value = vSaveFeeRem.FRM_BSU_ID
        End If
        cmd.Parameters.Add(sqlpFRM_BSU_ID)

        Dim sqlpFRM_ID As New SqlParameter("@FRM_ID", SqlDbType.Int)
        sqlpFRM_ID.Value = vSaveFeeRem.FRM_ID
        cmd.Parameters.Add(sqlpFRM_ID)

        Dim sqlpFRM_SIGNATORY As New SqlParameter("@FRM_SIGNATORY", SqlDbType.NVarChar)
        sqlpFRM_SIGNATORY.Value = vSaveFeeRem.FRM_SIGNATORY
        cmd.Parameters.Add(sqlpFRM_SIGNATORY)

        Dim sqlpFRM_TO_ADDRESS As New SqlParameter("@FRM_TO_ADDRESS", SqlDbType.NVarChar)
        sqlpFRM_TO_ADDRESS.Value = vSaveFeeRem.FRM_TO_ADDRESS
        cmd.Parameters.Add(sqlpFRM_TO_ADDRESS)

        Dim sqlpFRM_SMS As New SqlParameter("@FRM_SMS", SqlDbType.NVarChar)
        sqlpFRM_SMS.Value = vSaveFeeRem.FRM_SMS
        cmd.Parameters.Add(sqlpFRM_SMS)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vSaveFeeRem.bDeleted
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function
End Class
