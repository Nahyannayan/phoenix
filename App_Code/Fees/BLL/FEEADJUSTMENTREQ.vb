Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class FEEADJUSTMENTREQ
    Dim vFAR_ID As Integer
    Dim vFAR_STU_ID As Int64
    Dim vFAR_TO_STU_ID As Int64
    Dim vFAR_TO_STU_TYP As String
    Dim vFAR_TO_GRD_ID As String
    Dim vFAR_STU_NAME As String
    Dim vFAR_STU_NO As String
    Public vSTU_TYP As String
    Dim vFAR_ACD_ID As Integer
    Dim vFAR_DATE As DateTime
    Dim vFAR_REMARKS As String
    Dim vFAR_BSU_ID As String
    Dim vFAR_EVENT As String
    Dim vFAR_GRD_ID As String
    Dim vFAR_FAR_DOCNO As String
    Dim vTCM_LASTATTDATE As String

    Public vJOIN_GRD_ID As String
    Public vFAR_ACD_DISPLAY As String
    Public vJOIN_ACD_DISPLAY As String
    Public vJOIN_DATE As Date
    Public bDelete As Boolean
    'Public bEdit As Boolean
    Public CanEdit As Boolean
    Dim vFEE_ADJ_DET As Hashtable
    Dim vAPPR_STATUS As String
    Dim vGRM_DISPLAY As String
    Dim vAPPR_REMARKS As String
    Dim vAPPR_DATE As Date
    Dim vFAR_TO_STU_NAME As String
    Dim vGRM_TO_DISPLAY As String
    Dim vFAR_USER As String
    'Dim vFRS_TAX_CODE As String
    'Dim vFRS_TAX_AMOUNT As Double
    'Dim vFRS_NET_AMOUNT As Double
    Dim vFAR_bMANUAL_ADJ As String
    Dim vFAR_ARS_ID As String
    Dim vFAR_ISINCLUSIVE_TAX As Boolean

    Public Sub New()
        vFEE_ADJ_DET = New Hashtable
    End Sub

    Public Property FEE_ADJ_DET() As Hashtable
        Get
            Return vFEE_ADJ_DET
        End Get
        Set(ByVal value As Hashtable)
            vFEE_ADJ_DET = value
        End Set
    End Property

    Public Property IsEnquiry() As Boolean
        Get
            If vSTU_TYP = "E" Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            If value Then
                vSTU_TYP = "E"
            Else
                vSTU_TYP = "S"
            End If
        End Set
    End Property

    Public Property APPR_STATUS() As String
        Get
            Return vAPPR_STATUS
        End Get
        Set(ByVal value As String)
            vAPPR_STATUS = value
        End Set
    End Property

    Public Property LASTATTDATE() As String
        Get
            Return vTCM_LASTATTDATE
        End Get
        Set(ByVal value As String)
            vTCM_LASTATTDATE = value
        End Set
    End Property

    Public Property FAR_DOCNO() As String
        Get
            Return vFAR_FAR_DOCNO
        End Get
        Set(ByVal value As String)
            vFAR_FAR_DOCNO = value
        End Set
    End Property

    Public Property GRM_DISPLAY() As String
        Get
            Return vGRM_DISPLAY
        End Get
        Set(ByVal value As String)
            vGRM_DISPLAY = value
        End Set
    End Property

    Public Property GRM_TO_DISPLAY() As String
        Get
            Return vGRM_TO_DISPLAY
        End Get
        Set(ByVal value As String)
            vGRM_TO_DISPLAY = value
        End Set
    End Property

    Public Property FAR_TO_STU_NAME() As String
        Get
            Return vFAR_TO_STU_NAME
        End Get
        Set(ByVal value As String)
            vFAR_TO_STU_NAME = value
        End Set
    End Property

    Public Property FAH_STU_NAME() As String
        Get
            Return vFAR_STU_NAME
        End Get
        Set(ByVal value As String)
            vFAR_STU_NAME = value
        End Set
    End Property
    Public Property FAH_STU_NO() As String
        Get
            Return vFAR_STU_NO
        End Get
        Set(ByVal value As String)
            vFAR_STU_NO = value
        End Set
    End Property

    Public Property FAR_EVENT() As String
        Get
            Return vFAR_EVENT
        End Get
        Set(ByVal value As String)
            vFAR_EVENT = value
        End Set
    End Property

    Public Property FAH_BSU_ID() As String
        Get
            Return vFAR_BSU_ID
        End Get
        Set(ByVal value As String)
            vFAR_BSU_ID = value
        End Set
    End Property

    Public Property FAH_REMARKS() As String
        Get
            Return vFAR_REMARKS
        End Get
        Set(ByVal value As String)
            vFAR_REMARKS = value
        End Set
    End Property

    Public Property FAH_DATE() As DateTime
        Get
            Return vFAR_DATE
        End Get
        Set(ByVal value As DateTime)
            vFAR_DATE = value
        End Set
    End Property

    Public Property APPROVAL_REMARKS() As String
        Get
            Return vAPPR_REMARKS
        End Get
        Set(ByVal value As String)
            vAPPR_REMARKS = value
        End Set
    End Property

    Public Property APPR_DATE() As DateTime
        Get
            Return vAPPR_DATE
        End Get
        Set(ByVal value As DateTime)
            vAPPR_DATE = value
        End Set
    End Property

    Public Property FAH_GRD_ID() As String
        Get
            Return vFAR_GRD_ID
        End Get
        Set(ByVal value As String)
            vFAR_GRD_ID = value
        End Set
    End Property

    Public Property FAH_ACD_ID() As Integer
        Get
            Return vFAR_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFAR_ACD_ID = value
        End Set
    End Property

    Public Property FAH_STU_ID() As Int64
        Get
            Return vFAR_STU_ID
        End Get
        Set(ByVal value As Int64)
            vFAR_STU_ID = value
        End Set
    End Property

    Public Property FAR_TO_STU_ID() As Int64
        Get
            Return vFAR_TO_STU_ID
        End Get
        Set(ByVal value As Int64)
            vFAR_TO_STU_ID = value
        End Set
    End Property

    Public Property FAR_TO_STU_TYP() As String
        Get
            Return vFAR_TO_STU_TYP
        End Get
        Set(ByVal value As String)
            vFAR_TO_STU_TYP = value
        End Set
    End Property

    Public Property FAR_TO_GRD_ID() As String
        Get
            Return vFAR_TO_GRD_ID
        End Get
        Set(ByVal value As String)
            vFAR_TO_GRD_ID = value
        End Set
    End Property

    Public Property FAH_ID() As Integer
        Get
            Return vFAR_ID
        End Get
        Set(ByVal value As Integer)
            vFAR_ID = value
        End Set
    End Property
    'Public Property FAR_TAX_CODE() As String
    '    Get
    '        Return vFRS_TAX_CODE
    '    End Get
    '    Set(ByVal value As String)
    '        vFRS_TAX_CODE = value
    '    End Set
    'End Property

    'Public Property FAR_NET_AMOUNT() As Double
    '    Get
    '        Return vFRS_NET_AMOUNT
    '    End Get
    '    Set(ByVal value As Double)
    '        vFRS_NET_AMOUNT = value
    '    End Set
    'End Property
    'Public Property FAR_TAX_AMOUNT() As Double
    '    Get
    '        Return vFRS_TAX_AMOUNT
    '    End Get
    '    Set(ByVal value As Double)
    '        vFRS_TAX_AMOUNT = value
    '    End Set
    'End Property

    Public Property FAR_bMANUAL_ADJ() As Boolean
        Get
            Return vFAR_bMANUAL_ADJ
        End Get
        Set(ByVal value As Boolean)
            vFAR_bMANUAL_ADJ = value
        End Set
    End Property
    Public Property FAR_USER() As String
        Get
            Return vFAR_USER
        End Get
        Set(ByVal value As String)
            vFAR_USER = value
        End Set
    End Property



    Public Property FAR_ISINCLUSIVE_TAX() As Boolean
        Get
            Return vFAR_ISINCLUSIVE_TAX
        End Get
        Set(ByVal value As Boolean)
            vFAR_ISINCLUSIVE_TAX = value
        End Set
    End Property
    Public Property FAR_ARS_ID() As String
        Get
            Return vFAR_ARS_ID
        End Get
        Set(ByVal value As String)
            vFAR_ARS_ID = value
        End Set
    End Property
    Public Shared Function GetCurrentAcademicYearForBSU(ByVal BSU_ID As String) As Integer
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sql_query As String = "Select distinct ACADEMICYEAR_D.ACD_ID FROM ACADEMICYEAR_D " & _
        " where ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "' AND isnull(ACD_CURRENT, 0) = 1"
        Dim objCurrentAcademicYear As Object = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
        If Not objCurrentAcademicYear Is DBNull.Value AndAlso objCurrentAcademicYear IsNot Nothing Then
            Return objCurrentAcademicYear.ToString
        Else
            Return 0
        End If
    End Function

    Public Shared Function GetTotalFeePaid(ByVal vSTU_ID As Integer) As DataTable
        Dim conn_str As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String = "SELECT FEES.FEESCHEDULE.FSH_FEE_ID, FEES.FEES_M.FEE_DESCR," & _
         " FEES.FEESCHEDULE.FSH_DATE, FEES.FEESCHEDULE.FSH_AMOUNT,FEES.FEESCHEDULE.FSH_ACTDATE, " & _
         " FEES.FEESCHEDULE.FSH_SETTLEAMT, GRADE_M.GRD_DISPLAY, ACADEMICYEAR_M.ACY_DESCR,FSH_DRCR,FSH_SOURCE " & _
         " FROM oasis..ACADEMICYEAR_D INNER JOIN" & _
         " oasis..ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID AND " & _
         " ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN" & _
         " FEES.FEESCHEDULE INNER JOIN GRADE_M ON " & _
         " FEES.FEESCHEDULE.FSH_GRD_ID = GRADE_M.GRD_ID INNER JOIN" & _
         " FEES.FEES_M ON FEES.FEESCHEDULE.FSH_FEE_ID = FEES.FEES_M.FEE_ID ON " & _
         " ACADEMICYEAR_D.ACD_ID = FEES.FEESCHEDULE.FSH_ACD_ID " & _
         " WHERE FSH_STU_ID = " & vSTU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function IsRepeated(ByVal FRS_FAR_ID As Integer, ByVal vSTUD_ID As Integer, ByVal vACD_ID As Integer, ByVal vGRD_ID As String, ByVal vBSU_ID As String) As Boolean
        Dim conn_str As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String = "SELECT count(*) FROM FEES.FEEADJREQUEST_S INNER JOIN" & _
        " FEES.FEEADJREQUEST_H ON FEES.FEEADJREQUEST_S.FRS_FAR_ID = " & _
        " FEES.FEEADJREQUEST_H.FAR_ID WHERE isnull(FEES.FEEADJREQUEST_H.FAR_bDeleted, 0) = 0 and isnull(FAR_ApprStatus,'')='N'  " & _
        " AND FEES.FEEADJREQUEST_H.FAR_ACD_ID =  " & vACD_ID & " AND FEES.FEEADJREQUEST_H.FAR_GRD_ID = '" & vGRD_ID & _
        "' AND FEES.FEEADJREQUEST_H.FAR_STU_ID = " & vSTUD_ID & " AND FEES.FEEADJREQUEST_H.FAR_BSU_ID = '" & vBSU_ID & "'"
        If FRS_FAR_ID > 0 Then
            sql_query += " AND FRS_FAR_ID <> " & FRS_FAR_ID
        End If
        Dim obj As Object = SqlHelper.ExecuteScalar(conn_str, CommandType.Text, sql_query)
        If obj Is DBNull.Value Then
            Return False
        ElseIf obj >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function GetFeePaidForCurrentAcademicYear(ByVal vSTU_ID As Integer, ByVal vACD_ID As Integer) As DataTable
        Dim conn_str As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String = "SELECT FEES.FEESCHEDULE.FSH_FEE_ID, FEES.FEES_M.FEE_DESCR," & _
        " SUM(FEES.FEESCHEDULE.FSH_AMOUNT) FSH_AMOUNT, " & _
        " SUM(FEES.FEESCHEDULE.FSH_SETTLEAMT) FSH_SETTLEAMT, GRADE_M.GRD_DISPLAY" & _
        " FROM FEES.FEESCHEDULE INNER JOIN GRADE_M ON " & _
        " FEES.FEESCHEDULE.FSH_GRD_ID = GRADE_M.GRD_ID INNER JOIN" & _
        " FEES.FEES_M ON FEES.FEESCHEDULE.FSH_FEE_ID = FEES.FEES_M.FEE_ID" & _
        " WHERE FSH_STU_ID = " & vSTU_ID & " AND FEES.FEESCHEDULE.FSH_ACD_ID = " & vACD_ID & _
        " GROUP BY FEES.FEESCHEDULE.FSH_FEE_ID, FEES.FEES_M.FEE_DESCR," & _
        " GRADE_M.GRD_DISPLAY"

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function PopulateFeeMaster(ByVal vBSUID As String, ByVal vACD_ID As String, ByVal STU_ID As Integer, ByVal vGRD_ID As String, ByVal vFPM_STATUS As Integer, ByRef httabFPM_ID As Hashtable) As DataTable
        Dim connStr As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String = "select ISNULL(STU_STM_ID,1) from STUDENT_M WHERE STU_ID = " & STU_ID
        Dim vSTM_ID As Integer = SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql_query)
        sql_query = "SELECT FEES.FEES_M.FEE_ID, FEES.FEES_M.FEE_DESCR, " & _
        "(CASE " & vFPM_STATUS & " WHEN 1 THEN isnull(FSP_JOIN_FPM_ID, 1) " & _
        " ELSE isnull(FSP_DISCONTINUE_FPM_ID, 1) END) FPM_ID " & _
        " FROM FEES.FEES_M INNER JOIN FEES.FEESETUP_S ON FEES.FEES_M.FEE_ID = FEES.FEESETUP_S.FSP_FEE_ID " & _
        " WHERE     (ISNULL(FEES.FEES_M.FEE_SVC_ID, 0) NOT IN (SELECT SVB_SVC_ID " & _
        " FROM oasis..SERVICES_BSU_M WHERE (ISNULL(SVB_PROVIDER_BSU_ID, SVB_BSU_ID) <> SVB_BSU_ID " & _
        " and SVB_BSU_ID ='" & vBSUID & "')))" & _
        " AND FEES.FEESETUP_S.FSP_bActive = 1 AND " & _
        " FEES.FEESETUP_S.FSP_ACD_ID = '" & vACD_ID & "' AND " & _
        " FEES.FEESETUP_S.FSP_BSU_ID ='" & vBSUID & "' " & _
        " AND case when isnull(FEE_bSETUPBYGRADE,0)=1 THEN FSP_GRD_ID ELSE '" & vGRD_ID & "' END= '" & vGRD_ID & "'" & _
        " AND case when isnull(FEE_bSETUPBYGRADE,0)=1 THEN FSP_STM_ID ELSE " & vSTM_ID & " END=" & vSTM_ID

        sql_query = "EXEC FEES.GetFeeHeadforAdjustment @ACD_ID='" & vACD_ID & "',@GRD_ID='" & vGRD_ID & "',@BSU_ID='" & vBSUID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql_query)
        If Not dsData Is Nothing AndAlso Not dsData.Tables(0) Is Nothing Then
            For Each dr As DataRow In dsData.Tables(0).Rows
                httabFPM_ID(dr("FEE_ID")) = dr("FPM_ID")
            Next
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function LastAttendDate(ByVal vREFID As String) As String
        Dim connStr As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String = "SELECT REPLACE(CONVERT(VARCHAR(12),TCM_LASTATTDATE,106),' ','/')TCM_LASTATTDATE FROM OASIS.dbo.TCM_M WITH(NOLOCK) WHERE TCM_ID  = " & vREFID
        Dim LASTATTDATE As String = SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql_query)
        Return LASTATTDATE
    End Function

    Public Shared Function GetGradeAndSection(ByVal STU_ID As Integer, Optional ByVal STUD_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT) As String
        Dim obj As Object
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = ""
            Select Case STUD_TYP
                Case STUDENTTYPE.ENQUIRY
                    sql_query = "SELECT GRD_ID FROM GRADE_M WHERE GRD_DISPLAYORDER = " & _
                    " (SELECT GRADE_M.GRD_DISPLAYORDER  FROM GRADE_M INNER JOIN" & _
                    " ENQUIRY_SCHOOLPRIO_S ON GRADE_M.GRD_ID = ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID " & _
                    " WHERE EQS_ID = " & STU_ID & ") "
                Case STUDENTTYPE.STUDENT
                    sql_query = "SELECT   GRD_DISPLAY  FROM VW_OSO_STUDENT_M " & _
                    " WHERE STU_ID = '" & STU_ID & "'"
            End Select
            obj = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
        End Using
        If obj Is DBNull.Value Then
            Return ""
        Else
            Return obj
        End If
    End Function

    Public Shared Function GetFeeAdjustments(ByVal FAH_ID As Integer) As FEEADJUSTMENTREQ
        Dim str_Sql As String = String.Empty
        Dim tempvFEE_ADJ As New FEEADJUSTMENTREQ
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            str_Sql = "select isnull(FAR_STU_TYP, 'S') FROM FEES.FEEADJREQUEST_H " & _
            " WHERE FAR_ID =" & FAH_ID
            Dim vSTU_TYP As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, str_Sql)
            If vSTU_TYP = "S" Then
                str_Sql = " SELECT * FROM FEES.VW_FEEADJREQUEST_H " _
                & " WHERE FAR_ID =" & FAH_ID
            ElseIf vSTU_TYP = "E" Then
                'str_Sql = "SELECT  STU_NAME," & _
                '" ACY_DESCR JOIN_ACY_DESCR, GRD_DISPLAY JOIN_GRD_DISPLAY, '1/1/1900' STU_DOJ , ACY_DESCR, " & _
                '" STU_GRD_ID, FEES.FEEADJREQUEST_H.FAR_ID, FAR_APPRSTATUS, " & _
                '" FEES.FEEADJREQUEST_H.FAR_BSU_ID, FEES.FEEADJREQUEST_H.FAR_EVENT, " & _
                '" FEES.FEEADJREQUEST_H.FAR_REMARKS,FEES.vw_OSO_ENQUIRY_COMP.STU_NO," & _
                '" isnull(FEES.FEEADJREQUEST_H.FAR_STU_TYP,'S') FAR_STU_TYP,FAR_EVENT_SOURCE,FAR_EVENT_REF_ID," & _
                '" FEES.FEEADJREQUEST_H.FAR_DATE, isnull(FEES.FEEADJREQUEST_H.FAR_STU_TYP,'E') FAR_STU_TYP, " & _
                '" FEES.FEEADJREQUEST_H.FAR_ACD_ID, FEES.FEEADJREQUEST_H.FAR_STU_ID, GRADE_BSU_M.GRM_DISPLAY, " & _
                '" FAR_TO_STU_ID,  FAR_TO_STU_TYP, FAR_TO_GRD_ID, FAR_GRD_ID,ISNULL(FAR_bCanEdit,0)FAR_bCanEdit" & _
                '" , STU_TO.STU_NAME AS STU_NAME_TO,FEES.FEEADJREQUEST_H.FAR_DOCNO FROM FEES.vw_OSO_ENQUIRY_COMP INNER JOIN FEES.FEEADJREQUEST_H ON " & _
                '" FEES.vw_OSO_ENQUIRY_COMP.STU_ID = FEES.FEEADJREQUEST_H.FAR_STU_ID " & _
                '" INNER JOIN SECTION_M ON FAR.FAR_SCT_ID = SECTION_M.SCT_ID " & _
                '" INNER JOIN GRADE_BSU_M ON SECTION_M.SCT_GRM_ID = GRADE_BSU_M.GRM_ID " & _
                '" LEFT OUTER JOIN FEES.VW_OSO_STUDENT_ADJ_VIEW AS STU_TO ON FEES.FEEADJREQUEST_H.FAR_TO_STU_ID = STU_TO.STU_ID" & _
                '" WHERE FAR_ID =" & FAH_ID

                '                str_Sql = "SELECT     ENQ.STU_NAME, ENQ.ACY_DESCR AS JOIN_ACY_DESCR, " & _
                '" ENQ.GRD_DISPLAY AS JOIN_GRD_DISPLAY, '1/1/1900' AS STU_DOJ, ENQ.ACY_DESCR, " & _
                '" ENQ.STU_GRD_ID, FAR.FAR_ID, FAR.FAR_ApprStatus, FAR.FAR_BSU_ID, FAR.FAR_EVENT, FAR.FAR_REMARKS, ENQ.STU_NO, " & _
                '" ISNULL(FAR.FAR_STU_TYP, 'S') AS FAR_STU_TYP, FAR.FAR_EVENT_SOURCE, FAR.FAR_EVENT_REF_ID, FAR.FAR_DATE, " & _
                '" ISNULL(FAR.FAR_STU_TYP, 'E') AS FAR_STU_TYP, FAR.FAR_ACD_ID, FAR.FAR_STU_ID, ISNULL(GRM.GRM_DISPLAY,'')GRM_DISPLAY, " & _
                '" FAR.FAR_TO_STU_ID, FAR.FAR_TO_STU_TYP,FAR.FAR_TO_GRD_ID, FAR.FAR_GRD_ID, ISNULL(FAR.FAR_bCanEdit, 0) AS FAR_bCanEdit,  " & _
                '" STU_TO.STU_NAME AS STU_NAME_TO,FAR.FAR_DOCNO,FAR.FAR_ARS_ID,'' GRM_DISPLAY_TO FROM  GRADE_BSU_M AS GRM INNER JOIN " & _
                '" SECTION_M AS SCT ON GRM.GRM_ID = SCT.SCT_GRM_ID RIGHT OUTER JOIN " & _
                '" FEES.vw_OSO_ENQUIRY_COMP AS ENQ INNER JOIN " & _
                '" FEES.FEEADJREQUEST_H AS FAR ON ENQ.STU_ID = FAR.FAR_STU_ID ON SCT.SCT_ID = FAR.FAR_SCT_ID LEFT OUTER JOIN " & _
                '" FEES.VW_OSO_STUDENT_ADJ_VIEW AS STU_TO ON FAR.FAR_TO_STU_ID = STU_TO.STU_ID" & _
                '" WHERE FAR_ID =" & FAH_ID
                str_Sql = " SELECT * FROM FEES.VW_FEEADJREQUEST_H_ENQ " _
               & " WHERE FAR_ID =" & FAH_ID
            End If

            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ.APPR_STATUS = dReader("FAR_APPRSTATUS")
                tempvFEE_ADJ.FAH_ACD_ID = dReader("FAR_ACD_ID")
                tempvFEE_ADJ.FAH_BSU_ID = dReader("FAR_BSU_ID")
                tempvFEE_ADJ.FAH_DATE = dReader("FAR_DATE")
                tempvFEE_ADJ.FAH_GRD_ID = dReader("FAR_GRD_ID")
                tempvFEE_ADJ.FAH_ID = dReader("FAR_ID")
                tempvFEE_ADJ.FAR_EVENT = dReader("FAR_EVENT")
                tempvFEE_ADJ.FAH_REMARKS = dReader("FAR_REMARKS")
                tempvFEE_ADJ.FAH_STU_ID = dReader("FAR_STU_ID")
                tempvFEE_ADJ.vSTU_TYP = dReader("FAR_STU_TYP")
                tempvFEE_ADJ.FAH_STU_NAME = dReader("STU_NAME")
                tempvFEE_ADJ.vJOIN_ACD_DISPLAY = dReader("JOIN_ACY_DESCR").ToString
                tempvFEE_ADJ.vJOIN_DATE = dReader("STU_DOJ")
                tempvFEE_ADJ.vJOIN_GRD_ID = dReader("JOIN_GRD_DISPLAY")
                tempvFEE_ADJ.vFAR_ACD_DISPLAY = dReader("ACY_DESCR")
                tempvFEE_ADJ.vGRM_DISPLAY = dReader("GRM_DISPLAY")
                tempvFEE_ADJ.vFAR_TO_STU_NAME = IIf(Not dReader("STU_NAME_TO") Is System.DBNull.Value, dReader("STU_NAME_TO"), "")
                tempvFEE_ADJ.FEE_ADJ_DET = GetSubFEEAdjustmentSubDetais(FAH_ID)
                tempvFEE_ADJ.FAR_TO_GRD_ID = IIf(Not dReader("FAR_TO_GRD_ID") Is System.DBNull.Value, dReader("FAR_TO_GRD_ID"), "")
                tempvFEE_ADJ.FAR_TO_STU_ID = IIf(Not dReader("FAR_TO_STU_ID") Is System.DBNull.Value, dReader("FAR_TO_STU_ID"), 0)
                tempvFEE_ADJ.FAR_TO_STU_TYP = IIf(Not dReader("FAR_TO_STU_TYP") Is System.DBNull.Value, dReader("FAR_TO_STU_TYP"), "")
                tempvFEE_ADJ.CanEdit = dReader("FAR_bCanEdit")
                tempvFEE_ADJ.FAH_STU_NO = dReader("STU_NO")
                tempvFEE_ADJ.FAR_DOCNO = dReader("FAR_DOCNO")
                tempvFEE_ADJ.GRM_TO_DISPLAY = dReader("GRM_DISPLAY_TO")
                tempvFEE_ADJ.FAR_ARS_ID = dReader("FAR_ARS_ID")

                If dReader("FAR_EVENT_SOURCE") = "TCSO" Then
                    tempvFEE_ADJ.LASTATTDATE = LastAttendDate(dReader("FAR_EVENT_REF_ID").ToString())
                End If
                Exit While
            End While
        End Using




        Return tempvFEE_ADJ
    End Function

    Public Shared Function GetSubFEEAdjustmentSubDetais(ByVal FAH_ID As Integer) As Hashtable
        Dim str_Sql As String = String.Empty
        Dim htFEE_ADJ_DET As New Hashtable
        Dim tempvFEE_ADJ_S As New FEEADJUSTMENT_S
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            str_Sql = "SELECT     FEE.FEE_DESCR, FRS.FRS_ID, FRS.FRS_FEE_ID, FRS.FRS_FAR_ID, " _
                & " FRS.FRS_AMOUNT, FRS.FRS_REMARKS, isnull(FRS.FRS_Description,'')FRS_Description, isnull(FRS.FRS_Quantity,0)FRS_Quantity,  " _
                & " CASE FAR.FAR_ApprStatus WHEN 'A' THEN ISNULL(FRS.FRS_APPROVEDAMOUNT,0) ELSE FRS.FRS_AMOUNT END AS FRS_APPROVEDAMOUNT, " _
                & " ISNULL(FRS.FRS_TO_FEE_ID, 0) AS FRS_TO_FEE_ID, FEE_TO.FEE_DESCR AS FEE_DESCR_TO,ISNULL(FRS_TAX_CODE ,'')FRS_TAX_CODE, " _
                & " ISNULL(FRS_TAX_AMOUNT ,0)FRS_TAX_AMOUNT,ISNULL(FRS_NET_AMOUNT ,0)FRS_NET_AMOUNT " _
                & " FROM  FEES.FEES_M AS FEE WITH(NOLOCK) INNER JOIN " _
                & " FEES.FEEADJREQUEST_S AS FRS WITH(NOLOCK) ON FEE.FEE_ID = FRS.FRS_FEE_ID  INNER JOIN " _
                & " FEES.FEEADJREQUEST_H AS FAR WITH(NOLOCK) ON FRS.FRS_FAR_ID = FAR.FAR_ID LEFT OUTER JOIN " _
                & " FEES.FEES_M AS FEE_TO WITH(NOLOCK) ON FRS.FRS_TO_FEE_ID = FEE_TO.FEE_ID " _
                & " WHERE FRS.FRS_FAR_ID = " & FAH_ID
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ_S = New FEEADJUSTMENT_S
                tempvFEE_ADJ_S.FAD_ID = dReader("FRS_ID")
                tempvFEE_ADJ_S.FAD_FEE_ID = dReader("FRS_FEE_ID")
                tempvFEE_ADJ_S.FRS_TO_FEE_ID = dReader("FRS_TO_FEE_ID")
                tempvFEE_ADJ_S.FAD_FAH_ID = dReader("FRS_FAR_ID")
                tempvFEE_ADJ_S.FAD_AMOUNT = dReader("FRS_AMOUNT")
                tempvFEE_ADJ_S.APPROVEDAMOUNT = dReader("FRS_APPROVEDAMOUNT")
                tempvFEE_ADJ_S.FRS_Description = dReader("FRS_Description")
                tempvFEE_ADJ_S.DurationCount = dReader("FRS_Quantity")
                'If dReader("FRS_Description") = 1 Then
                '    tempvFEE_ADJ_S.DurationDescription = tempvFEE_ADJ_S.DurationCount & " Month(s)"
                'ElseIf dReader("FRS_Description") = 0 Then
                '    tempvFEE_ADJ_S.DurationDescription = tempvFEE_ADJ_S.DurationCount & " Week(s)"
                'ElseIf dReader("FRS_Description") = 3 Then
                '    tempvFEE_ADJ_S.DurationDescription = tempvFEE_ADJ_S.DurationCount & " Day(s)"
                'End If
                tempvFEE_ADJ_S.FAD_REMARKS = dReader("FRS_REMARKS")
                tempvFEE_ADJ_S.FEE_TYPE = dReader("FEE_DESCR")
                tempvFEE_ADJ_S.FEE_TYPE_TO = IIf(Not dReader("FEE_DESCR_TO") Is System.DBNull.Value, dReader("FEE_DESCR_TO"), "")
                tempvFEE_ADJ_S.FAD_TAX_CODE = dReader("FRS_TAX_CODE")
                tempvFEE_ADJ_S.FAD_TAX_AMOUNT = dReader("FRS_TAX_AMOUNT")
                tempvFEE_ADJ_S.FAD_NET_AMOUNT = dReader("FRS_NET_AMOUNT")
                htFEE_ADJ_DET(tempvFEE_ADJ_S.FAD_ID) = tempvFEE_ADJ_S
            End While
        End Using
        Return htFEE_ADJ_DET
    End Function

    Public Shared Function GetSubDetailsAsDataTable(ByVal vFEE_ADJ As FEEADJUSTMENTREQ) As DataTable
        Dim htDetails As Hashtable = vFEE_ADJ.FEE_ADJ_DET
        If htDetails Is Nothing Then
            Return Nothing
        End If
        Dim vFEE_DET As FEEADJUSTMENT_S
        Dim ienum As IDictionaryEnumerator = htDetails.GetEnumerator
        Dim dt As DataTable = CreateDataTable()
        Dim dr As DataRow
        While (ienum.MoveNext())
            If ienum.Value Is Nothing Then Return dt
            vFEE_DET = ienum.Value
            If vFEE_DET.bDelete Then Continue While
            dr = dt.NewRow
            GetChargedCollectedDetails(vFEE_ADJ.FAH_BSU_ID, vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_GRD_ID, vFEE_ADJ.FAH_STU_ID, vFEE_DET.FAD_FEE_ID, dr)
            dr("FAD_ID") = vFEE_DET.FAD_ID
            dr("FEE_ID") = vFEE_DET.FAD_FEE_ID
            dr("DURATION") = vFEE_DET.FRS_Description
            dr("FEE_TYPE") = vFEE_DET.FEE_TYPE
            dr("FEE_AMOUNT") = vFEE_DET.FAD_AMOUNT
            dr("FEE_APPR_AMOUNT") = vFEE_DET.APPROVEDAMOUNT
            dr("FEE_REMARKS") = vFEE_DET.FAD_REMARKS
            dr("FEE_DESCR_TO") = vFEE_DET.FEE_TYPE_TO
            dr("FAR_TAX_CODE") = vFEE_DET.FAD_TAX_CODE
            dr("FAR_TAX_AMOUNT") = vFEE_DET.FAD_TAX_AMOUNT
            dr("FAR_NET_AMOUNT") = vFEE_DET.FAD_NET_AMOUNT
            dt.Rows.Add(dr)
        End While
        Return dt
    End Function

    Private Shared Sub GetChargedCollectedDetails(ByVal vBSU_ID As String, ByVal vACD_ID As Integer, _
    ByVal vGRD_ID As String, ByVal vSTU_ID As Integer, ByVal vFEE_ID As Integer, ByRef dr As DataRow)
        Try
            Dim str_Sql As String = String.Empty
            Dim connStr As String = ConnectionManger.GetOASIS_FEESConnectionString
            str_Sql = "SELECT SUM(CASE FSH_DRCR WHEN 'DR' THEN FSH_AMOUNT ELSE 0 END) " & _
            "- SUM(CASE FSH_DRCR WHEN 'CR' THEN FSH_AMOUNT ELSE 0 END) AS 'CHARGEDAMOUNT', " & _
            " SUM(CASE FSH_DRCR WHEN 'DR' THEN FSH_SETTLEAMT ELSE 0 END) " & _
            " - SUM(CASE FSH_DRCR WHEN 'CR' THEN FSH_SETTLEAMT ELSE 0 END) AS 'PAIDAMOUNT'" & _
            " FROM FEES.FEESCHEDULE  " & _
            " WHERE ( FSH_STU_ID = " & vSTU_ID & ") AND (FSH_ACD_ID = " & vACD_ID & ") " & _
            " AND (FSH_GRD_ID = '" & vGRD_ID & "') AND FSH_FEE_ID =" & vFEE_ID & _
            " AND isnull(fsh_arm_id,0) not in (11,13) " & _
            " GROUP BY FSH_FEE_ID "
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(connStr, CommandType.Text, str_Sql)
            While (dReader.Read())
                dr("CHARGEDAMOUNT") = dReader("CHARGEDAMOUNT")
                dr("PAIDAMOUNT") = dReader("PAIDAMOUNT")
            End While
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Function CreateDataTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cFAD_ID As New DataColumn("FAD_ID", System.Type.GetType("System.Int32"))
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.Int32"))
            Dim cFEE_TYPE As New DataColumn("FEE_TYPE", System.Type.GetType("System.String"))
            Dim cCHARGEDAMOUNT As New DataColumn("CHARGEDAMOUNT", System.Type.GetType("System.Double"))
            Dim cPAIDAMOUNT As New DataColumn("PAIDAMOUNT", System.Type.GetType("System.Double"))
            Dim cDuration As New DataColumn("DURATION", System.Type.GetType("System.String"))
            Dim cFEE_AMOUNT As New DataColumn("FEE_AMOUNT", System.Type.GetType("System.Double"))
            Dim cFEE_APPR_AMOUNT As New DataColumn("FEE_APPR_AMOUNT", System.Type.GetType("System.Double"))
            Dim cFEE_REMARKS As New DataColumn("FEE_REMARKS", System.Type.GetType("System.String"))
            Dim cFEE_DESCR_TO As New DataColumn("FEE_DESCR_TO", System.Type.GetType("System.String"))
            Dim cFAR_TAX_CODE As New DataColumn("FAR_TAX_CODE", System.Type.GetType("System.String"))
            Dim cFAR_TAX_AMOUNT As New DataColumn("FAR_TAX_AMOUNT", System.Type.GetType("System.Double"))
            Dim cFAR_NET_AMOUNT As New DataColumn("FAR_NET_AMOUNT", System.Type.GetType("System.Double"))

            'dr("FAR_TAX_CODE") = vFEE_DET.FAD_TAX_CODE
            'dr("FAR_TAX_AMOUNT") = vFEE_DET.FAD_TAX_AMOUNT
            'dr("FAR_NET_AMOUNT")

            dtDt.Columns.Add(cFAD_ID)
            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cDuration)
            dtDt.Columns.Add(cFEE_TYPE)
            dtDt.Columns.Add(cCHARGEDAMOUNT)
            dtDt.Columns.Add(cPAIDAMOUNT)
            dtDt.Columns.Add(cFEE_AMOUNT)
            dtDt.Columns.Add(cFEE_APPR_AMOUNT)
            dtDt.Columns.Add(cFEE_REMARKS)
            dtDt.Columns.Add(cFEE_DESCR_TO)
            dtDt.Columns.Add(cFAR_TAX_CODE)
            dtDt.Columns.Add(cFAR_TAX_AMOUNT)
            dtDt.Columns.Add(cFAR_NET_AMOUNT)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Public Shared Function F_SAVEFEEADJREQUEST_H(ByVal FEE_ADJ_DET As FEEADJUSTMENTREQ, _
    ByRef vNEWFAR_ID As String, ByVal conn As SqlConnection, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.[F_SAVEFEEADJREQUEST_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAR_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAH_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAR_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpFAR_TO_STU_ID As New SqlParameter("@FAR_TO_STU_ID", SqlDbType.BigInt)
        sqlpFAR_TO_STU_ID.Value = FEE_ADJ_DET.FAR_TO_STU_ID
        cmd.Parameters.Add(sqlpFAR_TO_STU_ID)

        Dim sqlpFAR_TO_STU_TYP As New SqlParameter("@FAR_TO_STU_TYP", SqlDbType.VarChar)
        sqlpFAR_TO_STU_TYP.Value = FEE_ADJ_DET.FAR_TO_STU_TYP
        cmd.Parameters.Add(sqlpFAR_TO_STU_TYP)

        Dim sqlpFAR_TO_GRD_ID As New SqlParameter("@FAR_TO_GRD_ID", SqlDbType.VarChar)
        sqlpFAR_TO_GRD_ID.Value = FEE_ADJ_DET.FAR_TO_GRD_ID
        cmd.Parameters.Add(sqlpFAR_TO_GRD_ID)

        Dim sqlpFAR_STU_TYP As New SqlParameter("@FAR_STU_TYP", SqlDbType.VarChar, 1)
        sqlpFAR_STU_TYP.Value = FEE_ADJ_DET.vSTU_TYP
        cmd.Parameters.Add(sqlpFAR_STU_TYP)

        Dim sqlpFAR_GRD_ID As New SqlParameter("@FAR_GRD_ID", SqlDbType.VarChar, 10)
        sqlpFAR_GRD_ID.Value = FEE_ADJ_DET.FAH_GRD_ID
        cmd.Parameters.Add(sqlpFAR_GRD_ID)

        Dim sqlpFAH_DATE As New SqlParameter("@FAR_DATE", SqlDbType.DateTime)
        sqlpFAH_DATE.Value = FEE_ADJ_DET.FAH_DATE
        cmd.Parameters.Add(sqlpFAH_DATE)

        Dim sqlpFAH_ACD_ID As New SqlParameter("@FAR_ACD_ID", SqlDbType.Int)
        sqlpFAH_ACD_ID.Value = FEE_ADJ_DET.FAH_ACD_ID
        cmd.Parameters.Add(sqlpFAH_ACD_ID)

        Dim sqlpFAR_EVENT As New SqlParameter("@FAR_EVENT", SqlDbType.VarChar, 2)
        sqlpFAR_EVENT.Value = FEE_ADJ_DET.FAR_EVENT
        cmd.Parameters.Add(sqlpFAR_EVENT)

        Dim sqlpFAH_REMARKS As New SqlParameter("@FAR_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.FAH_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        If FEE_ADJ_DET.bDelete OrElse bEdit Then
            Dim sqlpFAH_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)
            iReturnvalue = FEEADJUSTMENTREQ.F_DeleteAllFEEADJREQUEST_S(FEE_ADJ_DET.FAH_ID, conn, trans)
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        End If

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = FEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim FAR_bCanEdit As New SqlParameter("@FAR_bCanEdit", SqlDbType.Bit)
        FAR_bCanEdit.Value = FEE_ADJ_DET.CanEdit
        cmd.Parameters.Add(FAR_bCanEdit)

        Dim sqlpFAR_USER As New SqlParameter("@FAR_USER", SqlDbType.VarChar)
        sqlpFAR_USER.Value = FEE_ADJ_DET.FAR_USER
        cmd.Parameters.Add(sqlpFAR_USER)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NewFAR_ID As New SqlParameter("@NEW_FAR_ID", SqlDbType.Int)
        NewFAR_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFAR_ID)

        Dim FAR_bMANUAL_ADJ As New SqlParameter("@FAR_bMANUAL_ADJ", SqlDbType.Bit)
        FAR_bMANUAL_ADJ.Value = FEE_ADJ_DET.FAR_bMANUAL_ADJ
        cmd.Parameters.Add(FAR_bMANUAL_ADJ)

        Dim FAR_ISINCLUSIVE_TAX As New SqlParameter("@FAR_ISINCLUSIVE_TAX", SqlDbType.Bit)
        FAR_ISINCLUSIVE_TAX.Value = FEE_ADJ_DET.FAR_ISINCLUSIVE_TAX
        cmd.Parameters.Add(FAR_ISINCLUSIVE_TAX)

        Dim FAR_ARS_ID As New SqlParameter("@FAR_ARS_ID", SqlDbType.VarChar)
        FAR_ARS_ID.Value = FEE_ADJ_DET.FAR_ARS_ID
        cmd.Parameters.Add(FAR_ARS_ID)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If Not NewFAR_ID Is Nothing AndAlso Not NewFAR_ID.Value Is DBNull.Value Then
            vNEWFAR_ID = NewFAR_ID.Value
        End If
        If iReturnvalue = 0 Then
            ' iReturnvalue = SaveFeeSubDetails(FEE_ADJ_DET, NewFAR_ID.Value, conn, trans)
            For Each vFEE_ADJ_DET As FEEADJUSTMENT_S In FEE_ADJ_DET.FEE_ADJ_DET.Values
                If vFEE_ADJ_DET Is Nothing Then
                    Continue For
                End If
                If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEEADJREQUEST_S(vFEE_ADJ_DET, NewFAR_ID.Value, conn, trans)
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                End If
            Next
        End If
        Return iReturnvalue
    End Function

    Public Shared Function F_DeleteAllFEEADJREQUEST_S(ByVal FRS_FAR_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim cmd As SqlCommand = New SqlCommand("FEES.F_DeleteAllFEEADJREQUEST_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRS_FAR_ID As New SqlParameter("@FRS_FAR_ID", SqlDbType.BigInt)
        sqlpFRS_FAR_ID.Value = FRS_FAR_ID
        cmd.Parameters.Add(sqlpFRS_FAR_ID)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()

        Return retSValParam.Value
    End Function

    Public Shared Function SaveToAdjustments(ByVal FEE_ADJ_DET As FEEADJUSTMENTREQ, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim cmd As SqlCommand = New SqlCommand("FEES.[F_SAVEFEEADJAPPOVED_TO_FEEADJ]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAR_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAH_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
        sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
        cmd.Parameters.Add(sqlpFAH_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAR_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpAPPR_DATE As New SqlParameter("@APPR_DATE", SqlDbType.DateTime)
        sqlpAPPR_DATE.Value = FEE_ADJ_DET.APPR_DATE
        cmd.Parameters.Add(sqlpAPPR_DATE)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        Return retSValParam.Value
    End Function

    Public Shared Function F_SAVEFEEADJAPPROVAL(ByVal FEE_ADJ_DET As FEEADJUSTMENTREQ, ByVal bApprove As Boolean, _
    ByRef NEW_FAH_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        cmd = New SqlCommand("FEES.[F_SAVEFEEADJAPPROVAL]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAR_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAH_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
        sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
        cmd.Parameters.Add(sqlpFAH_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAR_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpFAH_REMARKS As New SqlParameter("@APPR_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.APPROVAL_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        Dim sqlpAPPROV_CODE As New SqlParameter("@APPROV_CODE", SqlDbType.VarChar, 1)
        If bApprove Then
            sqlpAPPROV_CODE.Value = "A"
        Else
            sqlpAPPROV_CODE.Value = "R"
        End If
        cmd.Parameters.Add(sqlpAPPROV_CODE)

        Dim sqlpAPPR_DATE As New SqlParameter("@APPR_DATE", SqlDbType.DateTime)
        sqlpAPPR_DATE.Value = FEE_ADJ_DET.APPR_DATE
        cmd.Parameters.Add(sqlpAPPR_DATE)

        Dim sqlpNEW_FAH_ID As New SqlParameter("@NEW_FAH_ID", SqlDbType.Int)
        sqlpNEW_FAH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNEW_FAH_ID)

        Dim sqlpUser As New SqlParameter("@FAR_USER", SqlDbType.VarChar)
        sqlpUser.Value = FEE_ADJ_DET.FAR_USER
        cmd.Parameters.Add(sqlpUser)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If bApprove Then
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEEADJAPPROVAL_S(FEE_ADJ_DET, sqlpNEW_FAH_ID.Value, FEE_ADJ_DET.FAH_ID, conn, trans)
        End If
        NEW_FAH_ID = IIf(sqlpNEW_FAH_ID.Value Is System.DBNull.Value, "", sqlpNEW_FAH_ID.Value)
        Return iReturnvalue
    End Function

    Private Shared Function F_SAVEFEEADJAPPROVAL_S(ByVal vFEE_ADJ_APPR As FEEADJUSTMENTREQ, ByVal FAH_ID As Integer, ByVal FAR_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        For Each vFEE_ADJ_DET As FEEADJUSTMENT_S In vFEE_ADJ_APPR.FEE_ADJ_DET.Values
            If vFEE_ADJ_DET Is Nothing Then
                Continue For
            End If
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.F_SAVEFEEADJAPPROVAL_S", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFRS_FAR_ID As New SqlParameter("@FRS_FAR_ID", SqlDbType.Int)
            sqlpFRS_FAR_ID.Value = FAR_ID 'STU_SUB_DET.FPD_FPH_ID
            cmd.Parameters.Add(sqlpFRS_FAR_ID)

            Dim sqlpFAH_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)

            Dim sqlpFRS_ID As New SqlParameter("@FRS_ID", SqlDbType.Int)
            sqlpFRS_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFRS_ID)

            Dim sqlpFRS_FEE_ID As New SqlParameter("@FRS_FEE_ID", SqlDbType.Int)
            sqlpFRS_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
            cmd.Parameters.Add(sqlpFRS_FEE_ID)

            Dim sqlpFRS_AMOUNT As New SqlParameter("@APPR_AMOUNT", SqlDbType.Decimal)
            sqlpFRS_AMOUNT.Value = vFEE_ADJ_DET.APPROVEDAMOUNT
            cmd.Parameters.Add(sqlpFRS_AMOUNT)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            Dim sqlpFRS_TAX_CODE As New SqlParameter("@FRS_TAX_CODE", SqlDbType.VarChar)
            sqlpFRS_TAX_CODE.Value = vFEE_ADJ_DET.FAD_TAX_CODE
            cmd.Parameters.Add(sqlpFRS_TAX_CODE)

            Dim sqlpFRS_TAX_AMOUNT As New SqlParameter("@FRS_TAX_AMOUNT", SqlDbType.Decimal)
            sqlpFRS_TAX_AMOUNT.Value = vFEE_ADJ_DET.FAD_TAX_AMOUNT
            cmd.Parameters.Add(sqlpFRS_TAX_AMOUNT)

            Dim sqlpFRS_NET_AMOUNT As New SqlParameter("@FRS_NET_AMOUNT", SqlDbType.Decimal)
            sqlpFRS_NET_AMOUNT.Value = vFEE_ADJ_DET.FAD_NET_AMOUNT
            cmd.Parameters.Add(sqlpFRS_NET_AMOUNT)

            cmd.ExecuteNonQuery()
            iReturnvalue = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return 0
    End Function

    Private Shared Function GetSCTID(ByVal vBSU_ID As String, ByVal vSTU_ID As Integer) As Integer
        Dim str_sql As String
        str_sql = "SELECT STU_SCT_ID FROM STUDENT_M WHERE STU_ID = " & vSTU_ID
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Private Shared Function F_SAVEFEEADJREQUEST_S(ByVal vFEE_ADJ_DET As FEEADJUSTMENT_S, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer = 1
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEEADJREQUEST_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRS_FAR_ID As New SqlParameter("@FRS_FAR_ID", SqlDbType.Int)
        sqlpFRS_FAR_ID.Value = FAH_ID
        cmd.Parameters.Add(sqlpFRS_FAR_ID)

        Dim sqlpFRS_FEE_ID As New SqlParameter("@FRS_FEE_ID", SqlDbType.Int)
        sqlpFRS_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFRS_FEE_ID)

        Dim sqlpFRS_TO_FEE_ID As New SqlParameter("@FRS_TO_FEE_ID", SqlDbType.Int)
        sqlpFRS_TO_FEE_ID.Value = vFEE_ADJ_DET.FRS_TO_FEE_ID
        cmd.Parameters.Add(sqlpFRS_TO_FEE_ID)

        Dim sqlpFRS_AMOUNT As New SqlParameter("@FRS_AMOUNT", SqlDbType.Decimal)
        sqlpFRS_AMOUNT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFRS_AMOUNT)

        Dim sqlpFRS_Quantity As New SqlParameter("@FRS_Quantity", SqlDbType.Int)
        sqlpFRS_Quantity.Value = vFEE_ADJ_DET.DurationCount
        cmd.Parameters.Add(sqlpFRS_Quantity)

        Dim sqlpFRS_Description As New SqlParameter("@FRS_Description", SqlDbType.VarChar)
        sqlpFRS_Description.Value = vFEE_ADJ_DET.FRS_Description
        cmd.Parameters.Add(sqlpFRS_Description)

        Dim sqlpFRS_REMARKS As New SqlParameter("@FRS_REMARKS", SqlDbType.VarChar, 200)
        sqlpFRS_REMARKS.Value = vFEE_ADJ_DET.FAD_REMARKS
        cmd.Parameters.Add(sqlpFRS_REMARKS)

        If vFEE_ADJ_DET.bDelete Then
            Dim sqlpFRS_ID As New SqlParameter("@FRS_ID", SqlDbType.Int)
            sqlpFRS_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFRS_ID)
        End If

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = False
        cmd.Parameters.Add(sqlpbEdit)

        Dim FRS_TAX_CODE As New SqlParameter("@FRS_TAX_CODE", SqlDbType.VarChar)
        FRS_TAX_CODE.Value = vFEE_ADJ_DET.FAD_TAX_CODE
        cmd.Parameters.Add(FRS_TAX_CODE)

        Dim FRS_TAX_AMOUNT As New SqlParameter("@FRS_TAX_AMOUNT", SqlDbType.Decimal)
        FRS_TAX_AMOUNT.Value = vFEE_ADJ_DET.FAD_TAX_AMOUNT
        cmd.Parameters.Add(FRS_TAX_AMOUNT)

        Dim FRS_NET_AMOUNT As New SqlParameter("@FRS_NET_AMOUNT", SqlDbType.Decimal)
        FRS_NET_AMOUNT.Value = vFEE_ADJ_DET.FAD_NET_AMOUNT
        cmd.Parameters.Add(FRS_NET_AMOUNT)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        'If iReturnvalue <> 0 Then
        Return iReturnvalue
        'End If

    End Function

    Public Shared Function GET_ADJ_REASON(ByVal BSU_ID As String) As DataTable
        GET_ADJ_REASON = Nothing
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@USERNAME", SqlDbType.VarChar, 50)
        pParms(1).Value = HttpContext.Current.Session("sUsr_name")

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_ADJ_REASON", pParms)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            GET_ADJ_REASON = ds.Tables(0)
        End If
    End Function
End Class
