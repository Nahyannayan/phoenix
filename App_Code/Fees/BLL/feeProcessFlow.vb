Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
''' The class Created for handling the Fee Process Flow
''' Author : SHIJIN C A
''' Date : 30-APR-2008 
''' </summary>
''' <remarks></remarks>
Public Class feeProcessFlow
    Dim vFPB_ID As Integer
    Dim vFPB_PRB_ID As Integer
    Dim vFPB_BSU_ID As String
    Dim vFPB_FEE_ID As Integer
    Dim vFPB_FEE_TYPE As String
    Dim vFPB_bMandatory As Boolean
    Public bEdited As Boolean
    Public bDeleted As Boolean
    Dim bFromOtherProcess As Boolean

    Public Sub New()
        bDeleted = False
        bEdited = False
    End Sub


    ''' <summary>
    ''' Gets or Sets the FPB_ID for the current Object
    ''' This is the primary Key of the object and the 
    ''' Unique Identifier for the object created
    ''' </summary>
    ''' <value>Integer</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FPB_ID() As Integer
        Get
            Return vFPB_ID
        End Get
        Set(ByVal value As Integer)
            vFPB_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets The Process ID assosiated with the current FEE Process Flow
    ''' </summary>
    ''' <value>Integer</value>
    ''' <returns>Integer</returns>
    ''' <remarks></remarks>
    Public Property FPB_PRB_ID() As Integer
        Get
            Return vFPB_PRB_ID
        End Get
        Set(ByVal value As Integer)
            vFPB_PRB_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets The Business Unit ID assosiated with 
    ''' the current FEE Process Flow
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BSU_ID() As String
        Get
            Return vFPB_BSU_ID
        End Get
        Set(ByVal value As String)
            vFPB_BSU_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets The FEE ID assosiated with 
    ''' the current FEE Process Flow
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Public Property FEE_ID() As Integer
        Get
            Return vFPB_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFPB_FEE_ID = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets The FEE TYPE assosiated with 
    ''' the current FEE Process Flow
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FEE_TYPE() As String
        Get
            Return vFPB_FEE_TYPE
        End Get
        Set(ByVal value As String)
            vFPB_FEE_TYPE = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Gets the Required Flag
    ''' Indicates that the current Fee Type entered is 
    ''' a manditary one
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Required() As Boolean
        Get
            Return vFPB_bMandatory
        End Get
        Set(ByVal value As Boolean)
            vFPB_bMandatory = value
        End Set
    End Property

    Public Shared Function PopulateFeeMaster(ByVal vBSUID As String, ByVal vACD_ID As String) As DataTable
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim connStr As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String = "SELECT DISTINCT FEES.FEES_M.FEE_ID, FEES.FEES_M.FEE_DESCR " & _
        " FROM FEES.FEESETUP_S INNER JOIN FEES.FEES_M " & _
        " ON FEES.FEESETUP_S.FSP_FEE_ID = FEES.FEES_M.FEE_ID " & _
        " WHERE FEES.FEESETUP_S.FSP_bActive = 1 AND " & _
        " FEES.FEESETUP_S.FSP_ACD_ID = '" & vACD_ID & "' AND " & _
        " FEES.FEESETUP_S.FSP_BSU_ID ='" & vBSUID & "' " 
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Converts the feeProcessFlow objects to DataTable form.
    ''' This function is used to fill the DataGridView.
    ''' it takes a hashtable as object which contain 
    ''' FEE_ID as the Key and the feeProcessFlow as Value object
    ''' this value object will be converted into the DataRow and it 
    ''' will be added to the DataTable and returned.
    ''' </summary>
    ''' <param name="httable">HashTable that contains the objects</param>
    ''' <returns>DataTable in the converted form</returns>
    ''' <remarks></remarks>
    Public Shared Function GetDataTableFromObjects(ByVal httable As Hashtable) As DataTable
        If httable Is Nothing Then
            Return Nothing
        Else
            Dim dt As DataTable = CreateDataTable()
            Dim vFeeDet As feeProcessFlow
            Dim dr As DataRow
            Dim ienum As IDictionaryEnumerator = httable.GetEnumerator
            While (ienum.MoveNext)
                vFeeDet = ienum.Value
                If vFeeDet.bDeleted OrElse vFeeDet.bFromOtherProcess Then Continue While
                dr = dt.NewRow
                dr("FEE_ID") = vFeeDet.FEE_ID
                dr("FEE_TYPE") = vFeeDet.FEE_TYPE
                dr("bREQUIRED") = vFeeDet.Required
                dt.Rows.Add(dr)
            End While
            Return dt
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' This function is used by 'GetDataTableFromObjects' to create the table stub
    ''' </summary>
    ''' <returns>the DataTable stub</returns>
    ''' <remarks></remarks>

    Private Shared Function CreateDataTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.Int32"))
            Dim cFEE_TYPE As New DataColumn("FEE_TYPE", System.Type.GetType("System.String"))
            Dim cbRequired As New DataColumn("bRequired", System.Type.GetType("System.Boolean"))

            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cFEE_TYPE)
            dtDt.Columns.Add(cbRequired)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    ''' <summary>
    ''' Inserts  New feeProcessFlow object to the passed hashTable
    ''' It will over write if there is slready on object with the same FEE ID
    ''' </summary>
    ''' <param name="httable">hashTable to update</param>
    ''' <remarks></remarks>
    Public Sub Insert(ByRef httable As Hashtable)
        httable(FEE_ID) = Me
    End Sub

    ''' <summary>
    ''' This function marks the bDelete Flag to 'True' so that 
    ''' this can be considered as Deleted
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Delete()
        bDeleted = True
    End Sub

    ''' <summary>
    ''' Checks for Duplication of the FEE ID in the Passed HashTable
    ''' 
    ''' </summary>
    ''' <param name="htTable">THe HashTable to be checked</param>
    ''' <param name="FEEID">FEE ID for checking</param>
    ''' <returns>Return True if Duplicated</returns>
    ''' <remarks></remarks>
    Public Shared Function DuplicateEntry(ByVal htTable As Hashtable, ByVal FEEID As Integer) As Boolean
        If Not htTable(FEEID) Is Nothing Then
            Dim vFeeDet As feeProcessFlow = htTable(FEEID)
            If vFeeDet.bDeleted Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
    End Function

    Public Shared Function SaveProcessFlow(ByVal httable As Hashtable, ByVal trans As SqlTransaction, ByVal conn As SqlConnection) As Integer
        If httable Is Nothing Then
            Return -1
        End If
        Dim iReturnvalue As Integer
        Dim vSavefeePr As feeProcessFlow
        Dim cmd As SqlCommand
        Dim ienum As IDictionaryEnumerator = httable.GetEnumerator
        While ienum.MoveNext
            vSavefeePr = ienum.Value
            If vSavefeePr.bDeleted OrElse vSavefeePr.bEdited Then
                'If vSavefeePr.bDeleted OrElse Not vSavefeePr.bEdited Then
                '    Continue While
                'Else
                cmd = New SqlCommand("FEES.F_SaveFEE_PROCESSFO_BSU_D", conn, trans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpFPB_PRB_ID As New SqlParameter("@FPB_PRB_ID", SqlDbType.Int)
                sqlpFPB_PRB_ID.Value = vSavefeePr.FPB_PRB_ID
                cmd.Parameters.Add(sqlpFPB_PRB_ID)

                Dim sqlpFPB_BSU_ID As New SqlParameter("@FPB_BSU_ID", SqlDbType.VarChar, 20)
                sqlpFPB_BSU_ID.Value = vSavefeePr.BSU_ID
                cmd.Parameters.Add(sqlpFPB_BSU_ID)

                Dim sqlpFPB_FEE_ID As New SqlParameter("@FPB_FEE_ID", SqlDbType.Int)
                sqlpFPB_FEE_ID.Value = vSavefeePr.FEE_ID
                cmd.Parameters.Add(sqlpFPB_FEE_ID)

                Dim sqlpFPB_bMandatory As New SqlParameter("@FPB_bMandatory", SqlDbType.Bit)
                sqlpFPB_bMandatory.Value = vSavefeePr.Required
                cmd.Parameters.Add(sqlpFPB_bMandatory)

                If vSavefeePr.bDeleted Then
                    Dim sqlpFPB_ID As New SqlParameter("@FPB_ID", SqlDbType.Int)
                    sqlpFPB_ID.Value = vSavefeePr.FPB_ID
                    cmd.Parameters.Add(sqlpFPB_ID)
                End If

                Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
                sqlpbDelete.Value = vSavefeePr.bDeleted
                cmd.Parameters.Add(sqlpbDelete)

                Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retSValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retSValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retSValParam.Value
                If iReturnvalue <> 0 Then
                    Return iReturnvalue
                End If
            End If
        End While

    End Function

    ''' <summary>
    ''' Returns All the Fee Process Flow that is set 
    ''' from the table 'FEE_PROCESSFO_BSU_D' with the
    ''' Same Business Unit ID and Process ID
    ''' </summary>
    ''' <param name="PRD_ID">Process ID</param>
    ''' <param name="BSU_ID">Business Unit ID </param>
    ''' <param name="conn">SQL connection object</param>
    ''' <returns>HashTable contains feeProcessFlow object</returns>
    ''' <remarks></remarks>
    Public Shared Function GetFeeProcess(ByVal PRD_ID As Integer, ByVal BSU_ID As String, _
    ByVal ACD_ID As Integer, ByVal conn As SqlConnection, ByRef PROC_DESCR As String, ByRef ACY_ID As Integer, ByVal bGETALL As Boolean) As Hashtable
        Dim sql_str As String
        sql_str = "SELECT FEE_PROCESSFO_BSU_D.FPB_ID, FEE_PROCESSFO_BSU_D.FPB_PRB_ID, " & _
        " FEE_PROCESSFO_BSU_D.FPB_BSU_ID, FEE_PROCESSFO_BSU_D.FPB_FEE_ID, PROCESSFO_BSU_M.PRB_ACY_ID, " & _
        " FEE_PROCESSFO_BSU_D.FPB_bMandatory, PROCESSFO_BSU_M.PRB_ACD_ID, " & _
        " PROCESSFO_SYS_M.PRO_DESCRIPTION, PROCESSFO_BSU_M.PRB_STG_ID, FEES_M.FEE_DESCR" & _
        " FROM FEES.FEES_M RIGHT OUTER JOIN FEES.FEE_PROCESSFO_BSU_D ON " & _
        " FEES_M.FEE_ID = FEE_PROCESSFO_BSU_D.FPB_FEE_ID LEFT OUTER JOIN" & _
        " PROCESSFO_SYS_M INNER JOIN PROCESSFO_BSU_M ON PROCESSFO_SYS_M.PRO_ID = PROCESSFO_BSU_M.PRB_STG_ID ON " & _
        " FEE_PROCESSFO_BSU_D.FPB_PRB_ID = PROCESSFO_BSU_M.PRB_ID Where FPB_BSU_ID = '" & _
        BSU_ID & "' AND PRB_ACD_ID = '" & ACD_ID & "'"
        If Not bGETALL Then
            sql_str += " AND FPB_PRB_ID = " & PRD_ID
        End If
        Dim httab As New Hashtable
        Dim dreader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_str)
        Dim vFeeDet As feeProcessFlow
        While (dreader.Read())
            vFeeDet = New feeProcessFlow
            vFeeDet.FPB_ID = dreader("FPB_ID")
            vFeeDet.FPB_PRB_ID = dreader("FPB_PRB_ID")
            vFeeDet.BSU_ID = dreader("FPB_BSU_ID")
            vFeeDet.FEE_ID = dreader("FPB_FEE_ID")
            vFeeDet.FEE_TYPE = dreader("FEE_DESCR")
            vFeeDet.Required = dreader("FPB_bMandatory")
            'vFeeDet.BSU_ID = BSU_ID
            If dreader("FPB_PRB_ID") = PRD_ID Then
                vFeeDet.bEdited = True
                vFeeDet.bFromOtherProcess = False
            Else
                vFeeDet.bFromOtherProcess = True
            End If
            PROC_DESCR = dreader("PRO_DESCRIPTION")
            ACY_ID = dreader("PRB_ACY_ID")
            httab(vFeeDet.FEE_ID) = vFeeDet
        End While
        Return httab
    End Function

End Class
