﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class FEE_ADJ_INTERUNIT

#Region "Public Variables"

    Private pFAI_ID As Integer
    Public Property FAI_ID() As Integer
        Get
            Return pFAI_ID
        End Get
        Set(ByVal value As Integer)
            pFAI_ID = value
        End Set
    End Property

    Private pFAR_ID As Integer
    Public Property FAR_ID() As Integer
        Get
            Return pFAR_ID
        End Get
        Set(ByVal value As Integer)
            pFAR_ID = value
        End Set
    End Property

    Private pFRS_ID As Integer
    Public Property FRS_ID() As Integer
        Get
            Return pFRS_ID
        End Get
        Set(ByVal value As Integer)
            pFRS_ID = value
        End Set
    End Property

    Private pFAH_ID As Integer
    Public Property FAH_ID() As Integer
        Get
            Return pFAh_ID
        End Get
        Set(ByVal value As Integer)
            pFAh_ID = value
        End Set
    End Property

    Private pFAI_BSU_ID As String
    Public Property FAI_BSU_ID() As String
        Get
            Return pFAI_BSU_ID
        End Get
        Set(ByVal value As String)
            pFAI_BSU_ID = value
        End Set
    End Property
    Private pFAI_ACD_ID As Integer
    Public Property FAI_ACD_ID() As Integer
        Get
            Return pFAI_ACD_ID
        End Get
        Set(ByVal value As Integer)
            pFAI_ACD_ID = value
        End Set
    End Property
    Private pFAI_STU_ID As Integer
    Public Property FAI_STU_ID() As Integer
        Get
            Return pFAI_STU_ID
        End Get
        Set(ByVal value As Integer)
            pFAI_STU_ID = value
        End Set
    End Property
    Private pFAI_STU_TYPE As String
    Public Property FAI_STU_TYPE() As String
        Get
            Return pFAI_STU_TYPE
        End Get
        Set(ByVal value As String)
            pFAI_STU_TYPE = value
        End Set
    End Property
    Private pFAI_STU_DETAILS As String
    Public Property FAI_STU_DETAILS() As String
        Get
            Return pFAI_STU_DETAILS
        End Get
        Set(ByVal value As String)
            pFAI_STU_DETAILS = value
        End Set
    End Property
    Private pFAI_DATE As String
    Public Property FAI_DATE() As String
        Get
            Return pFAI_DATE
        End Get
        Set(ByVal value As String)
            pFAI_DATE = value
        End Set
    End Property

    Private pFAI_CR_BSU_ID As String
    Public Property FAI_CR_BSU_ID() As String
        Get
            Return pFAI_CR_BSU_ID
        End Get
        Set(ByVal value As String)
            pFAI_CR_BSU_ID = value
        End Set
    End Property

    Private pFAI_REMARKS As String
    Public Property FAI_REMARKS() As String
        Get
            Return pFAI_REMARKS
        End Get
        Set(ByVal value As String)
            pFAI_REMARKS = value
        End Set
    End Property

    Private pFAI_CREATEDON As DateTime
    Public Property FAI_CREATEDON() As DateTime
        Get
            Return pFAI_CREATEDON
        End Get
        Set(ByVal value As DateTime)
            pFAI_CREATEDON = value
        End Set
    End Property

    Private pFAI_MODIFIEDON As DateTime
    Public Property FAI_MODIFIEDON() As DateTime
        Get
            Return pFAI_MODIFIEDON
        End Get
        Set(ByVal value As DateTime)
            pFAI_MODIFIEDON = value
        End Set
    End Property

    Private pFAI_APPRVDON As DateTime
    Public Property FAI_APPRVDON() As DateTime
        Get
            Return pFAI_APPRVDON
        End Get
        Set(ByVal value As DateTime)
            pFAI_APPRVDON = value
        End Set
    End Property

    Private pFAI_REJTDON As DateTime
    Public Property FAI_REJTDON() As DateTime
        Get
            Return pFAI_REJTDON
        End Get
        Set(ByVal value As DateTime)
            pFAI_REJTDON = value
        End Set
    End Property

    Private pFAI_DELETEDON As DateTime
    Public Property FAI_DELETEDON() As DateTime
        Get
            Return pFAI_DELETEDON
        End Get
        Set(ByVal value As DateTime)
            pFAI_DELETEDON = value
        End Set
    End Property

    Private pUser As String
    Public Property User() As String
        Get
            Return pUser
        End Get
        Set(ByVal value As String)
            pUser = value
        End Set
    End Property

    Private pPROCESS As String
    Public Property PROCESS() As String
        Get
            Return pPROCESS
        End Get
        Set(ByVal value As String)
            pPROCESS = value
        End Set
    End Property

    Private pAPPROVE_STAGE As String
    Public Property APPROVE_STAGE() As String
        Get
            Return pAPPROVE_STAGE
        End Get
        Set(ByVal value As String)
            pAPPROVE_STAGE = value
        End Set
    End Property

    Private pFAI_CR_STU_ACD_ID As Integer
    Public Property FAI_CR_STU_ACD_ID() As Integer
        Get
            Return pFAI_CR_STU_ACD_ID
        End Get
        Set(ByVal value As Integer)
            pFAI_CR_STU_ACD_ID = value
        End Set
    End Property
    Private pFAI_CR_STU_ID As Integer
    Public Property FAI_CR_STU_ID() As Integer
        Get
            Return pFAI_CR_STU_ID
        End Get
        Set(ByVal value As Integer)
            pFAI_CR_STU_ID = value
        End Set
    End Property
    Private pFAI_CR_STU_TYPE As String
    Public Property FAI_CR_STU_TYPE() As String
        Get
            Return pFAI_CR_STU_TYPE
        End Get
        Set(ByVal value As String)
            pFAI_CR_STU_TYPE = value
        End Set
    End Property

    Private pFAI_CR_APPR_STATUS As String
    Public Property FAI_CR_APPR_STATUS() As String
        Get
            Return pFAI_CR_APPR_STATUS
        End Get
        Set(ByVal value As String)
            pFAI_CR_APPR_STATUS = value
        End Set
    End Property

    Private pFAI_CR_REMARKS As String
    Public Property FAI_CR_REMARKS() As String
        Get
            Return pFAI_CR_REMARKS
        End Get
        Set(ByVal value As String)
            pFAI_CR_REMARKS = value
        End Set
    End Property

    Private pFAI_DR_STU_BSU_ID As String
    Public Property FAI_DR_STU_BSU_ID() As String
        Get
            Return pFAI_DR_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            pFAI_DR_STU_BSU_ID = value
        End Set
    End Property

    Private pFAI_CR_STU_BSU_ID As String
    Public Property FAI_CR_STU_BSU_ID() As String
        Get
            Return pFAI_CR_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            pFAI_CR_STU_BSU_ID = value
        End Set
    End Property

    Private pFAI_DR_IsProvider As Boolean
    Public Property FAI_DR_IsProvider() As Boolean
        Get
            Return pFAI_DR_IsProvider
        End Get
        Set(ByVal value As Boolean)
            pFAI_DR_IsProvider = value
        End Set
    End Property

    Private pFAI_CR_IsProvider As Boolean
    Public Property FAI_CR_IsProvider() As Boolean
        Get
            Return pFAI_CR_IsProvider
        End Get
        Set(ByVal value As Boolean)
            pFAI_CR_IsProvider = value
        End Set
    End Property

    Private pFAI_COMMENTS As String
    Public Property FAI_COMMENTS() As String
        Get
            Return pFAI_COMMENTS
        End Get
        Set(ByVal value As String)
            pFAI_COMMENTS = value
        End Set
    End Property

#End Region

    Public Shared Sub PopulateBSU(ByVal ddlBSU As DropDownList, ByVal BSUID As String, ByVal NonSchool As Boolean)
        Dim dsBSU As DataSet
        Dim qry As String
        Dim COUNTRY_ID As Integer = 0
        qry = "SELECT ISNULL(BSU_COUNTRY_ID,0) AS BSU_COUNTRY_ID FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID ='" & BSUID & "'"
        COUNTRY_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        If NonSchool Then
            qry = "SELECT BSU_ID,BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID NOT IN ('" & BSUID & "') AND ISNULL(BSU_Bschool,0)=0 AND BSU_ID IN ('900501','900500') ORDER BY BSU_NAME"
        Else
            qry = "SELECT BSU_ID,BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE (ISNULL(BSU_COUNTRY_ID,0) = " & COUNTRY_ID & " OR '" & BSUID & "'='') AND BSU_ID NOT IN ('" & BSUID & "') AND (ISNULL(BSU_Bschool,0)=1 OR BSU_ID IN ('XXXXXX')) ORDER BY BSU_NAME"
        End If

        dsBSU = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        ddlBSU.DataTextField = "BSU_NAME"
        ddlBSU.DataValueField = "BSU_ID"
        ddlBSU.DataSource = dsBSU
        ddlBSU.DataBind()
    End Sub

    Public Shared Sub SetProvidersStudentBSU(ByVal ddlStudentBSU As DropDownList, ByVal ProviderBSUID As String)
        Dim str_sql As String = String.Empty
        Dim dsBSU As New DataTable
        Dim sqlParam(0) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@ProviderBsu_id", ProviderBSUID, SqlDbType.VarChar)
        dsBSU = Mainclass.getDataTable("Get_StudentBSU_For_Provider", sqlParam, ConnectionManger.GetOASIS_FEESConnectionString)
        ddlStudentBSU.DataTextField = "BSU_NAME"
        ddlStudentBSU.DataValueField = "BSU_ID"
        ddlStudentBSU.DataSource = dsBSU
        ddlStudentBSU.DataBind()
    End Sub

    Public Shared Sub PopulateACDYear(ByVal ddlAcademicYear As DropDownList, ByVal BSUID As String)
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(BSUID)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Public Shared Function CreateGvFeeDetailSchema() As DataTable
        Dim dtFee As New DataTable
        dtFee.Columns.Add("FRS_ID", System.Type.GetType("System.Int32"))
        dtFee.Columns.Add("FEE_ID", System.Type.GetType("System.Int32"))
        dtFee.Columns.Add("FEE_TYPE", System.Type.GetType("System.String"))
        dtFee.Columns.Add("DURATION", System.Type.GetType("System.Decimal"))
        dtFee.Columns.Add("FEE_AMOUNT", System.Type.GetType("System.Decimal"))
        dtFee.Columns.Add("APPR_AMOUNT", System.Type.GetType("System.Decimal"))
        dtFee.Columns.Add("FEE_REMARKS", System.Type.GetType("System.String"))


        CreateGvFeeDetailSchema = dtFee
    End Function

    Public Shared Function PopulateFeeMaster(ByVal vBSUID As String, ByVal STU_ID As Integer, ByVal vASON_DT As String, ByVal vSTU_TYPE As String, Optional ByVal vFEE_ID As Int32 = 0, Optional ByVal IUReceiving As Boolean = False) As DataTable
        Dim connStr As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String

        sql_query = "EXEC [FEES].[GET_FEEHEAD_ADJ_IU] @BSU_ID='" & vBSUID & "',@STU_ID = '" & STU_ID & "',@STU_TYPE = '" & vSTU_TYPE & "',@ASON_DT='" & vASON_DT & "',@FEE_ID=" & vFEE_ID & ",@BIUReceiving=" & IIf(IUReceiving, 1, 0).ToString

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql_query)
        If Not dsData Is Nothing AndAlso Not dsData.Tables(0) Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function PopulateFeeMaster(ByVal BSU_ID As String, ByVal ACD_ID As Int32, Optional ByVal vFEE_ID As Int32 = 0) As DataTable
        Dim connStr As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sql_query As String

        sql_query = "SELECT DISTINCT FEE_ID,FEE_DESCR FROM FEES.FEES_M A WITH(NOLOCK) INNER JOIN " & _
        "FEES.FEESETUP_M B ON A.FEE_ID=B.FSM_FEE_ID INNER JOIN ACADEMICYEAR_D ON ACD_ID=FSM_ACD_ID " & _
        "WHERE FSM_BSU_ID='" & BSU_ID & "' AND (FSM_ACD_ID=" & ACD_ID & " OR ACD_CURRENT=1) AND (FEE_ID=" & vFEE_ID & " OR " & vFEE_ID & "=0)"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql_query)
        If Not dsData Is Nothing AndAlso Not dsData.Tables(0) Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Sub LoadSavedAdjustmentH()
        Dim qry As String = " SELECT FAI_ACD_ID,FAI_DATE,FAI_STU_ID,FAI_STU_TYPE,FAI_CR_BSU_ID,FAI_STU_DETAILS,FAI_REMARKS, " & _
            "FAI_BSU_ID,ISNULL(FAI_CR_STU_ID,0)FAI_CR_STU_ID,ISNULL(FAI_CR_APPR_STATUS,'N')FAI_CR_APPR_STATUS,ISNULL(FAI_CR_REMARKS,'')FAI_CR_REMARKS, " & _
            "ISNULL(FAI_CR_STU_TYPE,'')FAI_CR_STU_TYPE,ISNULL(FAI_DR_STU_BSU_ID,'')FAI_DR_STU_BSU_ID ,ISNULL(FAI_CR_STU_BSU_ID,'')FAI_CR_STU_BSU_ID," & _
            " ISNULL(FAI_DR_IsProvider,0) FAI_DR_IsProvider,ISNULL(FAI_CR_IsProvider,0) FAI_CR_IsProvider, ISNULL(FAI_COMMENTS, '') AS FAI_COMMENTS" & _
            " FROM FEES.FEE_ADJ_INTERUNIT_H WHERE FAI_ID=" & FAI_ID & ""
        Dim connStr As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, qry)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            FAI_ACD_ID = ds.Tables(0).Rows(0)("FAI_ACD_ID").ToString
            FAI_BSU_ID = ds.Tables(0).Rows(0)("FAI_BSU_ID").ToString
            FAI_DATE = Format(ds.Tables(0).Rows(0)("FAI_DATE"), "dd/MMM/yyyy")
            FAI_STU_ID = ds.Tables(0).Rows(0)("FAI_STU_ID")
            FAI_STU_TYPE = ds.Tables(0).Rows(0)("FAI_STU_TYPE").ToString
            FAI_CR_BSU_ID = ds.Tables(0).Rows(0)("FAI_CR_BSU_ID").ToString
            FAI_STU_DETAILS = ds.Tables(0).Rows(0)("FAI_STU_DETAILS").ToString
            FAI_REMARKS = ds.Tables(0).Rows(0)("FAI_REMARKS").ToString
            FAI_CR_STU_ID = ds.Tables(0).Rows(0)("FAI_CR_STU_ID").ToString
            FAI_CR_APPR_STATUS = ds.Tables(0).Rows(0)("FAI_CR_APPR_STATUS").ToString
            FAI_CR_REMARKS = ds.Tables(0).Rows(0)("FAI_CR_REMARKS").ToString
            FAI_CR_STU_TYPE = ds.Tables(0).Rows(0)("FAI_CR_STU_TYPE").ToString

            FAI_DR_STU_BSU_ID = ds.Tables(0).Rows(0)("FAI_DR_STU_BSU_ID").ToString
            FAI_CR_STU_BSU_ID = ds.Tables(0).Rows(0)("FAI_CR_STU_BSU_ID").ToString
            FAI_DR_IsProvider = ds.Tables(0).Rows(0)("FAI_DR_IsProvider")
            FAI_CR_IsProvider = ds.Tables(0).Rows(0)("FAI_CR_IsProvider").ToString
            FAI_COMMENTS = ds.Tables(0).Rows(0)("FAI_COMMENTS").ToString
        End If

    End Sub

    Public Function LoadSavedAdjustmentS(Optional ByVal DRCR As String = "DR") As DataTable
        Dim ds As DataSet
        Dim qry = "SELECT 0 FRS_ID,FAID_ID,FEE_ID,FEE_AMOUNT,FEE_REMARKS,FEE_TYPE,0 DURATION,FEE_AMOUNT APPR_AMOUNT " & _
            "FROM FEES.vw_FEE_ADJ_INTERUNIT_D WHERE FAID_DRCR='" & DRCR & "' AND FAID_FAI_ID=" & FAI_ID & ""
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        If ds.Tables.Count > 0 Then
            LoadSavedAdjustmentS = ds.Tables(0)
        Else
            LoadSavedAdjustmentS = CreateGvFeeDetailSchema()
        End If
    End Function

    Public Function SaveAdjustmentHeader(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As String
        SaveAdjustmentHeader = "0"
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.SAVE_ADJ_INTERUNIT_H", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFAI_ID As New SqlParameter("@FAI_ID", SqlDbType.Int)
            sqlpFAI_ID.Direction = ParameterDirection.InputOutput
            sqlpFAI_ID.Value = FAI_ID
            cmd.Parameters.Add(sqlpFAI_ID)

            Dim sqlpFAR_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
            sqlpFAR_ID.Direction = ParameterDirection.InputOutput
            'sqlpFAR_ID.Value = FAR_ID
            cmd.Parameters.Add(sqlpFAR_ID)

            Dim sqlpFAI_DATE As New SqlParameter("@FAI_DATE", SqlDbType.VarChar)
            sqlpFAI_DATE.Value = FAI_DATE
            cmd.Parameters.Add(sqlpFAI_DATE)

            Dim sqlpFAI_BSU_ID As New SqlParameter("@FAI_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFAI_BSU_ID.Value = FAI_BSU_ID
            cmd.Parameters.Add(sqlpFAI_BSU_ID)

            Dim sqlpFAI_ACD_ID As New SqlParameter("@FAI_ACD_ID", SqlDbType.Int)
            sqlpFAI_ACD_ID.Value = FAI_ACD_ID
            cmd.Parameters.Add(sqlpFAI_ACD_ID)

            Dim sqlpFAI_STU_ID As New SqlParameter("@FAI_STU_ID", SqlDbType.Int)
            sqlpFAI_STU_ID.Value = FAI_STU_ID
            cmd.Parameters.Add(sqlpFAI_STU_ID)

            Dim sqlpFAI_STU_TYPE As New SqlParameter("@FAI_STU_TYPE", SqlDbType.VarChar, 4)
            sqlpFAI_STU_TYPE.Value = FAI_STU_TYPE
            cmd.Parameters.Add(sqlpFAI_STU_TYPE)

            Dim sqlpFAI_STU_DETAILS As New SqlParameter("@FAI_STU_DETAILS", SqlDbType.VarChar, 300)
            sqlpFAI_STU_DETAILS.Value = FAI_STU_DETAILS
            cmd.Parameters.Add(sqlpFAI_STU_DETAILS)

            Dim sqlpFAI_REMARKS As New SqlParameter("@FAI_REMARKS", SqlDbType.VarChar, 300)
            sqlpFAI_REMARKS.Value = FAI_REMARKS
            cmd.Parameters.Add(sqlpFAI_REMARKS)

            Dim sqlpFAI_OTHER_BSU_ID As New SqlParameter("@FAI_CR_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFAI_OTHER_BSU_ID.Value = FAI_CR_BSU_ID
            cmd.Parameters.Add(sqlpFAI_OTHER_BSU_ID)


            Dim sqlpFAI_DR_STU_BSU_ID As New SqlParameter("@FAI_DR_STU_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFAI_DR_STU_BSU_ID.Value = FAI_DR_STU_BSU_ID
            cmd.Parameters.Add(sqlpFAI_DR_STU_BSU_ID)

            Dim sqlpFAI_DR_IsProvider As New SqlParameter("@FAI_DR_IsProvider", SqlDbType.VarChar, 20)
            sqlpFAI_DR_IsProvider.Value = FAI_DR_IsProvider
            cmd.Parameters.Add(sqlpFAI_DR_IsProvider)

            Dim sqlpFAI_CR_IsProvider As New SqlParameter("@FAI_CR_IsProvider", SqlDbType.VarChar, 20)
            sqlpFAI_CR_IsProvider.Value = FAI_CR_IsProvider
            cmd.Parameters.Add(sqlpFAI_CR_IsProvider)


            Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 50)
            sqlpUSER.Value = User
            cmd.Parameters.Add(sqlpUSER)

            Dim sqlpPROCESS As New SqlParameter("@PROCESS", SqlDbType.VarChar, 10)
            sqlpPROCESS.Value = PROCESS
            cmd.Parameters.Add(sqlpPROCESS)

            If PROCESS = "RECEIVE" Then
                Dim sqlpCR_STU_ACD_ID As New SqlParameter("@FAI_CR_STU_ACD_ID", SqlDbType.Int)
                sqlpCR_STU_ACD_ID.Value = FAI_CR_STU_ACD_ID
                cmd.Parameters.Add(sqlpCR_STU_ACD_ID)

                Dim sqlpCR_STU_ID As New SqlParameter("@FAI_CR_STU_ID", SqlDbType.Int)
                sqlpCR_STU_ID.Value = FAI_CR_STU_ID
                cmd.Parameters.Add(sqlpCR_STU_ID)

                Dim sqlpCR_STU_TYPE As New SqlParameter("@FAI_CR_STU_TYPE", SqlDbType.VarChar)
                sqlpCR_STU_TYPE.Value = FAI_CR_STU_TYPE
                cmd.Parameters.Add(sqlpCR_STU_TYPE)

                Dim sqlpFAI_CR_REMARKS As New SqlParameter("@FAI_CR_REMARKS", SqlDbType.VarChar, 300)
                sqlpFAI_CR_REMARKS.Value = FAI_CR_REMARKS
                cmd.Parameters.Add(sqlpFAI_CR_REMARKS)

                Dim sqlpFAI_CR_STU_BSU_ID As New SqlParameter("@FAI_CR_STU_BSU_ID", SqlDbType.VarChar, 20)
                sqlpFAI_CR_STU_BSU_ID.Value = FAI_CR_STU_BSU_ID
                cmd.Parameters.Add(sqlpFAI_CR_STU_BSU_ID)

            End If

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            FAI_ID = sqlpFAI_ID.Value
            FAR_ID = sqlpFAR_ID.Value
            SaveAdjustmentHeader = retSValParam.Value

        Catch ex As Exception
            SaveAdjustmentHeader = "1"
            'TRANS.Rollback()
        End Try
    End Function

#Region "Detail Table Fileds"

    Private pFAID_FAI_ID As Integer
    Public Property FAID_FAI_ID() As Integer
        Get
            Return pFAID_FAI_ID
        End Get
        Set(ByVal value As Integer)
            pFAID_FAI_ID = value
        End Set
    End Property

    Private pFRS_FSP_ID As Integer
    Public Property FRS_FSP_ID() As Integer
        Get
            Return pFRS_FSP_ID
        End Get
        Set(ByVal value As Integer)
            pFRS_FSP_ID = value
        End Set
    End Property

    Private pFAID_FEE_ID As Integer
    Public Property FAID_FEE_ID() As Integer
        Get
            Return pFAID_FEE_ID
        End Get
        Set(ByVal value As Integer)
            pFAID_FEE_ID = value
        End Set
    End Property

    Private pFAID_AMOUNT As Double
    Public Property FAID_AMOUNT() As Double
        Get
            Return pFAID_AMOUNT
        End Get
        Set(ByVal value As Double)
            pFAID_AMOUNT = value
        End Set
    End Property

    Private pFAID_APR_AMOUNT As Double
    Public Property FAID_APPR_AMOUNT() As Double
        Get
            Return pFAID_APR_AMOUNT
        End Get
        Set(ByVal value As Double)
            pFAID_APR_AMOUNT = value
        End Set
    End Property

    Private pFAID_DRCR As String
    Public Property FAID_DRCR() As String
        Get
            Return pFAID_DRCR
        End Get
        Set(ByVal value As String)
            pFAID_DRCR = value
        End Set
    End Property

    Private pFAID_REMARKS As String
    Public Property FAID_REMARKS() As String
        Get
            Return pFAID_REMARKS
        End Get
        Set(ByVal value As String)
            pFAID_REMARKS = value
        End Set
    End Property

#End Region
    Public Function SaveAdjustment_Detail(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As String
        SaveAdjustment_Detail = "0"
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.SAVE_ADJ_INTERUNIT_D", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFAID_FAI_ID As New SqlParameter("@FAID_FAI_ID", SqlDbType.Int)
            sqlpFAID_FAI_ID.Value = FAID_FAI_ID
            cmd.Parameters.Add(sqlpFAID_FAI_ID)

            Dim sqlpFAID_FEE_ID As New SqlParameter("@FAID_FEE_ID", SqlDbType.Int)
            sqlpFAID_FEE_ID.Value = FAID_FEE_ID
            cmd.Parameters.Add(sqlpFAID_FEE_ID)

            Dim sqlpFAID_AMOUNT As New SqlParameter("@FAID_AMOUNT", SqlDbType.Decimal)
            sqlpFAID_AMOUNT.Value = FAID_AMOUNT
            cmd.Parameters.Add(sqlpFAID_AMOUNT)

            Dim sqlpFAID_DRCR As New SqlParameter("@FAID_DRCR", SqlDbType.VarChar)
            sqlpFAID_DRCR.Value = FAID_DRCR
            cmd.Parameters.Add(sqlpFAID_DRCR)

            Dim sqlpFAID_REMARKS As New SqlParameter("@FAID_REMARKS", SqlDbType.VarChar, 300)
            sqlpFAID_REMARKS.Value = FAID_REMARKS
            cmd.Parameters.Add(sqlpFAID_REMARKS)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            SaveAdjustment_Detail = retSValParam.Value
        Catch
            SaveAdjustment_Detail = "-1"
        End Try
    End Function

    Public Function ApproveAdjustment_IU(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As String
        ApproveAdjustment_IU = "0"
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("[FEES].[APPROVE_ADJ_INTERUNIT]", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFAI_ID As New SqlParameter("@FAI_ID", SqlDbType.Int)
            sqlpFAI_ID.Value = FAI_ID
            cmd.Parameters.Add(sqlpFAI_ID)

            Dim sqlpAPPROVE_STAGE As New SqlParameter("@APPROVE_STAGE", SqlDbType.VarChar, 25)
            sqlpAPPROVE_STAGE.Value = APPROVE_STAGE
            cmd.Parameters.Add(sqlpAPPROVE_STAGE)

            Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 50)
            sqlpUSER.Value = User
            cmd.Parameters.Add(sqlpUSER)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar, 200)
            retSValParam.Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add(retSValParam)

            Dim retVal As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar, 200)
            retVal.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retVal)


            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            ApproveAdjustment_IU = retVal.Value
        Catch
            ApproveAdjustment_IU = "-1"
        End Try
    End Function
#Region "Supporting Docs Detail Table Fileds"

    Private pFAS_ID As Integer
    Public Property FAS_ID() As Integer
        Get
            Return pFAS_ID
        End Get
        Set(ByVal value As Integer)
            pFAS_ID = value
        End Set
    End Property

    Private pFAS_CR_FAI_ID As Integer
    Public Property FAS_CR_FAI_ID() As Integer
        Get
            Return pFAS_CR_FAI_ID
        End Get
        Set(ByVal value As Integer)
            pFAS_CR_FAI_ID = value
        End Set
    End Property

    Private pFAS_DR_FAI_ID As Integer
    Public Property FAS_DR_FAI_ID() As Integer
        Get
            Return pFAS_DR_FAI_ID
        End Get
        Set(ByVal value As Integer)
            pFAS_DR_FAI_ID = value
        End Set
    End Property

    Private pFAS_bShared As Boolean
    Public Property FAS_bShared() As Boolean
        Get
            Return pFAS_bShared
        End Get
        Set(ByVal value As Boolean)
            pFAS_bShared = value
        End Set
    End Property

    Private pFAS_FILENAME As String
    Public Property FAS_FILENAME() As String
        Get
            Return pFAS_FILENAME
        End Get
        Set(ByVal value As String)
            pFAS_FILENAME = value
        End Set
    End Property

    Private pFAS_USER As String
    Public Property FAS_USER() As String
        Get
            Return pFAS_USER
        End Get
        Set(ByVal value As String)
            pFAS_USER = value
        End Set
    End Property

    Private pFAS_CONTENT_TYPE As String
    Public Property FAS_CONTENT_TYPE() As String
        Get
            Return pFAS_CONTENT_TYPE
        End Get
        Set(ByVal value As String)
            pFAS_CONTENT_TYPE = value
        End Set
    End Property

    Private pFAS_ORG_FILENAME As String
    Public Property FAS_ORG_FILENAME() As String
        Get
            Return pFAS_ORG_FILENAME
        End Get
        Set(ByVal value As String)
            pFAS_ORG_FILENAME = value
        End Set
    End Property

#End Region
    Public Function SAVE_ADJ_INTERUNIT_S(ByVal objCon As SqlConnection, ByVal TRAN As SqlTransaction) As String
        SAVE_ADJ_INTERUNIT_S = "0"
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("FEES.SAVE_FEE_ADJ_INTERUNIT_S", objCon, TRAN)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFAS_ID As New SqlParameter("@FAS_ID", SqlDbType.Int)
            sqlpFAS_ID.Value = FAS_ID
            sqlpFAS_ID.Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add(sqlpFAS_ID)

            Dim sqlpFAS_CONTENT_TYPE As New SqlParameter("@FAS_CONTENT_TYPE", SqlDbType.VarChar)
            sqlpFAS_CONTENT_TYPE.Value = FAS_CONTENT_TYPE
            cmd.Parameters.Add(sqlpFAS_CONTENT_TYPE)

            Dim sqlpFAS_ORG_FILENAME As New SqlParameter("@FAS_ORG_FILENAME", SqlDbType.VarChar)
            sqlpFAS_ORG_FILENAME.Value = FAS_ORG_FILENAME
            cmd.Parameters.Add(sqlpFAS_ORG_FILENAME)

            Dim sqlpFAS_USER As New SqlParameter("@FAS_USER", SqlDbType.VarChar)
            sqlpFAS_USER.Value = FAS_USER
            cmd.Parameters.Add(sqlpFAS_USER)

            Dim sqlpFAS_FILENAME As New SqlParameter("@FAS_FILENAME", SqlDbType.VarChar)
            sqlpFAS_FILENAME.Value = FAS_FILENAME
            cmd.Parameters.Add(sqlpFAS_FILENAME)

            Dim sqlpFAS_bShared As New SqlParameter("@FAS_bShared", SqlDbType.Bit)
            sqlpFAS_bShared.Value = FAS_bShared
            cmd.Parameters.Add(sqlpFAS_bShared)

            Dim sqlpFAS_CR_FAI_ID As New SqlParameter("@FAS_CR_FAI_ID", SqlDbType.Int)
            sqlpFAS_CR_FAI_ID.Value = FAS_CR_FAI_ID
            cmd.Parameters.Add(sqlpFAS_CR_FAI_ID)

            Dim sqlpFAS_DR_FAI_ID As New SqlParameter("@FAS_DR_FAI_ID", SqlDbType.VarChar)
            sqlpFAS_DR_FAI_ID.Value = FAS_DR_FAI_ID
            cmd.Parameters.Add(sqlpFAS_DR_FAI_ID)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            SAVE_ADJ_INTERUNIT_S = retSValParam.Value
            FAS_ID = sqlpFAS_ID.Value
        Catch
            SAVE_ADJ_INTERUNIT_S = "-1"
        End Try
    End Function
End Class
