Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class FEEADJUSTMENTTransport
    Dim vFAH_ID As Integer
    Dim vFAH_STU_ID As Integer
    Dim vFAH_STU_TYPE As String
    Dim vFAH_STU_NAME As String
    Dim vFAH_ACD_ID As Integer
    Dim vFAH_DATE As DateTime
    Dim vFAH_REMARKS As String
    Dim vFAH_BSU_ID As String
    Dim vFAH_GRD_ID As String
    Dim vFAH_DOC_NO As String

    Dim vADJ_TYPE As Integer
    Public bAllowEdit As Boolean
    Public bDelete As Boolean
    Public bEdit As Boolean
    Public vIsEnquiry As Boolean


    Dim vFEE_ADJ_DET As Hashtable
    Dim vFAH_STU_ID_CR As Integer
    Dim vFAH_STU_TYPE_CR As String
    Dim vFAH_STU_BSU_ID_CR As String
    Dim vFAH_STU_NAME_CR As String
    Dim vFAH_GRD_ID_CR As String
    Dim vFAH_STU_BSU_ID As String
    Dim vFAH_SSVID As String
    Dim vFAH_SBLID As String
    Dim VONEWAY As Boolean
    Public vFAH_SBL_ID As String
    Public vFAH_ACD_DESCR As String
    Public vTOTALAMT As Double

    ''' <summary>
    ''' Constructor to initializze variables
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        vFEE_ADJ_DET = New Hashtable
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FEE_ADJ_DET() As Hashtable
        Get
            Return vFEE_ADJ_DET
        End Get
        Set(ByVal value As Hashtable)
            vFEE_ADJ_DET = value
        End Set
    End Property

    Public Property IsEnquiry() As Boolean
        Get
            Return vIsEnquiry
        End Get
        Set(ByVal value As Boolean)
            vIsEnquiry = value
        End Set
    End Property

    Public Property ADJUSTMENTTYPE() As Integer
        Get
            Return vADJ_TYPE
        End Get
        Set(ByVal value As Integer)
            vADJ_TYPE = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_STU_TYPE() As String
        Get
            Return vFAH_STU_TYPE
        End Get
        Set(ByVal value As String)
            vFAH_STU_TYPE = value
        End Set
    End Property


    Public Property FAH_STU_NAME() As String
        Get
            Return vFAH_STU_NAME
        End Get
        Set(ByVal value As String)
            vFAH_STU_NAME = value
        End Set
    End Property

    Public Property FAH_STU_TYPE_CR() As String
        Get
            Return vFAH_STU_TYPE_CR
        End Get
        Set(ByVal value As String)
            vFAH_STU_TYPE_CR = value
        End Set
    End Property

    Public Property FAH_GRD_ID_CR() As String
        Get
            Return vFAH_GRD_ID_CR
        End Get
        Set(ByVal value As String)
            vFAH_GRD_ID_CR = value
        End Set
    End Property

    Public Property FAH_STU_NAME_CR() As String
        Get
            Return vFAH_STU_NAME_CR
        End Get
        Set(ByVal value As String)
            vFAH_STU_NAME_CR = value
        End Set
    End Property

    Public Property FAH_STU_ID_CR() As Integer
        Get
            Return vFAH_STU_ID_CR
        End Get
        Set(ByVal value As Integer)
            vFAH_STU_ID_CR = value
        End Set
    End Property

    Public Property FAH_DOC_NO() As String
        Get
            Return vFAH_DOC_NO
        End Get
        Set(ByVal value As String)
            vFAH_DOC_NO = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_BSU_ID() As String
        Get
            Return vFAH_BSU_ID
        End Get
        Set(ByVal value As String)
            vFAH_BSU_ID = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_REMARKS() As String
        Get
            Return vFAH_REMARKS
        End Get
        Set(ByVal value As String)
            vFAH_REMARKS = value
        End Set
    End Property

    

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_DATE() As DateTime
        Get
            Return vFAH_DATE
        End Get
        Set(ByVal value As DateTime)
            vFAH_DATE = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_GRD_ID() As String
        Get
            Return vFAH_GRD_ID
        End Get
        Set(ByVal value As String)
            vFAH_GRD_ID = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_STU_BSU_ID() As String
        Get
            Return vFAH_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            vFAH_STU_BSU_ID = value
        End Set
    End Property
    Public Property FAH_SSV_ID() As String
        Get
            Return vFAH_SSVID
        End Get
        Set(ByVal value As String)
            vFAH_SSVID = value
        End Set
    End Property
    Public Property FAH_SBL_ID() As String
        Get
            Return vFAH_SBLID
        End Get
        Set(ByVal value As String)
            vFAH_SBLID = value
        End Set
    End Property
    Public Property FAHONEWAY() As Boolean
        Get
            Return VONEWAY
        End Get
        Set(ByVal value As Boolean)
            VONEWAY = value
        End Set
    End Property

    Public Property FAH_STU_BSU_ID_CR() As String
        Get
            Return vFAH_STU_BSU_ID_CR
        End Get
        Set(ByVal value As String)
            vFAH_STU_BSU_ID_CR = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_ACD_ID() As Integer
        Get
            Return vFAH_ACD_ID
        End Get
        Set(ByVal value As Integer)
            vFAH_ACD_ID = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_STU_ID() As Integer
        Get
            Return vFAH_STU_ID
        End Get
        Set(ByVal value As Integer)
            vFAH_STU_ID = value
        End Set
    End Property


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FAH_ID() As Integer
        Get
            Return vFAH_ID
        End Get
        Set(ByVal value As Integer)
            vFAH_ID = value
        End Set
    End Property

    Public Property TotalAdjustmentAmount() As Double
        Get
            Return vTOTALAMT
        End Get
        Set(ByVal value As Double)
            vTOTALAMT = value
        End Set
    End Property

    Public Shared Function GetGrade(ByVal STUD_ID As String) As String
        Dim sql_query As String = "select STU_GRD_ID FROM STUDENT_M WHERE STU_ID =" & STUD_ID
        Dim connStr As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Return SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql_query)
    End Function


    Public Shared Function GetGradeDetails(ByVal STU_ID As Integer, ByVal bGetNxtGrade As Boolean, Optional ByVal STUD_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT) As String
        Dim obj As Object
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Dim sql_query As String = ""
            Select Case STUD_TYP
                Case STUDENTTYPE.ENQUIRY
                    sql_query = "SELECT GRD_ID, GRD_DISPLAY " & _
                    " FROM VW_OSO_GRADE_M WHERE GRD_DISPLAYORDER = " & _
                    " (SELECT VW_OSO_GRADE_M.GRD_DISPLAYORDER  FROM VW_OSO_GRADE_M INNER JOIN" & _
                    " ENQUIRY_SCHOOLPRIO_S ON VW_OSO_GRADE_M.GRD_ID = ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID " & _
                    " WHERE EQS_ID = " & STU_ID & ") "
                Case STUDENTTYPE.STUDENT
                    If bGetNxtGrade Then
                        sql_query = "SELECT GRD_ID, GRD_DISPLAY " & _
                        " FROM VW_OSO_GRADE_M WHERE (GRD_DISPLAYORDER -1) =" & _
                        " (SELECT VW_OSO_GRADE_M.GRD_DISPLAYORDER FROM VW_OSO_GRADE_M INNER JOIN" & _
                        " STUDENT_M ON VW_OSO_GRADE_M.GRD_ID = STUDENT_M.STU_GRD_ID" & _
                        " WHERE STU_ID = " & STU_ID & ")"
                    Else
                        sql_query = "SELECT GRD_ID, GRD_DISPLAY " & _
                        " FROM VW_OSO_GRADE_M WHERE GRD_DISPLAYORDER = " & _
                        " (SELECT VW_OSO_GRADE_M.GRD_DISPLAYORDER  FROM VW_OSO_GRADE_M INNER JOIN" & _
                        " STUDENT_M ON VW_OSO_GRADE_M.GRD_ID = STUDENT_M.STU_GRD_ID " & _
                        " WHERE STU_ID = " & STU_ID & ") or (GRD_DISPLAYORDER -1) =" & _
                        " (SELECT VW_OSO_GRADE_M.GRD_DISPLAYORDER FROM VW_OSO_GRADE_M INNER JOIN" & _
                        " STUDENT_M ON VW_OSO_GRADE_M.GRD_ID = STUDENT_M.STU_GRD_ID" & _
                        " WHERE STU_ID = " & STU_ID & ")"
                    End If
            End Select

            obj = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql_query)
        End Using
        If obj Is DBNull.Value Then
            Return ""
        Else
            Return obj
        End If
    End Function

    Public Shared Function GetGradeDetails(ByVal STU_ID As Integer, ByVal STUD_TYP As STUDENTTYPE) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Dim sql_query As String = ""
            Select Case STUD_TYP
                Case STUDENTTYPE.STUDENT
                    'sql_query = "SELECT GRD_ID, GRD_DISPLAY " & _
                    '" FROM GRADE_M WHERE GRD_DISPLAYORDER = " & _
                    '" (SELECT GRADE_M.GRD_DISPLAYORDER  FROM GRADE_M INNER JOIN" & _
                    '" STUDENT_M ON GRADE_M.GRD_ID = STUDENT_M.STU_GRD_ID " & _
                    '" WHERE STU_ID = " & STU_ID & ") or (GRD_DISPLAYORDER -1) =" & _
                    '" (SELECT GRADE_M.GRD_DISPLAYORDER FROM GRADE_M INNER JOIN" & _
                    '" STUDENT_M ON GRADE_M.GRD_ID = STUDENT_M.STU_GRD_ID" & _
                    '" WHERE STU_ID = " & STU_ID & ")"
                    sql_query = "select STU_GRD_ID GRD_ID ,GRM_DISPLAY " & _
                    "from STUDENT_M INNER JOIN  VW_OSO_GRADE_M ON " & _
                    "GRD_ID = STUDENT_M.STU_GRD_ID INNER JOIN VW_OSO_GRADE_BSU_M  " & _
                    "ON VW_OSO_GRADE_BSU_M.GRM_GRD_ID = STUDENT_M.STU_GRD_ID AND " & _
                    "VW_OSO_GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    "VW_OSO_GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID AND " & _
                    "VW_OSO_GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID AND " & _
                    "STU_ID = " & STU_ID & " union " & _
                    "select (select  GRD_ID from   VW_OSO_GRADE_M GRADE_M_1  " & _
                    "where  GRADE_M_1.GRD_DISPLAYORDER=VW_OSO_GRADE_M.GRD_DISPLAYORDER+1), " & _
                    "(select max(GRM_DISPLAY) from VW_OSO_GRADE_BSU_M,VW_OSO_GRADE_M GRADE_M_1 " & _
                    " where  GRADE_M_1.GRD_DISPLAYORDER=VW_OSO_GRADE_M.GRD_DISPLAYORDER+1 and " & _
                    " VW_OSO_GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    " GRM_GRD_ID =GRADE_M_1.GRD_ID and VW_OSO_GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID " & _
                    " AND VW_OSO_GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID ) from STUDENT_M " & _
                    " INNER JOIN  VW_OSO_GRADE_M ON GRD_ID = STUDENT_M.STU_GRD_ID INNER JOIN VW_OSO_GRADE_BSU_M " & _
                    " ON VW_OSO_GRADE_BSU_M.GRM_GRD_ID = STUDENT_M.STU_GRD_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID  AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID AND STU_ID = " & STU_ID
                Case STUDENTTYPE.ENQUIRY
                    sql_query = "SELECT GRD_ID, GRD_DISPLAY GRM_DISPLAY " & _
                    " FROM VW_OSO_GRADE_M WHERE GRD_DISPLAYORDER = " & _
                    " (SELECT VW_OSO_GRADE_M.GRD_DISPLAYORDER  FROM VW_OSO_GRADE_M INNER JOIN" & _
                    " ENQUIRY_SCHOOLPRIO_S ON VW_OSO_GRADE_M.GRD_ID = ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID " & _
                    " WHERE EQS_ID = " & STU_ID & ") "
            End Select
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            Dim dttable As New DataTable
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Public Shared Function GetFeeAdjustments(ByVal FAH_ID As Integer) As FEEADJUSTMENTTransport
        Dim str_Sql As String = String.Empty
        Dim tempvFEE_ADJ As New FEEADJUSTMENTTransport
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            str_Sql = "select isnull(FAH_STU_TYPE, 'S') FROM FEES.FEEADJUSTMENT_H " & _
            " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            Dim vSTU_TYP As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, str_Sql)
            If vSTU_TYP = "S" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " ISNULL(FEES.FEEADJUSTMENT_H.FAH_DOCNO, '') FAH_DOCNO, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE," & _
                " FEES.FEEADJUSTMENT_H.FAH_ACD_ID, FEES.FEEADJUSTMENT_H.FAH_STU_BSU_ID, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NAME, VW_OSO_STUDENT_M.STU_GRD_ID, " & _
                " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID" & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN VW_OSO_STUDENT_M " & _
                "ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = VW_OSO_STUDENT_M.STU_ID" & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            ElseIf vSTU_TYP = "E" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " ISNULL(FEES.FEEADJUSTMENT_H.FAH_DOCNO, '') FAH_DOCNO, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID, FEES.FEEADJUSTMENT_H.FAH_ACD_ID, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, FEES.vw_OSO_ENQUIRY_COMP.STU_GRD_ID, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID" & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP " & _
                " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID" & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            End If

            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ.FAH_ACD_ID = dReader("FAH_ACD_ID")
                tempvFEE_ADJ.FAH_DOC_NO = dReader("FAH_DOCNO").ToString()
                tempvFEE_ADJ.FAH_BSU_ID = dReader("FAH_BSU_ID")
                tempvFEE_ADJ.FAH_STU_BSU_ID = dReader("FAH_STU_BSU_ID")
                tempvFEE_ADJ.FAH_DATE = dReader("FAH_DATE")
                tempvFEE_ADJ.FAH_GRD_ID = dReader("STU_GRD_ID")
                tempvFEE_ADJ.FAH_ID = dReader("FAH_ID")
                tempvFEE_ADJ.FAH_REMARKS = dReader("FAH_REMARKS")
                tempvFEE_ADJ.FAH_STU_ID = dReader("FAH_STU_ID")
                tempvFEE_ADJ.FAH_STU_TYPE = dReader("FAH_STU_TYPE")
                tempvFEE_ADJ.FAH_STU_NAME = dReader("STU_NAME")
                tempvFEE_ADJ.FEE_ADJ_DET = GetSubFEEAdjustmentSubDetais(FAH_ID)
                If dReader("FAH_bPosted") = True OrElse dReader("FAH_FAR_ID") <> 0 Then
                    tempvFEE_ADJ.bAllowEdit = False
                Else
                    tempvFEE_ADJ.bAllowEdit = True
                End If
                Exit While
            End While
        End Using
        Return tempvFEE_ADJ
    End Function

    Public Shared Function GetFeeAdjustmentsInternalTransfer(ByVal FAH_ID As Integer) As FEEADJUSTMENTTransport
        Dim str_Sql As String = String.Empty
        Dim tempvFEE_ADJ As New FEEADJUSTMENTTransport
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            str_Sql = "select isnull(FAH_STU_TYPE, 'S') FROM FEES.FEEADJUSTMENT_H " & _
            " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            Dim vSTU_TYP As String = SqlHelper.ExecuteScalar(conn, CommandType.Text, str_Sql)
            If vSTU_TYP = "S" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FAH_STU_ID_CR, FAH_STU_ID) FAH_STU_ID_CR, " & _
                " isnull(FAH_STU_TYPE_CR, FAH_STU_TYPE) FAH_STU_TYPE_CR, " & _
                " FAH_STU_BSU_ID, FAH_STU_BSU_ID_CR, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE, FEES.FEEADJUSTMENT_H.FAH_ACD_ID, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NAME, VW_OSO_STUDENT_M.STU_GRD_ID, " & _
                " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID" & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN VW_OSO_STUDENT_M " & _
                " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = VW_OSO_STUDENT_M.STU_ID" & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            ElseIf vSTU_TYP = "E" Then
                str_Sql = "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_FAR_ID, 0) FAH_FAR_ID, " & _
                " isnull(FEES.FEEADJUSTMENT_H.FAH_bPosted, 0) FAH_bPosted , " & _
                " FEES.FEEADJUSTMENT_H.FAH_STU_ID, FEES.FEEADJUSTMENT_H.FAH_ACD_ID, " & _
                " isnull(FAH_STU_ID_CR, FAH_STU_ID) FAH_STU_ID_CR, " & _
                " isnull(FAH_STU_TYPE_CR, FAH_STU_TYPE) FAH_STU_TYPE_CR, " & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE,FEES.FEEADJUSTMENT_H.FAH_STU_TYPE, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, FEES.vw_OSO_ENQUIRY_COMP.STU_GRD_ID, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID" & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP " & _
                " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID" & _
                " WHERE FAH_ID =" & FAH_ID & " AND FEES.FEEADJUSTMENT_H.FAH_bDeleted = 0 "
            End If
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ.FAH_ACD_ID = dReader("FAH_ACD_ID")
                tempvFEE_ADJ.FAH_BSU_ID = dReader("FAH_BSU_ID")
                tempvFEE_ADJ.FAH_DATE = dReader("FAH_DATE")
                tempvFEE_ADJ.FAH_GRD_ID = dReader("STU_GRD_ID")
                tempvFEE_ADJ.FAH_STU_TYPE_CR = dReader("FAH_STU_TYPE_CR")
                tempvFEE_ADJ.FAH_STU_ID_CR = dReader("FAH_STU_ID_CR")
                tempvFEE_ADJ.FAH_STU_BSU_ID = dReader("FAH_STU_BSU_ID")
                tempvFEE_ADJ.FAH_STU_BSU_ID_CR = dReader("FAH_STU_BSU_ID_CR")
                If tempvFEE_ADJ.FAH_STU_TYPE_CR = "S" Then
                    tempvFEE_ADJ.FAH_STU_NAME_CR = GetStudentName(tempvFEE_ADJ.FAH_STU_ID_CR, False)
                ElseIf tempvFEE_ADJ.FAH_STU_TYPE_CR = "E" Then
                    tempvFEE_ADJ.FAH_STU_NAME_CR = GetStudentName(tempvFEE_ADJ.FAH_STU_ID_CR, True)
                End If
                tempvFEE_ADJ.FAH_ID = dReader("FAH_ID")
                tempvFEE_ADJ.FAH_REMARKS = dReader("FAH_REMARKS")
                tempvFEE_ADJ.FAH_STU_ID = dReader("FAH_STU_ID")
                tempvFEE_ADJ.FAH_STU_TYPE = dReader("FAH_STU_TYPE")
                tempvFEE_ADJ.FAH_STU_NAME = dReader("STU_NAME")
                tempvFEE_ADJ.TotalAdjustmentAmount = GetTotalAdjustmentAmount(FAH_ID)
                tempvFEE_ADJ.FEE_ADJ_DET = GetSubFEEAdjustmentSubDetais(FAH_ID)
                If dReader("FAH_bPosted") = True OrElse dReader("FAH_FAR_ID") <> 0 Then
                    tempvFEE_ADJ.bAllowEdit = False
                Else
                    tempvFEE_ADJ.bAllowEdit = True
                End If
                Exit While
            End While
        End Using
        Return tempvFEE_ADJ
    End Function

    Private Shared Function GetTotalAdjustmentAmount(ByVal vFAH_ID As Integer) As Double
        Dim strSql As String = "SELECT SUM(FAD_AMOUNT) FROM FEES.FEEADJUSTMENT_S WHERE FAD_FAH_ID =" & vFAH_ID
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, strSql)
    End Function

    Private Shared Function GetStudentName(ByVal STU_ID As String, ByVal bEnquiry As Boolean) As String
        Dim str_sql As String
        If bEnquiry Then
            str_sql = " SELECT STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP WHERE STU_ID = " & STU_ID
        Else
            str_sql = " SELECT VW_OSO_STUDENT_M.STU_NAME FROM VW_OSO_STUDENT_M WHERE STU_ID = " & STU_ID
        End If
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
    End Function

    Public Shared Function GetSubFEEAdjustmentSubDetais(ByVal FAH_ID As Integer) As Hashtable
        Dim str_Sql As String = String.Empty
        Dim htFEE_ADJ_DET As New Hashtable
        Dim tempvFEE_ADJ_S As New FEEADJUSTMENT_S
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            str_Sql = " SELECT FAD.FAD_ID, FAD.FAD_FAH_ID, FAD.FAD_FEE_ID, " & _
           " ISNULL(FAD.FAD_TO_FEE_ID, 0) AS FAD_TO_FEE_ID, FAD.FAD_AMOUNT, " & _
           " FAD.FAD_REMARKS, FEE.FEE_DESCR, ISNULL(FAH.FAH_bOneway, 0) AS FAH_bOneway" & _
           " FROM FEES.FEEADJUSTMENT_S AS FAD INNER JOIN" & _
           " VW_OSO_FEES_M AS FEE ON FAD.FAD_FEE_ID = FEE.FEE_ID INNER JOIN" & _
           " FEES.FEEADJUSTMENT_H AS FAH ON FAD.FAD_FAH_ID = FAH.FAH_ID" & _
           " WHERE FAD_FAH_ID =" & FAH_ID
            Dim dReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_Sql)
            While (dReader.Read())
                tempvFEE_ADJ_S = New FEEADJUSTMENT_S
                tempvFEE_ADJ_S.FAD_ID = dReader("FAD_ID")
                tempvFEE_ADJ_S.FAD_FEE_ID = dReader("FAD_FEE_ID")
                tempvFEE_ADJ_S.FAD_TO_FEE_ID = dReader("FAD_TO_FEE_ID")
                tempvFEE_ADJ_S.FAD_FAH_ID = dReader("FAD_FAH_ID")
                tempvFEE_ADJ_S.FAD_AMOUNT = dReader("FAD_AMOUNT")
                tempvFEE_ADJ_S.FAD_REMARKS = dReader("FAD_REMARKS")
                tempvFEE_ADJ_S.FEE_TYPE = dReader("FEE_DESCR")
                tempvFEE_ADJ_S.oneway = dReader("FAH_bOneway")
                htFEE_ADJ_DET(tempvFEE_ADJ_S.FAD_FEE_ID) = tempvFEE_ADJ_S
            End While
        End Using
        Return htFEE_ADJ_DET
    End Function

    Public Shared Function GetSubDetailsAsDataTable(ByVal htDetails As Hashtable) As DataTable
        If htDetails Is Nothing Then
            Return Nothing
        End If
        Dim vFEE_DET As FEEADJUSTMENT_S
        Dim ienum As IDictionaryEnumerator = htDetails.GetEnumerator
        Dim dt As DataTable = CreateDataTable()
        Dim dr As DataRow
        While (ienum.MoveNext())
            vFEE_DET = ienum.Value
            If vFEE_DET.bDelete Then Continue While
            dr = dt.NewRow
            dr("FEE_ID") = vFEE_DET.FAD_FEE_ID
            dr("FEE_TYPE") = vFEE_DET.FEE_TYPE
            If vFEE_DET.AdjustmentType = -1 Then
                dr("ADJTYPE") = "Negtive"
            ElseIf vFEE_DET.AdjustmentType = 1 Then
                dr("ADJTYPE") = "Positive"
            End If
            dr("FEE_AMOUNT") = vFEE_DET.FAD_AMOUNT
            dr("FEE_REMARKS") = vFEE_DET.FAD_REMARKS
            dr("ONE_WAY") = vFEE_DET.oneway
            dr("SSV_ID") = vFEE_DET.SSV_ID

            dt.Rows.Add(dr)
        End While
        Return dt
    End Function

    Private Shared Function CreateDataTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.Int32"))
            Dim cFEE_TYPE As New DataColumn("FEE_TYPE", System.Type.GetType("System.String"))
            Dim cFEE_ADJ_TYPE As New DataColumn("ADJTYPE", System.Type.GetType("System.String"))
            Dim cFEE_AMOUNT As New DataColumn("FEE_AMOUNT", System.Type.GetType("System.Double"))
            Dim cFEE_REMARKS As New DataColumn("FEE_REMARKS", System.Type.GetType("System.String"))
            Dim coneway As New DataColumn("ONE_WAY", System.Type.GetType("System.String"))
            Dim cSSV_ID As New DataColumn("SSV_ID", System.Type.GetType("System.String"))
            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cFEE_ADJ_TYPE)
            dtDt.Columns.Add(cFEE_TYPE)
            dtDt.Columns.Add(cFEE_AMOUNT)
            dtDt.Columns.Add(cFEE_REMARKS)
            dtDt.Columns.Add(coneway)
            dtDt.Columns.Add(cSSV_ID)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Public Shared Function GetBalanceAmountForFeeType(ByVal vBSU_ID As String, ByVal vSTU_BSU_ID As String, ByVal stud_typ As String, ByVal STU_ID As String, ByVal asondate As String, ByVal feeID As Integer) As Double
        If asondate = "" Then Return 0
        Dim str_sql As String
        str_sql = " SELECT SUM(CASE WHEN DRCR = 'DR' THEN 1 ELSE - 1 END * AMOUNT) AS AMOUNT " & _
        "FROM FEES.fn_STUDENTLEDGERALL('" & vBSU_ID & "', '" & vSTU_BSU_ID & "') " & _
        " WHERE (DOCDATE <= '" & asondate & "')" & " AND FEE_ID = " & feeID & _
        " AND STU_ID = " & STU_ID & " AND STU_TYPE = '" & stud_typ & "'"
        Dim obj As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
        If obj Is DBNull.Value Then
            Return 0
        Else
            Return CDbl(obj)
        End If
    End Function

    Public Shared Function SaveDetailsInternal(ByVal FEE_ADJ_DET As FEEADJUSTMENTTransport, ByRef vNewFAH_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.[F_SAVEFEEADJUSTMENT_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAH_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAH_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpFAH_STU_TYPE As New SqlParameter("@FAH_STU_TYPE", SqlDbType.VarChar, 2)
        sqlpFAH_STU_TYPE.Value = FEE_ADJ_DET.FAH_STU_TYPE
        cmd.Parameters.Add(sqlpFAH_STU_TYPE)

        Dim sqlpFAH_STU_BSU_ID As New SqlParameter("@FAH_STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_STU_BSU_ID.Value = FEE_ADJ_DET.FAH_STU_BSU_ID
        cmd.Parameters.Add(sqlpFAH_STU_BSU_ID)

        Dim sqlpFSH_STU_TYPE_CR As New SqlParameter("@FAH_STU_TYPE_CR", SqlDbType.VarChar, 4)
        sqlpFSH_STU_TYPE_CR.Value = FEE_ADJ_DET.FAH_STU_TYPE_CR
        cmd.Parameters.Add(sqlpFSH_STU_TYPE_CR)

        Dim sqlpFSH_STU_ID_CR As New SqlParameter("@FAH_STU_ID_CR", SqlDbType.VarChar, 15)
        sqlpFSH_STU_ID_CR.Value = FEE_ADJ_DET.FAH_STU_ID_CR
        cmd.Parameters.Add(sqlpFSH_STU_ID_CR)

        Dim sqlpFAH_STU_BSU_ID_CR As New SqlParameter("@FAH_STU_BSU_ID_CR", SqlDbType.VarChar, 20)
        sqlpFAH_STU_BSU_ID_CR.Value = FEE_ADJ_DET.FAH_STU_BSU_ID_CR
        cmd.Parameters.Add(sqlpFAH_STU_BSU_ID_CR)

        Dim sqlpFAH_DATE As New SqlParameter("@FAH_DATE", SqlDbType.DateTime)
        sqlpFAH_DATE.Value = FEE_ADJ_DET.FAH_DATE
        cmd.Parameters.Add(sqlpFAH_DATE)

        Dim sqlpFAH_bInter As New SqlParameter("@FAH_bInter", SqlDbType.Bit)
        sqlpFAH_bInter.Value = 1
        cmd.Parameters.Add(sqlpFAH_bInter)

        Dim sqlpFAH_ACD_ID As New SqlParameter("@FAH_ACD_ID", SqlDbType.Int)
        sqlpFAH_ACD_ID.Value = FEE_ADJ_DET.FAH_ACD_ID
        cmd.Parameters.Add(sqlpFAH_ACD_ID)

        Dim sqlpFAH_ADJTYPE As New SqlParameter("@FAH_ADJTYPE", SqlDbType.VarChar, 50)
        If FEE_ADJ_DET.ADJUSTMENTTYPE = 1 Then
            sqlpFAH_ADJTYPE.Value = "--"
        ElseIf FEE_ADJ_DET.ADJUSTMENTTYPE = -1 Then
            sqlpFAH_ADJTYPE.Value = "--"
        End If
        cmd.Parameters.Add(sqlpFAH_ADJTYPE)

        Dim sqlpFAH_REMARKS As New SqlParameter("@FAH_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.FAH_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        If FEE_ADJ_DET.bDelete OrElse FEE_ADJ_DET.bEdit Then
            Dim sqlpFAH_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = FEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = FEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NewFAH_ID As New SqlParameter("@NEW_FAH_ID", SqlDbType.Int)
        NewFAH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFAH_ID)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue = 0 Then
            vNewFAH_ID = NewFAH_ID.Value
            iReturnvalue = SaveFeeSubDetailsInternal(FEE_ADJ_DET, NewFAH_ID.Value, conn, trans)
        End If
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        Return 0
    End Function

    Private Shared Function SaveFeeSubDetailsInternal(ByVal vFEE_ADJ As FEEADJUSTMENTTransport, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        For Each vFEE_ADJ_DET As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
            If vFEE_ADJ_DET Is Nothing Then
                Continue For
            End If
            'vFEE_ADJ_DET.bEdit = vFEE_ADJ_DET.bEdit
            vFEE_ADJ_DET.bDelete = vFEE_ADJ_DET.bDelete
            If iReturnvalue = 0 Then iReturnvalue = SaveFeeAdjustmentDetailsInternal(vFEE_ADJ_DET, FAH_ID, conn, trans)
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEESCHEDULE(vFEE_ADJ_DET, FAH_ID, vFEE_ADJ.FAH_STU_ID, _
            vFEE_ADJ.FAH_STU_TYPE, vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_BSU_ID, vFEE_ADJ.FAH_GRD_ID, vFEE_ADJ.FAH_DATE, vFEE_ADJ.FAH_STU_BSU_ID, vFEE_ADJ.vFAH_SBL_ID, conn, trans, True)
            'Fee CR Entry
            Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(vFEE_ADJ.FAH_STU_BSU_ID_CR)

            Dim vACD_ID As String = String.Empty
            For Each rowACD As DataRow In dtACD.Rows
                If rowACD("ACY_DESCR") = vFEE_ADJ.vFAH_ACD_DESCR Then
                    vACD_ID = rowACD("ACD_ID")
                End If
            Next
            Dim vGRD_ID As String = GetGradeDetails(vFEE_ADJ.FAH_STU_ID_CR, False, STUDENTTYPE.STUDENT)
            vFEE_ADJ_DET.FAD_AMOUNT *= -1
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEESCHEDULE(vFEE_ADJ_DET, FAH_ID, vFEE_ADJ.FAH_STU_ID_CR, _
            vFEE_ADJ.FAH_STU_TYPE_CR, vACD_ID, vFEE_ADJ.FAH_BSU_ID, vGRD_ID, vFEE_ADJ.FAH_DATE, vFEE_ADJ.FAH_STU_BSU_ID_CR, vFEE_ADJ.vFAH_SBL_ID, conn, trans, True)
            If iReturnvalue <> 0 Then
                Return iReturnvalue
            End If
        Next
        Return iReturnvalue
    End Function


    Private Shared Function SaveFeeAdjustmentDetailsInternal(ByVal vFEE_ADJ_DET As FEEADJUSTMENT_S, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEEADJUSTMENT_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAD_FAH_ID As New SqlParameter("@FAD_FAH_ID", SqlDbType.Int)
        sqlpFAD_FAH_ID.Value = FAH_ID 'STU_SUB_DET.FPD_FPH_ID
        cmd.Parameters.Add(sqlpFAD_FAH_ID)

        Dim sqlpFAD_FEE_ID As New SqlParameter("@FAD_FEE_ID", SqlDbType.Int)
        sqlpFAD_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFAD_FEE_ID)

        Dim sqlpFAD_TO_FEE_ID As New SqlParameter("@FAD_TO_FEE_ID", SqlDbType.Int)
        sqlpFAD_TO_FEE_ID.Value = vFEE_ADJ_DET.FRS_TO_FEE_ID
        cmd.Parameters.Add(sqlpFAD_TO_FEE_ID)

        Dim sqlpFAD_AMOUNT As New SqlParameter("@FAD_AMOUNT", SqlDbType.Decimal)
        sqlpFAD_AMOUNT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFAD_AMOUNT)

        Dim sqlpFAD_REMARKS As New SqlParameter("@FAD_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAD_REMARKS.Value = "Internal FEE adjustment"
        cmd.Parameters.Add(sqlpFAD_REMARKS)

        If vFEE_ADJ_DET.bDelete Then
            Dim sqlpFAD_ID As New SqlParameter("@FAD_ID", SqlDbType.Int)
            sqlpFAD_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFAD_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = False
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function


    Public Shared Function F_SAVEFEEADJUSTMENT_H(ByVal FEE_ADJ_DET As FEEADJUSTMENTTransport, ByRef vNEWFAH_ID As String, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer

        If FEE_ADJ_DET Is Nothing Then
            Return 0
        End If
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand
        Dim vNewFAH_REFNO As String = ""


        cmd = New SqlCommand("FEES.[F_SAVEFEEADJUSTMENT_H]", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_BSU_ID As New SqlParameter("@FAH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_BSU_ID.Value = FEE_ADJ_DET.FAH_BSU_ID
        cmd.Parameters.Add(sqlpFAH_BSU_ID)

        Dim sqlpFAH_STU_ID As New SqlParameter("@FAH_STU_ID", SqlDbType.BigInt)
        sqlpFAH_STU_ID.Value = FEE_ADJ_DET.FAH_STU_ID
        cmd.Parameters.Add(sqlpFAH_STU_ID)

        Dim sqlpFAH_STU_TYPE As New SqlParameter("@FAH_STU_TYPE", SqlDbType.VarChar, 2)
        sqlpFAH_STU_TYPE.Value = FEE_ADJ_DET.FAH_STU_TYPE
        cmd.Parameters.Add(sqlpFAH_STU_TYPE)

        Dim sqlpFAH_DATE As New SqlParameter("@FAH_DATE", SqlDbType.DateTime)
        sqlpFAH_DATE.Value = FEE_ADJ_DET.FAH_DATE
        cmd.Parameters.Add(sqlpFAH_DATE)

        Dim sqlpFAH_ACD_ID As New SqlParameter("@FAH_ACD_ID", SqlDbType.Int)
        sqlpFAH_ACD_ID.Value = FEE_ADJ_DET.FAH_ACD_ID
        cmd.Parameters.Add(sqlpFAH_ACD_ID)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim sqlpFAH_SSV_ID As New SqlParameter("@SSV_ID", SqlDbType.VarChar, 20)
        sqlpFAH_SSV_ID.Value = FEE_ADJ_DET.FAH_SSV_ID
        cmd.Parameters.Add(sqlpFAH_SSV_ID)

        Dim sqlpFAH_ONEWAY As New SqlParameter("@ONEWAY", SqlDbType.Bit)
        sqlpFAH_ONEWAY.Value = FEE_ADJ_DET.FAHONEWAY
        cmd.Parameters.Add(sqlpFAH_ONEWAY)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        Dim sqlpFAH_ADJTYPE As New SqlParameter("@FAH_ADJTYPE", SqlDbType.VarChar, 50)
        If FEE_ADJ_DET.ADJUSTMENTTYPE = 1 Then
            sqlpFAH_ADJTYPE.Value = "POSITIVE"
        ElseIf FEE_ADJ_DET.ADJUSTMENTTYPE = -1 Then
            sqlpFAH_ADJTYPE.Value = "NEGATIVE"
        End If
        cmd.Parameters.Add(sqlpFAH_ADJTYPE)

        Dim sqlpFAH_REMARKS As New SqlParameter("@FAH_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAH_REMARKS.Value = FEE_ADJ_DET.FAH_REMARKS
        cmd.Parameters.Add(sqlpFAH_REMARKS)

        If FEE_ADJ_DET.bDelete OrElse FEE_ADJ_DET.bEdit Then
            Dim sqlpFAH_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
            sqlpFAH_ID.Value = FEE_ADJ_DET.FAH_ID
            cmd.Parameters.Add(sqlpFAH_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = FEE_ADJ_DET.bEdit
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = FEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim sqlpFAH_bInter As New SqlParameter("@FAH_bInter", SqlDbType.Bit)
        sqlpFAH_bInter.Value = 0
        cmd.Parameters.Add(sqlpFAH_bInter)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        Dim NewFAH_ID As New SqlParameter("@NEW_FAH_ID", SqlDbType.Int)
        NewFAH_ID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(NewFAH_ID)

        Dim sqlpFAH_STU_BSU_ID As New SqlParameter("@FAH_STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFAH_STU_BSU_ID.Value = FEE_ADJ_DET.FAH_STU_BSU_ID
        cmd.Parameters.Add(sqlpFAH_STU_BSU_ID)

        Dim NewFAH_REFNO As New SqlParameter("@NEW_FAH_REFNO", SqlDbType.VarChar, 20)
        NewFAH_REFNO.Direction = ParameterDirection.InputOutput
        cmd.Parameters.Add(NewFAH_REFNO)


        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue = 0 Then
            vNEWFAH_ID = NewFAH_ID.Value
            vNewFAH_REFNO = NewFAH_REFNO.Value
            iReturnvalue = SaveFeeSubDetails(FEE_ADJ_DET, NewFAH_ID.Value, conn, trans, vNewFAH_REFNO)
        End If
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
        Return 0
    End Function

    Private Shared Function SaveFeeSubDetails(ByVal vFEE_ADJ As FEEADJUSTMENTTransport, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal NewFAH_REFNO As String = "") As Integer
        Dim iReturnvalue As Integer
        For Each vFEE_ADJ_DET As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
            If vFEE_ADJ_DET Is Nothing Then
                Continue For
            End If
            If iReturnvalue = 0 Then iReturnvalue = SaveFeeAdjustmentDetails(vFEE_ADJ_DET, FAH_ID, conn, trans)
            If iReturnvalue = 0 Then iReturnvalue = F_SAVEFEESCHEDULE(vFEE_ADJ_DET, FAH_ID, vFEE_ADJ.FAH_STU_ID, vFEE_ADJ.FAH_STU_TYPE, _
            vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_BSU_ID, vFEE_ADJ.FAH_GRD_ID, vFEE_ADJ.FAH_DATE, vFEE_ADJ.FAH_STU_BSU_ID, vFEE_ADJ.vFAH_SBL_ID, conn, trans, False, NewFAH_REFNO)
            If iReturnvalue <> 0 Then
                Return 0 'return value is newid
            End If
        Next
        Return iReturnvalue
    End Function

    Private Shared Function F_SAVEFEESCHEDULE(ByVal vFEE_ADJ_DET As FEEADJUSTMENT_S, ByVal NEW_FAH_ID As Integer, _
    ByVal STU_ID As Integer, ByVal STU_TYPE As String, ByVal ACD_ID As Integer, ByVal BSU_ID As String, _
    ByVal GRD_ID As String, ByVal DT As Date, ByVal FSH_STU_BSU_ID As String, ByVal FSH_SBL_ID As Integer, _
    ByVal conn As SqlConnection, ByVal trans As SqlTransaction, Optional ByVal bheadtoHead As Boolean = False, Optional ByVal FSH_DOCNO As String = "") As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        If vFEE_ADJ_DET.bDelete Then

            cmd = New SqlCommand("FEES.F_DELETETransportFEESCHEDULE", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpDEL_FSH_BSU_ID As New SqlParameter("@FSH_BSU_ID", SqlDbType.VarChar, 20)
            sqlpDEL_FSH_BSU_ID.Value = BSU_ID
            cmd.Parameters.Add(sqlpDEL_FSH_BSU_ID)

            Dim sqlpDEL_FSH_STU_ID As New SqlParameter("@FSH_STU_ID", SqlDbType.VarChar, 20)
            sqlpDEL_FSH_STU_ID.Value = STU_ID
            cmd.Parameters.Add(sqlpDEL_FSH_STU_ID)

            Dim sqlpDEL_FSH_REF_ID As New SqlParameter("@FSH_REF_ID", SqlDbType.Int)
            sqlpDEL_FSH_REF_ID.Value = NEW_FAH_ID
            cmd.Parameters.Add(sqlpDEL_FSH_REF_ID)

            Dim sqlpDEL_FSH_STU_TYPE As New SqlParameter("@FSH_STU_TYPE", SqlDbType.VarChar, 4)
            sqlpDEL_FSH_STU_TYPE.Value = STU_TYPE
            cmd.Parameters.Add(sqlpDEL_FSH_STU_TYPE)

            Dim sqlpDEL_FSH_SOURCE As New SqlParameter("@FSH_SOURCE", SqlDbType.VarChar, 50)
            sqlpDEL_FSH_SOURCE.Value = "FEE Adjustment"
            cmd.Parameters.Add(sqlpDEL_FSH_SOURCE)

            Dim retDELSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retDELSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retDELSValParam)

            cmd.ExecuteNonQuery()
            iReturnvalue = retDELSValParam.Value
            If iReturnvalue <> 0 Then Return iReturnvalue
        End If

        cmd = New SqlCommand("FEES.F_SAVETransportFEESCHEDULE", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFSH_ID As New SqlParameter("@FSH_ID", SqlDbType.Int)
        sqlpFSH_ID.Value = DBNull.Value
        cmd.Parameters.Add(sqlpFSH_ID)

        Dim sqlpFSH_FSP_ID As New SqlParameter("@FSH_FSP_ID", SqlDbType.Int)
        sqlpFSH_FSP_ID.Value = 0
        cmd.Parameters.Add(sqlpFSH_FSP_ID)

        Dim sqlpFSH_BSU_ID As New SqlParameter("@FSH_BSU_ID", SqlDbType.VarChar, 20)
        sqlpFSH_BSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpFSH_BSU_ID)

        Dim sqlpFSH_ACD_ID As New SqlParameter("@FSH_ACD_ID", SqlDbType.Int)
        sqlpFSH_ACD_ID.Value = ACD_ID
        cmd.Parameters.Add(sqlpFSH_ACD_ID)

        Dim sqlpFSH_GRD_ID As New SqlParameter("@FSH_GRD_ID", SqlDbType.VarChar, 10)
        sqlpFSH_GRD_ID.Value = GRD_ID
        cmd.Parameters.Add(sqlpFSH_GRD_ID)

        Dim sqlpFSH_STU_TYPE As New SqlParameter("@FSH_STU_TYPE", SqlDbType.VarChar, 4)
        sqlpFSH_STU_TYPE.Value = STU_TYPE
        cmd.Parameters.Add(sqlpFSH_STU_TYPE)

        Dim sqlpFSH_STU_ID As New SqlParameter("@FSH_STU_ID", SqlDbType.VarChar, 20)
        sqlpFSH_STU_ID.Value = STU_ID
        cmd.Parameters.Add(sqlpFSH_STU_ID)

        Dim sqlpFSH_FEE_ID As New SqlParameter("@FSH_FEE_ID", SqlDbType.Int)
        sqlpFSH_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFSH_FEE_ID)

        Dim sqlpFSH_DATE As New SqlParameter("@FSH_DATE", SqlDbType.DateTime)
        sqlpFSH_DATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_DATE)

        Dim sqlpFSH_AMOUNT As New SqlParameter("@FSH_AMOUNT", SqlDbType.Decimal)
        sqlpFSH_AMOUNT.Value = IIf(vFEE_ADJ_DET.FAD_AMOUNT > 0, 1, -1) * vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFSH_AMOUNT)

        Dim sqlpFSH_SETTLEAMT As New SqlParameter("@FSH_SETTLEAMT", SqlDbType.Decimal)
        sqlpFSH_SETTLEAMT.Value = 0
        'If vFEE_ADJ_DET.FAD_AMOUNT < 0 Then
        '    sqlpFSH_SETTLEAMT.Value = 0
        'Else
        '    sqlpFSH_SETTLEAMT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        'End If
        cmd.Parameters.Add(sqlpFSH_SETTLEAMT)

        Dim sqlpFSH_SOURCE As New SqlParameter("@FSH_SOURCE", SqlDbType.VarChar, 50)
        sqlpFSH_SOURCE.Value = "FEE Adjustment"
        cmd.Parameters.Add(sqlpFSH_SOURCE)

        Dim sqlpFSH_REF_ID As New SqlParameter("@FSH_REF_ID", SqlDbType.Int)
        sqlpFSH_REF_ID.Value = NEW_FAH_ID
        cmd.Parameters.Add(sqlpFSH_REF_ID)

        Dim sqlpFSH_DRCR As New SqlParameter("@FSH_DRCR", SqlDbType.VarChar, 2)
        If vFEE_ADJ_DET.FAD_AMOUNT < 0 Then
            sqlpFSH_DRCR.Value = "CR"
        Else
            sqlpFSH_DRCR.Value = "DR"
        End If
        cmd.Parameters.Add(sqlpFSH_DRCR)

        Dim sqlpFSH_ROUNDOFF As New SqlParameter("@FSH_ROUNDOFF", SqlDbType.Decimal)
        sqlpFSH_ROUNDOFF.Value = 0
        cmd.Parameters.Add(sqlpFSH_ROUNDOFF)

        Dim sqlpFSH_NARRATION As New SqlParameter("@FSH_NARRATION", SqlDbType.VarChar, 500)
        If Not bheadtoHead Then
            sqlpFSH_NARRATION.Value = vFEE_ADJ_DET.FAD_REMARKS
        Else
            sqlpFSH_NARRATION.Value = "FEE Head to Head Transfer "
        End If
        cmd.Parameters.Add(sqlpFSH_NARRATION)

        Dim sqlpFSH_STU_BSU_ID As New SqlParameter("@FSH_STUDENT_BSU_ID", SqlDbType.Int)
        sqlpFSH_STU_BSU_ID.Value = FSH_STU_BSU_ID
        cmd.Parameters.Add(sqlpFSH_STU_BSU_ID)

        Dim sqlpFSH_FDUEDATE As New SqlParameter("@FSH_FDUEDATE", SqlDbType.DateTime)
        sqlpFSH_FDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_FDUEDATE)

        Dim sqlpFSH_SDUEDATE As New SqlParameter("@FSH_SDUEDATE", SqlDbType.DateTime)
        sqlpFSH_SDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_SDUEDATE)

        Dim sqlpFSH_TDUEDATE As New SqlParameter("@FSH_TDUEDATE", SqlDbType.DateTime)
        sqlpFSH_TDUEDATE.Value = DT
        cmd.Parameters.Add(sqlpFSH_TDUEDATE)

        Dim sqlpFSH_SBL_ID As New SqlParameter("@FSH_SBL_ID", SqlDbType.Decimal)
        sqlpFSH_SBL_ID.Value = FSH_SBL_ID
        cmd.Parameters.Add(sqlpFSH_SBL_ID)

        Dim sqlpFSH_DOCNO As New SqlParameter("@FSH_DOCNO", SqlDbType.VarChar)
        sqlpFSH_DOCNO.Value = FSH_DOCNO
        cmd.Parameters.Add(sqlpFSH_DOCNO)


        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        Return 0
        'If iReturnvalue <> 0 Then
        '    Return iReturnvalue
        'End If
    End Function

    Private Shared Function GetSCTID(ByVal vBSU_ID As String, ByVal vACD_ID As Integer, ByVal vGRD_ID As String, ByVal vSTU_ID As Integer) As Integer
        Dim str_sql As String
        str_sql = "SELECT STU_SCT_ID FROM STUDENT_M WHERE STU_ID = " & vSTU_ID
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
    End Function

    Private Shared Function SaveFeeAdjustmentDetails(ByVal vFEE_ADJ_DET As FEEADJUSTMENT_S, ByVal FAH_ID As Integer, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("FEES.F_SAVEFEEADJUSTMENT_S", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAD_FAH_ID As New SqlParameter("@FAD_FAH_ID", SqlDbType.Int)
        sqlpFAD_FAH_ID.Value = FAH_ID 'STU_SUB_DET.FPD_FPH_ID
        cmd.Parameters.Add(sqlpFAD_FAH_ID)

        Dim sqlpFAD_FEE_ID As New SqlParameter("@FAD_FEE_ID", SqlDbType.Int)
        sqlpFAD_FEE_ID.Value = vFEE_ADJ_DET.FAD_FEE_ID
        cmd.Parameters.Add(sqlpFAD_FEE_ID)

        Dim sqlpFAD_TO_FEE_ID As New SqlParameter("@FAD_TO_FEE_ID", SqlDbType.Int)
        sqlpFAD_TO_FEE_ID.Value = DBNull.Value
        cmd.Parameters.Add(sqlpFAD_TO_FEE_ID)

        Dim sqlpFAD_AMOUNT As New SqlParameter("@FAD_AMOUNT", SqlDbType.Decimal)
        sqlpFAD_AMOUNT.Value = vFEE_ADJ_DET.FAD_AMOUNT
        cmd.Parameters.Add(sqlpFAD_AMOUNT)

        Dim sqlpFAD_REMARKS As New SqlParameter("@FAD_REMARKS", SqlDbType.VarChar, 200)
        sqlpFAD_REMARKS.Value = vFEE_ADJ_DET.FAD_REMARKS
        cmd.Parameters.Add(sqlpFAD_REMARKS)

        If vFEE_ADJ_DET.bDelete Then
            Dim sqlpFAD_ID As New SqlParameter("@FAD_ID", SqlDbType.Int)
            sqlpFAD_ID.Value = vFEE_ADJ_DET.FAD_ID
            cmd.Parameters.Add(sqlpFAD_ID)
        End If

        Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
        sqlpbEdit.Value = False
        cmd.Parameters.Add(sqlpbEdit)

        Dim sqlpbDelete As New SqlParameter("@bDelete", SqlDbType.Bit)
        sqlpbDelete.Value = vFEE_ADJ_DET.bDelete
        cmd.Parameters.Add(sqlpbDelete)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        If iReturnvalue <> 0 Then
            Return iReturnvalue
        End If
    End Function

    Public Shared Function PopulateFeeMaster(ByVal vBSUID As String, ByVal vACD_ID As Integer) As DataTable
        Return FeeCommon.GetFEES_M_TRANSPORT()
    End Function

    Public Shared Function PrintAdjustmentVoucher(ByVal vFAR_ID As Integer, ByVal StuBSU_ID As String, _
    ByVal ProvBSU_ID As String, ByVal USER As String) As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETTRANSPORTFEEADJUSTMENTFORVOUCHER]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAR_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
        sqlpFAR_ID.Value = vFAR_ID
        cmd.Parameters.Add(sqlpFAR_ID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable

        Dim repSourceSubRep(0) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        ''''
        Dim cmdHeader As New SqlCommand("getBsuInFoWithImage_Provider", New SqlConnection(ConnectionManger.GetOASISConnectionString))
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.AddWithValue("@IMG_STU_BSU_ID", StuBSU_ID)
        cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
        cmdHeader.Parameters.AddWithValue("@IMG_PROVIDER_BSU_ID", ProvBSU_ID)
        repSourceSubRep(0).Command = cmdHeader
        repSource.SubReport = repSourceSubRep

        params("RPT_CAPTION") = "TRANSPORT FEE ADJUSTMENT VOUCHER"
        params("UserName") = USER
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.ResourceName = "../../fees/Reports/RPT/rptTransportFEEAdjustmentRequestVoucher.rpt"
        Return repSource
    End Function

End Class
