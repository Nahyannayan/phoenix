﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Linq
Imports System.Threading
Imports System.Threading.Tasks

Public Class FeeBulkAdjustment

    ''' <summary>
    ''' Gets the percent complete for the long-running process.
    ''' </summary>
    Private privatePercentComplete As Double
    Public Property PercentComplete() As Double
        Get
            Return privatePercentComplete
        End Get
        Protected Set(ByVal value As Double)
            privatePercentComplete = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is still executing.
    ''' </summary>
    Private privateIsRunning As Boolean
    Public Property IsRunning() As Boolean
        Get
            Return privateIsRunning
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsRunning = value
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the long-running process is done running.
    ''' </summary>
    Private privateIsComplete As Boolean
    Public Property IsComplete() As Boolean
        Get
            Return privateIsComplete
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsComplete = value
        End Set
    End Property
    Private privateIsSuccess As Boolean
    Public Property IsSuccess() As Boolean
        Get
            Return privateIsSuccess
        End Get
        Protected Set(ByVal value As Boolean)
            privateIsSuccess = value
        End Set
    End Property
    Private privateMessage As String
    Public Property Message() As String
        Get
            Return privateMessage
        End Get
        Protected Set(ByVal value As String)
            privateMessage = value
        End Set
    End Property
    Private privateBATCHNO As Long
    Public Property BATCHNO() As Long
        Get
            Return privateBATCHNO
        End Get
        Protected Set(ByVal value As Long)
            privateBATCHNO = value
        End Set
    End Property

    Private privateOBJSS As UpdateProgressService
    Public Property OBJSS() As UpdateProgressService
        Get
            Return privateOBJSS
        End Get
        Protected Set(ByVal value As UpdateProgressService)
            privateOBJSS = value
        End Set
    End Property
    ''' <summary>
    ''' Gets a brief description of what is currently being worked on, to update the end-user.
    ''' </summary>
    Public Property WorkDescription() As String

    ''' <summary>
    ''' Event for when progress has changed.
    ''' </summary>
    Public Event ProgressChanged As EventHandler(Of ProgressChangedEventArgs)

    ''' <summary>
    ''' Event for the the long-running process is complete.
    ''' </summary>
    Public Event ProcessComplete As EventHandler
    ''' <summary>
    ''' Private handle to the task that is currently execute the long-running process.
    ''' </summary>
    Private task As Task, task2 As Task

    Public Sub UpdatePercent(ByVal newPercentComplete As Double, ByVal workDescription As String)
        ' Update the properties for any callers casually checking the status.
        Me.PercentComplete = newPercentComplete
        Me.WorkDescription = workDescription
        If newPercentComplete = 100 Then
            IsComplete = True
        End If
        ' Fire the event for any callers who actively want to be notifed.
        RaiseEvent ProgressChanged(Me, New ProgressChangedEventArgs(newPercentComplete, IsComplete, workDescription))
    End Sub
    Public Sub StartProcess()
        IsComplete = False
        IsRunning = True
        IsSuccess = True
        PercentComplete = 0
        'Me.BATCHNO = BATCHNO
        'If Me.OBJSS Is Nothing Then Me.OBJSS = objups
        UpdatePercent(0, "Please wait..")
    End Sub
    Public Sub EndProcess(ByVal status As Boolean, ByVal msg As String)
        UpdatePercent(100, msg)
        Message = msg
        IsSuccess = status
        Thread.Sleep(1500)
        RaiseEvent ProcessComplete(Me, New EventArgs())
        IsRunning = False
        Errorlog("EndProcess - " & Message, "OASIS")
    End Sub

    Public Function IMPORT_ADJUSTMENT(ByVal BATCH_NO As Integer, ByVal ADJ_FOR As String) As Boolean
        IMPORT_ADJUSTMENT = False
        ' Kick off the actual, private long-running process in a new Task
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(IIf(ADJ_FOR = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString))
        'Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            'STATUS = 1000
            Dim objUPS As New UpdateProgressService
            task = System.Threading.Tasks.Task.Factory.StartNew(Sub()

                                                                    Dim flag As Boolean = True
                                                                    Try
                                                                        objUPS.StartProcess()
                                                                        'StartProcess()
                                                                        Dim i As Integer = 0
                                                                        Dim sqlParam(2) As SqlParameter
                                                                        objConn.Open()
                                                                        transaction = objConn.BeginTransaction("SampleTransaction")
                                                                        Dim cmd As New SqlCommand
                                                                        cmd.Dispose()
                                                                        cmd = New SqlCommand("IMPORT_FEE_ADJUSTMENTS_NEW", objConn, transaction)
                                                                        cmd.CommandType = CommandType.StoredProcedure
                                                                        cmd.CommandTimeout = 0
                                                                        Dim retmsg As String = ""
                                                                        sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retmsg, SqlDbType.VarChar, True, 200)
                                                                        cmd.Parameters.Add(sqlParam(0))
                                                                        sqlParam(1) = Mainclass.CreateSqlParameter("@FAI_BATCHNO", BATCH_NO, SqlDbType.VarChar)
                                                                        cmd.Parameters.Add(sqlParam(1))

                                                                        cmd.ExecuteNonQuery()
                                                                        retmsg = sqlParam(0).Value

                                                                        If retmsg = "" Then
                                                                            transaction.Commit()
                                                                            'transaction.Rollback()
                                                                            objUPS.EndProcess(True, "Adjustment imported successfully")
                                                                            'EndProcess(True, "Adjustment imported successfully")
                                                                            IMPORT_ADJUSTMENT = True
                                                                        Else
                                                                            transaction.Rollback()
                                                                            objUPS.EndProcess(False, retmsg)
                                                                            'EndProcess(False, retmsg)
                                                                            Errorlog("Rollback - " & retmsg, "OASIS")
                                                                            Exit Sub
                                                                        End If
                                                                    Catch ex As Exception
                                                                        objUPS.EndProcess(False, "Adjustment import Failed")
                                                                        'EndProcess(False, "Adjustment import Failed")
                                                                        Errorlog("IMPORT_ADJUSTMENT - Exception: " & ex.Message, "OASIS")
                                                                    Finally
                                                                        If objConn.State = ConnectionState.Open Then
                                                                            objConn.Close()
                                                                        End If
                                                                    End Try
                                                                End Sub)
            'If task.IsCompleted Then
            '    Dim a = task.Status
            '    Errorlog("IMPORT_ADJUSTMENT - taskStatus : " & task.Status, "OASIS")
            'Else
            '    Errorlog("IMPORT_ADJUSTMENT - taskStatus : " & task.Status, "OASIS")
            'End If
            If Not task.IsCompleted OrElse IsRunning Then
                task2 = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                         Dim Totalpending As Integer = GET_PENDING_COUNT(BATCH_NO, ADJ_FOR)
                                                                         Dim Currentpending As Integer = Totalpending
                                                                         Dim Completed As Integer = 1
                                                                         objUPS.StartProcess()
                                                                         'StartProcess()
                                                                         If Totalpending > 0 Then
                                                                             Do While Currentpending > 0
                                                                                 Dim currentPercentage As Integer = (Completed / Totalpending) * 100
                                                                                 If Not task.IsCompleted OrElse IsRunning Then
                                                                                     If Completed > 0 And currentPercentage < 100 Then
                                                                                         objUPS.UpdatePercentage(currentPercentage, "processed")
                                                                                     End If
                                                                                     'UpdatePercent(currentPercentage, "processed")
                                                                                 Else 'if task already completed
                                                                                     'Message = IIf(Message.ToString.Length <= 0, "", Message)
                                                                                     objUPS.UpdatePercentage(100, Message)
                                                                                     'UpdatePercent(100, Message)
                                                                                     Exit Do
                                                                                 End If
                                                                                 Currentpending = GET_PENDING_COUNT(BATCH_NO, ADJ_FOR)
                                                                                 Completed = Totalpending - Currentpending
                                                                             Loop
                                                                         Else
                                                                             'objss.UpdatePercentage(100, "processed")
                                                                         End If
                                                                     End Sub)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub GET_CURRENT_STATUS(ByVal ADJ_FOR As String)
        Dim Totalpending As Integer = GET_PENDING_COUNT(BATCHNO, ADJ_FOR)
        Dim Currentpending As Integer = Totalpending
        Dim Completed As Integer = 1
        'objss.StartProcess()
        'StartProcess()
        If Totalpending > 0 Then
            Do While Currentpending > 0
                Dim currentPercentage As Integer = (Completed / Totalpending) * 100
                If IsRunning Then
                    If Completed > 0 And currentPercentage < 100 Then
                        OBJSS.UpdatePercentage(currentPercentage, "processed")
                    End If
                    'UpdatePercent(currentPercentage, "processed")
                Else 'if task already completed
                    'Message = IIf(Message.ToString.Length <= 0, "", Message)
                    OBJSS.UpdatePercentage(100, Message)
                    'UpdatePercent(100, Message)
                    Exit Do
                End If
                Currentpending = GET_PENDING_COUNT(BATCHNO, ADJ_FOR)
                Completed = Totalpending - Currentpending
            Loop
        Else
            'objss.UpdatePercentage(100, "processed")
        End If
    End Sub
    Private Function GET_PENDING_COUNT(ByVal BatchNo As Integer, ByVal ADJ_FOR As String) As Integer
        GET_PENDING_COUNT = 0
        Try
            Dim qry As String = "SELECT COUNT(FAI_ID) as PENDING FROM DataImport.FEE_ADJUSTMENT_IMPORT WITH (NOLOCK) WHERE ISNULL(FAI_DOCNO, '') = '' AND ISNULL(FAI_PROCESSED, 0) = 0 AND FAI_BATCHNO = " & BatchNo
            GET_PENDING_COUNT = Convert.ToInt32(SqlHelper.ExecuteScalar(IIf(ADJ_FOR = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString), CommandType.Text, qry))
        Catch ex As Exception
            GET_PENDING_COUNT = 0
        End Try
    End Function

    Public Shared Sub Errorlog(ByVal p_Description As String, Optional ByVal p_source As String = "Not Set")
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ERR_PAGE", SqlDbType.VarChar, 200)
                pParms(0).Value = "FeeBulkAdjustment"
                pParms(1) = New SqlClient.SqlParameter("@ERR_SOURCE", SqlDbType.VarChar, 50)
                pParms(1).Value = p_source
                pParms(2) = New SqlClient.SqlParameter("@ERR_DESCRIPTION", SqlDbType.VarChar, 300)
                pParms(2).Value = p_Description
                SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "SAVEERROR_RUNTIME", pParms)
            End Using
        Catch ex As Exception
            Try
                Dim f As New IO.DirectoryInfo(HttpContext.Current.Server.MapPath("Logs"))
                If f.Exists = False Then
                    f.Create()
                End If
                Dim objStreamWriter As StreamWriter
                objStreamWriter = File.AppendText(HttpContext.Current.Server.MapPath("Logs\Logs.txt"))
                objStreamWriter.WriteLine("Page : ( " & HttpContext.Current.Request.Url.ToString() & " ) Source : ( " & p_source & " ) Description : ( " & p_Description & " ) Time : (" & DateTime.Now.ToString() & ")")
                objStreamWriter.Close()
            Catch ex1 As Exception
            End Try
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
