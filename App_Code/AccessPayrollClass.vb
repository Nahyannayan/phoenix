Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class AccessPayrollClass
#Region "All Get Function"
    'Public Shared Function GetMaxBankLoan_D() As Integer
    '    Try


    '        Dim sqlString As String = "select max(EBL_ID) from EMPBANKLOAN_D"
    '        Dim result As Object
    '        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

    '            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
    '            command.CommandType = Data.CommandType.Text
    '            result = command.ExecuteScalar
    '        End Using
    '        Return CInt(result)
    '    Catch ex As Exception
    '        Return -1
    '    End Try
    'End Function

    Public Shared Sub SetPayMonth_YearDetails(ByVal sBSUID As String, ByRef payYear As Integer, ByRef payMonth As Integer)
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim str_Sql As String = "SELECT BSU_PAYMONTH, BSU_PAYYEAR, BSU_CURRENCY FROM BUSINESSUNIT_M WHERE BSU_ID = '" & sBSUID & "'"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.Text, str_Sql)
        While (dr.Read())
            payMonth = dr("BSU_PAYMONTH")
            payYear = dr("BSU_PAYYEAR")
        End While
    End Sub

    Public Shared Function GetLeaveAdj_ID(ByVal EAH_ID As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --20/Dec/2007
        'Purpose--Get Leave Adjustment date based on the EAH_ID (empLeaveAdj.aspx)

        Dim sqlStringID As String = "select EMP_ID,EName,DTFROM,DTTO,PAYYEAR,REMARKS,PAYMONTH,ELT_ID,AdjAdd from(SELECT EMPLVLADJUSTMENT_H.EAH_ID as EAH_ID,EMPLVLADJUSTMENT_H.EAH_EMP_ID as EMP_ID , isnull(EMPLOYEE_M.EMP_FNAME,'')+'  '+isnull(EMPLOYEE_M.EMP_MNAME,'')+'  '+isnull(EMPLOYEE_M.EMP_LNAME,'') as EName, EMPLVLADJUSTMENT_H.EAH_DTFROM as DTFROM, EMPLVLADJUSTMENT_H.EAH_DTTO as DTTO , " & _
                    "  EMPLVLADJUSTMENT_H.EAH_PAYYEAR as PAYYEAR, EMPLVLADJUSTMENT_H.EAH_PAYMONTH as PAYMONTH, EMPLVLADJUSTMENT_H.EAH_REMARKS as REMARKS,EMPLVLADJUSTMENT_H.EAH_ELT_ID as ELT_ID,EMPLVLADJUSTMENT_H.EAH_bADJADD as AdjAdd   FROM  EMPLEAVETYPE_M INNER JOIN " & _
                     " EMPLVLADJUSTMENT_H ON EMPLEAVETYPE_M.ELT_ID = EMPLVLADJUSTMENT_H.EAH_ELT_ID INNER JOIN  EMPLOYEE_M ON EMPLVLADJUSTMENT_H.EAH_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN " & _
                     " BUSINESSUNIT_M ON EMPLVLADJUSTMENT_H.EAH_BSU_ID = BUSINESSUNIT_M.BSU_ID)a where a.EAH_ID='" & EAH_ID & "'"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlStringID, connection)
        command.CommandType = CommandType.Text
        Dim readerLeaveAdj_ID As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return readerLeaveAdj_ID

    End Function
    Public Shared Function GetEmpLoan_ID(ByVal EBL_ID As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --06/Jan/2008
        'Purpose--Get EmpLoan data based on the  EBL_ID (empBankLoan.aspx)

        Dim sqlStringID As String = "select  EBL_ACCNUM,Emp_ID,EName,BNK_ID,Bname,Branch,DTFROM,DTTO,REMARKS,Amount,bActive,Sal_tran from(SELECT   EMPBANKLOAN_D.EBL_ACCNUM, EMPBANKLOAN_D.EBL_ID as EBL_ID, EMPBANKLOAN_D.EBL_EMP_ID as Emp_ID, ISNULL(EMPLOYEE_M.EMP_FNAME, '') + '  ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '')+ '  ' + ISNULL(EMPLOYEE_M.EMP_LNAME, '') AS EName," & _
" EMPBANKLOAN_D.EBL_BNK_ID as BNK_ID,BANK_M.BNK_DESCRIPTION as Bname , EMPBANKLOAN_D.EBL_BRANCH as Branch, EMPBANKLOAN_D.EBL_REMARKS as Remarks,EMPBANKLOAN_D.EBL_FROMDT as DTFROM," & _
 " EMPBANKLOAN_D.EBL_TODT as DTTO, EMPBANKLOAN_D.EBL_AMOUNT as Amount, EMPBANKLOAN_D.EBL_bActive as bActive,EMPBANKLOAN_D.EBL_bSALARYTRANSFER as Sal_Tran " & _
" FROM  BANK_M INNER JOIN EMPBANKLOAN_D ON BANK_M.BNK_ID = EMPBANKLOAN_D.EBL_BNK_ID INNER JOIN EMPLOYEE_M ON EMPBANKLOAN_D.EBL_EMP_ID = EMPLOYEE_M.EMP_ID)a where a.EBL_ID='" & EBL_ID & "'"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlStringID, connection)
        command.CommandType = CommandType.Text
        Dim readerLeaveAdj_ID As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return readerLeaveAdj_ID

    End Function
    Public Shared Function GetEMPEXPENSES_D(ByVal EXD_ID As String) As SqlDataReader

        'Author(--Lijo)
        'Date   --3/Jan/2008
        'Purpose--Get EMP EXPENSES  date based on the EXD_ID (empRecruit_Exp_Trans_Edit.aspx)

        Dim sqlStringID As String = "SELECT  isnull(EMPLOYEE_M.EMP_FNAME,'')+'  '+isnull(EMPLOYEE_M.EMP_MNAME,'')+'  '+isnull(EMPLOYEE_M.EMP_LNAME,'') as EName, EMPEXPENSES_D.EXD_EMP_ID as Emp_ID, " & _
" EMPEXPENSES_D.EXD_RXM_ID as RXM_ID, EMPRECRUTMENTEXP_M.RXM_DESCR as RXM_DESCR, EMPEXPENSES_D.EXD_AMOUNT as EXD_AMOUNT, EMPEXPENSES_D.EXD_DATE as EXD_DATE, EMPEXPENSES_D.EXD_bPOSTED as EXD_bPOSTED, " & _
" EMPEXPENSES_D.EXD_VALIDMONTHS as EXD_VALIDMONTHS, EMPEXPENSES_D.EXD_PAID_EMP_ID as PAID_EMP_ID, EMPEXPENSES_D.EXD_REMARK as EXD_REMARK, isnull(A.EMP_FNAME,'')+'  '+isnull(A.EMP_MNAME,'')+'  '+isnull(A.EMP_LNAME,'') as PAIDBY " & _
" FROM  EMPLOYEE_M INNER JOIN EMPEXPENSES_D ON EMPLOYEE_M.EMP_ID = EMPEXPENSES_D.EXD_EMP_ID INNER JOIN " & _
" EMPRECRUTMENTEXP_M ON EMPEXPENSES_D.EXD_RXM_ID = EMPRECRUTMENTEXP_M.RXM_ID LEFT OUTER JOIN EMPLOYEE_M A ON EXD_PAID_EMP_ID=A.EMP_ID where EMPEXPENSES_D.EXD_ID='" & EXD_ID & "'"


        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlStringID, connection)
        command.CommandType = CommandType.Text
        Dim readerLeaveAdj_ID As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return readerLeaveAdj_ID

    End Function
    Public Shared Function GetMax_Min_Year(ByRef MinYear As Integer) As Integer

        'Author(--Lijo)
        'Date   --12/Dec/2007
        'Purpose--Get starting year and the ending year, which needs to be populated in payYear (empLeaveAdj.aspx)

        Dim sqlStringMin As String = "SELECT min(year(EMP_JOINDT))-2 FROM EMPLOYEE_M"
        Dim SqlStringMax As String = "SELECT max(year(EMP_RESGDT))+2 FROM EMPLOYEE_M"

        Dim totYear, MaxYear As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlStringMin, connection)
            command.CommandType = Data.CommandType.Text
            MinYear = command.ExecuteScalar

            command = New SqlCommand(SqlStringMax, connection)
            command.CommandType = Data.CommandType.Text
            MaxYear = command.ExecuteScalar

        End Using
        totYear = MaxYear - MinYear
        Return totYear
    End Function

    Public Shared Function GetLeaveType() As SqlDataReader
        'Author(--Lijo)
        'Date   --12/Dec/2007
        'Purpose--To populate leave type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlLeaveType As String = ""

        sqlLeaveType = "SELECT  [ELT_ID] ,[ELT_DESCR]  FROM [OASIS].[dbo].[EMPLEAVETYPE_M] order by ELT_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlLeaveType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    'V1.2  To list only leaves in leave type drop down-Ticket 53567 correction
    Public Shared Function GetOnlyLeaveTypes() As SqlDataReader
        'Author(--Lijo)
        'Date   --12/Dec/2007
        'Purpose--To populate leave type
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlLeaveType As String = ""

        sqlLeaveType = "SELECT  [ELT_ID] ,[ELT_DESCR]  FROM [OASIS].[dbo].[EMPLEAVETYPE_M] WHERE ELT_bLEAVE=1 order by ELT_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlLeaveType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
#End Region
#Region " All SAVE(Update/Insert) function"
    Public Shared Function SaveIssue_Asset(ByVal EAD_ID As Integer, ByVal EAD_EMP_ID As String, ByVal EAD_FAS_ID As String, ByVal EAD_ISSUE_DT As String, _
    ByVal EAD_RET_DT As String, ByVal EAD_VALUE As Double, ByVal EAD_REMARKS As String, ByVal EAD_STATUS As String, ByVal EAD_bACTIVE As Boolean, ByVal EAD_bCHKFORLEAVE As Boolean, ByVal bEdit As Boolean, ByVal EAD_RECV_EMP_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(12) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@EAD_ID", EAD_ID)
            pParms(1) = New SqlClient.SqlParameter("@EAD_EMP_ID", EAD_EMP_ID)
            pParms(2) = New SqlClient.SqlParameter("@EAD_FAS_ID", EAD_FAS_ID)
            pParms(3) = New SqlClient.SqlParameter("@EAD_ISSUE_DT", EAD_ISSUE_DT)
            pParms(4) = New SqlClient.SqlParameter("@EAD_RET_DT", EAD_RET_DT)
            pParms(5) = New SqlClient.SqlParameter("@EAD_VALUE", EAD_VALUE)
            pParms(6) = New SqlClient.SqlParameter("@EAD_REMARKS", EAD_REMARKS)
            pParms(7) = New SqlClient.SqlParameter("@EAD_STATUS", EAD_STATUS)
            pParms(8) = New SqlClient.SqlParameter("@EAD_bACTIVE", EAD_bACTIVE)
            pParms(9) = New SqlClient.SqlParameter("@EAD_bCHKFORLEAVE", EAD_bCHKFORLEAVE)
            pParms(10) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(11).Direction = ParameterDirection.ReturnValue
            pParms(12) = New SqlClient.SqlParameter("@EAD_RECV_EMP_ID", EAD_RECV_EMP_ID)
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveIssue_Asset", pParms)
            Dim ReturnFlag As Integer = pParms(11).Value
            Return ReturnFlag
        End Using

    End Function



    Public Shared Function SaveEMPLVLADJUSTMENT(ByVal EAH_ID As String, ByVal EAH_EMP_ID As String, ByVal EAH_BSU_ID As String, ByVal EAH_DTFROM As Date, _
ByVal EAH_DTTO As Date, ByVal EAH_PAYYEAR As String, ByVal EAH_PAYMONTH As String, ByVal EAH_ELT_ID As String, ByVal EAH_REMARKS As String, ByVal EAH_bADJADD As Boolean, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(11) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EAH_ID", EAH_ID)
            pParms(1) = New SqlClient.SqlParameter("@EAH_EMP_ID", EAH_EMP_ID)
            pParms(2) = New SqlClient.SqlParameter("@EAH_BSU_ID", EAH_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@EAH_DTFROM", EAH_DTFROM)
            pParms(4) = New SqlClient.SqlParameter("@EAH_DTTO", EAH_DTTO)
            pParms(5) = New SqlClient.SqlParameter("@EAH_PAYYEAR", EAH_PAYYEAR)
            pParms(6) = New SqlClient.SqlParameter("@EAH_PAYMONTH", EAH_PAYMONTH)
            pParms(7) = New SqlClient.SqlParameter("@EAH_ELT_ID", EAH_ELT_ID)
            pParms(8) = New SqlClient.SqlParameter("@EAH_REMARKS", EAH_REMARKS)
            pParms(9) = New SqlClient.SqlParameter("@EAH_bADJADD", EAH_bADJADD)
            pParms(10) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(11).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SAveEMPLVLADJUSTMENT_H", pParms)
            Dim ReturnFlag As Integer = pParms(11).Value
            Return ReturnFlag
        End Using

    End Function
    Public Shared Function SaveEMPBANKLOAN_D(ByVal EBL_ID As String, ByVal EBL_EMP_ID As String, _
    ByVal EBL_BNK_ID As String, ByVal EBL_BRANCH As String, ByVal EBL_REMARKS As String, _
    ByVal EBL_FROMDT As Date, ByVal EBL_TODT As String, ByVal EBL_AMOUNT As String, ByVal EBL_bActive As Boolean, _
    ByVal EBL_bSALARYTRANSFER As Boolean, ByVal EBL_BSU_ID As String, ByVal EBL_ACCNUM As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(12) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EBL_ID", EBL_ID)
            pParms(1) = New SqlClient.SqlParameter("@EBL_EMP_ID", EBL_EMP_ID)
            pParms(2) = New SqlClient.SqlParameter("@EBL_BNK_ID", EBL_BNK_ID)
            pParms(3) = New SqlClient.SqlParameter("@EBL_BRANCH", EBL_BRANCH)
            pParms(4) = New SqlClient.SqlParameter("@EBL_REMARKS", EBL_REMARKS)
            pParms(5) = New SqlClient.SqlParameter("@EBL_FROMDT", EBL_FROMDT)
            If EBL_TODT = "" Then
                pParms(6) = New SqlClient.SqlParameter("@EBL_TODT", System.DBNull.Value)
            Else
                pParms(6) = New SqlClient.SqlParameter("@EBL_TODT", EBL_TODT)
            End If
            pParms(7) = New SqlClient.SqlParameter("@EBL_AMOUNT", EBL_AMOUNT)
            pParms(8) = New SqlClient.SqlParameter("@EBL_bActive", EBL_bActive)
            pParms(9) = New SqlClient.SqlParameter("@EBL_bSALARYTRANSFER", EBL_bSALARYTRANSFER)
            pParms(10) = New SqlClient.SqlParameter("@EBL_BSU_ID", EBL_BSU_ID)


            pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(11).Direction = ParameterDirection.ReturnValue

            pParms(12) = New SqlClient.SqlParameter("@EBL_ACCNUM", EBL_ACCNUM)

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEMPBANKLOAN_D", pParms)
            Dim ReturnFlag As Integer = pParms(11).Value
            Return ReturnFlag
        End Using

    End Function
    Public Shared Function SaveEMPEXPENSES_D(ByVal EXD_ID As String, ByVal EXD_EMP_ID As String, ByVal EXD_BSU_ID As String, ByVal EXD_RXM_ID As String, _
    ByVal EXD_DATE As Date, ByVal EXD_AMOUNT As String, ByVal EXD_VALIDMONTHS As String, ByVal EXD_CUR_ID As String, ByVal EXD_EXGRATE1 As String, ByVal EXD_EXGRATE2 As String, ByVal EXD_PAID_EMP_ID As String, ByVal EXD_REMARK As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(12) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EXD_ID", EXD_ID)
            pParms(1) = New SqlClient.SqlParameter("@EXD_EMP_ID", EXD_EMP_ID)
            pParms(2) = New SqlClient.SqlParameter("@EXD_BSU_ID", EXD_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@EXD_RXM_ID", EXD_RXM_ID)
            pParms(4) = New SqlClient.SqlParameter("@EXD_DATE", EXD_DATE)
            pParms(5) = New SqlClient.SqlParameter("@EXD_AMOUNT", EXD_AMOUNT)
            pParms(6) = New SqlClient.SqlParameter("@EXD_VALIDMONTHS", EXD_VALIDMONTHS)
            pParms(7) = New SqlClient.SqlParameter("@EXD_CUR_ID", EXD_CUR_ID)
            pParms(8) = New SqlClient.SqlParameter("@EXD_EXGRATE1", EXD_EXGRATE1)
            pParms(9) = New SqlClient.SqlParameter("@EXD_EXGRATE2", EXD_EXGRATE2)
            pParms(10) = New SqlClient.SqlParameter("@EXD_PAID_EMP_ID", EXD_PAID_EMP_ID)
            pParms(11) = New SqlClient.SqlParameter("@EXD_REMARK", EXD_REMARK)

            pParms(12) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(12).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEMPEXPENSES_D", pParms)
            Dim ReturnFlag As Integer = pParms(12).Value
            Return ReturnFlag
        End Using

    End Function



    Public Shared Function SAVEEMPLOYEE_M(ByVal htSaveEmp As Hashtable, ByVal bEdit As Boolean, ByVal trans As SqlTransaction, ByRef EmpNewNo As String, ByRef empID As Integer) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection
            Dim pParms(50) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMPNO", htSaveEmp("EmpNo"))
            pParms(1) = New SqlClient.SqlParameter("@EMP_APPLNO", htSaveEmp("AppNo"))
            pParms(2) = New SqlClient.SqlParameter("@EMP_FNAME", htSaveEmp("Fname"))
            pParms(3) = New SqlClient.SqlParameter("@EMP_MNAME", htSaveEmp("Mname"))
            pParms(4) = New SqlClient.SqlParameter("@EMP_LNAME", htSaveEmp("Lname"))
            pParms(5) = New SqlClient.SqlParameter("@EMP_DES_ID", htSaveEmp("SD_ID"))
            pParms(6) = New SqlClient.SqlParameter("@EMP_ECT_ID", htSaveEmp("Cat_ID"))
            pParms(7) = New SqlClient.SqlParameter("@EMP_EGD_ID", htSaveEmp("Grade_ID"))
            pParms(8) = New SqlClient.SqlParameter("@EMP_BSU_ID", htSaveEmp("WU"))
            pParms(9) = New SqlClient.SqlParameter("@EMP_TGD_ID", htSaveEmp("TeachGrade_ID"))
            pParms(10) = New SqlClient.SqlParameter("@EMP_VISATYPE", htSaveEmp("Vtype"))
            pParms(11) = New SqlClient.SqlParameter("@EMP_VISASATUS", htSaveEmp("Vstatus"))
            pParms(12) = New SqlClient.SqlParameter("@EMP_JOINDT", htSaveEmp("Jdate"))
            pParms(13) = New SqlClient.SqlParameter("@EMP_LASTVACFROM", htSaveEmp("Viss_date"))
            pParms(14) = New SqlClient.SqlParameter("@EMP_LASTVACTO", htSaveEmp("VExp_date"))
            pParms(15) = New SqlClient.SqlParameter("@EMP_LASTREJOINDT", htSaveEmp("LRdate"))
            pParms(16) = New SqlClient.SqlParameter("@EMP_STATUS", htSaveEmp("Status_ID"))
            pParms(17) = New SqlClient.SqlParameter("@EMP_RESGDT", htSaveEmp("Resig"))
            pParms(18) = New SqlClient.SqlParameter("@EMP_PROBTILL", htSaveEmp("Prob"))
            pParms(19) = New SqlClient.SqlParameter("@EMP_ACCOMODATION", htSaveEmp("Accom"))
            pParms(20) = New SqlClient.SqlParameter("@EMP_bACTIVE", htSaveEmp("bitBActive"))
            pParms(21) = New SqlClient.SqlParameter("@EMP_CUR_ID", htSaveEmp("SalC"))
            pParms(22) = New SqlClient.SqlParameter("@EMP_PAY_CUR_ID", htSaveEmp("PayC"))
            pParms(23) = New SqlClient.SqlParameter("@EMP_MODE", htSaveEmp("PayMode"))
            pParms(24) = New SqlClient.SqlParameter("@EMP_BANK", htSaveEmp("Bank_ID"))
            pParms(25) = New SqlClient.SqlParameter("@EMP_ACCOUNT", htSaveEmp("AccCode"))
            pParms(26) = New SqlClient.SqlParameter("@EMP_SWIFTCODE", htSaveEmp("Bcode"))
            pParms(27) = New SqlClient.SqlParameter("@EMP_MOE_DES_ID", htSaveEmp("ME_ID"))
            pParms(28) = New SqlClient.SqlParameter("@EMP_VISA_DES_ID", htSaveEmp("ML_ID"))
            pParms(29) = New SqlClient.SqlParameter("@EMP_VISA_BSU_ID", htSaveEmp("IU"))
            pParms(30) = New SqlClient.SqlParameter("@EMP_MARITALSTATUS", htSaveEmp("Mstatus"))
            pParms(31) = New SqlClient.SqlParameter("@EMP_GROSSSAL", htSaveEmp("Gsalary"))
            pParms(32) = New SqlClient.SqlParameter("@EMP_PASSPORT", htSaveEmp("PassNo"))
            pParms(33) = New SqlClient.SqlParameter("@EMP_REMARKS", htSaveEmp("Remark"))
            pParms(34) = New SqlClient.SqlParameter("@EMP_CTY_ID", htSaveEmp("Country_ID"))
            pParms(35) = New SqlClient.SqlParameter("@EMP_SEX_bMALE", htSaveEmp("bitBmale"))
            pParms(36) = New SqlClient.SqlParameter("@EMP_QLF_ID", htSaveEmp("Qul"))
            pParms(37) = New SqlClient.SqlParameter("@EMP_SGD_ID", htSaveEmp("SalGrade"))
            pParms(38) = New SqlClient.SqlParameter("@EMP_DPT_ID", htSaveEmp("Dept_ID"))
            pParms(39) = New SqlClient.SqlParameter("@EMP_BLOODGRP", htSaveEmp("Bgroup"))
            pParms(40) = New SqlClient.SqlParameter("@EMP_FATHER", htSaveEmp("Father_Name"))
            pParms(41) = New SqlClient.SqlParameter("@EMP_MOTHER", htSaveEmp("Mother_Name"))
            pParms(42) = New SqlClient.SqlParameter("@EMP_REPORTTO_EMP_ID", htSaveEmp("Report"))

            pParms(43) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(44) = New SqlClient.SqlParameter("@EMP_STAFFNO", htSaveEmp("StaffNo"))
            pParms(45) = New SqlClient.SqlParameter("@EMP_ABC", htSaveEmp("ABC_Cat"))
            pParms(46) = New SqlClient.SqlParameter("@EMP_ID", htSaveEmp("EMP_ID"))
            pParms(47) = New SqlClient.SqlParameter("@EMp_bTEmp", htSaveEmp("TempEMP"))
            pParms(48) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(48).Direction = ParameterDirection.ReturnValue
            pParms(49) = New SqlClient.SqlParameter("@Emp_New_No", SqlDbType.VarChar, 15)
            pParms(49).Direction = ParameterDirection.Output
            pParms(50) = New SqlClient.SqlParameter("@NewEMP_ID", SqlDbType.Int)
            pParms(50).Direction = ParameterDirection.Output

            'Dim paramsIndex(17) As Integer

            'paramsIndex(0) = 46
            'paramsIndex(1) = 1
            'paramsIndex(2) = 5
            'paramsIndex(3) = 6
            'paramsIndex(4) = 7
            'paramsIndex(5) = 9
            'paramsIndex(6) = 10
            'paramsIndex(7) = 11
            'paramsIndex(8) = 16
            'paramsIndex(9) = 19
            'paramsIndex(10) = 27
            'paramsIndex(11) = 28
            'paramsIndex(12) = 34
            'paramsIndex(13) = 36
            'paramsIndex(14) = 37
            'paramsIndex(15) = 38
            'paramsIndex(16) = 42
            'For i As Integer = 0 To paramsIndex.Length
            '    If pParms(paramsIndex(i)).Value IsNot DBNull.Value Then
            '        pParms(paramsIndex(i)).Value = CInt(pParms(paramsIndex(i)).Value)
            '    End If
            'Next

            'pParms(46).Value = CInt(pParms(46).Value)
            'pParms(1).DbType = DbType.Int16
            'pParms(5).DbType = DbType.Int16
            'pParms(6).DbType = DbType.Int16
            'pParms(7).DbType = DbType.Int16
            'pParms(9).DbType = DbType.Int16
            'pParms(10).DbType = DbType.Int16
            'pParms(11).DbType = DbType.Int16
            'pParms(16).DbType = DbType.Int16
            'pParms(19).DbType = DbType.Int16
            'pParms(27).DbType = DbType.Int16
            'pParms(28).DbType = DbType.Int16
            'pParms(34).DbType = DbType.Int16
            'pParms(36).DbType = DbType.Int16
            'pParms(37).DbType = DbType.Int16
            'pParms(38).DbType = DbType.Int16
            'pParms(42).DbType = DbType.Int16

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SAVEEMPLOYEE_M", pParms)

            Dim ReturnFlag As Integer = pParms(48).Value
            If ReturnFlag = 0 Then
                EmpNewNo = pParms(49).Value
                empID = pParms(50).Value
            End If
            Return ReturnFlag
        End Using

    End Function
    '    Public Shared Function SAVEEMPLOYEE_M(ByVal varEmpNo As String, ByVal intAppNo As String, ByVal Fname As String, _
    ' ByVal Mname As String, ByVal Lname As String, ByVal intSD As String, ByVal intCat As String, ByVal intGrade As String, _
    ' ByVal varBSU As String, ByVal intTgrade As String, ByVal intVtype As String, ByVal intVstatus As String, ByVal dateJoin As String, _
    ' ByVal dateViss As String, ByVal dateVexp As String, ByVal dateLrejoin As String, ByVal intEmpStatus As String, _
    ' ByVal dateResig As String, ByVal dateProb As String, ByVal intAccom As String, ByVal bitBactive As Boolean, _
    ' ByVal varCur As String, ByVal varPay As String, ByVal varPayMode As String, ByVal varBank As String, ByVal varAccCode As String, _
    'ByVal varBankCode As String, ByVal intME As String, ByVal intML As String, ByVal varIU As String, _
    'ByVal varMarital As String, ByVal curGrossSal As String, ByVal varPassport As String, ByVal varRemark As String, _
    'ByVal intCountry As String, ByVal bitBmale As Boolean, ByVal intQlf_ID As String, ByVal intSalGrade As String, _
    'ByVal intDept As String, ByVal varBlood As String, ByVal varFather As String, ByVal varMother As String, _
    'ByVal intReportTo As String, ByVal staffNo As String, ByVal cat As String, ByVal empid As String, ByVal TempEMP As Boolean, ByVal bEdit As Boolean, ByRef newNo As String, ByRef empIDTEMP As Integer, ByVal trans As SqlTransaction) As Integer

    '        Using connection As SqlConnection = ConnectionManger.GetOASISConnection
    '            Dim pParms(50) As SqlClient.SqlParameter
    '            pParms(0) = New SqlClient.SqlParameter("@EMPNO", varEmpNo)
    '            pParms(1) = New SqlClient.SqlParameter("@EMP_APPLNO", intAppNo)
    '            pParms(2) = New SqlClient.SqlParameter("@EMP_FNAME", Fname)
    '            pParms(3) = New SqlClient.SqlParameter("@EMP_MNAME", Mname)
    '            pParms(4) = New SqlClient.SqlParameter("@EMP_LNAME", Lname)
    '            pParms(5) = New SqlClient.SqlParameter("@EMP_DES_ID", intSD)
    '            pParms(6) = New SqlClient.SqlParameter("@EMP_ECT_ID", intCat)
    '            pParms(7) = New SqlClient.SqlParameter("@EMP_EGD_ID", intGrade)
    '            pParms(8) = New SqlClient.SqlParameter("@EMP_BSU_ID", varBSU)
    '            pParms(9) = New SqlClient.SqlParameter("@EMP_TGD_ID", intTgrade)
    '            pParms(10) = New SqlClient.SqlParameter("@EMP_VISATYPE", intVtype)
    '            pParms(11) = New SqlClient.SqlParameter("@EMP_VISASATUS", intVstatus)
    '            pParms(12) = New SqlClient.SqlParameter("@EMP_JOINDT", dateJoin)
    '            pParms(13) = New SqlClient.SqlParameter("@EMP_LASTVACFROM", dateViss)
    '            pParms(14) = New SqlClient.SqlParameter("@EMP_LASTVACTO", dateVexp)
    '            pParms(15) = New SqlClient.SqlParameter("@EMP_LASTREJOINDT", dateLrejoin)
    '            pParms(16) = New SqlClient.SqlParameter("@EMP_STATUS", intEmpStatus)
    '            pParms(17) = New SqlClient.SqlParameter("@EMP_RESGDT", dateResig)
    '            pParms(18) = New SqlClient.SqlParameter("@EMP_PROBTILL", dateProb)
    '            pParms(19) = New SqlClient.SqlParameter("@EMP_ACCOMODATION", intAccom)
    '            pParms(20) = New SqlClient.SqlParameter("@EMP_bACTIVE", bitBactive)
    '            pParms(21) = New SqlClient.SqlParameter("@EMP_CUR_ID", varCur)
    '            pParms(22) = New SqlClient.SqlParameter("@EMP_PAY_CUR_ID", varPay)
    '            pParms(23) = New SqlClient.SqlParameter("@EMP_MODE", varPayMode)
    '            pParms(24) = New SqlClient.SqlParameter("@EMP_BANK", varBank)
    '            pParms(25) = New SqlClient.SqlParameter("@EMP_ACCOUNT", varAccCode)
    '            pParms(26) = New SqlClient.SqlParameter("@EMP_SWIFTCODE", varBankCode)
    '            pParms(27) = New SqlClient.SqlParameter("@EMP_MOE_DES_ID", intME)
    '            pParms(28) = New SqlClient.SqlParameter("@EMP_VISA_DES_ID", intML)
    '            pParms(29) = New SqlClient.SqlParameter("@EMP_VISA_BSU_ID", varIU)
    '            pParms(30) = New SqlClient.SqlParameter("@EMP_MARITALSTATUS", varMarital)
    '            pParms(31) = New SqlClient.SqlParameter("@EMP_GROSSSAL", curGrossSal)
    '            pParms(32) = New SqlClient.SqlParameter("@EMP_PASSPORT", varPassport)
    '            pParms(33) = New SqlClient.SqlParameter("@EMP_REMARKS", varRemark)
    '            pParms(34) = New SqlClient.SqlParameter("@EMP_CTY_ID", intCountry)
    '            pParms(35) = New SqlClient.SqlParameter("@EMP_SEX_bMALE", bitBmale)
    '            pParms(36) = New SqlClient.SqlParameter("@EMP_QLF_ID", intQlf_ID)
    '            pParms(37) = New SqlClient.SqlParameter("@EMP_SGD_ID", intSalGrade)
    '            pParms(38) = New SqlClient.SqlParameter("@EMP_DPT_ID", intDept)
    '            pParms(39) = New SqlClient.SqlParameter("@EMP_BLOODGRP", varBlood)
    '            pParms(40) = New SqlClient.SqlParameter("@EMP_FATHER", varFather)
    '            pParms(41) = New SqlClient.SqlParameter("@EMP_MOTHER", varMother)
    '            pParms(42) = New SqlClient.SqlParameter("@EMP_REPORTTO_EMP_ID", intReportTo)

    '            pParms(43) = New SqlClient.SqlParameter("@EMP_STAFFNO", staffNo)
    '            pParms(44) = New SqlClient.SqlParameter("@EMP_ABC", cat)
    '            pParms(45) = New SqlClient.SqlParameter("@EMP_ID", empid)
    '            pParms(46) = New SqlClient.SqlParameter("@EMp_bTEmp", TempEMP)

    '            pParms(47) = New SqlClient.SqlParameter("@bEdit", bEdit)



    '            pParms(48) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '            pParms(48).Direction = ParameterDirection.ReturnValue
    '            pParms(49) = New SqlClient.SqlParameter("@Emp_New_No", SqlDbType.VarChar, 15)
    '            pParms(49).Direction = ParameterDirection.Output
    '            pParms(50) = New SqlClient.SqlParameter("@NewEMP_ID", SqlDbType.Int)
    '            pParms(50).Direction = ParameterDirection.Output




    '            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SAVEEMPLOYEE_M", pParms)
    '            Dim EmpNewno As String


    '            Dim ReturnFlag As Integer = pParms(48).Value
    '            If ReturnFlag = 0 Then
    '                EmpNewno = IIf(TypeOf (pParms(49).Value) Is DBNull, String.Empty, pParms(49).Value)
    '                empIDTEMP = IIf(TypeOf (pParms(50).Value) Is DBNull, String.Empty, pParms(50).Value) 'pParms(50).Value
    '            End If
    '            Return ReturnFlag
    '        End Using

    '    End Function
#End Region

#Region " All Delete function"

    Public Shared Function DeleteIssue_Asset(ByVal EAD_ID As Integer, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EAD_ID", EAD_ID)

            'pParms(1) = New SqlClient.SqlParameter("@EAD_ID", EAD_ID)
            'pParms(2) = New SqlClient.SqlParameter("@EAD_ID", EAD_ID)

            ' ByVal EAD_EMP_ID As String, ByVal EAD_FAS_ID As String,

            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteIssue_Asset", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function Delete_EAH_ID(ByVal EAH_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EAH_ID", EAH_ID)

            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteEMPLVLADJUSTMENT", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function Delete_EXD_ID(ByVal EXD_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EXD_ID", EXD_ID)

            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteEMPEXPENSES_D", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function Delete_EBL_ID(ByVal EBL_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EBL_ID", EBL_ID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteEMPBANKLOAN_D", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

#End Region


End Class
