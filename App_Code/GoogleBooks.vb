﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Text.RegularExpressions

Namespace GoogleBooks



    Public Class GoogleBooks


        Public Shared Function GoogleBookData(ByVal ISBN As String) As Hashtable
            Dim Hash As New Hashtable
            '--------------------------------------------------------------------------------------------------------------------
            ''Get Encript Data
            Dim ISBNEnc = GetBookIDEnc(ISBN)

            If ISBNEnc <> "" Then
                Hash.Add("DataFound", "YES")
            Else
                Hash.Add("DataFound", "No")
            End If
            Hash.Add("ISBNEnc", ISBNEnc)
            '--------------------------------------------------------------------------------------------------------------------
            ''Get XML Document
            Dim xmlDoc As New XmlDocument()
            xmlDoc.Load("http://www.google.com/books/feeds/volumes/" & ISBNEnc)
            xmlDoc.InnerXml = xmlDoc.InnerXml.Replace("dc:", "")
            '--------------------------------------------------------------------------------------------------------------------
            ''Get NameSpace
            Dim nsMgr As New XmlNamespaceManager(xmlDoc.NameTable)
            nsMgr.AddNamespace("def", "http://www.w3.org/2005/Atom")
            '--------------------------------------------------------------------------------------------------------------------
            ''Get Title
            Dim title As String = GetBookData("/def:entry/def:title", xmlDoc, nsMgr)
            Dim temp As String()
            temp = Regex.Split(title, "&&") ''title.Split("&&")
            Dim i = 0
            Dim first = 0

            If temp.Length > 0 Then

                If temp.Length = 1 Then

                    title = temp(0)

                Else

                    For i = 1 To temp.Length - 1

                        If temp(i).Trim() <> "" Then

                            If first = 0 Then
                                title = temp(i)
                                first = 1
                            Else

                                title = title & " - " & temp(i)
                            End If

                        End If

                    Next

                End If

            End If
            Hash.Add("title", title)
            '--------------------------------------------------------------------------------------------------------------------
            Dim isbndata As String = GetBookData("/def:entry/def:identifier", xmlDoc, nsMgr)
            temp = Nothing
            temp = Regex.Split(isbndata, "&&") ''isbndata.Split("&&")
            i = 0
            first = 0
            Dim isbnEncData As String = ""


            If temp.Length > 0 Then
                isbnEncData = temp(0)

                For i = 1 To temp.Length - 1

                    If temp(i).Trim() <> "" Then

                        If first = 0 Then
                            isbndata = temp(i)
                            first = 1
                        Else

                            isbndata = isbndata & " , " & temp(i)
                        End If

                    End If

                Next


            End If
            Hash.Add("isbndata", isbndata)

            '--------------------------------------------------------------------------------------------------------------------
            ''Author,Publisher,Year,Pages,Subject,Description,ImageUrl,InfoUrl
            Hash.Add("Author", GetBookData("/def:entry/def:creator", xmlDoc, nsMgr).Replace("&&", " , "))
            Hash.Add("Publisher", GetBookData("/def:entry/def:publisher", xmlDoc, nsMgr).Replace("&&", " , "))
            Hash.Add("Year", GetBookData("/def:entry/def:date", xmlDoc, nsMgr).Replace("&&", " , "))
            Hash.Add("Pages", GetBookData("/def:entry/def:format", xmlDoc, nsMgr).Replace("&&", " - "))
            Hash.Add("Subject", GetBookData("/def:entry/def:subject", xmlDoc, nsMgr).Replace("&&", " , "))
            Hash.Add("Description", GetBookData("/def:entry/def:description", xmlDoc, nsMgr))
            Hash = GetBookDataUrl("/def:entry/def:link", xmlDoc, nsMgr, Hash)



            Return Hash
        End Function

        Private Shared Function GetBookDataUrl(ByVal SelectNode As String, ByVal xmlDoc As XmlDocument, ByVal nsMgr As XmlNamespaceManager, ByVal Hash As Hashtable) As Hashtable

            Dim HashTemp As Hashtable = Hash
            Dim nodes As XmlNodeList = xmlDoc.SelectNodes(SelectNode, nsMgr)

            Dim i = 0

            For Each node As XmlNode In nodes
                Dim rel As String = node.Attributes.GetNamedItem("rel").InnerText
                ''Get the Image
                If rel.IndexOf("thumbnail") > -1 Then
                    HashTemp.Add("ImageUrl", node.Attributes.GetNamedItem("href").InnerText.Replace("amp;", ""))
                End If
                '' Get the Product url
                If rel.IndexOf("info") > -1 Then
                    HashTemp.Add("InfoUrl", node.Attributes.GetNamedItem("href").InnerText.Replace("amp;", ""))
                End If
                'Exit For
            Next

            Return HashTemp
        End Function



        Private Shared Function GetBookIDEnc(ByVal ISBN As String) As String

            Dim sb As New StringBuilder()

            Dim xmlDoc As New XmlDocument()

            xmlDoc.Load("http://www.google.com/books/feeds/volumes?q=" & ISBN)

            xmlDoc.InnerXml = xmlDoc.InnerXml.Replace("dc:", "")

            Dim nsMgr As New XmlNamespaceManager(xmlDoc.NameTable)
            nsMgr.AddNamespace("def", "http://www.w3.org/2005/Atom")

            Dim entry As XmlNodeList = xmlDoc.SelectNodes("/def:feed/def:entry", nsMgr)

            If entry.Count = 1 Then '' Service Retrive  1 results for an given ISBN

                Dim nodes As XmlNodeList = xmlDoc.SelectNodes("/def:feed/def:entry/def:identifier", nsMgr)
                For Each node As XmlNode In nodes
                    sb.AppendLine(node.InnerXml)
                    Exit For
                Next

            Else '' Service Retrive more than 1 results for an given ISBN

                Dim nodes As XmlNodeList = xmlDoc.SelectNodes("/def:feed/def:entry/def:identifier", nsMgr)
                For Each node As XmlNode In nodes
                    Dim s = node.InnerXml.IndexOf("ISBN")
                    If s = "-1" Then
                        sb.AppendLine("%%")
                    End If
                    sb.AppendLine(node.InnerXml)
                    sb.AppendLine("&&")
                Next

                Dim identifiers As String = sb.ToString().Trim()

                sb = New StringBuilder
                Dim temp As String() = Regex.Split(identifiers, "%%") ''identifiers.Split("%%")

                Dim i = 0
                For i = 0 To temp.Length - 1
                    Dim s = temp(i).IndexOf(ISBN)
                    If s > -1 Then
                        Dim isbenc As String = temp(i).Replace("%%", "")
                        temp = Nothing
                        temp = Regex.Split(isbenc, "&&") ''isbenc.Split("&&")
                        Dim val = temp(0).ToString()
                        sb.Append(val)
                        Exit For
                    End If

                Next

            End If


            Return sb.ToString().Trim()

        End Function

        Private Shared Function GetBookData(ByVal SelectNode As String, ByVal xmlDoc As XmlDocument, ByVal nsMgr As XmlNamespaceManager) As String

            Dim sb As New StringBuilder()

            Dim nodes As XmlNodeList = xmlDoc.SelectNodes(SelectNode, nsMgr)

            Dim i = 0

            For Each node As XmlNode In nodes
                sb.AppendLine(node.InnerXml)
                i = i + 1
                If nodes.Count <> i Then
                    sb.AppendLine("&&")
                End If

                'Exit For
            Next

            Return sb.ToString().Trim()


        End Function


    End Class

End Namespace