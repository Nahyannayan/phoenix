Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Imports System.Data.Oledb
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class GetEmailData
     Inherits System.Web.Services.WebService

    <WebMethod()> _
     Public Function SendNewsletterExcel(ByVal Path As String, ByVal LastCount As String) As XmlDocument
        ''Path is Encrypted 
        Return GetGridDataNewsLetter_Plaintext(Path, LastCount)

    End Function

    Public Function GetGridDataNewsLetter_Plaintext(ByVal path As String, ByVal lastcount As String) As XmlDocument

        Dim Encr_decrData As New Encryption64
        Dim ds As New DataSet()
        Dim Xdoc As New XmlDocument
        Try

            Dim strConn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim condition = "select ROW_NUMBER() over(order by ID) as RowID , ID , EMAIL_ID from  COM_EXCEL_EMAIL_DATA_LOG where LOG_ID='" & path & "' and NO_ERROR_FLAG='True'"

            condition = " select top 6 isnull(ID,'') as UniqueID, isnull(EMAIL_ID,'') as EMAIL_ID ,RowID from ( " & condition & " ) a  where rowid >" & lastcount & " order by rowid"

            ds = SqlHelper.ExecuteDataset(strConn, CommandType.Text, condition)

            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)

        Catch ex As Exception

        End Try

        Return Xdoc

    End Function

    <WebMethod()> _
    Public Function SendPlainTextExcel(ByVal Path As String, ByVal LastCount As String) As XmlDocument
        ''Path is Encrypted 
        Return GetGridDataNewsLetter_Plaintext(Path, LastCount)

    End Function








    <WebMethod()> _
   Public Function SendNewsletterGroups(ByVal cse_id As String, ByVal LastCount As String) As XmlDocument

        Dim Xdoc As New XmlDocument
        Try
            Dim ds As DataSet = GetGridDataNewsLetter_Plaintext_Groups(cse_id, LastCount)
            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)
        Catch ex As Exception

        End Try



        Return Xdoc

    End Function

    <WebMethod()> _
      Public Function SendPlainTextGroups(ByVal cse_id As String, ByVal LastCount As String) As XmlDocument

        Dim Xdoc As New XmlDocument
        Try
            Dim ds As DataSet = GetGridDataNewsLetter_Plaintext_Groups(cse_id, LastCount)
            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)
        Catch ex As Exception

        End Try



        Return Xdoc

    End Function





    Public Function GetGridDataNewsLetter_Plaintext_Groups(ByVal cse_id As String, ByVal lastcount As String) As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = ""
        Dim ds As DataSet

        sql_query = "select CSE_CGR_ID,CSE_EML_ID from COM_SEND_EMAIL where  CSE_ID='" & cse_id & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        Dim groupid As String = ds.Tables(0).Rows(0).Item("CSE_CGR_ID").ToString()
        Dim Templateid = ds.Tables(0).Rows(0).Item("CSE_EML_ID").ToString()

        sql_query = "select CGR_GRP_TYPE,CGR_CONDITION,CGR_TYPE,CGR_REMOVE_EMAIL_IDS from COM_GROUPS_M where CGR_ID='" & groupid & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        Dim condition As String = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
        Dim grptype As String = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()

        If grptype = "GRP" Then  ''Group 


            condition = " select top 6 isnull(uniqueid,'') as uniqueid , isnull(Email,'') as Email  ,RowID from ( " & condition & " ) a  where STU_bRCVMAIL='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString() & " and rowid >" & lastcount & " order by rowid"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, condition)
        End If

        If grptype = "AON" Then ''Add On 

            condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVMAIL='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString()


            sql_query = " select top 6 isnull(uniqueid,'') as uniqueid , isnull(Email,'') as Email, RowID from ( " & condition & " ) a  where rowid >" & lastcount & " order by rowid"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        End If
        Return ds

    End Function

End Class
