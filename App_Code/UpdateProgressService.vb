﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class UpdateProgressService
    Inherits System.Web.Services.WebService

    Private Shared workProcessor As New FeeBulkAdjustment()


    <WebMethod, ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Function StartProcess() As String
        ' Non-blocking - kick off the long-running process and return immediately.
        workProcessor.StartProcess()

        Return "OK"
    End Function
    Public Function EndProcess(ByVal status As Boolean, ByVal msg As String) As String
        ' Non-blocking - kick off the long-running process and return immediately.
        WorkProcessor.EndProcess(status, msg)

        Return "OK"
    End Function

    Public Function UpdatePercentage(ByVal newPercentComplete As Double, ByVal workDescription As String) As String
        ' Non-blocking - kick off the long-running process and return immediately.
        workProcessor.UpdatePercent(newPercentComplete, workDescription)

        Return "OK"
    End Function


    <WebMethod, ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Function GetStatus() As FeeBulkAdjustment
        If workProcessor IsNot Nothing AndAlso workProcessor.IsRunning Then
            ' Return a dynamic object, jQuery will read the properties
            FeeBulkAdjustment.Errorlog("IMPORT_ADJUSTMENT - workProcessor : " & workProcessor.IsRunning, "OASIS")
            Return workProcessor
        ElseIf workProcessor IsNot Nothing And workProcessor.Message <> "" Then
            '    '    ' If null, the client knows it's not running/not initialized.
            FeeBulkAdjustment.Errorlog("IMPORT_ADJUSTMENT - workProcessor.Message : " & workProcessor.Message, "OASIS")
            '    '    workProcessor.EndProcess(workProcessor.IsSuccess, "")
            Return Nothing
        Else
            FeeBulkAdjustment.Errorlog("IMPORT_ADJUSTMENT - workProcessor : Nothing", "OASIS")
            Return Nothing
        End If
    End Function

End Class