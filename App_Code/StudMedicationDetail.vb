﻿Imports Microsoft.VisualBasic

Public Class StudMedicationDetail

    Private _MEDICATION_ID As Int32
    Private _INCIDENT_ID As Int32
    Private _MEDICATION As String
    Private _STU_ID As Int64
    Private _STU_NAME As String
    Private _MEDICALCONDITION As String

    Private _SIDEEFFECTS As String
    Private _NEXTDOSAGEIFANY As DateTime
    Private _LASTDOSAGE As DateTime
    Private _SPECIALPRECAUTIONS As String
    Private _CONDITION As Int32
    Private _ATTACHMENT As String
    Private _CONDITIONDESCR As String



    Public Property MEDICATION_ID() As Int32
        Get
            Return _MEDICATION_ID
        End Get
        Set(ByVal value As Int32)
            _MEDICATION_ID = value
        End Set
    End Property

    Public Property INCIDENT_ID() As Int32
        Get
            Return _INCIDENT_ID
        End Get
        Set(ByVal value As Int32)
            _INCIDENT_ID = value
        End Set
    End Property

    Public Property MEDICATION() As String
        Get
            Return _MEDICATION
        End Get
        Set(ByVal value As String)
            _MEDICATION = value
        End Set
    End Property

    Public Property STU_ID() As Int64
        Get
            Return _STU_ID
        End Get
        Set(ByVal value As Int64)
            _STU_ID = value
        End Set
    End Property

    Public Property STU_NAME() As String
        Get
            Return _STU_NAME
        End Get
        Set(ByVal value As String)
            _STU_NAME = value
        End Set
    End Property


    Public Property MEDICALCONDITION() As String
        Get
            Return _MEDICALCONDITION
        End Get
        Set(ByVal value As String)
            _MEDICALCONDITION = value
        End Set
    End Property

    Public Property SIDEEFFECTS() As String
        Get
            Return _SIDEEFFECTS
        End Get
        Set(ByVal value As String)
            _SIDEEFFECTS = value
        End Set
    End Property


    Public Property NEXTDOSAGEIFANY() As DateTime
        Get
            Return _NEXTDOSAGEIFANY
        End Get
        Set(ByVal value As DateTime)
            _NEXTDOSAGEIFANY = value
        End Set
    End Property


    Public Property LASTDOSAGE() As DateTime
        Get
            Return _LASTDOSAGE
        End Get
        Set(ByVal value As DateTime)
            _LASTDOSAGE = value
        End Set
    End Property


    Public Property SPECIALPRECAUTIONS() As String
        Get
            Return _SPECIALPRECAUTIONS
        End Get
        Set(ByVal value As String)
            _SPECIALPRECAUTIONS = value
        End Set
    End Property


    Public Property CONDITION() As Int32
        Get
            Return _CONDITION
        End Get
        Set(ByVal value As Int32)
            _CONDITION = value
        End Set
    End Property

    Public Property CONDITIONDESCR() As String
        Get
            Return _CONDITIONDESCR
        End Get
        Set(ByVal value As String)
            _CONDITIONDESCR = value
        End Set
    End Property
    Public Property ATTACHMENT() As String
        Get
            Return _ATTACHMENT
        End Get
        Set(ByVal value As String)
            _ATTACHMENT = value
        End Set
    End Property


End Class
