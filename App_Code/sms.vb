Imports Microsoft.VisualBasic
Imports System.Web.Configuration
Imports System.Web.SessionState.HttpSessionState
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Namespace SmsService
    Public Class sms

        Public Shared Function SendMessage(ByVal Mobilenumber As String, ByVal Message As String, ByVal From As String, ByVal Username As String, ByVal Password As String) As String
            Dim ReturnValue As String = ""

            Dim ServicePath, TestFlag As String

            ServicePath = GetSMSServicePath()
            TestFlag = GetSMSTestFlag()

            Dim myWebClient As New System.Net.WebClient
            'Dim wp As New System.Net.WebProxy("proxy1.emirates.net.ae", 8080)
            'wp.UseDefaultCredentials = True

            Dim session As HttpSessionState = HttpContext.Current.Session

            If session("sbsuid") <> "315888" Then 'All other schools except GIP Malaysia
                Dim ValueCollection As New System.Collections.Specialized.NameValueCollection
                ValueCollection.Add("UserName", Username)
                ValueCollection.Add("Password", Password)
                ValueCollection.Add("Message", Message)
                ValueCollection.Add("MobNumber", Mobilenumber)
                ValueCollection.Add("MsgType", "SMS")
                ValueCollection.Add("From", From)
                ValueCollection.Add("Test", TestFlag) '' 1- Test , 0- No Test (Live)
                ValueCollection.Add("Store", "1")
                'myWebClient.Proxy = wp
                Dim responseArray As Byte() = myWebClient.UploadValues(ServicePath, "POST", ValueCollection)
                ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)
            Else 'GIP Malaysia school
                ServicePath &= "username=" & GetSMSUserName() & " &password=" & GetSMSPassword() & "  &message=" & Message & "&mobile=" & Mobilenumber & "&sender= " & From & "&type=1"
                ReturnValue = myWebClient.DownloadString(ServicePath)
            End If

            Return ReturnValue
        End Function

        Public Shared Function GetSMSServicePath() As String
            Dim SMSServicePath As String = ""
            Dim session As HttpSessionState = HttpContext.Current.Session
            Dim DT As New DataTable

            DT = GetSMSConfiguration(session("sbsuid"), "COM")
            SMSServicePath = DT.Rows(0)("BSC_SMS_HOST").ToString()
            ''comemnted bynahyan on 29 may 2018 to fetch configuration dynamically
            'If session("sbsuid") <> "315888" Then 'All other schools except GIP Malaysia
            '    SMSServicePath = Web.Configuration.WebConfigurationManager.AppSettings("smsServicePath").ToString()
            'Else 'GIP Malaysia school
            '    SMSServicePath = Web.Configuration.WebConfigurationManager.AppSettings("GIPMalaysiaSMSServicePath").ToString()
            'End If
            Return SMSServicePath
        End Function

        Public Shared Function GetSMSTestFlag() As String
            Dim TestFlag As String
            Dim session As HttpSessionState = HttpContext.Current.Session
            If session("sbsuid") <> "315888" Then 'All other schools except GIP Malaysia
                TestFlag = Web.Configuration.WebConfigurationManager.AppSettings("smsTestFlag").ToString()
            Else 'GIP Malaysia school
                TestFlag = Web.Configuration.WebConfigurationManager.AppSettings("GIPMalaysiaSMSTestFlag").ToString()
            End If
            Return TestFlag
        End Function

        Public Shared Function GetSMSUserName() As String
            Dim retVal As String = ""


            Dim session As HttpSessionState = HttpContext.Current.Session
            Dim DT As New DataTable

            DT = GetSMSConfiguration(session("sbsuid"), "COM")
            retVal = DT.Rows(0)("BSC_SMS_USER").ToString()
            'Dim session As HttpSessionState = HttpContext.Current.Session
            'If session("sbsuid") <> "315888" Then 'All other schools except GIP Malaysia
            '    retVal = Web.Configuration.WebConfigurationManager.AppSettings("smsUsername").ToString()
            'Else 'GIP Malaysia school
            '    retVal = Web.Configuration.WebConfigurationManager.AppSettings("GIPMalaysiaSMSUsername").ToString()
            'End If
            Return retVal
        End Function

        Public Shared Function GetSMSPassword() As String
            Dim retVal As String = ""


            Dim session As HttpSessionState = HttpContext.Current.Session
            Dim DT As New DataTable

            DT = GetSMSConfiguration(session("sbsuid"), "COM")
            retVal = DT.Rows(0)("BSC_SMS_PASSWORD").ToString()
            'Dim session As HttpSessionState = HttpContext.Current.Session
            'If session("sbsuid") <> "315888" Then 'All other schools except GIP Malaysia
            '    retVal = Web.Configuration.WebConfigurationManager.AppSettings("smspwd").ToString()
            'Else 'GIP Malaysia school
            '    retVal = Web.Configuration.WebConfigurationManager.AppSettings("GIPMalaysiaSMSpwd").ToString()
            'End If
            Return retVal
        End Function

        Public Shared Function GetSMSConfiguration(ByVal bsuId As String, ByVal comType As String) As DataTable

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Try
                ''SMS configuration from db nahyan on 29 May 2018
                Dim param(3) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@bsuId", bsuId)
                param(1) = New SqlClient.SqlParameter("@ComType", comType)
                Dim ds As New DataSet
                Dim dtN As New DataTable

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_SMS_CONFIGURATION_BY_BSU", param)

                If Not ds Is Nothing Then
                    If Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        dtN = ds.Tables(0)

                    End If
                End If
                Return dtN
            Catch ex As Exception

            End Try

            ''ends here nahyan 
        End Function

    End Class
End Namespace
