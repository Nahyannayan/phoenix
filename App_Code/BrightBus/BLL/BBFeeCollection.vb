Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class BBFeeCollection

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE(ByVal p_FCL_ID As Integer, ByVal p_FCL_SOURCE As String, _
      ByVal p_FCL_DATE As DateTime, ByVal p_FCL_RECNO As String, ByVal p_FCL_ACD_ID As Integer, ByVal p_FCL_STU_ID As Integer, _
      ByVal p_FCL_AMOUNT As Decimal, ByVal p_FCL_Bposted As Boolean, _
      ByRef p_newFCL_ID As Integer, ByVal p_FCL_BSU_ID As String, ByVal p_FCL_NARRATION As String, _
      ByRef p_NEW_FCL_RECNO As String, ByVal p_FCL_OUTSTANDING As String, _
      ByVal p_FCL_STU_BSU_ID As String, ByVal p_FCL_REFNO As String, ByVal p_FCL_SBL_ID As String, _
      ByVal p_FCL_SCH_LIST As String, ByVal p_FCL_SCH_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(21) As SqlClient.SqlParameter

        'EXEC	@return_value = [FEES].[F_SaveFEECOLLECTION_H]
        '@FCL_ID = 0 ,
        '@FCL_SOURCE = N'COUNTER1',
        '@FCL_RECNO = N'VHH123',
        '@FCL_DATE = N'12-MAY-2008',
        '@FCL_ACD_ID = 79,

        pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.Int)
        pParms(0).Value = p_FCL_ID
        pParms(1) = New SqlClient.SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCL_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCL_RECNO
        pParms(3) = New SqlClient.SqlParameter("@FCL_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCL_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCL_ACD_ID", SqlDbType.VarChar, 100)
        pParms(4).Value = p_FCL_ACD_ID
        '@FCL_STU_ID = 52001,
        '@FCL_AMOUNT = 1200,
        '@FCL_EMP_ID = 122,

        pParms(5) = New SqlClient.SqlParameter("@FCL_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCL_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCL_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCL_AMOUNT

        pParms(7) = New SqlClient.SqlParameter("@FCL_Bposted", SqlDbType.Bit)
        pParms(7).Value = p_FCL_Bposted

        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        '@FCL_Bposted = 0
        '@NEW_FCL_ID
        '@FCL_NARRATION VARCHAR(100) , 
        '@FCL_DRCR VARCHAR(5)

        pParms(9) = New SqlClient.SqlParameter("@NEW_FCL_ID", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.Output
        pParms(10) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 100)
        pParms(10).Value = p_FCL_BSU_ID
        pParms(11) = New SqlClient.SqlParameter("@FCL_NARRATION", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCL_NARRATION
        pParms(12) = New SqlClient.SqlParameter("@NEW_FCL_RECNO", SqlDbType.VarChar, 30)
        pParms(12).Direction = ParameterDirection.Output
        '@FCL_OUTSTANDING NUMERIC(18,3), 
        '@FCL_STU_BSU_ID        
        '@FCL_REFNO varchar(20), 
        '@FCL_SBL_ID INT,
        pParms(13) = New SqlClient.SqlParameter("@FCL_OUTSTANDING", SqlDbType.Decimal)
        pParms(13).Value = p_FCL_OUTSTANDING
        pParms(14) = New SqlClient.SqlParameter("@FCL_STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(14).Value = p_FCL_STU_BSU_ID
        pParms(15) = New SqlClient.SqlParameter("@FCL_REFNO", SqlDbType.VarChar, 20)
        pParms(15).Value = p_FCL_REFNO
        pParms(16) = New SqlClient.SqlParameter("@FCL_SBL_ID", SqlDbType.Int)
        pParms(16).Value = p_FCL_SBL_ID

        '@FCL_SCH_LIST  INT,
        '@FCL_SCH_ID ,
        pParms(17) = New SqlClient.SqlParameter("@FCL_SCH_LIST", SqlDbType.VarChar, 100)
        pParms(17).Value = p_FCL_SCH_LIST
        pParms(18) = New SqlClient.SqlParameter("@FCL_SCH_ID", SqlDbType.Int)
        pParms(18).Value = p_FCL_SCH_ID

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H_ONLINE", pParms)
        F_SaveFEECOLLECTION_H_ONLINE = pParms(8).Value
        If pParms(8).Value = 0 Then
            p_newFCL_ID = pParms(9).Value
            p_NEW_FCL_RECNO = pParms(12).Value
        End If

    End Function


    Public Shared Function F_SaveFEECOLLSUB_ONLINE(ByVal p_FCS_ID As Integer, ByVal p_FCS_FCL_ID As Integer, _
    ByVal p_FCS_FEE_ID As Integer, ByVal p_FCS_AMOUNT As Decimal, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEECOLLSUB]
        '@FCS_ID = 0,
        '@FCS_FCL_ID = 1,
        '@FCS_FEE_ID = 3,
        '@FCS_AMOUNT = 500

        pParms(0) = New SqlClient.SqlParameter("@FCS_ID", SqlDbType.Int)
        pParms(0).Value = p_FCS_ID
        pParms(1) = New SqlClient.SqlParameter("@FCS_FCL_ID", SqlDbType.Int)
        pParms(1).Value = p_FCS_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCS_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FCS_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FCS_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCS_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_ONLINE", pParms)

        F_SaveFEECOLLSUB_ONLINE = pParms(4).Value
    End Function


    Public Shared Function F_SaveFEECOLLSUB_D_ONLINE(ByVal p_FCD_ID As Integer, ByVal p_FCD_FCL_ID As String, _
        ByVal p_FCD_CLT_ID As Integer, ByVal p_FCD_AMOUNT As Decimal, ByVal p_FCD_REFNO As String, _
        ByVal p_FCD_REF_ID As String, ByVal p_FCD_EMR_ID As String, ByVal p_FCD_DATE As String, _
        ByVal p_FCD_STATUS As String, ByVal p_FCD_VHH_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter

        '        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '       @FCD_ID INT, 
        '@FCD_FCL_ID INT,
        '@FCD_CLT_ID INT, 
        '@FCD_AMOUNT NUMERIC(18,3), 
        '@FCD_REFNO VARCHAR(20), 
        '@FCD_DATE DATETIME, 
        '@FCD_STATUS INT, 
        '@FCD_VHH_DOCNO VARCHAR(20),
        '@FCD_REF_ID VARCHAR(20), 
        '@FCD_EMR_ID VARCHAR(20) 
        pParms(0) = New SqlClient.SqlParameter("@FCD_ID", SqlDbType.Int)
        pParms(0).Value = p_FCD_ID
        pParms(1) = New SqlClient.SqlParameter("@FCD_FCL_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FCD_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCD_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FCD_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCD_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FCD_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FCD_REFNO

        pParms(5) = New SqlClient.SqlParameter("@FCD_REF_ID", SqlDbType.VarChar, 20)
        pParms(5).Value = p_FCD_REF_ID
        pParms(6) = New SqlClient.SqlParameter("@FCD_EMR_ID", SqlDbType.VarChar, 20)
        pParms(6).Value = p_FCD_EMR_ID

        pParms(7) = New SqlClient.SqlParameter("@FCD_DATE", SqlDbType.DateTime)
        If p_FCD_DATE = "" Then
            pParms(7).Value = System.DBNull.Value
        Else
            pParms(7).Value = p_FCD_DATE
        End If
        pParms(8) = New SqlClient.SqlParameter("@FCD_STATUS", SqlDbType.Int)
        pParms(8).Value = p_FCD_STATUS
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue

        pParms(10) = New SqlClient.SqlParameter("@FCD_VHH_DOCNO", SqlDbType.Int)
        pParms(10).Value = p_FCD_VHH_DOCNO
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D_ONLINE", pParms)

        F_SaveFEECOLLSUB_D_ONLINE = pParms(9).Value
    End Function


    Public Shared Function F_SaveFEECOLLSPLIT_ONLINE(ByVal p_FCC_ID As Integer, ByVal p_FCC_FCL_ID As Integer, _
    ByVal p_FCC_REF_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '      EXEC	@return_value = [FEES].[F_SaveFEECOLLSPLIT_ONLINE]
        '@FCC_ID = 0,
        '@FCC_FCL_ID = 12,
        '@FCC_REF_ID = 2332
        pParms(0) = New SqlClient.SqlParameter("@FCC_ID", SqlDbType.Int)
        pParms(0).Value = p_FCC_ID
        pParms(1) = New SqlClient.SqlParameter("@FCC_FCL_ID", SqlDbType.Int)
        pParms(1).Value = p_FCC_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCC_REF_ID", SqlDbType.Int)
        pParms(2).Value = p_FCC_REF_ID

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSPLIT_ONLINE", pParms)

        F_SaveFEECOLLSPLIT_ONLINE = pParms(3).Value
    End Function


    Public Shared Function Update_FEECOLLECTION_H_ONLINE_ID(ByVal p_FCL_ID As String, _
    ByVal p_FCL_ONLINE_ID As String, ByVal p_MODE As String) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '  EXEC	@return_value = [FEES].[Update_FEECOLLECTION_H_ONLINE_ID]
        '@ID INT,  
        '@FCL_ONLINE_ID VARCHAR(50),
        '@MODE
        pParms(0) = New SqlClient.SqlParameter("@ID", SqlDbType.VarChar, 50)
        pParms(0).Value = p_FCL_ID
        pParms(1) = New SqlClient.SqlParameter("@FCL_ONLINE_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = p_FCL_ONLINE_ID
        pParms(2) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar, 30)
        pParms(2).Value = p_MODE
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
        CommandType.StoredProcedure, "FEES.Update_FEECOLLECTION_H_ONLINE_ID", pParms)

        Update_FEECOLLECTION_H_ONLINE_ID = pParms(3).Value
    End Function

End Class
