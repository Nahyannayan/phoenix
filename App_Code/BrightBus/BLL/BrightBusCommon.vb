Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class BrightBusCommon


    Public Shared Function SERVICES_BSU_M_ALL() As DataTable
      
        Dim sql_query As String = " SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID, BUSINESSUNIT_M.BSU_NAME" _
            & " FROM vw_oso_SERVICES_BSU_M INNER JOIN BUSINESSUNIT_M " _
            & " ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID" _
            & " WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID = '900500') AND" _
            & " (vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1) "


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function Get_BSU_Currency(ByVal p_BSU_ID As String) As String

        Dim sql_query As String = " SELECT  BSU_CURRENCY  " _
        & " FROM  BUSINESSUNIT_M WHERE BSU_ID='" & p_BSU_ID & "'"

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function
    Public Shared Function TermOrMonth(ByVal p_BSU_ID As String) As Boolean
        Dim sql_query As String = " SELECT  isnull(BSU_FEE_BMonthly,0)  " _
        & " FROM  BUSINESSUNIT_M WHERE BSU_ID='" & p_BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0)
        Else
            Return False
        End If
    End Function

    'Public Shared Function getUserInfo() As DataSet
    '    Dim ds As DataSet = New DataSet()
    '    Using Connection As SqlConnection = ConnectionManger.GetOASISConnection()
    '        Dim sqlstring As String = "select users_m.usr_id as usr_id,users_m.usr_name as usr_name ,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') as Full_Name,users_m.usr_emp_id,users_m.usr_rol_id, roles_m.ROL_DESCR as DESCR  , businessunit_m.BSU_NAME as BSU_NAME , users_m.usr_bsu_id from users_m join businessunit_m  on users_m.usr_bsu_id=businessunit_m.BSU_ID join roles_m on users_m.usr_rol_id=roles_m.ROL_ID left outer join vw_OSO_EMPLOYEE_M on users_m.usr_emp_id=vw_OSO_EMPLOYEE_M.Emp_ID order by  DESCR"
    '        Dim command As SqlCommand = New SqlCommand(sqlstring, Connection)
    '        command.CommandType = CommandType.Text
    '        Dim adapter As SqlDataAdapter = New SqlDataAdapter(command)
    '        adapter.Fill(ds, "users_m")
    '        SqlConnection.ClearPool(Connection)
    '    End Using
    '    Return ds

    'End Function

    Public Shared Function GetUserDetailsBB(ByVal p_STU_NO As String, ByVal p_STU_PASSWORD As String, _
        ByVal p_AUD_HOST As String, ByVal p_AUD_WINUSER As String, ByVal p_STU_BSU_ID As String, _
        ByRef dtUserData As DataTable, ByVal stTrans As SqlTransaction) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        'DECLARE	@return_value int

        'EXEC	@return_value = [FEES].[GetUserDetailsBB]
        '		@STU_NO = N'11100100005134',
        '		@STU_PASSWORD = N'123',
        '		@AUD_HOST = N'WWW',
        '		@AUD_WINUSER = N'WSE',
        '		@STU_BSU_ID='111001'
        Try

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(0).Value = p_STU_NO
            pParms(1) = New SqlClient.SqlParameter("@STU_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_STU_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_STU_BSU_ID
            pParms(3) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar, 100)
            pParms(3).Value = p_AUD_HOST
            pParms(4) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(4).Value = p_AUD_WINUSER
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue 

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "FEES.GetUserDetailsBB", pParms)
            'retval = SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "GetUserDetails", pParms)
            If pParms(5).Value = "0" Then
                dtUserData = ds.Tables(0) 
            End If 
            GetUserDetailsBB = pParms(5).Value 
        Catch ex As Exception
            Errorlog(ex.Message)
            GetUserDetailsBB = "1000" 
        End Try




    End Function

End Class
