Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class FeeCollectionTransport

#Region "Public Variables"

    Private pBSU_ID As String
    Public Property BSU_ID() As String
        Get
            Return pBSU_ID
        End Get
        Set(ByVal value As String)
            pBSU_ID = value
        End Set
    End Property

    Private pSTU_ID As Long
    Public Property STU_ID() As Long
        Get
            Return pSTU_ID
        End Get
        Set(ByVal value As Long)
            pSTU_ID = value
        End Set
    End Property

    Private pSTU_NO As String
    Public Property STU_NO() As String
        Get
            Return pSTU_NO
        End Get
        Set(ByVal value As String)
            pSTU_NO = value
        End Set
    End Property

    Private pSTU_NAME As String
    Public Property STU_NAME() As String
        Get
            Return pSTU_NAME
        End Get
        Set(ByVal value As String)
            pSTU_NAME = value
        End Set
    End Property

    Private pGRADE As String
    Public Property GRADE() As String
        Get
            Return pGRADE
        End Get
        Set(ByVal value As String)
            pGRADE = value
        End Set
    End Property

    Private pSECTION As String
    Public Property SECTION() As String
        Get
            Return pSECTION
        End Get
        Set(ByVal value As String)
            pSECTION = value
        End Set
    End Property

    Private pSTATUS As String
    Public Property STATUS() As String
        Get
            Return pSTATUS
        End Get
        Set(ByVal value As String)
            pSTATUS = value
        End Set
    End Property

    Private pASON_DATE As String
    Public Property ASON_DATE() As String
        Get
            Return pASON_DATE
        End Get
        Set(ByVal value As String)
            pASON_DATE = value
        End Set
    End Property

    Private pUSER_NAME As String
    Public Property USER_NAME() As String
        Get
            Return pUSER_NAME
        End Get
        Set(ByVal value As String)
            pUSER_NAME = value
        End Set
    End Property

    Private pSTU_LASTATTDATE As String
    Public Property STU_LASTATTDATE() As String
        Get
            Return pSTU_LASTATTDATE
        End Get
        Set(ByVal value As String)
            pSTU_LASTATTDATE = value
        End Set
    End Property

    Private pSTU_CANCELDATE As String
    Public Property STU_CANCELDATE() As String
        Get
            Return pSTU_CANCELDATE
        End Get
        Set(ByVal value As String)
            pSTU_CANCELDATE = value
        End Set
    End Property

    Private pSTU_DOJ As String
    Public Property STU_DOJ() As String
        Get
            Return pSTU_DOJ
        End Get
        Set(ByVal value As String)
            pSTU_DOJ = value
        End Set
    End Property

    Private pSTU_CURRSTATUS As String
    Public Property STU_CURRSTATUS() As String
        Get
            Return pSTU_CURRSTATUS
        End Get
        Set(ByVal value As String)
            pSTU_CURRSTATUS = value
        End Set
    End Property

#End Region

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE(ByVal p_FCL_ID As Integer, ByVal p_FCL_SOURCE As String, _
      ByVal p_FCL_DATE As DateTime, ByVal p_FCL_RECNO As String, ByVal p_FCL_ACD_ID As Integer, ByVal p_FCL_STU_ID As Integer, _
      ByVal p_FCL_AMOUNT As Decimal, ByVal p_FCL_Bposted As Boolean, _
      ByRef p_newFCL_ID As Long, ByVal p_FCL_BSU_ID As String, ByVal p_FCL_NARRATION As String, _
      ByRef p_NEW_FCL_RECNO As String, ByVal p_FCL_OUTSTANDING As String, _
      ByVal p_FCL_STU_BSU_ID As String, ByVal p_FCL_REFNO As String, ByVal p_FCL_SBL_ID As String, _
      ByVal p_FCL_SCH_LIST As String, ByVal p_FCL_SCH_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(21) As SqlClient.SqlParameter

        'EXEC	@return_value = [FEES].[F_SaveFEECOLLECTION_H]
        '@FCL_ID = 0 ,
        '@FCL_SOURCE = N'COUNTER1',
        '@FCL_RECNO = N'VHH123',
        '@FCL_DATE = N'12-MAY-2008',
        '@FCL_ACD_ID = 79,

        pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.Int)
        pParms(0).Value = p_FCL_ID
        pParms(1) = New SqlClient.SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCL_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCL_RECNO
        pParms(3) = New SqlClient.SqlParameter("@FCL_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCL_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCL_ACD_ID", SqlDbType.VarChar, 100)
        pParms(4).Value = p_FCL_ACD_ID
        '@FCL_STU_ID = 52001,
        '@FCL_AMOUNT = 1200,
        '@FCL_EMP_ID = 122,

        pParms(5) = New SqlClient.SqlParameter("@FCL_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCL_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCL_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCL_AMOUNT

        pParms(7) = New SqlClient.SqlParameter("@FCL_Bposted", SqlDbType.Bit)
        pParms(7).Value = p_FCL_Bposted

        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        '@FCL_Bposted = 0
        '@NEW_FCL_ID
        '@FCL_NARRATION VARCHAR(100) , 
        '@FCL_DRCR VARCHAR(5)

        pParms(9) = New SqlClient.SqlParameter("@NEW_FCL_ID", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.Output
        pParms(10) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 100)
        pParms(10).Value = p_FCL_BSU_ID
        pParms(11) = New SqlClient.SqlParameter("@FCL_NARRATION", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCL_NARRATION
        pParms(12) = New SqlClient.SqlParameter("@NEW_FCL_RECNO", SqlDbType.VarChar, 30)
        pParms(12).Direction = ParameterDirection.Output
        '@FCL_OUTSTANDING NUMERIC(18,3), 
        '@FCL_STU_BSU_ID        
        '@FCL_REFNO varchar(20), 
        '@FCL_SBL_ID INT,
        pParms(13) = New SqlClient.SqlParameter("@FCL_OUTSTANDING", SqlDbType.Decimal)
        pParms(13).Value = p_FCL_OUTSTANDING
        pParms(14) = New SqlClient.SqlParameter("@FCL_STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(14).Value = p_FCL_STU_BSU_ID
        pParms(15) = New SqlClient.SqlParameter("@FCL_REFNO", SqlDbType.VarChar, 20)
        pParms(15).Value = p_FCL_REFNO
        pParms(16) = New SqlClient.SqlParameter("@FCL_SBL_ID", SqlDbType.Int)
        pParms(16).Value = p_FCL_SBL_ID

        '@FCL_SCH_LIST  INT,
        '@FCL_SCH_ID ,
        pParms(17) = New SqlClient.SqlParameter("@FCL_SCH_LIST", SqlDbType.VarChar, 100)
        pParms(17).Value = p_FCL_SCH_LIST
        pParms(18) = New SqlClient.SqlParameter("@FCL_SCH_ID", SqlDbType.Int)
        pParms(18).Value = p_FCL_SCH_ID

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H_ONLINE", pParms)
        F_SaveFEECOLLECTION_H_ONLINE = pParms(8).Value
        If pParms(8).Value = 0 Then
            p_newFCL_ID = pParms(9).Value
            p_NEW_FCL_RECNO = pParms(12).Value
        End If

    End Function

    Public Shared Function F_SaveFEECOLLSUB_ONLINE(ByVal p_FCS_ID As Integer, ByVal p_FCS_FCL_ID As Integer, _
    ByVal p_FCS_FEE_ID As Integer, ByVal p_FCS_AMOUNT As Decimal, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEECOLLSUB]
        '@FCS_ID = 0,
        '@FCS_FCL_ID = 1,
        '@FCS_FEE_ID = 3,
        '@FCS_AMOUNT = 500

        pParms(0) = New SqlClient.SqlParameter("@FCS_ID", SqlDbType.Int)
        pParms(0).Value = p_FCS_ID
        pParms(1) = New SqlClient.SqlParameter("@FCS_FCL_ID", SqlDbType.Int)
        pParms(1).Value = p_FCS_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCS_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FCS_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FCS_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCS_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_ONLINE", pParms)

        F_SaveFEECOLLSUB_ONLINE = pParms(4).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_D_ONLINE(ByVal p_FCD_ID As Integer, ByVal p_FCD_FCL_ID As String, _
        ByVal p_FCD_CLT_ID As Integer, ByVal p_FCD_AMOUNT As Decimal, ByVal p_FCD_REFNO As String, _
        ByVal p_FCD_REF_ID As String, ByVal p_FCD_EMR_ID As String, ByVal p_FCD_DATE As String, _
        ByVal p_FCD_STATUS As String, ByVal p_FCD_VHH_DOCNO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter

        '        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '       @FCD_ID INT, 
        '@FCD_FCL_ID INT,
        '@FCD_CLT_ID INT, 
        '@FCD_AMOUNT NUMERIC(18,3), 
        '@FCD_REFNO VARCHAR(20), 
        '@FCD_DATE DATETIME, 
        '@FCD_STATUS INT, 
        '@FCD_VHH_DOCNO VARCHAR(20),
        '@FCD_REF_ID VARCHAR(20), 
        '@FCD_EMR_ID VARCHAR(20) 
        pParms(0) = New SqlClient.SqlParameter("@FCD_ID", SqlDbType.Int)
        pParms(0).Value = p_FCD_ID
        pParms(1) = New SqlClient.SqlParameter("@FCD_FCL_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FCD_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCD_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FCD_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCD_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FCD_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FCD_REFNO

        pParms(5) = New SqlClient.SqlParameter("@FCD_REF_ID", SqlDbType.VarChar, 20)
        pParms(5).Value = p_FCD_REF_ID
        pParms(6) = New SqlClient.SqlParameter("@FCD_EMR_ID", SqlDbType.VarChar, 20)
        pParms(6).Value = p_FCD_EMR_ID

        pParms(7) = New SqlClient.SqlParameter("@FCD_DATE", SqlDbType.DateTime)
        If p_FCD_DATE = "" Then
            pParms(7).Value = System.DBNull.Value
        Else
            pParms(7).Value = p_FCD_DATE
        End If
        pParms(8) = New SqlClient.SqlParameter("@FCD_STATUS", SqlDbType.Int)
        pParms(8).Value = p_FCD_STATUS
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue

        pParms(10) = New SqlClient.SqlParameter("@FCD_VHH_DOCNO", SqlDbType.Int)
        pParms(10).Value = p_FCD_VHH_DOCNO
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D_ONLINE", pParms)

        F_SaveFEECOLLSUB_D_ONLINE = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLSPLIT_ONLINE(ByVal p_FCC_ID As Integer, ByVal p_FCC_FCL_ID As Integer, _
    ByVal p_FCC_REF_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '      EXEC	@return_value = [FEES].[F_SaveFEECOLLSPLIT_ONLINE]
        '@FCC_ID = 0,
        '@FCC_FCL_ID = 12,
        '@FCC_REF_ID = 2332
        pParms(0) = New SqlClient.SqlParameter("@FCC_ID", SqlDbType.Int)
        pParms(0).Value = p_FCC_ID
        pParms(1) = New SqlClient.SqlParameter("@FCC_FCL_ID", SqlDbType.Int)
        pParms(1).Value = p_FCC_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCC_REF_ID", SqlDbType.Int)
        pParms(2).Value = p_FCC_REF_ID

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSPLIT_ONLINE", pParms)

        F_SaveFEECOLLSPLIT_ONLINE = pParms(3).Value
    End Function

    Public Shared Function Update_FEECOLLECTION_H_ONLINE_ID(ByVal p_FCL_ID As String, _
    ByVal p_FCL_ONLINE_ID As String, ByVal p_MODE As String) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '  EXEC	@return_value = [FEES].[Update_FEECOLLECTION_H_ONLINE_ID]
        '@ID INT,  
        '@FCL_ONLINE_ID VARCHAR(50),
        '@MODE
        pParms(0) = New SqlClient.SqlParameter("@ID", SqlDbType.VarChar, 50)
        pParms(0).Value = p_FCL_ID
        pParms(1) = New SqlClient.SqlParameter("@FCL_ONLINE_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = p_FCL_ONLINE_ID
        pParms(2) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar, 30)
        pParms(2).Value = p_MODE
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
        CommandType.StoredProcedure, "FEES.Update_FEECOLLECTION_H_ONLINE_ID", pParms)

        Update_FEECOLLECTION_H_ONLINE_ID = pParms(3).Value
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_TRANSPORT(ByVal p_FCL_ID As Integer, ByVal p_FCL_SOURCE As String, _
    ByVal p_FCL_DATE As DateTime, ByVal p_FCL_RECNO As String, ByVal p_FCL_ACD_ID As Integer, ByVal p_FCL_STU_ID As Integer, _
    ByVal p_FCL_AMOUNT As Decimal, ByVal p_FCL_EMP_ID As String, ByVal p_FCL_Bposted As Boolean, _
    ByRef p_newFCL_ID As Long, ByVal p_FCL_BSU_ID As String, ByVal p_FCL_NARRATION As String, _
    ByVal p_FCL_DRCR As String, ByRef p_NEW_FCL_RECNO As String, ByVal p_FCL_OUTSTANDING As String, _
    ByVal p_FCL_STU_BSU_ID As String, ByVal p_FCL_SBL_ID As String, ByVal p_FCL_STU_TYPE As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(23) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEECOLLECTION_H]
        '@FCL_ID = 0 ,
        '@FCL_SOURCE = N'COUNTER1',
        '@FCL_RECNO = N'VHH123',
        '@FCL_DATE = N'12-MAY-2008',
        '@FCL_ACD_ID = 79,
        pParms(0) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCL_ID
        pParms(1) = New SqlClient.SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCL_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCL_RECNO
        pParms(3) = New SqlClient.SqlParameter("@FCL_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCL_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCL_ACD_ID", SqlDbType.VarChar, 100)
        pParms(4).Value = p_FCL_ACD_ID
        '@FCL_STU_ID = 52001,
        '@FCL_AMOUNT = 1200,
        '@FCL_EMP_ID = 122,
        pParms(5) = New SqlClient.SqlParameter("@FCL_STU_ID", SqlDbType.BigInt)
        pParms(5).Value = p_FCL_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCL_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCL_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCL_EMP_ID", SqlDbType.VarChar, 100)
        pParms(7).Value = p_FCL_EMP_ID
        pParms(8) = New SqlClient.SqlParameter("@FCL_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCL_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        '@FCL_Bposted = 0
        '@NEW_FCL_ID
        '@FCL_NARRATION VARCHAR(100) , 
        '@FCL_DRCR VARCHAR(5)
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCL_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCL_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCL_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FCL_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCL_DRCR", SqlDbType.VarChar, 5)
        pParms(13).Value = p_FCL_DRCR
        pParms(14) = New SqlClient.SqlParameter("@NEW_FCL_RECNO", SqlDbType.VarChar, 30)
        pParms(14).Direction = ParameterDirection.Output
        '@FCL_OUTSTANDING NUMERIC(18,3), 
        '@FCL_STU_BSU_ID        
        '@FCL_REFNO varchar(20), 
        '@FCL_SBL_ID INT,
        pParms(15) = New SqlClient.SqlParameter("@FCL_OUTSTANDING", SqlDbType.Decimal)
        pParms(15).Value = p_FCL_OUTSTANDING
        pParms(16) = New SqlClient.SqlParameter("@FCL_STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(16).Value = p_FCL_STU_BSU_ID
        pParms(17) = New SqlClient.SqlParameter("@FCL_REFNO", SqlDbType.VarChar, 20)
        pParms(17).Value = System.DBNull.Value
        pParms(18) = New SqlClient.SqlParameter("@FCL_SBL_ID", SqlDbType.VarChar)
        pParms(18).Value = p_FCL_SBL_ID
        '@FCL_SCH_LIST  INT,
        '@FCL_SCH_ID ,
        '@FCL_CONC_AMOUNT NUMERIC(18,3) ,
        '@FCL_CONC_EMPDESCR  VARCHAR(200) ,
        pParms(19) = New SqlClient.SqlParameter("@FCL_SCH_LIST", SqlDbType.VarChar, 200)
        pParms(19).Value = System.DBNull.Value
        pParms(20) = New SqlClient.SqlParameter("@FCL_SCH_ID", SqlDbType.Int)
        pParms(20).Value = System.DBNull.Value
        pParms(21) = New SqlClient.SqlParameter("@FCL_STU_TYPE", SqlDbType.VarChar, 5)
        pParms(21).Value = p_FCL_STU_TYPE
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H", pParms)
        If pParms(9).Value = 0 Then
            p_newFCL_ID = pParms(10).Value
            p_NEW_FCL_RECNO = pParms(14).Value
        End If
        F_SaveFEECOLLECTION_H_TRANSPORT = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_TRANSPORT(ByVal p_FCS_ID As Integer, ByVal p_FCS_FCL_ID As Integer, _
    ByVal p_FCS_FEE_ID As Integer, ByVal p_FCS_AMOUNT As Decimal, ByVal bChargeandPost As Boolean, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        'EXEC	@return_value = [FEES].[F_SaveFEECOLLSUB]
        '@FCS_ID = 0,
        '@FCS_FCL_ID = 1,
        '@FCS_FEE_ID = 3,
        '@FCS_AMOUNT = 500
        pParms(0) = New SqlClient.SqlParameter("@FCS_ID", SqlDbType.Int)
        pParms(0).Value = p_FCS_ID
        pParms(1) = New SqlClient.SqlParameter("@FCS_FCL_ID", SqlDbType.Int)
        pParms(1).Value = p_FCS_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCS_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FCS_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FCS_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCS_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@bChargeandPost", SqlDbType.Bit)
        pParms(5).Value = bChargeandPost
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB", pParms)
        F_SaveFEECOLLSUB_TRANSPORT = pParms(4).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_D_TRANSPORT(ByVal p_FCD_ID As Integer, ByVal p_FCD_FCL_ID As String, _
        ByVal p_FCD_CLT_ID As Integer, ByVal p_FCD_AMOUNT As String, ByVal p_FCD_REFNO As String, _
         ByVal p_FCD_DATE As String, ByVal p_FCD_STATUS As String, ByVal p_FCD_VHH_DOCNO As String, _
        ByVal p_FCD_REF_ID As String, ByVal p_FCD_EMR_ID As String, ByVal p_FCD_CCM_ID As Integer, _
        ByVal p_stTrans As SqlTransaction, Optional ByVal p_FCD_CHARGE_CLIENT As Double = 0, _
        Optional ByVal p_FCD_BANK_ACT_ID As String = "") As String
        Dim pParms(13) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCD_ID", SqlDbType.Int)
        pParms(0).Value = p_FCD_ID
        pParms(1) = New SqlClient.SqlParameter("@FCD_FCL_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FCD_FCL_ID
        pParms(2) = New SqlClient.SqlParameter("@FCD_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FCD_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FCD_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FCD_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FCD_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FCD_REFNO
        pParms(5) = New SqlClient.SqlParameter("@FCD_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FCD_DATE
        pParms(6) = New SqlClient.SqlParameter("@FCD_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_FCD_STATUS
        pParms(7) = New SqlClient.SqlParameter("@FCD_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCD_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@FCD_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_FCD_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@FCD_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FCD_EMR_ID
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        pParms(11) = New SqlClient.SqlParameter("@FCD_CCM_ID", SqlDbType.Int)
        If p_FCD_CCM_ID = 0 Then
            pParms(11).Value = System.DBNull.Value
        Else
            pParms(11).Value = p_FCD_CCM_ID
        End If
        pParms(12) = New SqlClient.SqlParameter("@FCD_CHARGE_CLIENT", SqlDbType.Decimal)
        pParms(12).Value = p_FCD_CHARGE_CLIENT
        pParms(13) = New SqlClient.SqlParameter("@FCD_BANK_ACT_ID", SqlDbType.VarChar, 20)
        pParms(13).Value = p_FCD_BANK_ACT_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D", pParms)
        F_SaveFEECOLLSUB_D_TRANSPORT = pParms(10).Value
    End Function

    Public Shared Function F_SaveFEECOLLALLOCATION_D_TRANSPORT(ByVal p_BSU_ID As String, ByVal p_FCL_ID As String, _
    ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(3) As SqlClient.SqlParameter
        '     EXEC	@return_value = [FEES].[F_SaveFEECOLLALLOCATION_D] 
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
        pParms(2).Value = p_FCL_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLALLOCATION_D", pParms)
        F_SaveFEECOLLALLOCATION_D_TRANSPORT = pParms(1).Value
    End Function

    Public Shared Function F_GetFeeDetailsForCollectionTransport(ByVal p_DOCDT As String, ByVal p_STU_ID As String, _
    ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal ACD_ID As String, Optional ByVal CollectionSource As String = "COUNTER") As DataTable
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(4).Value = ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@COLLECTION_SOURCE", SqlDbType.VarChar, 20) '---new parameter added by Jacob on 23/Ag/2020 to differentiate online and counter collections
        pParms(5).Value = CollectionSource
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeDetailsForCollectionTransport", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_SettleFee_Transport(ByVal p_BSU_ID As String, _
            ByVal p_Dt As String, ByVal p_STU_ID As String, _
            ByVal p_STU_BSU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        'procedure [FEES].[F_SettleFee](          
        '@BSU_ID varchar(20)='125016', 
        '@STU_ID bigint=93690,@Dt datetime)
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = p_Dt
        pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_STU_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = p_STU_BSU_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SettleFee", pParms)
        F_SettleFee_Transport = pParms(3).Value
    End Function

    Public Shared Function F_SAVECHEQUEDATA_Transport(ByVal p_BSU_ID As String, ByVal p_FCL_IDs As String, _
                ByVal p_STU_BSU_ID As String, ByVal p_FCL_EMP_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(5) As SqlClient.SqlParameter
        '   EXEC [FEES].[F_SAVECHEQUEDATA]
        '@BSU_ID  ='900500' ,
        '@FCL_IDS  ='1|2|3|345|4456777',
        '@STU_BSU_ID   ='125016'
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_STU_BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@FCL_IDs", SqlDbType.VarChar, 2000)
        pParms(3).Value = p_FCL_IDs
        pParms(4) = New SqlClient.SqlParameter("@FCL_EMP_ID", SqlDbType.VarChar, 50)
        pParms(4).Value = p_FCL_EMP_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SAVECHEQUEDATA", pParms)
        F_SAVECHEQUEDATA_Transport = pParms(1).Value
    End Function

    Public Shared Function F_GetFeeNarration(ByVal p_DOCDT As String, _
        ByVal p_ACD_ID As String, ByVal p_BSU_ID As String) As String
        '[FEES].[F_GetFeeNarration] 
        '@DOCDT varchar(12),
        '@BSU_ID varchar(20),
        '@ACD_ID bigint  
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function PrintReceipt(ByVal p_Receiptno As String, ByVal p_User As String, _
    ByVal p_Stu_Bsu_id As String, ByVal p_BSu_id As String) As MyReportClass
        Dim str_Sql, strFilter As String
        strFilter = "  FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & p_BSu_id & "' "
        str_Sql = "select * FROM FEES.VW_OSO_FEES_RECEIPT WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        ' Dim ds As New DataSet
        Dim repSource As New MyReportClass

        cmd.Connection = New SqlConnection(str_conn)
        Dim params As New Hashtable

        params("UserName") = p_User
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        '''''''''''''''
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(2) = New MyReportClass

        ''SUBREPORT1
        Dim cmdSubColln As New SqlCommand
        cmdSubColln.CommandText = "select * FROM FEES.VW_OSO_FEES_FEECOLLECTED where " & strFilter
        cmdSubColln.Connection = New SqlConnection(str_conn)
        cmdSubColln.CommandType = CommandType.Text
        repSourceSubRep(1).Command = cmdSubColln

        Dim cmdSubEarn As New SqlCommand
        cmdSubEarn.CommandText = "select * FROM FEES.VW_OSO_FEES_PAYMENTS where " & strFilter
        cmdSubEarn.Connection = New SqlConnection(str_conn)
        cmdSubEarn.CommandType = CommandType.Text
        repSourceSubRep(2).Command = cmdSubEarn
        repSource.SubReport = repSourceSubRep

        Dim cmdHeader As New SqlCommand("getBsuInFoWithImage_Provider", New SqlConnection(ConnectionManger.GetOASISConnectionString))
        cmdHeader.CommandType = CommandType.StoredProcedure
        cmdHeader.Parameters.AddWithValue("@IMG_STU_BSU_ID", p_Stu_Bsu_id)
        cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
        repSourceSubRep(0).Command = cmdHeader
        Select Case p_BSu_id
            Case OASISConstants.TransportBSU_BrightBus
                repSource.ResourceName = "../../fees/Reports/RPT/rptFeeReceipt.rpt"
                cmdHeader.Parameters.AddWithValue("@IMG_PROVIDER_BSU_ID", OASISConstants.TransportBSU_BrightBus)
            Case OASISConstants.TransportBSU_Sts
                repSource.ResourceName = "../../fees/Reports/RPT/rptFeeReceipt_sts.rpt"
                cmdHeader.Parameters.AddWithValue("@IMG_PROVIDER_BSU_ID", OASISConstants.TransportBSU_Sts)
        End Select
        Return repSource
    End Function

    Public Sub GET_STUDENT_AUDIT_DETAIL()
        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        Try
            str_query = "SELECT  STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                               & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_SHF_ID, " _
                               & " CASE WHEN (STU_LEAVEDATE IS NULL OR CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE())) " _
                               & " THEN CASE STU_CURRSTATUS WHEN 'EN' THEN 'Active' WHEN 'CN' THEN 'Cancelled Admission' END " _
                               & " ELSE CASE TCM_TCSO WHEN 'TC' THEN 'TC' WHEN 'SO' THEN 'Strike Off' END END AS STATUS,A.STU_LASTATTDATE,A.STU_CURRSTATUS," _
                               & " STU_CANCELDATE,CONVERT(DATETIME,STU_DOJ)STU_DOJ" _
                               & " FROM STUDENT_M AS A WITH(NOLOCK) INNER JOIN VV_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                               & " INNER JOIN VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                               & " LEFT OUTER JOIN OASIS..TCM_M AS D WITH(NOLOCK) ON A.STU_ID=D.TCM_STU_ID AND TCM_CANCELDATE IS NULL" _
                               & " WHERE STU_ID = " & STU_ID

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                STU_NO = ds.Tables(0).Rows(0)("STU_NO").ToString
                STU_NAME = ds.Tables(0).Rows(0)("STU_NAME").ToString
                GRADE = ds.Tables(0).Rows(0)("GRM_DISPLAY").ToString
                SECTION = ds.Tables(0).Rows(0)("SCT_DESCR").ToString
                STATUS = ds.Tables(0).Rows(0)("STATUS").ToString

                If IsDate(ds.Tables(0).Rows(0)("STU_LASTATTDATE").ToString) Then
                    STU_LASTATTDATE = Format(CDate(ds.Tables(0).Rows(0)("STU_LASTATTDATE")), "dd/MMM/yyyy")
                End If
                If IsDate(ds.Tables(0).Rows(0)("STU_CANCELDATE").ToString) Then
                    STU_CANCELDATE = Format(CDate(ds.Tables(0).Rows(0)("STU_CANCELDATE")), "dd/MMM/yyyy")
                End If
                If IsDate(ds.Tables(0).Rows(0)("STU_DOJ").ToString) Then
                    STU_DOJ = Format(CDate(ds.Tables(0).Rows(0)("STU_DOJ")), "dd/MMM/yyyy")
                End If
                STU_CURRSTATUS = ds.Tables(0).Rows(0)("STU_CURRSTATUS").ToString
            Else
                STU_NO = ""
                STU_NAME = ""
                GRADE = ""
                SECTION = ""
                STATUS = ""
                STU_LASTATTDATE = ""
                STU_CURRSTATUS = ""
                STU_CANCELDATE = ""
                STU_DOJ = ""
            End If
        Catch ex As Exception
            STU_NO = ""
            STU_NAME = ""
            GRADE = ""
            SECTION = ""
            STATUS = ""
            STU_LASTATTDATE = Nothing
            STU_CURRSTATUS = ""
            STU_CANCELDATE = Nothing
            STU_DOJ = Nothing
        End Try

    End Sub

End Class
