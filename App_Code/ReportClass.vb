﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class ReportClass
    Inherits CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Sub New()

    End Sub
    Dim resName As String
    Dim voucher As String
    Dim param As Hashtable
    Dim Formula As Hashtable
    Dim subRep As ReportClass()
    Dim lDisplayGroupTree As Boolean = False
    Dim bIncludeBSUImage As Boolean
    Dim strHeaderBSUID As String
    Dim strHeaderReportName As String
    Dim DTReportData As New DataTable

    Public Property DisplayGroupTree() As Boolean
        Get
            Return lDisplayGroupTree
        End Get
        Set(ByVal value As Boolean)
            lDisplayGroupTree = value
        End Set
    End Property
    Public Property IncludeBSUImage() As Boolean
        Get
            Return bIncludeBSUImage
        End Get
        Set(ByVal value As Boolean)
            bIncludeBSUImage = value
        End Set
    End Property

    Public Property SubReport() As ReportClass()
        Get
            Return subRep
        End Get
        Set(ByVal value As ReportClass())
            subRep = value
        End Set
    End Property
    Public Property Parameter() As Hashtable
        Get
            Return param
        End Get
        Set(ByVal value As Hashtable)
            If param Is Nothing Then
                param = New Hashtable()
            End If
            param = value
        End Set
    End Property
    Public Property Formulas() As Hashtable
        Get
            Return Formula
        End Get
        Set(ByVal value As Hashtable)
            If Formula Is Nothing Then
                Formula = New Hashtable()
            End If
            Formula = value
        End Set
    End Property
    Public Overrides Property ResourceName() As String
        Get
            Return resName
        End Get
        Set(ByVal value As [String])
            resName = value
        End Set
    End Property

    Public Property HeaderBSUID() As String
        Get
            Return strHeaderBSUID
        End Get
        Set(ByVal value As [String])
            strHeaderBSUID = value
        End Set
    End Property
    Public Property HeaderReportName() As String
        Get
            Return strHeaderReportName
        End Get
        Set(ByVal value As [String])
            strHeaderReportName = value
        End Set
    End Property
    Public Property ReportData() As DataTable
        Get
            Return DTReportData
        End Get
        Set(ByVal value As DataTable)
            DTReportData = value
        End Set
    End Property
End Class


