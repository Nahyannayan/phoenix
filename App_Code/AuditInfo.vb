﻿Imports Microsoft.VisualBasic

Public Class AuditInfo


    ''' <summary>
    ''' UNIQUE CLASS ID
    ''' </summary>
    ''' <value></value>
    ''' <returns>STRING</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ClientBrw() As String
        Get
            Dim _ClientBrw As String = String.Empty
            With HttpContext.Current.Request.Browser
                _ClientBrw &= "Browser Capabilities" & vbCrLf
                _ClientBrw &= "Type = " & .Type & vbCrLf
                _ClientBrw &= "Name = " & .Browser & vbCrLf
                _ClientBrw &= "Version = " & .Version & vbCrLf
                _ClientBrw &= "Major Version = " & .MajorVersion & vbCrLf
                _ClientBrw &= "Minor Version = " & .MinorVersion & vbCrLf
                _ClientBrw &= "Platform = " & .Platform & vbCrLf
                _ClientBrw &= "Is Beta = " & .Beta & vbCrLf
                _ClientBrw &= "Is Crawler = " & .Crawler & vbCrLf
                _ClientBrw &= "Is AOL = " & .AOL & vbCrLf
                _ClientBrw &= "Is Win16 = " & .Win16 & vbCrLf
                _ClientBrw &= "Is Win32 = " & .Win32 & vbCrLf
                _ClientBrw &= "Supports Frames = " & .Frames & vbCrLf
                _ClientBrw &= "Supports Tables = " & .Tables & vbCrLf
                _ClientBrw &= "Supports Cookies = " & .Cookies & vbCrLf
                _ClientBrw &= "Supports VBScript = " & .VBScript & vbCrLf
                _ClientBrw &= "Supports JavaScript = " & .EcmaScriptVersion.ToString() & vbCrLf
                _ClientBrw &= "Supports Java Applets = " & .JavaApplets & vbCrLf
                _ClientBrw &= "Supports ActiveX Controls = " & .ActiveXControls & _
                    vbCrLf
                _ClientBrw &= "Supports JavaScript Version = " & _
                .JScriptVersion.ToString & vbCrLf
            End With
            Return _ClientBrw
        End Get

    End Property
    Public Shared ReadOnly Property ClientIP() As String
        Get

            Return HttpContext.Current.Request.UserHostAddress()
        End Get

    End Property

End Class
