﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data

Public Class gps

    Public Shared Function BindVehicleTreeView(ByVal VehTreeview As TreeView, ByVal hash As Hashtable) As TreeView



        VehTreeview.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString()

        Dim param(2) As SqlClient.SqlParameter
        If hash("BSU") <> "All" Then
            param(0) = New SqlClient.SqlParameter("@BSU_ID", hash("BSU"))
        End If
        param(1) = New SqlClient.SqlParameter("@OPTION", 1)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_VEHICLES", param)

        Dim SuperParent As New TreeNode
        SuperParent.Text = "All"
        SuperParent.Value = "-1"
        VehTreeview.Nodes.Add(SuperParent)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = "<font color='Blue'><strong>" + ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString() + "</strong></font>"
                ParentNode.Value = "BSU" & ds.Tables(0).Rows(i).Item("BSU_ID").ToString()
                SuperParent.ChildNodes.Add(ParentNode)
            Next

        End If


        For Each node As TreeNode In VehTreeview.Nodes
            BindTreeVehicles(node, hash)
        Next

        If hash("EXPAND") = True Then
            VehTreeview.ExpandAll()
        Else
            VehTreeview.CollapseAll()
        End If


        Return VehTreeview

    End Function


    Private Shared Sub BindTreeVehicles(ByVal ParentNode As TreeNode, ByVal hash As Hashtable)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString()

        Dim childnodeValue = ParentNode.Value

        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@BSU_ID", childnodeValue.Replace("BSU", ""))
        param(1) = New SqlClient.SqlParameter("@OPTION", 2)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_VEHICLES", param)


        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = "<font color='RED'><strong>" + ds.Tables(0).Rows(i).Item("VEH_REGNO").ToString() + "</strong></font>"
                ChildNode.Value = ds.Tables(0).Rows(i).Item("VEH_UNITID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindTreeVehicles(node, hash)
        Next


    End Sub



End Class
