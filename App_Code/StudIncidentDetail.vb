﻿Imports Microsoft.VisualBasic

Public Class StudIncidentDetail

    Private _StudentName As String
    Private _WhatHappendNext As String
    Private _FirstAider As String
    Private _Category As String
    Private _Sub_Category As String
    Private _LOC_DESCR As String
    Private _SYMPTOMS_DESCR As String
    Private _INCIDENT_DATE As String
    Private _INJURED_AREA_DESCR As String
    Private _INCIDENT_ID As String
    Private _Notify As String
    Private _Attachment As String


    Public Property STUDENT_NAME() As String
        Get
            Return _StudentName
        End Get
        Set(ByVal value As String)
            _StudentName = value
        End Set
    End Property

    Public Property WHAHAPPENDNEXT() As String
        Get
            Return _WhatHappendNext
        End Get
        Set(ByVal value As String)
            _WhatHappendNext = value
        End Set
    End Property

    Public Property FIRSTAIDER() As String
        Get
            Return _FirstAider
        End Get
        Set(ByVal value As String)
            _FirstAider = value
        End Set
    End Property

    Public Property INCIDENT_ID() As String
        Get
            Return _INCIDENT_ID
        End Get
        Set(ByVal value As String)
            _INCIDENT_ID = value
        End Set
    End Property

    Public Property CATEGORY() As String
        Get
            Return _Category
        End Get
        Set(ByVal value As String)
            _Category = value
        End Set
    End Property

    Public Property SUB_CATEGORY() As String
        Get
            Return _Sub_Category
        End Get
        Set(ByVal value As String)
            _Sub_Category = value
        End Set
    End Property

    Public Property LOC_DESCR() As String
        Get
            Return _LOC_DESCR
        End Get
        Set(ByVal value As String)
            _LOC_DESCR = value
        End Set
    End Property


    Public Property SYMPTOMS_DESCR() As String
        Get
            Return _SYMPTOMS_DESCR
        End Get
        Set(ByVal value As String)
            _SYMPTOMS_DESCR = value
        End Set
    End Property


    Public Property INJURED_AREA_DESCR() As String
        Get
            Return _INJURED_AREA_DESCR
        End Get
        Set(ByVal value As String)
            _INJURED_AREA_DESCR = value
        End Set
    End Property

    Public Property INCIDENT_DATE() As String
        Get
            Return _INCIDENT_DATE
        End Get
        Set(ByVal value As String)
            _INCIDENT_DATE = value
        End Set
    End Property

    Public Property NOTIFY() As String
        Get
            Return _Notify
        End Get
        Set(ByVal value As String)
            _Notify = value
        End Set
    End Property


    Public Property ATTACHMENT() As String
        Get
            Return _Attachment
        End Get
        Set(ByVal value As String)
            _Attachment = value
        End Set
    End Property

End Class
