﻿Imports Microsoft.VisualBasic

Public Class CustomRadioButton
    Inherits RadioButton
    
    Private _commandArg As String
    Public Property CommandArgument() As String
        Get
            Return _commandArg
        End Get
        Set(ByVal value As String)
            _commandArg = value
        End Set
    End Property

    
End Class
