Imports Microsoft.VisualBasic
Imports System.Data.DataSet
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.StringWriter
Imports System.Web.Configuration
Imports System.Web.UI.HtmlTextWriter
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Public Class DataTables

    'Public Shared Function CreateDataTableCostcenter_delete() As DataTable
    '    Dim dtDt As DataTable
    '    dtDt = New DataTable
    '    Try
    '        Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
    '        Dim cVoucherid As New DataColumn("VoucherId", System.Type.GetType("System.String"))
    '        Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
    '        Dim cMember As New DataColumn("Memberid", System.Type.GetType("System.String"))
    '        Dim cName As New DataColumn("Name", System.Type.GetType("System.String"))
    '        Dim cERN_ID As New DataColumn("ERN_ID", System.Type.GetType("System.String"))
    '        Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
    '        Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
    '        'Dim cSubName As New DataColumn("SubName", System.Type.GetType("System.String"))
    '        Dim cSubMemberId As New DataColumn("SubMemberId", System.Type.GetType("System.String"))
    '        Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

    '        dtDt.Columns.Add(cId)
    '        dtDt.Columns.Add(cVoucherid)
    '        dtDt.Columns.Add(cCostcenter)

    '        dtDt.Columns.Add(cMember)
    '        dtDt.Columns.Add(cName)
    '        dtDt.Columns.Add(cERN_ID)
    '        dtDt.Columns.Add(cAmount)

    '        dtDt.Columns.Add(cStatus)
    '        dtDt.Columns.Add(cGuid)
    '        'dtDt.Columns.Add(cSubName)
    '        dtDt.Columns.Add(cSubMemberId)

    '        Return dtDt
    '    Catch ex As Exception
    '        Errorlog(ex.Message, "datatable")
    '        Return dtDt
    '    End Try
    'End Function

    Public Shared Function CreateDataTable_APPR() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Order", System.Type.GetType("System.Decimal"))
            Dim cUserId As New DataColumn("UserId", System.Type.GetType("System.String"))
            Dim cEmpName As New DataColumn("EmpName", System.Type.GetType("System.String"))
            Dim cLimit As New DataColumn("Limit", System.Type.GetType("System.Decimal"))

            Dim cPre As New DataColumn("Prior", System.Type.GetType("System.Boolean"))
            Dim cPost As New DataColumn("Post", System.Type.GetType("System.Boolean"))

            Dim cStatus As New DataColumn("GUID", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cUserId)
            dtDt.Columns.Add(cEmpName)
            dtDt.Columns.Add(cLimit)
            dtDt.Columns.Add(cPre)
            dtDt.Columns.Add(cPost)
            dtDt.Columns.Add(cStatus)


            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_CP() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCashflow As New DataColumn("Cashflow", System.Type.GetType("System.Decimal"))
            Dim cCashflowname As New DataColumn("Cashflowname", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Ply", System.Type.GetType("System.String"))

            Dim cExpense As New DataColumn("Expense", System.Type.GetType("System.String"))

            Dim cCCRequired As New DataColumn("CostReqd", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            Dim cCostUnit As New DataColumn("CostUnit", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.String"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.String"))
            Dim cExpAcc As New DataColumn("ExpAcc", System.Type.GetType("System.String"))
            Dim cExpAccName As New DataColumn("ExpAccName", System.Type.GetType("System.String"))
            Dim cPettycashId As New DataColumn("PettycashId", System.Type.GetType("System.String")) ' sadhu added
            Dim cPettycashName As New DataColumn("PettycashName", System.Type.GetType("System.String")) ' sadhu added
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCashflow)
            dtDt.Columns.Add(cCashflowname)
            dtDt.Columns.Add(cExpense)

            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)
            dtDt.Columns.Add(cCostUnit)
            dtDt.Columns.Add(cFromDate)
            dtDt.Columns.Add(cToDate)
            dtDt.Columns.Add(cExpAcc)
            dtDt.Columns.Add(cExpAccName)
            dtDt.Columns.Add(cPettycashId) ' sadhu added
            dtDt.Columns.Add(cPettycashName) ' sadhu added
            dtDt.Columns.Add(cTaxCode)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_DN() As DataTable
        Dim dtDt As DataTable 'CREATE THE STRUCTURE OF DETAIL TABLE
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            'Dim cCashflow As New DataColumn("Cashflow", System.Type.GetType("System.Decimal"))
            'Dim cCashflowname As New DataColumn("Cashflowname", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Ply", System.Type.GetType("System.String"))

            Dim cCCRequired As New DataColumn("CostReqd", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            'dtDt.Columns.Add(cCashflow)
            'dtDt.Columns.Add(cCashflowname)

            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)
            dtDt.Columns.Add(cTaxCode)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_CC() As DataTable ' made the jvdetail datatable structure
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCashflow As New DataColumn("Cashflow", System.Type.GetType("System.Decimal"))
            Dim cCashflowname As New DataColumn("Cashflowname", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))

            Dim cCollection As New DataColumn("Collection", System.Type.GetType("System.String"))
            Dim cCollectionCode As New DataColumn("CollectionCode", System.Type.GetType("System.Decimal"))

            Dim cCCRequired As New DataColumn("Required", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCashflow)
            dtDt.Columns.Add(cCashflowname)
            dtDt.Columns.Add(cCollection)
            dtDt.Columns.Add(cCollectionCode)

            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)
            dtDt.Columns.Add(cTaxCode)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_CR() As DataTable ' made the jvdetail datatable structure
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCashflow As New DataColumn("Cashflow", System.Type.GetType("System.Decimal"))
            Dim cCashflowname As New DataColumn("Cashflowname", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Ply", System.Type.GetType("System.String"))

            Dim cCollection As New DataColumn("Collection", System.Type.GetType("System.String"))
            Dim cCollectionCode As New DataColumn("CollectionCode", System.Type.GetType("System.Decimal"))

            Dim cCCRequired As New DataColumn("CostReqd", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))
            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCashflow)
            dtDt.Columns.Add(cCashflowname)
            dtDt.Columns.Add(cCollection)
            dtDt.Columns.Add(cCollectionCode)

            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)
            dtDt.Columns.Add(cTaxCode)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_JV() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCredit As New DataColumn("Credit", System.Type.GetType("System.Decimal"))
            Dim cDebit As New DataColumn("Debit", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Ply", System.Type.GetType("System.String"))
            Dim cCCRequired As New DataColumn("CostReqd", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCredit)
            dtDt.Columns.Add(cDebit)

            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)
            dtDt.Columns.Add(cTaxCode)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_OB() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cDocno As New DataColumn("Docno", System.Type.GetType("System.String"))
            Dim cDoctype As New DataColumn("Doctype", System.Type.GetType("System.String"))
            Dim cDocnoname As New DataColumn("Docname", System.Type.GetType("System.String"))
            Dim cDocdate As New DataColumn("Docdate", System.Type.GetType("System.DateTime"))

            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))

            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cDocno)
            dtDt.Columns.Add(cDocnoname)
            dtDt.Columns.Add(cDocdate)

            dtDt.Columns.Add(cDoctype)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_BP() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarrn As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCline As New DataColumn("CLine", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cChqBookId As New DataColumn("ChqBookId", System.Type.GetType("System.String"))
            Dim cChqBookLot As New DataColumn("ChqBookLot", System.Type.GetType("System.String"))

            Dim cChqNo As New DataColumn("ChqNo", System.Type.GetType("System.String"))
            Dim cChqDate As New DataColumn("ChqDate", System.Type.GetType("System.String"))

            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            Dim cPly As New DataColumn("Ply", System.Type.GetType("System.String"))
            Dim cCostReqd As New DataColumn("CostReqd", System.Type.GetType("System.Boolean"))
            Dim cRef As New DataColumn("isCheque", System.Type.GetType("System.Boolean"))

            'Dim cCostUnitID As New DataColumn("CostUnitID", System.Type.GetType("System.Integer"))
            Dim cCostUnit As New DataColumn("CostUnit", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.String"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.String"))
            Dim cExpAcc As New DataColumn("ExpAcc", System.Type.GetType("System.String"))
            Dim cExpAccName As New DataColumn("ExpAccName", System.Type.GetType("System.String"))
            Dim cPettycashId As New DataColumn("PettycashId", System.Type.GetType("System.String")) ' sadhu added
            Dim cPettycashName As New DataColumn("PettycashName", System.Type.GetType("System.String")) ' sadhu added
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarrn)
            dtDt.Columns.Add(cCline)

            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cChqBookId)
            dtDt.Columns.Add(cChqBookLot)
            dtDt.Columns.Add(cChqNo)
            dtDt.Columns.Add(cChqDate)

            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            dtDt.Columns.Add(cPly)
            dtDt.Columns.Add(cCostReqd)

            dtDt.Columns.Add(cRef)

            dtDt.Columns.Add(cCostUnit)
            'dtDt.Columns.Add(cCostUnitID)
            dtDt.Columns.Add(cFromDate)
            dtDt.Columns.Add(cToDate)
            dtDt.Columns.Add(cExpAcc)
            dtDt.Columns.Add(cExpAccName)
            dtDt.Columns.Add(cPettycashId) 'sadhu added
            dtDt.Columns.Add(cPettycashName) 'sadhu added
            dtDt.Columns.Add(cTaxCode)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_RJV() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarrn As New DataColumn("Narration", System.Type.GetType("System.String"))

            Dim cDebit As New DataColumn("Debit", System.Type.GetType("System.Decimal"))
            Dim cCredit As New DataColumn("Credit", System.Type.GetType("System.Decimal"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            Dim cPly As New DataColumn("Ply", System.Type.GetType("System.String"))
            Dim cCostReqd As New DataColumn("CostReqd", System.Type.GetType("System.Boolean"))


            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarrn)


            dtDt.Columns.Add(cDebit)
            dtDt.Columns.Add(cCredit)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            dtDt.Columns.Add(cPly)
            dtDt.Columns.Add(cCostReqd)



            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_BR() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarrn As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCline As New DataColumn("CLine", System.Type.GetType("System.String"))
            Dim cColln As New DataColumn("Colln", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cChqBookId As New DataColumn("ChqBookId", System.Type.GetType("System.String"))
            Dim cChqBookLot As New DataColumn("ChqBookLot", System.Type.GetType("System.String"))

            Dim cChqNo As New DataColumn("ChqNo", System.Type.GetType("System.String"))
            Dim cChqDate As New DataColumn("ChqDate", System.Type.GetType("System.String"))

            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            Dim cPly As New DataColumn("Ply", System.Type.GetType("System.String"))
            Dim cCostReqd As New DataColumn("CostReqd", System.Type.GetType("System.Boolean"))
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarrn)
            dtDt.Columns.Add(cCline)

            dtDt.Columns.Add(cColln)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cChqBookId)
            dtDt.Columns.Add(cChqBookLot)
            dtDt.Columns.Add(cChqNo)
            dtDt.Columns.Add(cChqDate)

            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            dtDt.Columns.Add(cPly)
            dtDt.Columns.Add(cCostReqd)
            dtDt.Columns.Add(cTaxCode)


            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_PDC() As DataTable
        Dim dtTempDTL As New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cPayAmount As New DataColumn("PayAmount", System.Type.GetType("System.Decimal"))
            Dim cBalDue As New DataColumn("BalDue", System.Type.GetType("System.Decimal"))
            Dim cChqBookId As New DataColumn("ChqBookId", System.Type.GetType("System.String"))
            Dim cChqBookLot As New DataColumn("ChqBookLot", System.Type.GetType("System.String"))
            Dim cChqNo As New DataColumn("ChqNo", System.Type.GetType("System.String"))
            Dim cChqDate As New DataColumn("ChqDate", System.Type.GetType("System.String"))
            Dim cGUID As New DataColumn("GUID", System.Type.GetType("System.String"))
            Dim cDeleted As New DataColumn("DELETED", System.Type.GetType("System.String"))
            Dim cPly As New DataColumn("Ply", System.Type.GetType("System.String"))
            Dim cCostReqd As New DataColumn("CostReqd", System.Type.GetType("System.Boolean"))
            Dim cAmtwoutInterest As New DataColumn("AmtWOInterest", System.Type.GetType("System.Decimal"))
            Dim cInterest As New DataColumn("Interest", System.Type.GetType("System.Decimal"))

            Dim cCostUnit As New DataColumn("CostUnit", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.String"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.String"))
            Dim cExpAcc As New DataColumn("ExpAcc", System.Type.GetType("System.String"))
            Dim cExpAccName As New DataColumn("ExpAccName", System.Type.GetType("System.String"))
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtTempDTL.Columns.Add(cId)
            dtTempDTL.Columns.Add(cPayAmount)
            dtTempDTL.Columns.Add(cBalDue)
            dtTempDTL.Columns.Add(cChqBookId)
            dtTempDTL.Columns.Add(cChqBookLot)
            dtTempDTL.Columns.Add(cChqNo)
            dtTempDTL.Columns.Add(cChqDate)

            dtTempDTL.Columns.Add(cGUID)
            dtTempDTL.Columns.Add(cDeleted)
            dtTempDTL.Columns.Add(cPly)
            dtTempDTL.Columns.Add(cCostReqd)
            dtTempDTL.Columns.Add(cAmtwoutInterest)
            dtTempDTL.Columns.Add(cInterest)

            dtTempDTL.Columns.Add(cCostUnit)
            dtTempDTL.Columns.Add(cFromDate)
            dtTempDTL.Columns.Add(cToDate)
            dtTempDTL.Columns.Add(cExpAcc)
            dtTempDTL.Columns.Add(cExpAccName)
            dtTempDTL.Columns.Add(cTaxCode)

            Return dtTempDTL
        Catch ex As Exception
            Return dtTempDTL
        End Try
    End Function

    Public Shared Function CreateDataTable_IJV() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCredit As New DataColumn("Credit", System.Type.GetType("System.Decimal"))
            Dim cDebit As New DataColumn("Debit", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
            Dim cCCRequired As New DataColumn("Required", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cEdit As New DataColumn("canEdit", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
            Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCredit)
            dtDt.Columns.Add(cDebit)

            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cEdit)

            dtDt.Columns.Add(cGuid)
            dtDt.Columns.Add(cTaxCode)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_IC() As DataTable ' made the jvdetail datatable structure
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCashflow As New DataColumn("Cashflow", System.Type.GetType("System.Decimal"))
            Dim cCashflowname As New DataColumn("Cashflowname", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
            Dim cRefno As New DataColumn("Refno", System.Type.GetType("System.String"))

            Dim cCollection As New DataColumn("Collection", System.Type.GetType("System.String"))
            Dim cCollectionCode As New DataColumn("CollectionCode", System.Type.GetType("System.Decimal"))

            Dim cCCRequired As New DataColumn("Required", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCashflow)
            dtDt.Columns.Add(cCashflowname)
            dtDt.Columns.Add(cCollection)
            dtDt.Columns.Add(cCollectionCode)
            dtDt.Columns.Add(cRefno)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_Settle() As DataTable ' made the jvdetail datatable structure
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cjnlid As New DataColumn("jnlid", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))


            'Dim cCollection As New DataColumn("Collection", System.Type.GetType("System.String"))
            'Dim cCollectionCode As New DataColumn("CollectionCode", System.Type.GetType("System.Decimal"))

            'Dim cCCRequired As New DataColumn("Required", System.Type.GetType("System.Decimal"))
            'Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            'Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cjnlid)
            dtDt.Columns.Add(cAmount)

            'dtDt.Columns.Add(cNarration)
            'dtDt.Columns.Add(cCashflow)
            'dtDt.Columns.Add(cCashflowname)
            'dtDt.Columns.Add(cCollection)
            'dtDt.Columns.Add(cCollectionCode)
            'dtDt.Columns.Add(cRefno)
            'dtDt.Columns.Add(cAmount)
            'dtDt.Columns.Add(cCostcenter)
            'dtDt.Columns.Add(cCCRequired)
            'dtDt.Columns.Add(cStatus)
            'dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_Loan() As DataTable ' made the jvdetail datatable structure
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cDates As New DataColumn("Dates", System.Type.GetType("System.DateTime"))
            Dim cELD_ID As New DataColumn("ELD_ID", System.Type.GetType("System.Decimal"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cPAmount As New DataColumn("PAmount", System.Type.GetType("System.Decimal"))
            Dim cstatus As New DataColumn("Status", System.Type.GetType("System.String"))


            dtDt.Columns.Add(cELD_ID)
            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cDates)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cPAmount)
            dtDt.Columns.Add(cstatus)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_CR_Clearance() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cNarrn As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCline As New DataColumn("CLine", System.Type.GetType("System.String"))
            Dim cColln As New DataColumn("Colln", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))


            Dim cChqNo As New DataColumn("ChqNo", System.Type.GetType("System.String"))
            Dim cChqDate As New DataColumn("ChqDate", System.Type.GetType("System.String"))

            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))


            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)


            dtDt.Columns.Add(cNarrn)
            dtDt.Columns.Add(cCline)
            dtDt.Columns.Add(cColln)
            dtDt.Columns.Add(cAmount)

            dtDt.Columns.Add(cChqNo)
            dtDt.Columns.Add(cChqDate)

            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)



            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDatesTable_CR_Clearance() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cDocDT As New DataColumn("DocDate", System.Type.GetType("System.DateTime"))
            dtDt.Columns.Add(cDocDT)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDocNosTable_CR_Clearance() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cDocNo As New DataColumn("DocNo", System.Type.GetType("System.DateTime"))
            dtDt.Columns.Add(cDocNo)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateRecTable_CR_Clearance() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))

            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAmount)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Public Shared Function CreateDataTable_PJ() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
        Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
        Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
        Dim cItem As New DataColumn("Item", System.Type.GetType("System.String"))
        Dim cQty As New DataColumn("Qty", System.Type.GetType("System.Decimal"))
        Dim cRate As New DataColumn("Rate", System.Type.GetType("System.Decimal"))
        Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
        Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
        Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))
        Dim cInvNo As New DataColumn("InvNo", System.Type.GetType("System.String"))
        Dim cInvDate As New DataColumn("InvDate", System.Type.GetType("System.DateTime"))
        Dim cLPONo As New DataColumn("LPONo", System.Type.GetType("System.String"))
        Dim cCreditDays As New DataColumn("CreditDays", System.Type.GetType("System.Decimal"))

        Dim cCostReqd As New DataColumn("CostReqd", System.Type.GetType("System.Boolean"))
        Dim cPly As New DataColumn("Ply", System.Type.GetType("System.String"))
        Dim cTaxCode As New DataColumn("TaxCode", System.Type.GetType("System.String"))
        Dim cTdsCode As New DataColumn("TdsCode", System.Type.GetType("System.String"))
        Dim cTaxAmount As New DataColumn("TaxAmount", System.Type.GetType("System.Decimal"))
        Dim cTdsAmount As New DataColumn("TdsAmount", System.Type.GetType("System.Decimal"))

        dtDt.Columns.Add(cId)
        dtDt.Columns.Add(cAccountid)
        dtDt.Columns.Add(cAccountname)

        dtDt.Columns.Add(cItem)
        dtDt.Columns.Add(cQty)
        dtDt.Columns.Add(cRate)
        dtDt.Columns.Add(cAmount)
        dtDt.Columns.Add(cStatus)
        dtDt.Columns.Add(cGuid)
        dtDt.Columns.Add(cInvNo)
        dtDt.Columns.Add(cInvDate)
        dtDt.Columns.Add(cLPONo)
        dtDt.Columns.Add(cCreditDays)
        dtDt.Columns.Add(cPly)
        dtDt.Columns.Add(cCostReqd)
        dtDt.Columns.Add(cTaxCode)
        dtDt.Columns.Add(cTdsCode)
        dtDt.Columns.Add(cTaxAmount)
        dtDt.Columns.Add(cTdsAmount)

        Return dtDt
    End Function

End Class
