Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports SmsService
Imports SmsServiceArabic

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class SendingSms
     Inherits System.Web.Services.WebService

    <WebMethod()> _
      Public Function SendSms(ByVal cmsid As String, ByVal GroupId As String, ByVal csm_id As String, ByVal MobileNumber As String, ByVal UniqueID As String, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)
        Dim Returnvalue = ""

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sql_query = "select * from  COM_MANAGE_SMS where CMS_ID='" & cmsid & "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            Dim smsmessage As String = ""
            Dim smsusername As String = ""
            Dim smspassword As String = ""
            Dim smsfrom As String = ""
            Dim smsutf16 As String = ""
            Dim smsarabic As Boolean = False

            If ds.Tables(0).Rows.Count > 0 Then
                smsmessage = ds.Tables(0).Rows(0).Item("CMS_SMS_TEXT").ToString()
                smsusername = ds.Tables(0).Rows(0).Item("CMS_USER_NAME").ToString()
                smspassword = ds.Tables(0).Rows(0).Item("CMS_PASSWORD").ToString()
                smsfrom = ds.Tables(0).Rows(0).Item("CMS_FROM").ToString()
                smsutf16 = ds.Tables(0).Rows(0).Item("CMS_UTF_16").ToString()
                smsarabic = ds.Tables(0).Rows(0).Item("CMS_ARABIC")
                Dim merge_id = ds.Tables(0).Rows(0).Item("CMS_MERGE_ID").ToString()

                If merge_id.ToString.Trim() <> "" Then
                    smsmessage = GetMergeMessage(smsmessage, merge_id, UniqueID)
                End If

                Returnvalue = send(MobileNumber, UniqueID, smsfrom, smsusername, smspassword, smsutf16, smsmessage, cmsid, GroupId, csm_id, smsarabic, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)
            Else
                Returnvalue = "Error : SMS Template Not Available"
            End If
        Catch ex As Exception
            Returnvalue = "Error in Application " & ex.Message
        End Try


        Return Returnvalue
    End Function

    Public Function GetMergeMessage(ByVal originalmessage As String, ByVal merge_id As String, ByVal uniqueid As String) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim message As String = "select '"
        message = message & originalmessage
        message = message.Replace("<#", " '+ ")
        message = message.Replace("#>", " +' ")
        message = message + "'"


        Dim sql_query As String = "select MERGE_TABLE,MERGE_UNIQUE_FIELD_NAME from COM_MERGE_TABLES where MERGE_ID='" & merge_id & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        Dim table = ""
        Dim unqfield = ""
        If ds.Tables(0).Rows.Count > 0 Then
            table = ds.Tables(0).Rows(0).Item("MERGE_TABLE").ToString()
            unqfield = ds.Tables(0).Rows(0).Item("MERGE_UNIQUE_FIELD_NAME").ToString()
            message = message & " from ( " & table & ") A WHERE " & unqfield & "='" & uniqueid & "'"
            message = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, message)
        End If


        Return message

    End Function

    Public Function send(ByVal mobilenumber As String, ByVal UniqueID As String, ByVal From As String, ByVal username As String, ByVal password As String, ByVal smsutf16 As String, ByVal message As String, ByVal cmsid As String, ByVal GroupId As String, ByVal csm_id As String, ByVal smsarabic As Boolean, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)
        Dim returnvalue = ""

        Try
            mobilenumber = mobilenumber.Replace("-", "")
            Dim status As String = ""
            If smsarabic = True Then
                status = SmsServiceArabic.smsArabic.SendMessage(mobilenumber, smsutf16, From, username, password)
            Else
                status = sms.SendMessage(mobilenumber, message, From, username, password)
            End If
            returnvalue = status
            SaveLog(GroupId, cmsid, UniqueID, mobilenumber, status, csm_id, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)
        Catch ex As Exception
            returnvalue = "Error : " & ex.Message
        End Try

        Return returnvalue
    End Function
    Public Sub SaveLog(ByVal GroupId As String, ByVal MessageId As String, ByVal UniqueID As String, ByVal Mobile As String, ByVal status As String, ByVal csm_id As String, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LOG_CGR_ID", GroupId) ''0 If Corporate Office or Value if By Sending as Groups
            pParms(1) = New SqlClient.SqlParameter("@LOG_CMS_ID", MessageId) '' MessageId
            pParms(2) = New SqlClient.SqlParameter("@LOG_UNIQUE_ID", UniqueID)
            If Mobile <> Nothing Then
                pParms(3) = New SqlClient.SqlParameter("@LOG_MOBILE_NUMBER", Mobile)
            End If
            pParms(4) = New SqlClient.SqlParameter("@LOG_STATUS", status)
            pParms(5) = New SqlClient.SqlParameter("@LOG_CSM_ID", csm_id) ''0 If Corporate Office or Value if By Sending as Groups
            pParms(6) = New SqlClient.SqlParameter("@LOG_SENDING_ID", LOG_SENDING_ID)
            pParms(7) = New SqlClient.SqlParameter("@SENDING_USER_EMP_ID", SENDING_USER_EMP_ID)
            pParms(8) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_LOG_SMS_TABLE_INSERT", pParms)

        Catch ex As Exception

        End Try

    End Sub

End Class
