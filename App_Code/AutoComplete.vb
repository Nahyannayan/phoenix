' (c) Copyright Microsoft Corporation. 
' This source is subject to the Microsoft Permissive License. 
' See http://www.microsoft.com/resources/sharedsource/licensingbasics/sharedsourcelicenses.mspx. 
' All other rights reserved. 

Imports System
Imports System.Collections.Generic
Imports System.Web.Services
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml
<WebService([Namespace]:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
Public Class AutoComplete
    Inherits WebService
    Public Sub New()
    End Sub

    <WebMethod()> _
    Public Function GetCompletionList(ByVal prefixText As String, ByVal Type As Integer, ByVal CurrentUnit As String) As String()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql, str_filter As String
        str_filter = ""
        Select Case Type
            Case AccountFilter.BANK
                str_filter = " AND ACT_BANKCASH='B'"
            Case AccountFilter.NOTCC
                str_filter = " AND ACT_BANKCASH<>'CC'"
            Case AccountFilter.CASHONLY
                str_filter = " AND ACT_BANKCASH='C'"
            Case AccountFilter.NORMAL
                str_filter = " AND ACT_BANKCASH='N'"
            Case AccountFilter.PARTY1
                str_filter = " AND ACT_FLAG='S'"
            Case AccountFilter.PARTY2
                str_filter = " AND ACT_TYPE in ('Liability','Asset')  and ACT_BANKCASH   in ('N') and ACT_BCTRLAC<>1"
            Case AccountFilter.DEBIT
                str_filter = " AND ACT_BANKCASH='N'"
                str_filter = str_filter & " AND ACT_FLAG = 'N'"
            Case AccountFilter.DEBIT_D
                str_filter = " AND ACT_BANKCASH='N'"
                str_filter = str_filter & " AND ACT_FLAG = 'N'"
            Case AccountFilter.CHQISSAC
                str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("ChqissAC") & "'"
            Case AccountFilter.INTRAC
                str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("IntrAC") & "'"
            Case AccountFilter.ACRDAC
                str_filter = " AND ACT_CTRLACC='" & HttpContext.Current.Session("AcrdAc") & "'"
            Case AccountFilter.PREPDAC
                str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("PrepdAC") & "'"
            Case AccountFilter.CHQISSAC_PDC
                str_filter = " AND ACT_SGP_ID='" & HttpContext.Current.Session("ChqissAC") & "'"
            Case AccountFilter.CUSTSUPP
                str_filter = " AND (ACT_FLAG='S' OR ACT_FLAG='C')"
            Case AccountFilter.CUSTSUPPnIJV
                str_filter = " AND (( ACT_FLAG='S' OR  ACT_FLAG='C') or ( ACT_SGP_ID='0810')) "
            Case AccountFilter.INCOME
                str_filter = " AND (ACT_TYPE ='INCOME')"
            Case AccountFilter.CHARGE
                str_filter = " AND (ACT_TYPE ='Liability' AND ACT_FLAG<>'S')"
            Case AccountFilter.CONCESSION
                str_filter = " AND (ACT_TYPE ='EXPENSES')"
            Case AccountFilter.FEEDISC
                str_filter = " AND (ACT_TYPE ='EXPENSES')"
        End Select

        If IsNumeric(prefixText) Then
            str_Sql = "SELECT act_id+'-'+act_name From ACCOUNTS_M " _
                & " WHERE (ACT_Bctrlac = 'FALSE') AND ACT_BACTIVE='TRUE' " _
                & " AND (act_id like '" + prefixText + "%' ) " _
                & " AND (ACT_BSU_ID LIKE '%" & CurrentUnit & "%')" _
                & str_filter
        Else
            str_Sql = "SELECT act_id+'-'+act_name From ACCOUNTS_M " _
                           & " WHERE (ACT_Bctrlac = 'FALSE') AND ACT_BACTIVE='TRUE' " _
                           & " AND (act_id like '%" + prefixText + "%' or act_name like '%" + prefixText + "%') " _
                           & " AND (ACT_BSU_ID LIKE '%" & CurrentUnit & "%')" _
                           & str_filter
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer
        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function


    <WebMethod()> _
        Public Function GetStudentList(ByVal prefixText As String, ByVal Type As Integer, ByVal CurrentUnit As String) As String()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        'str_filter = "", str_filter
        'Select Case Type
        '    Case AccountFilter.BANK
        '        str_filter = " AND ACT_BANKCASH='B'"
        'End Select

        If IsNumeric(prefixText) Then
            str_Sql = "select stu_NO+'-'+stu_NAME	from vw_OSO_STUDENT_M " _
                & " WHERE  (stu_NO like '" + prefixText + "%' ) " _
                & " AND (STU_BSU_ID LIKE '%" & CurrentUnit & "%')"  
        Else
            str_Sql = "select stu_NO+'-'+stu_NAME	from vw_OSO_STUDENT_M " _
                           & " WHERE   (stu_NO like '%" + prefixText + "%' or stu_NAME like '%" + prefixText + "%') " _
                           & " AND (STU_BSU_ID LIKE '%" & CurrentUnit & "%')"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer
        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function
    <WebMethod()> _
         Public Function DisplayName(ByVal BsuId As String) As XmlDocument
        Dim Encr_decrData As New Encryption64
        Dim Query As String = ""
        Dim Xdoc As New XmlDocument
        Dim SplitArr As String()
        SplitArr = BsuId.ToString().Split("_")
        Dim likString As String = "%" & SplitArr(1) & "%"
        Dim myDataset As New DataSet()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            'Dim pParms(1) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsuid)
            Query = " SELECT top 20 STU_ID,STU_NO as No, STU_NAME as Student  FROM vw_OSO_STUDENT_M " _
                               & " WHERE STU_BSU_ID ='" & SplitArr(0) & "' AND  STU_NAME LIKE '" & likString & "'  ORDER BY STU_NAME "
            myDataset = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)

            Dim strXdoc As String
            strXdoc = myDataset.GetXml()
            Xdoc.LoadXml(strXdoc)

        Catch ex As Exception

        End Try

        Return Xdoc
    End Function

End Class
