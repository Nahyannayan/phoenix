﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Public Class Library

    Public Shared Function LibrarySuperAcess(ByVal emp_id As String) As Boolean

        Dim hasrights As Boolean = False

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_UPLOAD_EMP_ACCESS_RIGHTS " & _
                        "WHERE EMP_ID='" & emp_id & "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            hasrights = True
        End If

        Return hasrights

    End Function

    Public Shared Function InsertPouch(ByVal Pouch_stock_id As String, ByVal bsu_id As String, ByVal stock_id As String) As String
        Dim rval As String = ""
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try

            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STOCK_ID_POUCH_ID", Pouch_stock_id)
            pParms(1) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", bsu_id)
            pParms(2) = New SqlClient.SqlParameter("@STOCK_ID", stock_id)
            pParms(3) = New SqlClient.SqlParameter("@OPTION", 1)
            rval = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "LIB_POUCH_TRAN", pParms)
            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()
        End Try

        Return rval

    End Function



End Class
