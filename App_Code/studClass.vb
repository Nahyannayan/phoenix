Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Collections
Imports System.Reflection
Imports System.Collections.Generic
Imports System
Imports System.Web.UI.WebControls

Public Class studClass

    Function PopulateCurriculum(ByVal ddlClm As DropDownList, ByVal bsuId As String) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CLM_ID,CLM_DESCR FROM CURRICULUM_M AS A" _
                                & " INNER JOIN ACADEMICYEAR_D AS B ON A.CLM_ID=B.ACD_CLM_ID" _
                                & " WHERE ACD_BSU_ID='" + bsuId + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClm.DataSource = ds
        ddlClm.DataTextField = "CLM_DESCR"
        ddlClm.DataValueField = "CLM_ID"
        ddlClm.DataBind()
        Return ddlClm
    End Function

    Public Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String)
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then


            Dim li As New ListItem
            li.Text = ds.Tables(0).Rows(0).Item(0)
            li.Value = ds.Tables(0).Rows(0).Item(1)
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        End If
        'Dim rdr As SqlDataReader
        'Using rdr
        '    rdr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        '    ' Using rdr As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, str_query)
        '    Dim li As New ListItem
        '    While rdr.Read
        '        li.Text = rdr.GetString(0)
        '        li.Value = rdr.GetValue(1)
        '        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        '    End While
        '    ' End Using
        '    rdr.Close()
        'End Using
        Return ddlAcademicYear
    End Function

    Function CheckRateExists(ByVal startDate As DateTime, ByVal sbl_id As String, ByVal acd_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(SBL_ID) FROM TRANSPORT.FN_getAREARATE(" + acd_id + ") " _
                                & " WHERE SBL_ID=" + sbl_id + " AND " _
                                 & "'" + Format(Date.Parse(startDate), "yyyy-MM-dd") + "'" _
                                & " BETWEEN STARTDATE AND ENDDATE AND AMOUNT<>0"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Function checkDATACORREXIST(ByVal STU_ID As String) As Boolean
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        str_conn = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT count(*) from STUDENT_SERVICES_D WHERE SSV_SVC_ID=1 AND  " _
                  & " convert(datetime,SSV_FROMDATE) >=GetDate() AND " _
                  & " SSV_TODATE IS NULL AND SSV_bACTIVE=0 AND SSV_STU_ID='" + STU_ID + "'  "
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count >= 1 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal SHF_id As String = "", Optional ByVal STM_id As String = "")
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_filter As String = String.Empty
        If SHF_id <> "" Then
            str_filter = " AND grm_shf_id=" + SHF_id + ""

        Else
            str_filter = " AND grm_shf_id<>''"
        End If
        If STM_id <> "" Then
            str_filter += " AND grm_STM_id=" + STM_id + ""

        Else
            str_filter += " AND grm_STM_id<>''"
        End If

        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                               & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                           & "  grm_acd_id=" + acdid + str_filter + " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Public Function PopulateGradeShift(ByVal ddl As DropDownList, ByVal grdid As String, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct shf_descr,shf_id FROM grade_bsu_m,shifts_m WHERE" _
                                 & " grade_bsu_m.grm_shf_id=shifts_m.shf_id and " _
                             & " grm_acd_id=" + acdid + " and grm_grd_id='" + grdid + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "shf_descr"
        ddl.DataValueField = "shf_id"
        ddl.DataBind()
        Return ddl
    End Function

    Public Function PopulateGradeStream(ByVal ddl As DropDownList, ByVal grdid As String, ByVal acdid As String, ByVal shfid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT stm_descr,grm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                             & " grm_acd_id=" + acdid + " and grm_grd_id='" + grdid + "' and grm_shf_id=" + shfid
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "stm_descr"
        ddl.DataValueField = "grm_id"
        ddl.DataBind()
        If Not ddl.Items.FindByValue("1") Is Nothing Then
            ddl.Items.FindByValue("1").Selected = True
        End If

        Return ddl
    End Function


    Public Function PopulateShift(ByVal ddl As DropDownList, ByVal bsuid As String, Optional ByVal acd_id As String = "")
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = String.Empty

        If acd_id <> "" Then
            str_query = "select SHF_ID,SHF_DESCR from SHIFTS_M WHERE SHF_ID IN(select distinct " _
                   & " grm_shf_id from grade_bsu_m where grm_acd_id='" & acd_id & "')"
        Else

            str_query = "Select shf_descr,shf_id from shifts_m where shf_bsu_id='" + bsuid + "'"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "shf_descr"
        ddl.DataValueField = "shf_id"
        ddl.DataBind()
        Return ddl
    End Function

    Public Function PopulateStream(ByVal ddl As DropDownList, Optional ByVal acd_id As String = "")
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = String.Empty
        If acd_id <> "" Then
            str_query = "SELECT STM_ID,STM_DESCR  FROM STREAM_M WHERE STM_ID IN( " _
                    & " select distinct grm_STM_id from grade_bsu_m where grm_acd_id='" & acd_id & "')"
        Else
            str_query = "Select stm_id,stm_descr from stream_m"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "stm_descr"
        ddl.DataValueField = "stm_id"
        ddl.DataBind()
        Return ddl
    End Function
    Public Function EnquiryChange(ByVal eqsid As String, ByVal eqs_status As String, ByVal offerdate As Date, ByVal current_status As Integer, ByVal pra_stg_id As Integer, ByVal pra_bcompleted As Boolean)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(7) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EQS_ID", eqsid)

        pParms(1) = New SqlClient.SqlParameter("@EQS_STATUS", eqs_status)

        pParms(2) = New SqlClient.SqlParameter("@OFFER_DATE", offerdate)

        pParms(3) = New SqlClient.SqlParameter("@CURRENT_STATUS", current_status)

        pParms(4) = New SqlClient.SqlParameter("@PRA_STG_ID", pra_stg_id)

        pParms(5) = New SqlClient.SqlParameter("@PRA_bCOMPLETED", pra_bcompleted)

        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)

        pParms(6).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "StudentEnqChange", pParms)

        Dim returnflag As Integer = pParms(6).Value

        Return returnflag

    End Function


    Public Sub SetChk(ByVal gvGrid As GridView, ByVal li As List(Of String))
        Try
            Dim i As Integer
            Dim chk As CheckBox
            If gvGrid.Rows.Count > 0 Then
                For i = 0 To gvGrid.Rows.Count - 1
                    chk = gvGrid.Rows(i).FindControl("chkSelect")
                    If chk Is Nothing Then Continue For
                    If chk.Checked = True Then
                        If list_add(chk.ClientID + "-" + gvGrid.PageIndex.ToString, li) = False Then
                            chk.Checked = True
                            gvGrid.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                        End If
                    Else
                        If list_exist(chk.ClientID + "-" + gvGrid.PageIndex.ToString, li) = True Then
                            chk.Checked = True
                            gvGrid.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                        End If
                        list_remove(chk.ClientID + "-" + gvGrid.PageIndex.ToString, li)
                    End If

                Next

            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Function list_exist(ByVal p_userid As String, ByVal li As List(Of String)) As Boolean
        If li.Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String, ByVal li As List(Of String)) As Boolean
        If li.Contains(p_userid) Then
            Return False
        Else
            li.Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String, ByVal li As List(Of String))
        If li.Contains(p_userid) Then
            li.Remove(p_userid)
        End If
    End Sub

    Public Function GetEmpId(ByVal userName As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT USR_EMP_ID FROM USERS_M WHERE USR_NAME='" + userName + "'"
        Dim empId As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return empId
    End Function

    Public Function isEmpTeacher(ByVal empId As Integer) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT EMP_ECT_ID FROM EMPLOYEE_M WHERE EMP_ID=" + empId.ToString
        Dim emp_ect_Id As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If HttpContext.Current.Session("SBSUID") = "125002" Then
            If emp_ect_Id = 1 Or emp_ect_Id = 3 Or emp_ect_Id = 4 Then
                Return True
            Else
                Return False
            End If
        Else
            If emp_ect_Id = 1 Or emp_ect_Id = 4 Then
                Return True
            Else
                Return False
            End If
        End If
    End Function


    Public Sub BindDocumentsList(ByVal lst As ListBox, ByVal stgId As String, ByVal bcomplete As String, ByVal eqsId As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String
        Dim li As ListItem
        Dim reader As SqlDataReader
        If bcomplete = "false" Then

            str_query = "SELECT DOC_DESCR=CASE DCE_COPIES WHEN 1 THEN DOC_DESCR ELSE CONVERT(VARCHAR(10),DCE_COPIES)+' '+DOC_DESCR END," _
                        & " DOC_ID=CONVERT(VARCHAR(100),DOC_ID)+'|'+CONVERT(VARCHAR(100),DCE_STG_ID),DCE_STG_ID FROM DOCREQD_M AS A INNER JOIN" _
                        & " ENQUIRY_JOINDOCUMENTS_S AS B ON A.DOC_ID=B.DCE_DOC_ID" _
                        & " WHERE  DCE_EQS_ID=" + eqsId + " AND DCE_STG_ID IN (" _
                        & " SELECT PRA_STG_ID FROM PROCESSFO_APPLICANT_S WHERE PRA_STG_ORDER<=(" _
                        & " SELECT PRA_STG_ORDER FROM PROCESSFO_APPLICANT_S WHERE PRA_STG_ID=" + stgId + " AND PRA_EQS_ID=" + eqsId + ")" _
                        & " AND PRA_EQS_ID=" + eqsId + ")" _
                        & " AND DCE_bCOMPLETE='false'"
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Else
            str_query = "SELECT DOC_DESCR=CASE DCE_COPIES WHEN 1 THEN DOC_DESCR ELSE CONVERT(VARCHAR(10),DCE_COPIES)+' '+DOC_DESCR END," _
                        & " DOC_ID=CONVERT(VARCHAR(100),DOC_ID)+'|'+CONVERT(VARCHAR(100),DCE_STG_ID),DCE_STG_ID FROM DOCREQD_M AS A INNER JOIN" _
                        & " ENQUIRY_JOINDOCUMENTS_S AS B ON A.DOC_ID=B.DCE_DOC_ID" _
                        & " WHERE  DCE_EQS_ID=" + eqsId + " AND DCE_bCOMPLETE='true'"

            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        End If

        While reader.Read
            li = New ListItem
            li.Text = reader.GetString(0)
            li.Value = reader.GetString(1)
            li.Attributes.Add("title", reader.GetString(0))
            If bcomplete = "false" Then
                If reader.GetValue(2).ToString <> stgId Then
                    li.Attributes.Add("style", "color:red")
                End If
            End If
            lst.Items.Add(li)
        End While
        reader.Close()
    End Sub

    Public Sub UpdateDocListItems(ByVal lstFrom As ListBox, ByVal lstTo As ListBox, ByVal complete As String, ByVal eqsId As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim li As ListItem
        Dim lstTemp As New ListBox


        For Each li In lstFrom.Items
            If li.Selected = True Then
                lstTo.Items.Add(li)
                lstTemp.Items.Add(li)
                Dim ids As String()
                ids = li.Value.Split("|")
                str_query = "exec updateENQUIRYJOINDOCUMENTS " _
                        & eqsId + "," _
                        & ids(1) + "," _
                        & ids(0) + "," _
                        & "'" + complete + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next

        For Each li In lstTemp.Items
            lstFrom.Items.Remove(li)
        Next

        For Each li In lstFrom.Items
            li.Attributes.Add("title", li.Text)
        Next

        For Each li In lstTo.Items
            li.Attributes.Add("title", li.Text)
        Next


        lstTo.ClearSelection()
        lstFrom.ClearSelection()
    End Sub

    Public Sub UpdateEnquiryStage(ByVal mode As String, ByVal eqsId As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "EXEC updateENQUIRYDOCUMENTSTAGE " + eqsId + ",'" + mode + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Function GetBsuShowDocs(ByVal bsuid As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(BSU_bSHOWDOCS,'false') FROM BUSINESSUNIT_M WHERE BSU_ID='" + bsuid + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim bShow As Boolean
        While reader.Read
            bShow = reader.GetBoolean(0)
        End While
        reader.Close()
        Return bShow
    End Function
    Function checkFeeClosingDate(ByVal BSU_ID As String, ByVal acd_id As Integer, ByVal currDate As DateTime) As Boolean
        Try
            Dim str_query As String
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            str_query = "SELECT SVB_PROVIDER_BSU_ID FROM SERVICES_BSU_M WHERE SVB_SVC_ID=1 AND SVB_BSU_ID='" + BSU_ID + "' AND SVB_ACD_ID=" + acd_id.ToString
            Dim providerBsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            Dim sqlParam(3) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PROV_BSU_ID", providerBsu, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@STU_BSU_ID", BSU_ID, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@CURRDATE", currDate.ToString("dd/MMM/yyyy"), SqlDbType.DateTime)
            sqlParam(3) = Mainclass.CreateSqlParameter("@RET_VALUE", False, SqlDbType.Bit, True)
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "FEES.CHECK_TRANSPORT_FEE_CLOSE_DATE", sqlParam)
            If sqlParam(3).Value Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function

    Function checkFeeClosingDate_old(ByVal BSU_ID As String, ByVal acd_id As Integer, ByVal currDate As DateTime) As Boolean
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT SVB_PROVIDER_BSU_ID FROM SERVICES_BSU_M WHERE SVB_SVC_ID=1 AND SVB_BSU_ID='" + BSU_ID + "' AND SVB_ACD_ID=" + acd_id.ToString
        Dim providerBsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        str_query = "select count(*) from FEES.FEE_TRNSALL where " _
                  & " convert(datetime,FTA_TRANDT) >=convert(datetime,'" + Format(currDate, "yyyy-MM-dd") + "') and " _
                  & " (FTA_BSU_ID='" + providerBsu + "' or   FTA_STU_BSU_ID='" + BSU_ID + "') and FTA_TRANTYPE='MONTHEND' and isnull(FTA_BCLOSE,0)=1"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count >= 1 Then
            Return False
        End If
        'str_conn = ConnectionManger.GetOASISConnectionString
        'str_query = "select count(*) from FEES.FEE_TRNSALL where " _
        '     & " convert(datetime,FTA_TRANDT) >=convert(datetime,'" + Format(currDate, "yyyy-MM-dd") + "') and " _
        '     & " (FTA_BSU_ID='" + BSU_ID + "') and FTA_TRANTYPE='MONTHEND' and isnull(FTA_BCLOSE,0)=1"

        'count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If count >= 1 Then
        '    Return False
        'End If
        Return True
    End Function

    Sub SortDropDown(ByVal dd As DropDownList)
        Dim ar As ListItem()
        Dim i As Long = 0
        For Each li As ListItem In dd.Items
            ReDim Preserve ar(i)
            ar(i) = li
            i += 1
        Next
        Dim ar1 As Array = ar

        ar1.Sort(ar1, New ListItemComparer)
        dd.Items.Clear()
        dd.Items.AddRange(ar1)
    End Sub

    Function isFeeIdAuto(ByVal acdId As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(ACD_bGENFEEID,'TRUE') FROM ACADEMICYEAR_D WHERE ACD_ID=" + acdId
        Dim auto As Boolean
        auto = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If auto = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetACD_ID(ByVal sDate As String, ByVal bsu_id As String, ByVal clm As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim STR_query As String = "SELECT ACD_ID FROM ACADEMICYEAR_D WHERE ACD_CLM_ID=" + clm _
                                & " AND ACD_BSU_ID='" + bsu_id + "' AND " _
                                & "'" + Format(Date.Parse(sDate), "yyyy-MM-dd") + "'" _
                                & " BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim acd_id As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, STR_query)
        Return acd_id
    End Function
End Class
