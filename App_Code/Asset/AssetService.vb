﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports UtilityObj
Public Class AssetService
    Structure ServiceDetail
        Public ServiceHeader As DataRow
        Public ServiceFooter As DataTable
    End Structure
    Public Shared Function GetAssetServiceTransaction(ByVal SVH_ID As String) As ServiceDetail
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SVH_ID", SVH_ID, SqlDbType.VarChar)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetServiceTransaction]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                Dim SVD_Detail As New ServiceDetail
                If mSet.Tables(0).Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mSet.Tables(0).NewRow
                    mSet.Tables(0).Rows.Add(mrow)
                End If
                SVD_Detail.ServiceHeader = mSet.Tables(0).Rows(0)
                If mSet.Tables.Count > 1 Then
                    Dim mtable As New DataTable
                    Dim dcID As New DataColumn("ID", GetType(Integer))
                    dcID.AutoIncrement = True
                    dcID.AutoIncrementSeed = 1
                    dcID.AutoIncrementStep = 1
                    mtable.Columns.Add(dcID)
                    mtable.Merge(mSet.Tables(1))
                    SVD_Detail.ServiceFooter = mtable
                    SVD_Detail.ServiceFooter.AcceptChanges()
                End If
                GetAssetServiceTransaction = SVD_Detail
            Else
                GetAssetServiceTransaction = Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            GetAssetServiceTransaction = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function SaveAssetServiceDetail(ByRef SVD_Detail As ServiceDetail, ByRef SVH_ID As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Dim sqlParam(11) As SqlParameter
        Try
            Dim mRow As DataRow
            Dim Retval As String
            Dim Success As Boolean = True
            Dim ErrorMsg As String = ""
            mRow = SVD_Detail.ServiceHeader
            ReDim sqlParam(13)
            sqlParam(0) = Mainclass.CreateSqlParameter("@SVH_ID", mRow("SVH_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SVH_BSU_ID", mRow("SVH_BSU_ID"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SVH_SUP_ACT_ID", mRow("SVH_SUP_ACT_ID"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@SVH_REQDT", mRow("SVH_REQDT"), SqlDbType.DateTime)
            sqlParam(4) = Mainclass.CreateSqlParameter("@SVH_REQUESTEDBY", mRow("SVH_REQUESTEDBY"), SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@SVH_bApproved", mRow("SVH_bApproved"), SqlDbType.Bit)
            sqlParam(6) = Mainclass.CreateSqlParameter("@SVH_REMARKS", mRow("SVH_REMARKS"), SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[ASSETS].[SaveAssetServiceHeader]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(7).Value = "" Then
                SVH_ID = sqlParam(0).Value
                Dim isRowDeleted As Boolean = False
                For Each mRow In SVD_Detail.ServiceFooter.Rows
                    isRowDeleted = False
                    If mRow.RowState = DataRowState.Deleted Then
                        isRowDeleted = True
                        mRow.RejectChanges()
                    End If
                    ReDim sqlParam(8)
                    sqlParam(0) = Mainclass.CreateSqlParameter("@SVD_ID", mRow("SVD_ID"), SqlDbType.Int, True)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@SVD_SVH_ID", SVH_ID, SqlDbType.Int)
                    sqlParam(2) = Mainclass.CreateSqlParameter("@SVD_LOC_ID", mRow("SVD_LOC_ID"), SqlDbType.Int)
                    sqlParam(3) = Mainclass.CreateSqlParameter("@SVD_ASM_ID", mRow("SVD_ASM_ID"), SqlDbType.Int)
                    sqlParam(4) = Mainclass.CreateSqlParameter("@SVD_bCOMPLETED", mRow("SVD_bCOMPLETED"), SqlDbType.Bit)
                    sqlParam(5) = Mainclass.CreateSqlParameter("@SVD_REMARKS", mRow("SVD_REMARKS"), SqlDbType.VarChar)
                    sqlParam(6) = Mainclass.CreateSqlParameter("@SVD_SERVICEDT", mRow("SVD_SERVICEDT"), SqlDbType.DateTime)
                    sqlParam(7) = Mainclass.CreateSqlParameter("@IsDeleted", isRowDeleted, SqlDbType.Bit)
                    sqlParam(8) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
                    Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[ASSETS].[SaveAssetServiceFooter]", sqlParam)
                    If (Retval = "0" Or Retval = "") And sqlParam(8).Value = "" Then
                    Else
                        Success = False
                        ErrorMsg = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(8).Value)
                        Exit For
                    End If
                Next
            Else
                Success = False
                ErrorMsg = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(7).Value)
            End If
            If (Success) Then
                SaveAssetServiceDetail = ""
                stTrans.Commit()
            Else
                stTrans.Rollback()
                SaveAssetServiceDetail = ErrorMsg
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Shared Function DeleteAssetServiceDetail(ByVal SVH_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SVH_ID", SVH_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetServiceDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetServiceDetail = ""
            Else
                DeleteAssetServiceDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

End Class