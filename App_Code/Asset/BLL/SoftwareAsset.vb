﻿Imports Microsoft.VisualBasic

Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Public Class SoftwareAsset
    Public AST_DESCR As String
    Public AST_CAD_ID As String
    Public AST_CAD_DESCR As String
    Public AST_INV_NO As String
    Public AST_SPL_ID As String
    Public AST_ACT_ID As String
    Public AST_BSU_ID As String
    Public AST_LOC_ID As Integer
    Public AST_LOC_DESCR As String

    Public SWA_MAN_ID As String
    Public SWA_VERSION As String
    Public SWA_LIC_NO As String
    Public SWA_SUBSCRIPTION_STDT As String
    Public SWA_SUBSCRIPTION_TILLDT As String
    Public SWA_RENEWAL_DT As String

    'Public AST_MAN_ID As String
    'Public AST_MAN_DESCR As String
    'Public AST_BRAND_ID As String
    'Public AST_BRAND_DESCR As String
    'Public AST_MODEL_NO As String
    'Public AST_SL_NO As String
    'Public AST_VALUE As Double
    'Public AST_bHasWarranty As Boolean
    'Public AST_WAR_ST_DT As String
    'Public AST_WAR_END_DT As String
    'Public AST_HasInsurance As Boolean
    'Public AST_INS_COMP_ID As String
    'Public AST_INS_POL_NO As String
    'Public AST_INS_ST_DT As String
    'Public AST_INS_END_DT As String
    'Public AST_INS_VALUE As Double
    Public AST_PARENT_ID As String
    Public AST_bAllowChangeStatus As Boolean
    Public AST_bAllowChangeLocation As Boolean
    Public AST_bAllowChangeAssign As Boolean
    Public AST_PICT_PATH As String
    Public AST_bResponsibleAsset As Boolean
    Public AST_ALLOC_EMP_ID As String
    Public AST_ALLOC_FROM_DT As String
    Public AST_CONDITION As String
    Public AST_STATUS As String
    Public AST_bAllocated As Boolean
    Public AST_DATE As String
    Public AST_INV_DATE As String
    Public AST_NUMBER As Integer
    Public AST_PUR_NO As String
    Public AST_PUR_DATE As String
    Public AST_DEL_NO As String
    Public AST_DEL_DATE As String
    Public AST_ASSET_COUNT As Integer

    Public Function HelloWorld() As String
        Return ""
    End Function

    Public Shared Function CreateDataTableAsset() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SL_NO"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AST_LOC"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AST_CAT"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AST_MAN"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AST_BRAND"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AST_MODEL"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AST_NO"
        dt.Columns.Add(column)

        Return dt
    End Function
End Class
