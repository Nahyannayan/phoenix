﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports UtilityObj

Public Class MasterDetails
    Public LocationDetail As DataRow
    Public Shared Function GetAssetLocationDetail(ByVal LOC_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@LOC_ID", LOC_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetLocationDetail]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SaveAssetLocationDetail(ByVal Locationdetail As DataRow, ByRef LOC_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(20) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@LOC_ID", Locationdetail("LOC_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@LOC_DESCR", Locationdetail("LOC_DESCR"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", Locationdetail("BSU_ID"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@LTM_ID", Locationdetail("LTM_ID"), SqlDbType.Int)
            sqlParam(4) = Mainclass.CreateSqlParameter("@LOM_ID", Locationdetail("LOM_ID"), SqlDbType.Int)
            sqlParam(5) = Mainclass.CreateSqlParameter("@CIT_ID", Locationdetail("CIT_ID"), SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@DPT_ID", Locationdetail("DPT_ID"), SqlDbType.Int)
            sqlParam(7) = Mainclass.CreateSqlParameter("@LOC_bActive", Locationdetail("LOC_bActive"), SqlDbType.Bit)
            sqlParam(8) = Mainclass.CreateSqlParameter("@LOC_RENT", Locationdetail("LOC_RENT"), SqlDbType.Float)
            sqlParam(9) = Mainclass.CreateSqlParameter("@LOC_DEPOSIT", Locationdetail("LOC_DEPOSIT"), SqlDbType.Float)
            sqlParam(10) = Mainclass.CreateSqlParameter("@LOC_PARTY", Locationdetail("LOC_PARTY"), SqlDbType.VarChar)
            sqlParam(11) = Mainclass.CreateSqlParameter("@LOC_PARTY_NAME", Locationdetail("LOC_PARTY_NAME"), SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@LOC_NOofROOMS", Locationdetail("LOC_NOofROOMS"), SqlDbType.Float)
            sqlParam(13) = Mainclass.CreateSqlParameter("@LOC_ROOM_NO", Locationdetail("LOC_ROOM_NO"), SqlDbType.Float)
            sqlParam(14) = Mainclass.CreateSqlParameter("@LOC_ADDRESS", Locationdetail("LOC_ADDRESS"), SqlDbType.VarChar)
            sqlParam(15) = Mainclass.CreateSqlParameter("@LOC_CONTACTPERSON", Locationdetail("LOC_CONTACTPERSON"), SqlDbType.VarChar)
            sqlParam(16) = Mainclass.CreateSqlParameter("@LOC_CONTACT_NO", Locationdetail("LOC_CONTACT_NO"), SqlDbType.VarChar)
            sqlParam(17) = Mainclass.CreateSqlParameter("@LOC_CON_STARTDT", Locationdetail("LOC_CON_STARTDT"), SqlDbType.DateTime)
            sqlParam(18) = Mainclass.CreateSqlParameter("@LOC_CON_EXP_DATE", Locationdetail("LOC_CON_EXP_DATE"), SqlDbType.DateTime)
            sqlParam(19) = Mainclass.CreateSqlParameter("@LOC_LFM_IDs", Locationdetail("LOC_LFM_IDs"), SqlDbType.VarChar)
            sqlParam(20) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[SaveAssetLocationDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(20).Value = "" Then
                SaveAssetLocationDetail = ""
                LOC_ID = sqlParam(0).Value
            Else
                SaveAssetLocationDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(20).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteAssetLocationDetail(ByVal LOC_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@LOC_ID", LOC_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetLocationDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetLocationDetail = ""
            Else
                DeleteAssetLocationDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetAssetMakeDetail(ByVal MKE_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@MKE_ID", MKE_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetMakeDetail]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SaveAssetMakeDetail(ByVal MakeDetail As DataRow, ByRef MKE_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(3) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@MKE_ID", MakeDetail("MKE_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@MKE_DESCR", MakeDetail("MKE_DESCR"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@MKE_ASC_IDs", MakeDetail("MKE_ASC_IDs"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[SaveAssetMakeDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(3).Value = "" Then
                SaveAssetMakeDetail = ""
                MKE_ID = sqlParam(0).Value
            Else
                SaveAssetMakeDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(3).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteAssetMakeDetail(ByVal MKE_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@MKE_ID", MKE_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetMakeDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetMakeDetail = ""
            Else
                DeleteAssetMakeDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetAssetModelDetail(ByVal MDL_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@MDL_ID", MDL_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetModelDetail]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SaveAssetModelDetail(ByVal ModelDetail As DataRow, ByRef MDL_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(4) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@MDL_ID", ModelDetail("MDL_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@MDL_DESCR", ModelDetail("MDL_DESCR"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@MDL_ASC_ID", ModelDetail("MDL_ASC_ID"), SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@MDL_MKE_IDs", ModelDetail("MDL_MKE_IDs"), SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[SaveAssetModelDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(4).Value = "" Then
                SaveAssetModelDetail = ""
                MDL_ID = sqlParam(0).Value
            Else
                SaveAssetModelDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(4).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteAssetModelDetail(ByVal MDL_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@MDL_ID", MDL_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetModelDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetModelDetail = ""
            Else
                DeleteAssetModelDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetLocationFacilityDetail(ByVal LFM_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@LFM_ID", LFM_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("[ASSETS].[GetLocationFacilityDetail]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SaveLocationFacilityDetail(ByVal LFMDetail As DataRow, ByRef LFM_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(2) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@LFM_ID", LFMDetail("LFM_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@LFM_DESCR", LFMDetail("LFM_DESCR"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[SaveLocationFacilityDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(2).Value = "" Then
                SaveLocationFacilityDetail = ""
                LFM_ID = sqlParam(0).Value
            Else
                SaveLocationFacilityDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(2).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteLocationFacilityDetail(ByVal LFM_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@LFM_ID", LFM_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteLocationFacilityDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteLocationFacilityDetail = ""
            Else
                DeleteLocationFacilityDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetAssetSubCategoryDetail(ByVal ASC_ID As Integer) As DataRow
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASC_ID", ASC_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetSubCategoryDetail]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                If mTable.Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mTable.NewRow
                    mTable.Rows.Add(mrow)
                End If
                Return mTable.Rows(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SaveAssetSubCategoryDetail(ByVal SubCatDetail As DataRow, ByRef ASC_ID As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(5) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASC_ID", SubCatDetail("ASC_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ASC_DESCR", SubCatDetail("ASC_DESCR"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ASC_ACM_ID", SubCatDetail("ASC_ACM_ID"), SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ASC_CODE", SubCatDetail("ASC_CODE"), SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ASC_DEP_RATE", SubCatDetail("ASC_DEP_RATE"), SqlDbType.Float)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[SaveAssetSubCategoryDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(5).Value = "" Then
                SaveAssetSubCategoryDetail = ""
                ASC_ID = sqlParam(0).Value
            Else
                SaveAssetSubCategoryDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(5).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteAssetSubCategoryDetail(ByVal ASC_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASC_ID", ASC_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetSubCategoryDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetSubCategoryDetail = ""
            Else
                DeleteAssetSubCategoryDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
End Class

