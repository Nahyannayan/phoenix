﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports UtilityObj
Public Class AssetAllocation
    Structure AllocationDetail
        Public TransferHeader As DataRow
        Public TransferFooter As DataTable
    End Structure
    Public Shared Function GetAssetAllocationTransaction(ByVal TRH_ID As String) As AllocationDetail
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TRH_ID", TRH_ID, SqlDbType.VarChar)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetAllocationTransaction]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                Dim TRD_Detail As New AllocationDetail
                If mSet.Tables(0).Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mSet.Tables(0).NewRow
                    mSet.Tables(0).Rows.Add(mrow)
                End If
                TRD_Detail.TransferHeader = mSet.Tables(0).Rows(0)
                If mSet.Tables.Count > 1 Then
                    Dim mtable As New DataTable
                    Dim dcID As New DataColumn("ID", GetType(Integer))
                    dcID.AutoIncrement = True
                    dcID.AutoIncrementSeed = 1
                    dcID.AutoIncrementStep = 1
                    mtable.Columns.Add(dcID)
                    mtable.Merge(mSet.Tables(1))
                    TRD_Detail.TransferFooter = mtable
                    TRD_Detail.TransferFooter.AcceptChanges()
                End If
                GetAssetAllocationTransaction = TRD_Detail
            Else
                GetAssetAllocationTransaction = Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            GetAssetAllocationTransaction = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function SaveAssetAllocationDetail(ByRef TRD_Detail As AllocationDetail, ByRef TRH_ID As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Dim sqlParam(11) As SqlParameter
        Try
            Dim mRow As DataRow
            Dim Retval As String
            Dim Success As Boolean = True
            Dim ErrorMsg As String = ""
            mRow = TRD_Detail.TransferHeader
            ReDim sqlParam(13)
            sqlParam(0) = Mainclass.CreateSqlParameter("@TRH_ID", mRow("TRH_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@TRH_BSU_ID", mRow("TRH_BSU_ID"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@TRH_TO_BSU_ID", mRow("TRH_TO_BSU_ID"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@TRH_TYM_ID", mRow("TRH_TYM_ID"), SqlDbType.Int)
            sqlParam(4) = Mainclass.CreateSqlParameter("@TRH_FROMDT", mRow("TRH_FROMDT"), SqlDbType.DateTime)
            sqlParam(5) = Mainclass.CreateSqlParameter("@TRH_EMP_ID", mRow("TRH_EMP_ID"), SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@TRH_ENTEREDBY", mRow("TRH_ENTEREDBY"), SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@TRH_RECEIVEDBY", mRow("TRH_RECEIVEDBY"), SqlDbType.VarChar)
            sqlParam(8) = Mainclass.CreateSqlParameter("@TRH_bApproved", mRow("TRH_bApproved"), SqlDbType.Bit)
            sqlParam(9) = Mainclass.CreateSqlParameter("@TRH_rApproved", mRow("TRH_rApproved"), SqlDbType.Bit)
            sqlParam(10) = Mainclass.CreateSqlParameter("@TRH_TO_BSU_bApproved", mRow("TRH_TO_BSU_bApproved"), SqlDbType.Bit)
            sqlParam(11) = Mainclass.CreateSqlParameter("@TRH_REMARKS", mRow("TRH_REMARKS"), SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@TRH_HHTDATA", mRow("TRH_HHTDATA"), SqlDbType.VarChar)
            sqlParam(13) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[ASSETS].[SaveAssetAllocationHeader]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(13).Value = "" Then
                TRH_ID = sqlParam(0).Value
                Dim isRowDeleted As Boolean = False
                For Each mRow In TRD_Detail.TransferFooter.Rows
                    isRowDeleted = False
                    If mRow.RowState = DataRowState.Deleted Then
                        isRowDeleted = True
                        mRow.RejectChanges()
                    End If
                    ReDim sqlParam(8)
                    sqlParam(0) = Mainclass.CreateSqlParameter("@TRS_ID", mRow("TRS_ID"), SqlDbType.Int, True)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@TRS_TRH_ID", TRH_ID, SqlDbType.Int)
                    sqlParam(2) = Mainclass.CreateSqlParameter("@TRS_FROM_LOC_ID", mRow("TRS_FROM_LOC_ID"), SqlDbType.Int)
                    sqlParam(3) = Mainclass.CreateSqlParameter("@TRS_TO_LOC_ID", mRow("TRS_TO_LOC_ID"), SqlDbType.Int)
                    sqlParam(4) = Mainclass.CreateSqlParameter("@TRS_ASM_ID", mRow("TRS_ASM_ID"), SqlDbType.Int)
                    sqlParam(5) = Mainclass.CreateSqlParameter("@TRS_REMARKS", mRow("TRS_REMARKS"), SqlDbType.VarChar)
                    sqlParam(6) = Mainclass.CreateSqlParameter("@TRS_TO_BSU_RECEIVED", mRow("TRS_TO_BSU_RECEIVED"), SqlDbType.Bit)
                    sqlParam(7) = Mainclass.CreateSqlParameter("@IsDeleted", isRowDeleted, SqlDbType.Bit)
                    sqlParam(8) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
                    Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[ASSETS].[SaveAssetAllocationFooter]", sqlParam)
                    If (Retval = "0" Or Retval = "") And sqlParam(8).Value = "" Then
                    Else
                        Success = False
                        ErrorMsg = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(8).Value)
                        Exit For
                    End If
                Next
            Else
                Success = False
                ErrorMsg = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(13).Value)
            End If
            If (Success) Then
                SaveAssetAllocationDetail = ""
                stTrans.Commit()
            Else
                stTrans.Rollback()
                SaveAssetAllocationDetail = ErrorMsg
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Shared Function DeleteAssetAllocationDetail(ByVal TRH_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TRH_ID", TRH_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetAllocationDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetAllocationDetail = ""
            Else
                DeleteAssetAllocationDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

End Class



