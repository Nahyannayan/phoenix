﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class asset_Common
#Region "SELECT STATEMENTS"
    Public Shared Function GETPURCHASE_ORDER_DATA(ByVal POS_POM_ID As String) As DataSet
        Using connection As SqlConnection = ConnectionManger.GetOASIS_ASSETConnection
            Dim sqlPURCHASE_ORDER_DETAILS As String = ""

            sqlPURCHASE_ORDER_DETAILS = "SELECT POS_ID,POS_MAN_ID, POS_MDM_ID, POS_ITEM_DESCR, POS_QTY, POS_RATE, " & _
 "  POS_WARRanty, POS_REMARKS,(select top 1 MDM_DESCR from MODEL_M where MDM_ID=POS_MDM_ID) as POS_MODEL, " & _
" (select top 1 MAN_DESCR from  MANUFACTURE_M where MAN_ID=POS_MAN_ID) as POS_MANUF,'OLD' AS STATUS,  " & _
" (SELECT REPLACE((select CONVERT(VARCHAR(20),POD_BSU_ID) + '_' + CAST(POD_QTY AS VARCHAR)+ CASE WHEN " & _
" POD_REC_QTY IS NULL THEN '-1' WHEN POD_REC_QTY=0 THEN '-1' ELSE '-0' END +'|'  AS [text()] FROM  PURCHASE_ORDER_D " & _
" WHERE POD_POS_ID=POS_ID FOR XML PATH('')),' ',',')) as bsu_qty " & _
" FROM   Purchase_Order_S WHERE POS_POM_ID='" & POS_POM_ID & "' ORDER BY POS_ITEM_DESCR "


            Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.Text, sqlPURCHASE_ORDER_DETAILS)
            Return ds
        End Using


    End Function





#End Region

#Region "Save Statements"

    Public Shared Function SavePurchase_Order(ByVal POM_NO As String, ByVal POM_DT As String, ByVal POM_ALLOC_BSU_ID As String, ByVal POM_SPL_ID As String, ByVal POM_CONT_PERSON As String, ByVal POM_CONT_NO1 As String, _
                ByVal POM_CONT_NO2 As String, ByVal POM_PAYMENT As String, ByVal POM_DELIVERY_CONT As String, ByVal POM_DELIVERY_DT As String, ByVal POM_DELIVERY_LOC As String, ByVal POM_DELIVERY_INVOICE As String, _
                ByVal POM_DELIVERY_REMARKS As String, ByVal POM_FILE_PATH As String, ByVal POS_POM_ID As String, ByVal str As String, ByVal str_D As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASIS_ASSETConnection
            Try
                Dim pParms(20) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@POM_NO", POM_NO)
                pParms(1) = New SqlClient.SqlParameter("@POM_DT", POM_DT)
                pParms(2) = New SqlClient.SqlParameter("@POM_ALLOC_BSU_ID", POM_ALLOC_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@POM_SPL_ID", POM_SPL_ID)
                pParms(4) = New SqlClient.SqlParameter("@POM_CONT_PERSON", POM_CONT_PERSON)
                pParms(5) = New SqlClient.SqlParameter("@POM_CONT_NO1", POM_CONT_NO1)
                pParms(6) = New SqlClient.SqlParameter("@POM_CONT_NO2", POM_CONT_NO2)
                pParms(7) = New SqlClient.SqlParameter("@POM_PAYMENT", POM_PAYMENT)
                pParms(8) = New SqlClient.SqlParameter("@POM_DELIVERY_CONT", POM_DELIVERY_CONT)
                pParms(9) = New SqlClient.SqlParameter("@POM_DELIVERY_DT", POM_DELIVERY_DT)
                pParms(10) = New SqlClient.SqlParameter("@POM_DELIVERY_LOC", POM_DELIVERY_LOC)
                pParms(11) = New SqlClient.SqlParameter("@POM_DELIVERY_INVOICE", POM_DELIVERY_INVOICE)
                pParms(12) = New SqlClient.SqlParameter("@POM_DELIVERY_REMARKS", POM_DELIVERY_REMARKS)
                pParms(13) = New SqlClient.SqlParameter("@POM_FILE_PATH", POM_FILE_PATH)
                pParms(14) = New SqlClient.SqlParameter("@POS_POM_ID", POS_POM_ID)
                pParms(15) = New SqlClient.SqlParameter("@str", str)
                pParms(16) = New SqlClient.SqlParameter("@str_D", str_D)
                pParms(17) = New SqlClient.SqlParameter("@bEdit", bEdit)
                pParms(18) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(18).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SavePurchase_Order", pParms)
                Dim ReturnFlag As Integer = pParms(18).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function
#End Region
End Class
