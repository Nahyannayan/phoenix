﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports UtilityObj
Public Class AssetDetails
    Structure AssetDetail
        Public ASM_ID As Integer
        Public ASI_ID As Integer
        Public ASM_ASC_ID As Integer
        Public ASM_CODE As String
        Public ASM_DESCR As String
        Public ASM_MAKE As String
        Public ASM_MAKE_DESC As String
        Public ASM_NEW_MAKE_DESC As String
        Public ASM_MODEL As String
        Public ASM_MODEL_DESC As String
        Public ASM_NEW_MODEL_DESC As String
        Public ASM_BSU_ID As String
        Public ASM_SUP_ACT_ID As String
        Public ASM_PUR_DT As String
        Public ASM_INVOICENO As String
        Public ASM_PO_NO As String
        Public ASM_CUR_ID As String
        Public ASM_PRICE As Double
        Public ASM_DEP_RATE As Double
        Public ASM_LOC_ID As Int16
        Public ASM_STATUS As Int16
        Public ASM_REMARKS As String
        Public ASM_DISP_DT As String
        Public ASM_ASC_DESC As String
        Public ASM_ACT_NAME As String
        Public ASM_BSU_NAME As String
        Public ASM_LOC_NAME As String
        Public ASM_INSUR_ACT_ID As String
        Public ASM_INSUR_ACT_NAME As String
        Public ASM_INSUR_EXP_DT As String
        Public ASM_INSUR_AMOUNT As Double
        Public ASM_SERIALNO As String
        Public ASM_CHASSISNO As String
        Public ASM_IMAGE() As Byte
        Public ASM_AMC_BY As String
        Public ASM_AMC_BY_NAME As String
        Public ASM_AMC_EXP_DT As String
        Public ASM_ENTERED_BY As String
        Public ASM_QTY As Int16
        Public ASM_SERIALNOs As String
        Public ASM_CHASSISNOs As String
        Public ASM_DOCNO As String
        Public TBLSERIAL As DataTable
    End Structure
    Public Shared Function GetAssetMasterDetail(ByVal ASM_ID As Integer) As AssetDetail
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASM_ID", ASM_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetMasterDetail]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
            End If
            If mTable.Rows.Count > 0 Then
                Dim ASM_Detail As New AssetDetail
                ASM_Detail.ASM_ID = mTable.Rows(0).Item("ASM_ID")
                ASM_Detail.ASM_ASC_ID = mTable.Rows(0).Item("ASM_ASC_ID")
                ASM_Detail.ASM_CODE = mTable.Rows(0).Item("ASM_CODE").ToString
                ASM_Detail.ASM_DESCR = mTable.Rows(0).Item("ASM_DESCR").ToString
                ASM_Detail.ASM_MAKE = mTable.Rows(0).Item("ASM_MAKE").ToString
                ASM_Detail.ASM_MAKE_DESC = mTable.Rows(0).Item("ASM_MAKE_DESC").ToString
                ASM_Detail.ASM_MODEL = mTable.Rows(0).Item("ASM_MODEL").ToString
                ASM_Detail.ASM_MODEL_DESC = mTable.Rows(0).Item("ASM_MODEL_DESC").ToString
                ASM_Detail.ASM_BSU_ID = mTable.Rows(0).Item("ASM_BSU_ID").ToString
                ASM_Detail.ASM_SUP_ACT_ID = mTable.Rows(0).Item("ASM_SUP_ACT_ID").ToString
                ASM_Detail.ASM_PUR_DT = mTable.Rows(0).Item("ASM_PUR_DT").ToString
                ASM_Detail.ASM_INVOICENO = mTable.Rows(0).Item("ASM_INVOICENO")
                ASM_Detail.ASM_PO_NO = mTable.Rows(0).Item("ASM_PO_NO").ToString.ToString
                ASM_Detail.ASM_CUR_ID = mTable.Rows(0).Item("ASM_CUR_ID").ToString
                ASM_Detail.ASM_PRICE = mTable.Rows(0).Item("ASM_PRICE")
                ASM_Detail.ASM_DEP_RATE = mTable.Rows(0).Item("ASM_DEP_RATE")
                ASM_Detail.ASM_LOC_ID = mTable.Rows(0).Item("ASM_LOC_ID")
                ASM_Detail.ASM_STATUS = mTable.Rows(0).Item("ASM_STATUS").ToString
                ASM_Detail.ASM_REMARKS = mTable.Rows(0).Item("ASM_REMARKS").ToString
                ASM_Detail.ASM_DISP_DT = mTable.Rows(0).Item("ASM_DISP_DT").ToString
                ASM_Detail.ASM_ASC_DESC = mTable.Rows(0).Item("ASM_ASC_DESC").ToString
                ASM_Detail.ASM_ACT_NAME = mTable.Rows(0).Item("ASM_ACT_NAME").ToString
                ASM_Detail.ASM_BSU_NAME = mTable.Rows(0).Item("ASM_BSU_NAME").ToString
                ASM_Detail.ASM_LOC_NAME = mTable.Rows(0).Item("ASM_LOC_NAME").ToString
                ASM_Detail.ASM_INSUR_ACT_ID = mTable.Rows(0).Item("ASM_INSUR_ACT_ID").ToString
                ASM_Detail.ASM_INSUR_ACT_NAME = mTable.Rows(0).Item("ASM_INSUR_ACT_NAME").ToString
                ASM_Detail.ASM_INSUR_EXP_DT = mTable.Rows(0).Item("ASM_INSUR_EXP_DT").ToString
                ASM_Detail.ASM_INSUR_AMOUNT = mTable.Rows(0).Item("ASM_INSUR_AMOUNT")
                ASM_Detail.ASM_SERIALNO = mTable.Rows(0).Item("ASM_SERIALNO").ToString
                ASM_Detail.ASM_CHASSISNO = mTable.Rows(0).Item("ASM_CHASSISNO").ToString
                If Not IsDBNull(mTable.Rows(0).Item("ASM_IMAGE")) Then
                    ASM_Detail.ASM_IMAGE = mTable.Rows(0).Item("ASM_IMAGE")
                Else
                    ASM_Detail.ASM_IMAGE = Nothing
                End If
                ASM_Detail.ASM_AMC_BY = mTable.Rows(0).Item("ASM_AMC_BY").ToString
                ASM_Detail.ASM_AMC_BY_NAME = mTable.Rows(0).Item("ASM_AMC_BY_NAME").ToString
                ASM_Detail.ASM_AMC_EXP_DT = mTable.Rows(0).Item("ASM_AMC_EXP_DT").ToString
                Return ASM_Detail
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetAssetInitDetail(ByVal ASI_ID As Integer) As AssetDetail
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASI_ID", ASI_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("[ASSETS].[GetAssetInitDetail]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
            Else
                Return Nothing
            End If
            Dim ASI_Detail As New AssetDetail
            If mTable.Rows.Count > 0 Then
                ASI_Detail.ASI_ID = mTable.Rows(0).Item("ASI_ID")
                ASI_Detail.ASM_ASC_ID = mTable.Rows(0).Item("ASI_ASC_ID")
                ASI_Detail.ASM_DESCR = mTable.Rows(0).Item("ASI_DESCR").ToString
                ASI_Detail.ASM_MAKE = mTable.Rows(0).Item("ASI_MAKE").ToString
                ASI_Detail.ASM_MAKE_DESC = mTable.Rows(0).Item("ASI_MAKE_DESC").ToString
                ASI_Detail.ASM_MODEL = mTable.Rows(0).Item("ASI_MODEL").ToString
                ASI_Detail.ASM_MODEL_DESC = mTable.Rows(0).Item("ASI_MODEL_DESC").ToString
                ASI_Detail.ASM_BSU_ID = mTable.Rows(0).Item("ASI_BSU_ID").ToString
                ASI_Detail.ASM_SUP_ACT_ID = mTable.Rows(0).Item("ASI_SUP_ACT_ID").ToString
                ASI_Detail.ASM_PUR_DT = mTable.Rows(0).Item("ASI_PUR_DT").ToString
                ASI_Detail.ASM_INVOICENO = mTable.Rows(0).Item("ASI_INVOICENO")
                ASI_Detail.ASM_PO_NO = mTable.Rows(0).Item("ASI_PO_NO").ToString.ToString
                ASI_Detail.ASM_CUR_ID = mTable.Rows(0).Item("ASI_CUR_ID").ToString
                ASI_Detail.ASM_PRICE = mTable.Rows(0).Item("ASI_PRICE")
                ASI_Detail.ASM_DEP_RATE = mTable.Rows(0).Item("ASI_DEP_RATE")
                ASI_Detail.ASM_LOC_ID = mTable.Rows(0).Item("ASI_LOC_ID")
                ASI_Detail.ASM_STATUS = mTable.Rows(0).Item("ASI_STATUS").ToString
                ASI_Detail.ASM_REMARKS = mTable.Rows(0).Item("ASI_REMARKS").ToString
                ASI_Detail.ASM_ASC_DESC = mTable.Rows(0).Item("ASI_ASC_DESC").ToString
                ASI_Detail.ASM_ACT_NAME = mTable.Rows(0).Item("ASI_ACT_NAME").ToString
                ASI_Detail.ASM_BSU_NAME = mTable.Rows(0).Item("ASI_BSU_NAME").ToString
                ASI_Detail.ASM_LOC_NAME = mTable.Rows(0).Item("ASI_LOC_NAME").ToString
                ASI_Detail.ASM_INSUR_ACT_ID = mTable.Rows(0).Item("ASI_INSUR_ACT_ID").ToString
                ASI_Detail.ASM_INSUR_ACT_NAME = mTable.Rows(0).Item("ASI_INSUR_ACT_NAME").ToString
                ASI_Detail.ASM_INSUR_EXP_DT = mTable.Rows(0).Item("ASI_INSUR_EXP_DT").ToString
                ASI_Detail.ASM_INSUR_AMOUNT = mTable.Rows(0).Item("ASI_INSUR_AMOUNT")
                ASI_Detail.ASM_QTY = Val(mTable.Rows(0).Item("ASI_QTY"))
                ASI_Detail.ASM_DOCNO = mTable.Rows(0).Item("ASI_DOCNO").ToString
                If Not IsDBNull(mTable.Rows(0).Item("ASI_IMAGE")) Then
                    ASI_Detail.ASM_IMAGE = mTable.Rows(0).Item("ASI_IMAGE")
                Else
                    ASI_Detail.ASM_IMAGE = Nothing
                End If
                ASI_Detail.ASM_AMC_BY = mTable.Rows(0).Item("ASI_AMC_BY").ToString
                ASI_Detail.ASM_AMC_BY_NAME = mTable.Rows(0).Item("ASI_AMC_BY_NAME").ToString
                ASI_Detail.ASM_AMC_EXP_DT = mTable.Rows(0).Item("ASI_AMC_EXP_DT").ToString
                ASI_Detail.ASM_NEW_MAKE_DESC = mTable.Rows(0).Item("ASI_NEW_MKE_DESCR").ToString
                ASI_Detail.ASM_NEW_MODEL_DESC = mTable.Rows(0).Item("ASI_NEW_MDL_DESCR").ToString
            End If
            If mSet.Tables.Count > 1 Then
                ASI_Detail.TBLSERIAL = mSet.Tables(1)
            End If

            Return ASI_Detail
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function SaveAssetMasterDetail(ByRef ASM_Detail As AssetDetail) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(26) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASM_ID", ASM_Detail.ASM_ID, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ASM_ASC_ID", ASM_Detail.ASM_ASC_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ASM_DESCR", ASM_Detail.ASM_DESCR, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ASM_MAKE", ASM_Detail.ASM_MAKE, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ASM_MODEL", ASM_Detail.ASM_MODEL, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ASM_BSU_ID", ASM_Detail.ASM_BSU_ID, SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@ASM_SUP_ACT_ID", ASM_Detail.ASM_SUP_ACT_ID, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@ASM_PUR_DT", ASM_Detail.ASM_PUR_DT, SqlDbType.DateTime)
            sqlParam(8) = Mainclass.CreateSqlParameter("@ASM_INVOICENO", ASM_Detail.ASM_INVOICENO, SqlDbType.VarChar)
            sqlParam(9) = Mainclass.CreateSqlParameter("@ASM_PO_NO", ASM_Detail.ASM_PO_NO, SqlDbType.VarChar)
            sqlParam(10) = Mainclass.CreateSqlParameter("@ASM_CUR_ID", ASM_Detail.ASM_CUR_ID, SqlDbType.VarChar)
            sqlParam(11) = Mainclass.CreateSqlParameter("@ASM_PRICE", ASM_Detail.ASM_PRICE, SqlDbType.Float)
            sqlParam(12) = Mainclass.CreateSqlParameter("@ASM_DEP_RATE", ASM_Detail.ASM_DEP_RATE, SqlDbType.Float)
            sqlParam(13) = Mainclass.CreateSqlParameter("@ASM_LOC_ID", ASM_Detail.ASM_LOC_ID, SqlDbType.VarChar)
            sqlParam(14) = Mainclass.CreateSqlParameter("@ASM_STATUS", ASM_Detail.ASM_STATUS, SqlDbType.Int)
            sqlParam(15) = Mainclass.CreateSqlParameter("@ASM_REMARKS", ASM_Detail.ASM_REMARKS, SqlDbType.VarChar)
            sqlParam(16) = Mainclass.CreateSqlParameter("@ASM_INSUR_ACT_ID", ASM_Detail.ASM_INSUR_ACT_ID, SqlDbType.VarChar)
            sqlParam(17) = Mainclass.CreateSqlParameter("@ASM_INSUR_EXP_DT", ASM_Detail.ASM_INSUR_EXP_DT, SqlDbType.DateTime)
            sqlParam(18) = Mainclass.CreateSqlParameter("@ASM_INSUR_AMOUNT", ASM_Detail.ASM_INSUR_AMOUNT, SqlDbType.Float)
            sqlParam(19) = Mainclass.CreateSqlParameter("@ASM_SERIALNO", ASM_Detail.ASM_SERIALNO, SqlDbType.VarChar)
            sqlParam(20) = Mainclass.CreateSqlParameter("@ASM_IMAGE", ASM_Detail.ASM_IMAGE, SqlDbType.Binary)
            sqlParam(21) = Mainclass.CreateSqlParameter("@ASM_AMC_BY", ASM_Detail.ASM_AMC_BY, SqlDbType.VarChar)
            sqlParam(22) = Mainclass.CreateSqlParameter("@ASM_AMC_EXP_DT", ASM_Detail.ASM_AMC_EXP_DT, SqlDbType.DateTime)
            sqlParam(23) = Mainclass.CreateSqlParameter("@ASM_QTY", ASM_Detail.ASM_QTY, SqlDbType.Int)
            sqlParam(24) = Mainclass.CreateSqlParameter("@ASM_ENTERED_BY", ASM_Detail.ASM_ENTERED_BY, SqlDbType.VarChar)
            sqlParam(25) = Mainclass.CreateSqlParameter("@ASM_CHASSISNO", ASM_Detail.ASM_CHASSISNO, SqlDbType.VarChar)
            sqlParam(26) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[SaveAssetMasterDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(26).Value = "" Then
                SaveAssetMasterDetail = ""
                ASM_Detail.ASM_ID = sqlParam(0).Value
            Else
                SaveAssetMasterDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(26).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function SaveAssetInitDetail(ByRef ASI_Detail As AssetDetail) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(29) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASI_ID", ASI_Detail.ASI_ID, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ASI_ASC_ID", ASI_Detail.ASM_ASC_ID, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ASI_DESCR", ASI_Detail.ASM_DESCR, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ASI_MAKE", ASI_Detail.ASM_MAKE, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ASI_MODEL", ASI_Detail.ASM_MODEL, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ASI_BSU_ID", ASI_Detail.ASM_BSU_ID, SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@ASI_SUP_ACT_ID", ASI_Detail.ASM_SUP_ACT_ID, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@ASI_PUR_DT", ASI_Detail.ASM_PUR_DT, SqlDbType.DateTime)
            sqlParam(8) = Mainclass.CreateSqlParameter("@ASI_INVOICENO", ASI_Detail.ASM_INVOICENO, SqlDbType.VarChar)
            sqlParam(9) = Mainclass.CreateSqlParameter("@ASI_PO_NO", ASI_Detail.ASM_PO_NO, SqlDbType.VarChar)
            sqlParam(10) = Mainclass.CreateSqlParameter("@ASI_CUR_ID", ASI_Detail.ASM_CUR_ID, SqlDbType.VarChar)
            sqlParam(11) = Mainclass.CreateSqlParameter("@ASI_PRICE", ASI_Detail.ASM_PRICE, SqlDbType.Float)
            sqlParam(12) = Mainclass.CreateSqlParameter("@ASI_DEP_RATE", ASI_Detail.ASM_DEP_RATE, SqlDbType.Float)
            sqlParam(13) = Mainclass.CreateSqlParameter("@ASI_LOC_ID", ASI_Detail.ASM_LOC_ID, SqlDbType.VarChar)
            sqlParam(14) = Mainclass.CreateSqlParameter("@ASI_STATUS", ASI_Detail.ASM_STATUS, SqlDbType.Int)
            sqlParam(15) = Mainclass.CreateSqlParameter("@ASI_REMARKS", ASI_Detail.ASM_REMARKS, SqlDbType.VarChar)
            sqlParam(16) = Mainclass.CreateSqlParameter("@ASI_INSUR_ACT_ID", ASI_Detail.ASM_INSUR_ACT_ID, SqlDbType.VarChar)
            sqlParam(17) = Mainclass.CreateSqlParameter("@ASI_INSUR_EXP_DT", ASI_Detail.ASM_INSUR_EXP_DT, SqlDbType.DateTime)
            sqlParam(18) = Mainclass.CreateSqlParameter("@ASI_INSUR_AMOUNT", ASI_Detail.ASM_INSUR_AMOUNT, SqlDbType.Float)
            sqlParam(19) = Mainclass.CreateSqlParameter("@ASI_ENTERED_BY", ASI_Detail.ASM_ENTERED_BY, SqlDbType.VarChar)
            sqlParam(20) = Mainclass.CreateSqlParameter("@ASI_IMAGE", ASI_Detail.ASM_IMAGE, SqlDbType.Binary)
            sqlParam(21) = Mainclass.CreateSqlParameter("@ASI_AMC_BY", ASI_Detail.ASM_AMC_BY, SqlDbType.VarChar)
            sqlParam(22) = Mainclass.CreateSqlParameter("@ASI_AMC_EXP_DT", ASI_Detail.ASM_AMC_EXP_DT, SqlDbType.DateTime)
            sqlParam(23) = Mainclass.CreateSqlParameter("@ASI_QTY", ASI_Detail.ASM_QTY, SqlDbType.Int)
            sqlParam(24) = Mainclass.CreateSqlParameter("@ASI_SERIALNOs", ASI_Detail.ASM_SERIALNOs, SqlDbType.VarChar)
            sqlParam(25) = Mainclass.CreateSqlParameter("@ASI_CHASSISNOs", ASI_Detail.ASM_CHASSISNOs, SqlDbType.VarChar)
            sqlParam(26) = Mainclass.CreateSqlParameter("@ASI_DOCNO", ASI_Detail.ASM_DOCNO, SqlDbType.VarChar)
            sqlParam(27) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            sqlParam(28) = Mainclass.CreateSqlParameter("@ASI_NEW_MKE_DESCR", ASI_Detail.ASM_NEW_MAKE_DESC, SqlDbType.VarChar)
            sqlParam(29) = Mainclass.CreateSqlParameter("@ASI_NEW_MDL_DESCR", ASI_Detail.ASM_NEW_MODEL_DESC, SqlDbType.VarChar)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[SaveAssetInitDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(27).Value = "" Then
                SaveAssetInitDetail = ""
                ASI_Detail.ASI_ID = sqlParam(0).Value
            Else
                SaveAssetInitDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(27).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function ApproveAssetInitDetail(ByRef ASI_ID As Integer, ByVal DOC_NO As String, ByVal ApprovedBy As String, ByVal INVOICE_NO As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(3) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASI_ID", ASI_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@DOC_NO", DOC_NO, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ASI_APPROVED_BY", ApprovedBy, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String = "0"
            ApproveAssetInitDetail = ""
            If INVOICE_NO.Length > 0 Then
                Dim mtable As DataTable = Mainclass.getDataTable("select ASI_ID from ASSETS.ASSETS_INIT where ASI_INVOICENO='" & INVOICE_NO & "'", str_conn)
                For rowNo As Integer = 0 To mtable.Rows.Count - 1
                    sqlParam(0) = Mainclass.CreateSqlParameter("@ASI_ID", mtable.Rows(rowNo)(0), SqlDbType.Int)
                    Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[ApproveAssetInitDetail]", sqlParam)
                    If (Retval = "0" Or Retval = "") And sqlParam(3).Value = "" Then
                    Else
                        ApproveAssetInitDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(3).Value)
                        Exit Function
                    End If
                Next
            Else
                Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[ApproveAssetInitDetail]", sqlParam)
                If (Retval = "0" Or Retval = "") And sqlParam(3).Value = "" Then
                Else
                    ApproveAssetInitDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(3).Value)
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteAssetMasterDetail(ByVal ASM_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASM_ID", ASM_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetMasterDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetMasterDetail = ""
            Else
                DeleteAssetMasterDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteAssetInitDetail(ByVal ASI_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ASI_ID", ASI_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[ASSETS].[DeleteAssetInitDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteAssetInitDetail = ""
            Else
                DeleteAssetInitDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetTransferHistoryOfAsset(ByVal ASM_ID As Integer) As DataTable
        Try
            Dim mtable As New DataTable
            Dim sql As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            sql = "select replace(convert(varchar(30),AAL_FROMDT,106),' ','/') AAL_FROMDT,replace(convert(varchar(30),AAL_TODT,106),' ','/') AAL_TODT,  AAL_FROM_LOC_NAME,AAL_TO_LOC_NAME,AAL_EMP_NAME "
            sql &= " FROM ASSETS.VW_TRANSFERHISTORY "
            sql &= "     WHERE AAL_ASM_ID = " & ASM_ID
            sql &= "     ORDER BY AAL_FROMDT ASC"
            mtable = Mainclass.getDataTable(sql, str_conn)
            GetTransferHistoryOfAsset = mtable
        Catch ex As Exception
            Errorlog(ex.Message)
            GetTransferHistoryOfAsset = Nothing
            Throw ex
        End Try
    End Function

End Class


