﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports UtilityObj
Public Class PurchaseRequest
    Structure PurchaseRequestDetail
        Public PurchaseRequestHeader As DataRow
        Public PurchaseRequestFooter As DataTable
    End Structure
    Public Shared Function GetPurchaseRequestTransaction(ByVal PRH_ID As String) As PurchaseRequestDetail
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PRH_ID", PRH_ID, SqlDbType.VarChar)
            mSet = Mainclass.getDataSet("[GetPurchaseRequestTransaction]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                Dim TRD_Detail As New PurchaseRequestDetail
                If mSet.Tables(0).Rows.Count = 0 Then
                    Dim mrow As DataRow
                    mrow = mSet.Tables(0).NewRow
                    mSet.Tables(0).Rows.Add(mrow)
                End If
                TRD_Detail.PurchaseRequestHeader = mSet.Tables(0).Rows(0)
                If mSet.Tables.Count > 1 Then
                    Dim mtable As New DataTable
                    Dim dcID As New DataColumn("ID", GetType(Integer))
                    dcID.AutoIncrement = True
                    dcID.AutoIncrementSeed = 1
                    dcID.AutoIncrementStep = 1
                    mtable.Columns.Add(dcID)
                    mtable.Merge(mSet.Tables(1))
                    TRD_Detail.PurchaseRequestFooter = mtable
                    TRD_Detail.PurchaseRequestFooter.AcceptChanges()
                End If
                GetPurchaseRequestTransaction = TRD_Detail
            Else
                GetPurchaseRequestTransaction = Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            GetPurchaseRequestTransaction = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function SavePurchaseRequestDetail(ByRef TRD_Detail As PurchaseRequestDetail, ByRef PRH_ID As String, ByVal AUD_WINUSER As String, ByVal Aud_form As String, ByVal Aud_user As String, ByVal Aud_module As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Dim sqlParam(11) As SqlParameter
        Try
            Dim mRow As DataRow
            Dim Retval As String
            Dim Success As Boolean = True
            Dim ErrorMsg As String = ""
            mRow = TRD_Detail.PurchaseRequestHeader
            ReDim sqlParam(14)
            sqlParam(0) = Mainclass.CreateSqlParameter("@PRH_ID", mRow("PRH_ID"), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@PRH_BSU_ID", mRow("PRH_BSU_ID"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@PRH_BUDGET_REF", mRow("PRH_BUDGET_REF"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@PRH_TRDATE", mRow("PRH_TRDATE"), SqlDbType.DateTime)
            sqlParam(4) = Mainclass.CreateSqlParameter("@PRH_ENTEREDBY", mRow("PRH_ENTEREDBY"), SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@PRH_CUR_ID", mRow("PRH_CUR_ID"), SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@PRH_REMARKS", mRow("PRH_REMARKS"), SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@PRH_EXG", mRow("PRH_EXG"), SqlDbType.Float)
            sqlParam(8) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            sqlParam(9) = Mainclass.CreateSqlParameter("@PRH_bForward", mRow("PRH_bForward"), SqlDbType.Bit)
            sqlParam(10) = Mainclass.CreateSqlParameter("@PRH_DOC_ID", mRow("PRH_DOC_ID"), SqlDbType.VarChar)
            sqlParam(11) = Mainclass.CreateSqlParameter("@AUD_WINUSER", AUD_WINUSER, SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@Aud_form", Aud_form, SqlDbType.VarChar)
            sqlParam(13) = Mainclass.CreateSqlParameter("@Aud_user", Aud_user, SqlDbType.VarChar)
            sqlParam(14) = Mainclass.CreateSqlParameter("@Aud_module", Aud_module, SqlDbType.VarChar)
            Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[SavePurchaseRequestHeader]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(8).Value = "" Then
                PRH_ID = sqlParam(0).Value
                Dim isRowDeleted As Boolean = False
                For Each mRow In TRD_Detail.PurchaseRequestFooter.Rows
                    isRowDeleted = False
                    If mRow.RowState = DataRowState.Deleted Then
                        isRowDeleted = True
                        mRow.RejectChanges()
                    End If
                    ReDim sqlParam(16)
                    sqlParam(0) = Mainclass.CreateSqlParameter("@PRD_ID", mRow("PRD_ID"), SqlDbType.Int, True)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@PRD_PRH_ID", PRH_ID, SqlDbType.Int)
                    sqlParam(2) = Mainclass.CreateSqlParameter("@PRD_ITM_ID", mRow("PRD_ITM_ID"), SqlDbType.VarChar)
                    sqlParam(3) = Mainclass.CreateSqlParameter("@PRD_UOM_ID", mRow("PRD_UOM_ID"), SqlDbType.Float)
                    sqlParam(4) = Mainclass.CreateSqlParameter("@PRD_QTY", mRow("PRD_QTY"), SqlDbType.Float)
                    sqlParam(5) = Mainclass.CreateSqlParameter("@PRD_SUP_ACT_ID1", mRow("PRD_SUP_ACT_ID1"), SqlDbType.VarChar)
                    sqlParam(6) = Mainclass.CreateSqlParameter("@PRD_UNITRATE1", mRow("PRD_UNITRATE1"), SqlDbType.Float)
                    sqlParam(7) = Mainclass.CreateSqlParameter("@PRD_AMOUNT1", mRow("PRD_AMOUNT1"), SqlDbType.Float)

                    sqlParam(8) = Mainclass.CreateSqlParameter("@PRD_SUP_ACT_ID2", mRow("PRD_SUP_ACT_ID2"), SqlDbType.VarChar)
                    sqlParam(9) = Mainclass.CreateSqlParameter("@PRD_UNITRATE2", mRow("PRD_UNITRATE2"), SqlDbType.Float)
                    sqlParam(10) = Mainclass.CreateSqlParameter("@PRD_AMOUNT2", mRow("PRD_AMOUNT2"), SqlDbType.Float)

                    sqlParam(11) = Mainclass.CreateSqlParameter("@PRD_SUP_ACT_ID3", mRow("PRD_SUP_ACT_ID3"), SqlDbType.VarChar)
                    sqlParam(12) = Mainclass.CreateSqlParameter("@PRD_UNITRATE3", mRow("PRD_UNITRATE3"), SqlDbType.Float)
                    sqlParam(13) = Mainclass.CreateSqlParameter("@PRD_AMOUNT3", mRow("PRD_AMOUNT3"), SqlDbType.Float)

                    sqlParam(14) = Mainclass.CreateSqlParameter("@PRD_REMARKS", mRow("PRD_REMARKS"), SqlDbType.VarChar)
                    sqlParam(15) = Mainclass.CreateSqlParameter("@IsDeleted", isRowDeleted, SqlDbType.Bit)
                    sqlParam(16) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
                    Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[SavePurchaseRequestFooter]", sqlParam)
                    If (Retval = "0" Or Retval = "") And sqlParam(16).Value = "" Then
                    Else
                        Success = False
                        ErrorMsg = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(16).Value)
                        Exit For
                    End If
                Next
            Else
                Success = False
                ErrorMsg = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(8).Value)
            End If
            If (Success) Then
                SavePurchaseRequestDetail = ""
                stTrans.Commit()
            Else
                stTrans.Rollback()
                SavePurchaseRequestDetail = ErrorMsg
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Shared Function DeletePurchaseRequestDetail(ByVal PRH_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PRH_ID", PRH_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[DeletePurchaseRequestDetail]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeletePurchaseRequestDetail = ""
            Else
                DeletePurchaseRequestDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function ApproveOrRejectPurchaseRequest(ByVal APS_ID As String, ByVal BSU_ID As String, ByVal APS_Status As String, ByVal AUD_WINUSER As String, ByVal Aud_form As String, ByVal Aud_user As String, ByVal Aud_module As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Dim sqlParam(11) As SqlParameter
        Try
            Dim Retval As String
            ReDim sqlParam(7)
            sqlParam(0) = Mainclass.CreateSqlParameter("@APS_ID", APS_ID, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@APS_STATUS", APS_Status, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            sqlParam(4) = Mainclass.CreateSqlParameter("@AUD_WINUSER", AUD_WINUSER, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@Aud_form", Aud_form, SqlDbType.VarChar)
            sqlParam(6) = Mainclass.CreateSqlParameter("@Aud_user", Aud_user, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@Aud_module", Aud_module, SqlDbType.VarChar)
            Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[ApprovePurchaseTransaction]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(3).Value = "" Then
                ApproveOrRejectPurchaseRequest = ""
                stTrans.Commit()
            Else
                stTrans.Rollback()
                ApproveOrRejectPurchaseRequest = sqlParam(3).Value
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
End Class


