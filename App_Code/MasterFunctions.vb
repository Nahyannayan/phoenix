Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj
Public Class MasterFunctions

    Public Shared Function GetExchangeRates(ByVal p_Date As Date, ByVal p_Bsu_id As String, ByVal p_Currency As String) As DataTable
        'select * from fn_ExgRate ('3/oct/2007','125016','usd')
        Dim ds As New DataSet
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim ExDate As String = Format(p_Date, OASISConstants.DateFormat)
            Dim str_Sql As String = "select * from fn_ExgRate('" & ExDate & "','" & p_Bsu_id & "','" & p_Currency & "' )"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            Return ds.Tables(0)

        Catch ex As Exception
            Return ds.Tables(0)
            Errorlog(ex.Message)
        End Try

    End Function



    Public Shared Function fnDoGroupOperations(ByVal p_mode As String, ByVal p_Groupcode As String, Optional ByVal p_Groupname As String = "") As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            'Adding header info
            Dim cmd As New SqlCommand("sp_GroupMaster", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpGroupCode As New SqlParameter("@GroupCode", SqlDbType.VarChar, 10)
            sqlpGroupCode.Value = p_Groupcode & ""
            cmd.Parameters.Add(sqlpGroupCode)

            Dim sqlpGroupName As New SqlParameter("@GroupName", SqlDbType.VarChar, 100)
            sqlpGroupName.Value = p_Groupname & ""
            cmd.Parameters.Add(sqlpGroupName)

            Dim sqlpModifyOrDelete As New SqlParameter("@ModifyOrDelete", SqlDbType.VarChar, 8)
            sqlpModifyOrDelete.Value = p_mode & ""
            cmd.Parameters.Add(sqlpModifyOrDelete)

            Dim Success As New SqlParameter("@Success", SqlDbType.Int, 8)
            cmd.Parameters.Add(Success)
            cmd.Parameters("@Success").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            'Dim result As String = cmd.Parameters("@ErrorMsg").Value
            Dim str_success As String = cmd.Parameters("@Success").Value
            If str_success = "0" Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
            cmd.Parameters.Clear()

            Return str_success
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Return "1000"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function

  
    Public Shared Function fnDoSubLedgerOperations(ByVal p_mode As String, ByVal p_SubLedgerCode As String, ByVal p_SubLedgerName As String, ByVal p_SubLedgerActive As Boolean, ByVal p_LinkedAccount As Object) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            'Adding header info
            Dim sqlParam(4) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SubLedgerCode", p_SubLedgerCode, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SubLedgerName", p_SubLedgerName, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SubLedger_Bactive", p_SubLedgerActive, SqlDbType.Bit)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ModifyOrDelete", p_mode, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@Success", 0, SqlDbType.Int, True)
            Dim str_success As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "sp_SubLedgerMaster", sqlParam)
            If str_success = 0 And UCase(p_mode) <> "DELETE" Then
                sqlParam(0) = Mainclass.CreateSqlParameter("@ASL_ASM_ID", p_SubLedgerCode, SqlDbType.VarChar)
                sqlParam(1) = Mainclass.CreateSqlParameter("@ASL_ACT_ID", "", SqlDbType.VarChar)
                sqlParam(2) = Mainclass.CreateSqlParameter("@ASL_bDefault", False, SqlDbType.Bit)
                sqlParam(3) = Mainclass.CreateSqlParameter("@ModifyOrDelete", "Delete", SqlDbType.VarChar)
                sqlParam(4) = Mainclass.CreateSqlParameter("@Success", 0, SqlDbType.Int, True)
                str_success = Mainclass.ExecuteParamQRY(objConn, stTrans, "sp_SubLedgerLinkMaster", sqlParam)
                If str_success = 0 Then
                    Dim i As Int16
                    For i = 0 To p_LinkedAccount.GetUpperBound(0)
                        If p_LinkedAccount(i, 0) Is Nothing Or p_LinkedAccount(i, 0) = "" Then Continue For
                        sqlParam(0) = Mainclass.CreateSqlParameter("@ASL_ASM_ID", p_SubLedgerCode, SqlDbType.VarChar)
                        sqlParam(1) = Mainclass.CreateSqlParameter("@ASL_ACT_ID", p_LinkedAccount(i, 0), SqlDbType.VarChar)
                        sqlParam(2) = Mainclass.CreateSqlParameter("@ASL_bDefault", IIf(p_LinkedAccount(i, 1), True, False), SqlDbType.Bit)
                        sqlParam(3) = Mainclass.CreateSqlParameter("@ModifyOrDelete", "Insert", SqlDbType.VarChar)
                        sqlParam(4) = Mainclass.CreateSqlParameter("@Success", 0, SqlDbType.Int, True)
                        str_success = Mainclass.ExecuteParamQRY(objConn, stTrans, "sp_SubLedgerLinkMaster", sqlParam)
                        If str_success <> 0 Then Exit For
                    Next
                End If
            End If
            If str_success = 0 Then
                stTrans.Commit()
                Return 0
            Else
                stTrans.Rollback()
            End If
            Return str_success
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Return "1000"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function



    Public Shared Function fnDoSubGroupOperations(ByVal p_mode As String, ByVal p_SubGroupCode As String, _
      ByVal p_SubGroupName As String, _
      ByVal p_ParentGroupName As String, ByVal p_Type As String) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim cmd As New SqlCommand("sp_SubGroupMaster", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpSubGroupCode As New SqlParameter("@SubGroupCode", SqlDbType.VarChar, 10)
            sqlpSubGroupCode.Value = p_SubGroupCode & ""
            cmd.Parameters.Add(sqlpSubGroupCode)

            Dim sqlpSubGroupName As New SqlParameter("@SubGroupName", SqlDbType.VarChar, 100)
            sqlpSubGroupName.Value = p_SubGroupName & ""
            cmd.Parameters.Add(sqlpSubGroupName)

            Dim sqlpParentGroupName As New SqlParameter("@ParentGroupName", SqlDbType.VarChar, 10)
            sqlpParentGroupName.Value = p_ParentGroupName & ""
            cmd.Parameters.Add(sqlpParentGroupName)

            Dim sqlpType As New SqlParameter("@Type", SqlDbType.VarChar, 100)
            sqlpType.Value = p_Type & ""
            cmd.Parameters.Add(sqlpType)

            Dim sqlpModifyOrDelete As New SqlParameter("@ModifyOrDelete", SqlDbType.VarChar, 8)
            sqlpModifyOrDelete.Value = p_mode & ""
            cmd.Parameters.Add(sqlpModifyOrDelete)

            Dim Success As New SqlParameter("@Success", SqlDbType.Int, 8)
            cmd.Parameters.Add(Success)
            cmd.Parameters("@Success").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            'Dim result As String = cmd.Parameters("@ErrorMsg").Value
            Dim str_success As String = cmd.Parameters("@Success").Value

            cmd.Parameters.Clear()
            If (str_success = "0") Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
            Return str_success
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Return "1000"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Function



    Public Shared Function fnDoCurrencyOperations(ByVal p_mode As String, ByVal p_CurrencyCode As String, _
    ByVal p_CurrencyName As String, _
    ByVal p_ExchRate As String, ByVal p_Denomination As String) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim cmd As New SqlCommand("sp_CurrencyMaster", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            ''@CurrencyCode
            ''@CurrencyName
            ''@ExchRate
            ''@Denomination

            Dim sqlpSubCurrencyCode As New SqlParameter("@CurrencyCode", SqlDbType.VarChar, 10)
            sqlpSubCurrencyCode.Value = p_CurrencyCode & ""
            cmd.Parameters.Add(sqlpSubCurrencyCode)

            Dim sqlpCurrencyName As New SqlParameter("@CurrencyName", SqlDbType.VarChar, 100)
            sqlpCurrencyName.Value = p_CurrencyName & ""
            cmd.Parameters.Add(sqlpCurrencyName)

            Dim sqlpExchRate As New SqlParameter("@ExchRate", SqlDbType.VarChar, 10)
            sqlpExchRate.Value = p_ExchRate & ""
            cmd.Parameters.Add(sqlpExchRate)

            Dim sqlpDenomination As New SqlParameter("@Denomination", SqlDbType.VarChar, 100)
            sqlpDenomination.Value = p_Denomination & ""
            cmd.Parameters.Add(sqlpDenomination)

            Dim sqlpModifyOrDelete As New SqlParameter("@ModifyOrDelete", SqlDbType.VarChar, 8)
            sqlpModifyOrDelete.Value = p_mode & ""
            cmd.Parameters.Add(sqlpModifyOrDelete)

            Dim Success As New SqlParameter("@Success", SqlDbType.Int, 8)
            cmd.Parameters.Add(Success)
            cmd.Parameters("@Success").Direction = ParameterDirection.Output


            'Dim ErrorMsg As New SqlParameter("@ErrorMsg", SqlDbType.VarChar, 200)
            'cmd.Parameters.Add(ErrorMsg)
            'cmd.Parameters("@ErrorMsg").Direction = ParameterDirection.Output
            'lblBalerror.Text = ""

            cmd.ExecuteNonQuery()

            'Dim result As String = cmd.Parameters("@ErrorMsg").Value
            Dim str_success As String = cmd.Parameters("@Success").Value

            cmd.Parameters.Clear()
            If (str_success = "0") Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If

            Return str_success
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Return "1000"
        End Try

    End Function



    Public Shared Function fnDoExchangeOperations(ByVal p_mode As String, ByVal p_EXG_ID As Integer, _
     ByVal p_EXG_CUR_ID As String, ByVal p_EXG_BSU_ID As String, _
     ByVal p_EXG_RATE As String, ByVal p_EXG_FDATE As String, _
     Optional ByVal p_EXG_TDATE As String = "") As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim cmd As New SqlCommand("SaveEXGRATE_S", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            ' ''@EXG_ID
            ' ''@EXG_CUR_ID
            ' ''@EXG_BSU_ID
            ' ''@EXG_RATE
            ' ''@EXG_FDATE
            ' ''@EXG_TDATE
            ' ''@bEdit

            Dim sqlpEXG_ID As New SqlParameter("@EXG_ID", SqlDbType.Int, 8)
            sqlpEXG_ID.Value = p_EXG_ID
            cmd.Parameters.Add(sqlpEXG_ID)

            Dim sqlpEXG_CUR_ID As New SqlParameter("@EXG_CUR_ID", SqlDbType.VarChar, 20)
            sqlpEXG_CUR_ID.Value = p_EXG_CUR_ID & ""
            cmd.Parameters.Add(sqlpEXG_CUR_ID)

            Dim sqlpEXG_BSU_ID As New SqlParameter("@EXG_BSU_ID", SqlDbType.VarChar, 20)
            sqlpEXG_BSU_ID.Value = p_EXG_BSU_ID & ""
            cmd.Parameters.Add(sqlpEXG_BSU_ID)

            Dim sqlpEXG_RATE As New SqlParameter("@EXG_RATE", SqlDbType.Decimal, 100)
            sqlpEXG_RATE.Value = p_EXG_RATE & ""
            cmd.Parameters.Add(sqlpEXG_RATE)

            Dim sqlpEXG_FDATE As New SqlParameter("@EXG_FDATE", SqlDbType.DateTime, 30)
            sqlpEXG_FDATE.Value = p_EXG_FDATE & ""
            cmd.Parameters.Add(sqlpEXG_FDATE)

            If p_EXG_TDATE = "" Then
                Dim sqlpEXG_TDATE As New SqlParameter("@EXG_TDATE", SqlDbType.DateTime, 30)
                sqlpEXG_TDATE.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpEXG_TDATE)
            Else
                Dim sqlpEXG_TDATE As New SqlParameter("@EXG_TDATE", SqlDbType.DateTime, 30)
                sqlpEXG_TDATE.Value = p_EXG_TDATE & ""
                cmd.Parameters.Add(sqlpEXG_TDATE)
            End If

            Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Int, 8)
            sqlpbEdit.Value = p_mode & ""
            cmd.Parameters.Add(sqlpbEdit)

            'Dim Success As New SqlParameter("@Success", SqlDbType.Int, 8)
            'cmd.Parameters.Add(Success)
            'cmd.Parameters("@Success").Direction = ParameterDirection.Output


            'Dim ErrorMsg As New SqlParameter("@ErrorMsg", SqlDbType.VarChar, 200)
            'cmd.Parameters.Add(ErrorMsg)
            'cmd.Parameters("@ErrorMsg").Direction = ParameterDirection.Output
            'lblBalerror.Text = ""
            Dim iReturnvalue As Integer
            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()

            'Dim result As String = cmd.Parameters("@ErrorMsg").Value
            'Dim str_success As String = cmd.Parameters("@Success").Value


            iReturnvalue = retValParam.Value

            cmd.Parameters.Clear()

            If (iReturnvalue = 0) Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If

            Return iReturnvalue


        Catch ex As Exception
            stTrans.Rollback()

            Errorlog(ex.Message)
            Return "280"
        End Try

    End Function



    Public Shared Function SAVECOSTUNIT_M(ByVal p_id As String, _
        ByVal p_CUT_DESCR As String, ByVal p_CUT_TYPE As String, _
        ByVal p_CUT_ITEM As String, ByVal p_CUT_PPAID_ACT_ID As String, ByVal p_CUT_EXP_ACT_ID As String, _
         ByVal p_edit As Boolean, _
        ByVal stTrans As SqlTransaction) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Try
            '@CUT_ID = 1,
            '@CUT_DESCR = N'TEST FIRST',
            '@CUT_TYPE = N'12 TYPE',
            '@CUT_ITEM = N'AAVO',

            Dim pParms(28) As SqlClient.SqlParameter
            If p_edit = False Then
                p_id = "1"
            End If
            pParms(0) = New SqlClient.SqlParameter("@CUT_ID", SqlDbType.Int)
            pParms(0).Value = p_id
            pParms(1) = New SqlClient.SqlParameter("@CUT_DESCR", SqlDbType.VarChar, 100)
            pParms(1).Value = p_CUT_DESCR
            pParms(2) = New SqlClient.SqlParameter("@CUT_TYPE", SqlDbType.VarChar, 20)
            pParms(2).Value = p_CUT_TYPE
            pParms(3) = New SqlClient.SqlParameter("@CUT_ITEM", SqlDbType.VarChar, 50)
            pParms(3).Value = p_CUT_ITEM

            '@CUT_PPAID_ACT_ID = N'333',
            '@CUT_EXP_ACT_ID = N'444',
            '@bEdit = 0

            pParms(4) = New SqlClient.SqlParameter("@CUT_PPAID_ACT_ID", SqlDbType.VarChar, 30)
            pParms(4).Value = p_CUT_PPAID_ACT_ID
            pParms(5) = New SqlClient.SqlParameter("@CUT_EXP_ACT_ID", SqlDbType.VarChar, 30)
            pParms(5).Value = p_CUT_EXP_ACT_ID
            pParms(6) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
            pParms(6).Value = p_edit



            pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SaveCOSTUNIT_M", pParms)
            If pParms(7).Value = "0" Then

                SAVECOSTUNIT_M = 0
            Else
                SAVECOSTUNIT_M = pParms(7).Value
            End If


        Catch ex As Exception
            Errorlog(ex.Message)
            SAVECOSTUNIT_M = 1000
        Finally

        End Try


    End Function



    Public Shared Function DeleteCOSTUNIT_M(ByVal p_id As String, _
         ByVal stTrans As SqlTransaction) As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Try
            '@CUT_ID = 1,
            

            Dim pParms(28) As SqlClient.SqlParameter
            
            pParms(0) = New SqlClient.SqlParameter("@CUT_ID", SqlDbType.Int)
            pParms(0).Value = p_id
           
            pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue


            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "DeleteCOSTUNIT_M", pParms)
            If pParms(2).Value = "0" Then
                DeleteCOSTUNIT_M = 0
            Else
                DeleteCOSTUNIT_M = pParms(2).Value
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            DeleteCOSTUNIT_M = 1000
        Finally

        End Try


    End Function



End Class
