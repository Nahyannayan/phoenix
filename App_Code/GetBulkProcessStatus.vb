﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Newtonsoft.Json

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
<ScriptService()>
Public Class GetBulkProcessStatus
    Inherits System.Web.Services.WebService

    <System.Web.Script.Services.ScriptMethod(),
     System.Web.Services.WebMethod(EnableSession:=True)>
    Public Function GetStatusPer(ByVal BATCH_NO As String) As FeeBulkProcess
        Dim Response As New FeeBulkProcess
        If Not BATCH_NO = "" Then
            Try
                Using con As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
                    con.Open()
                    Dim stTrans As SqlTransaction = con.BeginTransaction
                    Dim sqlParam(3) As SqlParameter
                    Dim RequestParam As String = TryCast(HttpContext.Current.Request.Params(0), String)
                    Using cmd As New SqlCommand("[DataImport].[DEBT_FOLLOW_UP_UPLOAD_TRAN]", con, stTrans)
                        sqlParam(0) = New SqlParameter("@StrMode", "GET_STATUS")
                        cmd.Parameters.Add(sqlParam(0))
                        sqlParam(1) = New SqlParameter("@BATCH_NO", BATCH_NO)
                        cmd.Parameters.Add(sqlParam(1))
                        sqlParam(2) = New SqlParameter("@PERCENTAGE", SqlDbType.VarChar, 50)
                        sqlParam(2).Direction = ParameterDirection.Output
                        cmd.Parameters.Add(sqlParam(2))
                        cmd.CommandTimeout = 0
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.ExecuteNonQuery()
                        Response.PERCENTAGE = sqlParam(2).Value
                        Response.WORKDESCRIPTION = " processed"
                        If (Response.PERCENTAGE = 100) Then
                            Response.ISCOMPLETE = True
                            Response.MESSAGE = " data processed successfully."
                        End If

                    End Using
                End Using

            Catch ex As Exception
                UtilityObj.Errorlog("Service Call : " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If

        Return Response
    End Function
End Class