Imports Microsoft.VisualBasic

Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Namespace Oasis_Counter

    Public Class Counter

        Public Shared Function GetNextStudentInQueue(ByVal CounterId As String, ByVal Bsuid As String, ByVal emp_id As String) As Long

            Dim Stu_id = ""
            Dim ds As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringCounter").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@COUNTER_ID", CounterId)
            pParms(1) = New SqlClient.SqlParameter("@COUNTER_BSU_ID", Bsuid)
            pParms(2) = New SqlClient.SqlParameter("@COUNTER_EMP_ID", emp_id)
            ds = SqlHelper.ExecuteDataset(str_conn, Data.CommandType.StoredProcedure, "COUNTER_CALL_NEXT_TOKEN_NUMBER_GENERAL", pParms)
            If ds.Tables(0).Rows.Count > 0 Then

                Stu_id = ds.Tables(0).Rows(0).Item("TOKEN_NUMBER").ToString()
            Else
                Stu_id = 0
            End If

            Return Stu_id
           

        End Function

        Public Shared Function GetNextStudentInQueue_GENERAL(ByVal CounterId As String, ByVal Bsuid As String, ByVal emp_id As String) As Long

            Dim Stu_id = ""
            Dim ds As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringCounter").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@COUNTER_ID", CounterId)
            pParms(1) = New SqlClient.SqlParameter("@COUNTER_BSU_ID", Bsuid)
            pParms(2) = New SqlClient.SqlParameter("@COUNTER_EMP_ID", emp_id)

            ds = SqlHelper.ExecuteDataset(str_conn, Data.CommandType.StoredProcedure, "COUNTER_CALL_NEXT_TOKEN_NUMBER_GENERAL", pParms)

            If ds.Tables(0).Rows.Count > 0 Then

                Stu_id = ds.Tables(0).Rows(0).Item("STU_ID").ToString()


            Else
                Stu_id = 0
            End If

            Return Stu_id

        End Function


    End Class

End Namespace
