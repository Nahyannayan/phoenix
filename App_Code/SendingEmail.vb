Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class SendingEmail
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function SendNewsletter(ByVal cse_id As String, ByVal GroupId As String, ByVal email_id As String, ByVal UniqueId As String, ByVal eml_id As String, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)
        Dim Returnvalue = ""

        Try
            Returnvalue = Send(GroupId, eml_id, email_id, UniqueId, cse_id, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)

        Catch ex As Exception
            Returnvalue = "Error in Application " & ex.Message
        End Try


        Return Returnvalue
    End Function

    <WebMethod()> _
        Public Function SendPlainTextEmail(ByVal cse_id As String, ByVal GroupId As String, ByVal email_id As String, ByVal UniqueId As String, ByVal eml_id As String, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)
        Dim Returnvalue = ""

        Try

            Returnvalue = SendPlainText(GroupId, eml_id, email_id, UniqueId, cse_id, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)

        Catch ex As Exception
            Returnvalue = "Error in Application " & ex.Message
        End Try


        Return Returnvalue
    End Function

    Public Function SendPlainText(ByVal groupid As String, ByVal templateid As String, ByVal email_id As String, ByVal UniqueId As String, ByVal cse_id As String, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)
        Dim Retutnvalue As String = ""

        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            ''Set avtive true for not editing the message text in future
            Dim str_query As String = "Update COM_MANAGE_EMAIL set EML_ACTIVE='True' where EML_ID='" & templateid & "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            str_query = "Select EML_BODY From COM_MANAGE_EMAIL where EML_ID='" & templateid & "'"

            Dim MailBody = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString()

            str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & templateid & "'"
            Dim ds1 As DataSet
            ds1 = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)

            Dim Subject = ds1.Tables(0).Rows(0).Item("EML_SUBJECT").ToString().Trim()
            Dim FromEmailid As String = ds1.Tables(0).Rows(0).Item("EML_FROM").ToString().Trim()
            Dim DisplayName = ds1.Tables(0).Rows(0).Item("EML_DISPLAY").ToString().Trim()
            Dim UserName = ds1.Tables(0).Rows(0).Item("EML_USERNAME").ToString().Trim()
            Dim Password = ds1.Tables(0).Rows(0).Item("EML_PASSWORD").ToString().Trim()
            Dim Host = ds1.Tables(0).Rows(0).Item("EML_HOST").ToString().Trim().Trim()
            Dim Port = Convert.ToInt16(ds1.Tables(0).Rows(0).Item("EML_PORT").ToString().Trim())
            Dim Hasattachments As Boolean = ds1.Tables(0).Rows(0).Item("EML_ATTACHMENT")
            Dim EML_MERGE_ID = ds1.Tables(0).Rows(0).Item("EML_MERGE_ID")

            If EML_MERGE_ID <> "" Then
                MailBody = GetMergeMessage(MailBody, EML_MERGE_ID, UniqueId)

            End If

            MailBody = MailBody.Replace("<$", "<")
            MailBody = MailBody.Replace("$>", ">")


            If isEmail(email_id) Then

                Retutnvalue = email.SendPlainTextEmails(FromEmailid, email_id, Subject, MailBody, UserName, Password, Host, Port, templateid, Hasattachments)
                SaveLog(groupid, templateid, UniqueId, Host, email_id, Retutnvalue, cse_id, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)

            Else
                Retutnvalue = "Error: Email Id Not Valid"
                SaveLog(groupid, templateid, UniqueId, Host, email_id, "Error: Email Id Not Valid", cse_id, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)

            End If


        Catch ex As Exception
            Retutnvalue = "Error : <br> " & ex.Message
        End Try


        Return Retutnvalue
    End Function

    Public Function GetMergeMessage(ByVal originalmessage As String, ByVal merge_id As String, ByVal uniqueid As String) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim message As String = "select '"
        message = message & originalmessage
        message = message.Replace("<#", " '+ ")
        message = message.Replace("#>", " +' ")
        message = message + "'"


        Dim sql_query As String = "select MERGE_TABLE,MERGE_UNIQUE_FIELD_NAME from COM_MERGE_TABLES where MERGE_ID='" & merge_id & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        Dim table = ""
        Dim unqfield = ""
        If ds.Tables(0).Rows.Count > 0 Then
            table = ds.Tables(0).Rows(0).Item("MERGE_TABLE").ToString()
            unqfield = ds.Tables(0).Rows(0).Item("MERGE_UNIQUE_FIELD_NAME").ToString()
            message = message & " from ( " & table & ") A WHERE " & unqfield & "='" & uniqueid & "'"
            message = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, message)
        End If


        Return message

    End Function

    Public Function Send(ByVal groupid As String, ByVal templateid As String, ByVal email_id As String, ByVal UniqueId As String, ByVal cse_id As String, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)
        Dim Retutnvalue = ""
        Try


            Dim Encr_decrData As New Encryption64
            Dim strStudentNo = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            ''Set avtive true for not editing the message text in future
            Dim str_query As String = "Update COM_MANAGE_EMAIL set EML_ACTIVE='True' where EML_ID='" & templateid & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            str_query = "Select EML_NEWS_LETTER_FILE_NAME From COM_MANAGE_EMAIL where EML_ID='" & templateid & "'"

            Dim MailBody As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            Dim fname = MailBody
            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
            Dim AlternatreMailBody As AlternateView


            str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & templateid & "'"
            Dim ds1 As DataSet
            ds1 = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)
            Dim Subject = ds1.Tables(0).Rows(0).Item("EML_SUBJECT").ToString().Trim()
            Dim FromEmailid As String = ds1.Tables(0).Rows(0).Item("EML_FROM").ToString().Trim()
            Dim DisplayName = ds1.Tables(0).Rows(0).Item("EML_DISPLAY").ToString().Trim()
            Dim UserName = ds1.Tables(0).Rows(0).Item("EML_USERNAME").ToString().Trim()
            Dim Password = ds1.Tables(0).Rows(0).Item("EML_PASSWORD").ToString().Trim()
            Dim Host = ds1.Tables(0).Rows(0).Item("EML_HOST").ToString().Trim().Trim()
            Dim Port = Convert.ToInt16(ds1.Tables(0).Rows(0).Item("EML_PORT").ToString())
            Dim Hasattachments As Boolean = ds1.Tables(0).Rows(0).Item("EML_ATTACHMENT")
            Dim schoolsurvey As Boolean = ds1.Tables(0).Rows(0).Item("EML_SCHOOL_SURVEY")
            Dim Surveyid = ""

            MailBody = ScreenScrapeHtml(serverpath + templateid + "/News Letters/" + fname)
            AlternatreMailBody = convertToEmbedResource(MailBody, templateid)

            If ds1.Tables(0).Rows(0).Item("EML_SURVEY_ID").ToString().Trim() <> "" Then
                Surveyid = ds1.Tables(0).Rows(0).Item("EML_SURVEY_ID")
            End If

            Dim emailcheck As String = email_id

            If isEmail(emailcheck) Then
                MailBody = ScreenScrapeHtml(serverpath + templateid + "/News Letters/" + fname)

                If Surveyid <> "" Then
                    MailBody = MailBody.Replace("%SurveyId%", Encr_decrData.Encrypt(Surveyid))
                    MailBody = MailBody.Replace("%PartcId%", Encr_decrData.Encrypt(UniqueId))
                    AlternatreMailBody = convertToEmbedResource(MailBody, templateid)

                    If UniqueId.Trim.Length >= 13 Then
                        strStudentNo = UniqueId.Substring(0, 6)
                    End If

                    '' Get credistials for each bsu for sending survey.
                    If schoolsurvey Then
                        Dim student_no = UniqueId
                        If student_no.ToString.Trim().Length >= 13 Then
                            Dim _bsu_id = student_no.ToString().Substring(0, 6)
                            str_query = "Select * from  BSU_COMMUNICATION_M where BSC_BSU_ID='" & _bsu_id & "' and BSC_TYPE='SURVEY'"
                            Dim bds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                            If bds.Tables(0).Rows.Count > 0 Then
                                FromEmailid = bds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
                                UserName = bds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                                Password = bds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                                Host = bds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                                Port = bds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                            End If

                        End If
                    End If
                Else

                    AlternatreMailBody = convertToEmbedResource(MailBody, templateid)

                End If

                Dim Responsevalue = email.SendNewsLetters(FromEmailid, emailcheck, Subject, AlternatreMailBody, UserName, Password, Host, Port, templateid, Hasattachments)

                SaveLog(groupid, templateid, UniqueId, Host, emailcheck, Responsevalue, cse_id, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)

                If Surveyid <> "" Then
                    'Email Sent Update the Count in the Survey Database
                    UpdateSurveySENT(Surveyid, CInt(strStudentNo))
                End If

                Retutnvalue = Responsevalue

            Else
                Retutnvalue = "Error: Email Id Not Valid"
                SaveLog(groupid, templateid, UniqueId, Host, emailcheck, "Error: Email Id Not Valid", cse_id, LOG_SENDING_ID, SENDING_USER_EMP_ID, BSU_ID)
            End If


        Catch ex As Exception
            Retutnvalue = "Error : <br>" & ex.Message
        End Try

        Return Retutnvalue

    End Function

    Public Shared Function convertToEmbedResource(ByVal emailHtml$, ByVal Templateid As String) As AlternateView

        'This is the website where the resources are located
        Dim ResourceUrl As String = WebConfigurationManager.AppSettings("WebsiteURLResource").ToString
        Dim webSiteUrl$ = ResourceUrl + Templateid + "/"

        ' The first regex finds all the url/src tags.
        Dim matchesCol As MatchCollection = Regex.Matches(emailHtml, "url\(['|\""]+.*['|\""]\)|src=[""|'][^""']+[""|']")

        Dim normalRes As Match


        Dim resCol As AlternateView = AlternateView.CreateAlternateViewFromString("", Nothing, "text/html")

        Dim resId% = 0

        ' Between the findings
        For Each normalRes In matchesCol

            Dim resPath$

            ' Replace it for the new content ID that will be embeded
            If Left(normalRes.Value, 3) = "url" Then
                emailHtml = emailHtml.Replace(normalRes.Value, "url(cid:EmbedRes_" & resId & ")")
            Else
                emailHtml = emailHtml.Replace(normalRes.Value, "src=""cid:EmbedRes_" & resId & """")
            End If

            ' Clean the path
            resPath = Regex.Replace(normalRes.Value, "url\(['|\""]", "")
            resPath = Regex.Replace(resPath, "src=['|\""]", "")
            resPath = Regex.Replace(resPath, "['|\""]\)", "").Replace(webSiteUrl, "").Replace("""", "")

            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

            ' Map it on the server
            ''resPath = Server.MapPath(resPath)
            resPath = serverpath + Templateid + "/News Letters/" + resPath
            resPath = resPath.Replace("%20", " ")
            ' Embed the resource
            If resPath.LastIndexOf("http://") = -1 Then
                Dim theResource As LinkedResource = New LinkedResource(resPath)
                theResource.ContentId = "EmbedRes_" & resId
                resCol.LinkedResources.Add(theResource)
            End If

            ' Next resource ID
            resId = resId + 1

        Next


        ' Create our final object
        Dim finalEmail As AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString(emailHtml, Nothing, "text/html")
        Dim transferResource As LinkedResource

        ' And transfer all the added resources to the output object
        For Each transferResource In resCol.LinkedResources
            finalEmail.LinkedResources.Add(transferResource)
        Next

        Return finalEmail

    End Function

    Public Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream(), System.Text.Encoding.Default)
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml

    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            'Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim re As New Regex(strRegex)
            'If re.IsMatch(inputEmail) Then
            '    Return (True)
            'Else
            '    Return (False)
            'End If
            Dim pattern As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" '"(.+@.+\.[a-z]+)"
            Dim expression As Regex = New Regex(pattern)
            Return expression.IsMatch(inputEmail)


        End If

    End Function

    Public Sub SaveLog(ByVal GroupId As String, ByVal TemplateId As String, ByVal UniqueId As String, ByVal stuServer As String, ByVal emailid As String, ByVal status As String, ByVal cse_id As String, ByVal LOG_SENDING_ID As String, ByVal SENDING_USER_EMP_ID As String, ByVal BSU_ID As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@LOG_CGR_ID", GroupId)
        pParms(1) = New SqlClient.SqlParameter("@LOG_EML_ID", TemplateId)
        pParms(2) = New SqlClient.SqlParameter("@LOG_UNIQUE_ID", UniqueId)
        pParms(3) = New SqlClient.SqlParameter("@LOG_EMAIL_ID", emailid)
        pParms(4) = New SqlClient.SqlParameter("@LOG_SERVER", stuServer)
        pParms(5) = New SqlClient.SqlParameter("@LOG_STATUS", status)
        pParms(6) = New SqlClient.SqlParameter("@LOG_CSE_ID", cse_id)
        pParms(7) = New SqlClient.SqlParameter("@LOG_SENDING_ID", LOG_SENDING_ID)
        pParms(8) = New SqlClient.SqlParameter("@SENDING_USER_EMP_ID", SENDING_USER_EMP_ID)
        pParms(9) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_LOG_EMAIL_TABLE_INSERT", pParms)


    End Sub

    Private Sub UpdateSurveySENT(ByVal SurveyId As Integer, ByVal strStudentNo As Integer)
        Dim IntResult As Object
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISSurveyConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = "exec SRV.updateSURVEYSENT " & SurveyId & "," & strStudentNo
                IntResult = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub


End Class
