Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Imports System.Data.Oledb
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class GetSmsData
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function SendSmsExcel(ByVal Path As String, ByVal LastCount As String) As XmlDocument
        Dim Encr_decrData As New Encryption64
        Dim ds As New DataSet()
        Dim Xdoc As New XmlDocument
        Try

            Dim strConn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim condition = "select ROW_NUMBER() over(order by ID) as RowID , ID , MOBILE_NO from  COM_EXCEL_SMS_DATA_LOG where LOG_ID='" & Path & "' and NO_ERROR_FLAG='True'"
            If Session("sbsuid") <> "315888" Then
                condition = " select top 6 isnull(ID,'') as uniqueid,isnull('971'+ right(replace(MOBILE_NO,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where rowid >" & LastCount & " order by rowid"
            Else
                condition = " select top 6 isnull(ID,'') as uniqueid,isnull('012'+ right(replace(MOBILE_NO,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  where rowid >" & LastCount & " order by rowid"
            End If


            ds = SqlHelper.ExecuteDataset(strConn, CommandType.Text, condition)
            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)

        Catch ex As Exception

        End Try

        Return Xdoc

    End Function


    <WebMethod()> _
          Public Function SendSmsGroups(ByVal csm_id As String, ByVal LastCount As String) As XmlDocument

        Dim Xdoc As New XmlDocument
        Try
            Dim ds As DataSet = GetGridDataSmsGroups(csm_id, LastCount)
            Dim strXdoc As String
            strXdoc = ds.GetXml
            Xdoc.LoadXml(strXdoc)
        Catch ex As Exception

        End Try



        Return Xdoc

    End Function



    Public Function GetGridDataSmsGroups(ByVal csm_id As String, ByVal lastcount As String) As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = ""
        Dim ds As DataSet

        sql_query = "select CSM_CGR_ID,CSM_CMS_ID from COM_SEND_SMS where  CSM_ID='" & csm_id & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        Dim groupid As String = ds.Tables(0).Rows(0).Item("CSM_CGR_ID").ToString()
        Dim Templateid = ds.Tables(0).Rows(0).Item("CSM_CMS_ID").ToString()

        sql_query = "select CGR_GRP_TYPE,CGR_CONDITION,CGR_TYPE,CGR_REMOVE_SMS_IDS from COM_GROUPS_M where CGR_ID='" & groupid & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        Dim condition As String = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
        Dim grptype As String = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()
        Dim session As HttpSessionState = HttpContext.Current.Session
        If grptype = "GRP" Then  ''Group 

            If session("sbsuid") <> "315888" Then
                condition = " select top 6 isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " and rowid >" & lastcount & " order by rowid"
            Else
                condition = " select top 6 isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " and rowid >" & lastcount & " order by rowid"
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, condition)
        End If

        If grptype = "AON" Then ''Add On 

            condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on  b.CGAO_UNIQUE_ID =a.uniqueid and CGAO_CGR_ID='" & groupid & "' and STU_bRCVMAIL='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()



            If session("sbsuid") <> "315888" Then
                sql_query = " select top 6 isnull(uniqueid,'')as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where rowid >" & lastcount & " order by rowid"
            Else
                sql_query = " select top 6 isnull(uniqueid,'')as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  where rowid >" & lastcount & " order by rowid"
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        End If
            Return ds

    End Function

End Class
