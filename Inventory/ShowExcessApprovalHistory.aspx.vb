﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj

Partial Class Inventory_ShowExcessApprovalHistory
    Inherits System.Web.UI.Page

    Private Sub Inventory_ShowExcessApprovalHistory_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ExcessApproval As DataTable
        Dim Empid As String = ""
        If Session("Empdata") Is Nothing Then
            ExcessApproval = Session("Empdata1")
        Else
            ExcessApproval = Session("Empdata")
        End If

        If Not ExcessApproval Is Nothing Then

            For Each mrow As DataRow In ExcessApproval.Rows
                Empid = Empid & mrow("tce_emp_id") & ","
            Next
            BindExcessHouseWaiverApproval(Empid)
        Else
            lblError.Text = "No data exists."
        End If



    End Sub
    Private Sub BindExcessHouseWaiverApproval(ByVal TCE_Id As String)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(1) As SqlParameter

            PARAM(1) = New SqlParameter("@TCE_ID", TCE_Id)


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetExcessApprovalList", PARAM)
            Dim dvOLDTRF As DataView = New DataView(dsDetails.Tables(0))


            GrdExcessHistory.DataSource = dvOLDTRF
            GrdExcessHistory.DataBind()
            If dvOLDTRF.Count = 0 Then
                lblError.Text = "No Excess House waiver approval data exists."
            End If



        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class
