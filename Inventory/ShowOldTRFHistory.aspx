﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowOldTRFHistory.aspx.vb" Inherits="Inventory_ShowOldTRFHistory" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />

    
    <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet"/>
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    

    </head>
 <body>
    
    <form id="bLOCKFORM" runat="server">   
        <%--<div>--%>


        <script type="text/javascript" language="javascript">
        
             function fancyClose() {
           
            parent.$.fancybox.close();

            }
            </script>

             <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
                    </ajaxToolkit:ToolkitScriptManager>

    
        <table width="100%" >

        
    

        <tr   runat="server" class="title-bg-lite">
                                        <td align="center" colspan="8" valign="middle">Old TRF Request list
                                        </td>
                                    </tr>





                            <tr>
                            <td colspan="8" align="center" >
                                    <asp:DataGrid ID="GrdHistory" runat="server" AutoGenerateColumns="true" Width="99%"
                                        CaptionAlign="Top" PageSize="15"  CssClass="table table-bordered table-row">
                                        

                                    </asp:DataGrid>
                                </td>
                        </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </td>
                            </tr>
            </table>
        <asp:HiddenField ID="h_Tch_id" runat="server" />                        

    </form>

    

</body> </html>