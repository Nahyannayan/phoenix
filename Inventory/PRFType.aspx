<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PRFType.aspx.vb" Inherits="Inventory_PRFType" Title="Priority" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            PRF Type Master
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"  valign="top">
                            <table   width="100%" >
                                <tr>
                                    <td align="left" class="matters" width="25%"  ><span class="field-label"> PRF Type Description</span></td>
                                    <td align="left" class="matters" width="75%">
                                        <asp:TextBox ID="txtPriDescr" runat="server" TabIndex="1" MaxLength="100"
                                             ></asp:TextBox>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="matters"  align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom"  >
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

