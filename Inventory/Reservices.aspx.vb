﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_REServices
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'ts
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "PI02033" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                BindLocation()

                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        'txtCnb_RatePerKm.Attributes.Add("onkeypress", " return Numeric_Only();")

        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        If mode = "edit" Then
            txtDescr.Enabled = Not EditAllowed
        Else
            txtDescr.Enabled = EditAllowed
        End If
        ddlLocation.Enabled = EditAllowed
        radAddn.Enabled = EditAllowed
        radDedn.Enabled = EditAllowed
        rbRefYes.Enabled = EditAllowed
        rbRefNo.Enabled = EditAllowed

        radPercent.Enabled = EditAllowed
        radValue.Enabled = EditAllowed
        txtDefaultAmount.Enabled = EditAllowed
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select  RES_ID, RES_DESCR, REC_CIT_ID, REC_PERCENT, REC_DEFAULT, RES_ADDDED,RES_REFUND from RESERVICES   ")
                strSql.Append("where RES_ID=" & h_EntryId.Value)
                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtDescr.Text = dt.Rows(0)("RES_DESCR")
                    txtDefaultAmount.Text = dt.Rows(0)("REC_DEFAULT")

                    If dt.Rows(0)("REC_PERCENT") = 1 Then
                        radPercent.Checked = True
                    Else
                        radValue.Checked = True
                    End If

                    If dt.Rows(0)("RES_ADDDED") = "A" Then
                        radAddn.Checked = True
                    Else
                        radDedn.Checked = True
                    End If
                    ddlLocation.SelectedValue = dt.Rows(0)("REC_CIT_ID")
                    h_EntryId.Value = dt.Rows(0)("RES_ID")
                    If dt.Rows(0)("RES_REFUND") = True Then
                        rbRefYes.Checked = True
                    Else
                        rbRefNo.Checked = True
                    End If
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindLocation()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim str_Sql As String = " SELECT '' CIT_ID,'' CIT_DESCR  UNION ALL SELECT 'ALL' CIT_ID,'ALL' CIT_DESCR UNION ALL select CIT_ID,CIT_DESCR from oasis..CITY_M  where CIT_ID in('RAK','DXB','SHJ','AUH','FUJ','AJN')"
            ddlLocation.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlLocation.DataTextField = "CIT_DESCR"
            ddlLocation.DataValueField = "CIT_ID"
            ddlLocation.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ClearDetails()

        txtDescr.Text = ""
        txtDefaultAmount.Text = ""


    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM TCP_D WHERE TCP_RES_ID =" & h_EntryId.Value) > 0 Then
                lblError.Text = "Used in Other Charges, unable to delete"
            ElseIf SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM TCT_D WHERE TCT_RES_ID =" & h_EntryId.Value) > 0 Then
                lblError.Text = "Used in Payment Details, unable to delete"
            Else
                Dim objConn As New SqlConnection(connectionString)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction

                Try
                    Dim pParms(1) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@RES_ID", h_EntryId.Value, SqlDbType.Int, True)
                    pParms(2) = Mainclass.CreateSqlParameter("@TABLE", "RESERVICE", SqlDbType.VarChar)

                    Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "DeleteMasterData", pParms)
                    If RetVal = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    Else
                        ViewState("EntryId") = pParms(1).Value
                    End If

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                    End If


                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                    Exit Sub
                Finally
                    objConn.Close()
                End Try
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                lblError.Text = "Record Deleted Successfully !!!"
                'Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean

        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(7) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@RES_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@RES_DESCR", txtDescr.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@REC_CIT_ID", ddlLocation.SelectedValue, SqlDbType.VarChar)
        If radPercent.Checked = True Then
            pParms(4) = Mainclass.CreateSqlParameter("@REC_PERCENT", 1, SqlDbType.Int)
        Else
            pParms(4) = Mainclass.CreateSqlParameter("@REC_PERCENT", 0, SqlDbType.Int)
        End If

        pParms(5) = Mainclass.CreateSqlParameter("@REC_DEFAULT", txtDefaultAmount.Text, SqlDbType.Float)
        If radAddn.Checked = True Then
            pParms(6) = Mainclass.CreateSqlParameter("@RES_ADDDED", "A", SqlDbType.VarChar)
        Else
            pParms(6) = Mainclass.CreateSqlParameter("@RES_ADDDED", "D", SqlDbType.VarChar)
        End If
        If rbRefYes.Checked = True Then
            pParms(7) = Mainclass.CreateSqlParameter("@RES_REFUND", 1, SqlDbType.Bit)
        Else
            pParms(7) = Mainclass.CreateSqlParameter("@RES_REFUND", 0, SqlDbType.Bit)
        End If

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveREADDITION", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class

