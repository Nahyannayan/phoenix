﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="AppGrpUsers.aspx.vb" Inherits="Inventory_AppGrpUsers" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <%--  <script language="javascript" type="text/javascript">
        
        function getGroupName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "GROUPLIST"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) return false;
            NameandCode = result.split('___');
            document.getElementById("<%=TxtGrpName.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtUserGrpId.ClientID %>").value = NameandCode[0];

        }
        function getUserName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            bsuId = document.getElementById("<%=txtBsuId.ClientID %>").value;
            pMode = "USERSEARCHAPPGRPUSER"
            url = "../common/PopupSelect.aspx?id=" + pMode + '&bsuid=' + bsuId;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) return false;
            NameandCode = result.split('___');
            document.getElementById("<%=txtUserName.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtUserEmail.ClientID %>").value = NameandCode[2];
        }

        function getDate() {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../accounts/calendar.aspx?nofuture=&dt=" + document.getElementById('<%=txtUserDate.ClientID %>').value, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            document.getElementById('<%=txtUserDate.ClientID %>').value = result;
    return true;
}


function getBusinessUnits() {
    var sFeatures;
    var lstrVal;
    var lintScrVal;
    var pMode;
    var NameandCode;
    sFeatures = "dialogWidth: 760px; ";
    sFeatures += "dialogHeight: 420px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    pMode = "BUSINESSUNITAPPGRPUSER"
    url = "../common/PopupSelect.aspx?id=" + pMode;
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) return false;
    NameandCode = result.split('___');
    document.getElementById("<%=txtBSUID.ClientID %>").value = NameandCode[0];
        document.getElementById("<%=txtBSUName.ClientID %>").value = NameandCode[1];
    }


    </script>--%>


    <script>
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        function getGroupNameNew() {
            var pMode;
            var NameandCode;

            pMode = "GROUPLIST"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_group");
        }
        function OnClientClose(oWnd, args) {  //Group
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=TxtGrpName.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtUserGrpId.ClientID %>").value = NameandCode[0];
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        function getBusinessUnitsNew() {
            var pMode;
            var NameandCode;

            pMode = "BUSINESSUNITAPPGRPUSER"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_bsu");
        }
        function OnClientClose1(oWnd, args) {  //BSU
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtBSUID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtBSUName.ClientID %>").value = NameandCode[1];
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------
        function getUserNameNew() {
            var pMode;
            var NameandCode;
            var bsuId;
            bsuId = document.getElementById("<%=txtBsuId.ClientID %>").value;
                pMode = "USERSEARCHAPPGRPUSER"
                url = "../common/PopupSelect.aspx?id=" + pMode + '&bsuid=' + bsuId;
                var oWnd = radopen(url, "pop_user");
            }
            function OnClientClose2(oWnd, args) {
                var NameandCode;
                var arg = args.get_argument();
                if (arg) {

                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtUserName.ClientID %>").value = NameandCode[1];
                    document.getElementById("<%=txtUserEmail.ClientID %>").value = NameandCode[2];
                }
            }
            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;

                var height = body.scrollHeight;
                var width = body.scrollWidth;

                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;

                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_group" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_user" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Approval Group Users
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Group ID</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtUserGrpId" runat="server" Enabled="false" OnTextChanged="txtUserGrpId_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="ImgGroup" runat="server" OnClientClick="getGroupNameNew();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Group Name</span></td>
                                    <td width="30%">
                                        <asp:TextBox ID="TxtGrpName" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Business Unit Code</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBsuId" runat="server" Enabled="false"></asp:TextBox>
                                         <asp:ImageButton ID="ImgBSU" runat="server" OnClientClick="getBusinessUnitsNew();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Business Unit Name</span>
                                       
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBSUName" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="vertical-align: top"><span class="field-label">User Name</span></td>
                                    <td align="left" style="vertical-align: top">
                                        <asp:TextBox ID="txtUserName" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="imgUserName" runat="server" OnClientClick="getUserNameNew();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                    <td align="left" style="vertical-align: top"><span class="field-label">Date</span></td>
                                    <td align="left" style="vertical-align: top">
                                        <asp:TextBox ID="txtUserDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnHdate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="btnHdate" TargetControlID="txtUserDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <br />
                                        <asp:CheckBox ID="ChkBox1" Text="Enable" runat="server" AutoPostBack="True" CssClass="field-label" /></td>
                                </tr>
                                <tr id="EmailRow" runat="server">
                                    <td align="left"><span class="field-label">Email</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserEmail" runat="server"></asp:TextBox></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                            <asp:HiddenField ID="h_Usra_id" runat="server" />
                            <asp:HiddenField ID="h_bsu_id" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


