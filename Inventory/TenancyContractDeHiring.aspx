﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TenancyContractDeHiring.aspx.vb" Inherits="Inventory_TenancyContractDeHiring" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/PHOENIXV2/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <style>
        .my-span {
            width: 125px;
            height: 125px;
            display: inline-block;
            vertical-align: top;
            padding: 4px;
            border: 2px solid rgba(0,0,0,0.3);
            margin-right: 2px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        
        function getTRFLIST() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "TRFLISTFORDEHIRING"
            url = "../common/PopupSelect.aspx?&id=" + pMode;        
            var oWnd = radopen(url, "pop_emp");
        }  

        function TabChanged(sender, args) {
            sender.get_clientStateField().value =
               sender.saveClientState();
        }

        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
       1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        

       
        

        function OnEmpClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                
                document.getElementById("<%=h_tcd_tch_ID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtTRFNo.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtStaff.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=TRFDetails.ClientID %>").value = NameandCode[3];    
                document.getElementById("<%=TRFApartmentDetails.ClientID %>").value = NameandCode[4];    
                __doPostBack('<%= txtTRFNo.ClientID%>', 'TextChanged');
                
            }
        }
        
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnEmpClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="pgTitle" Text="" runat="server"></asp:Label>
        </div>
        
                <div>
                    <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr valign="bottom">
                            <td align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                <asp:Label ID="Label5" runat="server" Style="text-align: right" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">

                                    <tr>
                                        <td align="left" width="20%">
                                            <asp:Label ID="lblPRFNo" runat="server" Text="Requisition No" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtTCD_No" runat="server" Enabled="False"></asp:TextBox>
                                        </td>

                                        <td align="left" width="20%">
                                            <asp:Label ID="Label3" runat="server" Text="Requisition Date" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtContractDate" runat="server" Enabled="false"></asp:TextBox>
                                            
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server"
                                                TargetControlID="txtContractDate" PopupButtonID="imgCondate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="field-label"> TRF Details</span></td>
                                        <td align="left" colspan="3">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtTRFNo"   runat="server" Width="140px" Enabled ="false" ></asp:TextBox>
                                                        </td>
                                                    <td>
                                            <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getTRFLIST();return false;" ImageUrl="~/Images/forum_search.gif" ></asp:ImageButton>

                                            
                                                            </td>
                                            
                                                    <td><asp:TextBox ID="txtStaff" Width="1150px" runat="server" Enabled ="false"></asp:TextBox></td>
                                                    
                                                    <asp:HiddenField ID="hTCH_ID" runat="server" />
                                                </tr>
                                            </table>

                                            
                                        </td>
                                        
                                    </tr>

                                    <tr>
                                        <td><span class="field-label">
                                            TRF Details </span>
                                        </td>
                                        <td align="left" colspan="3">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="TRFDetails" Width="1325px" runat="server" Enabled ="false"></asp:TextBox>
                                                    </td>
                                                    </tr></table></td>
                                    </tr>
                                    <tr>
                                        <td> <span class="field-label">
                                            Apartment Details </span>
                                        </td>
                                        <td align="left" colspan="3">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="TRFApartmentDetails" Width="1325px" runat="server" Enabled ="false"></asp:TextBox>
                                                    </td>
                                                    </tr></table></td>
                                    </tr>



                                    <tr>
                                        <td align="left"> <asp:Label ID="Label1" runat="server" Text="Date of Vacating" CssClass="field-label"></asp:Label></td>
                                        <td align="left"> <asp:TextBox ID="txtVacatingDate" runat="server" Enabled="false"></asp:TextBox>
                                            
                                        <asp:ImageButton Visible="true" ID="imgVDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                TargetControlID="txtVacatingDate" PopupButtonID="imgVDate">
                                            </ajaxToolkit:CalendarExtender>    
                                        
                                        </td>

                                        <td align="left"><span class="field-label">Reason for DeHiring</span></td>
                                        <td align="left"> <asp:DropDownList ID="ddlReason"  runat="server"></asp:DropDownList></td>
                                        

                                    </tr>
                                    <tr>
                                        <td align="left"> <asp:Label ID="Label2" runat="server" Text="Date of Landlord Confirmation" CssClass="field-label"></asp:Label></td>
                                        <td align="left"> <asp:TextBox ID="txtLandlordConfirmation" runat="server" Enabled="false"></asp:TextBox>
                                            
                                        <asp:ImageButton Visible="true" ID="imgLDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                                TargetControlID="txtLandlordConfirmation" PopupButtonID="imgLDate">
                                            </ajaxToolkit:CalendarExtender>    
                                        
                                        </td>

                                        <td align="left"><span class="field-label">Notice period provided to Landlord/Agent</span></td>
                                        <td align="left"> 
                                            <asp:RadioButton ID="radLandlordYES" runat="server" GroupName="Landlord" Text="Yes" Checked="false"  />
                                                        <asp:RadioButton ID="radLandlordNo" runat="server" GroupName="Landlord" Checked="true" Text="No"  />
                                        </td>
                                    </tr>
                              
                                     <tr>
                                                        <td width="20%" align="left"><span class="field-label">
                                                            Remarks</span>
                                                        </td>
                                                        <td align="left" width="80%" colspan="3">
                                                            <asp:TextBox ID="txtremarks" runat="server" Width="100%"
                                                                TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>

                                    <tr  runat="server" class="title-bg-lite">

                                        <td align="center" colspan="4" valign="middle">Service Payment Details
                                        </td>
                                    </tr>

                                    <tr id="rptServices" runat="server">
                                        <td colspan="4" valign="middle" width="100%">
                                            <asp:DataList ID="RepService" runat="server" RepeatColumns="6" RepeatDirection="Horizontal">
                                                <ItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="95%">
                                                                <asp:Label ID="lblRESDescr" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                                <asp:Label ID="lblRESID" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("ID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblRowId" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("RowId") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblPerc" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("PERC") %>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td width="5%" >
                                                                <%--<asp:TextBox ID="txtRESAmount" Enabled ="false"  runat ="server" Style="text-align: right" Text='<%# Bind("AMOUNT") %>'></asp:TextBox>--%>
                                                                <asp:Label ID="Label4"  runat="server" Style="text-align: right;font-size:14px !important; " Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                                            </td>
                                                            
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </td>
                                    </tr>
                                    <tr  runat="server" class="title-bg-lite">

                                        <td align="center" colspan="4" valign="middle">Rental Payment Details
                                        </td>
                                    </tr>

                                    <tr>
                                                        <td colspan="4" align="center" widht="100%">
                                                            <asp:GridView ID="grdPayment" runat="server" AutoGenerateColumns="False" PageSize="5"
                                                                Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                                                DataKeyNames="TCP_ID">
                                                                <Columns>
                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTCP_ID" runat="server" Text='<%# Bind("TCP_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField HeaderText="Description">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblITM_DESCR" runat="server" Text='<%# Bind("RES_DESCR") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Type">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPType" runat="server" Style="text-align: left"
                                                                                Text='<%# Bind("TCP_PAYMENTTYPE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Chq Date">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblChqDate" runat="server" Style="text-align: right"
                                                                                Text='<%#Eval("TCP_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                                                                            
                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAmount" runat="server" Style="text-align: right"
                                                                                Text='<%# Bind("TCP_AMOUNT") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Payable To">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPayable" runat="server" Style="text-align: left"
                                                                                Text='<%# Bind("TCP_PAYABLETO") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>
                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>

                                    <tr  runat="server" class="title-bg-lite">

                                        <td align="center" colspan="4" valign="middle">Recovery Payment Details from Staff
                                        </td>
                                    </tr>

                                    <tr>
                                                        <td colspan="4" align="center" width="100%">
                                                            <asp:GridView ID="GrdRec" runat="server" AutoGenerateColumns="False" PageSize="5"
                                                                Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                                                DataKeyNames="TCR_ID">
                                                                <Columns>
                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTCP_ID" runat="server" Text='<%# Bind("TCR_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField HeaderText="Description">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblITM_DESCR" runat="server" Text='<%# Bind("RES_DESCR") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Chq Date">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblChqDate" runat="server" Style="text-align: right"
                                                                                Text='<%#Eval("TCR_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAmount" runat="server" Style="text-align: right"
                                                                                Text='<%# Bind("TCR_AMOUNT") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>


                                    





                                                
                                    <tr>
                                        <td align="left" colspan="4">
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="TabChanged" AutoPostBack="true">
                                                <ajaxToolkit:TabPanel ID="trFlow" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel2" runat="server" ScrollBars="Vertical">
                                                            <asp:Repeater runat="server" ID="rptFlow"><ItemTemplate>
<span class="my-span" style="background-color: <%#DataBinder.Eval(Container.DataItem, "wfcolor")%>;"><%#DataBinder.Eval(Container.DataItem, "wfdescr")%></span>
</ItemTemplate>
</asp:Repeater>

                                                        </asp:Panel>

                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trComments" runat="server" HeaderText="Comments">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtComments" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>
                                                <%--<ajaxToolkit:TabPanel ID="trDocument" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <table width="100%">

                                                            <tr>

                                                                <td align="left" width="20%">
                                                                    <asp:Label ID="lblDocType" runat="server" Text="Document Type" CssClass="field-label"></asp:Label>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlDocType" runat="server" Style="width: 25% !important"></asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%">
                                                                    <asp:FileUpload ID="UploadDocPhoto" runat="server"
                                                                        ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />
                                                                </td>
                                                            </tr>


                                                        </table>




                                                        <asp:Repeater ID="rptImages" runat="server">
                                                            <ItemTemplate>
                                                                [
                                       <asp:TextBox ID="lblPriContentType" runat="server" Visible="false" Text='<%# Bind("tci_contenttype") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriFilename" runat="server" Visible="false" Text='<%# Bind("tci_filename") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriId" runat="server" Visible="false" Text='<%# Bind("tci_id") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="btnDocumentLink" Text='<%# DataBinder.Eval(Container.DataItem, "tci_name") %>' runat="server"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnDelete" Text='[x]' Visible='<%# DataBinder.Eval(Container.DataItem, "tci_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                                                ]
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>--%>
                                                <%--<ajaxToolkit:TabPanel ID="trUpload" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <table width="100%">

                                                            <tr>

                                                                <td align="left" width="20%">
                                                                    <asp:Label ID="Label21" runat="server" CssClass="field-label" Text="Document Type "></asp:Label>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlUploadType" runat="server" Style="width: 25% !important;"></asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%">
                                                                    <asp:FileUpload ID="UploadDocument" runat="server"
                                                                        ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:Button ID="btnUpload1" runat="server" CssClass="button" Text="Upload" />
                                                                </td>
                                                            </tr>


                                                        </table>






                                                        <asp:Repeater ID="rptDocument" runat="server">
                                                            <ItemTemplate>
                                                                [
                                       <asp:TextBox ID="lblPriDocumentContentType" runat="server" Visible="false" Text='<%# Bind("tci_contenttype") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriDocumentFilename" runat="server" Visible="false" Text='<%# Bind("tci_filename") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriDocumentId" runat="server" Visible="false" Text='<%# Bind("tci_id") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="btnDocumentLink1" Text='<%# DataBinder.Eval(Container.DataItem, "tci_name") %>' runat="server"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnDelete1" Text='[x]' Visible='<%# DataBinder.Eval(Container.DataItem, "tci_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                                                ]
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>--%>
                                                <ajaxToolkit:TabPanel ID="trApproval" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtApprovals" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trWorkflow" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel1" runat="server"  ScrollBars="Vertical">
                                                            <asp:GridView ID="gvWorkflow" runat="server" CssClass="table table-bordered table-row"
                                                                EmptyDataText="No Data" Width="98%" AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <asp:BoundField DataField="wrk_Date" HeaderText="Date" />
                                                                    <asp:BoundField DataField="wrk_User" HeaderText="User" />
                                                                    <asp:BoundField DataField="wrk_Action" HeaderText="Action" />
                                                                    <asp:BoundField DataField="wrk_Details" HeaderText="Details" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>


                                            </ajaxToolkit:TabContainer>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnConfirm" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Confirm" Visible="false" OnClientClick="return confirm('Are you sure you want to Confirm This Supplier ?');" />
                                            <input type="button" id="btnAccept" class="button" value=" Approve " runat="server" onclick="this.disabled = true;" onserverclick="btnAccept_Click" />
                                            <asp:Button ID="btnReject" runat="server" CausesValidation="False" CssClass="button" Text="Reject" />
                                            <input type="button" id="btnSend" class="button" value=" Send " runat="server" onclick="this.disabled = true;" onserverclick="btnSend_Click" />
                                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                            <input type="button" id="btnSave" class="button" value="  Save  " runat="server" onclick="this.disabled = true;" onserverclick="btnSave_Click" />
                                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                            <asp:Button ID="btnRecall" runat="server" CausesValidation="False" Visible ="false"  CssClass="button" Text="Recall" />
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_tcd_ID" runat="server" />
                                            <asp:HiddenField ID="h_tcd_tch_ID" runat="server" />
                                            
                                            <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_printed" runat="server" Value="0" />
                                            
                                            <asp:HiddenField ID="h_ThW_apr_id" Value="0" runat="server" />                                            
                                            <asp:HiddenField ID="h_THW_Status" runat="server" />
                                            <asp:FileUpload ID="UploadFile" runat="server" Width="250px" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB'
                                                Visible="false" />
                                            <asp:Button ID="btnUploadPrf" runat="server" CssClass="button" Visible="false" Text="Upload" />
                                            <asp:HyperLink ID="btnExport" runat="server" Visible="false">Export to Excel</asp:HyperLink>
                                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                            
                                            <asp:HiddenField ID="hViewState" Value="" runat="server" />
                                            <asp:HiddenField ID="h_EmpCount" Value="0" runat="server" />

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

            <%--</div>
        </div>
    </div>--%>


    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

    <asp:HiddenField ID="h_MSG2" runat="server" EnableViewState="true" />

</asp:Content>
