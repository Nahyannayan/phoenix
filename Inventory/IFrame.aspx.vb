﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Net
Imports System.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib.Checksums
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.GZip

Partial Class FM_IFrame
    Inherits System.Web.UI.Page
    Dim FileName As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim newImage As Boolean = True, ContentType As String = ""
        h_sessionId.Value = Session.SessionID
        If Request.QueryString("id").ToString = "0" Then
            FileName = Request.QueryString("filename").ToString
        Else
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim rdrDocument As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "select pri_contenttype, Pri_Name, Pri_File from PRF_I where Pri_id=" & Request.QueryString("id").ToString)
            If rdrDocument.HasRows Then
                rdrDocument.Read()
                FileName = Session.SessionID '& rdrDocument("pri_name")
                ContentType = rdrDocument("pri_contenttype")
                Dim fsDataArray As New System.IO.FileStream(WebConfigurationManager.AppSettings.Item("TempFileFolder") & FileName & ".zip", System.IO.FileMode.Create)
                fsDataArray.Write(rdrDocument("pri_file"), 0, rdrDocument("pri_file").Length)
                fsDataArray.Close() : fsDataArray.Dispose()
            End If
            newImage = False
        End If

        If FileName.Length > 0 Then
            Dim fileToDelete As New FileInfo(WebConfigurationManager.AppSettings.Item("TempFileFolder") & FileName & ".zip")
            FileName = UnZip(WebConfigurationManager.AppSettings.Item("TempFileFolder") & FileName & ".zip", WebConfigurationManager.AppSettings.Item("TempFileFolder"))
            If Not newImage Then fileToDelete.Delete()

            Dim ViewServer, ViewURL As String
            ViewServer = Replace(WebConfigurationManager.AppSettings.Item("DocViewerURL"), "*", "&")
            ViewURL = ViewServer & WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & FileName

            If ContentType.Contains("image") Then
                iFrame.Attributes("src") = WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & FileName
            Else
                iFrame.Attributes("src") = ViewURL
            End If
            Dim serverpath As String = ConfigurationManager.AppSettings("TempFileFolder").ToString()
            'lb.CommandArgument = serverpath + FileName
            lb.NavigateUrl = "Default2.aspx?name=" + serverpath + FileName
            iFrame.Attributes("width") = "100%"
            iFrame.Attributes("height") = "500px"
            'iFrame.Attributes("width") = "50px"
            'iFrame.Attributes("height") = "50px"
        End If
    End Sub

    Public Shared Function UnZip(ByVal SrcFile As String, ByVal DstFile As String) As String
        Dim fileStreamIn As FileStream = New FileStream(SrcFile, FileMode.Open, FileAccess.Read)
        Dim zipInStream As ZipInputStream = New ZipInputStream(fileStreamIn)
        Dim enTry As ZipEntry = zipInStream.GetNextEntry()
        Dim fileStreamOut As FileStream = New FileStream(DstFile + "\" + enTry.Name, FileMode.Create, FileAccess.Write)
        Dim size As Integer
        Dim buffer() As Byte = New Byte(4096) {}
        Do
            size = zipInStream.Read(buffer, 0, buffer.Length)
            fileStreamOut.Write(buffer, 0, size)
        Loop While size > 0
        zipInStream.Close()
        fileStreamOut.Close()
        fileStreamIn.Close()
        Return enTry.Name
    End Function

    <System.Web.Services.WebMethod()> _
        Public Shared Sub TestMethod(ByVal sessionId As String)
        Dim strFilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder")
        Dim FilesToDelete As New System.IO.DirectoryInfo(strFilepath)
        Dim Files As System.IO.FileInfo() = FilesToDelete.GetFiles(sessionId & "*")
        Dim FileToDelete As FileInfo
        For Each FileToDelete In Files
            If FileToDelete.Name.Contains(sessionId) Then
                Try
                    FileToDelete.Delete()
                Catch ex As Exception

                End Try
            End If
        Next
    End Sub

End Class
