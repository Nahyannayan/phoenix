﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class Inventory_Reports_Aspx_rptBS_SchoolItems
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            BindGrade()
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            'lblError.Text = ""
        End If
    End Sub

    'Private Sub PopulateGrades()
    '    ddlPrintedFor.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String = " SELECT DISTINCT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM VW_GRADE_M ORDER BY GRD_DISPLAYORDER "
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlPrintedFor.DataSource = ds
    '    ddlPrintedFor.DataTextField = "GRD_DISPLAY"
    '    ddlPrintedFor.DataValueField = "GRD_ID"
    '    ddlPrintedFor.DataBind()
    '    ddlPrintedFor.Items.Insert(0, (New RadComboBoxItem("ALL", "00")))
    '    ddlPrintedFor.ClearSelection()
    '    ddlPrintedFor.Items.FindItemByText("ALL").Selected = True
    '    ddlPrintedFor.Enabled = True
    'End Sub
    Private Sub BindGrade()
        Dim conn_str As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim sql_query As String = " SELECT '' GRD_ID,'ALL' GRD_DISPLAY,0 GRD_DISPLAYORDER  UNION ALL SELECT DISTINCT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM VW_GRADE_M ORDER BY GRD_DISPLAYORDER "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
            'Dim dr As DataRow = dsData.Tables(0).NewRow
            'dr(0) = ""
            'dr(1) = "ALL"
            'dsData.Tables(0).Rows.Add(dr)
        End If
        ddlGrade.DataSource = dsData
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count > 0 Then
            'ddlGrade.Items.FindByValue("-1").Selected = True
        End If
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        GenerateSchoolItems()

    End Sub
    'Function GetSelectedGrades() As String
    '    Dim str As String = ""
    '    Dim collection As IList(Of RadComboBoxItem) = ddlPrintedFor.CheckedItems

    '    If (collection.Count <> 0) Then
    '        For Each item As RadComboBoxItem In collection
    '            If item.Text = "ALL" Then
    '                str += item.Value
    '            Else
    '                str += item.Text
    '            End If

    '            If str <> "" Then
    '                str += "|"
    '            End If
    '        Next
    '    End If
    '    Return str

    'End Function
    Private Sub GenerateSchoolItems()
        Dim cmd As New SqlCommand("[dbo].[GET_BOOK_ITEM_M]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpOPTIONS As New SqlParameter("@OPTIONS", SqlDbType.Int)
        sqlpOPTIONS.Value = 3
        cmd.Parameters.Add(sqlpOPTIONS)
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuId")
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 20)
        sqlpGRD_ID.Value = ddlGrade.SelectedValue
        cmd.Parameters.Add(sqlpGRD_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim caption = "Book Sale School Items"
        
        params("userName") = Session("sUsr_name")
        
        params("RPT_CAPTION") = caption

        params("Grade") = ddlGrade.SelectedItem.Text

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
      
        repSource.ResourceName = "../../Inventory/Reports/RPT/rptBS_SchoolItems.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
            ReportLoadSelectionExport()
        Else
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelectionExport()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
End Class
