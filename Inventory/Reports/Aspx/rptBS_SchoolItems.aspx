﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="rptBS_SchoolItems.aspx.vb" Inherits="Inventory_Reports_Aspx_rptBS_SchoolItems" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sale Item Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>

                <table runat="server" style="width: 100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Grade</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label"></span></td>
                        <td align="left" width="30%"></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CausesValidation="False" CssClass="button"
                                Text="Generate Report" UseSubmitBehavior="False" TabIndex="8" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
