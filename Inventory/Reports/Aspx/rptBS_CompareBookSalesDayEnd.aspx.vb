﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports Telerik.Web.UI
Partial Class Inventory_Reports_Aspx_rptBS_CompareBookSalesDayEnd
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim MainMnu_code As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Me.txtFromDate.Text = Format(Date.Now, OASISConstants.DateFormat)
            Me.txtToDate.Text = Format(Date.Now, OASISConstants.DateFormat)

        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            'lblError.Text = ""
        End If
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        GenerateCompareBookSalesDayEnd()

    End Sub

    Private Sub GenerateCompareBookSalesDayEnd()
        Dim cmd As New SqlCommand("[dbo].[GET_BETWEEN_DATES]")
        cmd.CommandType = CommandType.StoredProcedure

        'Dim sqlpBSU_ID As New SqlParameter("@BSU_IDS", SqlDbType.VarChar)
        'sqlpBSU_ID.Value = Session("sBsuId")
        'cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpFromDate As New SqlParameter("@FROMDT", SqlDbType.Date)
        sqlpFromDate.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDate)
        Dim sqlpToDate As New SqlParameter("@TODT", SqlDbType.Date)
        sqlpToDate.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpToDate)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim caption = "Compare Book Sales Day End"

        params("userName") = Session("sUsr_name")

        params("reportCaption") = caption

        params("FilterDate") = txtFromDate.Text & " - " & txtToDate.Text



        'params("@BSU_IDS") = Session("sBsuId")
        'params("@FROMDT") = CDate(txtFromDate.Text)
        'params("userName") = Session("sUsr_name")


        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True


        Dim repSourceSubRep(3) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSub1 As New SqlCommand("[dbo].[GET_BOOK_SALES_COLLECTION_SUMMARY]")
        cmdSub1.CommandType = CommandType.StoredProcedure


        Dim sqlpBSU_ID_SUB1 As New SqlParameter("@BSU_IDS", SqlDbType.VarChar)
        sqlpBSU_ID_SUB1.Value = Session("sBsuId")
        cmdSub1.Parameters.Add(sqlpBSU_ID_SUB1)
        Dim sqlpFROMDATE_SUB1 As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDATE_SUB1.Value = CDate(txtFromDate.Text)
        cmdSub1.Parameters.Add(sqlpFROMDATE_SUB1)
        Dim sqlpTODATE_SUB1 As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODATE_SUB1.Value = CDate(txtToDate.Text)
        cmdSub1.Parameters.Add(sqlpTODATE_SUB1)

        cmdSub1.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        repSourceSubRep(0).Command = cmdSub1


        repSourceSubRep(1) = New MyReportClass
        Dim cmdSub2 As New SqlCommand("[dbo].[GET_BOOK_SALES_DAX_POSTING_SUMMARY]")
        cmdSub2.CommandType = CommandType.StoredProcedure


        Dim sqlpBSU_ID_SUB2 As New SqlParameter("@BSU_IDS", SqlDbType.VarChar)
        sqlpBSU_ID_SUB2.Value = Session("sBsuId")
        cmdSub2.Parameters.Add(sqlpBSU_ID_SUB2)
        Dim sqlpFROMDATE_SUB2 As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDATE_SUB2.Value = CDate(txtFromDate.Text)
        cmdSub2.Parameters.Add(sqlpFROMDATE_SUB2)
        Dim sqlpTODATE_SUB2 As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODATE_SUB2.Value = CDate(txtToDate.Text)
        cmdSub2.Parameters.Add(sqlpTODATE_SUB2)

        cmdSub2.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        repSourceSubRep(1).Command = cmdSub2


        repSourceSubRep(2) = New MyReportClass
        Dim cmdSub3 As New SqlCommand("[dbo].[GET_BOOK_SALES_DAYEND_POSTING_SUMMARY]")
        cmdSub3.CommandType = CommandType.StoredProcedure


        Dim sqlpBSU_ID_SUB3 As New SqlParameter("@BSU_IDS", SqlDbType.VarChar)
        sqlpBSU_ID_SUB3.Value = Session("sBsuId")
        cmdSub3.Parameters.Add(sqlpBSU_ID_SUB3)
        Dim sqlpFROMDATE_SUB3 As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDATE_SUB3.Value = CDate(txtFromDate.Text)
        cmdSub3.Parameters.Add(sqlpFROMDATE_SUB3)
        Dim sqlpTODATE_SUB3 As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODATE_SUB3.Value = CDate(txtToDate.Text)
        cmdSub3.Parameters.Add(sqlpTODATE_SUB3)

        cmdSub3.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        repSourceSubRep(2).Command = cmdSub3



        repSource.SubReport = repSourceSubRep

        repSource.ResourceName = "../../Inventory/Reports/RPT/rptBS_CompareBookSalesDayEnd.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
            ReportLoadSelectionExport()
        Else
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelectionExport()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
End Class
