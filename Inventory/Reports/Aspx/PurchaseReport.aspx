<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PurchaseReport.aspx.vb" Inherits="PurchaseReport" Title="Untitled Page" %>

<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function getAccount() {
           // var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
           // sFeatures = "dialogWidth: 760px; ";
           // sFeatures += "dialogHeight: 420px; ";
           // sFeatures += "help: no; ";
           // sFeatures += "resizable: no; ";
           // sFeatures += "scroll: yes; ";
           // sFeatures += "status: no; ";
           // sFeatures += "unadorned: no; ";
            pMode = "ITMACCOUNT"

            url = "../../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "popup1");
            //if (result == '' || result == undefined) {

            //    return false;
            //}
            //NameandCode = result.split('___');
            
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
           
            var arg = args.get_argument();
          
            if (arg) {                               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=h_Actid.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtACTID.ClientID %>").value = NameandCode[2];
                __doPostBack('<%= txtACTID.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function getClientName() {
          //  var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            //sFeatures = "dialogWidth: 760px; ";
            //sFeatures += "dialogHeight: 420px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            pMode = "ITMCLIENT"
            url = "../../../common/PopupSelect.aspx?id=" + pMode + "&jobtype=F";
            result = radopen(url, "popup2");
            <%--if (result == '' || result == undefined) return false;
            NameandCode = result.split('___');
            document.getElementById("<%=txtSupplier.ClientID %>").value = NameandCode[0];
          document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = NameandCode[1];--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
           
            var arg = args.get_argument();
          
            if (arg) {                               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtSupplier.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtSupplier.ClientID%>', 'TextChanged');
            }
        }

      function GetCATName() {
          //var sFeatures;
          //sFeatures = "dialogWidth: 729px; ";
          //sFeatures += "dialogHeight: 445px; ";
          //sFeatures += "help: no; ";
          //sFeatures += "resizable: no; ";
          //sFeatures += "scroll: yes; ";
          //sFeatures += "status: no; ";
          //sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          pMode = "ITMUNSPSCSEG"
          url = "../../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1";
          result = radopen(url, "popup3");
         <%-- if (result == '' || result == undefined) return false;
          document.getElementById("<%=h_CATID.ClientID %>").value = result;
          document.forms[0].submit();--%>

      }
        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            
            var arg = args.get_argument();
          
            if (arg) {                               
                //NameandCode = arg.NameCode.split('___');
                document.getElementById("<%=h_CATID.ClientID %>").value = arg.NameandCode;
                __doPostBack('<%= txtCatName.ClientID%>', 'TextChanged');
                document.forms[0].submit();
            }
        }
      function GetSubCATName() {
          //var sFeatures;
          //sFeatures = "dialogWidth: 729px; ";
          //sFeatures += "dialogHeight: 445px; ";
          //sFeatures += "help: no; ";
          //sFeatures += "resizable: no; ";
          //sFeatures += "scroll: yes; ";
          //sFeatures += "status: no; ";
          //sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          pMode = "ITMUNSPSC"
          var cat = document.getElementById('<%= h_CATID.ClientID %>').value;
          url = "../../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1&cat=" + cat;
          result = radopen(url, "popup4");
          <%--if (result == '' || result == undefined) return false;
          document.getElementById("<%=h_SubCATID.ClientID %>").value = result;
          document.forms[0].submit();--%>
      }

      function OnClientClose4(oWnd, args) {
            //get the transferred arguments
           
            var arg = args.get_argument();
          
            if (arg) {                               
              //  NameandCode = arg.NameCode.split('___');
                document.getElementById("<%=h_SubCATID.ClientID %>").value = arg.NameandCode;
                 __doPostBack('<%= txtSubCatName.ClientID%>', 'TextChanged');
                document.forms[0].submit();
            }
        }

      function ToggleSearchCategory() {
          if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
              document.getElementById('<%=trCategory.ClientID %>').style.display = '';
              document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
              document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
          }
          return false;
      }

      function ToggleSearchSubCategory() {
          if (document.getElementById('<%=hfSubCategory.ClientID %>').value == 'display') {
              document.getElementById('<%=trSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=trSelSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfSubCategory.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=hfSubCategory.ClientID %>').value = 'display';
          }
          return false;
      }

      function CheckOnPostback() {
          //          
          if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
              document.getElementById('<%=trCategory.ClientID %>').style.display = '';
              document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
          }
          //         
          //         
          return false;
      }
    </script>
      <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="popup1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
               <telerik:RadWindow ID="popup2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          </Windows>
          <Windows >
               <telerik:RadWindow ID="popup3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          </Windows>
           <Windows >
               <telerik:RadWindow ID="popup4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            <asp:Label ID="pgTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%"
                                cellpadding="5" cellspacing="0"
                                id="TB1" runat="server">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="2" valign="middle">
                            </td>                        
                        </tr>  --%>
                                <tr id="trDateRange" runat="server">
                                    <td colspan="4" align="left" class="title-bg ">Date Range</td>
                                </tr>
                                <tr>

                                    <td width="20%"><span class="field-label"> Range </span></td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtFDate" runat="server"  CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="lnkFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFDate" PopupButtonID="lnkFDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="20%"> <span class="field-label">To Date </span></td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtTDate" runat="server"  CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="lnkTDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTDate" PopupButtonID="lnkTDate"></ajaxToolkit:CalendarExtender>
                                    </td>

                                </tr>
                                <tr>
                                     <td colspan="4" align="left" ><br /></td>
                                </tr>
                                <tr>
                                     <td colspan="4" align="left" class="title-bg ">Details</td>
                                </tr>
                                <tr id="trSupplier">
                                    <td width="20%" align="left"><span class="field-label">Supplier</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtSupplier" runat="server"  CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getClientName();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr id="trBudget" runat="server" visible="false">
                                    <td width="20%" align="left"><span class="field-label">Budget Account</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtACTID" runat="server"  CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="imgActid" runat="server" OnClientClick="getAccount();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" /><asp:HiddenField ID="h_Actid" runat="server"></asp:HiddenField>
                                    </td>
                                     <td colspan="2"></td>
                                </tr>
                                <tr id="trSelcategory">
                                    <td align="left" colspan="4" valign="top">
                                        <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                                       <span class="field-label"> Select Segment Filter</span></td>
                                </tr>
                                <tr id="trCategory" style="display: none;">
                                    <td width="20%" align="left" valign="top">
                                        <asp:ImageButton ID="imgMinusCategory" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" /><span class="field-label">Segment</span></td>
                                    <td width="30%" align="left" style="text-align: left"
                                        nowrap="nowrap">(Enter the Segment ID you want to Add to the Search and click on Add)<br />
                                        <asp:TextBox ID="txtCatName" runat="server" CssClass="inputbox"   AutoPostBack="true" OnTextChanged="txtCatName_TextChanged"></asp:TextBox>
                                        <asp:LinkButton ID="lnlbtnAddCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetCATName(); return false;" /><br />
                                        <asp:GridView ID="gvCat" runat="server" AutoGenerateColumns="False"  AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="CAT ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr id="trSelSubCategory">
                                    <td align="left" colspan="4" valign="top">
                                        <asp:ImageButton ID="imgPlusSubCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchSubCategory();return false;" CausesValidation="False" />
                                       <span class="field-label"> Select Commodity Filter</span></td>
                                </tr>
                                <tr id="trSubCategory" style="display: none;">
                                    <td  width="20%" align="left" valign="top">
                                        <asp:ImageButton ID="imgMinusSubCategory" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchSubCategory();return false;" CausesValidation="False" />&nbsp;<span class="field-label">Commodity</span></td>
                                    <td width="30%" align="left" style="text-align: left">(Enter the Commodity ID you want to Add to the Search and click on Add)<br />
                                        <asp:TextBox ID="txtSubCatName" runat="server" CssClass="inputbox"   AutoPostBack="true" OnTextChanged="txtSubCatName_TextChanged"></asp:TextBox>
                                        <asp:LinkButton ID="lnlbtnAddSubCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImageButton4" runat="server"
                                            ImageUrl="~/Images/forum_search.gif" OnClientClick="GetSubCATName(); return false;" /><br />
                                        <asp:GridView ID="gvSubCat" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SUB CAT ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DESCR" HeaderText="SUB CAT NAME" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdSubCATDelete" runat="server" OnClick="lnkbtngrdSubCATDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr id="trBSUnit" runat="server">
                                    <td  width="20%" align="left" valign="top" ><span class="field-label">Business Unit</span ></td>
                                    <td width="30%" align="left" valign="top">
                                        <div class="checkbox-list"><uc1:usrBSUnits ID="UsrBSUnits1" runat="server" /></div>                                        
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr id="trSummary" runat="server">
                                    <td  width="20%" align="left" valign="top" ><span class="field-label">Report Type</span></td>
                                    <td width="30%" align="left" valign="top">
                                        <asp:CheckBox runat="server" ID="chkReport" Text="Summary Report" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td  align="center" valign="bottom">
                            <asp:Button ID="btnGenerateReport" runat="server" CausesValidation="False" CssClass="button"
                                Text="Generate Report" UseSubmitBehavior="False" TabIndex="8" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" >
                            <asp:HiddenField ID="hfMDM_ID" runat="server" />
                            <input id="hfCategory" runat="server" type="hidden" />
                            <input id="hfSubCategory" runat="server" type="hidden" />
                        </td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_CATID" runat="server" />
                <asp:HiddenField ID="h_SubCATID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>



