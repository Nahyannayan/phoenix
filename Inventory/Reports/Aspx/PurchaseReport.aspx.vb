﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Partial Class PurchaseReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim MainMnu_code As String

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||", "|")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdSubCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSubCATID As New Label
        lblSubCATID = TryCast(sender.FindControl("lblSubCATID"), Label)
        If Not lblSubCATID Is Nothing Then
            h_SubCATID.Value = h_SubCATID.Value.Replace(lblSubCATID.Text, "").Replace("||", "|")
            gvSubCat.PageIndex = gvSubCat.PageIndex
            FillSubCATNames(h_SubCATID.Value)
        End If
    End Sub

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean
        Dim IDs As String() = CATIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT  distinct USG_ID as ID, USG_DESCR as DESCR FROM UNSPSC_SEGMENT WHERE USG_ID in (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillSubCATNames(ByVal SubCATIDs As String) As Boolean
        Dim IDs As String() = SubCATIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT UCO_ID as ID, UCO_DESCR as DESCR FROM UNSPSC_COMMODITY where UCO_CODE IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvSubCat.DataSource = ds
        gvSubCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then
                hfCategory.Value = "display"
                txtFDate.Text = Now.ToString("dd/MMM/yyyy")
                txtTDate.Text = Now.ToString("dd/MMM/yyyy")
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            ClientScript.RegisterStartupScript(Me.GetType(), "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim ReportName As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select mnu_text from oasis..menus_m where mnu_code='" & MainMnu_code & "'")
            Page.Title = OASISConstants.Gemstitle

            pgTitle.Text = ReportName
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "PI05010" 'Framework
                    Me.trBSUnit.Visible = False
                    Me.trSummary.Visible = False
                    Me.trbudget.visible = False
                Case "PI05020" 'Procurement Ledger
                    Me.trSummary.Visible = False
                    Me.trbudget.visible = False
                Case "PI05030" 'Purchase Summary
                    Me.trSummary.Visible = False
                    Me.trBudget.Visible = False
                Case "PI05035" 'Capex Summary
                    Me.trBudget.Visible = False
                Case "PI05040" 'Goods Receipt
                    Me.trBudget.Visible = False
                Case "PI05050" 'Purchase Journal
                    Me.trBudget.Visible = False
                Case "PI05055" 'Monthly MIS-Purchases Provision
                    trSummary.Visible = False
                    trSelSubCategory.Visible = False
                    trCategory.Visible = False
                    trSelcategory.Visible = False
                Case "PI05060" 'Purchase Analysis
                    Me.trBudget.Visible = False
                Case "PI05070", "PI05090", "PI05095" 'Sales Reports
                    Me.trBudget.Visible = False
                    trSupplier.Visible = False
                    trSelSubCategory.Visible = False
                    trCategory.Visible = False
                    trSelcategory.Visible = False
                Case "PI09001"
                    'Me.trSummary.Visible=False  
                    Me.trBudget.Visible = False
                Case "PI09002"
                    Me.trSummary.Visible = False
                    Me.trBudget.Visible = True
                Case "PI09010"
                    Me.trBudget.Visible = False
                    trSupplier.Visible = False
                    trSelSubCategory.Visible = False
                    trCategory.Visible = False
                    trSelcategory.Visible = False
                    trBSUnit.Visible = True
                    trSummary.Visible = False
            End Select
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            FillCATNames(h_CATID.Value)
            FillSubCATNames(h_SubCATID.Value)
            If Not Session("PurchaseReport_FromDate") Is Nothing Then
                'Me.txtFDate.Text = Session("PurchaseReport_FromDate")
            End If
            If Not Session("PurchaseReport_ToDate") Is Nothing Then
                'Me.txtTDate.Text = Session("PurchaseReport_ToDate")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub GenerateReports()
        Try
            If Not Session("PurchaseReport_FromDate") Is Nothing Then
                Session.Remove("PurchaseReport_FromDate")
            End If
            If Not Session("PurchaseReport_ToDate") Is Nothing Then
                Session.Remove("PurchaseReport_ToDate")
            End If
            Session.Add("PurchaseReport_FromDate", Me.txtFDate.Text)
            Session.Add("PurchaseReport_ToDate", Me.txtTDate.Text)
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim strABC As String = String.Empty
            Dim strBsuid As String = ""
            Dim strCatIds As String = ""
            Dim strSubCatIDs As String = ""
            strCatIds = h_CATID.Value.ToString
            strSubCatIDs = h_SubCATID.Value.ToString

            UsrBSUnits1.MenuCode = MainMnu_code
            strBsuid = UsrBSUnits1.GetSelectedNode()

            Dim str_Sql As String = "rptPurchase"

            Select Case MainMnu_code
                Case "PI05050" 'Purchase Journal
                    If Not chkReport.Checked Then
                    Else
                        str_Sql = "rptPurchase_Summary"
                    End If
                Case "PI05060" 'Purchase Analysis
                    If Not chkReport.Checked Then
                    Else
                        str_Sql = "rptPurchase_Summary"
                    End If
                Case "PI05070", "PI05090", "PI05095" 'Sales Reports
                    str_Sql = "rptPurchase_Summary"
                Case "PI09010"
                    str_Sql = "rptTenancyContractCheckList"
            End Select
            Dim params As New Hashtable
            ' params("userName") = Session("sUsr_name")

            params.Add("userName", Session("sUsr_name"))
            params.Add("@IMG_TYPE", "LOGO")


            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = New SqlConnection(str_conn)
            If MainMnu_code = "PI09010" Then

                params.Add("@FROMDATE", txtFDate.Text)
                params.Add("@TODATE", txtTDate.Text)
                params.Add("@BSU", strBsuid)

                Dim sqlParam(3) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@FROMDATE", txtFDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@TODATE", txtTDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = Mainclass.CreateSqlParameter("@BSU", strBsuid, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(2))
            Else

                params.Add("@ACT_ID", hTPT_Client_ID.Value)
                params.Add("@RPT_MENU", MainMnu_code)
                params.Add("@FROMDATE", txtFDate.Text)
                params.Add("@TODATE", txtTDate.Text)

                Dim sqlParam(9) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@ACT_ID", hTPT_Client_ID.Value, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@RPT_MENU", MainMnu_code, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = Mainclass.CreateSqlParameter("@FROMDATE", txtFDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(2))
                sqlParam(3) = Mainclass.CreateSqlParameter("@TODATE", txtTDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(3))
                If strBsuid.Length = 0 Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@BSU", Session("sBsuid"), SqlDbType.VarChar)
                    params.add("@BSU", Session("sBsuid"))
                Else
                    sqlParam(4) = Mainclass.CreateSqlParameter("@BSU", strBsuid, SqlDbType.VarChar)
                    params.add("@BSU", strBsuid)
                End If
                params.Add("@IMG_BSU_ID", Session("sBsuid"))
                params.Add("@SEGMENT", strCatIds)
                params.Add("@COMMODITY", strSubCatIDs)
                params.Add("@FLAG", IIf(chkReport.Checked, "1", "0"))
                params.Add("@Budget_Act_Id", Me.h_Actid.Value)
                params.Add("@User", Session("sUsr_name"))

                cmd.Parameters.Add(sqlParam(4))
                sqlParam(5) = Mainclass.CreateSqlParameter("@SEGMENT", strCatIds, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(5))
                sqlParam(6) = Mainclass.CreateSqlParameter("@COMMODITY", strSubCatIDs, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(6))
                sqlParam(7) = Mainclass.CreateSqlParameter("@FLAG", IIf(chkReport.Checked, "1", "0"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(7))
                sqlParam(8) = Mainclass.CreateSqlParameter("@Budget_Act_Id", Me.h_Actid.Value, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(8))
                sqlParam(9) = Mainclass.CreateSqlParameter("@User", Session("sUsr_name"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(9))
            End If

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "OASIS_PUR_INV"
            End With
            Dim reportpath As String = ""

            ' params.Add("reportCaption", pgTitle.Text)

            Select Case MainMnu_code
                Case "PI05010" 'Framework
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    ' repSource.ResourceName = "../../Reports/RPT/rptFramework.rpt"
                    reportpath = "../../Reports/RPT/rptFramework.rpt"
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)

                Case "PI05020" 'Procurement Ledger
                    params.Add("reportCaption", pgTitle.Text)
                    'repSource.ResourceName = "../../Reports/RPT/rptPRFLedger.rpt"
                    reportpath = "../../Reports/RPT/rptPRFLedger.rpt"
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05030" 'Purchase Summary
                    params.Add("reportCaption", pgTitle.Text)
                    ' repSource.ResourceName = "../../Reports/RPT/rptQutLedger.rpt"
                    reportpath = "../../Reports/RPT/rptQutLedger.rpt"
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05035" 'Capex Ledger
                    params.Add("reportCaption", pgTitle.Text)
                    'repSource.ResourceName = "../../Reports/RPT/rptPRFLedger.rpt"
                    reportpath = "../../Reports/RPT/rptPRFLedger.rpt"
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05040" 'Goods Receipt Note Ledger
                    If Not chkReport.Checked Then
                        params.Add("reportCaption", "Goods Receipt Ledger")
                        params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                        '   repSource.ResourceName = "../../Reports/RPT/rptGrnLedger.rpt"
                        reportpath = "../../Reports/RPT/rptGrnLedger.rpt"
                        rptClass.reportParameters = params
                        rptClass.reportPath = Server.MapPath(reportpath)
                    Else
                        params.Add("reportCaption", "Goods Receipt Ledger Summary")
                        params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                        '  repSource.ResourceName = "../../Reports/RPT/rptGrnLedger_Summary.rpt"
                        reportpath = "../../Reports/RPT/rptGrnLedger_Summary.rpt"
                        rptClass.reportParameters = params
                        rptClass.reportPath = Server.MapPath(reportpath)
                    End If
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                Case "PI05050" 'Purchase Journal
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    If Not chkReport.Checked Then
                        params.Add("reportCaption", pgTitle.Text)
                        ' repSource.ResourceName = "../../Reports/RPT/rptPurchaseJournalLedger.rpt"
                        reportpath = "../../Reports/RPT/rptPurchaseJournalLedger.rpt"
                    Else
                        str_Sql = "rptPurchase_Summary"
                        params.Add("reportCaption", pgTitle.Text & " (Summary)")
                        ' repSource.ResourceName = "../../Reports/RPT/rptPurchaseJournalLedger_Summary.rpt"
                        reportpath = "../../Reports/RPT/rptPurchaseJournalLedger_Summary.rpt"
                    End If
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05055" 'Monthly MIS-Purchases Provision
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    ' repSource.ResourceName = "../../Reports/RPT/rptPurchaseMIS.rpt"
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05060" 'Purchase Analysis

                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    'repSource.ResourceName = "../../Reports/RPT/rptPurchase2.rpt"
                    If Not chkReport.Checked Then
                        params.Add("reportCaption", pgTitle.Text)
                        ' repSource.ResourceName = "../../Reports/RPT/rptPurchaseAnalysis.rpt"
                        reportpath = "../../Reports/RPT/rptPurchaseAnalysis.rpt"
                    Else
                        str_Sql = "rptPurchase_Summary"
                        params.Add("reportCaption", pgTitle.Text & " (Summary)")
                        '  repSource.ResourceName = "../../Reports/RPT/rptPurchaseAnalysis_Summary.rpt"
                        reportpath = "../../Reports/RPT/rptPurchaseAnalysis_Summary.rpt"
                    End If
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05070" 'Sales Collection
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    'repSource.ResourceName = "../../Reports/RPT/rptPurchase2.rpt"
                    If chkReport.Checked Then
                        ' repSource.ResourceName = "../../Reports/RPT/rptSaleSummary.rpt"
                        reportpath = "../../Reports/RPT/rptSaleSummary.rpt"
                    Else
                        '  repSource.ResourceName = "../../Reports/RPT/rptSaleCollection.rpt"
                        reportpath = "../../Reports/RPT/rptSaleCollection.rpt"
                    End If
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05090" 'Daily Report of Credit Card
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    ' repSource.ResourceName = "../../Reports/RPT/rptDailyCard.rpt"
                    reportpath = "../../Reports/RPT/rptDailyCard.rpt"
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI05095" 'Daily Receipt Journal by Payment
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    ' repSource.ResourceName = "../../Reports/RPT/rptDailyJournal.rpt"
                    reportpath = "../../Reports/RPT/rptDailyJournal.rpt"
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI09001" 'Pending PRF
                    str_Sql = "rptPurchase"
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    If Not chkReport.Checked Then
                        params.Add("reportCaption", pgTitle.Text)
                        ' repSource.ResourceName = "../../Reports/RPT/rptPendingPRF.rpt"
                        reportpath = "../../Reports/RPT/rptPendingPRF.rpt"
                    Else
                        params.Add("reportCaption", pgTitle.Text & " (Summary)")
                        ' repSource.ResourceName = "../../Reports/RPT/rptPendingPRF_Summary.rpt"
                        reportpath = "../../Reports/RPT/rptPendingPRF_Summary.rpt"
                    End If
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI09002" 'Capex Utilization
                    str_Sql = "rptPurchase"
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "As On " & Me.txtTDate.Text)
                    'repSource.ResourceName = "../../Reports/RPT/rptCapexUtilization.rpt"
                    reportpath = "../../Reports/RPT/rptCapexUtilization.rpt"
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI09010" 'TenancyContractCheckList
                    str_Sql = "rptTenancyContractCheckList"
                    params.Add("reportCaption", pgTitle.Text)
                    ' repSource.ResourceName = "../../Reports/RPT/rptTenancyContractCheckList.rpt"
                    reportpath = "../../Reports/RPT/rptTenancyContractCheckList.rpt"
                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
            End Select
            ' repSource.IncludeBSUImage = True
            ' repSource.DisplayGroupTree = False
            ' repSource.Parameter = params
            ' repSource.Command = cmd
            ' Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Session("rptClass") = rptClass
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvSubCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubCat.PageIndexChanging
        gvSubCat.PageIndex = e.NewPageIndex
        FillSubCATNames(h_SubCATID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "|" + txtCatName.Text.Replace(",", "|")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnSubAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddSubCATID.Click
        h_SubCATID.Value += "|" + txtSubCatName.Text.Replace(",", "|")
        FillSubCATNames(h_SubCATID.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        GenerateReports()
    End Sub

    Protected Sub txtCatName_TextChanged(sender As Object, e As EventArgs)
        h_CATID.Value += "|" + txtCatName.Text.Replace(",", "|")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub txtSubCatName_TextChanged(sender As Object, e As EventArgs)
        h_SubCATID.Value += "|" + txtSubCatName.Text.Replace(",", "|")
        FillSubCATNames(h_SubCATID.Value)
    End Sub
End Class
