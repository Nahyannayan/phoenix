﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class ItemSale
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI01140") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBSUID")
            pParms(1) = New SqlClient.SqlParameter("@ITM_ID", SqlDbType.Int)
            pParms(1).Value = p_Modifyid

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.GET_ITEM_SALE", pParms)
            'Dim dt As New DataTable
            'dt = MainObj.getRecords("select ITM_ID, isnull(ITB_DESCR,ITM_DESCR) ITM_DESCR,ITM_ISBN,ITM_CATEGORY,ITM_PUBLICATION from ITEM_SALE left outer join ITEM_SALEBSU on ITM_ID=ITB_ITM_ID and ITB_BSU_ID='" & Session("sBSUID") & "' where ITM_ID=" & p_Modifyid, "OASIS_PUR_INVConnectionString")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                h_ITM_ID.Value = ds.Tables(0).Rows(0)("ITM_ID").ToString
                txtItemDescr.Text = ds.Tables(0).Rows(0)("ITM_DESCR").ToString
                txtitemISBN.Text = ds.Tables(0).Rows(0)("ITM_ISBN").ToString
                txtitemCategory.Text = ds.Tables(0).Rows(0)("ITM_CATEGORY").ToString
                txtItemPublication.Text = ds.Tables(0).Rows(0)("ITM_PUBLICATION").ToString
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        txtItemDescr.Enabled = Not mDisable
        txtitemCategory.Enabled = Not mDisable
        txtitemISBN.Enabled = Not mDisable
        txtItemPublication.Enabled = Not mDisable

    End Sub

    Sub clear_All()
        h_ITM_ID.Value = 0
        txtItemDescr.Text = ""
        txtitemCategory.Text = ""
        txtitemISBN.Text = ""
        txtItemPublication.Text = ""
        ViewState("EntryId") = 0
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from ITEM_SALEBSU  where ITB_ITM_ID=" & h_ITM_ID.Value) = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from ITEM_SALE where ITM_ID=" & h_ITM_ID.Value)
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = "unable to delete, " & txtItemDescr.Text & " used in transactions"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            If txtItemDescr.Text.Trim = "" Then
                lblError.Text = "Item description cannot be blank"
                Exit Sub
            End If
            If txtitemISBN.Text.Trim = "" Then
                lblError.Text = "ISBN cannot be blank"
                Exit Sub
            End If

            Dim pParms(7) As SqlParameter
            pParms(0) = New SqlParameter("@ITM_ID", SqlDbType.Int)
            'pParms(0).Direction = ParameterDirection.Output
            pParms(0).Value = h_ITM_ID.Value
            pParms(1) = New SqlParameter("@ITM_DESCR", SqlDbType.VarChar, 100)
            pParms(1).Value = txtItemDescr.Text
            pParms(2) = New SqlParameter("@ITM_ISBN", SqlDbType.VarChar, 20)
            pParms(2).Value = txtitemISBN.Text
            pParms(3) = New SqlParameter("@ITM_CATEGORY", SqlDbType.VarChar, 50)
            pParms(3).Value = txtitemCategory.Text
            pParms(4) = New SqlParameter("@ITM_PUBLICATION", SqlDbType.VarChar, 50)
            pParms(4).Value = txtItemPublication.Text
            pParms(5) = New SqlParameter("@ITB_BSU_ID", SqlDbType.VarChar, 20)
            pParms(5).Value = Session("sBsuId")
            pParms(6) = New SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            pParms(7) = New SqlParameter("@NEW_ITM_ID", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.Output

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction()
            Try
                Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.ItemSale", pParms)
                If pParms(6).Value <> 0 Then
                    lblError.Text = UtilityObj.getErrorMessage(pParms(6).Value)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(7).Value
                    lblError.Text = "Data Saved Successfully !!!"
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
End Class
