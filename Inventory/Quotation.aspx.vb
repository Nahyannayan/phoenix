﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic

Partial Class Inventory_Quotation
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnUpload)
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "PI01090" And ViewState("MainMnu_code") <> "PI01090") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
            Else
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            End If
            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdQUD.DataSource = QUDFooter
            grdQUD.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            'grdQUD.DataSource = QUDFooter
            'grdQUD.DataBind()
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        imgClient.Visible = (ViewState("datamode") = "add")
        btnUpload.Visible = Not imgClient.Visible
        UploadFile.Visible = Not imgClient.Visible
        btnExport.Visible = Not imgClient.Visible
        txtContact.Enabled = EditAllowed
        'txtQUONo.Enabled = EditAllowed AndAlso (ViewState("datamode") = "add")
        txtQUODate.Enabled = EditAllowed AndAlso (ViewState("datamode") = "add")
        txtFDate.Enabled = EditAllowed AndAlso (ViewState("datamode") = "add")
        txtTDate.Enabled = EditAllowed AndAlso (ViewState("datamode") = "add")
        txtSupplier.Enabled = EditAllowed AndAlso (ViewState("datamode") = "add")
        txtTerms.Enabled = EditAllowed
        txtRefNo.Enabled = EditAllowed
        txtNotes.Enabled = EditAllowed
        txtDescr.Enabled = EditAllowed
        txtContact.Enabled = EditAllowed

        grdQUD.Columns(8).Visible = Not mDisable
        grdQUD.Columns(9).Visible = Not mDisable
        grdQUD.ShowFooter = Not mDisable

        'grdQUD.Columns(8).Visible = False
        'grdQUD.Columns(9).Visible = False
        'grdQUD.ShowFooter = False

        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable 'Or ViewState("MainMnu_code") = "U000086"
        btnEdit.Visible = mDisable
        btnPrint.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                dt = MainObj.getRecords("select replace(convert(varchar(30), Quo_Date, 106),' ','/') Quo_Date_Str, replace(convert(varchar(30), Quo_FDate, 106),' ','/') FDate_Str, replace(convert(varchar(30), Quo_TDate, 106),' ','/') TDate_Str, QUOTE_H.*, ACT_NAME  from QUOTE_H inner JOIN OASISFIN.dbo.accounts_M on QUO_SUP_ID=ACT_ID where QUO_ID=" & p_Modifyid, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    txtContact.Text = dt.Rows(0)("Quo_Contact")
                    txtQUODate.Text = dt.Rows(0)("Quo_Date_Str")
                    txtFDate.Text = dt.Rows(0)("FDate_Str")
                    txtTDate.Text = dt.Rows(0)("TDate_Str")
                    txtQUONo.Text = dt.Rows(0)("Quo_No")
                    txtSupplier.Text = dt.Rows(0)("Act_Name")
                    txtTerms.Text = dt.Rows(0)("Quo_Terms")
                    h_supid.Value = dt.Rows(0)("Quo_SUP_ID")
                    txtRefNo.Text = dt.Rows(0)("Quo_Ref_no")
                    txtNotes.Text = dt.Rows(0)("Quo_Notes")
                    txtDescr.Text = dt.Rows(0)("Quo_Descr")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If

            FormatFigures()
            fillGridView(QUDFooter, grdQUD, "select QUD_ID, QUD_ITM_ID, ITM_DESCR, UCO_DESCR, ITM_UNIT, ITM_PACKING, QUD_RATE, ITM_DETAILS from QUOTE_D inner JOIN ITEM on ITM_ID=QUD_ITM_ID left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE where QUD_QUO_ID=" & h_EntryId.Value & " order by QUD_ID")
            'If p_Modifyid > 0 Then txtTotalA.Text = TravelPlanFooter.Compute("sum(Trv_TotalExp)", "")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next
        'txtVisaCost.Text = Convert.ToDouble(txtVisaCost.Text).ToString("####0.00")
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        h_delete.Value = "0"
        h_supid.Value = ""

        txtContact.Text = ""
        txtQUODate.Text = Now.ToString("dd/MMM/yyyy")
        txtFDate.Text = Now.ToString("dd/MMM/yyyy")
        txtTDate.Text = Now.ToString("dd/MMM/yyyy")
        txtNotes.Text = ""
        txtSupplier.Text = ""
        txtTerms.Text = ""
        txtQUONo.Text = ""
        txtRefNo.Text = ""
        txtDescr.Text = ""
        txtContact.Text = ""
        'ddlEntitlement
        'txtEntitlement.Style.Add("display", "none")
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdQUD.DataSource = QUDFooter
        grdQUD.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdQUD.DataSource = QUDFooter
        grdQUD.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property QUDFooter() As DataTable
        Get
            Return ViewState("QUDFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("QUDFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
        Session("myData") = fillDataTable
        SetExportFileLink()
    End Sub

    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Framework - " & txtSupplier.Text)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub showNoRecordsFound()
        If Not QUDFooter Is Nothing AndAlso QUDFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdQUD.Columns.Count - 2
            grdQUD.Rows(0).Cells.Clear()
            grdQUD.Rows(0).Cells.Add(New TableCell())
            grdQUD.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdQUD.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdQUD.PageIndexChanging
        grdQUD.PageIndex = e.NewPageIndex
        grdQUD.DataSource = Session("myData")
        grdQUD.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdQUD.RowCancelingEdit
        grdQUD.ShowFooter = True
        grdQUD.EditIndex = -1
        grdQUD.DataSource = QUDFooter
        grdQUD.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdQUD_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdQUD.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtQUD_DESCR As TextBox = grdQUD.FooterRow.FindControl("txtQUD_DESCR")
            Dim txtQUD_BRAND As TextBox = grdQUD.FooterRow.FindControl("txtQUD_BRAND")
            Dim txtQUD_UNITS As TextBox = grdQUD.FooterRow.FindControl("txtQUD_UNITS")
            Dim txtQUD_PACKING As TextBox = grdQUD.FooterRow.FindControl("txtQUD_PACKING")
            Dim txtQUD_DETAILS As TextBox = grdQUD.FooterRow.FindControl("txtQUD_DETAILS")
            Dim txtQUD_RATE As TextBox = grdQUD.FooterRow.FindControl("txtQUD_RATE")
            Dim hdnITM_ID As HiddenField = grdQUD.FooterRow.FindControl("hdnITM_ID")

            lblError.Text = ""
            If txtQUD_DESCR.Text.Trim.Length = 0 Then lblError.Text &= "Item"
            If Val(txtQUD_PACKING.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Packing"
            If Val(txtQUD_RATE.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Rate"

            lblError.Text = ""
            'lblError.Text = validBO(txtSec_Travel.Text.Trim, txtBus_objectives.Text.Trim, txtSec_company.Text.Trim)
            If lblError.Text.Length > 0 Then
                lblError.Text &= " Mandatory!!!!"
                showNoRecordsFound()
                Exit Sub
            End If
            'QUD_ID, QUD_ITM_ID, ITM_DESCR, UCO_DESCR, ITM_UNIT, ITM_PACKING, QUD_RATE, ITM_DETAILS
            If QUDFooter.Rows(0)(1) = -1 Then QUDFooter.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = QUDFooter.NewRow
            mrow("QUD_ID") = 0
            mrow("QUD_ITM_ID") = hdnITM_ID.Value
            mrow("ITM_DESCR") = txtQUD_DESCR.Text
            mrow("UCO_DESCR") = txtQUD_BRAND.Text
            mrow("ITM_UNIT") = txtQUD_UNITS.Text
            mrow("ITM_PACKING") = txtQUD_PACKING.Text
            mrow("QUD_RATE") = txtQUD_RATE.Text
            mrow("ITM_DETAILS") = txtQUD_DETAILS.Text
            QUDFooter.Rows.Add(mrow)

            grdQUD.EditIndex = -1
            grdQUD.DataSource = QUDFooter
            grdQUD.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdQUD_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdQUD.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            
        End If
        
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgItem As ImageButton = e.Row.FindControl("imgItem")

            Dim txtQUD_DESCR As TextBox = e.Row.FindControl("txtQUD_DESCR")
            Dim txtQUD_BRAND As TextBox = e.Row.FindControl("txtQUD_BRAND")
            Dim txtQUD_UNITS As TextBox = e.Row.FindControl("txtQUD_UNITS")
            Dim txtQUD_PACKING As TextBox = e.Row.FindControl("txtQUD_PACKING")
            Dim txtQUD_DETAILS As TextBox = e.Row.FindControl("txtQUD_DETAILS")
            Dim txtQUD_RATE As TextBox = e.Row.FindControl("txtQUD_RATE")
            Dim hdnITM_ID As HiddenField = e.Row.FindControl("hdnITM_ID")
            imgItem.Attributes.Add("OnClick", "javascript:getItemAssignNew('" & txtQUD_DESCR.ClientID & "','" & txtQUD_BRAND.ClientID & "','" & txtQUD_UNITS.ClientID & "','" & txtQUD_PACKING.ClientID & "','" & txtQUD_DETAILS.ClientID & "','" & txtQUD_RATE.ClientID & "','" & hdnITM_ID.ClientID & "');return false")
        End If

    End Sub

    Protected Sub grdQUD_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdQUD.RowDeleting
        Dim mRow() As DataRow = QUDFooter.Select("ID=" & grdQUD.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_QUDGridDelete.Value &= ";" & mRow(0)("QUD_ID")
            QUDFooter.Select("ID=" & grdQUD.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            QUDFooter.AcceptChanges()
        End If
        If QUDFooter.Rows.Count = 0 Then
            QUDFooter.Rows.Add(QUDFooter.NewRow())
            QUDFooter.Rows(0)(1) = -1
        End If
        grdQUD.DataSource = QUDFooter
        grdQUD.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdQUD_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdQUD.RowEditing
        Try
            grdQUD.ShowFooter = True
            grdQUD.EditIndex = e.NewEditIndex
            grdQUD.DataSource = QUDFooter
            grdQUD.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdQUD_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdQUD.RowUpdating
        Dim s As String = grdQUD.DataKeys(e.RowIndex).Value.ToString()
        Dim lblQUD_ID As Label = grdQUD.Rows(e.RowIndex).FindControl("lblQUD_ID")
        Dim txtQUD_DESCR As TextBox = grdQUD.Rows(e.RowIndex).FindControl("txtQUD_DESCR")
        Dim txtQUD_BRAND As TextBox = grdQUD.Rows(e.RowIndex).FindControl("txtQUD_BRAND")
        Dim txtQUD_UNITS As TextBox = grdQUD.Rows(e.RowIndex).FindControl("txtQUD_UNITS")
        Dim txtQUD_PACKING As TextBox = grdQUD.Rows(e.RowIndex).FindControl("txtQUD_PACKING")
        Dim txtQUD_DETAILS As TextBox = grdQUD.Rows(e.RowIndex).FindControl("txtQUD_DETAILS")
        Dim txtQUD_RATE As TextBox = grdQUD.Rows(e.RowIndex).FindControl("txtQUD_RATE")
        Dim hdnITM_ID As HiddenField = grdQUD.Rows(e.RowIndex).FindControl("hdnITM_ID")

        'QUD_ID, QUD_ITM_ID, ITM_DESCR, UCO_DESCR, ITM_UNIT, ITM_PACKING, QUD_RATE, ITM_DETAILS
        Dim mrow As DataRow
        mrow = QUDFooter.Select("ID=" & s)(0)
        mrow("ITM_DESCR") = txtQUD_DESCR.Text
        mrow("UCO_DESCR") = txtQUD_BRAND.Text
        mrow("ITM_UNIT") = txtQUD_BRAND.Text
        mrow("ITM_PACKING") = txtQUD_BRAND.Text
        mrow("QUD_RATE") = txtQUD_RATE.Text
        mrow("ITM_DETAILS") = txtQUD_DETAILS.Text
        mrow("QUD_ITM_ID") = hdnITM_ID.Value

        grdQUD.EditIndex = -1
        grdQUD.DataSource = QUDFooter
        grdQUD.DataBind()
        showNoRecordsFound()
    End Sub

    <System.Web.Services.WebMethod()> _
Public Shared Function GetItmDescr(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim StrSQL As String
        If contextKey Is Nothing Then contextKey = ""
        StrSQL = "select top 10 ITM_ID, DESCRIPTION, COMMODITY, UNIT, PACKING, DETAILS, RATE from ("
        StrSQL &= "select ITM_ID, ITM_DESCR DESCRIPTION, isnull(UCO_DESCR,'') COMMODITY, ITM_UNIT UNIT, ITM_PACKING PACKING, ITM_DETAILS DETAILS, ITM_RATE RATE "
        StrSQL &= "FROM ITEM left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
        StrSQL &= ") a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("ITM_ID").ToString & "|" & ds.Tables(0).Rows(i2)("RATE").ToString & "|" & ds.Tables(0).Rows(i2)("DETAILS").ToString & "|" & ds.Tables(0).Rows(i2)("PACKING").ToString & "|" & ds.Tables(0).Rows(i2)("UNIT").ToString & "|" & ds.Tables(0).Rows(i2)("COMMODITY").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
            'items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblError.Text = ""
        If txtFDate.Text.Trim.Length = 0 Then lblError.Text &= "From Date,"
        If txtQUODate.Text.Trim.Length = 0 Then lblError.Text &= "Quotation Date,"
        If txtTerms.Text.Trim.Length = 0 Then lblError.Text &= "Payment Terms,"
        If txtRefNo.Text.Trim.Length = 0 Then lblError.Text &= "Framework Ref.No.,"
        If txtTDate.Text.Trim.Length = 0 Then lblError.Text &= "To Date,"
        If h_supid.Value.Trim.Length = 0 Then lblError.Text &= "Supplier,"
        If lblError.Text.Length > 0 Then lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Mandatory"
        If grdQUD.Rows.Count = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Quotation should contain atleast 1 item"
        'If IsDate(txtBTADate.Text) Then
        '    If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM oasisfin.dbo.vw_OSO_FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "' and '" & txtBTADate.Text & "' between FYR_FROMDT and isnull(FYR_TODT,getdate())") = 0 Then lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        'Else
        '    lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        'End If

        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(12) As SqlParameter
        'QUO_ID,QUO_NO,QUO_REF_NO,QUO_SUP_ID,QUO_TERMS,QUO_CONTACT,QUO_DESCR,QUO_DATE,QUO_FDATE,QUO_TDATE,QUO_NOTES
        pParms(1) = Mainclass.CreateSqlParameter("@QUO_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@QUO_NO", txtQUONo.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@QUO_REF_NO", txtRefNo.Text, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@QUO_SUP_ID", h_supid.Value, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@QUO_TERMS", txtTerms.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@QUO_CONTACT", txtContact.Text, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@QUO_DESCR", txtDescr.text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@QUO_DATE", txtQUODate.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@QUO_FDATE", txtFDate.Text, SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@QUO_TDATE", txtTDate.Text, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@QUO_NOTES", txtNotes.Text, SqlDbType.VarChar)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveQUOTE_H", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim RowCount As Integer
            For RowCount = 0 To QUDFooter.Rows.Count - 1
                Dim iParms(7) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, QUDFooter.Rows(RowCount)("QUD_ID"))
                If QUDFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                'QUD_ID, QUD_QUO_ID, QUD_ITM_ID, QUD_RATE, QUD_DESCR
                iParms(1) = Mainclass.CreateSqlParameter("@QUD_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@QUD_QUO_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@QUD_ITM_ID", QUDFooter.Rows(RowCount)("QUD_ITM_ID"), SqlDbType.Int)
                iParms(4) = Mainclass.CreateSqlParameter("@QUD_RATE", QUDFooter.Rows(RowCount)("QUD_RATE"), SqlDbType.Decimal)
                iParms(5) = Mainclass.CreateSqlParameter("@QUD_DESCR", "", SqlDbType.VarChar) 'QUDFooter.Rows(RowCount)("QUD_DESCR")
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveQUOTE_D", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            If h_QUDGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_QUDGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@QUD_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@QUD_QUO_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from QUOTE_D where QUD_ID=@QUD_ID and QUD_QUO_ID=@QUD_QUO_ID", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            stTrans.Commit()
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
            'setModifyvalues(ViewState("EntryId"))
            Response.Redirect(ViewState("ReferrerUrl"), False)
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadFile.HasFile Then
            Dim str_tempfilename As String = UploadFile.FileName 'Session("sUsr_id") & Session.SessionID & UploadFile.FileName
            Dim strFilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & str_tempfilename
            Dim FileError As Boolean = False
            Dim recCount As Integer = 0
            UploadFile.PostedFile.SaveAs(strFilepath)
            Dim strStep As Integer = 0
            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                If IO.File.Exists(strFilepath) Then
                    Dim filename As String = Path.GetFileName(strFilepath)
                    Dim ext As String = Path.GetExtension(strFilepath)
                    lblError.Text = ""
                    If ext = ".xls" Then
                        Try
                            Dim xltable As DataTable
                            xltable = Mainclass.FetchFromExcel("select * from [TableName]", strFilepath)
                            If xltable.Rows.Count.Equals(0) Then
                                lblError.Text = "Could not process the request.Invalid data in excel..!"
                            End If
                            strStep += 1
                            File.Delete(strFilepath)
                            strStep += 1
                            Dim objConn As New SqlConnection(connectionString)
                            objConn.Open()
                            strStep += 1
                            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
                            Dim sqlQuote As String, sqlItem As String
                            For Each dr As DataRow In xltable.Rows
                                strStep += 1
                                'sb.Append(("<AMOUNT>" & dr("Amount").ToString() & "</AMOUNT>"))
                                Dim idStr As String = dr("ID").ToString()
                                If dr("ID").ToString() = "" Then 'Not in framework
                                    If dr("QUD_ITM_ID").ToString() = "" Then 'Not in item
                                        sqlItem = "insert into item (ITM_DESCR, ITM_DETAILS, ITM_UNIT, ITM_PACKING, ITM_RATE, ITM_UNSPSC) SELECT "
                                        sqlItem &= "'" & dr("ITM_DESCR") & "', '" & dr("ITM_DETAILS") & "', '" & dr("ITM_UNIT") & "',"
                                        sqlItem &= "'" & dr("ITM_PACKING") & "', '" & dr("QUD_RATE") & "', "
                                        sqlItem &= "(select UCO_CODE FROM UNSPSC_COMMODITY where UCO_DESCR='" & dr("UCO_DESCR") & "') where '" & dr("ITM_DESCR") & "' not in (select ITM_DESCR from ITEM) "
                                        sqlQuote = "insert into quote_d (QUD_QUO_ID, QUD_ITM_ID, QUD_RATE, QUD_DESCR) select "
                                        sqlQuote &= h_EntryId.Value & ",ITM_ID, " & dr("QUD_RATE") & ",ITM_DESCR from item where ITM_DESCR='" & dr("ITM_DESCR") & "'"
                                        Dim retVal = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlItem & sqlQuote)
                                    End If
                                Else
                                    If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID AND QUD_ID=" & dr("QUD_ID") & " AND QUO_ID=" & h_EntryId.Value) = 0 Then
                                        lblError.Text = "Excel File does not belong to " & txtSupplier.Text & ", see line " & dr("QUD_ID")
                                        Exit For
                                    End If
                                    'ITM_DESCR, ITM_DETAILS, ITM_UNIT, ITM_PACKING, ITM_RATE, ITM_UNSPSC
                                    sqlQuote = "update quote_d set qud_rate=" & dr("QUD_RATE") & " where qud_id=" & dr("QUD_ID") & " and qud_itm_id=" & dr("QUD_ITM_ID")
                                    sqlItem = " update item set ITM_RATE=" & dr("QUD_RATE") & ", ITM_DESCR='" & dr("ITM_DESCR") & "', ITM_DETAILS='" & dr("ITM_DETAILS") & "', ITM_PACKING='" & dr("ITM_PACKING") & "', ITM_UNIT='" & dr("ITM_UNIT") & "' where itm_id=" & dr("QUD_ITM_ID")
                                    Dim retVal = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlQuote & sqlItem)
                                    If retVal = 0 Then
                                        lblError.Text = "Error on line " & dr("ID")
                                        Exit For
                                    End If
                                End If
                                recCount += 1
                            Next
                            strStep += 1000
                            If lblError.Text = "" Then
                                stTrans.Commit()
                                fillGridView(QUDFooter, grdQUD, "select QUD_ID, QUD_ITM_ID, ITM_DESCR, UCO_DESCR, ITM_UNIT, ITM_PACKING, QUD_RATE, ITM_DETAILS from QUOTE_D inner JOIN ITEM on ITM_ID=QUD_ITM_ID left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE where QUD_QUO_ID=" & h_EntryId.Value & " order by QUD_ID")
                                lblError.Text = "Successfully uploaded:" & recCount & " records"
                            Else
                                stTrans.Rollback()
                            End If

                        Catch ex As Exception
                            lblError.Text = "Error:" & ex.Message & ":" & recCount & ":" & strStep
                            Exit Sub
                        End Try
                    End If
                Else
                    lblError.Text = "upload only Excel File"
                End If
            End If
            If FileError And lblError.Text.Length = 0 Then lblError.Text = "Problems encountered while uploading file, please try again"
        Else
            lblError.Text = "File not selected !!!"
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update QUOTE_H set QUO_DELETED=1 where QUO_ID=@QUO_ID"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@QUO_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
