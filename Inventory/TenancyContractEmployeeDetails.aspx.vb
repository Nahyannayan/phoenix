﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_TenancyContractEmployeeDetails
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass(), requestForQuotation As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Request.QueryString("datamode")
                End If

                ViewState("datamode") = "add"
                If Request.QueryString("EntryId") <> "" And Request.QueryString("EntryId") <> "" Then
                    setModifyvalues(Request.QueryString("EntryId"))
                End If
                showNoRecordsFound()
                hGridRefresh.Value = 0

            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If hGridRefresh.Value = 1 Then
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdEmpDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdEmpDetails.RowCancelingEdit
        grdEmpDetails.ShowFooter = True
        grdEmpDetails.EditIndex = -1
        grdEmpDetails.DataSource = EmployeeDetails
        grdEmpDetails.DataBind()

    End Sub
    Protected Sub grdEmpDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdEmpDetails.RowCommand
        Try
            If e.CommandName = "AddNew" Then
                Dim txtEmployee As TextBox = grdEmpDetails.FooterRow.FindControl("txtEmployee")
                Dim txtOTHERS As TextBox = grdEmpDetails.FooterRow.FindControl("txtOTHERS")
                Dim h_Emp_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Emp_Id")
                Dim h_Des_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Des_Id")
                Dim h_BSU_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_BSU_Id")
                Dim h_UnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_UnitType")
                Dim h_EUnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_EUnitType")
                Dim txtLeaseAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtLeaseAmount")
                Dim txtOldRent As TextBox = grdEmpDetails.FooterRow.FindControl("txtOldRent")
                Dim txtEAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtEAmount")
                Dim txtDeduction As TextBox = grdEmpDetails.FooterRow.FindControl("txtDeduction")
                Dim ddlEUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlEUnitType")
                Dim txtEUnitType As TextBox = grdEmpDetails.FooterRow.FindControl("txtEUnitType")
                Dim ddlUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlUnitType")
                Dim ddlBSU As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlBSU")
                Dim ddlfurn As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlFurnish")
                Dim ddlMStatus As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlMStatus")
                Dim txtDESIGNATION As TextBox = grdEmpDetails.FooterRow.FindControl("txtDESIGNATION")
                Dim txtMSTATUS As TextBox = grdEmpDetails.FooterRow.FindControl("txtStatus")
                Dim txtDEPENDANTS As TextBox = grdEmpDetails.FooterRow.FindControl("txtDEPENDANTS")
                Dim lblMSTATUS As Label = grdEmpDetails.FooterRow.FindControl("lblMSTATUS")
                Dim lblDEPENDANTS As Label = grdEmpDetails.FooterRow.FindControl("lblDEPENDANTS")
                Dim txtUnitNo As TextBox = grdEmpDetails.FooterRow.FindControl("txtUnitNo")
                If IsNumeric(txtLeaseAmount.Text) = 0 Then lblError.Text = "enter Amount in correct format,"
                If Val(txtLeaseAmount.Text) = 0 Then lblError.Text = "enter Lease Amount,"
                If Len(h_dis_EmpName.Value) <= 0 Then lblError.Text = "select at least one employee,"

                If lblError.Text.Length > 0 Then
                    showNoRecordsFound()
                    Exit Sub
                End If
                If EmployeeDetails.Rows(0)(1) = -1 Then EmployeeDetails.Rows.RemoveAt(0)
                Dim mrow As DataRow
                mrow = EmployeeDetails.NewRow
                mrow("TCE_ID") = 0
                mrow("TCE_DEPENDANTS") = h_dis_dep.Value
                mrow("TCE_OLDRENT") = txtOldRent.Text
                'mrow("TCE_EAMOUNT") = txtEAmount.Text
                'mrow("TCE_DAMOUNT") = IIf(txtDeduction.Text = "", 0, txtDeduction.Text)
                mrow("TCE_EAMOUNT") = h_dis_Entitlement.Value
                mrow("TCE_DAMOUNT") = hf_h_DednAmt.Value
                mrow("TCE_LAMOUNT") = hf_h_LeasAmount.Value
                mrow("TCE_BSU_ID") = ddlBSU.SelectedValue
                mrow("TCE_REU_ID") = ddlUnitType.SelectedValue
                'ddlEUnitType.Items.FindByText(h_dis_EUnittype.Value).Selected = True
                'ddlEUnitType.Items.FindByText(h_dis_EUnittype.Value.Trim).Selected = True
                ddlEUnitType.SelectedIndex = ddlEUnitType.Items.IndexOf(ddlEUnitType.Items.FindByText(h_dis_EUnittype.Value))
                mrow("TCE_EREU_ID") = ddlEUnitType.SelectedValue
                mrow("EUNITTYPE") = ddlEUnitType.SelectedItem.Text
                mrow("TCE_FURN_ID") = ddlfurn.SelectedValue
                mrow("BSUSHORT") = ddlBSU.SelectedItem.Text
                mrow("UNITTYPE") = ddlUnitType.SelectedItem.Text

                mrow("FURN_DESCR") = ddlfurn.SelectedItem.Text
                mrow("TCE_EMP_ID") = h_Emp_Id.Value
                mrow("TCE_TCH_ID") = Request.QueryString("EntryId")
                mrow("TCE_DELETED") = 0
                'mrow("EMPNAME") = txtEmployee.Text
                mrow("EMPNAME") = h_dis_EmpName.Value
                'mrow("TCE_MSTATUS") = txtMSTATUS.Text
                mrow("TCE_MSTATUS") = h_dis_status.Value

                mrow("TCE_DES_ID") = 0
                mrow("DESIGNATION") = h_dis_designation.Value
                mrow("TCE_UNITNO") = txtUnitNo.Text

                EmployeeDetails.Rows.Add(mrow)
                grdEmpDetails.EditIndex = -1
                grdEmpDetails.DataSource = EmployeeDetails
                grdEmpDetails.DataBind()
                showNoRecordsFound()
            Else
                Dim txtEmployee As TextBox = grdEmpDetails.FooterRow.FindControl("txtEmployee")
                Dim txtOTHERS As TextBox = grdEmpDetails.FooterRow.FindControl("txtOTHERS")
                Dim h_Emp_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Emp_Id")
                Dim h_Des_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Des_Id")
                Dim h_BSU_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_BSU_Id")
                Dim h_UnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_UnitType")
                Dim h_EUnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_EUnitType")
                Dim txtLeaseAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtLeaseAmount")
                Dim txtOldRent As TextBox = grdEmpDetails.FooterRow.FindControl("txtOldRent")
                Dim txtEAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtEAmount")
                Dim txtDeduction As TextBox = grdEmpDetails.FooterRow.FindControl("txtDeduction")
                Dim ddlEUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlEUnitType")
                Dim ddlUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlUnitType")
                Dim ddlBSU As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlBSU")
                Dim ddlfurn As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlFurnish")
                Dim txtDESIGNATION As TextBox = grdEmpDetails.FooterRow.FindControl("txtDESIGNATION")
                Dim txtMSTATUS As TextBox = grdEmpDetails.FooterRow.FindControl("txtMSTATUS")
                Dim txtDEPENDANTS As TextBox = grdEmpDetails.FooterRow.FindControl("txtDEPENDANTS")


                'ddlBSU.Items.FindByValue(h_BSU_Id.Value).Selected = True

            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub grdEmpDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEmpDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
                If e.Row.RowType = DataControlRowType.Footer Then
                    Dim txtLeaseAmt As TextBox = e.Row.FindControl("txtLeaseAmount")
                    Dim txtEAmount As TextBox = e.Row.FindControl("txtEAmount")
                    Dim txtDeduction As TextBox = e.Row.FindControl("txtDeduction")
                    Dim txtEUnitType As TextBox = e.Row.FindControl("txtEUnitType")
                    Dim txtEmployee As TextBox = e.Row.FindControl("txtEmployee")
                    Dim txtDESIGNATION As TextBox = e.Row.FindControl("txtDESIGNATION")
                    Dim txtMSTATUS As TextBox = e.Row.FindControl("txtMSTATUS")
                    Dim txtDEPENDANTS As TextBox = e.Row.FindControl("txtDEPENDANTS")
                    Dim lblDESIGNATION As Label = e.Row.FindControl("lblDESIGNATION")
                    Dim lblMSTATUS As Label = e.Row.FindControl("lblMSTATUS")
                    Dim txtLeaseAmount As TextBox = e.Row.FindControl("txtLeaseAmount")
                    Dim txtOldRent As TextBox = e.Row.FindControl("txtOldRent")
                    Dim h_Emp_Id As HiddenField = e.Row.FindControl("h_Emp_Id")
                    Dim h_Des_Id As HiddenField = e.Row.FindControl("h_Des_Id")
                    Dim imgEmployee As ImageButton = e.Row.FindControl("imgEmployee")
                    Dim imgDesignation As ImageButton = e.Row.FindControl("imgDesignation")
                    Dim ddlBSU As DropDownList = e.Row.FindControl("ddlBSU")
                    'Dim ddlMStatus As DropDownList = e.Row.FindControl("ddlMStatus")
                    Dim txtStatus As TextBox = e.Row.FindControl("txtStatus")
                    ddlBSU.SelectedValue = Session("sBSUID")
                    h_bsu_id.Value = ddlBSU.SelectedValue
                    txtLeaseAmount.Text = "0.0"
                    txtDeduction.Text = "0.0"
                    txtEAmount.Text = "0.0"
                    txtDEPENDANTS.Text = "0"
                    txtOldRent.Text = "0"
                    h_bsu_id.Value = ddlBSU.SelectedValue
                    imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & txtDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & ddlBSU.ClientID & "','" & txtStatus.ClientID & "','" & txtDEPENDANTS.ClientID & "','" & txtEUnitType.ClientID & "','" & txtEAmount.ClientID & "'); return false;")
                    txtLeaseAmt.Attributes.Add("OnChange", "javascript:return calcdeductions('" & txtEAmount.ClientID & "','" & txtDeduction.ClientID & "','" & txtLeaseAmt.ClientID & "',this)")
                    txtEAmount.Attributes.Add("OnChange", "javascript:return calcdeductions('" & txtEAmount.ClientID & "','" & txtDeduction.ClientID & "','" & txtLeaseAmt.ClientID & "',this)")
                    'imgDesignation.Attributes.Add("OnClick", "javascript: getDesignation('" & txtDESIGNATION.ClientID & "','" & h_Des_Id.ClientID & "'); return false;")
                End If
                If grdEmpDetails.ShowFooter Or grdEmpDetails.EditIndex > -1 Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
                    Dim ddlUnitType As DropDownList = e.Row.FindControl("ddlUnitType")
                    Dim ddlEUnitType As DropDownList = e.Row.FindControl("ddlEUnitType")
                    Dim ddlBSU As DropDownList = e.Row.FindControl("ddlBSU")
                    'Dim ddlMStatus As DropDownList = e.Row.FindControl("ddlMStatus")
                    Dim ddlfurn As DropDownList = e.Row.FindControl("ddlFurnish")
                    Dim imgEmployee As ImageButton = e.Row.FindControl("imgEmployee")
                    Dim imgDesignation As ImageButton = e.Row.FindControl("imgDesignation")
                    Dim txtEmployee As TextBox = e.Row.FindControl("txtEmployee")
                    Dim h_Emp_Id As HiddenField = e.Row.FindControl("h_Emp_Id")
                    Dim h_Des_Id As HiddenField = e.Row.FindControl("h_Des_Id")
                    Dim txtDESIGNATION As TextBox = e.Row.FindControl("txtDESIGNATION")
                    Dim txtMSTATUS As TextBox = e.Row.FindControl("txtMSTATUS")
                    Dim txtDEPENDANTS As TextBox = e.Row.FindControl("txtDEPENDANTS")

                    Dim lblDESIGNATION As Label = e.Row.FindControl("lblDESIGNATION")
                    Dim lblMSTATUS As Label = e.Row.FindControl("lblMSTATUS")
                    Dim lblDEPENDANTS As Label = e.Row.FindControl("lblDEPENDANTS")

                    Dim txtLeaseAmount As TextBox = e.Row.FindControl("txtLeaseAmount")
                    Dim txtEntileAmount As TextBox = e.Row.FindControl("txtEAmount")
                    Dim txtEUnitType As TextBox = e.Row.FindControl("txtEUnitType")

                    Dim txtDeductionAmount As TextBox = e.Row.FindControl("txtDeduction")

                    Dim txtStatus As TextBox = e.Row.FindControl("txtStatus")

                    txtLeaseAmount.Attributes.Add("OnChange", "javascript:return calcdeductions('" & txtEntileAmount.ClientID & "','" & txtDeductionAmount.ClientID & "','" & txtLeaseAmount.ClientID & "',this)")
                    txtEntileAmount.Attributes.Add("OnChange", "javascript:return calcdeductions('" & txtEntileAmount.ClientID & "','" & txtDeductionAmount.ClientID & "','" & txtLeaseAmount.ClientID & "',this)")

                    If Not ddlUnitType Is Nothing And Not ddlEUnitType Is Nothing And Not ddlfurn Is Nothing Then
                        'And Not ddlMStatus Is Nothing 
                        Dim sqlStr As String = ""
                        Dim sqlStr1 As String = ""
                        Dim sqlStr2 As String = ""
                        Dim sqlStr3 As String = ""
                        Dim SuperUser As Integer = 0
                        sqlStr &= "select REU_ID,REU_NAME  from REUNITTYPE order by reu_name "
                        SuperUser = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from vw_SuperUser where usr_name ='" & UCase(Session("sUsr_name")) & "'")
                        If SuperUser > 0 Then
                            sqlStr1 &= "SELECT BSU_SHORTNAME,BSU_ID from oasis..BUSINESSUNIT_M order by  BSU_SHORTNAME  "
                        Else
                            sqlStr1 &= "SELECT BSU_SHORTNAME,BSU_ID from oasis..BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' "
                        End If
                        sqlStr2 &= "SELECT FURN_DESCR,FURN_ID FROM VW_FURNISHINGMASTER "
                        sqlStr3 &= "select 'MARRIED' ID ,'MARRIED' DESCR UNION ALL select 'SEPARATED' ID ,'SEPARATED' DESCR UNION ALL select 'WIDOWED' ID ,'WIDOWED' DESCR  UNION ALL select 'DIVORCED' ID ,'DIVORCED' DESCR  UNION ALL select 'SINGLE' ID ,'SINGLE' DESCR "
                        fillDropdown(ddlUnitType, sqlStr, "REU_NAME", "REU_ID", False)
                        fillDropdown(ddlEUnitType, sqlStr, "REU_NAME", "REU_ID", False)
                        fillDropdown(ddlBSU, sqlStr1, "BSU_SHORTNAME", "BSU_ID", False)
                        fillDropdown(ddlfurn, sqlStr2, "FURN_DESCR", "FURN_ID", False)
                        'fillDropdown(ddlMStatus, sqlStr3, "DESCR", "ID", False)
                        ddlBSU.SelectedValue = ddlBSU.SelectedValue
                        h_bsu_id.Value = ddlBSU.SelectedValue
                        If txtDESIGNATION Is Nothing Then
                            'imgEmployee.Attributes.Add("OnClick", "javascript:return getEmployee('" & txtEmployee.ClientID & "','" & lblDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & ddlBSU.ClientID & "','" & ddlMStatus.SelectedValue & "','" & lblDEPENDANTS.ClientID & "','" & h_Des_Id.ClientID & "')")

                            ' commnted on 14-Jul_2019 Mahesh
                            'imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & lblDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & ddlBSU.ClientID & "','" & txtStatus.ClientID & "','" & lblDEPENDANTS.ClientID & "','" & txtEUnitType.ClientID & "','" & txtEntileAmount.ClientID & "'); return false;")
                            imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & lblDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & ddlBSU.ClientID & "','" & txtStatus.ClientID & "','" & lblDEPENDANTS.ClientID & "','" & txtEUnitType.ClientID & "','" & txtEntileAmount.ClientID & "','" & txtLeaseAmount.ClientID & "','" & txtDeductionAmount.ClientID & "'); return false;")




                        Else
                            'imgEmployee.Attributes.Add("OnClick", "javascript:return getEmployee('" & txtEmployee.ClientID & "','" & txtDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & ddlBSU.ClientID & "','" & ddlMStatus.SelectedValue & "','" & txtDEPENDANTS.ClientID & "','" & h_Des_Id.ClientID & "')")
                            ' commnted on 14-Jul_2019 Mahesh
                            'imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & txtDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & ddlBSU.ClientID & "','" & txtStatus.ClientID & "','" & txtDEPENDANTS.ClientID & "','" & txtEUnitType.ClientID & "','" & txtEntileAmount.ClientID & "'); return false;")
                            imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & txtDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & ddlBSU.ClientID & "','" & txtStatus.ClientID & "','" & txtDEPENDANTS.ClientID & "','" & txtEUnitType.ClientID & "','" & txtEntileAmount.ClientID & "','" & txtLeaseAmount.ClientID & "','" & txtDeductionAmount.ClientID & "'); return false;")
                        End If
                        imgDesignation.Attributes.Add("OnClick", "javascript: getDesignation('" & txtDESIGNATION.ClientID & "','" & h_Des_Id.ClientID & "'); return false;")
                    End If
                End If
            End If

            For Each gvr As GridViewRow In grdEmpDetails.Rows
                If UCase(Request.QueryString("datamode")) = "VIEW" Then
                    gvr.Cells(13).Enabled = False
                    gvr.Cells(14).Enabled = False
                Else
                    gvr.Cells(13).Enabled = True
                    gvr.Cells(14).Enabled = True
                End If
            Next
            If UCase(Request.QueryString("datamode")) = "VIEW" Then
                grdEmpDetails.ShowFooter = False
            Else
                grdEmpDetails.ShowFooter = True
            End If


        Catch ex As Exception

        End Try
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub
    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        'fillEmpDetailsGridView(grdEmpDetails, "SELECT TCE_ID, TCE_BSU_ID, TCE_EMP_ID,TCE_DELETED, TCE_REU_ID, TCE_LAMOUNT, TCE_EREU_ID, TCE_EAMOUNT, TCE_DAMOUNT, TCE_TCH_ID,case when TCE_EMP_ID=0 then BSU_NAME  else EMPNAME end EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TCE_MSTATUS,TCE_DES_ID,DES_DESCR DESIGNATION,TCE_DEPENDANTS,TCE_FURN_ID,FURN_DESCR,TCE_UNITNO,TCE_OLDRENT  FROM TCT_E LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TCE_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TCE_FURN_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCE_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TCE_DES_ID  where TCE_TCH_ID=" & p_Modifyid & " and TCE_DELETED=0 order by TCE_ID")
        'fillEmpDetailsGridView(grdEmpDetails, "SELECT TCE_ID, TCE_BSU_ID, TCE_EMP_ID,TCE_DELETED, TCE_REU_ID, TCE_LAMOUNT, TCE_EREU_ID, TCE_EAMOUNT, TCE_DAMOUNT, TCE_TCH_ID,case when TCE_EMP_ID=0 then 'TBC'  else case when TCE_EMP_ID=1 then 'Vacant' else  EMPNAME end end EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TCE_MSTATUS,TCE_DES_ID,DES_DESCR DESIGNATION,TCE_DEPENDANTS,TCE_FURN_ID,FURN_DESCR,TCE_UNITNO,TCE_OLDRENT  FROM TCT_E LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TCE_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TCE_FURN_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCE_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TCE_DES_ID  where TCE_TCH_ID=" & p_Modifyid & " and TCE_DELETED=0 order by TCE_ID")
        'commented and added by Mahesh on 15-07-2019
        fillEmpDetailsGridView(grdEmpDetails, "SELECT TCE_ID, TCE_BSU_ID, TCE_EMP_ID,TCE_DELETED, TCE_REU_ID, TCE_LAMOUNT, TCE_EREU_ID, TCE_EAMOUNT, TCE_DAMOUNT, TCE_TCH_ID,case when TCE_EMP_ID=0 then 'TBC'  else case when TCE_EMP_ID=1 then 'Vacant' else case when tce_emp_id=2 then bsu_name else EMPNAME end end END EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TCE_MSTATUS,TCE_DES_ID,DES_DESCR DESIGNATION,TCE_DEPENDANTS,TCE_FURN_ID,FURN_DESCR,TCE_UNITNO,TCE_OLDRENT  FROM TCT_E LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TCE_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TCE_FURN_ID LEFT OUTER JOIN TCT_H on TCH_ID=TCE_TCH_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCH_BSU_ID  LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TCE_DES_ID  where TCE_TCH_ID=" & p_Modifyid & " and TCE_DELETED=0 order by TCE_ID")


        If UCase(Request.QueryString("datamode")) = "VIEW" Then
            btnSave.Visible = False
        End If
    End Sub
    Private Sub fillEmpDetailsGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        If Session("Empdata") Is Nothing Then
            EmployeeDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
            Dim mtable As New DataTable
            Dim dcID As New DataColumn("ID", GetType(Integer))
            dcID.AutoIncrement = True
            dcID.AutoIncrementSeed = 1
            dcID.AutoIncrementStep = 1
            mtable.Columns.Add(dcID)
            mtable.Merge(EmployeeDetails)

            If mtable.Rows.Count = 0 Then
                mtable.Rows.Add(mtable.NewRow())
                mtable.Rows(0)(1) = -1
            End If
            EmployeeDetails = mtable
        Else
            EmployeeDetails = Session("Empdata")
        End If
        fillGrdView.DataSource = EmployeeDetails
        Session("EmpOlddata") = EmployeeDetails
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub

    Private Sub showNoRecordsFound()
        If Not EmployeeDetails Is Nothing Then
            If EmployeeDetails.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdEmpDetails.Columns.Count - 2
                grdEmpDetails.Rows(0).Cells.Clear()
                grdEmpDetails.Rows(0).Cells.Add(New TableCell())
                grdEmpDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdEmpDetails.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub

    Private Property EmployeeDetails() As DataTable
        Get
            Return ViewState("EmployeeDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EmployeeDetails") = value
        End Set
    End Property

    Protected Sub DropDownList1_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlCurrentDropDownList As DropDownList = DirectCast(sender, DropDownList)
        Dim grdrDropDownRow As GridViewRow = DirectCast(ddlCurrentDropDownList.Parent.Parent, GridViewRow)
        Dim ss As String = ddlCurrentDropDownList.SelectedValue
        h_bsu_id.Value = ddlCurrentDropDownList.SelectedValue
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim NotSameUnitType As Boolean = False
        Dim i As Integer = 0
        Dim MaxAllowed As Integer = 0
        Dim TotalStaffCount As Integer = 0

        If EmployeeDetails.Rows(0)(1) = -1 Then
            lblError.Text = "select atleast one employee"
            showNoRecordsFound()
            Exit Sub
        Else

            Dim RecCount As Integer, RecInsCount As Integer = 0
            Dim Unittype As String = ""

            If EmployeeDetails.Rows.Count >= 1 Then
                For RecCount = 0 To EmployeeDetails.Rows.Count - 1
                    If i = 0 Then
                        Unittype = EmployeeDetails.Rows(RecCount)("UNITTYPE")
                    Else
                        If Unittype = EmployeeDetails.Rows(RecCount)("UNITTYPE") Then
                            NotSameUnitType = False
                        Else
                            NotSameUnitType = True
                        End If
                    End If
                    i = i + 1
                Next
            End If


            If NotSameUnitType = True Then
                lblError.Text = "One TRF one Unit Type only allowed..."
                showNoRecordsFound()
                Exit Sub
            End If

            MaxAllowed = Mainclass.getDataValue("select REU_ALLOWED_PERSONS from REUNITTYPE where reu_name='" & Unittype & "'", "OASIS_PUR_INVConnectionString")
            TotalStaffCount = EmployeeDetails.Rows.Count

            If TotalStaffCount > MaxAllowed Then
                lblError.Text = "only " & MaxAllowed & "  person allowd  for " & Unittype & " Unit Type...!"

                Exit Sub
            End If



            Dim dtTest As DataTable = New DataTable()
            Session("Empdata") = EmployeeDetails
            Session("DeletedEmployees") = hGridDelete.Value
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "CloseWindow", "setCloseFrame();", True)

        End If
    End Sub

    Protected Sub grdEmpDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEmpDetails.RowEditing
        Try



            grdEmpDetails.ShowFooter = False
            grdEmpDetails.EditIndex = e.NewEditIndex
            grdEmpDetails.DataSource = EmployeeDetails
            grdEmpDetails.DataBind()



            Dim h_BSU_Id As HiddenField = grdEmpDetails.Rows(e.NewEditIndex).FindControl("h_BSU_Id")
            Dim ddlProduct As DropDownList = grdEmpDetails.Rows(e.NewEditIndex).FindControl("ddlBSU")
            ddlProduct.SelectedValue = h_BSU_Id.Value

            Dim h_Furn_id As HiddenField = grdEmpDetails.Rows(e.NewEditIndex).FindControl("h_Furn_id")
            Dim ddlFurnish As DropDownList = grdEmpDetails.Rows(e.NewEditIndex).FindControl("ddlFurnish")
            ddlFurnish.SelectedValue = h_Furn_id.Value

            Dim h_EUnitType As HiddenField = grdEmpDetails.Rows(e.NewEditIndex).FindControl("h_EUnitType")
            Dim ddlEUnitType As DropDownList = grdEmpDetails.Rows(e.NewEditIndex).FindControl("ddlEUnitType")
            ddlEUnitType.SelectedValue = h_EUnitType.Value


            Dim h_UnitType As HiddenField = grdEmpDetails.Rows(e.NewEditIndex).FindControl("h_UnitType")
            Dim ddlUnitType As DropDownList = grdEmpDetails.Rows(e.NewEditIndex).FindControl("ddlUnitType")
            ddlUnitType.SelectedValue = h_UnitType.Value

            Dim h_EntAmount As HiddenField = grdEmpDetails.Rows(e.NewEditIndex).FindControl("h_OLD_EntAmont")
            Dim txtDesignation As TextBox = grdEmpDetails.Rows(e.NewEditIndex).FindControl("txtDesignation")
            Dim txtEmployee As TextBox = grdEmpDetails.Rows(e.NewEditIndex).FindControl("txtEmployee")
            Dim txtStatus As TextBox = grdEmpDetails.Rows(e.NewEditIndex).FindControl("txtStatus")


            h_dis_Entitlement.Value = h_EntAmount.Value
            h_dis_designation.Value = txtDesignation.Text
            h_dis_EmpName.Value = txtEmployee.Text
            h_dis_status.Value = txtStatus.Text











            'Dim ddlBSU As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlBSU")
            'Dim h_BSU_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_BSU_Id")
            'Dim txtInv_Rate_F As TextBox = grdPayment.Rows(e.NewEditIndex).FindControl("txtChqDate")
            'Dim txtInv_Qty_F As TextBox = grdPayment.Rows(e.NewEditIndex).FindControl("txtAmount")
            'Dim h_Service_ID_F As HiddenField = grdPayment.Rows(e.NewEditIndex).FindControl("hdnITM_ID")
            'Dim h_ITEMTYPE As HiddenField = grdPayment.Rows(e.NewEditIndex).FindControl("hdnITM_TYPE")
            'Dim ddlProduct As DropDownList = grdPayment.Rows(e.NewEditIndex).FindControl("ddlPaymentType")
            'Dim ddlPayment As DropDownList = grdPayment.Rows(e.NewEditIndex).FindControl("ddlPType")
            'Dim txtPayable_F As TextBox = grdPayment.Rows(e.NewEditIndex).FindControl("txtPayable")
            'ddlProduct.Items.FindByValue(h_Service_ID_F.Value).Selected = True
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdEmpDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdEmpDetails.RowUpdating
        Dim s As String = grdEmpDetails.DataKeys(e.RowIndex).Value.ToString()
        Dim TCE_id As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblTCE_ID")
        Dim txtEmployee As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtEmployee")
        Dim txtOTHERS As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtOTHERS")
        Dim h_Emp_Id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_Emp_Id")
        Dim h_BSU_Id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_BSU_Id")
        Dim h_Des_Id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_Des_Id")
        Dim h_UnitType As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_UnitType")
        Dim h_EUnitType As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_EUnitType")
        Dim h_Furn_id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_Furn_id")
        Dim txtLeaseAmount As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtLeaseAmount")
        Dim txtOldRent As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtOldRent")

        Dim txtEAmount As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtEAmount")
        Dim txtDeduction As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtDeduction")
        Dim ddlEUnitType As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlEUnitType")
        Dim ddlUnitType As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlUnitType")
        Dim ddlBSU As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlBSU")
        Dim ddlMStatus As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlMStatus")
        Dim ddlfurn As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlFurnish")
        Dim txtDESIGNATION As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtDESIGNATION")
        Dim txtMSTATUS As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtMSTATUS")

        Dim txtEUnitType As TextBox = grdEmpDetails.FooterRow.FindControl("txtEUnitType")

        Dim txtDEPENDANTS As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtDEPENDANTS")
        Dim lblDEPENDANTS As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblDEPENDANTS")
        Dim lblDESIGNATION As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblDESIGNATION")
        Dim lblMSTATUS As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblMSTATUS")
        Dim txtUnitNo As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtUnitNo")


        If grdEmpDetails.ShowFooter Or grdEmpDetails.EditIndex > -1 And Not txtLeaseAmount Is Nothing Then
            If Not IsNumeric(txtLeaseAmount.Text) Then lblError.Text = "enter numbers only"
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            Dim mrow As DataRow
            mrow = EmployeeDetails.Select("ID=" & s)(0)
            mrow("TCE_LAMOUNT") = txtLeaseAmount.Text
            mrow("TCE_OLDRENT") = txtOldRent.Text
            'mrow("TCE_EAMOUNT") = hf_h_EntAmt.Value  ' txtEAmount.Text
            mrow("TCE_EAMOUNT") = h_dis_Entitlement.Value
            If h_Emp_Id.Value = "0" Then
                mrow("TCE_DAMOUNT") = 0
            Else
                mrow("TCE_DAMOUNT") = IIf(hf_h_DednAmt.Value = "", 0, hf_h_DednAmt.Value)

                'mrow("TCE_DAMOUNT") = IIf(txtDeduction.Text = "", 0, txtDeduction.Text)
            End If

            'mrow("TCE_BSU_ID") = ddlBSU.SelectedValue
            mrow("TCE_BSU_ID") = h_BSU_Id.Value
            mrow("TCE_REU_ID") = ddlUnitType.SelectedValue

            'Try
            '    ddlEUnitType.Items.FindByText(txtEUnitType.Text).Selected = True
            'Catch ex As Exception
            'End Try

            Try
                ddlEUnitType.SelectedIndex = ddlEUnitType.Items.IndexOf(ddlEUnitType.Items.FindByText(h_dis_EUnittype.Value))
            Catch ex As Exception

            End Try
            mrow("TCE_EREU_ID") = ddlEUnitType.SelectedValue
            mrow("TCE_FURN_ID") = ddlfurn.SelectedValue
            mrow("BSUSHORT") = ddlBSU.SelectedItem.Text
            mrow("UNITTYPE") = ddlUnitType.SelectedItem.Text
            mrow("EUNITTYPE") = ddlEUnitType.SelectedItem.Text
            mrow("FURN_DESCR") = ddlfurn.SelectedItem.Text
            mrow("TCE_EMP_ID") = h_Emp_Id.Value
            'mrow("EMPNAME") = txtEmployee.Text
            mrow("EMPNAME") = h_dis_EmpName.Value
            mrow("TCE_DES_ID") = h_Des_Id.Value
            'mrow("DESIGNATION") = txtDESIGNATION.Text
            mrow("DESIGNATION") = h_dis_designation.Value
            mrow("TCE_UNITNO") = txtUnitNo.Text

            mrow("TCE_ID") = TCE_id.Text
            If txtDEPENDANTS Is Nothing Then
                mrow("TCE_DEPENDANTS") = IIf(lblDEPENDANTS.Text = "", DBNull.Value, lblDEPENDANTS.Text)
                'mrow("TCE_MSTATUS") = lblMSTATUS.Text
                mrow("TCE_MSTATUS") = h_dis_status.Value
            Else
                mrow("TCE_DEPENDANTS") = IIf(txtDEPENDANTS.Text = "", DBNull.Value, txtDEPENDANTS.Text)
                If Not ddlMStatus Is Nothing Then
                    mrow("TCE_MSTATUS") = ddlMStatus.SelectedItem.Text.ToString
                Else
                    mrow("TCE_MSTATUS") = h_dis_status.Value
                End If
            End If
            grdEmpDetails.EditIndex = -1
            grdEmpDetails.ShowFooter = True
            grdEmpDetails.DataSource = EmployeeDetails
            grdEmpDetails.DataBind()
        End If
    End Sub

    Protected Sub grdEmpDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEmpDetails.RowDeleting
        Dim mRow() As DataRow = EmployeeDetails.Select("ID=" & grdEmpDetails.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= mRow(0)("TCE_ID") & ";"
            EmployeeDetails.Select("ID=" & grdEmpDetails.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            EmployeeDetails.AcceptChanges()

        End If
        If EmployeeDetails.Rows.Count = 0 Then
            EmployeeDetails.Rows.Add(EmployeeDetails.NewRow())
            EmployeeDetails.Rows(0)(1) = -1
        Else
        End If
        grdEmpDetails.DataSource = EmployeeDetails
        grdEmpDetails.DataBind()
        showNoRecordsFound()
    End Sub

    'Protected Sub hf_h_EntAmt_ValueChanged(sender As Object, e As EventArgs)
    '    grdEmpDetails
    'End Sub
End Class
