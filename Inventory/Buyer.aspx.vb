﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic

Partial Class Inventory_Buyer
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "PI01030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            pgTitle.Text = "Buyer Master"

            If Request.QueryString("viewid") <> "" Then
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                SetDataMode("view")
            Else
                ViewState("EntryId") = 0
                SetDataMode("add")
            End If
            setModifyvalues(ViewState("EntryId"))
            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdBYR.DataSource = BYRFooter
            grdBYR.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            'grdBYR.DataSource = BYRFooter
            'grdBYR.DataBind()
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        grdBYR.Columns(5).Visible = Not mDisable
        grdBYR.Columns(6).Visible = Not mDisable
        grdBYR.ShowFooter = Not mDisable

        txtBuyer.Enabled = EditAllowed
        txtBYR_Descr.Enabled = EditAllowed
        chkBYR_ACTIVE.Enabled = EditAllowed
        imgBuyer.Enabled = EditAllowed

        btnCopy.Visible = mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable 'Or ViewState("MainMnu_code") = "U000086"
        btnEdit.Visible = mDisable
        btnPrint.Visible = mDisable
        btnAdd.Visible = mDisable

        imgBuyer.Attributes.Add("onclick", "getBsuNameNew(" & Session("sBsuid") & ");return false;") 'OnClientClick="getClientName();return true;" 
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim sqlStr As New StringBuilder
            h_EntryId.Value = p_Modifyid
            If p_Modifyid > 0 Then
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                sqlStr.Append("select USR_DISPLAY_NAME, BYR_H.* FROM BYR_H inner JOIN OASIS.dbo.USERS_M on USR_NAME=BYR_USR_ID where BYR_ID=" & p_Modifyid)
                dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    h_BYR_ID.Value = dt.Rows(0)("BYR_USR_ID")
                    txtBuyer.Text = dt.Rows(0)("USR_DISPLAY_NAME")
                    txtBYR_Descr.Text = dt.Rows(0)("BYR_DESCR")
                    chkBYR_ACTIVE.Checked = dt.Rows(0)("BYR_ACTIVE")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
            FormatFigures()
            sqlStr = New StringBuilder
            sqlStr.Append("select BYD_ID, BYD_BSU_ID, BSU_NAME, replace(convert(varchar(30), BYD_FDate, 106),' ','/') BYD_FDATE, replace(convert(varchar(30), BYD_EDate, 106),' ','/') BYD_EDATE FROM BYR_H inner JOIN BYR_D on BYR_ID=BYD_BYR_ID INNER JOIN vw_OSO_BUSINESSUNIT_M on BSU_ID=BYD_BSU_ID where BYD_BYR_ID=" & p_Modifyid)
            fillGridView(BYRFooter, grdBYR, sqlStr.ToString)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next
        'txtOthers.Text = Convert.ToDouble(txtOthers.Text).ToString("####0.00")
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        h_delete.Value = "0"
        chkBYR_ACTIVE.Enabled = True

        txtBuyer.Text = ""
        txtBYR_Descr.Text = ""
    End Sub

    Private Property BYRFooter() As DataTable
        Get
            Return ViewState("BYRFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("BYRFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
    End Sub

    Private Sub showNoRecordsFound()
        If Not BYRFooter Is Nothing AndAlso BYRFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdBYR.Columns.Count - 2
            grdBYR.Rows(0).Cells.Clear()
            grdBYR.Rows(0).Cells.Add(New TableCell())
            grdBYR.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdBYR.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdBYR_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdBYR.RowCancelingEdit
        grdBYR.EditIndex = -1
        grdBYR.DataSource = BYRFooter
        grdBYR.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdBYR_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdBYR.RowCommand
        If e.CommandName = "AddNew" Then
            Dim hdnBSU_ID As HiddenField = grdBYR.FooterRow.FindControl("hdnBSU_ID")
            Dim txtBSU_NAME As TextBox = grdBYR.FooterRow.FindControl("txtBSU_NAME")
            Dim txtBYDFDate As TextBox = grdBYR.FooterRow.FindControl("txtBYDFDate")
            Dim txtBYDEDate As TextBox = grdBYR.FooterRow.FindControl("txtBYDEDate")

            lblError.Text = ""
            If txtBSU_NAME.Text.Trim.Length = 0 Then lblError.Text &= "Business Unit"
            If txtBSU_NAME.Text.Trim.Length = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Quantity"
            If lblError.Text.Length > 0 Then
                lblError.Text &= " Mandatory!!!!"
                showNoRecordsFound()
                Exit Sub
            End If

            If BYRFooter.Rows(0)(1) = -1 Then BYRFooter.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = BYRFooter.NewRow
            mrow("BYD_ID") = 0
            mrow("BYD_BSU_ID") = hdnBSU_ID.Value
            mrow("BSU_NAME") = txtBSU_NAME.Text
            mrow("BYD_FDATE") = txtBYDFDate.Text
            mrow("BYD_EDATE") = txtBYDEDate.Text
            BYRFooter.Rows.Add(mrow)

            grdBYR.EditIndex = -1
            grdBYR.DataSource = BYRFooter
            grdBYR.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdBYR_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdBYR.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim hdnBSU_ID As HiddenField = e.Row.FindControl("hdnBSU_ID")
            Dim txtBSU_NAME As TextBox = e.Row.FindControl("txtBSU_NAME")
            Dim txtBYDFDate As TextBox = e.Row.FindControl("txtBYDFDate")
            Dim txtBYDEDate As TextBox = e.Row.FindControl("txtBYDEDate")
            Dim imgClient As ImageButton = e.Row.FindControl("imgClient")

            If Not txtBSU_NAME Is Nothing Then
                imgClient.Attributes.Add("onclick", "javascript: getClientNameNew('" & hdnBSU_ID.ClientID & "','" & txtBSU_NAME.ClientID & "','" & txtBYDFDate.ClientID & "','" & txtBYDEDate.ClientID & "'); return false")
            End If
        End If
    End Sub

    Protected Sub grdBYR_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdBYR.RowDeleting
        Dim mRow() As DataRow = BYRFooter.Select("ID=" & grdBYR.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_PRFGridDelete.Value &= ";" & mRow(0)("PRD_ID")
            BYRFooter.Select("ID=" & grdBYR.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            BYRFooter.AcceptChanges()
        End If
        If BYRFooter.Rows.Count = 0 Then
            BYRFooter.Rows.Add(BYRFooter.NewRow())
            BYRFooter.Rows(0)(1) = -1
        End If

        grdBYR.DataSource = BYRFooter
        grdBYR.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdBYR_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdBYR.RowEditing
        Try
            grdBYR.EditIndex = e.NewEditIndex
            grdBYR.DataSource = BYRFooter
            grdBYR.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdBYR_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdBYR.RowUpdating
        Dim s As String = grdBYR.DataKeys(e.RowIndex).Value.ToString()
        Dim hdnBSU_ID As HiddenField = grdBYR.Rows(e.RowIndex).FindControl("hdnBSU_ID")
        Dim txtBSU_NAME As TextBox = grdBYR.Rows(e.RowIndex).FindControl("txtBSU_NAME")
        Dim txtBYDFDate As TextBox = grdBYR.Rows(e.RowIndex).FindControl("txtBYDFDate")
        Dim txtBYDEDate As TextBox = grdBYR.Rows(e.RowIndex).FindControl("txtBYDEDate")

        Dim mrow As DataRow
        mrow = BYRFooter.Select("ID=" & s)(0)
        mrow("BYD_BSU_ID") = hdnBSU_ID.Value
        mrow("BSU_NAME") = txtBSU_NAME.Text
        mrow("BYD_FDATE") = txtBYDFDate.Text
        mrow("BYD_EDATE") = txtBYDEDate.Text

        grdBYR.EditIndex = -1
        grdBYR.DataSource = BYRFooter
        grdBYR.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblError.Text = ""
        If lblError.Text.Length > 0 Then lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Mandatory"
        If grdBYR.Rows.Count = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "PRF should contain atleast 1 item"

        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(4) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@BYR_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@BYR_DESCR", txtBYR_Descr.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@BYR_ACTIVE", IIf(chkBYR_ACTIVE.Checked, 1, 0), SqlDbType.Int)
        pParms(4) = Mainclass.CreateSqlParameter("@BYR_USR_ID", h_BYR_ID.Value, SqlDbType.VarChar)
        'BYR_DESCR, BYR_ACTIVE, BYR_USR_ID
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveBYR_H", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim RowCount As Integer
            For RowCount = 0 To BYRFooter.Rows.Count - 1
                Dim iParms(5) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, BYRFooter.Rows(RowCount)("BYD_ID"))
                If BYRFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@BYD_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("BYD_BYR_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@BYD_BSU_ID", BYRFooter.Rows(RowCount)("BYD_BSU_ID"), SqlDbType.VarChar)
                iParms(4) = Mainclass.CreateSqlParameter("@BYD_FDATE", BYRFooter.Rows(RowCount)("BYD_FDATE"), SqlDbType.Date)
                iParms(5) = Mainclass.CreateSqlParameter("@BYD_EDATE", BYRFooter.Rows(RowCount)("BYD_EDATE"), SqlDbType.Date)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveBYR_D", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            If h_PRFGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_PRFGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@BYD_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@BYD_BYR_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from BYR_D where BYD_ID=@BYD_ID and BYD_BYR_ID=@BYD_BYR_ID", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            stTrans.Commit()
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))

        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdBYR.DataSource = BYRFooter
        grdBYR.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdBYR.DataSource = BYRFooter
        grdBYR.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "delete from BYR_H where BYR_ID=@BYR_ID"
                sqlStr &= " delete from BYR_D where BYD_BYR_ID=@BYR_ID"

                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@BYR_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim PRF_ID As Integer = CType(h_EntryId.Value, Integer)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptPRFDetails"
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PRF_ID", PRF_ID, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            If ViewState("MainMnu_code") = "PI02005" Then
                params("reportCaption") = "Procurement Requisition Form (Framework) "
            ElseIf ViewState("MainMnu_code") = "PI02005" Then
                params("reportCaption") = "Procurement Requisition Form (Non Framework) "
            ElseIf ViewState("MainMnu_code") = "PI02007" Then
                params("reportCaption") = "Quotation Request Form"
            Else
                params("reportCaption") = "Procurement Requisition Form (Non Framework) "
            End If
            params("@PRF_ID") = 1
            If ViewState("MainMnu_code") = "PI02007" Then
                params("showHide") = "quote"
            Else
                params("showHide") = "prf"
            End If
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptPRF.rpt"

            Dim subRep(2) As MyReportClass

            subRep(0) = New MyReportClass
            Dim subcmd1 As New SqlCommand
            subcmd1.CommandText = "rptPRF_Items"
            Dim sqlParam1(1) As SqlParameter
            sqlParam1(0) = Mainclass.CreateSqlParameter("@PRF_ID", PRF_ID, SqlDbType.Int)
            subcmd1.Parameters.Add(sqlParam1(0))
            subcmd1.CommandType = Data.CommandType.StoredProcedure
            subcmd1.Connection = New SqlConnection(str_conn)
            subRep(0).Command = subcmd1
            subRep(0).ResourceName = "wBTA_Sector"

            Dim subcmd2 As New SqlCommand
            subRep(1) = New MyReportClass
            subcmd2.CommandText = "rptPRF_Items"
            Dim sqlParam2(0) As SqlParameter
            sqlParam2(0) = Mainclass.CreateSqlParameter("@PRF_ID", PRF_ID, SqlDbType.Int)
            subcmd2.Parameters.Add(sqlParam2(0))
            subcmd2.CommandType = Data.CommandType.StoredProcedure
            subcmd2.Connection = New SqlConnection(str_conn)
            subRep(1).Command = subcmd2
            subRep(1).ResourceName = "wQUT_Report"

            repSource.IncludeBSUImage = True
            repSource.SubReport = subRep
            'End If
            Session("ReportSource") = repSource
            Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        h_EntryId.Value = 0
        'h_bta_Status.Value = "O"
        ViewState("EntryId") = 0
        SetDataMode("add")
        grdBYR.DataSource = BYRFooter
        grdBYR.DataBind()
        showNoRecordsFound()
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
End Class
