﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="REServices.aspx.vb" Inherits="Inventory_REServices" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
           <asp:Label runat="server" ID="rptHeader" Text="Additions and Deductions"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server"   style="width: 100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td  >
                            <table width="100%"  >
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Description<span style="color: #800000">*</span></span></td>
                                    <td    width="30%">
                                        <asp:TextBox ID="txtDescr" runat="server"   CssClass="inputbox"></asp:TextBox></td>
                                    <td align="left" width="20%" ><span class="field-label">Add/Ded</span></td>
                                    <td align="left" width="30%" >
                                        <asp:RadioButton ID="radAddn" runat="server" GroupName="Addn_Dedn" Text="Addition" Checked="true" CssClass="field-label"  />
                                        <asp:RadioButton ID="radDedn" runat="server" GroupName="Addn_Dedn" Checked="false" Text="Deduction" CssClass="field-label"  /></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Is Default Percent(Y/N)</span></td>
                                    <td  align="left">
                                        <asp:RadioButton ID="radPercent" runat="server" GroupName="default" Text="Yes" Checked="true" CssClass="field-label" />
                                        <asp:RadioButton ID="radValue" runat="server" GroupName="default" Checked="false" Text="No" CssClass="field-label" /></td>
                                    <td align="left" ><span class="field-label">Default Value</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtDefaultAmount" runat="server"  CssClass="inputbox"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Location</span></td>
                                    <td  align="left">
                                        <asp:DropDownList ID="ddlLocation"   runat="server" AutoPostBack="True"  ></asp:DropDownList></td>
                                    <td align="left" ><span class="field-label">Refundable</span></td>
                                    <td align="left" >
                                        <asp:RadioButton ID="rbRefYes" runat="server" GroupName="Refund" Text="Yes" Checked="true" CssClass="field-label" />
                                        <asp:RadioButton ID="rbRefNo" runat="server" GroupName="Refund" Checked="false" Text="No" CssClass="field-label" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
