<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="GoodsReceipt.aspx.vb" Inherits="Inventory_GoodsReceipt" Title="Goods Receipt Note" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

      <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />


    <script type="text/javascript" language="javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
    1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function calculate() {
        }

        function test(from, to) {
            if (document.getElementById(to).value == "") document.getElementById(to).value = document.getElementById(from).value;
        }

        function calculate() {
            var net = 0;
            document.getElementById("<%=txtTotal.ClientID %>").value = eval(document.getElementById("<%=h_Total.ClientID %>").value) + eval(document.getElementById("<%=txtOthers.ClientID %>").value) + eval(document.getElementById("<%=txtVat.ClientID %>").value);
            return false;
        }
        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
        function showDocument() {
            var sFeatures;

            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";
            contenttype = document.getElementById('<%=hdnContentType.ClientID %>').value;
            filename = document.getElementById('<%=hdnFileName.ClientID %>').value;
            //result = window.showModalDialog("IFrameNew.aspx?id=0&path=Inventory&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            return ShowWindowWithClose("IFrameNew.aspx?id=0&path=Inventory&filename=" + filename + "&contenttype=" + contenttype, 'search', '55%', '85%')
            return false;
            
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="pgTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div>
                    <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr valign="bottom">
                            <td align="left" colspan="4">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <%--<tr class="subheader_img">
                                        <td align="center" colspan="4" valign="middle">
                                            <div align="center">
                                             </div>
                                        </td>
                                    </tr>--%>
                                    <tr id="PJRow" runat ="server" visible ="false"   >
                                        <td align="left" width="20%">
                                            <asp:Label ID="Label1" runat="server" Text="PJ Number" CssClass="field-label"></asp:Label><span class="text-danger">*</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtPJ" runat="server" Enabled="false"></asp:TextBox></td>
                                        <td align="left" width="20%"><span class="field-label">PJ Date</span><span class="text-danger">*</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtPJDate" runat="server"></asp:TextBox>
                                            
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left" width="20%">
                                            <asp:Label ID="lblGRNNo" runat="server" Text="GRN Number" CssClass="field-label"></asp:Label><span class="text-danger">*</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtGRNNo" runat="server" Enabled="false"></asp:TextBox></td>
                                        <td align="left" width="20%"><span class="field-label">GRN Date</span><span class="text-danger">*</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtGRNDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="lnkGRNDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtGRNDate" PopupButtonID="lnkGRNDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblPRFNo" runat="server" Text="PRF Number" CssClass="field-label"></asp:Label></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtPRFNo" runat="server" Enabled="false"></asp:TextBox></td>
                                        <td align="left"><span class="field-label">PRF Date</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtPRFDate" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>

                                        <td align="left"><span class="field-label">Budget Account</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtACTID" runat="server" Enabled="false"></asp:TextBox></td>
                                        <td align="left"></td>
                                        <td align="left"></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Supplier</span><span class="text-danger">*</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSupplier" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                                        </td>
                                        <td align="left"><span class="field-label">Payment Terms</span><span class="text-danger">*</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTerms" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Supplier Address</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSupplierAddress" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                        <td align="left"><span class="field-label">Contact Person for Delivery</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtContact" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Invoice Number</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtInvNo" runat="server"></asp:TextBox></td>
                                        <td align="left"><span class="field-label">Invoice Date</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtINVDate" AutoPostBack="True" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="lnkINVDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtINVDate" PopupButtonID="lnkINVDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:HyperLink ID="lnkPRF" runat="server" Visible="false"></asp:HyperLink></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Invoice Narration</span></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtNarration" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="left">
                                            <asp:GridView ID="grdGRN" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true"
                                                CaptionAlign="Top" PageSize="15" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                                <Columns>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPRD_ID" runat="server" Text='<%# Bind("PRD_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPRD_ITEM_ID" runat="server" Text='<%# Bind("PRD_ITM_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPRD_DESCR" runat="server" Text='<%# Bind("PRD_DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="h_Special" runat="server" Value='<%# Bind("PRD_SPECIAL") %>'></asp:HiddenField>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="txtPRD_DESCR" runat="server" Text='<%# Bind("PRD_DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="h_Special" runat="server" Value='<%# Bind("PRD_SPECIAL") %>'></asp:HiddenField>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PRF Quantity" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPRD_QTY" runat="server" Text='<%# Bind("PRD_QTY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="txtPRD_QTY" runat="server" Style="text-align: right" Text='<%# Bind("PRD_QTY") %>'></asp:Label><span class="text-danger">*</span>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="GRN Quantity" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGRD_QTY" runat="server" Text='<%# Bind("GRD_QTY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="splGRD_QTY" runat="server" Style="text-align: right" Text='<%# Bind("GRD_QTY") %>'></asp:Label>
                                                            <asp:TextBox ID="txtGRD_QTY" runat="server" Style="text-align: right" Text='<%# Bind("GRD_QTY") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Unit Rate" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPRD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("PRD_RATE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="txtPRD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("PRD_RATE") %>'></asp:Label>
                                                            <asp:TextBox ID="splPRD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("PRD_RATE") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="lblPRD_TOTAL" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("PRD_TOTAL") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPRD_TOTAL" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("PRD_TOTAL") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateGRN" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelGRN" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditGRN" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Upload Total</span>
                                            <asp:TextBox ID="txtUploadTotal" Style="text-align: right" Text="0.0" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left">
                                            <asp:FileUpload ID="UploadDocPhoto" runat="server" Visible="false" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                            <asp:HiddenField ID="hdnFileName" runat="server" />
                                            <asp:HiddenField ID="hdnContentType" runat="server" />
                                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Visible="false" />
                                        </td>
                                        <td align="left">
                                            <%--Total Amount(AED)--%>
                                            <asp:Label ID="lblTotalAmt" runat="server" Style="text-align: right" Text="" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTotal" Style="text-align: right" Text="0.0" runat="server" Enabled="false"></asp:TextBox></td>
                                    </tr>
                                    <tr id="trCCS" runat="server" visible="False">
                                        <td align="left" colspan="4">
                                            <asp:GridView ID="gvCCS" runat="server"
                                                EmptyDataText="No Data" Width="98%" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField DataField="Type" HeaderText="Type" />
                                                    <asp:BoundField DataField="Description" HeaderText="Description" />
                                                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Procurement Requistion Form Comments</span></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtPRFComments" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">GRN Comments</span></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtGRNComments" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                                            <asp:Button ID="btnTransfer" runat="server" CausesValidation="False" CssClass="button" Visible="false" />
                                            <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button" Text="Post PJV" Visible="false" />
                                            <input type="button" id="btnSave" class="button" value="  Save  " runat="server" onclick="this.disabled = true;" onserverclick="btnSave_Click" />
                                            <asp:Button ID="btnClosePRF" runat="server" CausesValidation="False" CssClass="button" Text="Close PRF" Visible="false" OnClientClick="return confirm('Are you sure you will NOT receive pending value ?');" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_Prf_Id" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_total" runat="server" />
                                            <asp:HiddenField ID="h_Grn_PJV_No" runat="server" />
                                            <asp:HiddenField ID="h_GRNGridDelete" Value="0" runat="server" />
                                            <asp:TextBox ID="txtOthers" Visible="false" Style="text-align: right" Text="0.0" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtVAT" Visible="false" Style="text-align: right" Text="0.0" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>
