<%@ Page Language="VB" MasterPageFile="~/mainMasterPageSS.master" AutoEventWireup="false" CodeFile="BTA.aspx.vb" Inherits="BTA" title="Business Travel Approval" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ MasterType VirtualPath="~/mainMasterPageSS.master"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script type="text/javascript" language="javascript">
    window.format = function(b, a) {
        if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f; ) c[0] = "0" + c[0]; else+c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
    };

    function calculate() {
        var txtTotalA = document.getElementById('<%=txtTotalA.ClientID %>').value.replace(",", "");
        var txtVisaCost = document.getElementById('<%=txtVisaCost.ClientID %>').value.replace(",", "");
        var txtExpAmtAed = document.getElementById('<%=txtExpAmtAed.ClientID %>').value.replace(",", "");
        var txtBusinessClassAed = document.getElementById('<%=txtBusinessClassAed.ClientID %>').value.replace(",", "");
        var txtEconomyClassAed = document.getElementById('<%=txtEconomyClassAed.ClientID %>').value.replace(",", "");
        var txtTravelInsuranceAed = document.getElementById('<%=txtTravelInsuranceAed.ClientID %>').value.replace(",", "");

        document.getElementById('<%=txtTotalC.ClientID %>').value = format( "#,##0.00", parseFloat(txtExpAmtAed) + parseFloat(txtEconomyClassAed) + parseFloat(txtBusinessClassAed) + parseFloat(txtTravelInsuranceAed));
        var txtTotalC = document.getElementById('<%=txtTotalC.ClientID %>').value.replace(",", "");

        document.getElementById('<%=lblTotalABC.ClientID %>').value = format( "#,##0.00", parseFloat(txtTotalA) + parseFloat(txtVisaCost) + parseFloat(txtTotalC));
    }

    function test(from, to) {
        if (document.getElementById(to).value == "") document.getElementById(to).value = document.getElementById(from).value;
    }

    function getClientName(txtClient,hdnClient) {
        var sFeatures;
        var lstrVal;
        var lintScrVal;
        var pMode;
        var NameandCode;
        sFeatures = "dialogWidth: 760px; ";
        sFeatures += "dialogHeight: 420px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        pMode = "TPTBSU"
        url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=Business Unit";
        result = window.showModalDialog(url, "", sFeatures);
        if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById(txtClient).value = NameandCode[1];
        document.getElementById(hdnClient).value = NameandCode[0];
        document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
    }
    
    function calculateTravel() {
        var net = 0;
        if (typeof grdNights != 'undefined')
        for (cnt = 0; cnt < (grdNights.length/2); cnt++)
            if (document.getElementById(grdNights[cnt])) {
                if (document.getElementById(grdNights[cnt]).value == "") document.getElementById(grdNights[cnt]).value = 1;
                if (document.getElementById(grdDailyExp[cnt]).value == "") document.getElementById(grdDailyExp[cnt]).value = 0;
                var nights = document.getElementById(grdNights[cnt]).value;
                var dailyexp = document.getElementById(grdDailyExp[cnt]).value;
                var total = eval(nights) * eval(dailyexp);
                document.getElementById(grdTotalExp[cnt]).value = format( "#,##0.00", total);
                net = net + total;
        }
        document.getElementById('<%=txtTotalA.ClientID %>').value = format( "#,##0.00", net);
        calculate();
        return false;
    }
    function entitlement() {
        if (document.getElementById('<%=ddlEntitlement.ClientID %>').value == 5)
            document.getElementById('<%=txtEntitlement.ClientID %>').style.setAttribute('display', '');
        else
            document.getElementById('<%=txtEntitlement.ClientID %>').style.setAttribute('display', 'none');
    }
    function formatme(me) {
        document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
    }
    function Numeric_Only() {
        //alert(event.keyCode)
        if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
            if (event.keyCode == 13 || event.keyCode == 46)
            { return false; }
            event.keyCode = 0
        }
    }
</script>
    <div>
<table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
    cellspacing="0" style="width: 90%; border-collapse: collapse;">
<tr valign="bottom">
    <td align="center" style="height: 19px" valign="bottom" colspan="2">
       <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td></tr>
<tr>
    <td align="left" style="height: 166px" valign="top">
        <table align="left" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" style="width: 100%">
        <tr class="subheader_img">
            <td align="center" colspan="8" valign="middle"><div align="center">Business Travel and Advance Approval</div></td></tr>
        <tr>
            <td width="10%" class="matters"><asp:Label ID="lblBTANo" runat="server" Text="BTA NO."></asp:Label></td><td class="matters">:</td>
            <td width="40%" class="matters" align="left"><asp:TextBox ID="txtBTANo" runat="server" Enabled="false" Width="120px" CssClass="inputbox"></asp:TextBox></td>
            <td width="10%" class="matters">LPO NO.</td><td class="matters">:</td>
            <td width="40%" colspan="3" class="matters" align="left"><asp:TextBox ID="txtLPONo" runat="server" Width="90px" CssClass="inputbox"></asp:TextBox></td></tr>
        <tr>
            <td class="matters">Name of Employee<span style="color: #800000">*</span></td><td class="matters">:</td>
            <td class="matters" align="left"><asp:TextBox ID="txtEmployeeName" runat="server" Width="300px" CssClass="inputbox"></asp:TextBox></td>
            <td class="matters">Date of BTA<span style="color: #800000">*</span></td><td class="matters">:</td>
            <td class="matters" colspan="3" align="left">
                <asp:TextBox ID="txtBTADate" runat="server" Width="110px" CssClass="inputbox"></asp:TextBox>
                <asp:ImageButton ID="lnkBTADate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtBTADate" PopupButtonID="lnkBTADate">
                            </ajaxToolkit:CalendarExtender></td></tr>
        <tr>
            <td class="matters">Designation</td><td class="matters">:</td>
            <td class="matters" align="left"><asp:TextBox ID="txtDesgination" runat="server" Width="200px" CssClass="inputbox"></asp:TextBox></td>
            <td class="matters">Department / Unit</td><td class="matters">:</td>
            <td class="matters" colspan="3" align="left"><asp:TextBox ID="txtDepartment" runat="server" Width="200px" CssClass="inputbox"></asp:TextBox></td></tr>
        <tr>
            <td class="matters">Entitlement<span style="color: #800000">*</span></td><td class="matters">:</td>
            <td class="matters" align="left"><asp:DropDownList ID="ddlEntitlement" runat="server"></asp:DropDownList>
            <asp:TextBox ID="txtEntitlement" runat="server" Width="200px" style="display:none" CssClass="inputbox"></asp:TextBox></td>
            <td class="matters">Mobile No</td><td class="matters">:</td>
            <td class="matters" colspan="3" align="left"><asp:TextBox ID="txtMobileNo" runat="server" Width="200px" CssClass="inputbox"></asp:TextBox></td></tr>
        <tr>
            <td colspan="8" align="left" class="matters">
                <asp:GridView ID="grdBO" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true"
                    CaptionAlign="Top" PageSize="15" SkinID="GridViewView" CssClass="BlueTable" DataKeyNames="ID">
                    <columns>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblSEC_NO" runat="server" Text='<%# Bind("SEC_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sector Travelling">
                            <ItemTemplate>
                                <asp:Label ID="lblSec_Travel" runat="server" Width="90%" Text='<%# Bind("Sec_Travel") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSec_Travel" runat="server" Width="90%" Text='<%# Bind("Sec_Travel") %>'></asp:TextBox><span style="color: #800000">*</span>
                            <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx"
                                CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                ServiceMethod="GetSector" ServicePath="~/Inventory/BTA.aspx"
                                TargetControlID="txtSec_Travel">
                                <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                            </ajaxToolkit:AutoCompleteExtender>

                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtSec_Travel" runat="server" Width="90%" Text='<%# Bind("Sec_Travel") %>'></asp:TextBox><span style="color: #800000">*</span>
                            <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx1"
                                CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                ServiceMethod="GetSector" ServicePath="~/Inventory/BTA.aspx"
                                TargetControlID="txtSec_Travel">
                                <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                            </ajaxToolkit:AutoCompleteExtender>

                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Business Objectives">
                            <ItemTemplate>
                                <asp:Label ID="lblBus_objectives" runat="server" Width="90%" Text='<%# Bind("Bus_objectives") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtBus_objectives" runat="server" Width="90%" Text='<%# Bind("Bus_objectives") %>'></asp:TextBox><span style="color: #800000">*</span>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtBus_objectives" runat="server" Width="90%" Text='<%# Bind("Bus_objectives") %>'></asp:TextBox><span style="color: #800000">*</span>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Travel Related to which Company">
                            <ItemTemplate>
                                <asp:Label ID="lblSec_company" runat="server" Width="85%" Text='<%# Bind("Sec_company") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSec_company" runat="server" Width="85%" Text='<%# Bind("Sec_company") %>'></asp:TextBox><span style="color: #800000">*</span>
                                <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                            <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtSec_company" runat="server" Width="85%" Text='<%# Bind("Sec_company") %>'></asp:TextBox><span style="color: #800000">*</span>
                                <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                            <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkUpdateBO" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                            <asp:LinkButton ID="lnkCancelBO" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="lnkAddBo" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEditBO" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />                       
                </columns>
                </asp:GridView>
            </td></tr>
        <tr class="subheader_img">
            <td valign="middle" style="height: 10px;" align="left" colspan="8">Itinerary</td></tr>
        <tr>
            <td colspan="8" align="left" class="matters">
                <asp:GridView ID="grdItinerary" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true"
                    CaptionAlign="Top" PageSize="15" SkinID="GridViewView" CssClass="BlueTable" DataKeyNames="ID">
                    <columns>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblItn_No" runat="server" Text='<%# Bind("Itn_No") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date of Travel">
                        <itemtemplate>
                            <asp:Label ID="lblItn_Date" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Date") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItn_Date" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Date") %>'></asp:TextBox>
                <asp:ImageButton ID="lnkItn_Date" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtItn_Date" PopupButtonID="lnkItn_Date">
                            </ajaxToolkit:CalendarExtender><span style="color: #800000">*</span>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItn_Date" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Date") %>'></asp:TextBox>
                <asp:ImageButton ID="lnkItn_Date" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtItn_Date" PopupButtonID="lnkItn_Date">
                            </ajaxToolkit:CalendarExtender><span style="color: #800000">*</span>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sector">
                        <itemtemplate>
                            <asp:Label ID="lblItn_Sector" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Sector") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItn_Sector" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Sector") %>'></asp:TextBox><span style="color: #800000">*</span>
                            <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx3"
                                CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                ServiceMethod="GetSector" ServicePath="~/Inventory/BTA.aspx"
                                TargetControlID="txtItn_Sector">
                                <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx3');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx3')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx3')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                            </ajaxToolkit:AutoCompleteExtender>
                            
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItn_Sector" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Sector") %>'></asp:TextBox><span style="color: #800000">*</span>
                            <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx4"
                                CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                ServiceMethod="GetSector" ServicePath="~/Inventory/BTA.aspx"
                                TargetControlID="txtItn_Sector">
                                <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx4');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx4')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx4')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                            </ajaxToolkit:AutoCompleteExtender>

                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Flight No">
                        <itemtemplate>
                            <asp:Label ID="lblItn_FlightNo" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_FlightNo") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItn_FlightNo" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_FlightNo") %>'></asp:TextBox><span style="color: #800000">*</span>
                            <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO1" runat="server" BehaviorID="AutoCompleteEx5"
                                CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                ServiceMethod="GetFlight" ServicePath="~/Inventory/BTA.aspx"
                                TargetControlID="txtItn_FlightNo">
                                <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx5');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx5')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx5')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                            </ajaxToolkit:AutoCompleteExtender>

                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItn_FlightNo" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_FlightNo") %>'></asp:TextBox><span style="color: #800000">*</span>
                            <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO1" runat="server" BehaviorID="AutoCompleteEx6"
                                CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                ServiceMethod="GetFlight" ServicePath="~/Inventory/BTA.aspx"
                                TargetControlID="txtItn_FlightNo">
                                <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx6');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx6')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx6')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                            </ajaxToolkit:AutoCompleteExtender>

                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Departure/Arrival Time">
                        <itemtemplate>
                            <asp:Label ID="lblItn_Time" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Time") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItn_Time" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Time") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItn_Time" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Time") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Flying Hours">
                        <itemtemplate>
                            <asp:Label ID="lblItn_Hours" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Hours") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItn_Hours" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Hours") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItn_Hours" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Itn_Hours") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkItineraryUpdate" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                            <asp:LinkButton ID="lnkItineraryCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="lnkItineraryAdd" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkItineraryEdit" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />                       
                </columns>
                </asp:GridView>
            </td></tr>
        <tr class="subheader_img">
            <td valign="middle" colspan="8" style="height: 10px;" align="left" width="25%">Travel Plan (estimated expenditure to include Hotel, local transport, food expenses, etc.)</td></tr>
        <tr>
            <td colspan="8" align="left" class="matters">
                <asp:GridView ID="grdTravelPlan" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true"
                    CaptionAlign="Top" PageSize="15" SkinID="GridViewView" CssClass="BlueTable" DataKeyNames="ID">
                    <columns>
                    <asp:TemplateField Visible="false">
                        <itemtemplate>
                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                        </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false">
                        <itemtemplate>
                            <asp:Label ID="lblTrv_No" runat="server" Text='<%# Bind("Trv_No") %>'></asp:Label>
                        </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From" FooterStyle-Width="130px">
                        <itemtemplate>
                            <asp:Label ID="lblTrv_From" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_From") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTrv_From" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_From") %>'></asp:TextBox>
                <asp:ImageButton ID="lnkTrv_From" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTrv_From" PopupButtonID="lnkTrv_From">
                            </ajaxToolkit:CalendarExtender>                                                       
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTrv_From" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_From") %>'></asp:TextBox>
                <asp:ImageButton ID="lnkTrv_From" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTrv_From" PopupButtonID="lnkTrv_From">
                            </ajaxToolkit:CalendarExtender>                            
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To" FooterStyle-Width="130px">
                        <itemtemplate>
                            <asp:Label ID="lblTrv_To" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_To") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTrv_To" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_To") %>'></asp:TextBox>
                <asp:ImageButton ID="lnkTrv_To" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTrv_To" PopupButtonID="lnkTrv_To">
                            </ajaxToolkit:CalendarExtender>                            
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTrv_To" runat="server" Width="65%" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_To") %>'></asp:TextBox>
                <asp:ImageButton ID="lnkTrv_To" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTrv_To" PopupButtonID="lnkTrv_To">
                            </ajaxToolkit:CalendarExtender>                            
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Service (Hotel or Car Hire)">
                        <itemtemplate>
                            <asp:Label ID="lblTrv_service" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_service") %>'></asp:Label>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTrv_service" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_service") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTrv_service" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_service") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No.of Nights">
                        <itemtemplate>
                            <asp:TextBox ID="txtTrv_Nights" style="text-align:right" runat="server" Width="75px" Enabled="false" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_Nights") %>'></asp:TextBox>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTrv_Nights" style="text-align:right" runat="server" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_Nights") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTrv_Nights" style="text-align:right" runat="server" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_Nights") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Est.Daily Expenditure">
                        <itemtemplate>
                            <asp:TextBox ID="txtTrv_DailyExp" style="text-align:right" runat="server" Width="75px" Enabled="false" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_DailyExp","{0:0.00}") %>'></asp:TextBox>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTrv_DailyExp" style="text-align:right" runat="server" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_DailyExp","{0:0.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTrv_DailyExp" style="text-align:right" runat="server" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_DailyExp","{0:0.00}") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Est.Total Expenditure AED">
                        <itemtemplate>
                            <asp:TextBox ID="txtTrv_TotalExp" style="text-align:right" runat="server" Width="75px" Enabled="false" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_TotalExp","{0:0.00}") %>'></asp:TextBox>
                        </itemtemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTrv_TotalExp" style="text-align:right" runat="server" Width="75px" Enabled="false" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_TotalExp","{0:0.00}") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTrv_TotalExp" style="text-align:right" runat="server" Width="75px" Enabled="false" Text='<%# DataBinder.Eval(Container.DataItem, "Trv_TotalExp","{0:0.00}") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkTravelUpdate" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                            <asp:LinkButton ID="lnkTravelCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="lnkTravelAdd" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkTravelEdit" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />                       
                </columns>
                </asp:GridView>
            </td></tr>
        <tr>
            <td colspan="6"></td>
            <td class="subheader_img">Total (A) AED</td>
            <td class="matters"><asp:TextBox ID="txtTotalA" style="text-align:right" Text="0.0" runat="server" Width="120px" Enabled="false" CssClass="inputbox"></asp:TextBox></td></tr>
        <tr>
            <td colspan="8">
            <table align="left" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" style="width: 100%"><tr>
            <td class="subheader_img">Visa Requirements</td>
            <td>:</td>
            <td class="matters" colspan="2"><asp:CheckBox ID="chkVisa" TextAlign="Right" Text="Visa required" runat="server" /></td>
            <td class="subheader_img">Cost of Visa (B)</td>
            <td class="matters">AED</td>
            <td class="matters"><asp:TextBox ID="txtVisaCost" style="text-align:right" Text="0.0" runat="server" Width="120px" CssClass="inputbox"></asp:TextBox></td></tr></table></td></tr>
        <tr>
            <td class="matters" colspan="8">
            <table align="left" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" style="width: 50%">
            <tr class="subheader_img">
            <td>Expenses</td>
            <td>Amount (AED)</td></tr>
            <tr>
            <td class="matters">Local Travel Expenses(if any)</td>
            <td class="matters" align="left"><asp:TextBox ID="txtExpAmtAed" style="text-align:right" Text="0.0" runat="server" Width="120px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td colspan="3" class="matters">Cost of tickets</td></tr>
            <tr>
            <td class="matters">&nbsp&nbspBusiness class</td>
            <td class="matters" align="left"><asp:TextBox ID="txtBusinessClassAed" style="text-align:right" Text="0.0" runat="server" Width="120px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td class="matters">&nbsp&nbspEconomy Class</td>
            <td class="matters" align="left"><asp:TextBox ID="txtEconomyClassAed" style="text-align:right" Text="0.0" runat="server" Width="120px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td class="matters">Others:Travel insurance</td>
            <td class="matters" align="left"><asp:TextBox ID="txtTravelInsuranceAed" style="text-align:right" Text="0.0" runat="server" Width="120px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td class="subheader_img">Total in AED (C)</td>
            <td class="matters" align="left"><asp:TextBox ID="txtTotalC" style="text-align:right" Text="0.0" Enabled="false" runat="server" Width="120px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td class="matters">Travel Trip Advance Request</td>
            <td class="matters" align="left"><asp:TextBox ID="txtAdvanceRequestAed" style="text-align:right" Text="0.0" runat="server" Width="120px" CssClass="inputbox"></asp:TextBox></td></tr>
            </table>
            <table align="left" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" style="width: 50%">
            <tr valign="top">
            <td  class="subheader_img" align="left">Total Expenses(A+B+C)</td><td>:</td>
            <td class="matters" colspan="2" align="left"><asp:TextBox ID="lblTotalABC" style="text-align:right" Text="0.0" runat="server" Enabled="false" Width="125px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td class="matters">Amount Budgeted<span style="color: #800000">*</span></td><td>:</td>
            <td class="matters" colspan="2" align="left"><asp:TextBox ID="txtAmountBudgeted" style="text-align:right" Text="0.0" runat="server" Width="125px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td class="matters">Amount Chargeable to the Client<span style="color: #800000">*</span></td><td>:</td>
            <td class="matters" colspan="2" align="left"><asp:TextBox ID="txtAmountChgClient" style="text-align:right" Text="0.0" runat="server" Width="125px" CssClass="inputbox"></asp:TextBox></td></tr>
            <tr>
            <td class="matters">Client Name<span style="color: #800000">*</span></td><td>:</td>
            <td class="matters" colspan="2" align="left"><asp:TextBox ID="txtClientName" TextMode="MultiLine" Height="30px" runat="server" Width="210px" SkinID="MultiText"></asp:TextBox></td></tr>
            </table>
            </td>
            </tr>

        <tr>
            <td align="center" class="matters" colspan="8">
                <asp:Button ID="btnAccept" runat="server" CausesValidation="False" CssClass="button" Text="Accept" />
                <asp:Button ID="btnReject" runat="server" CausesValidation="False" CssClass="button" Text="Reject" />
                <asp:Button ID="btnCopy" runat="server" CausesValidation="False" CssClass="button" Text="Copy" />
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td></tr>
        <tr>
            <td align="center" class="matters" colspan="8">
                <asp:HiddenField id="h_ACTTYPE" runat="server" />
                <asp:HiddenField ID="h_EntryId" runat="server" value="0"/>
                <asp:HiddenField ID="h_Mode" runat="server" />
                <asp:HiddenField ID="h_ItemID" runat="server" EnableViewState="False" />
                <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                <asp:HiddenField ID="h_bta_Status" runat="server" Value="O" />
                <asp:HiddenField ID="h_Emp_id" runat="server" /></td></tr>
                <asp:HiddenField ID="h_BOGridDelete" value="0" runat="server" />
                <asp:HiddenField ID="h_itnGridDelete" value="0" runat="server" />
                <asp:HiddenField ID="h_trvGridDelete" value="0" runat="server" />
                <asp:HiddenField ID="hGridRefresh" value="0" runat="server" />                
        </table>
        </td>
    </tr>           
    </table>
    </div>
</asp:Content>

