<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InvNewItemPopup.aspx.vb"
    Inherits="Inventory_InvNewItemPopup" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
<link href="cssfiles/Accordian.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../cssfiles/tabber.js"></script>

<link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
<link href="cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
<link href="cssfiles/example.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript" src="../chromejs/chrome.js"></script>

<script language="javascript" type="text/javascript">
    function getSubCategory() {
        var sFeatures;
        var lstrVal;
        var lintScrVal;
        var pMode;
        var NameandCode;
        sFeatures = "dialogWidth: 760px; ";
        sFeatures += "dialogHeight: 420px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        pMode = "INVSUBCATEGORY"
        url = "../common/PopupSelect.aspx?id=" + pMode;
        result = window.showModalDialog(url, "", sFeatures);
        if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById("<%=txtSubCategory.ClientID %>").value = NameandCode[1];
        document.getElementById("<%=txtCategory.ClientID %>").value = NameandCode[2];
        document.getElementById("<%=hdnISCID.ClientID %>").value = NameandCode[0];
        return false;
    }
</script>

<head runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/EOSStyleSheet.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../cssfiles/tabber.js"></script>

    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../cssfiles/tabber.js"></script>

    <style type="text/css">
        .style1
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            height: 33px;
        }
    </style>

</head>
<body onload="listen_window();" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server" style="text-align: center">
    <!--1st drop down menu -->
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600"
        EnablePageMethods="true">
    </ajaxToolkit:ToolkitScriptManager>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="matters" align="center">
                <table align="center" style="border-collapse: collapse; height: 15px;" border="1"
                    bordercolor="#1b80b6" cellpadding="3" cellspacing="0" width="100%">
                    <tr class="subheader_img" valign="top">
                        <td align="left" colspan="3" style="height: 19px" valign="middle">
                            Add New Item
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left" colspan="1">
                            Sub Category
                        </td>
                        <td class="matters" width="1%">
                            :
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtSubCategory" runat="server" CssClass="inputbox" Height="20px"
                                Enabled="False"></asp:TextBox>
                            <asp:ImageButton ID="imgCategory" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getSubCategory();return false;" TabIndex="14" />
                            <asp:CustomValidator ID="CustomValidator4" runat="server" ControlToValidate="txtSubCategory"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Sub Category"
                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left" colspan="1">
                            Category</td>
                        <td class="matters" width="1%">
                            :</td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtCategory" runat="server" CssClass="inputbox" Height="22px" Enabled="False"
                                AutoCompleteType="Disabled"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" align="left">
                            Item Description
                        </td>
                        <td class="style1" width="1%">
                            :
                        </td>
                        <td class="style1" align="left">
                            <asp:TextBox ID="txtItemDescr" runat="server" Height="22px" Width="322px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                            Default UOM
                        </td>
                        <td class="matters" width="1%">
                            :
                        </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlUOM" runat="server" Height="100%" Width="75px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                            Item Type
                        </td>
                        <td class="matters" width="1%">
                            :
                        </td>
                        <td class="matters" align="left">
                            <asp:CheckBox ID="chkIsInv" runat="server" Text="Inventory Item" Width="150px" />
                            <asp:CheckBox ID="chkIsIT" runat="server" Text="IT Item" Width="200px" 
                                Enabled="False" />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left">
                            Narration
                        </td>
                        <td class="matters" width="1%">
                            :
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" Height="64px" SkinID="MultiText" TabIndex="160"
                                TextMode="MultiLine" Width="342px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="3">
                            &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="button" SkinID="ButtonNormal"
                                Text="Save" />
                            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_ItemID" runat="server" EnableViewState="False" />
    <asp:HiddenField ID="h_ITMID" runat="server" EnableViewState="False" />
    <asp:HiddenField ID="hdnISCID" runat="server" />
    <asp:HiddenField ID="h_SelectedId" runat="server" />
    </form>
</body>
</html>
