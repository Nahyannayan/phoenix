﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Web.Services

Partial Class Inventory_ItemPrice
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                'ts
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI01150") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim dt As New DataTable
            dt = MainObj.getRecords("SELECT BSU_NAME, ITEM_SALEBSU.*, isnull(ITB_DESCR,ITM_DESCR) ITM_DESCR FROM OASIS..BUSINESSUNIT_M WITH(NOLOCK) left outer join ITEM_SALEBSU WITH(NOLOCK) ON ITB_BSU_ID=BSU_ID AND BSU_ID='" & Session("sBSUID") & "' left outer join ITEM_SALE on ITM_ID=ITB_ITM_ID where ITB_ID=" & p_Modifyid, "OASIS_PUR_INVConnectionString")

            If p_Modifyid > 0 AndAlso dt.Rows.Count > 0 Then
                txtItemId.Text = dt.Rows(0)("ITB_ITM_ID").ToString
                h_ITM_ID.Value = dt.Rows(0)("ITB_ITM_ID").ToString
                h_ITB_ID.Value = dt.Rows(0)("ITB_ID").ToString
                txtBUID.Text = dt.Rows(0)("ITB_BSU_ID").ToString
                txtItemCPrice.Text = dt.Rows(0)("ITB_COST").ToString
                txtItemSPrice.Text = dt.Rows(0)("ITB_SELL").ToString
                TxtItemName.Text = dt.Rows(0)("ITM_DESCR").ToString
                txtItem.Text = dt.Rows(0)("ITM_DESCR").ToString
                TxtBUName.Text = dt.Rows(0)("BSU_NAME").ToString
            Else
                TxtBUName.Text = dt.Rows(0)("BSU_NAME").ToString
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable

        If ViewState("datamode") = "edit" Then
            txtItemId.Enabled = mDisable
            txtItem.Enabled = mDisable
        Else
            txtItemId.Enabled = Not mDisable
            txtItem.Enabled = Not mDisable
        End If

        If ViewState("datamode") = "edit" Or ViewState("datamode") = "view" Then
            imgICMDescr.Visible = False
            imgItem.Visible = False
            lbtnClearItem.Visible = False
        Else
            imgICMDescr.Visible = True
            imgItem.Visible = True
            lbtnClearItem.Visible = True
        End If
        txtItemCPrice.Enabled = Not mDisable
        txtItemSPrice.Enabled = Not mDisable
    End Sub

    Sub clear_All()
        h_ITB_ID.Value = 0
        h_ITM_ID.Value = 0
        txtItem.Text = ""
        txtItemId.Text = ""
        txtBUID.Text = Session("sBsuid")
        txtBUID.ReadOnly = True
        TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Session("sBsuid") & "'", "MainDBO")
        txtItemId.Text = ""
        TxtItemName.Text = ""
        txtItemCPrice.Text = "0.00"
        txtItemSPrice.Text = "0.00"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from sal_d where sad_itm_id=" & h_ITB_ID.Value) = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from ITEM_SALEBSU where ITB_ITM_id=" & h_ITB_ID.Value & "and ITB_BSU_ID='" & Session("sBsuid") & "'")
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = "unable to delete, " & txtItemId.Text & " used in transactions"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            'If txtItemId.Text.Trim = "" Then
            '    lblError.Text = "Item description cannot be blank"
            '    Exit Sub
            'End If
            If h_ITM_ID.Value = "" Or Not IsNumeric(h_ITM_ID.Value) Then
                lblError.Text = "Please select an Item"
                Exit Sub
            End If
            If Not IsNumeric(txtItemCPrice.Text) Then
                lblError.Text = "Enter numbers only"
                Exit Sub
            End If
            If Not IsNumeric(txtItemSPrice.Text) Then
                lblError.Text = "Enter numbers only"
                Exit Sub
            End If

            Dim pParms(6) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@ITB_ID", h_ITB_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@ITB_ITM_ID", h_ITM_ID.Value, SqlDbType.Int)
            pParms(3) = Mainclass.CreateSqlParameter("@ITB_BSU_ID", txtBUID.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@ITB_COST", txtItemCPrice.Text, SqlDbType.Decimal)
            pParms(5) = Mainclass.CreateSqlParameter("@ITB_SELL", txtItemSPrice.Text, SqlDbType.Decimal)
            pParms(6) = Mainclass.CreateSqlParameter("@ITB_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "ItemPrice", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(1).Value
                    lblError.Text = "Data Saved Successfully !!!"
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    <WebMethod()> _
    Public Shared Function GetItem(ByVal BSUID As String, ByVal prefix As String) As String()

        Return clsInventory.GetItem(BSUID, prefix)

    End Function

    Protected Sub lbtnLoadItemDetail_Click(sender As Object, e As EventArgs)

    End Sub
    Protected Sub lbtnClearItem_Click(sender As Object, e As EventArgs) Handles lbtnClearItem.Click
        h_ITM_ID.Value = 0
        txtItem.Text = ""
    End Sub
    Protected Sub h_ITM_ID_ValueChanged(sender As Object, e As EventArgs) Handles h_ITM_ID.ValueChanged
        Dim ITM_ID As String = DirectCast(sender, HiddenField).Value
        ITM_ID = h_ITM_ID.Value
        Dim qry As String = "SELECT ISNULL(ITM_DESCR, '') ITM_DESCR, ISNULL(ITM_ISBN, '') ITM_ISBN FROM dbo.ITEM_SALE WITH (NOLOCK) WHERE ITM_ID = " & IIf(ITM_ID = "", 0, ITM_ID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, qry)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            txtItem.Text = ds.Tables(0).Rows(0)(0)
        End If
    End Sub
End Class
