﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="feereceiptSalesCategory.aspx.vb" Inherits="Fees_feereceiptSalesCategory" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Online Payment Receipt</title>
    <base target="_self" />
    <style type="text/css">
        img {
            display: block;
        }

        .ReceiptCaption {
            FONT-WEIGHT: bold;
            FONT-SIZE: 10pt;
            COLOR: #000000;
            FONT-FAMILY: Calibri;
            HEIGHT: 19px;
        }

        .matters_print {
            font-family: Calibri;
            font-size: 10px;
            font-weight: bold;
            color: #000000;
        }

        .matters_heading {
            font-family: Calibri;
            font-size: 13px;
            font-weight: bold;
            color: #000000;
        }

        .matters_normal {
            font-family: Calibri;
            font-size: 10px;
            color: #000000;
        }

        .matters_grid {
            font-family: Calibri;
            font-size: 10px;
            color: #000000;
            TEXT-INDENT: 20px;
        }

        .matters_small {
            font-family: Calibri;
            font-size: 8px;
            color: #000000;
        }

        .Printbg {
            vertical-align: middle;
            font-family: Calibri;
            font-size: 8px;
            color: #000000;
        }

        .PrintSource {
            vertical-align: bottom;
            font-family: Calibri;
            font-size: 8px;
            color: #000000;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function PrintReceipt() {
            document.getElementById('tr_Print').style.display = 'none';
            window.print();
            document.getElementById('tr_Print').style.display = 'inline';
        }
    </script>
</head>
<body onload="PrintReceipt();">
    <form id="form1" runat="server">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
            <tr valign="top" id="tr_Print">
                <td align="right">
                    <img alt="" src="../Images/print.gif" onclick="PrintReceipt();" style="cursor: hand" /></td>
            </tr>
            <tr valign="top">
                <td colspan="2" align="center" style="border-bottom: #8dc24c 2pt solid">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" rowspan="4" width="10%">
                                <asp:Image ID="imgLogo" runat="server" />
                            </td>
                            <td class="matters_heading">
                                <asp:Label ID="lblSchool" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters_print" style="height: 12px">
                                <asp:Label ID="lblHeader1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters_print">
                                <asp:Label ID="lblHeader2" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="matters_print">
                                <asp:Label ID="lblHeader3" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </td>
                <td align="center" colspan="1" style="height: 90px"></td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <table align="center">
                        <tr>
                            <td colspan="6" align="center" class="ReceiptCaption">
                                <asp:Label ID="lblCaption" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="6">
                                <!-- box starts here -->
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="16" valign="top">
                                            <%--<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Misc/rounded1.gif" Width="16px" Height="16px" />--%>
                                        </td>
                                        <td>
                                            <%--<asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Misc/back1.gif" Width="100%" Height="16px" />--%>
                                        </td>
                                        <td width="16" valign="top">
                                            <%--<asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Misc/rounded2.gif" Width="16px" Height="16px" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left" style="height: 70px">
                                            <%--<asp:Image ID="imgLogo1" runat="server"
                                                ImageUrl="~/Images/Misc/back4.gif" Width="16px" Height="85px" />--%>
                                        </td>
                                        <td>
                                            <!-- content starts here -->
                                            <table cellpadding="3" width="100%" border="0">
                                                <tr>
                                                    <td align="left" class="matters_normal" width="110">Receipt</td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_print">
                                                        <asp:Label ID="lblRecno" runat="server"></asp:Label></td>
                                                    <td align="left" class="matters_normal" width="50px">Date</td>
                                                    <td align="left" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_normal" width="70">
                                                        <asp:Label ID="lblDate" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="matters_normal">Student ID</td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_print">
                                                        <asp:Label ID="lblStudentNo" runat="server"></asp:Label></td>
                                                    <td align="left" class="matters_normal">
                                                        <asp:Label ID="lblGradeTitle" runat="server" Text="Grade"></asp:Label></td>
                                                    <td align="left" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_normal">
                                                        <asp:Label ID="lblGrade" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="matters_normal">Name</td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_print" colspan="4">
                                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="matters_normal">School</td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_print" colspan="4">
                                                        <asp:Label ID="lblSchoolName" runat="server"></asp:Label></td>
                                                </tr>
                                            </table>
                                            <!-- content ends here -->
                                        </td>
                                        <td style="height: 70px" valign="top">
                                            <%--<asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Misc/back2.gif"
                                                Width="16px" Height="85px" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Misc/rounded3.gif" Width="16px" Height="16px" />--%>
                                        </td>
                                        <td>
                                            <%--<asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Misc/back3.gif" Width="100%" Height="16px" />--%>
                                        </td>
                                        <td>
                                            <%--<asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Misc/rounded4.gif" Width="16px" Height="16px" />--%>
                                        </td>
                                    </tr>
                                </table>
                                <!-- box ends here -->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="matters_grid" colspan="6" height="120" valign="top">
                                <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" CellPadding="4" BorderColor="#000000" BorderWidth="1pt" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="SAD_DESCR" HeaderText="Charge Details">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SAD_QTY" HeaderText="Qty" DataFormatString="{0:0}">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SAD_TOTAL" HeaderText="Amount" DataFormatString="{0:0.00}">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr id="Tr2" runat="server">
                            <td align="right" colspan="6" class="matters_print">
                                <table>
                                    <tr id="Tr1" runat="server">
                                        <td>Sub Total</td>
                                        <td align="right"><asp:Label ID="lblBalance" runat="server" Text="0.00"></asp:Label></td>
                                    </tr>
                                    <tr id="TrVAT1" runat="server">
                                        <td>VAT Amount</td>
                                        <td align="right"><asp:Label ID="lblVATAmount" runat="server" Text="0.00"></asp:Label></td>
                                    </tr>
                                    <tr id="TrVAT2" runat="server">
                                        <td>NET Total</td>
                                        <td align="right"><asp:Label ID="lblNETAmount" runat="server" Text="0.00"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="ReceiptCaption">
                            <td align="left" class="matters_print">Payment Details</td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;" class="matters_normal">
                                <asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False" BorderColor="#000000"
                                    CellPadding="4" BorderWidth="1pt" Width="100%" ShowFooter="True">
                                    <Columns>
                                        <asp:BoundField DataField="CLT_DESCR" HeaderText="Payment Mode">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CRI_DESCR" HeaderText="Credit Card"></asp:BoundField>
                                        <asp:BoundField DataField="FCD_REFNO" HeaderText="Auth Code" />
                                        <asp:BoundField DataField="FCD_AMOUNT" DataFormatString="{0:###,###,###,##0.00}"
                                            HeaderText="Amount">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;" class="matters_normal" colspan="6">
                                <asp:Label ID="lblPaymentDetals" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="text-indent: 10px;"
                    class="matters_normal">
                    <asp:Label ID="lblSchoolName1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="text-indent: 30px;"
                    class="matters_normal">
                    <br />
                    <br />
                    ------------------------------</td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="text-indent: 60px; height: 20px"
                    class="matters_normal">Cashier/Accountant</td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="text-indent: 10px;"
                    class="matters_normal">Thank you for choosing &nbsp;<asp:Label ID="lblSchoolname2"
                        runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="text-indent: 10px;"
                    class="matters_normal">We value your patronage</td>
            </tr>
            <tr id="NetworkIntl" runat="server">
                <td align="center" class="Printbg" valign="middle">
                    <br />
                    This receipt is electronically generated and does not require any signature.<br />
                    This receipt is subject to Network International LLC crediting the amount to our
             account.<br />
                    <asp:Label ID="lblDiscount" Visible="false" Text="The discounted fee structure is applicable on the current fee structure of the School.<br />Any  revision in fee for the year if approved by the regulatory authority later, the difference shall be payable by the parent" runat="server"> </asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" valign="bottom" class="matters_normal">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-top: #003A63 1px solid; width: 100%; height: 100%">
                        <tr>
                            <td align="left" class="PrintSource" valign="top">
                                <asp:Label ID="lblPrintTime" runat="server" CssClass="matters_small"></asp:Label>
                            </td>
                            <td>
                                <td valign="top" align="right" class="PrintSource">
                                    <asp:Label ID="lblLogged" runat="server" CssClass="matters_small" Text="User : "></asp:Label>
                                    <asp:Label ID="lbluserloggedin" runat="server" CssClass="matters_small"></asp:Label></td>
                                <td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
