﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IFrame.aspx.vb" Inherits="FM_IFrame" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
        <script type="text/javascript">
        //<![CDATA[

        function HandleClose()
        {
            PageMethods.TestMethod(document.getElementById('<%=h_sessionId.ClientID %>').value);
        }

        //]]>   
    </script>
</head>
<body onunload="HandleClose();">
    <form id="form1" runat="server">
    <asp:HiddenField ID="h_sessionId" runat="server"/>
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
            <asp:HyperLink ID="lb" runat="server" Text="Download File" Visible="true"></asp:HyperLink>
            <iframe id="iFrame" runat="server" scrolling="auto">
            </iframe>   
    </div>
    </form>
</body>
</html>
