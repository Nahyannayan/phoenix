﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_reAgent
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'ts
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "PI02029" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        'txtCnb_RatePerKm.Attributes.Add("onkeypress", " return Numeric_Only();")

        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        txtContact.Enabled = EditAllowed
        txtagent.Enabled = EditAllowed
        txtDetails.Enabled = EditAllowed
        txtFax.Enabled = EditAllowed
        txtMobile.Enabled = EditAllowed
        txtOffice.Enabled = EditAllowed
        txtPayname.Enabled = EditAllowed
        txtActNo.Enabled = EditAllowed
        txtEmail.Enabled = EditAllowed
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        txtMainAgent.Enabled = EditAllowed
        imgAgent.Enabled = EditAllowed
        btnDelete.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select  A.REA_ID, A.REA_NAME, A.REA_CONTACT_PERSON, A.REA_MOBILE,A.REA_PHONE,A.REA_FAX,A.REA_PAYNAME,A.REA_DETAILS,A.REA_REA_ID,B.REA_NAME MainAgent,A.REA_ACTNO,A.REA_EMAIL  from REAGENT A left outer join REAGENT  B on B.REA_ID =A.REA_REA_ID ")
                strSql.Append("where A.REA_ID=" & h_EntryId.Value)
                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtagent.Text = dt.Rows(0)("REA_NAME")
                    txtContact.Text = dt.Rows(0)("REA_CONTACT_PERSON")
                    txtDetails.Text = dt.Rows(0)("REA_DETAILS")
                    txtFax.Text = dt.Rows(0)("REA_FAX")
                    txtMobile.Text = dt.Rows(0)("REA_MOBILE")
                    txtOffice.Text = dt.Rows(0)("REA_PHONE")
                    txtPayname.Text = dt.Rows(0)("REA_PAYNAME")
                    h_EntryId.Value = dt.Rows(0)("REA_ID")
                    h_REA_REA_ID.Value = dt.Rows(0)("REA_REA_ID")
                    txtMainAgent.Text = dt.Rows(0)("MainAgent")
                    txtActNo.Text = dt.Rows(0)("REA_ACTNO")
                    txtEmail.Text = dt.Rows(0)("REA_EMAIL")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ClearDetails()
        Dim MainAgentId As Integer = Mainclass.getDataValue("select REA_ID from REAGENT where REA_NAME ='OTHERS'", "OASIS_PUR_INVConnectionString")

        txtagent.Text = ""
        txtContact.Text = ""
        txtDetails.Text = ""
        txtFax.Text = ""
        txtMobile.Text = ""
        txtOffice.Text = ""
        txtPayname.Text = ""
        h_EntryId.Value = 0
        h_REA_REA_ID.Value = MainAgentId
        txtMainAgent.Text = "OTHERS"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM TCT_H WHERE TCH_REA_ID =" & h_EntryId.Value) > 0 Then
                lblError.Text = "Agent in use, unable to delete"
            Else
                Dim objConn As New SqlConnection(connectionString)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction

                Try
                    Dim pParms(2) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@REA_ID", h_EntryId.Value, SqlDbType.Int, True)
                    pParms(2) = Mainclass.CreateSqlParameter("@TABLE", "REAGENT", SqlDbType.VarChar)
                    Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "DeleteMasterData", pParms)

                    If RetVal = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    Else
                        ViewState("EntryId") = pParms(1).Value
                    End If

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                    End If

                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                    Exit Sub
                Finally
                    objConn.Close()
                End Try

                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                lblError.Text = "Record Deleted Successfully !!!"
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean

        If txtagent.Text.Trim.Length = 0 Then strMandatory.Append("Agent Name,")
        If txtOffice.Text = "" Then strMandatory.Append("Office Number,")
        If txtMainAgent.Text.Trim.Length = 0 Then strMandatory.Append("Main Agent Name,")
        If txtEmail.Text.ToString.Contains("@") = False Then strMandatory.Append("not a valid email id, ")



        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(12) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@REA_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@REA_NAME", txtagent.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@REA_CONTACT_PERSON", txtContact.Text, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@REA_MOBILE", txtMobile.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@REA_PHONE", txtOffice.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@REA_FAX", txtFax.Text, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@REA_PAYNAME", txtPayname.Text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@REA_DETAILS", txtDetails.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@REA_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@REA_REA_ID", h_REA_REA_ID.Value, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@REA_ACTNO", txtActNo.Text, SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@REA_EMAIL", txtEmail.Text, SqlDbType.VarChar)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveREAgent", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Function ValidTPTRates(ByVal txtCnd_Rate As String, ByVal txtCnd_Hrs As String, ByVal txtCnd_Km As String) As String
        ValidTPTRates = ""
        If txtCnd_Rate.Length = 0 Then ValidTPTRates &= "Rate"
        If txtCnd_Hrs.Length = 0 Then ValidTPTRates &= IIf(ValidTPTRates.Length = 0, "", ",") & "Qty"
        If txtCnd_Km.Length = 0 Then ValidTPTRates &= IIf(ValidTPTRates.Length = 0, "", ",") & "Km"
    End Function
End Class

