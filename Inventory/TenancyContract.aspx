<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="TenancyContract.aspx.vb" Inherits="Inventory_TenancyContract" Title="Tenancy Contract Form" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>


<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>   
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="/PHOENIXBETA/cssfiles/Popup.css" rel="stylesheet" />

    <style>
        .my-span {
            width: 125px;
            height: 125px;
            display: inline-block;
            vertical-align: top;
            padding: 4px;
            border: 2px solid rgba(0,0,0,0.3);
            margin-right: 2px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function showDocument(id, filename, contenttype) {
            var sFeatures;

            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";

            //result = window.showModalDialog("IFrameNew.aspx?id=" + id + "&path=Tenancy&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            return ShowWindowWithClose("IFrameNew.aspx?id=" + id + "&path=Tenancy&filename=" + filename + "&contenttype=" + contenttype, 'search', '55%', '85%')
            return false;

        }

        function calculateRent() {

            total = 0;
            if (typeof (wtgBP) != "undefined")
                for (cnt = 0; cnt < wtgBP.length; cnt++)
                    if (document.getElementById(wtgBP[cnt])) total = total + parseFloat(document.getElementById(wtgBP[cnt]).value);

            //alert(total);
            document.getElementById("<%=txtCharges.ClientID %>").value = total;
            document.getElementById("<%=txttotal.ClientID %>").value = parseFloat(document.getElementById("<%=txtCharges.ClientID %>").value) + parseFloat(document.getElementById("<%=txttrent.ClientID %>").value);

        }

        function getAgent() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            //pMode = "REAGENT"
            pMode = "REVENDOR"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=E";
            
            var oWnd = radopen(url, "pop_rel");


        }

        function getLandlord() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            //pMode = "RELANDLORD"
            pMode = "REVENDOR"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=E";
            
            var oWnd = radopen(url, "pop_landl");

        }


        function getArea() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            var BuildingName;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "REAREA"

            var e = document.getElementById("<%=ddlLocation.ClientID %>");
            if (e == undefined)
                var ara_id = 0;
            else
                var ara_id = e.options[e.selectedIndex].value;

            url = "../common/PopupSelect.aspx?id=" + pMode + "&cit_id=" + ara_id + "&jobtype=E";
            BuildingName = document.getElementById("<%=txtBuildingName.ClientID %>").value;
            var oWnd = radopen(url, "pop_area");
        }

        function getBuilding() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "REBUILDING"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=E";
            
            var oWnd = radopen(url, "pop_building");

        }

        function getEmployeeDetails() {
            sFeatures = "dialogWidth: 1150px; ";
            sFeatures += "dialogHeight: 565px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var OldEntryId;
            OldEntryId = document.getElementById('<%=h_Old_EntryID.ClientID %>').value;


            if (OldEntryId == 0)
                url = "TenancyContractEmployeeDetails.aspx?EntryId=" + document.getElementById('<%=h_EntryId.ClientID %>').value + "&bsuid=" + document.getElementById('<%=h_bsu_id.ClientID %>').value + "&OldEntryId=" + document.getElementById('<%=h_Old_EntryID.ClientID %>').value;
            else
                url = "TenancyContractEmployeeDetails.aspx?EntryId=" + document.getElementById('<%=h_Old_EntryID.ClientID %>').value + "&bsuid=" + document.getElementById('<%=h_bsu_id.ClientID %>').value + "&OldEntryId=" + document.getElementById('<%=h_Old_EntryID.ClientID %>').value;

            url = "TenancyContractEmployeeDetails.aspx?EntryId=" + document.getElementById('<%=h_EntryId.ClientID %>').value + "&bsuid=" + document.getElementById('<%=h_bsu_id.ClientID %>').value + "&datamode=" + document.getElementById('<%=hViewState.ClientID %>').value;

            //result = window.showModalDialog(url, "", sFeatures);
            document.getElementById("<%=hCalculate.ClientID %>").value = 1;
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            return ShowWindowWithClose(url, 'search', '90%', '85%')
            return false;


        }

        function TabChanged(sender, args) {
            sender.get_clientStateField().value =
                sender.saveClientState();
        }

        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
                1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode[1]);                   
                <%--if (document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>')).value == "0" || document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>')).value == "") document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>')).value = "1";
                if (document.getElementById(document.getElementById('<%=hf_txtSAD_COST.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtSAD_COST.ClientID%>').value).value = NameandCode[4];
                if (document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value).value = NameandCode[3];
                if (document.getElementById(document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value).value = NameandCode[2];
                if (document.getElementById(document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtSAD_DETAILS.ClientID%>').value).value = NameandCode[1];
                if (document.getElementById(document.getElementById('<%=hf_ITM_ID.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_ITM_ID.ClientID%>').value).value = NameandCode[0];
                if (document.getElementById(document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>').value).value = eval(document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value).value) * eval(document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value).value);
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;--%>

                //NameandCode = result.split('___');
                document.getElementById("<%=h_REAId.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtAgent.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtCPerson.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=txtCNumber.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }

        }

        function OnlandlClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode[0]);
                document.getElementById("<%=h_RELId.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtLandlord.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtLLCPerson.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=txtLLCNo.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=h_PayName.ClientID %>").value = NameandCode[4];
                

                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;

            }

        }
        function OnbuildingClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode[0]);
                document.getElementById("<%=h_building_id.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtBuildingName.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtArea.ClientID %>").value = NameandCode[4];
                document.getElementById("<%=h_area_id.ClientID %>").value = NameandCode[5];

                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }

        }

        function OnareaClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode[0]);
                document.getElementById("<%=h_area_id.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtArea.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }

        }

        //general close frame 
        function setCloseFrame(msg) {
            CloseFrame();
            $("#<%=h_MSG2.ClientID%>").val(msg);
            __doPostBack('<%=h_MSG2.ClientID%>', "");
        }

        function CloseFrame() {
            jQuery.fancybox.close();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_landl" runat="server" Behaviors="Close,Move" OnClientClose="OnlandlClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_building" runat="server" Behaviors="Close,Move" OnClientClose="OnbuildingClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_area" runat="server" Behaviors="Close,Move" OnClientClose="OnareaClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="pgTitle" Text="" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <ajaxToolkit:ModalPopupExtender ID="Panel1_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="btnTCNew" PopupControlID="Panel3" TargetControlID="Label5">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel3" runat="server" Width="30%"
                    CssClass="panel-cover" Style="display: none">
                    <div>
                        <div id="PopupHeader" class="text-center"><span class="field-label">Select Tenancy Contract Type</span></div>
                        <div class="text-center">
                            <br />
                            <br />
                            <asp:Button ID="btnTCNew" runat="server" CausesValidation="true" CssClass="button" Text="New" />
                            <asp:Button ID="btnTCReNew" runat="server" CausesValidation="true" CssClass="button" Text="Renew" />
                        </div>
                    </div>
                </asp:Panel>
                <div>
                    <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr valign="bottom">
                            <td align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                <asp:Label ID="Label5" runat="server" Style="text-align: right" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">

                                    <tr>
                                        <td align="left" width="20%">
                                            <asp:Label ID="lblPRFNo" runat="server" Text="Requisition No" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtTCH_No" runat="server" Enabled="False"></asp:TextBox>
                                        </td>

                                        <td align="left" width="20%">
                                            <asp:Label ID="Label3" runat="server" Text="Requisition Date" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtContractDate" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:ImageButton ID="imgCondate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server"
                                                TargetControlID="txtContractDate" PopupButtonID="imgCondate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label1" runat="server" Text="Start Date" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtContractSDate" runat="server" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton Visible="true" ID="imgSDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                TargetControlID="txtContractSDate" PopupButtonID="imgSDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td align="left"><span class="field-label">End Date</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtContractEDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton Visible="true" ID="imgeEDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                                TargetControlID="txtContractEDate" PopupButtonID="imgeEDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Location</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlLocation" AutoPostBack="True" runat="server"></asp:DropDownList>
                                        </td>
                                        <td align="left"><span class="field-label">Company  Info</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlGCoInfo" runat="server"></asp:DropDownList>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="field-label">Employee Details</span><span class="text-danger">*</span></td>
                                        <td align="left" colspan="2">
                                            <asp:Label ID="lblEmpDetails" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblEmpCount" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnEmpDetails" runat="server" CssClass="button" Text="Select Staff Details" OnClientClick=" getEmployeeDetails(); return false;" CausesValidation="False" />
                                        </td>
                                    </tr>
                                    <tr id="trRentalInc" runat="server" visible="false">
                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td align="left" width="10%"><span class="field-label">Old Requisition No</span></td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtOldContractNo" runat="server"></asp:TextBox><br />

                                                    </td>

                                                    <td align="left" width="12%"><span class="field-label">Flat Hired Since (Yrs) <span class="text-danger">*</span></span></td>


                                                    <td align="left" width="23%">
                                                        <asp:TextBox ID="txtHiredSince" runat="server" Style="text-align: right"></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:CheckBox ID="chkIncAsperRera" runat="server" Text="Inc. as per Rera" CssClass="field-label"
                                                            TextAlign="Left" />

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><span class="field-label">Old Rent</span></td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtRevrent" runat="server" Enabled="false" Style="text-align: right" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left"><span class="field-label">Increase/Decrease</span></td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtPerc" runat="server" ReadOnly="true"></asp:TextBox>

                                                    </td>

                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    <tr id="Tr2" runat="server">
                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td align="left" width="10%"><span class="field-label">Real Estate Agent</span><span class="text-danger">*</span>
                                                    </td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtAgent" runat="server" Enabled="false"></asp:TextBox>
                                                        <asp:ImageButton ID="ImgREAgent" runat="server" OnClientClick="getAgent();return false;"
                                                            ImageUrl="~/Images/forum_search.gif"  /></td>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="lblCPerson" runat="server" Text="Contact Person" CssClass="field-label"></asp:Label></td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtCPerson" runat="server" Enabled="false"></asp:TextBox></td>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="lblCNumber" runat="server" Text="Contact No" CssClass="field-label"></asp:Label></td>
                                                    <td align="left" width="20%">
                                                        <asp:TextBox ID="txtCNumber" runat="server" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="Tr5" runat="server">
                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>

                                                    <td align="left" width="10%"><span class="field-label">Landlord</span><span class="text-danger">*</span></td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtLandlord" runat="server" Enabled="false"></asp:TextBox>
                                                        <asp:ImageButton ID="ImgLandLord" runat="server" OnClientClick="getLandlord();return false;"
                                                            ImageUrl="~/Images/forum_search.gif" /></td>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="Label6" runat="server" Text="Contact Person" CssClass="field-label"></asp:Label></td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtLLCPerson" runat="server" Enabled="false"></asp:TextBox></td>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="Label7" runat="server" Text="Contact No" CssClass="field-label"></asp:Label></td>
                                                    <td align="left" width="20%">
                                                        <asp:TextBox ID="txtLLCNo" runat="server" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="Tr6" runat="server">
                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td align="left" width="10%"><span class="field-label">Building Details</span><span class="text-danger">*</span>
                                                    </td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtBuildingName" runat="server" Enabled="TRUE"></asp:TextBox>
                                                        <asp:ImageButton ID="imgBuilding" runat="server" OnClientClick="getBuilding();return false;"
                                                            ImageUrl="~/Images/forum_search.gif"  Visible="False" />
                                                    </td>
                                                    <td align="left" width="10%"><span class="field-label">Area</span><span class="text-danger">*</span></td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtArea" runat="server" Enabled="false"></asp:TextBox>
                                                        <asp:ImageButton ID="imgArea" runat="server" OnClientClick="getArea();return false;"
                                                            ImageUrl="~/Images/forum_search.gif"  />
                                                    </td>
                                                    <td align="left" width="10%"><span class="field-label">Unit</span><span class="text-danger">*</span></td>
                                                    <td align="left" width="20%">
                                                        <asp:TextBox ID="txtBuilding" runat="server" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td align="left" width="10%"><span class="field-label">Budgeted Year</span></td>
                                                    <td align="left" width="25%">
                                                        <asp:DropDownList ID="ddlBudget" AutoPostBack="True" runat="server"></asp:DropDownList>
                                                    </td>
                                                    <td align="left" width="10%"><span class="field-label">Budgeted</span></td>
                                                    <td align="left" width="25%">
                                                        <asp:RadioButton ID="radBudgeted" runat="server" GroupName="Budget" Text="Yes" Checked="true" Enabled="False" />
                                                        <asp:RadioButton ID="radUnBudgeted" runat="server" GroupName="Budget" Checked="false" Text="No" Enabled="false" />
                                                    </td>
                                                    <td align="left" width="10%"><span class="field-label">TRF History</span></td>
                                                    <td align="left" width="20%">
                                                        <asp:LinkButton ID="lnkTRFHISTORY" runat="server" Text="Show History" OnClientClick="return showOLDTRF()" CssClass="button medium"></asp:LinkButton>
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td align="left" width="10%"><span class="field-label">Notice Period</span></td>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtNoticePeriod" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="10%"><span class="field-label">House Waiver</span></td>
                                                    <td align="left" width="25%">
                                                        <asp:LinkButton ID="lnkTRFHWHISTORY" runat="server" Text="Show House Waiver History" OnClientClick="return showExcessRentApprovalList()" CssClass="button medium"></asp:LinkButton>
                                                    </td>
                                                    <td align="left" width="10%"></td>
                                                    <td align="left" width="20%">
                                                        
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>






                                    <tr id="ContractTermination" runat="server" visible="false">
                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>

                                                    <td align="left" colspan="4"><span class="field-label">Contract Termination</span><span class="text-danger">*</span></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%">
                                                        <asp:Label ID="Label8" runat="server" Text="Termination Date" CssClass="field-label"></asp:Label>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtTermDate" runat="server" Enabled="false" Width="90px" CssClass="inputbox"></asp:TextBox>
                                                        <asp:ImageButton ID="imgTdate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                            Style="cursor: hand"></asp:ImageButton>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" Format="dd/MMM/yyyy" runat="server"
                                                            TargetControlID="txtTermDate" PopupButtonID="imgTdate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </td>
                                                    <td align="left" width="20%">
                                                        <asp:Label ID="Label9" runat="server" Text="Remarks" CssClass="field-label"></asp:Label>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtTermRemarks" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="tr3" runat="server" class="title-bg-lite">
                                        <td align="center" colspan="4" valign="middle">Other  Charges
                                        </td>
                                    </tr>
                                    <tr id="rptServices" runat="server">
                                        <td colspan="4" valign="middle">
                                            <asp:DataList ID="RepService" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                                <ItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="23%">
                                                                <asp:Label ID="lblRESDescr" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                                <asp:Label ID="lblRESID" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("ID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblRowId" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("RowId") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblPerc" CssClass="field-label" runat="server" Style="text-align: right" Text='<%# Bind("PERC") %>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td width="10%">
                                                                <asp:TextBox ID="txtRESAmount" runat="server" Style="text-align: right" Text='<%# Bind("AMOUNT") %>'></asp:TextBox>
                                                            </td>
                                                            <%--<td width="3%"></td>--%>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </td>
                                    </tr>
                                    <tr id="Tr4" runat="server">

                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>

                                                    <td align="left" colspan="6" class="title-bg-lite"><span class="field-label">Lease Values</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="lblrent" runat="server" Text="Rent" CssClass="field-label"></asp:Label>
                                                    </td>

                                                    <%--OnTextChanged="txttrent_TextChanged"--%>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txttrent" AutoPostBack="true" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>

                                                    <td align="left" width="10%">
                                                        <asp:Label ID="lblCharges" runat="server" Text="Charges" CssClass="field-label"></asp:Label>
                                                    </td>

                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtCharges" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="Label4" runat="server" Text="Total" CssClass="field-label"></asp:Label>
                                                    </td>

                                                    <td align="left" width="20%">
                                                        <asp:TextBox ID="txttotal" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td align="left" colspan="6" class="title-bg-lite"><span class="field-label">Entitlement</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="Label10" runat="server" Text="Rent" CssClass="field-label"></asp:Label></td>

                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtERent" runat="server" Style="text-align: right" Enabled="false"> </asp:TextBox></td>

                                                    <td align="left" width="10%">
                                                        <asp:Label ID="Label11" runat="server" Text="Deduction" CssClass="field-label"></asp:Label></td>

                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtEDed" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="Label14" runat="server" Text="Other Deduction" CssClass="field-label"></asp:Label></td>
                                                    <td align="left" width="20%">
                                                        <asp:TextBox ID="txtOtherDed" runat="server" Style="text-align: right"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                   
                                    <tr id="trChangeRequest1" runat="server" class="subheader_img">
                                        <td align="center" colspan="4">
                                            <asp:LinkButton ID="LnkSearch" runat="server" OnClientClick="javascript:return false;">Change Request</asp:LinkButton>
                                        </td>
                                    </tr>

                                    <tr id="trChangeRequest2" runat="server">
                                        <td colspan="4">
                                            <asp:Panel runat="server" ID="pnlInvoice">
                                                <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" colspan="6" class="title-bg-lite"><span class="field-label">Approval for Recovery Process Details</span>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td width="20%">
                                                            <asp:Label ID="lblRSup_id" runat="server" Text="Recovery process from staff for any deduction" CssClass="field-label"></asp:Label>
                                                        </td>
                                                        <td align="left" width="80%">
                                                            <asp:TextBox ID="txtRecovery" runat="server" Width="100%"
                                                                TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>




                                                    <%--Staff Recover Start--%>

                                                    <tr id="tr1" runat="server">
                                                        <td align="left" colspan="2">


                                                            <table width="100%" border="0">
                                                                <tr>

                                                                    <td align="left" width="20%">
                                                                        <asp:Label ID="Label15" runat="server" Text="Recovery Start Date" CssClass="field-label"></asp:Label>
                                                                    </td>

                                                                    <td align="left" width="80%" colspan="5">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtRecPSDate" runat="server"></asp:TextBox>
                                                                                    <asp:ImageButton Visible="true" ID="imgRSDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                                                        Style="cursor: hand"></asp:ImageButton>
                                                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender7" Format="dd/MMM/yyyy" runat="server"
                                                                                        TargetControlID="txtRecPSDate" PopupButtonID="imgRSDate">
                                                                                    </ajaxToolkit:CalendarExtender>
                                                                                </td>

                                                                                <td>
                                                                                    <asp:Label ID="Label16" runat="server" Text="No of Payments" CssClass="field-label"></asp:Label></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtRecnoofPayments" runat="server"></asp:TextBox></td>
                                                                                <td>
                                                                                    <asp:Button ID="btnRecoverypayment" runat="server" CausesValidation="False" Visible="TRUE" CssClass="button" Text="Create Recovery Payments" /></td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>


                                                                </tr>

                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4" align="center">
                                                            <asp:GridView ID="GrdRec" runat="server" AutoGenerateColumns="False" PageSize="5"
                                                                Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                                                DataKeyNames="ID">
                                                                <Columns>
                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTCP_ID" runat="server" Text='<%# Bind("TCR_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTCP_RES_ID" runat="server" Text='<%# Bind("TCR_RES_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Description">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblITM_DESCR" runat="server" Text='<%# Bind("RES_DESCR") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:DropDownList ID="ddlPaymentType" AutoPostBack="True" runat="server"></asp:DropDownList>
                                                                            <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("TCR_RES_ID") %>' runat="server" />
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:DropDownList ID="ddlPaymentType" AutoPostBack="True" runat="server"></asp:DropDownList>
                                                                            <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("TCR_RES_ID") %>' runat="server" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Chq Date">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblChqDate" runat="server" Style="text-align: right"
                                                                                Text='<%#Eval("TCR_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtRChqDate" Enabled="false" Width="60px" runat="server" Style="text-align: right"
                                                                                Text='<%#Eval("TCR_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                                            <asp:ImageButton ID="imgEChqDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                                                TargetControlID="txtRChqDate" PopupButtonID="imgEChqDate">
                                                                            </ajaxToolkit:CalendarExtender>
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtRChqDate" runat="server" Width="60px" Enabled="false" Style="text-align: right"
                                                                                Text='<%#Eval("TCR_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                                            <asp:ImageButton ID="imgFChqDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                                                TargetControlID="txtRChqDate" PopupButtonID="imgFChqDate">
                                                                            </ajaxToolkit:CalendarExtender>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAmount" runat="server" Style="text-align: right"
                                                                                Text='<%# Bind("TCR_AMOUNT") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtAMOUNT" runat="server" Width="60px" Style="text-align: right"
                                                                                Text='<%# Bind("TCR_AMOUNT") %>'></asp:TextBox>
                                                                            <span class="text-danger">*</span>
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtAMOUNT" runat="server" Width="60px" Style="text-align: right"
                                                                                Text='<%# Bind("TCR_AMOUNT") %>'></asp:TextBox>
                                                                            <span class="text-danger">*</span>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit"
                                                                                Text="Edit"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update"
                                                                                Text="Update"></asp:LinkButton>
                                                                            <asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                                Text="Cancel"></asp:LinkButton>
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                                Text="Add New"></asp:LinkButton>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>

                                                    <%--Staff Recovery End--%>














                                                    <tr>
                                                        <td width="20%">
                                                            <asp:Label ID="Label2" runat="server" Text="Contract Terms" CssClass="field-label"></asp:Label>
                                                        </td>
                                                        <td align="left" width="80%">
                                                            <asp:TextBox ID="txtTerms" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <asp:Label ID="Label12" runat="server" Text="Employee Details" CssClass="field-label"></asp:Label>
                                                        </td>
                                                        <td align="left" width="80%">
                                                            <asp:TextBox ID="txtEmpDetails" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trVisited">
                                                        <td colspan="2">I,
                                                            <asp:Label ID="lblMSO" runat="server"></asp:Label>, confirm that this apartment has been visually inspected on 
                        <asp:TextBox ID="txtVisitDate" runat="server" Enabled="false"></asp:TextBox>
                                                            <asp:ImageButton ID="imgVisitDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                                Style="cursor: hand"></asp:ImageButton>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" Format="dd/MMM/yyyy" runat="server"
                                                                TargetControlID="txtVisitDate" PopupButtonID="imgVisitDate">
                                                            </ajaxToolkit:CalendarExtender>
                                                            by
                                                            <asp:TextBox ID="txtVisitor" runat="server"></asp:TextBox>
                                                            (name of the visitor if not MSO) 
                                 and confirm the apartment and surroundings seem suitable for our staffs. 
                                 Defect arising only on long term and/or with the utilities connection in place could not be checked at time of the visit.  
                                                        </td>
                                                    </tr>

                                                    <tr id="trCalcRent" runat="server">
                                                        <td align="left" colspan="2">


                                                            <table width="100%" border="0">

                                                                <tr>
                                                                    <td align="left" colspan="6" class="title-bg-lite"><span class="field-label">Payment Details</span>
                                                                    </td>

                                                                    <tr>

                                                                        <td align="left" width="20%">
                                                                            <asp:Label ID="lblPSDate" runat="server" Text="Payment Start Date" CssClass="field-label"></asp:Label>
                                                                        </td>
                                                                        <td align="left" width="80%" colspan="5">
                                                                            <table>
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:TextBox ID="txtPSDate" runat="server"></asp:TextBox>
                                                                                        <asp:ImageButton Visible="true" ID="imgPSDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                                                            Style="cursor: hand"></asp:ImageButton>
                                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender6" Format="dd/MMM/yyyy" runat="server"
                                                                                            TargetControlID="txtPSDate" PopupButtonID="imgPSDate">
                                                                                        </ajaxToolkit:CalendarExtender>
                                                                                    </td>

                                                                                    <td align="left">
                                                                                        <asp:Label ID="lblNoOfPayment" runat="server" Text="No of Payments" CssClass="field-label"></asp:Label>
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <asp:TextBox ID="txtnoofPayments" runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:Button ID="btnCreatePayments" runat="server" CausesValidation="False" Visible="TRUE" CssClass="button" Text="CreatePayments" />
                                                                                    </td>

                                                                                </tr>

                                                                            </table>
                                                                        </td>


                                                                    </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4" align="center">
                                                            <asp:GridView ID="grdPayment" runat="server" AutoGenerateColumns="False" PageSize="5"
                                                                Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                                                DataKeyNames="ID">
                                                                <Columns>
                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTCP_ID" runat="server" Text='<%# Bind("TCP_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTCP_RES_ID" runat="server" Text='<%# Bind("TCP_RES_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Description">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblITM_DESCR" runat="server" Text='<%# Bind("RES_DESCR") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:DropDownList ID="ddlPaymentType" AutoPostBack="True" runat="server"></asp:DropDownList>
                                                                            <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("TCP_RES_ID") %>' runat="server" />
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:DropDownList ID="ddlPaymentType" AutoPostBack="True"  runat="server"></asp:DropDownList>
                                                                            <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("TCP_RES_ID") %>' runat="server" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Type">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPType" runat="server" Style="text-align: left"
                                                                                Text='<%# Bind("TCP_PAYMENTTYPE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:DropDownList ID="ddlPType" runat="server"></asp:DropDownList>
                                                                            <asp:HiddenField ID="hdnITM_TYPE" Value='<%# Bind("TCP_PAYMENTTYPE") %>' runat="server" />
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:DropDownList ID="ddlPType" runat="server"></asp:DropDownList>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Chq Date">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblChqDate" runat="server" Style="text-align: right"
                                                                                Text='<%#Eval("TCP_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                                                                            <%--  <asp:ImageButton ID="imgChqDate" runat="server" ImageUrl="~/Images/calendar.gif" />--%>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtChqDate" Enabled="false" runat="server" Style="text-align: right"
                                                                                Text='<%#Eval("TCP_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                                            <asp:ImageButton ID="imgEChqDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                                                TargetControlID="txtChqDate" PopupButtonID="imgEChqDate">
                                                                            </ajaxToolkit:CalendarExtender>
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtChqDate" runat="server" Enabled="false" Style="text-align: right"
                                                                                Text='<%#Eval("TCP_CHQDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                                            <asp:ImageButton ID="imgFChqDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                                                TargetControlID="txtChqDate" PopupButtonID="imgFChqDate">
                                                                            </ajaxToolkit:CalendarExtender>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAmount" runat="server" Style="text-align: right"
                                                                                Text='<%# Bind("TCP_AMOUNT") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtAMOUNT" runat="server" Style="text-align: right"
                                                                                Text='<%# Bind("TCP_AMOUNT") %>'></asp:TextBox>
                                                                            <span class="text-danger">*</span>
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtAMOUNT" runat="server" Style="text-align: right"
                                                                                Text='<%# Bind("TCP_AMOUNT") %>'></asp:TextBox>
                                                                            <span class="text-danger">*</span>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Payable To">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPayable" runat="server" Style="text-align: left"
                                                                                Text='<%# Bind("TCP_PAYABLETO") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtPayable" runat="server" Style="text-align: left"
                                                                                Text='<%# Bind("TCP_PAYABLETO") %>'></asp:TextBox>
                                                                            <span class="text-danger">*</span>
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtPayable" runat="server" Style="text-align: left"
                                                                                Text='<%# Bind("TCP_PAYABLETO") %>'></asp:TextBox>
                                                                            <span class="text-danger">*</span>
                                                                            <asp:DropDownList ID="ddlMultiplePaymentList" Visible ="false"  runat="server"></asp:DropDownList>

                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit"
                                                                                Text="Edit"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update"
                                                                                Text="Update"></asp:LinkButton>
                                                                            <asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                                Text="Cancel"></asp:LinkButton>
                                                                        </EditItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                                Text="Add New"></asp:LinkButton>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                CollapseControlID="LnkSearch" CollapsedSize="0" CollapsedText="Show Other Details"
                                                ExpandControlID="LnkSearch" ExpandedText="Hide Other Details" TargetControlID="pnlInvoice"
                                                TextLabelID="LnkSearch" Collapsed="True">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td align="left" colspan="4">
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="TabChanged" AutoPostBack="true">
                                                <ajaxToolkit:TabPanel ID="trFlow" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel2" runat="server" ScrollBars="Vertical">
                                                            <asp:Repeater runat="server" ID="rptFlow">
                                                                <ItemTemplate>
                                                                    <span class="my-span" style="background-color: <%#DataBinder.Eval(Container.DataItem, "wfcolor")%>;"><%#DataBinder.Eval(Container.DataItem, "wfdescr")%></span>
                                                                </ItemTemplate>
                                                            </asp:Repeater>

                                                        </asp:Panel>


                                                    </ContentTemplate>

                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trComments" runat="server" HeaderText="Comments">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtComments" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>

                                                    </ContentTemplate>

                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trDocument" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <table width="100%">

                                                            <tr>

                                                                <td align="left" width="20%">
                                                                    <asp:Label ID="lblDocType" runat="server" Text="Document Type" CssClass="field-label"></asp:Label>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlDocType" runat="server" Style="width: 25% !important"></asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%">
                                                                    <asp:FileUpload ID="UploadDocPhoto" runat="server"
                                                                        ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />
                                                                </td>
                                                            </tr>


                                                        </table>




                                                        <asp:Repeater ID="rptImages" runat="server">
                                                            <ItemTemplate>
                                                                [
                                       <asp:TextBox ID="lblPriContentType" runat="server" Visible="false" Text='<%# Bind("tci_contenttype") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriFilename" runat="server" Visible="false" Text='<%# Bind("tci_filename") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriId" runat="server" Visible="false" Text='<%# Bind("tci_id") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="btnDocumentLink" Text='<%# DataBinder.Eval(Container.DataItem, "tci_name") %>' runat="server"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnDelete" Text='[x]' Visible='<%# DataBinder.Eval(Container.DataItem, "tci_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                                                ]
                                                            </ItemTemplate>
                                                        </asp:Repeater>

                                                    </ContentTemplate>

                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trUpload" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <table width="100%">

                                                            <tr>

                                                                <td align="left" width="20%">
                                                                    <asp:Label ID="Label13" runat="server" CssClass="field-label" Text="Document Type "></asp:Label>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlUploadType" runat="server" Style="width: 25% !important;"></asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%">
                                                                    <asp:FileUpload ID="UploadDocument" runat="server"
                                                                        ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:Button ID="btnUpload1" runat="server" CssClass="button" Text="Upload" />
                                                                </td>
                                                            </tr>


                                                        </table>






                                                        <asp:Repeater ID="rptDocument" runat="server">
                                                            <ItemTemplate>
                                                                [
                                       <asp:TextBox ID="lblPriDocumentContentType" runat="server" Visible="false" Text='<%# Bind("tci_contenttype") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriDocumentFilename" runat="server" Visible="false" Text='<%# Bind("tci_filename") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriDocumentId" runat="server" Visible="false" Text='<%# Bind("tci_id") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="btnDocumentLink1" Text='<%# DataBinder.Eval(Container.DataItem, "tci_name") %>' runat="server"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnDelete1" Text='[x]' Visible='<%# DataBinder.Eval(Container.DataItem, "tci_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                                                ]
                                                            </ItemTemplate>
                                                        </asp:Repeater>

                                                    </ContentTemplate>

                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trApproval" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtApprovals" runat="server" TextMode="MultiLine"></asp:TextBox>

                                                    </ContentTemplate>

                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trWorkflow" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical">
                                                            <asp:GridView ID="gvWorkflow" runat="server" CssClass="table table-bordered table-row"
                                                                EmptyDataText="No Data" Width="98%" AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <asp:BoundField DataField="wrk_Date" HeaderText="Date" />
                                                                    <asp:BoundField DataField="wrk_User" HeaderText="User" />
                                                                    <asp:BoundField DataField="wrk_Action" HeaderText="Action" />
                                                                    <asp:BoundField DataField="wrk_Details" HeaderText="Details" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>

                                                    </ContentTemplate>

                                                </ajaxToolkit:TabPanel>


                                            </ajaxToolkit:TabContainer>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnConfirm" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Confirm" Visible="false" OnClientClick="return confirm('Are you sure you want to Confirm This Supplier ?');" />
                                            <input type="button" id="btnAccept" class="button" value=" Approve " runat="server" onclick="this.disabled = true;" onserverclick="btnAccept_Click" />
                                            <asp:Button ID="btnReject" runat="server" CausesValidation="False" CssClass="button" Text="Reject" />
                                            <input type="button" id="btnSend" class="button" value=" Send " runat="server" onclick="this.disabled = true;" onserverclick="btnSend_Click" />
                                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                            <input type="button" id="btnSave" class="button" value="  Save  " runat="server" onclick="this.disabled = true;" onserverclick="btnSave_Click" />
                                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                                            <asp:Button ID="btnMessage" runat="server" CausesValidation="False" CssClass="button" Text="Message" />
                                            <asp:Button ID="btnRenew" runat="server" CausesValidation="False" Visible="false" CssClass="button" Text="Renew" />
                                            <asp:Button ID="btnRecall" runat="server" CausesValidation="False" CssClass="button" Text="Recall" />
                                            <input type="button" id="btnReturn" class="button" value=" Return " runat="server" onclick="this.disabled = true;" onserverclick="btnReturn_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_Old_EntryID" runat="server" Value="0" />

                                            <asp:HiddenField ID="h_UnitTypeId" runat="server" />
                                            <asp:HiddenField ID="h_Loc_id" runat="server" />
                                            <asp:HiddenField ID="h_empID" runat="server" />
                                            <asp:HiddenField ID="h_emp_ids" runat="server" />
                                            <asp:HiddenField ID="h_linkingId" runat="server" />
                                            <asp:HiddenField ID="h_linkingIds" runat="server" />
                                            <asp:HiddenField ID="h_REAId" runat="server" />
                                            <asp:HiddenField ID="h_RELId" runat="server" />
                                            <asp:HiddenField ID="h_total" runat="server" />
                                            <asp:HiddenField ID="h_ItemID" runat="server" EnableViewState="False" />
                                            <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_printed" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_Tch_apr_id" Value="0" runat="server" />
                                            <asp:HiddenField ID="h_TCH_Type" runat="server" />
                                            <asp:HiddenField ID="h_TCH_Status" runat="server" />
                                            <asp:HiddenField ID="hGridDelete" runat="server" />
                                            <asp:HiddenField ID="h_building_ID" runat="server" />
                                            <asp:HiddenField ID="h_area_ID" runat="server" />
                                            <asp:HiddenField ID="h_bsu_id" runat="server" />
                                            <asp:HiddenField ID="h_fyear" runat="server" />
                                            <asp:FileUpload ID="UploadFile" runat="server" Width="250px" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB'
                                                Visible="false" />
                                            <asp:Button ID="btnUploadPrf" runat="server" CssClass="button" Visible="false" Text="Upload" />
                                            <asp:HyperLink ID="btnExport" runat="server" Visible="false">Export to Excel</asp:HyperLink>
                                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                            <asp:HiddenField ID="hCalculate" Value="0" runat="server" />
                                            <asp:HiddenField ID="hViewState" Value="" runat="server" />
                                            <asp:HiddenField ID="h_EmpCount" Value="0" runat="server" />
                                            <asp:HiddenField ID="h_PayName" Value="" runat="server" />
                                            <asp:HiddenField ID="h_enableMutliPayName" Value="0" runat="server" />

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>
<%--    <div id="divHistory" runat="server" title="Old TRF History" class="panel-cover" visible="false" style=" background-color:darkseagreen ; position: absolute; z-index: 100000; left: 200px;top: 500px;" clientidmode="Static">
            <asp:GridView ID="GrdHistory" runat="server" AutoGenerateColumns="true" PageSize="5"
                                    Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                   DataKeyNames="ID">
                                </asp:GridView>
        </div>--%>

  

  
    <script type="text/javascript" lang="javascript">


   function showOLDTRF() {
                var EntryId = document.getElementById('<%=h_EntryId.ClientID %>').value;
                $.fancybox({
                    type: 'iframe',
                    href: 'ShowOldTRFHistory.aspx?viewid=' + EntryId,
                    fitToView: false,
                    width: '100%',
                    height: '70%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    'onClosed': function () {
                        $.fancybox.close();
                    }
                })
                ; return false;
        }


        function showExcessRentApprovalList() {
                var EntryId = document.getElementById('<%=h_EntryId.ClientID %>').value;
                $.fancybox({
                    type: 'iframe',
                    href: 'ShowExcessApprovalHistory.aspx?viewid=' + EntryId,
                    fitToView: false,
                    width: '100%',
                    height: '70%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    'onClosed': function () {
                        $.fancybox.close();
                    }
                })
                ; return false;
            }


        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

    <asp:HiddenField ID="h_MSG2" runat="server" EnableViewState="true" />


</asp:Content>
