﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="SalesRequest.aspx.vb" Inherits="Inventory_SalesRequest" Title="Retail Sales Form" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .darkPanlAlumini {
            background: rgb(235, 228, 228) !important;
        }

        .completionListElement {
            width: 60% !important;
            margin: auto;
            list-style: none;
            left: 0px !important;
            /*width: 350px !important;*/
        }

            .completionListElement .listitem {
                cursor: pointer;
            }

            .completionListElement .highlightedListItem {
                background-color: rgba(0,0,0,0.2);
            }
    </style>


    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
    function CheckForPrint() {
        var ddlBSU = document.getElementById('<%=ddlBSU.ClientID%>');
        var ddlBSUSel = ddlBSU.options[ddlBSU.selectedIndex].value;
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            if (ddlBSUSel == '125011') {//WIN-THE WINCHESTER SCHOOL - DUBAI
                //window.showModalDialog('FeeReceiptSalesCategory.aspx?salid=' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");

                if (isIE()) {
                    window.showModalDialog('FeeReceiptSalesCategory.aspx?salid=' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                } else {
                    return ShowWindowWithClose('FeeReceiptSalesCategory.aspx?salid=' + document.getElementById('<%= h_print.ClientID %>').value, 'search', '55%', '85%');
                    return false;
                }

            } else {
                //window.showModalDialog('FeeReceiptSales.aspx?salid=' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");                
                if (isIE()) {
                    window.showModalDialog('FeeReceiptSales.aspx?salid=' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                } else {
                    return ShowWindowWithClose('FeeReceiptSales.aspx?salid=' + document.getElementById('<%= h_print.ClientID %>').value, 'search', '55%', '85%');
                    return false;
                }

            } document.getElementById('<%= h_print.ClientID %>').value = '';
        }
    }
    );

    function isIE() {
        ua = navigator.userAgent;
        /* MSIE used to detect old browsers and Trident used to newer ones*/
        var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

        return is_ie;
    }

    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });
            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
    function UpdateSum() {
        var txtCCTotal, txtCashTotal, txtTotal, txtReturn, txtReceivedTotal, txtBalance, txtDue;
        CheckRate();
        if (document.getElementById('<%=txtDue.ClientID %>'))
            txtDue = parseFloat(document.getElementById('<%=txtDue.ClientID %>').value);
        else
            txtDue = 0;
        if (isNaN(txtDue))
            txtDue = 0;
        if (document.getElementById('<%=txtCCTotal.ClientID %>'))
            txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
        else
            txtCCTotal = 0;
        if (isNaN(txtCCTotal))
            txtCCTotal = 0;
        txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
        if (isNaN(txtCashTotal))
            txtCashTotal = 0;
        txtTotal = parseFloat(document.getElementById('<%=lblTotalNETAmount.ClientID%>').innerHTML.replace(',', ''));
        if (isNaN(txtTotal))
            txtTotal = 0;
        var CrCardCharge = parseFloat(document.getElementById('<%=txtCrCardCharges.ClientID %>').value);
        txtReceivedTotal = txtCCTotal + txtCashTotal;

        txtBalance = 0;
        if (txtCashTotal > 0)
            if (txtReceivedTotal > txtTotal) {
                txtBalance = txtReceivedTotal - txtTotal;
                if (txtBalance > txtCashTotal)
                    txtBalance = 0;
            }
        if (txtCCTotal > txtDue) alert("Credit card amt more than due");
        txtReceivedTotal = txtCCTotal + txtCashTotal + CrCardCharge;
        if (document.getElementById('<%=txtReceivedTotal.ClientID %>'))
            document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtReceivedTotal.toFixed(getRoundOff());
        if (document.getElementById('<%=txtCCTotal.ClientID %>'))
            document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());

        document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
        if (document.getElementById('<%=txtBalance.ClientID %>')) document.getElementById('<%=txtBalance.ClientID %>').value = txtBalance.toFixed(getRoundOff());
    }

    function CheckRate() {
        var temp = new Array();
        var txtCr = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
        CrCardCharge = 0.00;
        temp = document.getElementById('<%=hfCobrand.ClientID %>').value.split("|");
        var ddlist = document.getElementById('<%=ddCreditcard.ClientID %>');

        for (var i = 0; i < temp.length; i++) {
            if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                CrCardCharge = Math.ceil((txtCr * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff()));
            }
        }
        //alert(CrCardCharge);
        document.getElementById('<%=txtCrCardCharges.ClientID %>').value = CrCardCharge.toFixed(getRoundOff());
        //}return true;
    }

    function getRoundOff() {
        var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
        var amt;
        amt = parseFloat(roundOff)
        if (isNaN(amt))
            amt = 2;
        return amt;
    }



    function TabChanged(sender, args) {
        sender.get_clientStateField().value =
    sender.saveClientState();
    }

    window.format = function (b, a) {
        if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
    };

    var myIds = new Array();
    var myDetails = new Array();
    var myRates = new Array();
    var myCosts = new Array();
    var myISBNs = new Array();

    function ClientItemSelected(sender, e) {
        var index = sender._selectIndex;
        var hdnITM_ID = sender.get_element().id.replace("txtSAD_DESCR", "hdnITM_ID");
        var txtSAD_QTY = sender.get_element().id.replace("txtSAD_DESCR", "txtSAD_QTY");
        var txtSAD_RATE = sender.get_element().id.replace("txtSAD_DESCR", "txtSAD_RATE");
        var txtSAD_TOTAL = sender.get_element().id.replace("txtSAD_DESCR", "txtSAD_TOTAL");
        var txtSAD_DETAILS = sender.get_element().id.replace("txtSAD_DESCR", "txtSAD_DETAILS");
        var txtSAD_COST = sender.get_element().id.replace("txtSAD_DESCR", "txtSAD_COST");
        $get(hdnITM_ID).value = myIds[index];
        $get(sender.get_element().id).value = myDetails[index];
        if ($get(txtSAD_RATE)) $get(txtSAD_RATE).value = format("#,##0.00", myRates[index]);
        if ($get(txtSAD_DETAILS)) $get(txtSAD_DETAILS).value = myISBNs[index];
        if ($get(txtSAD_COST)) $get(txtSAD_COST).value = format("#,##0.00", myCosts[index]);
        if ($get(txtSAD_QTY).value == "") {
            $get(txtSAD_QTY).value = 1;
            if ($get(txtSAD_TOTAL)) $get(txtSAD_TOTAL).value = format("#,##0.00", myRates[index]);
        }
        else $get(txtSAD_TOTAL).value = format("#,##0.00", eval(myRates[index]) * eval($get(txtSAD_QTY).value));
    }

    function MultiSelect(sender, e) {
        var comletionList = sender.get_completionList();
        for (i = 0; i < comletionList.childNodes.length; i++) {
            var itemobj = new Object();
            var _data = comletionList.childNodes[i]._value;
            itemobj.descr = _data.substring(_data.lastIndexOf('|') + 1); // parse name as item value
            comletionList.childNodes[i]._value = itemobj.name;
            _data = _data.substring(0, _data.lastIndexOf('|'));
            itemobj.isbn = _data.substring(_data.lastIndexOf('|') + 1);
            _data = _data.substring(0, _data.lastIndexOf('|'));
            itemobj.rate = _data.substring(_data.lastIndexOf('|') + 1);
            _data = _data.substring(0, _data.lastIndexOf('|'));
            itemobj.id = _data.substring(_data.lastIndexOf('|') + 1);
            _data = _data.substring(0, _data.lastIndexOf('|'));
            itemobj.cost = _data.substring(_data.lastIndexOf('|') + 1);
            _data = _data.substring(0, _data.lastIndexOf('|'));
            itemobj.last = _data.substring(_data.lastIndexOf('|') + 1);
            if (itemobj.id) {
                myIds[i] = itemobj.id; // id used in updating hidden file
                myDetails[i] = itemobj.descr;
                myRates[i] = itemobj.rate;
                myCosts[i] = itemobj.cost;
                myISBNs[i] = itemobj.isbn;
            }
            comletionList.childNodes[i].innerHTML = "<div class='container-fluid darkPanlAlumini'>  <div class='row-fluid'>    <div class='col-lg-12 col-md-12 col-sm-12'>" + itemobj.descr + "</div>"
                                              //+ "<div width='30%' class='col-lg-4 col-md-4 col-sm-4'>" + itemobj.isbn + "</div>"
                                              //+ "<div width='10%' class='col-lg-2 col-md-2 col-sm-2'>" + itemobj.rate + "</div> "
                                              + "</div></div>";
        }
    }


    function getItemName(hdnClient, txtClient, txtDetails, txtCost, txtQty, txtRate, txtTotal) {
        var sFeatures;
        var lstrVal;
        var lintScrVal;
        var pMode;
        var NameandCode;
        sFeatures = "dialogWidth: 860px; ";
        sFeatures += "dialogHeight: 700px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var ddlBSU = document.getElementById('<%=ddlBSU.ClientID%>');
        var ddlBSUSel = ddlBSU.options[ddlBSU.selectedIndex].value;
        var saletype = document.getElementById("<%=h_SAL_Type.ClientID %>").value;

        //alert(document.getElementById('<%=chkbStationary.ClientID%>'));
        if (document.getElementById('<%=chkbStationary.ClientID%>') != null) {

            var IsStationary = document.getElementById('<%=chkbStationary.ClientID%>').checked;
            url = "../common/PopupSelect.aspx?id=" + "ITMITEMSALE" + "&sBsuid=" + ddlBSUSel + "&saletype=" + saletype + "&bTaxable=" + IsStationary;
        }
        else {
            url = "../common/PopupSelect.aspx?id=" + "ITMITEMSALE" + "&sBsuid=" + ddlBSUSel + "&saletype=" + saletype + "&bTaxable=false"
        }
        //url = "../common/PopupSelect.aspx?id=" + "ITMITEMSALE" + "&sBsuid=" + ddlBSUSel + "&saletype=" + saletype + "&bTaxable=" + IsStationary;
        //url = "../common/PopupSelect.aspx?id=" + "ITMITEMSALE" + "&sBsuid=" + ddlBSUSel + "&saletype=A&bTaxable=" + IsStationary;
        //result = window.showModalDialog(url, "", sFeatures
        document.getElementById('<%=hf_ITM_ID.ClientID%>').value = hdnClient;
        document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value = txtClient;
        document.getElementById('<%=hf_txtSAD_DETAILS.ClientID%>').value = txtDetails;
        document.getElementById('<%=hf_txtSAD_COST.ClientID%>').value = txtCost;
        document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value = txtQty;
        document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value = txtRate;
        document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>').value = txtTotal;

        var oWnd = radopen(url, "pop_rel");
        oWnd.set_modal(true);
       <%-- if (result == '' || result == undefined) {
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            return false;
        }
        NameandCode = result.split('___');
        if (document.getElementById(txtQty).value == "0" || document.getElementById(txtQty).value == "") document.getElementById(txtQty).value = "1";
        if (document.getElementById(txtCost)) document.getElementById(txtCost).value = NameandCode[4];
        if (document.getElementById(txtRate)) document.getElementById(txtRate).value = NameandCode[3];
        if (document.getElementById(txtClient)) document.getElementById(txtClient).value = NameandCode[2];
        if (document.getElementById(txtClient)) document.getElementById(txtDetails).value = NameandCode[1];
        if (document.getElementById(hdnClient)) document.getElementById(hdnClient).value = NameandCode[0];
        if (document.getElementById(txtTotal)) document.getElementById(txtTotal).value = eval(document.getElementById(txtRate).value) * eval(document.getElementById(txtQty).value);
        document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;--%>
    }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode[1]);                   

                var qty_id = document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value
                if (document.getElementById(qty_id).value == "0" || document.getElementById(qty_id).value == "")
                    document.getElementById(qty_id).value = "1";


                //alert(document.getElementById(document.getElementById('<%=hf_txtSAD_COST.ClientID%>').value))
                if (document.getElementById('<%=hf_txtSAD_COST.ClientID%>') && document.getElementById(document.getElementById('<%=hf_txtSAD_COST.ClientID%>').value) != null) {

                    var cost_id = document.getElementById('<%=hf_txtSAD_COST.ClientID%>').value
                    //alert(cost_id);
                    //alert(NameandCode[4]);
                    document.getElementById(cost_id).value = NameandCode[4];
                }

                if (document.getElementById('<%=hf_txtSAD_RATE.ClientID%>') && document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value) != null) {
                    var rate_id = document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value
                    //alert(rate_id);
                    document.getElementById(rate_id).value = NameandCode[3];
                }


                if (document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>') && document.getElementById(document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value) != null) {
                    var descr_id = document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value
                    //alert(descr_id);
                    document.getElementById(descr_id).value = NameandCode[2];
                }

                if (document.getElementById('<%=hf_txtSAD_DETAILS.ClientID%>') && document.getElementById(document.getElementById('<%=hf_txtSAD_DETAILS.ClientID%>').value) != null) {
                    var details_id = document.getElementById('<%=hf_txtSAD_DETAILS.ClientID%>').value
                    //alert(details_id);
                    document.getElementById(details_id).value = NameandCode[1];
                }

                if (document.getElementById('<%=hf_ITM_ID.ClientID%>') && document.getElementById(document.getElementById('<%=hf_ITM_ID.ClientID%>').value) != null) {
                    var client_id = document.getElementById('<%=hf_ITM_ID.ClientID%>').value
                    //alert(client_id);
                    document.getElementById(client_id).value = NameandCode[0];
                }

                if (document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>') && document.getElementById(document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>').value) != null) {
                    var total_id = document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>').value
                    //alert(total_id);
                    document.getElementById(total_id).value = eval(document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value).value) * eval(document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value).value);
                }

                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }

        }

        function getClientName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMCLIENT"

            //var e = document.getElementById("<%=ddlTerm.ClientID %>");
            //if (e == undefined)
            if (document.getElementById('<%=ddlTerm.ClientID%>') != null)
                var selACD_ID = 0;
            else
                var selACD_ID = e.options[e.selectedIndex].value;

            //var selACD_ID = document.getElementById('<%=h_ACD_ID.ClientID %>').value;
            var TYPE;
            TYPE = 'TC';
            //url = '../FEES/ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + "1" + '&ACD_ID=' + selACD_ID;
            url = '../Students/ShowStudentNew.aspx?TYPE=' + TYPE + '&VAL=' + "1" + '&ACD_ID=' + selACD_ID;
            //var oWnd = radopen('../FEES/ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + "1" + '&ACD_ID=' + selACD_ID, "pop_stu");
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
       <%-- result = window.showModalDialog(url, "", sFeatures);
        if (result == '' || result == undefined) {
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            return false;
        }
        NameandCode = result.split('||');
        document.getElementById('<%= hTPT_Client_ID.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= txtStuName.ClientID %>').value = NameandCode[1];
        document.getElementById('<%= txtStuNo.ClientID %>').value = NameandCode[2];
        document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        return true;--%>
        }




        function OnClientStuClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode[0]);
                //alert(NameandCode[1]);
                //alert(NameandCode[2]);

                document.getElementById('<%= hTPT_Client_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtStuName.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtStuNo.ClientID %>').value = NameandCode[2];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                __doPostBack('<%=hGridRefresh.ClientID%>', "");
                //document.forms[0].submit();
            }
        }

        function SetSalesClientValue(result, ctrl) {
            NameandCode = result.split('||');
            document.getElementById('<%= hTPT_Client_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtStuName.ClientID %>').value = NameandCode[2];
            document.getElementById('<%= txtStuNo.ClientID %>').value = NameandCode[1];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            CloseFrame();
            __doPostBack('<%=hGridRefresh.ClientID%>', "ValueChanged");
            return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }

        function calculateItem(txtQty, txtRate, txtTotal) {
            var total;
            if (document.getElementById(txtRate)) {
                total = eval(document.getElementById(txtRate).value) * eval(document.getElementById(txtQty).value);
                document.getElementById(txtTotal).value = format("#,##0.00", total);
            }
        }

        function calculate() {
            var total = 0;
            document.getElementById("<%=txtTotal.ClientID %>").value = format("#,##0.00", total);
            return false;
        }
        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function GetBsuId() {
            return '<%= Session("sBsuId")%>';
        }
    </script>

    <%-- <script type="text/javascript" language="javascript">
         $(document).ready(function () {
             
             var prm = Sys.WebForms.PageRequestManager.getInstance();
             prm.add_initializeRequest(InitializeRequest);
             prm.add_endRequest(EndRequest);
             InitStudentAutoComplete();
             InitCompanyAutoComplete();
         });
         function InitializeRequest(sender, args) {
         }
         function EndRequest(sender, args) {
             // after update occur on UpdatePanel re-init the Autocomplete
             InitStudentAutoComplete();
             InitCompanyAutoComplete();
         }

         function InitCompanyAutoComplete() {
             
             $("[id$=txtSAD_DESCR]".autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: "SalesRequest.aspx/GetItems",
                                    data: "{ 'prefix': '" + request.term + "','BSUID':'" + GetBsuId() + "'}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('-')[0],
                                                val: item.split('-')[1]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        alert(response.responseText);
                                    },
                                    failure: function (response) {
                                        alert(response.responseText);
                                    }
                                });
                            },
                            select: function (e, i) {
                                $("#<%=hf_ITM_ID.ClientID%>").val(i.item.val);
                            },
                            minLength: 1
                        }));
         }

        
  
            </script>--%>
    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_stu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientStuClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="pgTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <div>
                    <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr valign="bottom">
                            <td align="left">
                                <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">

                                    <tr>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblPRFNo" runat="server" Text="Sales Number" CssClass="field-label"></asp:Label><span class="text-danger">*</span></td>
                                        <td align="left" width="25%">
                                            <asp:TextBox ID="txtSALNo" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%">
                                            <asp:DropDownList ID="ddlTerm" runat="server"></asp:DropDownList></td>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblPRFDate" runat="server" Text="Sale Date" CssClass="field-label"></asp:Label><span class="text-danger">*</span></td>
                                        <td align="left" width="25%">
                                            <asp:TextBox ID="txtSALDate" Enabled="false" runat="server"></asp:TextBox>
                                            <asp:ImageButton Visible="false" ID="lnkSALDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtSALDate" PopupButtonID="lnkSALDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td align="left" width="10%"></td>

                                    </tr>
                                    <tr>
                                        <td align="left" width="10%"><span class="field-label">Business Unit</span><span class="text-danger">*</span></td>
                                        <td align="left" width="25%">
                                            <asp:DropDownList ID="ddlBSU" runat="server"></asp:DropDownList></td>
                                        <td align="left" width="20%">
                                            <asp:Label ID="lblSALNo" runat="server" Text="Sales Number" CssClass="field-label"></asp:Label></td>
                                        <td align="left" colspan="2" width="35%">
                                            <asp:TextBox ID="txtOldSalNo" runat="server" AutoPostBack="true"></asp:TextBox></td>
                                        <td align="left" width="10%"></td>
                                    </tr>
                                    <tr runat="server" id="trStudent">
                                        <td align="left" width="10%"><span class="field-label">Student No+Name</span><span class="text-danger">*</span></td>
                                        <td align="left" width="25%">
                                            <asp:TextBox ID="txtStuNo" runat="server" AutoPostBack="true"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%">
                                            <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getClientName();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                            <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                                            <asp:Label ID="lblStuCurrstatus" runat="server"></asp:Label>
                                        </td>
                                        <td align="left" width="10%"><span class="field-label">Grade+Section</span></td>
                                        <td align="left" width="25%">
                                            <asp:TextBox ID="txtGrade" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td align="left" width="10%">
                                            <asp:TextBox ID="txtSection" runat="server" Enabled="false"></asp:TextBox></td>
                                    </tr>
                                    <tr runat="server" id="trVATCode">
                                        <td align="left" width="10%">
                                            <asp:CheckBox ID="chkbStationary" runat="server" AutoPostBack="True" Text="Stationary Sales" ToolTip="Select if stationary sales" CssClass="field-label" />
                                        </td>
                                        <td align="left" width="25%">
                                            <pre id="preError" class="invisible" runat="server"></pre>
                                        </td>
                                        <td align="left" width="20%"></td>
                                        <td align="left" width="10%"><span class="field-label">Tax Code</span></td>
                                        <td align="left" width="25%">
                                            <asp:DropDownList ID="ddlVATCode" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" width="10%"></td>

                                    </tr>
                                    <tr>
                                        <td colspan="6" align="left">
                                            <asp:GridView ID="grdSAL" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="true"
                                                CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                                <Columns>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_ID" runat="server" Text='<%# Bind("SAD_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_DESCR" runat="server" Text='<%# Bind("SAD_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtSAD_DESCR" runat="server" Text='<%# Bind("SAD_DESCR") %>'></asp:TextBox><span class="text-danger">*</span>
                                                            <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                            <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx3" OnClientPopulated="MultiSelect" FirstRowSelected="true"
                                                                CompletionListCssClass="completionListElement"
                                                                OnClientItemSelected="ClientItemSelected"
                                                                CompletionSetCount="5" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                                ServiceMethod="GetPrdDescr" ServicePath="SalesRequest.aspx" TargetControlID="txtSAD_DESCR">
                                                            </ajaxToolkit:AutoCompleteExtender>
                                                            <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("SAD_ITM_ID") %>' runat="server" />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSAD_DESCR" runat="server" Text='<%# Bind("SAD_DESCR") %>'></asp:TextBox><span class="text-danger">*</span>
                                                            <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                            <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx4" OnClientPopulated="MultiSelect" FirstRowSelected="true"
                                                                CompletionListCssClass="completionListElement"
                                                                OnClientItemSelected="ClientItemSelected"
                                                                CompletionSetCount="5" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                                ServiceMethod="GetPrdDescr" ServicePath="SalesRequest.aspx" TargetControlID="txtSAD_DESCR">
                                                            </ajaxToolkit:AutoCompleteExtender>
                                                            <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("SAD_ITM_ID") %>' runat="server" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ISBN\Barcode">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_DETAILS" runat="server" Text='<%# Bind("SAD_DETAILS") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtSAD_DETAILS" runat="server" Text='<%# Bind("SAD_DETAILS") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSAD_DETAILS" runat="server" Text='<%# Bind("SAD_DETAILS") %>'></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cost Price" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_COST" runat="server" Style="text-align: right" Text='<%# Bind("SAD_COST") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtSAD_COST" runat="server" Style="text-align: right" Text='<%# Bind("SAD_COST") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSAD_COST" runat="server" Style="text-align: right" Text='<%# Bind("SAD_COST") %>'></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Quantity" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_QTY" runat="server" Width="50px" Text='<%# Bind("SAD_QTY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtSAD_QTY" runat="server" Style="text-align: right" Text='<%# Bind("SAD_QTY") %>'></asp:TextBox><span class="text-danger">*</span>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSAD_QTY" runat="server" Style="text-align: right" Text='<%# Bind("SAD_QTY") %>'></asp:TextBox><span class="text-danger">*</span>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Unit Rate" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("SAD_RATE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtSAD_RATE" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("SAD_RATE") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSAD_RATE" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("SAD_RATE") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="lblSAD_TOTAL" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("SAD_TOTAL") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtSAD_TOTAL" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("SAD_TOTAL") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSAD_TOTAL" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("SAD_TOTAL") %>'></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tax Amount" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_TAX_AMT" runat="server" Style="text-align: right" Text='<%# Bind("SAD_TAX_AMOUNT")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="NET Amount" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSAD_NET_AMT" runat="server" Style="text-align: right" Text='<%# Bind("SAD_NET_AMOUNT") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdatePRF" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelPRF" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddPRF" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditPRF" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr id="trTotal" runat="server">
                                        <td colspan="6" align="right">
                                            <table border="0" width="100%">
                                                <tr>
                                                    <td align="left" width="20%"></td>
                                                    <td align="left" width="30%"></td>
                                                    <td align="left" width="20%"></td>
                                                    <td align="left" width="15%">
                                                        <span class="field-label">Sub Total</span>  </td>
                                                    <td align="left" width="15%">
                                                        <asp:Label ID="lblSubTotal" runat="server" Text="" CssClass="field-label"></asp:Label></td>
                                                </tr>
                                                <tr runat="server" id="trVATTotal">
                                                    <td align="left"></td>
                                                    <td align="left"></td>
                                                    <td align="left">
                                                        <asp:Label ID="lblTotal" runat="server" CssClass="field-label"></asp:Label></td>
                                                    <td align="left"><span class="field-label">Tax Amount</span>  </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblTotalVATAmount" runat="server" Text="" CssClass="field-label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left"></td>
                                                    <td align="left"></td>
                                                    <td align="left"></td>
                                                    <td align="left"><span class="field-label">Net Total</span></td>
                                                    <td align="left">
                                                        <asp:Label ID="lblTotalNETAmount" runat="server" Text="" CssClass="field-label"></asp:Label></td>
                                                </tr>
                                            </table>
                                            <asp:TextBox ID="txtTotal" Style="text-align: right; display: none;" Text="0.0" runat="server" Width="1" Height="1" Enabled="false" CssClass="vbn"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="title-bg-lite">
                                        <td align="left" colspan="6">Payment Details</td>
                                    </tr>
                                    <tr runat="server" id="trCash">
                                        <td colspan="6">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Cash</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtCashTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.select();" onBlur="UpdateSum();"
                                                            Style="text-align: right" TabIndex="45"></asp:TextBox>&nbsp;
                                                    </td>
                                                    <td align="left" width="20%"><span class="field-label">Due</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtDue" runat="server" Style="text-align: right" TabIndex="205" Enabled="false"></asp:TextBox></td>

                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    <tr runat="server" id="trCreditCard">
                                        <td colspan="6">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Credit Card </span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtCCTotal" Style="text-align: right" runat="server" onFocus="this.select();" onBlur="UpdateSum();" AutoCompleteType="Disabled" TabIndex="50"></asp:TextBox>
                                                        <ajaxToolkit:PopupControlExtender ID="pceCreditCard" runat="server" PopupControlID="pnlCreditCard"
                                                            Position="Bottom" TargetControlID="txtCCTotal">
                                                        </ajaxToolkit:PopupControlExtender>
                                                    </td>
                                                    <td align="left" width="20%"><span class="field-label">Credit Card Charge</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtCrCardCharges" runat="server" Text="0.00" Style="text-align: right;"
                                                            TabIndex="200"></asp:TextBox></td>

                                                </tr>
                                            </table>
                                        </td>


                                    </tr>

                                    <tr>
                                        <td colspan="6">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Total Received</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtReceivedTotal" Style="text-align: right" runat="server" TabIndex="205"></asp:TextBox></td>
                                                    <td align="left" width="20%">
                                                        <asp:Label ID="lblBalance" runat="server" Text="Balance" CssClass="field-label"></asp:Label>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" TabIndex="220"></asp:TextBox></td>

                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" rowspan="2" width="20%"><span class="field-label">Narration</span></td>
                                                    <td align="left" colspan="3">
                                                        <asp:TextBox ID="txtRemarks" runat="server" Height="32px" TextMode="MultiLine" TabIndex="160"></asp:TextBox>
                                                        <asp:LinkButton ID="lbViewTotal" runat="server" Visible="False">Preview Total</asp:LinkButton></td>

                                                </tr>
                                            </table>
                                        </td>



                                    </tr>


                                    <tr>
                                        <td align="center" colspan="6">
                                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print Receipt" />
                                            <asp:Button ID="btnInvoice" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print Invoice" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                    </tr>
                                    <asp:HiddenField ID="h_SALGridDelete" Value="0" runat="server" />
                                    <asp:HiddenField ID="h_print" Value="" runat="server" />
                                    <asp:HiddenField ID="hfCobrand" runat="server" />
                                    <asp:HiddenField ID="h_SAL_Type" runat="server" />
                                    <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                    <asp:HiddenField ID="h_Cusid" runat="server" />
                                    <asp:HiddenField ID="h_Mode" runat="server" />
                                    <asp:HiddenField ID="h_empid" runat="server" />
                                    <asp:HiddenField ID="h_term" runat="server" />
                                    <asp:HiddenField ID="h_total" runat="server" />
                                    <asp:HiddenField ID="h_acd_id" runat="server" />
                                    <asp:HiddenField ID="h_ItemID" runat="server" EnableViewState="False" />
                                    <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                    <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

                <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="exec GetCreditCardListForCollection 'FEES'"></asp:SqlDataSource>
                <asp:Panel ID="pnlCreditCard" Style="display: none" runat="server" CssClass="panel-cover">
                    <table border="0" cellpadding="3" cellspacing="0" align="center" width="100%">
                        <tr>
                            <td align="left" colspan="1"><span class="field-label">Credit Card </span>
                            </td>
                            <td align="right">
                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close.png" OnClientClick="return HideAll();" TabIndex="54" Style="text-align: right" /></td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1"><span class="field-label">Card Type</span></td>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard"
                                    DataTextField="CRI_DESCR" DataValueField="CRR_ID" SkinID="DropDownListNormal" TabIndex="55">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Authorisation Code</span></td>
                            <td align="left" colspan="1">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="60" SkinID="TextBoxCollection"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                        SelectCommand="exec GetCreditCardListForCollection 'FEES'">
                        <SelectParameters>
                            <asp:Parameter Name="CollectFrom" DefaultValue="FEES" Type="String" />
                            <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hf_ITM_ID" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_DESCR" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_DETAILS" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_COST" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_QTY" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_RATE" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_TOTAL" runat="server" />
</asp:Content>
