﻿Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Inventory_HousingWaiver
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass(), requestForQuotation As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
            'smScriptManager.RegisterPostBackControl(btnUpload)
            'smScriptManager.RegisterPostBackControl(btnUpload1)

            If Not ViewState("ActiveTabIndex") Is Nothing Then TabContainer1.ActiveTabIndex = ViewState("ActiveTabIndex")

            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                hViewState.Value = ViewState("datamode")
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")


                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI02055") And (ViewState("MainMnu_code") <> "PI04020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtRent.Attributes.Add("onblur", " return calc();")
                BindDropDown()
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(ViewState("EntryId"))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If
            End If
            ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
            DisableButtons(ViewState("menu_rights"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub DisableButtons(ByVal AccesRights As Integer)

        If AccesRights = 1 Then
            btnAdd.Visible = False
            btnRecall.Visible = False
            btnEdit.Visible = False
            btnSave.Visible = False
            btnAccept.Visible = False
            btnRecall.Visible = False
            btnSend.Visible = False
            btnSend.Visible = False
            btnPrint.Visible = False
            
        End If
    End Sub
    Private Sub BindDropDown()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim str_Sql1 As String = "SELECT RCM_ID ID,RCM_DESCR DESCR FROM RECITY_M "
            ddlLocation.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql1)
            ddlLocation.DataTextField = "DESCR"
            ddlLocation.DataValueField = "ID"
            ddlLocation.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            hViewState.Value = ViewState("datamode")
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            hViewState.Value = ViewState("datamode")
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            hViewState.Value = ViewState("datamode")
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        ddlLocation.Enabled = Not mDisable
        'imgCondate.Enabled = EditAllowed
        txtremarks.Enabled = EditAllowed
        txtRent.Enabled = EditAllowed
        btnSend.Visible = mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnEdit.Visible = mDisable
        btnPrint.Visible = Not btnSave.Visible
        btnAccept.Visible = mDisable And (ViewState("MainMnu_code") = "PI04020")
        btnReject.Visible = btnAccept.Visible
        'btnMessage.Visible = btnAccept.Visible
        btnAdd.Visible = mDisable And Not (ViewState("MainMnu_code") = "PI04020")
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Dim sqlStr As New StringBuilder
        Dim sqlWhere As String = p_Modifyid
        Dim str_conn As String = connectionString
        Dim dt As New DataTable
        h_EntryId.Value = p_Modifyid
        h_bsu_id.Value = Session("sBSUID")
        If p_Modifyid = 0 Then
            trApproval.Visible = False
            trWorkflow.Visible = False
            trFlow.Visible = False
            trApproval.HeaderText = ""
            trWorkflow.HeaderText = ""
            trFlow.HeaderText = ""
            ClearDetails()
            txtTCH_No.Text = "New"
            btnAccept.Visible = False
            btnReject.Visible = False
            btnRecall.Visible = False
            btnEdit.Visible = False
            btnSend.Visible = False
            btnSave.Visible = True
            txtAllow.Text = "0.00"
            txtRent.Text = "0.00"
            txtSchoolCost.Text = "0.00"
            h_Loc_id.Value = ddlLocation.SelectedValue
            pgTitle.Text = "New Housing waiver Application"
            Session("Empdata") = Nothing
        Else
            'Dim RerurnEnable As Integer = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select isnull(count(*),0)Total from VW_Enable_Return_User_list where UserID='" & Session("sUsr_name") & "'")
            sqlStr = New StringBuilder
            sqlStr.Append("SELECT THW_ID,THW_NO,THW_BSU_ID,THW_DATE,THW_EMP_ID,THW_RENT,THW_ALLOW,THW_COST,THW_CIT_ID,THW_REMARKS,THW_USR_NAME,THW_FYEAR,THW_DELETED")
            sqlStr.Append(",ISNULL((SELECT COUNT(*) FROM OASIS..EMPDEPENDANTS_D WHERE EDD_EMP_ID = E1.EMP_ID),0) DEPENDANTS,THW_DELETED,THW_USR_NAME,")
            sqlStr.Append("E1.EMP_NAME Name, E1.EMP_DES_DESCR DESIGNATION, E1.EMPNO EMPNO ")
            sqlStr.Append(",isnull(THW_STATUS,'N')THW_STATUS,isnull(THW_APR_ID,0)THW_APR_ID,isnull(THW_STATUS_STR,'')THW_STATUS_STR,isnull(THW_msg_usr,''),isnull(THW_msg_status,'')THW_msg_status,isnull(THW_msg_qry,'')THW_msg_qry")
            sqlStr.Append(" FROM TCT_HOUSEWAIVER ")
            sqlStr.Append(" inner join  oasis_docs..VW_EMPLOYEE_V E1 on E1.emp_id=THW_EMP_ID")
            sqlStr.Append(" inner join  RECITY_M ON RCM_ID =THW_CIT_ID ")
            sqlStr.Append(" WHERE THW_ID = " & p_Modifyid)
            dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")
            If dt.Rows.Count > 0 Then

                h_empID.Value = dt.Rows(0)("THW_EMP_ID")
                txtEmpno.Text = dt.Rows(0)("EMPNO")
                txtEmpName.Text = dt.Rows(0)("Name")
                txtDes.Text = dt.Rows(0)("DESIGNATION")
                txtDep.Text = dt.Rows(0)("DEPENDANTS")
                h_Loc_id.Value = dt.Rows(0)("THW_CIT_ID")
                h_THW_Status.Value = dt.Rows(0)("THW_STATUS")
                h_delete.Value = dt.Rows(0)("THW_DELETED")
                txtAllow.Text = dt.Rows(0)("THW_ALLOW")
                txtSchoolCost.Text = dt.Rows(0)("THW_COST")
                h_bsu_id.Value = dt.Rows(0)("THW_BSU_ID")
                h_fyear.Value = dt.Rows(0)("THW_FYEAR")
                txtTCH_No.Text = dt.Rows(0)("THW_NO")
                ddlLocation.SelectedValue = dt.Rows(0)("THW_CIT_ID")
                txtRent.Text = dt.Rows(0)("THW_RENT")
                txtContractDate.Text = Format(dt.Rows(0)("THW_DATE"), "dd/MMM/yyyy")
                txtremarks.Text = dt.Rows(0)("THW_REMARKS")
                h_ThW_apr_id.Value = dt.Rows(0)("THW_APR_ID")
                btnAccept.Visible = btnAccept.Visible And dt.Rows(0)("THW_STATUS") = "S"
                btnReject.Visible = btnAccept.Visible
                'btnRecall.Visible = dt.Rows(0)("THW_STATUS") = "R" Or dt.Rows(0)("THW_STATUS") = "D"
                btnSend.Visible = btnSend.Visible And (dt.Rows(0)("THW_STATUS") = "N")
                btnSave.Visible = btnSave.Visible And ((dt.Rows(0)("THW_STATUS") = "N"))
                'btnRenew.Visible = (UCase(dt.Rows(0)("THW_STATUS")) = "C")
                btnPrint.Visible = Not btnSave.Visible
                btnEdit.Visible = btnEdit.Visible And (dt.Rows(0)("THW_STATUS") = "N") And dt.Rows(0)("THW_APR_ID") = 0 And (ViewState("MainMnu_code") = "PI02055")
                btnEdit.Visible = btnEdit.Visible Or ViewState("MainMnu_code") = "PI02035"
                'If dt.Rows(0)("THW_USR_NAME") <> Session("sUsr_name") Then
                '    Tr4.Visible = True
                'End If
                'btnAdd.Visible = (Session("F_YEAR") >= "2014") And btnAdd.Visible And Not (ViewState("MainMnu_code") = "PI04013" Or ViewState("MainMnu_code") = "PI02035")
                'btnMessage.Visible = btnAccept.Visible Or dt.Rows(0)("TCH_STATUS") = "S" Or (ViewState("MainMnu_code") = "PI02035")
            Else
                    Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
        If p_Modifyid > 0 Then
            If ViewState("MainMnu_code") = "PI04020" Then
                trApproval.Visible = True
                trApproval.HeaderText = "Approval\Rejection Comments"
            End If
            Dim ds As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "workflowqueryHW " & h_EntryId.Value).Tables(0)
            rptFlow.DataSource = ds
            rptFlow.DataBind()
            trFlow.Visible = True
            trFlow.HeaderText = "Workflow progress"
            trWorkflow.Visible = True
            trWorkflow.HeaderText = "Workflow Details"
            'trDocument.Visible = True
            'trDocument.HeaderText = "Documents"

            'trUpload.Visible = True
            'trUpload.HeaderText = "Contracts"
            'If btnMessage.Visible Then
            '    trApproval.Visible = True
            '    If ViewState("MainMnu_code") = "PI04013" Then
            '        trApproval.HeaderText = "Approval\Rejection Comments"
            '    Else
            '        trApproval.HeaderText = "Message Comments"
            '    End If
            'End If
            sqlStr = New StringBuilder
            sqlStr.Append("select replace(convert(varchar(16), WRK_DATE, 106),' ','/') WRK_DATE, USR_DISPLAY_NAME WRK_USER, WRK_ACTION, WRK_DETAILS from WORKFLOWHW inner JOIN OASIS.dbo.USERS_M on WRK_USERNAME=USR_NAME where WRK_THW_ID=" & h_EntryId.Value & " order by WRK_ID desc")
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr.ToString).Tables(0)
            gvWorkflow.DataSource = ds
            gvWorkflow.DataBind()
            'If dt.Rows(0)("THW_STATUS") = "N" And dt.Rows(0)("THW_APR_ID") = 0 Then
            '    Dim isReturned As Int16 = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from WORKFLOWTCT where WRK_ACTION='Returned' and WRK_TCH_ID=" & h_EntryId.Value)
            '    If isReturned > 0 Then
            '        btnSend.Visible = False
            '        btnPrint.Visible = False
            '        btnAdd.Visible = False
            '    End If
            'End If

            showImages()
            showImagesDocument()
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try

            If h_empID.Value = "" Then lblError.Text = "Employee Not Selected,"
            If ddlLocation.SelectedValue = "" Then lblError.Text = lblError.Text & "Select Location,"

            If lblError.Text.Length > 0 Then
                Exit Sub
            End If

            Dim pParms(12) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@THW_ID", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@THW_NO", txtTCH_No.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@THW_BSU_ID", Session("sBsuID"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@THW_DATE", txtContractDate.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@THW_EMP_ID", h_empID.Value, SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@THW_RENT", txtRent.Text, SqlDbType.Decimal)
            pParms(7) = Mainclass.CreateSqlParameter("@THW_ALLOW", txtAllow.Text, SqlDbType.Decimal)
            pParms(8) = Mainclass.CreateSqlParameter("@THW_COST", txtSchoolCost.Text, SqlDbType.Decimal)
            pParms(9) = Mainclass.CreateSqlParameter("@THW_CIT_ID", h_Loc_id.Value, SqlDbType.Int)
            pParms(10) = Mainclass.CreateSqlParameter("@THW_REMARKS", txtremarks.Text, SqlDbType.VarChar)
            pParms(11) = Mainclass.CreateSqlParameter("@THW_USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@THW_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)





            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTCT_HOUSEWAIVER", pParms)

            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


        writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), txtComments.Text, 0, "N")


        SetDataMode("view")
        setModifyvalues(ViewState("EntryId"))
        lblError.Text = "Data Saved Successfully !!!"
    End Sub

    Private Sub showImages()
        '    Dim sqlStr As String
        '    sqlStr = "select tci_id, tci_filename, tci_contenttype, RES_DESCR tci_name, case when tci_username='" & Session("sUsr_name") & "' then 'true' else 'false' end tci_visible "
        '    sqlStr &= "from TCT_I inner JOIN RESERVICES on RES_ID=tci_doctype where RES_CONTRACT_STATUS<>'U' and tci_tch_id=" & h_EntryId.Value
        '    Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr).Tables(0)
        '    rptImages.DataSource = dt
        '    rptImages.DataBind()
    End Sub
    Private Sub showImagesDocument()
        'Dim sqlStr As String
        'sqlStr = "select tci_id, tci_filename, tci_contenttype, RES_DESCR tci_name, case when tci_username='" & Session("sUsr_name") & "' then 'true' else 'false' end tci_visible "
        'sqlStr &= "from TCT_I inner JOIN RESERVICES on RES_ID=tci_doctype where RES_CONTRACT_STATUS='U' and tci_tch_id=" & h_EntryId.Value
        'Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr).Tables(0)
        'rptDocument.DataSource = dt
        'rptDocument.DataBind()
    End Sub
    Sub ClearDetails()
        txtContractDate.Text = Now.ToString("dd/MMM/yyyy")
        txtremarks.Text = ""
        txtTCH_No.Text = ""
        h_empID.Value = ""

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        h_Loc_id.Value = ddlLocation.SelectedItem.Value

    End Sub
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim objConn As New SqlConnection(connectionString)
    '    objConn.Open()
    '    Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
    '    Try
    '        If h_emp_ids.Value = "" Then lblError.Text = "Employee Not Selected,"
    '        If ddlLocation.SelectedValue = "" Then lblError.Text = lblError.Text & "Select Location,"


    '        If lblError.Text.Length > 0 Then

    '            Exit Sub
    '        End If

    '        Dim pParms(38) As SqlParameter
    '        pParms(1) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int, True)
    '        pParms(2) = Mainclass.CreateSqlParameter("@TCH_NO", txtTCH_No.Text, SqlDbType.VarChar)
    '        pParms(3) = Mainclass.CreateSqlParameter("@TCH_DATE", txtContractDate.Text, SqlDbType.VarChar)
    '        pParms(5) = Mainclass.CreateSqlParameter("@TCH_CIT_ID", h_Loc_id.Value, SqlDbType.VarChar)
    '        pParms(6) = Mainclass.CreateSqlParameter("@TCH_RENT", txttrent.Text, SqlDbType.Decimal)
    '        pParms(9) = Mainclass.CreateSqlParameter("@TCH_OTHERS", 0, SqlDbType.Decimal)
    '        pParms(10) = Mainclass.CreateSqlParameter("@TCH_TOTAL", txttotal.Text, SqlDbType.Decimal)
    '        pParms(18) = Mainclass.CreateSqlParameter("@TCH_RECOVERYPROCESS", txtRecovery.Text, SqlDbType.VarChar)
    '        pParms(19) = Mainclass.CreateSqlParameter("@TCH_REQUESTBY", Session("sUsr_name"), SqlDbType.VarChar)
    '        pParms(20) = Mainclass.CreateSqlParameter("@TCH_OCCUPANTS", h_emp_ids.Value, SqlDbType.VarChar)
    '        pParms(22) = Mainclass.CreateSqlParameter("@TCH_REA_ID", h_REAId.Value, SqlDbType.Int)
    '        pParms(23) = Mainclass.CreateSqlParameter("@TCH_REL_ID", h_RELId.Value, SqlDbType.Int)
    '        pParms(24) = Mainclass.CreateSqlParameter("@TCH_BSU_ID", Session("sBsuID"), SqlDbType.VarChar)
    '        pParms(25) = Mainclass.CreateSqlParameter("@TCH_USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
    '        pParms(26) = Mainclass.CreateSqlParameter("@TCH_FYEAR", h_fyear.Value, SqlDbType.VarChar)
    '        pParms(27) = Mainclass.CreateSqlParameter("@TCH_DELETED", h_delete.Value, SqlDbType.Int)
    '        pParms(29) = Mainclass.CreateSqlParameter("@TCH_BLD_ID", h_building_ID.Value, SqlDbType.VarChar)
    '        pParms(30) = Mainclass.CreateSqlParameter("@TCH_RAR_ID", h_area_ID.Value, SqlDbType.VarChar)
    '        pParms(35) = Mainclass.CreateSqlParameter("@TCH_MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
    '        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTCT_H", pParms)

    '        If RetVal = "-1" Then
    '            lblError.Text = "Unexpected Error !!!"
    '            stTrans.Rollback()
    '            Exit Sub
    '        Else
    '            ViewState("EntryId") = pParms(1).Value
    '        End If

    '        stTrans.Commit()
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '        stTrans.Rollback()
    '        Exit Sub
    '    Finally
    '        If objConn.State = ConnectionState.Open Then
    '            objConn.Close()
    '        End If
    '    End Try

    '    If ViewState("MainMnu_code") = "PI02035" Then
    '        writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), txtComments.Text, 0, "A")
    '    Else
    '        writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), txtComments.Text, 0, "N")
    '    End If

    '    SetDataMode("view")
    '    setModifyvalues(ViewState("EntryId"))
    '    lblError.Text = "Data Saved Successfully !!!"
    'End Sub
    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnSend.Visible = Not btnSave.Visible
        btnPrint.Visible = Not btnSave.Visible
        'btnReturn.Visible = Not btnSave.Visible

    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        Session("Empdata") = Nothing
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(h_EntryId.Value)
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim cmd As New SqlCommand

        cmd.CommandText = "rptHouseWaiver"

        Dim sqlParam(1) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@HWID", h_EntryId.Value, SqlDbType.Int)
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = Mainclass.CreateSqlParameter("@LOCID", h_Loc_id.Value, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(1))
        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        'V1.2 comments start------------
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")


        Dim repSource As New MyReportClass
        repSource.Command = cmd
        repSource.Parameter = params

        repSource.ResourceName = "../../Inventory/Reports/RPT/rptHouseWaiver.rpt"

        Dim subRep(0) As MyReportClass
        subRep(0) = New MyReportClass
        Dim subcmd1 As New SqlCommand
        subcmd1.CommandText = "rptTenancyPaymentDetails"
        Dim sqlParam1(0) As SqlParameter
        sqlParam1(0) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int)
        subcmd1.Parameters.Add(sqlParam1(0))
        subcmd1.CommandType = Data.CommandType.StoredProcedure
        subcmd1.Connection = New SqlConnection(str_conn)
        subRep(0).Command = subcmd1
        subRep(0).ResourceName = "wBTA_Sector"
        subRep(0).Command = subcmd1
        subRep(0).ResourceName = "wBTA_Sector"
        repSource.IncludeBSUImage = True
        repSource.SubReport = subRep
        Session("ReportSource") = repSource
        'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub



    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        doApproval()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Function doApproval(Optional ByVal THW_ID As Integer = 0) As Integer
        If THW_ID = 0 Then THW_ID = h_EntryId.Value
        Dim strApprover() As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverHW(" & THW_ID & ")").ToString.Split(";")
        doApproval = -1
        If strApprover(0) = 500 Then 'finally approved
            doApproval = writeWorkFlow(THW_ID, "Housingwaiver", "Housing Waiver:Archived", 0, "C")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_HOUSEWAIVER set THW_APR_ID=500, THW_STATUS='C', THW_STATUS_STR='Housing Waiver:Archived' where THW_ID=" & THW_ID)
            End If
        Else
            doApproval = writeWorkFlow(THW_ID, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_HOUSEWAIVER set THW_STATUS_STR='Approval:with " & strApprover(1) & "', THW_STATUS=case when THW_STATUS='N' then 'S' else THW_STATUS end, THW_APR_ID=" & strApprover(0) & " where thW_id=" & THW_ID)
            End If
        End If
    End Function
    Private Function writeWorkFlow(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_THW_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", txtApprovals.Text.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveWorkFlowHW", iParms)
        writeWorkFlow = iParms(8).Value
    End Function

    'Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
    '    'If UploadDocPhoto.FileName <> "" Then
    '    '    Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Tenancy\"
    '    '    Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
    '    '    Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename
    '    '    If ViewState("MainMnu_code") <> "PI02035" Then
    '    '        If h_THW_Status.Value = "C" Then
    '    '            lblError.Text = "upload not allowed for Archived Contracts!!!!"
    '    '            Exit Sub
    '    '        End If
    '    '    End If

    '    '    Try
    '    '        UploadDocPhoto.PostedFile.SaveAs(strFilepath)
    '    '    Catch ex As Exception
    '    '        Exit Sub
    '    '    End Try
    '    '    If Not IO.File.Exists(strFilepath) Then
    '    '        lblError.Text = "Invalid FilePath!!!!"
    '    '        Exit Sub
    '    '    Else
    '    '        Dim ContentType As String = getContentType(strFilepath)
    '    '        Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
    '    '        If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
    '    '        If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
    '    '        Dim sqlParam(6) As SqlParameter
    '    '        sqlParam(0) = Mainclass.CreateSqlParameter("@TCI_ID", 0, SqlDbType.Int, True)
    '    '        sqlParam(1) = Mainclass.CreateSqlParameter("@TCI_TCH_ID", h_EntryId.Value, SqlDbType.Int)
    '    '        sqlParam(2) = Mainclass.CreateSqlParameter("@TCI_CONTENTTYPE", ContentType, SqlDbType.VarChar)
    '    '        sqlParam(3) = Mainclass.CreateSqlParameter("@TCI_DOCTYPE", ddlDocType.SelectedValue, SqlDbType.Int)
    '    '        sqlParam(4) = Mainclass.CreateSqlParameter("@TCI_NAME", UploadDocPhoto.FileName, SqlDbType.VarChar)
    '    '        sqlParam(5) = Mainclass.CreateSqlParameter("@TCI_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
    '    '        sqlParam(6) = Mainclass.CreateSqlParameter("@TCI_FILENAME", txtTCH_No.Text & "_~" & FileExt, SqlDbType.VarChar)
    '    '        Try
    '    '            SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveTCT_I", sqlParam)
    '    '            IO.File.Move(strFilepath, str_img & txtTCH_No.Text & "_" & sqlParam(0).Value & FileExt)
    '    '            showImages()
    '    '            writeWorkFlow(ViewState("EntryId"), "ADDEDIMG", "Added Contract " & UploadDocPhoto.FileName, 0, "O")
    '    '        Catch ex As Exception

    '    '        End Try
    '    '    End If
    '    'ElseIf Not ViewState("imgAsset") Is Nothing Then

    '    'Else
    '    '    ViewState("imgAsset") = Nothing
    '    'End If
    '    'trComments.Focus()
    'End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select

    End Function
    'Protected Sub rptImages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptImages.ItemCommand
    '    If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
    '        If e.CommandSource.text = "[x]" Then
    '            If h_THW_Status.Value = "C" Or h_THW_Status.Value = "R" Or h_THW_Status.Value = "D" Then
    '                lblError.Text = "Cannot delete file"
    '            Else
    '                Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete"), LinkButton)
    '                Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
    '                Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
    '                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from tct_i where tci_id=" & lblPriId.Text)
    '                writeWorkFlow(ViewState("EntryId"), "DELETEDIMG", "Deleted Contract " & btnDocumentLink.Text, 0, "O")
    '                showImages()
    '            End If
    '        End If
    '    End If
    'End Sub
    'Protected Sub rptImages_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptImages.ItemDataBound
    '    If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
    '        Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
    '        Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
    '        Dim lblPriFilename As TextBox = CType(e.Item.FindControl("lblPriFilename"), TextBox)
    '        Dim lblPriContentType As TextBox = CType(e.Item.FindControl("lblPriContentType"), TextBox)
    '        btnDocumentLink.Attributes.Add("OnClick", "javascript: showDocument(" & lblPriId.Text & ",'" & lblPriFilename.Text & "','" & lblPriContentType.Text & "'); return false;")
    '    End If
    'End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Rejection Comments are Mandatory"
            Exit Sub
        End If
        If writeWorkFlow(ViewState("EntryId"), "Rejected", txtApprovals.Text, h_ThW_apr_id.Value, "R") = 0 Then
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_STATUS_STR='Rejected:by " & Session("sUsr_name") & "', TCH_STATUS='R' where TCH_ID=" & h_EntryId.Value)
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If Tr4.Visible Then
        If (txtRent.Text = "" Or Val(txtRent.Text) <= 0) Then
            lblError.Text = "enter Rent Amount"
            Exit Sub
        End If

        If IsNumeric(txtRent.Text) = False Then
            lblError.Text = "enter numbers only"
            Exit Sub
        End If


        If doApproval() = 0 Then
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        '    End If

    End Sub
    'Protected Sub btnMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMessage.Click
    '    If txtApprovals.Text.Trim = "" Then
    '        lblError.Text = "Message comments are Mandatory"
    '        Exit Sub
    '    End If
    '    writeWorkFlow(ViewState("EntryId"), "Message", txtApprovals.Text, h_ThW_apr_id.Value, "M")
    '    Response.Redirect(ViewState("ReferrerUrl"))
    'End Sub



    Protected Sub btnRecall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecall.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update TCT_H set TCH_DELETED=0, TCH_STATUS='N', TCH_STATUS_STR='RECALL', TCH_APR_ID=0 where TCH_ID=@TCH_ID"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                writeWorkFlow(ViewState("EntryId"), "Recalled", txtApprovals.Text, 0, "E")
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    'Protected Sub btnUpload1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload1.Click
    '    If UploadDocument.FileName <> "" Then
    '        Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Tenancy\"
    '        Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocument.FileName
    '        Dim strFilepath As String = str_img & str_tempfilename
    '        If ViewState("MainMnu_code") <> "PI02035" Then
    '            If h_THW_Status.Value <> "C" Then
    '                lblError.Text = "upload not allowed for Non Archived Contracts!!!!"
    '                Exit Sub
    '            End If
    '        End If

    '        Try
    '            UploadDocument.PostedFile.SaveAs(strFilepath)
    '        Catch ex As Exception
    '            Exit Sub
    '        End Try
    '        If Not IO.File.Exists(strFilepath) Then
    '            lblError.Text = "Invalid FilePath!!!!"
    '            Exit Sub
    '        Else
    '            Dim ContentType As String = getContentType(strFilepath)
    '            Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
    '            If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
    '            If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
    '            Dim sqlParam(6) As SqlParameter
    '            sqlParam(0) = Mainclass.CreateSqlParameter("@TCI_ID", 0, SqlDbType.Int, True)
    '            sqlParam(1) = Mainclass.CreateSqlParameter("@TCI_TCH_ID", h_EntryId.Value, SqlDbType.Int)
    '            sqlParam(2) = Mainclass.CreateSqlParameter("@TCI_CONTENTTYPE", ContentType, SqlDbType.VarChar)
    '            sqlParam(3) = Mainclass.CreateSqlParameter("@TCI_DOCTYPE", ddlUploadType.SelectedValue, SqlDbType.Int)
    '            sqlParam(4) = Mainclass.CreateSqlParameter("@TCI_NAME", UploadDocument.FileName, SqlDbType.VarChar)
    '            sqlParam(5) = Mainclass.CreateSqlParameter("@TCI_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
    '            sqlParam(6) = Mainclass.CreateSqlParameter("@TCI_FILENAME", txtTCH_No.Text & "_~" & FileExt, SqlDbType.VarChar)
    '            Try
    '                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveTCT_I", sqlParam)
    '                IO.File.Move(strFilepath, str_img & txtTCH_No.Text & "_" & sqlParam(0).Value & FileExt)
    '                showImagesDocument()
    '                writeWorkFlow(ViewState("EntryId"), "ADDEDIMG", "Added Contract " & UploadDocument.FileName, 0, "O")
    '            Catch ex As Exception

    '            End Try
    '        End If
    '    ElseIf Not ViewState("imgAsset") Is Nothing Then

    '    Else
    '        ViewState("imgAsset") = Nothing
    '    End If

    'End Sub
    'Protected Sub rptDocument_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDocument.ItemCommand
    '    If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
    '        If e.CommandSource.text = "[x]" Then
    '            If h_THW_Status.Value = "C" Or h_THW_Status.Value = "R" Or h_THW_Status.Value = "D" Then
    '                lblError.Text = "Cannot delete file"
    '            Else
    '                Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete1"), LinkButton)
    '                Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriDocumentId"), TextBox)
    '                Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink1"), LinkButton)
    '                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from tct_i where tci_id=" & lblPriId.Text)
    '                writeWorkFlow(ViewState("EntryId"), "DELETEDIMG", "Deleted Contract " & btnDocumentLink.Text, 0, "O")
    '                showImagesDocument()
    '            End If
    '        End If
    '    End If
    'End Sub
    'Protected Sub rptDocument_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDocument.ItemDataBound
    '    If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
    '        Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink1"), LinkButton)
    '        Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriDocumentId"), TextBox)
    '        Dim lblPriFilename As TextBox = CType(e.Item.FindControl("lblPriDocumentFilename"), TextBox)
    '        Dim lblPriContentType As TextBox = CType(e.Item.FindControl("lblPriDocumentContentType"), TextBox)
    '        btnDocumentLink.Attributes.Add("OnClick", "javascript: showDocument(" & lblPriId.Text & ",'" & lblPriFilename.Text & "','" & lblPriContentType.Text & "'); return false;")
    '    End If
    'End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub txtRent_TextChanged(sender As Object, e As EventArgs) Handles txtRent.TextChanged
        If txtRent.Text <> "" Then
            txtSchoolCost.Text = Val(txtAllow.Text) - Val(txtRent.Text)
        End If
    End Sub
End Class
