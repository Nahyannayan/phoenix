﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Rebuilding.aspx.vb" Inherits="Inventory_Rebuilding" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

       <%-- function getArea() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            pMode = "REAREA"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtArea.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=h_Area_ID.ClientID %>").value = NameandCode[0];
        }--%>

        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
        function getAreaNew() {
            var pMode;
            var url;
            pMode = "REAREA"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url,"pop_area");
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
        if (arg) {

            NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_area_id.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtArea.ClientID%>').value = NameandCode[1];
                // __doPostBack('<%= txtArea.ClientID%>', 'TextChanged');
            }
              }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_area" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Building Master
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" style="width: 100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="matters" style="height: 93px; font-weight: bold;" valign="top">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Building Name<span style="color: #800000">*</span></span></td>
                                    <td class="matters" width="30%">
                                        <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox></td>
                                    <td align="left" class="matters" rowspan="3" valign="top"><span class="field-label">Address</span></td>
                                    <td class="matters" align="left" rowspan="3" valign="top">
                                        <asp:TextBox ID="txtAddress"
                                            runat="server" Height="200px"
                                            TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Area<span style="color: #800000">*</span></span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtArea" runat="server" CssClass="inputbox" TabIndex="23"></asp:TextBox>
                                        <asp:ImageButton ID="imgArea" Visible="true" runat="server" OnClientClick="getAreaNew();return false;"
                                            ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">D.M.No<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtdmno" runat="server" CssClass="inputbox"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="h_area_id" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
