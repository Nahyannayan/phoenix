﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ReLandlord.aspx.vb" Inherits="Inventory_reLandlord" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
       <%-- function getMainLandlords() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            pMode = "RELANDLORD"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&BSUID=999998";
            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtMainLandlord.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=h_REL_REL_ID.ClientID %>").value = NameandCode[0];
        }--%>

        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

    </script>

     <script>
   
         function getMainLandlordsNew() {
            var pMode;
            var NameandCode;

            pMode = "RELANDLORD"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&BSUID=999998";
            var oWnd = radopen(url, "pop_landlord");
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
            document.getElementById("<%=txtMainLandlord.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=h_REL_REL_ID.ClientID %>").value = NameandCode[0];
        }
    }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_landlord" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>




    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Real Estate Landlord"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td style="width: 100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Link Landlord<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:TextBox ID="txtMainLandlord" Enabled="false"   runat="server"   ></asp:TextBox>
                                        <asp:ImageButton ID="imgLandlord" Visible="true" runat="server" OnClientClick="getMainLandlordsNew();return false;"
                                            ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                    <td width="20%">

                                    </td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Landlord Name<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:TextBox ID="txtLandlord" runat="server" ></asp:TextBox></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">PayName</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtPayname" runat="server"  ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Mobile Number</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtMobile"   runat="server"  ></asp:TextBox></td>
                                    <td align="left" class="matters"><span class="field-label">Office Number<span style="color: #800000">*</span></span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtOffice" runat="server"  ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Fax</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtFax" Enabled="false"  runat="server" ></asp:TextBox></td>
                                    <td class="matters" align="left"><span class="field-label">P.O.Box</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtPoBOX" runat="server"   ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Account No.</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtAcno" Enabled="false"  runat="server"  ></asp:TextBox></td>
                                    <td class="matters" align="left"><span class="field-label">Email<span style="color: #800000">*</span></span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtEmail" runat="server"  ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Details</span></td>
                                    <td align="left" class="matters" colspan="3">
                                        <asp:TextBox ID="txtDetails"   TextMode="MultiLine"   runat="server" TabIndex="23" Width="93%"  ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"  align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="h_REL_REL_ID" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
