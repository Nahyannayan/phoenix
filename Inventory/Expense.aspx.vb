﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Net
Imports System.Xml
Imports System.Web.Services
Imports System.Collections.Generic

Partial Class Inventory_Expense
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass(), requestForQuotation As Boolean = False
    Dim Discounted As Boolean = True
    Dim ESNo As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnUpload)

            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim USR_NAME As String = Session("sUsr_name")
                Dim CurBsUnit As String = Session("sBsuid")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "PI02023" And ViewState("MainMnu_code") <> "PI02025" And ViewState("MainMnu_code") <> "PI04011") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If

                BindPurpose()
                isAdvance = ViewState("MainMnu_code") = "PI02023" Or ViewState("MainMnu_code") = "PI04011"
                If isAdvance Then
                    rptHeader.Text = "Advances for Expenses / Activities"
                Else
                    rptHeader.Text = "Expense Settlement"
                End If

                If ViewState("EntryId") = "0" Then
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                Else
                    If ViewState("datamode") = "add" Then
                        SetDataMode("add")
                        setModifyvalues(ViewState("EntryId"))
                    Else
                        SetDataMode("view")
                        setModifyvalues(ViewState("EntryId"))
                        If Not isAdvance Then
                            grdInvoice.DataSource = TblExpense
                            grdInvoice.DataBind()
                            showNoRecordsFound()
                        End If
                    End If
                End If
            End If

            If Not isAdvance Then
                If hGridRefresh.Value = 1 Then
                    grdInvoice.DataSource = TblExpense
                    grdInvoice.DataBind()
                    hGridRefresh.Value = 0
                End If
                showNoRecordsFound()
                hGridRefresh.Value = 0
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        
        grdInvoice.ShowFooter = Not mDisable
        txtEmployee.Enabled = EditAllowed And txtEmployee.Text.Length = 0
        txtARDate.Enabled = EditAllowed
        txtSettleDate.Enabled = EditAllowed
        txtAmount.Enabled = EditAllowed
        txtComments.Enabled = EditAllowed
        imgSettle_Date.Enabled = EditAllowed
        ImgARDate.Enabled = EditAllowed
        radBudgeted.Enabled = EditAllowed
        radUnBudgeted.Enabled = EditAllowed
        ddlPurpose.Enabled = EditAllowed
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        btnEdit.Visible = mDisable
        btnPrint.Visible = Not btnSave.Visible
        btnAdd.Visible = mDisable And Not ViewState("MainMnu_code") = "PI04011"
        btnAccept.Visible = mDisable And ViewState("MainMnu_code") = "PI04011" 'approval menu
        btnReject.Visible = btnAccept.Visible
        btnMessage.Visible = btnAccept.Visible
        imgClient.Visible = ViewState("datamode") = "add"
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim sqlStr As New StringBuilder
            Dim sqlWhere As String = p_Modifyid
            h_EntryId.Value = p_Modifyid

            If isAdvance Then
                ESROW.Visible = False
                trTotal.Visible = False

                ESRemarks.Visible = False
                ExpDetails.Visible = False
                ExpDetailsGrid.Visible = False
                lblStatus.Visible = False
            End If

            txtAmount.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
            If p_Modifyid = 0 Then
                trApproval.Visible = False
                trWorkflow.Visible = False
                trFlow.Visible = False
                trDocument.HeaderText = ""
                trApproval.HeaderText = ""
                trWorkflow.HeaderText = ""
                trFlow.HeaderText = ""
                trDocument.Visible = False

                Dim dt As New DataTable
                sqlStr = New StringBuilder
                sqlStr.Append("SELECT EMP_ID, isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') NAME,DPT_DESCR [DEPARTMENT] ")
                sqlStr.Append("From OASIS..USERS_M inner join OASIS..EMPLOYEE_M on USR_EMP_ID=EMP_ID INNER join OASIS..DEPARTMENT_M ")
                sqlStr.Append("ON DPT_ID=EMP_DPT_ID WHERE USR_NAME='" & Session("sUsr_name") & "'")
                dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")

                h_empid.Value = dt.Rows(0)("EMP_ID")
                txtEmployee.Text = dt.Rows(0)("NAME")
                txtEmpDept.Text = dt.Rows(0)("DEPARTMENT")
            Else
                If isAdvance Then
                    Dim str_conn As String = connectionString
                    Dim dt As New DataTable
                    sqlStr = New StringBuilder
                    sqlStr.Append("SELECT ARF_ID,ARF_NO, ARF_DATE, ARF_REQDATE, ARF_PUR_ID, ARF_SETDATE, ARF_BUDGETED, ARF_AMOUNT, ARF_REMARKS, ARF_BSU_ID, ARF_FYEAR, ARF_EMP_ID, ARF_APR_ID, ")
                    sqlStr.Append("isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') NAME,DPT_DESCR [DEPARTMENT],ARF_STATUS,case when isnull(ARF_JHD_DOCNO,'')='' then 'Not Posted' else 'Posted' end Status  from ARF ")
                    sqlStr.Append("INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID ")
                    sqlStr.Append("INNER join OASIS..DEPARTMENT_M  ON DPT_ID =EMP_DPT_ID WHERE ARF_ID=" & p_Modifyid)
                    dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")

                    If dt.Rows.Count > 0 Then
                        h_EntryId.Value = dt.Rows(0)("ARF_ID")
                        h_Arf_Id.Value = dt.Rows(0)("ARF_ID")
                        h_empid.Value = dt.Rows(0)("ARF_EMP_ID")
                        h_ARF_STATUS.Value = dt.Rows(0)("ARF_STATUS")
                        txtArNo.Text = dt.Rows(0)("ARF_NO")
                        txtComments.Text = dt.Rows(0)("ARF_REMARKS")
                        txtTrDate.Text = Format(dt.Rows(0)("ARF_DATE"), "dd-MMM-yyyy")
                        txtARDate.Text = Format(dt.Rows(0)("ARF_REQDATE"), "dd-MMM-yyyy")
                        txtEmployee.Text = dt.Rows(0)("NAME")
                        txtEmpDept.Text = dt.Rows(0)("DEPARTMENT")
                        txtSettleDate.Text = Format(dt.Rows(0)("ARF_SETDATE"), "dd-MMM-yyyy")
                        radBudgeted.Checked = (dt.Rows(0)("ARF_BUDGETED") = "1")
                        radUnBudgeted.Checked = (dt.Rows(0)("ARF_BUDGETED") = "0")
                        txtAmount.Text = dt.Rows(0)("ARF_AMOUNT")
                        arf_Apr_id = dt.Rows(0)("ARF_APR_ID")
                        ddlPurpose.SelectedValue = dt.Rows(0)("ARF_PUR_ID")

                        btnAdd.Visible = Not (dt.Rows(0)("ARF_STATUS") = "C" Or dt.Rows(0)("ARF_STATUS") = "S" Or dt.Rows(0)("ARF_STATUS") = "P")
                        btnEdit.Visible = btnAdd.Visible
                        btnClose.Visible = dt.Rows(0)("ARF_STATUS") = "A"
                        btnDelete.Visible = dt.Rows(0)("ARF_STATUS") = "N"
                        btnAccept.Visible = btnAccept.Visible And dt.Rows(0)("ARF_STATUS") = "S"
                        btnReject.Visible = btnAccept.Visible
                        btnSend.Visible = dt.Rows(0)("ARF_STATUS") = "N"
                        btnSave.Visible = dt.Rows(0)("ARF_STATUS") = "P"
                        lblPayment.Visible = dt.Rows(0)("ARF_STATUS") = "P"
                        txtPayment.Visible = lblPayment.Visible
                        imgPayment.Visible = lblPayment.Visible
                    Else
                        Response.Redirect(ViewState("ReferrerUrl"))
                    End If
                Else
                    If ViewState("datamode") = "add" Then
                        Dim str_conn As String = connectionString
                        Dim dt As New DataTable
                        sqlStr = New StringBuilder
                        sqlStr.Append("SELECT ARF_ID,ARF_NO, ARF_DATE, ARF_REQDATE, ARF_PUR_ID, ARF_SETDATE, ARF_BUDGETED, ARF_AMOUNT, ARF_REMARKS, ARF_BSU_ID, ARF_FYEAR, ARF_EMP_ID, ")
                        sqlStr.Append("isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') NAME,DPT_DESCR [DEPARTMENT],isnull(C.TOTAL,0) AMT_SETTLED, ARF_STATUS from ARF ")
                        sqlStr.Append("INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID ")
                        sqlStr.Append("INNER join OASIS..DEPARTMENT_M  ON DPT_ID =EMP_DPT_ID ")
                        sqlStr.Append("LEFT OUTER JOIN(SELECT SUM(ESH_TOTAL) TOTAL,ESH_ARF_ID ID FROM ESF_H GROUP BY ESH_ARF_ID)C ON C.ID=ARF_ID ")
                        sqlStr.Append(" WHERE ARF_ID = " & p_Modifyid)
                        dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")
                        If dt.Rows.Count > 0 Then
                            If ESNo = 0 Then
                                txtEsNo.Text = "NEW"
                            Else
                                txtEsNo.Text = ESNo
                            End If
                            txtESDate.Text = Now.ToString("dd/MMM/yyyy")
                            h_EntryId.Value = 0
                            h_Arf_Id.Value = dt.Rows(0)("ARF_ID")
                            h_empid.Value = dt.Rows(0)("ARF_EMP_ID")
                            txtArNo.Text = dt.Rows(0)("ARF_NO")
                            txtComments.Text = dt.Rows(0)("ARF_REMARKS")
                            txtTrDate.Text = Format(dt.Rows(0)("ARF_DATE"), "dd-MMM-yyyy")
                            txtARDate.Text = Format(dt.Rows(0)("ARF_REQDATE"), "dd-MMM-yyyy")
                            txtEmployee.Text = dt.Rows(0)("NAME")
                            txtEmpDept.Text = dt.Rows(0)("DEPARTMENT")
                            txtSettleDate.Text = Format(dt.Rows(0)("ARF_SETDATE"), "dd-MMM-yyyy")
                            radBudgeted.Checked = (dt.Rows(0)("ARF_BUDGETED") = "1")
                            radUnBudgeted.Checked = (dt.Rows(0)("ARF_BUDGETED") = "0")
                            txtAmount.Text = dt.Rows(0)("ARF_AMOUNT")
                            ddlPurpose.SelectedValue = dt.Rows(0)("ARF_PUR_ID")
                            txtAmtSettle.Text = dt.Rows(0)("AMT_SETTLED")
                            DisableControls()
                            btnClose.Visible = dt.Rows(0)("ARF_STATUS") = "S"
                        Else
                            Response.Redirect(ViewState("ReferrerUrl"))
                        End If
                    Else
                        Dim str_conn As String = connectionString
                        Dim dt As New DataTable
                        sqlStr = New StringBuilder
                        sqlStr.Append("select ESH_ID,ESH_NO,ESH_DATE,ESH_ARF_NO,ESH_ARF_ID,ESH_ARF_DATE,ESH_REMARKS,ESH_TOTAL,")
                        sqlStr.Append("ARF_EMP_ID,ARF_REMARKS,ARF_REQDATE,ARF_SETDATE,ARF_BUDGETED,ARF_AMOUNT,ARF_PUR_ID, ")
                        sqlStr.Append("ARF_REMARKS,isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') NAME,DPT_DESCR [DEPARTMENT],isnull(C.TOTAL,0) AMT_SETTLED,ESH_STATUS from ESF_H  ")
                        sqlStr.Append("LEFT OUTER JOIN ARF ON ARF_ID=ESH_ARF_ID INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID INNER join OASIS..DEPARTMENT_M  ON DPT_ID =EMP_DPT_Id ")
                        sqlStr.Append("LEFT OUTER JOIN(SELECT SUM(ESH_TOTAL) TOTAL,ESH_ARF_ID ID FROM ESF_H GROUP BY ESH_ARF_ID)C ON C.ID=ESH_ARF_ID ")
                        sqlStr.Append(" WHERE ESH_ID=" & p_Modifyid)
                        dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")

                        If dt.Rows.Count > 0 Then
                            h_EntryId.Value = dt.Rows(0)("ESH_ID")
                            txtEsNo.Text = dt.Rows(0)("ESH_NO")
                            txtESDate.Text = Format(dt.Rows(0)("ESH_DATE"), "dd-MMM-yyyy")
                            h_empid.Value = dt.Rows(0)("ARF_EMP_ID")
                            txtArNo.Text = dt.Rows(0)("ESH_ARF_NO")
                            h_Arf_Id.Value = dt.Rows(0)("ESH_ARF_ID")
                            txtComments.Text = dt.Rows(0)("ARF_REMARKS")
                            txtRemarks.Text = dt.Rows(0)("ESH_REMARKS")
                            txtTrDate.Text = Format(dt.Rows(0)("ESH_ARF_DATE"), "dd-MMM-yyyy")
                            txtARDate.Text = Format(dt.Rows(0)("ARF_REQDATE"), "dd-MMM-yyyy")
                            txtEmployee.Text = dt.Rows(0)("NAME")
                            txtEmpDept.Text = dt.Rows(0)("DEPARTMENT")
                            txtSettleDate.Text = Format(dt.Rows(0)("ARF_SETDATE"), "dd-MMM-yyyy")
                            radBudgeted.Checked = (dt.Rows(0)("ARF_BUDGETED") = "1")
                            radUnBudgeted.Checked = (dt.Rows(0)("ARF_BUDGETED") = "")
                            txtAmount.Text = dt.Rows(0)("ARF_AMOUNT")
                            ddlPurpose.SelectedValue = dt.Rows(0)("ARF_PUR_ID")
                            txtAmtSettle.Text = dt.Rows(0)("AMT_SETTLED")
                            DisableControls()

                            If dt.Rows(0)("ESH_STATUS") = "P" Or dt.Rows(0)("ESH_STATUS") = "S" Then
                                grdInvoice.Columns(7).Visible = False
                                grdInvoice.Columns(8).Visible = False
                                btnEdit.Visible = False
                                btnDelete.Visible = False
                                If dt.Rows(0)("ESH_STATUS") = "P" Then
                                    lblStatus.Text = "Posted"
                                End If
                            Else
                                grdInvoice.Columns(7).Visible = True
                                grdInvoice.Columns(8).Visible = True
                                If dt.Rows(0)("ESH_STATUS") = "A" Then
                                    lblStatus.Text = "Not Posted"
                                Else
                                    lblStatus.Text = ""
                                End If
                            End If
                        Else
                            Response.Redirect(ViewState("ReferrerUrl"))
                        End If
                    End If
                End If
            End If
            If p_Modifyid > 0 Then
                If ViewState("MainMnu_code") = "PI04011" Then
                    trApproval.Visible = True
                    trApproval.HeaderText = "Approval\Rejection Comments"
                End If

                Dim ds As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "workflowqueryARF " & h_EntryId.Value).Tables(0)
                rptFlow.DataSource = ds
                rptFlow.DataBind()
                trFlow.Visible = True
                trFlow.HeaderText = "Workflow progress"

                trWorkflow.Visible = True
                trWorkflow.HeaderText = "Workflow Details"
                trDocument.Visible = True
                trDocument.HeaderText = "Documents"

                If btnMessage.Visible Then
                    trApproval.Visible = True
                    If ViewState("MainMnu_code") = "PI04011" Then
                        trApproval.HeaderText = "Approval\Rejection Comments"
                    Else
                        trApproval.HeaderText = "Message Comments"
                    End If
                End If

                sqlStr = New StringBuilder
                sqlStr.Append("select replace(convert(varchar(16), WRK_DATE, 106),' ','/') WRK_DATE, USR_DISPLAY_NAME WRK_USER, WRK_ACTION, WRK_DETAILS from WORKFLOWARF inner JOIN OASIS.dbo.USERS_M on WRK_USERNAME=USR_NAME where WRK_ARF_ID=" & h_EntryId.Value & " order by WRK_ID desc")
                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr.ToString).Tables(0)
                gvWorkflow.DataSource = ds
                gvWorkflow.DataBind()

                showImages()
                If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from ARF INNER JOIN GROUPSA on GRPA_ID=ARF_APR_ID and GRPA_DESCR='Accounts Officer' and ARF_ID=" & p_Modifyid) = 1 Then
                    radBudgeted.Enabled = True
                    radUnBudgeted.Enabled = True
                End If
            End If

            FormatFigures()
            If Not isAdvance Then updateWorkflow()

        Catch ex As Exception
            Errorlog(ex.Message)
            If Not (ex.Message.Contains("Thread was being aborted") Or ex.Message.Contains("Timeout expired")) Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "insert into [OASIS].[dbo].[COM_EMAIL_ALERTS] (log_type, log_stu_id, log_fromemail, log_toemail, log_subject, log_message, log_date, log_username, log_password, log_host, log_port) SELECT 'ACU', 17973, 'communication@gemsedu.com', 'sandip.kapil@gemseducation.com', 'Error in Purchase load', 'BSU:" & Session("sBsuid") & ", User:" & Session("sUsr_name") & ", message:" & ex.Message.Replace("'", "`") & "', getdate(),'communication', 'gems123', '172.16.3.10', 25 ")
            End If
        End Try
    End Sub

    Private Sub showImages()
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select ari_id, ari_name, ari_filename, ari_contenttype, case when ari_username='" & Session("sUsr_name") & "' then 'true' else 'false' end ari_visible from ARF_I where ARI_ARF_ID=" & ViewState("EntryId")).Tables(0)
        rptImages.DataSource = dt
        rptImages.DataBind()
    End Sub

    Private Sub BindPurpose()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT PUR_ID,PUR_DESCR FROM ADVREQPURPOSE"
            'ds = 
            ddlPurpose.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlPurpose.DataTextField = "PUR_DESCR"
            ddlPurpose.DataValueField = "PUR_ID"
            ddlPurpose.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub DisableControls()
        txtArNo.Enabled = False
        txtComments.Enabled = False
        txtTrDate.Enabled = False
        txtARDate.Enabled = False
        txtEmployee.Enabled = False
        txtEmpDept.Enabled = False
        txtSettleDate.Enabled = False
        radBudgeted.Enabled = False
        radUnBudgeted.Enabled = False
        txtAmount.Enabled = False
        ddlPurpose.Enabled = False
        imgClient.Enabled = False
        ImgARDate.Enabled = False
        imgSettle_Date.Enabled = False
        txtESDate.Enabled = False
        ddlPurpose.Enabled = False
        txtAmtSettle.Enabled = False
        If ViewState("datamode") <> "add" Then
            imgESDate.Enabled = False
            txtESDate.Enabled = False
        End If

    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        h_Arf_Id.Value = "0"
        h_empid.Value = Session("EmployeeId")
        txtArNo.Text = "NEW"
        txtEsNo.Text = "NEW"
        txtESDate.Text = Now.ToString("dd/MMM/yyyy")
        txtTrDate.Text = Now.ToString("dd/MMM/yyyy")
        txtARDate.Text = DateAdd(DateInterval.Day, 2, Now).ToString("dd/MMM/yyyy")
        txtSettleDate.Text = DateAdd(DateInterval.Day, 14, Now).ToString("dd/MMM/yyyy")
        txtEmployee.Text = ""
        txtBillingTotal.Text = "0.0"
        txtAmount.Text = "0.0"
        radBudgeted.Checked = True
        txtComments.Text = ""
        txtEmpDept.Text = ""
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If Not isAdvance Then
            ClearDetails()
            setModifyvalues(0)
            SetDataMode("add")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            grdInvoice.DataSource = TblExpense
            grdInvoice.DataBind()
            showNoRecordsFound()
        Else
            If ViewState("datamode") = "edit" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If Not isAdvance Then
            ItemEditMode = False
            SetDataMode("edit")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            grdInvoice.DataSource = TblExpense
            grdInvoice.DataBind()
            showNoRecordsFound()
        Else
            If ViewState("datamode") = "view" Then
                SetDataMode("edit")
                setModifyvalues(ViewState("EntryId"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim newARF As Boolean = (h_EntryId.Value = 0)
        If txtPayment.Visible Then
            If txtPayment.Text = "" Then
                lblError.Text = "Select Payment details"
                Exit Sub
            End If
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update ARF set ARF_STATUS_STR='Payments Processed', ARF_STATUS='S' where ARF_ID=" & h_EntryId.Value)
            writeWorkFlow(ViewState("EntryId"), "Payment", txtPayment.Text, 0, "P")
        Else
            lblError.Text = ""
            If Not IsNumeric(txtAmount.Text) Or txtAmount.Text = "" Or Val(txtAmount.Text) <= 0 Then lblError.Text = "Enter Valid Amount"
            If txtEmployee.Text.ToString.Length = 0 Then lblError.Text = lblError.Text & ",Select employee"

            If lblError.Text.Length > 0 Then
                If Not isAdvance Then showNoRecordsFound()
                Exit Sub
            End If

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

            If isAdvance Then
                Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
                Dim pParms(12) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@ARF_ID", h_EntryId.Value, SqlDbType.Int, True)
                pParms(2) = Mainclass.CreateSqlParameter("@ARF_DATE", txtTrDate.Text, SqlDbType.VarChar)
                pParms(3) = Mainclass.CreateSqlParameter("@ARF_REQDATE", txtARDate.Text, SqlDbType.VarChar)
                pParms(4) = Mainclass.CreateSqlParameter("@ARF_PUR_ID", ddlPurpose.SelectedValue, SqlDbType.Int)
                pParms(5) = Mainclass.CreateSqlParameter("@ARF_SETDATE", txtSettleDate.Text, SqlDbType.VarChar)
                If radBudgeted.Checked = True Then
                    pParms(6) = Mainclass.CreateSqlParameter("@ARF_BUDGETED", "1", SqlDbType.VarChar)
                ElseIf radUnBudgeted.Checked = True Then
                    pParms(6) = Mainclass.CreateSqlParameter("@ARF_BUDGETED", "0", SqlDbType.VarChar)
                End If
                pParms(7) = Mainclass.CreateSqlParameter("@ARF_AMOUNT", txtAmount.Text, SqlDbType.Float)
                pParms(8) = Mainclass.CreateSqlParameter("@ARF_REMARKS", txtComments.Text, SqlDbType.VarChar)
                pParms(9) = Mainclass.CreateSqlParameter("@ARF_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                pParms(10) = Mainclass.CreateSqlParameter("@ARF_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                pParms(11) = Mainclass.CreateSqlParameter("@ARF_EMP_ID", h_empid.Value, SqlDbType.Int)
                pParms(12) = Mainclass.CreateSqlParameter("@ARF_USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveARF", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    ViewState("EntryId") = pParms(1).Value
                    stTrans.Commit()
                End If
            Else
                Try
                    Dim pParms(15) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@ESH_ID", h_EntryId.Value, SqlDbType.Int, True)
                    pParms(2) = Mainclass.CreateSqlParameter("@ESH_NO", txtEsNo.Text, SqlDbType.VarChar)
                    pParms(3) = Mainclass.CreateSqlParameter("@ESH_DATE", txtESDate.Text, SqlDbType.VarChar)
                    pParms(4) = Mainclass.CreateSqlParameter("@ESH_ARF_NO", txtArNo.Text, SqlDbType.VarChar)
                    pParms(5) = Mainclass.CreateSqlParameter("@ESH_ARF_ID", h_Arf_Id.Value, SqlDbType.Int)
                    pParms(6) = Mainclass.CreateSqlParameter("@ESH_ARF_DATE", txtARDate.Text, SqlDbType.VarChar)
                    pParms(7) = Mainclass.CreateSqlParameter("@ESH_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
                    pParms(8) = Mainclass.CreateSqlParameter("@ESH_TOTAL", txtBillingTotal.Text, SqlDbType.Decimal)
                    pParms(9) = Mainclass.CreateSqlParameter("@ESH_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    pParms(10) = Mainclass.CreateSqlParameter("@ESH_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    pParms(11) = Mainclass.CreateSqlParameter("@ESH_STATUS", "N", SqlDbType.VarChar)
                    pParms(12) = Mainclass.CreateSqlParameter("@ESH_APR_ID", 0, SqlDbType.Int)
                    pParms(13) = Mainclass.CreateSqlParameter("@ESH_STATUS_STR", "New", SqlDbType.VarChar)
                    pParms(14) = Mainclass.CreateSqlParameter("@ESH_DELETED", 0, SqlDbType.Int)
                    pParms(15) = Mainclass.CreateSqlParameter("@ESH_USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                    Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveESF_H", pParms)

                    If RetVal = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    Else
                        ViewState("EntryId") = pParms(1).Value
                        Session("ESH_ID") = pParms(1).Value
                    End If

                    Dim RowCount As Integer, InsCount As Integer = 0
                    For RowCount = 0 To TblExpense.Rows.Count - 1
                        Dim iParms(7) As SqlClient.SqlParameter
                        Dim rowState As Integer = ViewState("EntryId")
                        Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, TblExpense.Rows(RowCount)("ESD_ID"))
                        If TblExpense.Rows(RowCount).RowState = 8 Then rowState = -1
                        iParms(1) = Mainclass.CreateSqlParameter("@ESD_ID", RowEntryId, SqlDbType.Int)
                        iParms(2) = Mainclass.CreateSqlParameter("@ESD_ESH_ID", rowState, SqlDbType.Int)
                        iParms(3) = Mainclass.CreateSqlParameter("@ESD_EEF_ID", TblExpense.Rows(RowCount)("ESD_EEF_ID"), SqlDbType.VarChar)
                        iParms(4) = Mainclass.CreateSqlParameter("@ESD_DETAILS", TblExpense.Rows(RowCount)("ESD_DETAILS"), SqlDbType.VarChar)
                        iParms(5) = Mainclass.CreateSqlParameter("@ESD_INVOICE", TblExpense.Rows(RowCount)("ESD_INVOICE"), SqlDbType.VarChar)
                        iParms(6) = Mainclass.CreateSqlParameter("@ESD_AMOUNT", TblExpense.Rows(RowCount)("ESD_AMOUNT"), SqlDbType.Decimal)
                        If Not IsDBNull(TblExpense.Rows(RowCount)("ESD_EEF_ID")) AndAlso TblExpense.Rows(RowCount)("ESD_EEF_ID") > 0 Then
                            InsCount += 1
                            Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveESF_D", iParms)
                            If RetValFooter = "-1" Then
                                lblError.Text = "Unexpected Error !!!"
                                stTrans.Rollback()
                                Exit Sub
                            End If
                        End If
                    Next

                    If Not isAdvance And InsCount <= 0 Then
                        lblError.Text = "ARF should contain atleast 1 item"
                        stTrans.Rollback()
                        showNoRecordsFound()
                        Exit Sub
                    Else
                        If hGridDelete.Value <> "0" Then
                            Dim deleteId() As String = hGridDelete.Value.Split(";")
                            Dim iParms(2) As SqlClient.SqlParameter
                            For RowCount = 0 To deleteId.GetUpperBound(0)
                                iParms(1) = Mainclass.CreateSqlParameter("@ESF_ID", deleteId(RowCount), SqlDbType.Int)
                                iParms(2) = Mainclass.CreateSqlParameter("@ESH_ID", ViewState("EntryId"), SqlDbType.Int)
                                Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from ESH_F where ESF_ID=@ESF_ID and ESH_ID=@ESH_ID", iParms)
                                If RetValFooter = "-1" Then
                                    lblError.Text = "Unexpected Error !!!"
                                    stTrans.Rollback()
                                    Exit Sub
                                End If
                            Next
                        End If
                        stTrans.Commit()
                    End If

                Catch ex As Exception
                    lblError.Text = ex.Message
                    stTrans.Rollback()
                    Exit Sub
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try
            End If
            writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), "", 0, "N")
        End If
        If isAdvance And newARF Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"), False)
        End If
    End Sub

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        If doApproval() = 0 Then
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Rejection Comments are Mandatory"
            Exit Sub
        End If
        If writeWorkFlow(ViewState("EntryId"), "Rejected", txtApprovals.Text, arf_Apr_id, "R") = 0 Then
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update ARF set ARF_STATUS_STR='Rejected:by " & Session("sUsr_name") & "', ARF_STATUS='R' where ARF_ID=" & h_EntryId.Value)
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Dim sqlStr As String = "update ARF set ARF_STATUS_STR='Deleted', ARF_STATUS='D', ARF_DELETED=1 where ARF_ID=@ARF_ID"
            Dim pParms(1) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@ARF_ID", h_EntryId.Value, SqlDbType.Int, True)
            Try
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                writeWorkFlow(ViewState("EntryId"), "Deleted", txtApprovals.Text, 0, "D")

            Catch ex As Exception
                stTrans.Rollback()
                'Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try

            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Property isAdvance() As Boolean
        Get
            Return ViewState("Advance")
        End Get
        Set(ByVal value As Boolean)
            ViewState("Advance") = value
        End Set
    End Property

    Private Property arf_Apr_id() As Integer
        Get
            Return ViewState("arf_Apr_id")
        End Get
        Set(ByVal value As Integer)
            ViewState("arf_Apr_id") = value
        End Set
    End Property

    Private Property TblExpense() As DataTable
        Get
            Return ViewState("TblExpense")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TblExpense") = value
        End Set
    End Property

    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TblExpense = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TblExpense)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TblExpense = mtable
        fillGrdView.DataSource = TblExpense
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub

    Private Sub showNoRecordsFound()
        If TblExpense.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoice.Columns.Count - 2
            grdInvoice.Rows(0).Cells.Clear()
            grdInvoice.Rows(0).Cells.Add(New TableCell())
            grdInvoice.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoice.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Protected Sub grdInvoice_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoice.RowCancelingEdit
        grdInvoice.ShowFooter = True
        grdInvoice.EditIndex = -1
        grdInvoice.DataSource = TblExpense
        grdInvoice.DataBind()
    End Sub

    Protected Sub grdInvoice_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdInvoice.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtInv_Particulars As TextBox = grdInvoice.FooterRow.FindControl("txtParticulars")
            Dim txtInv As TextBox = grdInvoice.FooterRow.FindControl("txtInv")
            Dim txtInv_Product As TextBox = grdInvoice.FooterRow.FindControl("txtProduct")
            Dim txtProductid As HiddenField = grdInvoice.FooterRow.FindControl("h_prod_id")
            Dim txtInv_Amt As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Amt")

            If txtInv_Product.Text = "" Then lblError.Text &= " Select Product"
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If

            If TblExpense.Rows(0)(1) = -1 Then TblExpense.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = TblExpense.NewRow
            mrow("ESD_id") = 0
            'mrow("ESF_DETAILS") = txtProductid.Value.Split(";")(0)
            mrow("ESD_EEF_ID") = txtProductid.Value
            mrow("PUR_DESCR") = txtInv_Product.Text
            mrow("ESD_DETAILS") = txtInv_Particulars.Text
            mrow("ESD_INVOICE") = txtInv.Text
            mrow("ESD_AMOUNT") = txtInv_Amt.Text
            TblExpense.Rows.Add(mrow)
            grdInvoice.EditIndex = -1
            grdInvoice.DataSource = TblExpense
            grdInvoice.DataBind()
            showNoRecordsFound()
            txtBillingTotal.Text = TblExpense.Compute("sum(ESD_AMOUNT)", "")
        Else
            Dim txtInv_Rate As TextBox = grdInvoice.FooterRow.FindControl("txtParticulars")
            Dim txtInv As TextBox = grdInvoice.FooterRow.FindControl("txtInv")
            Dim txtInv_Amt As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Amt")

            'If Val(txtInv_Rate.Text) <> 0 Then
            '    txtInv_Rate.Enabled = False
            '    txtInv_Amt.Text = Val(txtInv_Qty.Text) * Val(txtInv_Rate.Text)
            'Else
            '    txtInv_Rate.Enabled = True
            '    txtInv_Amt.Text = Val(txtInv_Qty.Text) * Val(txtInv_Rate.Text)
            'End If

        End If
    End Sub

    Protected Sub grdInvoice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoice.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim txtInv_Rate As TextBox = e.Row.FindControl("txtParticulars")
            Dim txtInv As TextBox = e.Row.FindControl("txtInv")
            Dim txtInv_Amt As TextBox = e.Row.FindControl("txtInv_Amt")


            If e.Row.RowType = DataControlRowType.Footer Then
                Dim txtInv_Rate_F As TextBox = e.Row.FindControl("txtParticulars")
                Dim txtInv_Qty_F As TextBox = e.Row.FindControl("txtInv")
                Dim imgSubLedger As ImageButton = e.Row.FindControl("imgSubLedger")
                Dim txtProduct_F As TextBox = e.Row.FindControl("txtProduct")
                Dim h_PRODUCT_ID_F As HiddenField = e.Row.FindControl("h_prod_id")
                Dim txtInv_Amt_F As TextBox = e.Row.FindControl("txtInv_Amt")
                imgSubLedger.Attributes.Add("OnClick", "javascript: getExpenses('" & txtProduct_F.ClientID & "','" & h_PRODUCT_ID_F.ClientID & "'); return false;")
            End If

            ClientScript.RegisterArrayDeclaration("grdRate", String.Concat("'", txtInv_Rate.ClientID, "'"))
            ClientScript.RegisterArrayDeclaration("grdQty", String.Concat("'", txtInv.ClientID, "'"))
        End If
    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub

    Protected Sub grdInvoice_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdInvoice.RowDeleting
        Dim mRow() As DataRow = TblExpense.Select("ID=" & grdInvoice.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= ";" & mRow(0)("ESD_ID")
            TblExpense.Select("ID=" & grdInvoice.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            TblExpense.AcceptChanges()
        End If
        If TblExpense.Rows.Count = 0 Then
            TblExpense.Rows.Add(TblExpense.NewRow())
            TblExpense.Rows(0)(1) = -1
            txtBillingTotal.Text = "0"
        Else
            txtBillingTotal.Text = TblExpense.Compute("sum(ESD_AMOUNT)", "")
        End If

        grdInvoice.DataSource = TblExpense
        grdInvoice.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdInvoice_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdInvoice.RowEditing
        Try
            grdInvoice.ShowFooter = False
            grdInvoice.EditIndex = e.NewEditIndex
            grdInvoice.DataSource = TblExpense
            grdInvoice.DataBind()
            Dim txtInv_Rate_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtParticulars")
            Dim txtInv_Qty_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtInv")
            Dim imgSubLedger As ImageButton = grdInvoice.Rows(e.NewEditIndex).FindControl("imgSubLedger_e")
            Dim txtProduct_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtProduct")
            Dim h_PRODUCT_ID_F As HiddenField = grdInvoice.Rows(e.NewEditIndex).FindControl("h_prod_id_e")
            Dim txtInv_Product As TextBox = grdInvoice.FooterRow.FindControl("txtProduct")
            Dim txtInv_Amt As TextBox = grdInvoice.FooterRow.FindControl("txtAmount")
            imgSubLedger.Visible = True
            'imgSubLedger.Attributes.Add("OnClick", "javascript:return getProduct('" & txtProduct_F.ClientID & "','" & txtInv_Rate_F.ClientID & "','" & h_PRODUCT_ID_F.ClientID & "')")
            'imgSubLedger.Attributes.Add("OnClick", "javascript:return getProduct('" & txtProduct_F.ClientID & "','" & txtInv_Rate_F.ClientID & "','" & h_PRODUCT_ID_F.ClientID & "','" & txtInv_Qty_F.ClientID & "')")
            imgSubLedger.Attributes.Add("OnClick", "javascript: getExpenses('" & txtProduct_F.ClientID & "','" & h_PRODUCT_ID_F.ClientID & "'); return false;")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdInvoice_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdInvoice.RowUpdating
        Dim s As String = grdInvoice.DataKeys(e.RowIndex).Value.ToString()
        Dim lblInv_id As Label = grdInvoice.Rows(e.RowIndex).FindControl("lblInv_ID")
        'Dim ddlProducts As DropDownList = grdInvoice.Rows(e.RowIndex).FindControl("ddlProducts")
        Dim txtParticulars As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtParticulars")
        Dim txtInvoice As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInv")
        Dim txtAmt As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInv_Amt")
        Dim txtInv_Product As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtProduct")
        Dim txtProductid As HiddenField = grdInvoice.Rows(e.RowIndex).FindControl("h_prod_id_e")
        Dim lblINVCNID As Label = grdInvoice.Rows(e.RowIndex).FindControl("lblInv_CN_ID")

        Dim mrow As DataRow
        mrow = TblExpense.Select("ID=" & s)(0)
        mrow("ESD_ID") = lblInv_id.Text
        mrow("ESD_EEF_ID") = txtProductid.Value.Split(";")(0)
        'mrow("Inv_CN_ID") = lblINVCNID.Text
        mrow("PUR_DESCR") = txtInv_Product.Text
        mrow("ESD_DETAILS") = txtParticulars.Text
        mrow("ESD_INVOICE") = txtInvoice.Text
        mrow("ESD_AMOUNT") = Convert.ToDouble(txtAmt.Text).ToString("####0.00")

        grdInvoice.EditIndex = -1
        grdInvoice.ShowFooter = True
        grdInvoice.DataSource = TblExpense
        grdInvoice.DataBind()
        txtBillingTotal.Text = Convert.ToDouble(TblExpense.Compute("sum(ESD_AMOUNT)", "")).ToString("####0.00")
    End Sub

    Private Sub updateWorkflow()
        Dim sqlStr As New StringBuilder
        If ViewState("datamode") = "add" Then
            fillGridView(grdInvoice, "select ESD_ID,ESD_ESH_ID,ESD_EEF_ID,PUR_DESCR,ESD_DETAILS,ESD_INVOICE,ESD_AMOUNT from ESF_D left outer join ADVREQPURPOSE on PUR_ID=ESD_EEF_ID where ESD_ID=0 order by ESD_ID")
        Else
            fillGridView(grdInvoice, "select ESD_ID,ESD_ESH_ID,ESD_EEF_ID,PUR_DESCR,ESD_DETAILS,ESD_INVOICE,ESD_AMOUNT from ESF_D left outer join ADVREQPURPOSE on PUR_ID=ESD_EEF_ID where ESD_ESH_ID=" & h_EntryId.Value & " order by ESD_ID")
            txtBillingTotal.Text = TblExpense.Compute("sum(ESD_AMOUNT)", "")
        End If
    End Sub

    Private Function writeWorkFlow(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_ARF_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", txtApprovals.Text.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveWorkFlowARF", iParms)
        writeWorkFlow = iParms(8).Value
    End Function

    Function ValidTblExpense(ByVal txtTrv_Rate As String, ByVal txtTrv_Qty As String) As String
        ValidTblExpense = ""
        If txtTrv_Rate.Length = 0 Then ValidTblExpense &= "Rate"
        If txtTrv_Qty.Length = 0 Then ValidTblExpense &= IIf(ValidTblExpense.Length = 0, "", ",") & "Qty"
    End Function

    Private Sub FormatFigures()
        On Error Resume Next
        txtBillingTotal.Text = Convert.ToDouble(txtBillingTotal.Text).ToString("####0.00")
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "UPDATE ARF SET ARF_STATUS_STR='ARF Archived', ARF_STATUS='C' WHERE ARF_ID=" & h_Arf_Id.Value)
        writeWorkFlow(ViewState("EntryId"), "Closed", "", 0, "C")
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        doApproval()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Function doApproval(Optional ByVal arf_id As Integer = 0) As Integer
        If arf_id = 0 Then arf_id = h_EntryId.Value
        Dim strApprover() As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverARF(" & arf_id & ")").ToString.Split(";")
        doApproval = -1
        If strApprover(0) = 500 Then 'finally approved
            doApproval = writeWorkFlow(arf_id, "Advance", "Advance Request, ARF Approved", 0, "P")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update ARF set ARF_APR_ID=500, ARF_STATUS='P', ARF_STATUS_STR='Advance:Process Payment' where ARF_ID=" & arf_id)
            End If
        Else
            doApproval = writeWorkFlow(arf_id, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update ARF set ARF_STATUS_STR='Approval:with " & strApprover(1) & "', ARF_STATUS=case when ARF_STATUS='N' then 'S' else ARF_STATUS end, ARF_APR_ID=" & strApprover(0) & " where ARF_ID=" & arf_id)
            End If
        End If
    End Function

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            If isAdvance Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Dim cmd As New SqlCommand
                cmd.CommandText = "RPTARF"
                Dim sqlParam(1) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@ARF_NO", txtArNo.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))

                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure

                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("reportCaption") = "Advance Request Form"

                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params
                repSource.ResourceName = "../../Inventory/Reports/RPT/RPTARF.rpt"

                repSource.IncludeBSUImage = True
                Session("ReportSource") = repSource
                Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
            Else
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Dim cmd As New SqlCommand
                cmd.CommandText = "rptExpSettlement"
                Dim sqlParam(1) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@ESH_ID", h_EntryId.Value, SqlDbType.Int)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure
                'V1.2 comments start------------
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("reportCaption") = "Expense Settlement Details"
                'params("@TRN_NO") = TRN_NO

                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params
                repSource.ResourceName = "../../Inventory/Reports/RPT/rptExpSettlement.rpt"

                Dim subRep(0) As MyReportClass
                subRep(0) = New MyReportClass
                Dim subcmd1 As New SqlCommand

                subcmd1.CommandText = "rptSettlementDetails"
                Dim sqlParam1(0) As SqlParameter
                sqlParam1(0) = Mainclass.CreateSqlParameter("@ESH_ID", h_EntryId.Value, SqlDbType.Int)
                subcmd1.Parameters.Add(sqlParam1(0))
                subcmd1.CommandType = Data.CommandType.StoredProcedure
                subcmd1.Connection = New SqlConnection(str_conn)
                subRep(0).Command = subcmd1
                subRep(0).ResourceName = "wBTA_Sector"

                repSource.IncludeBSUImage = True

                repSource.SubReport = subRep

                'End If
                Session("ReportSource") = repSource
                Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Advance\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename

            If h_ARF_STATUS.Value = "C" Then
                lblError.Text = "upload not allowed for Archived Advance Requests!!!!"
                Exit Sub
            End If

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Dim sqlParam(6) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@ARI_ID", 0, SqlDbType.Int, True)
                sqlParam(1) = Mainclass.CreateSqlParameter("@ARI_TCH_ID", h_EntryId.Value, SqlDbType.Int)
                sqlParam(2) = Mainclass.CreateSqlParameter("@ARI_CONTENTTYPE", ContentType, SqlDbType.VarChar)
                sqlParam(3) = Mainclass.CreateSqlParameter("@ARI_DOCTYPE", 0, SqlDbType.Int)
                sqlParam(4) = Mainclass.CreateSqlParameter("@ARI_NAME", UploadDocPhoto.FileName, SqlDbType.VarChar)
                sqlParam(5) = Mainclass.CreateSqlParameter("@ARI_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                sqlParam(6) = Mainclass.CreateSqlParameter("@ARI_FILENAME", txtArNo.Text & "_~" & FileExt, SqlDbType.VarChar)

                Try
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveARF_I", sqlParam)
                    IO.File.Move(strFilepath, str_img & txtArNo.Text & "_" & sqlParam(0).Value & FileExt)
                    showImages()
                    writeWorkFlow(ViewState("EntryId"), "ADDEDIMG", "Added upload document " & UploadDocPhoto.FileName, 0, "O")
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If

        trComments.Focus()
    End Sub

    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select

    End Function

    Protected Sub rptImages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptImages.ItemCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.CommandSource.text = "x" Then
                If h_ARF_STATUS.Value = "C" Or h_ARF_STATUS.Value = "R" Or h_ARF_STATUS.Value = "D" Then
                    lblError.Text = "Cannot delete file"
                Else
                    Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete"), LinkButton)
                    Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
                    Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from arf_i where ari_id=" & lblPriId.Text)
                    writeWorkFlow(ViewState("EntryId"), "DELETEDIMG", "Deleted upload document " & btnDocumentLink.Text, 0, "O")
                    showImages()
                End If
            End If
        End If
    End Sub

    Protected Sub rptImages_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptImages.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
            Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
            Dim lblPriFilename As TextBox = CType(e.Item.FindControl("lblPriFilename"), TextBox)
            Dim lblPriContentType As TextBox = CType(e.Item.FindControl("lblPriContentType"), TextBox)
            btnDocumentLink.Attributes.Add("OnClick", "javascript: return showDocument(" & lblPriId.Text & ",'" & lblPriFilename.Text & "','" & lblPriContentType.Text & "');")
        End If
    End Sub
    Protected Sub txtCommenttxt_TextChanged(sender As Object, e As EventArgs)

    End Sub
End Class




