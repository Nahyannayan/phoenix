<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="InvApprovalmaster.aspx.vb" Inherits="Inventory_InvApprovalmaster" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        <%--function getDepartment() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var bsu = document.getElementById('<%= txtBusId.ClientID %>').value;
            pMode = "BSUDPT"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtDepartment.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtDepartmentId.ClientID %>").value = NameandCode[0];
        }--%>
        <%--function getUserName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var bsu = document.getElementById('<%= txtBusId.ClientID %>').value;
            pMode = "BSUUSER"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtEmployee.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=h_Emp_No.ClientID %>").value = NameandCode[0];
        }--%>
        <%--function getPurDocType() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "PURDOCTYPE"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtDocType.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtDoctypeId.ClientID %>").value = NameandCode[0];
        }--%>

        function getFilter(NameObj, IdObj, frm) {

            var bsuId = "";
            if (frm == 'BSUEMP') {
                bsuId = document.getElementById('<%=txtBusId.ClientID %>').value
                if (bsuId == '') {
                    alert('Please Select Business Unit..!');
                    return;
                }
            }
            if (frm == 'BSUNIT') {
                document.getElementById('<%=txtEmployee.ClientID %>').value = "";
            }

            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 650px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Common/PopUpSelect.aspx?id=BSUDPT;BSU=" + bsuId, "", sFeatures)
            //result = window.showModalDialog("Fillterany.aspx?FROM="+frm+"&BSUID="+bsuId,"", sFeatures)
            //Fillterany.aspx
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('__');
            document.getElementById(IdObj).value = NameandCode[0];
            document.getElementById(NameObj).value = NameandCode[1];
            return false;
        }

        function getInvisible() {
            var BsuId = document.getElementById('<%=txtBusId.ClientID %>').value
            if (BsuId == "")
                document.getElementById("ImgBsuName").style.display = 'Inline'
            else
                document.getElementById("ImgBsuName").style.display = 'none'
        }

        function Numeric_Only() {
            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function CtrlDisable() {
            if (event.keyCode == 17) {
                alert('This function disabled..!');
                return;
            }
        }
        function Mouse_Move(Obj) {
            document.getElementById(Obj).style.color = "Red";
        }
        function Mouse_Out(Obj) {
            document.getElementById(Obj).style.color = "#1b80b6"
        }
        //function getdate(objname) {
        //    var sfeatures;
        //    sfeatures = "dialogwidth: 229px; ";
        //    sfeatures += "dialogheight: 234px; ";
        //    sfeatures += "help: no; ";
        //    sfeatures += "resizable: no; ";
        //    sfeatures += "scroll: yes; ";
        //    sfeatures += "status: no; ";
        //    sfeatures += "unadorned: no; ";
        //    var nameandcode;
        //    var result;
        //    result = window.showmodaldialog("../accounts/calendar.aspx?dt=" + document.getelementbyid(objname).value, "", sfeatures)
        //    if (result == '' || result == undefined) {
        //        return false;
        //    }
        //    document.getelementbyid(objname).value = result;
        //    return true;
        //}

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmployee.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;
        }
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }
    </script>

     <script>
        
        function getPurDocTypeNew() {
            var pMode;
            var NameandCode;

            pMode = "PURDOCTYPE"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_doc");
        }
        function OnClientClose(oWnd, args) {  //Doc Type
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtDocType.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtDoctypeId.ClientID %>").value = NameandCode[0];
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------
         function getDepartmentNew() {
            var pMode;
            var NameandCode;

            var bsu = document.getElementById('<%= txtBusId.ClientID %>').value;
            pMode = "BSUDPT"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu;
            var oWnd = radopen(url, "pop_dept");
        }
        function OnClientClose1(oWnd, args) {  //Department
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtDepartment.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtDepartmentId.ClientID %>").value = NameandCode[0];
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------
         function getUserNameNew() {
            var pMode;
            var NameandCode;
            var bsu = document.getElementById('<%= txtBusId.ClientID %>').value;
            pMode = "BSUUSER"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu;            
                var oWnd = radopen(url, "pop_user");
            }
            function OnClientClose2(oWnd, args) { //USER
                var NameandCode;
                var arg = args.get_argument();
                if (arg) {

                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtEmployee.ClientID %>").value = NameandCode[1];
                    document.getElementById("<%=h_Emp_No.ClientID %>").value = NameandCode[0];
                }
            }
            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;

                var height = body.scrollHeight;
                var width = body.scrollWidth;

                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;

                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_doc" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_dept" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_user" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>





    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Approval Hierarchy Master
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="MainTable" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label><br />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="SingleParagraph" ValidationGroup="validate"
                                HeaderText=" * Marked fields are mandatory"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" valign="middle" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtBsuName" runat="server" Enabled="False"></asp:TextBox>
                            <asp:TextBox ID="txtBusId" runat="server"></asp:TextBox></td>
                        <td align="left" class="matters" valign="middle" width="20%"><span class="field-label">Document Type</span>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtDocType" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgDoctype" CausesValidation="False" runat="server" ImageUrl="~/Images/forum_search.gif"
                                AccessKey="T" OnClientClick="getPurDocTypeNew();return false;"></asp:ImageButton>
                            <asp:TextBox ID="txtDoctypeId" runat="server"></asp:TextBox>&nbsp;<span
                                style="font-size:9px;color: gray;">(Alt+T)</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDoctypeId" ValidationGroup="validate"
                                CssClass="error">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="middle"><span class="field-label">Department</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtDepartment" runat="server">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="ImgDepartment" CausesValidation="False"
                                runat="server" ImageUrl="~/Images/forum_search.gif" AccessKey="P"
                                OnClientClick="getDepartmentNew();return false;"></asp:ImageButton>
                            <asp:TextBox ID="txtDepartmentId" runat="server"></asp:TextBox>
                            <span style="font-size:9px;color: gray;">(Alt+P)</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDepartmentId" ValidationGroup="validate"
                                CssClass="error">*</asp:RequiredFieldValidator>
                        </td>

                        <td align="left" class="matters" valign="middle"><span class="field-label">User Name</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtEmployee" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgUser" CausesValidation="False" runat="server" ImageUrl="~/Images/forum_search.gif"
                                AccessKey="U" OnClientClick="getUserNameNew();return false;"></asp:ImageButton> <span style="font-size:9px;color: gray;">(Alt+U)</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmployee" ValidationGroup="validate"
                                CssClass="error">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="middle"><span class="field-label">Amount Limit</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server" CssClass="inputbox"
                                TabIndex="4"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="validate"
                                    runat="server" ControlToValidate="txtAmount" CssClass="error">*</asp:RequiredFieldValidator></td>

                        <td align="left" class="matters" valign="middle"><span class="field-label">Date From</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtDDocdate" runat="server" AutoPostBack="True" CssClass="inputbox"
                                OnTextChanged="txtDDocdate_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                ImageAlign="Middle" CausesValidation="False" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgCalendar" TargetControlID="txtDDocdate">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDDocdate" ValidationGroup="validate"
                                CssClass="error">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td class="matters">
                            <asp:CheckBox ID="ChkPrevious" runat="server" Text="Previous Approval" CssClass="field-label"></asp:CheckBox>
                        </td>
                        <td class="matters">
                            <asp:CheckBox ID="ChkHigher" runat="server" Text="Higher Approval" CssClass="field-label"></asp:CheckBox>
                        </td>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnInsert" runat="server" CssClass="button" Text="Add" OnClick="btnInsert_Click" ValidationGroup="validate"
                                TabIndex="5" />
                            <asp:Button ID="btnClear" runat="server" CssClass="button" Text="Clear" CausesValidation="False"
                                OnClick="btnClear_Click" TabIndex="6" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" Width="100%" EmptyDataText="No Item Added"
                                AutoGenerateColumns="False" OnRowDataBound="gvJournal_RowDataBound" ShowFooter="False" CssClass="table table-row table-bordered"
                                Style="cursor: hand" OnRowDeleting="gvJournal_RowDeleting">
                                <FooterStyle CssClass="gridheader_new" />
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True"></EmptyDataRowStyle>
                                <Columns>
                                    <asp:BoundField DataField="OrderNo" HeaderText="Order No">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UserName" HeaderText="User Name">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PrvApproval" HeaderText="Prv Approval">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="HgApproval" HeaderText="Hg Approval">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="FromDate"
                                        SortExpression="FromDate" HeaderText=" From Date">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Limit Amount">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:CommandField ShowDeleteButton="True" DeleteText="Edit" HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:CommandField>
                                    <asp:BoundField DataField="EmpId" Visible="False" SortExpression="EmpId" HeaderText="EmpId"></asp:BoundField>
                                </Columns>
                                <RowStyle CssClass="griditem"></RowStyle>
                                <SelectedRowStyle BackColor="Aqua"></SelectedRowStyle>
                                <HeaderStyle CssClass="gridheader_new"></HeaderStyle>
                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" CausesValidation="False" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False"
                                OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="viewId" runat="server" Value="0" />
                <asp:HiddenField ID="h_Emp_No" runat="server" Value="0" />

                <script type="text/javascript">
                    //        getInvisible();
                </script>
            </div>
        </div>
    </div>
</asp:Content>
