﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="BS_ReturnSalesInvoice.aspx.vb" Inherits="Inventory_BookSale_BS_ReturnSalesInvoice" %>

<%@ Register Src="~/UserControls/uscStudentPicker.ascx" TagPrefix="uc1" TagName="uscStudentPicker2" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        /*.RadSlider_Metro .rslHorizontal .rslItem {
            background-image:none!important;            
        }*/

        .RadSlider .rslHorizontal .rslItem, .RadSlider .rslHorizontal .rslLargeTick, .RadSlider .rslHorizontal .rslSmallTick {
            background-position: center !important;
        }

        .RadSlider_Silk {
            height: 80px !important;
        }

        .imgStyle {
            float: left;
        }

        .border0 {
            border: 0px !important;
        }


        .book-name {
            font-weight: 600;
            font-size: 16px;
        }

        .book-id {
            font-weight: 400;
            font-size: 14px;
        }

        .def-text {
            font-weight: 400;
            font-size: 16px;
        }

        .footer-row {
            background-color: #e8e5e5 !important;
        }

        .header-row {
            background-color: rgb(0, 97, 195) !important;
            color: #ffffff !important;
        }

        .listRadio {
            display: block;
            padding: 0 18px 0 0;
        }
        /*Book sales css style by Aji Rajan dated 16th April 2019*/

        .sib-profile {
            height: 100px;
            width: 80px;
            border: 1px solid rgba(0,0,0,0.2);
            border-radius: 6px;
            box-shadow: 0px 2px 10px rgba(0,0,0,0.18);
        }

        .form-control label {
            display: inline;
            color: rgba(0,0,0,0.5);
            margin-right: 10px;
        }

        .pagination {
            display: table-row !important;
        }

            .pagination table tr td {
                /*padding: 6px;
    border: 1px solid rgba(0,0,0,0.2);*/
                padding: 6px 0;
            }

                .pagination table tr td span, .pagination table tr td a {
                    padding: 6px;
                    border: 1px solid rgba(0,0,0,0.2);
                }

                .pagination table tr td a {
                    background-color: rgba(0,0,0,0.1);
                }

                    .pagination table tr td a:hover {
                        background-color: rgb(0, 97, 195);
                        color: #ffffff;
                    }

        .hr-shadow {
            box-shadow: 6px 2px 5px rgba(0,0,0,0.04);
        }

        .book-quantity {
            vertical-align: super;
            font-weight: bold;
        }

        .qty-img {
            vertical-align: bottom;
        }

        input:hover, textarea:hover, input:focus, textarea:focus {
            border-color: none !important;
            -webkit-box-shadow: none !important;
        }

        .txt-bold {
            font-weight: bold;
        }
         .nodisplay {
            display: none;
        }

        .txt-italic {
            font-style: italic;
        }


        /*auto complete extender css class goes here*/
        div .darkPanlAlumini {
            padding: 2px 10px;
            border: 1px solid rgba(0,0,0,0.08);
        }

        .darkPanlAlumini {
            background: rgba(235, 228, 228,1) !important;
            display: block;
            width: 40% !important;
            color: #333;
            cursor: pointer;
            border-bottom: 1px solid rgba(0,0,0,0.1);
        }

        span.darkPanlAlumini:hover {
            background: #63bcf7 !important;
            color: #ffffff;
        }

        .completionListElement {
            width: 100% !important;
            margin: auto;
            list-style: none;
            display: block;
            /*left: 230px !important;
            position: absolute !important;*/
            /*width: 350px !important;*/
            left: 0;
        }

            .completionListElement .listitem {
                cursor: pointer;
            }

            .completionListElement .highlightedListItem {
                background-color: rgba(0,0,0,0.2);
            }
    </style>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />
    <script type="text/javascript" language="javascript">

        Sys.Application.add_load(
 function CheckForPrint() {
     var printval = document.getElementById('<%= h_print.ClientID %>').value
     if (document.getElementById('<%= h_print.ClientID %>').value != '') {
         document.getElementById('<%= h_print.ClientID %>').value = '';
         if (isIE()) {
             window.showModalDialog('rptBS_Receipt.aspx?type=receipt&options=1&saleid=' + printval, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
         } else {
             //alert(document.getElementById('<%= h_print.ClientID %>').value);
             return ShowWindowWithClose('rptBS_Receipt.aspx?type=receipt&options=1&saleid=' + printval, 'search', '55%', '85%');
             return false;
         }


     }
 }
    );

 function isIE() {
     ua = navigator.userAgent;
     /* MSIE used to detect old browsers and Trident used to newer ones*/
     var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

     return is_ie;
 }
 function getPopup() {
     var sFeatures, url;
     sFeatures = "dialogWidth: 600px; dialogHeight: 500px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
     var NameandCode;
     var result;
     //url = "/PHOENIXBETA/Inventory/BookSale/BS_Popup.aspx?TYPE=RETURNSALES";
     url = "BS_Popup.aspx?TYPE=RETURNSALES";
     return ShowWindowWithClose(url, 'search', '55%', '85%')
     return false;
 }
 function setReceiptValue(code) {
     //alert(code);
     NameandCode = code.split('||');
     document.getElementById('<%= txtReceiptNo.ClientID%>').value = NameandCode[1];
     //alert(NameandCode[1]);
     CloseFrame();
     return false;
 }


 function ShowWindowWithClose(gotourl, pageTitle, w, h) {
     $.fancybox({
         type: 'iframe',
         //maxWidth: 300,
         href: gotourl,
         //maxHeight: 600,
         fitToView: true,
         padding: 6,
         width: w,
         height: h,
         autoSize: false,
         openEffect: 'none',
         showLoading: true,
         closeClick: true,
         closeEffect: 'fade',
         'closeBtn': true,
         afterLoad: function () {
             this.title = '';//ShowTitle(pageTitle);
         },
         helpers: {
             overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
             title: { type: 'inside' }
         },
         onComplete: function () {
             $("#fancybox-wrap").css({ 'top': '90px' });
         },
         onCleanup: function () {
             var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

             if (hfPostBack == "Y")
                 window.location.reload(true);
         }
     });

     return false;
 }
 function CloseFrame() {
     jQuery.fancybox.close();
 }

 window.format = function (b, a) {
     if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
 1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
 };

 var myIds = new Array();
 var myDescrs = new Array();



 function ClientItemSelected(sender, e) {
     var row = e._item.parentElement;
     //alert(row.getElementsByTagName('span')[0].textContent);
     //alert(row.getElementsByTagName('span')[1].textContent);

     var hf_Item_ID = sender.get_element().id.replace("txtItmSelect", "hf_Item_ID");
     //alert(2);
     $get(hf_Item_ID).value = row.getElementsByTagName('span')[0].textContent;
     //alert(3);
     $get(sender.get_element().id).value = row.getElementsByTagName('span')[1].textContent;
     //alert(4);
     //var index = sender._selectIndex;       
     //var hf_Item_ID = sender.get_element().id.replace("txtItmSelect", "hf_Item_ID");
     //$get(hf_Item_ID).value = myIds[index];
     //$get(sender.get_element().id).value = myDescrs[index];

 }

 function MultiSelect(sender, e) {
     var comletionList = sender.get_completionList();


     for (i = 0; i < comletionList.childNodes.length; i++) {
         var itemobj = new Object();
         var _data = comletionList.childNodes[i]._value;

         itemobj.descr = _data.substring(_data.lastIndexOf('|') + 1);
         comletionList.childNodes[i]._value = itemobj.name;

         _data = _data.substring(0, _data.lastIndexOf('|'));
         itemobj.itmid = _data.substring(_data.lastIndexOf('|') + 1); // parse name as item value



         if (itemobj.descr) {
             myIds[i] = itemobj.itmid; // id used in updating hidden file
             myDescrs[i] = itemobj.descr;

         }

         //comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:80%;float:left'>" + itemobj.descr + "</div>" + "<div class='cloumnspan' style='width:20%;'>" + itemobj.itmid + "</div>";
         //comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:100%;float:left'>" + itemobj.descr + "</div>";
         comletionList.childNodes[i].innerHTML = "<div><span class='darkPanlAlumini' style='display:none' align='left'> " + itemobj.itmid + " </span><span class='darkPanlAlumini' align='left'> " + itemobj.descr + " </span></div>"

     }
 }


 function formatme(me) {
     document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
 }
 function Numeric_Only() {
     //alert(event.keyCode)
     if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
         if (event.keyCode == 13 || event.keyCode == 46)
         { return false; }
         event.keyCode = 0
     }
 }

 function UpdateSum() {

     var txtCCTotal, txtCashTotal, txtTotal, txtReturn, txtReceivedTotal, txtBalance, txtDue;
     //CheckRate();

     if (document.getElementById('<%=txtDue.ClientID %>'))
         txtDue = parseFloat(document.getElementById('<%=txtDue.ClientID %>').value);
     else
         txtDue = 0;
     if (isNaN(txtDue))
         txtDue = 0;

       <%-- if (document.getElementById('<%=txtCCTotal.ClientID %>'))
            txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
        else
            txtCCTotal = 0;
        if (isNaN(txtCCTotal))--%>
     txtCCTotal = 0;

     txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
     if (isNaN(txtCashTotal))
         txtCashTotal = 0;

     txtTotal = parseFloat(document.getElementById('<%=lblTotal.ClientID%>').innerHTML.replace(',', ''));
            if (isNaN(txtTotal))
                txtTotal = 0;

            var CrCardCharge = 0
           <%--  CrCardCharge = parseFloat(document.getElementById('<%=txtCrCardCharges.ClientID %>').value);--%>
     txtReceivedTotal = txtCCTotal + txtCashTotal;

     txtBalance = 0;
     if (txtCashTotal > 0)
         if (txtReceivedTotal > txtTotal) {
             txtBalance = txtReceivedTotal - txtTotal;
             if (txtBalance > txtCashTotal)
                 txtBalance = 0;
         }
     if (txtCCTotal > txtDue) alert("Credit card amt more than due");
     txtReceivedTotal = txtCCTotal + txtCashTotal + CrCardCharge;
     if (document.getElementById('<%=txtReceivedTotal.ClientID %>')) {
         if (txtReceivedTotal > 0)
             document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtReceivedTotal.toFixed(getRoundOff());
                else
                    document.getElementById('<%=txtReceivedTotal.ClientID%>').value = 0.00
            }

          <%--  if (document.getElementById('<%=txtCCTotal.ClientID %>'))
            document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());--%>

     document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
     if (document.getElementById('<%=txtBalance.ClientID %>')) {
         if (txtBalance > 0)
             document.getElementById('<%=txtBalance.ClientID %>').value = txtBalance.toFixed(getRoundOff());
         else
             document.getElementById('<%=txtBalance.ClientID%>').value = 0.00

     }
 }

 function CheckRate() {
     var temp = new Array();
     var txtCr = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
        CrCardCharge = 0.00;
        temp = document.getElementById('<%=hfCobrand.ClientID %>').value.split("|");
        var ddlist = document.getElementById('<%=ddCreditcard.ClientID %>');

        for (var i = 0; i < temp.length; i++) {
            if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                CrCardCharge = Math.ceil((txtCr * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff()));
            }
        }
        //alert(CrCardCharge);
        if (CrCardCharge > 0)
            document.getElementById('<%=txtCrCardCharges.ClientID %>').value = CrCardCharge.toFixed(getRoundOff());
        else
            document.getElementById('<%=txtCrCardCharges.ClientID %>').value = 0.00
        //}return true;
    }

    function getRoundOff() {
        var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
            var amt;
            amt = parseFloat(roundOff)
            if (isNaN(amt))
                amt = 2;
            return amt;
        }



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>




    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sale Return
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="2">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <table runat="server" style="width: 100%">
                    <%-- <tr>
                        <td align="left" width="20%"><span class="field-label">Enter Student No</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudentNo" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label"></span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button"
                                Text="Search" TabIndex="5" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Enter Receipt</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtReceiptNo" runat="server"></asp:TextBox>
                             <asp:ImageButton ID="btnReceiptSearch" OnClick="ImageButton1_Click" runat="server" OnClientClick="return getPopup();"
                                ImageUrl="../../Images/forum_search.gif"></asp:ImageButton>
                        </td>
                        <td align="left" width="20%"><span class="field-label"></span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button"
                                Text="Search" TabIndex="5" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                </table>

                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left" colspan="2">
                            <asp:Repeater ID="repInfo" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" style="padding: 0;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%-- <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>--%>
                                    <tr class="trSub_Header_Small">
                                        <th align="left" colspan="2">
                                            <h4>
                                                <asp:Label ID="lbSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>&nbsp;[<asp:Label ID="lbSNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>]</h4>
                                        </th>
                                        <th align="right" class="text-right">
                                            <div style="clear: both;">
                                                <asp:Label ID="lbGrade" runat="server" Text='<%# Bind("GRM_DISPLAY")%>'></asp:Label>-<asp:Label ID="lbSection" runat="server" Text='<%# Bind("SCT_DESCR")%>'></asp:Label>
                                                <%--  <asp:LinkButton ID="lnkViewFeeSetup" Text="View Fee Schedule" CssClass="linkFee" runat="server"></asp:LinkButton>--%>
                                                <asp:HiddenField ID="hfSTU_ID" Value='<%# Bind("STU_ID") %>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_GRD_ID" Value='<%# Bind("STU_GRD_ID") %>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_FEE_ID" Value='<%# Bind("STU_FEE_ID")%>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_ACD_ID" Value='<%# Bind("STU_ACD_ID")%>' runat="server" />
                                            </div>
                                        </th>
                                    </tr>
                                    <%-- <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>--%>
                                    <tr id="trgvrerror" runat="server" visible="false">
                                        <td colspan="3" align="center">
                                            <asp:Label ID="lblGvrError" CssClass="linkText" Text="" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="left" style="border: none 0px !important;">
                                        <td valign="top" colspan="3" align="left">
                                            <table width="100%" style="border-collapse: collapse !important; border-spacing: 0 !important; padding: 0 !important; border: none 0px !important;">
                                                <tr>
                                                    <td align="left" width="90%">
                                                        <%--<h6>Select the Set</h6>--%>
                                                        <%--<asp:DropDownList ID="drpBookSets" runat="server" CssClass="form-control" Width="50%" >
                                                                                                                <asp:ListItem Text="Set 1" Selected ="True"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Set 2" Selected ="false"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Set 3" Selected ="false"></asp:ListItem>
                                                                                                            </asp:DropDownList>--%>
                                                        <asp:CheckBoxList ID="rdbBookSets" Width="100%" runat="server" Visible="false" CssClass="form-control border0 txt-bold" RepeatDirection="Horizontal" RepeatColumns="4" OnSelectedIndexChanged="rdbBookSets_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                    <td align="right" width="10%" style="display: none;">
                                                        <asp:Button ID="btnSelectBook" runat="server" CssClass="btn btn-info" Text="Show Set Items" OnClick="btnSelectBook_Click" Visible="false" /></td>
                                                </tr>
                                                <%--<tr>
                                                    <td colspan="3">&nbsp;</td>
                                                </tr>--%>
                                                <tr>
                                                    <td align="left" width="100%" colspan="3">

                                                        <!-- Accordion starts -->
                                                        <%--  <div class="accordionMod panel-group">
                                                                                        <div class="accordion-item">
                                                                                            <h4 class="accordion-toggle">Select Books</h4>
                                                                                            <section class="accordion-inner panel-body">--%>

                                                        <table width="100%">
                                                            <%--<tr>
                                                                                                        <th align="left" width="20%">
                                                                                                            <asp:Label ID="lblBookDescr" runat="server" CssClass="title" Text="Select Your SET"></asp:Label></th>
                                                                                                        
                                                                                                    </tr>--%>

                                                            <tr>
                                                                <td width="100%" colspan="3">
                                                                    <asp:Panel ID="GridScroll" runat="server" CssClass="grid_scroll" Width="100%">
                                                                        <div id="DivRoot" align="left">
                                                                            <div style="overflow: hidden; width: 100%;" class="header_row_grid" id="DivHeaderRow" runat="server">
                                                                            </div>
                                                                            <div style="overflow-y: scroll; width: 100%;" class="main_content_grid" id="DivMainContent" runat="server">

                                                                                <asp:GridView ID="grdSAL" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="false" AllowPaging="false"
                                                                                    CaptionAlign="Top" class="table table-bordered table-row" DataKeyNames="ID" OnPageIndexChanging="grdSAL_PageIndexChanging">
                                                                                    <Columns>
                                                                                        <asp:TemplateField Visible="True" HeaderText="Sr.No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField Visible="False" HeaderText="S.No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_ITMID" runat="server" Text='<%# Bind("ITEM_ID")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField Visible="False" HeaderText="S.No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_PID" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField Visible="False">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblSAD_ID" runat="server" Text='<%# Bind("BSH_ID")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField Visible="True" HeaderText="Book Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_ITEM_DESCR" CssClass="txt-bold1 nodisplay" runat="server" Text='<%# Bind("ITEM_DESCR") %>'></asp:Label><br class="nodisplay" />
                                                                                                <asp:Label ID="lbl_BOOK_NAME" runat="server" Text='<%# Bind("BOOK_NAME") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="txt-bold1" Width="50%" />
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField Visible="True" HeaderText="ISBN">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_ISBN" runat="server" Text='<%# Bind("ISBN")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="txt-bold1" Width="10%" />
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField Visible="True" HeaderText="Rate" ItemStyle-CssClass="text-right">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_PRICE" runat="server" Text='<%# Bind("PRICE", "{0:0.00}")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="txt-bold1" Width="5%" />
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField Visible="True" HeaderText="Qty" ItemStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="btnMinus" runat="server" Enabled="false" Text="-" OnClick="btnMinus_Click" CssClass="qty-img" ImageUrl="/images/minus_book.png" Width="36px" />
                                                                                                <asp:Label ID="lbl_QTY" runat="server" Text='<%# Bind("QTY")%>' CssClass="book-quantity"></asp:Label>
                                                                                                <asp:ImageButton ID="btnPlus" runat="server"  Text="+" OnClick="btnPlus_Click" CssClass="qty-img" ImageUrl="/images/add_book.png" Width="36px" />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle Width="140px" CssClass="fixed_row_header" />
                                                                                            <ItemStyle CssClass="text-primary text-center" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField Visible="false" HeaderText="Tax Amount" ItemStyle-CssClass="text-right">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_TAX_AMOUNT" runat="server" Text='<%# Bind("TAX_AMOUNT")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField Visible="true" HeaderText="Tax Amount" ItemStyle-CssClass="text-right">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_SHOW_TAX_AMOUNT" runat="server" Text='<%# Bind("TAX_AMOUNT", "{0:0.000}")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField Visible="true" HeaderText="Tax Type" ItemStyle-CssClass="text-left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_TAX_TYPE" runat="server" Text='<%# Bind("TAX_CODE")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField Visible="True" HeaderText="Net Amount" ItemStyle-CssClass="text-right">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbl_NET_AMOUNT" runat="server" Text='<%# Bind("NET_AMOUNT", "{0:0.000}")%>'  ></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="txt-bold1" Width="5%" />
                                                                                            <HeaderStyle CssClass="fixed_row_header" />
                                                                                        </asp:TemplateField>

                                                                                    </Columns>
                                                                                    <PagerStyle CssClass="pagination" />
                                                                                    <FooterStyle CssClass="pagination" />
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <div id="DivFooterRow" style="overflow: hidden" runat="server">
                                                                            </div>
                                                                    </asp:Panel>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="2" class="tdfields" width="60%">

                                                                    <div id="SelectItem" runat="server">
                                                                        <table width="100%">

                                                                            <td align="left" width="75%">
                                                                                <asp:TextBox ID="txtItmSelect" CssClass="form-control" runat="server" Visible="true" Style="width: 100% !important;">
                                                                                </asp:TextBox>
                                                                                <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID='<%# Bind("STU_ID")%>'
                                                                                    OnClientPopulated="MultiSelect" CompletionListCssClass="completionListElement"
                                                                                    CompletionListItemCssClass="listItem" FirstRowSelected="false" CompletionListHighlightedItemCssClass="highlightedListItem"
                                                                                    OnClientItemSelected="ClientItemSelected" CompletionSetCount="5" EnableCaching="false"
                                                                                    MinimumPrefixLength="1" UseContextKey="true" ServiceMethod="GetItmDescr" ServicePath="BS_SalesInvoice.aspx"
                                                                                    TargetControlID="txtItmSelect">
                                                                                </ajaxToolkit:AutoCompleteExtender>
                                                                                <asp:HiddenField ID="hf_Item_ID" runat="server" />
                                                                            </td>
                                                                            <td align="left" width="25%">
                                                                                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add More Item" OnClick="btnAdd_Click" />
                                                                            </td>
                                                                        </table>



                                                                    </div>
                                                                </td>

                                                                <td align="right" class="tdfields" width="40%">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td align="left"><span class="field-label">Total : </span>
                                                                                <asp:Label ID="txtSubTotal" runat="server" Style="text-align: left" Text="0.00" CssClass="field-label"></asp:Label>
                                                                            </td>

                                                                            <td align="left"><span class="field-label">VAT Total : </span>
                                                                                <asp:Label ID="txtSubVATTotal" runat="server" Style="text-align: left" Text="0.00" CssClass="field-label"></asp:Label>
                                                                            </td>
                                                                            <td align="left"><span class="field-label">Grand Total : </span></td>
                                                                            <td align="left" class="mattersBigger">

                                                                                <asp:Label ID="txtGrandTotal" runat="server" Style="text-align: left" Text="0.00" CssClass="field-label"></asp:Label>
                                                                            </td>
                                                                            <%--<tbody>
                                                                            <tr>
                                                                                <td width="50%" align="left" class="tdfields">
                                                                                 <span class="field-label">  Total </span>
                                                                                </td>
                                                                                <td width="50%" align="left">                                                                                  
                                                                                        <asp:Label ID="txtSubTotal" runat="server" CssClass="form-control text-right" Style="display: inline-block; width: 100%" Text="0.00"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>--%>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <%--  <tr>
                                                                <td align="left" colspan="2" class="tdfields"></td>
                                                                <td align="right" class="tdfields">
                                                                    <table width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td width="50%" align="left">
                                                                                    <span class="field-label">VAT Total </span>
                                                                                </td>
                                                                                <td width="50%" align="left">                                                                                   
                                                                                        <asp:Label ID="txtSubVATTotal" runat="server" CssClass="form-control text-right" Style="display: inline-block; width: 100%" Text="0.00"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>

                                                            </tr>--%>
                                                            <%-- <tr>
                                                                <td align="left" colspan="2" class="tdfields"></td>
                                                                <td align="right" class="tdfields">
                                                                    <table width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td width="50%" align="left">
                                                                                   <span class="field-label">Grand Total </span>
                                                                                </td>
                                                                                <td width="50%" align="left">                                                                                    
                                                                                        <asp:Label ID="txtGrandTotal" runat="server" CssClass="form-control text-right" Style="display: inline-block; width: 100%" Text="0.00"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>

                                                            </tr>--%>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <hr class="hr-shadow" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <%-- </section>
                                                                                        </div>

                                                                                    </div>--%>
                                                        <!-- Accordion ends -->

                                                    </td>
                                                </tr>
                                        </td>
                                    </tr>
                                    </table>
                                                                    </td>
                                                                </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>

                <table id="pay_section" runat="server" width="100%" visible="false">

                    <tr class="tdblankAll">
                        <td width="60%" class="tdfields"></td>
                        <td align="right" width="40%" valign="middle" class="tdfields">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td width="50%" align="left">
                                            <span class="field-label">Net Payable </span>
                                        </td>
                                        <td width="50%" align="left">
                                            <asp:TextBox ID="txtTotal" CssClass="form-control" Style="text-align: right" runat="server" Text="0.00" TabIndex="152" Visible="false">
                                            </asp:TextBox>
                                            <asp:Label ID="lblTotal" CssClass="form-control text-right" runat="server" Text="0.00" TabIndex="152" Width="100%">    </asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr class="title-bg-lite">
                        <td align="left" colspan="2">Payment Details</td>
                    </tr>
                    <tr runat="server" id="trCash">
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Cash</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCashTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.select();" onBlur="UpdateSum();"
                                            Style="text-align: right" TabIndex="45" Text="0.00"></asp:TextBox>&nbsp;
                                    </td>
                                    <td align="right" width="20%"><span class="field-label">Due</span></td>
                                    <td align="right" width="30%">
                                        <asp:TextBox ID="txtDue" runat="server" Style="text-align: right" TabIndex="205" Enabled="false" Text="0.00"></asp:TextBox></td>

                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr runat="server" id="trCreditCard">
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Credit Card </span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCCTotal" Style="text-align: right" runat="server" onFocus="this.select();" onBlur="UpdateSum();" AutoCompleteType="Disabled" TabIndex="50" Text="0.00"></asp:TextBox>
                                        <ajaxToolkit:PopupControlExtender ID="pceCreditCard" runat="server" PopupControlID="pnlCreditCard"
                                            Position="Bottom" TargetControlID="txtCCTotal">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                    <td align="right" width="20%"><span class="field-label">Credit Card Charge</span></td>
                                    <td align="right" width="30%">
                                        <asp:TextBox ID="txtCrCardCharges" runat="server" Text="0.00" Style="text-align: right;"
                                            TabIndex="200"></asp:TextBox></td>

                                </tr>
                            </table>
                        </td>


                    </tr>

                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Total Received</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtReceivedTotal" Style="text-align: right" runat="server" TabIndex="205" Text="0.00"></asp:TextBox></td>
                                    <td align="right" width="20%">
                                        <asp:Label ID="lblBalance" runat="server" Text="Balance" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="right" width="30%">
                                        <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" TabIndex="220" Text="0.00"></asp:TextBox></td>

                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" rowspan="2" width="20%"><span class="field-label">Narration</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" Height="32px" TextMode="MultiLine" TabIndex="160"></asp:TextBox>
                                        <asp:LinkButton ID="lbViewTotal" runat="server" Visible="False">Preview Total</asp:LinkButton></td>

                                </tr>
                            </table>
                        </td>



                    </tr>


                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnAddNew" runat="server" CausesValidation="False" CssClass="button" Text="Add" Visible="False" />
                            <%--<asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />--%>
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <%--<asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print Receipt" />
                            <asp:Button ID="btnInvoice" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print Invoice" />--%>
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <%--<asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>--%>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="exec GetCreditCardListForCollection 'FEES'"></asp:SqlDataSource>
                <asp:Panel ID="pnlCreditCard" Style="display: none" runat="server" CssClass="panel-cover">
                    <table border="0" cellpadding="3" cellspacing="0" align="center" width="100%">
                        <tr>
                            <td align="left" colspan="1"><span class="field-label">Credit Card </span>
                            </td>
                            <td align="right">
                                <%--<asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close.png" OnClientClick="return HideAll();" TabIndex="54" Style="text-align: right" />--%>

                            </td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1"><span class="field-label">Card Type</span></td>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard"
                                    DataTextField="CRI_DESCR" DataValueField="CRR_ID" SkinID="DropDownListNormal" TabIndex="55">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Authorisation Code</span></td>
                            <td align="left" colspan="1">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="60" SkinID="TextBoxCollection"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                        SelectCommand="exec GetCreditCardListForCollection 'FEES'">
                        <SelectParameters>
                            <asp:Parameter Name="CollectFrom" DefaultValue="FEES" Type="String" />
                            <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                </asp:Panel>

                <asp:HiddenField ID="hfCobrand" runat="server" />
                <asp:HiddenField ID="h_print" Value="" runat="server" />
            </div>
        </div>
    </div>



    <script type="text/javascript">

        function MakeStaticHeader(gridId, HeaderRowId, MainContentId, FooterRowId, height, width, headerHeight, isFooter) {
            //alert(gridId);   
            var tbl = document.getElementById(gridId);
            if (tbl) {

                var DivHR = document.getElementById(HeaderRowId);
                var DivMC = document.getElementById(MainContentId);
                var DivFR = document.getElementById(FooterRowId);

                //*** Set divheaderRow Properties ****
                DivHR.style.height = headerHeight + 'px';
                DivHR.style.width = (parseInt(width) - 16) + 'px';
                DivHR.style.position = 'relative';
                DivHR.style.top = '0px';
                DivHR.style.zIndex = '15';
                DivHR.style.verticalAlign = 'top';
                //*** Set divMainContent Properties ****
                DivMC.style.width = width + 'px';
                DivMC.style.height = height + 'px';
                DivMC.style.position = 'relative';
                DivMC.style.top = -headerHeight + 'px';
                DivMC.style.zIndex = '1';
                //*** Set divFooterRow Properties ****
                DivFR.style.width = (parseInt(width) - 16) + 'px';
                DivFR.style.position = 'relative';
                DivFR.style.top = -headerHeight + 'px';
                DivFR.style.verticalAlign = 'top';
                DivFR.style.paddingtop = '2px';
                if (isFooter) {
                    var tblfr = tbl.cloneNode(true);
                    tblfr.removeChild(tblfr.getElementsByTagName('tbody')[0]);
                    var tblBody = document.createElement('tbody');
                    tblfr.style.width = '100%';
                    tblfr.cellSpacing = "0";
                    tblfr.border = "0px";
                    tblfr.rules = "none";
                    //*****In the case of Footer Row *******
                    tblBody.appendChild(tbl.rows[tbl.rows.length - 1]);
                    tblfr.appendChild(tblBody);
                    DivFR.appendChild(tblfr);
                }
                //****Copy Header in divHeaderRow****
                DivHR.appendChild(tbl.cloneNode(true));
            }
        }



    </script>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src='<%= ResolveUrl("../js/ScrollableGridPlugin.js")%>'></script>
    <style>
        .grid_scroll {
            /*overflow: auto;*/
            /*height: 430px;*/
            /*overflow-y: scroll;*/
        }

        .grid_scroll0 {
            height: 230px;
        }

        .fixed_row_header {
            height: 49px;
        }

        .main_content_grid0 {
            overflow-y: scroll;
            /* width: 1000px; */
            width: 100% !important;
            height: 200px !important;
            position: relative;
            top: -60px;
            z-index: 1;
        }

        .main_content_grid {
            overflow-y: scroll;
            /* width: 1000px; */
            width: 100% !important;
            /*height: 400px !important; */
            position: relative;
            top: -60px;
            z-index: 1;
        }

        .header_row_grid {
            overflow: hidden;
            height: 60px;
            width: 98.5% !important;
            top: 0px;
            z-index: 15;
            position: relative;
            vertical-align: top;
        }
    </style>
</asp:Content>
