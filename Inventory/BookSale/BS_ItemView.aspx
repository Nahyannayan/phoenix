﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="BS_ItemView.aspx.vb" Inherits="Inventory_BookSale_BS_ItemView" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 25%;
            top: 40%;
            position: fixed;
            width: 50%;
        }

        .gridrow_bold {
            font-weight: bold;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sale Item Master
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <table runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left">

                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">

                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" AllowPaging="True" PageSize="25" class="table table-row table-bordered">
                                <Columns>

                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <HeaderTemplate>
                                            ID<br />
                                            <asp:TextBox ID="txt_ID" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnIDSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Book Name">
                                        <HeaderTemplate>
                                            Book Name<br />
                                            <asp:TextBox ID="txtBookName" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBookName" runat="server" Text='<%# Bind("BOOKNAME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                            Description<br />
                                            <asp:TextBox ID="txtDescription" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDescriptionSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("DESCRIPTION")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ISBN">
                                        <HeaderTemplate>
                                            ISBN<br />
                                            <asp:TextBox ID="txtISBN" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunoSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblISBN" runat="server" Text='<%# Bind("ISBN")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Publication" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPUBLICATION" runat="server" Text='<%# Bind("PUBLICATION")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="ShowOnline" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBIM_bSHOWONLINE" runat="server" Text='<%# Bind("BIM_bSHOWONLINE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- <asp:TemplateField HeaderText="View">
                                        <HeaderTemplate>
                                            View
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="hlview" runat="server">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>--%>

                                    <asp:ButtonField CommandName="edit" HeaderText="View" Text="View">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                    <asp:ButtonField CommandName="view" HeaderText="Quick Edit" Text="Quick Edit">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />

                <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <%--style="display:none;"--%>
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: black; border: 1px solid black; border-radius: 10px 10px; background-color: lightgray;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="CENTER" class="title-bg-lite">
                                    <asp:Label ID="lblpnltitle" runat="server" EnableViewState="True"> Update Book Sale Item Master</asp:Label>
                                </div>
                                <div align="CENTER">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>


                                <table align="center" width="100%" cellpadding="2" cellspacing="0">

                                    <tr id="TR1" runat="server">
                                        <td colspan="2">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="20%">
                                                        <span class="field-label">Book Name</span>
                                                        <asp:HiddenField ID="HDN_ITEM_ID" runat="server" />

                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:Label ID="LBL_BOOKNAME" runat="server" EnableViewState="False"></asp:Label>
                                                    </td>
                                                    <td align="left" width="20%">
                                                        <span class="field-label"></span>

                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:CheckBox ID="chkbShowOnline" runat="server" Text="Show Online" CssClass="field-label" />
                                                    </td>
                                                </tr>


                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="TR2" runat="server">
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" />
                                            <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                        </td>
                                    </tr>


                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
