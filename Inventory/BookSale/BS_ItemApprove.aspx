﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BS_ItemApprove.aspx.vb" Inherits="Inventory_BookSale_BS_ItemApprove" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function CheckNumber(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(2);
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Approve/Reject Book Sale Item Master
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <%-- <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Book Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBookName" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtItemDescr" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">ISBN</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtitemISBN" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Publication</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtItemPublication" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Category</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtitemCategory" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Type</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlItemType" runat="server" AutoPostBack="True"></asp:DropDownList>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Cost Price</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtItemCPrice" runat="server" Text="0.00" onblur="CheckNumber(this);" onFocus="this.select();" Style="text-align: right"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtItemCPrice" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtItemCPrice" />
                                    </td>
                                    <td align="left"><span class="field-label">Selling Price</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtItemSPrice" runat="server" AutoPostBack="true" Text="0.00" onblur="CheckNumber(this);" onFocus="this.select();" Style="text-align: right"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtItemSPrice" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtItemSPrice" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">TAX Type</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlVATCode" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Tax Amount</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTaxAmount" runat="server" Enabled="false" Text="0.00" onblur="CheckNumber(this);" onFocus="this.select();" Style="text-align: right"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtTaxAmount" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtTaxAmount" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Net Price</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNetPrice" runat="server" Enabled="false" Text="0.00" onblur="CheckNumber(this);" onFocus="this.select();" Style="text-align: right"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtNetPrice" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtNetPrice" />
                                    </td>

                                    <%-- <td align="left"><span class="field-label">Upload Image</span></td>
                                    <td align="left">
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                    </td>--%>
                                    <td align="left"><span class="field-label">Select Grade</span></td>
                                    <td align="left">

                                        <telerik:RadComboBox RenderMode="Lightweight" ID="ddlPrintedFor" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlPrintedFor_SelectedIndexChanged" Width="100%">
                                        </telerik:RadComboBox>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkbShowOnline" runat="server" AutoPostBack="True" Text="Show Online" CssClass="field-label" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="button"
                                Text="Approve" TabIndex="5" />
                            <asp:Button ID="btnReject" runat="server" CausesValidation="False" CssClass="button"
                                Text="Reject" TabIndex="6" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="h_ITM_ID" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
