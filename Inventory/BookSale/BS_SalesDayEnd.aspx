﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BS_SalesDayEnd.aspx.vb" Inherits="Inventory_BookSale_BS_SalesDayEnd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sales Day End Posting
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 60%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                               <%-- <tr class="subheader_img">
                                    <td align="left" colspan="3" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                            <span style="font-family: Verdana">
                                Day End Posting</span></font></td>
                                </tr>--%>


                                <tr>
                                    <td align="left" >
                                        <asp:GridView ID="gvDayEnd" runat="server" PageSize="25" class="table table-row table-bordered"
                                            EmptyDataText="No Data" Width="98%" SkinID="GridViewPickDetails" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="sap_date"  HeaderText="Date" />
                                                <asp:BoundField DataField="sap_type" HeaderText="Type" />
                                                <asp:BoundField DataField="TaxCode" HeaderText="VAT Code" />
                                                <asp:BoundField DataField="sap_Amount" HeaderText="Amount" />
                                            </Columns>
                                        </asp:GridView>

                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="matters" style="height: 19px" valign="bottom">
                            <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button"
                                Text="Post" TabIndex="5" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" /></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" style="height: 52px">
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

