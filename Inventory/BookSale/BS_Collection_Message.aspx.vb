﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Inventory_BookSale_BS_Collection_Message
    Inherits System.Web.UI.Page
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Page.Title = OASISConstants.Gemstitle

            Try
                ViewState("MainMnu_code") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "PI01143" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    'Select Case ViewState("MainMnu_code").ToString
                    '    Case OASISConstants.MNU_FEE_COLLECTION

                    '        ViewState("trantype") = "A"
                    'End Select

                    Dim con As String = ConnectionManger.GetOASIS_PUR_INVConnectionString

                    Dim param(2) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@OPTIONS", 1)
                    param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))

                    Dim ds As New DataSet
                    ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[DBO].[GET_BOOKCOURIER_ENABLED]", param)
                    If ds.Tables(0).Rows.Count > 0 Then
                        If CBool((ds.Tables(0).Rows(0)("IS_ENABLE"))) Then
                            ID_COLD.Enabled = True
                        Else
                            ID_COLD.Enabled = False
                        End If
                    End If


                    End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Public Function checkdatetime(ByVal f_date As String, ByVal t_date As String) As Boolean
        Try
            Dim c_date As String
            Dim ps_date As String
            ps_date = f_date + " 00:00"
            c_date = t_date + " 00:00" 'DateTime.Now.ToString("dd/MM/yyyy HH:mm")

            Dim from_date As DateTime = DateTime.ParseExact(ps_date, "dd/MMM/yyyy HH:mm", Nothing)
            Dim to_date As DateTime = DateTime.ParseExact(c_date, "dd/MMM/yyyy HH:mm", Nothing)

            If from_date > to_date Then
                Return False

            Else
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If txtFromDate.Text.Trim = "" Then
                'lblError.Text = "Item description cannot be blank"
                usrMessageBar2.ShowNotification("From Date cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            If txtToDate.Text.Trim = "" Then
                'lblError.Text = "Item description cannot be blank"
                usrMessageBar2.ShowNotification("To Date cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            If txtMessage.Text.Trim = "" Then
                'lblError.Text = "Item description cannot be blank"
                usrMessageBar2.ShowNotification("Message cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            Dim bool As Boolean = checkdatetime(txtFromDate.Text.Trim, txtToDate.Text.Trim)
            If bool = False Then
                usrMessageBar2.ShowNotification("Check Date Format (Or) To Date Should be Greater than From Date", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            Dim pParms(12) As SqlParameter
            pParms(0) = New SqlParameter("@BCM_ID", SqlDbType.Int)
            'pParms(0).Direction = ParameterDirection.Output
            pParms(0).Value = 0
            pParms(1) = New SqlParameter("@BCM_FROMDATE", SqlDbType.VarChar, 100)
            pParms(1).Value = txtFromDate.Text
            pParms(2) = New SqlParameter("@BCM_TODATE", SqlDbType.VarChar, 250)
            pParms(2).Value = txtToDate.Text
            pParms(3) = New SqlParameter("@BCM_BSU_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = Session("sBsuid")
            pParms(4) = New SqlParameter("@BCM_MESSAGE", SqlDbType.NVarChar)
            pParms(4).Value = txtMessage.Text
            pParms(5) = New SqlParameter("@BCM_COLP", SqlDbType.Bit)
            pParms(5).Value = ID_COLP.Checked
            pParms(6) = New SqlParameter("@BCM_COLC", SqlDbType.Bit)
            pParms(6).Value = ID_COLC.Checked
            pParms(7) = New SqlParameter("@BCM_COLD", SqlDbType.Bit)
            pParms(7).Value = ID_COLD.Checked

            pParms(8) = New SqlParameter("@USER_ID", SqlDbType.VarChar, 50)
            pParms(8).Value = Session("sUsr_id")
            pParms(9) = New SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(9).Direction = ParameterDirection.ReturnValue
            pParms(10) = New SqlParameter("@NEW_BCM_ID", SqlDbType.Int)
            pParms(10).Direction = ParameterDirection.Output

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction()
            Try
                Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.[SAVE_BOOK_COLLECTION_MESSAGE_M]", pParms)
                If pParms(9).Value <> 0 Then
                    'lblError.Text = UtilityObj.getErrorMessage(pParms(6).Value)
                    usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(pParms(9).Value), UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    'lblError.Text = "Data Saved Successfully !!!"
                    usrMessageBar2.ShowNotification("Data Saved Successfully", UserControls_usrMessageBar.WarningType.Success)
                    btnSave.Visible = False

                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Response.Redirect(ViewState("ReferrerUrl"))

    End Sub
End Class
