﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Inventory_BookSale_BS_ItemView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "BS_Item.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            Page.Title = OASISConstants.Gemstitle

            Try
                ViewState("MainMnu_code") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "PI01165" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    'Select Case ViewState("MainMnu_code").ToString
                    '    Case OASISConstants.MNU_FEE_COLLECTION

                    '        ViewState("trantype") = "A"
                    'End Select

                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False
        'divAge.Style.Add("display", "none")
    End Sub
    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divAge.Visible = False
    End Sub
    Protected Sub gvDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDetails.RowEditing

    End Sub


    'Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
    '    Try
    '        Dim lblItemColID As Label
    '        lblItemColID = TryCast(e.Row.FindControl("lbl_ID"), Label)


    '        Dim cmdCol As Integer = gvDetails.Columns.Count - 1
    '        Dim hlview As New LinkButton
    '        hlview = TryCast(e.Row.FindControl("hlview"), LinkButton)
    '        If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
    '            ViewState("datamode") = Encr_decrData.Encrypt("view")
    '            'hlEdit.NavigateUrl = "FEEReminderViewDetails.aspx?FRH_ID=" & Encr_decrData.Encrypt(lblFRH_ID.Text) & _
    '            '"&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
    '            hlview. = "BS_Item.aspx" & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view")

    '        End If



    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub


    Protected Sub gvDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails.RowCommand
        Try
            Dim lbl_ID As New Label
            'Dim lblFromDate As New Label
            'Dim lblToDate As Label
            'Dim lblMessage As Label
            'Dim lblMessage1 As Label
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvDetails.Rows(index), GridViewRow)
            lbl_ID = selectedRow.FindControl("lbl_ID")
            'lblFromDate = selectedRow.FindControl("lblFromDate")
            'lblToDate = selectedRow.FindControl("lblToDate")
            'lblMessage = selectedRow.FindControl("lblMessage")
            'lblMessage1 = selectedRow.FindControl("lblMessage1")
            'Dim hlview As New HyperLink
            'hlview = selectedRow.FindControl("hlview")
            If e.CommandName = "edit" Then


                If lbl_ID IsNot Nothing Then
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    'hlEdit.NavigateUrl = "FEEReminderViewDetails.aspx?FRH_ID=" & Encr_decrData.Encrypt(lblFRH_ID.Text) & _
                    '"&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Dim url As String = "~\Inventory\BookSale\BS_Item.aspx" & "?viewid=" & Encr_decrData.Encrypt(lbl_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view")
                    Response.Redirect(url)
                End If
            End If
            If e.CommandName = "view" Then

                Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
                'Dim str_query As String = " SELECT ISNULL(BCM_COLP,0) BCM_COLP,ISNULL(BCM_COLC,0) BCM_COLC,ISNULL(BCM_COLD,0) BCM_COLD FROM [dbo].[BOOK_COLLECT_MESSAGE_M] WHERE BCM_ID= " & lblBCM_ID.Text.ToString
                Dim str_query As String = " select ID, [BOOKNAME], DESCRIPTION,ISBN,CATEGORY,PUBLICATION,BIM_bSHOWONLINE from (select BIM_ID id,[BIM_BOOK_NAME] [BOOKNAME], BIM_DESCR DESCRIPTION,BIM_ISBN ISBN,BIM_CATEGORY CATEGORY,BIM_PUBLICATION PUBLICATION, ISNULL(BIM_bSHOWONLINE,0) BIM_bSHOWONLINE from BOOK_ITEM_M WITH (NOLOCK) WHERE BIM_ID='" & lbl_ID.Text & "' AND  BIM_BSU_ID='" & Session("sBSUID") & "' AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1) a where 1=1 "
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    LBL_BOOKNAME.Text = ds.Tables(0).Rows(0)("BOOKNAME")
                    chkbShowOnline.Checked = ds.Tables(0).Rows(0)("BIM_bSHOWONLINE")
                    HDN_ITEM_ID.Value = lbl_ID.Text
                End If
                divAge.Visible = True

            End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gridbind()


        Dim str_Filter As String = "", FCL_bDeleted As String = "", FCL_ACD_ID As Integer = 0

        Dim lstrCondn1, lstrCondn2, lstrCondn3 As String

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim CurBsUnit As String = Session("sBsuid")
        lstrCondn1 = ""
        lstrCondn2 = ""
        lstrCondn3 = ""

        str_Filter = ""

        If gvDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   txtReceiptno FCL_RECNO
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtBookName")
            lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BOOKNAME", lstrCondn1)

            '   -- 2  txtDate
            larrSearchOpr = h_selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtDescription")
            lstrCondn2 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DESCRIPTION", lstrCondn2)

            '   -- 3  txtGrade
            larrSearchOpr = h_selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtISBN")
            lstrCondn3 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ISBN", lstrCondn3)


        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        'Dim StrSQL As String = "select BCM_ID, BCM_FROM_DATE ,BCM_TO_DATE , substring(BCM_MESSAGE,0,150)+'....' as BCM_MESSAGE,BCM_MESSAGE AS MESSAGE from [dbo].[BOOK_COLLECT_MESSAGE_M] where [BCM_bDELETE]=0 AND [BCM_BSU_ID]='" & Session("sBsuid") & "'" & str_Filter & " ORDER BY  BCM_ID DESC"
        Dim StrSQL As String = "select ID, BOOKNAME, DESCRIPTION,ISBN,CATEGORY,PUBLICATION,BIM_bSHOWONLINE from (select BIM_ID id,[BIM_BOOK_NAME] [BOOKNAME], BIM_DESCR DESCRIPTION,BIM_ISBN ISBN,BIM_CATEGORY CATEGORY,BIM_PUBLICATION PUBLICATION,BIM_bSHOWONLINE from BOOK_ITEM_M WITH (NOLOCK) WHERE BIM_BSU_ID='" & Session("sBSUID") & "' AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1) a where 1=1 " & "" & str_Filter & ""
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)
        gvDetails.DataSource = ds
        gvDetails.DataBind()


        If gvDetails.Rows.Count > 0 Then

            txtSearch = gvDetails.HeaderRow.FindControl("txtBookName")
            txtSearch.Text = lstrCondn1

            txtSearch = gvDetails.HeaderRow.FindControl("txtDescription")
            txtSearch.Text = lstrCondn2

            txtSearch = gvDetails.HeaderRow.FindControl("txtISBN")
            txtSearch.Text = lstrCondn3


        End If
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        update_showonline()
        gridbind()
    End Sub
    Sub update_showonline()
        Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim pParms(3) As SqlParameter


        pParms(0) = New SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlParameter("@BIM_ID", SqlDbType.Int)
        pParms(1).Value = HDN_ITEM_ID.Value
        pParms(2) = New SqlParameter("@BIM_bSHOWONLINE", SqlDbType.Bit)
        pParms(2).Value = chkbShowOnline.Checked
        pParms(3) = New SqlParameter("@BIM_BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = Session("sBsuId")

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction()
        Try
            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.[UPDATE_BOOK_ITEM]", pParms)
            If RetVal <> -1 Then
                'lblError.Text = UtilityObj.getErrorMessage(pParms(6).Value)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                divAge.Visible = False
                Exit Sub
            Else
                stTrans.Commit()
                'lblError.Text = "Data Updated Successfully !!!"
                usrMessageBar2.ShowNotification("Data Updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                divAge.Visible = False
            End If

        Catch ex As Exception
            divAge.Visible = False
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
End Class
