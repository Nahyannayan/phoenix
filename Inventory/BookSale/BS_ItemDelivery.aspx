﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BS_ItemDelivery.aspx.vb" Inherits="Inventory_BookSale_BS_ItemDelivery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <script type="text/javascript" language="javascript">

        Sys.Application.add_load(
     function CheckForPrint() {
         var printval = document.getElementById('<%= h_print.ClientID %>').value
         if (document.getElementById('<%= h_print.ClientID %>').value != '') {
             document.getElementById('<%= h_print.ClientID %>').value = '';
             if (isIE()) {
                 window.showModalDialog('rptBS_Receipt.aspx?type=delivery&options=1&saleid=' + printval, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
             } else {
                 //alert(document.getElementById('<%= h_print.ClientID %>').value);
             return ShowWindowWithClose('rptBS_Receipt.aspx?type=delivery&options=1&saleid=' + printval, 'search', '55%', '85%');
             return false;
         }


     }
     }
    );

 function isIE() {
     ua = navigator.userAgent;
     /* MSIE used to detect old browsers and Trident used to newer ones*/
     var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

     return is_ie;
 }
 function getPopup() {
     var sFeatures, url;
     sFeatures = "dialogWidth: 600px; dialogHeight: 500px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
     var NameandCode;
     var result;
     url = "BS_Popup.aspx?TYPE=ITEMDELIVERY";

     return ShowWindowWithClose(url, 'search', '55%', '85%')
     return false;
 }
 function setReceiptValue(code) {
     //alert(code);
     NameandCode = code.split('||');
     document.getElementById('<%= txtReceiptNo.ClientID%>').value = NameandCode[1];
     //alert(NameandCode[1]);
     CloseFrame();
     return false;
 }


 function ShowWindowWithClose(gotourl, pageTitle, w, h) {
     $.fancybox({
         type: 'iframe',
         //maxWidth: 300,
         href: gotourl,
         //maxHeight: 600,
         fitToView: true,
         padding: 6,
         width: w,
         height: h,
         autoSize: false,
         openEffect: 'none',
         showLoading: true,
         closeClick: true,
         closeEffect: 'fade',
         'closeBtn': true,
         afterLoad: function () {
             this.title = '';//ShowTitle(pageTitle);
         },
         helpers: {
             overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
             title: { type: 'inside' }
         },
         onComplete: function () {
             $("#fancybox-wrap").css({ 'top': '90px' });
         },
         onCleanup: function () {
             var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

             if (hfPostBack == "Y")
                 window.location.reload(true);
         }
     });

     return false;
 }
 function CloseFrame() {
     jQuery.fancybox.close();
 }

    </script>
    <script language="javascript" type="text/javascript">
        function change_chk_state(chkThis) {
            var ids = chkThis.id

            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = state;

                }
            }
            //document.forms[0].submit()
            return false;
        }


        function uncheckall(chkThis) {
            var ids = chkThis.id
            alert(ids)
            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent

            var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child

            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }
            var uncheckflag = 0

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                    if (currentid.indexOf(value) == -1) {
                        if (document.forms[0].elements[i].checked == false) {
                            uncheckflag = 1
                        }
                    }
                }
            }

            if (uncheckflag == 1) {
                // uncheck parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = false;
                    }
                }
            }
            else {
                // Check parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = true;

                    }
                }
                // document.forms[0].submit();
            }


            return false;
        }


        function change_chk_stateg(chkThis) {
            //             var a=chkThis.id
            //             alert(a)

            var chk_state = !chkThis.checked;


            var a = document.getElementsByName('').length

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sale Item Delivery
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>

                <table runat="server" style="width: 100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Enter Receipt No</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtReceiptNo" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btnReceiptSearch" OnClick="ImageButton1_Click" runat="server" OnClientClick="return getPopup();"
                                ImageUrl="../../Images/forum_search.gif"></asp:ImageButton>
                        </td>
                        <td align="left" width="20%"><span class="field-label"></span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button"
                                Text="Search" TabIndex="5" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Visible="false"
                                Text="Print" TabIndex="5" />
                        </td>
                    </tr>
                    <tr id="TR_RecDate" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Receipt Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="txtReceiptDate" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label"></span>
                        </td>
                        <td align="left" width="30%"></td>
                    </tr>

                    <tr id="TR_NoName" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Student No</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="txt_StudentNo" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Student Name</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="txt_StudentName" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr id="TR_GrdSec" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Grade</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="txt_grade" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Section</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="txt_section" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>

                    <tr>
                        <td colspan="4" align="left">
                            <asp:GridView ID="grdItem" runat="server" AutoGenerateColumns="False" PageSize="5"
                                Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select All">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ckbheader" runat="server" Text="SelectAll" OnCheckedChanged="ckbheader_CheckedChanged" AutoPostBack="True"></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckbSelect" runat="server" OnCheckedChanged="ckbSelect_CheckedChanged" AutoPostBack="false"></asp:CheckBox>
                                        </ItemTemplate>

                                        <ItemStyle Width="50px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="BSAD ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSAD_ID" runat="server" Text='<%# Bind("BSAD_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblITEM_ID" runat="server" Text='<%# Bind("ITEM_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Name" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblITEM_DESCR" runat="server" Text='<%# Bind("ITEM_DESCR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty">
                                        <ItemTemplate>
                                            <asp:Label ID="lblITEM_QTY" runat="server" Style="text-align: right"
                                                Text='<%# Bind("ITEM_QTY")%>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Right" />

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblITEM_AMOUNT" runat="server" Text='<%# Bind("ITEM_AMOUNT", "{0:0.00}")%>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Right" />

                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnDeliver" runat="server" CssClass="button" Visible="false"
                                Text="Deliver" TabIndex="5" />


                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" Value="" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
