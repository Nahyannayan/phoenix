﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="BS_Collection_Message_View.aspx.vb" Inherits="Inventory_BookSale_BS_Collection_Message_View" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 25%;
            top: 40%;
            position: fixed;
            width: 50%;
        }

        .gridrow_bold {
            font-weight: bold;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sale Collection Message
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <table runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left">

                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">

                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" AllowPaging="True" PageSize="25" class="table table-row table-bordered">
                                <Columns>

                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <HeaderTemplate>
                                            ID<br />
                                            <asp:TextBox ID="txtBCM_ID" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnReceiptSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBCM_ID" runat="server" Text='<%# Bind("BCM_ID")%>'></asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <HeaderTemplate>
                                            From Date<br />
                                            <asp:TextBox ID="txtFromDate" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFromDate" runat="server" Text='<%# Bind("BCM_FROM_DATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <HeaderTemplate>
                                            To Date<br />
                                            <asp:TextBox ID="txtToDate" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblToDate" runat="server" Text='<%# Bind("BCM_TO_DATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Message">
                                        <HeaderTemplate>
                                            Message<br />
                                            <asp:TextBox ID="txtMessage" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunoSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMessage" runat="server" Text='<%# Bind("BCM_MESSAGE")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Message" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMessage1" runat="server" Text='<%# Bind("MESSAGE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--<asp:TemplateField HeaderText="View">
                                        <HeaderTemplate>
                                            View
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="hlview" runat="server">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>--%>

                                    <asp:ButtonField CommandName="edit" HeaderText="View" Text="View">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />

                <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <%--style="display:none;"--%>
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: black; border: 1px solid black; border-radius: 10px 10px; background-color: lightgray;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="CENTER" class="title-bg-lite">
                                    <asp:Label ID="lblpnltitle" runat="server" EnableViewState="True">Book Sale Collection Message</asp:Label>
                                </div>
                                <div align="CENTER">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>


                                <table align="center" width="100%" cellpadding="2" cellspacing="0">

                                    <tr id="TR1" runat="server">
                                        <td colspan="2">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="20%">
                                                        <span class="field-label">From Date</span>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtfromdate" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="20%">
                                                        <span class="field-label">To Date</span>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txttodate" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <span class="field-label">Message</span>
                                                    </td>
                                                    <td align="left" colspan="3">
                                                        <asp:TextBox ID="txtMessage" runat="server" Height="32px" TextMode="MultiLine" TabIndex="160"></asp:TextBox></td>

                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Deliver Mode</span></td>
                                                    <td align="left" colspan="3">
                                                        <asp:CheckBox ID="ID_COLP" Width="100%" runat="server" Text="Collect the Books from School Personally" Checked="true"></asp:CheckBox>
                                                        <asp:CheckBox ID="ID_COLC" Width="100%" runat="server" Text="Deliver Books To Child" Checked="true"></asp:CheckBox>
                                                        <asp:CheckBox ID="ID_COLD" Width="100%" runat="server" Text="Deliver Books By Courier" Enabled="false"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="TR2" runat="server">
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                        </td>
                                    </tr>


                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
