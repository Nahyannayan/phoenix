﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_BookSale_BS_ItemDelivery
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

        End If


    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Gridbind()
    End Sub
    Protected Sub Gridbind()
        Dim ReceiptNo As String = ""
        ReceiptNo = txtReceiptNo.Text.Trim.ToString
        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim str_paymnts As String = " exec [dbo].[GET_BOOKSALES_DELIVERY] @OPTIONS =1, @BSAH_BSU_ID='" & Session("sBsuid") & "', @BSAH_NO  = '" & ReceiptNo & "'"
        Dim ds1 As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
        grdItem.DataSource = ds1.Tables(0)
        grdItem.DataBind()

        str_paymnts = " exec [dbo].[GET_BOOKSALES_DELIVERY] @OPTIONS =2, @BSAH_BSU_ID='" & Session("sBsuid") & "', @BSAH_NO  = '" & ReceiptNo & "'"
        Dim ds2 As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds2, Nothing)

        If ds2.Tables(0).Rows.Count > 0 Then
            txt_StudentNo.Text = ds2.Tables(0).Rows(0)("BSAH_CUST_NO").ToString
            txt_StudentName.Text = ds2.Tables(0).Rows(0)("BSAH_CUST_NAME").ToString
            txt_grade.Text = ds2.Tables(0).Rows(0)("BSAH_GRADE").ToString
            txt_section.Text = ds2.Tables(0).Rows(0)("BSAH_SECTION").ToString
            txtReceiptDate.Text = Format(ds2.Tables(0).Rows(0)("BSAH_DATE"), "dd/MMM/yyyy")
        End If

        If ds1.Tables(0).Rows.Count > 0 Then
            TR_RecDate.Visible = True
            TR_NoName.Visible = True
            TR_GrdSec.Visible = True
            btnDeliver.Visible = True
            btnPrint.Visible = True
        Else
            lblError.Text = "All items in this Receipt Delivered"
            TR_RecDate.Visible = False
            btnDeliver.Visible = False
            TR_NoName.Visible = False
            TR_GrdSec.Visible = False
            btnPrint.Visible = True
        End If

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Gridbind()
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Dim ds5 As New DataSet
        Dim BSAHO_DELIVERY_TYPE As Integer = -1
        ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT BSAH_ID FROM [OASIS_PUR_INV].[dbo].[BOOK_SALE_H] WITH (NOLOCK) WHERE BSAH_NO='" & txtReceiptNo.Text & "'")
        If ds5.Tables(0).Rows.Count > 0 Then
            h_print.Value = txtReceiptNo.Text
        Else
            lblError.Text = "Check entered Receipt No is correct !!!"
        End If

    End Sub
    Protected Sub btnDeliver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeliver.Click
        Dim final_string As String = ""
        For Each gvrow As GridViewRow In grdItem.Rows
            Dim chk As CheckBox
            chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
            Dim lblBSAD_ID As Label
            lblBSAD_ID = TryCast(gvrow.FindControl("lblBSAD_ID"), Label)
            If chk.Checked Then
                If final_string = "" Then
                    final_string = final_string + lblBSAD_ID.Text
                Else
                    final_string = final_string + "|" + lblBSAD_ID.Text
                End If

            End If
        Next
        Dim ReceiptNo As String = ""
        ReceiptNo = txtReceiptNo.Text.ToString

        Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim pParms(7) As SqlParameter
        pParms(0) = New SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlParameter("@BSAH_NO", SqlDbType.VarChar, 100)
        pParms(1).Value = ReceiptNo
        pParms(2) = New SqlParameter("@BSAD_ID", SqlDbType.VarChar, 500)
        pParms(2).Value = final_string
        pParms(3) = New SqlParameter("@USER_NAME", SqlDbType.VarChar, 50)
        pParms(3).Value = Session("sUsr_id")
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction()
        Try
            Dim RetVal As Integer = 0
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.[UPDATE_BOOKSALES_DELIVERY]", pParms)
            RetVal = pParms(4).Value
            If RetVal <> 0 Then
                lblError.Text = "Cannot Deliver Selected Books !!!"
                stTrans.Rollback()
                Gridbind()
                Exit Sub
            Else
                stTrans.Commit()
                lblError.Text = "Delivered Successfully !!!"
                Gridbind()
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub ckbheader_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkhead As CheckBox
        chkhead = TryCast(grdItem.HeaderRow.FindControl("ckbheader"), CheckBox)
        For Each gvrow As GridViewRow In grdItem.Rows
            Dim chk As CheckBox
            chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
            If chkhead.Checked = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next
    End Sub

    Protected Sub ckbSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim chk As CheckBox
        chk = TryCast(sender.FindControl("ckbSelect"), CheckBox)
        Dim chkhead As CheckBox
        chkhead = TryCast(grdItem.HeaderRow.FindControl("ckbheader"), CheckBox)
        If chk.Checked = False Then
            chkhead.Checked = False
        End If

    End Sub
End Class
