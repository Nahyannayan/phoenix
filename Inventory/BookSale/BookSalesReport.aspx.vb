﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Partial Class Inventory_BookSale_BookSalesReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then

                txtFDate.Text = Now.ToString("dd/MMM/yyyy")
                txtTDate.Text = Now.ToString("dd/MMM/yyyy")
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            ClientScript.RegisterStartupScript(Me.GetType(), "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim ReportName As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select mnu_text from oasis..menus_m where mnu_code='" & MainMnu_code & "'")
            Page.Title = OASISConstants.Gemstitle

            pgTitle.Text = ReportName
            UsrBSUnits1.MenuCode = MainMnu_code
            'Select Case MainMnu_code
            '    Case "PI05010" 'Framework
            '        Me.trBSUnit.Visible = False
            '        Me.trSummary.Visible = False
            '        Me.trbudget.visible = False

            'End Select
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If

            If Not Session("PurchaseReport_FromDate") Is Nothing Then
                'Me.txtFDate.Text = Session("PurchaseReport_FromDate")
            End If
            If Not Session("PurchaseReport_ToDate") Is Nothing Then
                'Me.txtTDate.Text = Session("PurchaseReport_ToDate")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateReports()
        Try
            If Not Session("PurchaseReport_FromDate") Is Nothing Then
                Session.Remove("PurchaseReport_FromDate")
            End If
            If Not Session("PurchaseReport_ToDate") Is Nothing Then
                Session.Remove("PurchaseReport_ToDate")
            End If
            Session.Add("PurchaseReport_FromDate", Me.txtFDate.Text)
            Session.Add("PurchaseReport_ToDate", Me.txtTDate.Text)
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim strABC As String = String.Empty
            Dim strBsuid As String = ""

            UsrBSUnits1.MenuCode = MainMnu_code
            strBsuid = UsrBSUnits1.GetSelectedNode()

            'Dim str_Sql As String = "GET_BOOK_SALE_DAILY_CREDIT_CARD_REPORT"
            Dim params As New Hashtable
            Select Case MainMnu_code
                Case "PI09060" 'Book Sale Daily Report of Credit Card
                    If Not chkReport.Checked Then
                        params.Add("@OPTIONS", 1)
                    Else
                        params.Add("@OPTIONS", 2)
                    End If

                Case "PI09061" 'Book Sale Daily Receipt Journal by Payment or Daily Sales
                    If Not chkReport.Checked Then
                        params.Add("@OPTIONS", 3)
                    Else
                        params.Add("@OPTIONS", 4)
                    End If
                Case "PI09062" 'Book Sales Collection
                    If Not chkReport.Checked Then
                        params.Add("@OPTIONS", 5)
                    Else
                        params.Add("@OPTIONS", 6)
                    End If
            End Select

            params.Add("userName", Session("sUsr_name"))
            params.Add("@IMG_TYPE", "LOGO")
            params.Add("@IMG_BSU_ID", Session("sBsuid"))
            'params.Add("@FLAG", IIf(chkReport.Checked, "1", "0"))
            'Dim cmd As New SqlCommand
            'cmd.CommandText = str_Sql
            'cmd.CommandType = CommandType.StoredProcedure
            'cmd.Connection = New SqlConnection(str_conn)
            If MainMnu_code = "PI09060" Or MainMnu_code = "PI09061" Or MainMnu_code = "PI09062" Then
                params.Add("@FROMDATE", txtFDate.Text)
                params.Add("@TODATE", txtTDate.Text)
                params.Add("@BSU", strBsuid)

                'Dim sqlParam(3) As SqlParameter
                'sqlParam(0) = Mainclass.CreateSqlParameter("@FROMDATE", txtFDate.Text, SqlDbType.VarChar)
                'cmd.Parameters.Add(sqlParam(0))
                'sqlParam(1) = Mainclass.CreateSqlParameter("@TODATE", txtTDate.Text, SqlDbType.VarChar)
                'cmd.Parameters.Add(sqlParam(1))
                'sqlParam(2) = Mainclass.CreateSqlParameter("@BSU", strBsuid, SqlDbType.VarChar)
                'cmd.Parameters.Add(sqlParam(2))
            Else


            End If

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "OASIS_PUR_INV"

            End With
            Dim reportpath As String = ""
            Select Case MainMnu_code

                Case "PI09062" 'Sales Collection
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    If Not chkReport.Checked Then
                        reportpath = "../Reports/RPT/rptBookSaleCollection.rpt"
                    Else
                        reportpath = "../Reports/RPT/rptBookSaleSummary.rpt"
                    End If

                    rptClass.reportParameters = params
                    rptClass.reportPath = Server.MapPath(reportpath)
                Case "PI09060" 'Daily Report of Credit Card
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    'repSource.ResourceName = "../../Reports/RPT/rptDailyCard.rpt"
                    reportpath = "../Reports/RPT/rptBookSaleDailyCreditCard.rpt"
                    rptClass.reportPath = Server.MapPath(reportpath)
                    rptClass.reportParameters = params
                Case "PI09061" 'Book Sale Daily Receipt Journal by Payment or Daily Sales
                    params.Add("reportCaption", pgTitle.Text)
                    params.Add("reportDate", "For the Period " & Me.txtFDate.Text & " To " & Me.txtTDate.Text)
                    'repSource.ResourceName = "../../Reports/RPT/rptDailyJournal.rpt"
                    reportpath = "../Reports/RPT/rptBookSaleDailySales.rpt"
                    rptClass.reportPath = Server.MapPath(reportpath)
                    rptClass.reportParameters = params
            End Select
            ' repSource.IncludeBSUImage = True
            ' repSource.DisplayGroupTree = False
            ' repSource.Parameter = params
            ' repSource.Command = cmd
            ' Session("ReportSource") = repSource
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            Session("rptClass") = rptClass
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        GenerateReports()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
