﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Inventory_BookSale_BS_SalesDayEnd
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI01169") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                refreshScreen()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub refreshScreen()
        Dim sqlStr As String = ""
        'sqlStr = "select SAL_CREDITCARD SAP_ID, 'Credit Card' SAP_TYPE, replace(convert(varchar(30),getdate(),106),' ','/') SAP_DATE, sum(sal_cctotal) SAP_Amount "
        'sqlStr &= "from sal_h inner join oasis_fees..CREDITCARD_S on crr_id=SAL_CREDITCARD where sal_deleted=0 and sal_type in ('S','R') and sal_bsu_id='" & Session("sBsuid") & "' and sal_date=DATEADD(day, DATEDIFF(day, 0, getdate()),0) and sal_cctotal>0 "
        'sqlStr &= "group by SAL_CREDITCARD "
        'sqlStr &= "union all "
        'sqlStr &= "select 0, 'Cash', replace(convert(varchar(30),getdate(),106),' ','/'), sum(case when SAL_TYPE='S' THEN SAL_CASHTOTAL-SAL_BALANCE ELSE SAL_TOTAL end)*(CASE when SAL_TYPE='S' THEN 1 ELSE -1 END) "
        'sqlStr &= "from sal_h where sal_deleted=0 and sal_type in ('S','R') and sal_bsu_id='" & Session("sBsuid") & "' and sal_date=DATEADD(day, DATEDIFF(day, 0, getdate()),0) "
        'sqlStr &= "group by sal_type "
        'sqlStr &= "union all "
        'sqlStr &= "select SAL_CREDITCARD SAP_ID, 'Credit Card Charge' SAP_TYPE, replace(convert(varchar(30),getdate(),106),' ','/') SAP_DATE, sum(sal_creditcard_charge) SAP_Amount "
        'sqlStr &= "from sal_h inner join oasis_fees..CREDITCARD_S on crr_id=SAL_CREDITCARD where sal_deleted=0 and sal_type in ('S','R') and sal_bsu_id='" & Session("sBsuid") & "' and sal_date=DATEADD(day, DATEDIFF(day, 0, getdate()),0) and sal_creditcard_charge<>0 "
        'sqlStr &= "group by SAL_CREDITCARD "

        'Dim ds As New DataSet
        'ds = clsInventory.GET_SALES_DAYEND_AMOUNT(Session("sBsuid").ToString)

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(0).Value = Session("sBsuid").ToString
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_BOOKSALES_DAYEND]", pParms)


        gvDayEnd.DataSource = ds.Tables(0)
        gvDayEnd.DataBind()

        'sqlStr = "SELECT COUNT(*) "
        'sqlStr &= "FROM OASIS_FEES.FEES.FEEOTHCOLLECTION_H inner join OASIS_FEES.FEES.FEEOTHCOLLSUB_D ON FOD_FOC_ID=FOC_ID "
        'sqlStr &= "WHERE FOC_BSU_ID='" & Session("sBsuid") & "' AND FOC_EMP_ID='Auto' and FOC_DATE=DATEADD(day, DATEDIFF(day, 0, getdate()),0) GROUP BY FOC_RECNO"
        'Dim postCnt As Integer = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, sqlStr.ToString)
        btnPost.Visible = Not clsInventory.IsPostedFortheDay(Session("sBsuid").ToString) '(postCnt = 0)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        lblError.Text = ""
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim pParms(2) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@USR_ID", Session("sUsr_name"), SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[dbo].[PostBookSales]", pParms)
            stTrans.Commit()
            Response.Redirect(ViewState("ReferrerUrl"), False)
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
            Errorlog(ex.Message)
            Exit Sub
        Finally
            objConn.Close()
        End Try
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
