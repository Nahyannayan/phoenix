﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Inventory_BookSale_BS_StockAdjustmentsView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim sbReceiptNo As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "BS_StockAdjustment.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "PI01146" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    'Select Case ViewState("MainMnu_code").ToString
                    '    Case OASISConstants.MNU_FEE_COLLECTION

                    '        ViewState("trantype") = "A"
                    'End Select

                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label
            lblItemColID = TryCast(e.Row.FindControl("lblReceiptId"), Label)
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New LinkButton
            hlview = TryCast(e.Row.FindControl("hlView"), LinkButton)
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                hlview.Attributes.Add("onclick", "CheckForPrint(" & lblItemColID.Text & ",'PI01146')")
                hlview.Text = "Receipt"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gridbind()


        Dim str_Filter As String = "", FCL_bDeleted As String = "", FCL_ACD_ID As Integer = 0

        Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7 As String

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim CurBsUnit As String = Session("sBsuid")
        lstrCondn1 = ""
        lstrCondn2 = ""
        lstrCondn3 = ""
        lstrCondn4 = ""
        lstrCondn5 = ""
        lstrCondn6 = ""
        lstrCondn7 = ""
        str_Filter = ""

        If gvDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   txtReceiptno FCL_RECNO
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtReceiptId")
            lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSAH_ID", lstrCondn1)

            '   -- 2  txtDate
            larrSearchOpr = h_selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtReceiptNo")
            lstrCondn2 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSAH_NO", lstrCondn2)

            '   -- 3  txtGrade
            larrSearchOpr = h_selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtSource")
            lstrCondn3 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSAH_SOURCE", lstrCondn3)

            '   -- 4   txtStuno
            larrSearchOpr = h_selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtDate")
            lstrCondn4 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSAH_DATE", lstrCondn4)

            '   -- 5  txtStuname
            larrSearchOpr = h_selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtCustomerNo")
            lstrCondn5 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSAH_CUST_NO", lstrCondn5)

            '   -- 6  txtAmount
            larrSearchOpr = h_selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtCustomerName")
            lstrCondn6 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSAH_CUST_NAME", lstrCondn6)

            '   -- 7  txtDesc
            larrSearchOpr = h_selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtNetAmount")
            lstrCondn7 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSAH_NET_AMOUNT", lstrCondn7)
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim StrSQL As String = "select BSAH_ID AS ID, BSAH_ID  ,BSAH_NO ,BSAH_SOURCE , BSAH_DATE ,BSAH_CUST_NO ,BSAH_CUST_NAME  ,BSAH_NET_AMOUNT from [dbo].[BOOK_SALE_H] where BSAH_TYPE='A' AND ISNULL(BSAH_DELETED,0)=0 AND [BSAH_BSU_ID]='" & Session("sBsuid") & "'" & str_Filter & " ORDER BY  BSAH_ID DESC"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)
        gvDetails.DataSource = ds
        gvDetails.DataBind()

        If gvDetails.Rows.Count > 0 Then
            txtSearch = gvDetails.HeaderRow.FindControl("txtReceiptId")
            txtSearch.Text = lstrCondn1

            txtSearch = gvDetails.HeaderRow.FindControl("txtReceiptNo")
            txtSearch.Text = lstrCondn2

            txtSearch = gvDetails.HeaderRow.FindControl("txtSource")
            txtSearch.Text = lstrCondn3

            txtSearch = gvDetails.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn4

            txtSearch = gvDetails.HeaderRow.FindControl("txtCustomerNo")
            txtSearch.Text = lstrCondn5

            txtSearch = gvDetails.HeaderRow.FindControl("txtCustomerName")
            txtSearch.Text = lstrCondn6

            txtSearch = gvDetails.HeaderRow.FindControl("txtNetAmount")
            txtSearch.Text = lstrCondn7
        End If
    End Sub

    Private Sub BindGrid(ByVal dt As DataTable)
        gvDetails.DataSource = dt
        gvDetails.DataBind()
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
