﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="BookSalesReport.aspx.vb" Inherits="Inventory_BookSale_BookSalesReport" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

     
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            <asp:Label ID="pgTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%"
                                cellpadding="5" cellspacing="0"
                                id="TB1" runat="server">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="2" valign="middle">
                            </td>                        
                        </tr>  --%>
                                <tr id="trDateRange" runat="server">
                                    <td colspan="4" align="left" class="title-bg ">Date Range</td>
                                </tr>
                                <tr>

                                    <td width="20%"><span class="field-label">From Date </span></td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtFDate" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="lnkFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFDate" PopupButtonID="lnkFDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="20%"><span class="field-label">To Date </span></td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtTDate" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="lnkTDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTDate" PopupButtonID="lnkTDate"></ajaxToolkit:CalendarExtender>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4" align="left">
                                        <br />
                                    </td>
                                </tr>

                                <tr id="trBSUnit" runat="server">
                                    <td width="20%" align="left" valign="top"><span class="field-label">Business Unit</span></td>
                                    <td align="left" valign="top" colspan="2">
                                        <div class="checkbox-list">
                                            <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                                        </div>
                                    </td>

                                </tr>
                                <tr id="trSummary" runat="server">
                                    <td width="20%" align="left" valign="top"><span class="field-label">Report Type</span></td>
                                    <td width="30%" align="left" valign="top">
                                        <asp:CheckBox runat="server" ID="chkReport" Text="Summary Report" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Button ID="btnGenerateReport" runat="server" CausesValidation="False" CssClass="button"
                                Text="Generate Report" UseSubmitBehavior="False" TabIndex="8" />
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
</asp:Content>
