﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="BS_ItemMasterBulkUpload.aspx.vb" Inherits="Inventory_BookSale_BS_ItemMasterBulkUpload" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function CheckNumber(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(2);
        }
        function AlertCall() {
            alert("This is a sample Excel File. You may use this excel file for entering the valid data and upload the excel file.")
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sale Item Master Bulk Upload
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" style="width: 100%">
                    <tr>
                        <td align="left" width="100%" colspan="4">
                            <%-- <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Upload File</span></td>
                        <td align="left" width="30%">
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button" />
                        </td>
                    </tr>

                </table>
                <br />
                <br />
                Sample Excel Format :
            <asp:LinkButton ID="LinkSample" OnClientClick="javascrip:AlertCall();" runat="server">Click Here</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>



