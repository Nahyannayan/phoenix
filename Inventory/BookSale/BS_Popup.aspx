﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BS_Popup.aspx.vb" Inherits="Inventory_BookSale_BS_Popup" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Select From the List</title>

    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">

        function SetValuetoItemDelivery(code, id) {
            //alert(id);
            parent.setReceiptValue(code);
            return false;
        }
        function SetValuetoReturnSales(code, id) {
            //alert(id);
            parent.setReceiptValue(code);
            return false;
        }

    </script>

    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>


    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <%--        <link href="/cssfiles/sb-admin.css" rel="stylesheet" >--%>
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="../../cssfiles/all-ie-only.css">
    <!--[endif]-->

    <!-- Bootstrap header files ends here -->

</head>
<body onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
        <input id="h_SelectedId" runat="server" type="hidden" value="" />
        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />

        <input id="hf_id" runat="server" type="hidden" value="=" />
        <input id="hf_code" runat="server" type="hidden" value="=" />

        <table width="100%" align="center">
            <tr valign="top">
                <td>
                    <asp:GridView ID="gvReceiptList" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                        EmptyDataText="No Data" Width="100%" AllowPaging="True" PageSize="16">
                        <Columns>
                            <asp:TemplateField HeaderText="ReceiptNo">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>

                                    <%--OnClick="linklblReceiptNo_Click"--%>
                                    <asp:LinkButton ID="lnkCode" runat="server" Text='<%# Bind("ReceiptNo")%>'></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblCode" runat="server"></asp:Label><br />
                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="StudentName">
                                <HeaderTemplate>
                                    <asp:Label ID="lblName" runat="server"></asp:Label><br />
                                    <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnStudentNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lnkName" runat="server" Text='<%# Bind("StudentName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Id" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("BSAH_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>

    </form>


</body>
</html>
