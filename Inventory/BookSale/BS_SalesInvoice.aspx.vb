﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports Lesnikowski.Barcode
Partial Class Inventory_BookSale_BS_SalesInvoice
    Inherits System.Web.UI.Page

    Dim INITIAL_QTY_CHECK As Boolean = False
    Dim INITIAL_QTY_CHECK_FOR_PLUS_QTY As Boolean = True
    Dim INITIAL_QTY_CHECK_FOR_ADD_ITEM As Boolean = True
    Private Property SALGRD() As DataTable
        Get
            Return ViewState("SALGRD")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SALGRD") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("sBsuid")
        If Session("sUsr_name") Is Nothing Then
            Response.Redirect("~\Login.aspx")
        End If
        If Page.IsPostBack = False Then

            Page.Title = "::GEMS Education |Book Sale::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = "add"
            Dim USR_NAME As String = Session("sUsr_name")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            'Dim CurUsr_id As String = Session("STU_ID")
            Dim CurBsUnit As String = Session("sBsuid")
            'Dim USR_NAME As String = Session("STU_NAME")
            'InitialiseCompnents()

            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else




                'lblschool.Text = ViewState("BSU_NAME")


                Gridbind_StuDetails()

            End If
            'Dim obj As Object = sender.parent
            'Dim GridSelBook As Label = DirectCast(obj.FindControl("txtItmSelect"), Label)
            'GridSelBook.Visible = True



            If SALGRD Is Nothing Then


            End If

            txtReceivedTotal.Enabled = False
            txtBalance.Enabled = False
            Dim qry As String = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM CREDITCARD_S WITH(NOLOCK) INNER JOIN " & _
                  " CREDITCARD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
                  " CREDITCARD_PROVD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
                  " WHERE (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
            Me.hfCobrand.Value = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)

        End If
    End Sub

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

    '    Gridbind_StuDetails()
    'End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Response.Redirect(ViewState("ReferrerUrl"))

    End Sub

    Protected Sub uscStudentPicker_StudentCleared(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentCleared
        ClearDetails()
    End Sub
    Sub ClearDetails()

        txtRemarks.Text = ""
        txtCashTotal.Text = "0.0"
        txtDue.Text = "0.0"
        txtCCTotal.Text = "0.0"
        txtReceivedTotal.Text = "0.0"
        txtBalance.Text = "0.0"
        txtCreditno.Text = ""

        pay_section.Visible = False
        repInfo.Visible = False

        ddCreditcard.SelectedIndex = 0
        Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S", ConnectionManger.GetOASISFINConnectionString)
        If Not ddCreditcard.Items.FindByValue(str_CRR_ID) Is Nothing Then
            ddCreditcard.ClearSelection()
            ddCreditcard.Items.FindByValue(str_CRR_ID).Selected = True
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        ClearDetails()
        'SetDataMode("add")
        'setModifyvalues(0)
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        uscStudentPicker.STU_ID = 0
        uscStudentPicker.ClearDetails()
        Gridbind_StuDetails()
    End Sub
    Protected Sub uscStudentPicker_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentNoChanged
        Try
            btnSave.Enabled = True
            Gridbind_StuDetails()
            uscStudentPicker.STU_MODULE = "BS"
            showsibling.Visible = True
            SALGRD = GetTable()
            CalculateTotal()
        Catch ex As Exception

        End Try
    End Sub
    Private Function TrimStudentNo(ByVal STUNO As String) As String
        TrimStudentNo = STUNO
        If STUNO.Length >= 14 Then
            TrimStudentNo = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(REPLACE('" & STUNO & "',SUBSTRING('" & STUNO & "',1,6),'')AS INTEGER)AS STUNO")
        End If
    End Function
    Public Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        btnSave.Enabled = False

        Dim vpc_Amount As Decimal = 0.0
        Dim StuNos As String = ""
        Dim errors As String = ""
        Dim Qty_Error As String = ""
        Dim Qty_Error_Check As Integer = 0
        If (FeeCollection.GetDoubleVal(txtCashTotal.Text) + FeeCollection.GetDoubleVal(txtCCTotal.Text) + FeeCollection.GetDoubleVal(txtCrCardCharges.Text) <> FeeCollection.GetDoubleVal(lblTotal.Text)) Then
            errors &= IIf(errors.Length = 0, "", ",") & "There is a mismatch in payable amount and paid amount"
        End If

        If errors.Length > 0 Then
            usrMessageBar2.ShowNotification(errors, UserControls_usrMessageBar.WarningType.Danger)
            btnSave.Enabled = True
            Exit Sub
        End If

        If CDbl(txtCCTotal.Text) > 0 Then
            If ddCreditcard.SelectedValue = "14" Or ddCreditcard.SelectedValue = "15" Or ddCreditcard.SelectedValue = "27" Or ddCreditcard.SelectedValue = "28" Or ddCreditcard.SelectedValue = "26" Then
                If txtCreditno.Text = "" Then
                    usrMessageBar2.ShowNotification("Please enter Authorisation Code for Credit Card", UserControls_usrMessageBar.WarningType.Danger)
                    btnSave.Enabled = True
                    Exit Sub
                End If

            End If
        End If
        'checking the available quantity on save click
        'Dim dtt As DataTable = SALGRD
        'For Each gvrow As DataRow In dtt.Rows
        '    Dim FNL_QTY As Integer = 0
        '    Dim dt0 As New DataTable
        '    Dim filter_str As String = ""
        '    Dim dtView As DataView = New DataView(SALGRD)
        '    filter_str = " ITEM_ID=" & gvrow("ITEM_ID") & ""
        '    dtView.RowFilter = filter_str
        '    dt0 = dtView.ToTable
        '    Dim ds5 As New DataSet
        '    Dim AVAIL_QTY As Integer = 0
        '    ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID") & ") AS AVAIL_QTY")
        '    AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
        '    If dt0.Rows.Count > 0 Then
        '        For Each gvrow2 As DataRow In dt0.Rows
        '            FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
        '        Next
        '    End If
        '    If FNL_QTY > AVAIL_QTY Then
        '        usrMessageBar2.ShowNotification("Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
        '        Exit Sub
        '    End If
        'Next


        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer = 0
        Try


            Dim STR_TYPE As Char = "S"
            Dim BSAHO_ID As Int64 = 0
            Dim BSAHO_BSAHO_ID As Int64 = 0

            Dim totalcc As Decimal = CDbl(txtCCTotal.Text) '
            Dim totalcash As Decimal = CDbl(txtCashTotal.Text)
            Dim netn As Decimal = 0.0
            Dim lopcc As Decimal = 0.0
            Dim lopcas As Decimal = 0.0

            For Each repitm As RepeaterItem In repInfo.Items
                Dim GridSelBook As New GridView

                GridSelBook = DirectCast(repitm.FindControl("grdSAL"), GridView)
                Dim hfSTU_ID As HiddenField = CType(repitm.FindControl("hfSTU_ID"), HiddenField)
                Dim hfSTU_FEE_ID As HiddenField = CType(repitm.FindControl("hfSTU_FEE_ID"), HiddenField)
                Dim hfSTU_ACD_ID As HiddenField = CType(repitm.FindControl("hfSTU_ACD_ID"), HiddenField)
                Dim lblStuNo As Label = CType(repitm.FindControl("lbSNo"), Label)
                Dim lblStuName As Label = CType(repitm.FindControl("lbSName"), Label)
                Dim lblGrade As Label = CType(repitm.FindControl("lbGrade"), Label)
                Dim lblSection As Label = CType(repitm.FindControl("lbSection"), Label)
                'Dim lblAmounttoPayF As Label = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label)
                Dim lblAmounttoPayF As Label = DirectCast(repitm.FindControl("txtGrandTotal"), Label) 'gives total amount paying now of current student
                'Dim h_ProcessingChargeF As HiddenField = DirectCast(repitm.FindControl("h_ProcessingChargeF"), HiddenField) 'gives total processing charge of current student

                'Total sales invoice payment should split to 1 to n receipts
                netn = CDbl(lblAmounttoPayF.Text)
                If netn <= totalcc And totalcc >= 1 And netn >= 1 Then

                    totalcc = totalcc - netn

                    lopcc = netn
                    netn = 0
                    lopcas = 0

                Else

                    If netn >= 1 Then
                        netn = netn - totalcc

                        lopcc = totalcc
                        totalcc = 0
                        lopcas = 0
                    End If
                End If

                If netn <= totalcash And totalcash >= 1 And netn >= 1 Then
                    totalcash = totalcash - netn

                    'lopcc=0
                    lopcas = netn
                    netn = 0
                Else

                    If netn >= 1 Then
                        netn = netn - totalcash

                        'lopcc= 0
                        lopcas = totalcash
                        totalcash = 0
                    End If
                End If
                'Total sales invoice payment should split to 1 to n receipts Ends Here..



                If Convert.ToDouble(lblAmounttoPayF.Text) > 0 Then
                    vpc_Amount = vpc_Amount + Convert.ToDouble(lblAmounttoPayF.Text)
                    StuNos = StuNos & IIf(StuNos = "", TrimStudentNo(hfSTU_FEE_ID.Value), "," & TrimStudentNo(hfSTU_FEE_ID.Value))

                    'Dim ds5 As New DataSet
                    Dim F_Year As String = "2019"
                    'ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from [DBO].FINANCIALYEAR_S WITH (NOLOCK) WHERE  bDefault=1")
                    'F_Year = (ds5.Tables(0).Rows(0)("FYR_ID").ToString())


                    Dim BSAH_NO As String = "NEW"

                    'SAVING HEADER DATA
                    retval = clsBookSalesOnline.SAVE_BOOK_SALE_H(BSAHO_ID, BSAHO_BSAHO_ID, BSAH_NO, "COUNTER", Session("sBsuid"), _
                    Format(Date.Now, "dd/MMM/yyyy"), "S", Session("sUsr_name"), hfSTU_ID.Value, lblStuNo.Text, lblStuName.Text, lblGrade.Text, lblSection.Text, _
                     F_Year, txtRemarks.Text, lblAmounttoPayF.Text, "", "", txtCrCardCharges.Text, lopcas, lopcc, _
                    txtReceivedTotal.Text, txtBalance.Text, txtCreditno.Text, ddCreditcard.SelectedValue, _
                    "", 0, lblAmounttoPayF.Text, stTrans, "", -1, "", "")

                    If retval = 0 Then



                        'txtCashTotal.Text, txtCCTotal.Text
                        If BSAHO_BSAHO_ID = 0 Then
                            BSAHO_BSAHO_ID = BSAHO_ID 'retval

                        End If
                        If BSAHO_ID = 0 Then
                            BSAHO_ID = BSAHO_ID 'retval
                        End If

                        'Dim ds7 As New DataSet
                        Dim p_Receiptno As String = ""
                        'ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT BSAH_NO from [dbo].[BOOK_SALE_H] WHERE  BSAH_ID=" & BSAHO_ID)
                        'p_Receiptno = (ds7.Tables(0).Rows(0)("BSAH_NO").ToString())
                        p_Receiptno = BSAH_NO
                        Dim barcode As BaseBarcode
                        barcode = BarcodeFactory.GetBarcode(Symbology.Code128)

                        barcode.Number = p_Receiptno
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                        barcode.ChecksumAdd = True
                        barcode.CustomText = p_Receiptno
                        barcode.NarrowBarWidth = 3
                        barcode.Height = 300
                        barcode.FontHeight = 0.3F
                        barcode.ForeColor = Drawing.Color.Black
                        Dim b As Byte()
                        ReDim b(barcode.Render(ImageType.Png).Length)
                        b = barcode.Render(ImageType.Png)

                        'Dim base64string As String = System.Convert.ToBase64String(b, 0, b.Length)
                        'Dim imgs As HtmlImage = New HtmlImage
                        'imgs = "data:image/png;base64," + base64string

                        'Dim query As String = "update [dbo].[BOOK_SALE_H] set [BSAH_BARCODE]='" + b & "' where BIM_ID=" & BSAHO_ID
                        'SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update [BOOK_ITEM_M] set [BIM_bDELETE]=1, [BIM_DELETED_DATE]='" + CDate(DateTime.Now) + "', [BIM_DELETED_USER]='" + Session("sUsr_id") + "' where BIM_ID=" & h_ITM_ID.Value)
                        'Dim cmd As SqlCommand = New SqlCommand("insert into binaryTable (example) values (@BSAH_BARCODE)", stTrans)
                        'cmd.Parameters.Add(new SqlParameter("@BSAH_BARCODE",HashEncript("password"))

                        retval = clsBookSalesOnline.UPDATE_BARCODE(1, BSAHO_ID, Session("sBsuid"), b, stTrans)
                        Dim RETURN_MESSGAE_VALUE As String = ""
                        If retval = 0 Then

                            retval = clsBookSalesOnline.SAVE_BOOK_SALE_DTLS(BSAHO_ID, SALGRD, lblStuNo.Text, Session("sBSUID").ToString, RETURN_MESSGAE_VALUE, stTrans)
                            If retval = -1 Then
                                'usrMessageBar2.ShowNotification("Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                                Qty_Error = RETURN_MESSGAE_VALUE '"Item quantity higher than available quantity. Available quantity"
                                'retval = -1
                                Qty_Error_Check = 1
                                Exit For
                            End If
                            'Dim dt As New DataTable '= SALGRD
                            'If Not SALGRD Is Nothing Then
                            '    Try
                            '        Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lblStuNo.Text.ToString Select order
                            '        dt = query.CopyToDataTable()
                            '    Catch ex As Exception
                            '    End Try
                            'Else
                            '    'dt = SALGRD
                            'End If
                            'If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            '    'Dim dbl_net_amt As Double
                            '    For Each gvrow As DataRow In dt.Rows
                            '        If lblStuNo.Text.ToString = gvrow("STU_NO").ToString Then
                            '            If CDbl(gvrow("NET_AMOUNT")) > 0 Then

                            '                'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                            '                '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                            '                '    retval = -1
                            '                '    Qty_Error_Check = 1
                            '                '    Exit For
                            '                'End If


                            '                'checking the quantity before saving line item
                            '                Dim FNL_QTY As Integer = 0
                            '                Dim dt0 As New DataTable
                            '                Dim filter_str As String = ""
                            '                Dim dtView As DataView = New DataView(SALGRD)
                            '                filter_str = " ITEM_ID=" & gvrow("ITEM_ID") & ""
                            '                dtView.RowFilter = filter_str
                            '                dt0 = dtView.ToTable
                            '                Dim ds50 As New DataSet
                            '                Dim AVAIL_QTY As Integer = 0
                            '                ds50 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID") & ") AS AVAIL_QTY")
                            '                AVAIL_QTY = CInt((ds50.Tables(0).Rows(0)("AVAIL_QTY")))
                            '                If dt0.Rows.Count > 0 Then
                            '                    For Each gvrow2 As DataRow In dt0.Rows
                            '                        FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                            '                    Next
                            '                End If
                            '                If FNL_QTY > AVAIL_QTY Then
                            '                    'usrMessageBar2.ShowNotification("Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                            '                    Qty_Error = "Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
                            '                    retval = -1
                            '                    Qty_Error_Check = 1
                            '                    Exit For
                            '                End If



                            '                'SAVING CHILD DATA
                            '                retval = clsBookSalesOnline.SAVE_BOOK_SALE_D(0, BSAHO_ID, CInt(gvrow("BSH_ID")), CDbl(gvrow("ITEM_ID")), _
                            '                       gvrow("ITEM_DESCR"), (gvrow("BOOK_NAME")), CDbl(gvrow("QTY")), CDbl(gvrow("PRICE")),
                            '                        CDbl(0), CDbl(gvrow("NET_AMOUNT")), CStr(gvrow("TAX_CODE")), CDbl(gvrow("TAX_AMOUNT")), stTrans)
                            '                If retval <> 0 Then
                            '                    'stTrans.Rollback()
                            '                    Exit For
                            '                End If
                            '            End If
                            '        End If

                            '    Next
                            'End If
                        Else
                            'stTrans.Rollback()
                            Exit For
                        End If
                        retval = clsBookSalesOnline.BOOKSALES_PAYMENT(BSAHO_ID, stTrans)
                    Else
                        stTrans.Rollback()
                        btnSave.Enabled = True
                        usrMessageBar2.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                End If
                BSAHO_ID = 0
                netn = 0
                lopcc = 0
                lopcas = 0
            Next
            If retval = 0 And Qty_Error_Check = 0 Then


                stTrans.Commit()
                'btnSave.Enabled = True
                'SendEmailNotification(2, BSAHO_BSAHO_ID, Session("sBsuid"), "BOOKSALE", "GEMS")
                SALGRD = Nothing
                h_print.Value = BSAHO_BSAHO_ID
                btnAddNew_Click(sender, e)
            Else
                stTrans.Rollback()
                btnSave.Enabled = True
                If Qty_Error_Check = 1 Then
                    'Qty_Error = "Below Items has Zero Quantity </br>" & Qty_Error
                    usrMessageBar2.ShowNotification(Qty_Error, UserControls_usrMessageBar.WarningType.Danger)
                Else
                    usrMessageBar2.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If

                'lblError.Text = getErrorMessage(retval)
                'lblError.CssClass = "alert alert-warning"
            End If
        Catch ex As Exception
            stTrans.Rollback()
            btnSave.Enabled = True
            usrMessageBar2.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Text = getErrorMessage(1000)
            'lblError.CssClass = "alert alert-warning"
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try
        'If retval = 0 Then
        '    Response.Redirect(ViewState("ReferrerUrl"))
        'End If
    End Sub
    Public Shared Function SendEmailNotification(ByVal OPTIONS As Integer, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal EML_TYPE As String, ByVal COMPANY As String) As Integer
        Dim pParms(5) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 1000)
        pParms(1).Value = STU_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 100)
        pParms(3).Value = EML_TYPE
        pParms(4) = New SqlClient.SqlParameter("@COMPANY", SqlDbType.VarChar, 50)
        pParms(4).Value = COMPANY

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
         CommandType.StoredProcedure, "OASIS.[DBO].[BULK_EMAIL_INSERT_SCHEDULE_JOB]", pParms)
        Return ReturnFlag

    End Function
    Protected Sub PaySibling_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))           
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(1) = New SqlClient.SqlParameter("@STU_ID", uscStudentPicker.STU_ID.ToString)

            If PaySibling.Checked Then
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 1)
            Else
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 2)
            End If

            SALGRD = GetTable()

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[DBO].[GETCHILD_INFO]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                pay_section.Visible = True
                repInfo.Visible = True
            Else
                pay_section.Visible = False
                repInfo.Visible = False
            End If
            'ViewState("STU_NO") = Session("STU_NO")
            'ViewState("STU_NAME") = Session("STU_NAME")repInfo
            'ViewState("STU_GRD_ID") = Session("STU_GRD_ID")
            'ViewState("stu_section") = Session("stu_section")
            'ViewState("BSU_NAME") = Session("BSU_NAME")
            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
            CalculateTotal()
        Catch ex As Exception
            repInfo.DataBind()

        End Try

    End Sub
    Sub Gridbind_StuDetails()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))           
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(1) = New SqlClient.SqlParameter("@STU_ID", uscStudentPicker.STU_ID.ToString)

            If PaySibling.Checked Then
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 1)
            Else
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 2)
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[DBO].[GETCHILD_INFO]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                pay_section.Visible = True
                repInfo.Visible = True
            Else
                pay_section.Visible = False
                repInfo.Visible = False
            End If
            'ViewState("STU_NO") = Session("STU_NO")
            'ViewState("STU_NAME") = Session("STU_NAME")repInfo
            'ViewState("STU_GRD_ID") = Session("STU_GRD_ID")
            'ViewState("stu_section") = Session("stu_section")
            'ViewState("BSU_NAME") = Session("BSU_NAME")
            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()

        Catch ex As Exception
            repInfo.DataBind()

        End Try
    End Sub

    Protected Sub rdbBookSets_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles rdbBookSets.SelectedIndexChanged
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label

        Dim Qty_Error As String = ""
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        DivHeaderRow = DirectCast(obj.FindControl("DivHeaderRow"), HtmlGenericControl)
        DivMainContent = DirectCast(obj.FindControl("DivMainContent"), HtmlGenericControl)
        DivFooterRow = DirectCast(obj.FindControl("DivFooterRow"), HtmlGenericControl)
        rblist = DirectCast(obj.FindControl("rdbBookSets"), CheckBoxList)
        txtTotal = DirectCast(obj.FindControl("txtGrandTotal"), Label)


        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)
        Dim lblStuNo As Label = DirectCast(obj.FindControl("lbSNo"), Label)

        Dim dt0 As New DataTable '= SALGRD
        Dim dt1 As New DataTable '= SALGRD
        If Not SALGRD Is Nothing Then

            Try
                'Dim query0 As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lblStuNo.Text.ToString And order.Field(Of String)("BSH_ID") <> "-1" Select order
                '   dt0 = query0.CopyToDataTable()               

                Dim filter_str As String = ""
                Dim dtView As DataView = New DataView(SALGRD)
                filter_str = "BSH_ID = -1 AND STU_NO=" & lblStuNo.Text.ToString
                dtView.RowFilter = filter_str
                dt0 = dtView.ToTable
                Dim incr As Integer = 1
                If Not dt0 Is Nothing AndAlso dt0.Rows.Count > 0 Then
                    For Each gvrow As DataRow In dt0.Rows
                        gvrow("ID") = incr
                        incr = incr + 1
                    Next
                End If


                dtView = New DataView(SALGRD)
                filter_str = " STU_NO <>" & lblStuNo.Text.ToString
                dtView.RowFilter = filter_str
                dt1 = dtView.ToTable
                SALGRD = dt0
                SALGRD.Merge(dt1)


            Catch ex As Exception
            End Try

            'add to salgrd   
            Dim rbvalue As String = "" '= rblist.SelectedValue
            For i As Integer = 0 To rblist.Items.Count - 1
                If rblist.Items(i).Selected Then
                    rbvalue = rbvalue + rblist.Items(i).Value + "|"
                End If
            Next

            If Not SALGRD Is Nothing And rbvalue <> "" Then

                Dim dt As DataTable = GetTable()
                'Dim dt2 As DataTable = GetTable()

                GridSelBook.Visible = True
                GridSelBook.ShowFooter = False


                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
                pParms(0).Value = 1
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = Session("sBSUID")
                pParms(2) = New SqlClient.SqlParameter("@BSH_ID", SqlDbType.VarChar, 200)
                pParms(2).Value = rbvalue

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_SET_ITEM_M]", pParms)
                If Not SALGRD Is Nothing Then
                    Try
                        Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                        dt = query.CopyToDataTable()

                    Catch ex As Exception
                    End Try
                    'dt = SALGRD
                    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Dim Row_Count As Integer = dt.Rows.Count + 1
                        For Each gvrow As DataRow In ds.Tables(0).Rows
                            Dim dr As DataRow
                            dr = dt.NewRow()

                            Dim FNL_QTY As Integer = 0
                            Dim AVAIL_QTY As Integer = 0
                            If INITIAL_QTY_CHECK = True Then
                                'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                                '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                                'End If
                                Dim ds5 As New DataSet
                                ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                                AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                                'in plus, check item multiple times in salgrd
                                Dim dt00 As New DataTable
                                Dim filter_str0 As String = ""
                                Dim dtView0 As DataView = New DataView(SALGRD)
                                filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                                dtView0.RowFilter = filter_str0
                                dt00 = dtView0.ToTable
                                If dt00.Rows.Count > 0 Then
                                    For Each gvrow2 As DataRow In dt00.Rows
                                        FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                                    Next
                                End If
                                FNL_QTY = FNL_QTY + 1
                            End If

                            If FNL_QTY > AVAIL_QTY And INITIAL_QTY_CHECK = True Then
                                'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                                'Exit Sub
                            Else

                                dr("ID") = Row_Count 'gvrow("ID").ToString
                                dr("BSH_ID") = gvrow("BSH_ID").ToString
                                dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                                dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                                dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                                dr("ISBN") = gvrow("ISBN").ToString
                                dr("PRICE") = gvrow("PRICE").ToString
                                dr("QTY") = gvrow("QTY").ToString
                                dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                                dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                                dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                                dr("STU_NO") = lbSNo.Text

                                dt.Rows.Add(dr)
                                SALGRD.ImportRow(dr)
                                Row_Count = Row_Count + 1
                            End If
                        Next
                    End If

                Else
                    'dt = SALGRD
                    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each gvrow As DataRow In ds.Tables(0).Rows
                            Dim dr As DataRow
                            dr = dt.NewRow()

                            Dim FNL_QTY As Integer = 0
                            Dim AVAIL_QTY As Integer = 0
                            If INITIAL_QTY_CHECK = True Then
                                'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                                '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                                'End If

                                Dim ds5 As New DataSet
                                ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                                AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                                'in plus, check item multiple times in salgrd
                                Dim dt00 As New DataTable
                                Dim filter_str0 As String = ""
                                Dim dtView0 As DataView = New DataView(SALGRD)
                                filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                                dtView0.RowFilter = filter_str0
                                dt00 = dtView0.ToTable
                                If dt00.Rows.Count > 0 Then
                                    For Each gvrow2 As DataRow In dt00.Rows
                                        FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                                    Next
                                End If
                                FNL_QTY = FNL_QTY + 1
                            End If

                            If FNL_QTY > AVAIL_QTY And INITIAL_QTY_CHECK = True Then
                                'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                                'Exit Sub
                            Else
                                dr("ID") = gvrow("ID").ToString
                                dr("BSH_ID") = gvrow("BSH_ID").ToString
                                dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                                dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                                dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                                dr("ISBN") = gvrow("ISBN").ToString
                                dr("PRICE") = gvrow("PRICE").ToString
                                dr("QTY") = gvrow("QTY").ToString
                                dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                                dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                                dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                                dr("STU_NO") = lbSNo.Text

                                dt.Rows.Add(dr)
                            End If
                        Next
                    End If
                    SALGRD = dt

                End If


                'GridSelBook.Height = 240
                GridSelBook.DataSource = dt
                GridSelBook.DataBind()
                CalculateTotal()
            Else

                Dim filter_str As String = ""
                Dim dtView As DataView = New DataView(SALGRD)
                filter_str = "STU_NO=" & lblStuNo.Text.ToString
                dtView.RowFilter = filter_str
                dt0 = dtView.ToTable
                'GridSelBook.Height = 240
                GridSelBook.DataSource = dt0
                GridSelBook.DataBind()
                CalculateTotal()
                'If dt0.Rows.Count <> 0 Then
                '    'Try
                '    'Dim dt As DataTable = GetTable()
                '    'Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                '    'dt = query.CopyToDataTable()
                '    'Catch ex As Exception
                '    'End Try
                '    GridSelBook.Visible = True
                '    GridSelBook.ShowFooter = False
                'Else
                '    GridSelBook.Visible = False
                '    GridSelBook.ShowFooter = False
                'End If
            End If

        Else
            'dt = SALGRD


            Dim dt As DataTable = GetTable()
            'Dim dt2 As DataTable = GetTable()

            GridSelBook.Visible = True
            GridSelBook.ShowFooter = False
            Dim rbvalue As String = "" '= rblist.SelectedValue

            For i As Integer = 0 To rblist.Items.Count - 1
                If rblist.Items(i).Selected Then
                    rbvalue = rbvalue + rblist.Items(i).Value + "|"
                End If
            Next

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = Session("sBSUID")
            pParms(2) = New SqlClient.SqlParameter("@BSH_ID", SqlDbType.VarChar, 200)
            pParms(2).Value = rbvalue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_SET_ITEM_M]", pParms)
            If Not SALGRD Is Nothing Then
                Try
                    Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                    dt = query.CopyToDataTable()

                Catch ex As Exception
                End Try
                'dt = SALGRD
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim Row_Count As Integer = dt.Rows.Count + 1
                    For Each gvrow As DataRow In ds.Tables(0).Rows
                        Dim dr As DataRow
                        dr = dt.NewRow()

                        Dim FNL_QTY As Integer = 0
                        Dim AVAIL_QTY As Integer = 0
                        If INITIAL_QTY_CHECK = True Then
                            'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                            '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                            'End If

                            Dim ds5 As New DataSet
                            ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                            AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                            'in plus, check item multiple times in salgrd
                            Dim dt00 As New DataTable
                            Dim filter_str0 As String = ""
                            Dim dtView0 As DataView = New DataView(SALGRD)
                            filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                            dtView0.RowFilter = filter_str0
                            dt00 = dtView0.ToTable
                            If dt00.Rows.Count > 0 Then
                                For Each gvrow2 As DataRow In dt00.Rows
                                    FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                                Next
                            End If
                            FNL_QTY = FNL_QTY + 1
                        End If

                        If FNL_QTY > AVAIL_QTY And INITIAL_QTY_CHECK = True Then
                            'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                            'Exit Sub
                        Else
                            dr("ID") = Row_Count 'gvrow("ID").ToString
                            dr("BSH_ID") = gvrow("BSH_ID").ToString
                            dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                            dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                            dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                            dr("ISBN") = gvrow("ISBN").ToString
                            dr("PRICE") = gvrow("PRICE").ToString
                            dr("QTY") = gvrow("QTY").ToString
                            dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                            dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                            dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                            dr("STU_NO") = lbSNo.Text

                            dt.Rows.Add(dr)
                            SALGRD.ImportRow(dr)
                            Row_Count = Row_Count + 1
                        End If
                    Next
                End If
                'Dim dr1 As DataRow = dt.NewRow()
                'dr1("ID") = ds.Tables(0).Rows(0)("ID").ToString
                'dr1("BSH_ID") = ds.Tables(0).Rows(0)("BSH_ID").ToString
                'dr1("ITEM_DESCR") = ds.Tables(0).Rows(0)("ITEM_DESCR").ToString
                'dr1("ISBN") = ds.Tables(0).Rows(0)("ISBN").ToString
                'dr1("PRICE") = ds.Tables(0).Rows(0)("PRICE").ToString
                'dr1("QTY") = ds.Tables(0).Rows(0)("QTY").ToString
                'dr1("TAX_AMOUNT") = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
                'dr1("NET_AMOUNT") = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
                'dt.Rows.Add(dr1)
                'SALGRD = dt
                'SALGRD.Merge(dt2)
            Else
                'dt = SALGRD
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each gvrow As DataRow In ds.Tables(0).Rows
                        Dim dr As DataRow
                        dr = dt.NewRow()

                        Dim FNL_QTY As Integer = 0
                        Dim AVAIL_QTY As Integer = 0
                        If INITIAL_QTY_CHECK = True Then
                            'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                            '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                            'End If

                            Dim ds5 As New DataSet
                            ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                            AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                            'in plus, check item multiple times in salgrd
                            Dim dt00 As New DataTable
                            Dim filter_str0 As String = ""
                            Dim dtView0 As DataView = New DataView(SALGRD)
                            filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                            dtView0.RowFilter = filter_str0
                            dt00 = dtView0.ToTable
                            If dt00.Rows.Count > 0 Then
                                For Each gvrow2 As DataRow In dt00.Rows
                                    FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                                Next
                            End If
                            FNL_QTY = FNL_QTY + 1
                        End If

                        If FNL_QTY > AVAIL_QTY And INITIAL_QTY_CHECK = True Then
                            'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                            'Exit Sub
                        Else
                            dr("ID") = gvrow("ID").ToString
                            dr("BSH_ID") = gvrow("BSH_ID").ToString
                            dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                            dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                            dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                            dr("ISBN") = gvrow("ISBN").ToString
                            dr("PRICE") = gvrow("PRICE").ToString
                            dr("QTY") = gvrow("QTY").ToString
                            dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                            dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                            dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                            dr("STU_NO") = lbSNo.Text

                            dt.Rows.Add(dr)
                        End If
                    Next
                End If
                SALGRD = dt

            End If


            'GridSelBook.Height = 240
            GridSelBook.DataSource = dt
            GridSelBook.DataBind()
            CalculateTotal()


        End If

        If Qty_Error <> "" Then

            'Qty_Error = "Below Items has Zero Quantity </br>" & Qty_Error
            usrMessageBar2.ShowNotification(Qty_Error, UserControls_usrMessageBar.WarningType.Danger)
        End If

        'Dim k As String = GridSelBook.ClientID
        'Dim et As String = k(k.Length - 1)
        'If et = "0" Then
        '    ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader0('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)
        'ElseIf et = "1" Then
        '    ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader1('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

        'End If
        'used script
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

    End Sub

    Protected Function GET_QTY(ByVal STR_ITEM_ID As String) As Integer
        Dim QTY As Integer = 0
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        pParms(2) = New SqlClient.SqlParameter("@BIM_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = STR_ITEM_ID
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_ITEM_QTY]", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            QTY = CInt(ds.Tables(0).Rows(0)("QTY"))
        End If
        Return QTY
    End Function
    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        Dim hfSTU_ID As New HiddenField
        Dim hfSTU_ACD_ID As New HiddenField
        Dim GridSelBook As New Panel
        Dim lblGvrError As New Label
        Dim grdSAL As New GridView
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl
        Dim SelectItem As New HtmlGenericControl

        'Dim btnMinus As New ImageButton
        'Dim btnPlus As New ImageButton
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            'Dim rblist As RadioButtonList = DirectCast(e.Item.FindControl("rdbBookSets"), RadioButtonList)
            Dim rblist As CheckBoxList = DirectCast(e.Item.FindControl("rdbBookSets"), CheckBoxList)
            GridSelBook = DirectCast(e.Item.FindControl("GridScroll"), Panel)
            grdSAL = DirectCast(e.Item.FindControl("grdSAL"), GridView)
            DivHeaderRow = DirectCast(e.Item.FindControl("DivHeaderRow"), HtmlGenericControl)
            DivMainContent = DirectCast(e.Item.FindControl("DivMainContent"), HtmlGenericControl)
            DivFooterRow = DirectCast(e.Item.FindControl("DivFooterRow"), HtmlGenericControl)
            SelectItem = DirectCast(e.Item.FindControl("SelectItem"), HtmlGenericControl)
            'btnMinus = DirectCast(e.Item.FindControl("btnMinus"), ImageButton)
            'btnPlus = DirectCast(e.Item.FindControl("btnPlus"), ImageButton)

            Dim hfSTU_GRD_ID As HiddenField = DirectCast(e.Item.FindControl("hfSTU_GRD_ID"), HiddenField)
            'rblist.Items.Add(New ListItem("Science", "1"))
            Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim str_Sql As String = " SELECT * FROM [dbo].[BOOK_SET_H]  WHERE ISNULL([BSH_bSHOWONLINE],0) IN (0,1) AND ISNULL([BSH_DELETED],0)=0 AND " _
                                     & " [BSH_BSU_ID]  ='" + Session("SBSUID") + "' AND [BSH_GRD_ID]= '" + hfSTU_GRD_ID.Value + "'  ORDER BY [BSH_ID] "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            rblist.DataSource = ds
            rblist.DataTextField = "BSH_DESCR"
            rblist.DataValueField = "BSH_ID"
            rblist.DataBind()
            If SALGRD Is Nothing Then
                GridSelBook.Visible = False
                'grdSAL.Visible = False
            Else

                GridSelBook.Visible = True
                'grdSAL.Visible = True
            End If

            'id_show_add_items
            Dim id_show_add_items As Boolean = 0
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = Session("sBSUID")
            Dim ds0 As New DataSet
            ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MORE_ITEMS]", pParms)
            id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
            If id_show_add_items = True Then
                SelectItem.Visible = True
            Else
                SelectItem.Visible = False
            End If

        End If
    End Sub

    Protected Sub btnMinus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbl_PID"), Label)
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        Dim rowIndex = (CType((CType(sender, Control)).NamingContainer, GridViewRow)).RowIndex
        'GridSelBook.Rows(GridSelBook.SelectedIndex).Cells(1).Text = "7"
        'GridSelBook.Rows(0).Cells(3).Text = "7"
        Dim lbl1 As Label = DirectCast(obj.FindControl("lbl_QTY"), Label) '(Label)GridSelBook.FindControl("lblQuestion_Scripting");
        Dim lbl_ID As Label = DirectCast(obj.FindControl("lbl_ID"), Label)
        Dim Cur_Qty As Integer = CInt(lbl1.Text)
        Dim Cur_Net_Amt As Double = 0.0

        'id_show_add_items
        Dim id_show_add_items As Boolean = 0
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 1
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms0(1).Value = Session("sBSUID")
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MINUS_ITEMS]", pParms0)
        id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        If id_show_add_items = True Then


            If Cur_Qty >= 1 Then
                Cur_Qty = Cur_Qty - 1
                lbl1.Text = "" & Cur_Qty

                Dim lbl2 As Label = DirectCast(obj.FindControl("lbl_PRICE"), Label)
                Dim lbl3 As Label = DirectCast(obj.FindControl("lbl_TAX_AMOUNT"), Label)
                Dim lbl4 As Label = DirectCast(obj.FindControl("lbl_NET_AMOUNT"), Label)
                Dim Cur_Price As Double = CDbl(lbl2.Text)
                Dim Cur_Tax_Amt As Double = CDbl(lbl3.Text)
                Cur_Net_Amt = CDbl(lbl4.Text)
                Cur_Net_Amt = (Cur_Price + Cur_Tax_Amt) * Cur_Qty
                lbl4.Text = "" & Cur_Net_Amt

                ''TAX AMOUNT SHOWS AS ZERO FOR ZERO QTY
                'If Cur_Qty = 0 Then
                '    lbl3.Text = "0.00"
                'End If

            End If

            For Each dr As DataRow In SALGRD.Rows

                If dr("ID") = lbl_ID.Text.ToString And lbSNo.Text.ToString = dr("STU_NO") Then
                    dr("QTY") = "" & Cur_Qty
                    dr("NET_AMOUNT") = "" & Cur_Net_Amt
                End If
            Next

            CalculateTotal()
        End If
    End Sub
    Protected Sub btnPlus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbl_PID"), Label)
        'GridSelBook = DirectCast(Me.FindControl("grdSAL"), GridView)
        'Dim rowIndex = (CType((CType(sender, Control)).NamingContainer, GridViewRow)).RowIndex
        Dim lbl1 As Label = DirectCast(obj.FindControl("lbl_QTY"), Label) '(Label)GridSelBook.FindControl("lblQuestion_Scripting");
        Dim lbl_ID As Label = DirectCast(obj.FindControl("lbl_ID"), Label)
        Dim lbl_ITMID As Label = DirectCast(obj.FindControl("lbl_ITMID"), Label)
        Dim lbl_BOOK_NAME As Label = DirectCast(obj.FindControl("lbl_BOOK_NAME"), Label)
        Dim Cur_Qty As Integer = CInt(lbl1.Text)
        Dim Cur_Net_Amt As Double = 0.0

        Dim AVAIL_QTY As Integer = 0
        If INITIAL_QTY_CHECK_FOR_PLUS_QTY = True Then


            Dim FNL_QTY As Integer = 0
            Dim ds5 As New DataSet

            ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & lbl_ITMID.Text & ") AS AVAIL_QTY")
            AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))

            'in plus, check item multiple times in salgrd
            Dim dt00 As New DataTable
            Dim filter_str0 As String = ""
            Dim dtView0 As DataView = New DataView(SALGRD)
            filter_str0 = " ITEM_ID=" & lbl_ITMID.Text & ""
            dtView0.RowFilter = filter_str0
            dt00 = dtView0.ToTable



            If dt00.Rows.Count > 0 Then
                For Each gvrow2 As DataRow In dt00.Rows
                    FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                Next
            End If

            FNL_QTY = FNL_QTY + 1
            If FNL_QTY > AVAIL_QTY Then
                usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        Else
            AVAIL_QTY = Cur_Qty + 2
        End If

        'id_show_add_items
        Dim id_show_add_items As Boolean = 0
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 1
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms0(1).Value = Session("sBSUID")
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MINUS_ITEMS]", pParms0)
        id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        If id_show_add_items = True And Cur_Qty < AVAIL_QTY Then 'not inside if 2<2

            If Cur_Qty >= 0 And Cur_Qty < 99 Then
                Cur_Qty = Cur_Qty + 1
                lbl1.Text = "" & Cur_Qty

                Dim lbl2 As Label = DirectCast(obj.FindControl("lbl_PRICE"), Label)
                Dim lbl3 As Label = DirectCast(obj.FindControl("lbl_TAX_AMOUNT"), Label)
                Dim lbl4 As Label = DirectCast(obj.FindControl("lbl_NET_AMOUNT"), Label)
                Dim Cur_Price As Double = CDbl(lbl2.Text)
                Dim Cur_Tax_Amt As Double = CDbl(lbl3.Text)
                Cur_Net_Amt = CDbl(lbl4.Text)
                Cur_Net_Amt = (Cur_Price + Cur_Tax_Amt) * Cur_Qty
                lbl4.Text = "" & Cur_Net_Amt
            End If

            For Each dr As DataRow In SALGRD.Rows

                If dr("ID") = lbl_ID.Text.ToString And lbSNo.Text.ToString = dr("STU_NO") Then

                    dr("QTY") = "" & Cur_Qty
                    dr("NET_AMOUNT") = "" & Cur_Net_Amt
                End If
            Next

            CalculateTotal()
        End If
    End Sub
    Private Sub CalculateTotal()
        Dim Cur_Net_Amt As Double
        Dim courier_amount As Decimal
        Dim Cur_VAT_Amt As Double
        Dim Cur_SubTotal_Amt As Double
        Dim Net_Pay_Amt As Double
        For Each repitm As RepeaterItem In repInfo.Items
            Dim GridSelBook As New GridView
            Dim GridScroll As New Panel
            Dim lbSNo As New Label
            Dim DivHeaderRow As New HtmlGenericControl
            Dim DivMainContent As New HtmlGenericControl
            Dim DivFooterRow As New HtmlGenericControl
            'Dim btnMinus1 As New ImageButton
            'Dim btnPlus1 As New ImageButton

            lbSNo = DirectCast(repitm.FindControl("lbSNo"), Label)
            GridSelBook = DirectCast(repitm.FindControl("grdSAL"), GridView)
            GridScroll = DirectCast(repitm.FindControl("GridScroll"), Panel)
            Dim lbl0 As Label = DirectCast(repitm.FindControl("txtGrandTotal"), Label)
            Dim lblSubTotal As Label = DirectCast(repitm.FindControl("txtSubTotal"), Label)
            Dim lblSubVATTotal As Label = DirectCast(repitm.FindControl("txtSubVATTotal"), Label)
            DivHeaderRow = DirectCast(repitm.FindControl("DivHeaderRow"), HtmlGenericControl)
            DivMainContent = DirectCast(repitm.FindControl("DivMainContent"), HtmlGenericControl)
            DivFooterRow = DirectCast(repitm.FindControl("DivFooterRow"), HtmlGenericControl)
            'btnMinus1 = DirectCast(repitm.FindControl("btnMinus"), ImageButton)
            'btnPlus1 = DirectCast(repitm.FindControl("btnPlus"), ImageButton)


            'Dim lbl10 As Label = DirectCast(repitm.FindControl("lblTotal"), Label)
            'For Each gvRow As GridViewRow In GridSelBook.Rows

            '    Dim lbl4 As Label = (CType(gvRow.FindControl("lbl_NET_AMOUNT"), Label))
            '    Cur_Net_Amt = Cur_Net_Amt + CDbl(lbl4.Text)

            'Next
            Dim dt As New DataTable '= SALGRD
            If Not SALGRD Is Nothing Then
                Try
                    Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                    dt = query.CopyToDataTable()
                Catch ex As Exception
                End Try
            Else
                'dt = SALGRD
            End If


            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                'Dim dbl_net_amt As Double

                'GridSelBook.Visible = True
                If dt.Rows.Count <= 4 Then

                    DivMainContent.Attributes("class") = "main_content_grid0"
                    GridScroll.CssClass = "grid_scroll0"
                Else
                    DivMainContent.Attributes("class") = "main_content_grid"
                    GridScroll.CssClass = "grid_scroll"

                End If
                GridScroll.Visible = True

                For Each gvrow As DataRow In dt.Rows
                    If lbSNo.Text = gvrow("STU_NO").ToString Then

                        Cur_Net_Amt = Cur_Net_Amt + CDbl(gvrow("NET_AMOUNT"))
                        Cur_SubTotal_Amt = Cur_SubTotal_Amt + (CDbl(gvrow("PRICE")) * CDbl(gvrow("QTY")))
                        Cur_VAT_Amt = Cur_VAT_Amt + (CDbl(gvrow("TAX_AMOUNT")) * CDbl(gvrow("QTY")))
                    End If

                Next
            Else
                GridScroll.Visible = False
            End If
            lbl0.Text = "" & Convert.ToDouble(Cur_Net_Amt).ToString("#,###,##0.00")
            lblSubTotal.Text = "" & Convert.ToDouble(Cur_SubTotal_Amt).ToString("#,###,##0.00")
            lblSubVATTotal.Text = "" & Convert.ToDouble(Cur_VAT_Amt).ToString("#,###,##0.00")



            Net_Pay_Amt = Net_Pay_Amt + Cur_Net_Amt
            Cur_Net_Amt = 0.0
            Cur_SubTotal_Amt = 0.0
            Cur_VAT_Amt = 0.0
            lblTotal.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00")
            txtTotal.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00")
            txtDue.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00")

        Next


        'COURIER AMOUNT ADDING SECTION
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_COURIER_FEE]", pParms)
        courier_amount = Convert.ToDecimal(ds.Tables(0).Rows(0)("COURIER_FEE").ToString())
        'If rbl_deliverytype.SelectedItem.Value = 2 Then
        '    If Net_Pay_Amt > 0 Then
        'Net_Pay_Amt = Net_Pay_Amt + courier_amount
        'lblTotal.Text = Net_Pay_Amt
        'Else
        '    rbl_deliverytype.SelectedValue = 0
        '    id_courier.Visible = False

        '    End If
        'End If

    End Sub

    Public Function GetTable() As DataTable
        Dim dt As DataTable = New DataTable()
        dt.Columns.Add("ID")
        dt.Columns.Add("BSH_ID")
        dt.Columns.Add("ITEM_ID")
        dt.Columns.Add("ITEM_DESCR")
        dt.Columns.Add("BOOK_NAME")
        dt.Columns.Add("ISBN")
        dt.Columns.Add("PRICE")
        dt.Columns.Add("QTY")
        dt.Columns.Add("TAX_CODE")
        dt.Columns.Add("TAX_AMOUNT")
        dt.Columns.Add("NET_AMOUNT")
        dt.Columns.Add("STU_NO")
        Return dt
    End Function
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim obj As Object = sender.parent

        Dim hf_Item_ID As New HiddenField
        hf_Item_ID = DirectCast(obj.FindControl("hf_Item_ID"), HiddenField)
        Dim STR_ITEM_ID As String = hf_Item_ID.Value

        Dim GridSelBook As New GridView
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl

        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        DivHeaderRow = DirectCast(obj.FindControl("DivHeaderRow"), HtmlGenericControl)
        DivMainContent = DirectCast(obj.FindControl("DivMainContent"), HtmlGenericControl)
        DivFooterRow = DirectCast(obj.FindControl("DivFooterRow"), HtmlGenericControl)

        'GridSelBook.AllowPaging = False
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 5
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        pParms(2) = New SqlClient.SqlParameter("@BIM_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = STR_ITEM_ID



        Dim dt0 As New DataTable
        Dim filter_str As String = ""
        Dim dtView As DataView = New DataView(SALGRD)
        filter_str = " BSH_ID=-1 AND ITEM_ID=" & STR_ITEM_ID & " AND STU_NO='" & lbSNo.Text & "'"
        dtView.RowFilter = filter_str
        dt0 = dtView.ToTable
        If dt0.Rows.Count > 0 Then
            usrMessageBar2.ShowNotification("Cannot add same item master", UserControls_usrMessageBar.WarningType.Danger)
        Else

            Dim FNL_QTY As Integer = 0
            Dim STR_ITEM_NAME As String = ""
            If INITIAL_QTY_CHECK_FOR_ADD_ITEM = True Then


                'add more item, check the item already (multiple times) in salgrd
                Dim dtt As DataTable '= SALGRD
                'Dim dt00 As New DataTable
                Dim filter_str0 As String = ""
                Dim dtView0 As DataView = New DataView(SALGRD)
                'filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID") & "" 'commented on 31AUG2019
                filter_str0 = " ITEM_ID=" & STR_ITEM_ID & ""
                dtView0.RowFilter = filter_str0
                dtt = dtView0.ToTable
                Dim ds5 As New DataSet
                Dim AVAIL_QTY As Integer = 0
                ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & STR_ITEM_ID & ") AS AVAIL_QTY")
                AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))

                For Each gvrow As DataRow In dtt.Rows
                    FNL_QTY = FNL_QTY + CDbl(gvrow("QTY"))
                    STR_ITEM_NAME = gvrow("BOOK_NAME")
                Next
                FNL_QTY = FNL_QTY + 1 'we are incrementing because we have to add one item to the list
                If FNL_QTY > AVAIL_QTY Then
                    usrMessageBar2.ShowNotification("Item (" & STR_ITEM_NAME & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If

            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_ITEM_M]", pParms)


            Dim dt As DataTable = GetTable()

            If Not SALGRD Is Nothing Then '
                'dt = SALGRD
                Try
                    Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                    dt = query.CopyToDataTable()

                Catch ex As Exception
                End Try
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then



                    Dim dr1 As DataRow = dt.NewRow()
                    ''Dim dr2 As DataRow = dt2.NewRow()
                    dr1("ID") = dt.Rows.Count + 1 'ds.Tables(0).Rows(0)("ID").ToString
                    dr1("BSH_ID") = "-1" 'ds.Tables(0).Rows(0)("BSH_ID").ToString
                    dr1("ITEM_ID") = ds.Tables(0).Rows(0)("ITEM_ID").ToString
                    dr1("ITEM_DESCR") = ds.Tables(0).Rows(0)("ITEM_DESCR").ToString
                    dr1("BOOK_NAME") = ds.Tables(0).Rows(0)("BOOK_NAME").ToString
                    dr1("ISBN") = ds.Tables(0).Rows(0)("ISBN").ToString
                    dr1("PRICE") = ds.Tables(0).Rows(0)("PRICE").ToString
                    dr1("QTY") = ds.Tables(0).Rows(0)("QTY").ToString
                    dr1("TAX_CODE") = ds.Tables(0).Rows(0)("TAX_CODE").ToString
                    dr1("TAX_AMOUNT") = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
                    dr1("NET_AMOUNT") = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
                    dr1("STU_NO") = lbSNo.Text
                    dt.Rows.Add(dr1)
                    SALGRD.ImportRow(dr1)
                    'dt2.Rows.Add(dr2)
                End If
                ''SALGRD = dt
                'SALGRD.Merge(dt2)
            Else
                'dt = SALGRD
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then



                    For Each gvrow As DataRow In ds.Tables(0).Rows
                        Dim dr As DataRow
                        dr = dt.NewRow()

                        dr("ID") = gvrow("ID").ToString
                        dr("BSH_ID") = "-1" 'gvrow("BSH_ID").ToString
                        dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                        dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                        dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                        dr("ISBN") = gvrow("ISBN").ToString
                        dr("PRICE") = gvrow("PRICE").ToString
                        dr("QTY") = gvrow("QTY").ToString
                        dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                        dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                        dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                        dr("STU_NO") = lbSNo.Text

                        dt.Rows.Add(dr)
                    Next
                End If

                SALGRD = dt
            End If
            GridSelBook.DataSource = dt
            GridSelBook.DataBind()
            CalculateTotal()
        End If
        'GridSelBook.Height = 240

        'used script
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FREEZE", "FreezeGrid('" & GridSelBook.ClientID & "');", True)
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "', 200, 1000 , 40 ,false); </script>", False)
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

    End Sub
    <System.Web.Services.WebMethod()>
    Public Shared Function GetItmDescr(ByVal prefixText As String, ByVal contextKey As String) As String()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

        Dim StrGrade As String = "00"
        Dim StrSQL As String
        If Not contextKey Is Nothing Then
            'contextKey = ""
            Dim StrSQL2 = "select STU_GRD_ID from [OASIS].[DBO].[STUDENT_M] WITH (NOLOCK) where [STU_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [STU_ID] = '" & contextKey & "'"

            Dim ds2 As DataSet
            ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL2)
            StrGrade = ds2.Tables(0).Rows(0)("STU_GRD_ID").ToString()

        End If

        StrSQL = "select top 10 DESCRIPTION,BIM_ID from (select RTRIM(LTRIM(CONCAT([BIM_BOOK_NAME],' - ',BIM_ISBN))) AS DESCRIPTION,[BIM_BOOK_NAME] BOOK_NAME,BIM_ID from [DBO].[BOOK_ITEM_M] WITH (NOLOCK) where [BIM_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1 AND BIM_GRD_ID LIKE '%" & StrGrade & "%' ) a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("BIM_ID").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
        Next
        Return items.ToArray()
    End Function
    Protected Sub btnSelectBook_Click(sender As Object, e As EventArgs)

        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        'rblist = DirectCast(obj.FindControl("rdbBookSets"), RadioButtonList)
        DivHeaderRow = DirectCast(obj.FindControl("DivHeaderRow"), HtmlGenericControl)
        DivMainContent = DirectCast(obj.FindControl("DivMainContent"), HtmlGenericControl)
        DivFooterRow = DirectCast(obj.FindControl("DivFooterRow"), HtmlGenericControl)
        rblist = DirectCast(obj.FindControl("rdbBookSets"), CheckBoxList)
        txtTotal = DirectCast(obj.FindControl("txtGrandTotal"), Label)
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)

        Dim dt As DataTable = GetTable()
        'Dim dt2 As DataTable = GetTable()

        GridSelBook.Visible = True
        GridSelBook.ShowFooter = False
        Dim rbvalue As String = "" '= rblist.SelectedValue

        For i As Integer = 0 To rblist.Items.Count - 1
            If rblist.Items(i).Selected Then
                rbvalue = rbvalue + rblist.Items(i).Value + "|"
            End If
        Next

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        pParms(2) = New SqlClient.SqlParameter("@BSH_ID", SqlDbType.VarChar, 200)
        pParms(2).Value = rbvalue

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_SET_ITEM_M]", pParms)
        If Not SALGRD Is Nothing Then
            Try
                Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                dt = query.CopyToDataTable()

            Catch ex As Exception
            End Try
            'dt = SALGRD
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim Row_Count As Integer = dt.Rows.Count + 1
                For Each gvrow As DataRow In ds.Tables(0).Rows
                    Dim dr As DataRow
                    dr = dt.NewRow()


                    dr("ID") = Row_Count 'gvrow("ID").ToString
                    dr("BSH_ID") = gvrow("BSH_ID").ToString
                    dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                    dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                    dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                    dr("ISBN") = gvrow("ISBN").ToString
                    dr("PRICE") = gvrow("PRICE").ToString
                    dr("QTY") = gvrow("QTY").ToString
                    dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                    dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                    dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                    dr("STU_NO") = lbSNo.Text

                    dt.Rows.Add(dr)
                    SALGRD.ImportRow(dr)
                    Row_Count = Row_Count + 1
                Next
            End If
            'Dim dr1 As DataRow = dt.NewRow()
            'dr1("ID") = ds.Tables(0).Rows(0)("ID").ToString
            'dr1("BSH_ID") = ds.Tables(0).Rows(0)("BSH_ID").ToString
            'dr1("ITEM_DESCR") = ds.Tables(0).Rows(0)("ITEM_DESCR").ToString
            'dr1("ISBN") = ds.Tables(0).Rows(0)("ISBN").ToString
            'dr1("PRICE") = ds.Tables(0).Rows(0)("PRICE").ToString
            'dr1("QTY") = ds.Tables(0).Rows(0)("QTY").ToString
            'dr1("TAX_AMOUNT") = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
            'dr1("NET_AMOUNT") = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
            'dt.Rows.Add(dr1)
            'SALGRD = dt
            'SALGRD.Merge(dt2)
        Else
            'dt = SALGRD
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each gvrow As DataRow In ds.Tables(0).Rows
                    Dim dr As DataRow
                    dr = dt.NewRow()

                    dr("ID") = gvrow("ID").ToString
                    dr("BSH_ID") = gvrow("BSH_ID").ToString
                    dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                    dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                    dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                    dr("ISBN") = gvrow("ISBN").ToString
                    dr("PRICE") = gvrow("PRICE").ToString
                    dr("QTY") = gvrow("QTY").ToString
                    dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                    dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                    dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                    dr("STU_NO") = lbSNo.Text

                    dt.Rows.Add(dr)

                Next
            End If
            SALGRD = dt

        End If


        GridSelBook.Height = 240
        GridSelBook.DataSource = dt
        GridSelBook.DataBind()
        CalculateTotal()

        Dim id_as_string As String = GridSelBook.ClientID
        Dim last_char As String = id_as_string(id_as_string.Length - 1)
        If last_char = "0" Then
            ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)
        ElseIf last_char = "1" Then
            ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

        End If
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FREEZE", "FreezeGrid('" & GridSelBook.ClientID & "');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

    End Sub
    Protected Sub grdSAL_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) 'Handles grdSAL.PageIndexChanging
        '    grdSAL.PageIndex = e.NewPageIndex
        Dim gvOrders As GridView = TryCast(sender, GridView)
        gvOrders.PageIndex = e.NewPageIndex
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        'rblist = DirectCast(obj.FindControl("rdbBookSets"), RadioButtonList)
        rblist = DirectCast(obj.FindControl("rdbBookSets"), CheckBoxList)
        txtTotal = DirectCast(obj.FindControl("txtGrandTotal"), Label)
        'Dim lbSNo As New Label
        'lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)
        Dim dt As DataTable = GetTable()
        If Not SALGRD Is Nothing Then
            Try
                Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                dt = query.CopyToDataTable()

            Catch ex As Exception
            End Try
            'dt = SALGRD
        End If
        GridSelBook.Visible = True
        GridSelBook.ShowFooter = False
        GridSelBook.DataSource = dt
        GridSelBook.DataBind()
        CalculateTotal()


    End Sub

    Public Sub New()

    End Sub
End Class
