<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BS_OnlineReconciliation.aspx.vb" Inherits="BS_OnlineReconciliation" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("ChkSelAll").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Books and Stationery Online payment reconciliation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" style="width: 100%;" border="0">
                    <tr>
                        <td>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <input id="h_NextLine" runat="server" type="hidden" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" style="width: 100%;">
                                <tr>
                                    <td style="width: 20%;"><span class="field-label">File Name</span></td>
                                    <td style="width: 30%;">
                                        <asp:FileUpload ID="uploadFile" runat="server"></asp:FileUpload></td>
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload File" CausesValidation="False"
                                            TabIndex="30" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" style="width: 100%;">
                                <tr>
                                    <td align="left" class="title-bg-lite">Reconciliation Details</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvExcel" runat="server" Width="100%" EmptyDataText="No Accounts are Listing"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" ShowFooter="True" OnRowDataBound="gvExcel_RowDataBound">
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <EmptyDataRowStyle Wrap="True"></EmptyDataRowStyle>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <input id="ChkSelect" type="checkbox" runat="server" value='<%# Bind("FCO_ID") %>' />
                                                        <asp:HiddenField ID="hdnAmount" runat="server" Value='<%# Bind("FCO_AMOUNT")%>' />
                                                        <asp:HiddenField ID="hdnFCO_FCO_ID" runat="server" Value='<%# Bind("FCO_FCO_ID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="FCO_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date" SortExpression="FCO_DATE"></asp:BoundField>
                                                <asp:BoundField DataField="STU_NO" HeaderText="StudentID"></asp:BoundField>
                                                <asp:BoundField DataField="NAME" HeaderText="Student Name"></asp:BoundField>
                                                <asp:BoundField DataField="STU_GRD_ID" HeaderText="Grade"></asp:BoundField>
                                                <asp:BoundField DataField="FCO_AMOUNT" HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STS_FFIRSTNAME" HeaderText="Father Name"></asp:BoundField>
                                                <asp:BoundField DataField="STS_FMOBILE" HeaderText="Father Mob.No"></asp:BoundField>
                                                <asp:BoundField DataField="STS_MFIRSTNAME" HeaderText="Mother Name"></asp:BoundField>
                                                <asp:BoundField DataField="STS_MMOBILE" HeaderText="Mother Mob.No"></asp:BoundField>
                                                <asp:BoundField DataField="FCO_ID" HeaderText="Ref ID"></asp:BoundField>
                                            </Columns>
                                            <FooterStyle Height="20px" CssClass="griditem_alternative" />
                                            <SelectedRowStyle BackColor="Aqua"></SelectedRowStyle>
                                            <HeaderStyle CssClass="gridheader_new" Height="20px"></HeaderStyle>
                                            <AlternatingRowStyle CssClass="gridheader_new"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button CssClass="button" ID="btnSave" runat="server" TabIndex="26" Text="Save" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="HidUpload" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

