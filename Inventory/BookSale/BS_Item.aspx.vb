﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Telerik.Web.UI
Imports System.Collections.Generic

Partial Class Inventory_BookSale_BS_Item
    Inherits System.Web.UI.Page

    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                LOAD_VAT_CODES()
                LOAD_ITEM_TYPE()
                PopulateGrades()
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI01165") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar2.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlPrintedFor.SelectedIndexChanged

        Dim grades As String = ddlPrintedFor.SelectedItem.Text
        Dim Reportcard As String = "'" + GetSelectedGrades().Replace("|", "','") + "'"

    End Sub
    Function GetSelectedGrades() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlPrintedFor.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                If item.Text = "ALL" Then
                    str += item.Value
                Else
                    str += item.Text
                End If



                If str <> "" Then
                    str += "|"
                End If
            Next
        End If


        Return str

    End Function
    Private Sub PopulateGrades()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " SELECT DISTINCT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM VW_GRADE_M ORDER BY GRD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "GRD_DISPLAY"
        ddlPrintedFor.DataValueField = "GRD_ID"
        ddlPrintedFor.DataBind()
        'ddlPrintedFor.Items.Insert(0, (New RadComboBoxItem("ALL", "00")))
        ddlPrintedFor.ClearSelection()
        'ddlPrintedFor.Items.FindItemByText("ALL").Selected = True
        ddlPrintedFor.Enabled = True
    End Sub
    Private Sub CALCULATE_TAX(ByVal Amount As Double, ByRef lblTaxAmt As Label, ByRef lblNETAmt As Label)
        lblTaxAmt.Text = "0.000"
        lblNETAmt.Text = "0.00"
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & Amount & ",'" & ddlVATCode.SelectedValue & "')")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTaxAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.000")
                lblNETAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub
    Protected Sub ddlItemType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlItemType.SelectedIndexChanged

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [BIT_ID],[BIT_DESC],ISNULL(BIT_VAT_CODE,'VAT0') AS BIT_VAT_CODE FROM dbo.[BOOK_ITEM_TYPE_M] WHERE ISNULL(BIT_bDELETE,0)=0 AND BIT_ID=" & ddlItemType.SelectedValue & " ORDER BY BIT_ID")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            If ddlItemType.SelectedValue = "1" Then
                ddlVATCode.SelectedValue = ds.Tables(0).Rows(0)("BIT_VAT_CODE").ToString '"VAT5"
            Else
                ddlVATCode.SelectedValue = ds.Tables(0).Rows(0)("BIT_VAT_CODE").ToString '"VAT0"
            End If
        End If



        Dim dblPrice As Double = 0, lblTaxAmt As New Label, lblNetAmt As New Label
        If IsNumeric(CDbl(txtItemSPrice.Text)) Then
            dblPrice = CDbl(txtItemSPrice.Text)
            CALCULATE_TAX(dblPrice, lblTaxAmt, lblNetAmt)
            txtTaxAmount.Text = Format(CDbl(lblTaxAmt.Text), "#,##0.000")
            txtNetPrice.Text = Format(CDbl(lblNetAmt.Text), "#,##0.00")
        End If

    End Sub
    Protected Sub txtItemSPrice_TextChanged(sender As Object, e As EventArgs) Handles txtItemSPrice.TextChanged

        Dim dblPrice As Double = 0, lblTaxAmt As New Label, lblNetAmt As New Label
        If IsNumeric(CDbl(txtItemSPrice.Text)) Then
            dblPrice = CDbl(txtItemSPrice.Text)
            CALCULATE_TAX(dblPrice, lblTaxAmt, lblNetAmt)
            txtTaxAmount.Text = Format(CDbl(lblTaxAmt.Text), "#,##0.000")
            txtNetPrice.Text = Format(CDbl(lblNetAmt.Text), "#,##0.00")
        End If

    End Sub
    Protected Sub ddlVATCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVATCode.SelectedIndexChanged

        Dim dblPrice As Double = 0, lblTaxAmt As New Label, lblNetAmt As New Label
        If IsNumeric(CDbl(txtItemSPrice.Text)) Then
            dblPrice = CDbl(txtItemSPrice.Text)
            CALCULATE_TAX(dblPrice, lblTaxAmt, lblNetAmt)
            txtTaxAmount.Text = Format(CDbl(lblTaxAmt.Text), "#,##0.000")
            txtNetPrice.Text = Format(CDbl(lblNetAmt.Text), "#,##0.00")
        End If

    End Sub
    Sub LOAD_VAT_CODES()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT TAX_ID,TAX_CODE,TAX_DESCR FROM TAX.VW_TAX_CODES WHERE ISNULL(TAX_bDeleted,0)=0 ORDER BY TAX_ID")
        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAX_DESCR"
        ddlVATCode.DataValueField = "TAX_CODE"
        ddlVATCode.DataBind()
        Dim QRY As String = "SELECT ISNULL(TAX_CODE,'')TAX_CODE FROM OASIS.TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuid") & "','FEE','70',GETDATE(),0,'')"
        Dim TAX_CODE As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, QRY)
        If ddlVATCode.Items.Count > 0 AndAlso Not ddlVATCode.Items.FindByValue(TAX_CODE) Is Nothing Then
            ddlVATCode.SelectedValue = TAX_CODE
        End If
    End Sub

    Sub LOAD_ITEM_TYPE()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_BOOK_ITEM_TYPE]", pParms)
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT '-1' AS [BIT_ID], '--- Please Select ---' AS [BIT_DESC] FROM dbo.[BOOK_ITEM_TYPE_M] UNION SELECT [BIT_ID],[BIT_DESC] FROM dbo.[BOOK_ITEM_TYPE_M] WHERE ISNULL(BIT_bDELETE,0)=0 ORDER BY BIT_ID")
        ddlItemType.DataSource = ds
        ddlItemType.DataTextField = "BIT_DESC"
        ddlItemType.DataValueField = "BIT_ID"
        ddlItemType.DataBind()
        ddlItemType.SelectedValue = "-1"
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = Session("sBSUID")
            pParms(2) = New SqlClient.SqlParameter("@BIM_ID", SqlDbType.Int)
            pParms(2).Value = p_Modifyid

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_ITEM_M]", pParms)
            'Dim dt As New DataTable
            'dt = MainObj.getRecords("select ITM_ID, isnull(ITB_DESCR,ITM_DESCR) ITM_DESCR,ITM_ISBN,ITM_CATEGORY,ITM_PUBLICATION from ITEM_SALE left outer join ITEM_SALEBSU on ITM_ID=ITB_ITM_ID and ITB_BSU_ID='" & Session("sBSUID") & "' where ITM_ID=" & p_Modifyid, "OASIS_PUR_INVConnectionString")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                h_ITM_ID.Value = ds.Tables(0).Rows(0)("BIM_ID").ToString
                txtBookName.Text = ds.Tables(0).Rows(0)("BIM_BOOK_NAME").ToString
                txtItemDescr.Text = ds.Tables(0).Rows(0)("BIM_DESCR").ToString
                txtitemISBN.Text = ds.Tables(0).Rows(0)("BIM_ISBN").ToString
                txtitemCategory.Text = ds.Tables(0).Rows(0)("BIM_CATEGORY").ToString
                txtItemPublication.Text = ds.Tables(0).Rows(0)("BIM_PUBLICATION").ToString
                chkbShowOnline.Checked = ds.Tables(0).Rows(0)("BIM_bSHOWONLINE").ToString

                ddlItemType.SelectedValue = ds.Tables(0).Rows(0)("BIM_ITEM_TYPE").ToString
                ddlVATCode.SelectedValue = ds.Tables(0).Rows(0)("BIM_TAX_CODE").ToString

                txtItemCPrice.Text = ds.Tables(0).Rows(0)("BIM_COST_PRICE").ToString
                txtItemSPrice.Text = ds.Tables(0).Rows(0)("BIM_SELL_PRICE").ToString
                txtTaxAmount.Text = ds.Tables(0).Rows(0)("BIM_TAX_AMOUNT").ToString
                txtNetPrice.Text = ds.Tables(0).Rows(0)("BIM_NET_AMOUNT").ToString

                Dim rdSelectedItems() As String = ds.Tables(0).Rows(0)("BIM_GRD_ID").ToString.Split("|")
                If (rdSelectedItems.Length >= 1) Then
                    For Each item As String In rdSelectedItems

                        If item <> "" Then
                            'Dim index As Integer = ddlPrintedFor.FindItemIndexByValue(item)
                            'ddlPrintedFor.SelectedIndex = index
                            'ddlPrintedFor.SelectedValue = item

                            Dim item2 As RadComboBoxItem = ddlPrintedFor.FindItemByText(item)


                            item2.Checked = True

                            'added by Mahesh 23-Jul-2019
                            Dim COUNTH As Integer = 0
                            COUNTH = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(bsh_id) from BOOK_SET_D  inner join BOOK_SET_H on BSH_ID=BSD_BSH_ID where bsd_bim_id=" & h_ITM_ID.Value & " and isnull(BSH_DELETED,0)=0 and BSH_GRD_ID='" & item & "' and BSH_BSU_ID='" & Session("sBSUID") & "'")

                            If COUNTH > 0 Then
                                item2.Enabled = False
                            Else
                                item2.Enabled = True
                            End If
                            'added by Mahesh 23-Jul-2019

                        End If
                    Next
                End If




            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"

        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        txtItemDescr.Enabled = Not mDisable
        txtitemCategory.Enabled = Not mDisable
        txtitemISBN.Enabled = Not mDisable
        txtItemPublication.Enabled = Not mDisable

        chkbShowOnline.Enabled = Not mDisable
        txtBookName.Enabled = Not mDisable
        ddlItemType.Enabled = Not mDisable
        ddlVATCode.Enabled = mDisable

        txtItemCPrice.Enabled = Not mDisable
        txtItemSPrice.Enabled = Not mDisable
        txtTaxAmount.Enabled = mDisable
        txtNetPrice.Enabled = mDisable
        ddlPrintedFor.Enabled = Not mDisable
        If mode = "view" Then
            txtTaxAmount.Enabled = False
            txtNetPrice.Enabled = False
            ddlVATCode.Enabled = False
        End If

        If mode = "editlimited" Then
            mDisable = False
            ViewState("datamode") = "edit"

            txtItemDescr.Enabled = mDisable
            txtitemCategory.Enabled = mDisable
            txtitemISBN.Enabled = mDisable
            txtItemPublication.Enabled = mDisable

            chkbShowOnline.Enabled = mDisable
            txtBookName.Enabled = mDisable
            ddlItemType.Enabled = mDisable
            ddlVATCode.Enabled = mDisable

            txtItemCPrice.Enabled = Not mDisable
            txtItemSPrice.Enabled = Not mDisable
            txtTaxAmount.Enabled = mDisable
            txtNetPrice.Enabled = mDisable
            ddlPrintedFor.Enabled = mDisable
        End If
    End Sub

    Sub clear_All()
        h_ITM_ID.Value = 0
        txtBookName.Text = ""
        txtItemDescr.Text = ""
        txtitemCategory.Text = ""
        txtitemISBN.Text = ""
        txtItemPublication.Text = ""
        txtItemCPrice.Text = "0.00"
        txtItemSPrice.Text = "0.00"
        txtTaxAmount.Text = "0.00"
        txtNetPrice.Text = "0.00"
        chkbShowOnline.Checked = False
        ddlVATCode.SelectedValue = "VAT0"
        ddlItemType.SelectedValue = "-1"
        ddlPrintedFor.ClearCheckedItems()
        ViewState("EntryId") = 0
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'Dim COUNTO As Integer = 0
            'Dim COUNTH As Integer = 0
            'COUNTO = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from [dbo].[BOOK_SALE_ONLINE_D]  where [BSADO_ITM_ID]=" & h_ITM_ID.Value)
            'COUNTH = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from [dbo].[BOOK_SALE_D]  where [BSAD_ITM_ID]=" & h_ITM_ID.Value)

            Dim ds50 As New DataSet
            Dim IS_ITEM_EXISTS As Boolean = 0
            ds50 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_ITEM_SALE_EXISTS] ('" & Session("sBSUID") & "'," & h_ITM_ID.Value & ") AS IS_ITEM_EXISTS")
            IS_ITEM_EXISTS = CInt((ds50.Tables(0).Rows(0)("IS_ITEM_EXISTS")))

            If IS_ITEM_EXISTS = 0 Then

                SetDataMode("edit")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                SetDataMode("editlimited")
                'lblError.Text = "unable to delete, " & txtItemDescr.Text & " used in transactions"
                usrMessageBar2.ShowNotification("edit is limited item -  " & txtItemDescr.Text & " is used in transactions", UserControls_usrMessageBar.WarningType.Danger)
                'SetDataMode("edit")
                ddlPrintedFor.Enabled = True


            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'Dim COUNTO As Integer = 0
            'Dim COUNTH As Integer = 0
            'COUNTO = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from [dbo].[BOOK_SALE_ONLINE_D]  where [BSADO_ITM_ID]=" & h_ITM_ID.Value)
            'COUNTH = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from [dbo].[BOOK_SALE_D]  where [BSAD_ITM_ID]=" & h_ITM_ID.Value)
            Dim ds50 As New DataSet
            Dim IS_ITEM_EXISTS As Boolean = 0
            ds50 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_ITEM_SALE_EXISTS] ('" & Session("sBSUID") & "'," & h_ITM_ID.Value & ") AS IS_ITEM_EXISTS")
            IS_ITEM_EXISTS = CInt((ds50.Tables(0).Rows(0)("IS_ITEM_EXISTS")))
            If IS_ITEM_EXISTS = 0 Then
                'SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from [BOOK_ITEM_M] where BIM_ID=" & h_ITM_ID.Value)
                Dim query As String = "update [BOOK_ITEM_M] set [BIM_bDELETE]=1, [BIM_DELETED_DATE]='" + CDate(DateTime.Now) + "', [BIM_DELETED_USER]='" + Session("sUsr_id") + "' where BIM_ID=" & h_ITM_ID.Value
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update [BOOK_ITEM_M] set [BIM_bDELETE]=1, [BIM_DELETED_DATE]='" + CDate(DateTime.Now) + "', [BIM_DELETED_USER]='" + Session("sUsr_id") + "' where BIM_ID=" & h_ITM_ID.Value)
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                'lblError.Text = "unable to delete, " & txtItemDescr.Text & " used in transactions"
                usrMessageBar2.ShowNotification("unable to delete, " & txtItemDescr.Text & " used in transactions", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'lblError.Text = ""
            If txtItemDescr.Text.Trim = "" Then
                'lblError.Text = "Item description cannot be blank"
                usrMessageBar2.ShowNotification("Item description cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            If ddlItemType.SelectedValue = "-1" Then
                'lblError.Text = "Item description cannot be blank"
                usrMessageBar2.ShowNotification("Please select Item Type", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            If txtBookName.Text.Trim = "" Then
                'lblError.Text = "Item description cannot be blank"
                usrMessageBar2.ShowNotification("Book Name cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            'ISBN is not mandatory - 27MAY2019 - Majo
            'If txtitemISBN.Text.Trim = "" Then
            '    'lblError.Text = "ISBN cannot be blank"
            '    usrMessageBar2.ShowNotification("ISBN cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
            '    Exit Sub
            '    'Else
            '    '    If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from [BOOK_ITEM_M]  where [BIM_ISBN]='" & txtitemISBN.Text.Trim & "'") <> 0 Then
            '    '        usrMessageBar2.ShowNotification("ISBN exists for another product ", UserControls_usrMessageBar.WarningType.Danger)
            '    '        Exit Sub
            '    '    End If
            'End If

            If GetSelectedGrades() = "" Then
                usrMessageBar2.ShowNotification("Select atleast one grade", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            If CDbl(txtItemSPrice.Text) = 0.0 Then
                usrMessageBar2.ShowNotification("Selling Price is Zero", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            If CDbl(txtItemCPrice.Text) = 0.0 Then
                usrMessageBar2.ShowNotification("Cost Price is Zero", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If


            Dim pParms(17) As SqlParameter
            pParms(0) = New SqlParameter("@BIM_ID", SqlDbType.Int)
            'pParms(0).Direction = ParameterDirection.Output
            pParms(0).Value = h_ITM_ID.Value
            pParms(1) = New SqlParameter("@BIM_DESCR", SqlDbType.VarChar, 250)
            pParms(1).Value = txtItemDescr.Text
            pParms(2) = New SqlParameter("@BIM_GRD_ID", SqlDbType.VarChar, 250)
            pParms(2).Value = GetSelectedGrades()
            pParms(3) = New SqlParameter("@BIM_ISBN", SqlDbType.VarChar, 20)
            pParms(3).Value = txtitemISBN.Text
            pParms(4) = New SqlParameter("@BIM_CATEGORY", SqlDbType.VarChar, 50)
            pParms(4).Value = txtitemCategory.Text
            pParms(5) = New SqlParameter("@BIM_PUBLICATION", SqlDbType.VarChar, 50)
            pParms(5).Value = txtItemPublication.Text

            pParms(6) = New SqlParameter("@BIM_ITEM_TYPE", SqlDbType.Int)
            pParms(6).Value = ddlItemType.SelectedValue
            pParms(7) = New SqlParameter("@BIM_BSU_ID", SqlDbType.VarChar, 20)
            pParms(7).Value = Session("sBsuId")
            pParms(8) = New SqlParameter("@BIM_COST_PRICE", SqlDbType.VarChar, 20)
            pParms(8).Value = txtItemCPrice.Text
            pParms(9) = New SqlParameter("@BIM_SELL_PRICE", SqlDbType.VarChar, 20)
            pParms(9).Value = txtItemSPrice.Text
            pParms(10) = New SqlParameter("@BIM_TAX_CODE", SqlDbType.VarChar, 20)
            pParms(10).Value = ddlVATCode.SelectedValue
            pParms(11) = New SqlParameter("@BIM_TAX_AMOUNT", SqlDbType.VarChar, 20)
            pParms(11).Value = txtTaxAmount.Text
            pParms(12) = New SqlParameter("@BIM_NET_AMOUNT", SqlDbType.VarChar, 20)
            pParms(12).Value = txtNetPrice.Text

            pParms(13) = New SqlParameter("@BIM_BOOK_NAME", SqlDbType.VarChar, 250)
            pParms(13).Value = txtBookName.Text
            pParms(14) = New SqlParameter("@BIM_bSHOWONLINE", SqlDbType.Bit)
            pParms(14).Value = chkbShowOnline.Checked

            pParms(15) = New SqlParameter("@USER_ID", SqlDbType.VarChar, 50)
            pParms(15).Value = Session("sUsr_id")
            pParms(16) = New SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(16).Direction = ParameterDirection.ReturnValue
            pParms(17) = New SqlParameter("@NEW_BIM_ID", SqlDbType.Int)
            pParms(17).Direction = ParameterDirection.Output

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction()
            Try
                Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.[SAVE_BOOK_ITEM_M]", pParms)
                If pParms(16).Value <> 0 Then
                    'lblError.Text = UtilityObj.getErrorMessage(pParms(6).Value)
                    usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(pParms(16).Value), UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(17).Value
                    'lblError.Text = "Data Saved Successfully !!!"
                    usrMessageBar2.ShowNotification("Data Saved Successfully and forwarded for Approval", UserControls_usrMessageBar.WarningType.Success)
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

End Class
