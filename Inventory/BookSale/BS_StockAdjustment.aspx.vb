﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports Lesnikowski.Barcode
Partial Class Inventory_BookSale_BS_StockAdjustment
    Inherits System.Web.UI.Page
    Private Property SALGRD() As DataTable
        Get
            Return ViewState("SALGRD")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SALGRD") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("sBsuid")
        If Session("sUsr_name") Is Nothing Then
            Response.Redirect("~\Login.aspx")
        End If
        If Page.IsPostBack = False Then

            Page.Title = "::GEMS Education |Book Sale::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = "add"
            Dim USR_NAME As String = Session("sUsr_name")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            'Dim CurUsr_id As String = Session("STU_ID")
            Dim CurBsUnit As String = Session("sBsuid")
            'Dim USR_NAME As String = Session("STU_NAME")
            'InitialiseCompnents()

            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else




                'lblschool.Text = ViewState("BSU_NAME")




            End If
            'Dim obj As Object = sender.parent
            'Dim GridSelBook As Label = DirectCast(obj.FindControl("txtItmSelect"), Label)
            'GridSelBook.Visible = True



            If SALGRD Is Nothing Then
                GridScroll.Visible = False

            End If

            txtReceivedTotal.Enabled = False
            txtBalance.Enabled = False
            Dim qry As String = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM CREDITCARD_S WITH(NOLOCK) INNER JOIN " & _
                  " CREDITCARD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
                  " CREDITCARD_PROVD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
                  " WHERE (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
            Me.hfCobrand.Value = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)
            trCreditCard.Visible = False
            ddlType_OnSelectedIndexChanged(Nothing, Nothing)
        End If


        If Not SALGRD Is Nothing Then
            If SALGRD.Rows.Count >= 1 Then
                rad1.Enabled = False
                rad2.Enabled = False
            End If
        End If

        'id_show_add_items
        Dim id_show_add_items As Boolean = 0
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 1
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms0(1).Value = Session("sBSUID")
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_STOCK_ADJUSTMENT_TYPES]", pParms0)
        id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        If id_show_add_items = True Then
            rad1.Visible = True
            rad2.Visible = True
        Else

            rad1.Visible = False
            rad2.Visible = False
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Response.Redirect(ViewState("ReferrerUrl"))

    End Sub
    Sub ClearDetails()

        txtRemarks.Text = ""
        txtCashTotal.Text = "0.0"
        txtDue.Text = "0.0"
        txtCCTotal.Text = "0.0"
        txtReceivedTotal.Text = "0.0"
        txtBalance.Text = "0.0"
        txtCreditno.Text = ""
        GridScroll.Visible = False
        pay_section.Visible = False
        txtItmSelect.Text = ""

        txtSubTotal.Text = "0.0"
        txtSubVATTotal.Text = "0.0"
        txtGrandTotal.Text = "0.0"
        lblTotal.Text = "0.0"

        hf_Item_ID.Value = "0"
        hf_Type_ID.Value = "0"

        ddCreditcard.SelectedIndex = 0
        Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S", ConnectionManger.GetOASISFINConnectionString)
        If Not ddCreditcard.Items.FindByValue(str_CRR_ID) Is Nothing Then
            ddCreditcard.ClearSelection()
            ddCreditcard.Items.FindByValue(str_CRR_ID).Selected = True
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        ClearDetails()
        'SetDataMode("add")
        'setModifyvalues(0)
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'uscStudentPicker.STU_ID = 0
        'uscStudentPicker.ClearDetails()
        'Gridbind_StuDetails()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim vpc_Amount As Decimal = 0.0
        Dim StuNos As String = ""
        Dim Qty_Error As String = ""
        Dim Qty_Error_Check As Integer = 0
        Dim errors As String = ""
        If (FeeCollection.GetDoubleVal(txtCashTotal.Text) + FeeCollection.GetDoubleVal(txtCCTotal.Text) + FeeCollection.GetDoubleVal(txtCrCardCharges.Text) <> FeeCollection.GetDoubleVal(lblTotal.Text)) Then
            errors &= IIf(errors.Length = 0, "", ",") & "There is a mismatch in payable amount and paid amount"
        End If

        If ddlType.SelectedValue = "1" Then
            If txtDAXInvoiceNo.Text = "" Then
                errors &= IIf(errors.Length = 0, "", ", ") & "Please enter DAX Invoice Number"
            End If
            If txtSUPPLInvoiceNo.Text = "" Then
                errors &= IIf(errors.Length = 0, "", ", ") & "Please enter Supplier Invoice Number"
            End If
        End If
        If ddlType.SelectedValue = "2" Then
            If txtTypeElement.Text = "" Then
                errors &= IIf(errors.Length = 0, "", ", ") & "Please select Student"
            Else
                Dim str_conn1 As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Dim StrSQL As String = "select top 10 ID +'-'+Name As Name,ID from (select STU.STU_FIRSTNAME  + case when LTRIM(RTRIM(isnull(STU.STU_MIDNAME, ' '))) ='' then '' else ' '+isnull(STU.STU_MIDNAME, ' ') end + case when LTRIM(RTRIM(isnull(STU.STU_LASTNAME, ' '))) ='' then '' else ' '+isnull(STU.STU_LASTNAME, ' ') end [Name] ,STU_NO As ID from OASIS..STUDENT_M STU INNER JOIN OASIS..ACADEMICYEAR_D ON ACD_ID=STU_ACD_ID AND ACD_BSU_ID=STU_BSU_ID AND ISNULL(ACD_CURRENT,0)=1 WHERE [STU_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [STU_CURRSTATUS]='EN' ) a WHERE 1=1 AND ID='" & hf_TypeElement_ID.Value.Trim & "'"
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, StrSQL)
                Dim intRows As Int32 = ds.Tables(0).Rows.Count
                If intRows = 0 Then
                    errors &= IIf(errors.Length = 0, "", ", ") & "Selected Student is wrong. Please select again.."
                End If
            End If
        End If
        If ddlType.SelectedValue = "3" Then
            If txtTypeElement.Text = "" Then
                errors &= IIf(errors.Length = 0, "", ", ") & "Please select Employee"
            Else
                Dim StrSQL As String = "select top 10 ID +'-'+Name As Name,ID from (SELECT   EMP.EMP_FNAME  + case when LTRIM(RTRIM(isnull(EMP.EMP_MNAME, ' '))) ='' then '' else ' '+isnull(EMP.EMP_MNAME, ' ') end + case when LTRIM(RTRIM(isnull(EMP.EMP_LNAME, ' '))) ='' then '' else ' '+isnull(EMP.EMP_LNAME, ' ') end [Name],EMPNO AS ID FROM OASIS..EMPLOYEE_M EMP WHERE ISNULL(EMP_bACTIVE,0)=1 AND EMP_STATUS IN (1,2,9) AND EMP_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "' ) a WHERE 1=1 AND ID='" & hf_TypeElement_ID.Value.Trim & "'"

                Dim str_conn1 As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, StrSQL)
                Dim intRows As Int32 = ds.Tables(0).Rows.Count
                If intRows = 0 Then
                    errors &= IIf(errors.Length = 0, "", ", ") & "Selected Employee is wrong. Please select again.."
                End If
            End If
        End If


        If errors.Length > 0 Then
            usrMessageBar2.ShowNotification(errors, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer = 0
        Try


            Dim STR_TYPE As Char = "S"
            Dim BSAHO_ID As Int64 = 0
            Dim BSAHO_BSAHO_ID As Int64 = 0


            Dim GridSelBook As New GridView

            GridSelBook = grdSAL ' DirectCast(Me.FindControl("grdSAL"), GridView)
            Dim hfSTU_ID As String = "0" 'CType(Me.FindControl("hfSTU_ID"), HiddenField)
            'Dim hfSTU_FEE_ID As HiddenField = CType(Me.FindControl("hfSTU_FEE_ID"), HiddenField)
            'Dim hfSTU_ACD_ID As HiddenField = CType(Me.FindControl("hfSTU_ACD_ID"), HiddenField)

            Dim lblStuNo As String = "00000000000000" 'CType(Me.FindControl("lbSNo"), Label)
            If ddlType.SelectedValue = "1" Then
                lblStuNo = "00000000000000"
            ElseIf ddlType.SelectedValue = "2" Then
                lblStuNo = hf_TypeElement_ID.Value.Trim
            ElseIf ddlType.SelectedValue = "3" Then
                lblStuNo = hf_TypeElement_ID.Value.Trim
            End If
            Dim lblStuName As String = "None" 'CType(Me.FindControl("lbSName"), Label)
            Dim lblGrade As String = "00" 'CType(Me.FindControl("lbGrade"), Label)
            Dim lblSection As String = "00" 'CType(Me.FindControl("lbSection"), Label)
            'Dim lblAmounttoPayF As Label = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label)
            Dim lblAmounttoPayF As Label = txtGrandTotal 'DirectCast(Me.FindControl("txtGrandTotal"), Label) 'gives total amount paying now of current student
            'Dim h_ProcessingChargeF As HiddenField = DirectCast(repitm.FindControl("h_ProcessingChargeF"), HiddenField) 'gives total processing charge of current student

            If Convert.ToDouble(lblAmounttoPayF.Text) <> 0 Then
                vpc_Amount = vpc_Amount + Convert.ToDouble(lblAmounttoPayF.Text)
                StuNos = "00000000000000"

                Dim ds5 As New DataSet
                Dim F_Year As String = ""
                ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1")
                F_Year = (ds5.Tables(0).Rows(0)("FYR_ID").ToString())

                Dim BSAH_NO As String = "NEW"

                'SAVING HEADER DATA
                retval = clsBookSalesOnline.SAVE_BOOK_SALE_H(BSAHO_ID, BSAHO_BSAHO_ID, BSAH_NO, "COUNTER", Session("sBsuid"), _
                Format(Date.Now, "dd/MMM/yyyy"), "A", Session("sUsr_name"), hfSTU_ID, lblStuNo, lblStuName, lblGrade, lblSection, _
                 F_Year, txtRemarks.Text, lblAmounttoPayF.Text, "", "", txtCrCardCharges.Text, txtCashTotal.Text, txtCCTotal.Text, _
                txtReceivedTotal.Text, txtBalance.Text, txtCreditno.Text, ddCreditcard.SelectedValue, _
                "", 0, lblAmounttoPayF.Text, stTrans, IIf(rad1.Checked = True, "STK_ADJ", "RET_ADJ"), ddlType.SelectedValue, txtDAXInvoiceNo.Text, txtSUPPLInvoiceNo.Text)
                If retval = 0 Then


                    If BSAHO_BSAHO_ID = 0 Then
                        BSAHO_BSAHO_ID = BSAHO_ID 'retval

                    End If
                    If BSAHO_ID = 0 Then
                        BSAHO_ID = BSAHO_ID 'retval
                    End If

                    'Adding Barcode starts here
                    Dim p_Receiptno As String = ""
                    p_Receiptno = BSAH_NO
                    Dim barcode As BaseBarcode
                    barcode = BarcodeFactory.GetBarcode(Symbology.Code128)

                    barcode.Number = p_Receiptno
                    barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                    barcode.ChecksumAdd = True
                    barcode.CustomText = p_Receiptno
                    barcode.NarrowBarWidth = 3
                    barcode.Height = 300
                    barcode.FontHeight = 0.3F
                    barcode.ForeColor = Drawing.Color.Black
                    Dim b As Byte()
                    ReDim b(barcode.Render(ImageType.Png).Length)
                    b = barcode.Render(ImageType.Png)
                    retval = clsBookSalesOnline.UPDATE_BARCODE(1, BSAHO_ID, Session("sBsuid"), b, stTrans)
                    'Adding barcode ends here

                    If retval = 0 Then

                        Dim dt As New DataTable '= SALGRD
                        If Not SALGRD Is Nothing Then
                            Try
                                Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lblStuNo.ToString Select order
                                dt = query.CopyToDataTable()
                            Catch ex As Exception
                            End Try
                        Else
                            'dt = SALGRD
                        End If
                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            'Dim dbl_net_amt As Double
                            For Each gvrow As DataRow In dt.Rows
                                If lblStuNo.ToString = gvrow("STU_NO").ToString Then
                                    If CDbl(gvrow("NET_AMOUNT")) <> 0 Then
                                        'SAVING CHILD DATA
                                        'Dim Qts As Integer = CDbl(gvrow("QTY"))


                                        'checking the quantity before saving line item
                                        Dim FNL_QTY As Integer = 0
                                        Dim dt0 As New DataTable
                                        Dim filter_str As String = ""
                                        Dim dtView As DataView = New DataView(SALGRD)
                                        filter_str = " ITEM_ID=" & gvrow("ITEM_ID") & ""
                                        dtView.RowFilter = filter_str
                                        dt0 = dtView.ToTable
                                        Dim ds50 As New DataSet
                                        Dim AVAIL_QTY As Integer = 0
                                        ds50 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID") & ") AS AVAIL_QTY")
                                        AVAIL_QTY = CInt((ds50.Tables(0).Rows(0)("AVAIL_QTY")))
                                        If dt0.Rows.Count > 0 Then
                                            For Each gvrow2 As DataRow In dt0.Rows
                                                FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                                            Next
                                        End If
                                        If (FNL_QTY + AVAIL_QTY) < 0 Then
                                            'usrMessageBar2.ShowNotification("Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                                            Qty_Error = "Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
                                            retval = -1
                                            Qty_Error_Check = 1
                                            Exit For
                                        End If


                                        retval = clsBookSalesOnline.SAVE_BOOK_SALE_D(0, BSAHO_ID, CInt(gvrow("BSH_ID")), CDbl(gvrow("ITEM_ID")), _
                                               gvrow("ITEM_DESCR"), (gvrow("BOOK_NAME")), CDbl(gvrow("QTY")), CDbl(gvrow("PRICE")),
                                                CDbl(0), CDbl(gvrow("NET_AMOUNT")), CStr(gvrow("TAX_CODE")), CDbl(gvrow("TAX_AMOUNT")), stTrans)
                                        If retval <> 0 Then
                                            'stTrans.Rollback()
                                            Exit For
                                        End If
                                    End If
                                End If

                            Next
                        End If
                    Else
                        'stTrans.Rollback()
                        'Exit For
                    End If
                Else
                    stTrans.Rollback()
                    usrMessageBar2.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            Else
                Qty_Error = "Cannot save with total value 0"
                Qty_Error_Check = 1 'total amount 0, not entered to save header and details, no need commit tran
            End If

            BSAHO_ID = 0


            'If retval = 0 Then
            If retval = 0 And Qty_Error_Check = 0 Then
                hf_TypeElement_ID.Value = ""
                txtTypeElement.Text = ""
                txtDAXInvoiceNo.Text = ""
                txtSUPPLInvoiceNo.Text = ""
                'ddlType.SelectedValue = "1"
                txtTypeElement.Enabled = True
                stTrans.Commit()
                SALGRD = Nothing
                h_print.Value = BSAHO_BSAHO_ID
                btnAddNew_Click(sender, e)
            Else
                stTrans.Rollback()
                'usrMessageBar2.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)

                If Qty_Error_Check = 1 Then
                    'Qty_Error = "Below Items has Zero Quantity </br>" & Qty_Error
                    usrMessageBar2.ShowNotification(Qty_Error, UserControls_usrMessageBar.WarningType.Danger)
                Else
                    usrMessageBar2.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If

                'lblError.Text = getErrorMessage(retval)
                'lblError.CssClass = "alert alert-warning"
            End If
        Catch ex As Exception
            stTrans.Rollback()
            usrMessageBar2.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Text = getErrorMessage(1000)
            'lblError.CssClass = "alert alert-warning"
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try
        'If retval = 0 Then
        '    Response.Redirect(ViewState("ReferrerUrl"))
        'End If
    End Sub
    'lbl_QTY_OnTextChanged
    Protected Sub lbl_QTY_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbl_PID"), Label)
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        Dim rowIndex = (CType((CType(sender, Control)).NamingContainer, GridViewRow)).RowIndex
        'GridSelBook.Rows(GridSelBook.SelectedIndex).Cells(1).Text = "7"
        'GridSelBook.Rows(0).Cells(3).Text = "7"
        Dim lbl1 As TextBox = DirectCast(obj.FindControl("lbl_QTY"), TextBox) '(Label)GridSelBook.FindControl("lblQuestion_Scripting");
        Dim lbl_ID As Label = DirectCast(obj.FindControl("lbl_ID"), Label)
        Dim lbl_BOOK_NAME As Label = DirectCast(obj.FindControl("lbl_BOOK_NAME"), Label)
        Dim lbl_ITMID As Label = DirectCast(obj.FindControl("lbl_ITMID"), Label)
        Dim Cur_Qty As Integer = 0
        Try
            Cur_Qty = CInt(lbl1.Text)
        Catch ex As Exception
            lbl1.Text = 0
            Cur_Qty = 0
        End Try
        Dim Cur_Net_Amt As Double = 0.0

        'id_show_add_items
        'Dim id_show_add_items As Boolean = 0
        'Dim pParms0(2) As SqlClient.SqlParameter
        'pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        'pParms0(0).Value = 1
        'pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        'pParms0(1).Value = Session("sBSUID")
        'Dim ds0 As New DataSet
        'ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MINUS_ITEMS]", pParms0)
        'id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        'If id_show_add_items = True Then



        Dim FNL_QTY As Integer = 0
        Dim ds5 As New DataSet
        Dim AVAIL_QTY As Integer = 0
        ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & lbl_ITMID.Text & ") AS AVAIL_QTY")
        AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))

        'in plus, check item multiple times in salgrd
        Dim dt00 As New DataTable
        Dim filter_str0 As String = ""
        Dim dtView0 As DataView = New DataView(SALGRD)
        filter_str0 = " ITEM_ID=" & lbl_ITMID.Text & " AND ID <>" & lbl_ID.Text
        dtView0.RowFilter = filter_str0
        dt00 = dtView0.ToTable

        If dt00.Rows.Count > 0 Then
            For Each gvrow2 As DataRow In dt00.Rows
                FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
            Next
        End If

        FNL_QTY = FNL_QTY + Cur_Qty
        If FNL_QTY + AVAIL_QTY < 0 Then

            usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
            'Exit Sub
        End If
        'If Cur_Qty >= 1 Then
        'Cur_Qty = Cur_Qty - 1
        'lbl1.Text = "" & Cur_Qty

        Dim lbl2 As Label = DirectCast(obj.FindControl("lbl_PRICE"), Label)
        Dim lbl3 As Label = DirectCast(obj.FindControl("lbl_TAX_AMOUNT"), Label)
        Dim lbl4 As Label = DirectCast(obj.FindControl("lbl_NET_AMOUNT"), Label)
        Dim Cur_Price As Double = CDbl(lbl2.Text)
        Dim Cur_Tax_Amt As Double = 0 'CDbl(lbl3.Text)' no TAX in Stock Adjustments
        If rad1.Checked = True Then
            Cur_Tax_Amt = 0
        Else
            Cur_Tax_Amt = CDbl(lbl3.Text)
        End If
        Cur_Net_Amt = CDbl(lbl4.Text)
        Cur_Net_Amt = (Cur_Price + Cur_Tax_Amt) * Cur_Qty
        lbl4.Text = "" & Cur_Net_Amt

        ''TAX AMOUNT SHOWS AS ZERO FOR ZERO QTY
        'If Cur_Qty = 0 Then
        '    lbl3.Text = "0.00"
        'End If

        'End If

        For Each dr As DataRow In SALGRD.Rows

            If dr("ID") = lbl_ID.Text.ToString And lbSNo.Text.ToString = dr("STU_NO") Then
                dr("QTY") = "" & Cur_Qty
                dr("NET_AMOUNT") = "" & Cur_Net_Amt
            End If
        Next

        CalculateTotal()


        'End If
    End Sub
    Protected Sub btnMinus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbl_PID"), Label)
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        Dim rowIndex = (CType((CType(sender, Control)).NamingContainer, GridViewRow)).RowIndex
        'GridSelBook.Rows(GridSelBook.SelectedIndex).Cells(1).Text = "7"
        'GridSelBook.Rows(0).Cells(3).Text = "7"
        Dim lbl1 As Label = DirectCast(obj.FindControl("lbl_QTY"), Label) '(Label)GridSelBook.FindControl("lblQuestion_Scripting");
        Dim lbl_ID As Label = DirectCast(obj.FindControl("lbl_ID"), Label)
        Dim Cur_Qty As Integer = CInt(lbl1.Text)
        Dim Cur_Net_Amt As Double = 0.0

        'id_show_add_items
        Dim id_show_add_items As Boolean = 0
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 1
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms0(1).Value = Session("sBSUID")
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MINUS_ITEMS]", pParms0)
        id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        If id_show_add_items = True Then


            If Cur_Qty >= 1 Then
                Cur_Qty = Cur_Qty - 1
                lbl1.Text = "" & Cur_Qty

                Dim lbl2 As Label = DirectCast(obj.FindControl("lbl_PRICE"), Label)
                Dim lbl3 As Label = DirectCast(obj.FindControl("lbl_TAX_AMOUNT"), Label)
                Dim lbl4 As Label = DirectCast(obj.FindControl("lbl_NET_AMOUNT"), Label)
                Dim Cur_Price As Double = CDbl(lbl2.Text)
                Dim Cur_Tax_Amt As Double = CDbl(lbl3.Text)
                Cur_Net_Amt = CDbl(lbl4.Text)
                Cur_Net_Amt = (Cur_Price + Cur_Tax_Amt) * Cur_Qty
                lbl4.Text = "" & Cur_Net_Amt

                ''TAX AMOUNT SHOWS AS ZERO FOR ZERO QTY
                'If Cur_Qty = 0 Then
                '    lbl3.Text = "0.00"
                'End If

            End If

            For Each dr As DataRow In SALGRD.Rows

                If dr("ID") = lbl_ID.Text.ToString And lbSNo.Text.ToString = dr("STU_NO") Then
                    dr("QTY") = "" & Cur_Qty
                    dr("NET_AMOUNT") = "" & Cur_Net_Amt
                End If
            Next

            CalculateTotal()
        End If
    End Sub
    Protected Sub btnPlus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbl_PID"), Label)
        'GridSelBook = DirectCast(Me.FindControl("grdSAL"), GridView)
        'Dim rowIndex = (CType((CType(sender, Control)).NamingContainer, GridViewRow)).RowIndex
        Dim lbl1 As Label = DirectCast(obj.FindControl("lbl_QTY"), Label) '(Label)GridSelBook.FindControl("lblQuestion_Scripting");
        Dim lbl_ID As Label = DirectCast(obj.FindControl("lbl_ID"), Label)
        Dim Cur_Qty As Integer = CInt(lbl1.Text)
        Dim Cur_Net_Amt As Double = 0.0

        'id_show_add_items
        Dim id_show_add_items As Boolean = 0
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 1
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms0(1).Value = Session("sBSUID")
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MINUS_ITEMS]", pParms0)
        id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        If id_show_add_items = True Then

            If Cur_Qty >= 0 And Cur_Qty < 99 Then
                Cur_Qty = Cur_Qty + 1
                lbl1.Text = "" & Cur_Qty

                Dim lbl2 As Label = DirectCast(obj.FindControl("lbl_PRICE"), Label)
                Dim lbl3 As Label = DirectCast(obj.FindControl("lbl_TAX_AMOUNT"), Label)
                Dim lbl4 As Label = DirectCast(obj.FindControl("lbl_NET_AMOUNT"), Label)
                Dim Cur_Price As Double = CDbl(lbl2.Text)
                Dim Cur_Tax_Amt As Double = CDbl(lbl3.Text)
                Cur_Net_Amt = CDbl(lbl4.Text)
                Cur_Net_Amt = (Cur_Price + Cur_Tax_Amt) * Cur_Qty
                lbl4.Text = "" & Cur_Net_Amt
            End If

            For Each dr As DataRow In SALGRD.Rows

                If dr("ID") = lbl_ID.Text.ToString And lbSNo.Text.ToString = dr("STU_NO") Then

                    dr("QTY") = "" & Cur_Qty
                    dr("NET_AMOUNT") = "" & Cur_Net_Amt
                End If
            Next

            CalculateTotal()
        End If
    End Sub
    Private Sub CalculateTotal()
        Dim Cur_Net_Amt As Double
        Dim courier_amount As Decimal
        Dim Cur_VAT_Amt As Double
        Dim Cur_SubTotal_Amt As Double
        Dim Net_Pay_Amt As Double
        'For Each repitm As RepeaterItem In repInfo.Items
        Dim GridSelBook As New GridView
        'Dim GridScroll As New Panel
        Dim lbSNo As String
        'Dim DivHeaderRow As New HtmlGenericControl
        'Dim DivMainContent As New HtmlGenericControl
        'Dim DivFooterRow As New HtmlGenericControl
        'Dim btnMinus1 As New ImageButton
        'Dim btnPlus1 As New ImageButton

        lbSNo = "00000000000000" ' DirectCast(Me.FindControl("lbSNo"), Label)
        If ddlType.SelectedValue = "1" Then
            lbSNo = "00000000000000"
        ElseIf ddlType.SelectedValue = "2" Then
            lbSNo = hf_TypeElement_ID.Value.Trim()
        ElseIf ddlType.SelectedValue = "3" Then
            lbSNo = hf_TypeElement_ID.Value.Trim()
        End If
        GridSelBook = grdSAL ' Content1.FindControl("grdSAL")
        'GridScroll = GridScroll ' CType(Me.FindControl("GridScroll"), Panel)
        Dim lbl0 As Label = txtGrandTotal 'CType(Me.FindControl("txtGrandTotal"), Label)
        Dim lblSubTotal As Label = txtSubTotal 'DirectCast(Me.FindControl("txtSubTotal"), Label)
        Dim lblSubVATTotal As Label = txtSubVATTotal 'DirectCast(Me.FindControl("txtSubVATTotal"), Label)
        'DivHeaderRow = DivHeaderRow 'DirectCast(Me.FindControl("DivHeaderRow"), HtmlGenericControl)
        'DivMainContent = DivMainContent 'DirectCast(Me.FindControl("DivMainContent"), HtmlGenericControl)
        'DivFooterRow = DivFooterRow 'DirectCast(Me.FindControl("DivFooterRow"), HtmlGenericControl)
        'btnMinus1 = DirectCast(repitm.FindControl("btnMinus"), ImageButton)
        'btnPlus1 = DirectCast(repitm.FindControl("btnPlus"), ImageButton)


        'Dim lbl10 As Label = DirectCast(repitm.FindControl("lblTotal"), Label)
        'For Each gvRow As GridViewRow In GridSelBook.Rows

        '    Dim lbl4 As Label = (CType(gvRow.FindControl("lbl_NET_AMOUNT"), Label))
        '    Cur_Net_Amt = Cur_Net_Amt + CDbl(lbl4.Text)

        'Next
        Dim dt As New DataTable '= SALGRD
        If Not SALGRD Is Nothing Then
            Try
                Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.ToString Select order
                dt = query.CopyToDataTable()
            Catch ex As Exception
            End Try
        Else
            'dt = SALGRD
        End If


        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            'Dim dbl_net_amt As Double

            'GridSelBook.Visible = True
            If dt.Rows.Count <= 4 Then

                DivMainContent.Attributes("class") = "main_content_grid0"
                GridScroll.CssClass = "grid_scroll0"
            Else
                DivMainContent.Attributes("class") = "main_content_grid"
                GridScroll.CssClass = "grid_scroll"

            End If
            GridScroll.Visible = True

            For Each gvrow As DataRow In dt.Rows
                If lbSNo = gvrow("STU_NO").ToString Then
                    Cur_Net_Amt = Cur_Net_Amt + CDbl(gvrow("NET_AMOUNT"))
                    Cur_SubTotal_Amt = Cur_SubTotal_Amt + (CDbl(gvrow("PRICE")) * CDbl(gvrow("QTY")))

                    If rad1.Checked = True Then
                        Cur_VAT_Amt = Cur_VAT_Amt + (CDbl(gvrow("TAX_AMOUNT")) * 0) 'CDbl(gvrow("QTY")) 'no TAX in Stock Adjustments
                    Else
                        Cur_VAT_Amt = Cur_VAT_Amt + (CDbl(gvrow("TAX_AMOUNT")) * CDbl(gvrow("QTY"))) 'CDbl(gvrow("QTY")) 'no TAX in Stock Adjustments
                    End If

                End If

            Next
            pay_section.Visible = True
        Else
            GridScroll.Visible = False
            pay_section.Visible = False
        End If
        lbl0.Text = "" & Convert.ToDouble(Cur_Net_Amt).ToString("#,###,##0.00")
        lblSubTotal.Text = "" & Convert.ToDouble(Cur_SubTotal_Amt).ToString("#,###,##0.00")
        lblSubVATTotal.Text = "" & Convert.ToDouble(Cur_VAT_Amt).ToString("#,###,##0.00")



        Net_Pay_Amt = Net_Pay_Amt + Cur_Net_Amt
        Cur_Net_Amt = 0.0
        Cur_SubTotal_Amt = 0.0
        Cur_VAT_Amt = 0.0
        lblTotal.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00")
        txtTotal.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00")
        txtDue.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00")
        txtCashTotal.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00")
        'Next


        'COURIER AMOUNT ADDING SECTION
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_COURIER_FEE]", pParms)
        courier_amount = Convert.ToDecimal(ds.Tables(0).Rows(0)("COURIER_FEE").ToString())
        'If rbl_deliverytype.SelectedItem.Value = 2 Then
        '    If Net_Pay_Amt > 0 Then
        'Net_Pay_Amt = Net_Pay_Amt + courier_amount
        'lblTotal.Text = Net_Pay_Amt
        'Else
        '    rbl_deliverytype.SelectedValue = 0
        '    id_courier.Visible = False

        '    End If
        'End If

    End Sub

    Public Function GetTable() As DataTable
        Dim dt As DataTable = New DataTable()
        dt.Columns.Add("ID")
        dt.Columns.Add("BSH_ID")
        dt.Columns.Add("ITEM_ID")
        dt.Columns.Add("ITEM_DESCR")
        dt.Columns.Add("BOOK_NAME")
        dt.Columns.Add("ISBN")
        dt.Columns.Add("PRICE")
        dt.Columns.Add("QTY")
        dt.Columns.Add("TAX_CODE")
        dt.Columns.Add("TAX_AMOUNT")
        dt.Columns.Add("NET_AMOUNT")
        dt.Columns.Add("STU_NO")
        Return dt
    End Function
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim obj As Object = sender.parent

        Dim errors As String = ""
        If ddlType.SelectedValue = "2" Then
            If txtTypeElement.Text = "" Then
                errors &= IIf(errors.Length = 0, "", ", ") & "Please select Student"
            Else
                Dim str_conn1 As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Dim StrSQL As String = "select top 10 ID +'-'+Name As Name,ID from (select STU.STU_FIRSTNAME  + case when LTRIM(RTRIM(isnull(STU.STU_MIDNAME, ' '))) ='' then '' else ' '+isnull(STU.STU_MIDNAME, ' ') end + case when LTRIM(RTRIM(isnull(STU.STU_LASTNAME, ' '))) ='' then '' else ' '+isnull(STU.STU_LASTNAME, ' ') end [Name] ,STU_NO As ID from OASIS..STUDENT_M STU INNER JOIN OASIS..ACADEMICYEAR_D ON ACD_ID=STU_ACD_ID AND ACD_BSU_ID=STU_BSU_ID AND ISNULL(ACD_CURRENT,0)=1 WHERE [STU_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [STU_CURRSTATUS]='EN' ) a WHERE 1=1 AND ID='" & hf_TypeElement_ID.Value.Trim & "'"
                Dim ds07 As DataSet
                ds07 = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, StrSQL)
                Dim intRows As Int32 = ds07.Tables(0).Rows.Count
                If intRows = 0 Then
                    errors &= IIf(errors.Length = 0, "", ", ") & "Selected Student is wrong. Please select again.."
                End If
            End If
        End If
        If ddlType.SelectedValue = "3" Then
            If txtTypeElement.Text = "" Then
                errors &= IIf(errors.Length = 0, "", ", ") & "Please select Employee"
            Else
                Dim StrSQL As String = "select top 10 ID +'-'+Name As Name,ID from (SELECT   EMP.EMP_FNAME  + case when LTRIM(RTRIM(isnull(EMP.EMP_MNAME, ' '))) ='' then '' else ' '+isnull(EMP.EMP_MNAME, ' ') end + case when LTRIM(RTRIM(isnull(EMP.EMP_LNAME, ' '))) ='' then '' else ' '+isnull(EMP.EMP_LNAME, ' ') end [Name],EMPNO AS ID FROM OASIS..EMPLOYEE_M EMP WHERE ISNULL(EMP_bACTIVE,0)=1 AND EMP_STATUS IN (1,2,9) AND EMP_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "' ) a WHERE 1=1 AND ID='" & hf_TypeElement_ID.Value.Trim & "'"

                Dim str_conn1 As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Dim ds09 As DataSet
                ds09 = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, StrSQL)
                Dim intRows As Int32 = ds09.Tables(0).Rows.Count
                If intRows = 0 Then
                    errors &= IIf(errors.Length = 0, "", ", ") & "Selected Employee is wrong. Please select again.."
                End If
            End If
        End If


        If errors.Length > 0 Then
            usrMessageBar2.ShowNotification(errors, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtTypeElement.Enabled = False
        End If


        Dim hf_Item_ID As New HiddenField
        hf_Item_ID = DirectCast(obj.FindControl("hf_Item_ID"), HiddenField)
        Dim STR_ITEM_ID As String = hf_Item_ID.Value

        Dim hf_Type_ID As New HiddenField
        hf_Type_ID = DirectCast(obj.FindControl("hf_Type_ID"), HiddenField)
        Dim STR_TYPE_ID As String = hf_Type_ID.Value

        Dim GridSelBook As New GridView
        'Dim DivHeaderRow As New HtmlGenericControl
        'Dim DivMainContent As New HtmlGenericControl
        'Dim DivFooterRow As New HtmlGenericControl

        GridSelBook = grdSAL 'DirectCast(obj.FindControl("grdSAL"), GridView)
        'DivHeaderRow = DivHeaderRow 'DirectCast(obj.FindControl("DivHeaderRow"), HtmlGenericControl)
        'DivMainContent = DivMainContent 'DirectCast(obj.FindControl("DivMainContent"), HtmlGenericControl)
        'DivFooterRow = DivFooterRow 'DirectCast(obj.FindControl("DivFooterRow"), HtmlGenericControl)

        'GridSelBook.AllowPaging = False
        Dim lbSNo As String
        lbSNo = "00000000000000" ' DirectCast(obj.FindControl("lbSNo"), Label)
        If ddlType.SelectedValue = "1" Then
            lbSNo = "00000000000000"
        ElseIf ddlType.SelectedValue = "2" Then
            lbSNo = hf_TypeElement_ID.Value.Trim()
        ElseIf ddlType.SelectedValue = "3" Then
            lbSNo = hf_TypeElement_ID.Value.Trim()
        End If

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 6
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms(1).Value = Session("sBSUID")
        pParms(2) = New SqlClient.SqlParameter("@BIM_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = STR_ITEM_ID

        Dim pParms1(2) As SqlClient.SqlParameter
        pParms1(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms1(0).Value = 4
        pParms1(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms1(1).Value = Session("sBSUID")
        pParms1(2) = New SqlClient.SqlParameter("@BSH_ID", SqlDbType.VarChar, 200)
        pParms1(2).Value = STR_ITEM_ID

        Dim ds As New DataSet
        If STR_TYPE_ID.Trim = "0" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_ITEM_M]", pParms)
        ElseIf STR_TYPE_ID.Trim = "1" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_SET_ITEM_M]", pParms1)
        End If


        Dim dt As DataTable = GetTable()

        If Not SALGRD Is Nothing Then '
            'dt = SALGRD

            'check duplicate values
            Dim dt0 As New DataTable '= SALGRD
            Dim filter_str As String = ""
            Dim dtView As DataView = New DataView(SALGRD)
            If STR_TYPE_ID = 0 Then
                filter_str = "BSH_ID = -1 AND ITEM_ID = " & STR_ITEM_ID & " AND STU_NO=" & lbSNo.ToString
            ElseIf STR_TYPE_ID = 1 Then
                filter_str = "BSH_ID = " & STR_ITEM_ID & " AND STU_NO=" & lbSNo.ToString
            End If

            dtView.RowFilter = filter_str
            dt0 = dtView.ToTable

            If Not dt0 Is Nothing AndAlso dt0.Rows.Count > 0 Then
                usrMessageBar2.ShowNotification("Cannot add duplicate set (or) individual items", UserControls_usrMessageBar.WarningType.Danger)
            Else
                'check ends here


                Try
                    Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.ToString Select order
                    dt = query.CopyToDataTable()

                Catch ex As Exception
                End Try
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each gvrow As DataRow In ds.Tables(0).Rows
                        Dim dr1 As DataRow = dt.NewRow()
                        ''Dim dr2 As DataRow = dt2.NewRow()
                        dr1("ID") = dt.Rows.Count + 1 'ds.Tables(0).Rows(0)("ID").ToString
                        dr1("BSH_ID") = gvrow("BSH_ID").ToString
                        dr1("ITEM_ID") = gvrow("ITEM_ID").ToString
                        dr1("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                        dr1("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                        dr1("ISBN") = gvrow("ISBN").ToString

                        If rad1.Checked = True Then
                            dr1("PRICE") = gvrow("COST").ToString 'Changing to Cost Price from Selling Price, it was gvrow("PRICE").ToString
                        Else
                            dr1("PRICE") = gvrow("PRICE").ToString 'Changing to Cost Price from Selling Price, it was gvrow("PRICE").ToString
                        End If

                        dr1("QTY") = 0 'gvrow("QTY").ToString
                        dr1("TAX_CODE") = gvrow("TAX_CODE").ToString
                        dr1("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                        dr1("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                        dr1("STU_NO") = lbSNo.ToString
                        dt.Rows.Add(dr1)
                        SALGRD.ImportRow(dr1) '
                        'dt2.Rows.Add(dr2)
                    Next
                End If
                'SALGRD = dt
                'SALGRD.Merge(dt2)
            End If
        Else
            'dt = SALGRD
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each gvrow As DataRow In ds.Tables(0).Rows
                    Dim dr As DataRow
                    dr = dt.NewRow()

                    dr("ID") = gvrow("ID").ToString
                    dr("BSH_ID") = gvrow("BSH_ID").ToString
                    dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                    dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                    dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                    dr("ISBN") = gvrow("ISBN").ToString

                    If rad1.Checked = True Then
                        dr("PRICE") = gvrow("COST").ToString 'Changing to Cost Price from Selling Price, it was gvrow("PRICE").ToString 
                    Else
                        dr("PRICE") = gvrow("PRICE").ToString 'Changing to Cost Price from Selling Price, it was gvrow("PRICE").ToString 
                    End If
                    dr("QTY") = 0 'gvrow("QTY").ToString
                    dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                    dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                    dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                    dr("STU_NO") = lbSNo.ToString

                    dt.Rows.Add(dr)
                Next
            End If

            SALGRD = dt
        End If
        'GridSelBook.Height = 240
        GridSelBook.DataSource = SALGRD
        GridSelBook.DataBind()
        CalculateTotal()

        'used script
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FREEZE", "FreezeGrid('" & GridSelBook.ClientID & "');", True)
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "', 200, 1000 , 40 ,false); </script>", False)
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "', 200, 1000 , 50 ,false); </script>", False)
        If Not SALGRD Is Nothing Then
            If SALGRD.Rows.Count >= 1 Then
                rad1.Enabled = False
                rad2.Enabled = False
            End If
        End If
    End Sub
    <System.Web.Services.WebMethod()>
    Public Shared Function GetItmDescr(ByVal prefixText As String, ByVal contextKey As String) As String()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

        Dim StrSQL As String
        If contextKey Is Nothing Then contextKey = ""
        StrSQL = "select top 10 DESCRIPTION,BIM_ID, TYPE from (select [BIM_BOOK_NAME] DESCRIPTION,BIM_ID,0 AS TYPE from [BOOK_ITEM_M] where [BIM_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1 AND BIM_GRD_ID LIKE '%%' UNION ALL select [BSH_DESCR] DESCRIPTION,BSH_ID AS BIM_ID,1 AS TYPE from [BOOK_SET_H] where [BSH_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND ISNULL([BSH_DELETED],0)=0) a where 1=1 and DESCRIPTION like '%" & prefixText & "%'  "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("BIM_ID").ToString & "|" & ds.Tables(0).Rows(i2)("TYPE").ToString & "$" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
        Next
        Return items.ToArray()
    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function GetTypeElements(ByVal prefixText As String, ByVal contextKey As String) As String()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

        Dim StrSQL As String = ""
        If contextKey Is Nothing Then contextKey = ""
        If contextKey = "2" Then
            StrSQL = "select top 10 ID +'-'+Name As Name,ID from (select STU.STU_FIRSTNAME  + case when LTRIM(RTRIM(isnull(STU.STU_MIDNAME, ' '))) ='' then '' else ' '+isnull(STU.STU_MIDNAME, ' ') end + case when LTRIM(RTRIM(isnull(STU.STU_LASTNAME, ' '))) ='' then '' else ' '+isnull(STU.STU_LASTNAME, ' ') end [Name] ,STU_NO As ID FROM OASIS..STUDENT_M STU INNER JOIN OASIS..ACADEMICYEAR_D ON ACD_ID=STU_ACD_ID AND ACD_BSU_ID=STU_BSU_ID AND ISNULL(ACD_CURRENT,0)=1 WHERE [STU_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [STU_CURRSTATUS]='EN' ) a WHERE 1=1 AND Name like '%" & prefixText & "%'  "
        ElseIf contextKey = "3" Then
            StrSQL = "select top 10 ID +'-'+Name As Name,ID from (SELECT   EMP.EMP_FNAME  + case when LTRIM(RTRIM(isnull(EMP.EMP_MNAME, ' '))) ='' then '' else ' '+isnull(EMP.EMP_MNAME, ' ') end + case when LTRIM(RTRIM(isnull(EMP.EMP_LNAME, ' '))) ='' then '' else ' '+isnull(EMP.EMP_LNAME, ' ') end [Name],EMPNO AS ID FROM OASIS..EMPLOYEE_M EMP WHERE ISNULL(EMP_bACTIVE,0)=1 AND EMP_STATUS IN (1,2,9) AND EMP_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "' ) a WHERE 1=1 AND Name like '%" & prefixText & "%'  "
        End If

        If StrSQL <> "" Then
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

            Dim intRows As Int32 = ds.Tables(0).Rows.Count
            Dim items As New List(Of String)(intRows)
            Dim i2 As Integer

            For i2 = 0 To intRows - 1
                Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("Name").ToString, ds.Tables(0).Rows(i2)("ID").ToString & "|" & ds.Tables(0).Rows(i2)("Name").ToString)
                items.Add(item)
            Next
            Return items.ToArray()
        End If

    End Function
    Protected Sub ddlType_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) 'Handles ddlType.SelectedIndexChanged
        Dim AutoCompleteExtender1 As AjaxControlToolkit.AutoCompleteExtender = acSTU
        AutoCompleteExtender1.ContextKey = ddlType.SelectedValue
        If ddlType.SelectedValue = "1" Then
            txtTypeElement.Visible = False
            txtTypeElement.Enabled = True
            txtTypeElement.Text = ""
            hf_TypeElement_ID.Value = ""
            lbl_SelType.Visible = False
            id_invoice.Visible = True
            TBWE3.WatermarkText = "Search Type"
            id_txtdanger.Visible = False

            Dim GridSelBook As New GridView
            GridSelBook = grdSAL
            GridSelBook.DataSource = Nothing
            GridSelBook.DataBind()
            SALGRD = Nothing

            txtItmSelect.Text = ""
            hf_Item_ID.Value = ""
        ElseIf ddlType.SelectedValue = "2" Then
            txtTypeElement.Visible = True
            txtTypeElement.Enabled = True
            txtTypeElement.Text = ""
            hf_TypeElement_ID.Value = ""
            lbl_SelType.Visible = True
            id_txtdanger.Visible = True
            lbl_SelType.Text = "Select Student "
            id_invoice.Visible = False
            TBWE3.WatermarkText = "Search Student"

            Dim GridSelBook As New GridView
            GridSelBook = grdSAL
            GridSelBook.DataSource = Nothing
            GridSelBook.DataBind()
            SALGRD = Nothing

            txtItmSelect.Text = ""
            hf_Item_ID.Value = ""
        ElseIf ddlType.SelectedValue = "3" Then

            txtTypeElement.Visible = True
            txtTypeElement.Enabled = True
            txtTypeElement.Text = ""
            hf_TypeElement_ID.Value = ""
            lbl_SelType.Visible = True
            id_txtdanger.Visible = True
            id_invoice.Visible = False
            lbl_SelType.Text = "Select Employee "
            TBWE3.WatermarkText = "Search Employee"

            Dim GridSelBook As New GridView
            GridSelBook = grdSAL
            GridSelBook.DataSource = Nothing
            GridSelBook.DataBind()
            SALGRD = Nothing

            txtItmSelect.Text = ""
            hf_Item_ID.Value = ""
        End If


    End Sub
    Protected Sub grdSAL_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) 'Handles grdSAL.PageIndexChanging
        '    grdSAL.PageIndex = e.NewPageIndex
        Dim gvOrders As GridView = TryCast(sender, GridView)
        gvOrders.PageIndex = e.NewPageIndex
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As String
        lbSNo = "00000000000000" ' DirectCast(obj.FindControl("lbSNo"), Label)
        If ddlType.SelectedValue = "1" Then
            lbSNo = "00000000000000"
        ElseIf ddlType.SelectedValue = "2" Then
            lbSNo = hf_TypeElement_ID.Value.Trim()
        ElseIf ddlType.SelectedValue = "3" Then
            lbSNo = hf_TypeElement_ID.Value.Trim()
        End If

        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        'rblist = DirectCast(obj.FindControl("rdbBookSets"), RadioButtonList)
        rblist = DirectCast(obj.FindControl("rdbBookSets"), CheckBoxList)
        txtTotal = DirectCast(obj.FindControl("txtGrandTotal"), Label)
        'Dim lbSNo As New Label
        'lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)
        Dim dt As DataTable = GetTable()
        If Not SALGRD Is Nothing Then
            Try
                Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.ToString Select order
                dt = query.CopyToDataTable()

            Catch ex As Exception
            End Try
            'dt = SALGRD
        End If
        GridSelBook.Visible = True
        GridSelBook.ShowFooter = False
        GridSelBook.DataSource = dt
        GridSelBook.DataBind()
        CalculateTotal()


    End Sub

    Public Sub New()

    End Sub
End Class
