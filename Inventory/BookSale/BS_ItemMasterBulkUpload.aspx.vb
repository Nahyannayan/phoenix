﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports DocumentFormat.OpenXml.Office.Excel
Imports System.Data.OleDb

Partial Class Inventory_BookSale_BS_ItemMasterBulkUpload
    Inherits System.Web.UI.Page

    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI01149") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If



            End If
            Dim sm As ScriptManager = Master.FindControl("ScriptManager1")
            sm.RegisterPostBackControl(btnUpload)
            sm.RegisterPostBackControl(LinkSample)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar2.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim connString As String = ""

        Dim strFileType As String = Path.GetExtension(FileUpload1.FileName).ToLower()
        Dim Cur_Date As String = DateTime.Now.ToString("ddMMMyyyy")
        Dim Cur_Time As String = DateTime.Now.Hour.ToString + DateTime.Now.Minute.ToString + DateTime.Now.Second.ToString
        Dim strFileName As String = Path.GetFileNameWithoutExtension(FileUpload1.FileName) + "_" + Session("sBsuId") + "_" + Cur_Date + "_" + Cur_Time + strFileType.Trim()
        'Dim path2 As String = Server.MapPath("~/App_Data/") + FileUpload1.FileName
        Dim path2 = WebConfigurationManager.AppSettings("UploadExcelFile").ToString() & "BookSale\" + strFileName
        FileUpload1.SaveAs(path2)


        Dim path__1 As String = FileUpload1.PostedFile.FileName
        'Connection String to Excel Workbook
        If strFileType.Trim() = ".xls" Then
            connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path2 & ";Extended Properties=""Excel 8.0;""" 'HDR=Yes;IMEX=2
        ElseIf strFileType.Trim() = ".xlsx" Then
            'connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & path__1 & ";Extended Properties=""Excel 12.0;""" 'HDR=Yes;IMEX=2
            connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path2 + ";Extended Properties=Excel 8.0"
        End If



        'OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", con_excel);



        'Dim query As String = "SELECT [BookName],[Description],[ISBN],[Publication],[Category],[Type],[CostPrice],[SellingPrice],[TaxType],[TaxAmount],[NetPrice],[Grade],[ShowOnline] FROM [Sheet1$]"
        Dim query As String = "SELECT [BookName],[Description],[ISBN],[Publication],[Category],[Type],[CostPrice],[SellingPrice],[Grade],[ShowOnline] FROM [Sheet1$]"
        Dim conn As New OleDbConnection(connString)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim cmd As New OleDbCommand(query, conn)
        Dim da As New OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        'grvExcelData.DataSource = ds.Tables(0)
        'grvExcelData.DataBind()
        da.Dispose()
        conn.Close()
        conn.Dispose()

        Dim VAT_TYPE As String = ""
        Dim VAT_AMOUNT As Decimal
        Dim NET_AMOUNT As Decimal
        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer = 0
        Try
            Dim dt As New DataTable '
            dt = ds.Tables(0)
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                'Dim dbl_net_amt As Double
                For Each gvrow As DataRow In dt.Rows

                    If gvrow("Grade").ToString = "" Or gvrow("Type").ToString = "" Or gvrow("BookName").ToString = "" Or gvrow("Description").ToString = "" _
                    Or gvrow("ISBN").ToString = "" Or gvrow("ShowOnline").ToString() = "" Then ' Or gvrow("TaxType").ToString = ""


                        retval = 1
                        usrMessageBar2.ShowNotification("Error in Upload. Please verify the excel upload data", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit For
                    End If

                    If gvrow("Grade").ToString = "PN1" Or gvrow("Grade").ToString = "PN2" Or gvrow("Grade").ToString = "PK" Or
                            gvrow("Grade").ToString = "KG1" Or gvrow("Grade").ToString = "KG2" Or gvrow("Grade").ToString = "01" Or
                            gvrow("Grade").ToString = "02" Or gvrow("Grade").ToString = "03" Or gvrow("Grade").ToString = "04" Or
                            gvrow("Grade").ToString = "05" Or gvrow("Grade").ToString = "06" Or gvrow("Grade").ToString = "07" Or
                            gvrow("Grade").ToString = "08" Or gvrow("Grade").ToString = "09" Or gvrow("Grade").ToString = "10" Or
                            gvrow("Grade").ToString = "11" Or gvrow("Grade").ToString = "12" Or gvrow("Grade").ToString = "13" Then
                    Else
                        retval = 1
                        usrMessageBar2.ShowNotification("Error in Upload. Please verify the grade in excel upload data", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit For
                    End If
                    'If gvrow("TaxType").ToString = "VAT0" Or gvrow("TaxType").ToString = "VAT5" Then
                    '    If gvrow("TaxType").ToString = "VAT5" Then
                    '        Try
                    '            If gvrow("TaxAmount").ToString = "" Then 'Or CDbl(gvrow("TaxAmount").ToString) = 0
                    '                retval = 1
                    '                usrMessageBar2.ShowNotification("Error in Upload. Please verify the tax amount for VAT enabled items in excel upload data", UserControls_usrMessageBar.WarningType.Danger)
                    '                stTrans.Rollback()
                    '                Exit For
                    '            End If
                    '        Catch ex As Exception
                    '            retval = 1
                    '            usrMessageBar2.ShowNotification("Error in Upload. Please verify the tax amount for VAT enabled items in excel upload data", UserControls_usrMessageBar.WarningType.Danger)
                    '            stTrans.Rollback()
                    '            Exit For
                    '        End Try
                    '    End If

                    'Else
                    '    retval = 1
                    '    usrMessageBar2.ShowNotification("Error in Upload. Please verify the vat type in excel upload data", UserControls_usrMessageBar.WarningType.Danger)
                    '    stTrans.Rollback()
                    '    Exit For
                    'End If

                    If gvrow("Type").ToString = "1" Or gvrow("Type").ToString = "2" Or gvrow("Type").ToString = "3" Or gvrow("Type").ToString = "4" Then
                    Else
                        retval = 1
                        usrMessageBar2.ShowNotification("Error in Upload. Please verify the stationary or nonstationary type in excel upload data", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit For
                    End If

                    If gvrow("ShowOnline").ToString = "0" Or gvrow("ShowOnline").ToString = "1" Then
                    Else
                        retval = 1
                        usrMessageBar2.ShowNotification("Error in Upload. Please verify the show online option in excel upload data", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit For
                    End If

                 
                    Dim ds09 As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [BIT_ID],[BIT_DESC],ISNULL(BIT_VAT_CODE,'VAT0') AS BIT_VAT_CODE FROM dbo.[BOOK_ITEM_TYPE_M] WHERE ISNULL(BIT_bDELETE,0)=0 AND BIT_ID='" & gvrow("Type").ToString & "' ORDER BY BIT_ID")
                    If Not ds09 Is Nothing AndAlso ds09.Tables(0).Rows.Count > 0 Then
                        If gvrow("Type").ToString = "1" Then
                            VAT_TYPE = ds09.Tables(0).Rows(0)("BIT_VAT_CODE").ToString '"VAT5"
                        Else
                            VAT_TYPE = ds09.Tables(0).Rows(0)("BIT_VAT_CODE").ToString '"VAT0"
                        End If
                    End If
                    Dim dblPrice As Double = 0, lblTaxAmt As New Label, lblNetAmt As New Label
                    If IsNumeric(CDbl(gvrow("SellingPrice").ToString)) Then
                        dblPrice = CDbl(gvrow("SellingPrice").ToString)
                        CALCULATE_TAX(dblPrice, lblTaxAmt, lblNetAmt, VAT_TYPE)
                        VAT_AMOUNT = Format(CDbl(lblTaxAmt.Text), "#,##0.000")
                        NET_AMOUNT = Format(CDbl(lblNetAmt.Text), "#,##0.00")
                    End If


                    Dim pParms(17) As SqlParameter
                    pParms(0) = New SqlParameter("@BIM_ID", SqlDbType.Int)
                    'pParms(0).Direction = ParameterDirection.Output
                    pParms(0).Value = 0
                    pParms(1) = New SqlParameter("@BIM_DESCR", SqlDbType.VarChar, 250)
                    pParms(1).Value = gvrow("Description").ToString
                    pParms(2) = New SqlParameter("@BIM_GRD_ID", SqlDbType.VarChar, 250)
                    pParms(2).Value = gvrow("Grade").ToString & "|"
                    pParms(3) = New SqlParameter("@BIM_ISBN", SqlDbType.VarChar, 20)
                    pParms(3).Value = gvrow("ISBN").ToString
                    pParms(4) = New SqlParameter("@BIM_CATEGORY", SqlDbType.VarChar, 50)
                    pParms(4).Value = gvrow("Category").ToString
                    pParms(5) = New SqlParameter("@BIM_PUBLICATION", SqlDbType.VarChar, 50)
                    pParms(5).Value = gvrow("Publication").ToString

                    pParms(6) = New SqlParameter("@BIM_ITEM_TYPE", SqlDbType.Int)
                    pParms(6).Value = gvrow("Type").ToString
                    pParms(7) = New SqlParameter("@BIM_BSU_ID", SqlDbType.VarChar, 20)
                    pParms(7).Value = Session("sBsuId")
                    pParms(8) = New SqlParameter("@BIM_COST_PRICE", SqlDbType.VarChar, 20)
                    pParms(8).Value = gvrow("CostPrice").ToString
                    pParms(9) = New SqlParameter("@BIM_SELL_PRICE", SqlDbType.VarChar, 20)
                    pParms(9).Value = gvrow("SellingPrice").ToString
                    pParms(10) = New SqlParameter("@BIM_TAX_CODE", SqlDbType.VarChar, 20)
                    pParms(10).Value = VAT_TYPE
                    pParms(11) = New SqlParameter("@BIM_TAX_AMOUNT", SqlDbType.VarChar, 20)
                    pParms(11).Value = VAT_AMOUNT
                    pParms(12) = New SqlParameter("@BIM_NET_AMOUNT", SqlDbType.VarChar, 20)
                    pParms(12).Value = NET_AMOUNT

                    pParms(13) = New SqlParameter("@BIM_BOOK_NAME", SqlDbType.VarChar, 250)
                    pParms(13).Value = gvrow("BookName").ToString
                    pParms(14) = New SqlParameter("@BIM_bSHOWONLINE", SqlDbType.Bit)
                    pParms(14).Value = CBool(gvrow("ShowOnline").ToString())

                    pParms(15) = New SqlParameter("@USER_ID", SqlDbType.VarChar, 50)
                    pParms(15).Value = Session("sUsr_id")
                    pParms(16) = New SqlParameter("@RET_VAL", SqlDbType.Int)
                    pParms(16).Direction = ParameterDirection.ReturnValue
                    pParms(17) = New SqlParameter("@NEW_BIM_ID", SqlDbType.Int)
                    pParms(17).Direction = ParameterDirection.Output

                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.[SAVE_BOOK_ITEM_M]", pParms)
                    If pParms(16).Value <> 0 Then
                        'lblError.Text = UtilityObj.getErrorMessage(pParms(16).Value)
                        retval = 1
                        usrMessageBar2.ShowNotification("Error in Upload. Please verify the data / " & UtilityObj.getErrorMessage(pParms(16).Value), UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit For
                    End If



                Next

                If retval = 0 Then
                    stTrans.Commit()
                    'lblError.Text = "Data Saved Successfully !!!"
                    usrMessageBar2.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)

                End If

            End If
        Catch ex As Exception
            stTrans.Rollback()
            usrMessageBar2.ShowNotification("Error in Upload Excel File", UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Text = getErrorMessage(1000)
            'lblError.CssClass = "alert alert-warning"
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If

        End Try

    End Sub
    Private Sub CALCULATE_TAX(ByVal Amount As Double, ByRef lblTaxAmt As Label, ByRef lblNETAmt As Label, ByVal VAT_CODE As String)
        lblTaxAmt.Text = "0.000"
        lblNETAmt.Text = "0.00"
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & Amount & ",'" & VAT_CODE & "')")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTaxAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.000")
                lblNETAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub
    Protected Sub LinkSample_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkSample.Click
        Try

            Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString() & "BookSale\BookSaleItemUpload.xls"


            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()


        Catch ex As Exception

        End Try
    End Sub
End Class
