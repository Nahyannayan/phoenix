﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="BS_Collection_Message.aspx.vb" Inherits="Inventory_BookSale_BS_Collection_Message" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Book Sale Collection Message
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <%-- <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1"></asp:TextBox>
                                    <asp:ImageButton ID="lnkFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFromDate" PopupButtonID="lnkFDate"></ajaxToolkit:CalendarExtender>
                                </td>
                                <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="1"></asp:TextBox>
                                    <asp:ImageButton ID="lnkTDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtToDate" PopupButtonID="lnkTDate"></ajaxToolkit:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Message</span></td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtMessage" runat="server" Height="32px" TextMode="MultiLine" TabIndex="160"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Deliver Mode</span></td>
                                <td align="left" colspan="3">
                                    <asp:CheckBox ID="ID_COLP" Width="100%" runat="server" Text="Collect the Books from School Personally" Checked="true"></asp:CheckBox>
                                    <asp:CheckBox ID="ID_COLC" Width="100%" runat="server" Text="Deliver Books To Child" Checked="true"></asp:CheckBox>
                                    <asp:CheckBox ID="ID_COLD" Width="100%" runat="server" Text="Deliver Books By Courier" ></asp:CheckBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </div>
        </div>
    </div>
</asp:Content>
