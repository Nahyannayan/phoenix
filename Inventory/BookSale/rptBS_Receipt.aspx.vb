﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports Telerik.Web.UI
Partial Class Inventory_BookSale_rptBS_Receipt
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Dim MainMnu_code As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            'If Request.QueryString("MainMnu_code") = "" Then
            '    Response.Redirect("..\..\noAccess.aspx")
            'End If
            If Request.QueryString("type") = "receipt" And Request.QueryString("saleid") <> "" Then
                GenerateBookSaleReceipt(Request.QueryString("options"), Request.QueryString("saleid"))
            End If
            If Request.QueryString("type") = "delivery" And Request.QueryString("saleid") <> "" Then
                GenerateBookSaleDeliveryNote(Request.QueryString("options"), Request.QueryString("saleid"))
            End If
            If Request.QueryString("type") = "invoice" And Request.QueryString("saleid") <> "" Then
                PRINT_BOOK_SALES_TAX_INVOICE(Request.QueryString("saleid"))
            End If

        End If
        'Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)

    End Sub
    Private Sub GenerateBookSaleReceipt(ByVal options As String, ByVal id As String)
        Dim cmd As New SqlCommand("[dbo].[GET_BOOKSALES_COUNTER_RECEIPT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpOPTIONS As New SqlParameter("@OPTIONS", SqlDbType.Int)
        sqlpOPTIONS.Value = options
        cmd.Parameters.Add(sqlpOPTIONS)
        Dim sqlpBSAH_BSAH_ID As New SqlParameter("@BSAH_BSAH_ID", SqlDbType.Int)
        sqlpBSAH_BSAH_ID.Value = id
        cmd.Parameters.Add(sqlpBSAH_BSAH_ID)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)

        Dim caption As String = "Book Sale Counter Receipt"
        Dim deliverymode As String = ""
        Dim Sale_type As String = ""
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 2
        pParms0(1) = New SqlClient.SqlParameter("@BSAH_BSAH_ID", SqlDbType.Int)
        pParms0(1).Value = id
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_BOOKSALES_COUNTER_RECEIPT]", pParms0)
        Sale_type = Convert.ToString(ds0.Tables(0).Rows(0)("BSAH_TYPE"))
        If Sale_type = "S" Then
            If Convert.ToString(ds0.Tables(0).Rows(0)("BSAH_SOURCE")) = "ONLINE" Then


                caption = "Book Sale Online Receipt"
            ElseIf Convert.ToString(ds0.Tables(0).Rows(0)("BSAH_SOURCE")) = "COUNTER" Then
                caption = "Book Sale Counter Receipt"
            End If

        ElseIf Sale_type = "R" Then
            caption = "Return Book Sale Counter Receipt"
        ElseIf Sale_type = "A" Then
            caption = "Book Stock Adjustment Receipt"
        End If


        Dim repSource As New MyReportClass
        Dim params As New Hashtable


        params("userName") = Session("sUsr_name")

        params("RPT_CAPTION") = caption

        Dim ds5 As New DataSet
        Dim BSAHO_DELIVERY_TYPE As Integer = -1
        ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT BSAHO_DELIVERY_TYPE FROM [OASIS_PUR_INV].[dbo].[BOOK_SALE_ONLINE_H] WITH (NOLOCK) WHERE BSAHO_BSAH_ID=" & id)
        If ds5.Tables(0).Rows.Count > 0 Then
            BSAHO_DELIVERY_TYPE = CInt((ds5.Tables(0).Rows(0)("BSAHO_DELIVERY_TYPE")))
        End If

        If Sale_type = "S" Or Sale_type = "R" Then
            If Sale_type = "S" Then
                If BSAHO_DELIVERY_TYPE = 0 Then
                    params("deliverymode") = "Delivery Mode : Collect the Books from School Personally"
                ElseIf BSAHO_DELIVERY_TYPE = 1 Then
                    params("deliverymode") = "Delivery Mode : Deliver Books To Child"
                ElseIf BSAHO_DELIVERY_TYPE = 2 Then
                    params("deliverymode") = "Delivery Mode : Deliver Books By Courier"
                Else
                    params("deliverymode") = ""
                End If
            Else
                params("deliverymode") = ""
            End If

        End If


        'params.Add("@IMG_BSU_ID", Session("sbsuid"))
        'params.Add("@IMG_TYPE", "LOGO")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSub As New SqlCommand("[dbo].[GET_BOOKSALES_COUNTER_RECEIPT_DETAILS]")
        cmdSub.CommandType = CommandType.StoredProcedure

        Dim sqlSubpOPTIONS As New SqlParameter("@OPTIONS", SqlDbType.Int)
        sqlSubpOPTIONS.Value = options
        cmdSub.Parameters.Add(sqlSubpOPTIONS)
        Dim sqlSubpBSAH_BSAH_ID As New SqlParameter("@BSAH_BSAH_ID", SqlDbType.Int)
        sqlSubpBSAH_BSAH_ID.Value = id
        cmdSub.Parameters.Add(sqlSubpBSAH_BSAH_ID)
        cmdSub.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        repSourceSubRep(0).Command = cmdSub

        If Sale_type = "S" Or Sale_type = "R" Then
            repSourceSubRep(1) = New MyReportClass
            Dim cmdSub1 As New SqlCommand("[dbo].[GET_BOOKSALES_COUNTER_PAY_DETAILS]")
            cmdSub1.CommandType = CommandType.StoredProcedure

            Dim sqlSubpOPTIONS1 As New SqlParameter("@OPTIONS", SqlDbType.Int)
            sqlSubpOPTIONS1.Value = options
            cmdSub1.Parameters.Add(sqlSubpOPTIONS1)
            Dim sqlSubpBSAH_BSAH_ID1 As New SqlParameter("@BSAH_ID", SqlDbType.Int)
            sqlSubpBSAH_BSAH_ID1.Value = id
            cmdSub1.Parameters.Add(sqlSubpBSAH_BSAH_ID1)
            cmdSub1.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
            repSourceSubRep(1).Command = cmdSub1
        End If


        repSource.SubReport = repSourceSubRep

        If Sale_type = "S" Then
            caption = "Book Sale Counter Receipt"
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptBS_CounterReceipt.rpt"
        ElseIf Sale_type = "R" Then
            caption = "Return Book Sale Counter Receipt"
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptBS_CounterReceipt.rpt"
        ElseIf Sale_type = "A" Then
            caption = "Book Stock Adjustment Receipt"
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptBS_StockReceipt.rpt"
        End If

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        'If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
        '    'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
        '    ReportLoadSelectionExport()
        'Else
        'Response.Redirect("../../Reports/ASPX Report/rptReportViewerNew.aspx", True)
        Response.Redirect("../../Reports/ASPX Report/RptviewerNew.aspx", True)
        'ReportLoadSelection()
        'End If
    End Sub

    Private Sub GenerateBookSaleDeliveryNote(ByVal options As String, ByVal id As String)
        Dim cmd As New SqlCommand("[dbo].[GET_BOOKSALES_DELIVERY_RECEIPT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpOPTIONS As New SqlParameter("@OPTIONS", SqlDbType.Int)
        sqlpOPTIONS.Value = 1
        cmd.Parameters.Add(sqlpOPTIONS)
        Dim sqlpBSAH_NO As New SqlParameter("@BSAH_NO", SqlDbType.VarChar)
        sqlpBSAH_NO.Value = id
        cmd.Parameters.Add(sqlpBSAH_NO)
        Dim sqlpBSAH_BSU_ID As New SqlParameter("@BSAH_BSU_ID", SqlDbType.VarChar)
        sqlpBSAH_BSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSAH_BSU_ID)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)

        Dim caption As String = "Book Sale Delivery Note"
     

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RPT_CAPTION") = caption

        'params.Add("@IMG_BSU_ID", Session("sbsuid"))
        'params.Add("@IMG_TYPE", "LOGO")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSub As New SqlCommand("[dbo].[GET_BOOKSALES_DELIVERY_RECEIPT_DETAILS]")
        cmdSub.CommandType = CommandType.StoredProcedure

        Dim sqlSubpOPTIONS As New SqlParameter("@OPTIONS", SqlDbType.Int)
        sqlSubpOPTIONS.Value = 1
        cmdSub.Parameters.Add(sqlSubpOPTIONS)
        Dim sqlSubpBSAH_NO As New SqlParameter("@BSAH_NO", SqlDbType.VarChar)
        sqlSubpBSAH_NO.Value = id
        cmdSub.Parameters.Add(sqlSubpBSAH_NO)
        Dim sqlpSubBSAH_BSU_ID As New SqlParameter("@BSAH_BSU_ID", SqlDbType.VarChar)
        sqlpSubBSAH_BSU_ID.Value = Session("sBsuid")
        cmdSub.Parameters.Add(sqlpSubBSAH_BSU_ID)


        cmdSub.Connection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        repSourceSubRep(0).Command = cmdSub

        repSource.SubReport = repSourceSubRep
        'caption = "Book Sale Delivery Note"
        repSource.ResourceName = "../../Inventory/Reports/RPT/rptBS_DeliveryNote.rpt"
        Session("ReportSource") = repSource
        Response.Redirect("../../Reports/ASPX Report/RptviewerNew.aspx", True)

    End Sub

    Sub PRINT_BOOK_SALES_TAX_INVOICE(ByVal id As String)
        Dim SAL_ID As String = "", BSU_ID As String = Session("sBsuId")
        'If Not Request.QueryString("ID") Is Nothing Then
        '    SAL_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        'End If
        SAL_ID = id

        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim cmd As New SqlCommand("dbo.PRINT_NEW_BOOK_SALES_TAX_INVOICE")
        cmd.CommandType = CommandType.StoredProcedure
        Dim cmdSub1 As New SqlCommand("dbo.PRINT_NEW_BOOK_SALES_TAX_INVOICE_SPLITUP")
        cmdSub1.CommandType = CommandType.StoredProcedure
        Dim cmdSub2 As New SqlCommand("dbo.getBsuInFoWithImage")
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSAL_ID As New SqlParameter("@BSAH_ID", SqlDbType.BigInt)
        sqlpSAL_ID.Value = SAL_ID
        cmd.Parameters.Add(sqlpSAL_ID)
        cmd.Connection = New SqlConnection(str_conn)

        Dim sqlpSAL_IDsub As New SqlParameter("@BSAH_ID", SqlDbType.BigInt)
        sqlpSAL_IDsub.Value = SAL_ID
        cmdSub1.Parameters.Add(sqlpSAL_IDsub)
        cmdSub1.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub2 As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub2.Value = BSU_ID
        cmdSub2.Parameters.Add(sqlpBSU_IDsub2)
        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmdSub2.Parameters.Add(sqlpIMG_TYPE)
        cmdSub2.Connection = New SqlConnection(ConnectionManger.GetOASISConnectionString)

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2


        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("UserName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.HeaderBSUID = BSU_ID
        'repSource.ResourceName = "../../Fees/Reports/RPT/rptNewBookSalesTAXInvoice.rpt"
        repSource.ResourceName = "../../Inventory/Reports/RPT/rptNewBookSalesTAXInvoice.rpt"

        Session("ReportSource") = repSource
        Response.Redirect("../../Reports/ASPX Report/RptViewerModal.aspx")
        'Response.Redirect("../../Reports/ASPX Report/RptviewerNew.aspx", True)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelectionExport()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
End Class
