﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Inventory_BookSale_BS_ItemSet
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")


            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "PI01166" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            PopulateGrades()

            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
            Else
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            End If
            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdSetItem.DataSource = SetDFooter
            grdSetItem.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            ddlGrade.Enabled = Not mDisable
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            ddlGrade.Enabled = Not mDisable
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            ddlGrade.Enabled = mDisable
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        grdSetItem.Columns(6).Visible = Not mDisable
        grdSetItem.Columns(7).Visible = Not mDisable
        grdSetItem.ShowFooter = Not mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        btnEdit.Visible = mDisable
        btnPrint.Visible = False
        txtDescr.Enabled = Not mDisable
        txtISBN.Enabled = Not mDisable

    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Seh_Id.Value = p_Modifyid
            If p_Modifyid = 0 Then
                txtBuID.Text = Session("sBsuid")
                TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m WITH(NOLOCK) where BSU_ID='" & Trim(txtBuID.Text) & "'", "MainDBO")
            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                dt = MainObj.getRecords("SELECT * FROM dbo.[BOOK_SET_H] WHERE [BSH_ID]=" & p_Modifyid, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    txtBuID.Text = dt.Rows(0)("BSH_BSU_ID")
                    ddlGrade.SelectedValue = dt.Rows(0)("BSH_GRD_ID")
                    Session("SEL_Grade") = ddlGrade.SelectedValue
                    txtDescr.Text = dt.Rows(0)("BSH_DESCR")
                    txtISBN.Text = dt.Rows(0)("BSH_ISBN")
                    TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m WITH(NOLOCK) where BSU_ID='" & Trim(txtBuID.Text) & "'", "MainDBO")
                    chkbShowOnline.Checked = dt.Rows(0)("BSH_bSHOWONLINE")
                    chkMandatory.Checked = IIf(dt.Rows(0)("BSH_bMANDATORY") Is DBNull.Value, False, dt.Rows(0)("BSH_bMANDATORY"))
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If

            FormatFigures()
            fillGridView(SetDFooter, grdSetItem, "select [BSD_ID],[BSD_BSH_ID],[BSD_BIM_ID],[BIM_ISBN], [BIM_BOOK_NAME],[BSD_QTY],ISNULL(BSD_bLOCKQTY, 0) BSD_bLOCKQTY FROM [BOOK_SET_D] inner join [BOOK_ITEM_M] WITH(NOLOCK) on BOOK_SET_D.[BSD_BIM_ID] = BOOK_ITEM_M.BIM_ID and [BIM_BSU_ID]='" & Session("sBSUID") & "' where  [BSD_BSH_ID]=" & Seh_Id.Value & " order by [BSD_ID]")

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next

    End Sub

    Sub ClearDetails()
        Seh_Id.Value = "0"
        txtDescr.Text = ""
        txtISBN.Text = ""
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property SetDFooter() As DataTable
        Get
            Return ViewState("SetDFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SetDFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
        Session("myData") = fillDataTable
        SetExportFileLink()
    End Sub

    Private Sub SetExportFileLink()
        'Try
        '    btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Framework - " & txtSupplier.Text)
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub showNoRecordsFound()
        If Not SetDFooter Is Nothing AndAlso SetDFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdSetItem.Columns.Count - 2
            grdSetItem.Rows(0).Cells.Clear()
            grdSetItem.Rows(0).Cells.Add(New TableCell())
            grdSetItem.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdSetItem.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSetItem.PageIndexChanging
        grdSetItem.PageIndex = e.NewPageIndex
        grdSetItem.DataSource = Session("myData")
        grdSetItem.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdSetItem.RowCancelingEdit
        grdSetItem.ShowFooter = True
        grdSetItem.EditIndex = -1
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdSetItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSetItem.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtSED_SEH_ID As TextBox = grdSetItem.FooterRow.FindControl("txtSED_SEH_ID")
            Dim hdnITM_ID As HiddenField = grdSetItem.FooterRow.FindControl("hdnITM_ID")
            Dim txtITM_QTY As TextBox = grdSetItem.FooterRow.FindControl("txtITM_QTY")
            Dim hdnSED_ID As HiddenField = grdSetItem.FooterRow.FindControl("hdnSED_ID")
            Dim txtITM_Descr As TextBox = grdSetItem.FooterRow.FindControl("txtITM_Descr")
            Dim chkLockQtyF As CheckBox = grdSetItem.FooterRow.FindControl("chkLockQtyF")
            'lblError.Text = ""
            Dim errString As String = ""
            'If hdnITM_ID.Value.Trim.Length = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Item"
            'If Val(txtITM_QTY.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Quantity"
            If hdnITM_ID.Value.Trim.Length = 0 Then errString &= IIf(errString.Length = 0, "", ",") & "Item"
            If Val(txtITM_QTY.Text) = 0 Then errString &= IIf(errString.Length = 0, "", ",") & "Quantity"

            If errString.Length > 0 Then
                errString &= " Mandatory"
                usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
                showNoRecordsFound()
                Exit Sub
            End If
            'validate duplicate item using Seh_Id or viewstate and add isbn for new item by using database query
            'if Seh_Id=0 then check the same item using viewstate           

            Dim dt0 As New DataTable
            Dim filter_str As String = ""
            Dim dtView As DataView = New DataView(SetDFooter)
            filter_str = "BSD_BIM_ID=" & hdnITM_ID.Value
            dtView.RowFilter = filter_str
            dt0 = dtView.ToTable
            If dt0.Rows.Count > 0 Then
                usrMessageBar2.ShowNotification("Cannot add same item master to the set", UserControls_usrMessageBar.WarningType.Danger)
            Else
                If SetDFooter.Rows(0)(1) = -1 Then SetDFooter.Rows.RemoveAt(0)
                Dim mrow As DataRow
                mrow = SetDFooter.NewRow
                mrow("BSD_ID") = 0
                mrow("BSD_BSH_ID") = Seh_Id.Value
                mrow("BSD_BIM_ID") = Val(hdnITM_ID.Value)
                mrow("BSD_QTY") = txtITM_QTY.Text
                mrow("BIM_BOOK_NAME") = txtITM_Descr.Text
                mrow("BSD_bLOCKQTY") = IIf(Not chkLockQtyF Is Nothing, chkLockQtyF.Checked, False)
                Dim dt As New DataTable
                dt = MainObj.getRecords("SELECT BIM_ISBN FROM dbo.[BOOK_ITEM_M] WITH(NOLOCK) WHERE [BIM_ID]=" & hdnITM_ID.Value, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    mrow("BIM_ISBN") = dt.Rows(0)("BIM_ISBN")
                End If
                SetDFooter.Rows.Add(mrow)
                grdSetItem.EditIndex = -1
                grdSetItem.DataSource = SetDFooter
                grdSetItem.DataBind()
                showNoRecordsFound()
                grdSetItem.EditIndex = -1
                grdSetItem.DataSource = SetDFooter
                grdSetItem.DataBind()
                showNoRecordsFound()
            End If
        End If
    End Sub

    Protected Sub grdSetItem_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdSetItem.RowDeleting
        Dim mRow() As DataRow = SetDFooter.Select("ID=" & grdSetItem.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_SetItemGridDelete.Value &= ";" & mRow(0)("BSD_ID")
            SetDFooter.Select("ID=" & grdSetItem.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            SetDFooter.AcceptChanges()
        End If
        If SetDFooter.Rows.Count = 0 Then
            SetDFooter.Rows.Add(SetDFooter.NewRow())
            SetDFooter.Rows(0)(1) = -1
        End If
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()

    End Sub

    Protected Sub grdSetItem_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdSetItem.RowEditing
        Try
            grdSetItem.ShowFooter = True
            grdSetItem.EditIndex = e.NewEditIndex
            grdSetItem.DataSource = SetDFooter
            grdSetItem.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub grdSetItem_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdSetItem.RowUpdating
        Dim s As String = grdSetItem.DataKeys(e.RowIndex).Value.ToString()
        Dim txtSED_SEH_ID As TextBox = grdSetItem.Rows(e.RowIndex).FindControl("txtSED_SEH_ID")
        Dim hdnITM_ID As HiddenField = grdSetItem.Rows(e.RowIndex).FindControl("hdnITM_ID")
        Dim txtITM_QTY As TextBox = grdSetItem.Rows(e.RowIndex).FindControl("txtITM_QTY")
        Dim hdnSED_ID As HiddenField = grdSetItem.Rows(e.RowIndex).FindControl("hdnSED_ID")
        Dim txtITM_Descr As TextBox = grdSetItem.Rows(e.RowIndex).FindControl("txtITM_Descr")
        Dim chkLockQty As CheckBox = grdSetItem.Rows(e.RowIndex).FindControl("chkLockQtyF")
        Dim mrow As DataRow
        mrow = SetDFooter.Select("ID=" & s)(0)
        mrow("BSD_BSH_ID") = Seh_Id.Value
        mrow("BSD_BIM_ID") = Val(hdnITM_ID.Value)
        mrow("BSD_QTY") = txtITM_QTY.Text
        mrow("BIM_BOOK_NAME") = txtITM_Descr.Text
        mrow("BSD_bLOCKQTY") = chkLockQty.Checked
        grdSetItem.EditIndex = -1
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetItmDescr(ByVal prefixText As String, ByVal contextKey As String) As String()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

        Dim StrSQL As String
        If contextKey Is Nothing Then contextKey = ""
        StrSQL = "select top 10 DESCRIPTION,BIM_ID from (select [BIM_BOOK_NAME] DESCRIPTION,BIM_ID from [BOOK_ITEM_M] WITH(NOLOCK) where [BIM_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1 AND BIM_GRD_ID LIKE '%" & HttpContext.Current.Session("SEL_Grade") & "%' ) a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("BIM_ID").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
        Next
        Return items.ToArray()
    End Function
    Private Sub PopulateGrades()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM VW_GRADE_M ORDER BY GRD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        Session("SEL_Grade") = ddlGrade.SelectedValue
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Session("SEL_Grade") = ddlGrade.SelectedValue

            fillGridView(SetDFooter, grdSetItem, "SELECT [BSD_ID],[BSD_BSH_ID],[BSD_BIM_ID],[BIM_ISBN], [BIM_BOOK_NAME],[BSD_QTY],ISNULL(BSD_bLOCKQTY, 0) BSD_bLOCKQTY FROM [BOOK_SET_D] inner join [BOOK_ITEM_M] WITH(NOLOCK) on BOOK_SET_D.[BSD_BIM_ID] = BOOK_ITEM_M.BIM_ID and [BIM_BSU_ID]='" & Session("sBSUID") & "' where  [BSD_BSH_ID]=" & Seh_Id.Value & " order by [BSD_ID]")

            grdSetItem.DataSource = SetDFooter
            grdSetItem.DataBind()
            showNoRecordsFound()

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'lblError.Text = ""
        Dim errString As String = ""
        If errString.Length > 0 Then errString = errString.Substring(0, errString.Length - 1) & " Mandatory"
        If grdSetItem.Rows.Count = 0 Then errString &= IIf(errString.Length = 0, "", ",") & "Set Item should contain atleast 1 item"

        If txtDescr.Text.Trim = "" Then
            'lblError.Text = "Item description cannot be blank"
            usrMessageBar2.ShowNotification("Set Name/Description cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If errString.Length > 0 Then
            usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
            showNoRecordsFound()
            Exit Sub
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(8) As SqlParameter

        pParms(1) = Mainclass.CreateSqlParameter("@BSH_ID", Seh_Id.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@BSH_BSU_ID", txtBuID.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@BSH_GRD_ID", ddlGrade.SelectedValue, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@BSH_ISBN", txtISBN.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@BSH_DESCR", txtDescr.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@BSH_bSHOWONLINE", chkbShowOnline.Checked, SqlDbType.Bit)
        pParms(7) = Mainclass.CreateSqlParameter("@BSH_USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@BSH_bMANDATORY", chkMandatory.Checked, SqlDbType.Bit)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "[dbo].[SAVE_BOOK_SET_H]", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar2.ShowNotification("Unexpected Error", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim RowCount As Integer
            For RowCount = 0 To SetDFooter.Rows.Count - 1
                Dim iParms(5) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, SetDFooter.Rows(RowCount)("BSD_ID"))
                If SetDFooter.Rows(RowCount).RowState = 8 Then rowState = -1

                iParms(1) = Mainclass.CreateSqlParameter("@BSD_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@BSD_BSH_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@BSD_BIM_ID", SetDFooter.Rows(RowCount)("BSD_BIM_ID"), SqlDbType.Int)
                iParms(4) = Mainclass.CreateSqlParameter("@BSD_QTY", SetDFooter.Rows(RowCount)("BSD_QTY"), SqlDbType.Decimal)
                iParms(5) = Mainclass.CreateSqlParameter("@BSD_bLOCKQTY", SetDFooter.Rows(RowCount)("BSD_bLOCKQTY"), SqlDbType.Bit)

                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "[dbo].[SAVE_BOOK_SET_D]", iParms)
                If RetValFooter = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar2.ShowNotification("Cannot create/edit a Set with No Item selection", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            If h_SetItemGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_SetItemGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@BSD_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@BSD_BSH_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from [dbo].[BOOK_SET_D] where BSD_ID=@BSD_ID and BSD_BSH_ID=@BSD_BSH_ID", iParms)
                    If RetValFooter = "-1" Then
                        'lblError.Text = "Unexpected Error !!!"
                        usrMessageBar2.ShowNotification("Unexpected Error", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"), False)
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'lblError.Text = ""
        If Seh_Id.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update [BOOK_SET_H] set [BSH_DELETED]=1, [BSH_DELETED_USER]=@BSH_USER, [BSH_DELETED_DATE]=getdate() where BSH_ID=@BSH_ID"
                Dim pParms(2) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@BSH_ID", Seh_Id.Value, SqlDbType.Int, True)
                pParms(2) = Mainclass.CreateSqlParameter("@BSH_USER", Session("sUsr_name"), SqlDbType.VarChar)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                'lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                usrMessageBar2.ShowNotification(ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", ""), UserControls_usrMessageBar.WarningType.Danger)
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, Seh_Id.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

End Class
