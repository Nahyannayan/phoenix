﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Inventory_BookSale_BS_Collection_Message_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "BS_Collection_Message.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

            Page.Title = OASISConstants.Gemstitle

            Try
                ViewState("MainMnu_code") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "PI01143" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    'Select Case ViewState("MainMnu_code").ToString
                    '    Case OASISConstants.MNU_FEE_COLLECTION

                    '        ViewState("trantype") = "A"
                    'End Select

                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False
        'divAge.Style.Add("display", "none")
    End Sub
    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divAge.Visible = False
    End Sub
    Protected Sub gvDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDetails.RowEditing

    End Sub
    Protected Sub gvDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails.RowCommand
        Try
            Dim lblBCM_ID As New Label
            Dim lblFromDate As New Label
            Dim lblToDate As Label
            Dim lblMessage As Label
            Dim lblMessage1 As Label
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvDetails.Rows(index), GridViewRow)
            lblBCM_ID = selectedRow.FindControl("lblBCM_ID")
            lblFromDate = selectedRow.FindControl("lblFromDate")
            lblToDate = selectedRow.FindControl("lblToDate")
            lblMessage = selectedRow.FindControl("lblMessage")
            lblMessage1 = selectedRow.FindControl("lblMessage1")

            If e.CommandName = "edit" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
                Dim str_query As String = " SELECT ISNULL(BCM_COLP,0) BCM_COLP,ISNULL(BCM_COLC,0) BCM_COLC,ISNULL(BCM_COLD,0) BCM_COLD FROM [dbo].[BOOK_COLLECT_MESSAGE_M] WHERE BCM_ID= " & lblBCM_ID.Text.ToString
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    ID_COLP.Checked = ds.Tables(0).Rows(0)("BCM_COLP")
                    ID_COLC.Checked = ds.Tables(0).Rows(0)("BCM_COLC")
                    ID_COLD.Checked = ds.Tables(0).Rows(0)("BCM_COLD")
                End If
                divAge.Visible = True
                txtfromdate.Text = lblFromDate.Text
                txttodate.Text = lblToDate.Text
                txtMessage.Text = lblMessage1.Text
            End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gridbind()


        Dim str_Filter As String = "", FCL_bDeleted As String = "", FCL_ACD_ID As Integer = 0

        Dim lstrCondn1, lstrCondn2, lstrCondn3 As String

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim CurBsUnit As String = Session("sBsuid")
        lstrCondn1 = ""
        lstrCondn2 = ""
        lstrCondn3 = ""

        str_Filter = ""

        If gvDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   txtReceiptno FCL_RECNO
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtFromDate")
            lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BCM_FROM_DATE", lstrCondn1)

            '   -- 2  txtDate
            larrSearchOpr = h_selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtToDate")
            lstrCondn2 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BCM_TO_DATE", lstrCondn2)

            '   -- 3  txtGrade
            larrSearchOpr = h_selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtMessage")
            lstrCondn3 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BCM_MESSAGE", lstrCondn3)


        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim StrSQL As String = "select BCM_ID, BCM_FROM_DATE ,BCM_TO_DATE , substring(BCM_MESSAGE,0,150)+'....' as BCM_MESSAGE,BCM_MESSAGE AS MESSAGE from [dbo].[BOOK_COLLECT_MESSAGE_M] where [BCM_bDELETE]=0 AND [BCM_BSU_ID]='" & Session("sBsuid") & "'" & str_Filter & " ORDER BY  BCM_ID DESC"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)
        gvDetails.DataSource = ds
        gvDetails.DataBind()


        If gvDetails.Rows.Count > 0 Then

            txtSearch = gvDetails.HeaderRow.FindControl("txtFromDate")
            txtSearch.Text = lstrCondn1

            txtSearch = gvDetails.HeaderRow.FindControl("txtToDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvDetails.HeaderRow.FindControl("txtMessage")
            txtSearch.Text = lstrCondn3


        End If
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
