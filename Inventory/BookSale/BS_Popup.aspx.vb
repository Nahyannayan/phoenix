﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Inventory_BookSale_BS_Popup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = 0
        Response.Cache.SetNoStore()
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        If Page.IsPostBack = False Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            gridbind()
        End If
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvReceiptList.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvReceiptList.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvReceiptList.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvReceiptList.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Select Case Request.QueryString("TYPE")
            Case "ITEMDELIVERY"
                GridBindItemDelivery()
            Case "RETURNSALES"
                GridBindReturnSales()

        End Select
    End Sub

    Sub GridBindItemDelivery()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvReceiptList.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvReceiptList.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BSAH_NO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvReceiptList.HeaderRow.FindControl("txtStudentName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BSAH_CUST_NAME", str_Sid_search(0), str_txtName)
            End If
            str_query_header = "SELECT DISTINCT BSAH_ID AS BSAH_ID, BSAH_NO AS ReceiptNo, BSAH_CUST_NAME AS StudentName FROM    [BOOK_SALE_D] WITH (NOLOCK) INNER JOIN [BOOK_SALE_H] WITH (NOLOCK) ON [BOOK_SALE_D].[BSAD_BSAH_ID]=[BOOK_SALE_H].BSAH_ID" &
                               " WHERE  BSAH_BSU_ID='" & Session("sBsuid") & "' AND ISNULL(BSAH_DELETED,0)=0 AND  [BSAD_bDELIVERED]=0 AND  BSAH_TYPE='S' AND 1=1 " _
                   & str_filter_code & str_filter_name _
                   & " ORDER BY BSAH_CUST_NAME"
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvReceiptList.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvReceiptList.DataBind()
                Dim columnCount As Integer = gvReceiptList.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvReceiptList.Rows(0).Cells.Clear()
                gvReceiptList.Rows(0).Cells.Add(New TableCell)
                gvReceiptList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReceiptList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReceiptList.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvReceiptList.DataBind()
            End If
            txtSearch = gvReceiptList.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvReceiptList.HeaderRow.FindControl("txtStudentName")
            txtSearch.Text = str_txtName

            lblheader = gvReceiptList.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvReceiptList.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindReturnSales()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvReceiptList.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvReceiptList.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BSAH_NO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvReceiptList.HeaderRow.FindControl("txtStudentName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BSAH_CUST_NAME", str_Sid_search(0), str_txtName)
            End If
            'str_query_header = "SELECT DISTINCT BSAH_ID AS BSAH_ID, BSAH_NO AS ReceiptNo, BSAH_CUST_NAME AS StudentName FROM    [BOOK_SALE_D] WITH (NOLOCK) INNER JOIN [BOOK_SALE_H] WITH (NOLOCK) ON [BOOK_SALE_D].[BSAD_BSAH_ID]=[BOOK_SALE_H].BSAH_ID" &
            '                    " WHERE  BSAH_BSU_ID='131001' AND ISNULL(BSAH_DELETED,0)=0 AND  [BSAD_bDELIVERED]=0 AND  BSAH_TYPE='S' AND 1=1 " _
            '        & str_filter_code & str_filter_name _
            '        & " ORDER BY BSAH_CUST_NAME"


            str_query_header = " SELECT a.BSAH_ID AS BSAH_ID, a.BSAH_NO AS ReceiptNo ,a.BSAH_CUST_NAME AS StudentName from (select BSAH_ID AS ID, BSAH_ID  ,BSAH_NO ,BSAH_SOURCE , " &
                            " BSAH_DATE ,BSAH_CUST_NO ,BSAH_CUST_NAME  ,(BSAH_GRADE+'-'+BSAH_SECTION) AS BSAH_GRADE , BSAH_NET_AMOUNT, " &
                            " ISNULL(BSAH_INVOICE_NO,'') BSAH_INVOICE_NO from [dbo].[BOOK_SALE_H] where [BSAH_BSU_ID]= '" & Session("sBsuid") & "'" &
                            " AND ISNULL(BSAH_DELETED,0)=0 AND BSAH_TYPE='S' " & str_filter_code & str_filter_name & " ) a LEFT JOIN [dbo].[BOOK_SALE_H] b ON a.BSAH_NO=b.BSAH_OLDSAL_NO WHERE 1 = 1 " &
                            " GROUP BY a.ID,a.BSAH_ID,a.BSAH_NO ,a.BSAH_SOURCE , a.BSAH_DATE ,a.BSAH_CUST_NO ,a.BSAH_CUST_NAME,a.BSAH_GRADE , " &
                            " a.BSAH_NET_AMOUNT, a.BSAH_INVOICE_NO   HAVING sum(ISNULL(b.BSAH_NET_AMOUNT, 0)) <> a.BSAH_NET_AMOUNT ORDER BY  a.BSAH_CUST_NAME DESC"


            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvReceiptList.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvReceiptList.DataBind()
                Dim columnCount As Integer = gvReceiptList.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvReceiptList.Rows(0).Cells.Clear()
                gvReceiptList.Rows(0).Cells.Add(New TableCell)
                gvReceiptList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReceiptList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReceiptList.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvReceiptList.DataBind()
            End If
            txtSearch = gvReceiptList.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvReceiptList.HeaderRow.FindControl("txtStudentName")
            txtSearch.Text = str_txtName

            lblheader = gvReceiptList.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvReceiptList.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Protected Sub gvReceiptList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReceiptList.PageIndexChanging
        gvReceiptList.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    'Protected Sub linklblReceiptNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lblid As New Label
    '    Dim lnkCode As New LinkButton
    '    lnkCode = sender
    '    lblid = sender.Parent.FindControl("lblid")
    '    If (Not lblid Is Nothing) Then

    '        '   Response.Write(lblcode.Text)
    '        'Response.Write("<script language='javascript'> function listen_window(){")
    '        'Response.Write("window.returnValue = '" & lblid.Text.Replace("'", "\'") & "___" & lnkDESCR.Text.Replace("'", "\'") & "';")
    '        'Response.Write("window.close();")
    '        'Response.Write("} </script>")
    '        'lblid.Text = lblid.Text.Replace("___", "||")
    '        'Response.Write("<script language='javascript'> function listen_window(){")
    '        'Response.Write(" var oArg = new Object();")
    '        'Response.Write("oArg.NameCode = '" & lblid.Text.Replace("'", "\'") & "||" & lnkCode.Text.Replace("'", "\'") & "';")
    '        'Response.Write("var oWnd = GetRadWindow('" & lblid.Text.Replace("'", "\'") & "||" & lnkCode.Text.Replace("'", "\'") & "');")
    '        'Response.Write("oWnd.close(oArg);")
    '        'Response.Write("} </script>")

    '        h_SelectedId.Value = "Close"
    '    End If
    'End Sub
    Protected Sub gvReceiptList_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvReceiptList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblid As Label = DirectCast(e.Row.FindControl("lblid"), Label)
            Dim lnkCode As LinkButton = DirectCast(e.Row.FindControl("lnkCode"), LinkButton)

            If Not lnkCode Is Nothing AndAlso Not lblid Is Nothing Then
                hf_id.Value = lblid.Text.Replace("'", "\'")
                hf_code.Value = lnkCode.Text.Replace("'", "\'")
                hf_code.Value = lblid.Text.Replace("'", "\'") & "||" & lnkCode.Text.Replace("'", "\'") & ""
                'hf_code.Value = Request.QueryString("ctrl")
                If Request.QueryString("TYPE") = "ITEMDELIVERY" Then
                    lnkCode.Attributes.Add("onClick", "return SetValuetoItemDelivery('" & hf_code.Value & "','" & hf_id.Value & "');")
                ElseIf Request.QueryString("TYPE") = "RETURNSALES" Then
                    lnkCode.Attributes.Add("onClick", "return SetValuetoReturnSales('" & hf_code.Value & "');")

                End If

            End If
        End If
    End Sub
End Class
