<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ItemCategory.aspx.vb" Inherits="Inventory_Category" title="Category Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
}

function getPurchaseName() {
    var sFeatures;
    var lstrVal;
    var lintScrVal;
    var pMode;
    var NameandCode;
    sFeatures = "dialogWidth: 760px; ";
    sFeatures += "dialogHeight: 420px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    pMode = "ITMCLIENT"
    url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=P";
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) return false;
    NameandCode = result.split('___');
    document.getElementById("<%=txtPurchase.ClientID %>").value = NameandCode[2];
    document.getElementById("<%=h_Purchase_ID.ClientID %>").value = NameandCode[1];
}

function getBudgetName() {
    var sFeatures;
    var lstrVal;
    var lintScrVal;
    var pMode;
    var NameandCode;
    sFeatures = "dialogWidth: 760px; ";
    sFeatures += "dialogHeight: 420px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    pMode = "TPTBUDGET"
    url = "../common/PopupSelect.aspx?id=" + pMode;
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) return false;
    NameandCode = result.split('___');
    document.getElementById("<%=h_Budget_ID.ClientID %>").value = NameandCode[0];
    document.getElementById("<%=txtPurchase.ClientID %>").value = NameandCode[1];
    document.getElementById("<%=txtBudget.ClientID %>").value = NameandCode[2];
    document.getElementById("<%=txtSubBudget.ClientID %>").value = NameandCode[3];
    document.getElementById("<%=h_Purchase_ID.ClientID %>").value = NameandCode[4];
}
   
</script>
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 60%">
        <tr>
            <td align="left" width="100%">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                &nbsp;&nbsp;
                </td>
        </tr>
        <tr>
            <td class="matters" style="height: 54px; font-weight: bold;" valign="top">
                <table align="center" border="1" width="100%" class="BlueTableView" bordercolor="#1b80b6" cellpadding="5" cellspacing="0">
                    <tr class="subheader_img">
                        <td align="left" colspan="3" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                            <span style="font-family: Verdana">
                                Category Master</span></font></td>
                    </tr>                                                         
                    <tr>
                        <td class="matters">Category Type<span style="color: #800000">*</span></td>
                        <td class="matters" width="2%" style="height: 27px" >:</td>
                        <td class="matters" align="left"><asp:DropDownList ID="ddlCategoryType" runat="server"></asp:DropDownList></td></tr>
                    <tr>
                        <td align="left" class="matters" width="25%" style="height: 27px" >
                            Category Description</td>
                        <td class="matters" width="2%" style="height: 27px" >:</td>
                        <td align="left" class="matters" style="width: 70%; height: 27px;" >
                               <asp:TextBox ID="txtPriDescr" runat="server" TabIndex="1" MaxLength="100" 
                                   Width="188px"></asp:TextBox></td></tr>
                    <tr>
                        <td class="matters">Purchase Account</td>
                        <td class="matters" width="2%" style="height: 27px" >:</td>
                        <td class="matters" align="left"><asp:TextBox ID="txtPurchase" runat="server" Width="300px" CssClass="inputbox"></asp:TextBox>
                        <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getPurchaseName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                        <asp:HiddenField ID="h_Purchase_ID" runat="server" /></td></tr>
                    <tr>
                        <td class="matters">Budget Link (Main Budget)</td>
                        <td class="matters" width="2%" style="height: 27px" >:</td>
                        <td class="matters" align="left"><asp:TextBox ID="txtBudget" runat="server" Width="300px" CssClass="inputbox"></asp:TextBox>
                        <asp:ImageButton ID="imgBudget" runat="server" OnClientClick="getBudgetName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                        <asp:HiddenField ID="h_Budget_ID" runat="server" /></td></tr>
                    <tr>
                        <td class="matters">Budget Link (Sub Budget)</td>
                        <td class="matters" width="2%" style="height: 27px" >:</td>
                        <td class="matters" align="left"><asp:TextBox ID="txtSubBudget" runat="server" Width="300px" Enabled="false" CssClass="inputbox"></asp:TextBox></td></tr>
                    <tr>
                        <td class="matters" colspan="3" style="height: 19px" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                            <asp:HiddenField ID="h_ICM_ID" runat="server" /></td></tr>
            </table>
        </td></tr>
    </table>    
</asp:Content>

