﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_Item
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSave)
        'ts
        Dim smScriptUpload As New ScriptManager
        smScriptUpload = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptUpload.RegisterPostBackControl(btnSave)
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI01080" And ViewState("MainMnu_code") <> "PI01085") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim dt As New DataTable, sqlStr As String
            sqlStr = "select ICM_DESCR, ISC_DESCR, MKE_DESCR, MDL_DESCR, ITEM.* from ITEM "
            sqlStr &= "inner JOIN ITEM_CATEGORY_M on ITM_ICM_ID=ICM_ID INNER JOIN ITEM_SUBCATEGORY_M on ITM_ISC_ID=ISC_ID "
            sqlStr &= "INNER JOIN ITEM_MAKE on ITM_MKE_ID=MKE_ID INNER JOIN ITEM_MODEL on ITM_MDL_ID=MDL_ID "
            sqlStr &= "where ITM_ID=" & p_Modifyid

            sqlStr = "select USG_DESCR, USF_DESCR, UCL_DESCR, UCO_DESCR,isnull(ITM_IMAGE,'') ITM_IMAGE, ITEM.* FROM ITEM "
            sqlStr &= "LEFT OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE LEFT OUTER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
            sqlStr &= "LEFT OUTER JOIN UNSPSC_FAMILY on UNSPSC_COMMODITY.USF_ID=UNSPSC_FAMILY.USF_ID "
            sqlStr &= "LEFT OUTER JOIN UNSPSC_CLASS on UNSPSC_CLASS.UCL_ID=UNSPSC_COMMODITY.UCL_ID "
            sqlStr &= "where ITM_ID=" & p_Modifyid
            dt = MainObj.getRecords(sqlStr, "OASIS_PUR_INVConnectionString")
            If dt.Rows.Count > 0 Then
                h_ITM_ID.Value = dt.Rows(0)("ITM_ID").ToString
                txtSegment.Text = dt.Rows(0)("USG_DESCR").ToString
                txtFamily.Text = dt.Rows(0)("USF_DESCR").ToString
                txtClass.Text = dt.Rows(0)("UCL_DESCR").ToString
                txtCommodity.Text = dt.Rows(0)("UCO_DESCR").ToString

                'txtICMDescr.Text = dt.Rows(0)("ICM_DESCR").ToString
                'txtISCDescr.Text = dt.Rows(0)("ISC_DESCR").ToString
                'txtMKEDescr.Text = dt.Rows(0)("MKE_DESCR").ToString
                'txtMDLDescr.Text = dt.Rows(0)("MDL_DESCR").ToString
                txtDESCR.Text = dt.Rows(0)("ITM_DESCR").ToString
                txtDetails.Text = dt.Rows(0)("ITM_DETAILS").ToString
                txtUnit.Text = dt.Rows(0)("ITM_UNIT").ToString
                txtPacking.Text = dt.Rows(0)("ITM_PACKING").ToString
                txtRate.Text = dt.Rows(0)("ITM_RATE").ToString
                setImageDisplay(imgAsset, dt.Rows(0)("ITM_IMAGE"))

                hICMID.Value = dt.Rows(0)("ITM_ICM_ID").ToString
                hISCID.Value = dt.Rows(0)("ITM_ISC_ID").ToString
                hMKEID.Value = dt.Rows(0)("ITM_MKE_ID").ToString
                hMDLID.Value = dt.Rows(0)("ITM_MDL_ID").ToString
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        txtDESCR.Enabled = Not mDisable
        txtDetails.Enabled = Not mDisable
        txtICMDescr.Enabled = Not mDisable
        txtCommodity.Enabled = Not mDisable
        txtISCDescr.Enabled = Not mDisable
        txtMKEDescr.Enabled = Not mDisable
        txtMDLDescr.Enabled = Not mDisable
        txtPacking.Enabled = Not mDisable
        txtRate.Enabled = Not mDisable
        txtUnit.Enabled = Not mDisable
        UploadDocPhoto.Enabled = Not mDisable

        imgICMDescr.Enabled = Not mDisable
        imgISCDescr.Enabled = Not mDisable
        imgMDLDescr.Enabled = Not mDisable
        imgMKEDescr.Enabled = Not mDisable
        imgCommodity.Enabled = Not mDisable
    End Sub

    Sub clear_All()
        h_ITM_ID.Value = 0
        txtDESCR.Text = ""
        txtDetails.Text = ""
        txtICMDescr.Text = ""
        txtISCDescr.Text = ""
        txtMDLDescr.Text = ""
        txtMKEDescr.Text = ""
        txtPacking.Text = ""
        txtRate.Text = "0.00"
        txtUnit.Text = ""

        txtCommodity.Text = ""
        txtSegment.Text = ""
        txtFamily.Text = ""
        txtClass.Text = ""

        hICMID.Value = 0
        hISCID.Value = 0
        hMKEID.Value = 0
        hMDLID.Value = 0
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from PRF_D where PRD_ITM_ID=" & h_ITM_ID.Value) = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from ITEM where ITM_ID=" & h_ITM_ID.Value)
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                'lblError.Text = "unable to delete, " & txtDESCR.Text & " used in transactions"
                usrMessageBar.ShowNotification("unable to delete, " & txtDESCR.Text & " used in transactions", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            If txtDESCR.Text.Trim = "" Then
                'lblError.Text = "Item description cannot be blank"
                usrMessageBar.ShowNotification("Item description cannot be blank", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            If UploadDocPhoto.FileName <> "" Then
                Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder")
                Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName

                Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
                'If hdnAssetImagePath.Value <> "" Then
                If Not IO.File.Exists(strFilepath) Then
                    'lblError.Text = "Invalid FilePath!!!!"
                    usrMessageBar.ShowNotification("Invalid FilePath!!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                Else
                    ViewState("imgAsset") = Mainclass.ConvertImageFiletoBytes(strFilepath)
                    IO.File.Delete(strFilepath)
                End If
            ElseIf Not ViewState("imgAsset") Is Nothing Then

            Else
                ViewState("imgAsset") = Nothing
            End If

            'ITM_ID, ITM_ICM_ID, ITM_ISC_ID, ITM_MKE_ID, ITM_MDL_ID, ITM_DESCR, ITM_DETAILS, ITM_UNIT, ITM_PACKING, ITM_RATE
            Dim pParms(11) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@ITM_ID", h_ITM_ID.Value, SqlDbType.Int, True)
            'pParms(2) = Mainclass.CreateSqlParameter("@ITM_ICM_ID", hICMID.Value, SqlDbType.Int)
            'pParms(3) = Mainclass.CreateSqlParameter("@ITM_ISC_ID", hISCID.Value, SqlDbType.Int)
            'pParms(4) = Mainclass.CreateSqlParameter("@ITM_MKE_ID", hMKEID.Value, SqlDbType.Int)
            'pParms(5) = Mainclass.CreateSqlParameter("@ITM_MDL_ID", hMDLID.Value, SqlDbType.Int)
            pParms(2) = Mainclass.CreateSqlParameter("@ITM_UNSPSC", hdnCommodity.Value, SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@ITM_DESCR", txtDESCR.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@ITM_DETAILS", txtDetails.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@ITM_UNIT", txtUnit.Text, SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@ITM_PACKING", txtPacking.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@ITM_RATE", txtRate.Text, SqlDbType.Decimal)
            pParms(11) = Mainclass.CreateSqlParameter("@ITM_IMAGE", ViewState("imgAsset"), SqlDbType.Image)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveItem", pParms)
                If RetVal = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(1).Value
                    usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    'lblError.Text = "Data Saved Successfully !!!"
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub setImageDisplay(ByVal imgMap As Object, ByVal PhotoBytes As Byte(), Optional ByVal ID As String = "")
        Try
            Dim OC As System.Drawing.Image ' = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            If Not PhotoBytes Is Nothing Then
                ViewState("imgAsset") = PhotoBytes
                OC = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            Else
                OC = System.Drawing.Image.FromFile(Server.MapPath("~/Images/Photos/no_image.gif"))
            End If
            If OC Is Nothing Then Exit Sub
            Dim nameOfImage As String = Session.SessionID & Replace(Now.ToString, ":", "") & ID & "CurrentImage"
            If Not Cache.Get(nameOfImage) Is Nothing Then
                Cache.Remove(nameOfImage)
            End If
            Cache.Insert(nameOfImage, OC, Nothing, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Default, Nothing)
            imgMap.ImageUrl = "../Common/ShowImage.aspx?ID=" & nameOfImage
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

End Class



