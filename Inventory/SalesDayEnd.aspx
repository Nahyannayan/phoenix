<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="SalesDayEnd.aspx.vb" Inherits="Inventory_SalesDayEnd" Title="Priority" %>

<%@ Register Src="../UserControls/usrMessageBar.ascx" TagName="usrMessageBar" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script lang="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Day End Posting
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" style="width: 100%;">
                    <tr>
                        <td align="left" style="width: 100%;">
                            <uc1:usrMessageBar ID="usrMessageBar1" runat="server" />
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left" class="field-value">
                                        <asp:GridView ID="gvDayEnd" runat="server"
                                            EmptyDataText="No Data" Width="100%" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="sap_date" HeaderText="Date" />
                                                <asp:BoundField DataField="sap_type" HeaderText="Type" />
                                                <asp:BoundField DataField="TaxCode" HeaderText="TaxCode" />
                                                <asp:BoundField DataField="sap_Amount" HeaderText="Amount" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center">
                            <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button"
                                Text="Post" TabIndex="5" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Do you want to Continue?" TargetControlID="btnPost">
                            </ajaxToolkit:ConfirmButtonExtender>
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

