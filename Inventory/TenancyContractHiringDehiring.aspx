<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TenancyContractHiringDehiring.aspx.vb" Inherits="Inventory_TenancyContractHiringDehiring" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }


        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }


        function getEmployee(Employee, designation, Empid, Bsu, status, dependants, desid) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            var bsuSelected;

            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "TENANCYEMPLOYEE"
            var Lbsuid = '<%= Session("sBSUID") %>';
            bsuSelected = document.getElementById("<%=h_bsu_id.ClientID %>").value;

            url = "../common/PopupSelect.aspx?id=" + pMode + "&SBsuid=" + bsuSelected + "&LBsuid=" + Lbsuid;

            document.getElementById('<%=hf_txtEmployee.ClientID%>').value = Employee;
            document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value = designation;
            document.getElementById('<%=hf_h_Emp_Id.ClientID%>').value = Empid;
            document.getElementById('<%=hf_txtDEPENDANTS.ClientID%>').value = dependants;
            document.getElementById('<%=hf_h_Des_Id.ClientID%>').value = desid;


            var oWnd = radopen(url, "pop_rel");
            //result = window.showModalDialog(url, "", sFeatures);
            //if (result == '' || result == undefined) {
            //    return false;
            //}

        }


        function getDesignation(designation, Desid) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            var bsuSelected;

            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "TENANCYDESIGNATION"
            var Lbsuid = '<%= Session("sBSUID") %>';

            bsuSelected = document.getElementById("<%=h_bsu_id.ClientID %>").value;

            url = "../common/PopupSelect.aspx?id=" + pMode + "&SBsuid=" + bsuSelected + "&LBsuid=" + Lbsuid;

            var oWnd = radopen(url, "pop_desig");
            //result = window.showModalDialog(url, "", sFeatures);
            //if (result == '' || result == undefined) {
            //    return false;
            //}

        }



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                if (document.getElementById(document.getElementById('<%=hf_txtEmployee.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtEmployee.ClientID%>').value).value = NameandCode[1];
        if (document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value).value = NameandCode[2];
        if (document.getElementById(document.getElementById('<%=hf_h_Emp_Id.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_h_Emp_Id.ClientID%>').value).value = NameandCode[0];
        if (document.getElementById(document.getElementById('<%=hf_txtDEPENDANTS.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtDEPENDANTS.ClientID%>').value).value = NameandCode[4];
        if (document.getElementById(document.getElementById('<%=hf_h_Des_Id.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_h_Des_Id.ClientID%>').value).value = NameandCode[5];

        document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;


    }
}
function OndesigClientClose(oWnd, args) {

    var NameandCode;
    var arg = args.get_argument();

    if (arg) {
        NameandCode = arg.NameandCode.split('||');
        //document.getElementById(Desid).value = NameandCode[0];
        //document.getElementById(designation).value = NameandCode[1];
        if (document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value).value = NameandCode[1];
                if (document.getElementById(document.getElementById('<%=hf_h_Des_Id.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_h_Des_Id.ClientID%>').value).value = NameandCode[0];

                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;

            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Tenancy Contract Hiring DeHiring"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
                    ReloadOnShow="true" runat="server" EnableShadow="true">
                    <Windows>
                        <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>
                    <Windows>
                        <telerik:RadWindow ID="pop_desig" runat="server" Behaviors="Close,Move" OnClientClose="OndesigClientClose"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>

                </telerik:RadWindowManager>


                <table id="tbl_AddGroup" runat="server" align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                                <%--<tr class="subheader_img">
                                    <td align="left" colspan="11" valign="middle">
                                        </td>
                                </tr>--%>
                                <tr id="rptServices" runat="server">
                                    <td colspan="6" valign="middle">
                                        <div class="checkbox-list">
                                            <asp:DataList ID="RepService" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblRESDescr" runat="server" Style="text-align: right" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRESAmount" runat="server" Style="text-align: right" Width="100px" Text='<%# Bind("Value") %>' Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="title-bg-lite" runat="server" id="trOld" visible="false">
                                    <td align="left" colspan="6">
                                        <asp:Label runat="server" ID="Label1" Text="Old Details" CssClass="field-label"></asp:Label></td>
                                </tr>
                                <tr id="trEmpOldDetails" runat="server" visible="false">

                                    <td colspan="6" align="left">
                                        <asp:GridView ID="grdOldEmpDetails" runat="server" AutoGenerateColumns="False" PageSize="5"
                                            Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                            DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Designation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Width="170px" Text='<%# Bind("TCE_MSTATUS") %>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dependants">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDependants" runat="server" Style="text-align: left" Text='<%# Bind("TCE_DEPENDANTS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unit Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnitType" runat="server" Style="text-align: right" Text='<%# Bind("UNITTYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unit No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnitNo" runat="server" Style="text-align: right" Text='<%# Bind("TCE_UNITNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Lease Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaseAmount" runat="server" Style="text-align: left" Text='<%# Bind("TCE_LAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Old Rent">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOldRent" runat="server" Style="text-align: left" Text='<%# Bind("TCE_OLDRENT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Ent.Unit Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEntUnitType" runat="server" Style="text-align: left" Text='<%# Bind("EUNITTYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Ent.Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEntAmount" runat="server" Style="text-align: left" Text='<%# Bind("TCE_EAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Ent.Deduction">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEntDed" runat="server" Style="text-align: left" Text='<%# Bind("TCE_DAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Furniture">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFurniture" runat="server" Style="text-align: left" Text='<%# Bind("FURN_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>


                                <tr id="TrVacatingDate" runat="server" visible="false">
                                    <td colspan="6" align="left" width="100%">

                                        <table width="100%">

                                            <tr>

                                                <td align="left" width="20%">
                                                    <asp:Label ID="Label3" runat="server" Text="Vacating Date" CssClass="field-label"></asp:Label></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtVacatingDate" runat="server" Enabled="false"></asp:TextBox></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtContractEndDate" Visible="false" runat="server" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCondate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        Style="cursor: hand"></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server"
                                                        TargetControlID="txtVacatingDate" PopupButtonID="imgCondate">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                        </table>



                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr class="title-bg-lite" id="trEmpDetailsHeading" runat="server">
                        <td align="left" colspan="6">
                            <asp:Label runat="server" ID="Label2" Text="Modified Details"></asp:Label></td>
                    </tr>


                    <tr id="trEmpDetails" runat="server">
                        <td align="left" colspan="6">
                            <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                                <tr>
                                    <td colspan="4" align="left" class="matters">
                                        <asp:GridView ID="grdEmpDetails" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="true"
                                            CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <th colspan="4">Employee Details</th>
                                                        <th colspan="4">Lease Details</th>
                                                        <th colspan="4">Entitlement Details</th>
                                                        <th colspan="3">Hiring/DeHiring Dates</th>
                                                        <tr class="title-bg-lite">
                                                            <th>DeHiring</th>
                                                            <th>Employee Name</th>
                                                            <th>Designation</th>
                                                            <th>Status</th>
                                                            <th>Dependants</th>
                                                            <th>Type</th>
                                                            <th>Unit No.</th>
                                                            <th>Amount</th>
                                                            <th>Old Rent</th>
                                                            <th>Type</th>
                                                            <th>Amount</th>
                                                            <th>Deduction</th>
                                                            <th>Furniture</th>
                                                            <th>DeHiring Date</th>
                                                            <th colspan="2">Hiring Date</th>
                                                            </th>
                                                        </tr>
                                                    </HeaderTemplate>

                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server" Visible="false"></asp:CheckBox>
                                                        <asp:Label ID="lblTDH_ID" runat="server" Visible="false" Text='<%# Bind("TDH_ID") %>'></asp:Label>
                                                        <asp:HiddenField ID="h_Deleted" runat="server" Value='<%# Bind("TDH_DELETED") %>' />
                                                        <asp:HiddenField ID="h_Apr_Id" runat="server" Value='<%# Bind("TDH_APR_ID") %>' />
                                                    </ItemTemplate>

                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server" Visible="false"></asp:CheckBox>
                                                        <asp:Label ID="lblTDH_ID" runat="server" Visible="false" Text='<%# Bind("TDH_ID") %>'></asp:Label>

                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                    </FooterTemplate>
                                                </asp:TemplateField>





                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                                        <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />
                                                        <asp:HiddenField ID="h_Emp_Id1" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />


                                                        <asp:TextBox ID="txtEmployee" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />
                                                        <asp:TextBox ID="txtEmployee" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:Label>
                                                        <asp:HiddenField ID="h_Des_Id" runat="server" Value='<%# Bind("TDH_DES_ID") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgDesignation" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                        <asp:HiddenField ID="h_Des_Id" runat="server" Value='<%# Bind("TDH_DES_ID") %>' />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>

                                                        <asp:TextBox ID="txtDesignation" runat="server" Style="text-align: left" Text='<%# Bind("DESIGNATION") %>'></asp:TextBox>

                                                        <asp:ImageButton ID="imgDesignation" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                        <asp:HiddenField ID="h_Des_Id" runat="server" Value='<%# Bind("TDH_DES_ID") %>' />

                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMStatus" runat="server" Style="text-align: left" Text='<%# Bind("TDH_MSTATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:HiddenField ID="h_MStatus" runat="server" Value='<%# Bind("TDH_MSTATUS") %>' />
                                                        <asp:DropDownList ID="ddlMStatus" Enabled="false" runat="server" Width="90px"></asp:DropDownList>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:HiddenField ID="h_MStatus" runat="server" Value='<%# Bind("TDH_MSTATUS") %>' />
                                                        <asp:DropDownList ID="ddlMStatus" Enabled="false" runat="server" Width="90px"></asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDependants" runat="server" Text='<%# Bind("TDH_DEPENDANTS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDEPENDANTS" Enabled="false" runat="server" Text='<%# Bind("TDH_DEPENDANTS") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtDEPENDANTS" Enabled="false" runat="server" Style="text-align: left" Text='<%# Bind("TDH_DEPENDANTS") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaseType" runat="server" Text='<%# Bind("UNITTYPE") %>'></asp:Label>
                                                        <asp:HiddenField ID="h_REU_ID" runat="server" Value='<%# Bind("TDH_REU_ID") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:HiddenField ID="h_UnitType" runat="server" Value='<%# Bind("TDH_REU_ID") %>' />
                                                        <asp:DropDownList ID="ddlUnitType" Enabled="false" runat="server"></asp:DropDownList>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:HiddenField ID="h_UnitType" runat="server" Value='<%# Bind("TDH_REU_ID") %>' />
                                                        <asp:DropDownList ID="ddlUnitType" Enabled="true" runat="server"></asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnitNo" runat="server" Text='<%# Bind("TDH_UNITNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtUnitNo" Enabled="false" runat="server" Text='<%# Bind("TDH_UNITNO") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtUnitNo" Enabled="false" runat="server" Text='<%# Bind("TDH_UNITNO") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaseAmount" runat="server" Text='<%# Bind("TDH_LAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtLeaseAmount" Enabled="false" runat="server" Text='<%# Bind("TDH_LAMOUNT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtLeaseAmount" Enabled="false" runat="server" Text='<%# Bind("TDH_LAMOUNT") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOldRent" runat="server" Text='<%# Bind("TDH_OLDRENT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtOldRent" Enabled="false" runat="server" Text='<%# Bind("TDH_OLDRENT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtOldRent" Enabled="false" runat="server" Text='<%# Bind("TDH_OLDRENT") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaseEType" runat="server" Text='<%# Bind("EUNITTYPE") %>'></asp:Label>
                                                        <asp:HiddenField ID="h_EREU_ID" runat="server" Value='<%# Bind("TDH_EREU_ID") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:HiddenField ID="h_EUnitType" runat="server" Value='<%# Bind("TDH_EREU_ID") %>' />
                                                        <asp:DropDownList ID="ddlEUnitType" Enabled="true" runat="server"></asp:DropDownList>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:HiddenField ID="h_EUnitType" runat="server" Value='<%# Bind("TDH_EREU_ID") %>' />
                                                        <asp:DropDownList ID="ddlEUnitType" Enabled="true" runat="server"></asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOtherDetails" runat="server" Text='<%# Bind("TDH_EAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEAmount" Enabled="true" runat="server" Text='<%# Bind("TDH_EAMOUNT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtEAmount" Enabled="true" runat="server" Text='<%# Bind("TDH_EAMOUNT") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDetails" runat="server" Text='<%# Bind("TDH_DAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDeduction" Enabled="true" runat="server" Text='<%# Bind("TDH_DAMOUNT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtDeduction" Enabled="true" runat="server" Text='<%# Bind("TDH_DAMOUNT") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfurnished" runat="server" Text='<%# Bind("FURN_DESCR") %>'></asp:Label>
                                                        <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TDH_FURN_ID") %>' />
                                                        <asp:TextBox ID="txtEmployeeID" Visible="false" runat="server" Text='<%# Bind("TDH_EMP_ID") %>'></asp:TextBox>
                                                        <asp:Label ID="lblEmpChanged" runat="server" Visible="false" Text='<%# Bind("TDH_EMPLOYEE_CHANGED") %>'></asp:Label>
                                                        <asp:Label ID="lbl_TDH_APR_ID" runat="server" Visible="false" Text='<%# Bind("TDH_APR_ID") %>'></asp:Label>
                                                        <asp:HiddenField ID="h_TDH_TCE_ID" runat="server" Value='<%# Bind("TDH_TCE_ID") %>' />
                                                        <asp:HiddenField ID="h_TDH_TCH_ID" runat="server" Value='<%# Bind("TDH_TCH_ID") %>' />
                                                        <asp:HiddenField ID="h_TDH_BSU_ID" runat="server" Value='<%# Bind("TDH_BSU_ID") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TDH_FURN_ID") %>' />
                                                        <asp:DropDownList ID="ddlFurnish" Enabled="true" runat="server"></asp:DropDownList>
                                                        <asp:TextBox ID="txtEEmployeeID" Visible="false" runat="server" Text='<%# Bind("TDH_EMP_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TDH_FURN_ID") %>' />
                                                        <asp:DropDownList ID="ddlFurnish" Enabled="false" runat="server"></asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("TDH_DDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDDate" Enabled="false" runat="server" Style="text-align: right"
                                                            Text='<%# eval("TDH_DDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgEDDate" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtDDate" PopupButtonID="imgEdDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtDDate" runat="server" Enabled="false" Style="text-align: right"
                                                            Text='<%# eval("TDH_DDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgFDDate" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtDDate" PopupButtonID="imgFdDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("TDH_HDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtHDate" Enabled="false" runat="server" Style="text-align: right"
                                                            Text='<%# eval("TDH_HDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgHDate" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CEDDATE" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtHDate" PopupButtonID="imgHDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtHDate" runat="server" Enabled="false" Style="text-align: right"
                                                            Text='<%# eval("TDH_HDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgFHDate" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CEHDATE" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtHDate" PopupButtonID="imgFHDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <%--<tr>
               <td>
                  <table border="0" cellpadding="4" cellspacing="0" align="left" width="100%" style="border-right: #1b80b6 1pt solid; border-top: #1b80b6 1pt solid; border-left: #1b80b6 1pt solid; border-bottom: #1b80b6 1pt solid">
                     <tr>
                        <td colspan="4" align="left" class="matters">
                           <asp:GridView ID="grdEmpDetails" runat="server" AutoGenerateColumns="False"  PageSize="5" Width="100%" ShowFooter="true"
                              CaptionAlign="Top" SkinID="GridViewView" CssClass="BlueTable" DataKeyNames="ID">
                              <Columns>
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                        <th colspan="4">Employee Details</th><th colspan="4">Lease Details</th><th colspan="6">Entitlement Details</th>
                     <tr class="subheader_img">
                     <th>DeHiring</th><th>Employee Name</th><th>Designation</th><th>Status</th><th>Dependants</th>
                     <th>Type</th><th>Unit No.</th><th>Amount</th><th>Old Rent</th><th>Type</th><th>Amount</th><th>Deduction</th><th colspan="3">Furniture</th></tr>
                     </HeaderTemplate>
                     
                                        
                     
                     <ItemTemplate>
                     <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                     <asp:Label ID="lblTDH_ID" runat="server" Visible="false" Text='<%# Bind("TDH_ID") %>'></asp:Label>
                     <asp:HiddenField ID="h_Deleted" runat="server" Value='<%# Bind("TDH_DELETED") %>' />
                     <asp:HiddenField ID="h_Apr_Id" runat="server" Value='<%# Bind("TDH_APR_ID") %>' />
                     </ItemTemplate>
                                         
                     <EditItemTemplate>
                          <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                          <asp:Label ID="lblTDH_ID" runat="server" Visible="false" Text='<%# Bind("TDH_ID") %>'></asp:Label>
                                                  
                     </EditItemTemplate>
                     <FooterTemplate>
                     </FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblEmpName" Width="180px" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                     <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />
                     <asp:HiddenField ID="h_Emp_Id1" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />
                     </ItemTemplate>
                     <EditItemTemplate>
                     <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />
                     
                     
                     <asp:TextBox ID="txtEmployee" Width="150px"  runat="server" Text='<%# Bind("EMPNAME") %>'></asp:TextBox>
                     <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" /></EditItemTemplate>
                     <FooterTemplate>
                     <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TDH_EMP_ID") %>' />
                     <asp:TextBox ID="txtEmployee" runat="server"     Width ="150px" Text='<%# Bind("EMPNAME") %>'></asp:TextBox>
                     <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif"/></FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lbldesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:Label>
                     <asp:HiddenField ID="h_Des_Id" runat="server" Value='<%# Bind("TDH_DES_ID") %>' />    
                     </ItemTemplate>
                     <EditItemTemplate>
                     <asp:TextBox ID="txtDesignation" Width="120px" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:TextBox>
                     <asp:ImageButton ID="imgDesignation" runat="server" ImageUrl="~/Images/forum_search.gif" />
                     <asp:HiddenField ID="h_Des_Id" runat="server" Value='<%# Bind("TDH_DES_ID") %>' />
                     </EditItemTemplate>
                     <FooterTemplate>
                     <table >
                     <tr>
                     <td width="90%">
                     <asp:TextBox ID="txtDesignation" runat="server"  CssClass="inputbox_rtl"  Style="text-align: left" Width="120px" Text='<%# Bind("DESIGNATION") %>'></asp:TextBox>    
                     </td>
                     <td width="90%">
                     <asp:ImageButton ID="imgDesignation" runat="server" ImageUrl="~/Images/forum_search.gif"/>
                     <asp:HiddenField ID="h_Des_Id" runat="server" Value='<%# Bind("TDH_DES_ID") %>' />    
                     </td>
                     </tr>
                     </table>
                     </FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblMStatus" runat="server" Style="text-align: left" Text='<%# Bind("TDH_MSTATUS") %>'></asp:Label></ItemTemplate>
                     <EditItemTemplate>
                     <asp:HiddenField ID="h_MStatus" runat="server" Value='<%# Bind("TDH_MSTATUS") %>' />
                     <asp:DropDownList ID="ddlMStatus"  Enabled ="false"  runat="server" Width="90px"></asp:DropDownList>
                     </EditItemTemplate>
                     <FooterTemplate>
                     <asp:HiddenField ID="h_MStatus" runat="server" Value='<%# Bind("TDH_MSTATUS") %>' />
                     <asp:DropDownList ID="ddlMStatus" Enabled ="false" runat="server" Width="90px"></asp:DropDownList>
                     </FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblDependants" runat="server" Text='<%# Bind("TDH_DEPENDANTS") %>'></asp:Label></ItemTemplate>
                     <EditItemTemplate>
                     <asp:TextBox ID="txtDEPENDANTS" Enabled ="false" Width="80px" CssClass="inputbox_rtl"   runat="server" Text='<%# Bind("TDH_DEPENDANTS") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <FooterTemplate>
                     <asp:TextBox ID="txtDEPENDANTS" Enabled ="false" runat="server"  Style="text-align: left" Width="80px" Text='<%# Bind("TDH_DEPENDANTS") %>'></asp:TextBox>    
                     </FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblLeaseType" runat="server" Width="50px" Text='<%# Bind("UNITTYPE") %>'></asp:Label>
                     <asp:HiddenField ID="h_REU_ID"   runat="server" Value='<%# Bind("TDH_REU_ID") %>' />
                     </ItemTemplate>
                     <EditItemTemplate>
                     <asp:HiddenField ID="h_UnitType" runat="server" Value='<%# Bind("TDH_REU_ID") %>' />
                     <asp:DropDownList ID="ddlUnitType" Enabled ="false" runat="server" Width="75px"></asp:DropDownList></EditItemTemplate>
                     <FooterTemplate>
                     <asp:HiddenField ID="h_UnitType" runat="server" Value='<%# Bind("TDH_REU_ID") %>' />
                     <asp:DropDownList ID="ddlUnitType" Enabled ="true" runat="server" Width="75px"></asp:DropDownList></FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblLeaseAmount" runat="server"  Width="70px" Text='<%# Bind("TDH_LAMOUNT") %>'></asp:Label>
                     </ItemTemplate>
                     <EditItemTemplate>
                     <asp:TextBox ID="txtLeaseAmount" Enabled ="false" runat="server" Width="70px" Text='<%# Bind("TDH_LAMOUNT") %>'></asp:TextBox></EditItemTemplate>
                     <FooterTemplate>
                     <asp:TextBox ID="txtLeaseAmount" Enabled ="false" runat="server" CssClass="inputbox_rtl"   Width="70px" Text='<%# Bind("TDH_LAMOUNT") %>'></asp:TextBox></FooterTemplate></asp:TemplateField>
                     
                     
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblOldRent" runat="server" Width="50px" Text='<%# Bind("TDH_OLDRENT") %>'></asp:Label>
                     </ItemTemplate>
                     <EditItemTemplate>
                     <asp:TextBox ID="txtOldRent" Enabled ="false" runat="server" Width="70px" Text='<%# Bind("TDH_OLDRENT") %>'></asp:TextBox></EditItemTemplate>
                     <FooterTemplate>
                     <asp:TextBox ID="txtOldRent" Enabled ="false" runat="server" Width="70px" Text='<%# Bind("TDH_OLDRENT") %>'></asp:TextBox>
                     </FooterTemplate></asp:TemplateField>
                     
                     
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblLeaseEType" runat="server" Width="70px" Text='<%# Bind("EUNITTYPE") %>'></asp:Label>
                     <asp:HiddenField ID="h_EREU_ID" runat="server" Value='<%# Bind("TDH_EREU_ID") %>' />
                     </ItemTemplate>
                     <EditItemTemplate>
                     <asp:HiddenField ID="h_EUnitType" runat="server" Value='<%# Bind("TDH_EREU_ID") %>' />
                     <asp:DropDownList ID="ddlEUnitType" Enabled ="true" runat="server" Width="80px"></asp:DropDownList></EditItemTemplate>
                     <FooterTemplate>
                     <asp:HiddenField ID="h_EUnitType" runat="server" Value='<%# Bind("TDH_EREU_ID") %>' />
                     <asp:DropDownList ID="ddlEUnitType" Enabled ="true" runat="server" Width="80px"></asp:DropDownList></FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblOtherDetails"  runat="server"  Width="70px" Text='<%# Bind("TDH_EAMOUNT") %>'></asp:Label></ItemTemplate>
                     <EditItemTemplate>
                     <asp:TextBox ID="txtEAmount" Enabled ="true" runat="server"  Width="70px" Text='<%# Bind("TDH_EAMOUNT") %>'></asp:TextBox></EditItemTemplate>
                     <FooterTemplate>
                     <asp:TextBox ID="txtEAmount"  Enabled ="true" runat="server" CssClass="inputbox_rtl"   Width="70px" Text='<%# Bind("TDH_EAMOUNT") %>'></asp:TextBox></FooterTemplate></asp:TemplateField>
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblDetails" runat="server" Width="70px" Text='<%# Bind("TDH_DAMOUNT") %>'></asp:Label></ItemTemplate>
                     <EditItemTemplate>
                     <asp:TextBox ID="txtDeduction" Enabled ="true" runat="server"  Width="70px" Text='<%# Bind("TDH_DAMOUNT") %>'></asp:TextBox></EditItemTemplate>
                     <FooterTemplate>
                     <asp:TextBox ID="txtDeduction" Enabled ="true" runat="server" CssClass="inputbox_rtl"   Width="70px" Text='<%# Bind("TDH_DAMOUNT") %>'></asp:TextBox></FooterTemplate></asp:TemplateField>
                     
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:Label ID="lblfurnished" runat="server" Width="70px" Text='<%# Bind("FURN_DESCR") %>'></asp:Label>
                     <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TDH_FURN_ID") %>' />
                     </ItemTemplate>
                     <EditItemTemplate>
                     <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TDH_FURN_ID") %>' />
                     <asp:DropDownList ID="ddlFurnish" Enabled ="true" runat="server" Width="80px"></asp:DropDownList></EditItemTemplate>
                     <FooterTemplate>
                     <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TDH_FURN_ID") %>' />
                     <asp:DropDownList ID="ddlFurnish" Enabled ="false" runat="server" Width="80px"></asp:DropDownList></FooterTemplate></asp:TemplateField>
                     
                     <asp:TemplateField>
                     <ItemTemplate>
                     <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton></ItemTemplate>
                     <EditItemTemplate>
                     <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                     <asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton></EditItemTemplate>
                     <FooterTemplate>
                     <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton></FooterTemplate>
                     </asp:TemplateField>
                     
                     <asp:TemplateField>     
                     <ItemTemplate>
                     <asp:TextBox ID="txtEmployeeID" Visible ="false"  Width="150px"  runat="server" Text='<%# Bind("TDH_EMP_ID") %>'></asp:TextBox>
                     <asp:Label ID="lblEmpChanged" runat="server" Visible="false" Text='<%# Bind("TDH_EMPLOYEE_CHANGED") %>'></asp:Label>
                     <asp:Label ID="lbl_TDH_APR_ID" runat="server" Visible="false" Text='<%# Bind("TDH_APR_ID") %>'></asp:Label>
                     <asp:HiddenField ID="h_TDH_TCE_ID" runat="server" Value='<%# Bind("TDH_TCE_ID") %>' />
                     <asp:HiddenField ID="h_TDH_TCH_ID" runat="server" Value='<%# Bind("TDH_TCH_ID") %>' />
                     <asp:HiddenField ID="h_TDH_BSU_ID" runat="server" Value='<%# Bind("TDH_BSU_ID") %>' />
                     </ItemTemplate> 
                     <EditItemTemplate>
                     <asp:TextBox ID="txtEEmployeeID" Width="150px" Visible ="false"   runat="server" Text='<%# Bind("TDH_EMP_ID") %>'></asp:TextBox>                     
                     </EditItemTemplate> 
                     <FooterTemplate></FooterTemplate>
                     </asp:TemplateField>
                     
                     
                                      
                    </Columns></asp:GridView></td></tr>
                  </table>
               </td>
            </tr>--%>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnSend" runat="server" CausesValidation="False" CssClass="button" Text="Send" Visible="false" UseSubmitBehavior="False" TabIndex="31" />
                            <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="button" Text="Approve" Visible="false" UseSubmitBehavior="False" TabIndex="32" />
                            <asp:Button ID="btnReject" runat="server" CausesValidation="False" CssClass="button" Text="Reject" Visible="false" UseSubmitBehavior="False" TabIndex="33" />
                            <asp:HiddenField ID="hGridDelete" runat="server" />
                            <asp:HiddenField ID="h_JT_JHD_ID" runat="server" />
                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="h_bsu_id" runat="server" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />

                            <asp:HiddenField ID="hf_txtEmployee" runat="server" />
                            <asp:HiddenField ID="hf_txtDESIGNATION" runat="server" />
                            <asp:HiddenField ID="hf_h_Emp_Id" runat="server" />
                            <asp:HiddenField ID="hf_txtDEPENDANTS" runat="server" />
                            <asp:HiddenField ID="hf_h_Des_Id" runat="server" />

                            <asp:HiddenField ID="hf2_txtDESIGNATION" runat="server" />
                            <asp:HiddenField ID="hf2_h_Des_Id" runat="server" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
