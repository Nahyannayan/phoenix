Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Inventory_InvNewItemPopup
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Private Property NewItemToBeAdded() As DataTable
        Get
            Return ViewState("NewItemToBeAdded")
        End Get
        Set(ByVal value As DataTable)
            ViewState("NewItemToBeAdded") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                filldrp()
                If Session("NewItemToBeAdded") IsNot Nothing Then
                    NewItemToBeAdded = Session("NewItemToBeAdded")
                End If
                BindNewItemDescription()
            End If
            If h_SelectedId.Value <> "Close" Then
                Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)
                Response.Write("} </script>" & vbCrLf)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)
        drpObj.DataSource = MainObj.getRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub
    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "SELECT UOM_ID ,UOM_DESCR  FROM UOM_M "
            fillDropdown(ddlUOM, Query, "UOM_DESCR", "UOM_ID", "OASIS_PUR_INVConnectionString", True)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub BindNewItemDescription()
        Try
            h_ITMID.Value = 0
            txtItemDescr.Text = ""
            hdnISCID.Value = ""
            txtSubCategory.Text = ""
            chkIsInv.Checked = False
            chkIsIT.Checked = False
            ddlUOM.SelectedValue = Nothing
            txtRemarks.Text = ""
            If Request.QueryString("IsIT") = 1 Then
                chkIsIT.Checked = True
            Else
                chkIsIT.Checked = False
            End If
            Dim mRow() As DataRow
            mRow = NewItemToBeAdded.Select("PRD_ITM_ID='0'", "")
            If mRow.Length > 0 Then
                h_ItemID.Value = mRow(0)("ID")
                txtItemDescr.Text = mRow(0)("NEWITEMDESCR")
                Dim lstDrp As New ListItem
                lstDrp = ddlUOM.Items.FindByValue(mRow(0)("PRD_UOM_ID"))
                If Not lstDrp Is Nothing Then
                    ddlUOM.SelectedValue = lstDrp.Value
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Function SaveInventoryItemMaster() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim sqlParam(7) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ITM_ID", h_ITMID.Value, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ITM_DESCR", txtItemDescr.Text, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@ITM_ISC_ID", Val(hdnISCID.Value), SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ITM_bInventory", chkIsInv.Checked, SqlDbType.Bit)
            sqlParam(4) = Mainclass.CreateSqlParameter("@ITM_bITItem", chkIsIT.Checked, SqlDbType.Bit)
            sqlParam(5) = Mainclass.CreateSqlParameter("@ITM_UOM_ID", ddlUOM.SelectedValue, SqlDbType.Int)
            sqlParam(6) = Mainclass.CreateSqlParameter("@ITM_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[SaveInventoryItemMaster]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(7).Value = "" Then
                SaveInventoryItemMaster = ""
                h_ITMID.Value = sqlParam(0).Value
            Else
                SaveInventoryItemMaster = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(7).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim retval As String
            Dim new_elh_id As Integer = 0
            If Val(hdnISCID.Value) = "0" Then
                lblError.Text = "Invalid Sub Category !!!!"
                Exit Sub
            End If
            retval = SaveInventoryItemMaster()
            If (retval = "0" Or retval = "") Then
                Dim mRow As DataRow
                If NewItemToBeAdded.Select("ID=" & h_ItemID.Value, "").Length > 0 Then
                    mRow = NewItemToBeAdded.Select("ID=" & h_ItemID.Value, "")(0)
                    mRow("PRD_ITM_ID") = h_ITMID.Value
                    mRow("ITM_DESCR") = txtItemDescr.Text
                    mRow("NEWITEMDESCR") = ""
                End If
                If NewItemToBeAdded.Select("PRD_ITM_ID='0'", "").Length > 0 Then
                    BindNewItemDescription()
                    Exit Sub
                End If
                Session("NewItemToBeAdded") = NewItemToBeAdded
                lblError.Text = "Data Saved Successfully !!!"
            Else
                lblError.Text = IIf(IsNumeric(retval), getErrorMessage(retval), retval)
            End If
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("window.returnValue = '" & ViewState("RetValue") & "';")
            Response.Write("window.close();")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("window.returnValue = '" & ViewState("RetValue") & "';")
        Response.Write("window.close();")
        Response.Write("} </script>")
        h_SelectedId.Value = "Close"
    End Sub
End Class
