﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Buyer.aspx.vb" Inherits="Inventory_Buyer" Title="Buyer Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPageSS.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<%--    <script type="text/javascript" language="javascript">

        function getClientName(bsu_id, bsu_name, fdate, edate) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMCLIENT"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&JobType=Business Unit";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                return false;
            }
            NameandCode = result.split('___');
            if (document.getElementById(edate)) document.getElementById(edate).value = NameandCode[4];
            if (document.getElementById(fdate)) document.getElementById(fdate).value = NameandCode[3];
            if (document.getElementById(bsu_name)) document.getElementById(bsu_name).value = NameandCode[2];
            if (document.getElementById(bsu_id)) document.getElementById(bsu_id).value = NameandCode[1];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        }

        function getBsuNameNew(bsu) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "BSUUSER"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById("<%=txtBuyer.ClientID %>").value = NameandCode[1];
        document.getElementById("<%=h_BYR_ID.ClientID %>").value = NameandCode[0];
        document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
    }
    </script>--%>
    <script>
        function getClientNameNew(bsu_id, bsu_name, fdate, edate) {
            var pMode;
            var url;
            pMode = "ITMCLIENT"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&JobType=Business Unit";
            var oWnd = radopen(url, "pop_buyer");
            document.getElementById("<%=hfedate.ClientID%>").value = edate;
            document.getElementById("<%=hffdate.ClientID%>").value = fdate;
            document.getElementById("<%=hfbsu_name.ClientID%>").value = bsu_name;
            document.getElementById("<%=hfbsu_id.ClientID%>").value = bsu_id;
        }
        function getBsuNameNew(bsu) {
            var pMode;
            var url;
            pMode = "REAREA"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_bsu");
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hfedate.ClientID%>").value).value = NameandCode[4];
                document.getElementById(document.getElementById("<%=hffdate.ClientID%>").value).value = NameandCode[3];
                document.getElementById(document.getElementById("<%=hfbsu_name.ClientID%>").value).value = NameandCode[2];
                document.getElementById(document.getElementById("<%=hfbsu_id.ClientID%>").value).value = NameandCode[1];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        }
    }
    function OnClientClose1(oWnd, args) {
        var NameandCode;
        var arg = args.get_argument();
        if (arg) {

            NameandCode = arg.NameandCode.split('||');
            document.getElementById("<%=txtBuyer.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=h_BYR_ID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_buyer" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="pgTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblAddLedger" runat="server" style="width: 100%;">
                    <tr valign="bottom">
                        <td align="left" style="height: 19px" valign="bottom" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 166px" valign="top">
                            <table style="width: 100%">
                                <tr id="trSupplier" runat="server">
                                    <td class="matters" width="20%"><span class="field-label">Buyer<span style="color: #800000">*</span></span></td>
                                    <td class="matters" width="30%">
                                        <asp:TextBox ID="txtBuyer" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="imgBuyer" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="h_BYR_ID" runat="server" />
                                    </td>
                                    <td class="matters" width="20%"><span class="field-label">Enable</span></td>
                                    <td class="matters" width="30%">
                                        <asp:CheckBox ID="chkBYR_ACTIVE" Checked="true" runat="server" CssClass="inputbox"></asp:CheckBox></td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">Buyer Notes</span></td>
                                    <td class="matters" align="left" colspan="3">
                                        <asp:TextBox ID="txtBYR_Descr" runat="server" TextMode="MultiLine" Width="100%" SkinID="MultiText"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left" class="matters">
                                        <asp:GridView ID="grdBYR" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true"
                                            CaptionAlign="Top" PageSize="15" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBYD_ID" runat="server" Text='<%# Bind("BYD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBSU_NAME" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBSU_NAME" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                        <asp:HiddenField ID="hdnBSU_ID" Value='<%# Bind("BYD_BSU_ID") %>' runat="server" />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtBSU_NAME" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                        <asp:HiddenField ID="hdnBSU_ID" Value='<%# Bind("BYD_BSU_ID") %>' runat="server" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBYR_FDATE" runat="server" Text='<%# Bind("BYD_FDATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBYDFDate" runat="server" CssClass="inputbox" Text='<%# Bind("BYD_FDATE") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="lnkBYDFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtBYDFDate" PopupButtonID="lnkBYDFDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtBYDFDate" runat="server" CssClass="inputbox" Text='<%# Bind("BYD_FDATE") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="lnkBYDFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtBYDFDate" PopupButtonID="lnkBYDFDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBYR_EDATE" runat="server" Text='<%# Bind("BYD_EDATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBYDEDATE" runat="server" CssClass="inputbox" Text='<%# Bind("BYD_EDATE") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="lnkBYDEDATE" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtBYDEDATE" PopupButtonID="lnkBYDEDATE">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtBYDEDATE" runat="server" CssClass="inputbox" Text='<%# Bind("BYD_EDATE") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="lnkBYDEDATE" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtBYDEDATE" PopupButtonID="lnkBYDEDATE">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdatePRF" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelPRF" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddPRF" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditPRF" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Button ID="btnCopy" runat="server" CausesValidation="False" CssClass="button" Text="Copy" />
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_PRFGridDelete" Value="0" runat="server" />
                                        <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />

                                        <asp:HiddenField ID="hfbsu_id" runat="server" />
                                        <asp:HiddenField ID="hfbsu_name" runat="server" />
                                        <asp:HiddenField ID="hffdate" runat="server" />
                                        <asp:HiddenField ID="hfedate" runat="server" />
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
