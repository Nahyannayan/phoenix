<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Expense.aspx.vb" Inherits="Inventory_Expense" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function showDocument(id, filename, contenttype) {
            var sFeatures;
            //sFeatures = "dialogWidth: 920px; ";
            //sFeatures += "dialogHeight: 500px; ";
            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";
            //result = window.showModalDialog("IFrame.aspx?id=" + id + "&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            result = window.showModalDialog("IFrameNew.aspx?id=" + id + "&path=Advance&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            return false;
        }

        function TabChanged(sender, args) {
            sender.get_clientStateField().value =
        sender.saveClientState();
        }

        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
    1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

        function getExpenses(descr, ItmId) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "EXPENSES"

            url = "../common/PopupSelect.aspx?id=" + pMode;
            document.getElementById('<%=hf_products.ClientID%>').value = descr;
            document.getElementById('<%=hf_products_id.ClientID%>').value = ItmId;
            var oWnd = radopen(url, "pop_rel");
            //result = window.showModalDialog(url, "", sFeatures);

            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //document.getElementById(descr).value = NameandCode[1];
            //document.getElementById(ItmId).value = NameandCode[0];
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode[1]);
                document.getElementById(document.getElementById('<%=hf_products.ClientID%>').value).value = NameandCode[1];
                document.getElementById(document.getElementById('<%=hf_products_id.ClientID%>').value).value = NameandCode[0];
                //__doPostBack('<%= txtCommenttxt.ClientID%>', 'TextChanged');
              
            }

        }


        function getPayment() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            arf_id = document.getElementById("<%=h_EntryId.ClientID %>").value;
                pMode = "PAYMENT"
                url = "../common/PopupSelect.aspx?id=" + pMode + "&arf_id=" + arf_id;
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('||');
                document.getElementById("<%=txtPayment.ClientID %>").value = NameandCode[0];
            }

            function getEmpDepartment() {
                var sFeatures;
                var lstrVal;
                var lintScrVal;
                var pMode;
                var NameandCode;
                sFeatures = "dialogWidth: 760px; ";
                sFeatures += "dialogHeight: 420px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: yes; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                pMode = "EMPDEPT"
                url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=E";
                result = window.showModalDialog(url, "", sFeatures);
                if (result == '' || result == undefined) {
                    document.getElementById("<%=h_empID.ClientID %>").value = '';
                    document.getElementById("<%=txtEmployee.ClientID %>").value = '';
                    document.getElementById("<%=txtEmpDept.ClientID %>").value = '';

                    return false;
                }
                NameandCode = result.split('||');
                document.getElementById("<%=h_empID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtEmployee.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtEmpDept.ClientID %>").value = NameandCode[2];

                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }

            function Numeric_Only() {
                if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                    if (event.keyCode == 13 || event.keyCode == 46)
                    { return false; }
                    else if (event.keyCode == 45)
                    { return event.keyCode; }
                    event.keyCode = 0
                }
            }
    </script>

       <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
       </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  <asp:Label runat="server" ID="rptHeader" Text="Advances for Expenses /Activities"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                              
                                <tr id="ESROW" runat="server">
                                    <td align="left" width="20%"><span class="field-label">ES Number</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEsNo" Enabled="false" runat="server" MaxLength="100" ></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">ES Date </span><span class="text-danger">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtESDate" runat="server" > </asp:TextBox>
                                        <asp:ImageButton ID="imgESDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor:hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtESDate" PopupButtonID="imgESDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Advance Request Number</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtArNo" Enabled="false" runat="server" ></asp:TextBox>
                                        <asp:Label ID="lblPayment" Visible="false" Text="Payment No" runat="server" CssClass="field-label"></asp:Label>
                                        <asp:TextBox ID="txtPayment" Enabled="false" runat="server" Visible="false"></asp:TextBox>
                                        <asp:ImageButton ID="imgPayment" Visible="false" runat="server" OnClientClick="getPayment();return true;"
                                            ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                    <td align="left" width="20%"><span class="field-label">Advance Request Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTrDate" Enabled="false" runat="server" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Employee</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtEmployee" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getEmpDepartment();return true;"
                                            ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                    <td align="left" ><span class="field-label">Required On</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtARDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="ImgARDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor:hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtARDate" PopupButtonID="ImgARDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Employee Department</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtEmpDept" runat="server" Enabled="False"></asp:TextBox><br />
                                        <asp:RadioButton ID="radBudgeted" runat="server" GroupName="Budget" Text="Budgeted" Checked="true" />
                                        <asp:RadioButton ID="radUnBudgeted" runat="server" GroupName="Budget" Checked="false" Text="UnBudgeted" /></td>
                                    <td align="left" ><span class="field-label">Settlement Date</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtSettleDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgSettle_Date" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor:hand" ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtSettleDate" PopupButtonID="imgSettle_Date">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Purpose of Request</span></td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlPurpose" runat="server"></asp:DropDownList></td>
                                    <td align="left" ><span class="field-label">Amount Required</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" ></asp:TextBox></td>
                                </tr>
                                <tr id="ESRemarks" runat="server" visible="false">
                                    <td align="left" ><span class="field-label">Remarks</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine"
                                            runat="server" ></asp:TextBox></td>
                                     <td align="left"></td>
                                     <td align="left"></td>
                                </tr>
                                <tr id="ExpDetails" runat="server" class="title-bg-lite">
                                    <td  align="left" colspan="4">Expense Details</td>
                                </tr>
                                <tr id="ExpDetailsGrid" runat="server">
                                    <td colspan="4">
                                        <asp:GridView ID="grdInvoice" runat="server" AutoGenerateColumns="False" Width="100%"
                                            ShowFooter="true" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="15"
                                            CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInv_ID" runat="server" Text='<%# Bind("ESD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInv_TRN_No" runat="server" Text='<%# Bind("ESD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInv_CN_ID" runat="server" Text='<%# Bind("ESD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expense Item" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtProduct" Width="80%" Enabled="false" runat="server" Text='<%# Bind("PUR_DESCR") %>'></asp:TextBox><asp:ImageButton
                                                            ID="imgSubLedger_e" Visible="false" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />
                                                        <asp:HiddenField ID="h_prod_id_e" runat="server" Value='<%# Bind("ESD_EEF_ID") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtProduct" Width="80%" Enabled="false" runat="server" Text='<%# Bind("PUR_DESCR") %>'></asp:TextBox><asp:ImageButton
                                                            ID="imgSubLedger_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        <asp:HiddenField ID="h_prod_id_e" runat="server" Value='<%# Bind("ESD_EEF_ID") %>' />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtProduct" Width="80%" Enabled="false" runat="server"></asp:TextBox><asp:HiddenField
                                                            ID="h_prod_id" runat="server" Value='<%# Bind("ESD_EEF_ID") %>' />
                                                        <asp:ImageButton ID="imgSubLedger" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Particulars" ItemStyle-Width="40%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtParticulars" Width="95%" Enabled="false" runat="server" Text='<%# Eval("ESD_DETAILS") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtParticulars" Width="95%" Enabled="true" runat="server" Text='<%# Eval("ESD_DETAILS") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtParticulars" Width="95%" Enabled="true" runat="server" Text='<%# Eval("ESD_DETAILS") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Invoice" ItemStyle-Width="12%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInv" Width="95%" Enabled="false" runat="server" Text='<%# Eval("ESD_INVOICE") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInv" Width="95%" runat="server" Style="text-align: left" Text='<%# Eval("ESD_INVOICE") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInv" Width="95%" runat="server" Style="text-align: left" Text='<%# Eval("ESD_INVOICE") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount" ItemStyle-Width="12%" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                    FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" runat="server" Enabled="false" Style="text-align: right"
                                                            Text='<%# Eval("ESD_AMOUNT","{0:0.00}") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" runat="server" Enabled="true" Style="text-align: right"
                                                            Text='<%# Eval("ESD_AMOUNT","{0:0.00}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" runat="server" Enabled="true" Style="text-align: right"
                                                            Text='<%# Eval("ESD_AMOUNT","{0:0.00}") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateBO" runat="server" CausesValidation="True" CommandName="Update"
                                                            Text="Update"></asp:LinkButton><asp:LinkButton ID="lnkCancelBO" runat="server" CausesValidation="False"
                                                                CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddBo" runat="server" CausesValidation="False" CommandName="AddNew"
                                                            Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditBO" runat="server" CausesValidation="False" CommandName="Edit"
                                                            Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trTotal" runat="server">
                                    <td align="left" ><span class="field-label">Amount Settled</span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtAmtSettle" runat="server"></asp:TextBox><asp:Label
                                            ID="lblStatus" runat="server" Visible="TRUE" CssClass="text-danger"></asp:Label>
                                        <asp:Label ID="Label1" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                                    <td align="left"><span class="field-label">Billing Total</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBillingTotal" Enabled="false" runat="server" align="right"> </asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">

                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="TabChanged" AutoPostBack="true">

                                <ajaxToolkit:TabPanel ID="trFlow" runat="server" Visible="false">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel2" runat="server" Height="65px" ScrollBars="Vertical">
                                            <asp:Repeater runat="server" ID="rptFlow">
                                                <HeaderTemplate></HeaderTemplate>
                                                <ItemTemplate><span class="my-span" style="background-color: <%#DataBinder.Eval(Container.DataItem, "wfcolor")%>;"><%#DataBinder.Eval(Container.DataItem, "wfdescr")%></span></ItemTemplate>
                                                <FooterTemplate></FooterTemplate>
                                            </asp:Repeater>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>

                                <ajaxToolkit:TabPanel ID="trComments" runat="server" HeaderText="Comments">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" ></asp:TextBox>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>

                                <ajaxToolkit:TabPanel ID="trDocument" runat="server" Visible="false">
                                    <ContentTemplate>
                                        <asp:FileUpload ID="UploadDocPhoto" runat="server"
                                            ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />

                                        <asp:Repeater ID="rptImages" runat="server">
                                            <ItemTemplate>
                                                [
				<asp:TextBox ID="lblPriContentType" runat="server" Visible="false" Text='<%# Bind("ari_contenttype") %>'></asp:TextBox>
                                                <asp:TextBox ID="lblPriFilename" runat="server" Visible="false" Text='<%# Bind("ari_filename") %>'></asp:TextBox>
                                                <asp:TextBox ID="lblPriId" runat="server" Visible="false" Text='<%# Bind("ari_id") %>'></asp:TextBox>
                                                <asp:LinkButton ID="btnDocumentLink" Text='<%# DataBinder.Eval(Container.DataItem, "ari_name") %>' runat="server"></asp:LinkButton>
                                                <asp:LinkButton ID="btnDelete" Text='x' Visible='<%# DataBinder.Eval(Container.DataItem, "ari_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>]
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>

                                <ajaxToolkit:TabPanel ID="trApproval" runat="server" Visible="false">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtApprovals" runat="server" TextMode="MultiLine" ></asp:TextBox>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>

                                <ajaxToolkit:TabPanel ID="trEmail" runat="server" Visible="false">
                                    <ContentTemplate>
                                        <asp:Label ID="lblSupplierEmail" runat="server" Text="Select Supplier" ></asp:Label>
                                        <asp:TextBox ID="txtSupplierEmail" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgSupplierEmail" runat="server" OnClientClick="getClientNameEmail();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:Label ID="lblSupplierEmailId" runat="server" Text="Email" ></asp:Label>
                                        <asp:TextBox ID="txtSupplierEmailId" runat="server" Text=""></asp:TextBox>
                                        <asp:HiddenField ID="hdnSupplierEmail" runat="server" />
                                        <asp:Button ID="btnSendEmail" runat="server" CausesValidation="False" CssClass="button" Text="Send Email" />
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>

                                <ajaxToolkit:TabPanel ID="trWorkflow" runat="server" Visible="false">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel1" runat="server" Height="75px" ScrollBars="Vertical">
                                            <asp:GridView ID="gvWorkflow" runat="server" CssClass="table table-bordered table-row"
                                                EmptyDataText="No Data" Width="100%"  AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField DataField="wrk_Date" HeaderText="Date" />
                                                    <asp:BoundField DataField="wrk_User" HeaderText="User" />
                                                    <asp:BoundField DataField="wrk_Action" HeaderText="Action" />
                                                    <asp:BoundField DataField="wrk_Details" HeaderText="Details" />
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>
                            </ajaxToolkit:TabContainer>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm_delete();" />
                            <asp:Button ID="btnMessage" runat="server" CausesValidation="False" CssClass="button" Text="Message" />
                            <asp:Button ID="btnClose" runat="server" Visible="false" CausesValidation="False" CssClass="button" Text="Close Request" />
                            <asp:Button ID="btnSend" runat="server" Visible="false" CausesValidation="False" CssClass="button" Text="Send" />
                            <asp:Button ID="btnAccept" runat="server" Visible="false" CausesValidation="False" CssClass="button" Text="Approve" />
                            <asp:Button ID="btnReject" runat="server" Visible="false" CausesValidation="False" CssClass="button" Text="Reject" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="h_empid" runat="server" />
                            <asp:HiddenField ID="h_Arf_Id" runat="server" />
                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="hGridDelete" Value="0" runat="server" />
                            <asp:HiddenField ID="h_ARF_STATUS" runat="server" />
                        </td>
                    </tr>
                </table>

                 <asp:HiddenField ID="hf_products" runat="server" />
         <asp:HiddenField ID="hf_products_id" runat="server" />
            <asp:TextBox ID="txtCommenttxt" runat="server" Visible="false" OnTextChanged="txtCommenttxt_TextChanged" ></asp:TextBox>
                <%--</table>--%>
            </div>
        </div>
    </div>

</asp:Content>
