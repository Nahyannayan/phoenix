﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Item.aspx.vb" Inherits="Inventory_Item" Title="Item Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function calculateItem(txtQty, txtRate, txtTotal) {
            var total;
            if (document.getElementById(txtRate)) {
                total = eval(document.getElementById(txtRate).value) * eval(document.getElementById(txtQty).value);
                document.getElementById(txtTotal).value = format("#,##0.00", total);
            }
        }

        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

    </script>


    <script>
        function getUNSPSCNew() {
            var pMode;
            var NameandCode;

            pMode = "ITMUNSPSC"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_commodity");
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtCommodity.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hdnCommodity.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtSegment.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=txtFamily.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=txtClass.ClientID %>").value = NameandCode[4];
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_commodity" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Item Master
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">

                                <tr runat="server" id="trCategory" visible="false">
                                    <td align="left" class="matters">Category</td>
                                    <td align="left" class="matters" colspan="2">
                                        <asp:TextBox ID="txtICMDescr" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgICMDescr" runat="server" OnClientClick="getCategoryName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hICMID" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trSubCategory" visible="false">
                                    <td align="left" class="matters">Sub Category</td>
                                    <td align="left" class="matters" colspan="2">
                                        <asp:TextBox ID="txtISCDescr" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgISCDescr" runat="server" OnClientClick="getSubCategoryName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hISCID" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trBrand" visible="false">
                                    <td align="left" class="matters">Brand\Make</td>
                                    <td align="left" class="matters" colspan="2">
                                        <asp:TextBox ID="txtMKEDescr" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgMKEDescr" runat="server" OnClientClick="getMakeName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hMKEID" runat="server" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trMake" visible="false">
                                    <td align="left" class="matters">SubBrand\Model\Year</td>
                                    <td align="left" class="matters" colspan="2">
                                        <asp:TextBox ID="txtMDLDescr" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgMDLDescr" runat="server" OnClientClick="getModelName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hMDLID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Segment</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtSegment" runat="server" Enabled="false"></asp:TextBox></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Family</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtFamily" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Class</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtClass" runat="server" Enabled="false"></asp:TextBox></td>
                                    <td align="left" class="matters"><span class="field-label">Commodity</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtCommodity" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="imgCommodity" runat="server" OnClientClick="getUNSPSCNew();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hdnCommodity" Value="0" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Item Description</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtDESCR" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    <td align="left" class="matters"><span class="field-label">Item Details</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox></td>

                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Unit</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtUnit" runat="server"></asp:TextBox></td>
                                    <td rowspan="3" colspan="2">
                                        <asp:ImageMap ID="imgAsset" runat="server" Height="139px" Width="140px"></asp:ImageMap>
                                        <asp:FileUpload ID="UploadDocPhoto" runat="server" Width="325px"
                                            ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Packing</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtPacking" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Rate</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtRate" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="matters" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="bottom">
                            <asp:HiddenField ID="h_ITM_ID" runat="server" />
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
        </div>
    </div>
</asp:Content>

