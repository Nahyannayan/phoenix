<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DataAdministration.aspx.vb" Inherits="DataAdministration" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
       <script src="../Scripts/jquery.mtz.monthpicker.js" type="text/javascript"></script>
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Oasis Administration
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
                <table id="Table1" width="100%">
                    <tr>
                        <td>
                            <table align="left" width="100%">
                                <tr>
                                    <td align="left" class="matters">
                                        <table width="100%">
                                            <tr id="TrModule" runat="server" class="matters">
                                                <td id="Td2"
                                                    valign="middle" colspan="2" runat="server">
                                                    <asp:DataList ID="RepModule" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblModDescr" runat="server" Visible="false" Style="text-align: right" Text='<%# Bind("Module") %>'></asp:Label>
                                                                        <asp:LinkButton ID="lnkSelect" runat="server" CommandArgument='<%# Eval("Module") %>' OnCommand="LinkButton1_Command" CommandName="MyUpdate" Text='<%# Bind("Module") %>'></asp:LinkButton>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </td>
                                            </tr>
                                            <tr id="tr1" runat="server">
                                                <td id="Td7" class="matters" runat="server" width="20%"><span class="field-label"> Updation Type </span></td>
                                                <td id="Td8" class="matters" align="left" runat="server" width="30%">
                                                    <telerik:RadComboBox ID="radFinance" RenderMode="Lightweight" Width="100%" runat="server" EmptyMessage="Type here to search..."
                                                        AutoPostBack="True">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td width="20%" align="center">
                                                    <asp:Button ID="btnView" runat="server" Text="View" CssClass="button" />

                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" Visible="false" />

                                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click" Text="Cancel" />
                                                </td>
                                                <td width="30%" align="center">
                                                    <asp:LinkButton ID="lnkExportToExcel" runat="server" CommandName="ExportToExcel">Export To Excel</asp:LinkButton>

                                                    | 
                              <asp:LinkButton ID="lnkExportToPDF" runat="server" CommandName="ExportToExcel">Export To PDF</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr id="rptServices" runat="server" class="matters">
                                                <td id="Td1" colspan="4" runat="server">
                                                    <asp:DataList ID="RepFinance" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                        <ItemTemplate>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td width="20%">
                                                                        <asp:Label ID="lblRESDescr" runat="server"  CssClass="field-label"  Text='<%# Bind("FieldName") %>'></asp:Label>
                                                                    </td>
                                                                    <td width="30%">
                                                                        <asp:TextBox ID="txtRESAmount" runat="server"   Text='<%# Bind("TEXTBOXVALUE") %>'></asp:TextBox>
                                                                    </td>
                                                                    <td colspan="2" width="50%"></td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="matters"><span class="field-label">Remarks</span></td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtFinanceRemarks" runat="server" Width="93%" Style="text-align: left" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="Grid1" visible="false" runat="server">
                                                <td class="matters" colspan="2">
                                                    <center>
                                 <telerik:RadGrid ID="RadGridReport" runat="server" AllowSorting="True" ShowStatusBar="True"
                                    ShowGroupPanel="True" Width="100%" GridLines="None" AllowPaging="True" PageSize="100"
                                    EnableTheming="False" CellSpacing="0">
                                    <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="false"></ClientSettings>
                                    <AlternatingItemStyle CssClass="RadGridAltRow" HorizontalAlign="Left" />
                                    <MasterTableView GridLines="Both">
                                       <ItemStyle CssClass="RadGridRow" HorizontalAlign="Left" />
                                       <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Center" />
                                    </MasterTableView>
                                 </telerik:RadGrid>
                              </center>
                                                </td>
                                            </tr>
                                            <tr id="Grid2" visible="false" runat="server">
                                                <td class="matters" colspan="2">
                                                    <center>
                                 <telerik:RadGrid ID="RadGridReport1" runat="server" AllowSorting="True" ShowStatusBar="True"
                                    ShowGroupPanel="True" Width="100%" GridLines="None" AllowPaging="True" PageSize="100"
                                    EnableTheming="False" CellSpacing="0">
                                    <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="false"></ClientSettings>
                                    <AlternatingItemStyle CssClass="RadGridAltRow" HorizontalAlign="Left" />
                                    <MasterTableView GridLines="Both">
                                       <ItemStyle CssClass="RadGridRow" HorizontalAlign="Left" />
                                       <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Center" />
                                    </MasterTableView>
                                 </telerik:RadGrid>
                              </center>
                                                </td>
                                            </tr>
                                            <tr id="Grid3" visible="false" runat="server">
                                                <td class="matters" colspan="2">
                                                    <center>
                                 <telerik:RadGrid ID="RadGridReport2" runat="server" AllowSorting="True" ShowStatusBar="True"
                                    ShowGroupPanel="True" Width="100%" GridLines="None" AllowPaging="True" PageSize="100"
                                    EnableTheming="False" CellSpacing="0">
                                    <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="false"></ClientSettings>
                                    <AlternatingItemStyle CssClass="RadGridAltRow" HorizontalAlign="Left" />
                                    <MasterTableView GridLines="Both">
                                       <ItemStyle CssClass="RadGridRow" HorizontalAlign="Left" />
                                       <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Center" />
                                    </MasterTableView>
                                 </telerik:RadGrid>
                              </center>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <script type="text/javascript">
                    function pageLoad() {

                        $('.monthpicker').monthpicker();
                    };
                </script>
            </div>
        </div>
    </div>
</asp:Content>
