﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic

Partial Class Inventory_SetItem
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")


            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "PI01160" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
            Else
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            End If
            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdSetItem.DataSource = SetDFooter
            grdSetItem.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        grdSetItem.Columns(4).Visible = Not mDisable
        grdSetItem.Columns(5).Visible = Not mDisable
        grdSetItem.ShowFooter = Not mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        btnEdit.Visible = mDisable
        btnPrint.Visible = False
        txtDescr.Enabled = Not mDisable
        txtISBN.Enabled = Not mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Seh_Id.Value = p_Modifyid
            If p_Modifyid = 0 Then
                txtBuID.Text = Session("sBsuid")
                TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Trim(txtBuID.Text) & "'", "MainDBO")
            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                dt = MainObj.getRecords("select *from set_h where seh_id=" & p_Modifyid, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    txtBuID.Text = dt.Rows(0)("SEH_BSU_ID")
                    txtDescr.Text = dt.Rows(0)("SEH_DESCR")
                    txtISBN.Text = dt.Rows(0)("SEH_ISBN")
                    TxtBUName.Text = Mainclass.getDataValue("select BSU_NAME from businessunit_m where BSU_ID='" & Trim(txtBuID.Text) & "'", "MainDBO")
                Else

                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If

            FormatFigures()
            fillGridView(SetDFooter, grdSetItem, "select SED_ID,SED_SEH_ID,SED_ITM_ID,isnull(itb_descr,itm_descr) ITM_DESCR,SED_QTY from set_D inner join ITEM_SALE on set_D.SED_ITM_ID = ITEM_SALE.ITM_ID inner join item_salebsu on itm_id=itb_itm_id and itb_bsu_id='" & Session("sBSUID") & "' where  SED_SEH_ID=" & Seh_Id.Value & " order by SED_ID")

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next

    End Sub

    Sub ClearDetails()
        Seh_Id.Value = "0"
        txtDescr.Text = ""
        txtISBN.Text = ""
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property SetDFooter() As DataTable
        Get
            Return ViewState("SetDFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SetDFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
        Session("myData") = fillDataTable
        SetExportFileLink()
    End Sub

    Private Sub SetExportFileLink()
        'Try
        '    btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Framework - " & txtSupplier.Text)
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub showNoRecordsFound()
        If Not SetDFooter Is Nothing AndAlso SetDFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdSetItem.Columns.Count - 2
            grdSetItem.Rows(0).Cells.Clear()
            grdSetItem.Rows(0).Cells.Add(New TableCell())
            grdSetItem.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdSetItem.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSetItem.PageIndexChanging
        grdSetItem.PageIndex = e.NewPageIndex
        grdSetItem.DataSource = Session("myData")
        grdSetItem.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdSetItem.RowCancelingEdit
        grdSetItem.ShowFooter = True
        grdSetItem.EditIndex = -1
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdSetItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSetItem.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtSED_SEH_ID As TextBox = grdSetItem.FooterRow.FindControl("txtSED_SEH_ID")
            Dim hdnITM_ID As HiddenField = grdSetItem.FooterRow.FindControl("hdnITM_ID")
            Dim txtITM_QTY As TextBox = grdSetItem.FooterRow.FindControl("txtITM_QTY")
            Dim hdnSED_ID As HiddenField = grdSetItem.FooterRow.FindControl("hdnSED_ID")
            Dim txtITM_Descr As TextBox = grdSetItem.FooterRow.FindControl("txtITM_Descr")
            lblError.Text = ""

            If hdnITM_ID.Value.Trim.Length = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Item"
            If Val(txtITM_QTY.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Quantity"


            If lblError.Text.Length > 0 Then
                lblError.Text &= " Mandatory!!!!"
                showNoRecordsFound()
                Exit Sub
            End If
            If SetDFooter.Rows(0)(1) = -1 Then SetDFooter.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = SetDFooter.NewRow
            mrow("SED_ID") = 0
            mrow("SED_SEH_ID") = Seh_Id.Value
            mrow("SED_ITM_ID") = Val(hdnITM_ID.Value)
            mrow("SED_QTY") = txtITM_QTY.Text
            mrow("ITM_DESCR") = txtITM_Descr.Text
            SetDFooter.Rows.Add(mrow)
            grdSetItem.EditIndex = -1
            grdSetItem.DataSource = SetDFooter
            grdSetItem.DataBind()
            showNoRecordsFound()
            grdSetItem.EditIndex = -1
            grdSetItem.DataSource = SetDFooter
            grdSetItem.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdSetItem_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdSetItem.RowDeleting
        Dim mRow() As DataRow = SetDFooter.Select("ID=" & grdSetItem.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_SetItemGridDelete.Value &= ";" & mRow(0)("SED_ID")
            SetDFooter.Select("ID=" & grdSetItem.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            SetDFooter.AcceptChanges()
        End If
        If SetDFooter.Rows.Count = 0 Then
            SetDFooter.Rows.Add(SetDFooter.NewRow())
            SetDFooter.Rows(0)(1) = -1
        End If
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()

    End Sub

    Protected Sub grdSetItem_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdSetItem.RowEditing
        Try
            grdSetItem.ShowFooter = True
            grdSetItem.EditIndex = e.NewEditIndex
            grdSetItem.DataSource = SetDFooter
            grdSetItem.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub grdSetItem_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdSetItem.RowUpdating
        Dim s As String = grdSetItem.DataKeys(e.RowIndex).Value.ToString()
        Dim txtSED_SEH_ID As TextBox = grdSetItem.Rows(e.RowIndex).FindControl("txtSED_SEH_ID")
        Dim hdnITM_ID As HiddenField = grdSetItem.Rows(e.RowIndex).FindControl("hdnITM_ID")
        Dim txtITM_QTY As TextBox = grdSetItem.Rows(e.RowIndex).FindControl("txtITM_QTY")
        Dim hdnSED_ID As HiddenField = grdSetItem.Rows(e.RowIndex).FindControl("hdnSED_ID")
        Dim txtITM_Descr As TextBox = grdSetItem.Rows(e.RowIndex).FindControl("txtITM_Descr")
        Dim mrow As DataRow
        mrow = SetDFooter.Select("ID=" & s)(0)
        mrow("SED_SEH_ID") = Seh_Id.Value
        mrow("SED_ITM_ID") = Val(hdnITM_ID.Value)
        mrow("SED_QTY") = txtITM_QTY.Text
        mrow("ITM_DESCR") = txtITM_Descr.Text
        grdSetItem.EditIndex = -1
        grdSetItem.DataSource = SetDFooter
        grdSetItem.DataBind()
        showNoRecordsFound()
    End Sub

    <System.Web.Services.WebMethod()> _
        Public Shared Function GetItmDescr(ByVal prefixText As String, ByVal contextKey As String) As String()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

        Dim StrSQL As String
        If contextKey Is Nothing Then contextKey = ""
        StrSQL = "select top 10 DESCRIPTION,ITM_ID from (select isnull(ITB_DESCR, ITM_DESCR) DESCRIPTION,ITM_ID from  ITEM_SALE inner join ITEM_SALEBSU on itm_id=iTB_itm_id where ITB_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "') a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("ITM_ID").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
        Next
        Return items.ToArray()
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblError.Text = ""

        If lblError.Text.Length > 0 Then lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Mandatory"
        If grdSetItem.Rows.Count = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Set Item should contain atleast 1 item"


        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(5) As SqlParameter

        pParms(1) = Mainclass.CreateSqlParameter("@SEH_ID", Seh_Id.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@SEH_BSU_ID", txtBuID.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@SEH_ISBN", txtISBN.Text, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@SEH_DESCR", txtDescr.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@SEH_USER", Session("sUsr_name"), SqlDbType.VarChar)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveSET_H", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim RowCount As Integer
            For RowCount = 0 To SetDFooter.Rows.Count - 1
                Dim iParms(4) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, SetDFooter.Rows(RowCount)("SED_ID"))
                If SetDFooter.Rows(RowCount).RowState = 8 Then rowState = -1

                iParms(1) = Mainclass.CreateSqlParameter("@SED_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@SED_SEH_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@SED_ITM_ID", SetDFooter.Rows(RowCount)("SED_ITM_ID"), SqlDbType.Int)
                iParms(4) = Mainclass.CreateSqlParameter("@SED_QTY", SetDFooter.Rows(RowCount)("SED_QTY"), SqlDbType.Decimal)

                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveSet_D", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            If h_SetItemGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_SetItemGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@SED_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@SED_SEH_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from SET_D where SED_ID=@SED_ID and SED_SEH_ID=@SED_SEH_ID", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"), False)
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If Seh_Id.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update set_H set SEH_DELETED=1, SEH_USER=@SEH_USER, SEH_SYSDATE=getdate() where SEH_ID=@SEH_ID"
                Dim pParms(2) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@SEH_ID", Seh_Id.Value, SqlDbType.Int, True)
                pParms(2) = Mainclass.CreateSqlParameter("@SEH_USER", Session("sUsr_name"), SqlDbType.VarChar)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, Seh_Id.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

End Class
