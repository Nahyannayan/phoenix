﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Drawing
Partial Class Inventory_TenancyContractHiringDehiring
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI02050" And ViewState("MainMnu_code") <> "PI02060" And ViewState("MainMnu_code") <> "PI02070") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If

                If ViewState("MainMnu_code") = "PI02070" Then
                    TrVacatingDate.Visible = True
                Else
                    TrVacatingDate.Visible = False
                End If

                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))

                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            'btnReject.Visible = False
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            'btnReject.Visible = False
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        btnSave.Visible = Not mDisable
        rptHeader.Text = "Tenancy Contract Hiring / DeHiring"

    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim Status As String = "N"
            Dim Status1 As String = ""
            h_EntryId.Value = p_Modifyid
            h_bsu_id.Value = Session("sBSUID")
            If p_Modifyid = 0 Then
             
            Else
                If UCase(ViewState("datamode")) = "VIEW" Then
                    Status = Mainclass.getDataValue("select top 1 TDH_MSG_STATUS FROM TCT_DH WHERE TDH_TCH_ID='" & p_Modifyid & "' order by tdh_id desc", "OASIS_PUR_INVConnectionString")
                    Status1 = Mainclass.getDataValue("select isnull(TCH_HD_STATUS,'') FROM TCT_H WHERE TCH_ID='" & p_Modifyid & "'", "OASIS_PUR_INVConnectionString")
                    If Status = "S" Then
                        btnApprove.Visible = True
                        btnReject.Visible = True
                    Else
                        If ViewState("MainMnu_code") = "PI02060" Then
                            trEmpDetails.Visible = False
                            trEmpDetailsHeading.Visible = False
                        End If
                        btnApprove.Visible = False
                        btnReject.Visible = False
                    End If
            End If
            DisplayServices(p_Modifyid)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub ClearDetails()
    End Sub
    Private Sub DisplayServices(ByVal tch_id As String)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select Descr,Value from TenancyContractValues where ID=" & tch_id).Tables(0)
        Dim Status As String = "N"
        RepService.DataSource = dt
        RepService.DataBind()
        Dim dt1 As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "SELECT TDH_ID, TDH_BSU_ID, TDH_EMP_ID,TDH_DELETED, TDH_REU_ID, TDH_LAMOUNT, TDH_EREU_ID, TDH_EAMOUNT, TDH_DAMOUNT, TDH_TCH_ID,case when TDH_EMP_ID=0 then BSU_NAME  else EMPNAME end EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TDH_MSTATUS,TDH_DES_ID,DES_DESCR DESIGNATION,TDH_DEPENDANTS,TDH_FURN_ID,FURN_DESCR  FROM TCT_DH LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TDH_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TDH_FURN_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TDH_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TDH_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TDH_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TDH_DES_ID  where TDH_TCH_ID=" & tch_id & " and TDH_MSG_STATUS<>'A' order by TDH_ID").Tables(0)
        txtContractEndDate.Text = Mainclass.getDataValue("select tch_end from tct_h where tch_id=" & tch_id & "", "OASIS_PUR_INVConnectionString")
        fillEmpDetailsGridView(grdEmpDetails, "SELECT TDH_ID, TDH_BSU_ID, TDH_EMP_ID,TDH_EMP_ID TDH_EMP_ID1, TDH_DELETED,TDH_REU_ID, TDH_LAMOUNT, TDH_EREU_ID, TDH_EAMOUNT, TDH_DAMOUNT, TDH_TCH_ID,TDH_TCE_ID,case when (TDH_EMP_ID=0 OR TDH_deleted=1) then 'School'  else EMPNAME end EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TDH_MSTATUS,TDH_DES_ID,ODE.EMP_DES_DESCR DESIGNATION,TDH_DEPENDANTS,TDH_FURN_ID,FURN_DESCR,TDH_HDATE,TDH_DDATE,TDH_EMPLOYEE_CHANGED,TDH_APR_ID,TDH_UNITNO,TDH_OLDRENT,TDH_MSG_STATUS FROM TCT_DH LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TDH_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TDH_FURN_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TDH_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TDH_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TDH_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TDH_DES_ID  LEFT OUTER JOIN OASIS_DOCS..VW_EMPLOYEE_V ODE ON ODE.emp_id =TCT_DH.TDH_EMP_ID where TDH_TCH_ID=" & tch_id & " and TDH_MSG_STATUS<>'A'  union SELECT 0 TDH_ID,TCE_BSU_ID TDH_BSU_ID, TCE_EMP_ID TDH_EMP_ID,TCE_EMP_ID TDH_EMP_ID1,TCE_DELETED TDH_DELETED, TCE_REU_ID TDH_REU_ID, TCE_LAMOUNT TDH_LAMOUNT, TCE_EREU_ID TDH_EREU_ID, TCE_EAMOUNT TDH_EAMOUNT, TCE_DAMOUNT TDH_DAMOUNT, TCE_TCH_ID TDH_TCH_ID ,TCE_ID TDH_TCE_ID,case when TCE_EMP_ID=0 then 'School'  else isnull(EMPNAME,'School') end EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TCE_MSTATUS TDH_MSTATUS,TCE_DES_ID TDH_DES_ID,ODE.EMP_DES_DESCR DESIGNATION,TCE_DEPENDANTS TDH_DEPENDANTS,TCE_FURN_ID TDH_FURN_ID,FURN_DESCR,NULL TDH_HDATE,NULL TDH_DDATE,0 TDH_EMPLOYEE_CHANGED,0 TDH_APR_ID,isnull(TCE_UNITNO,'')TCE_UNITNO,isnull(TCE_OLDRENT,0)TCE_OLDRENT,'N' tdh_msg_status  FROM TCT_E LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TCE_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TCE_FURN_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCE_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TCE_DES_ID LEFT OUTER JOIN OASIS_DOCS..VW_EMPLOYEE_V ODE ON ODE.emp_id =TCE_EMP_ID  where TCE_TCH_ID=" & tch_id & " and TCE_DELETED=0 and tce_id not in(select tdh_tce_id from tct_dh where TDH_MSG_STATUS<>'A') order by tdh_id")
        If ViewState("MainMnu_code") = "PI02060" Then
            FillOldEmpdetailsGrisview(grdOldEmpDetails, "SELECT TCE_ID, TCE_BSU_ID,TCE_MSTATUS,TCE_DEPENDANTS, TCE_EMP_ID,TCE_REU_ID, TCE_LAMOUNT, TCE_EREU_ID, TCE_EAMOUNT, TCE_DAMOUNT, TCE_TCH_ID,case when (TCE_EMP_ID=0 OR TCE_deleted=1) then 'School'  else EMPNAME end EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TCE_DES_ID,ODE.EMP_DES_DESCR DESIGNATION,TCE_DEPENDANTS,TCE_FURN_ID,FURN_DESCR,TCE_UNITNO,TCE_OLDRENT FROM  TCT_E LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TCE_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TCE_FURN_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCE_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TCE_DES_ID  LEFT OUTER JOIN OASIS_DOCS..VW_EMPLOYEE_V ODE ON ODE.emp_id =TCE_EMP_ID where TCE_TCH_ID='" & tch_id & "' and isnull(tce_deleted,0)=0  order by tCE_id")
        End If
        Status = Mainclass.getDataValue("select isnull(tch_hd_status,'') FROM TCT_H WHERE TCH_ID='" & tch_id & "'", "OASIS_PUR_INVConnectionString")
        If dt1.Rows.Count > 0 And (Status = "" Or Status = "A") Then
            btnSend.Visible = True
        End If
        If ViewState("MainMnu_code") = "PI02060" Then
            trEmpOldDetails.Visible = True
            trOld.Visible = True
        End If

        If Status = "N" Or Status = "A" Or Status = "" Then
            If ViewState("MainMnu_code") <> "PI02060" Then
                btnSave.Visible = True
            End If
        End If
    End Sub
    Private Sub fillEmpDetailsGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        If Session("Empdata") Is Nothing Then
            EmployeeDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
            Dim mtable As New DataTable
            Dim dcID As New DataColumn("ID", GetType(Integer))
            dcID.AutoIncrement = True
            dcID.AutoIncrementSeed = 1
            dcID.AutoIncrementStep = 1
            mtable.Columns.Add(dcID)
            mtable.Merge(EmployeeDetails)

            If mtable.Rows.Count = 0 Then
                mtable.Rows.Add(mtable.NewRow())
                mtable.Rows(0)(1) = -1
            End If
            EmployeeDetails = mtable
        Else
            EmployeeDetails = Session("Empdata")
        End If
        fillGrdView.DataSource = EmployeeDetails
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub
    Private Sub FillOldEmpdetailsGrisview(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        OldEmployeeDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(OldEmployeeDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        OldEmployeeDetails = mtable

        fillGrdView.DataSource = OldEmployeeDetails
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub
    Private Property EmployeeDetails() As DataTable
        Get
            Return ViewState("EmployeeDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EmployeeDetails") = value
        End Set
    End Property
    Private Property OldEmployeeDetails() As DataTable
        Get
            Return ViewState("OldEmployeeDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("OldEmployeeDetails") = value
        End Set
    End Property

    Private Sub showNoRecordsFound()
        If Not EmployeeDetails Is Nothing Then
            If EmployeeDetails.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdEmpDetails.Columns.Count - 2
                grdEmpDetails.Rows(0).Cells.Clear()
                grdEmpDetails.Rows(0).Cells.Add(New TableCell())
                grdEmpDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdEmpDetails.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    SetDataMode("edit")
    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    'End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            If ViewState("MainMnu_code") = "PI02050" Then
                If Not EmployeeDetails Is Nothing Then
                    Dim RowCount1 As Integer, InsCount1 As Integer = 0
                    For RowCount1 = 0 To EmployeeDetails.Rows.Count - 1
                        Dim eParms(23) As SqlClient.SqlParameter
                        Dim rowState As Integer = ViewState("EntryId")
                        eParms(1) = Mainclass.CreateSqlParameter("@TDH_ID", EmployeeDetails.Rows(RowCount1)("TDH_ID"), SqlDbType.Int)
                        eParms(2) = Mainclass.CreateSqlParameter("@TDH_TCH_ID", EmployeeDetails.Rows(RowCount1)("TDH_TCH_ID"), SqlDbType.Int)
                        eParms(3) = Mainclass.CreateSqlParameter("@TDH_TCE_ID", EmployeeDetails.Rows(RowCount1)("TDH_TCE_ID"), SqlDbType.Int)
                        eParms(4) = Mainclass.CreateSqlParameter("@TDH_BSU_ID", EmployeeDetails.Rows(RowCount1)("TDH_BSU_ID"), SqlDbType.VarChar)
                        eParms(5) = Mainclass.CreateSqlParameter("@TDH_EMP_ID", EmployeeDetails.Rows(RowCount1)("TDH_EMP_ID"), SqlDbType.VarChar)
                        eParms(6) = Mainclass.CreateSqlParameter("@TDH_REU_ID", EmployeeDetails.Rows(RowCount1)("TDH_REU_ID"), SqlDbType.Int)
                        eParms(7) = Mainclass.CreateSqlParameter("@TDH_EREU_ID", EmployeeDetails.Rows(RowCount1)("TDH_EREU_ID"), SqlDbType.Int)
                        eParms(8) = Mainclass.CreateSqlParameter("@TDH_LAMOUNT", EmployeeDetails.Rows(RowCount1)("TDH_LAMOUNT"), SqlDbType.Decimal)
                        eParms(9) = Mainclass.CreateSqlParameter("@TDH_EAMOUNT", EmployeeDetails.Rows(RowCount1)("TDH_EAMOUNT"), SqlDbType.Decimal)
                        eParms(10) = Mainclass.CreateSqlParameter("@TDH_DAMOUNT", EmployeeDetails.Rows(RowCount1)("TDH_DAMOUNT"), SqlDbType.Decimal)
                        eParms(11) = Mainclass.CreateSqlParameter("@TDH_DES_ID", EmployeeDetails.Rows(RowCount1)("TDH_DES_ID"), SqlDbType.VarChar)
                        eParms(12) = Mainclass.CreateSqlParameter("@TDH_MSTATUS", EmployeeDetails.Rows(RowCount1)("TDH_MSTATUS"), SqlDbType.VarChar)
                        eParms(13) = Mainclass.CreateSqlParameter("@TDH_DEPENDANTS", EmployeeDetails.Rows(RowCount1)("TDH_DEPENDANTS"), SqlDbType.Int)
                        eParms(14) = Mainclass.CreateSqlParameter("@TDH_FURN_ID", EmployeeDetails.Rows(RowCount1)("TDH_FURN_ID"), SqlDbType.Int)
                        eParms(15) = Mainclass.CreateSqlParameter("@TDH_USER", Session("sUsr_name"), SqlDbType.VarChar)

                        If ViewState("MainMnu_code") = "PI02050" Then
                            eParms(16) = Mainclass.CreateSqlParameter("@TDH_STATUS", "Hire", SqlDbType.VarChar)
                            If EmployeeDetails.Rows(RowCount1)("TDH_EMP_ID") = 0 Then
                                eParms(17) = Mainclass.CreateSqlParameter("@TDH_DELETED", 1, SqlDbType.Int)
                            Else
                                eParms(17) = Mainclass.CreateSqlParameter("@TDH_DELETED", 0, SqlDbType.Int)
                            End If
                        Else
                            eParms(16) = Mainclass.CreateSqlParameter("@TDH_STATUS", "DeHire", SqlDbType.VarChar)
                            eParms(17) = Mainclass.CreateSqlParameter("@TDH_DELETED", 1, SqlDbType.Int)
                        End If
                        eParms(18) = Mainclass.CreateSqlParameter("@Old_EmpId", EmployeeDetails.Rows(RowCount1)("TDH_EMP_ID1"), SqlDbType.Int)
                        If EmployeeDetails.Rows(RowCount1)("TDH_EMP_ID") <> EmployeeDetails.Rows(RowCount1)("TDH_EMP_ID1") Then
                            eParms(19) = Mainclass.CreateSqlParameter("@isHiring", 1, SqlDbType.Int)
                        Else
                            eParms(19) = Mainclass.CreateSqlParameter("@isHiring", 0, SqlDbType.Int)
                        End If

                        eParms(20) = Mainclass.CreateSqlParameter("@TDH_UNITNO", EmployeeDetails.Rows(RowCount1)("TDH_UNITNO"), SqlDbType.VarChar)
                        eParms(21) = Mainclass.CreateSqlParameter("@TDH_OLDRENT", EmployeeDetails.Rows(RowCount1)("TDH_OLDRENT"), SqlDbType.Int)
                        'eParms(22) = Mainclass.CreateSqlParameter("@TDH_DDATE", EmployeeDetails.Rows(RowCount1)("TDH_DDATE"), SqlDbType.Date)
                        'eParms(23) = Mainclass.CreateSqlParameter("@TDH_HDATE", EmployeeDetails.Rows(RowCount1)("TDH_HDATE"), SqlDbType.Date)
                        eParms(22) = Mainclass.CreateSqlParameter("@TDH_HDATE", IIf(EmployeeDetails.Rows(RowCount1)("TDH_HDATE") Is Nothing, "1900-01-01", EmployeeDetails.Rows(RowCount1)("TDH_HDATE")), SqlDbType.Date)
                        eParms(23) = Mainclass.CreateSqlParameter("@TDH_DDATE", IIf(EmployeeDetails.Rows(RowCount1)("TDH_DDATE") Is Nothing, "1900-01-01", EmployeeDetails.Rows(RowCount1)("TDH_DDATE")), SqlDbType.Date)
                        '
                        'If EmployeeDetails.Rows(RowCount1)("TDH_DDATE") = "" Then
                        '    eParms(22) = Mainclass.CreateSqlParameter("@TDH_DDATE", "01/01/1900", SqlDbType.Date)
                        'Else
                        '    eParms(22) = Mainclass.CreateSqlParameter("@TDH_DDATE", EmployeeDetails.Rows(RowCount1)("TDH_DDATE"), SqlDbType.Date)
                        'End If

                        'If EmployeeDetails.Rows(RowCount1)("TDH_HDATE") = "" Then
                        '    eParms(22) = Mainclass.CreateSqlParameter("@TDH_HDATE", "01/01/1900", SqlDbType.Date)
                        'Else
                        '    eParms(22) = Mainclass.CreateSqlParameter("@TDH_HDATE", EmployeeDetails.Rows(RowCount1)("TDH_HDATE"), SqlDbType.Date)
                        'End If






                        If Not IsDBNull(EmployeeDetails.Rows(RowCount1)("TDH_TCH_ID")) Then
                            InsCount1 += 1
                            Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETCT_DH", eParms)
                            If RetValFooter = "-1" Then
                                lblError.Text = "Unexpected Error !!!"
                                stTrans.Rollback()
                                Exit Sub
                            Else
                            End If
                        End If
                    Next
                End If
                'ElseIf ViewState("MainMnu_code") = "PI02060" Then ' DeHiring
                '    For Each mrow In grdEmpDetails.Rows
                '        Dim ChkBxItem As CheckBox = TryCast(mrow.FindControl("chkControl"), CheckBox)
                '        Dim TDH_ID As Label = TryCast(mrow.FindControl("lblTDH_ID"), Label)
                '        Dim TDH_REU_ID As HiddenField = TryCast(mrow.FindControl("h_REU_ID"), HiddenField)
                '        Dim TDH_EREU_ID As HiddenField = TryCast(mrow.FindControl("h_EREU_ID"), HiddenField)
                '        Dim tdh_LAmount As Label = TryCast(mrow.FindControl("lblLeaseAmount"), Label)
                '        Dim TDH_EAMOUNT As Label = TryCast(mrow.FindControl("lblOtherDetails"), Label)
                '        Dim TDH_DAMOUNT As Label = TryCast(mrow.FindControl("lblDetails"), Label)
                '        Dim TDH_DES_ID As HiddenField = TryCast(mrow.FindControl("h_Des_Id"), HiddenField)
                '        Dim TDH_MSTATUS As Label = TryCast(mrow.FindControl("lblMStatus"), Label)
                '        Dim TDH_FURN_ID As HiddenField = TryCast(mrow.FindControl("h_Furn_id"), HiddenField)
                '        Dim TDH_EMP_ID As TextBox = TryCast(mrow.FindControl("txtEmployeeID"), TextBox)
                '        Dim TDH_EMP_ID1 As HiddenField = TryCast(mrow.FindControl("h_emp_Id1"), HiddenField)
                '        Dim TDH_DEPENDANTS As Label = TryCast(mrow.FindControl("lblDependants"), Label)
                '        Dim TDH_DESIGNATION As Label = TryCast(mrow.FindControl("lbldesignation"), Label)
                '        Dim TDH_TCE_ID As HiddenField = TryCast(mrow.FindControl("h_TDH_TCE_ID"), HiddenField)
                '        Dim TDH_BSU_ID As HiddenField = TryCast(mrow.FindControl("h_TDH_BSU_ID"), HiddenField)
                '        Dim TDH_TCH_ID As HiddenField = TryCast(mrow.FindControl("h_TDH_TCH_ID"), HiddenField)
                '        Dim eParms(20) As SqlClient.SqlParameter
                '        eParms(1) = Mainclass.CreateSqlParameter("@TDH_ID", TDH_ID.Text, SqlDbType.Int)
                '        eParms(2) = Mainclass.CreateSqlParameter("@TDH_TCH_ID", TDH_TCH_ID.Value, SqlDbType.Int)
                '        eParms(3) = Mainclass.CreateSqlParameter("@TDH_TCE_ID", TDH_TCE_ID.Value, SqlDbType.Int)
                '        eParms(4) = Mainclass.CreateSqlParameter("@TDH_BSU_ID", TDH_BSU_ID.Value, SqlDbType.VarChar)

                '        If ChkBxItem.Checked = True Then
                '            eParms(5) = Mainclass.CreateSqlParameter("@TDH_EMP_ID", 0, SqlDbType.Int)
                '            eParms(6) = Mainclass.CreateSqlParameter("@TDH_DELETED", 1, SqlDbType.Int)
                '            eParms(12) = Mainclass.CreateSqlParameter("@TDH_DES_ID", 0, SqlDbType.VarChar)
                '            eParms(18) = Mainclass.CreateSqlParameter("@isDehiring", 1, SqlDbType.VarChar)
                '        Else
                '            If TDH_EMP_ID.Text = 0 Then
                '                eParms(6) = Mainclass.CreateSqlParameter("@TDH_DELETED", 1, SqlDbType.Int)
                '                eParms(5) = Mainclass.CreateSqlParameter("@TDH_EMP_ID", 0, SqlDbType.Int)
                '                eParms(12) = Mainclass.CreateSqlParameter("@TDH_DES_ID", 0, SqlDbType.VarChar)
                '                eParms(18) = Mainclass.CreateSqlParameter("@isDehiring", 0, SqlDbType.VarChar)
                '            Else
                '                eParms(6) = Mainclass.CreateSqlParameter("@TDH_DELETED", 0, SqlDbType.Int)
                '                eParms(5) = Mainclass.CreateSqlParameter("@TDH_EMP_ID", TDH_EMP_ID.Text, SqlDbType.Int)
                '                eParms(12) = Mainclass.CreateSqlParameter("@TDH_DES_ID", TDH_DES_ID.Value, SqlDbType.VarChar)
                '                eParms(18) = Mainclass.CreateSqlParameter("@isDehiring", 0, SqlDbType.VarChar)
                '            End If
                '        End If
                '        eParms(7) = Mainclass.CreateSqlParameter("@TDH_REU_ID", TDH_REU_ID.Value, SqlDbType.Int)
                '        eParms(8) = Mainclass.CreateSqlParameter("@TDH_EREU_ID", TDH_EREU_ID.Value, SqlDbType.Int)
                '        eParms(9) = Mainclass.CreateSqlParameter("@TDH_LAMOUNT", tdh_LAmount.Text, SqlDbType.Decimal)
                '        eParms(10) = Mainclass.CreateSqlParameter("@TDH_EAMOUNT", TDH_EAMOUNT.Text, SqlDbType.Decimal)
                '        eParms(11) = Mainclass.CreateSqlParameter("@TDH_DAMOUNT", TDH_DAMOUNT.Text, SqlDbType.Decimal)
                '        eParms(13) = Mainclass.CreateSqlParameter("@TDH_MSTATUS", TDH_MSTATUS.Text, SqlDbType.VarChar)
                '        eParms(14) = Mainclass.CreateSqlParameter("@TDH_DEPENDANTS", TDH_DEPENDANTS.Text, SqlDbType.Int)
                '        eParms(15) = Mainclass.CreateSqlParameter("@TDH_FURN_ID", TDH_FURN_ID.Value, SqlDbType.Int)
                '        eParms(16) = Mainclass.CreateSqlParameter("@TDH_USER", Session("sUsr_name"), SqlDbType.VarChar)
                '        eParms(17) = Mainclass.CreateSqlParameter("@TDH_STATUS", "DeHire", SqlDbType.VarChar)
                '        eParms(18) = Mainclass.CreateSqlParameter("@Old_EmpId", TDH_EMP_ID1.Value, SqlDbType.VarChar)
                '        Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETCT_DH", eParms)
                '        If RetValFooter = "-1" Then
                '            lblError.Text = "Unexpected Error !!!"
                '            stTrans.Rollback()
                '            Exit Sub
                '        End If
                '    Next
            ElseIf ViewState("MainMnu_code") = "PI02070" Then 'Vacating
                Dim eParms1(1) As SqlClient.SqlParameter
                If Not CDate(txtVacatingDate.Text) <= CDate(txtContractEndDate.Text) Then
                    lblError.Text = "Vacating Date Should Be less than Contract End Date"
                    Exit Sub
                End If
                If txtVacatingDate.Text = "" Then
                    lblError.Text = "Please Select the Vacating Date."
                    Exit Sub
                End If
                eParms1(0) = Mainclass.CreateSqlParameter("@TCH_ID", ViewState("EntryId"), SqlDbType.Int)
                eParms1(1) = Mainclass.CreateSqlParameter("@VACATE_DATE", txtVacatingDate.Text, SqlDbType.DateTime)
                Dim RetValFooter1 As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETCT_CONTRACTVACATEDATE", eParms1)
                If RetValFooter1 = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            End If
            stTrans.Commit()
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally

            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub grdEmpDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdEmpDetails.RowCancelingEdit
        grdEmpDetails.ShowFooter = True
        grdEmpDetails.EditIndex = -1
        grdEmpDetails.DataSource = EmployeeDetails
        grdEmpDetails.DataBind()
    End Sub
    Protected Sub grdEmpDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdEmpDetails.RowCommand
        Try
            If e.CommandName = "AddNew" Then
                Dim txtEmployee As TextBox = grdEmpDetails.FooterRow.FindControl("txtEmployee")
                Dim txtOTHERS As TextBox = grdEmpDetails.FooterRow.FindControl("txtOTHERS")
                Dim h_Emp_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Emp_Id")
                Dim h_Des_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Des_Id")
                Dim h_BSU_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_BSU_Id")
                Dim h_UnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_UnitType")
                Dim h_EUnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_EUnitType")
                Dim txtLeaseAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtLeaseAmount")
                Dim txtEAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtEAmount")
                Dim txtDeduction As TextBox = grdEmpDetails.FooterRow.FindControl("txtDeduction")
                Dim ddlEUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlEUnitType")
                Dim ddlUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlUnitType")
                Dim ddlBSU As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlBSU")
                Dim ddlfurn As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlFurnish")
                Dim ddlMStatus As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlMStatus")
                Dim txtDESIGNATION As TextBox = grdEmpDetails.FooterRow.FindControl("txtDESIGNATION")
                Dim txtMSTATUS As TextBox = grdEmpDetails.FooterRow.FindControl("txtMSTATUS")
                Dim txtDEPENDANTS As TextBox = grdEmpDetails.FooterRow.FindControl("txtDEPENDANTS")
                Dim lblMSTATUS As Label = grdEmpDetails.FooterRow.FindControl("lblMSTATUS")
                Dim lblDEPENDANTS As Label = grdEmpDetails.FooterRow.FindControl("lblDEPENDANTS")
                If IsNumeric(txtLeaseAmount.Text) = 0 Then lblError.Text = "enter Amount in correct format,"
                If Val(txtLeaseAmount.Text) = 0 Then lblError.Text = "enter Lease Amount,"
                If Len(txtEmployee.Text) <= 0 Then lblError.Text = "select at least one employee,"

                If lblError.Text.Length > 0 Then
                    showNoRecordsFound()
                    Exit Sub
                End If
                If EmployeeDetails.Rows(0)(1) = -1 Then EmployeeDetails.Rows.RemoveAt(0)
                Dim mrow As DataRow
                mrow = EmployeeDetails.NewRow
                mrow("TDH_ID") = 0
                mrow("TDH_DEPENDANTS") = IIf(txtDEPENDANTS.Text = "", DBNull.Value, txtDEPENDANTS.Text)
                mrow("TDH_LAMOUNT") = txtLeaseAmount.Text
                mrow("TDH_EAMOUNT") = txtEAmount.Text
                mrow("TDH_DAMOUNT") = IIf(txtDeduction.Text = "", 0, txtDeduction.Text)
                mrow("TDH_BSU_ID") = Session("sBsuid")
                mrow("TDH_REU_ID") = ddlUnitType.SelectedValue
                mrow("TDH_EREU_ID") = ddlEUnitType.SelectedValue
                mrow("TDH_FURN_ID") = ddlfurn.SelectedValue
                mrow("UNITTYPE") = ddlUnitType.SelectedItem.Text
                mrow("EUNITTYPE") = ddlEUnitType.SelectedItem.Text
                mrow("FURN_DESCR") = ddlfurn.SelectedItem.Text
                mrow("TDH_EMP_ID") = h_Emp_Id.Value
                mrow("TDH_TCH_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                mrow("TDH_DELETED") = 0
                mrow("EMPNAME") = txtEmployee.Text
                mrow("TDH_MSTATUS") = ddlMStatus.SelectedValue
                mrow("TDH_DES_ID") = h_Des_Id.Value
                mrow("DESIGNATION") = txtDESIGNATION.Text
                EmployeeDetails.Rows.Add(mrow)
                grdEmpDetails.EditIndex = -1
                grdEmpDetails.DataSource = EmployeeDetails
                grdEmpDetails.DataBind()
                showNoRecordsFound()
            Else
                Dim txtEmployee As TextBox = grdEmpDetails.FooterRow.FindControl("txtEmployee")
                Dim txtOTHERS As TextBox = grdEmpDetails.FooterRow.FindControl("txtOTHERS")
                Dim h_Emp_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Emp_Id")
                Dim h_Des_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_Des_Id")
                Dim h_BSU_Id As HiddenField = grdEmpDetails.FooterRow.FindControl("h_BSU_Id")
                Dim h_UnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_UnitType")
                Dim h_EUnitType As HiddenField = grdEmpDetails.FooterRow.FindControl("h_EUnitType")
                Dim txtLeaseAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtLeaseAmount")
                Dim txtEAmount As TextBox = grdEmpDetails.FooterRow.FindControl("txtEAmount")
                Dim txtDeduction As TextBox = grdEmpDetails.FooterRow.FindControl("txtDeduction")
                Dim ddlEUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlEUnitType")
                Dim ddlUnitType As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlUnitType")
                Dim ddlBSU As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlBSU")
                Dim ddlfurn As DropDownList = grdEmpDetails.FooterRow.FindControl("ddlFurnish")
                Dim txtDESIGNATION As TextBox = grdEmpDetails.FooterRow.FindControl("txtDESIGNATION")
                Dim txtMSTATUS As TextBox = grdEmpDetails.FooterRow.FindControl("txtMSTATUS")
                Dim txtDEPENDANTS As TextBox = grdEmpDetails.FooterRow.FindControl("txtDEPENDANTS")
                Dim txtDDate As TextBox = grdEmpDetails.FooterRow.FindControl("txtDDate")
                Dim txtHDate As TextBox = grdEmpDetails.FooterRow.FindControl("txtHDate")

            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub grdEmpDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEmpDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
                Dim txtLeaseAmt As TextBox = e.Row.FindControl("txtLeaseAmount")
                Dim txtEAmount As TextBox = e.Row.FindControl("txtEAmount")
                Dim txtDeduction As TextBox = e.Row.FindControl("txtDeduction")
                Dim lnkEdit As LinkButton = e.Row.FindControl("lnkEditQUD")
                Dim txtEmpid As TextBox = e.Row.FindControl("txtEmployeeID")
                Dim chkBox As CheckBox = e.Row.FindControl("chkControl")
                Dim h_Deleted As HiddenField = e.Row.FindControl("h_Deleted")
                Dim lblOtherDetails As Label = e.Row.FindControl("lblOtherDetails")
                Dim lblLeaseEType As Label = e.Row.FindControl("lblLeaseEType")
                Dim lblLeaseAmount As Label = e.Row.FindControl("lblLeaseAmount")
                Dim lblLeaseType As Label = e.Row.FindControl("lblLeaseType")
                Dim lblDepend As Label = e.Row.FindControl("lblDependants")
                Dim lblMStat As Label = e.Row.FindControl("lblMStatus")
                Dim lbldesig As Label = e.Row.FindControl("lbldesignation")
                Dim lblEmpName As Label = e.Row.FindControl("lblEmpName")
                Dim lblDeduction As Label = e.Row.FindControl("lblDetails")
                Dim lblfurnished As Label = e.Row.FindControl("lblfurnished")
                Dim lblEmpChanged As Label = e.Row.FindControl("lblEmpChanged")
                Dim lbl_TDH_APR_ID As Label = e.Row.FindControl("lbl_TDH_APR_ID")
                Dim lblUnitNo As Label = e.Row.FindControl("lblUnitNo")
                Dim lblOldRent As Label = e.Row.FindControl("lblOldRent")
                Dim lblDDate As Label = e.Row.FindControl("lblDDate")
                Dim lblHDate As Label = e.Row.FindControl("lblHDate")
                If Not txtEmpid Is Nothing Then
                    If ViewState("MainMnu_code") = "PI02050" Then
                        If txtEmpid.Text = "0" Or h_Deleted.Value = True Or lbl_TDH_APR_ID.Text = "0" Then
                            lnkEdit.Enabled = True
                        Else
                            lnkEdit.Enabled = False
                        End If
                        txtEmpid.Visible = False
                    Else
                        lnkEdit.Enabled = False
                    End If
                End If

                If Not chkBox Is Nothing Then
                    If ViewState("MainMnu_code") = "PI02050" Then
                        chkBox.Enabled = False
                    Else
                        If h_Deleted.Value = False And txtEmpid.Text <> 0 Then
                            chkBox.Enabled = True
                        Else
                            chkBox.Enabled = False
                        End If
                    End If
                End If

                'If Not lblEmpChanged Is Nothing Then
                '    If lblEmpChanged.Text = "1" Then
                '        lblDeduction.ForeColor = System.Drawing.Color.Red
                '        lblfurnished.ForeColor = System.Drawing.Color.Red
                '        lblOtherDetails.ForeColor = System.Drawing.Color.Red
                '        lblLeaseEType.ForeColor = System.Drawing.Color.Red
                '        lblLeaseAmount.ForeColor = System.Drawing.Color.Red
                '        lblLeaseType.ForeColor = System.Drawing.Color.Red
                '        lblDepend.ForeColor = System.Drawing.Color.Red
                '        lblMStat.ForeColor = System.Drawing.Color.Red
                '        lbldesig.ForeColor = System.Drawing.Color.Red
                '        lblEmpName.ForeColor = System.Drawing.Color.Red
                '        lblUnitNo.ForeColor = System.Drawing.Color.Red
                '        lblOldRent.ForeColor = System.Drawing.Color.Red
                '        lblHDate.ForeColor = System.Drawing.Color.Red
                '        lblDDate.ForeColor = System.Drawing.Color.Red

                '    End If
                'End If
                If Not txtEmpid Is Nothing Then
                    If ViewState("MainMnu_code") = "PI02050" Then
                        If txtEmpid.Text = "0" Or h_Deleted.Value = True Or lbl_TDH_APR_ID.Text = "0" Then
                            lnkEdit.Enabled = True
                        Else
                            lnkEdit.Enabled = False
                        End If
                        txtEmpid.Visible = False
                    Else
                        lnkEdit.Enabled = False
                    End If
                End If
                If e.Row.RowType = DataControlRowType.Footer Then
                    Dim txtEmployee As TextBox = e.Row.FindControl("txtEmployee")
                    Dim txtDESIGNATION As TextBox = e.Row.FindControl("txtDESIGNATION")
                    Dim txtMSTATUS As TextBox = e.Row.FindControl("txtMSTATUS")
                    Dim txtDEPENDANTS As TextBox = e.Row.FindControl("txtDEPENDANTS")
                    Dim lblDESIGNATION As Label = e.Row.FindControl("lblDESIGNATION")
                    Dim lblMSTATUS As Label = e.Row.FindControl("lblMSTATUS")
                    Dim txtLeaseAmount As TextBox = e.Row.FindControl("txtLeaseAmount")
                    Dim h_Emp_Id As HiddenField = e.Row.FindControl("h_Emp_Id")
                    Dim h_Des_Id As HiddenField = e.Row.FindControl("h_Des_Id")
                    Dim imgEmployee As ImageButton = e.Row.FindControl("imgEmployee")
                    Dim imgDesignation As ImageButton = e.Row.FindControl("imgDesignation")
                    Dim ddlMStatus As DropDownList = e.Row.FindControl("ddlMStatus")
                    txtLeaseAmount.Text = "0.0"
                    txtDeduction.Text = "0.0"
                    txtEAmount.Text = "0.0"
                    txtDEPENDANTS.Text = "0"
                    imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & txtDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "'," & Session("sBsuid") & "','" & ddlMStatus.SelectedValue & "','" & txtDEPENDANTS.ClientID & "','" & h_Des_Id.ClientID & "'); return false;")
                    imgDesignation.Attributes.Add("OnClick", "javascript: getDesignation('" & txtDESIGNATION.ClientID & "','" & h_Des_Id.ClientID & "'); return false;")
                End If
                If grdEmpDetails.ShowFooter Or grdEmpDetails.EditIndex > -1 Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
                    Dim ddlUnitType As DropDownList = e.Row.FindControl("ddlUnitType")
                    Dim ddlEUnitType As DropDownList = e.Row.FindControl("ddlEUnitType")
                    Dim ddlMStatus As DropDownList = e.Row.FindControl("ddlMStatus")
                    Dim ddlfurn As DropDownList = e.Row.FindControl("ddlFurnish")
                    Dim imgEmployee As ImageButton = e.Row.FindControl("imgEmployee")
                    Dim imgDesignation As ImageButton = e.Row.FindControl("imgDesignation")
                    Dim txtEmployee As TextBox = e.Row.FindControl("txtEmployee")
                    Dim h_Emp_Id As HiddenField = e.Row.FindControl("h_Emp_Id")
                    Dim h_Des_Id As HiddenField = e.Row.FindControl("h_Des_Id")
                    Dim txtDESIGNATION As TextBox = e.Row.FindControl("txtDESIGNATION")
                    Dim txtMSTATUS As TextBox = e.Row.FindControl("txtMSTATUS")
                    Dim txtDEPENDANTS As TextBox = e.Row.FindControl("txtDEPENDANTS")
                    Dim lblDESIGNATION As Label = e.Row.FindControl("lblDESIGNATION")
                    Dim lblMSTATUS As Label = e.Row.FindControl("lblMSTATUS")
                    Dim lblDEPENDANTS As Label = e.Row.FindControl("lblDEPENDANTS")

                    If Not ddlUnitType Is Nothing And Not ddlEUnitType Is Nothing And Not ddlfurn Is Nothing And Not ddlMStatus Is Nothing Then
                        Dim sqlStr As String = ""
                        Dim sqlStr1 As String = ""
                        Dim sqlStr2 As String = ""
                        Dim sqlStr3 As String = ""
                        sqlStr &= "select REU_ID,REU_NAME  from REUNITTYPE order by reu_name "
                        sqlStr1 &= "SELECT BSU_SHORTNAME,BSU_ID from oasis..BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "' "
                        sqlStr2 &= "SELECT FURN_DESCR,FURN_ID FROM VW_FURNISHINGMASTER "
                        sqlStr3 &= "select 'MARRIED' ID ,'MARRIED' DESCR UNION ALL select 'SEPARATED' ID ,'SEPARATED' DESCR UNION ALL select 'WIDOWED' ID ,'WIDOWED' DESCR  UNION ALL select 'DIVORCED' ID ,'DIVORCED' DESCR  UNION ALL select 'SINGLE' ID ,'SINGLE' DESCR "
                        fillDropdown(ddlUnitType, sqlStr, "REU_NAME", "REU_ID", False)
                        fillDropdown(ddlEUnitType, sqlStr, "REU_NAME", "REU_ID", False)
                        fillDropdown(ddlfurn, sqlStr2, "FURN_DESCR", "FURN_ID", False)
                        fillDropdown(ddlMStatus, sqlStr3, "DESCR", "ID", False)
                        If txtDESIGNATION Is Nothing Then
                            imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & lblDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & Session("sBsuid") & "','" & ddlMStatus.SelectedValue & "','" & lblDEPENDANTS.ClientID & "','" & h_Des_Id.ClientID & "'); return false;")
                        Else
                            imgEmployee.Attributes.Add("OnClick", "javascript: getEmployee('" & txtEmployee.ClientID & "','" & txtDESIGNATION.ClientID & "','" & h_Emp_Id.ClientID & "','" & Session("sBsuid") & "','" & ddlMStatus.SelectedValue & "','" & txtDEPENDANTS.ClientID & "','" & h_Des_Id.ClientID & "'); return false;")
                        End If
                        imgDesignation.Attributes.Add("OnClick", "javascript: getDesignation('" & txtDESIGNATION.ClientID & "','" & h_Des_Id.ClientID & "'); return false;")
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
        grdEmpDetails.ShowFooter = False
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub
    Protected Sub grdEmpDetails_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles grdEmpDetails.RowDeleted

    End Sub
    Protected Sub grdEmpDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEmpDetails.RowDeleting
        Dim mRow() As DataRow = EmployeeDetails.Select("ID=" & grdEmpDetails.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= mRow(0)("TCE_ID") & ";"
            EmployeeDetails.Select("ID=" & grdEmpDetails.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            EmployeeDetails.AcceptChanges()

        End If
        If EmployeeDetails.Rows.Count = 0 Then
            EmployeeDetails.Rows.Add(EmployeeDetails.NewRow())
            EmployeeDetails.Rows(0)(1) = -1
        Else
        End If
        grdEmpDetails.DataSource = EmployeeDetails
        grdEmpDetails.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdEmpDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEmpDetails.RowEditing
        Try
            grdEmpDetails.ShowFooter = False
            grdEmpDetails.EditIndex = e.NewEditIndex
            grdEmpDetails.DataSource = EmployeeDetails
            grdEmpDetails.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdEmpDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdEmpDetails.RowUpdating
        Dim s As String = grdEmpDetails.DataKeys(e.RowIndex).Value.ToString()
        Dim TDH_id As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblTDH_ID")
        Dim txtEmployee As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtEmployee")
        Dim txtOTHERS As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtOTHERS")
        Dim h_Emp_Id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_Emp_Id")
        Dim h_BSU_Id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_BSU_Id")
        Dim h_Des_Id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_Des_Id")
        Dim h_UnitType As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_UnitType")
        Dim h_EUnitType As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_EUnitType")
        Dim h_Furn_id As HiddenField = grdEmpDetails.Rows(e.RowIndex).FindControl("h_Furn_id")
        Dim txtLeaseAmount As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtLeaseAmount")
        Dim txtEAmount As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtEAmount")
        Dim txtDeduction As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtDeduction")
        Dim ddlEUnitType As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlEUnitType")
        Dim ddlUnitType As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlUnitType")
        Dim ddlBSU As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlBSU")
        Dim ddlMStatus As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlMStatus")
        Dim ddlfurn As DropDownList = grdEmpDetails.Rows(e.RowIndex).FindControl("ddlFurnish")
        Dim txtDESIGNATION As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtDESIGNATION")
        Dim txtMSTATUS As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtMSTATUS")
        Dim txtDEPENDANTS As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtDEPENDANTS")
        Dim lblDEPENDANTS As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblDEPENDANTS")
        Dim lblDESIGNATION As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblDESIGNATION")
        Dim lblMSTATUS As Label = grdEmpDetails.Rows(e.RowIndex).FindControl("lblMSTATUS")
        Dim txtDDATE As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtDDATE")
        Dim txtHDATE As TextBox = grdEmpDetails.Rows(e.RowIndex).FindControl("txtHDATE")
        If grdEmpDetails.ShowFooter Or grdEmpDetails.EditIndex > -1 And Not txtLeaseAmount Is Nothing Then
            If Not IsNumeric(txtLeaseAmount.Text) Then lblError.Text = "Enter numbers only,"
            If txtHDATE.Text = "" Then lblError.Text = lblError.Text & "Enter Hiring Date,"
            If txtDDATE.Text = "" Then lblError.Text = lblError.Text & "Enter DeHiring Date"
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            Dim mrow As DataRow
            mrow = EmployeeDetails.Select("ID=" & s)(0)
            mrow("TDH_LAMOUNT") = txtLeaseAmount.Text
            mrow("TDH_EAMOUNT") = txtEAmount.Text
            mrow("TDH_DAMOUNT") = IIf(txtDeduction.Text = "", 0, txtDeduction.Text)
            mrow("TDH_BSU_ID") = Session("sBsuid")
            mrow("TDH_REU_ID") = ddlUnitType.SelectedValue
            mrow("TDH_EREU_ID") = ddlEUnitType.SelectedValue
            mrow("TDH_FURN_ID") = ddlfurn.SelectedValue
            'mrow("BSUSHORT") = ddlBSU.SelectedItem.Text
            mrow("UNITTYPE") = ddlUnitType.SelectedItem.Text
            mrow("EUNITTYPE") = ddlEUnitType.SelectedItem.Text
            mrow("FURN_DESCR") = ddlfurn.SelectedItem.Text
            mrow("TDH_EMP_ID") = h_Emp_Id.Value
            mrow("EMPNAME") = txtEmployee.Text
            mrow("TDH_DES_ID") = h_Des_Id.Value
            mrow("DESIGNATION") = txtDESIGNATION.Text
            mrow("TDH_ID") = TDH_id.Text
            If txtDEPENDANTS Is Nothing Then
                mrow("TDH_DEPENDANTS") = IIf(lblDEPENDANTS.Text = "", 0, lblDEPENDANTS.Text)
                mrow("TDH_MSTATUS") = lblMSTATUS.Text
            Else
                mrow("TDH_DEPENDANTS") = IIf(txtDEPENDANTS.Text = "", 0, txtDEPENDANTS.Text)
                mrow("TDH_MSTATUS") = ddlMStatus.SelectedItem.Text
            End If
            mrow("TDH_DDATE") = txtDDATE.Text
            mrow("TDH_HDATE") = txtHDATE.Text

            grdEmpDetails.EditIndex = -1
            grdEmpDetails.ShowFooter = True
            grdEmpDetails.DataSource = EmployeeDetails
            grdEmpDetails.DataBind()
        End If
    End Sub
    Protected Sub btnSend_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim RowCount1 As Integer, InsCount1 As Integer = 0
        For RowCount1 = 0 To EmployeeDetails.Rows.Count - 1
            If EmployeeDetails.Rows(RowCount1)("TDH_EMPLOYEE_CHANGED") <> 0 And (EmployeeDetails.Rows(RowCount1)("TDH_MSG_STATUS") = "N" Or EmployeeDetails.Rows(RowCount1)("TDH_MSG_STATUS") = "R") Then
                doSend(0, EmployeeDetails.Rows(RowCount1)("TDH_ID"))
            End If
        Next
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Function doSend(Optional ByVal tch_id As Integer = 0, Optional ByVal tdh_id As Integer = 0) As Integer
        If tch_id = 0 Then tch_id = h_EntryId.Value
        Dim EmpData As String = ""
        Dim Empid As Integer
        Try
            EmpData = Mainclass.getDataValue("select  'Old Employee Details:' + CASE WHEN TCE_EMP_ID=0 THEN 'SCHOOL' ELSE 'EmpNo:'+ cast(E.EMPNO as varchar(10)) + '  Name:'+ E.EMP_NAME END  + '-New Employee Details :'+ CASE WHEN TDH_EMP_ID=0 THEN 'School' ELSE 'EmpNo:' + cast(E1.EMPNO as varchar(10)) + '  Name:'+ E1.EMP_NAME END from TCT_DH LEFT OUTER JOIN tct_e ON TCE_ID=TDH_TCE_ID left outer join oasis_docs..VW_EMPLOYEE_V E on emp_id=tce_emp_id left outer join oasis_docs..VW_EMPLOYEE_V e1 on E1.EMP_ID=TDH_EMP_ID where   TDH_ID='" & tdh_id & "'", "OASIS_PUR_INVConnectionString")
            Empid = Mainclass.getDataValue("select  tdh_emp_id from TCT_DH  where   TDH_ID='" & tdh_id & "'", "OASIS_PUR_INVConnectionString")
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_DH set tdh_msg_status='S' where TDH_ID=" & tdh_id)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_HD_STATUS='S' where TCH_ID=" & tch_id)
            writeWorkFlow(tch_id, "Send", "Sent for " & IIf(Empid = 0, "DeHiring", "Hiring") & " Approval " & EmpData, "0", "H", "Sent For Approval")
        Catch ex As Exception
        End Try
    End Function
    Private Function writeWorkFlow(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String, ByVal WRK_COMMENTS As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TCH_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", WRK_COMMENTS, SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveWorkFlowTCT", iParms)
        writeWorkFlow = iParms(8).Value
    End Function
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim RowCount1 As Integer, InsCount1 As Integer = 0
        For RowCount1 = 0 To EmployeeDetails.Rows.Count - 1
            If EmployeeDetails.Rows(RowCount1)("TDH_EMPLOYEE_CHANGED") <> 0 And EmployeeDetails.Rows(RowCount1)("TDH_MSG_STATUS") = "S" Then
                doApprove(0, EmployeeDetails.Rows(RowCount1)("TDH_ID"), EmployeeDetails.Rows(RowCount1)("TDH_EMP_ID"), EmployeeDetails.Rows(RowCount1)("TDH_TCE_ID"))
            End If
        Next
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Function doApprove(ByVal TCH_ID As Integer, ByVal tdh_id As Integer, ByVal EMP_ID As Integer, ByVal TCE_ID As Integer) As Integer
        If TCH_ID = 0 Then TCH_ID = h_EntryId.Value
        Dim EmpData As String = ""
        Dim Empid As Integer
        Try
            EmpData = Mainclass.getDataValue("select  'Old Employee Details:' + CASE WHEN TCE_EMP_ID=0 THEN 'SCHOOL' ELSE 'EmpNo:'+ cast(E.EMPNO as varchar(10)) + '  Name:'+ E.EMP_NAME END  + '-New Employee Details :'+ CASE WHEN TDH_EMP_ID=0 THEN 'School' ELSE 'EmpNo:' + cast(E1.EMPNO as varchar(10)) + '  Name:'+ E1.EMP_NAME END from TCT_DH LEFT OUTER JOIN tct_e ON TCE_ID=TDH_TCE_ID left outer join oasis_docs..VW_EMPLOYEE_V E on emp_id=tce_emp_id left outer join oasis_docs..VW_EMPLOYEE_V e1 on E1.EMP_ID=TDH_EMP_ID where   TDH_ID='" & tdh_id & "'", "OASIS_PUR_INVConnectionString")
            Empid = Mainclass.getDataValue("select  tdh_emp_id from TCT_DH  where   TDH_ID='" & tdh_id & "'", "OASIS_PUR_INVConnectionString")
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_DH set tdh_msg_status='A' where TDH_ID=" & tdh_id)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_HD_STATUS='' where TCH_ID=" & TCH_ID)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_E set TCE_EMP_ID=" & EMP_ID & " where TCE_ID=" & TCE_ID)
            writeWorkFlow(TCH_ID, "Approved", IIf(Empid = 0, "DeHiring", "Hiring") & " Approved " & EmpData, "0", "H", "Approved")
        Catch ex As Exception
        End Try
    End Function
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim RowCount1 As Integer, InsCount1 As Integer = 0
        For RowCount1 = 0 To EmployeeDetails.Rows.Count - 1
            If EmployeeDetails.Rows(RowCount1)("TDH_EMPLOYEE_CHANGED") <> 0 And EmployeeDetails.Rows(RowCount1)("TDH_MSG_STATUS") = "S" Then
                doReject(0, EmployeeDetails.Rows(RowCount1)("TDH_ID"), EmployeeDetails.Rows(RowCount1)("TDH_EMP_ID"), EmployeeDetails.Rows(RowCount1)("TDH_TCE_ID"))
            End If
        Next
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Function doReject(ByVal TCH_ID As Integer, ByVal tdh_id As Integer, ByVal EMP_ID As Integer, ByVal TCE_ID As Integer) As Integer
        If TCH_ID = 0 Then TCH_ID = h_EntryId.Value
        Dim EmpData As String = ""
        Dim Empid As Integer
        Try
            EmpData = Mainclass.getDataValue("select  'Old Employee Details:' + CASE WHEN TCE_EMP_ID=0 THEN 'SCHOOL' ELSE 'EmpNo:'+ cast(E.EMPNO as varchar(10)) + '  Name:'+ E.EMP_NAME END  + '-New Employee Details :'+ CASE WHEN TDH_EMP_ID=0 THEN 'School' ELSE 'EmpNo:' + cast(E1.EMPNO as varchar(10)) + '  Name:'+ E1.EMP_NAME END from TCT_DH LEFT OUTER JOIN tct_e ON TCE_ID=TDH_TCE_ID left outer join oasis_docs..VW_EMPLOYEE_V E on emp_id=tce_emp_id left outer join oasis_docs..VW_EMPLOYEE_V e1 on E1.EMP_ID=TDH_EMP_ID where   TDH_ID='" & tdh_id & "'", "OASIS_PUR_INVConnectionString")
            Empid = Mainclass.getDataValue("select  tdh_emp_id from TCT_DH  where   TDH_ID='" & tdh_id & "'", "OASIS_PUR_INVConnectionString")
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_DH set tdh_msg_status='R' where TDH_ID=" & tdh_id)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_HD_STATUS='' where TCH_ID=" & TCH_ID)
            writeWorkFlow(TCH_ID, "Rejected", IIf(Empid = 0, "DeHiring", "Hiring") & EmpData, "0", "H", "Rejected")
        Catch ex As Exception
        End Try
    End Function
End Class
