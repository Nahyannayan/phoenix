﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Data.OleDb
Imports Telerik.Web
Imports Telerik.Web.UI


Partial Class DataAdministration
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(lnkExportToPDF)

        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> "PI04015" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ClearAllFields()
            FillReportType()
        End If
    End Sub

    Private Property SPName() As String
        Get
            Return ViewState("SPName")
        End Get
        Set(ByVal value As String)
            ViewState("SPName") = value
        End Set
    End Property

    Private Property GroupBy() As String
        Get
            Return ViewState("GroupBy")
        End Get
        Set(ByVal value As String)
            ViewState("GroupBy") = value
        End Set
    End Property
    Private Property RESULTTYPE_ITEMS() As String
        Get
            Return ViewState("RESULTTYPE_ITEMS")
        End Get
        Set(ByVal value As String)
            ViewState("RESULTTYPE_ITEMS") = value
        End Set
    End Property
    Private Sub ClearAllFields()
        cleargrid()
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call ClearAllFields()
    End Sub
#Region "Reports"
    Sub FillReportType()
        Dim sql, modSql As String
        Dim ds, ModDs, ModList As New DataSet
        sql = "Select * from DataAdministration "
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, sql)
        radFinance.DataSource = ds.Tables(0)
        radFinance.DataTextField = "Title"
        radFinance.DataValueField = "ID"
        radFinance.DataBind()
        modSql = "select distinct Module from DataAdministration where Module<>''"
        Dim dt1 As DataTable = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, modSql).Tables(0)
        RepModule.DataSource = dt1
        RepModule.DataBind()
    End Sub
    Private Sub FillReportData(ByVal bind As Boolean)
        Try
            Dim ConnectionString As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim strSQL As String
            Dim ParamList As DataTable
            If SPName = "" Then Exit Sub
            strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType from information_schema.parameters where PARAMETER_MODE='IN' AND specific_name = '" & SPName & "'"
            ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
            Dim cmd As New SqlCommand
            cmd.Dispose()
            Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString)
            Connection.Open()
            cmd = New SqlCommand(SPName, Connection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim pParms(10) As SqlParameter, Ctr As Int16 = 1
            For Each item As DataListItem In RepFinance.Items
                Dim lblRESDescr As Label = item.FindControl("lblRESDescr")
                Dim txtRESAmount As TextBox = item.FindControl("txtRESAmount")
                If Not IsNothing(lblRESDescr) Then      ' <--- this is the line I changed       
                    If txtRESAmount.Text.Length > 0 Then pParms(Ctr) = Mainclass.CreateSqlParameter("@" & lblRESDescr.Text, txtRESAmount.Text, SqlDbType.VarChar)
                    Ctr += 1
                Else
                End If
            Next            
            pParms(Ctr) = Mainclass.CreateSqlParameter("@ACTION", "V", SqlDbType.VarChar)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SPName, pParms)
            If ds IsNot Nothing Then
                If ds.Tables.Count > 0 Then
                    For i As Integer = 0 To ds.Tables.Count - 1
                        If i = 0 Then
                            Grid1.Visible = True
                            RadGridReport.DataSource = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SPName, pParms).Tables(0)
                            RadGridReport.DataBind()

                            If ds.Tables(0).Rows.Count > 0 Then
                                btnUpdate.Visible = True
                            Else
                                btnUpdate.Visible = False
                            End If

                        ElseIf i = 1 Then
                            Grid2.Visible = True
                            RadGridReport1.DataSource = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SPName, pParms).Tables(1)
                            RadGridReport1.DataBind()

                        ElseIf i = 2 Then
                            Grid3.Visible = True
                            RadGridReport2.DataSource = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SPName, pParms).Tables(2)
                            RadGridReport2.DataBind()
                        End If
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        FillReportData(True)
    End Sub
    Private Sub SetFilters()
        Try
            Dim ConnectionString As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim strSQL As String
            Dim ParamList As DataTable
            If SPName = "" Then Exit Sub
            strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType,''TEXTBOXVALUE,right(parameter_name,len(parameter_name)-1)FieldName from information_schema.parameters where PARAMETER_MODE='IN' AND specific_name = '" & SPName & "' and replace(replace(replace(parameter_name,'@',''),' ',''),'_','') not in ('ACTION')"
            ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
            Dim dt As DataTable = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, strSQL).Tables(0)
            RepFinance.DataSource = dt
            RepFinance.DataBind()
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub RadGridReport_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGridReport.NeedDataSource
        FillReportData(False)
    End Sub
    Protected Sub RadGridReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGridReport.ItemDataBound
        If TypeOf e.Item Is GridGroupHeaderItem Then
            Dim groupHeader As GridGroupHeaderItem = TryCast(e.Item, GridGroupHeaderItem)
            groupHeader.DataCell.Text = groupHeader.DataCell.Text.Split(":")(1).ToString()
        End If
    End Sub
    Protected Sub lnkExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportToExcel.Click
        RadGridReport.ExportSettings.ExportOnlyData = True
        RadGridReport.ExportSettings.IgnorePaging = True
        RadGridReport.ExportSettings.OpenInNewWindow = True
        RadGridReport.MasterTableView.ExportToExcel()
    End Sub
    Protected Sub lnkExportToPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportToPDF.Click
        RadGridReport.ExportSettings.ExportOnlyData = True
        RadGridReport.ExportSettings.IgnorePaging = True
        RadGridReport.ExportSettings.OpenInNewWindow = True
        RadGridReport.MasterTableView.ExportToPdf()
    End Sub
    Protected Sub radFinance_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radFinance.SelectedIndexChanged
        cleargrid()
    End Sub
#End Region
    Private Sub cleargrid()
        Try
            Dim ReportType As String
            If radFinance.SelectedItem IsNot Nothing Then
                ReportType = radFinance.SelectedItem.Value
                If ReportType = "0" Then
                    SPName = ""
                    GroupBy = ""
                Else
                    Dim sql As String
                    Dim ds As New DataTable
                    sql = "Select * from DataAdministration where ID='" & ReportType & "'"
                    ds = Mainclass.getDataTable(sql, ConnectionManger.GetOASIS_PUR_INVConnectionString)
                    If ds.Rows.Count > 0 Then
                        SPName = ds.Rows(0).Item("SPNAME").ToString
                        GroupBy = ds.Rows(0).Item("GroupBy").ToString
                        RESULTTYPE_ITEMS = ds.Rows(0).Item("ResultType").ToString
                    End If
                End If
                Dim i As Int16
                For i = 0 To RadGridReport.MasterTableView.GroupByExpressions.Count - 1
                    RadGridReport.MasterTableView.GroupByExpressions.RemoveAt(i)
                Next
                For i = 0 To RadGridReport1.MasterTableView.GroupByExpressions.Count - 1
                    RadGridReport1.MasterTableView.GroupByExpressions.RemoveAt(i)
                Next

                For i = 0 To RadGridReport2.MasterTableView.GroupByExpressions.Count - 1
                    RadGridReport2.MasterTableView.GroupByExpressions.RemoveAt(i)
                Next
                Me.RadGridReport.AllowPaging = False
                Me.RadGridReport.DataSource = ""
                Me.RadGridReport.DataBind()
                Me.RadGridReport.AllowPaging = True
                Me.RadGridReport1.AllowPaging = False
                Me.RadGridReport1.DataSource = ""
                Me.RadGridReport1.DataBind()
                Me.RadGridReport1.AllowPaging = True
                Me.RadGridReport2.AllowPaging = False
                Me.RadGridReport2.DataSource = ""
                Me.RadGridReport2.DataBind()
                Me.RadGridReport2.AllowPaging = True
                Grid1.Visible = False
                Grid2.Visible = False
                Grid3.Visible = False
                SetFilters()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim pParms(10) As SqlParameter, Ctr As Int16 = 1, parmStr As String = ""
        Dim ConnectionString As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        For Each item As DataListItem In RepFinance.Items
            Dim lblRESDescr As Label = item.FindControl("lblRESDescr")
            Dim txtRESAmount As TextBox = item.FindControl("txtRESAmount")
            If Not IsNothing(lblRESDescr) Then      ' <--- this is the line I changed       
                pParms(Ctr) = Mainclass.CreateSqlParameter("@" & lblRESDescr.Text, txtRESAmount.Text, SqlDbType.VarChar)
                parmStr &= lblRESDescr.Text & "," & txtRESAmount.Text & "|"
                Ctr += 1
            Else
            End If
        Next
        pParms(Ctr) = Mainclass.CreateSqlParameter("@ACTION", "U", SqlDbType.VarChar)
        Try
            operOnAudiTable(SPName, "U", parmStr, Page.User.Identity.Name.ToString)
            SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, SPName, pParms)
            lblError.Text = "Update completed successfully"
            FillReportData(True)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Function operOnAudiTable(ByVal aud_form As String, ByVal aud_action As String, ByVal aud_remarks As String, _
            Optional ByVal userInfo As String = "", Optional ByVal ocontrol As Control = Nothing) As Integer
        Dim aud_remark As String = ""
        Dim Encr_decrData As New Encryption64
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@Aud_user", HttpContext.Current.Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@Aud_bsu_id", HttpContext.Current.Session("sBsuid"))
                pParms(3) = New SqlClient.SqlParameter("@Aud_form", aud_form)
                pParms(5) = New SqlClient.SqlParameter("@Aud_action", aud_action)
                pParms(6) = New SqlClient.SqlParameter("@Aud_remarks", aud_remark & " " & aud_remarks)
                pParms(7) = New SqlClient.SqlParameter("@AUD_HOST", HttpContext.Current.Request.UserHostAddress.ToString()) 'host.HostName)
                pParms(8) = New SqlClient.SqlParameter("@AUD_WINUSER", userInfo)
                pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue
                Dim ConnectionString As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.SaveAudit", pParms)
                Dim ReturnFlag As Integer = pParms(9).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            Return -1
        End Try
    End Function
    Protected Sub LinkButton1_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim sql As String
        Dim ds As New DataSet
        cleargrid()
        If e.CommandName = "MyUpdate" Then
            For Each RI As DataListItem In RepModule.Items
                Dim MyLabel As Label = TryCast(RI.FindControl("lblModDescr"), Label)
                Dim MyLnkSel As LinkButton = TryCast(RI.FindControl("lnkSelect"), LinkButton)
                If MyLabel IsNot Nothing Then
                    If MyLabel.Text = e.CommandArgument Then
                        MyLnkSel.ForeColor = System.Drawing.Color.Blue
                        sql = "Select * from DataAdministration where module='" & MyLabel.Text & "' union all Select * from DataAdministration where module='' order by TITLE"
                        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, sql)
                        radFinance.DataSource = ds.Tables(0)
                        radFinance.DataTextField = "Title"
                        radFinance.DataValueField = "ID"
                        radFinance.DataBind()
                        ClearRepeater()
                    Else
                        MyLnkSel.ForeColor = System.Drawing.Color.FromArgb(1, 107, 5)
                    End If
                End If
            Next
        End If
    End Sub
    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Private Sub ClearRepeater()
        RepFinance.DataSource = Nothing
        RepFinance.DataSourceID = Nothing
        RepFinance.DataBind()
        radFinance.SelectedValue = 0
        btnUpdate.Visible = False
    End Sub
End Class
