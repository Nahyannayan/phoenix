﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic

Partial Class Inventory_PurchaseRequest
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass(), requestForQuotation As Boolean = False
    Public FyearClosed As Boolean = False
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePageMethods = True
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnUpload)
        smScriptManager.RegisterPostBackControl(btnUploadPrf)

        'If Not ViewState("ActiveTabIndex") Is Nothing Then TabContainer1.ActiveTabIndex = ViewState("ActiveTabIndex")

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            Else
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            Dim SEACount As Integer = 0

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "PI02003" And ViewState("MainMnu_code") <> "PI02004" And ViewState("MainMnu_code") <> "PI02005" And ViewState("MainMnu_code") <> "PI02006" And ViewState("MainMnu_code") <> "PI02007" And ViewState("MainMnu_code") <> "PI04005" And ViewState("MainMnu_code") <> "PI04007" And ViewState("MainMnu_code") <> "PI02012" And ViewState("MainMnu_code") <> "PI02016" And ViewState("MainMnu_code") <> "PI02014" And ViewState("MainMnu_code") <> "PI02020" And ViewState("MainMnu_code") <> "HD02020" And ViewState("MainMnu_code") <> "PI02018") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If ViewState("MainMnu_code") = "PI02003" Then
                ViewState("MainMnu_code") = "PI02005"
                isList = True
            End If
            If ViewState("MainMnu_code") = "PI02004" Then
                ViewState("MainMnu_code") = "PI02006"
                isList = True
            End If
            If ViewState("MainMnu_code") = "PI02007" Then isList = True
            isFramework = (ViewState("MainMnu_code") = "PI02005")
            isQuotation = False
            isSEA = (Session("sBSUID") = "900201" Or Session("sBSUID") = "800555" Or Session("sBSUID") = "315888")


            SEACount = Mainclass.getDataValue("select count(*) from VW_ISSEA_BSUS where bsu_id='" & Session("sBSUID") & "'", "OASIS_PUR_INVConnectionString")



            If SEACount > 0 Then
                isSEA = True
            Else
                isSEA = False
            End If

            isCAPEX = ViewState("MainMnu_code") = "HD02020"
            Select Case ViewState("MainMnu_code")
                Case "HD02020"
                    pgTitle.Text = "Capex Works"
                Case "PI02005"
                    pgTitle.Text = "Framework Purchase Request Form"
                Case "PI02006", "PI02012"
                    pgTitle.Text = "Non Framework Purchase Request Form"
                Case "PI02007"
                    pgTitle.Text = "CAPEX Purchase Request Form"
                Case "PI04005"
                    pgTitle.Text = "Framework Purchase Request Form Approval"
                Case "PI04007"
                    pgTitle.Text = "Non Framework Purchase Request Form Approval"
                Case "PI02014"
                    pgTitle.Text = "Purchase Journal Voucher"
                Case "PI02020"
                    pgTitle.Text = "Purchase Change Request"
            End Select
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            If Not Session("prf_id") Is Nothing Then
                If Session("prf_id") > 0 Then
                    ViewState("EntryId") = Session("prf_id")
                    Session("prf_id") = 0
                End If
            End If


            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
            End If

            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdPRF.DataSource = PRFFooter
            grdPRF.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            'grdPRF.DataSource = PRFFooter
            'grdPRF.DataBind()
            hGridRefresh.Value = 0
            showNoRecordsFound()
            If hActRefresh.Value = 1 Then GetActuals()
        End If

    End Sub
    'Protected Sub hGridRefresh_ValueChanged(sender As Object, e As EventArgs) Handles hGridRefresh.ValueChanged


    '    hGridRefresh.Value = 0
    '    showNoRecordsFound()
    '    If hActRefresh.Value = 1 Then GetActuals()

    'End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        trChangeRequest1.Visible = False
        trChangeRequest2.Visible = False
        trSupplier.Visible = isFramework Or isQuotation Or isSEA Or isCAPEX
        'lblTotal.Visible = isFramework Or isQuotation
        'txtTotal.Visible = isFramework Or isQuotation
        grdPRF.Columns(7).Visible = Not mDisable
        grdPRF.Columns(8).Visible = Not mDisable
        grdPRF.ShowFooter = Not mDisable

        txtOldNo.Visible = False
        txtContact.Enabled = EditAllowed
        txtSupplierAddress.Enabled = EditAllowed
        txtShipment.Enabled = EditAllowed
        txtProcurement.Enabled = EditAllowed
        txtSupplier.Enabled = EditAllowed And txtSupplier.Text.Length = 0
        txtDELDate.Enabled = EditAllowed
        lnkDELDate.Visible = EditAllowed
        radBudgeted.Enabled = EditAllowed
        radUnBudgeted.Enabled = EditAllowed
        p_Budgeted = True
        ddlPriority.Enabled = EditAllowed
        ddlDepartment.Enabled = EditAllowed
        ddlCurrency.Enabled = EditAllowed And Not isFramework 'And Not isSEA
        txtExgRate.Enabled = EditAllowed And Not isFramework 'And Not isSEA
        txtAltTotal.Enabled = EditAllowed And Not isFramework 'And Not isSEA
        txtComments.ReadOnly = Not EditAllowed
        txtACTID.Enabled = False
        imgActid.Enabled = EditAllowed
        ddlBSU.Enabled = False 'EditAllowed And (ViewState("datamode") = "add")
        txtTerms.Enabled = EditAllowed And (ViewState("datamode") = "add") And (isSEA Or isCAPEX)
        UploadFile.Visible = False '(ViewState("datamode") = "add")
        btnUploadPrf.Visible = False '(ViewState("datamode") = "add")
        btnExport.Visible = False '(ViewState("datamode") <> "add")
        btnCopy.Visible = mDisable
        btnSend.Visible = mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable 'Or ViewState("MainMnu_code") = "U000086"
        btnEdit.Visible = mDisable
        btnPrint.Visible = Not btnSave.Visible
        btnAccept.Visible = mDisable And (ViewState("MainMnu_code") = "PI04005" Or ViewState("MainMnu_code") = "PI04007") 'approval menu
        btnReject.Visible = btnAccept.Visible
        btnMessage.Visible = btnAccept.Visible
        trButtons.Visible = btnAccept.Visible
        btnBypass.Visible = btnAccept.Visible
        btnBypassTop.Visible = btnBypass.Visible
        btnAdd.Visible = mDisable And Not (ViewState("MainMnu_code") = "PI04005" Or ViewState("MainMnu_code") = "PI04007")
        imgClient.Visible = ViewState("datamode") = "add"
        lblGreen.Visible = False
        lblRed.Visible = False
        ddlTAX.Enabled = EditAllowed 'And Not isFramework
    End Sub

    Private Sub BindTAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim BSUParms(2) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            ddlTAX.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "getTAXLISTFORPURCHASE", BSUParms)
            ddlTAX.DataTextField = "TAX_DESCR"
            ddlTAX.DataValueField = "TAX_CODE" '"TAX_PERC_VALUE"
            ddlTAX.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim sqlStr As New StringBuilder
            Dim sqlWhere As String = p_Modifyid
            h_EntryId.Value = p_Modifyid
            If isCAPEX Then
                fillDropdown(ddlPriority, "select pri_Id, pri_descr  from priorityCapex where pri_id > 2", "pri_descr", "pri_Id", False)
                lblPriority.Text = "Type"
            Else
                fillDropdown(ddlPriority, "select pri_Id, pri_descr  from priorityCapex where pri_id in (1,2)", "pri_descr", "pri_Id", False)
            End If
            'fillDropdown(ddlCurrency, "select CUR_ID, CUR_ID from oasisfin..CURRENCY_M where cur_id in ('AED','GBP','FRF','EUR','USD', 'SGD', 'CHF', 'NZD','QAR','MYR','GIP','JOD') ", "CUR_ID", "CUR_ID", False)
            'commented and added by mahesh on 12-Sep-2018
            fillDropdown(ddlCurrency, "select CUR_ID, CUR_ID from oasisfin..CURRENCY_M where cur_id in (select CUR_ID from  VW_LOADDEFAULTCURRENCIES) ", "CUR_ID", "CUR_ID", False)
            BindTAXCodes()

            fillDropdown(ddlStatus, "select prt_Id, prt_descr  from prftype", "prt_descr", "prt_Id", False)
            If requestForQuotation Or p_Modifyid > 0 Then
                fillDropdown(ddlBSU, "select BSU_ID , BSU_NAME from oasis.dbo.BUSINESSUNIT_M WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME", "bsu_Name", "bsu_Id", False)
            Else
                fillDropdown(ddlBSU, "select BSU_ID , BSU_NAME from oasis.dbo.fn_GetBusinessUnits " & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME", "bsu_Name", "bsu_Id", False)
            End If
            ddlBSU.SelectedValue = Session("sBsuid")
            If ViewState("MainMnu_code") = "PI02005" Then
                h_PRF_Type.Value = "F"
            ElseIf isCAPEX Then
                h_PRF_Type.Value = "C"
            Else
                h_PRF_Type.Value = "N"
                hTPT_Client_ID.Value = "none"
            End If

            If p_Modifyid = 0 Then
                'Dim emp_Details() As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnection, CommandType.Text, sqlStr.ToString).ToString.Split(";")
                sqlStr = New StringBuilder
                sqlStr.Append("SELECT cast(EMP_ID AS varchar)+'|'+isnull(rtrim(EMP_DISPLAYNAME),rtrim(EMP_FNAME)+' '+rtrim(EMP_MNAME)+' '+rtrim(EMP_LNAME))+'|P.O BOX :'+rtrim(BSU_POBOX)+', ' +upper(rtrim(BSU_CITY))+'|Tel :'+rtrim(BSU_TEL)+', '+'Fax :'  + rtrim(BSU_FAX)+', ' +'|Email :'+rtrim(USRA_EMAIL)+'|'+BSU_CURRENCY+'|'+cast(isnull(usra_subdpt_id,0) as varchar) ")
                sqlStr.Append("FROM EMPLOYEE_M innner JOIN EMPDESIGNATION_M on EMP_DES_ID=DES_ID ")
                sqlStr.Append("cross join BUSINESSUNIT_M inner join oasis_pur_inv..USERSA on usra_bsu_id=bsu_id and usra_email is not null and bsu_id='" & Session("sBsuid") & "' ")
                If Session("sBsuid") = "999998" Or Session("sBsuid") = "800030" Or Session("sBsuid") = "900001" Then sqlStr.Append("and usra_usr_name='" & Session("sUsr_name") & "'")
                sqlStr.Append("where EMP_ID = " & Session("EmployeeId") & "And usra_grp_id = 381")
                Try
                    Dim emp_Details() As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnection, CommandType.Text, sqlStr.ToString).ToString.Split("|")
                    h_empid.Value = emp_Details(0)
                    txtContact.Text = emp_Details(1) & vbCrLf & emp_Details(2) & vbCrLf & emp_Details(3) & vbCrLf & emp_Details(4)
                    h_PRF_Cur_Id.Value = emp_Details(5)
                    ddlCurrency.SelectedValue = h_PRF_Cur_Id.Value
                    If Session("sBsuid") = "999998" Or Session("sBsuid") = "800030" Or Session("sBsuid") = "900001" Then
                        fillDropdown(ddlDepartment, "select cct_Id, cct_descr  from oasisfin..costcenter_m where cct_id in (select usra_dpt_id from usersa where usra_bsu_id='" & Session("sBsuid") & "' and usra_usr_name='" & Session("sUsr_name") & "')", "cct_descr", "cct_Id", False)
                        h_Dpt_id.Value = ddlDepartment.SelectedValue
                        h_SubDpt_id.Value = emp_Details(6)
                        ddlDepartment.Visible = True
                        lblDepartment.Visible = True
                    End If
                Catch ex As Exception
                    Response.Redirect(ViewState("ReferrerUrl"))
                End Try
                trDocument.Visible = False
                trApproval.Visible = False
                trEmail.Visible = False
                trWorkflow.Visible = False
                trFlow.Visible = False
                trDocument.HeaderText = ""
                trApproval.HeaderText = ""
                trEmail.HeaderText = ""
                trWorkflow.HeaderText = ""
                trFlow.HeaderText = ""
                If isCAPEX Then
                    txtSupplierAddress.Text = "Chicago Maintenance & Construction, P.O.Box(27499), Dubai, UAE"
                    txtSupplier.Text = "CHICAGO MAINT. & CONSTRUCTION CO. (LLC)"
                    hTPT_Client_ID.Value = "063C0053"
                    txtTerms.Text = "90 days"
                    h_term.Value = "90 days"
                End If
                btnQuotation.Visible = False
            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                sqlStr = New StringBuilder
                sqlStr.Append("select isnull(xBUDMTD,0) xBUDMTD, isnull(xBUDYTD,0) xBUDYTD, isnull(A1.ACT_NAME,'none') ACT_NAME, A2.ACT_NAME BUDGET, isnull(A1.ACT_EMAIL,'') ACT_EMAIL, isnull(CCT_DESCR,'') CCT_DESCR, ")
                sqlStr.Append("isnull((select xACTYTD from vw_PRF_ACTUALBak where VW_ACT_ID=PRF_ACT_ID AND BSU_ID=PRF_BSU_ID AND FYEAR=PRF_FYEAR AND MTH=month(PRF_DATE) AND DPT_ID=PRF_DPT_ID),0) xACTYTD, ")
                sqlStr.Append("(select top 1 byr_descr from byr_d inner join byr_h on byd_byr_id=byr_id where byd_bsu_id=prf_bsu_id) PRF_BUYER, case when isnull(usra_id,0)>0 and prf_request='' then 1 else 0 end isMSO, ")
                sqlStr.Append("(select stuff((select WRK_USERNAME+':'+WRK_DETAILS+'|' from WORKFLOW where WRK_PRF_ID=PRF_ID and WRK_STATUS='M' for xml path(''), type).value('.','varchar(max)'),1,0,'')) PRF_APR_COMMENTS, ")
                sqlStr.Append("replace(convert(varchar(30), prf_Date, 106),' ','/') PRF_DATE_STR, replace(convert(varchar(30), PRF_DELDATE, 106),' ','/')  PRF_DELDATE_STR, case when grn_pjv_no is null then 0 else 1 end isPJV, ")
                sqlStr.Append("isnull(prf_GROSS,0) PRF_GROSS, isnull(PRF_TAX_AMOUNT,0)PRF_TAX_AMOUNT, PRF_TAX,isnull(T.TAX_PERC_VALUE,0)TAX_PERC_VALUE,")

                sqlStr.Append("PRF_H.* from PRF_H left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M A1 on A1.ACT_ID=PRF_SUP_ID ")
                sqlStr.Append("inner JOIN OASISFIN..ACCOUNTS_M A2 on A2.ACT_ID=PRF_ACT_ID left OUTER JOIN vw_PRF_ACTUALBak on VW_ACT_ID=PRF_H.PRF_ACT_ID ")
                sqlStr.Append("AND BSU_ID=PRF_BSU_ID AND FYEAR=PRF_FYEAR AND MTH=month(PRF_DATE) AND PRF_DPT_ID=DPT_ID LEFT OUTER JOIN USERSA on USRA_USR_NAME='" & Session("sUsr_name") & "' and usra_grp_id=20 ")
                sqlStr.Append("LEFT OUTER JOIN OASISFIN..COSTCENTER_M CCT on CCT.CCT_ID=PRF_DPT_ID ")
                sqlStr.Append("LEFT OUTER JOIN oasis.TAX.TAX_CODES_M T on T.TAX_CODE=PRF_TAX ")
                'select TAX_CODE,TAX_PERC_VALUE from oasis.TAX.TAX_CODES_M where TAX_CTY_ID=6
                sqlStr.Append("left outer join grn_h on prf_id=grn_prf_id where PRF_ID = " & sqlWhere)
                dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("PRF_STATUS") = "N" And isList Then Response.Redirect(Request.UrlReferrer.ToString())
                    If sqlWhere.Length > 0 Then
                        h_EntryId.Value = dt.Rows(0)("PRF_ID")
                    End If
                    Prf_Id_New = dt.Rows(0)("PRF_ID")
                    txtComments.Text = dt.Rows(0)("PRF_REMARKS").ToString.Replace(Convert.ToChar(10), " ") & vbCrLf & dt.Rows(0)("PRF_APR_COMMENTS").ToString.Replace("|", vbCrLf).ToString.Replace(Convert.ToChar(10), " ")
                    txtContact.Text = dt.Rows(0)("PRF_CONTACT")
                    txtDELDate.Text = dt.Rows(0)("PRF_DELDATE_STR")
                    txtProcurement.Text = dt.Rows(0)("PRF_PROCUREMENT")
                    txtSupplierAddress.Text = dt.Rows(0)("PRF_SUPPLIER")
                    txtPRFDate.Text = dt.Rows(0)("PRF_DATE_STR")
                    txtACTID.Text = dt.Rows(0)("BUDGET")
                    h_Actid.Value = dt.Rows(0)("PRF_ACT_ID")
                    h_PRF_Cur_Id.Value = dt.Rows(0)("PRF_CUR_ID")
                    p_Actid = dt.Rows(0)("PRF_ACT_ID")
                    txtPRFNo.Text = dt.Rows(0)("PRF_NO")
                    txtOldNo.Text = dt.Rows(0)("PRF_OLDNO")
                    txtSupplier.Text = dt.Rows(0)("ACT_NAME")
                    hTPT_Client_ID.Value = dt.Rows(0)("PRF_SUP_ID")
                    h_term.Value = dt.Rows(0)("PRF_TERMS")
                    txtTerms.Text = dt.Rows(0)("PRF_TERMS")
                    txtTotal.Text = dt.Rows(0)("PRF_TOTAL")
                    ddlBSU.SelectedValue = dt.Rows(0)("PRF_BSU_ID")
                    radBudgeted.Checked = (dt.Rows(0)("PRF_BUDGETED") = 0)
                    radUnBudgeted.Checked = (dt.Rows(0)("PRF_BUDGETED") = 1)
                    p_Budgeted = (dt.Rows(0)("PRF_BUDGETED") = 0)
                    txtBudMtd.Text = dt.Rows(0)("xBUDMTD")
                    txtBudYtd.Text = dt.Rows(0)("xBUDYTD")
                    txtActMtd.Text = dt.Rows(0)("xACTYTD")
                    txtActYtd.Text = dt.Rows(0)("xACTYTD")
                    isMSO = dt.Rows(0)("isMSO")
                    isPJV = dt.Rows(0)("isPJV")
                    If dt.Rows(0)("PRF_ADJUST") > 0 Then lblPrf_Adjust.Text = "PRF Adjustment:" & dt.Rows(0)("PRF_ADJUST")
                    If (dt.Rows(0)("xBUDYTD") - dt.Rows(0)("xACTYTD")) >= 0 Then
                        lblGreen.Text = Convert.ToDouble(dt.Rows(0)("xBUDYTD") - dt.Rows(0)("xACTYTD")).ToString("#,###,##0.00")
                        lblGreen.Visible = True
                        lblRed.Visible = False
                    Else
                        lblRed.Text = Convert.ToDouble(dt.Rows(0)("xBUDYTD") - dt.Rows(0)("xACTYTD")).ToString("#,###,##0.00")
                        lblRed.Visible = True
                        lblGreen.Visible = False
                    End If
                    h_PRF_Type.Value = dt.Rows(0)("PRF_TYPE")
                    isCAPEX = (h_PRF_Type.Value = "C")
                    If isCAPEX Then
                        fillDropdown(ddlPriority, "select pri_Id, pri_descr  from priorityCapex where pri_id > 2", "pri_descr", "pri_Id", False)
                        lblPriority.Text = "Type"
                        pgTitle.Text = "Capex Works"
                    End If
                    h_QUO_ID1.Value = dt.Rows(0)("QUO_ID1")
                    h_QUO_ID2.Value = dt.Rows(0)("QUO_ID2")
                    h_QUO_ID3.Value = dt.Rows(0)("QUO_ID3")
                    h_Prf_apr_id.Value = dt.Rows(0)("PRF_APR_ID")
                    ddlPriority.SelectedValue = dt.Rows(0)("PRF_PRI_ID")
                    p_Priority = dt.Rows(0)("PRF_PRI_ID")
                    ddlStatus.SelectedValue = dt.Rows(0)("PRF_TYP_ID")
                    txtShipment.Text = dt.Rows(0)("PRF_SHIPMENT")
                    h_Prf_Status.Value = dt.Rows(0)("PRF_STATUS")
                    ddlCurrency.SelectedValue = dt.Rows(0)("PRF_ALT_CUR_ID")
                    txtExgRate.Text = dt.Rows(0)("PRF_EXGRATE")
                    lblExgCurr.Text = "Total Amount (" & ddlCurrency.SelectedItem.Value & ")"
                    txtAltTotal.Text = dt.Rows(0)("PRF_ALT_TOTAL")
                    hdnAltTotal.Value = dt.Rows(0)("PRF_ALT_TOTAL")
                    h_Dpt_id.Value = dt.Rows(0)("PRF_DPT_ID")
                    h_SubDpt_id.Value = dt.Rows(0)("PRF_SUBDPT_ID")

                    'Mahesh

                    txtGross.Text = dt.Rows(0)("PRF_GROSS")
                    txtTax.Text = dt.Rows(0)("PRF_TAX_AMOUNT")

                    h_TAXPerc.Value = dt.Rows(0)("TAX_PERC_VALUE")
                    ddlTAX.SelectedValue = dt.Rows(0)("PRF_TAX") 'dt.Rows(0)("TAX_PERC_VALUE")
                    



                    If h_Dpt_id.Value <> "0" Then
                        fillDropdown(ddlDepartment, "select cct_Id, cct_descr from oasisfin..costcenter_m where cct_id=" & h_Dpt_id.Value, "cct_descr", "cct_Id", False)
                        ddlDepartment.SelectedValue = h_Dpt_id.Value
                        ddlDepartment.Visible = True
                        lblDepartment.Visible = True
                    End If

                    If h_PRF_Type.Value = "N" AndAlso dt.Rows(0)("PRF_SUP_ID").ToString <> "none" Then
                        trSupplier.Visible = True
                        'txtTotal.Visible = True
                        'lblTotal.Visible = True
                    ElseIf h_PRF_Type.Value = "N" Then
                        lblBuyer.Text = "Buyer:" & dt.Rows(0)("PRF_BUYER")
                    End If
                    ddlStatus.Enabled = False

                    isQuotation = (h_PRF_Type.Value = "Q") Or requestForQuotation
                    isFramework = (h_PRF_Type.Value = "F")
                    'btnUploadPrf.Visible = isQuotation
                    'UploadFile.Visible = isQuotation
                    trSupplier.Visible = isFramework Or isQuotation Or h_QUO_ID3.Value > 0 Or isSEA Or isCAPEX
                    'txtTotal.Visible = isFramework Or isQuotation Or h_QUO_ID3.Value > 0
                    'lblTotal.Visible = isFramework Or isQuotation Or h_QUO_ID3.Value > 0

                    'dt.Rows(0)("PRF_STATUS") N-NEW, S-SENT FOR APPROVAL, D-DELETED, R-REJECTED, B-BUYER, Q-QUOTATION, P-WAITING FOR PRINT, C-COMPLETE
                    btnAccept.Visible = btnAccept.Visible And dt.Rows(0)("PRF_STATUS") = "S"
                    btnReject.Visible = btnAccept.Visible
                    btnBypass.Visible = btnAccept.Visible And Session("sroleid") = 344
                    btnBypassTop.Visible = btnBypass.Visible
                    ddlPriority.Enabled = btnAccept.Visible
                    btnSend.Visible = btnSend.Visible And (dt.Rows(0)("PRF_STATUS") = "N")
                    btnSave.Visible = btnSave.Visible And ((dt.Rows(0)("PRF_STATUS") = "N") Or requestForQuotation)
                    btnPrint.Visible = Not btnSave.Visible
                    btnEdit.Visible = btnEdit.Visible And (dt.Rows(0)("PRF_STATUS") = "N" Or dt.Rows(0)("PRF_STATUS") = "Q") And dt.Rows(0)("PRF_APR_ID") = 0 And (ViewState("MainMnu_code") = "PI02005" Or ViewState("MainMnu_code") = "PI02006" Or isCAPEX)
                    btnCopy.Visible = (Session("F_YEAR") >= "2016") And btnCopy.Visible And Not isList And Not (ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02014" Or ViewState("MainMnu_code") = "PI02016" Or ViewState("MainMnu_code") = "PI04005" Or ViewState("MainMnu_code") = "PI04007")
                    btnAdd.Visible = btnCopy.Visible
                    btnDelete.Visible = (("QN").Contains(dt.Rows(0)("PRF_STATUS")) And btnDelete.Visible)
                    If isCAPEX Then
                        If h_Prf_apr_id.Value >= 30 Then
                            ddlCurrency.Visible = False
                            txtExgRate.Visible = False
                            lblExchRate.Visible = False
                            lblCurrency.Text = "Supplier Estimate"
                            lblExgCurr.Text = ""
                            txtAltTotal.Enabled = (h_Prf_apr_id.Value = 30)
                            If h_Prf_apr_id.Value > 30 Then
                                Dim varAmt As Decimal = Val(txtAltTotal.Text) - Val(txtTotal.Text)
                                lblExgCurr.Text = "(Variance:" & (varAmt).ToString("#,###,##0.00") & ")"
                                lblExgCurr.ForeColor = IIf(varAmt > 0, Drawing.Color.Red, Drawing.Color.Green)
                            End If
                        End If
                    End If
                    If (dt.Rows(0)("PRF_APR_ID") = 380 And (ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02016")) Then h_printed.Value = 1
                    btnBuyer.Visible = False
                    btnQuotation.Visible = False
                    btnConfirm.Visible = dt.Rows(0)("PRF_STATUS") = "Q"
                    'with buyer for quotation
                    If dt.Rows(0)("PRF_APR_ID") = 200 And Not requestForQuotation And (ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02016") Then
                        btnBuyer.Visible = True
                        If dt.Rows(0)("QUO_ID1") = "0" Then
                            btnBuyer.Text = "Accept"
                        Else
                            btnBuyer.Text = "Release"
                            btnQuotation.Visible = True
                        End If
                    End If
                    btnMessage.Visible = btnAccept.Visible Or btnQuotation.Visible Or h_Prf_Status.Value = "S"
                    trBuyer.Visible = (ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02016")
                    'btnSuper.Visible = dt.Rows(0)("SuperUser") = 1 And isList And (dt.Rows(0)("prf_status") = "C" Or dt.Rows(0)("prf_status") = "S")

                    If Not isQuotation And h_EntryId.Value > 0 Then 'Not btnDelete.Visible
                        Dim ds As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "workflowqueryMain " & h_EntryId.Value).Tables(0)
                        rptFlow.DataSource = ds
                        rptFlow.DataBind()
                        trFlow.Visible = True
                        trFlow.HeaderText = "Workflow progress"
                    End If

                    If (ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02016" And ViewState("MainMnu_code") = "PI02007") And hTPT_Client_ID.Value = "none" Then '
                        If hTPT_Client_ID.Value = "none" Then
                            trEmail.Visible = True
                            trEmail.HeaderText = "Email Quotation"
                        End If
                        grdPRF.ShowFooter = False
                    ElseIf ViewState("MainMnu_code") = "PI02005" Or ((ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02016" Or ViewState("MainMnu_code") = "PI02006") And hTPT_Client_ID.Value <> "none") Then
                        pgTitle.Text = "Procurement Requisition " & IIf(h_PRF_Type.Value = "F", "(Framework)", "(Non Framework)")
                        txtSupplierEmail.Text = dt.Rows(0)("ACT_NAME")
                        txtSupplierEmailId.Text = dt.Rows(0)("ACT_EMAIL")
                        If h_Prf_apr_id.Value = 500 Or h_Prf_apr_id.Value = 380 Then
                            trEmail.Visible = True
                            trEmail.HeaderText = "Email Purchase Order"
                            imgSupplierEmail.Visible = False
                            txtSupplier.Enabled = False
                        Else
                            trEmail.Visible = False
                        End If
                    End If
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
                End If
            If h_PRF_Cur_Id.Value <> "AED" Then
                lblTotal.Text = "Total Amount (" & h_PRF_Cur_Id.Value & ")"
                'ddlCurrency.SelectedValue = h_PRF_Cur_Id.Value
                'trSupplier.Visible = True
            End If
            If p_Modifyid > 0 And Not isQuotation Then
                trWorkflow.Visible = True
                trWorkflow.HeaderText = "Workflow Details"
                trDocument.Visible = True
                trDocument.HeaderText = "Documents"
                Dim ds As New DataSet
                sqlStr = New StringBuilder
                sqlStr.Append("select replace(convert(varchar(16), WRK_DATE, 106),' ','/') WRK_DATE, USR_DISPLAY_NAME WRK_USER, WRK_ACTION, WRK_DETAILS from WORKFLOW inner JOIN OASIS.dbo.USERS_M on WRK_USERNAME=USR_NAME where WRK_PRF_ID=" & p_Modifyid & " order by WRK_ID desc")
                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr.ToString)
                gvWorkflow.DataSource = ds.Tables(0)
                gvWorkflow.DataBind()
                showImages()
                If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from PRF_H INNER JOIN GROUPSA on GRPA_ID=PRF_APR_ID and GRPA_DESCR='Accounts Officer' and PRF_ID=" & p_Modifyid) = 1 Then
                    imgActid.Enabled = True
                    radBudgeted.Enabled = True
                    radUnBudgeted.Enabled = True
                End If
            End If
            imgActid.Visible = imgActid.Enabled
            If ViewState("MainMnu_code") = "PI02018" Then
                btnAdd.Visible = False
                btnMessage.Visible = False
                btnCopy.Visible = False
            End If
            If btnMessage.Visible Then
                trApproval.Visible = True
                If ViewState("MainMnu_code") = "PI04005" Or ViewState("MainMnu_code") = "PI04007" Then
                    trApproval.HeaderText = "Approval\Rejection Comments"
                Else
                    trApproval.HeaderText = "Message Comments"
                End If
            End If
            FormatFigures()
            updateWorkflow()

        Catch ex As Exception
            Errorlog(ex.Message)
            If Not (ex.Message.Contains("Thread was being aborted") Or ex.Message.Contains("Timeout expired")) Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "insert into [OASIS].[dbo].[COM_EMAIL_ALERTS] (log_type, log_stu_id, log_fromemail, log_toemail, log_subject, log_message, log_date, log_username, log_password, log_host, log_port) SELECT 'ACU', 17973, 'communication@gemsedu.com', 'sandip.kapil@gemseducation.com', 'Error in Purchase load', 'BSU:" & Session("sBsuid") & ", User:" & Session("sUsr_name") & ", message:" & ex.Message.Replace("'", "`") & ":" & h_EntryId.Value & "', getdate(),'communication', 'gems123', '172.16.3.10', 25 ")
            End If
        End Try
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next
        'txtOthers.Text = Convert.ToDouble(txtOthers.Text).ToString("####0.00")
        'txtVAT.Text = Convert.ToDouble(txtVAT.Text).ToString("####0.00")
        txtTotal.Text = Convert.ToDouble(txtTotal.Text).ToString("#,###,##0.00")
        txtBudMtd.Text = Convert.ToDouble(txtBudMtd.Text).ToString("#,###,##0.00")
        txtBudYtd.Text = Convert.ToDouble(txtBudYtd.Text).ToString("#,###,##0.00")
        txtActMtd.Text = Convert.ToDouble(txtActMtd.Text).ToString("#,###,##0.00")
        txtActYtd.Text = Convert.ToDouble(txtActYtd.Text).ToString("#,###,##0.00")
        txtAltTotal.Text = Convert.ToDouble(txtAltTotal.Text).ToString("#,###,##0.00")
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        h_delete.Value = "0"
        h_total.Value = "0"
        h_empid.Value = Session("EmployeeId")
        hTPT_Client_ID.Value = ""
        h_Actid.Value = ""
        txtACTID.Text = ""
        h_Dpt_id.Value = "0"
        h_SubDpt_id.Value = "0"
        txtPRFNo.Text = "NEW"
        txtOldNo.Text = ""

        'txtPRFDate.Text = Now.ToString("dd/MMM/yyyy")
        'txtDELDate.Text = DateAdd(DateInterval.Day, 14, Now).ToString("dd/MMM/yyyy")
        'Commented on 17-Dec-2020 as per Sagi Request

        Dim LastdateFyear As DateTime
        FyearClosed = Mainclass.getDataValue("select bdefault from oasis..FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "'", "OASIS_PUR_INVConnectionString")
        LastdateFyear = Mainclass.getDataValue("select FYR_TODT from oasis..FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "'", "OASIS_PUR_INVConnectionString")
        If FyearClosed = False Then
            txtPRFDate.Text = LastdateFyear.ToString("dd/MMM/yyyy")
            txtDELDate.Text = LastdateFyear.ToString("dd/MMM/yyyy")
        Else
            txtPRFDate.Text = Now.ToString("dd/MMM/yyyy")
            txtDELDate.Text = DateAdd(DateInterval.Day, 14, Now).ToString("dd/MMM/yyyy")
        End If

        txtContact.Text = ""

        txtSupplierAddress.Text = ""
        txtProcurement.Text = ""
        txtSupplier.Text = ""
        txtTotal.Text = "0.0"
        txtAltTotal.Text = "0.0"
        hdnAltTotal.Value = "0.0"
        txtBudMtd.Text = "0.0"
        txtBudYtd.Text = "0.0"
        txtActMtd.Text = "0.0"
        txtActYtd.Text = "0.0"
        h_QUO_ID1.Value = "0"
        h_QUO_ID2.Value = "0"
        h_QUO_ID3.Value = "0"
        isMSO = 0
        isPJV = 0
        radBudgeted.Checked = True
    End Sub

    Private Property PRFFooter() As DataTable
        Get
            Return ViewState("PRFFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("PRFFooter") = value
        End Set
    End Property

    Private Property PRCFooter() As DataTable
        Get
            Return ViewState("PRCFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("PRCFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Property QuoteId() As Integer
        Get
            Return ViewState("QuoteId")
        End Get
        Set(ByVal value As Integer)
            ViewState("QuoteId") = value
        End Set
    End Property

    Private Property isQuotation() As Boolean
        Get
            Return ViewState("Quotation")
        End Get
        Set(ByVal value As Boolean)
            ViewState("Quotation") = value
        End Set
    End Property

    Private Property Prf_Id_New() As Integer
        Get
            Return ViewState("Prf_Id_New")
        End Get
        Set(ByVal value As Integer)
            ViewState("Prf_Id_New") = value
        End Set
    End Property

    Private Property isList() As Boolean
        Get
            Return ViewState("List")
        End Get
        Set(ByVal value As Boolean)
            ViewState("List") = value
        End Set
    End Property

    Private Property isMSO() As Integer
        Get
            Return ViewState("isMSO")
        End Get
        Set(ByVal value As Integer)
            ViewState("isMSO") = value
        End Set
    End Property

    Private Property isPJV() As Integer
        Get
            Return ViewState("isPJV")
        End Get
        Set(ByVal value As Integer)
            ViewState("isPJV") = value
        End Set
    End Property

    Private Property p_Actid() As String
        Get
            Return ViewState("p_Actid")
        End Get
        Set(ByVal value As String)
            ViewState("p_Actid") = value
        End Set
    End Property

    Private Property p_Priority() As Integer
        Get
            Return ViewState("p_Priority")
        End Get
        Set(ByVal value As Integer)
            ViewState("p_Priority") = value
        End Set
    End Property

    Private Property p_Budgeted() As Boolean
        Get
            Return ViewState("p_Budgeted")
        End Get
        Set(ByVal value As Boolean)
            ViewState("p_Budgeted") = value
        End Set
    End Property

    Private Property isSEA() As Boolean
        Get
            Return ViewState("isSEA")
        End Get
        Set(ByVal value As Boolean)
            ViewState("isSEA") = value
        End Set
    End Property

    Private Property isCAPEX() As Boolean
        Get
            Return ViewState("isCAPEX")
        End Get
        Set(ByVal value As Boolean)
            ViewState("isCAPEX") = value
        End Set
    End Property

    Private Property isFramework() As Boolean
        Get
            Return ViewState("Framework")
        End Get
        Set(ByVal value As Boolean)
            ViewState("Framework") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
    End Sub

    Private Sub showNoRecordsFound()
        If Not PRFFooter Is Nothing AndAlso PRFFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdPRF.Columns.Count - 2
            grdPRF.Rows(0).Cells.Clear()
            grdPRF.Rows(0).Cells.Add(New TableCell())
            grdPRF.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdPRF.Rows(0).Cells(0).Text = "No Record Found"
        End If
        If trCC.Visible AndAlso Not PRCFooter Is Nothing AndAlso PRCFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdCCS.Columns.Count - 2
            grdCCS.Rows(0).Cells.Clear()
            grdCCS.Rows(0).Cells.Add(New TableCell())
            grdCCS.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdCCS.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdPRF_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdPRF.RowCancelingEdit
        grdPRF.EditIndex = -1
        grdPRF.DataSource = PRFFooter
        grdPRF.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdPRF_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPRF.RowCommand
        If e.CommandName = "AddNew" Then
            Dim hdnITM_ID As HiddenField = grdPRF.FooterRow.FindControl("hdnITM_ID")
            Dim txtPRD_DESCR As TextBox = grdPRF.FooterRow.FindControl("txtPRD_DESCR")
            Dim txtPRD_DETAILS As TextBox = grdPRF.FooterRow.FindControl("txtPRD_DETAILS")
            Dim txtPRD_QTY As TextBox = grdPRF.FooterRow.FindControl("txtPRD_QTY")
            Dim txtPRD_RATE As TextBox = grdPRF.FooterRow.FindControl("txtPRD_RATE")
            Dim txtPRD_TOTAL As TextBox = grdPRF.FooterRow.FindControl("txtPRD_TOTAL")
            If txtPRD_TOTAL.Text.Length = 0 Then txtPRD_TOTAL.Text = "0"
            If txtPRD_TOTAL.Visible Then txtPRD_TOTAL.Text = Convert.ToDouble(txtPRD_TOTAL.Text).ToString("#,###,##0.00")

            lblError.Text = ""
            'If isFramework Or isQuotation Then
            If hdnITM_ID.Value.Trim.Length = 0 Then
                lblError.Text &= "Item"
            Else
                If isFramework Then
                    txtPRD_DESCR.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING from item where itm_id=" & hdnITM_ID.Value)
                Else
                    If isSEA = True Then
                        'txtPRD_DESCR.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING from item where itm_id=" & hdnITM_ID.Value)
                        txtPRD_DESCR.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select ITM_DESCR  from item where itm_id=" & hdnITM_ID.Value)
                    Else
                        txtPRD_DESCR.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select ASC_DESCR+' '+ACM_DESCR [DESCRIPTION] from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID where ASC_ID=" & hdnITM_ID.Value)
                    End If


                End If
            End If
            'End If
            If Val(txtPRD_RATE.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Rate"
            If Val(txtPRD_QTY.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Quantity"

            If lblError.Text.Length > 0 Then
                lblError.Text &= " Mandatory" '!!!!
                showNoRecordsFound()
                Exit Sub
            End If

            If PRFFooter.Rows(0)(1) = -1 Then PRFFooter.Rows.RemoveAt(0)
            If hdnITM_ID.Value = "" Then hdnITM_ID.Value = "0"
            Dim mrow As DataRow
            mrow = PRFFooter.NewRow
            mrow("PRD_ID") = 0
            mrow("PRD_QTY") = txtPRD_QTY.Text
            mrow("PRD_ITM_ID") = hdnITM_ID.Value
            mrow("PRD_DESCR") = txtPRD_DESCR.Text
            mrow("PRD_DETAILS") = txtPRD_DETAILS.Text
            mrow("PRD_RATE") = txtPRD_RATE.Text
            mrow("PRD_TOTAL") = txtPRD_TOTAL.Text
            PRFFooter.Rows.Add(mrow)
            'txtTotal.Text = Convert.ToDouble(PRFFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")
            '#395065 05-Nov-2019
            txtGross.Text = Convert.ToDouble(PRFFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")
            txtTax.Text = Convert.ToDouble((Val(CDbl(txtGross.Text)) * Val(h_TAXPerc.Value) / 100.0)).ToString("#,###,##0.00")
            txtTotal.Text = Convert.ToDouble(Val(CDbl(txtGross.Text)) + Val(CDbl(txtTax.Text))).ToString("#,###,##0.00")
            txtAltTotal.Text = (Convert.ToDouble(txtTotal.Text) / Convert.ToDouble(txtExgRate.Text)).ToString("#,###,##0.00")
            h_total.Value = txtTotal.Text
            hdnAltTotal.Value = txtAltTotal.Text
            'radBudgeted.Checked = (Val(txtTotal.Text) + Val(txtActMtd.Text)) <= Val(txtBudMtd.Text.Replace(",", ""))
            'radUnBudgeted.Checked = (Val(txtTotal.Text) + Val(txtActMtd.Text)) > Val(txtBudMtd.Text.Replace(",", ""))

            grdPRF.EditIndex = -1
            grdPRF.DataSource = PRFFooter
            grdPRF.DataBind()
            showNoRecordsFound()

            Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
            acBSUFooter = TryCast(grdPRF.FooterRow.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
            acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & h_Dpt_id.Value & ";" & h_Actid.Value & ";" & Session("sBsuid") & ";" & Session("BSU_CURRENCY")
            txtSupplier.Enabled = False 'as soon as any item is selected
            imgClient.Visible = False
        End If
    End Sub

    Protected Sub grdPRF_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPRF.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim imgClient As ImageButton = e.Row.FindControl("imgClient")
            Dim txtPRD_DESCR As TextBox = e.Row.FindControl("txtPRD_DESCR")
            Dim txtPRD_RATE As TextBox = e.Row.FindControl("txtPRD_RATE")
            Dim txtPRD_QTY As TextBox = e.Row.FindControl("txtPRD_QTY")
            Dim txtPRD_TOTAL As TextBox = e.Row.FindControl("txtPRD_TOTAL")
            Dim hdnITM_ID As HiddenField = e.Row.FindControl("hdnITM_ID")
            Dim txtPRD_DETAILS As TextBox = e.Row.FindControl("txtPRD_DETAILS")

            If Not txtPRD_DESCR Is Nothing Then
                Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
                acBSUFooter = TryCast(e.Row.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
                If Not acBSUFooter Is Nothing Then
                    acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & h_Dpt_id.Value & ";" & h_Actid.Value & ";" & Session("sBsuid") & ";" & Session("BSU_CURRENCY")
                End If
                'txtPRD_RATE.Enabled = isQuotation Or (h_PRF_Type.Value = "N" And (h_EntryId.Value = 0 Or h_Prf_Status.Value = "N"))
                'txtPRD_RATE.Enabled = h_PRF_Type.Value = "F" And (h_EntryId.Value = 0 Or h_Prf_Status.Value = "N") And (txtACTID.Text = "063A0111" Or txtACTID.Text = "063B0104")
                If h_block.Value = "" Then
                    If isCAPEX Or isQuotation Or (h_PRF_Type.Value = "N" And (h_EntryId.Value = 0 Or h_Prf_Status.Value = "N")) Or (h_PRF_Type.Value = "F" And h_Prf_Status.Value = "N" And (hTPT_Client_ID.Value = "063A0111" Or hTPT_Client_ID.Value = "063B0104")) Then
                        h_block.Value = "F"
                        h_blockForm.Value = "F"
                    Else
                        h_block.Value = "T"
                        h_blockForm.Value = "T"
                    End If
                ElseIf isQuotation Then
                    h_block.Value = "F"
                    h_blockForm.Value = "F"
                End If
                txtPRD_DESCR.Enabled = Not isQuotation
                txtPRD_QTY.Enabled = Not isQuotation
                txtPRD_QTY.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
                txtPRD_RATE.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
                txtPRD_RATE.Attributes.Add("onkeydown", "javascript:return Numeric_Block()")
                txtRAdjust.Attributes.Add("onkeydown", "javascript:return Numeric_Only()")
                'imgClient.Attributes.Add("onclick", "javascript:return getItemName('" & hdnITM_ID.ClientID & "','" & txtPRD_DESCR.ClientID & "','" & txtPRD_QTY.ClientID & "','" & txtPRD_RATE.ClientID & "','" & txtPRD_TOTAL.ClientID & "');")
                imgClient.Attributes.Add("onclick", "javascript: getItemNameNew('" & hdnITM_ID.ClientID & "','" & txtPRD_DESCR.ClientID & "','" & txtPRD_QTY.ClientID & "','" & txtPRD_RATE.ClientID & "','" & txtPRD_TOTAL.ClientID & "'); return false;")

                If isFramework Then
                    'imgClient.Attributes.Add("onclick", "javascript:return getItemName('" & hdnITM_ID.ClientID & "','" & txtPRD_DESCR.ClientID & "','" & txtPRD_QTY.ClientID & "','" & txtPRD_RATE.ClientID & "','" & txtPRD_TOTAL.ClientID & "');")
                    'imgClient.Visible = True
                Else
                    'imgClient.Visible = False
                End If
                'txtSupplier, txtClient, txtRate, txtQty, txtTotal, hdnClient
                txtPRD_QTY.Attributes.Add("OnKeyUp", "javascript:return calculateItem('" & txtPRD_QTY.ClientID & "','" & txtPRD_RATE.ClientID & "','" & txtPRD_TOTAL.ClientID & "');")
                txtPRD_RATE.Attributes.Add("OnKeyUp", "javascript:return calculateItem('" & txtPRD_QTY.ClientID & "','" & txtPRD_RATE.ClientID & "','" & txtPRD_TOTAL.ClientID & "');")
            End If
        End If
    End Sub

    Protected Sub grdPRF_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdPRF.RowDeleting
        Dim mRow() As DataRow = PRFFooter.Select("ID=" & grdPRF.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_PRFGridDelete.Value &= ";" & mRow(0)("PRD_ID")
            PRFFooter.Select("ID=" & grdPRF.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            PRFFooter.AcceptChanges()
        End If
        If PRFFooter.Rows.Count = 0 Then
            PRFFooter.Rows.Add(PRFFooter.NewRow())
            PRFFooter.Rows(0)(1) = -1
        End If
        Try
            'txtTotal.Text = Convert.ToDouble(PRFFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")
            '#395065 05-Nov-2019

            txtGross.Text = Convert.ToDouble(PRFFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")
            txtTax.Text = Convert.ToDouble((Val(CDbl(txtGross.Text)) * Val(h_TAXPerc.Value) / 100.0)).ToString("#,###,##0.00")
            txtTotal.Text = Convert.ToDouble(Val(txtGross.Text) + Val(txtTax.Text)).ToString("#,###,##0.00")


            txtAltTotal.Text = (Convert.ToDouble(txtTotal.Text) / Convert.ToDouble(txtExgRate.Text)).ToString("#,###,##0.00")
        Catch ex As Exception
            txtSupplier.Enabled = True And ViewState("datamode") = "add" 'if the last item is deleted
            imgClient.Visible = True And ViewState("datamode") = "add"
        End Try
        'radBudgeted.Checked = (Val(txtTotal.Text) + Val(txtActMtd.Text)) <= Val(txtBudMtd.Text.Replace(",", ""))
        'radUnBudgeted.Checked = (Val(txtTotal.Text) + Val(txtActMtd.Text)) > Val(txtBudMtd.Text.Replace(",", ""))

        h_total.Value = txtTotal.Text
        grdPRF.DataSource = PRFFooter
        grdPRF.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdPRF_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdPRF.RowEditing
        Try
            grdPRF.EditIndex = e.NewEditIndex
            grdPRF.DataSource = PRFFooter
            grdPRF.DataBind()
            showNoRecordsFound()

            Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
            acBSUFooter = TryCast(grdPRF.Rows(grdPRF.EditIndex).FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
            acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & h_Dpt_id.Value & ";" & h_Actid.Value & ";" & Session("sBsuid") & ";" & Session("BSU_CURRENCY")

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdPRF_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdPRF.RowUpdating
        Dim s As String = grdPRF.DataKeys(e.RowIndex).Value.ToString()
        Dim hdnITM_ID As HiddenField = grdPRF.Rows(e.RowIndex).FindControl("hdnITM_ID")
        Dim lblPRD_ID As Label = grdPRF.Rows(e.RowIndex).FindControl("lblPRD_ID")
        Dim txtPRD_DESCR As TextBox = grdPRF.Rows(e.RowIndex).FindControl("txtPRD_DESCR")
        Dim txtPRD_DETAILS As TextBox = grdPRF.Rows(e.RowIndex).FindControl("txtPRD_DETAILS")
        Dim txtPRD_QTY As TextBox = grdPRF.Rows(e.RowIndex).FindControl("txtPRD_QTY")
        Dim txtPRD_RATE As TextBox = grdPRF.Rows(e.RowIndex).FindControl("txtPRD_RATE")
        Dim txtPRD_TOTAL As TextBox = grdPRF.Rows(e.RowIndex).FindControl("txtPRD_TOTAL")
        If txtPRD_TOTAL.Visible Then txtPRD_TOTAL.Text = Convert.ToDouble(txtPRD_TOTAL.Text).ToString("#,###,##0.00")

        Dim mrow As DataRow
        mrow = PRFFooter.Select("ID=" & s)(0)
        mrow("PRD_ITM_ID") = hdnITM_ID.Value
        mrow("PRD_DESCR") = txtPRD_DESCR.Text
        mrow("PRD_RATE") = txtPRD_RATE.Text
        mrow("PRD_TOTAL") = txtPRD_TOTAL.Text
        mrow("PRD_DETAILS") = txtPRD_DETAILS.Text
        mrow("PRD_QTY") = txtPRD_QTY.Text
        'txtTotal.Text = Convert.ToDouble(PRFFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")
        '#395065 05-Nov-2019
        txtGross.Text = Convert.ToDouble(PRFFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")
        txtTax.Text = Convert.ToDouble((Val(CDbl(txtGross.Text)) * Val(h_TAXPerc.Value) / 100.0)).ToString("#,###,##0.00")
        txtTotal.Text = Convert.ToDouble((Val(txtGross.Text) + Val(txtTax.Text))).ToString("#,###,##0.00")

        txtAltTotal.Text = (Convert.ToDouble(txtTotal.Text) / Convert.ToDouble(txtExgRate.Text)).ToString("#,###,##0.00")


        grdPRF.EditIndex = -1
        grdPRF.DataSource = PRFFooter
        grdPRF.DataBind()
        showNoRecordsFound()

        Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
        acBSUFooter = TryCast(grdPRF.FooterRow.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
        acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & h_Dpt_id.Value & ";" & h_Actid.Value & ";" & Session("sBsuid") & ";" & Session("BSU_CURRENCY")
        txtSupplier.Enabled = False
        imgClient.Visible = False Or isQuotation

    End Sub

    Protected Sub grdCCSRowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdCCS.RowCancelingEdit
        grdCCS.EditIndex = -1
        grdCCS.DataSource = PRCFooter
        grdCCS.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdCCSRowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCCS.RowCommand
        If e.CommandName = "AddNew" Then
            Dim ddlPRC_TYPE As DropDownList = grdCCS.FooterRow.FindControl("ddlPRC_TYPE")
            Dim hdnITM_ID As HiddenField = grdCCS.FooterRow.FindControl("hdnITM_ID")
            Dim txtPRC_DESCR As TextBox = grdCCS.FooterRow.FindControl("txtPRC_DESCR")
            Dim txtPRC_AMT As TextBox = grdCCS.FooterRow.FindControl("txtPRC_AMT")

            lblError.Text = ""
            If hdnITM_ID.Value.Trim.Length = 0 Then lblError.Text &= "Cost Center\Sub Ledger"
            If Val(txtPRC_AMT.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Amount"

            If lblError.Text.Length > 0 Then
                lblError.Text &= " Mandatory" '!!!!
                showNoRecordsFound()
                Exit Sub
            End If

            If PRCFooter.Rows(0)(1) = -1 Then PRCFooter.Rows.RemoveAt(0)
            If hdnITM_ID.Value = "" Then hdnITM_ID.Value = "0"
            Dim mrow As DataRow
            mrow = PRCFooter.NewRow
            mrow("PRC_ID") = 0
            mrow("PRM_CODE") = ddlPRC_TYPE.SelectedItem.Value
            mrow("PRM_DESCR") = ddlPRC_TYPE.SelectedItem.Text
            mrow("PRC_CCS_ID") = hdnITM_ID.Value
            mrow("PRC_CCS_DESCR") = txtPRC_DESCR.Text
            mrow("PRC_AMT") = txtPRC_AMT.Text
            PRCFooter.Rows.Add(mrow)

            grdCCS.EditIndex = -1
            grdCCS.DataSource = PRCFooter
            grdCCS.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdCCSRowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCCS.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim imgClient As ImageButton = e.Row.FindControl("imgClient")
            Dim ddlPRC_TYPE As DropDownList = e.Row.FindControl("ddlPRC_TYPE")
            Dim hdnITM_ID As HiddenField = e.Row.FindControl("hdnITM_ID")
            Dim txtPRC_DESCR As TextBox = e.Row.FindControl("txtPRC_DESCR")
            Dim txtPRC_AMT As TextBox = e.Row.FindControl("txtPRC_AMT")
            If Not ddlPRC_TYPE Is Nothing Then
                fillDropdown(ddlPRC_TYPE, "select prm_code, prm_descr from PRF_CM", "prm_descr", "prm_code", False)
                imgClient.Attributes.Add("onclick", "javascript:return getCCS('" & hdnITM_ID.ClientID & "','" & txtPRC_DESCR.ClientID & "','" & ddlPRC_TYPE.ClientID & "','" & Now.ToString("dd/MMM/yyyy") & "');")

            End If
        End If
    End Sub

    Protected Sub grdCCSRowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdCCS.RowDeleting
        Dim mRow() As DataRow = PRCFooter.Select("ID=" & grdCCS.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_PRCGridDelete.Value &= ";" & mRow(0)("PRC_ID")
            PRCFooter.Select("ID=" & grdCCS.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            PRCFooter.AcceptChanges()
        End If
        If PRCFooter.Rows.Count = 0 Then
            PRCFooter.Rows.Add(PRCFooter.NewRow())
            PRCFooter.Rows(0)(1) = -1
        End If

        grdCCS.DataSource = PRCFooter
        grdCCS.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdCCSRowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdCCS.RowEditing
        Try
            grdCCS.EditIndex = e.NewEditIndex
            grdCCS.DataSource = PRCFooter
            grdCCS.DataBind()
            showNoRecordsFound()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdCCSRowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdCCS.RowUpdating
        Dim s As String = grdCCS.DataKeys(e.RowIndex).Value.ToString()
        Dim ddlPRC_TYPE As DropDownList = grdCCS.Rows(e.RowIndex).FindControl("ddlPRC_TYPE")
        Dim hdnITM_ID As HiddenField = grdCCS.Rows(e.RowIndex).FindControl("hdnITM_ID")
        Dim txtPRC_DESCR As TextBox = grdCCS.Rows(e.RowIndex).FindControl("txtPRC_DESCR")
        Dim txtPRC_AMT As TextBox = grdCCS.Rows(e.RowIndex).FindControl("txtPRC_AMT")

        Dim mrow As DataRow
        mrow = PRCFooter.Select("ID=" & s)(0)
        mrow("PRM_CODE") = ddlPRC_TYPE.SelectedItem.Value
        mrow("PRM_DESCR") = ddlPRC_TYPE.SelectedItem.Text
        mrow("PRC_CCS_ID") = hdnITM_ID.Value
        mrow("PRC_CCS_DESCR") = txtPRC_DESCR.Text
        mrow("PRC_AMT") = txtPRC_AMT.Text

        grdCCS.EditIndex = -1
        grdCCS.DataSource = PRCFooter
        grdCCS.DataBind()
        showNoRecordsFound()

    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetPrdDescr(ByVal prefixText As String, ByVal contextKey As String) As String()
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        'Dim StrSQL As String
        'If contextKey Is Nothing Then contextKey = ";;;;"
        'If contextKey.Split(";")(0) = "none" Then 'non framework
        '    StrSQL = "select top 10 ID, COMMODITY, DESCRIPTION, RATE, '' xQUO_SUP_ID, '' xACT_NAME from ("
        '    StrSQL &= "SELECT ITM_ID ID, UCO_DESCR COMMODITY, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING DESCRIPTION, cast(isnull(ITM_RATE,0) as varchar) RATE "
        '    StrSQL &= "FROM ITEM left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
        '    StrSQL &= "where 1=0 and ITM_ID NOT in (SELECT distinct isnull(QUD_ITM_ID,0) FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID)) a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"
        '    'Select ID,DESCR,CATEGORY from(select ASC_ID AS ID,ASC_DESCR AS DESCR,ACM_DESCR [CATEGORY] from ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID ) A where A.ID<>'' 
        '    If contextKey.Split(";")(1) = 302 Then
        '        StrSQL = "select top 10 ID, COMMODITY, DESCRIPTION, RATE, '' xQUO_SUP_ID, '' xACT_NAME from ("
        '        StrSQL &= "select distinct '' COMMODITY, ASC_ID AS ID,ASC_DESCR+' '+ACM_DESCR [DESCRIPTION], '' RATE "
        '        StrSQL &= "from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID and ACT_ID='" & contextKey.Split(";")(2) & "'"
        '        StrSQL &= ") a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"
        '    Else
        '        StrSQL = "select top 10 ID, COMMODITY, DESCRIPTION, RATE, '' xQUO_SUP_ID, '' xACT_NAME from ("
        '        StrSQL &= "select '' COMMODITY, ASC_ID AS ID,ASC_DESCR+' '+ACM_DESCR [DESCRIPTION], '' RATE "
        '        StrSQL &= "from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID and ACT_ID=''"
        '        StrSQL &= ") a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"
        '    End If
        'ElseIf contextKey.Split(";")(0) = "" Then 'framework
        '    StrSQL = "select top 10 ID, COMMODITY, DESCRIPTION, RATE, xQUO_SUP_ID, xACT_NAME from ( "
        '    StrSQL &= "SELECT ITM_ID ID, UCO_DESCR COMMODITY, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING DESCRIPTION, "
        '    StrSQL &= "cast(cast(QUD_RATE as decimal(12,3)) as varchar) Rate, "
        '    StrSQL &= "ACT_NAME+';'+QUO_TERMS+';'+QUO_CONTACT+'\n'+QUO_ADDRESS+'\nTel.'+QUO_PHONE+' Mob.'+QUO_MOBILE+' Fax.'+QUO_FAX xACT_NAME, QUO_SUP_ID xQUO_SUP_ID  "
        '    StrSQL &= "FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID AND QUO_DELETED=0 AND QUD_DELETED=0 INNER JOIN ITEM on ITM_ID=QUD_ITM_ID "
        '    StrSQL &= "left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
        '    StrSQL &= "INNER JOIN OASISFIN.dbo.ACCOUNTS_M on QUO_SUP_ID=ACT_ID "
        '    StrSQL &= "where (QUO_DESCR='' or QUO_DESCR LIKE '%" & contextKey.Split(";")(3) & "%' or QUO_DESCR LIKE '%" & contextKey.Split(";")(4) & "%') "
        '    StrSQL &= ") a where 1=1 and DESCRIPTION like '%" & prefixText & "%' "
        'Else
        '    StrSQL = "select top 10 ID, COMMODITY, DESCRIPTION, RATE, '' xQUO_SUP_ID, '' xACT_NAME from ("
        '    StrSQL &= "SELECT ITM_ID ID, UCO_DESCR COMMODITY, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING DESCRIPTION, "
        '    StrSQL &= "cast(cast(QUD_RATE as decimal(12,3)) as varchar) Rate "
        '    StrSQL &= "FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID AND QUO_DELETED=0 AND QUD_DELETED=0 INNER JOIN ITEM on ITM_ID=QUD_ITM_ID left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
        '    StrSQL &= "WHERE (QUO_DESCR='' or QUO_DESCR LIKE '%" & contextKey.Split(";")(3) & "%' or QUO_DESCR LIKE '%" & contextKey.Split(";")(4) & "%') and QUO_SUP_ID='" & contextKey.Split(";")(0) & "' "
        '    StrSQL &= ") a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"
        'End If

        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        'Dim intRows As Int32 = ds.Tables(0).Rows.Count
        'Dim items As New List(Of String)(intRows)
        'Dim i2 As Integer

        'For i2 = 0 To intRows - 1
        '    Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("xQUO_SUP_ID").ToString & "|" & ds.Tables(0).Rows(i2)("xACT_NAME").ToString & "|" & ds.Tables(0).Rows(i2)("ID").ToString & "|" & ds.Tables(0).Rows(i2)("RATE").ToString & "|" & ds.Tables(0).Rows(i2)("COMMODITY").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
        '    items.Add(item)
        'Next
        'Return items.ToArray()
    End Function

    Private Sub GetActuals()
        txtActMtd.Text = "0.00"
        txtActYtd.Text = "0.00"
        Dim sqlStr As String
        sqlStr = "select CONVERT(varchar, CAST(xACTMTD AS money), 1), CONVERT(varchar, CAST(xACTYTD AS money), 1) from vw_PRF_ACTUALBak where VW_ACT_ID='" & h_Actid.Value & "' and BSU_ID='" & Session("sBsuid") & "' and fyear='" & Session("F_YEAR") & "' and MTH=month('" & txtPRFDate.Text & "') and DPT_ID='" & h_Dpt_id.Value & "'"
        Dim actActuals As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr)
        Try
            If actActuals.HasRows Then
                actActuals.Read()
                txtActMtd.Text = actActuals(0)
                txtActYtd.Text = actActuals(1)
                If (txtBudYtd.Text - txtActYtd.Text) >= 0 Then
                    lblGreen.Text = Convert.ToDouble(txtBudYtd.Text - txtActYtd.Text).ToString("#,###,##0.00")
                    lblGreen.Visible = True
                    lblRed.Visible = False
                Else
                    lblRed.Text = Convert.ToDouble(txtBudYtd.Text - txtActYtd.Text).ToString("#,###,##0.00")
                    lblRed.Visible = True
                    lblGreen.Visible = False
                End If
            End If
            Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
            acBSUFooter = TryCast(grdPRF.FooterRow.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
            acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & ddlDepartment.SelectedValue & ";" & h_Actid.Value & ";" & Session("sBsuid") & ";" & Session("BSU_CURRENCY")

        Catch ex As Exception

        End Try
        actActuals.Close()
        actActuals = Nothing
        hActRefresh.Value = "0"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim quoteEdit As String = "N"
        lblError.Text = ""

        If (hTPT_Client_ID.Value = "none" Or hTPT_Client_ID.Value.Trim.Length = 0) AndAlso (isSEA Or isFramework Or isQuotation) Then lblError.Text &= "&nbsp; Supplier Name <br/> "
        If txtPRFDate.Text.Trim.Length = 0 Then lblError.Text &= "&nbsp; PRF Date  <br/>  "
        If (isSEA Or h_PRF_Type.Value = "Q") And txtTerms.Text.Trim.Length = 0 Then lblError.Text &= "&nbsp; Payment Terms  <br/>  "
        If h_Actid.Value.Trim.Length = 0 And imgActid.Enabled Then lblError.Text &= "&nbsp; Budget Account  <br/>  " 'And h_EntryId.Value > 0 
        If h_PRF_Type.Value = "C" And hTPT_Client_ID.Value = "none" Then lblError.Text &= "&nbsp; Supplier  <br/>  "
        If PRFFooter.Rows.Count = 1 And PRFFooter.Rows(0)(1) = -1 Then lblError.Text &= "&nbsp; PRF should contain atleast 1 item  <br/>  "
        If grdPRF.Rows.Count = 0 Or (CType(txtTotal.Text, Decimal) = 0 And (isFramework Or isQuotation)) Then lblError.Text &= "&nbsp; PRF should contain atleast 1 item " 'IIf(lblError.Text.Length = 0, "", ",") &
        If lblError.Text.Length > 0 Then lblError.Text = "Below fields are mandatory <br/>" + lblError.Text 'lblError.Text.Substring(0, lblError.Text.Length - 1) & " are mandatory "


        If ddlDepartment.Visible Then h_Dpt_id.Value = ddlDepartment.SelectedValue
        'If Not Session("F_YEAR").ToString.Contains("2013") Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Invalid Year"

        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If
        If isQuotation And txtPRFNo.Text = "NEW" Then
            h_QUO_ID1.Value = Session("EmployeeId")
            h_QUO_ID2.Value = h_EntryId.Value
            h_EntryId.Value = "0"
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(38) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@PRF_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@PRF_NO", txtPRFNo.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@PRF_PRI_ID", ddlPriority.SelectedValue, SqlDbType.Int)
        If h_PRF_Type.Value = "Q" Then
            pParms(4) = Mainclass.CreateSqlParameter("@PRF_STATUS", "Q", SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@PRF_TERMS", txtTerms.Text, SqlDbType.VarChar)
        ElseIf isSEA Then
            pParms(4) = Mainclass.CreateSqlParameter("@PRF_STATUS", "N", SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@PRF_TERMS", txtTerms.Text, SqlDbType.VarChar)
        Else
            pParms(4) = Mainclass.CreateSqlParameter("@PRF_STATUS", "N", SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@PRF_TERMS", h_term.Value, SqlDbType.VarChar)
        End If
        pParms(5) = Mainclass.CreateSqlParameter("@PRF_APR_ID", 0, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@PRF_BSU_ID", ddlBSU.SelectedValue, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@PRF_DATE", txtPRFDate.Text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@PRF_PROCUREMENT", txtProcurement.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@PRF_SUPPLIER", txtSupplierAddress.Text, SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@PRF_CONTACT", txtContact.Text, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@PRF_SHIPMENT", txtShipment.Text, SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@PRF_SUP_ID", hTPT_Client_ID.Value, SqlDbType.VarChar)
        pParms(14) = Mainclass.CreateSqlParameter("@PRF_DELDATE", txtDELDate.Text, SqlDbType.VarChar)
        pParms(15) = Mainclass.CreateSqlParameter("@PRF_CHARGES", 0, SqlDbType.Decimal)
        pParms(16) = Mainclass.CreateSqlParameter("@PRF_VAT", 0, SqlDbType.Decimal)
        pParms(17) = Mainclass.CreateSqlParameter("@PRF_TOTAL", txtTotal.Text, SqlDbType.Decimal)
        pParms(18) = Mainclass.CreateSqlParameter("@PRF_REMARKS", txtComments.Text, SqlDbType.VarChar)
        pParms(19) = Mainclass.CreateSqlParameter("@PRF_BUDGETED", IIf(radBudgeted.Checked, 0, 1), SqlDbType.VarChar)
        pParms(20) = Mainclass.CreateSqlParameter("@PRF_TYPE", h_PRF_Type.Value, SqlDbType.VarChar)
        pParms(21) = Mainclass.CreateSqlParameter("@PRF_QUOTE", quoteEdit, SqlDbType.VarChar)
        pParms(22) = Mainclass.CreateSqlParameter("@PRF_ACT_ID", h_Actid.Value, SqlDbType.VarChar)
        pParms(23) = Mainclass.CreateSqlParameter("@QUO_ID1", h_QUO_ID1.Value, SqlDbType.Int)
        pParms(24) = Mainclass.CreateSqlParameter("@QUO_ID2", h_QUO_ID2.Value, SqlDbType.Int)
        pParms(25) = Mainclass.CreateSqlParameter("@QUO_ID3", h_QUO_ID3.Value, SqlDbType.Int)
        pParms(26) = Mainclass.CreateSqlParameter("@PRF_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(27) = Mainclass.CreateSqlParameter("@PRF_USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(28) = Mainclass.CreateSqlParameter("@PRF_OLDNO", txtOldNo.Text, SqlDbType.VarChar)
        pParms(29) = Mainclass.CreateSqlParameter("@PRF_CUR_ID", h_PRF_Cur_Id.Value, SqlDbType.VarChar)
        pParms(30) = Mainclass.CreateSqlParameter("@PRF_ALT_CUR_ID", ddlCurrency.SelectedItem.Text, SqlDbType.VarChar)
        pParms(31) = Mainclass.CreateSqlParameter("@PRF_EXGRATE", txtExgRate.Text, SqlDbType.Decimal)
        pParms(32) = Mainclass.CreateSqlParameter("@PRF_ALT_TOTAL", txtAltTotal.Text, SqlDbType.Decimal)
        pParms(33) = Mainclass.CreateSqlParameter("@PRF_REQUESTBY", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(34) = Mainclass.CreateSqlParameter("@PRF_DPT_ID", h_Dpt_id.Value, SqlDbType.Int)
        pParms(35) = Mainclass.CreateSqlParameter("@PRF_SUBDPT_ID", h_SubDpt_id.Value, SqlDbType.Int)
        'pParms(36) = Mainclass.CreateSqlParameter("@PRF_TAX", ddlTAX.SelectedItem.Text, SqlDbType.VarChar)
        pParms(36) = Mainclass.CreateSqlParameter("@PRF_TAX", ddlTAX.SelectedValue, SqlDbType.VarChar)
        pParms(37) = Mainclass.CreateSqlParameter("@PRF_TAXAMOUNT", txtTax.Text, SqlDbType.Decimal)
        pParms(38) = Mainclass.CreateSqlParameter("@PRF_GROSS", txtGross.Text, SqlDbType.Decimal)


        'savePRF_H 0, '', 0, '', 0, '999998', '8/oct/2012', 'sandip.kapil', '', 0, 0, '063M0122', '60 days', '8/oct/2012', 0,0,0,''
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "savePRF_H", pParms)
            If RetVal = "-1" Or pParms(1).Value = 0 Then
                lblError.Text = "Unexpected Error " '!!!
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
                Session("prf_id") = pParms(1).Value
                h_EntryId.Value = pParms(1).Value
            End If

            Dim RowCount As Integer, InsCount As Integer = 0
            For RowCount = 0 To PRFFooter.Rows.Count - 1
                Dim iParms(9) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add" Or quoteEdit = "Y", 0, PRFFooter.Rows(RowCount)("PRD_ID"))
                If PRFFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                'PRD_ID, PRD_ITM_ID, ITM_DESCR PRD_DESCR, PRD_QTY, PRD_RATE, PRD_QTY*PRD_RATE PRD_TOTAL
                iParms(1) = Mainclass.CreateSqlParameter("@PRD_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@PRD_PRF_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@PRD_DESCR", PRFFooter.Rows(RowCount)("PRD_DESCR"), SqlDbType.VarChar)
                iParms(4) = Mainclass.CreateSqlParameter("@PRD_ITM_ID", PRFFooter.Rows(RowCount)("PRD_ITM_ID"), SqlDbType.Int)
                iParms(5) = Mainclass.CreateSqlParameter("@PRD_QTY", PRFFooter.Rows(RowCount)("PRD_QTY"), SqlDbType.Decimal)
                iParms(6) = Mainclass.CreateSqlParameter("@PRD_RATE", PRFFooter.Rows(RowCount)("PRD_RATE"), SqlDbType.Decimal)
                iParms(7) = Mainclass.CreateSqlParameter("@PRD_DETAILS", PRFFooter.Rows(RowCount)("PRD_DETAILS"), SqlDbType.VarChar)
                'iParms(8) = Mainclass.CreateSqlParameter("@PRD_TAX", ddlTAX.SelectedItem.Text, SqlDbType.VarChar)
                iParms(8) = Mainclass.CreateSqlParameter("@PRD_TAX", ddlTAX.SelectedValue, SqlDbType.VarChar)
                'iParms(9) = Mainclass.CreateSqlParameter("@PRD_TAXPERC", ddlTAX.SelectedValue, SqlDbType.Decimal)
                iParms(9) = Mainclass.CreateSqlParameter("@PRD_TAXPERC", h_TAXPerc.Value, SqlDbType.Decimal)

                If Not IsDBNull(PRFFooter.Rows(RowCount)("PRD_ITM_ID")) AndAlso PRFFooter.Rows(RowCount)("PRD_ITM_ID") > 0 Then

                    InsCount += 1
                    Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "savePRF_D", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error " '!!!
                        stTrans.Rollback()
                        Exit Sub
                    End If
                End If
            Next
            If h_PRFGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_PRFGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    'InsCount -= 1
                    iParms(1) = Mainclass.CreateSqlParameter("@PRD_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@PRD_PRF_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from PRF_D where PRD_ID=@PRD_ID and PRD_PRF_ID=@PRD_PRF_ID", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error " '!!!
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If
            If InsCount <= 0 Then
                lblError.Text = "PRF should contain atleast 1 item"
                stTrans.Rollback()
                showNoRecordsFound()
                Exit Sub
            Else
                stTrans.Commit()
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        If ViewState("datamode") <> "edit" Then
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtPRFNo.Text, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            lblError.Text = getErrorMessage("0")
        Else
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtPRFNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

            lblError.Text = getErrorMessage("0")
        End If
        'writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), txtComments.Text, 0, "N")
        SetDataMode("view")
        If isQuotation Then
            Response.Redirect(ViewState("ReferrerUrl"), False)
        Else
            setModifyvalues(ViewState("EntryId"))
        End If

    End Sub

    Private Sub updateWorkflow()
        Dim sqlStr As New StringBuilder

        Dim TakeDirectItem As Integer
        TakeDirectItem = Mainclass.getDataValue("select count(*) from VW_ISSEA_BSUS where bsu_id='" & Session("sBSUID") & "'", "OASIS_PUR_INVConnectionString")



        If isFramework Then
            sqlStr.Append("select PRD_ID, PRD_ITM_ID, isnull(ITM_DESCR+' '+ITM_DETAILS+' '+ITM_UNIT+' '+ITM_PACKING,PRD_DESCR) PRD_DESCR, PRD_DETAILS, PRD_IMAGE, ")
            sqlStr.Append("cast(PRD_QTY as float) PRD_QTY, cast(PRD_RATE as float) as PRD_RATE, cast(PRD_QTY*PRD_RATE as float) PRD_TOTAL ")
            sqlStr.Append("from PRF_D left outer JOIN ITEM on ITM_ID=PRD_ITM_ID LEFT OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE where PRD_PRF_ID=" & h_EntryId.Value & " order by PRD_ID")
        ElseIf h_Dpt_id.Value = 302 Then
            sqlStr.Append("select PRD_ID, PRD_ITM_ID, isnull(ASC_DESCR+' '+ACM_DESCR,PRD_DESCR)+' '+ ")
            sqlStr.Append("(")
            sqlStr.Append("select ")
            sqlStr.Append("            '<font color='''+case when sum(prd_rate*prd_qty)>max(xbudytd) then 'red''>' else 'green''>' end+")
            sqlStr.Append("            '[B='+convert(varchar,cast(max(xbudytd) as money),1)+',A='+convert(varchar,cast(sum(prd_rate*prd_qty) as money),1) ")
            sqlStr.Append("from prf_d pd ")
            sqlStr.Append("inner join prf_h ph on ph.prf_id=pd.prd_prf_id inner join oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] asm on asc_id=PRD_ITM_ID and asc_id=oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M].asc_id ")
            sqlStr.Append("            where ph.prf_dpt_id = prf_h.prf_dpt_id And ph.prf_bsu_id = prf_h.prf_bsu_id AND PRF_DELETED=0 and PRF_STATUS<>'R' and PRF_TYPE IN ('C','N','F') and prf_oldno='' and prf_budgeted=0 ")
            sqlStr.Append(")+']</font>' PRD_DESCR, PRD_DETAILS, PRD_IMAGE, cast(PRD_QTY as float) PRD_QTY, ")
            sqlStr.Append("cast(PRD_RATE as float) as PRD_RATE, cast(PRD_QTY*PRD_RATE as float) PRD_TOTAL ")
            sqlStr.Append("from PRF_D inner join prf_h on prf_id=prd_prf_id left outer join oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] on asc_id=PRD_ITM_ID ")
            sqlStr.Append("left outer join oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID ")
            sqlStr.Append("where(PRD_PRF_ID = " & h_EntryId.Value & ") order by PRD_ID")
        Else
            If TakeDirectItem > 0 Then
                'sqlStr.Append("select PRD_ID, PRD_ITM_ID, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING+' '+ITM_UNIT [PRD_DESCR], PRD_DETAILS, PRD_IMAGE, cast(PRD_QTY as float) PRD_QTY, cast(PRD_RATE as float) as PRD_RATE, cast(PRD_QTY*PRD_RATE as float) PRD_TOTAL from PRF_D left outer join ITEM on ITM_ID=PRD_ITM_ID  where PRD_PRF_ID=" & h_EntryId.Value & "  order by PRD_ID")
                sqlStr.Append("select PRD_ID, PRD_ITM_ID, ITM_DESCR [PRD_DESCR], PRD_DETAILS, PRD_IMAGE, cast(PRD_QTY as float) PRD_QTY, cast(PRD_RATE as float) as PRD_RATE, cast(PRD_QTY*PRD_RATE as float) PRD_TOTAL from PRF_D left outer join ITEM on ITM_ID=PRD_ITM_ID  where PRD_PRF_ID=" & h_EntryId.Value & "  order by PRD_ID")
            Else
                sqlStr.Append("select PRD_ID, PRD_ITM_ID, isnull(ASC_DESCR+' '+ACM_DESCR,PRD_DESCR) PRD_DESCR, PRD_DETAILS, PRD_IMAGE, ")
                sqlStr.Append("cast(PRD_QTY as float) PRD_QTY, cast(PRD_RATE as float) as PRD_RATE, cast(PRD_QTY*PRD_RATE as float) PRD_TOTAL ")
                sqlStr.Append("from PRF_D left outer join oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] on asc_id=PRD_ITM_ID left outer join oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID where PRD_PRF_ID=" & h_EntryId.Value & " order by PRD_ID")
            End If

        End If
        fillGridView(PRFFooter, grdPRF, sqlStr.ToString)
        If (ViewState("datamode") = "edit" Or ViewState("datamode") = "view") And (Session("sroleid") = 14 Or (Not Session("PJVPost") Is Nothing AndAlso Session("PJVPost") = "1")) Then
            Session("PJVPost") = "0"
            If Not isPJV Then
                trCC.Visible = True
                btnCCSave.Visible = True
                fillGridView(PRCFooter, grdCCS, "SELECT PRC_ID, PRM_CODE, PRM_DESCR, PRC_CCS_ID, PRC_CCS_DESCR, PRC_AMT from PRF_C inner JOIN PRF_CM on PRM_ID=PRC_TYP_ID where PRC_PRF_ID=" & h_EntryId.Value & " order by PRC_ID")

                If h_Prf_Status.Value = "C" Then
                    trChangeRequest1.Visible = True
                    trChangeRequest2.Visible = True
                    txtRSup_id.Visible = Not isFramework
                    imgRSup_id.Visible = Not isFramework
                    lblRSup_id.Visible = Not isFramework
                    txtRAdjust.Text = "0.0"
                End If
            End If
        ElseIf isMSO And Not isPJV And (h_Prf_Status.Value = "C" Or h_Prf_Status.Value = "S") Then 'And h_Prf_apr_id.Value > 200 
            trChangeRequest1.Visible = True
            trChangeRequest2.Visible = True
            trChangeRequest3.Visible = False
            lblRAdjust.Visible = False
            lblRAdjustMore.Visible = False
            txtRAdjust.Visible = False
            txtRAdjust.Text = "0.0"
        ElseIf ViewState("MainMnu_code") = "PI02020" Then
            btnAccept.Visible = True
            btnReject.Visible = True
            trApproval.Visible = True
            trApproval.HeaderText = "Change Request Comments"
        End If
        SetExportFileLink()
    End Sub

    Private Function writeWorkFlow(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_PRF_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", txtApprovals.Text.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveWorkFlowMain", iParms)
        writeWorkFlow = iParms(8).Value
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnPrint.Visible = Not btnSave.Visible
        grdPRF.DataSource = PRFFooter
        grdPRF.DataBind()
        showNoRecordsFound()


        Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
        acBSUFooter = TryCast(grdPRF.FooterRow.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
        acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & h_Dpt_id.Value & ";" & h_Actid.Value & ";" & Session("sBsuid") & ";" & Session("BSU_CURRENCY")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancelTop.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        doApproval()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Function doApproval(Optional ByVal prf_id As Integer = 0) As Integer
        Dim byPass As Boolean = False
        If prf_id = 0 Then prf_id = h_EntryId.Value
        If prf_id = -1 Then
            byPass = True
            prf_id = h_EntryId.Value
        End If

        Dim strApprover() As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverMain(" & prf_id & ")").ToString.Split(";")
        doApproval = -1
        If strApprover(0) = 500 Then 'finally approved
            doApproval = writeWorkFlow(prf_id, "Procurement", "Procurement Department, PRF archived", 0, "C")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update PRF_H set PRF_APR_ID=500, PRF_STATUS='C', PRF_STATUS_STR='Procurement:PRF Archived' where PRF_ID=" & prf_id)
            End If
        Else
            If byPass Then
                doApproval = writeWorkFlow(prf_id, "Pass", "technical approval is not required for this order", strApprover(0), "S")
            Else
                doApproval = writeWorkFlow(prf_id, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S")
            End If
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update PRF_H set PRF_STATUS_STR='Approval:with " & strApprover(1) & "', PRF_STATUS=case when PRF_STATUS='N' then 'S' else PRF_STATUS end, PRF_APR_ID=" & strApprover(0) & " where PRF_ID=" & prf_id)
            End If
        End If
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        SetDataMode("add")
        setModifyvalues(0)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdPRF.DataSource = PRFFooter
        grdPRF.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sqlStr As String, sqlActid As String = "", sqlBudgeted As String = "", sqlPriority As String = ""
        If ViewState("MainMnu_code") = "PI02020" Then
            Dim iParms(4) As SqlClient.SqlParameter
            iParms(1) = Mainclass.CreateSqlParameter("@PRF_ID", h_EntryId.Value, SqlDbType.Int)
            iParms(2) = Mainclass.CreateSqlParameter("@PRF_APPROVE", "1234", SqlDbType.VarChar)
            iParms(3) = Mainclass.CreateSqlParameter("@PRF_RUSERNAME", Session("sUsr_name"), SqlDbType.VarChar)
            iParms(4) = Mainclass.CreateSqlParameter("@prf_comments", txtApprovals.Text, SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "updateChangeRequest", iParms)
        Else
            If imgActid.Visible And lblRed.Visible And h_unBudgeted.Value = "0" And radBudgeted.Checked And txtApprovals.Text <> "" Then
                lblError.Text = "This PRF is exceeding budgets, if you still want to save as Budgeted then enter valid reasons in comments, then click on approve"
                h_unBudgeted.Value = "1"
                Exit Sub
            ElseIf radUnBudgeted.Enabled And radUnBudgeted.Checked And (txtComments.Text = "" Or txtApprovals.Text = "") Then
                lblError.Text = "This PRF is un budget, enter valid reasons in comments, then click on approve"
                Exit Sub
            End If
            If doApproval() = 0 Then
                If imgActid.Visible And h_Actid.Value <> p_Actid Then
                    sqlActid = "PRF_ACT_ID='" & h_Actid.Value & "',"
                End If
                If radBudgeted.Checked <> p_Budgeted Then
                    sqlBudgeted = "PRF_BUDGETED='" & IIf(radBudgeted.Checked, 0, 1) & "',"
                End If
            End If
            If ddlPriority.SelectedValue <> p_Priority Then
                sqlPriority = "PRF_PRI_ID=" & ddlPriority.SelectedValue & ","
            End If
            If sqlActid & sqlBudgeted & sqlPriority <> "" Then
                sqlStr = "update PRF_H set " & sqlActid & sqlBudgeted & sqlPriority & " PRF_BSU_ID=PRF_BSU_ID FROM PRF_H WHERE PRF_ID=" & h_EntryId.Value
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, sqlStr)
            End If
            If isCAPEX And txtAltTotal.Enabled AndAlso Convert.ToDouble(txtAltTotal.Text) <> Convert.ToDouble(hdnAltTotal.Value) Then
                sqlStr = "update PRF_H set PRF_ALT_TOTAL='" & txtAltTotal.Text.Replace(",", "") & "' WHERE PRF_ID=" & h_EntryId.Value
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, sqlStr)
                writeWorkFlow(ViewState("EntryId"), "Estimate", "Supplier Estimate:" & txtAltTotal.Text, 0, "V")
            End If
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnPass_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If doApproval(-1) = 0 Then
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click, btnRejectTop.Click
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Rejection Comments are Mandatory"
            Exit Sub
        End If
        If ViewState("MainMnu_code") = "PI02020" Then
            Dim iParms(4) As SqlClient.SqlParameter
            iParms(1) = Mainclass.CreateSqlParameter("@PRF_ID", h_EntryId.Value, SqlDbType.Int)
            iParms(2) = Mainclass.CreateSqlParameter("@PRF_APPROVE", "", SqlDbType.VarChar)
            iParms(3) = Mainclass.CreateSqlParameter("@PRF_RUSERNAME", Session("sUsr_name"), SqlDbType.VarChar)
            iParms(4) = Mainclass.CreateSqlParameter("@prf_comments", txtApprovals.Text, SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "updateChangeRequest", iParms)
        Else
            If h_Prf_apr_id.Value = 220 Or h_Prf_apr_id.Value = 280 Or h_Prf_apr_id.Value = 270 Then
                If writeWorkFlow(ViewState("EntryId"), "Declined", txtApprovals.Text, h_Prf_apr_id.Value, "R") = 0 Then
                    Dim sqlStr As String = "update PRF_H set PRF_STATUS_STR='Rejected:by " & Session("sUsr_name") & "', QUO_ID1=0, QUO_ID3=0, PRF_APR_ID=200, prf_sup_id='none' where PRF_ID=" & h_EntryId.Value
                    sqlStr &= " update b set b.prf_deleted=1 from prf_h a inner join prf_h b on a.prf_id=b.quo_id2 where a.prf_id=" & h_EntryId.Value
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, sqlStr)
                End If
            Else
                If writeWorkFlow(ViewState("EntryId"), "Rejected", txtApprovals.Text, h_Prf_apr_id.Value, "R") = 0 Then
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update PRF_H set PRF_STATUS_STR='Rejected:by " & Session("sUsr_name") & "', PRF_STATUS='R' where PRF_ID=" & h_EntryId.Value)
                End If
            End If
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update PRF_H set PRF_STATUS_STR='Deleted', PRF_STATUS='D', PRF_DELETED=1 where PRF_ID=@PRF_ID"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@PRF_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                writeWorkFlow(ViewState("EntryId"), "Deleted", txtApprovals.Text, 0, "D")
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click, btnPrintTop.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim PRF_ID As Integer = CType(h_EntryId.Value, Integer)
            Dim cmd As New SqlCommand
            'writeWorkFlow(ViewState("EntryId"), "Printed", "", 0, "P")

            cmd.CommandText = "rptPRFDetails"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@PRF_ID", PRF_ID, SqlDbType.Int)
            cmd.Parameters.Add(sqlParam(0))
            If (isSEA And h_Prf_apr_id.Value = 500) Or (ViewState("MainMnu_code") = "PI02005" And h_Prf_apr_id.Value = 500) Or (ViewState("MainMnu_code") = "PI02012" And h_Prf_apr_id.Value = 380) Then
                sqlParam(1) = Mainclass.CreateSqlParameter("@PRF_FLAG", 0, SqlDbType.Int)
            Else
                sqlParam(1) = Mainclass.CreateSqlParameter("@PRF_FLAG", 1, SqlDbType.Int)
            End If

            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            params("showHide") = "prf"
            params("userName") = Session("sUsr_name")
            If ViewState("MainMnu_code") = "PI02005" Or ViewState("MainMnu_code") = "PI04005" Then
                If h_Prf_apr_id.Value = 500 Then
                    If isSEA Then
                        params("reportCaption") = "Purchase Order"
                    Else
                        params("reportCaption") = "Framework Purchase Request Form"
                    End If

                Else
                    params("reportCaption") = "Framework Purchase Request Form - " & IIf(h_Prf_Status.Value = "N", "New Approval Pending", IIf(h_Prf_Status.Value = "S", "Approval Pending", IIf(h_Prf_Status.Value = "R", "Rejected", "Archived")))
                End If
            ElseIf ViewState("MainMnu_code") = "PI02006" Or ViewState("MainMnu_code") = "PI04007" Then
                If isSEA And h_Prf_apr_id.Value = 500 Then
                    params("reportCaption") = "Purchase Order"
                Else
                    params("reportCaption") = "Non Framework Purchase Request Form - " & IIf(h_Prf_Status.Value = "N", "New", IIf(h_Prf_Status.Value = "S", "Approval Pending", IIf(h_Prf_Status.Value = "R", "Rejected", "Archived")))
                End If
            ElseIf ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02016" Then
                If txtSupplier.Text = "none" Then
                    params("reportCaption") = "Quotation Request Form"
                    params("showHide") = "quote"
                Else
                    params("reportCaption") = "Purchase Order"
                End If
            Else
                params("reportCaption") = "Non Framework Purchase Request Form - Approval Pending"
            End If


            params("@PRF_ID") = PRF_ID
            params("Menu") = ViewState("MainMnu_code")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.IncludeBSUImage = True

            If h_printed.Value = 1 And (ViewState("MainMnu_code") = "PI02012" Or ViewState("MainMnu_code") = "PI02016") Then
                writeWorkFlow(ViewState("EntryId"), "PRINTPO", "Purchase Order Printed", 0, "O")

                If CDate(txtPRFDate.Text) <= CDate("25/Nov/2019") Then
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptPO.rpt"
                Else
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptPONEW.rpt"
                End If



                doApproval()
                writeWorkFlow(ViewState("EntryId"), "Buyer", "buyer, processed Purchase order", 0, "Z")
                writeWorkFlow(ViewState("EntryId"), "Procurement", "Procurement Department, PRF archived", 0, "C")
                h_printed.Value = 0
            ElseIf isSEA And h_Prf_apr_id.Value = 500 Then
                writeWorkFlow(ViewState("EntryId"), "PRINTPO", "Purchase Order Printed", 0, "O")
                If Session("sBSUID") = "800555" Then
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptPOGWE.rpt"
                    
                ElseIf Session("sBSUID") = "900201" Then
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptPOGWS.rpt"
                ElseIf Session("sBSUID") = "315888" Then
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptPOGIP.rpt"
                Else
                    If CDate(txtPRFDate.Text) < CDate("25/Nov/2019") Then
                        repSource.ResourceName = "../../Inventory/Reports/RPT/rptPOCIK.rpt"
                    Else
                        repSource.ResourceName = "../../Inventory/Reports/RPT/rptPOCIKNEW.rpt"
                    End If
                End If
                h_printed.Value = 0
            ElseIf ViewState("MainMnu_code") = "PI02005" And h_Prf_apr_id.Value = 500 Then
                writeWorkFlow(ViewState("EntryId"), "PRINTPRF", "Framework Request Form Printed", 0, "O")

                If CDate(txtPRFDate.Text) <= CDate("25/Nov/2019") Then
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptPRF.rpt"
                Else
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptPRFNEW.rpt"
                End If



            Else


                Dim subRep(2) As MyReportClass
                subRep(0) = New MyReportClass
                Dim subcmd1 As New SqlCommand
                subcmd1.CommandText = "rptPRF_Items"
                Dim sqlParam1(1) As SqlParameter
                sqlParam1(0) = Mainclass.CreateSqlParameter("@PRF_ID", PRF_ID, SqlDbType.Int)
                subcmd1.Parameters.Add(sqlParam1(0))
                subcmd1.CommandType = Data.CommandType.StoredProcedure
                subcmd1.Connection = New SqlConnection(str_conn)
                subRep(0).Command = subcmd1
                subRep(0).ResourceName = "wBTA_Sector"

                Dim subcmd2 As New SqlCommand
                subRep(1) = New MyReportClass
                subcmd2.CommandText = "rptPRF_Items"
                Dim sqlParam2(0) As SqlParameter
                sqlParam2(0) = Mainclass.CreateSqlParameter("@PRF_ID", PRF_ID, SqlDbType.Int)
                subcmd2.Parameters.Add(sqlParam2(0))
                subcmd2.CommandType = Data.CommandType.StoredProcedure
                subcmd2.Connection = New SqlConnection(str_conn)
                subRep(1).Command = subcmd2
                subRep(1).ResourceName = "wQUT_Report"

                If CDate(txtPRFDate.Text) < CDate("25/Nov/2019") Then
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptReq.rpt"
                Else
                    repSource.ResourceName = "../../Inventory/Reports/RPT/rptReqNEW.rpt"
                End If


                repSource.SubReport = subRep
            End If
            repSource.IncludeBSUImage = True
            'End If
            Session("ReportSource") = repSource
            'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then

            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)

        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        h_EntryId.Value = 0
        ViewState("EntryId") = 0
        Session("prf_id") = 0
        SetDataMode("add")
        h_block.Value = ""
        txtPRFNo.Text = "NEW"
        txtPRFDate.Text = Now.ToString("dd/MMM/yyyy")
        txtDELDate.Text = DateAdd(DateInterval.Day, 14, Now).ToString("dd/MMM/yyyy")
        h_Prf_apr_id.Value = "0"
        h_QUO_ID1.Value = "0"
        h_QUO_ID2.Value = "0"
        h_QUO_ID3.Value = "0"
        If Not isSEA And Not isFramework Then
            hTPT_Client_ID.Value = "none"
            txtSupplier.Text = ""
        End If
        grdPRF.DataSource = PRFFooter
        grdPRF.DataBind()
        showNoRecordsFound()
        trFlow.Visible = False
        trFlow.HeaderText = ""
        trDocument.Visible = False
        trDocument.HeaderText = ""
        trWorkflow.Visible = False
        trWorkflow.HeaderText = ""
        imgClient.Visible = False
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        If doApproval(h_QUO_ID2.Value) = 0 And h_QUO_ID2.Value > 0 Then
            Dim sqlStr As String
            Try
                sqlStr = "update a set a.prd_details=b.prd_details, a.prd_rate=b.prd_rate from ("
                sqlStr &= "select ROW_NUMBER() OVER (ORDER BY f1.prd_id ASC) id, f1.* "
                sqlStr &= "from prf_h h1 inner join prf_d f1 on h1.prf_id=f1.prd_prf_id where h1.prf_id=" & h_QUO_ID2.Value & ") a inner join "
                sqlStr &= "(select ROW_NUMBER() OVER (ORDER BY f2.prd_id ASC) id, f2.* "
                sqlStr &= "from prf_h h2 inner join prf_d f2 on h2.prf_id=f2.prd_prf_id where h2.prf_id=" & h_EntryId.Value & ") b on a.id=b.id "

                'sqlStr = "UPDATE D1 set D1.PRD_RATE=D2.PRD_RATE, D1.prd_details=D2.prd_details FROM PRF_D D1 INNER JOIN PRF_D D2 ON D1.PRD_ITM_ID=D2.PRD_ITM_ID AND D1.PRD_DESCR=D2.PRD_DESCR and D1.PRD_DETAILS=D2.PRD_DETAILS and D1.PRD_PRF_ID=" & h_QUO_ID2.Value & " and D2.PRD_PRF_ID=" & h_EntryId.Value
                sqlStr &= " update prf_h set PRF_TOTAL=(select sum(PRD_QTY*PRD_RATE) FROM PRF_D where PRF_ID=PRD_PRF_ID), prf_status='S', prf_sup_id='" & hTPT_Client_ID.Value & "' where prf_id=" & h_QUO_ID2.Value
                sqlStr &= " update prf_h set PRF_SUPPLIER='" & txtSupplierAddress.Text.Replace("'", "`") & "', PRF_TERMS='" & txtTerms.Text & "', QUO_ID3=" & h_EntryId.Value & " where (prf_id=" & h_QUO_ID2.Value & " or QUO_ID2=" & h_QUO_ID2.Value & ")"
                sqlStr &= " update oasisfin..accounts_m set ACT_CONTACTPERSON='" & IIf(txtSupplierAddress.Text.Length >= 50, "", txtSupplierAddress.Text.Replace("'", "`")) & "' where act_id='" & hTPT_Client_ID.Value & "'"

                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, sqlStr)
                writeWorkFlow(h_QUO_ID2.Value, "Quotation", "Confirm Supplier: " & txtSupplier.Text, 0, "Q")
            Catch ex As Exception

            End Try
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnSendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click
        If ViewState("MainMnu_code") = "PI02012" Then
            If isFramework Then
                lblError.Text = "only Non Framework PRF can be emailed"
            Else
                SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "insert into prf_m (prm_prf_id, prm_femail, prm_temail, prm_contact, prm_status, prm_flag, prm_result) select " & h_EntryId.Value & ", emd_email, '', '', 0, 0, '' from oasis..employee_d inner join oasis..users_m on usr_emp_id=emd_emp_id and usr_name='" & Session("sUsr_name") & "' ")
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Else
            If txtSupplierEmail.Text = "" Then
                lblError.Text = "select Supplier"
                Exit Sub
            ElseIf txtSupplierEmailId.Text = "" Then
                lblError.Text = "enter email"
                Exit Sub
            End If
            'Dim msoEmail As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select distinct USRA_EMAIL PRM_TEMAIL from PRF_H inner join oasis..businessunit_m on BSU_ID=PRF_BSU_ID inner join usersa on usra_bsu_id=prf_bsu_id and prf_dpt_id=usra_dpt_id and prf_subdpt_id=usra_subdpt_id and usra_email is not null where PRF_ID=" & h_EntryId.Value)
            Dim msoEmail As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select '" & txtSupplierEmailId.Text & "' PRM_TEMAIL from PRF_H inner join oasis..businessunit_m on BSU_ID=PRF_BSU_ID inner join usersa on usra_bsu_id=prf_bsu_id and prf_dpt_id=usra_dpt_id and prf_subdpt_id=usra_subdpt_id and usra_email is not null where PRF_ID=" & h_EntryId.Value)
            lblError.Text = "Email Sent to " & msoEmail

            'If isQuotation Or txtSupplier.Text = "none" Then
            '    writeWorkFlow(ViewState("EntryId"), "Email Quotation", "Quotation emailed to " & msoEmail, 0, "E")
            '    txtSupplierEmail.Text = ""
            '    txtSupplierEmailId.Text = ""
            '    hdnSupplierEmail.Value = ""
            'Else
            '    writeWorkFlow(ViewState("EntryId"), "Email PO", "Purchase order emailed to " & msoEmail, 0, "E")
            'End If
            'If Not txtSupplierEmailId.Text.Contains("gems") Then
            '    SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "UPDATE oasisfin..accounts_m set ACT_EMAIL='" & txtSupplierEmailId.Text & "' where ACT_EMAIL='' and ACT_ID='" & hTPT_Client_ID.Value & "'")
            'End If

            SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "insert into prf_m (prm_prf_id, prm_femail, prm_temail, prm_contact, prm_status, prm_flag, prm_result) select " & h_EntryId.Value & ",'" & txtSupplierEmailId.Text & "','Email PO', '', 0, 0, '' from oasis..employee_d inner join oasis..users_m on usr_emp_id=emd_emp_id and usr_name='" & Session("sUsr_name") & "' ")
            SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "insert into workflow (WRK_DATE, WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) select getdate(),'" & Session("sUsr_name") & "','Email PO', 'Purchase order emailed to  " & txtSupplierEmail.Text & "','" & h_EntryId.Value & "',0, 'E'")

            gvWorkflow.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select replace(convert(varchar(16), WRK_DATE, 106),' ','/') WRK_DATE, USR_DISPLAY_NAME WRK_USER, WRK_ACTION, WRK_DETAILS from WORKFLOW inner JOIN OASIS.dbo.USERS_M on WRK_USERNAME=USR_NAME where WRK_PRF_ID=" & h_EntryId.Value & " order by WRK_ID desc").Tables(0)
            gvWorkflow.DataBind()
        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Purchase\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename

            If h_Prf_Status.Value = "C" Then
                lblError.Text = "upload not allowed for Archived PRFs" '!!!!
                Exit Sub
            End If

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath" '!!!!
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Dim sqlParam(5) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@PRI_ID", 0, SqlDbType.Int, True)
                sqlParam(1) = Mainclass.CreateSqlParameter("@PRI_PRF_ID", h_EntryId.Value, SqlDbType.Int)
                sqlParam(2) = Mainclass.CreateSqlParameter("@PRI_CONTENTTYPE", ContentType, SqlDbType.VarChar)
                sqlParam(3) = Mainclass.CreateSqlParameter("@PRI_NAME", UploadDocPhoto.FileName, SqlDbType.VarChar)
                sqlParam(4) = Mainclass.CreateSqlParameter("@PRI_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                sqlParam(5) = Mainclass.CreateSqlParameter("@PRI_FILENAME", txtPRFNo.Text & "_~" & FileExt, SqlDbType.VarChar)

                Try
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "savePRF_I", sqlParam)
                    IO.File.Move(strFilepath, str_img & txtPRFNo.Text & "_" & sqlParam(0).Value & FileExt)
                    showImages()
                    writeWorkFlow(ViewState("EntryId"), "ADDEDIMG", "Added upload document " & UploadDocPhoto.FileName, 0, "O")
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If

        trComments.Focus()
    End Sub

    Private Sub showImages()
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select pri_id, pri_name, case when pri_username='" & Session("sUsr_name") & "' then 'true' else 'false' end pri_visible, PRI_Filename, PRI_ContentType from PRF_P inner join PRF_H on PRI_PRF_ID=PRF_ID where PRI_PRF_ID=" & ViewState("EntryId")).Tables(0)
        rptImages.DataSource = dt
        rptImages.DataBind()
    End Sub

    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select

    End Function

    Protected Sub txtSupplier_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSupplier.TextChanged
        Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
        acBSUFooter = TryCast(grdPRF.FooterRow.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
        If isFramework Then acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & h_Dpt_id.Value & ";" & h_Actid.Value & ";" & ddlBSU.SelectedValue & ";" & Session("BSU_CURRENCY")
    End Sub

    Protected Sub rptImages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptImages.ItemCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.CommandSource.text = "x" Then
                If h_Prf_Status.Value = "C" Or h_Prf_Status.Value = "R" Or h_Prf_Status.Value = "D" Then
                    lblError.Text = "Cannot delete file"
                Else
                    Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete"), LinkButton)
                    Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
                    Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from prf_p where pri_id=" & lblPriId.Text)
                    writeWorkFlow(ViewState("EntryId"), "DELETEDIMG", "Deleted upload document " & btnDocumentLink.Text, 0, "O")
                    showImages()
                End If
            End If
        End If
    End Sub

    Protected Sub rptImages_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptImages.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
            Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
            Dim lblPriFilename As TextBox = CType(e.Item.FindControl("lblPriFilename"), TextBox)
            Dim lblPriContentType As TextBox = CType(e.Item.FindControl("lblPriContentType"), TextBox)
            btnDocumentLink.Attributes.Add("OnClick", "javascript:  showDocument(" & lblPriId.Text & ",'" & lblPriFilename.Text & "','" & lblPriContentType.Text & "'); return false;")
        End If
    End Sub

    Protected Sub TabContainer1_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabContainer1.ActiveTabChanged
        ViewState("ActiveTabIndex") = TabContainer1.ActiveTabIndex
    End Sub

    Protected Sub btnBuyer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuyer.Click
        If btnBuyer.Text = "Release" Then
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update PRF_H set QUO_ID1=0 where QUO_ID2=" & h_EntryId.Value & " or PRF_ID=" & h_EntryId.Value)
        Else
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update PRF_H set QUO_ID1='" & Session("EmployeeId") & "' where QUO_ID2=" & h_EntryId.Value & " or PRF_ID=" & h_EntryId.Value)
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Sub SetExportFileLink()
        Try
            Session("myData") = PRFFooter
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("PRF-" & txtPRFNo.Text)

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnUploadPrf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadPrf.Click
        If UploadFile.HasFile Then
            Dim str_tempfilename As String = UploadFile.FileName 'Session("sUsr_name") & Session.SessionID & UploadFile.FileName
            Dim strFilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & str_tempfilename
            Dim FileError As Boolean = False
            UploadFile.PostedFile.SaveAs(strFilepath)

            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath" '!!!!
                Exit Sub
            Else
                If IO.File.Exists(strFilepath) Then
                    Dim filename As String = Path.GetFileName(strFilepath)
                    Dim ext As String = Path.GetExtension(strFilepath)
                    lblError.Text = ""
                    If ext = ".xls" Then
                        Try
                            Dim xlTable As DataTable, resTable As DataTable
                            xlTable = Mainclass.FetchFromExcel("select * from [TableName]", strFilepath)
                            If xlTable.Rows.Count.Equals(0) Then
                                lblError.Text = "Could not process the request.Invalid data in excel..!"
                            End If
                            File.Delete(strFilepath)

                            Dim strFields As String = ""
                            For cols As Int16 = 0 To xlTable.Columns.Count - 1
                                strFields &= xlTable.Columns(cols).ColumnName
                            Next
                            If strFields.Contains("IDPRD_IDPRD_ITM_IDPRD_DESCRPRD_DETAILSPRD_IMAGEPRD_QTYPRD_RATEPRD_TOTAL") Then
                                Dim iParms(4) As SqlClient.SqlParameter
                                iParms(1) = Mainclass.CreateSqlParameter("@strtransfer", DataTableToString(xlTable, "transfer"), SqlDbType.VarChar)
                                iParms(2) = Mainclass.CreateSqlParameter("@strprftype", h_PRF_Type.Value, SqlDbType.VarChar)
                                iParms(3) = Mainclass.CreateSqlParameter("@strprfid", h_EntryId.Value, SqlDbType.Int)
                                iParms(4) = Mainclass.CreateSqlParameter("@strmessage", Space(100), SqlDbType.VarChar, True)

                                resTable = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "importPRF", iParms).Tables(0)

                                If resTable.Rows.Count > 0 And iParms(4).Value = "" Then
                                    For Each dr As DataRow In resTable.Rows
                                        Dim idStr As String = dr("ID").ToString()
                                        If h_EntryId.Value = "0" Then
                                            Dim mrow As DataRow
                                            mrow = PRFFooter.NewRow
                                            mrow("PRD_ID") = 0
                                            mrow("PRD_QTY") = dr("PRD_QTY").ToString()
                                            mrow("PRD_ITM_ID") = dr("PRD_ITM_ID").ToString()
                                            mrow("PRD_DESCR") = dr("PRD_DESCR").ToString()
                                            mrow("PRD_DETAILS") = dr("PRD_DETAILS").ToString()
                                            mrow("PRD_RATE") = dr("PRD_RATE").ToString()
                                            mrow("PRD_TOTAL") = dr("PRD_TOTAL").ToString()
                                            txtSupplier.Text = dr("ACT_NAME").ToString()
                                            hTPT_Client_ID.Value = dr("ACT_ID").ToString()
                                            h_term.Value = dr("PRD_TOTAL").ToString()
                                            txtSupplierAddress.Text = dr("ACT_ADDRESS").ToString()

                                            PRFFooter.Rows.Add(mrow)
                                        Else
                                            Dim mrow As DataRow
                                            mrow = PRFFooter.Select("ID=" & dr("ID").ToString())(0)
                                            mrow("PRD_ITM_ID") = dr("PRD_ITM_ID").ToString()
                                            mrow("PRD_DESCR") = dr("PRD_DESCR").ToString()
                                            mrow("PRD_RATE") = dr("PRD_RATE").ToString()
                                            mrow("PRD_TOTAL") = dr("PRD_TOTAL").ToString()
                                            mrow("PRD_DETAILS") = dr("PRD_DETAILS").ToString()
                                            mrow("PRD_QTY") = dr("PRD_QTY").ToString()
                                        End If
                                    Next
                                    txtTotal.Text = Convert.ToDouble(PRFFooter.Compute("sum(PRD_TOTAL)", "")).ToString("#,###,##0.00")
                                    txtAltTotal.Text = (Convert.ToDouble(txtTotal.Text) / Convert.ToDouble(txtExgRate.Text)).ToString("#,###,##0.00")
                                    h_total.Value = txtTotal.Text

                                    grdPRF.EditIndex = -1
                                    grdPRF.DataSource = PRFFooter
                                    grdPRF.DataBind()
                                    showNoRecordsFound()
                                Else
                                    If resTable.Rows.Count = 0 Then
                                        lblError.Text = "Error: nothing to import"
                                    Else
                                        lblError.Text = "Error:" & iParms(4).Value
                                    End If
                                End If
                            Else
                                lblError.Text = "Excel file format invalid"
                            End If

                        Catch ex As Exception
                            lblError.Text = "Error:" & ex.Message
                            Exit Sub
                        End Try
                    End If
                Else
                    lblError.Text = "upload only Excel File"
                End If
            End If
            If FileError And lblError.Text.Length = 0 Then lblError.Text = "Problems encountered while uploading file, please try again"
        Else
            lblError.Text = "File not selected " '!!!
        End If
    End Sub

    Private Function DataTableToString(ByRef dt As DataTable, ByVal strRootElementName As String) As String
        Dim sb As New StringBuilder()
        If strRootElementName = "" Then
            strRootElementName = Convert.ToString(dt.TableName)
        End If
        For Each dr As DataRow In dt.Rows
            sb.Append("<" & strRootElementName & ">")
            For Each dc As DataColumn In dt.Columns
                If dc.ColumnName.ToLower.Contains("date") AndAlso dr(dc).ToString().Length > 0 Then
                    sb.Append(("<" & dc.ColumnName & ">" & Format(dr(dc), "dd-MMM-yyyy hh:mm:ss") & "</") & dc.ColumnName & ">")
                Else
                    sb.Append(("<" + dc.ColumnName & ">" & dr(dc).ToString().Replace("’", "`") & "</") + dc.ColumnName & ">")
                End If
            Next
            sb.Append("</" & strRootElementName & ">")
        Next
        Return sb.ToString()
    End Function

    Protected Sub btnQuotation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        requestForQuotation = True
        SetDataMode("edit")
        setModifyvalues(ViewState("EntryId"))
        txtPRFNo.Text = "NEW"
        hTPT_Client_ID.Value = ""
        h_PRF_Type.Value = "Q"
        ViewState("datamode") = "add"
        imgClient.Visible = True
        imgActid.Visible = False
        grdPRF.Columns(8).Visible = False
        trDocument.Visible = False
        trApproval.Visible = False
        trEmail.Visible = False
        trWorkflow.Visible = False
        trFlow.Visible = False
        trDocument.HeaderText = ""
        trApproval.HeaderText = ""
        trEmail.HeaderText = ""
        trWorkflow.HeaderText = ""
        trFlow.HeaderText = ""
        txtTerms.Enabled = True
        btnQutSave.Visible = True
    End Sub

    Protected Sub txtDELDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDELDate.TextChanged
        Try
            If CType(txtDELDate.Text, Date) < CType(txtPRFDate.Text, Date) Then
                txtDELDate.Text = txtPRFDate.Text
            End If

            'Added by Mahesh on 17-Dec-2020 as per Sagi Request

            If FyearClosed = False Then
                txtDELDate.Text = txtPRFDate.Text
            End If
        Catch ex As Exception
            txtDELDate.Text = txtPRFDate.Text
        End Try

        

        showNoRecordsFound()
    End Sub

    Protected Sub btnMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMessage.Click, btnMessageTop.Click
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Message comments are Mandatory"
            Exit Sub
        End If
        writeWorkFlow(ViewState("EntryId"), "Message", txtApprovals.Text, h_Prf_apr_id.Value, "M")
        If isCAPEX And txtAltTotal.Enabled AndAlso Convert.ToDouble(txtAltTotal.Text) <> Convert.ToDouble(hdnAltTotal.Value) Then
            Dim sqlStr As String
            sqlStr = "update PRF_H set PRF_ALT_TOTAL='" & txtAltTotal.Text.Replace(",", "") & "' WHERE PRF_ID=" & h_EntryId.Value
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, sqlStr)
            writeWorkFlow(ViewState("EntryId"), "Estimate", "Supplier Estimate:" & txtAltTotal.Text, 0, "V")
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChange.Click
        Dim RFlag As String = "", RReason As String = ""
        If hdnRAct_id.Value <> "" And h_Actid.Value <> hdnRAct_id.Value Then
            RFlag &= "1"
            RReason &= "Budget Account=" & hdnRAct_id.Value & ","
        End If
        If hdnRSup_id.Value <> "" And hTPT_Client_ID.Value <> hdnRSup_id.Value Then
            RFlag &= "2"
            RReason &= "Supplier Account=" & hdnRSup_id.Value & ","
        End If
        If chkRCancel.Checked Then
            RFlag &= "3"
            RReason &= "Cancel PRF,"
        End If
        If Val(txtRAdjust.Text) > -2 AndAlso Val(txtRAdjust.Text) > 0 AndAlso Val(txtRAdjust.Text) <> Val(txtTotal.Text) Then
            RFlag &= "4"
            RReason &= "Adjust Amount=" & txtRAdjust.Text & ","
        End If
        If RFlag = "" Then
            lblError.Text = "Nothing to save"
            Exit Sub
        ElseIf txtRReason.Text.Trim.Length < 3 Then
            lblError.Text = "Reason for change mandatory"
            Exit Sub
        End If

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        Dim kParms(10) As SqlClient.SqlParameter
        kParms(1) = Mainclass.CreateSqlParameter("@PRF_ID", h_EntryId.Value, SqlDbType.VarChar)
        kParms(2) = Mainclass.CreateSqlParameter("@PRF_RACT_ID", hdnRAct_id.Value, SqlDbType.VarChar)
        kParms(3) = Mainclass.CreateSqlParameter("@PRF_RSUP_ID", hdnRSup_id.Value, SqlDbType.VarChar)
        kParms(4) = Mainclass.CreateSqlParameter("@PRF_RCANCEL", IIf(chkRCancel.Checked, 1, 0), SqlDbType.Int)
        kParms(5) = Mainclass.CreateSqlParameter("@PRF_RADJUST", txtRAdjust.Text, SqlDbType.Decimal)
        kParms(6) = Mainclass.CreateSqlParameter("@PRF_REQUEST", RFlag, SqlDbType.VarChar)
        kParms(8) = Mainclass.CreateSqlParameter("@PRF_RREASON", RReason & "Reason=" & txtRReason.Text, SqlDbType.VarChar)
        kParms(9) = Mainclass.CreateSqlParameter("@PRF_RUSERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        kParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        kParms(7).Direction = ParameterDirection.ReturnValue
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "savePRF_Change", kParms)
            stTrans.Commit()

        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        End Try
        Response.Redirect(ViewState("ReferrerUrl"))

    End Sub

    Protected Sub btnCCSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCCSave.Click
        Dim sqlStr As String, sqlActid As String = "", sqlBudgeted As String = "", sqlPriority As String = ""
        If imgActid.Visible And h_Actid.Value <> p_Actid Then
            sqlActid = "PRF_ACT_ID='" & h_Actid.Value & "',"
        End If
        If radBudgeted.Checked <> p_Budgeted Then
            sqlBudgeted = "PRF_BUDGETED='" & IIf(radBudgeted.Checked, 0, 1) & "',"
        End If
        If ddlPriority.SelectedValue <> p_Priority Then
            sqlPriority = "PRF_PRI_ID=" & ddlPriority.SelectedValue & ","
        End If
        If sqlActid & sqlBudgeted & sqlPriority <> "" Then
            sqlStr = "update PRF_H set " & sqlActid & sqlBudgeted & sqlPriority & " PRF_BSU_ID=PRF_BSU_ID FROM PRF_H WHERE PRF_ID=" & h_EntryId.Value
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, sqlStr)
        End If

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Dim flag As Integer = 1
        Try
            Dim PRCOrder As Integer = 0
            Dim RowCount As Integer, InsCount As Integer = 0
            For RowCount = 0 To PRCFooter.Rows.Count - 1
                Dim iParms(7) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = PRCFooter.Rows(RowCount)("PRC_ID")
                'If PRFFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@PRC_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@PRC_PRF_ID", h_EntryId.Value, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@PRM_CODE", PRCFooter.Rows(RowCount)("PRM_CODE"), SqlDbType.VarChar)
                If Not IsDBNull(PRCFooter.Rows(RowCount)("PRM_CODE")) Then If PRCFooter.Rows(RowCount)("PRM_CODE") <> "0000" Then PRCOrder += 1
                iParms(4) = Mainclass.CreateSqlParameter("@PRC_ORDER", PRCOrder, SqlDbType.Int)
                iParms(5) = Mainclass.CreateSqlParameter("@PRC_CCS_ID", PRCFooter.Rows(RowCount)("PRC_CCS_ID"), SqlDbType.VarChar)
                iParms(6) = Mainclass.CreateSqlParameter("@PRC_CCS_DESCR", PRCFooter.Rows(RowCount)("PRC_CCS_DESCR"), SqlDbType.VarChar)
                iParms(7) = Mainclass.CreateSqlParameter("@PRC_AMT", PRCFooter.Rows(RowCount)("PRC_AMT"), SqlDbType.Decimal)

                InsCount += 1
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "savePRF_C", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error " '!!!
                    stTrans.Rollback()
                    Exit Sub
                Else
                    flag = 0
                End If
            Next
            If flag = 0 And h_PRCGridDelete.Value <> "0" Then
                flag = 1
                Dim deleteId() As String = h_PRCGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    InsCount -= 1
                    iParms(1) = Mainclass.CreateSqlParameter("@PRC_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@PRC_PRF_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from PRF_C where PRC_ID=@PRC_ID and PRC_PRF_ID=@PRC_PRF_ID", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error " '!!!
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If
            Dim jParms(2) As SqlClient.SqlParameter
            jParms(1) = Mainclass.CreateSqlParameter("@PRF_ID", h_EntryId.Value, SqlDbType.VarChar)
            jParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            jParms(2).Direction = ParameterDirection.ReturnValue
            Try
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "validCCS", jParms)
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try

            If jParms(2).Value = -1 Then
                lblError.Text = "Cost Center, Subledger mismatch"
                showNoRecordsFound()
                stTrans.Rollback()
            ElseIf jParms(2).Value = -2 And h_Actid.Value = p_Actid Then
                lblError.Text = "Cost Center, Subledger totals not equal to PRF Total"
                showNoRecordsFound()
                stTrans.Rollback()
            ElseIf jParms(2).Value = -3 Then
                lblError.Text = "Subledger mandatory"
                showNoRecordsFound()
                stTrans.Rollback()
            ElseIf jParms(2).Value = -4 Then
                lblError.Text = "Cost Center mandatory"
                showNoRecordsFound()
                stTrans.Rollback()
            ElseIf jParms(2).Value = -5 Then
                lblError.Text = "unexpected error"
                showNoRecordsFound()
                stTrans.Rollback()
            Else
                stTrans.Commit()
                flag = 0
                Response.Redirect(ViewState("ReferrerUrl"))
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
            showNoRecordsFound()
            If flag = 1 Then stTrans.Rollback()
        End Try
    End Sub

    Protected Sub btnSuper_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSuper.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update PRF_H set PRF_STATUS_STR='Deleted', PRF_STATUS='D', PRF_DELETED=1 where PRF_ID=@PRF_ID"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@PRF_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                writeWorkFlow(ViewState("EntryId"), "Deleted", "Deleted by Purchase Department", 0, "D")
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
        End If
    End Sub

    Protected Sub ddlCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrency.SelectedIndexChanged
        'txtExgRate.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select CUR_EXGRATE from oasisfin..CURRENCY_M where CUR_ID='" & ddlCurrency.SelectedItem.Value & "'")
        txtExgRate.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select EXG_RATE from oasisfin.[dbo].[EXGRATE_S] where EXG_BSU_ID='" & Session("sBsuid") & "' and getdate() between EXG_FDATE and isnull(exg_tdate,getdate()) and EXG_CUR_ID='" & ddlCurrency.SelectedItem.Value & "'")
        ddlCurrency.SelectedValue = ddlCurrency.SelectedItem.Value
        lblExgCurr.Text = "Total Amount (" & ddlCurrency.SelectedItem.Value & ")"

        If CType(txtExgRate.Text, Decimal) <> 0 Then txtAltTotal.Text = CType(txtTotal.Text, Decimal) / CType(txtExgRate.Text, Decimal)
        lblExgCurr.Visible = True
        txtAltTotal.Visible = True
        showNoRecordsFound()
    End Sub

    Protected Sub btnQutSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnSave_Click(sender, e)
        btnConfirm_Click(sender, e)
    End Sub

    Protected Sub ddlDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged
        Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
        acBSUFooter = TryCast(grdPRF.FooterRow.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
        acBSUFooter.ContextKey = hTPT_Client_ID.Value & ";" & ddlDepartment.SelectedValue & ";" & h_Actid.Value & ";" & Session("sBsuid") & ";" & Session("BSU_CURRENCY")
    End Sub

    Protected Sub btnSave_ServerClick(sender As Object, e As EventArgs) Handles btnSave.ServerClick

    End Sub

    Protected Sub btnSend_ServerClick(sender As Object, e As EventArgs) Handles btnSend.ServerClick

    End Sub

    Protected Sub ddlTAX_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTAX.SelectedIndexChanged

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 2
        pParms(1) = New SqlClient.SqlParameter("@TAXCODE", SqlDbType.VarChar, 20)
        pParms(1).Value = ddlTAX.SelectedValue
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_CODES]", pParms)

        If Not ds Is Nothing And ds.Tables(0).Rows.Count >= 1 Then
            h_TAXPerc.Value = ds.Tables(0).Rows(0)("TAX_PERC_VALUE")
        End If

        'h_TAXPerc.Value =ddlTAX.SelectedValue
        calculateWithTAX()
    End Sub
    Private Sub calculateWithTAX()
        txtTax.Text = Convert.ToDouble(Val(CDbl(txtGross.Text)) * h_TAXPerc.Value / 100.0).ToString("#,###,##0.00")
        txtTotal.Text = Convert.ToDouble(Val(CDbl(txtGross.Text)) + Val(CDbl(txtTax.Text))).ToString("#,###,##0.00")
        txtAltTotal.Text = (Convert.ToDouble(txtTotal.Text) / Convert.ToDouble(txtExgRate.Text)).ToString("#,###,##0.00")
        h_total.Value = txtTotal.Text
        hdnAltTotal.Value = txtAltTotal.Text

        'txtTotal.Text = Convert.ToDouble(Val(CDbl(txtGross.Text)) + Val(CDbl(txtTax.Text))).ToString("#,###,##0.00")
    End Sub

End Class
