Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic

Partial Class BTA
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "U000088" And ViewState("MainMnu_code") <> "U000086") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
            Else
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            End If
            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdBO.DataSource = BOFooter
            grdBO.DataBind()
            grdItinerary.DataSource = ItineraryFooter
            grdItinerary.DataBind()
            grdTravelPlan.DataSource = TravelPlanFooter
            grdTravelPlan.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            'grdBO.DataSource = BOFooter
            'grdBO.DataBind()
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If
    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        lblBTANo.Visible = Not (ViewState("datamode") = "add")
        txtBTANo.Visible = Not (ViewState("datamode") = "add")

        txtVisaCost.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtVisaCost.Attributes.Add("onblur", " return formatme('" & txtVisaCost.ClientID & "');")
        'txtExpAmt.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtExpAmtAed.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtExpAmtAed.Attributes.Add("onblur", " return formatme('" & txtExpAmtAed.ClientID & "');")
        'txtEconomyClass.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtEconomyClassAed.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtEconomyClassAed.Attributes.Add("onblur", " return formatme('" & txtEconomyClassAed.ClientID & "');")
        'txtBusinessClass.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtBusinessClassAed.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtBusinessClassAed.Attributes.Add("onblur", " return formatme('" & txtBusinessClassAed.ClientID & "');")
        'txtTravelInsurance.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtTravelInsuranceAed.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtTravelInsuranceAed.Attributes.Add("onblur", " return formatme('" & txtTravelInsuranceAed.ClientID & "');")

        txtAdvanceRequestAed.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtAdvanceRequestAed.Attributes.Add("onblur", " return formatme('" & txtAdvanceRequestAed.ClientID & "');")

        'txtAdvanceRequest.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtAdvanceRequestAed.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtAdvanceRequestAed.Attributes.Add("onblur", " return formatme('" & txtAdvanceRequestAed.ClientID & "');")
        txtAmountBudgeted.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtAmountBudgeted.Attributes.Add("onblur", " return formatme('" & txtAmountBudgeted.ClientID & "');")
        txtAmountChgClient.Attributes.Add("onkeypress", " return Numeric_Only()")
        txtAmountChgClient.Attributes.Add("onblur", " return formatme('" & txtAmountChgClient.ClientID & "');")

        txtVisaCost.Attributes.Add("onkeyup", " return calculate();")
        txtExpAmtAed.Attributes.Add("onkeyup", " return calculate();")
        txtEconomyClassAed.Attributes.Add("onkeyup", " return calculate();")
        txtBusinessClassAed.Attributes.Add("onkeyup", " return calculate();")
        txtTravelInsuranceAed.Attributes.Add("onkeyup", " return calculate();")

        grdBO.Columns(5).Visible = Not mDisable
        grdBO.Columns(6).Visible = Not mDisable
        grdBO.ShowFooter = Not mDisable
        grdItinerary.Columns(7).Visible = Not mDisable
        grdItinerary.Columns(8).Visible = Not mDisable
        grdItinerary.ShowFooter = Not mDisable
        grdTravelPlan.Columns(8).Visible = Not mDisable
        grdTravelPlan.Columns(9).Visible = Not mDisable
        grdTravelPlan.ShowFooter = Not mDisable

        'txtBTANo.Enabled = EditAllowed
        txtEmployeeName.Enabled = EditAllowed
        'txtDepartment.Enabled = EditAllowed
        txtDesgination.Enabled = EditAllowed
        'txtAdvanceRequest.Enabled = EditAllowed
        txtAdvanceRequestAed.Enabled = EditAllowed
        txtAmountBudgeted.Enabled = EditAllowed
        txtAmountChgClient.Enabled = EditAllowed
        txtBTADate.Enabled = EditAllowed
        'txtBusinessClass.Enabled = EditAllowed
        txtBusinessClassAed.Enabled = EditAllowed
        txtClientName.Enabled = EditAllowed
        'txtEconomyClass.Enabled = EditAllowed
        txtEconomyClassAed.Enabled = EditAllowed
        'txtExpAmt.Enabled = EditAllowed
        txtExpAmtAed.Enabled = EditAllowed
        txtLPONo.Enabled = EditAllowed
        txtMobileNo.Enabled = EditAllowed
        'txtTravelInsurance.Enabled = EditAllowed
        txtTravelInsuranceAed.Enabled = EditAllowed
        txtVisaCost.Enabled = EditAllowed
        txtEntitlement.Enabled = EditAllowed

        ddlEntitlement.Enabled = EditAllowed
        chkVisa.Enabled = EditAllowed
        'upload.Style.Add("display", IIf(bDisable, "none", ""))
        'SetItemEditMode(Not bDisable)

        btnCopy.Visible = ViewState("MainMnu_code") = "U000086" And mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = ViewState("MainMnu_code") = "U000086" And mDisable
        btnDelete.Visible = False 'mDisable Or ViewState("MainMnu_code") = "U000086"
        btnEdit.Visible = mDisable And ViewState("MainMnu_code") = "U000088"
        btnPrint.Visible = mDisable
        btnAccept.Visible = ViewState("MainMnu_code") = "U000088" And mDisable
        btnReject.Visible = ViewState("MainMnu_code") = "U000088" And mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim empDetailsSql As String = "SELECT cast(EMP_ID AS varchar)+';'+EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME+';'+DES_DESCR+';'+DPT_DESCR FROM EMPLOYEE_M innner JOIN EMPDESIGNATION_M on EMP_DES_ID=DES_ID INNER JOIN DEPARTMENT_M on EMP_DPT_ID=DPT_ID where EMP_ID=" & Session("EmployeeId")
            h_EntryId.Value = p_Modifyid
            fillDropdown(ddlEntitlement, "select 0 ddlId, 'Select Entitlement' ddlDescr union select 1, 'First' union select 2, 'Business' union select 3, 'Premium Economy' union select 4, 'Economy' union select 5, 'Others'", "ddlDescr", "ddlId", False)
            If p_Modifyid = 0 Then
                Dim emp_Details() As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnection, CommandType.Text, empDetailsSql).ToString.Split(";")

                h_Emp_id.Value = emp_Details(0)
                txtEmployeeName.Text = emp_Details(1)
                txtDesgination.Text = emp_Details(2)
                txtDepartment.Text = emp_Details(3)
            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                dt = MainObj.getRecords("select replace(convert(varchar(30), Bta_Date, 106),' ','/') Bta_Date_Str, * from BTA_Details where BTA_No=" & p_Modifyid, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    empDetailsSql = "SELECT cast(EMP_ID AS varchar)+';'+EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME+';'+DES_DESCR+';'+DPT_DESCR FROM EMPLOYEE_M innner JOIN EMPDESIGNATION_M on EMP_DES_ID=DES_ID INNER JOIN DEPARTMENT_M on EMP_DPT_ID=DPT_ID where EMP_ID=" & dt.Rows(0)("Emp_id")
                    h_Emp_id.Value = dt.Rows(0)("Emp_id")
                    txtEmployeeName.Text = dt.Rows(0)("Emp_name")
                    txtDesgination.Text = dt.Rows(0)("Emp_designation")
                    txtDepartment.Text = dt.Rows(0)("Emp_department")

                    txtBTANo.Text = dt.Rows(0)("BTA_NumberStr")
                    txtLPONo.Text = dt.Rows(0)("LPO_no")
                    txtBTADate.Text = dt.Rows(0)("Bta_Date_Str")

                    chkVisa.Checked = dt.Rows(0)("visa_req")
                    txtVisaCost.Text = dt.Rows(0)("visa_cost")

                    'txtExpAmt.Text = dt.Rows(0)("LocalTravel_Exp")
                    txtExpAmtAed.Text = dt.Rows(0)("LocalTravel_ExpAed")
                    'txtBusinessClass.Text = dt.Rows(0)("Tkt_Business")
                    txtBusinessClassAed.Text = dt.Rows(0)("Tkt_BusinessAed")
                    'txtEconomyClass.Text = dt.Rows(0)("Tkt_Economy")
                    txtEconomyClassAed.Text = dt.Rows(0)("Tkt_EconomyAed")
                    'txtTravelInsurance.Text = dt.Rows(0)("Insurance")
                    txtTravelInsuranceAed.Text = dt.Rows(0)("InsuranceAed")
                    txtMobileNo.Text = dt.Rows(0)("MobileNo")
                    txtTotalC.Text = dt.Rows(0)("TotalC")
                    txtAdvanceRequestAed.Text = dt.Rows(0)("Trip_Advance")

                    txtAmountBudgeted.Text = dt.Rows(0)("Amt_Budgeted")
                    txtAmountChgClient.Text = dt.Rows(0)("Amt_Client")
                    txtClientName.Text = dt.Rows(0)("Client_Name")
                    txtTotalA.Text = dt.Rows(0)("TotalA")
                    lblTotalABC.Text = dt.Rows(0)("TotalA") + dt.Rows(0)("visa_cost") + dt.Rows(0)("TotalC")
                    ddlEntitlement.SelectedValue = dt.Rows(0)("Entitlement")
                    txtEntitlement.Text = dt.Rows(0)("Entitlementtxt")
                    h_bta_Status.Value = dt.Rows(0)("BTA_Status")
                    If ddlEntitlement.SelectedValue = 5 Then
                        txtEntitlement.Style.Add("display", "")
                    Else
                        txtEntitlement.Style.Add("display", "none")
                    End If
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
            btnAccept.Visible = btnAccept.Visible And h_bta_Status.Value = "O"
            btnReject.Visible = btnAccept.Visible And h_bta_Status.Value = "O"
            btnEdit.Visible = btnAccept.Visible And h_bta_Status.Value = "O"

            FormatFigures()

            fillGridView(BOFooter, grdBO, "select Sec_No, Sec_Travel, Bus_objectives, Sec_company from BTA_Sector where Sec_BTA_No=" & h_EntryId.Value & " order by Sec_No")
            fillGridView(ItineraryFooter, grdItinerary, "select Itn_No, replace(convert(varchar(30), Itn_Date, 106),' ','/') Itn_Date, Itn_Sector, Itn_FlightNo, Itn_Time, Itn_Hours from BTA_itinerary where Itn_BTA_No=" & h_EntryId.Value & " order by Itn_No")
            fillGridView(TravelPlanFooter, grdTravelPlan, "select Trv_No, Trv_BTA_No, replace(convert(varchar(30), Trv_From, 106),' ','/') Trv_From, replace(convert(varchar(30), Trv_To, 106),' ','/') Trv_To, Trv_service, Trv_Nights, Trv_DailyExp, Trv_TotalExp from BTA_Travel where Trv_BTA_No=" & h_EntryId.Value & " order by Trv_No")

            ddlEntitlement.Attributes.Add("onChange", "javascript:entitlement()")
            If p_Modifyid > 0 Then txtTotalA.Text = TravelPlanFooter.Compute("sum(Trv_TotalExp)", "")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        h_delete.Value = "0"
        h_Emp_id.Value = Session("EmployeeId")

        txtLPONo.Text = ""
        txtBTADate.Text = Now.ToString("dd/MMM/yyyy")
        txtMobileNo.Text = ""
        'ddlEntitlement

        txtEmployeeName.Text = ""
        txtDesgination.Text = ""
        'txtExpAmt.Text = "0.0"
        txtExpAmtAed.Text = "0.0"
        'txtBusinessClass.Text = "0.0"
        txtBusinessClassAed.Text = "0.0"
        'txtEconomyClass.Text = "0.0"
        txtEconomyClassAed.Text = "0.0"
        'txtTravelInsurance.Text = "0.0"
        txtTravelInsuranceAed.Text = "0.0"
        txtTotalC.Text = "0.0"
        txtTotalA.Text = "0.0"
        txtEntitlement.Text = ""
        txtVisaCost.Text = "0.0"
        lblTotalABC.Text = "0.0"
        txtAmountBudgeted.Text = "0.0"
        txtAmountChgClient.Text = "0.0"
        txtClientName.Text = ""

        txtEntitlement.Style.Add("display", "none")
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdBO.DataSource = BOFooter
        grdBO.DataBind()
        grdItinerary.DataSource = ItineraryFooter
        grdItinerary.DataBind()
        grdTravelPlan.DataSource = TravelPlanFooter
        grdTravelPlan.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdBO.DataSource = BOFooter
        grdBO.DataBind()
        grdItinerary.DataSource = ItineraryFooter
        grdItinerary.DataBind()
        grdTravelPlan.DataSource = TravelPlanFooter
        grdTravelPlan.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblError.Text = ""
        If txtClientName.Text.Trim.Length = 0 Then lblError.Text &= "Client Name,"
        If txtBTADate.Text.Length = 0 Then lblError.Text &= "BTA Date,"
        If ddlEntitlement.SelectedIndex = 0 Then lblError.Text &= "Entitlement,"
        If lblError.Text.Length > 0 Then lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Mandatory"
        If CType(lblTotalABC.Text, Decimal) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Total Expenses (A+B+C) cannot be 0"
        If IsDate(txtBTADate.Text) Then
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM oasisfin.dbo.vw_OSO_FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "' and '" & txtBTADate.Text & "' between FYR_FROMDT and isnull(FYR_TODT,getdate())") = 0 Then lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        Else
            lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        End If

        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(23) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@BTA_No", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@LPO_no", txtLPONo.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@Emp_id", h_Emp_id.Value, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@BTA_date", txtBTADate.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@Entitlement", ddlEntitlement.SelectedIndex, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@MobileNo", txtMobileNo.Text, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@Visa_req", IIf(chkVisa.Checked, 1, 0), SqlDbType.Int)
        pParms(8) = Mainclass.CreateSqlParameter("@Visa_cost", txtVisaCost.Text, SqlDbType.Decimal)
        'pParms(9) = Mainclass.CreateSqlParameter("@LocalTravel_Exp", txtExpAmt.Text, SqlDbType.Decimal)
        pParms(9) = Mainclass.CreateSqlParameter("@LocalTravel_ExpAed", txtExpAmtAed.Text, SqlDbType.Decimal)
        'pParms(11) = Mainclass.CreateSqlParameter("@Tkt_Business", txtBusinessClass.Text, SqlDbType.Decimal)
        pParms(10) = Mainclass.CreateSqlParameter("@Tkt_BusinessAed", txtBusinessClassAed.Text, SqlDbType.Decimal)
        'pParms(13) = Mainclass.CreateSqlParameter("@Tkt_Economy", txtEconomyClass.Text, SqlDbType.Decimal)
        pParms(11) = Mainclass.CreateSqlParameter("@Tkt_EconomyAed", txtEconomyClassAed.Text, SqlDbType.Decimal)
        'pParms(15) = Mainclass.CreateSqlParameter("@Insurance", txtTravelInsurance.Text, SqlDbType.Decimal)
        pParms(12) = Mainclass.CreateSqlParameter("@InsuranceAed", txtTravelInsuranceAed.Text, SqlDbType.Decimal)
        'pParms(17) = Mainclass.CreateSqlParameter("@Trip_Advance", txtAdvanceRequest.Text, SqlDbType.Decimal)
        pParms(13) = Mainclass.CreateSqlParameter("@Amt_Budgeted", txtAmountBudgeted.Text, SqlDbType.Decimal)
        pParms(14) = Mainclass.CreateSqlParameter("@Amt_Client", txtAmountChgClient.Text, SqlDbType.Decimal)
        pParms(15) = Mainclass.CreateSqlParameter("@Client_Name", txtClientName.Text, SqlDbType.VarChar)
        pParms(16) = Mainclass.CreateSqlParameter("@TotalA", txtTotalA.Text, SqlDbType.Decimal)
        pParms(17) = Mainclass.CreateSqlParameter("@TotalC", txtTotalC.Text, SqlDbType.Decimal)
        pParms(18) = Mainclass.CreateSqlParameter("@Entitlementtxt", txtEntitlement.Text, SqlDbType.VarChar)
        pParms(20) = Mainclass.CreateSqlParameter("@Emp_Name", txtEmployeeName.Text, SqlDbType.VarChar)
        pParms(21) = Mainclass.CreateSqlParameter("@Emp_Designation", txtDesgination.Text, SqlDbType.VarChar)
        pParms(22) = Mainclass.CreateSqlParameter("@Emp_Department", txtDepartment.Text, SqlDbType.VarChar)
        pParms(23) = Mainclass.CreateSqlParameter("@Trip_Advance", txtAdvanceRequestAed.Text, SqlDbType.Decimal)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveBTA_Details", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim RowCount As Integer
            For RowCount = 0 To BOFooter.Rows.Count - 1
                Dim iParms(5) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, BOFooter.Rows(RowCount)("Sec_No"))
                If BOFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@Sec_No", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@Sec_BTA_No", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@Sec_Travel", BOFooter.Rows(RowCount)("Sec_Travel"), SqlDbType.VarChar)
                iParms(4) = Mainclass.CreateSqlParameter("@Bus_objectives", BOFooter.Rows(RowCount)("Bus_objectives"), SqlDbType.VarChar)
                iParms(5) = Mainclass.CreateSqlParameter("@Sec_company", BOFooter.Rows(RowCount)("Sec_company"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveBTA_Sector", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            For RowCount = 0 To ItineraryFooter.Rows.Count - 1
                Dim iParms(7) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, ItineraryFooter.Rows(RowCount)("Itn_No"))
                If ItineraryFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@Itn_No", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@Itn_BTA_No", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@Itn_Date", ItineraryFooter.Rows(RowCount)("Itn_Date"), SqlDbType.VarChar)
                iParms(4) = Mainclass.CreateSqlParameter("@Itn_Sector", ItineraryFooter.Rows(RowCount)("Itn_Sector"), SqlDbType.VarChar)
                iParms(5) = Mainclass.CreateSqlParameter("@Itn_FlightNo", ItineraryFooter.Rows(RowCount)("Itn_FlightNo"), SqlDbType.VarChar)
                iParms(6) = Mainclass.CreateSqlParameter("@Itn_Time", ItineraryFooter.Rows(RowCount)("Itn_Time"), SqlDbType.VarChar)
                iParms(7) = Mainclass.CreateSqlParameter("@Itn_Hours", ItineraryFooter.Rows(RowCount)("Itn_Hours"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveBTA_Itinerary", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            For RowCount = 0 To TravelPlanFooter.Rows.Count - 1
                Dim iParms(7) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, TravelPlanFooter.Rows(RowCount)("Trv_No"))
                If TravelPlanFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@Trv_No", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@Trv_BTA_No", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@Trv_From", TravelPlanFooter.Rows(RowCount)("Trv_From"), SqlDbType.VarChar)
                iParms(4) = Mainclass.CreateSqlParameter("@Trv_To", TravelPlanFooter.Rows(RowCount)("Trv_To"), SqlDbType.VarChar)
                iParms(5) = Mainclass.CreateSqlParameter("@Trv_service", TravelPlanFooter.Rows(RowCount)("Trv_service"), SqlDbType.VarChar)
                iParms(6) = Mainclass.CreateSqlParameter("@Trv_Nights", TravelPlanFooter.Rows(RowCount)("Trv_Nights"), SqlDbType.VarChar)
                iParms(7) = Mainclass.CreateSqlParameter("@Trv_DailyExp", TravelPlanFooter.Rows(RowCount)("Trv_DailyExp"), SqlDbType.VarChar)
                'iParms(8) = Mainclass.CreateSqlParameter("@Trv_TotalExp", TravelPlanFooter.Rows(RowCount)("Trv_TotalExp"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveBTA_Travel", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next

            If h_BOGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_BOGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@Sec_No", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@Sec_Bta_No", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from BTA_Sector where Sec_No=@Sec_No and Sec_Bta_No=@Sec_Bta_No", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            If h_itnGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_itnGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@Itn_No", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@Itn_Bta_No", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from BTA_itinerary where Itn_No=@Itn_No and Itn_Bta_No=@Itn_Bta_No", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            If h_trvGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_trvGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@Trv_No", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@Trv_Bta_No", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from BTA_Travel where Trv_No=@Trv_No and Trv_Bta_No=@Trv_Bta_No", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Response.Redirect(ViewState("ReferrerUrl"), False)
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "delete from BTA_Travel where Trv_BTA_No=@BTA_NO"
                sqlStr &= " delete from BTA_itinerary where Itn_BTA_No=@BTA_NO"
                sqlStr &= " delete from BTA_Sector where Sec_BTA_No=@BTA_NO"
                sqlStr &= " delete from BTA_Details where BTA_no=@BTA_NO"

                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@BTA_NO", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"))
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
    End Sub

    Private Sub showNoRecordsFound()
        If Not BOFooter Is Nothing AndAlso BOFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdBO.Columns.Count - 2
            grdBO.Rows(0).Cells.Clear()
            grdBO.Rows(0).Cells.Add(New TableCell())
            grdBO.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdBO.Rows(0).Cells(0).Text = "No Record Found"
        End If
        If Not ItineraryFooter Is Nothing AndAlso ItineraryFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdItinerary.Columns.Count - 2
            grdItinerary.Rows(0).Cells.Clear()
            grdItinerary.Rows(0).Cells.Add(New TableCell())
            grdItinerary.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdItinerary.Rows(0).Cells(0).Text = "No Record Found"
        End If
        If Not TravelPlanFooter Is Nothing AndAlso TravelPlanFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdTravelPlan.Columns.Count - 2
            grdTravelPlan.Rows(0).Cells.Clear()
            grdTravelPlan.Rows(0).Cells.Add(New TableCell())
            grdTravelPlan.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdTravelPlan.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Private Property BOFooter() As DataTable
        Get
            Return ViewState("BOFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("BOFooter") = value
        End Set
    End Property

    Private Property ItineraryFooter() As DataTable
        Get
            Return ViewState("ItineraryFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("ItineraryFooter") = value
        End Set
    End Property

    Private Property TravelPlanFooter() As DataTable
        Get
            Return ViewState("TravelPlanFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TravelPlanFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub grdBO_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdBO.RowCancelingEdit
        grdBO.ShowFooter = True
        grdBO.EditIndex = -1
        grdBO.DataSource = BOFooter
        grdBO.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdBO_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdBO.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtSec_Travel As TextBox = grdBO.FooterRow.FindControl("txtSec_Travel")
            Dim txtBus_objectives As TextBox = grdBO.FooterRow.FindControl("txtBus_objectives")
            Dim txtSec_company As TextBox = grdBO.FooterRow.FindControl("txtSec_company")
            If txtClientName.Text.Length = 0 Then txtClientName.Text = txtSec_company.Text

            lblError.Text = ""
            If txtSec_Travel.Text.Trim.Length = 0 Then lblError.Text &= "Sector Travelling"
            If txtBus_objectives.Text.Trim.Length = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Business Objectives"
            If txtSec_company.Text.Trim.Length = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Travel related to which Company"

            lblError.Text = ""
            lblError.Text = validBO(txtSec_Travel.Text.Trim, txtBus_objectives.Text.Trim, txtSec_company.Text.Trim)
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If

            If lblError.Text.Length > 0 Then
                lblError.Text &= " Mandatory!!!!"
                showNoRecordsFound()
                Exit Sub
            End If

            If BOFooter.Rows(0)(1) = -1 Then BOFooter.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = BOFooter.NewRow
            mrow("Sec_No") = 0
            mrow("Sec_Travel") = txtSec_Travel.Text
            mrow("Bus_objectives") = txtBus_objectives.Text
            mrow("Sec_company") = txtSec_company.Text
            BOFooter.Rows.Add(mrow)

            grdBO.EditIndex = -1
            grdBO.DataSource = BOFooter
            grdBO.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdBO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdBO.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim imgClient As ImageButton = e.Row.FindControl("imgClient")
            Dim txtSec_company As TextBox = e.Row.FindControl("txtSec_company")
            Dim hTPT_Client_ID As HiddenField = e.Row.FindControl("hTPT_Client_ID")
            If Not txtSec_company Is Nothing Then
                imgClient.Attributes.Add("onclick", "javascript:return getClientName('" & txtSec_company.ClientID & "','" & hTPT_Client_ID.ClientID & "');")
            End If
        End If
    End Sub

    Protected Sub grdBO_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdBO.RowDeleting
        Dim mRow() As DataRow = BOFooter.Select("ID=" & grdBO.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_BOGridDelete.Value &= ";" & mRow(0)("Sec_No")
            BOFooter.Select("ID=" & grdBO.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            BOFooter.AcceptChanges()
        End If
        If BOFooter.Rows.Count = 0 Then
            BOFooter.Rows.Add(BOFooter.NewRow())
            BOFooter.Rows(0)(1) = -1
        End If
        grdBO.DataSource = BOFooter
        grdBO.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdBO_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdBO.RowEditing
        Try
            grdBO.ShowFooter = True
            grdBO.EditIndex = e.NewEditIndex
            grdBO.DataSource = BOFooter
            grdBO.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdBO_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdBO.RowUpdating
        Dim s As String = grdBO.DataKeys(e.RowIndex).Value.ToString()
        Dim lblSEC_NO As Label = grdBO.Rows(e.RowIndex).FindControl("lblSEC_NO")
        Dim txtSec_Travel As TextBox = grdBO.Rows(e.RowIndex).FindControl("txtSec_Travel")
        Dim txtBus_objectives As TextBox = grdBO.Rows(e.RowIndex).FindControl("txtBus_objectives")
        Dim txtSec_company As TextBox = grdBO.Rows(e.RowIndex).FindControl("txtSec_company")

        Dim mrow As DataRow
        mrow = BOFooter.Select("ID=" & s)(0)
        mrow("Sec_Travel") = txtSec_Travel.Text
        mrow("Bus_objectives") = txtBus_objectives.Text
        mrow("Sec_company") = txtSec_company.Text

        grdBO.EditIndex = -1
        grdBO.DataSource = BOFooter
        grdBO.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdItinerary_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdItinerary.RowCancelingEdit
        grdItinerary.ShowFooter = True
        grdItinerary.EditIndex = -1
        grdItinerary.DataSource = ItineraryFooter
        grdItinerary.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdItinerary_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdItinerary.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtItn_Date As TextBox = grdItinerary.FooterRow.FindControl("txtItn_Date")
            Dim txtItn_Sector As TextBox = grdItinerary.FooterRow.FindControl("txtItn_Sector")
            Dim txtItn_FlightNo As TextBox = grdItinerary.FooterRow.FindControl("txtItn_FlightNo")
            Dim txtItn_Time As TextBox = grdItinerary.FooterRow.FindControl("txtItn_Time")
            Dim txtItn_Hours As TextBox = grdItinerary.FooterRow.FindControl("txtItn_Hours")

            lblError.Text = ""
            lblError.Text = validItineraryPlan(txtItn_Date.Text.Trim, txtItn_Sector.Text.Trim, txtItn_FlightNo.Text.Trim)
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If

            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If

            If ItineraryFooter.Rows(0)(1) = -1 Then ItineraryFooter.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = ItineraryFooter.NewRow
            mrow("Itn_No") = 0
            mrow("Itn_Date") = txtItn_Date.Text
            mrow("Itn_Sector") = txtItn_Sector.Text
            mrow("Itn_FlightNo") = txtItn_FlightNo.Text
            mrow("Itn_Time") = txtItn_Time.Text
            mrow("Itn_Hours") = txtItn_Hours.Text
            ItineraryFooter.Rows.Add(mrow)

            grdItinerary.EditIndex = -1
            grdItinerary.DataSource = ItineraryFooter
            grdItinerary.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdItinerary_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdItinerary.RowDeleting
        Dim mRow() As DataRow = ItineraryFooter.Select("ID=" & grdItinerary.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_itnGridDelete.Value &= ";" & mRow(0)("Itn_No")
            ItineraryFooter.Select("ID=" & grdItinerary.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            ItineraryFooter.AcceptChanges()
        End If
        If ItineraryFooter.Rows.Count = 0 Then
            ItineraryFooter.Rows.Add(ItineraryFooter.NewRow())
            ItineraryFooter.Rows(0)(1) = -1
        End If
        grdItinerary.DataSource = ItineraryFooter
        grdItinerary.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdItinerary_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdItinerary.RowEditing
        Try
            grdItinerary.ShowFooter = True
            grdItinerary.EditIndex = e.NewEditIndex
            grdItinerary.DataSource = ItineraryFooter
            grdItinerary.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdItinerary_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdItinerary.RowUpdating
        Dim s As String = grdItinerary.DataKeys(e.RowIndex).Value.ToString()
        Dim lblItn_No As Label = grdItinerary.Rows(e.RowIndex).FindControl("lblItn_No")
        Dim txtItn_Date As TextBox = grdItinerary.Rows(e.RowIndex).FindControl("txtItn_Date")
        Dim txtItn_Sector As TextBox = grdItinerary.Rows(e.RowIndex).FindControl("txtItn_Sector")
        Dim txtItn_FlightNo As TextBox = grdItinerary.Rows(e.RowIndex).FindControl("txtItn_FlightNo")
        Dim txtItn_Time As TextBox = grdItinerary.Rows(e.RowIndex).FindControl("txtItn_Time")
        Dim txtItn_Hours As TextBox = grdItinerary.Rows(e.RowIndex).FindControl("txtItn_Hours")

        lblError.Text = ""
        lblError.Text = validItineraryPlan(txtItn_Date.Text.Trim, txtItn_Sector.Text.Trim, txtItn_FlightNo.Text.Trim)
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim mrow As DataRow
        mrow = ItineraryFooter.Select("ID=" & s)(0)
        mrow("Itn_Date") = txtItn_Date.Text
        mrow("Itn_Sector") = txtItn_Sector.Text
        mrow("Itn_FlightNo") = txtItn_FlightNo.Text
        mrow("Itn_Time") = txtItn_Time.Text
        mrow("Itn_Hours") = txtItn_Hours.Text

        grdItinerary.EditIndex = -1
        grdItinerary.DataSource = ItineraryFooter
        grdItinerary.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdTravelPlan_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdTravelPlan.RowCancelingEdit
        grdTravelPlan.ShowFooter = True
        grdTravelPlan.EditIndex = -1
        grdTravelPlan.DataSource = TravelPlanFooter
        grdTravelPlan.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdTravelPlan_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTravelPlan.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtTrv_From As TextBox = grdTravelPlan.FooterRow.FindControl("txtTrv_From")
            Dim txtTrv_To As TextBox = grdTravelPlan.FooterRow.FindControl("txtTrv_To")
            Dim txtTrv_service As TextBox = grdTravelPlan.FooterRow.FindControl("txtTrv_service")
            Dim txtTrv_Nights As TextBox = grdTravelPlan.FooterRow.FindControl("txtTrv_Nights")
            Dim txtTrv_DailyExp As TextBox = grdTravelPlan.FooterRow.FindControl("txtTrv_DailyExp")
            Dim txtTrv_TotalExp As TextBox = grdTravelPlan.FooterRow.FindControl("txtTrv_TotalExp")

            lblError.Text = validTravelPlan(txtTrv_From.Text.Trim, txtTrv_To.Text.Trim, txtTrv_service.Text.Trim, txtTrv_Nights.Text.Trim, txtTrv_DailyExp.Text.Trim)
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If

            If TravelPlanFooter.Rows(0)(1) = -1 Then TravelPlanFooter.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = TravelPlanFooter.NewRow
            mrow("Trv_No") = 0
            mrow("Trv_From") = txtTrv_From.Text
            mrow("Trv_To") = txtTrv_To.Text
            mrow("Trv_service") = txtTrv_service.Text
            mrow("Trv_Nights") = txtTrv_Nights.Text
            mrow("Trv_DailyExp") = txtTrv_DailyExp.Text
            mrow("Trv_TotalExp") = txtTrv_DailyExp.Text * txtTrv_Nights.Text
            TravelPlanFooter.Rows.Add(mrow)

            txtTotalA.Text = Convert.ToDouble(TravelPlanFooter.Compute("sum(Trv_TotalExp)", "")).ToString("####0.00")
            lblTotalABC.Text = CType(txtTotalA.Text, Decimal) + CType(txtVisaCost.Text, Decimal) + CType(txtTotalC.Text, Decimal)

            grdTravelPlan.EditIndex = -1
            grdTravelPlan.DataSource = TravelPlanFooter
            grdTravelPlan.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Function validBO(ByVal txtSec_Travel As String, ByVal txtBus_objectives As String, ByVal txtSec_company As String) As String
        validBO = ""
        If txtSec_Travel.Length = 0 Then validBO &= "Sector Travelling"
        If txtBus_objectives.Length = 0 Then validBO &= IIf(validBO.Length = 0, "", ",") & "Business Objectives"
        If txtSec_company.Length = 0 Then validBO &= IIf(validBO.Length = 0, "", ",") & "Travel related to which Company"
    End Function

    Function validItineraryPlan(ByVal txtItn_Date As String, ByVal txtItn_Sector As String, ByVal txtItn_FlightNo As String) As String
        validItineraryPlan = ""
        If txtItn_Date.Length = 0 Then validItineraryPlan &= "Date of Travel"
        If txtItn_Sector.Length = 0 Then validItineraryPlan &= IIf(validItineraryPlan.Length = 0, "", ",") & "Sector"
        If txtItn_FlightNo.Length = 0 Then validItineraryPlan &= IIf(validItineraryPlan.Length = 0, "", ",") & "Flight No"
        If validItineraryPlan.Length > 0 Then validItineraryPlan &= "Mandatory!!!!"
        If Not IsDate(txtItn_Date) Then validItineraryPlan &= IIf(validItineraryPlan.Length = 0, "", ",") & "Invalid Date of Travel"
    End Function

    Function validTravelPlan(ByVal txtTrv_From As String, ByVal txtTrv_To As String, ByVal txtTrv_service As String, ByVal txtTrv_Nights As String, ByVal txtTrv_DailyExp As String) As String
        validTravelPlan = ""
        If txtTrv_From.Length = 0 Then validTravelPlan &= "From"
        If txtTrv_To.Length = 0 Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "To"
        If txtTrv_service.Length = 0 Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "Service"
        If txtTrv_Nights.Length = 0 Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "No of Nights"
        If txtTrv_DailyExp.Length = 0 Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "Est.Daily Expenditure"
        If validTravelPlan.Length > 0 Then validTravelPlan &= "Mandatory!!!!"
        If Not IsDate(txtTrv_From) Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "Invalid From Date"
        If Not IsDate(txtTrv_To) Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "Invalid To Date"
        If Not IsNumeric(txtTrv_Nights) Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "Invalid No of Nights"
        If Not IsNumeric(txtTrv_DailyExp) Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "Invalid Est.Daily Expenditure"
        If IsDate(txtTrv_From) And IsDate(txtTrv_To) Then
            If CType(txtTrv_To, Date) < CType(txtTrv_From, Date) Then validTravelPlan &= IIf(validTravelPlan.Length = 0, "", ",") & "From Date cannot be greater than To Date"
        End If
    End Function

    Protected Sub grdTravelPlan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTravelPlan.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Or e.Row.RowType = DataControlRowType.Footer Then
            Dim txtTrv_From As TextBox = e.Row.FindControl("txtTrv_From")
            If Not txtTrv_From Is Nothing Then
                Dim txtTrv_To As TextBox = e.Row.FindControl("txtTrv_To")
                Dim txttxtTrv_Nights As TextBox = e.Row.FindControl("txtTrv_Nights")
                txtTrv_From.Attributes.Add("onchange", "javascript:return test(" & String.Concat("'", txtTrv_From.ClientID, "'") & "," & String.Concat("'", txtTrv_To.ClientID, "'") & ")")
            End If

            Dim txtTrv_Nights As TextBox = e.Row.FindControl("txtTrv_Nights")
            txtTrv_Nights.Attributes.Add("OnKeyUp", "javascript:return calculateTravel()")
            txtTrv_Nights.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
            ClientScript.RegisterArrayDeclaration("grdNights", String.Concat("'", txtTrv_Nights.ClientID, "'"))

            Dim txtTrv_DailyExp As TextBox = e.Row.FindControl("txtTrv_DailyExp")
            txtTrv_DailyExp.Attributes.Add("OnKeyUp", "javascript:return calculateTravel()")
            txtTrv_DailyExp.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
            ClientScript.RegisterArrayDeclaration("grdDailyExp", String.Concat("'", txtTrv_DailyExp.ClientID, "'"))

            Dim txtTrv_TotalExp As TextBox = e.Row.FindControl("txtTrv_TotalExp")
            ClientScript.RegisterArrayDeclaration("grdTotalExp", String.Concat("'", txtTrv_TotalExp.ClientID, "'"))
        End If
    End Sub

    Protected Sub grdTravelPlan_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdTravelPlan.RowDeleting
        Dim mRow() As DataRow = TravelPlanFooter.Select("ID=" & grdTravelPlan.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_trvGridDelete.Value &= ";" & mRow(0)("Trv_No")
            TravelPlanFooter.Select("ID=" & grdTravelPlan.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            TravelPlanFooter.AcceptChanges()
        End If
        If TravelPlanFooter.Rows.Count = 0 Then
            TravelPlanFooter.Rows.Add(TravelPlanFooter.NewRow())
            TravelPlanFooter.Rows(0)(1) = -1
            txtTotalA.Text = "0"
            lblTotalABC.Text = CType(txtTotalA.Text, Decimal) + CType(txtVisaCost.Text, Decimal) + CType(txtTotalC.Text, Decimal)
        Else
            txtTotalA.Text = Convert.ToDouble(TravelPlanFooter.Compute("sum(Trv_TotalExp)", "")).ToString("####0.00")
            lblTotalABC.Text = CType(txtTotalA.Text, Decimal) + CType(txtVisaCost.Text, Decimal) + CType(txtTotalC.Text, Decimal)
        End If

        grdTravelPlan.DataSource = TravelPlanFooter
        grdTravelPlan.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdTravelPlan_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdTravelPlan.RowEditing
        Try
            grdTravelPlan.ShowFooter = True
            grdTravelPlan.EditIndex = e.NewEditIndex
            grdTravelPlan.DataSource = TravelPlanFooter
            grdTravelPlan.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdTravelPlan_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdTravelPlan.RowUpdating
        Dim s As String = grdTravelPlan.DataKeys(e.RowIndex).Value.ToString()
        Dim lblTrv_No As Label = grdTravelPlan.Rows(e.RowIndex).FindControl("lblTrv_No")
        Dim txtTrv_From As TextBox = grdTravelPlan.Rows(e.RowIndex).FindControl("txtTrv_From")
        Dim txtTrv_To As TextBox = grdTravelPlan.Rows(e.RowIndex).FindControl("txtTrv_To")
        Dim txtTrv_service As TextBox = grdTravelPlan.Rows(e.RowIndex).FindControl("txtTrv_service")
        Dim txtTrv_Nights As TextBox = grdTravelPlan.Rows(e.RowIndex).FindControl("txtTrv_Nights")
        Dim txtTrv_DailyExp As TextBox = grdTravelPlan.Rows(e.RowIndex).FindControl("txtTrv_DailyExp")
        Dim txtTrv_TotalExp As TextBox = grdTravelPlan.Rows(e.RowIndex).FindControl("txtTrv_TotalExp")

        lblError.Text = validTravelPlan(txtTrv_From.Text.Trim, txtTrv_To.Text.Trim, txtTrv_service.Text.Trim, txtTrv_Nights.Text.Trim, txtTrv_DailyExp.Text.Trim)
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim mrow As DataRow
        mrow = TravelPlanFooter.Select("ID=" & s)(0)
        mrow("Trv_From") = txtTrv_From.Text
        mrow("Trv_To") = txtTrv_To.Text
        mrow("Trv_service") = txtTrv_service.Text
        mrow("Trv_Nights") = txtTrv_Nights.Text
        mrow("Trv_DailyExp") = txtTrv_DailyExp.Text
        mrow("Trv_TotalExp") = txtTrv_DailyExp.Text * txtTrv_Nights.Text

        txtTotalA.Text = Convert.ToDouble(TravelPlanFooter.Compute("sum(Trv_TotalExp)", "")).ToString("####0.00")
        lblTotalABC.Text = CType(txtTotalA.Text,Decimal) + CType(txtVisaCost.Text, Decimal) + CType(txtTotalC.Text, Decimal)

        grdTravelPlan.EditIndex = -1
        grdTravelPlan.DataSource = TravelPlanFooter
        grdTravelPlan.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim BTA_No As Integer = CType(h_EntryId.Value, Integer)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptBTADetails"
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BTA_No", BTA_No, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "Business Travel and Advance Approval "
            params("@BTA_No") = 1
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptBTA.rpt"

            Dim subRep(2) As MyReportClass

            subRep(0) = New MyReportClass
            Dim subcmd1 As New SqlCommand
            subcmd1.CommandText = "rptBTASector"
            Dim sqlParam1(1) As SqlParameter
            sqlParam1(0) = Mainclass.CreateSqlParameter("@BTA_No", BTA_No, SqlDbType.Int)
            subcmd1.Parameters.Add(sqlParam1(0))
            subcmd1.CommandType = Data.CommandType.StoredProcedure
            subcmd1.Connection = New SqlConnection(str_conn)
            subRep(0).Command = subcmd1
            subRep(0).ResourceName = "wBTA_Sector"

            Dim subcmd2 As New SqlCommand
            subRep(1) = New MyReportClass
            subcmd2.CommandText = "rptBTAItinerary"
            Dim sqlParam2(0) As SqlParameter
            sqlParam2(0) = Mainclass.CreateSqlParameter("@BTA_No", BTA_No, SqlDbType.Int)
            subcmd2.Parameters.Add(sqlParam2(0))
            subcmd2.CommandType = Data.CommandType.StoredProcedure
            subcmd2.Connection = New SqlConnection(str_conn)
            subRep(1).Command = subcmd2
            subRep(1).ResourceName = "xBTA_Itinerary"

            Dim subcmd3 As New SqlCommand
            subRep(2) = New MyReportClass
            subcmd3.CommandText = "rptBTATravel"
            Dim sqlParam3(0) As SqlParameter
            sqlParam3(0) = Mainclass.CreateSqlParameter("@BTA_No", BTA_No, SqlDbType.Int)
            subcmd3.Parameters.Add(sqlParam3(0))
            subcmd3.CommandType = Data.CommandType.StoredProcedure
            subcmd3.Connection = New SqlConnection(str_conn)
            subRep(2).Command = subcmd3
            subRep(2).ResourceName = "yBTA_Travel"

            repSource.IncludeBSUImage = True
            repSource.SubReport = subRep
            'End If
            Session("ReportSource") = repSource
            Response.Redirect("../Reports/ASPX Report/RptviewerSS.aspx", True)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next
        txtVisaCost.Text = Convert.ToDouble(txtVisaCost.Text).ToString("####0.00")
        'txtExpAmt.Text = Convert.ToDouble(txtExpAmt.Text).ToString("####0.00")
        txtExpAmtAed.Text = Convert.ToDouble(txtExpAmtAed.Text).ToString("####0.00")
        'txtEconomyClass.Text = Convert.ToDouble(txtEconomyClass.Text).ToString("####0.00")
        txtEconomyClassAed.Text = Convert.ToDouble(txtEconomyClassAed.Text).ToString("####0.00")
        'txtBusinessClass.Text = Convert.ToDouble(txtBusinessClass.Text).ToString("####0.00")
        txtBusinessClassAed.Text = Convert.ToDouble(txtBusinessClassAed.Text).ToString("####0.00")
        'txtTravelInsurance.Text = Convert.ToDouble(txtTravelInsurance.Text).ToString("####0.00")
        txtTravelInsuranceAed.Text = Convert.ToDouble(txtTravelInsuranceAed.Text).ToString("####0.00")
        'txtAdvanceRequest.Text = Convert.ToDouble(txtAdvanceRequest.Text).ToString("####0.00")
        txtAdvanceRequestAed.Text = Convert.ToDouble(txtAdvanceRequestAed.Text).ToString("####0.00")
        txtAmountBudgeted.Text = Convert.ToDouble(txtAmountBudgeted.Text).ToString("####0.00")
        txtAmountChgClient.Text = Convert.ToDouble(txtAmountChgClient.Text).ToString("####0.00")

        txtTotalA.Text = Convert.ToDouble(txtTotalA.Text).ToString("####0.00")
        txtTotalC.Text = Convert.ToDouble(txtTotalC.Text).ToString("####0.00")
        lblTotalABC.Text = Convert.ToDouble(lblTotalABC.Text).ToString("####0.00")
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        h_EntryId.Value = 0
        h_bta_Status.Value = "O"
        ViewState("EntryId") = 0
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update BTA_Details set BTA_Status='A' where BTA_No=" & h_EntryId.Value)
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update BTA_Details set BTA_Status='R' where BTA_No=" & h_EntryId.Value)
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetSector(ByVal prefixText As String) As String()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim str_Sql As String

        str_Sql = " select DISTINCT Sector from ( "
        str_Sql &= "select Itn_Sector sector from BTA_itinerary where Itn_BTA_No IN (select BTA_no from BTA_Details where BTA_Status='A') union ALL "
        str_Sql &= "select Sec_Travel from BTA_Sector where Sec_BTA_No IN (select BTA_no from BTA_Details where BTA_Status='A')) A where sector like '%" & prefixText & "%' ORDER by sector "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count

        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetFlight(ByVal prefixText As String) As String()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim str_Sql As String

        str_Sql = " select Itn_FlightNo from BTA_itinerary where Itn_BTA_No IN (select BTA_no from BTA_Details where BTA_Status='A') and Itn_FlightNo like '%" & prefixText & "%' ORDER by Itn_FlightNo "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count

        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function

End Class
