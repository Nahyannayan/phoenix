﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="HousingWaiver.aspx.vb" Inherits="Inventory_HousingWaiver" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/PHOENIXV2/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <style>
        .my-span {
            width: 125px;
            height: 125px;
            display: inline-block;
            vertical-align: top;
            padding: 4px;
            border: 2px solid rgba(0,0,0,0.3);
            margin-right: 2px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        
        function getEmployee() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "EMPHOUSEWAIVER"
            url = "../common/PopupSelect.aspx?&id=" + pMode;        
            var oWnd = radopen(url, "pop_emp");
        }  

        function TabChanged(sender, args) {
            sender.get_clientStateField().value =
               sender.saveClientState();
        }

        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
       1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        

       
        

        function OnEmpClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                
                document.getElementById("<%=h_empID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtEmpno.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=txtDes.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=txtDep.ClientID %>").value = NameandCode[4];
                var allow=NameandCode[5];
                document.getElementById("<%=hEntitlement.ClientID %>").value = allow;
                document.getElementById("<%=txtAllow.ClientID %>").value = allow;

                
            }
        }


        function calc() {

            
                
            var total = eval(document.getElementById("<%=txtAllow.ClientID %>").value) - eval(document.getElementById("<%=txtRent.ClientID %>").value);
            

            if (total < 0) {
                document.getElementById("<%=txtSchoolCost.ClientID %>").value = total;
            } else {
                document.getElementById("<%=txtSchoolCost.ClientID %>").value = 0;
            }
            

                
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnEmpClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="pgTitle" Text="" runat="server"></asp:Label>
        </div>
        
                <div>
                    <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr valign="bottom">
                            <td align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                <asp:Label ID="Label5" runat="server" Style="text-align: right" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">

                                    <tr>
                                        <td align="left" width="20%">
                                            <asp:Label ID="lblPRFNo" runat="server" Text="Requisition No" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtTCH_No" runat="server" Enabled="False"></asp:TextBox>
                                        </td>

                                        <td align="left" width="20%">
                                            <asp:Label ID="Label3" runat="server" Text="Requisition Date" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtContractDate" runat="server" Enabled="false"></asp:TextBox>
                                            <%--<asp:ImageButton ID="imgCondate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                Style="cursor: hand"></asp:ImageButton>--%>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server"
                                                TargetControlID="txtContractDate" PopupButtonID="imgCondate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="field-label"> Employee Details</span></td>
                                        <td align="left" colspan="3">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtEmpno" runat="server" Width="120px" Enabled ="false" ></asp:TextBox>
                                                        </td>
                                                    <td>
                                            <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getEmployee();return false;" ImageUrl="~/Images/forum_search.gif" ></asp:ImageButton>

                                            
                                                            </td>
                                            
                                                    <td><asp:TextBox ID="txtEmpName" Width="500px" runat="server" Enabled ="false"></asp:TextBox></td>
                                                    <td><asp:TextBox ID="txtDes" Width="400px" runat="server" Enabled ="false"></asp:TextBox></td>
                                                    <td><asp:TextBox ID="txtDep" Width="40px" runat="server" Enabled ="false"></asp:TextBox></td>
                                                    <asp:HiddenField ID="hEMP_ID" runat="server" />
                                                </tr>
                                            </table>

                                            
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Location</span></td>
                                        <td align="left"> <asp:DropDownList ID="ddlLocation" AutoPostBack="True" runat="server"></asp:DropDownList></td>
                                        <td align="left"> </td>
                                        <td align="left"> </td>

                                    </tr>
                                    
                                    
                                    
                                    
                                    
                                    
                                    

                                    
                                    <tr id="Tr4" runat="server"  >

                                        <td align="left" colspan="4">
                                            <table width="100%" border="0">
                                                <tr>

                                                    <td align="left" colspan="6" class="title-bg-lite"><span class="field-label">Lease and Entitlement Details</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="10%">
                                                        <asp:Label ID="lblrent" runat="server" Text="Allowance value" CssClass="field-label"></asp:Label>
                                                    </td>

                                                    <%--OnTextChanged="txttrent_TextChanged"--%>
                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtAllow"    runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>

                                                    <td align="left" width="10%">
                                                        <asp:Label ID="lblCharges" runat="server" Text="Rent" CssClass="field-label"></asp:Label>
                                                    </td>

                                                    <td align="left" width="25%">
                                                        <asp:TextBox ID="txtRent" runat="server" Style="text-align: right" ></asp:TextBox>
                                                    </td>
                                                     <td align="left" width="10%">
                                                        <asp:Label ID="Label4" runat="server" Text="Cost by school" CssClass="field-label"></asp:Label>
                                                    </td>

                                                    <td align="left" width="20%">
                                                        <asp:TextBox ID="txtSchoolCost" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                              

                                                
                                            </table>
                                        </td>
                                    </tr>
                                    
                                    
                                    <tr id="trChangeRequest2" runat="server" visible ="false">
                                        <td colspan="4">
                                            <asp:Panel runat="server" ID="pnlInvoice">
                                                <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                    <td align="left" colspan="6" class="title-bg-lite"><span class="field-label">Reason</span>
                                                    </td>
                                                </tr>

                                                    <tr>
                                                        <td width="20%">
                                                            <asp:Label ID="lblRSup_id" runat="server" Text="Retaining this unit over allowance" CssClass="field-label"></asp:Label>
                                                        </td>
                                                        <td align="left" width="80%">
                                                            <asp:TextBox ID="txtremarks" runat="server" Width="100%"
                                                                TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>

                                                    
                                                   
                                                    

                                                    
                                                </table>
                                            </asp:Panel>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="TabChanged" AutoPostBack="true">
                                                <ajaxToolkit:TabPanel ID="trFlow" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel2" runat="server" ScrollBars="Vertical">
                                                            <asp:Repeater runat="server" ID="rptFlow"><ItemTemplate>
<span class="my-span" style="background-color: <%#DataBinder.Eval(Container.DataItem, "wfcolor")%>;"><%#DataBinder.Eval(Container.DataItem, "wfdescr")%></span>
</ItemTemplate>
</asp:Repeater>

                                                        </asp:Panel>

                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trComments" runat="server" HeaderText="Comments">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtComments" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>
                                                <%--<ajaxToolkit:TabPanel ID="trDocument" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <table width="100%">

                                                            <tr>

                                                                <td align="left" width="20%">
                                                                    <asp:Label ID="lblDocType" runat="server" Text="Document Type" CssClass="field-label"></asp:Label>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlDocType" runat="server" Style="width: 25% !important"></asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%">
                                                                    <asp:FileUpload ID="UploadDocPhoto" runat="server"
                                                                        ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />
                                                                </td>
                                                            </tr>


                                                        </table>




                                                        <asp:Repeater ID="rptImages" runat="server">
                                                            <ItemTemplate>
                                                                [
                                       <asp:TextBox ID="lblPriContentType" runat="server" Visible="false" Text='<%# Bind("tci_contenttype") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriFilename" runat="server" Visible="false" Text='<%# Bind("tci_filename") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriId" runat="server" Visible="false" Text='<%# Bind("tci_id") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="btnDocumentLink" Text='<%# DataBinder.Eval(Container.DataItem, "tci_name") %>' runat="server"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnDelete" Text='[x]' Visible='<%# DataBinder.Eval(Container.DataItem, "tci_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                                                ]
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>--%>
                                                <%--<ajaxToolkit:TabPanel ID="trUpload" runat="server" Visible="false">
                                                    <ContentTemplate>

                                                        <table width="100%">

                                                            <tr>

                                                                <td align="left" width="20%">
                                                                    <asp:Label ID="Label21" runat="server" CssClass="field-label" Text="Document Type "></asp:Label>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlUploadType" runat="server" Style="width: 25% !important;"></asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%">
                                                                    <asp:FileUpload ID="UploadDocument" runat="server"
                                                                        ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:Button ID="btnUpload1" runat="server" CssClass="button" Text="Upload" />
                                                                </td>
                                                            </tr>


                                                        </table>






                                                        <asp:Repeater ID="rptDocument" runat="server">
                                                            <ItemTemplate>
                                                                [
                                       <asp:TextBox ID="lblPriDocumentContentType" runat="server" Visible="false" Text='<%# Bind("tci_contenttype") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriDocumentFilename" runat="server" Visible="false" Text='<%# Bind("tci_filename") %>'></asp:TextBox>
                                                                <asp:TextBox ID="lblPriDocumentId" runat="server" Visible="false" Text='<%# Bind("tci_id") %>'></asp:TextBox>
                                                                <asp:LinkButton ID="btnDocumentLink1" Text='<%# DataBinder.Eval(Container.DataItem, "tci_name") %>' runat="server"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnDelete1" Text='[x]' Visible='<%# DataBinder.Eval(Container.DataItem, "tci_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                                                ]
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>--%>
                                                <ajaxToolkit:TabPanel ID="trApproval" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtApprovals" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="trWorkflow" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel1" runat="server"  ScrollBars="Vertical">
                                                            <asp:GridView ID="gvWorkflow" runat="server" CssClass="table table-bordered table-row"
                                                                EmptyDataText="No Data" Width="98%" AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <asp:BoundField DataField="wrk_Date" HeaderText="Date" />
                                                                    <asp:BoundField DataField="wrk_User" HeaderText="User" />
                                                                    <asp:BoundField DataField="wrk_Action" HeaderText="Action" />
                                                                    <asp:BoundField DataField="wrk_Details" HeaderText="Details" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    
</ContentTemplate>
                                                
</ajaxToolkit:TabPanel>


                                            </ajaxToolkit:TabContainer>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnConfirm" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Confirm" Visible="false" OnClientClick="return confirm('Are you sure you want to Confirm This Supplier ?');" />
                                            <input type="button" id="btnAccept" class="button" value=" Approve " runat="server" onclick="this.disabled = true;" onserverclick="btnAccept_Click" />
                                            <asp:Button ID="btnReject" runat="server" CausesValidation="False" CssClass="button" Text="Reject" />
                                            <input type="button" id="btnSend" class="button" value=" Send " runat="server" onclick="this.disabled = true;" onserverclick="btnSend_Click" />
                                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                            <input type="button" id="btnSave" class="button" value="  Save  " runat="server" onclick="this.disabled = true;" onserverclick="btnSave_Click" />
                                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                            <%--<asp:Button ID="Button1" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />--%>
                                            <%--<asp:Button ID="btnMessage" runat="server" CausesValidation="False" Visible ="false"  CssClass="button" Text="Message" />--%>
                                            <%--<asp:Button ID="btnRenew" runat="server" CausesValidation="False" Visible="false" CssClass="button" Text="Renew" />--%>
                                            <asp:Button ID="btnRecall" runat="server" CausesValidation="False" Visible ="false"  CssClass="button" Text="Recall" />
                                            <%--<input type="button" id="btnReturn" class="button" value=" Return " Visible ="false"  runat="server" onclick="this.disabled = true;" onserverclick="btnReturn_Click" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_empID" runat="server" />
                                            
                                            <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_printed" runat="server" Value="0" />
                                            <asp:HiddenField ID="h_ThW_apr_id" Value="0" runat="server" />                                            
                                            <asp:HiddenField ID="h_THW_Status" runat="server" />
                                            <asp:HiddenField ID="h_bsu_id" runat="server" />
                                            <asp:HiddenField ID="h_Loc_id" runat="server" />
                                            <asp:HiddenField ID="hEntitlement" Value="0" runat="server" />
                                            <asp:HiddenField ID="h_fyear" runat="server" />
                                            <asp:FileUpload ID="UploadFile" runat="server" Width="250px" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB'
                                                Visible="false" />
                                            <asp:Button ID="btnUploadPrf" runat="server" CssClass="button" Visible="false" Text="Upload" />
                                            <asp:HyperLink ID="btnExport" runat="server" Visible="false">Export to Excel</asp:HyperLink>
                                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                            
                                            <asp:HiddenField ID="hViewState" Value="" runat="server" />
                                            <asp:HiddenField ID="h_EmpCount" Value="0" runat="server" />

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

            <%--</div>
        </div>
    </div>--%>


    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

    <asp:HiddenField ID="h_MSG2" runat="server" EnableViewState="true" />

</asp:Content>
