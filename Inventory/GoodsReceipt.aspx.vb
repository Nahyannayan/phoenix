﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.OleDb

Partial Class Inventory_GoodsReceipt
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnUpload)
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            initialMode = ViewState("datamode")
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "PI02005" And ViewState("MainMnu_code") <> "PI02008" And ViewState("MainMnu_code") <> "PI02014") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            pgTitle.Text = "Goods Receipt Note"
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            If Request.QueryString("viewid") <> "" Then
                If initialMode = "add" Then
                    SetDataMode("edit")
                Else
                    SetDataMode("view")
                End If
                setModifyvalues(ViewState("EntryId"))
            Else
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            End If
            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdGRN.DataSource = GRNFooter
            grdGRN.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            'grdGRN.DataSource = GRNFooter
            'grdGRN.DataBind()
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        grdGRN.Columns(8).Visible = Not mDisable
        grdGRN.Columns(9).Visible = Not mDisable
        grdGRN.ShowFooter = Not mDisable

        txtOthers.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtOthers.Attributes.Add("onblur", " return formatme('" & txtOthers.ClientID & "');")
        txtOthers.Attributes.Add("onkeyup", " return calculate();")
        txtVAT.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtVAT.Attributes.Add("onblur", " return formatme('" & txtVAT.ClientID & "');")
        txtVAT.Attributes.Add("onkeyup", " return calculate();")

        txtContact.Enabled = False
        txtSupplierAddress.Enabled = False
        txtGRNDate.Enabled = EditAllowed Or ViewState("MainMnu_code") = "PI02014"
        lnkGRNDate.Visible = EditAllowed Or ViewState("MainMnu_code") = "PI02014"
        txtPRFDate.Enabled = False
        txtSupplier.Enabled = False
        txtOthers.Enabled = EditAllowed
        txtPRFComments.Enabled = False
        txtGRNComments.Enabled = EditAllowed
        txtVAT.Enabled = EditAllowed
        txtINVDate.Enabled = EditAllowed
        txtInvNo.Enabled = EditAllowed Or ViewState("MainMnu_code") = "PI02014"
        txtNarration.Enabled = EditAllowed Or ViewState("MainMnu_code") = "PI02014"
        lnkINVDate.Enabled = EditAllowed Or ViewState("MainMnu_code") = "PI02014"
        txtTerms.Enabled = False

        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnDelete.Visible = mDisable And ViewState("MainMnu_code") <> "PI02014"
        btnEdit.Visible = mDisable And ViewState("MainMnu_code") <> "PI02014"
        btnPrint.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            h_EntryId.Value = p_Modifyid
            Dim str_conn As String = connectionString
            Dim dt As New DataTable
            Dim sqlStr As String = ""
            If initialMode = "add" Then
                sqlStr &= "select isnull(PRF_NEWYEAR,case when PRF_FYEAR<'2016' then 'YES' else 'NO' end) PRF_MOVE, BSU_NAME PRF_BSU_STR, '' GRN_PJV_NO, '' GRN_REMARKS, 'New' GRN_NO, replace(convert(varchar(30), getdate(), 106),' ','/') GRN_DATE_STR, "
                sqlStr &= "A1.ACT_NAME, A2.ACT_NAME BUD_ACT_NAME, replace(convert(varchar(30), Prf_Date, 106),' ','/') PRF_DATE_STR, (select sum(PRD_TOTAL) "
                sqlStr &= "from pendingPRF where PRD_PRF_ID=PRF_ID) GRN_TOTAL, 0 GRN_NOPJ, PRF_H.*, '' GRN_INVNO, '' GRN_INVDATE, '' GRN_INVREMARKS, PRF_ID GRN_PRF_ID, "
                sqlStr &= "case when BSU_FREEZEDT>=FYR_TODT then 1 else 0 end Freeze from PRF_H "
                sqlStr &= "inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M A1 on A1.ACT_ID=PRF_SUP_ID "
                sqlStr &= "inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M A2 on A2.ACT_ID=PRF_ACT_ID "
                sqlStr &= "inner join oasis..FINANCIALYEAR_S on FYR_ID=isnull(PRF_NEWYEAR,PRF_FYEAR) "
                sqlStr &= "inner JOIN vw_OSO_BUSINESSUNIT_M on BSU_ID=PRF_BSU_ID where PRF_ID=" & p_Modifyid
                dt = MainObj.getRecords(sqlStr, "OASIS_PUR_INVConnectionString")

                sqlStr = "select PRD_ID, PRD_ITM_ID, case when PRD_QTY=1 then 1 else PRD_QTY-isnull(sum(GRD_QTY),0) end PRD_QTY, cast(PRD_QTY as int) PRD_SPECIAL, "
                sqlStr &= "case when PRD_QTY=1 then 1 else PRD_QTY-isnull(sum(GRD_QTY),0) end GRD_QTY, case when PRD_QTY=1 then cast(((PRD_QTY*PRD_RATE)-isnull(sum(GRD_QTY*GRD_RATE),0)) as decimal(12,3)) else PRD_RATE end PRD_RATE,"
                sqlStr &= "case when prf_type='F' then isnull(ITM_DESCR+' '+ITM_UNIT+' '+ITM_PACKING, PRD_DESCR) else isnull(ASC_DESCR+' '+ACM_DESCR,PRD_DESCR) end+' '+PRD_DETAILS PRD_DESCR, "
                sqlStr &= "cast(((PRD_QTY*PRD_RATE)-isnull(sum(GRD_QTY*GRD_RATE),0)) as decimal(12,3)) PRD_TOTAL "
                sqlStr &= "from PRF_D inner join PRF_H on prf_id=prd_prf_id "
                sqlStr &= "left outer join oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] on asc_id=PRD_ITM_ID left outer join oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID "
                sqlStr &= "left outer JOIN ITEM on ITM_ID=PRD_ITM_ID LEFT OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
                sqlStr &= "left OUTER JOIN GRN_D on GRD_PRD_ID=PRD_ID AND GRD_GRN_ID NOT IN (select grn_id from GRN_H where GRN_DELETED=1) where PRD_PRF_ID = " & h_EntryId.Value & " "
                sqlStr &= "GROUP by PRD_ID, PRD_ITM_ID, ITM_DESCR, PRD_QTY, PRD_RATE, UCO_DESCR, ITM_UNIT, ITM_PACKING, PRD_DESCR, PRD_DETAILS, prf_type, ASC_DESCR, ACM_DESCR "
                sqlStr &= "having ((PRD_QTY*PRD_RATE)-isnull(sum(GRD_QTY*GRD_RATE),0))<>0"
                fillGridView(GRNFooter, grdGRN, sqlStr)
                h_Prf_Id.Value = h_EntryId.Value
                h_EntryId.Value = "0"
                txtINVDate.Text = Now.ToString("dd/MMM/yyyy")

            Else
                sqlStr &= "select isnull(PRF_NEWYEAR,case when PRF_FYEAR='2016' then 'YES' else 'NO' end) PRF_MOVE, GRN_PRF_ID, isnull(GRN_PJV_NO,'') GRN_PJV_NO, BSU_NAME PRF_BSU_STR, GRN_REMARKS, replace(convert(varchar(30), GRN_DATE, 106),' ','/') GRN_DATE_STR, "
                sqlStr &= "GRN_INVNO, replace(convert(varchar(30), isnull(GRN_INVDATE,getdate()), 106),' ','/') GRN_INVDATE, GRN_INVREMARKS, GRN_NOPJ, "
                sqlStr &= "GRN_NO, A1.ACT_NAME, A2.ACT_NAME BUD_ACT_NAME, replace(convert(varchar(30), Prf_Date, 106),' ','/') PRF_DATE_STR, (select sum(GRD_QTY*GRD_RATE) "
                sqlStr &= "from GRN_D where GRD_GRN_ID = " & h_EntryId.Value & ") GRN_TOTAL, PRF_H.*,  "
                sqlStr &= "(select replace(convert(varchar(30), WRK_DATE, 106),' ','/') from  WORKFLOW inner join grn_h on GRN_PRF_ID=WRK_PRF_ID where GRN_ID=" & p_Modifyid & " and WRK_ACTION='GRN' and WRK_DETAILS='POSTED')PJDATE,"
                sqlStr &= "(select case when sum(prd_qty)=1 then 1 else 0 end from prf_d where prd_prf_id=prf_id) Special, case when BSU_FREEZEDT>=FYR_TODT then 1 else 0 end Freeze from PRF_H "
                sqlStr &= "inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M A2 on A2.ACT_ID=PRF_ACT_ID "
                sqlStr &= "inner join GRN_H on PRF_ID=GRN_PRF_ID inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M A1 on A1.ACT_ID=PRF_SUP_ID "
                sqlStr &= "inner join oasis..FINANCIALYEAR_S on FYR_ID=isnull(PRF_NEWYEAR,PRF_FYEAR) "
                sqlStr &= "inner JOIN vw_OSO_BUSINESSUNIT_M on BSU_ID=PRF_BSU_ID where GRN_ID=" & p_Modifyid
                dt = MainObj.getRecords(sqlStr, "OASIS_PUR_INVConnectionString")

                sqlStr = "select GRD_ID, PRD_ID PRD_ID, GRD_ITM_ID PRD_ITM_ID, PRD_QTY, cast(PRD_QTY as int) PRD_SPECIAL, "
                sqlStr &= "case when prf_type='F' then isnull(ITM_DESCR+' '+ITM_UNIT+' '+ITM_PACKING, PRD_DESCR) else isnull(ASC_DESCR+' '+ACM_DESCR,PRD_DESCR) end+' '+PRD_DETAILS PRD_DESCR, "
                sqlStr &= "GRD_QTY, GRD_RATE PRD_RATE, cast(GRD_QTY*GRD_RATE as decimal(12,3)) PRD_TOTAL "
                sqlStr &= "from GRN_D inner JOIN PRF_D on GRD_PRD_ID=PRD_ID INNER JOIN PRF_H on PRF_ID=PRD_PRF_ID "
                sqlStr &= "left outer join oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] on asc_id=PRD_ITM_ID left outer join oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID "
                sqlStr &= "left outer join ITEM on ITM_ID=GRD_ITM_ID LEFT OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE where GRD_QTY>0 and GRD_GRN_ID = " & h_EntryId.Value & " "
                sqlStr &= "order by GRD_ID"
                fillGridView(GRNFooter, grdGRN, sqlStr)
            End If
            If dt.Rows.Count > 0 Then
                'PRF_NO, PRF_PRI_ID, PRF_APPROVAL, PRF_LVL_ID, PRF_BSU_ID, PRF_DATE, PRF_REQUESTBY, PRF_CONTACT, PRF_EXP_ID, PRF_TYP_ID, PRF_SUP_ID, 
                'PRF_TERMS, PRF_DELDATE, PRF_CHARGES, PRF_VAT, PRF_TOTAL, PRF_REMARKS, PRF_TYPE
                txtPRFComments.Text = dt.Rows(0)("PRF_REMARKS")
                txtGRNComments.Text = dt.Rows(0)("GRN_REMARKS")
                txtContact.Text = dt.Rows(0)("PRF_CONTACT")
                txtSupplierAddress.Text = dt.Rows(0)("PRF_SUPPLIER")
                txtOthers.Text = dt.Rows(0)("PRF_CHARGES")
                txtGRNDate.Text = dt.Rows(0)("GRN_DATE_STR")
                txtGRNNo.Text = dt.Rows(0)("GRN_NO")
                txtPRFDate.Text = dt.Rows(0)("PRF_DATE_STR")
                txtPRFNo.Text = dt.Rows(0)("PRF_NO")
                txtACTID.Text = dt.Rows(0)("BUD_ACT_NAME")
                txtSupplier.Text = dt.Rows(0)("ACT_NAME")
                hTPT_Client_ID.Value = dt.Rows(0)("PRF_SUP_ID")
                txtTerms.Text = dt.Rows(0)("PRF_TERMS")
                txtTotal.Text = dt.Rows(0)("GRN_TOTAL")
                txtInvNo.Text = dt.Rows(0)("GRN_INVNO")
                txtINVDate.Text = dt.Rows(0)("GRN_INVDATE")
                txtNarration.Text = dt.Rows(0)("GRN_INVREMARKS")
                txtVAT.Text = dt.Rows(0)("PRF_VAT")
                h_Prf_Id.Value = dt.Rows(0)("GRN_PRF_ID")
                h_Grn_PJV_No.Value = dt.Rows(0)("GRN_PJV_NO")
                btnDelete.Visible = (initialMode = "view" And h_Grn_PJV_No.Value.Length = 0 And dt.Rows(0)("GRN_NOPJ") = 0 And dt.Rows(0)("FREEZE") = 0) 'And (Session("F_YEAR") >= "2014")
                btnSave.Visible = btnSave.Visible And dt.Rows(0)("FREEZE") = 0
                btnClosePRF.Visible = (initialMode = "add")
                PRF_FYEAR = dt.Rows(0)("PRF_FYEAR")


                If h_Grn_PJV_No.Value.Length > 0 Then
                    txtPJ.Text = dt.Rows(0)("GRN_PJV_NO")
                    txtPJDate.Text = dt.Rows(0)("PJDATE")
                End If



                If ViewState("MainMnu_code") = "PI02014" Then 'purchase journal voucher
                    Session("PJVPost") = "1"
                    If txtPRFNo.Text.Contains(PRF_FYEAR.Substring(2) & "F") Then
                        lnkPRF.NavigateUrl = "PurchaseRequest.aspx?viewid=" & Encr_decrData.Encrypt(h_Prf_Id.Value) & "&MainMnu_code=DWleYPuD3MY=&datamode=4xCdg/cr4Xw="
                    Else
                        lnkPRF.NavigateUrl = "PurchaseRequest.aspx?viewid=" & Encr_decrData.Encrypt(h_Prf_Id.Value) & "&MainMnu_code=7aTrxQ3Cjog=&datamode=4xCdg/cr4Xw="
                    End If
                    lnkPRF.Text = txtPRFNo.Text
                    lnkPRF.Visible = h_Grn_PJV_No.Value.Length = 0 And (dt.Rows(0)("PRF_REQUEST") = "") And dt.Rows(0)("FREEZE") = 0
                    btnPost.Visible = h_Grn_PJV_No.Value.Length = 0 And (dt.Rows(0)("PRF_REQUEST") = "") And dt.Rows(0)("FREEZE") = 0
                End If
                If h_Grn_PJV_No.Value = "" And dt.Rows(0)("PRF_MOVE") = "YES" Then
                    btnTransfer.Visible = True
                    btnTransfer.Text = "Transfer to 2016"
                Else
                    btnTransfer.Visible = False
                End If
                lblTotalAmt.Text = "Total Amount ( " + dt.Rows(0)("PRF_CUR_ID") + " )"
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
            btnEdit.Visible = btnEdit.Visible And btnDelete.Visible

            btnUpload.Visible = True
            UploadDocPhoto.Visible = True


            FormatFigures()
            If ViewState("MainMnu_code") = "PI02014" Then
                btnDelete.Visible = False
                btnPost.Visible = h_Grn_PJV_No.Value.Length = 0 And (dt.Rows(0)("PRF_REQUEST") = "") And dt.Rows(0)("FREEZE") = 0
                PJRow.Visible = Not h_Grn_PJV_No.Value.Length = 0

                If Not btnPost.Visible = True Then
                    pgTitle.Text = "Purchase Journal"
                End If

                trCCS.Visible = True
                gvCCS.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select prm_descr Type, prc_ccs_descr Description, prc_amt Amount from prf_c inner join prf_cm on prc_typ_id=prm_id and prc_prf_id=" & h_Prf_Id.Value & " order by prc_id").Tables(0)
                gvCCS.DataBind()
            Else

            End If

            'If p_Modifyid > 0 Then txtTotalA.Text = TravelPlanFooter.Compute("sum(Trv_TotalExp)", "")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next
        txtOthers.Text = Convert.ToDouble(txtOthers.Text).ToString("####0.00")
        txtVAT.Text = Convert.ToDouble(txtVAT.Text).ToString("####0.00")
        txtTotal.Text = Convert.ToDouble(txtTotal.Text).ToString("####0.00")
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        h_total.Value = "0"

        txtGRNNo.Text = "New"
        txtGRNDate.Text = Now.ToString("dd/MMM/yyyy")
        txtContact.Text = ""
        'ddlEntitlement

        txtSupplierAddress.Text = ""
        txtSupplier.Text = ""
        txtTotal.Text = "0.0"

        'txtEntitlement.Style.Add("display", "none")
    End Sub

    Private Property GRNFooter() As DataTable
        Get
            Return ViewState("GRNFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("GRNFooter") = value
        End Set
    End Property

    Private Property PRF_FYEAR() As String
        Get
            Return ViewState("PRF_FYEAR")
        End Get
        Set(ByVal value As String)
            ViewState("PRF_FYEAR") = value
        End Set
    End Property

    Private Property initialMode() As String
        Get
            Return ViewState("initialMode")
        End Get
        Set(ByVal value As String)
            ViewState("initialMode") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
    End Sub

    Private Sub showNoRecordsFound()
        If Not GRNFooter Is Nothing AndAlso GRNFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdGRN.Columns.Count - 2
            grdGRN.Rows(0).Cells.Clear()
            grdGRN.Rows(0).Cells.Add(New TableCell())
            grdGRN.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdGRN.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdGRN_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdGRN.RowCancelingEdit
        grdGRN.ShowFooter = True
        grdGRN.EditIndex = -1
        grdGRN.DataSource = GRNFooter
        grdGRN.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdGRN_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdGRN.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtPRD_DESCR As TextBox = grdGRN.FooterRow.FindControl("txtPRD_DESCR")
            Dim txtPRD_QTY As TextBox = grdGRN.FooterRow.FindControl("txtPRD_QTY")
            Dim txtPRD_RATE As TextBox = grdGRN.FooterRow.FindControl("txtPRD_RATE")
            Dim txtPRD_TOTAL As TextBox = grdGRN.FooterRow.FindControl("txtPRD_TOTAL")
            Dim lblPRD_ITEM_ID As Label = grdGRN.FooterRow.FindControl("lblPRD_ITEM_ID")

            lblError.Text = ""
            If txtPRD_DESCR.Text.Trim.Length = 0 Then lblError.Text &= "Item"
            If Val(txtPRD_QTY.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Quantity"
            If Val(txtPRD_RATE.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Rate"

            lblError.Text = ""
            'lblError.Text = validBO(txtSec_Travel.Text.Trim, txtBus_objectives.Text.Trim, txtSec_company.Text.Trim)
            If lblError.Text.Length > 0 Then
                lblError.Text &= " Mandatory!!!!"
                showNoRecordsFound()
                Exit Sub
            End If

            If GRNFooter.Rows(0)(1) = -1 Then GRNFooter.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = GRNFooter.NewRow
            mrow("PRD_ID") = 0
            mrow("PRD_ITM_ID") = lblPRD_ITEM_ID.Text
            mrow("PRD_DESCR") = txtPRD_DESCR.Text
            mrow("PRD_QTY") = txtPRD_QTY.Text
            mrow("PRD_RATE") = txtPRD_RATE.Text
            mrow("PRD_TOTAL") = txtPRD_TOTAL.Text
            GRNFooter.Rows.Add(mrow)
            txtTotal.Text = Convert.ToDouble(GRNFooter.Compute("sum(PRD_TOTAL)", "")).ToString("####0.00")
            h_total.Value = txtTotal.Text

            grdGRN.EditIndex = -1
            grdGRN.DataSource = GRNFooter
            grdGRN.DataBind()
            showNoRecordsFound()
        End If
    End Sub

    Protected Sub grdGRN_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdGRN.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then
            Dim txtGRD_QTY As TextBox = e.Row.FindControl("txtGRD_QTY")
            Dim splGRD_QTY As Label = e.Row.FindControl("splGRD_QTY")
            Dim txtPRD_RATE As Label = e.Row.FindControl("txtPRD_RATE")
            Dim splPRD_RATE As TextBox = e.Row.FindControl("splPRD_RATE")
            Dim h_Special As HiddenField = e.Row.FindControl("h_Special")

            If Not splPRD_RATE Is Nothing Then
                splPRD_RATE.Visible = (h_Special.Value = "1")
                splPRD_RATE.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
            End If
            If Not txtPRD_RATE Is Nothing Then txtPRD_RATE.Visible = (h_Special.Value <> "1")

            If Not splGRD_QTY Is Nothing Then splGRD_QTY.Visible = (h_Special.Value = "1")
            If Not txtGRD_QTY Is Nothing Then
                txtGRD_QTY.Visible = (h_Special.Value <> "1")
                txtGRD_QTY.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
            End If

        End If
    End Sub

    Protected Sub grdGRN_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdGRN.RowDeleting
        Dim mRow() As DataRow = GRNFooter.Select("ID=" & grdGRN.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_GRNGridDelete.Value &= ";" & mRow(0)("PRD_ID")
            GRNFooter.Select("ID=" & grdGRN.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            GRNFooter.AcceptChanges()
        End If
        If GRNFooter.Rows.Count = 0 Then
            GRNFooter.Rows.Add(GRNFooter.NewRow())
            GRNFooter.Rows(0)(1) = -1
        End If
        Try
            txtTotal.Text = Convert.ToDouble(GRNFooter.Compute("sum(PRD_TOTAL)", "")).ToString("####0.00")
            h_total.Value = txtTotal.Text
        Catch ex As Exception
            txtTotal.Text = "0.0"
        End Try
        grdGRN.DataSource = GRNFooter
        grdGRN.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdGRN_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdGRN.RowEditing
        Try
            grdGRN.ShowFooter = True
            grdGRN.EditIndex = e.NewEditIndex
            grdGRN.DataSource = GRNFooter
            grdGRN.DataBind()
            showNoRecordsFound()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdGRN_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdGRN.RowUpdating
        Dim s As String = grdGRN.DataKeys(e.RowIndex).Value.ToString()
        Dim lblPRD_ID As Label = grdGRN.Rows(e.RowIndex).FindControl("lblPRD_ID")
        'Dim txtPRD_DESCR As TextBox = grdGRN.Rows(e.RowIndex).FindControl("txtPRD_DESCR")
        Dim txtPRD_QTY As Label = grdGRN.Rows(e.RowIndex).FindControl("txtPRD_QTY")
        'Dim lblPRD_ITEM_ID As Label = grdGRN.Rows(e.RowIndex).FindControl("lblPRD_ITEM_ID")
        Dim txtPRD_RATE As Label = grdGRN.Rows(e.RowIndex).FindControl("txtPRD_RATE")
        Dim txtPRD_TOTAL As TextBox = grdGRN.Rows(e.RowIndex).FindControl("txtPRD_TOTAL")
        Dim txtGRD_QTY As TextBox = grdGRN.Rows(e.RowIndex).FindControl("txtGRD_QTY")
        Dim splGRD_QTY As Label = grdGRN.Rows(e.RowIndex).FindControl("splGRD_QTY")
        Dim splPRD_RATE As TextBox = grdGRN.Rows(e.RowIndex).FindControl("splPRD_RATE")
        Dim h_Special As HiddenField = grdGRN.Rows(e.RowIndex).FindControl("h_Special")

        lblError.Text = ""
        If h_Special.Value = 1 Then
            If (Val(txtGRD_QTY.Text) * Val(splPRD_RATE.Text)) > Val(txtPRD_TOTAL.Text) Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Invalid Rate"
        Else
            If Val(txtGRD_QTY.Text) > Val(txtPRD_QTY.Text) Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Invalid Quantity"
        End If

        If lblError.Text.Length > 0 Then
            'lblError.Text &= " Mandatory!!!!"
            showNoRecordsFound()
            Exit Sub
        End If

        Dim mrow As DataRow
        mrow = GRNFooter.Select("ID=" & s)(0)
        'mrow("PRD_ITM_ID") = lblPRD_ITEM_ID.Text
        'mrow("PRD_DESCR") = txtPRD_DESCR.Text
        'mrow("PRD_QTY") = txtPRD_QTY.Text
        'mrow("PRD_RATE") = txtPRD_RATE.Text
        If h_Special.Value = 1 Then
            mrow("PRD_TOTAL") = Val(txtGRD_QTY.Text) * Val(splPRD_RATE.Text)
            mrow("PRD_RATE") = splPRD_RATE.Text
            mrow("GRD_QTY") = txtGRD_QTY.Text
        Else
            mrow("PRD_TOTAL") = Val(txtGRD_QTY.Text) * Val(txtPRD_RATE.Text)
            mrow("GRD_QTY") = txtGRD_QTY.Text
        End If
        txtTotal.Text = Convert.ToDouble(GRNFooter.Compute("sum(PRD_TOTAL)", "")).ToString("####0.00")

        grdGRN.EditIndex = -1
        grdGRN.DataSource = GRNFooter
        grdGRN.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        'If txtInvNo.Text.Trim.Length = 0 Then lblError.Text &= "Invoice Number,"
        'If txtINVDate.Text.Trim.Length = 0 Then lblError.Text &= "Invoice Date,"
        'If txtNarration.Text.Trim.Length = 0 Then lblError.Text &= "Invoice Narration,"
        If lblError.Text.Length > 0 Then lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Mandatory"
        If grdGRN.Rows.Count = 0 Or CType(txtTotal.Text, Decimal) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "GRN should contain atleast 1 item"
        'If IsDate(txtBTADate.Text) Then
        '    If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM oasisfin.dbo.vw_OSO_FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "' and '" & txtBTADate.Text & "' between FYR_FROMDT and isnull(FYR_TODT,getdate())") = 0 Then lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        'Else
        '    lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        'End If

        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(11) As SqlParameter
        'GRN_ID, GRN_PRF_ID, GRN_NO, GRN_DATE, GRN_CHARGES, GRN_VAT, GRN_TOTAL, GRN_REMARKS
        pParms(1) = Mainclass.CreateSqlParameter("@GRN_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@GRN_PRF_ID", h_Prf_Id.Value, SqlDbType.Int)
        pParms(3) = Mainclass.CreateSqlParameter("@GRN_NO", txtGRNNo.Text, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@GRN_DATE", txtGRNDate.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@GRN_CHARGES", txtOthers.Text, SqlDbType.Decimal)
        pParms(6) = Mainclass.CreateSqlParameter("@GRN_VAT", txtVAT.Text, SqlDbType.Decimal)
        pParms(7) = Mainclass.CreateSqlParameter("@GRN_TOTAL", txtTotal.Text, SqlDbType.Decimal)
        pParms(8) = Mainclass.CreateSqlParameter("@GRN_REMARKS", txtGRNComments.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@GRN_INVNO", txtInvNo.Text, SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@GRN_INVDATE", txtINVDate.Text, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@GRN_INVREMARKS", txtNarration.Text, SqlDbType.VarChar)

        'savePRF_H 0, '', 0, '', 0, '999998', '8/oct/2012', 'sandip.kapil', '', 0, 0, '063M0122', '60 days', '8/oct/2012', 0,0,0,''
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveGRN_H", pParms)
            If RetVal = "-1" Or pParms(1).Value = 0 Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim RowCount As Integer
            For RowCount = 0 To GRNFooter.Rows.Count - 1
                Dim iParms(7) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If GRNFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                'GRD_ID, GRD_GRN_ID, GRD_PRD_ID, GRD_ITM_ID, GRD_QTY, GRD_RATE, GRD_DETAILS
                If initialMode = "add" Then
                    iParms(1) = Mainclass.CreateSqlParameter("@GRD_ID", 0, SqlDbType.Int)
                Else
                    iParms(1) = Mainclass.CreateSqlParameter("@GRD_ID", GRNFooter.Rows(RowCount)("GRD_ID"), SqlDbType.Int)
                End If
                iParms(2) = Mainclass.CreateSqlParameter("@GRD_GRN_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@GRD_PRD_ID", GRNFooter.Rows(RowCount)("PRD_ID"), SqlDbType.Int)
                iParms(4) = Mainclass.CreateSqlParameter("@GRD_ITM_ID", GRNFooter.Rows(RowCount)("PRD_ITM_ID"), SqlDbType.Int)
                iParms(5) = Mainclass.CreateSqlParameter("@GRD_QTY", GRNFooter.Rows(RowCount)("GRD_QTY"), SqlDbType.Decimal)
                iParms(6) = Mainclass.CreateSqlParameter("@GRD_RATE", GRNFooter.Rows(RowCount)("PRD_RATE"), SqlDbType.Decimal)
                iParms(7) = Mainclass.CreateSqlParameter("@GRD_DETAILS", "", SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveGRN_D", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            If h_GRNGridDelete.Value <> "0" And Not initialMode = "add" Then
                Dim deleteId() As String = h_GRNGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@GRD_PRD_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@GRD_GRN_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from GRN_D where GRD_PRD_ID=@GRD_PRD_ID and GRD_GRN_ID=@GRD_GRN_ID", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            stTrans.Commit()
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from pendingprf where prd_prf_id=" & h_Prf_Id.Value) = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "GRN" & "','" & "Goods received" & "'," & h_Prf_Id.Value & ",0,'G')")
            End If
            If ViewState("datamode") <> "edit" Then
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtPRFNo.Text, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = getErrorMessage("0")
            Else
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtPRFNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                lblError.Text = getErrorMessage("0")
            End If
            initialMode = "view"
            'SetDataMode("view")
            'setModifyvalues(ViewState("EntryId"))
            Response.Redirect(ViewState("ReferrerUrl"), False)
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnPrint.Visible = Not btnSave.Visible
        grdGRN.DataSource = GRNFooter
        grdGRN.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" And h_EntryId.Value > 0 Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update GRN_H set GRN_DELETED=1 where GRN_ID=@GRN_ID"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@GRN_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        lblError.Text = ""
        If txtInvNo.Text.Trim.Length = 0 Then lblError.Text &= "Invoice Number,"
        If txtINVDate.Text.Trim.Length = 0 Then lblError.Text &= "Invoice Date,"
        If txtNarration.Text.Trim.Length = 0 Then lblError.Text &= "Invoice Narration,"
        If lblError.Text.Length > 0 Then lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Mandatory"
        If grdGRN.Rows.Count = 0 Or CType(txtTotal.Text, Decimal) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "GRN should contain atleast 1 item"
        'If IsDate(txtBTADate.Text) Then
        '    If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM oasisfin.dbo.vw_OSO_FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "' and '" & txtBTADate.Text & "' between FYR_FROMDT and isnull(FYR_TODT,getdate())") = 0 Then lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        'Else
        '    lblError.Text = IIf(lblError.Text.Length = 0, "", ",") & "Invalid Financial Year/Date"
        'End If

        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@GRN_ID", h_EntryId.Value, SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@GRN_INVNO", txtInvNo.Text, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@GRN_INVDATE", txtINVDate.Text, SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@GRN_INVREMARKS", txtNarration.Text, SqlDbType.VarChar)
        iParms(5) = Mainclass.CreateSqlParameter("@GRN_DATE", txtGRNDate.Text, SqlDbType.VarChar)
        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "postPurchase", iParms)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "GRN" & "','" & "Posted" & "'," & h_Prf_Id.Value & ",0,'G')")
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim GRN_ID As Integer = CType(h_EntryId.Value, Integer)
            Dim cmd As New SqlCommand
            'writeWorkFlow(ViewState("EntryId"), "Printed", "", 0, "P")

            cmd.CommandText = "rptGRNDetails"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@GRN_ID", GRN_ID, SqlDbType.Int)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@GRN_FLAG", 0, SqlDbType.Int)
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            params("showHide") = "prf"
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "Goods Receipt Note"
            params("@GRN_ID") = GRN_ID
            params("Menu") = ViewState("MainMnu_code")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptGRN.rpt"
            'End If
            Session("ReportSource") = repSource
            'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)

        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)

        End If
    End Sub
    Protected Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update prf_h set prf_newyear='2016' where prf_id=" & h_Prf_Id.Value & " insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "GRN" & "','" & "Transfer to new FY" & "'," & h_Prf_Id.Value & ",0,'G')")
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Inventory\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename
            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Try
                    IO.File.Move(strFilepath, str_img & txtPRFNo.Text & "_" & Session("sBsuid") & FileExt)
                    UploadDocPhoto.Visible = False
                    btnUpload.Visible = False
                    'fileToDelete.Delete()
                Catch ex As Exception
                Finally
                    Dim infoNew As New FileInfo(strFilepath)
                    If infoNew.Exists Then infoNew.Delete()
                End Try
                If UCase(FileExt) = ".XLS" Or UCase(FileExt) = ".XLSX" Then
                    Dim objConn As New SqlConnection(connectionString)
                    objConn.Open()
                    Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
                    If ImportfromExcel(stTrans, str_img & txtPRFNo.Text & "_" & Session("sBsuid") & FileExt, Path.GetExtension(UploadDocPhoto.FileName)) = 0 Then
                        Dim info As New FileInfo(str_img & txtPRFNo.Text & "_" & Session("sBsuid") & FileExt)
                        If info.Exists Then info.Delete()
                        Dim iParms(8) As SqlClient.SqlParameter
                        iParms(1) = Mainclass.CreateSqlParameter("@PRF_NO", txtPRFNo.Text, SqlDbType.VarChar)
                        iParms(2) = Mainclass.CreateSqlParameter("@GRN_TOTAL", txtUploadTotal.Text, SqlDbType.Float)
                        Try
                            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "uploadGRN", iParms)
                            stTrans.Commit()
                            Response.Redirect(ViewState("ReferrerUrl"), True)
                        Catch ex As Exception
                            stTrans.Rollback()
                            lblError.Text = ex.Message
                        End Try
                    End If
                End If
            End If

        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub
    Private Function ImportfromExcel(ByRef stTrans As SqlTransaction, ByVal FILEPATH As String, ByVal fileExt As String) As Int16
        Dim connString As String = ""
        Dim strFileType As String = fileExt.ToLower()
        Dim path__1 As String = FILEPATH
        Dim RowCount As Integer = 0
        ImportfromExcel = 1

        If strFileType.Trim() = ".xls" Then
            connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path__1 & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
        ElseIf strFileType.Trim() = ".xlsx" Then
            connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & path__1 & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
        End If
        Dim query As String = "SELECT * FROM [Sheet1$]"

        Dim conn As New OleDbConnection(connString)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim cmd As New OleDbCommand(query, conn)
        Dim da As New OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        Dim dt As New DataTable
        Try
            da.Fill(ds)
            dt = ds.Tables(0)

            For RowCount = 0 To dt.Rows.Count - 2
                If dt.Rows(RowCount)(3) = txtPRFNo.Text Then
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "insert into GRN_H_EXCEL(GRX_PRF_NO, GRX_INVDATE, GRX_INVNO, GRX_AMOUNT, GRX_INVNARRATION) values('" & txtPRFNo.Text & "','" & dt.Rows(RowCount)(0) & "','" & dt.Rows(RowCount)(1) & "'," & dt.Rows(RowCount)(2) & ",'" & dt.Rows(RowCount)(4) & "')")
                End If
            Next
            ImportfromExcel = 0

        Catch ex As Exception
            lblError.Text = "Invalid excel format!!!!"
        End Try
        dt.Dispose()
        da.Dispose()
        conn.Close()
        conn.Dispose()
    End Function
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select
    End Function

    Protected Sub btnClosePRF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClosePRF.Click
        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update PRF_D SET REQ_QTY=isnull(PRD_TOTAL,0) from pendingPRF inner join PRF_D on pendingPRF.PRD_ID=PRF_D.PRD_ID inner join PRF_H on PRF_ID=pendingPRF.PRD_PRF_ID and PRF_ID=" & h_Prf_Id.Value)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "GRN" & "','" & "PRF Closed" & "'," & h_Prf_Id.Value & ",0,'G')")
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class
