﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_reLandlord
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'ts
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "PI02031" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If

                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        txtPoBOX.Enabled = EditAllowed
        If mode = "edit" Then
            txtLandlord.Enabled = Not EditAllowed
        Else
            txtLandlord.Enabled = EditAllowed
        End If

        txtDetails.Enabled = EditAllowed
        txtFax.Enabled = EditAllowed
        txtMobile.Enabled = EditAllowed
        txtOffice.Enabled = EditAllowed
        txtPayname.Enabled = EditAllowed
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        imgLandlord.Enabled = EditAllowed
        txtMainLandlord.Enabled = EditAllowed
        txtAcno.Enabled = EditAllowed
        txtEmail.Enabled = EditAllowed

        btnDelete.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select  A.REL_ID,A.REL_NAME,A.REL_MOBILE,A.REL_PHONE,A.REL_FAX,A.REL_PAYNAME,A.REL_DETAILS,A.REL_POBOX,A.REL_REL_ID,B.REL_NAME MAINLANDLORD,A.REL_ACTNO,A.REL_EMAIL FROM RELANDLORD A LEFT OUTER JOIN RELANDLORD B ON B.REL_ID =A.REL_REL_ID  ")
                strSql.Append("where A.REL_ID=" & h_EntryId.Value)
                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtLandlord.Text = dt.Rows(0)("REL_NAME")
                    txtPoBOX.Text = dt.Rows(0)("REL_POBOX")
                    txtDetails.Text = dt.Rows(0)("REL_DETAILS")
                    txtFax.Text = dt.Rows(0)("REL_FAX")
                    txtMobile.Text = dt.Rows(0)("REL_MOBILE")
                    txtOffice.Text = dt.Rows(0)("REL_PHONE")
                    txtPayname.Text = dt.Rows(0)("REL_PAYNAME")
                    h_EntryId.Value = dt.Rows(0)("REL_ID")
                    h_REL_REL_ID.Value = dt.Rows(0)("REL_REL_ID")
                    txtMainLandlord.Text = dt.Rows(0)("MAINLANDLORD")
                    txtAcno.Text = dt.Rows(0)("REL_ACTNO")
                    txtEmail.Text = dt.Rows(0)("REL_EMAIL")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ClearDetails()
        Dim MainLandLordID As Integer = Mainclass.getDataValue("select REL_ID from RELANDLORD where REL_NAME ='OTHERS'", "OASIS_PUR_INVConnectionString")

        txtLandlord.Text = ""
        txtPoBOX.Text = ""
        txtDetails.Text = ""
        txtFax.Text = ""
        txtMobile.Text = ""
        txtOffice.Text = ""
        txtPayname.Text = ""
        h_REL_REL_ID.Value = MainLandLordID
        txtMainLandlord.Text = "OTHERS"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM TCT_H WHERE TCH_REL_ID =" & h_EntryId.Value) > 0 Then
                lblError.Text = "Landlord in use, unable to delete"
            Else
                Dim objConn As New SqlConnection(connectionString)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction

                Try
                    Dim pParms(2) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@ID", h_EntryId.Value, SqlDbType.Int, True)
                    pParms(2) = Mainclass.CreateSqlParameter("@TABLE", "RELANDLORD", SqlDbType.VarChar)

                    Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "DeleteMasterData", pParms)
                    If RetVal = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    Else
                        ViewState("EntryId") = pParms(1).Value
                    End If

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                    End If

                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                    Exit Sub
                Finally
                    objConn.Close()
                End Try

                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                lblError.Text = "Record Deleted Successfully !!!"
                'Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean

        If txtLandlord.Text.Trim.Length = 0 Then strMandatory.Append("Landlord Name,")
        If txtOffice.Text = "" Then strMandatory.Append("Office Number,")
        If txtMainLandlord.Text = "" Then strMandatory.Append("Main Landlord,")
        If txtEmail.Text.ToString.Contains("@") = False Then strMandatory.Append("not a valid email id, ")


        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(12) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@REL_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@REL_NAME", txtLandlord.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@REL_POBOX", txtPoBOX.Text, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@REL_MOBILE", txtMobile.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@REL_PHONE", txtOffice.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@REL_FAX", txtFax.Text, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@REL_PAYNAME", txtPayname.Text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@REL_DETAILS", txtDetails.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@REL_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@REL_REL_ID", h_REL_REL_ID.Value, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@REL_ACTNO", txtAcno.Text, SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@REL_EMAIL", txtEmail.Text, SqlDbType.VarChar)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SaveRELandlord", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class

