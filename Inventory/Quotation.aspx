﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Quotation.aspx.vb" Inherits="Inventory_Quotation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>

    <script type="text/javascript" language="javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
    1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        var myIds = new Array();
        var myDetails = new Array();
        var myRates = new Array();
        var myPackings = new Array();
        var myUnits = new Array();
        var myDescrs = new Array();
        var myCommodities = new Array();

        function calculate() {
        }

        function ClientItemSelected(sender, e) {
            var index = sender._selectIndex;
            var hdnITM_ID = sender.get_element().name.replace("txtQUD_DESCR", "hdnITM_ID");
            var txtQUD_RATE = sender.get_element().name.replace("txtQUD_DESCR", "txtQUD_RATE");
            var txtQUD_UNITS = sender.get_element().name.replace("txtQUD_DESCR", "txtQUD_UNITS");
            var txtQUD_BRAND = sender.get_element().name.replace("txtQUD_DESCR", "txtQUD_BRAND");
            var txtQUD_PACKING = sender.get_element().name.replace("txtQUD_DESCR", "txtQUD_PACKING");
            var txtQUD_DETAILS = sender.get_element().name.replace("txtQUD_DESCR", "txtQUD_DETAILS");

            $get(txtQUD_RATE).value = myRates[index];
            $get(hdnITM_ID).value = myIds[index];
            $get(sender.get_element().name).value = myDescrs[index];
            $get(txtQUD_UNITS).value = myUnits[index];
            $get(txtQUD_BRAND).value = myCommodities[index];
            $get(txtQUD_PACKING).value = myPackings[index];
            $get(txtQUD_DETAILS).value = myDetails[index];
        }

        //function getItemAssign(DESCR, BRAND, UNITS, PACKING, DETAILS, RATE, hdnITM_ID) {
        //    var sFeatures;
        //    var lstrVal;
        //    var lintScrVal;
        //    var pMode;
        //    var NameandCode;
        //    sFeatures = "dialogWidth: 760px; ";
        //    sFeatures += "dialogHeight: 420px; ";
        //    sFeatures += "help: no; ";
        //    sFeatures += "resizable: no; ";
        //    sFeatures += "scroll: yes; ";
        //    sFeatures += "status: no; ";
        //    sFeatures += "unadorned: no; ";
        //    pMode = "NON_PRF_ITEMLIST"
        //    url = "../common/PopupSelect.aspx?id=" + pMode;
        //    result = window.showModalDialog(url, "", sFeatures);
        //    if (result == '' || result == undefined) {
        //        return false;
        //    }
        //    NameandCode = result.split('___');

        //    document.getElementById(DESCR).value = NameandCode[1];
        //    document.getElementById(BRAND).value = NameandCode[2];
        //    document.getElementById(UNITS).value = NameandCode[3];
        //    document.getElementById(PACKING).value = NameandCode[4];
        //    document.getElementById(DETAILS).value = NameandCode[5];
        //    document.getElementById(RATE).value = NameandCode[6];
        //    document.getElementById(hdnITM_ID).value = NameandCode[0];

        //}


        function MultiSelect(sender, e) {
            var comletionList = sender.get_completionList();
            for (i = 0; i < comletionList.childNodes.length; i++) {
                var itemobj = new Object();
                var _data = comletionList.childNodes[i]._value;
                itemobj.descr = _data.substring(_data.lastIndexOf('|') + 1); // parse name as item value
                comletionList.childNodes[i]._value = itemobj.name;
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.commodity = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.unit = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.packing = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.details = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.rate = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.itmid = _data.substring(_data.lastIndexOf('|') + 1);
                if (itemobj.descr) {
                    myIds[i] = itemobj.itmid; // id used in updating hidden file
                    myDescrs[i] = itemobj.descr;
                    myRates[i] = itemobj.rate;
                    myPackings[i] = itemobj.packing;
                    myDetails[i] = itemobj.details;
                    myUnits[i] = itemobj.unit;
                    myCommodities[i] = itemobj.commodity;
                }
                //comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:60%;float:left'>" + itemobj.descr + "</div>"
                //                                      + "<div class='cloumnspan' style='width:30%;float:left'>" + itemobj.commodity + "</div>"
                //                                      + "<div class='cloumnspan' style='width:10%;'>" + itemobj.rate + "</div>";
            }
        }

       <%-- function getClientName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMCLIENT"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=N";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtSupplier.ClientID %>").value = NameandCode[2];
            document.getElementById("<%=h_supid.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        }--%>

        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
    </script>
    <script>
        function getItemAssignNew(DESCR, BRAND, UNITS, PACKING, DETAILS, RATE, hdnITM_ID) {
            var pMode;
            var NameandCode;
            pMode = "NON_PRF_ITEMLIST"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            document.getElementById("<%=hfDESCR.ClientID%>").value = DESCR;
            document.getElementById("<%=hfBRAND.ClientID%>").value = BRAND;
            document.getElementById("<%=hfUNITS.ClientID%>").value = UNITS;
            document.getElementById("<%=hfPACKING.ClientID%>").value = PACKING;
            document.getElementById("<%=hfDETAILS.ClientID%>").value = DETAILS;
            document.getElementById("<%=hfRATE.ClientID%>").value = RATE;
            document.getElementById("<%=hfhdnITM_ID.ClientID%>").value = hdnITM_ID;

            var oWnd = radopen(url, "pop_items");



        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hfDESCR.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hfBRAND.ClientID%>").value).value = NameandCode[2];
                document.getElementById(document.getElementById("<%=hfUNITS.ClientID%>").value).value = NameandCode[3];
                document.getElementById(document.getElementById("<%=hfPACKING.ClientID%>").value).value = NameandCode[4];
                document.getElementById(document.getElementById("<%=hfDETAILS.ClientID%>").value).value = NameandCode[5];
                document.getElementById(document.getElementById("<%=hfRATE.ClientID%>").value).value = NameandCode[6];
                document.getElementById(document.getElementById("<%=hfhdnITM_ID.ClientID%>").value).value = NameandCode[0];



            }
        }




        function getClientNameNew() {
            var pMode;
            var NameandCode;

            pMode = "ITMCLIENT"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=N";
            var oWnd = radopen(url, "pop_supplier");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtSupplier.ClientID %>").value = NameandCode[2];
            document.getElementById("<%=h_supid.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        }
    }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_items" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_supplier" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Framework Agreement
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table style="width: 100%">
                                <tr>
                                    <td class="matters" width="20%">
                                        <asp:Label ID="lblQUONo" runat="server" Text="Quotation Number" CssClass="field-label"></asp:Label></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:TextBox ID="txtQUONo" runat="server" Enabled="false"></asp:TextBox></td>
                                    <td class="matters" width="20%"><span class="field-label">Date<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:TextBox ID="txtQUODate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="lnkQUODate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtQUODate" PopupButtonID="lnkQUODate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">Supplier<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtSupplier" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getClientNameNew();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" /><asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                                    </td>
                                    <td class="matters"><span class="field-label">Payment Terms<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtTerms" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">From Date<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtFDate" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="lnkFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFDate" PopupButtonID="lnkFDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td class="matters"><span class="field-label">To Date<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtTDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="lnkTDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTDate" PopupButtonID="lnkTDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">Contact Details</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtContact" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    <td class="matters"><span class="field-label">Quotation Details</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtDescr" runat="server" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">Reference No.<span style="color: #800000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtRefNo" runat="server" CssClass="inputbox"></asp:TextBox></td>
                                    <td rowspan="2" class="matters"><span class="field-label">Notes</span></td>
                                    <td rowspan="2" class="matters" align="left">
                                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox></td>
                                </tr>
                                <tr runat="server" id="trUpload">
                                    <td class="matters"><span class="field-label">Upload Rates</span></td>
                                    <td class="matters">
                                        <asp:FileUpload ID="UploadFile" runat="server"
                                            ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />
                                        <asp:HyperLink ID="btnExport" runat="server">Export</asp:HyperLink></td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="left" class="matters">
                                        <asp:GridView ID="grdQUD" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="true"
                                            CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="ID" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_ID" runat="server" Text='<%# Bind("QUD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_ITEM_ID" runat="server" Text='<%# Bind("QUD_ITM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" HeaderStyle-Width="30%" FooterStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_DESCR" runat="server" Text='<%# Bind("ITM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQUD_DESCR" runat="server" Text='<%# Bind("ITM_DESCR") %>' Width="30%"></asp:TextBox><span style="color: #800000">*</span>
                                                        <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx2" OnClientPopulated="MultiSelect"
                                                            CompletionListCssClass="completionListElement" CompletionListItemCssClass="listItem" FirstRowSelected="true"
                                                            CompletionListHighlightedItemCssClass="highlightedListItem" OnClientItemSelected="ClientItemSelected"
                                                            CompletionSetCount="5" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                            ServiceMethod="GetItmDescr" ServicePath="Quotation.aspx" TargetControlID="txtQUD_DESCR">
                                                        </ajaxToolkit:AutoCompleteExtender>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("QUD_ITM_ID") %>' runat="server" />
                                                        <asp:ImageButton ID="imgItem" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />

                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtQUD_DESCR" runat="server" Text='<%# Bind("ITM_DESCR") %>' Width="30%"></asp:TextBox><span style="color: #800000">*</span>
                                                        <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx3" OnClientPopulated="MultiSelect"
                                                            CompletionListCssClass="completionListElement" CompletionListItemCssClass="listItem" FirstRowSelected="true"
                                                            CompletionListHighlightedItemCssClass="highlightedListItem" OnClientItemSelected="ClientItemSelected"
                                                            CompletionSetCount="5" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                            ServiceMethod="GetItmDescr" ServicePath="Quotation.aspx" TargetControlID="txtQUD_DESCR">
                                                        </ajaxToolkit:AutoCompleteExtender>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("QUD_ITM_ID") %>' runat="server" />
                                                        <asp:ImageButton ID="imgItem" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                    </FooterTemplate>
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Commodity" ItemStyle-Width="20%" HeaderStyle-Width="20%" FooterStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_BRAND" runat="server" Text='<%# Bind("UCO_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQUD_BRAND" runat="server" Enabled="false" Text='<%# Bind("UCO_DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtQUD_BRAND" runat="server" Enabled="false" Text='<%# Bind("UCO_DESCR") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Units">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_UNITS" runat="server" Text='<%# Bind("ITM_UNIT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQUD_UNITS" runat="server" Enabled="false" Text='<%# Bind("ITM_UNIT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtQUD_UNITS" runat="server" Enabled="false" Text='<%# Bind("ITM_UNIT") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Packing">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_PACKING" runat="server" Text='<%# Bind("ITM_PACKING") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQUD_PACKING" runat="server" Enabled="false" Text='<%# Bind("ITM_PACKING") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtQUD_PACKING" runat="server" Enabled="false" Text='<%# Bind("ITM_PACKING") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_DETAILS" runat="server" Text='<%# Bind("ITM_DETAILS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQUD_DETAILS" runat="server" Enabled="false" Text='<%# Bind("ITM_DETAILS") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtQUD_DETAILS" runat="server" Enabled="false" Text='<%# Bind("ITM_DETAILS") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unit Rate" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQUD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("QUD_RATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQUD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("QUD_RATE") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtQUD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("QUD_RATE") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton><asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="6">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="6">
                                        <asp:HiddenField ID="h_ACTTYPE" runat="server" />
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_supid" runat="server" />
                                        <asp:HiddenField ID="h_ItemID" runat="server" EnableViewState="False" />
                                        <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_QUDGridDelete" Value="0" runat="server" />
                                        <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />

                                        <asp:HiddenField ID="hfDESCR" runat="server" />
                                        <asp:HiddenField ID="hfBRAND" runat="server" />
                                        <asp:HiddenField ID="hfUNITS" runat="server" />
                                        <asp:HiddenField ID="hfPACKING" runat="server" />
                                        <asp:HiddenField ID="hfDETAILS" runat="server" />
                                        <asp:HiddenField ID="hfRATE" runat="server" />
                                        <asp:HiddenField ID="hfhdnITM_ID" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
