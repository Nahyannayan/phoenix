Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_Category
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI01060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim dt As New DataTable
            dt = MainObj.getRecords("SELECT ICM_ID, ICM_DESCR, ICM_TYPE, BUD_ID, BUD_MAIN, BUD_SUBHEAD, ACT_ID, ACT_NAME FROM ITEM_CATEGORY_M left outer JOIN oasisfin.dbo.BUDGET_D on ICM_BUD_ID=BUD_ID left outer JOIN oasisfin.dbo.ACCOUNTS_M on ACT_ID=ICM_ACT_ID where ICM_ID=" & p_Modifyid, "OASIS_PUR_INVConnectionString")
            fillDropdown(ddlCategoryType, "select 'A' ID, 'Assets' TYPE union select 'I', 'Inventory' ", "TYPE", "ID", False)
            If dt.Rows.Count > 0 Then
                h_ICM_ID.Value = dt.Rows(0)("ICM_ID").ToString
                txtPriDescr.Text = dt.Rows(0)("ICM_DESCR").ToString
                txtPurchase.Text = dt.Rows(0)("ACT_NAME").ToString
                txtBudget.Text = dt.Rows(0)("BUD_MAIN").ToString
                txtSubBudget.Text = dt.Rows(0)("BUD_SUBHEAD").ToString
                h_Purchase_ID.Value = dt.Rows(0)("ACT_ID").ToString
                ddlCategoryType.SelectedValue = dt.Rows(0)("ICM_TYPE").ToString
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        txtPriDescr.Enabled = Not mDisable
        txtBudget.Enabled = Not mDisable
        txtPurchase.Enabled = Not mDisable
        ddlCategoryType.Enabled = ViewState("datamode") = "add"
        imgBudget.Enabled = Not mDisable
        imgClient.Enabled = Not mDisable
        txtSubBudget.Enabled = Not mDisable
    End Sub

    Sub clear_All()
        h_ICM_ID.Value = 0
        h_Budget_ID.Value = 0
        h_ICM_ID.Value = 0
        h_Purchase_ID.Value = 0
        txtPriDescr.Text = ""
        txtBudget.Text = ""
        txtPurchase.Text = ""
        ddlCategoryType.SelectedIndex = 0
        txtSubBudget.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) FROM item where ITM_ICM_ID=" & h_ICM_ID.Value) = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from ITEM_CATEGORY_M where ICM_ID=" & h_ICM_ID.Value)
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = "unable to delete, " & txtPriDescr.Text & " used in transactions"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            If txtPriDescr.Text.Trim.Length = 0 Then lblError.Text &= "Category Description "
            'If txtPRFDate.Text.Trim.Length = 0 Then lblError.Text &= "PRF Date,"
            'If txtNeededBy.Text.Trim.Length = 0 Then lblError.Text &= "Needed by,"
            If lblError.Text.Length > 0 Then lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Mandatory"
            If h_Budget_ID.Value = "" Then h_Budget_ID.Value = "0"

            'ICM_DESCR, ICM_TYPE, ICM_CODE, ISC_BUD_ID
            Dim pParms(5) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@ICM_ID", h_ICM_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@ICM_DESCR", txtPriDescr.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@ICM_TYPE", ddlCategoryType.SelectedValue, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@ICM_BUD_ID", h_Budget_ID.Value, SqlDbType.Int)
            pParms(5) = Mainclass.CreateSqlParameter("@ICM_ACT_ID", h_Purchase_ID.Value, SqlDbType.VarChar)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveCategory", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(1).Value
                    lblError.Text = "Data Saved Successfully !!!"
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub



End Class



