﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class Fees_feereceiptSales
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'Page.Title = FeeCollectionOnlineBB.GetTransportTitle()7bad10
            gvFeeDetails.Attributes.Add("bordercolor", "#000095")
            gvPayments.Attributes.Add("bordercolor", "#000095")
            DISABLE_TAX_FIELDS()
            If Request.QueryString("id") <> "" Then
                PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")))
            ElseIf Request.QueryString("editid") <> "" Then
                PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")))
            ElseIf Request.QueryString("salid") <> "" Then
                PrintReceipt(Request.QueryString("salid"))
            End If
        End If
    End Sub
    Private Sub DISABLE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        Dim bShow As Boolean = False
        If Not Session("BSU_bVATEnabled") Is Nothing AndAlso IsNumeric(Session("BSU_bVATEnabled")) Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        TrVAT1.Visible = bShow
        TrVAT2.Visible = bShow
    End Sub
    Protected Sub PrintReceipt(ByVal p_Receiptno As String)
        Dim str_Sql As String
        str_Sql = "SELECT * FROM vw_SAL_H_RECEIPT WHERE SAL_ID = " & p_Receiptno
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = connectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            Dim TOTAL_AMOUNT As Double = 0
            imgLogo.ImageUrl = "~/Common/GetLogo.aspx?BSU_ID=" & ds.Tables(0).Rows(0)("BSU_ID")
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3")
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            If ds.Tables(0).Rows(0)("SAL_TYPE") = "S" Then
                lblCaption.Text = "Receipt"
            Else
                lblCaption.Text = "Refund"
            End If
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblStudentNo.Text = ds.Tables(0).Rows(0)("SAL_CUS_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("SAL_CUS_NAME").ToString().ToUpper()
            lblSchoolName.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            lblSchoolName1.Text = lblSchoolName.Text
            lblSchoolname2.Text = lblSchoolName.Text
            lblDate.Text = Format(ds.Tables(0).Rows(0)("SAL_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = ds.Tables(0).Rows(0)("GRADE_DISPLAY") + "-" + ds.Tables(0).Rows(0)("SAL_SECTION")
            lblRecno.Text = ds.Tables(0).Rows(0)("SAL_NO") 'ds.Tables(0).Rows(0)("CRT_ID")
            lblGradeTitle.Text = GET_GRADE_TITLE(ds.Tables(0).Rows(0)("BSU_CITY").ToString)

            lblVATAmount.Text = Format(CDbl(ds.Tables(0).Rows(0)("SAL_TAX_AMOUNT")), "#,##0.00")
            lblNETAmount.Text = Format(CDbl(ds.Tables(0).Rows(0)("SAL_NET_AMOUNT")), "#,##0.00")

            TOTAL_AMOUNT = CDbl(ds.Tables(0).Rows(0)("SAL_TOTAL")) + CDbl(ds.Tables(0).Rows(0)("SAL_TAX_AMOUNT")) + CDbl(ds.Tables(0).Rows(0)("SAL_CREDITCARD_CHARGE"))

            Dim receiptTotal As Decimal = ds.Tables(0).Rows(0)("SAL_TOTAL") + ds.Tables(0).Rows(0)("SAL_CREDITCARD_CHARGE")
            lblPaymentDetals.Visible = False
            NetworkIntl.Visible = False
            If receiptTotal < 0 Then receiptTotal = receiptTotal * -1
            lblBalance.Text = Format(receiptTotal, "#,##0.00")
            'If receiptTotal < 0 Then
            '    lblPaymentDetals.Visible = False
            '    NetworkIntl.Visible = False
            'Else
            '    Dim mcSpell As New Mainclass
            '    '"Received with thanks
            '    lblPaymentDetals.Text = ds.Tables(0).Rows(0)("CRT_CCMESSAGE") & " <b>" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(receiptTotal, "0.00") & _
            '    "</b> (" & mcSpell.SpellNumberWithDenomination(receiptTotal, "AED", 0) & _
            '    ") through Network International LLC's Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("FCO_ID") & " ) over the Internet towards the Catering Card ReCharge. "
            'End If
            Dim userName As String
            If Session("sUsr_name") Is Nothing Then
                userName = Encr_decrData.Decrypt(Request.QueryString("Mode"))
            Else
                userName = Session("sUsr_name").ToString
            End If
            lbluserloggedin.Text = userName & " (Issued Time: " + CDate(ds.Tables(0).Rows(0)("SAL_SYSDATE")).ToString("hh:mm tt") & ")"
            lblPrintTime.Text = "Printed Time: " + Now.ToString("dd/MMM/yyyy hh:mm tt") & ""

            'lblAmount.Text = Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            ''Dim str_paymnts As String = " exec fees.GetReceiptPrint_Online @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & Session("sBsuid") & "'"
            ''lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), "0.00")


            Dim str_paymnts As String
            str_paymnts = "select SAD_ID, SAD_DESCR+'-'+SAD_DETAILS SAD_DESCR, SAD_QTY, SAD_RATE, SAD_QTY*SAD_RATE SAD_TOTAL from SAL_D where SAD_SAL_ID=" & p_Receiptno
            str_paymnts &= "union all "
            str_paymnts &= "select 0, 'Transaction Processing Charge',1.0, SAL_CREDITCARD_CHARGE, SAL_CREDITCARD_CHARGE from SAL_H where SAL_ID=" & p_Receiptno & " and SAL_CREDITCARD_CHARGE<>0 "

            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()
            gvFeeDetails.HeaderRow.Cells(3).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
            'gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True

            str_paymnts = "select 'Cash' CLT_DESCR, '' CRI_DESCR, '' FCD_REFNO, SAL_CASHTOTAL-SAL_BALANCE FCD_AMOUNT from SAL_H where (SAL_CASHTOTAL-SAL_BALANCE)>0 and SAL_ID=" & p_Receiptno
            str_paymnts &= " union ALL "
            str_paymnts &= "SELECT 'Credit Card', CRI_DESCR, SAL_CREDITNO, SAL_CCTOTAL+SAL_CREDITCARD_CHARGE from SAL_H inner join (SELECT     CREDITCARD_M.CRI_DESCR + ' - ' + CREDITCARD_PROVD_M.CPM_DESCR AS CRI_DESCR, CREDITCARD_S.CRR_ID  "
            str_paymnts &= "FROM oasisfin..CREDITCARD_S INNER JOIN  "
            str_paymnts &= "oasisfin..CREDITCARD_M ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN  "
            str_paymnts &= "oasisfin..CREDITCARD_PROVD_M ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID  "
            str_paymnts &= "WHERE (CREDITCARD_S.CRR_bOnline = 0)) CREDITCARD on CRR_ID=SAL_CREDITCARD where SAL_CCTOTAL>0 and SAL_ID=" & p_Receiptno & " "
            str_paymnts &= "union all "
            str_paymnts &= "SELECT '','',''," & TOTAL_AMOUNT & " "

            Dim mcSpell As New Mainclass
            Dim ds2 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds2, Nothing)
            gvPayments.DataSource = ds2.Tables(0)
            gvPayments.DataBind()
            Dim columnCount As Integer = gvPayments.FooterRow.Cells.Count
            gvPayments.FooterRow.Cells.Clear()
            gvPayments.FooterRow.Cells.Add(New TableCell)
            gvPayments.FooterRow.Cells(0).ColumnSpan = columnCount
            gvPayments.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Left
            gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(TOTAL_AMOUNT, ds.Tables(0).Rows(0)("BSU_CURRENCY"), Session("BSU_ROUNDOFF"))
            gvPayments.FooterRow.Cells(0).Font.Bold = True
            Dim str_Total As String = gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(3).Text
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Clear()
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).ColumnSpan = columnCount - 1
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).Text = "Total"
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).HorizontalAlign = HorizontalAlign.Right

            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).Text = str_Total
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).HorizontalAlign = HorizontalAlign.Right
        End If
    End Sub
    Private Function GET_GRADE_TITLE(ByVal BSU_CITY As String) As String
        If BSU_CITY = "AUH" Then
            GET_GRADE_TITLE = "Year"
        Else
            GET_GRADE_TITLE = "Grade"
        End If
    End Function
End Class

