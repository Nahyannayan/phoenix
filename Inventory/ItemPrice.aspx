﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ItemPrice.aspx.vb" Inherits="Inventory_ItemPrice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    <%--    function getItemName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMLIST"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) return false;
            NameandCode = result.split('___');
            document.getElementById("<%=txtItem.ClientID%>").value = NameandCode[1];
            document.getElementById("<%=h_ITM_ID.ClientID%>").value = NameandCode[0];
        }--%>
    </script>

    
 <script>
     function getItemNameNew() {
            var pMode;
            var NameandCode;

            pMode = "ITMLIST"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_isbn");
        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
             document.getElementById("<%=txtItem.ClientID%>").value = NameandCode[1];
            document.getElementById("<%=h_ITM_ID.ClientID%>").value = NameandCode[0];
        }
    }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>
<telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_isbn" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Item Price
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                             
                        </td>
                    </tr>
                    <tr>
                        <td  >
                            <table   width="100%"  >
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label"> Business Unit Code</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtBUID" runat="server"  Enabled="false"></asp:TextBox></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Business Unit Name</span></td>
                                    <td width="30%">
                                        <asp:TextBox ID="TxtBUName" runat="server"  Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td align="left" class="matters"><span class="field-label">Item ID</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtItemId" runat="server" ></asp:TextBox>
                                        <asp:TextBox ID="TxtItemName" runat="server"  Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="imgICMDescr" runat="server" OnClientClick="getItemNameNew();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="h_ITB_ID" runat="server" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Item</span></td>
                                    <td align="left" class="matters" colspan="3">
                                        <asp:TextBox ID="txtItem" runat="server" Width="93%"  ></asp:TextBox>
                                        <asp:HiddenField ID="h_ITM_ID" runat="server" />
                                        <asp:ImageButton ID="imgItem" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getItemNameNew();return false" />
                                        <asp:LinkButton ID="lbtnClearItem" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                                        <asp:LinkButton ID="lbtnLoadItemDetail" runat="server" CausesValidation="False" OnClick="lbtnLoadItemDetail_Click"></asp:LinkButton>
                                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                            TargetControlID="txtItem"
                                            WatermarkText="Type in or Search Item description or ISBN"
                                            WatermarkCssClass="watermarked" />
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                            TargetControlID="lbtnClearItem"
                                            ConfirmText="Are you sure you want to remove the Item selection?" />
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Cost Price</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtItemCPrice" runat="server" ></asp:TextBox></td>
                                    <td align="left" class="matters"><span class="field-label">Sale Price</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtItemSPrice" runat="server" ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="matters" align="center" valign="bottom" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" ></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitItemAutoComplete();
        });
        function InitializeRequest(sender, args) {
        }
        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitItemAutoComplete();
        }
        function InitItemAutoComplete() {

            $("#<%=txtItem.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ItemPrice.aspx/GetItem",
                        data: "{ 'BSUID': '" + $("#<%=txtBUID.ClientID%>").val() + "','prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('$$$')[0] + ' | ' + item.split('$$$')[1],
                                    val: item.split('$$$')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=h_ITM_ID.ClientID%>").val(i.item.val);
                    __doPostBack('<%=h_ITM_ID.ClientID%>', "");
                },
                minLength: 1
            });
        }

        function GetItmId() {
            alert($("#<%=h_ITM_ID.ClientID%>").val());
            return false;
        }
    </script>
</asp:Content>

