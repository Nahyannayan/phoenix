﻿Imports Microsoft.ApplicationBlocks.Data
'Imports System
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj


Partial Class Inventory_TenancyContractDeHiring
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass(), requestForQuotation As Boolean = False
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")


            If Not ViewState("ActiveTabIndex") Is Nothing Then TabContainer1.ActiveTabIndex = ViewState("ActiveTabIndex")

            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                hViewState.Value = ViewState("datamode")
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")


                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI04030") And (ViewState("MainMnu_code") <> "PI02050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                BindDropDown()
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(ViewState("EntryId"))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If

                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                DisableButtons(ViewState("menu_rights"))


            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub DisableButtons(ByVal AccesRights As Integer)

        If AccesRights = 1 Then
            btnAdd.Visible = False
            btnRecall.Visible = False

            btnEdit.Visible = False
            btnSave.Visible = False
            btnAccept.Visible = False
            btnRecall.Visible = False
            btnSend.Visible = False
            btnSend.Visible = False
            btnPrint.Visible = False


        End If
    End Sub
    Private Sub BindDropDown()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim str_Sql1 As String = "SELECT ID,DESCR FROM VW_DEHIRINGREASON ORDER BY ID"
            ddlReason.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql1)
            ddlReason.DataTextField = "DESCR"
            ddlReason.DataValueField = "ID"
            ddlReason.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            hViewState.Value = ViewState("datamode")
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            hViewState.Value = ViewState("datamode")
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            hViewState.Value = ViewState("datamode")
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        ddlReason.Enabled = Not mDisable
        imgLDate.Enabled = EditAllowed
        imgVDate.Enabled = EditAllowed
        txtremarks.Enabled = EditAllowed
        btnSend.Visible = mDisable
        radLandlordYES.Enabled = Not mDisable
        radLandlordNo.Enabled = Not mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnEdit.Visible = mDisable
        btnPrint.Visible = Not btnSave.Visible
        btnAccept.Visible = mDisable And (ViewState("MainMnu_code") = "PI02050" Or ViewState("MainMnu_code") = "PI04030")
        btnReject.Visible = btnAccept.Visible
        imgClient.Enabled = EditAllowed
        btnAdd.Visible = mDisable And Not (ViewState("MainMnu_code") = "PI02050")
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Dim sqlStr As New StringBuilder
        Dim sqlWhere As String = p_Modifyid
        Dim str_conn As String = connectionString
        Dim dt As New DataTable
        h_EntryId.Value = p_Modifyid

        If p_Modifyid = 0 Then
            trApproval.Visible = False
            trWorkflow.Visible = False
            trFlow.Visible = False
            trApproval.HeaderText = ""
            trWorkflow.HeaderText = ""
            trFlow.HeaderText = ""
            ClearDetails()
            txtTCD_No.Text = "New"
            btnAccept.Visible = False
            btnReject.Visible = False
            btnRecall.Visible = False
            btnEdit.Visible = False
            btnSend.Visible = False
            btnSave.Visible = True
            'txtAllow.Text = "0.00"
            'txtRent.Text = "0.00"
            'txtSchoolCost.Text = "0.00"
            pgTitle.Text = "De Hiring"

        Else

            sqlStr = New StringBuilder
            sqlStr.Append("select [TCD_ID], [TCD_NO], [TCD_TCH_ID], [TCD_BSU_ID], [TCD_DH_DATE], [TCD_REASON],TCH_NO, [TCD_NP], ")
            sqlStr.Append("[TCD_LANDLORD_CONFIRMATION], [TCD_REMARKS], [TCD_FYEAR], isnull(TCD_STATUS,'N')TCD_STATUS,")
            sqlStr.Append("isnull(TCD_APR_ID,0)TCD_APR_ID,isnull(TCD_STATUS_STR,'')TCD_STATUS_STR, [TCD_DELETED], [TCD_LOG_USER], [TCD_LOG_DATE]")
            sqlStr.Append(",'Start Date:'+ CONVERT(varchar, TCH_START, 106) + '   , End Date :'+ CONVERT(varchar, TCH_END, 106) + '  ,Rent :'+  cast(tch_rent as varchar(15)) TRFDetails")
            sqlStr.Append(",LEFT(STUFF((SELECT  CAST(ROW_NUMBER() OVER(PARTITION  BY TCE_TCH_ID ORDER BY TCE_TCH_ID ) AS VARCHAR) + '.' + case when tce_emp_id=0 then 'TBC' else case when TCE_EMP_ID=1 then 'Vacant' else ISNULL(emp_name,'') end end + '-' + isnull(DES_DESCR,'') + ',' FROM    TCT_E  LEFT OUTER JOIN oasis_docs..VW_EMPLOYEE_V E ON TCE_EMP_ID=e.EMP_ID LEFT OUTER JOIN oasis..EMPLOYEE_M EM ON EM.emp_id=e.EMP_ID LEFT OUTER JOIN oasis..empdesignation_m ON des_id=emp_des_id WHERE   TCH_ID=TCE_TCH_ID AND TCE_DELETED<>1 FOR   XML PATH('') ,TYPE ).value('.','varchar(max)'),1,0,''), LEN(STUFF((SELECT  CAST(ROW_NUMBER() OVER(PARTITION  BY TCE_TCH_ID ORDER BY TCE_TCH_ID ) AS VARCHAR) + '.' + case when tce_emp_id=0 then 'TBC' else case when TCE_EMP_ID=1 then 'Vacant' else ISNULL(emp_name,'') end end + '-' + isnull(DES_DESCR,'') + ',' FROM    TCT_E LEFT OUTER JOIN oasis_docs..VW_EMPLOYEE_V E ON TCE_EMP_ID=e.EMP_ID LEFT OUTER JOIN oasis..EMPLOYEE_M EM ON EM.emp_id=e.EMP_ID LEFT OUTER JOIN oasis..empdesignation_m ON des_id=emp_des_id   WHERE   TCH_ID=TCE_TCH_ID AND TCE_DELETED<>1FOR   XML PATH ('') ,TYPE ).value('.','varchar(max)'),1,0,'')) - 1) AS StaffDetails ")
            sqlStr.Append(" from TCT_DEHIRING inner join tct_h on tch_id=tcd_tch_id where TCD_ID=" & p_Modifyid)

            dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")
            If dt.Rows.Count > 0 Then

                h_tcd_ID.Value = dt.Rows(0)("TCD_ID")
                txtTRFNo.Text = dt.Rows(0)("TCH_NO")
                txtStaff.Text = dt.Rows(0)("StaffDetails")
                TRFDetails.Text = dt.Rows(0)("TRFDetails")
                txtTCD_No.Text = dt.Rows(0)("TCD_NO")
                h_THW_Status.Value = dt.Rows(0)("TCD_STATUS")
                h_delete.Value = dt.Rows(0)("TCD_DELETED")
                txtContractDate.Text = Format(dt.Rows(0)("TCD_LOG_DATE"), "dd/MMM/yyyy")
                txtremarks.Text = dt.Rows(0)("TCD_REMARKS")
                h_ThW_apr_id.Value = dt.Rows(0)("TCD_APR_ID")
                h_tcd_tch_ID.Value = dt.Rows(0)("TCD_TCH_ID")

                ddlReason.Items.FindByValue(dt.Rows(0)("TCD_REASON")).Selected = True

                btnAccept.Visible = btnAccept.Visible And dt.Rows(0)("TCD_STATUS") = "S"
                btnReject.Visible = btnAccept.Visible
                txtVacatingDate.Text = Format(dt.Rows(0)("TCD_DH_DATE"), "dd/MMM/yyyy")
                txtLandlordConfirmation.Text = Format(dt.Rows(0)("TCD_LANDLORD_CONFIRMATION"), "dd/MMM/yyyy")
                btnSend.Visible = btnSend.Visible And (dt.Rows(0)("TCD_STATUS") = "N")
                btnSave.Visible = btnSave.Visible And ((dt.Rows(0)("TCD_STATUS") = "N"))
                btnPrint.Visible = Not btnSave.Visible
                btnEdit.Visible = btnEdit.Visible And (dt.Rows(0)("TCD_STATUS") = "N") And dt.Rows(0)("TCD_APR_ID") = 0 And (ViewState("MainMnu_code") = "PI02050")
                'btnEdit.Visible = btnEdit.Visible Or ViewState("MainMnu_code") = "PI02050"

                bindServicePaymentDetails(txtTRFNo.Text)
                bindRentalPaymentDetails(txtTRFNo.Text)
                bindRecoveryPaymentDetails(txtTRFNo.Text)
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
        If p_Modifyid > 0 Then
            If ViewState("MainMnu_code") = "PI04030" Then
                trApproval.Visible = True
                trApproval.HeaderText = "Approval\Rejection Comments"
            End If
            Dim ds As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "workflowqueryDH " & h_EntryId.Value).Tables(0)
            rptFlow.DataSource = ds
            rptFlow.DataBind()
            trFlow.Visible = True
            trFlow.HeaderText = "Workflow progress"
            trWorkflow.Visible = True
            trWorkflow.HeaderText = "Workflow Details"

            sqlStr = New StringBuilder
            sqlStr.Append("select replace(convert(varchar(16), WRK_DATE, 106),' ','/') WRK_DATE, USR_DISPLAY_NAME WRK_USER, WRK_ACTION, WRK_DETAILS from WORKFLOWDH inner JOIN OASIS.dbo.USERS_M on WRK_USERNAME=USR_NAME where WRK_TCD_ID=" & h_EntryId.Value & " order by WRK_ID desc")
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr.ToString).Tables(0)
            gvWorkflow.DataSource = ds
            gvWorkflow.DataBind()

        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try

            If h_tcd_tch_ID.Value = "" Then lblError.Text = "TRF Not Selected,"


            If lblError.Text.Length > 0 Then
                Exit Sub
            End If



            Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
            Dim pParms(13) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@TCD_ID", SqlDbType.Int)
            pParms(1).Value = h_EntryId.Value

            pParms(2) = New SqlClient.SqlParameter("@TCD_NO", SqlDbType.VarChar)
            pParms(2).Value = txtTCD_No.Text

            pParms(3) = New SqlClient.SqlParameter("@TCD_TCH_ID", SqlDbType.Int)
            pParms(3).Value = h_tcd_tch_ID.Value

            pParms(4) = New SqlClient.SqlParameter("@TCD_BSU_ID", SqlDbType.VarChar)
            pParms(4).Value = Session("sBsuID")

            pParms(5) = New SqlClient.SqlParameter("@TCD_DH_DATE", SqlDbType.DateTime)
            pParms(5).Value = txtVacatingDate.Text

            pParms(6) = New SqlClient.SqlParameter("@TCD_REASON", SqlDbType.Int)
            pParms(6).Value = ddlReason.SelectedValue

            pParms(7) = New SqlClient.SqlParameter("@TCD_NP", SqlDbType.Int)
            If radLandlordYES.Checked = True Then
                pParms(7).Value = 1
            Else
                pParms(7).Value = 0
            End If
            pParms(8) = New SqlClient.SqlParameter("@TCD_LANDLORD_CONFIRMATION", SqlDbType.DateTime)
            pParms(8).Value = txtLandlordConfirmation.Text

            pParms(9) = New SqlClient.SqlParameter("@TCD_REMARKS", SqlDbType.VarChar)
            pParms(9).Value = txtremarks.Text

            pParms(10) = New SqlClient.SqlParameter("@TCD_FYEAR", SqlDbType.VarChar)
            pParms(10).Value = Session("F_YEAR")

            pParms(11) = New SqlClient.SqlParameter("@TCD_LOG_USER", SqlDbType.VarChar)
            pParms(11).Value = Session("sUsr_name")

            pParms(12) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
            pParms(12).Direction = ParameterDirection.Output

            pParms(13) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(13).Direction = ParameterDirection.ReturnValue



            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveTCT_DEHIRING", pParms)
            Dim ErrorMSG As String = pParms(12).Value.ToString()
            If ErrorMSG = "" Then
                stTrans.Commit()
                ViewState("EntryId") = pParms(13).Value
                writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), txtComments.Text, 0, "N")
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                lblError.Text = "Data Saved Successfully !!!"
            Else
                lblError.Text = ErrorMSG
                stTrans.Rollback()
                Exit Sub
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Sub ClearDetails()
        txtContractDate.Text = Now.ToString("dd/MMM/yyyy")
        txtremarks.Text = ""
        txtTCD_No.Text = ""
        h_tcd_ID.Value = ""
        h_tcd_tch_ID.Value = ""

    End Sub
    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnSend.Visible = Not btnSave.Visible
        btnPrint.Visible = Not btnSave.Visible
        'btnReturn.Visible = Not btnSave.Visible

    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()

        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(h_EntryId.Value)
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim cmd As New SqlCommand
        cmd.CommandText = "rptDehiringTRFREQUEST"
        Dim sqlParam(1) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@DH_ID", h_EntryId.Value, SqlDbType.Int)
        cmd.Parameters.Add(sqlParam(0))
        'sqlParam(1) = Mainclass.CreateSqlParameter("@LOCID", h_Loc_id.Value, SqlDbType.VarChar)
        'cmd.Parameters.Add(sqlParam(1))
        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        'V1.2 comments start------------
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")


        Dim repSource As New MyReportClass
        repSource.Command = cmd
        repSource.Parameter = params
        repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractDeHiring.rpt"

        Dim subRep(0) As MyReportClass
        subRep(0) = New MyReportClass
        Dim subcmd1 As New SqlCommand
        subcmd1.CommandText = "rptTenancyPaymentDetails"
        Dim sqlParam1(0) As SqlParameter
        sqlParam1(0) = Mainclass.CreateSqlParameter("@TCH_ID", h_tcd_tch_ID.Value, SqlDbType.Int)
        subcmd1.Parameters.Add(sqlParam1(0))
        subcmd1.CommandType = Data.CommandType.StoredProcedure
        subcmd1.Connection = New SqlConnection(str_conn)
        subRep(0).Command = subcmd1
        subRep(0).ResourceName = "wBTA_Sector"
        subRep(0).Command = subcmd1
        subRep(0).ResourceName = "wBTA_Sector"
        repSource.IncludeBSUImage = True
        repSource.SubReport = subRep
        Session("ReportSource") = repSource

        ReportLoadSelection()
    End Sub



    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        doApproval()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Function doApproval(Optional ByVal TCD_ID As Integer = 0) As Integer
        If TCD_ID = 0 Then TCD_ID = h_EntryId.Value
        Dim strApprover() As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverDH(" & TCD_ID & ")").ToString.Split(";")
        doApproval = -1
        If strApprover(0) = 500 Then 'finally approved
            doApproval = writeWorkFlow(TCD_ID, "Approval", "DeHiring:Archived", 0, "C")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_DEHIRING set TCD_APR_ID=500, TCD_STATUS='C', TCD_STATUS_STR='DeHiring:Archived' where TCD_ID=" & TCD_ID)
            End If
        Else
            doApproval = writeWorkFlow(TCD_ID, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_DEHIRING set TCD_STATUS_STR='Approval:with " & strApprover(1) & "', TCD_STATUS=case when TCD_STATUS='N' then 'S' else TCD_STATUS end, TCD_APR_ID=" & strApprover(0) & " where TCD_id=" & TCD_ID)
            End If
        End If
    End Function
    Private Function writeWorkFlow(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TCD_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", txtApprovals.Text.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveWorkFlowDH", iParms)
        writeWorkFlow = iParms(8).Value
    End Function

    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select

    End Function

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Rejection Comments are Mandatory"
            Exit Sub
        End If
        If writeWorkFlow(ViewState("EntryId"), "Rejected", txtApprovals.Text, h_ThW_apr_id.Value, "R") = 0 Then
            'SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_STATUS_STR='Rejected:by " & Session("sUsr_name") & "', TCH_STATUS='R' where TCH_ID=" & h_EntryId.Value)
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_DEHIRING set TCD_STATUS_STR='Rejected:by " & Session("sUsr_name") & "',TCD_STATUS='R' where TCD_ID=" & h_EntryId.Value)
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If Tr4.Visible Then
        '    If (txtRent.Text = "" Or Val(txtRent.Text) <= 0) Then
        '        lblError.Text = "enter Rent Amount"
        '        Exit Sub
        '    End If
        '    If IsNumeric(txtRent.Text) = False Then
        '        lblError.Text = "enter numbers only"
        '        Exit Sub
        '    End If
        'End If
        If doApproval() = 0 Then
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    'Protected Sub btnRecall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecall.Click
    '    lblError.Text = ""
    '    If h_EntryId.Value > 0 Then
    '        Dim objConn As New SqlConnection(connectionString)
    '        objConn.Open()
    '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
    '        Try
    '            Dim sqlStr As String = "update TCT_H set TCH_DELETED=0, TCH_STATUS='N', TCH_STATUS_STR='RECALL', TCH_APR_ID=0 where TCH_ID=@TCH_ID"
    '            Dim pParms(1) As SqlParameter
    '            pParms(1) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int, True)
    '            SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
    '            stTrans.Commit()
    '            writeWorkFlow(ViewState("EntryId"), "Recalled", txtApprovals.Text, 0, "E")
    '            Response.Redirect(ViewState("ReferrerUrl"), False)
    '        Catch ex As Exception
    '            stTrans.Rollback()
    '            lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
    '            Errorlog(ex.Message)
    '            Exit Sub
    '        Finally
    '            objConn.Close()
    '        End Try
    '        Response.Redirect(ViewState("ReferrerUrl"))
    '    End If
    'End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub txtTRFNo_TextChanged(sender As Object, e As EventArgs) Handles txtTRFNo.TextChanged
        bindServicePaymentDetails(txtTRFNo.Text)
        bindRentalPaymentDetails(txtTRFNo.Text)
        bindRecoveryPaymentDetails(txtTRFNo.Text)
    End Sub
    Sub bindServicePaymentDetails(trfNo As String)
        'Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID in(select RCM_ID FROM RECITY_M WHERE RCM_DESCR='ALL') AND RES_DELETED<>1   UNION ALL select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID ='" & LocId & "' AND RES_DELETED<>1  order by ID").Tables(0)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "DehiringpaymentDetails '" & trfNo & "'").Tables(0)
        RepService.DataSource = dt
        RepService.DataBind()
    End Sub
    Sub bindRentalPaymentDetails(trfNo As String)
        'Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID in(select RCM_ID FROM RECITY_M WHERE RCM_DESCR='ALL') AND RES_DELETED<>1   UNION ALL select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID ='" & LocId & "' AND RES_DELETED<>1  order by ID").Tables(0)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "DehiringpaymentDetails '" & trfNo & "'").Tables(1)
        grdPayment.DataSource = dt
        grdPayment.DataBind()
    End Sub
    Sub bindRecoveryPaymentDetails(trfNo As String)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "DehiringpaymentDetails '" & trfNo & "'").Tables(2)
        'Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID in(select RCM_ID FROM RECITY_M WHERE RCM_DESCR='ALL') AND RES_DELETED<>1   UNION ALL select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID ='" & LocId & "' AND RES_DELETED<>1  order by ID").Tables(0)
        GrdRec.DataSource = dt
        GrdRec.DataBind()
    End Sub

    Private Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click

    End Sub
End Class

