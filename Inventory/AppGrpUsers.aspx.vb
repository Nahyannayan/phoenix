﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_AppGrpUsers
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                'ts
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI04003") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim dt As New DataTable
            dt = MainObj.getRecords("select BSU_NAME, ROL_DESCR, GRPA_DESCR, usersa.* from usersa inner join groupsa on GRPA_ID=USRA_GRP_ID inner join oasis..users_m on usr_name=usra_usr_name inner join oasis..ROLES_M on usr_rol_id=rol_id inner join oasis..businessunit_m on bsu_id=usra_bsu_id where usra_id=" & p_Modifyid, "OASIS_PUR_INVConnectionString")

            If dt.Rows.Count > 0 Then
                txtUserName.Text = dt.Rows(0)("USRA_USR_Name").ToString
                h_Usra_id.Value = dt.Rows(0)("usra_id").ToString
                txtUserGrpId.Text = dt.Rows(0)("usra_grp_id").ToString
                txtUserDate.Text = Format(dt.Rows(0)("usra_date"), "dd-MMM-yyyy")
                txtUserEmail.Text = dt.Rows(0)("usra_email").ToString
                txtBsuId.Text = dt.Rows(0)("usra_BSU_ID").ToString
                TxtGrpName.Text = dt.Rows(0)("GRPA_DESCR").ToString
                txtBSUName.Text = dt.Rows(0)("BSU_NAME").ToString
                If dt.Rows(0)("usra_enable").ToString = "1" Then
                    ChkBox1.Checked = True
                Else
                    ChkBox1.Checked = False
                End If

                If txtUserGrpId.Text <> "20" Then
                    EmailRow.Visible = False
                Else
                    EmailRow.Visible = True
                End If
            Else
                txtUserDate.Text = Now.ToString("dd/MMM/yyyy")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable

        If ViewState("datamode") = "edit" Or ViewState("datamode") = "view" Then
            ImgGroup.Visible = False
            imgUserName.Visible = False
            ImgBSU.Visible = False
        Else
            ImgGroup.Visible = True
            imgUserName.Visible = True
        End If
        txtUserDate.Enabled = Not mDisable
        txtUserEmail.Enabled = Not mDisable
    End Sub

    Sub clear_All()
        h_Usra_id.Value = 0
        txtUserName.Text = ""
        txtBsuId.Text = ""
        txtUserGrpId.Text = ""
        TxtGrpName.Text = ""
        txtUserName.Text = ""
        txtUserDate.Text = Now.ToString("dd/MMM/yyyy")
        txtUserEmail.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim flagAudit As Integer
        Dim pParms(8) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@USRA_ID", h_Usra_id.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@USRA_GRP_ID", txtUserGrpId.Text, SqlDbType.Int)
        pParms(3) = Mainclass.CreateSqlParameter("@USRA_USR_Name", txtUserName.Text, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@USRA_BSU_ID", txtBsuId.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@USRA_DATE", txtUserDate.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@USRA_ENABLE", IIf(ChkBox1.Checked = True, "1", "0"), SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@USRA_EMAIL", txtUserEmail.Text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@DELETE", "1", SqlDbType.VarChar)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "Save_AppGrpUsers", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else

                flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, pParms(1).Value, "Delete", Page.User.Identity.Name.ToString, Me.Page, "")

                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                    SetDataMode("view")
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim flagAudit As Integer
            lblError.Text = ""
            If EmailRow.Visible Then
                If Not txtUserEmail.Text.Contains("@") Or Not txtUserEmail.Text.Contains(".") Then
                    lblError.Text = "Enter valid email Id"
                    Exit Sub
                End If
            ElseIf h_Usra_id.Value = "" Then
                lblError.Text = "Group id is empty"
                Exit Sub
            ElseIf txtUserName.Text = "" Then
                lblError.Text = "User Name is empty"
                Exit Sub
            ElseIf txtBsuId.Text = "" Then
                lblError.Text = "BSU is empty"
                Exit Sub
            End If
            Dim pParms(8) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@USRA_ID", h_Usra_id.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@USRA_GRP_ID", txtUserGrpId.Text, SqlDbType.Int)
            pParms(3) = Mainclass.CreateSqlParameter("@USRA_USR_Name", txtUserName.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@USRA_BSU_ID", txtBsuId.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@USRA_DATE", txtUserDate.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@USRA_ENABLE", IIf(ChkBox1.Checked = True, "1", "0"), SqlDbType.VarChar)
            If txtUserGrpId.Text <> "20" Then
                pParms(7) = Mainclass.CreateSqlParameter("@USRA_EMAIL", "", SqlDbType.VarChar)
            Else
                pParms(7) = Mainclass.CreateSqlParameter("@USRA_EMAIL", txtUserEmail.Text, SqlDbType.VarChar)
            End If
            pParms(8) = Mainclass.CreateSqlParameter("@DELETE", "0", SqlDbType.VarChar)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "Save_AppGrpUsers", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    If ViewState("datamode") = "edit" Then
                        flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, pParms(1).Value, "Edit", Page.User.Identity.Name.ToString, Me.Page, "")
                    Else
                        flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, pParms(1).Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                    End If

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                        SetDataMode("view")
                        stTrans.Rollback()
                    Else
                        stTrans.Commit()
                        ViewState("EntryId") = pParms(1).Value
                        lblError.Text = getErrorMessage("0")
                        setModifyHeader(ViewState("EntryId"))
                        SetDataMode("view")
                    End If
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub txtUserGrpId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserGrpId.TextChanged
        If txtUserGrpId.Text <> "20" Then
            EmailRow.Visible = False
        Else
            EmailRow.Visible = True
        End If
    End Sub
End Class
