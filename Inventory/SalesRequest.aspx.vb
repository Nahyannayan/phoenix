﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports ICSharpCode.SharpZipLib.Checksums
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.GZip
Imports System.Drawing

Partial Class Inventory_SalesRequest
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass(), requestForQuotation As Boolean = False

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            Else
                Response.Redirect("~\noAccess.aspx")
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "PI02009" And ViewState("MainMnu_code") <> "PI02011" And ViewState("MainMnu_code") <> "PI02013" And ViewState("MainMnu_code") <> "PI02015") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Select Case ViewState("MainMnu_code")
                Case "PI02009"
                    pgTitle.Text = "Retail Sales"
                Case "PI02011"
                    pgTitle.Text = "Retail Sales Return"
                Case "PI02015"
                    pgTitle.Text = "Stock Adjustment"
            End Select
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            If Not Session("sal_id") Is Nothing Then
                If Session("sal_id") > 0 Then
                    ViewState("EntryId") = Session("sal_id")
                    Session("sal_id") = 0
                End If
            End If
            LOAD_VAT_CODES()
            DISABLE_TAX_FIELDS()
            SET_VAT_DROPDOWN_RIGHT()
            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                SET_INVOICE_PRINT()
            End If
            lblTotal.Text = "Total (" & Session("BSU_CURRENCY").ToString & ")"
            showNoRecordsFound()
            hGridRefresh.Value = 0
            grdSAL.DataSource = SALFooter
            grdSAL.DataBind()
            showNoRecordsFound()
        End If
        If hGridRefresh.Value = 1 Then
            'grdSAL.DataSource = SALFooter
            'grdSAL.DataBind()
            hGridRefresh.Value = 0
            showNoRecordsFound()
        End If

    End Sub
    Sub SET_INVOICE_PRINT()
        Dim QueryString = "&ID=" & Encr_decrData.Encrypt(ViewState("EntryId")) & ""
        btnInvoice.Attributes.Remove("onClick")
        btnInvoice.Attributes.Add("onClick", "return ShowWindowWithClose('../Fees/FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("BSTAXINV") & QueryString & "','INVOICE','55%','85%');")
    End Sub
    Sub LOAD_VAT_CODES()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT TAX_ID,TAX_CODE,TAX_DESCR FROM TAX.VW_TAX_CODES WHERE ISNULL(TAX_bDeleted,0)=0 ORDER BY TAX_ID")
        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAX_DESCR"
        ddlVATCode.DataValueField = "TAX_CODE"
        ddlVATCode.DataBind()
        Dim QRY As String = "SELECT ISNULL(TAX_CODE,'')TAX_CODE FROM OASIS.TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuid") & "','FEE','70',GETDATE(),0,'')"
        Dim TAX_CODE As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, QRY)
        If ddlVATCode.Items.Count > 0 AndAlso Not ddlVATCode.Items.FindByValue(TAX_CODE) Is Nothing Then
            ddlVATCode.SelectedValue = TAX_CODE
        End If
    End Sub
    Private Sub DISABLE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        Dim bShow As Boolean = False
        If Not Session("BSU_bVATEnabled") Is Nothing Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        trVATCode.Visible = bShow
        trVATTotal.Visible = bShow
        btnInvoice.Visible = bShow
        grdSAL.Columns(8).Visible = bShow
        grdSAL.Columns(9).Visible = bShow
    End Sub

    Private Sub CALCULATE_TAX(ByVal Amount As Double)
        lblTotalVATAmount.Text = "0.00"
        lblTotalNETAmount.Text = "0.00"
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & Amount & ",'" & ddlVATCode.SelectedValue & "')")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTotalVATAmount.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                lblTotalNETAmount.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub
    Sub SET_VAT_DROPDOWN_RIGHT()
        Dim bHasRight As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CASE WHEN COUNT(USER_NAME)>0 THEN 1 ELSE 0 END AS bEXIST FROM OASIS.TAX.VW_VAT_ENABLE_USERS WHERE USER_NAME='" & Session("sUsr_name").ToString & "' AND USER_SOURCE='FEES'"))
        If Not bHasRight Then
            ddlVATCode.Enabled = False
        ElseIf bHasRight AndAlso (ViewState("MainMnu_code") = "PI02015" Or ViewState("MainMnu_code") = "PI02009") Then 'if Sales Invoice or Stock Adjustment
            ddlVATCode.Enabled = True
        End If

    End Sub
    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        grdSAL.Columns(4).Visible = (ViewState("MainMnu_code") = "PI02015")
        grdSAL.Columns(10).Visible = Not mDisable
        grdSAL.Columns(11).Visible = Not mDisable
        grdSAL.ShowFooter = Not mDisable

        txtRemarks.Enabled = EditAllowed
        txtCashTotal.Enabled = EditAllowed
        txtDue.Enabled = False
        txtCCTotal.Enabled = EditAllowed
        txtCrCardCharges.Enabled = EditAllowed
        txtReceivedTotal.Enabled = False
        txtBalance.Enabled = False
        txtCreditno.Enabled = txtCreditno.Enabled
        ddCreditcard.Enabled = ddCreditcard.Enabled
        SET_VAT_DROPDOWN_RIGHT()
        txtStuNo.Enabled = EditAllowed
        txtStuName.Enabled = EditAllowed
        ddlBSU.Enabled = False 'EditAllowed And (ViewState("datamode") = "add")
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable 'Or ViewState("MainMnu_code") = "U000086"
        btnEdit.Visible = mDisable
        btnPrint.Visible = Not btnSave.Visible
        btnAdd.Visible = (Session("F_YEAR") >= "2014") And mDisable And Not (ViewState("MainMnu_code") = "PI02013")
        imgClient.Visible = ViewState("datamode") = "add"

    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim sqlWhere As String = p_Modifyid, qry As String, isReturn As Boolean = False
            h_EntryId.Value = p_Modifyid
            If requestForQuotation Or p_Modifyid > 0 Then
                fillDropdown(ddlBSU, "select BSU_ID , BSU_NAME from OASIS.dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME", "bsu_Name", "bsu_Id", False)
            Else
                fillDropdown(ddlBSU, "select BSU_ID , BSU_NAME from OASIS.dbo.fn_GetBusinessUnits " & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME", "bsu_Name", "bsu_Id", False)
            End If
            qry = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM CREDITCARD_S WITH(NOLOCK) INNER JOIN " & _
                    " CREDITCARD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
                    " CREDITCARD_PROVD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
                    " WHERE (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
            Me.hfCobrand.Value = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)
            ddlBSU.SelectedValue = Session("sBsuid")
            If ViewState("MainMnu_code") = "PI02009" Then
                h_SAL_Type.Value = "S"
            ElseIf ViewState("MainMnu_code") = "PI02011" Then
                h_SAL_Type.Value = "R"
                lblPRFNo.Text = "Return Number"
                lblPRFDate.Text = "Return Date"
                isReturn = True
            ElseIf ViewState("MainMnu_code") = "PI02015" Then
                h_SAL_Type.Value = "A"
                lblPRFNo.Text = "Tran Number"
                lblPRFDate.Text = "Tran Date"
            Else
                lblPRFNo.Text = "Tran Number"
                lblPRFDate.Text = "Tran Date"
            End If

            If p_Modifyid = 0 Then
                h_empid.Value = ""
                qry = "SELECT CLM_DESCR + '-' + ACY.ACY_DESCR  AS ACY_DESCR, ACD.ACD_ID " & _
                        "FROM oasis_fees..ACADEMICYEAR_D AS ACD INNER JOIN oasis_fees..ACADEMICYEAR_M AS ACY ON ACD.ACD_ACY_ID = ACY.ACY_ID " & _
                        "INNER JOIN oasis_fees..CURRICULUM_M AS CLM ON ACD.ACD_CLM_ID = CLM.CLM_ID " & _
                        "WHERE (ACD.ACD_BSU_ID = '123004') AND (ISNULL(ACD.ACD_bCLOSE, 0) = 0) and ACD_CURRENT=1 " & _
                        "ORDER BY ACD.ACD_STARTDT"
                qry = qry.Replace("123004", Session("sBsuid"))

                Dim dtACD As DataTable = GetAcademicYear(Session("sBSUID"), 0)
                ddlTerm.DataSource = dtACD
                ddlTerm.DataTextField = "ACY_DESCR"
                ddlTerm.DataValueField = "ACD_ID"
                ddlTerm.DataBind()
                For Each rowACD As DataRow In dtACD.Rows
                    If rowACD("ACD_CURRENT") Then
                        ddlTerm.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                        Exit For
                    End If
                Next
            Else
                ddlTerm.Visible = False
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                Dim sqlStr As New StringBuilder
                sqlStr.Append("SELECT SAL_ID, SAL_TYPE, SAL_NO, SAL_NARRATION, SAL_BSU_ID,SAL_CASHTOTAL, SAL_CCTOTAL, SAL_RECVDTOTAL, SAL_BALANCE, SAL_CREDITNO, SAL_CREDITCARD, ")
                sqlStr.Append("REPLACE(convert(varchar(30), sal_Date, 106),' ','/') SAL_DATE_STR, SAL_CUS_ID, SAL_OLDSAL_NO, SAL_POSTED, SAL_DELETED, SAL_CREDITCARD_CHARGE, ")
                sqlStr.Append("SAL_CUS_NO, SAL_CUS_NAME, SAL_GRADE, SAL_SECTION, SAL_TOTAL, SAL_REMARKS, SAL_FYEAR, SAL_USR_NAME,ISNULL(SAL_TAX_CODE,'NA')AS SAL_TAX_CODE, ")
                sqlStr.Append("ISNULL(SAL_TAX_AMOUNT,0) AS SAL_TAX_AMOUNT, ISNULL(SAL_NET_AMOUNT,0) AS SAL_NET_AMOUNT,ISNULL(SAL_bSTATIONARY,0)SAL_bSTATIONARY FROM SAL_H WITH(NOLOCK)")
                sqlStr.Append("WHERE SAL_ID = " & sqlWhere)
                dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    If sqlWhere.Length > 0 Then
                        h_EntryId.Value = dt.Rows(0)("SAL_ID")
                    End If
                    txtRemarks.Text = dt.Rows(0)("SAL_NARRATION")
                    txtCashTotal.Text = dt.Rows(0)("SAL_CASHTOTAL")
                    txtDue.Text = dt.Rows(0)("SAL_TOTAL")
                    txtCCTotal.Text = dt.Rows(0)("SAL_CCTOTAL")
                    txtOldSalNo.Text = dt.Rows(0)("SAL_OLDSAL_NO")
                    txtReceivedTotal.Text = dt.Rows(0)("SAL_RECVDTOTAL")
                    txtBalance.Text = dt.Rows(0)("SAL_BALANCE")
                    txtCreditno.Text = dt.Rows(0)("SAL_CREDITNO")
                    ddCreditcard.SelectedValue = dt.Rows(0)("SAL_CREDITCARD")

                    txtSALDate.Text = dt.Rows(0)("SAL_DATE_STR")
                    txtSALNo.Text = dt.Rows(0)("SAL_NO")
                    txtStuNo.Text = dt.Rows(0)("SAL_CUS_NO")
                    txtStuName.Text = dt.Rows(0)("SAL_CUS_NAME")
                    txtGrade.Text = dt.Rows(0)("SAL_GRADE")
                    txtSection.Text = dt.Rows(0)("SAL_SECTION")
                    hTPT_Client_ID.Value = dt.Rows(0)("SAL_CUS_ID")
                    'txtTotal.Text = dt.Rows(0)("SAL_TOTAL")
                    lblSubTotal.Text = Format(CDbl(dt.Rows(0)("SAL_TOTAL")), "#,##0.00")
                    ddlVATCode.SelectedValue = dt.Rows(0)("SAL_TAX_CODE").ToString
                    lblTotalVATAmount.Text = Format(CDbl(dt.Rows(0)("SAL_TAX_AMOUNT")), "#,##0.00")
                    lblTotalNETAmount.Text = Format(CDbl(dt.Rows(0)("SAL_NET_AMOUNT")), "#,##0.00")
                    ddlBSU.SelectedValue = dt.Rows(0)("SAL_BSU_ID")
                    h_SAL_Type.Value = dt.Rows(0)("SAL_TYPE")
                    txtCrCardCharges.Text = dt.Rows(0)("SAL_CREDITCARD_CHARGE")
                    chkbStationary.Checked = CBool(dt.Rows(0)("SAL_bSTATIONARY"))
                    'chkbStationary_CheckedChanged(Nothing, Nothing)

                    'dt.Rows(0)("PRF_STATUS") N-NEW, S-SENT FOR APPROVAL, D-DELETED, R-REJECTED, B-BUYER, Q-QUOTATION, P-WAITING FOR PRINT, C-COMPLETE
                    btnPrint.Visible = Not btnSave.Visible And dt.Rows(0)("SAL_DELETED") = 0
                    btnEdit.Visible = btnEdit.Visible And ViewState("MainMnu_code") = "PI02013" And dt.Rows(0)("SAL_POSTED") = 0 And dt.Rows(0)("SAL_DELETED") = 0
                    btnDelete.Visible = btnDelete.Visible And ViewState("MainMnu_code") = "PI02013" And dt.Rows(0)("SAL_POSTED") = 0 And dt.Rows(0)("SAL_DELETED") = 0 And txtOldSalNo.Text = ""
                    btnAdd.Visible = btnAdd.Visible
                    'with buyer for quotation
                    If isReturn Then 'if sales return, the amount will be in cash.Added by Jacob on 09/Jan/2018
                        If IsNumeric(txtCCTotal.Text) AndAlso CDbl(txtCCTotal.Text) <> 0 Then
                            txtCashTotal.Text = txtCCTotal.Text
                            txtCCTotal.Text = 0
                        End If
                    End If
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
            isSales = (h_SAL_Type.Value = "S")
            lblSALNo.Visible = (ViewState("MainMnu_code") = "PI02011") 'sales return
            txtOldSalNo.Visible = (ViewState("MainMnu_code") = "PI02011") 'sales return
            'trCreditCard.Visible = isSales
            'trCash.Visible = isSales
            txtCashTotal.Enabled = isSales
            txtCCTotal.Enabled = isSales
            txtCrCardCharges.Enabled = isSales

            trStudent.Visible = Not (ViewState("MainMnu_code") = "PI02015") 'stock adjustment
            lblBalance.Visible = isSales
            txtBalance.Visible = isSales
            If ViewState("MainMnu_code") = "PI02013" Then
                If isSales Then
                    pgTitle.Text = "Retail Sales"
                Else
                    pgTitle.Text = "Retail Sales Return"
                End If
            End If
            Dim strSQL As String
            strSQL = "select TOP 1 trm_acd_id from ( "
            strSQL &= "select trm_acd_id, 0 pref from oasis.dbo.TERM_MASTER where TRM_BSU_ID='" & Session("sBsuid") & "' and getdate() between TRM_STARTDATE and TRM_ENDDATE "
            strSQL &= "UNION "
            strSQL &= "select top 1 trm_acd_id, 1 from oasis.dbo.TERM_MASTER where TRM_BSU_ID='" & Session("sBsuid") & "' and trm_startdate>getdate() order by trm_acd_id) a order by pref "
            h_acd_id.Value = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, strSQL)
            FormatFigures()
            updateWorkflow()

        Catch ex As Exception
            Errorlog(ex.Message)
            If Not ex.Message.Contains("Thread was being aborted") Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "insert into [OASIS].[dbo].[COM_EMAIL_ALERTS] (log_type, log_stu_id, log_fromemail, log_toemail, log_subject, log_message, log_date, log_username, log_password, log_host, log_port) SELECT 'ACU', 17973, 'communication@gemsedu.com', 'sandip.kapil@gemseducation.com', 'Error in Purchase load', 'BSU:" & Session("sBsuid") & ", User:" & Session("sUsr_name") & ", message:" & ex.Message.Replace("'", "`") & "', getdate(),'communication', 'gems123', '172.16.3.10', 25 ")
            End If
        End Try
    End Sub

    Private Sub FormatFigures()
        On Error Resume Next
        'txtOthers.Text = Convert.ToDouble(txtOthers.Text).ToString("####0.00")
        'txtVAT.Text = Convert.ToDouble(txtVAT.Text).ToString("####0.00")
        txtTotal.Text = Convert.ToDouble(txtTotal.Text).ToString("#,###,##0.00")
        txtDue.Text = txtTotal.Text
        txtBalance.Text = txtReceivedTotal.Text - txtDue.Text - txtCrCardCharges.Text
        If txtBalance.Text < 0 Then txtBalance.Text = "0.0"
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        h_delete.Value = "0"
        h_total.Value = "0"
        h_empid.Value = Session("EmployeeId")
        hTPT_Client_ID.Value = ""

        txtSALNo.Text = "NEW"
        txtSALDate.Text = Now.ToString("dd/MMM/yyyy")
        txtStuNo.Text = "00000000000000"
        hTPT_Client_ID.Value = "0"
        txtStuName.Text = "None"
        txtGrade.Text = "00"
        txtSection.Text = "00"

        txtRemarks.Text = ""
        txtCashTotal.Text = "0.0"
        txtDue.Text = "0.0"
        txtCCTotal.Text = "0.0"
        txtReceivedTotal.Text = "0.0"
        txtBalance.Text = "0.0"
        txtCreditno.Text = ""
        ddCreditcard.SelectedIndex = 0
        Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S", ConnectionManger.GetOASISFINConnectionString)
        If Not ddCreditcard.Items.FindByValue(str_CRR_ID) Is Nothing Then
            ddCreditcard.ClearSelection()
            ddCreditcard.Items.FindByValue(str_CRR_ID).Selected = True
        End If
        btnInvoice.Visible = False
        txtTotal.Text = "0.0"
        lblSubTotal.Text = "0.00"
        lblTotalVATAmount.Text = "0.00"
        lblTotalNETAmount.Text = "0.00"
    End Sub

    Private Property SALFooter() As DataTable
        Get
            Return ViewState("SALFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SALFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Property isSales() As Boolean
        Get
            Return ViewState("isSales")
        End Get
        Set(ByVal value As Boolean)
            ViewState("isSales") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub fillGridView(ByRef fillDataTable As DataTable, ByRef fillGrdView As GridView, ByVal fillSQL As String)
        fillDataTable = MainObj.getRecords(fillSQL, "OASIS_PUR_INVConnectionString")
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(fillDataTable)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        fillDataTable = mtable
        fillGrdView.DataSource = fillDataTable
        fillGrdView.DataBind()
    End Sub

    Private Sub showNoRecordsFound()
        If Not SALFooter Is Nothing AndAlso SALFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdSAL.Columns.Count - 2
            grdSAL.Rows(0).Cells.Clear()
            grdSAL.Rows(0).Cells.Add(New TableCell())
            grdSAL.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdSAL.Rows(0).Cells(0).Text = "No Record Found"
            chkbStationary.Enabled = True
        Else
            chkbStationary.Enabled = False
        End If
    End Sub
    Private Sub CALCULATE_TAX(ByVal Amount As Double, ByRef lblTaxAmt As Label, ByRef lblNETAmt As Label)
        lblTaxAmt.Text = "0.00"
        lblNETAmt.Text = "0.00"
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & Amount & ",'" & ddlVATCode.SelectedValue & "')")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTaxAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                lblNETAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub
    Protected Sub grdSAL_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdSAL.RowCancelingEdit
        grdSAL.EditIndex = -1
        grdSAL.DataSource = SALFooter
        grdSAL.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdSAL_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSAL.RowCommand
        Dim errors As String = ""
        If e.CommandName = "AddNew" Then
            If trVATCode.Visible AndAlso ddlVATCode.SelectedValue = "NA" Then
                'lblError.Text = "Please select a TAX code and continue"
                'usrMessageBar2.ShowNotification("Please select a TAX code and continue", UserControls_usrMessageBar.WarningType.Danger)
                errors = "Please select a TAX code and continue"
                Exit Sub
            End If
            Dim hdnITM_ID As HiddenField = grdSAL.FooterRow.FindControl("hdnITM_ID")
            Dim txtSAD_DESCR As TextBox = grdSAL.FooterRow.FindControl("txtSAD_DESCR")
            Dim txtSAD_DETAILS As TextBox = grdSAL.FooterRow.FindControl("txtSAD_DETAILS")
            Dim txtSAD_COST As TextBox = grdSAL.FooterRow.FindControl("txtSAD_COST")
            Dim txtSAD_QTY As TextBox = grdSAL.FooterRow.FindControl("txtSAD_QTY")
            Dim txtSAD_RATE As TextBox = grdSAL.FooterRow.FindControl("txtSAD_RATE")
            Dim txtSAD_TOTAL As TextBox = grdSAL.FooterRow.FindControl("txtSAD_TOTAL")
            If txtSAD_TOTAL.Text.Length = 0 Then txtSAD_TOTAL.Text = "0"
            If txtSAD_TOTAL.Visible Then txtSAD_TOTAL.Text = Convert.ToDouble(txtSAD_TOTAL.Text).ToString("#,###,##0.00")

            'lblError.Text = ""
            If hdnITM_ID.Value.Trim.Length = 0 Then
                'lblError.Text &= "Item"
                'usrMessageBar2.ShowNotification("Item", UserControls_usrMessageBar.WarningType.Danger)
                errors &= "Item"
            Else
                If isSales Then
                    txtSAD_DESCR.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT isnull(ITB_DESCR,ITM_DESCR) ITM_DESCR FROM ITEM_SALE WITH (NOLOCK) INNER JOIN ITEM_SALEBSU  WITH (NOLOCK) ON ITM_ID=ITB_ITM_ID WHERE itb_bsu_id='" & Session("sBSUID") & "' and itm_id=" & hdnITM_ID.Value)
                Else

                End If
            End If
            'End If
            'If Val(txtSAD_RATE.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Rate"
            'If Val(txtSAD_QTY.Text) = 0 Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Quantity"
            If Val(txtSAD_RATE.Text) = 0 Then errors &= IIf(errors.Length = 0, "", ",") & "Rate"
            If Val(txtSAD_QTY.Text) = 0 Then errors &= IIf(errors.Length = 0, "", ",") & "Quantity"

            If errors.Length > 0 Then
                'lblError.Text &= " Mandatory!!!!"
                errors &= " Mandatory!!!!"
                usrMessageBar2.ShowNotification(errors, UserControls_usrMessageBar.WarningType.Danger)
                showNoRecordsFound()
                Exit Sub
            End If

            If SALFooter.Rows(0)(1) = -1 Then SALFooter.Rows.RemoveAt(0)
            If hdnITM_ID.Value = "" Then hdnITM_ID.Value = "0"
            If hdnITM_ID.Value < 0 Then
                Dim dt As DataTable
                dt = MainObj.getRecords("select SED_ITM_ID, ITB_SELL, isnull(ITB_DESCR,ITM_DESCR) ITM_DESCR, ITM_ISBN, SED_QTY from SET_H WITH (NOLOCK) inner JOIN SET_D WITH (NOLOCK) on SEH_ID=SED_SEH_ID INNER JOIN ITEM_SALEBSU WITH (NOLOCK) on SED_ITM_ID=ITB_ITM_ID AND ITB_BSU_ID=SEH_BSU_ID inner JOIN ITEM_SALE WITH (NOLOCK) on ITM_ID=ITB_ITM_ID INNER JOIN SAL_C WITH (NOLOCK) on ITB_BSU_ID=SAC_BSU_ID AND ITM_ID=SAC_ITM_ID AND SAC_QTY>=SED_QTY WHERE SEH_ID=" & -1 * hdnITM_ID.Value, "OASIS_PUR_INVConnectionString")
                If dt.Rows.Count > 0 Then
                    For rowCount As Integer = 0 To dt.Rows.Count - 1
                        Dim mrow As DataRow
                        mrow = SALFooter.NewRow
                        mrow("SAD_ID") = 0
                        mrow("SAD_QTY") = txtSAD_QTY.Text * dt.Rows(rowCount)("SED_QTY")
                        mrow("SAD_COST") = 0
                        mrow("SAD_ITM_ID") = dt.Rows(rowCount)("SED_ITM_ID")
                        mrow("SAD_DESCR") = dt.Rows(rowCount)("ITM_DESCR")
                        mrow("SAD_DETAILS") = dt.Rows(rowCount)("ITM_ISBN")
                        mrow("SAD_RATE") = dt.Rows(rowCount)("ITB_SELL")
                        mrow("SAD_TOTAL") = dt.Rows(rowCount)("ITB_SELL") * txtSAD_QTY.Text * dt.Rows(rowCount)("SED_QTY")
                        Dim dblPrice As Double = 0, lblTaxAmt As New Label, lblNetAmt As New Label
                        dblPrice = CDbl(mrow("SAD_TOTAL"))
                        CALCULATE_TAX(dblPrice, lblTaxAmt, lblNetAmt)
                        mrow("SAD_TAX_AMOUNT") = lblTaxAmt.Text
                        mrow("SAD_NET_AMOUNT") = lblNetAmt.Text
                        SALFooter.Rows.Add(mrow)
                    Next
                End If
            Else
                Dim mrow As DataRow
                mrow = SALFooter.NewRow
                mrow("SAD_ID") = 0
                mrow("SAD_QTY") = txtSAD_QTY.Text
                mrow("SAD_COST") = IIf(txtSAD_COST.Text = "", 0, txtSAD_COST.Text)
                mrow("SAD_ITM_ID") = hdnITM_ID.Value
                mrow("SAD_DESCR") = txtSAD_DESCR.Text
                mrow("SAD_DETAILS") = txtSAD_DETAILS.Text
                mrow("SAD_RATE") = txtSAD_RATE.Text
                mrow("SAD_TOTAL") = txtSAD_TOTAL.Text
                Dim dblPrice As Double = 0, lblTaxAmt As New Label, lblNetAmt As New Label
                dblPrice = CDbl(mrow("SAD_TOTAL"))
                CALCULATE_TAX(dblPrice, lblTaxAmt, lblNetAmt)
                mrow("SAD_TAX_AMOUNT") = lblTaxAmt.Text
                mrow("SAD_NET_AMOUNT") = lblNetAmt.Text
                SALFooter.Rows.Add(mrow)
            End If
            txtTotal.Text = Convert.ToDouble(SALFooter.Compute("sum(SAD_TOTAL)", "")).ToString("#,###,##0.00")
            'lblSubTotal.Text = Format(CDbl(txtTotal.Text), "#,##0.00")
            'CALCULATE_TAX(CDbl(lblSubTotal.Text))
            'txtDue.Text = CDbl(lblTotalNETAmount.Text)
            'txtBalance.Text = txtReceivedTotal.Text - txtDue.Text
            'If txtBalance.Text < 0 Then txtBalance.Text = "0.0"
            SET_TAX_TOTAL()

            h_total.Value = txtTotal.Text


            'grdSAL.Columns.Clear()

            grdSAL.EditIndex = -1
            grdSAL.DataSource = SALFooter
            grdSAL.DataBind()
            showNoRecordsFound()

            imgClient.Visible = False
        End If
    End Sub

    Public Function GET_ITMITEMSALE() As DataSet
        Dim conn As SqlConnection = New SqlConnection()
        Try

            conn.ConnectionString = ConnectionManger.GetOASIS_PUR_INVConnectionString
            conn.Open()
            Dim pParms(5) As System.Data.SqlClient.SqlParameter
            pParms(0) = New System.Data.SqlClient.SqlParameter("@ID", ddlBSU.SelectedValue)
            pParms(1) = New System.Data.SqlClient.SqlParameter("@BSU_ID", ddlBSU.SelectedValue)
            pParms(2) = New System.Data.SqlClient.SqlParameter("@bSTATIONARY", chkbStationary.Checked)
            pParms(3) = New System.Data.SqlClient.SqlParameter("@SALETYPE", h_SAL_Type.Value)
            pParms(4) = New System.Data.SqlClient.SqlParameter("@DESC", h_SAL_Type.Value)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, "[DBO].[GET_ITEM_FOR_SALE_SEARCH]", pParms)

            Return ds
        Catch ex As Exception
        Finally
            conn.Close()

        End Try

    End Function
    Protected Sub hf_ITM_ID_ValueChanged(sender As Object, e As EventArgs) Handles hf_ITM_ID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value


    End Sub
    <WebMethod()> _
    Public Shared Function GetItems(ByVal prefix As String, ByVal BSUID As String) As String()
        Dim company As New List(Of String)()
        Dim StrSQL As String = ""
        'Dim BSU_ID As String = "125010" 'Session("sBsuid")
        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandType = CommandType.StoredProcedure

                'cmd.CommandText = "SELECT COMP_ID,COMP_NAME FROM dbo.COMP_LISTED_M WITH(NOLOCK) WHERE ISNULL(COMP_bACTIVE,0)=1 AND ISNULL(COMP_COUNTRY,'')='" & HttpContext.Current.Session("BSU_COUNTRY_ID") & "' AND " & "COMP_NAME like @SearchText + '%'"
                'cmd.Parameters.AddWithValue("@SearchText", prefix)

                StrSQL = "select top 10 ID, ISBN, DESCRIPTION, SELL, COST xCOST, '' xLAST from ("
                StrSQL &= "SELECT ITM_ID ID, ITM_ISBN ISBN, isnull(ITB_DESCR,ITM_DESCR)+' ' DESCRIPTION, cast(cast(ITB_SELL as decimal(12,3)) as varchar) SELL, cast(cast(ITB_COST as decimal(12,3)) as varchar) COST from ITEM_SALE WITH (NOLOCK) inner JOIN ITEM_SALEBSU WITH (NOLOCK) on ITM_ID=ITB_ITM_ID "
                StrSQL &= "INNER JOIN SAL_C WITH (NOLOCK) on ITB_BSU_ID=SAC_BSU_ID AND ITM_ID=SAC_ITM_ID AND SAC_QTY>0 "
                StrSQL &= "WHERE ITB_BSU_ID='" & BSUID & "' and (ITM_ISBN='" & prefix & "' or isnull(ITB_DESCR,ITM_DESCR) like '%" & prefix & "%') "
                StrSQL &= "UNION ALL "
                StrSQL &= "select -SEH_ID,'',SEH_DESCR, sum(ITB_SELL*SED_QTY), sum(ITB_COST*SED_QTY) "
                StrSQL &= "from SET_H WITH (NOLOCK) inner JOIN SET_D WITH (NOLOCK) on SEH_ID=SED_SEH_ID INNER JOIN ITEM_SALE WITH (NOLOCK) on ITM_ID=SED_ITM_ID inner JOIN ITEM_SALEBSU WITH (NOLOCK) on ITM_ID=ITB_ITM_ID AND ITB_BSU_ID=SEH_BSU_ID "
                StrSQL &= "INNER JOIN SAL_C WITH (NOLOCK) on ITB_BSU_ID=SAC_BSU_ID AND ITM_ID=SAC_ITM_ID AND SAC_QTY>=SED_QTY "
                StrSQL &= "where SEH_BSU_ID='" & BSUID & "' and (SEH_ISBN='" & prefix & "' or SEH_DESCR like '%" & prefix & "%') "
                StrSQL &= "group by SEH_ID, SEH_DESCR"
                StrSQL &= ") a where 1=1 order by DESCRIPTION"
                cmd.CommandText = StrSQL

                'cmd.CommandText = "dbo.GET_ITEM_FOR_SALE_SEARCH"
                'cmd.Parameters.Add("@BSU_ID", ddlBSU.SelectedValue, SqlDbType.VarChar)
                'cmd.Parameters.Add("@bSTATIONARY", chkbStationary.Checked, SqlDbType.Bit)
                'cmd.Parameters.Add("@SALETYPE", h_SAL_Type.Value, SqlDbType.VarChar)
                'cmd.Parameters.Add("@DESC", prefix, SqlDbType.VarChar)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        company.Add(String.Format("{0}-{1}", sdr("DESCRIPTION").ToString.Replace("-", " "), sdr("ID")))
                    End While
                End Using
                conn.Close()
            End Using
            'Dim k As DataSet = GET_ITMITEMSALE(conn)
            Return company.ToArray()
        End Using
    End Function
    Protected Sub grdSAL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSAL.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim Actualcolor As Color
            If e.Row.RowIndex = 0 Then
                Actualcolor = e.Row.Cells(0).ForeColor
            End If
            Dim imgClient As ImageButton = e.Row.FindControl("imgClient")
            Dim txtSAD_DESCR As TextBox = e.Row.FindControl("txtSAD_DESCR")
            Dim txtSAD_RATE As TextBox = e.Row.FindControl("txtSAD_RATE")
            Dim txtSAD_COST As TextBox = e.Row.FindControl("txtSAD_COST")
            Dim txtSAD_QTY As TextBox = e.Row.FindControl("txtSAD_QTY")
            Dim txtSAD_TOTAL As TextBox = e.Row.FindControl("txtSAD_TOTAL")
            Dim hdnITM_ID As HiddenField = e.Row.FindControl("hdnITM_ID")
            Dim txtSAD_DETAILS As TextBox = e.Row.FindControl("txtSAD_DETAILS")
            Dim lblSAD_TAX_AMT As Label = e.Row.FindControl("lblSAD_TAX_AMT")
            Dim lblSAD_NET_AMT As Label = e.Row.FindControl("lblSAD_NET_AMT")

            If Not txtSAD_DESCR Is Nothing Then
                txtSAD_COST.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
                'txtSAD_QTY.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
                txtSAD_RATE.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
                imgClient.Attributes.Add("onclick", "javascript: getItemName('" & hdnITM_ID.ClientID & "','" & txtSAD_DESCR.ClientID & "','" & txtSAD_DETAILS.ClientID & "','" & txtSAD_COST.ClientID & "','" & txtSAD_QTY.ClientID & "','" & txtSAD_RATE.ClientID & "','" & txtSAD_TOTAL.ClientID & "'); return false;")
                Dim acBSUFooter As AjaxControlToolkit.AutoCompleteExtender
                acBSUFooter = TryCast(e.Row.FindControl("acBSU"), AjaxControlToolkit.AutoCompleteExtender)
                acBSUFooter.ContextKey = ddlBSU.SelectedValue
                If isSales Then
                    'imgClient.Attributes.Add("onclick", "javascript:return getItemName('" & hdnITM_ID.ClientID & "','" & txtSAD_DESCR.ClientID & "','" & txtSAD_QTY.ClientID & "','" & txtSAD_RATE.ClientID & "','" & txtSAD_TOTAL.ClientID & "');")
                    'imgClient.Visible = True
                Else
                    'imgClient.Visible = False
                End If
                'txtSupplier, txtClient, txtRate, txtQty, txtTotal, hdnClient
                txtSAD_QTY.Attributes.Add("OnKeyUp", "javascript:return calculateItem('" & txtSAD_QTY.ClientID & "','" & txtSAD_RATE.ClientID & "','" & txtSAD_TOTAL.ClientID & "');")
                txtSAD_RATE.Attributes.Add("OnKeyUp", "javascript:return calculateItem('" & txtSAD_QTY.ClientID & "','" & txtSAD_RATE.ClientID & "','" & txtSAD_TOTAL.ClientID & "');")


            End If
            If chkbStationary.Checked Then
                e.Row.Cells(2).ForeColor = Drawing.Color.Purple
                e.Row.Cells(3).ForeColor = Drawing.Color.Purple
            Else
                e.Row.Cells(2).ForeColor = Actualcolor
                e.Row.Cells(3).ForeColor = Actualcolor
            End If
            'If Not txtSAD_TOTAL Is Nothing AndAlso Not lblSAD_NET_AMT Is Nothing AndAlso Not lblSAD_TAX_AMT Is Nothing Then
            '    CALCULATE_TAX(txtSAD_TOTAL.Text, lblSAD_TAX_AMT, lblSAD_NET_AMT)
            'End If
        End If
    End Sub

    Protected Sub grdSAL_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdSAL.RowDeleting
        Dim mRow() As DataRow = SALFooter.Select("ID=" & grdSAL.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_SALGridDelete.Value &= ";" & mRow(0)("SAD_ID")
            SALFooter.Select("ID=" & grdSAL.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            SALFooter.AcceptChanges()
        End If
        If SALFooter.Rows.Count = 0 Then
            SALFooter.Rows.Add(SALFooter.NewRow())
            SALFooter.Rows(0)(1) = -1
        End If
        Try
            txtTotal.Text = Convert.ToDouble(SALFooter.Compute("sum(SAD_TOTAL)", "")).ToString("#,###,##0.00")
            'lblSubTotal.Text = Format(CDbl(txtTotal.Text), "#,##0.00")
            'CALCULATE_TAX(CDbl(lblSubTotal.Text))
            'txtDue.Text = CDbl(lblTotalNETAmount.Text)
            'txtBalance.Text = txtReceivedTotal.Text - txtDue.Text
            'If txtBalance.Text < 0 Then txtBalance.Text = "0.0"
            SET_TAX_TOTAL()
        Catch ex As Exception
            txtStuNo.Enabled = True And ViewState("datamode") = "add" 'if the last item is deleted
            imgClient.Visible = True And ViewState("datamode") = "add"
        End Try

        h_total.Value = txtTotal.Text
        grdSAL.DataSource = SALFooter
        grdSAL.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdSAL_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdSAL.RowEditing
        Try
            grdSAL.EditIndex = e.NewEditIndex
            grdSAL.DataSource = SALFooter
            grdSAL.DataBind()
            showNoRecordsFound()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdSAL_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdSAL.RowUpdating
        Dim s As String = grdSAL.DataKeys(e.RowIndex).Value.ToString()
        Dim hdnITM_ID As HiddenField = grdSAL.Rows(e.RowIndex).FindControl("hdnITM_ID")
        Dim lblSAD_ID As Label = grdSAL.Rows(e.RowIndex).FindControl("lblSAD_ID")
        Dim txtSAD_DESCR As TextBox = grdSAL.Rows(e.RowIndex).FindControl("txtSAD_DESCR")
        Dim txtSAD_DETAILS As TextBox = grdSAL.Rows(e.RowIndex).FindControl("txtSAD_DETAILS")
        Dim txtSAD_COST As TextBox = grdSAL.Rows(e.RowIndex).FindControl("txtSAD_COST")
        Dim txtSAD_QTY As TextBox = grdSAL.Rows(e.RowIndex).FindControl("txtSAD_QTY")
        Dim txtSAD_RATE As TextBox = grdSAL.Rows(e.RowIndex).FindControl("txtSAD_RATE")
        Dim txtSAD_TOTAL As TextBox = grdSAL.Rows(e.RowIndex).FindControl("txtSAD_TOTAL")
        Dim lblSAD_TAX_AMT As Label = grdSAL.Rows(e.RowIndex).FindControl("lblSAD_TAX_AMT")
        Dim lblSAD_NET_AMT As Label = grdSAL.Rows(e.RowIndex).FindControl("lblSAD_NET_AMT")
        If txtSAD_TOTAL.Visible Then txtSAD_TOTAL.Text = Convert.ToDouble(txtSAD_TOTAL.Text).ToString("#,###,##0.00")

        CALCULATE_TAX(txtSAD_TOTAL.Text, lblSAD_TAX_AMT, lblSAD_NET_AMT)
        Dim mrow As DataRow
        mrow = SALFooter.Select("ID=" & s)(0)
        mrow("SAD_ITM_ID") = hdnITM_ID.Value
        mrow("SAD_DESCR") = txtSAD_DESCR.Text
        mrow("SAD_RATE") = txtSAD_RATE.Text
        mrow("SAD_TOTAL") = txtSAD_TOTAL.Text
        mrow("SAD_DETAILS") = txtSAD_DETAILS.Text
        mrow("SAD_QTY") = txtSAD_QTY.Text
        mrow("SAD_COST") = IIf(txtSAD_COST.Text = "", 0, txtSAD_COST.Text)
        mrow("SAD_TAX_AMOUNT") = lblSAD_TAX_AMT.Text
        mrow("SAD_NET_AMOUNT") = lblSAD_NET_AMT.Text
        txtTotal.Text = Convert.ToDouble(SALFooter.Compute("sum(SAD_TOTAL)", "")).ToString("#,###,##0.00")
        txtDue.Text = txtTotal.Text
        txtBalance.Text = txtReceivedTotal.Text - txtDue.Text
        If txtBalance.Text < 0 Then txtBalance.Text = "0.0"

        grdSAL.EditIndex = -1
        grdSAL.DataSource = SALFooter
        grdSAL.DataBind()
        showNoRecordsFound()
        SET_TAX_TOTAL()
        txtStuNo.Enabled = False
        imgClient.Visible = False

    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetPrdDescr(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim StrSQL As String = ""
        If contextKey Is Nothing Then contextKey = ""
        If contextKey = "none" Then 'non framework

        ElseIf contextKey = "" Then 'framework

        Else
            StrSQL = "select top 10 ID, ISBN, DESCRIPTION, SELL, COST xCOST, '' xLAST from ("
            StrSQL &= "SELECT ITM_ID ID, ITM_ISBN ISBN, isnull(ITB_DESCR,ITM_DESCR)+' ' DESCRIPTION, cast(cast(ITB_SELL as decimal(12,3)) as varchar) SELL, cast(cast(ITB_COST as decimal(12,3)) as varchar) COST from ITEM_SALE WITH (NOLOCK) inner JOIN ITEM_SALEBSU WITH (NOLOCK) on ITM_ID=ITB_ITM_ID "
            StrSQL &= "INNER JOIN SAL_C WITH (NOLOCK) on ITB_BSU_ID=SAC_BSU_ID AND ITM_ID=SAC_ITM_ID AND SAC_QTY>0 "
            StrSQL &= "WHERE ITB_BSU_ID='" & contextKey & "' and (ITM_ISBN='" & prefixText & "' or isnull(ITB_DESCR,ITM_DESCR) like '%" & prefixText & "%') "
            StrSQL &= "UNION ALL "
            StrSQL &= "select -SEH_ID,'',SEH_DESCR, sum(ITB_SELL*SED_QTY), sum(ITB_COST*SED_QTY) "
            StrSQL &= "from SET_H WITH (NOLOCK) inner JOIN SET_D WITH (NOLOCK) on SEH_ID=SED_SEH_ID INNER JOIN ITEM_SALE WITH (NOLOCK) on ITM_ID=SED_ITM_ID inner JOIN ITEM_SALEBSU WITH (NOLOCK) on ITM_ID=ITB_ITM_ID AND ITB_BSU_ID=SEH_BSU_ID "
            StrSQL &= "INNER JOIN SAL_C WITH (NOLOCK) on ITB_BSU_ID=SAC_BSU_ID AND ITM_ID=SAC_ITM_ID AND SAC_QTY>=SED_QTY "
            StrSQL &= "where SEH_BSU_ID='" & contextKey & "' and (SEH_ISBN='" & prefixText & "' or SEH_DESCR like '%" & prefixText & "%') "
            StrSQL &= "group by SEH_ID, SEH_DESCR"
            StrSQL &= ") a where 1=1 order by DESCRIPTION"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("xLAST").ToString & "|" & ds.Tables(0).Rows(i2)("xCOST").ToString & "|" & ds.Tables(0).Rows(i2)("ID").ToString & "|" & ds.Tables(0).Rows(i2)("SELL").ToString & "|" & ds.Tables(0).Rows(i2)("ISBN").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
        Next
        Return items.ToArray()
    End Function

    Private Sub updateWorkflow()
        Dim sqlStr As New StringBuilder
        sqlStr.Append("SELECT SAD_ID, SAD_ITM_ID, SAD_DESCR, SAD_DETAILS, SAD_COST,cast(SAD_QTY as float) SAD_QTY, ")
        sqlStr.Append("CAST(SAD_RATE as float) AS SAD_RATE, CAST(SAD_QTY*SAD_RATE as float) SAD_TOTAL,ISNULL(SAD_TAX_AMOUNT,0) AS SAD_TAX_AMOUNT, ")
        sqlStr.Append("CAST(SAD_QTY * SAD_RATE as float) + ISNULL(SAD_TAX_AMOUNT,0) AS SAD_NET_AMOUNT ")
        sqlStr.Append("FROM SAL_D LEFT outer JOIN ITEM_SALE ON ITM_ID=SAD_ITM_ID where SAD_SAL_ID=" & h_EntryId.Value & " ORDER BY SAD_ID")
        fillGridView(SALFooter, grdSAL, sqlStr.ToString)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim quoteEdit As String = "N"
        Dim errors As String = ""
        'lblError.Text = ""
        If hTPT_Client_ID.Value.Trim.Length = 0 AndAlso (isSales) Then errors &= "Supplier Name,"
        If txtSALDate.Text.Trim.Length = 0 Then errors &= "Date,"
        If errors.Length > 0 Then errors = errors.Substring(0, errors.Length - 1) & " Mandatory"
        If h_SAL_Type.Value = "S" Then If Val(txtReceivedTotal.Text) < Val(txtDue.Text) Then errors &= IIf(errors.Length = 0, "", ",") & "Payment details missing"
        If grdSAL.Rows.Count = 0 Or (CType(lblSubTotal.Text, Decimal) = 0 And (isSales)) Then errors &= IIf(errors.Length = 0, "", ",") & "Transaction should contain atleast 1 item"
        If Val(txtCCTotal.Text) > 0 And txtCreditno.Text.Length = 0 And h_SAL_Type.Value <> "R" Then
            errors &= IIf(errors.Length = 0, "", ",") & "Credit card Auth No missing"
        End If

        If h_SAL_Type.Value = "R" Then If txtOldSalNo.Text = "" Then errors &= IIf(errors.Length = 0, "", ",") & "Sales Number reference missing"
        'If Not Session("F_YEAR").ToString.Contains("2013") Then lblError.Text &= IIf(lblError.Text.Length = 0, "", ",") & "Invalid Year"

        If h_SAL_Type.Value = "S" And (FeeCollection.GetDoubleVal(txtCashTotal.Text) + FeeCollection.GetDoubleVal(txtCCTotal.Text) + FeeCollection.GetDoubleVal(txtCrCardCharges.Text) <> FeeCollection.GetDoubleVal(lblTotalNETAmount.Text)) Then
            errors &= IIf(errors.Length = 0, "", ",") & "There is a mismatch in payable amount and paid amount"
        End If


        If errors.Length > 0 Then
            usrMessageBar2.ShowNotification(errors, UserControls_usrMessageBar.WarningType.Danger)
            showNoRecordsFound()
            Exit Sub
        End If
        If txtSALNo.Text = "NEW" Then
            h_EntryId.Value = "0"
        End If
        '@SAL_ID int output, @SAL_NO varchar(20)=null,@SAL_BSU_ID varchar(10)=null,@SAL_DATE smalldatetime, @SAL_TYPE varchar=null,   
        '@SAL_USR_NAME varchar(20), @SAL_CUS_ID varchar(20)=null, @SAL_CUS_NO varchar(10)=null, @SAL_GRADE varchar(10), @SAL_SECTION varchar(10), 
        '@SAL_FYEAR varchar(4)='2012', @SAL_NARRATION varchar(500),@SAL_TOTAL numeric(12,3)=null, @SAL_REMARKS varchar(max)=null
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(27) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@SAL_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@SAL_NO", txtSALNo.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@SAL_BSU_ID", ddlBSU.SelectedValue, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@SAL_DATE", txtSALDate.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@SAL_NARRATION", txtRemarks.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@SAL_CASHTOTAL", txtCashTotal.Text, SqlDbType.Decimal)
        pParms(7) = Mainclass.CreateSqlParameter("@SAL_CCTOTAL", txtCCTotal.Text, SqlDbType.Decimal)
        pParms(8) = Mainclass.CreateSqlParameter("@SAL_RECVDTOTAL", txtReceivedTotal.Text, SqlDbType.Decimal)
        pParms(9) = Mainclass.CreateSqlParameter("@SAL_BALANCE", txtBalance.Text, SqlDbType.Decimal)
        pParms(10) = Mainclass.CreateSqlParameter("@SAL_CREDITNO", txtCreditno.Text, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@SAL_CREDITCARD", ddCreditcard.SelectedValue, SqlDbType.Int)

        pParms(12) = Mainclass.CreateSqlParameter("@SAL_CUS_ID", hTPT_Client_ID.Value, SqlDbType.VarChar)
        pParms(13) = Mainclass.CreateSqlParameter("@SAL_CUS_NO", txtStuNo.Text, SqlDbType.VarChar)
        pParms(14) = Mainclass.CreateSqlParameter("@SAL_CUS_NAME", txtStuName.Text, SqlDbType.VarChar)
        pParms(15) = Mainclass.CreateSqlParameter("@SAL_GRADE", txtGrade.Text, SqlDbType.VarChar)
        pParms(16) = Mainclass.CreateSqlParameter("@SAL_SECTION", txtSection.Text, SqlDbType.VarChar)
        pParms(17) = Mainclass.CreateSqlParameter("@SAL_TOTAL", lblSubTotal.Text, SqlDbType.Decimal)
        pParms(18) = Mainclass.CreateSqlParameter("@SAL_TYPE", h_SAL_Type.Value, SqlDbType.VarChar)
        pParms(19) = Mainclass.CreateSqlParameter("@SAL_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(20) = Mainclass.CreateSqlParameter("@SAL_USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(21) = Mainclass.CreateSqlParameter("@SAL_OLDSAL_NO", txtOldSalNo.Text, SqlDbType.VarChar)
        pParms(22) = Mainclass.CreateSqlParameter("@SAL_CREDITCARD_CHARGE", txtCrCardCharges.Text, SqlDbType.Decimal)
        pParms(23) = Mainclass.CreateSqlParameter("@SAL_TAX_CODE", ddlVATCode.SelectedValue, SqlDbType.VarChar)
        pParms(24) = Mainclass.CreateSqlParameter("@SAL_TAX_AMOUNT", CDbl(lblTotalVATAmount.Text), SqlDbType.Decimal)
        pParms(25) = Mainclass.CreateSqlParameter("@SAL_NET_AMOUNT", CDbl(lblTotalNETAmount.Text), SqlDbType.Decimal)
        pParms(26) = Mainclass.CreateSqlParameter("@SAL_bSTATIONARY", chkbStationary.Checked, SqlDbType.Bit)
        'saveSAL_H 0, '', 0, '', 0, '999998', '8/oct/2012', 'sandip.kapil', '', 0, 0, '063M0122', '60 days', '8/oct/2012', 0,0,0,''
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveSAL_H", pParms)
            If RetVal = "-1" Or pParms(1).Value = 0 Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar2.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
                Session("SAL_id") = pParms(1).Value
            End If

            Dim RowCount As Integer, InsCount As Integer = 0
            For RowCount = 0 To SALFooter.Rows.Count - 1
                Dim iParms(8) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add" Or quoteEdit = "Y", 0, SALFooter.Rows(RowCount)("SAD_ID"))
                If SALFooter.Rows(RowCount).RowState = 8 Then rowState = -1
                '@SAD_ID int, @SAD_SAL_ID int,@SAD_ITM_ID int,@SAD_QTY numeric(9,2),@SAD_RATE numeric(12,3),@SAD_DETAILS varchar(max),@SAD_DESCR varchar(max)
                iParms(1) = Mainclass.CreateSqlParameter("@SAD_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@SAD_SAL_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@SAD_DESCR", SALFooter.Rows(RowCount)("SAD_DESCR"), SqlDbType.VarChar)
                iParms(4) = Mainclass.CreateSqlParameter("@SAD_ITM_ID", SALFooter.Rows(RowCount)("SAD_ITM_ID"), SqlDbType.Int)
                iParms(5) = Mainclass.CreateSqlParameter("@SAD_COST", SALFooter.Rows(RowCount)("SAD_COST"), SqlDbType.Decimal)
                iParms(6) = Mainclass.CreateSqlParameter("@SAD_QTY", SALFooter.Rows(RowCount)("SAD_QTY"), SqlDbType.Decimal)
                iParms(7) = Mainclass.CreateSqlParameter("@SAD_RATE", SALFooter.Rows(RowCount)("SAD_RATE"), SqlDbType.Decimal)
                iParms(8) = Mainclass.CreateSqlParameter("@SAD_DETAILS", SALFooter.Rows(RowCount)("SAD_DETAILS"), SqlDbType.VarChar)
                If Not IsDBNull(SALFooter.Rows(RowCount)("SAD_ITM_ID")) AndAlso SALFooter.Rows(RowCount)("SAD_ITM_ID") > 0 Then
                    InsCount += 1
                    Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveSAL_D", iParms)
                    If RetValFooter = "-1" Then
                        'lblError.Text = "Unexpected Error !!!"
                        usrMessageBar2.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                End If
            Next
            If h_SALGridDelete.Value <> "0" Then
                Dim deleteId() As String = h_SALGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    InsCount -= 1
                    iParms(1) = Mainclass.CreateSqlParameter("@SAD_ID", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@SAD_SAL_ID", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from SAL_D where SAD_ID=@SAD_ID and SAD_SAL_ID=@SAD_SAL_ID", iParms)
                    If RetValFooter = "-1" Then
                        'lblError.Text = "Unexpected Error !!!"
                        usrMessageBar2.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If
            If IsDBNull(SALFooter.Rows(0)("SAD_ITM_ID")) Then 'InsCount <= 0 
                'lblError.Text = "Transaction should contain atleast 1 item"
                usrMessageBar2.ShowNotification("Transaction should contain atleast 1 item", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                showNoRecordsFound()
                btnInvoice.Visible = False
                Exit Sub
            Else
                stTrans.Commit()
                SET_INVOICE_PRINT()
                If h_SAL_Type.Value = "S" Then
                    h_print.Value = ViewState("EntryId")
                    btnAdd_Click(sender, e)
                    'lblError.Text = "next Customer details"
                    usrMessageBar2.ShowNotification("next Customer details", UserControls_usrMessageBar.WarningType.Danger)
                ElseIf h_SAL_Type.Value = "R" Then 'if sales return
                    setModifyvalues(ViewState("EntryId"))
                    btnSave.Visible = False
                    btnPrint.Visible = True
                    'If trVATCode.Visible = True Then
                    '    btnInvoice.Visible = True
                    'End If
                    h_print.Value = ViewState("EntryId")
                Else
                    SetDataMode("view")
                    Response.Redirect(ViewState("ReferrerUrl"), False)
                End If
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        If ViewState("datamode") <> "edit" Then
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSALNo.Text, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            'lblError.Text = getErrorMessage("0")
            usrMessageBar2.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
        Else
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSALNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

            'lblError.Text = getErrorMessage("0")
            usrMessageBar2.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
        End If

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        grdSAL.Columns(10).Visible = False
        grdSAL.Columns(11).Visible = False
        grdSAL.ShowFooter = False
        ddlVATCode.Enabled = False
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnPrint.Visible = Not btnSave.Visible
        grdSAL.DataSource = SALFooter
        grdSAL.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        SetDataMode("add")
        setModifyvalues(0)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdSAL.DataSource = SALFooter
        grdSAL.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update SAL_H set SAL_DELETED=1 where SAL_ID=@SAL_ID and SAL_OLDSAL_NO='' and SAL_POSTED=0 "
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@SAL_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                'lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                usrMessageBar2.ShowNotification(ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", ""), UserControls_usrMessageBar.WarningType.Danger)
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            If h_SAL_Type.Value <> "A" Then
                h_print.Value = h_EntryId.Value
            Else
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Dim SAL_ID As Integer = CType(h_EntryId.Value, Integer)
                Dim cmd As New SqlCommand
                'writeWorkFlow(ViewState("EntryId"), "Printed", "", 0, "P")

                cmd.CommandText = "rptSALDetails"
                Dim sqlParam(1) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@SAL_ID", SAL_ID, SqlDbType.Int)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@SAL_FLAG", 0, SqlDbType.Int)

                cmd.Parameters.Add(sqlParam(1))
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure
                'V1.2 comments start------------
                Dim params As New Hashtable
                params("showHide") = "prf"
                params("userName") = Session("sUsr_name")
                If ViewState("MainMnu_code") = "PI02009" Then
                    params("reportCaption") = "Receipt"
                ElseIf ViewState("MainMnu_code") = "PI02011" Then
                    params("reportCaption") = "Refund"
                ElseIf ViewState("MainMnu_code") = "PI02015" Then
                    params("reportCaption") = "Stock Adjustment"
                Else
                    If h_SAL_Type.Value = "S" Then
                        params("reportCaption") = "Receipt"
                    Else
                        params("reportCaption") = "Refund"
                    End If
                End If
                params("@SAL_ID") = SAL_ID
                params("Menu") = ViewState("MainMnu_code")
                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params
                repSource.IncludeBSUImage = True
                repSource.ResourceName = "../../Inventory/Reports/RPT/rptSAL.rpt"
                repSource.IncludeBSUImage = True
                'End If
                Session("ReportSource") = repSource
                'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub txtStuNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStuNo.TextChanged
        lblStuCurrstatus.Text = ""
        If txtStuNo.Text.Trim.Length > 0 Then
            Dim sqlStr As New StringBuilder
            'sqlStr.Append("")
            sqlStr.Append("select top 1 CAST(STU_ID AS varchar)+';'+STU_NO+';'+ISNULL(STU_NAME,'')+';'+ISNULL(STU_GRD_ID,'')+';'+ISNULL(SCT_DESCR,'')+';'+ISNULL(STU_CURRSTATUS,'') ")
            sqlStr.Append("FROM OASIS.dbo.vw_oso_student_enquiry ")
            sqlStr.Append("WHERE STU_BSU_ID='" & ddlBSU.SelectedValue & "' AND STU_NO LIKE @search")

            Dim pParms(1) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@search", "%" & txtStuNo.Text & "%", SqlDbType.VarChar)
            Dim result As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, sqlStr.ToString, pParms)
            If result = Nothing Then
                txtStuNo.Text = "00000000000000"
                txtStuName.Text = "None"
                txtGrade.Text = "00"
                txtSection.Text = "00"
                txtStuNo.ForeColor = Color.Black
                txtStuName.ForeColor = Color.Black
                lblStuCurrstatus.Text = ""
            Else
                Dim results() As String = result.Split(";")
                txtStuNo.Text = results(1)
                txtStuName.Text = results(2)
                h_Cusid.Value = results(0)
                txtGrade.Text = results(3)
                txtSection.Text = results(4)
                If results(5).Trim = "TC" Or results(5).Trim = "SO" Then
                    txtStuNo.ForeColor = Color.Red
                    txtStuName.ForeColor = Color.Red
                    lblStuCurrstatus.Text = "Student's Current Status : " & results(5).Trim
                Else
                    txtStuNo.ForeColor = Color.Black
                    txtStuName.ForeColor = Color.Black
                    lblStuCurrstatus.Text = ""
                End If
            End If
        Else
            txtStuNo.Text = "00000000000000"
            txtStuName.Text = "None"
            txtGrade.Text = "00"
            txtSection.Text = "00"
            txtStuNo.ForeColor = Color.Black
            txtStuName.ForeColor = Color.Black
            lblStuCurrstatus.Text = ""
        End If
        showNoRecordsFound()
    End Sub

    Protected Sub txtOldSalNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOldSalNo.TextChanged
        Dim str_conn As String = connectionString
        Dim dt As New DataTable
        Dim sqlStr As New StringBuilder
        sqlStr.Append("SELECT SAL_ID, SAL_TYPE, SAL_NO, SAL_NARRATION, SAL_BSU_ID,ISNULL(SAL_CASHTOTAL, 0)SAL_CASHTOTAL, ISNULL(SAL_CCTOTAL, 0)SAL_CCTOTAL, ISNULL(SAL_RECVDTOTAL, 0)SAL_RECVDTOTAL,")
        sqlStr.Append("SAL_BALANCE, SAL_CREDITNO, SAL_CREDITCARD,REPLACE(convert(varchar(30), sal_Date, 106),' ','/') SAL_DATE_STR, SAL_CUS_ID, ")
        sqlStr.Append("SAL_CUS_NO, SAL_CUS_NAME, SAL_GRADE, SAL_SECTION, SAL_TOTAL, SAL_REMARKS, SAL_FYEAR, SAL_USR_NAME, ")
        sqlStr.Append("(SELECT COUNT(SAL_ID) FROM SAL_H WITH ( NOLOCK ) WHERE SAL_OLDSAL_NO='" & txtOldSalNo.Text & "' and ISNULL(SAL_DELETED,0)=0) CNT,ISNULL(SAL_TAX_CODE,'NA')SAL_TAX_CODE, ")
        sqlStr.Append("ISNULL(SAL_TAX_AMOUNT, 0) SAL_TAX_AMOUNT, CASE WHEN ISNULL(SAL_NET_AMOUNT, 0) = 0 THEN ( SAL_TOTAL ) + ISNULL(SAL_TAX_AMOUNT, 0) ELSE ISNULL(SAL_NET_AMOUNT, 0) END SAL_NET_AMOUNT, ")
        sqlStr.Append("ISNULL(SAL_bSTATIONARY,0)SAL_bSTATIONARY FROM SAL_H WITH ( NOLOCK ) WHERE SAL_NO = '" & txtOldSalNo.Text & "' ")
        dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)("CNT") > 10 Then 'disabled
                'lblError.Text = txtOldSalNo.Text & ", already refunded"
                usrMessageBar2.ShowNotification(txtOldSalNo.Text & ", already refunded", UserControls_usrMessageBar.WarningType.Danger)
                txtOldSalNo.Text = ""
            Else
                txtRemarks.Text = dt.Rows(0)("SAL_NARRATION")
                txtCashTotal.Text = CDbl(dt.Rows(0)("SAL_TOTAL")) + CDbl(dt.Rows(0)("SAL_TAX_AMOUNT")) 'dt.Rows(0)("SAL_CASHTOTAL")
                txtDue.Text = CDbl(dt.Rows(0)("SAL_TOTAL")) + CDbl(dt.Rows(0)("SAL_TAX_AMOUNT"))
                txtCCTotal.Text = "0.00" 'dt.Rows(0)("SAL_CCTOTAL")
                txtReceivedTotal.Text = CDbl(dt.Rows(0)("SAL_TOTAL")) + CDbl(dt.Rows(0)("SAL_TAX_AMOUNT"))
                txtBalance.Text = "0.00"
                txtCreditno.Text = ""
                ddCreditcard.SelectedIndex = 0

                'txtSALDate.Text = dt.Rows(0)("SAL_DATE_STR")
                'txtSALNo.Text = dt.Rows(0)("SAL_NO")
                txtStuNo.Text = dt.Rows(0)("SAL_CUS_NO")
                txtStuName.Text = dt.Rows(0)("SAL_CUS_NAME")
                txtGrade.Text = dt.Rows(0)("SAL_GRADE")
                txtSection.Text = dt.Rows(0)("SAL_SECTION")
                hTPT_Client_ID.Value = dt.Rows(0)("SAL_CUS_ID")
                txtTotal.Text = dt.Rows(0)("SAL_TOTAL")
                ddlBSU.SelectedValue = dt.Rows(0)("SAL_BSU_ID")
                'h_SAL_Type.Value = dt.Rows(0)("SAL_TYPE")
                ddlVATCode.SelectedValue = dt.Rows(0)("SAL_TAX_CODE").ToString
                lblSubTotal.Text = Format(CDbl(dt.Rows(0)("SAL_TOTAL")), "#,##0.00")
                lblTotalVATAmount.Text = Format(CDbl(dt.Rows(0)("SAL_TAX_AMOUNT")), "#,##0.00")
                lblTotalNETAmount.Text = Format(CDbl(dt.Rows(0)("SAL_NET_AMOUNT")), "#,##0.00")
                chkbStationary.Checked = CBool(dt.Rows(0)("SAL_bSTATIONARY"))
                'If IsNumeric(txtCCTotal.Text) AndAlso CDbl(txtCCTotal.Text) <> 0 Then 'if sales return move the amount to cash
                '    txtCashTotal.Text = txtCCTotal.Text
                '    txtCCTotal.Text = 0
                'End If

                sqlStr = New StringBuilder
                sqlStr.Append("select SAD_ID, SAD_ITM_ID, SAD_DESCR, SAD_DETAILS, 0 SAD_COST,cast(SAD_QTY as float) SAD_QTY, ")
                sqlStr.Append("cast(SAD_RATE as float) as SAD_RATE, cast(SAD_QTY*SAD_RATE as float) SAD_TOTAL, ISNULL(SAD_TAX_AMOUNT,0) AS SAD_TAX_AMOUNT, ")
                sqlStr.Append("CAST(SAD_QTY * SAD_RATE as float) + ISNULL(SAD_TAX_AMOUNT,0) AS SAD_NET_AMOUNT ")
                sqlStr.Append("from SAL_D left outer JOIN ITEM_SALE on ITM_ID=SAD_ITM_ID where SAD_SAL_ID=" & dt.Rows(0)("SAL_ID") & " and sad_itm_id not in (select sad_itm_id from sal_h inner join sal_d on sal_id=sad_sal_id where sal_oldsal_no='" & txtOldSalNo.Text & "') order by SAD_ID")
                fillGridView(SALFooter, grdSAL, sqlStr.ToString)
            End If
        Else
            'lblError.Text = txtOldSalNo.Text & " Sale no not found"
            usrMessageBar2.ShowNotification(txtOldSalNo.Text & " Sale no not found", UserControls_usrMessageBar.WarningType.Danger)
            txtOldSalNo.Text = ""
        End If
        showNoRecordsFound()
    End Sub

    Function GetAcademicYear(ByVal BSU_ID As String, ByVal Mode As String) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar)
        pParms(1).Value = Mode
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "[FEES].[F_GetAcademicYear]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Sub SET_TAX_TOTAL()
        lblSubTotal.Text = Format(CDbl(txtTotal.Text), "#,##0.00")
        CALCULATE_TAX(CDbl(lblSubTotal.Text))
        txtDue.Text = CDbl(lblTotalNETAmount.Text)
        txtBalance.Text = CDbl(lblTotalNETAmount.Text) - txtDue.Text
        If txtBalance.Text < 0 Then txtBalance.Text = "0.0"
        If (ViewState("MainMnu_code") = "PI02011") Then 'Sales Return
            txtCashTotal.Text = txtDue.Text
            txtCCTotal.Text = 0
            txtReceivedTotal.Text = txtCashTotal.Text
        End If
    End Sub
    Protected Sub ddlVATCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVATCode.SelectedIndexChanged
        For Each dr As DataRow In SALFooter.Rows
            Dim dblPrice As Double = 0, lblTaxAmt As New Label, lblNetAmt As New Label
            If IsNumeric(dr("SAD_TOTAL")) Then
                dblPrice = CDbl(dr("SAD_TOTAL"))
                CALCULATE_TAX(dblPrice, lblTaxAmt, lblNetAmt)
                dr("SAD_TAX_AMOUNT") = lblTaxAmt.Text
                dr("SAD_NET_AMOUNT") = lblNetAmt.Text
            End If
        Next
        grdSAL.DataSource = SALFooter
        grdSAL.DataBind()
        showNoRecordsFound()
        SET_TAX_TOTAL()
    End Sub

    Protected Sub btnInvoice_Click(sender As Object, e As EventArgs) Handles btnInvoice.Click

    End Sub

    Protected Sub chkbStationary_CheckedChanged(sender As Object, e As EventArgs) Handles chkbStationary.CheckedChanged
        If ddlVATCode.Items.Count > 0 Then
            If chkbStationary.Checked Then
                ddlVATCode.SelectedValue = "VAT5"
                ddlVATCode_SelectedIndexChanged(Nothing, Nothing)
                preError.Attributes.Remove("class")
                preError.Attributes.Add("class", "alert alert-info")
                preError.InnerHtml = "Please add only taxable items for this sale."
            Else
                ddlVATCode.SelectedValue = "VAT0"
                ddlVATCode_SelectedIndexChanged(Nothing, Nothing)
                preError.Attributes.Remove("class")
                preError.Attributes.Add("class", "invisible")
            End If
        End If
    End Sub

    Protected Sub hGridRefresh_ValueChanged(sender As Object, e As EventArgs) Handles hGridRefresh.ValueChanged
        txtStuNo_TextChanged(sender, e)
    End Sub
End Class
