﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Inventory_TenancyContract
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass(), requestForQuotation As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnUpload)
            smScriptManager.RegisterPostBackControl(btnUpload1)

            If Not ViewState("ActiveTabIndex") Is Nothing Then TabContainer1.ActiveTabIndex = ViewState("ActiveTabIndex")

            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                hViewState.Value = ViewState("datamode")
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI02021" And ViewState("MainMnu_code") <> "PI02027" And ViewState("MainMnu_code") <> "PI04013" And ViewState("MainMnu_code") <> "PI02035") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                BindDropDown()

                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If

                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(ViewState("EntryId"))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If
                showNoRecordsFound()
                hGridRefresh.Value = 0

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            If hGridRefresh.Value = 1 Then
                hGridRefresh.Value = 0
                showNoRecordsFound()
            End If
            If hCalculate.Value = 1 Then
                If Not Session("Empdata") Is Nothing Then
                    h_emp_ids.Value = Session("Empdata").rows(0)("TCE_EMP_ID")
                    txttrent.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_LAMOUNT)", "")).ToString("######0.00")
                    txtERent.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_EAMOUNT)", "")).ToString("######0.00")
                    txtEDed.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_DAMOUNT)", "")).ToString("######0.00")
                    lblEmpDetails.Text = Session("Empdata").rows(0)("EMPNAME") & ", Room Type:" & Session("Empdata").rows(0)("EUNITTYPE") & ",Unit No:" & Session("Empdata").rows(0)("TCE_UNITNO")
                    '19-05-2015
                    txtRevrent.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_OLDRENT)", "")).ToString("######0.00")
                    h_EmpCount.Value = Session("Empdata").rows.count
                    lblEmpCount.Text = "        -      Total Employees : " + h_EmpCount.Value
                    Dim i As Integer
                    Dim UnitNo As String = ""
                    Dim dt As DataTable = Nothing
                    dt = Session("Empdata")
                    For i = 0 To dt.Rows.Count - 1
                        UnitNo = UnitNo & dt.Rows(i)("TCE_UNITNO") & ","
                    Next
                    txtBuilding.Text = Left(UnitNo, Len(UnitNo) - 1)
                    hCalculate.Value = 0
                    CalculateTotal()
                End If
            End If


            ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
            DisableButtons(ViewState("menu_rights"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Sub DisableButtons(ByVal AccesRights As Integer)

        If AccesRights = 1 Then
            btnAdd.Visible = False
            btnRecall.Visible = False
            btnRenew.Visible = False
            btnEdit.Visible = False
            btnSave.Visible = False
            btnAccept.Visible = False
            btnRecall.Visible = False
            btnSend.Visible = False
            btnSend.Visible = False
            btnPrint.Visible = False
            btnDelete.Visible = False
            btnReturn.Visible = False
        End If
    End Sub
    Protected Sub h_MSG2_ValueChanged(sender As Object, e As EventArgs) Handles h_MSG2.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        If Not Session("Empdata") Is Nothing Then
            h_emp_ids.Value = Session("Empdata").rows(0)("TCE_EMP_ID")
            txttrent.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_LAMOUNT)", "")).ToString("######0.00")
            txtERent.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_EAMOUNT)", "")).ToString("######0.00")
            txtEDed.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_DAMOUNT)", "")).ToString("######0.00")
            lblEmpDetails.Text = Session("Empdata").rows(0)("EMPNAME") & ", Room Type:" & Session("Empdata").rows(0)("EUNITTYPE") & ",Unit No:" & Session("Empdata").rows(0)("TCE_UNITNO")
            '19-05-2015
            txtRevrent.Text = Convert.ToDouble(Session("Empdata").Compute("sum(TCE_OLDRENT)", "")).ToString("######0.00")
            h_EmpCount.Value = Session("Empdata").rows.count
            lblEmpCount.Text = "        -      Total Employees : " + h_EmpCount.Value
            Dim i As Integer
            Dim UnitNo As String = ""
            Dim dt As DataTable = Nothing
            dt = Session("Empdata")
            For i = 0 To dt.Rows.Count - 1
                UnitNo = UnitNo & dt.Rows(i)("TCE_UNITNO") & ","
            Next
            txtBuilding.Text = Left(UnitNo, Len(UnitNo) - 1)
            hCalculate.Value = 0
            CalculateTotal()
        End If

        h_MSG2.Value = ""
    End Sub
    Private Sub BindDropDown()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim str_Sql As String = "select REU_ID,REU_NAME  FROM REUNITTYPE "
            Dim str_Sql1 As String = "SELECT RCM_ID ID,RCM_DESCR DESCR FROM RECITY_M "
            'Dim str_Sql2 As String = "SELECT BSU_SHORTNAME,BSU_ID from oasis..BUSINESSUNIT_M where BSU_ID='" & Session("sBsuid") & "'"
            Dim str_Sql2 As String




            If h_EntryId.Value = 0 Then
                str_Sql2 = "SELECT DESCR ,ID from VW_GET_BUDGETS where BSU_ID='" & Session("sBsuid") & "'  union all select '---Select Budget Year---',0 order by ID "
            Else
                str_Sql2 = "SELECT DESCR ,ID from VW_GET_BUDGETS where BSU_ID='" & Session("sBsuid") & "'  union all select '---Select Budget Year---',0 order by ID "
            End If


            Dim str_Sql3 As String = "select RES_DESCR,RES_ID from RESERVICES where REC_CIT_ID ='" & ddlLocation.SelectedValue & "' union all select RES_DESCR,RES_ID from RESERVICES where REC_CIT_ID ='000'"
            Dim str_Sql4 As String = "select ID,DESCR from VW_GCO_INFO order by id "

            ddlLocation.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql1)
            ddlLocation.DataTextField = "DESCR"
            ddlLocation.DataValueField = "ID"
            ddlLocation.DataBind()

            ddlGCoInfo.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql4)
            ddlGCoInfo.DataTextField = "DESCR"
            ddlGCoInfo.DataValueField = "ID"
            ddlGCoInfo.DataBind()

            ddlBudget.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql2)
            ddlBudget.DataTextField = "DESCR"
            ddlBudget.DataValueField = "ID"
            ddlBudget.DataBind()



        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            hViewState.Value = ViewState("datamode")
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            hViewState.Value = ViewState("datamode")
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            hViewState.Value = ViewState("datamode")
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        ddlLocation.Enabled = Not mDisable
        imgCondate.Enabled = EditAllowed
        imgSDate.Enabled = EditAllowed
        imgeEDate.Enabled = EditAllowed
        ImgLandLord.Enabled = EditAllowed
        ImgREAgent.Enabled = EditAllowed
        txtContractSDate.Enabled = EditAllowed
        txtContractEDate.Enabled = EditAllowed
        'radBudgeted.Enabled = EditAllowed
        'radUnBudgeted.Enabled = EditAllowed
        txtOldContractNo.Enabled = EditAllowed
        txtPerc.Enabled = EditAllowed
        'txtRevrent.Enabled = EditAllowed
        chkIncAsperRera.Enabled = EditAllowed
        RepService.Enabled = EditAllowed
        txtRecovery.Enabled = EditAllowed
        txtTerms.Enabled = EditAllowed
        txtEmpDetails.Enabled = EditAllowed
        grdPayment.Enabled = EditAllowed
        'txtBuilding.Enabled = EditAllowed
        txtBuildingName.Enabled = EditAllowed
        imgArea.Enabled = EditAllowed
        imgBuilding.Enabled = EditAllowed
        btnSend.Visible = mDisable
        btnReturn.Visible = mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable 'Or ViewState("MainMnu_code") = "U000086"
        btnEdit.Visible = mDisable
        btnPrint.Visible = Not btnSave.Visible
        btnAccept.Visible = mDisable And (ViewState("MainMnu_code") = "PI04013") 'approval menu
        btnReject.Visible = btnAccept.Visible
        btnMessage.Visible = btnAccept.Visible
        btnAdd.Visible = mDisable And Not (ViewState("MainMnu_code") = "PI04013")
        'btnEmpDetails.Visible = EditAllowed
        If grdPayment.Rows.Count - 1 > 0 Or ViewState("datamode") = "edit" Then ddlLocation.Enabled = False
        txtnoofPayments.Enabled = EditAllowed
        txtPSDate.Enabled = EditAllowed
        btnCreatePayments.Enabled = EditAllowed
        imgPSDate.Enabled = EditAllowed
        ddlGCoInfo.Enabled = EditAllowed
        txtOtherDed.Enabled = EditAllowed
        ddlBudget.Enabled = EditAllowed
        txtNoticePeriod.Enabled = EditAllowed


    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Dim sqlStr As New StringBuilder
        Dim sqlWhere As String = p_Modifyid
        Dim str_conn As String = connectionString
        Dim dt As New DataTable
        h_EntryId.Value = p_Modifyid
        h_bsu_id.Value = Session("sBSUID")
        trCalcRent.Visible = (p_Modifyid = 0)
        Session("Empdata") = Nothing

        If p_Modifyid = 0 Then
            trApproval.Visible = False
            trWorkflow.Visible = False
            trFlow.Visible = False
            trUpload.Visible = False
            trDocument.HeaderText = ""
            trUpload.HeaderText = ""
            trApproval.HeaderText = ""
            trWorkflow.HeaderText = ""
            trFlow.HeaderText = ""
            trDocument.Visible = False
            ClearDetails()
            txtTCH_No.Text = "New"
            h_TCH_Type.Value = "N"
            DisplayServices(ddlLocation.SelectedValue)
            btnAccept.Visible = False
            btnReject.Visible = False
            btnRecall.Visible = False
            btnEdit.Visible = False
            btnMessage.Visible = False
            btnSend.Visible = False
            btnReturn.Visible = False
            btnDelete.Visible = False
            btnSave.Visible = True
            txttotal.Text = "0.00"
            txtRevrent.Text = "0.00"
            txtPerc.Text = "0.00"
            txtBuilding.Text = ""
            txttrent.Text = "0.00"
            txtPerc.Text = "0.00"
            txtOtherDed.Text = "0.00"

            trVisited.Visible = False
            h_Loc_id.Value = ddlLocation.SelectedValue
            Panel1_ModalPopupExtender.Show()
            pgTitle.Text = "New Tenancy Contract For Staff Accommodation"
            CalculateTotal()
            Session("Empdata") = Nothing
        Else



            Dim RerurnEnable As Integer = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select isnull(count(*),0)Total from VW_Enable_Return_User_list where UserID='" & Session("sUsr_name") & "'")
            sqlStr = New StringBuilder
            sqlStr.Append("SELECT TCH_ID, TCH_FYEAR, TCH_NO, TCH_DATE, TCH_TYPE, TCH_CIT_ID, TCH_RENT, TCH_INCDEC,TCH_REVISED,TCH_HIREDSINCE, TCH_OTHERS,")
            sqlStr.Append("TCH_TOTAL, TCH_START, TCH_END, TCH_ENT_TYPE,TCH_ENT_AMOUNT, TCH_ENT_DEDUCTION, TCH_BUDGETED, isnull(TCH_APR_ID,0)TCH_APR_ID, TCH_BSU_ID,isnull(TCH_EMPDETAILS,'')TCH_EMPDETAILS, ")
            sqlStr.Append("ISNULL(TCH_TERMS,'') TCH_TERMS,isnull(TCH_RECOVERYPROCESS,'')TCH_RECOVERYPROCESS,TCH_UNIT_PAYING,TCH_OCCUPANTS,isnull(TCH_OLDCONTRACT,'')TCH_OLDCONTRACT,TCH_REA_ID,TCH_REL_ID,TCH_VIEWBY,TCH_VIEWDATE,")
            'sqlStr.Append("isnull(REA_NAME,'')REA_NAME,isnull(REA_CONTACT_PERSON,'')REA_CONTACT_PERSON,isnull(REA_MOBILE,'')REA_MOBILE,REL_NAME, REL_MOBILE,")
            'New Vendor Master Start
            'sqlStr.Append("isnull(TA.TRV_NAME,REA_NAME)REA_NAME,isnull(TA.TRF_VENDOR_CONTACT_PERSON,REA_CONTACT_PERSON)REA_CONTACT_PERSON,isnull(TA.TRV_PRIMARY_PHONE,REA_MOBILE)REA_MOBILE, ")
            'sqlStr.Append(" isnull(TL.TRV_NAME,REL_NAME)REL_NAME,isnull(TL.TRF_VENDOR_CONTACT_PERSON,REL_NAME)REL_CONTACT_PERSON,isnull(TL.TRV_PRIMARY_PHONE,REL_MOBILE)REL_MOBILE")


            sqlStr.Append("isnull(isnull(TA.TRV_NAME,REA_NAME),'Not Linked')REA_NAME,isnull(isnull(TA.TRF_VENDOR_CONTACT_PERSON,REA_CONTACT_PERSON),'Not Linked')REA_CONTACT_PERSON,isnull(isnull(TA.TRV_PRIMARY_PHONE,REA_MOBILE),'Not Linked')REA_MOBILE,  ")
            sqlStr.Append("isnull(isnull(TL.TRV_NAME,REL_NAME),'Not Linked')REL_NAME,isnull(isnull(TL.TRF_VENDOR_CONTACT_PERSON,REL_NAME),'Not Linked')REL_CONTACT_PERSON,isnull(isnull(TL.TRV_PRIMARY_PHONE,REL_MOBILE),'Not Linked')REL_MOBILE")

            'New Vendor Master End
            sqlStr.Append(",isnull(TCH_BUDGET_FYEAR,0)TCH_BUDGET_FYEAR,TCH_DELETED,TCH_STATUS,isnull(TCH_BUILDINGNAME,'') TCH_BUILDINGNAME,TCH_BLD_ID,TCH_RAR_ID,TCH_BUILDING BUILDING,ISNULL(RAR_NAME,'') AREANAME,TCH_REU_ID ")
            sqlStr.Append(",isnull(a.EntAmount,0)EntAmount,isnull(a.EntDed,0)EntDed,isnull(a.LeaseAmount,0)LeaseAmount,isnull(TCH_INC_RERA,0)TCH_INC_RERA,case when isnull(TCH_GCO_INFO,'')='' then 'NONGEMS' else TCH_GCO_INFO END TCH_GCO_INFO  ")
            sqlStr.Append(",isnull(TCH_OTHERDED,0)TCH_OTHERDED,isnull(TCH_NOTICE_PERIOD,'')TCH_NOTICE_PERIOD ")
            sqlStr.Append(" FROM TCT_H ")
            sqlStr.Append(" LEFT OUTER JOIN REAGENT ON REA_ID =TCH_REA_ID LEFT OUTER JOIN RELANDLORD ON REL_ID =TCH_REL_ID")
            sqlStr.Append(" LEFT OUTER JOIN TRFVENDOR TA ON TA.TRV_ID =TCH_REA_ID  LEFT OUTER JOIN TRFVENDOR TL ON TL.TRV_ID =TCH_REL_ID ")
            sqlStr.Append(" LEFT OUTER JOIN REAREA ON RAR_ID =TCH_RAR_ID ")
            sqlStr.Append(" left outer join (SELECT SUM(TCE_EAMOUNT)EntAmount,SUM(TCE_LAMOUNT)LeaseAmount,SUM(TCE_DAMOUNT)EntDed,TCE_TCH_ID   FROM TCT_E where TCE_DELETED=0 GROUP BY TCE_TCH_ID) a  ON a.TCE_TCH_ID  =TCT_H.TCH_ID ")
            sqlStr.Append(" WHERE TCH_ID = " & p_Modifyid)
            dt = MainObj.getRecords(sqlStr.ToString, "OASIS_PUR_INVConnectionString")

            If dt.Rows.Count > 0 Then
                Dim SQL, SQL1 As String
                'SQL = "SELECT top 1 isnull(EMP_NAME,bsu_name)+', Room Type:'+U1.REU_NAME + ', Unit No:'+ ISNULL(TCE_UNITNO,'') FROM TCT_E LEFT OUTER JOIN oasis_docs..VW_EMPLOYEE_V  ON EMP_ID=TCE_EMP_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCE_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID where TCE_TCH_ID=" & dt.Rows(0)("TCH_ID") & " and TCE_DELETED=0 order by TCE_ID"
                'commented on 15-07-2019 Mahesh
                SQL = "SELECT top 1 case when tce_emp_id=0 then 'TBC' else case when tce_emp_id=1 then 'Vacant'else case when tce_emp_id=2 then bsu_name else isnull(EMP_NAME,bsu_name) END END END +', Room Type:'+U1.REU_NAME + ', Unit No:'+ ISNULL(TCE_UNITNO,'') FROM TCT_E LEFT OUTER JOIN oasis_docs..VW_EMPLOYEE_V  ON EMP_ID=TCE_EMP_ID LEFT OUTER JOIN TCT_H on TCH_ID=TCE_TCH_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCH_BSU_ID  LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID where TCE_TCH_ID=" & dt.Rows(0)("TCH_ID") & " and TCE_DELETED=0 order by TCE_ID"


                SQL1 = "select COUNT(*) Count from TCT_E where TCE_TCH_ID =" & dt.Rows(0)("TCH_ID") & " and TCE_DELETED =0"
                lblEmpDetails.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, SQL)
                If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, SQL1) > 1 Then
                    h_EmpCount.Value = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, SQL1).ToString()
                    lblEmpCount.Text = "        -      Total Employees : " + h_EmpCount.Value
                End If

                Dim ddlBudgetSql As String = "SELECT DESCR ,ID from VW_GET_BUDGETS where BSU_ID='" & dt.Rows(0)("TCH_BSU_ID") & "'  union all select '---Select Budget Year---',0 order by ID "

                ddlBudget.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, ddlBudgetSql)
                ddlBudget.DataTextField = "DESCR"
                ddlBudget.DataValueField = "ID"
                ddlBudget.DataBind()



                Session("Empdata1") = Nothing
                'Dim TempDt As DataTable = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select distinct tce_emp_id from  tct_E where tce_tch_id=" & dt.Rows(0)("TCH_ID") & " and tce_deleted=0")

                Dim TempDt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select distinct tce_emp_id from  tct_E where tce_tch_id=" & dt.Rows(0)("TCH_ID") & " and tce_deleted=0").Tables(0)
                Session("Empdata1") = TempDt

                h_EmpCount.Value = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, SQL1).ToString()
                h_emp_ids.Value = dt.Rows(0)("TCH_OCCUPANTS")
                h_EntryId.Value = dt.Rows(0)("TCH_ID")
                h_Loc_id.Value = dt.Rows(0)("TCH_CIT_ID")
                h_REAId.Value = dt.Rows(0)("TCH_REA_ID")
                h_RELId.Value = dt.Rows(0)("TCH_REL_ID")
                h_TCH_Status.Value = dt.Rows(0)("TCH_STATUS")
                h_area_ID.Value = dt.Rows(0)("TCH_RAR_ID")
                h_building_ID.Value = dt.Rows(0)("TCH_BLD_ID")
                h_delete.Value = dt.Rows(0)("TCH_DELETED")
                h_TCH_Type.Value = dt.Rows(0)("TCH_TYPE")
                txttrent.Text = dt.Rows(0)("TCH_RENT")
                txtHiredSince.Text = dt.Rows(0)("TCH_HIREDSINCE")
                DisplayServicesAmounts(h_EntryId.Value, h_Loc_id.Value)
                txtERent.Text = dt.Rows(0)("EntAmount")
                txtEDed.Text = dt.Rows(0)("EntDed")
                txttrent.Text = dt.Rows(0)("LeaseAmount")
                h_bsu_id.Value = dt.Rows(0)("TCH_BSU_ID")
                h_fyear.Value = dt.Rows(0)("TCH_FYEAR")
                If h_TCH_Type.Value = "N" Then
                    pgTitle.Text = "New Tenancy Contract For Staff Accommodation"
                Else
                    pgTitle.Text = "Renewal Tenancy Contract For Staff Accommodation"
                End If
                BindDocuments(h_TCH_Type.Value)
                txtTCH_No.Text = dt.Rows(0)("TCH_NO")
                ddlLocation.SelectedValue = dt.Rows(0)("TCH_CIT_ID")
                txtPerc.Text = dt.Rows(0)("TCH_INCDEC")
                txtRevrent.Text = dt.Rows(0)("TCH_REVISED")
                txttotal.Text = dt.Rows(0)("TCH_TOTAL")
                txtContractDate.Text = Format(dt.Rows(0)("TCH_DATE"), "dd/MMM/yyyy")
                txtContractSDate.Text = Format(dt.Rows(0)("TCH_START"), "dd/MMM/yyyy")
                txtContractEDate.Text = Format(dt.Rows(0)("TCH_END"), "dd/MMM/yyyy")
                If dt.Rows(0)("TCH_BUDGETED") = "1" Then
                    radBudgeted.Checked = True
                Else
                    radUnBudgeted.Checked = True
                End If
                txtTerms.Text = dt.Rows(0)("TCH_TERMS")
                txtEmpDetails.Text = dt.Rows(0)("TCH_EMPDETAILS")
                txtRecovery.Text = dt.Rows(0)("TCH_RECOVERYPROCESS")
                txtOldContractNo.Text = dt.Rows(0)("TCH_OLDCONTRACT")
                If h_REAId.Value = "0" Then
                    txtAgent.Text = "None"
                    txtCPerson.Text = "NA"
                    txtCNumber.Text = "NA"
                Else
                    txtAgent.Text = dt.Rows(0)("REA_NAME")
                    txtCPerson.Text = dt.Rows(0)("REA_CONTACT_PERSON")
                    txtCNumber.Text = dt.Rows(0)("REA_MOBILE")
                End If
                txtLandlord.Text = dt.Rows(0)("REL_NAME")
                txtLLCNo.Text = dt.Rows(0)("REL_MOBILE")
                txtLLCPerson.Text = dt.Rows(0)("REL_NAME")
                txtBuilding.Text = dt.Rows(0)("TCH_BUILDINGNAME")
                txtBuildingName.Text = dt.Rows(0)("BUILDING")
                txtArea.Text = dt.Rows(0)("AREANAME")
                If IsDate(dt.Rows(0)("TCH_VIEWDATE")) Then
                    txtVisitDate.Text = IIf((dt.Rows(0)("TCH_VIEWDATE") = "1980-01-01" Or Not IsDate(dt.Rows(0)("TCH_VIEWDATE"))), "", dt.Rows(0)("TCH_VIEWDATE"))
                End If
                txtVisitor.Text = IIf(IsDBNull(dt.Rows(0)("TCH_VIEWBY")), "", dt.Rows(0)("TCH_VIEWBY"))
                h_Tch_apr_id.Value = dt.Rows(0)("TCH_APR_ID")
                CalculateTotal()
                txtOtherDed.Text = dt.Rows(0)("TCH_OTHERDED")
                txtNoticePeriod.Text = dt.Rows(0)("TCH_NOTICE_PERIOD")

                btnAccept.Visible = btnAccept.Visible And dt.Rows(0)("TCH_STATUS") = "S"
                btnReject.Visible = btnAccept.Visible
                btnRecall.Visible = dt.Rows(0)("TCH_STATUS") = "R" Or dt.Rows(0)("TCH_STATUS") = "D"
                btnSend.Visible = btnSend.Visible And (dt.Rows(0)("TCH_STATUS") = "N")

                ddlBudget.Items.FindByValue(dt.Rows(0)("TCH_BUDGET_FYEAR")).Selected = True

                'btnReturn.Visible = btnAccept.Visible And Session("sUsr_name") = "jemelie.comaingking"

                If RerurnEnable > 0 Then
                    btnReturn.Visible = True
                Else
                    btnReturn.Visible = False
                End If



                btnSave.Visible = btnSave.Visible And ((dt.Rows(0)("TCH_STATUS") = "N"))
                btnRenew.Visible = (UCase(dt.Rows(0)("TCH_STATUS")) = "C")
                btnPrint.Visible = Not btnSave.Visible
                btnEdit.Visible = btnEdit.Visible And (dt.Rows(0)("TCH_STATUS") = "N" Or dt.Rows(0)("TCH_STATUS") = "Q") And dt.Rows(0)("TCH_APR_ID") = 0 And (ViewState("MainMnu_code") = "PI02027")
                btnEdit.Visible = btnEdit.Visible Or ViewState("MainMnu_code") = "PI02035"
                btnAdd.Visible = (Session("F_YEAR") >= "2014") And btnAdd.Visible And Not (ViewState("MainMnu_code") = "PI04013" Or ViewState("MainMnu_code") = "PI02035")
                btnDelete.Visible = (("QN").Contains(dt.Rows(0)("TCH_STATUS")) And btnDelete.Visible)
                btnMessage.Visible = btnAccept.Visible Or dt.Rows(0)("TCH_STATUS") = "S" Or (ViewState("MainMnu_code") = "PI02035")
                trVisited.Visible = (h_Tch_apr_id.Value = 20 And btnAccept.Visible And h_TCH_Type.Value = "N")
                If trVisited.Visible Then
                    lblMSO.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select usr_display_name from usersa inner join oasis..users_m on usra_usr_name=usr_name where usra_bsu_id='" & h_bsu_id.Value & "' and usra_email is not null")
                    txtVisitDate.Enabled = (h_Tch_apr_id.Value = 20)
                    txtVisitor.Enabled = (h_Tch_apr_id.Value = 20)
                End If
                If dt.Rows(0)("TCH_TYPE") = "R" Then
                    trRentalInc.Visible = True
                    lblMSO.Text = Session("sUsr_name")

                    If dt.Rows(0)("TCH_INC_RERA") = True Then
                        chkIncAsperRera.Checked = True
                    Else
                        chkIncAsperRera.Checked = False
                    End If
                End If

                ddlGCoInfo.SelectedItem.Text = dt.Rows(0)("TCH_GCO_INFO")

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
        If p_Modifyid > 0 Then
            If ViewState("MainMnu_code") = "PI04013" Then
                trApproval.Visible = True
                trApproval.HeaderText = "Approval\Rejection Comments"
            End If
            Dim ds As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "workflowqueryTCT " & h_EntryId.Value).Tables(0)
            rptFlow.DataSource = ds
            rptFlow.DataBind()
            trFlow.Visible = True
            trFlow.HeaderText = "Workflow progress"
            trWorkflow.Visible = True
            trWorkflow.HeaderText = "Workflow Details"
            trDocument.Visible = True
            trDocument.HeaderText = "Documents"

            trUpload.Visible = True
            trUpload.HeaderText = "Contracts"
            If btnMessage.Visible Then
                trApproval.Visible = True
                If ViewState("MainMnu_code") = "PI04013" Then
                    trApproval.HeaderText = "Approval\Rejection Comments"
                Else
                    trApproval.HeaderText = "Message Comments"
                End If
            End If
            sqlStr = New StringBuilder
            sqlStr.Append("select replace(convert(varchar(16), WRK_DATE, 106),' ','/') WRK_DATE, USR_DISPLAY_NAME WRK_USER, WRK_ACTION, WRK_DETAILS from WORKFLOWTCT inner JOIN OASIS.dbo.USERS_M on WRK_USERNAME=USR_NAME where WRK_TCH_ID=" & h_EntryId.Value & " order by WRK_ID desc")
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr.ToString).Tables(0)
            gvWorkflow.DataSource = ds
            gvWorkflow.DataBind()
            If dt.Rows(0)("TCH_STATUS") = "N" And dt.Rows(0)("TCH_APR_ID") = 0 Then
                Dim isReturned As Int16 = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from WORKFLOWTCT where WRK_ACTION='Returned' and WRK_TCH_ID=" & h_EntryId.Value)
                If isReturned > 0 Then
                    btnSend.Visible = False
                    btnPrint.Visible = False
                    btnAdd.Visible = False
                End If
            End If
            ddlBudget.Enabled = False
            showImages()
            showImagesDocument()
        End If
        fillGridView(grdPayment, "SELECT TCP_ID,TCP_TCH_ID,TCP_RES_ID,RES_DESCR,TCP_CHQDATE,TCP_AMOUNT,TCP_PAYMENTTYPE,TCP_DELETED,TCP_PAYABLETO  FROM TCT_P INNER JOIN (select RES_ID, RES_DESCR FROM RESERVICES union select 0, 'Rental') a on RES_ID=TCP_RES_ID where TCP_TCH_ID=" & h_EntryId.Value & " and isnull(tcp_deleted,0)=0 order by TCP_ID")
        fillRecGridView(GrdRec, "SELECT TCR_ID,TCR_TCH_ID,TCR_RES_ID,A.TCR_PAYMENTTYPE RES_DESCR,TCR_CHQDATE,TCR_AMOUNT,TCR_DELETED  FROM TCT_R INNER JOIN (select RT_ID, TCR_PAYMENTTYPE FROM VW_GET_RECOVERYTPE) a on RT_ID=TCR_RES_ID where TCR_TCH_ID=" & h_EntryId.Value & " and isnull(TCR_DELETED,0)=0 order by TCR_ID")
    End Sub

    Private Sub showImages()
        Dim sqlStr As String
        sqlStr = "select tci_id, tci_filename, tci_contenttype, RES_DESCR tci_name, case when tci_username='" & Session("sUsr_name") & "' then 'true' else 'false' end tci_visible "
        sqlStr &= "from TCT_I inner JOIN RESERVICES on RES_ID=tci_doctype where RES_CONTRACT_STATUS<>'U' and tci_tch_id=" & h_EntryId.Value
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr).Tables(0)
        rptImages.DataSource = dt
        rptImages.DataBind()
    End Sub
    Private Sub showImagesDocument()
        Dim sqlStr As String
        sqlStr = "select tci_id, tci_filename, tci_contenttype, RES_DESCR tci_name, case when tci_username='" & Session("sUsr_name") & "' then 'true' else 'false' end tci_visible "
        sqlStr &= "from TCT_I inner JOIN RESERVICES on RES_ID=tci_doctype where RES_CONTRACT_STATUS='U' and tci_tch_id=" & h_EntryId.Value
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr).Tables(0)
        rptDocument.DataSource = dt
        rptDocument.DataBind()
    End Sub

    Private Sub CalculateTotal()
        Dim servicetotal As Long = 0
        For Each item As DataListItem In RepService.Items
            Dim txtRESAmt As TextBox = CType(item.FindControl("txtRESAmount"), TextBox)
            servicetotal = servicetotal + IIf((txtRESAmt.Text = ""), 0, txtRESAmt.Text)
        Next
        txtCharges.Text = servicetotal
        txttotal.Text = Val(txttrent.Text) + Val(txtCharges.Text)
        If btnSave.Visible Then
            txtPerc.Text = Convert.ToDouble((Val(txttrent.Text) - Val(txtRevrent.Text)) / Val(txtRevrent.Text) * 100).ToString("######0.00")
        End If
        'txtPerc.Text = Convert.ToDouble(((Val(txttrent.Text) / h_EmpCount.Value) - Val(txtRevrent.Text)) / Val(txtRevrent.Text) * 100).ToString("######0.00")
        For Each item As DataListItem In RepService.Items
            Dim lblPerc As Label = CType(item.FindControl("lblPerc"), Label)
            Dim txtAmt As TextBox = CType(item.FindControl("txtRESAmount"), TextBox)
            Dim RowID As Label = CType(item.FindControl("lblRowId"), Label)
            Dim DParms(5) As SqlClient.SqlParameter

            If Val(lblPerc.Text) > 0 Then

                txtAmt.Text = Val(txttrent.Text) * Val(lblPerc.Text) / 100.0
            End If

        Next

        showNoRecordsFound()
    End Sub
    Private Sub CheckRentalAmounts()
        Dim TotalRent As Decimal = 0
        Dim RowCount As Integer
        If Not PaymentDetails Is Nothing Then
            If PaymentDetails.Rows(0)(1) <> -1 Then
                For RowCount = 0 To PaymentDetails.Rows.Count - 1
                    If PaymentDetails.Rows(RowCount)("TCP_RES_ID") = "0" Then
                        TotalRent = TotalRent + PaymentDetails.Rows(RowCount)("TCP_AMOUNT")
                    End If
                Next
            End If
        End If
        If Val(txttrent.Text) > TotalRent Then
            lblError.Text = "Rent Payment details are not matching!"
            Exit Sub
        End If
    End Sub
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        PaymentDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(PaymentDetails)
        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        PaymentDetails = mtable
        fillGrdView.DataSource = PaymentDetails
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub

    Private Sub fillRecGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        RecoveryDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(RecoveryDetails)
        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        RecoveryDetails = mtable
        fillGrdView.DataSource = RecoveryDetails
        fillGrdView.DataBind()
        showNoRecordsFound1()
    End Sub
    Private Sub showNoRecordsFound()
        If Not PaymentDetails Is Nothing Then
            If PaymentDetails.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdPayment.Columns.Count - 2
                grdPayment.Rows(0).Cells.Clear()
                grdPayment.Rows(0).Cells.Add(New TableCell())
                grdPayment.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdPayment.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub
    Private Sub showNoRecordsFound1()
        If Not RecoveryDetails Is Nothing Then
            If RecoveryDetails.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = GrdRec.Columns.Count - 2
                GrdRec.Rows(0).Cells.Clear()
                GrdRec.Rows(0).Cells.Add(New TableCell())
                GrdRec.Rows(0).Cells(0).ColumnSpan = TotalColumns
                GrdRec.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub
    Private Property PaymentDetails() As DataTable
        Get
            Return ViewState("PaymentDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("PaymentDetails") = value
        End Set
    End Property
    Private Property RecoveryDetails() As DataTable
        Get
            Return ViewState("RecoveryDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("RecoveryDetails") = value
        End Set
    End Property
    Private Sub DisplayServices(ByVal LocId As String)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select  CASE WHEN isnull(REC_PERCENT,0)=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,REC_DEFAULT AMOUNT, RES_ID ID,0  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES where  REC_CIT_ID ='" & LocId & "' AND RES_DELETED<>1  union all select CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,REC_DEFAULT AMOUNT, RES_ID ID,0  RowId,REC_PERCENT PERC    from RESERVICES where  REC_CIT_ID in(select RCM_ID FROM RECITY_M WHERE RCM_DESCR='ALL') AND RES_DELETED<>1  order by ID").Tables(0)
        RepService.DataSource = dt
        RepService.DataBind()


        For Each item As DataListItem In RepService.Items
            Dim lblPerc As Label = CType(item.FindControl("lblPerc"), Label)
            Dim txtAmt As TextBox = CType(item.FindControl("txtRESAmount"), TextBox)
            Dim RowID As Label = CType(item.FindControl("lblRowId"), Label)
            Dim DParms(5) As SqlClient.SqlParameter

            If Val(lblPerc.Text) > 0 Then
                txtAmt.Enabled = False
            End If

        Next


        chkIncAsperRera.Visible = (ddlLocation.SelectedItem.Text = "DUBAI")
    End Sub
    Private Sub Bindpayment()
        Dim ddlPayment As DropDownList = grdPayment.FooterRow.FindControl("ddlPaymentType")
        Dim sqlStr As String = ""
        sqlStr &= "SELECT 0 RES_ID,'Rental'RES_DESCR UNION ALL SELECT RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID  ='" & h_Loc_id.Value & "' UNION ALL select RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID ='000' UNION ALL select RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID IN(SELECT RCM_ID FROM RECITY_M WHERE RCM_DESCR ='ALL' ) "
        fillDropdown(ddlPayment, sqlStr, "RES_DESCR", "RES_ID", False)
    End Sub
    Private Sub BindDocuments(ByVal status As String)
        fillDropdown(ddlDocType, "select res_id, res_descr from reservices where res_image=1 and REC_CIT_ID in(select rcm_id from recity_m where rcm_id=" & h_Loc_id.Value & ") AND RES_DELETED<>1 union select res_id, res_descr from RESERVICES  where REC_CIT_ID in(select rcm_id from recity_m where rcm_descr='IMAGE') and RES_CONTRACT_STATUS='" & status & "' AND RES_DELETED<>1 order by res_id  ", "RES_DESCR", "RES_ID", False)
        fillDropdown(ddlUploadType, "select res_id, res_descr from reservices where res_image=1 and REC_CIT_ID=9 and RES_CONTRACT_STATUS='U' AND RES_DELETED<>1 order by res_id  ", "RES_DESCR", "RES_ID", False)

    End Sub
    Private Sub DisplayServicesAmounts(ByVal EntryId As String, ByVal LocId As String)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID in(select RCM_ID FROM RECITY_M WHERE RCM_DESCR='ALL') AND RES_DELETED<>1   UNION ALL select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,TCT_ID  RowId,isnull(REC_PERCENT,0) PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & EntryId & " where  REC_CIT_ID ='" & LocId & "' AND RES_DELETED<>1  order by ID").Tables(0)
        RepService.DataSource = dt
        RepService.DataBind()
    End Sub
    Sub ClearDetails()
        txtContractDate.Text = Now.ToString("dd/MMM/yyyy")
        txtContractSDate.Text = Now.ToString("dd/MMM/yyyy")
        txtContractEDate.Text = Format(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Year, 1, CDate(txtContractSDate.Text))), "dd/MMM/yyyy")
        txtRecovery.Text = ""
        txtTerms.Text = ""
        txtEmpDetails.Text = ""
        txtRevrent.Text = "0.00"
        txtPerc.Text = "0.00"
        txtHiredSince.Text = "0"
        txtTCH_No.Text = ""
        txtCPerson.Text = ""
        txtCNumber.Text = ""
        txtLandlord.Text = ""
        txtLLCNo.Text = ""
        txtLLCPerson.Text = ""
        txtOldContractNo.Text = ""
        txtAgent.Text = ""
        h_emp_ids.Value = ""
        h_area_ID.Value = ""
        h_building_ID.Value = ""
        txtBuilding.Text = ""
        txtBuildingName.Text = ""
        txtArea.Text = ""
        lblEmpDetails.Text = ""
        lblEmpCount.Text = ""
        txtNoticePeriod.Text = ""
        txtERent.Text = "0.00"
        txtEDed.Text = "0.00"
        txtOtherDed.Text = "0.00"
        ddlBudget.Items.FindByValue(0).Selected = True
        'ddlProduct.Items.FindByValue(h_Service_ID_F.Value).Selected = True

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        h_Loc_id.Value = ddlLocation.SelectedItem.Value
        DisplayServices(h_Loc_id.Value)
        Bindpayment()
    End Sub

    Protected Sub txtContractSDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtContractSDate.TextChanged
        If txtContractSDate.Text <> "" Then
            txtContractEDate.Text = Format(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Year, 1, CDate(txtContractSDate.Text))), "dd/MMM/yyyy")
        End If
    End Sub

    Protected Sub grdPayment_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdPayment.RowCancelingEdit
        grdPayment.ShowFooter = True
        grdPayment.EditIndex = -1
        grdPayment.DataSource = PaymentDetails
        grdPayment.DataBind()
    End Sub
    Protected Sub grdPayment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPayment.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtChqDate As TextBox = grdPayment.FooterRow.FindControl("txtChqDate")
            Dim txtAmount As TextBox = grdPayment.FooterRow.FindControl("txtAmount")
            Dim txtProductid As HiddenField = grdPayment.FooterRow.FindControl("h_prod_id")
            Dim ddlPayment As DropDownList = grdPayment.FooterRow.FindControl("ddlPaymentType")
            Dim ddlPType As DropDownList = grdPayment.FooterRow.FindControl("ddlPType")
            Dim txtPayable As TextBox = grdPayment.FooterRow.FindControl("txtPayable")

            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            If PaymentDetails.Rows(0)(1) = -1 Then PaymentDetails.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = PaymentDetails.NewRow
            mrow("TCP_ID") = 0
            mrow("TCP_RES_ID") = ddlPayment.SelectedValue
            mrow("RES_DESCR") = ddlPayment.SelectedItem.Text
            mrow("TCP_CHQDATE") = txtChqDate.Text
            mrow("TCP_AMOUNT") = txtAmount.Text
            mrow("TCP_PAYMENTTYPE") = ddlPType.SelectedItem.Text
            If ddlPayment.SelectedValue = 0 Then
                mrow("TCP_PAYABLETO") = h_PayName.Value
            Else
                mrow("TCP_PAYABLETO") = txtPayable.Text
            End If


            PaymentDetails.Rows.Add(mrow)
            grdPayment.EditIndex = -1
            grdPayment.DataSource = PaymentDetails
            grdPayment.DataBind()
            showNoRecordsFound()
        Else
            Dim txtInv_Rate As TextBox = grdPayment.FooterRow.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = grdPayment.FooterRow.FindControl("txtChqDate")
            Dim txtInv_Amt As TextBox = grdPayment.FooterRow.FindControl("txtAmount")
            Dim ddlPayment As DropDownList = grdPayment.FooterRow.FindControl("ddlPaymentType")
            Dim ddlPType As DropDownList = grdPayment.FooterRow.FindControl("ddlPType")
            Dim txtPayable As TextBox = grdPayment.FooterRow.FindControl("txtPayable")

        End If
    End Sub
    Protected Sub grdPayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPayment.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then
            Dim txtInv_Rate As TextBox = e.Row.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = e.Row.FindControl("txtInv_Qty")
            Dim txtInv As TextBox = e.Row.FindControl("txtInv_Qty")


            If e.Row.RowType = DataControlRowType.Footer Then
                Dim txtChqDate_F As TextBox = e.Row.FindControl("txtChqDate")
                Dim txtAmount_F As TextBox = e.Row.FindControl("txtAMOUNT")
                Dim hdnITM_ID_F As HiddenField = e.Row.FindControl("hdnITM_ID")
                Dim imgchqDate As ImageButton = e.Row.FindControl("imgChqDate")
                Dim ddlPType As DropDownList = e.Row.FindControl("ddlPType")
                Dim txtPayable_F As TextBox = e.Row.FindControl("txtPayable")

            End If
            If grdPayment.ShowFooter Or grdPayment.EditIndex > -1 Then
                Dim ddlPayment As DropDownList = e.Row.FindControl("ddlPaymentType")
                Dim ddlPType As DropDownList = e.Row.FindControl("ddlPType")
                If Not ddlPayment Is Nothing Then

                    Dim sqlStr As String = ""
                    Dim sqlStr1 As String = ""
                    sqlStr &= "SELECT 0 RES_ID,'Rental'RES_DESCR UNION ALL SELECT RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID  ='" & h_Loc_id.Value & "' AND RES_DELETED<>1 UNION ALL select RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID ='000' UNION ALL select RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID IN(SELECT RCM_ID FROM RECITY_M WHERE RCM_DESCR ='ALL' ) AND RES_DELETED<>1 "
                    sqlStr1 &= "SELECT '1' PT_ID,'Cash' TCP_PAYMENTTYPE UNION ALL select '2' PT_ID,'Cheque' TCP_PAYMENTTYPE "
                    fillDropdown(ddlPayment, sqlStr, "RES_DESCR", "RES_ID", False)
                    fillDropdown(ddlPType, sqlStr1, "TCP_PAYMENTTYPE", "PT_ID", False)
                    Dim prdTable As DataTable = ddlPayment.DataSource
                    For rowNum As Int16 = 0 To prdTable.Rows.Count - 1
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub

    Protected Sub grdPayment_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdPayment.RowDeleting
        Dim mRow() As DataRow = PaymentDetails.Select("ID=" & grdPayment.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= mRow(0)("TCP_ID") & ";"
            PaymentDetails.Select("ID=" & grdPayment.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            PaymentDetails.AcceptChanges()
        End If
        If PaymentDetails.Rows.Count = 0 Then
            PaymentDetails.Rows.Add(PaymentDetails.NewRow())
            PaymentDetails.Rows(0)(1) = -1
        End If

        grdPayment.DataSource = PaymentDetails
        grdPayment.DataBind()
        showNoRecordsFound()
    End Sub


    Protected Sub grdPayment_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdPayment.RowUpdating
        Dim s As String = grdPayment.DataKeys(e.RowIndex).Value.ToString()
        Dim TCP_id As Label = grdPayment.Rows(e.RowIndex).FindControl("lblTCP_ID")
        Dim ddlPayment As DropDownList = grdPayment.Rows(e.RowIndex).FindControl("ddlPaymentType")
        Dim ddlPType As DropDownList = grdPayment.Rows(e.RowIndex).FindControl("ddlPType")
        Dim txtChqDate As TextBox = grdPayment.Rows(e.RowIndex).FindControl("txtChqDate")
        Dim txtAmount As TextBox = grdPayment.Rows(e.RowIndex).FindControl("txtAmount")
        Dim txtProductid As HiddenField = grdPayment.Rows(e.RowIndex).FindControl("hdnITM_ID")
        Dim lblTCPRECID As Label = grdPayment.Rows(e.RowIndex).FindControl("lblTCP_RES_ID")
        Dim txtPayable As TextBox = grdPayment.Rows(e.RowIndex).FindControl("txtPayable")

        If Not IsNumeric(txtAmount.Text) Then lblError.Text = "enter numbers only"
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If

        Dim mrow As DataRow
        mrow = PaymentDetails.Select("ID=" & s)(0)
        mrow("TCP_ID") = TCP_id.Text
        mrow("TCP_RES_ID") = ddlPayment.SelectedValue
        mrow("RES_DESCR") = ddlPayment.SelectedItem
        mrow("TCP_CHQDATE") = txtChqDate.Text
        mrow("TCP_PAYMENTTYPE") = ddlPType.SelectedItem
        mrow("TCP_AMOUNT") = txtAmount.Text
        mrow("TCP_PAYABLETO") = txtPayable.Text

        grdPayment.EditIndex = -1
        grdPayment.ShowFooter = True
        grdPayment.DataSource = PaymentDetails
        grdPayment.DataBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            If h_emp_ids.Value = "" Then lblError.Text = "Employee Not Selected,"
            If ddlLocation.SelectedValue = "" Then lblError.Text = lblError.Text & "Select Location,"
            If txtAgent.Text = "" Then lblError.Text = lblError.Text & "select Agent details,"
            If txtLandlord.Text = "" Then lblError.Text = lblError.Text & "Select Landlord details,"
            If txtBuilding.Text = "" Then lblError.Text = lblError.Text & "Enter Unit details,"
            If txtBuildingName.Text = "" Then lblError.Text = lblError.Text & "Enter Building details,"
            If Val(txttrent.Text) = 0 Then lblError.Text = lblError.Text & "Invalid Rent,"
            If h_TCH_Type.Value = "R" Then
                If Val(txtHiredSince.Text) <= 0 Then lblError.Text = lblError.Text & "Invalid Hired Since,"
                If Val(txtRevrent.Text) <= 0 Then lblError.Text = lblError.Text & "Invalid old Rent,"
                If txtPerc.Text = "" Then lblError.Text = lblError.Text & "Invalid Incre/Decre Percentage,"
            End If

            'If ddlBudget.SelectedValue = 0 Then lblError.Text = lblError.Text & "select Budgeted Year,"

            If Val(txtPerc.Text) = 0 Then txtPerc.Text = "0"
            If Val(txtRevrent.Text) = 0 Then txtRevrent.Text = "0"
            If Val(txtHiredSince.Text) = 0 Then txtHiredSince.Text = "0"
            CheckRentalAmounts()

            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            CalculateTotal()
            Dim pParms(39) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TCH_NO", txtTCH_No.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@TCH_DATE", txtContractDate.Text, SqlDbType.VarChar)
            'If txtOldContractNo.Text <> "" Then

            If h_TCH_Type.Value <> "N" Then
                pParms(4) = Mainclass.CreateSqlParameter("@TCH_TYPE", "R", SqlDbType.VarChar)
                pParms(7) = Mainclass.CreateSqlParameter("@TCH_INCDEC", txtPerc.Text, SqlDbType.Decimal)
            Else
                pParms(4) = Mainclass.CreateSqlParameter("@TCH_TYPE", "N", SqlDbType.VarChar)
                pParms(7) = Mainclass.CreateSqlParameter("@TCH_INCDEC", "0", SqlDbType.Decimal)
            End If
            pParms(5) = Mainclass.CreateSqlParameter("@TCH_CIT_ID", h_Loc_id.Value, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@TCH_RENT", txttrent.Text, SqlDbType.Decimal)
            pParms(8) = Mainclass.CreateSqlParameter("@TCH_REVISED", txtRevrent.Text, SqlDbType.Decimal)
            pParms(9) = Mainclass.CreateSqlParameter("@TCH_OTHERS", 0, SqlDbType.Decimal)
            pParms(10) = Mainclass.CreateSqlParameter("@TCH_TOTAL", txttotal.Text, SqlDbType.Decimal)
            pParms(11) = Mainclass.CreateSqlParameter("@TCH_START", txtContractSDate.Text, SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@TCH_END", txtContractEDate.Text, SqlDbType.VarChar)
            If radBudgeted.Checked Then
                pParms(16) = Mainclass.CreateSqlParameter("@TCH_BUDGETED", "1", SqlDbType.VarChar)
            Else
                pParms(16) = Mainclass.CreateSqlParameter("@TCH_BUDGETED", "0", SqlDbType.VarChar)
            End If
            pParms(17) = Mainclass.CreateSqlParameter("@TCH_TERMS", txtTerms.Text, SqlDbType.VarChar)
            pParms(18) = Mainclass.CreateSqlParameter("@TCH_RECOVERYPROCESS", txtRecovery.Text, SqlDbType.VarChar)
            pParms(19) = Mainclass.CreateSqlParameter("@TCH_REQUESTBY", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(20) = Mainclass.CreateSqlParameter("@TCH_OCCUPANTS", h_emp_ids.Value, SqlDbType.VarChar)
            pParms(21) = Mainclass.CreateSqlParameter("@TCH_OLDCONTRACT", txtOldContractNo.Text, SqlDbType.VarChar)
            pParms(22) = Mainclass.CreateSqlParameter("@TCH_REA_ID", h_REAId.Value, SqlDbType.Int)
            pParms(23) = Mainclass.CreateSqlParameter("@TCH_REL_ID", h_RELId.Value, SqlDbType.Int)
            pParms(24) = Mainclass.CreateSqlParameter("@TCH_BSU_ID", Session("sBsuID"), SqlDbType.VarChar)
            pParms(25) = Mainclass.CreateSqlParameter("@TCH_USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(26) = Mainclass.CreateSqlParameter("@TCH_FYEAR", h_fyear.Value, SqlDbType.VarChar)
            pParms(27) = Mainclass.CreateSqlParameter("@TCH_DELETED", h_delete.Value, SqlDbType.Int)
            pParms(28) = Mainclass.CreateSqlParameter("@TCH_BUILDINGNAME", txtBuilding.Text, SqlDbType.VarChar)
            pParms(29) = Mainclass.CreateSqlParameter("@TCH_BLD_ID", h_building_ID.Value, SqlDbType.VarChar)
            pParms(30) = Mainclass.CreateSqlParameter("@TCH_RAR_ID", h_area_ID.Value, SqlDbType.VarChar)
            pParms(31) = Mainclass.CreateSqlParameter("@TCH_BUILDING", txtBuildingName.Text, SqlDbType.VarChar)
            pParms(32) = Mainclass.CreateSqlParameter("@TCH_HIREDSINCE", txtHiredSince.Text, SqlDbType.Int)
            pParms(33) = Mainclass.CreateSqlParameter("@TCH_INC_RERA", IIf(chkIncAsperRera.Checked, 1, 0), SqlDbType.Int)
            pParms(34) = Mainclass.CreateSqlParameter("@TCH_EMPDETAILS", txtEmpDetails.Text, SqlDbType.VarChar)
            pParms(35) = Mainclass.CreateSqlParameter("@TCH_MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
            pParms(36) = Mainclass.CreateSqlParameter("@TCH_GCO_INFO", ddlGCoInfo.SelectedItem.Text, SqlDbType.VarChar)
            pParms(37) = Mainclass.CreateSqlParameter("@TCH_OTHERDED", txtOtherDed.Text, SqlDbType.Decimal)
            pParms(38) = Mainclass.CreateSqlParameter("@TCH_BUDGET_FYEAR", ddlBudget.SelectedValue, SqlDbType.Int)
            pParms(39) = Mainclass.CreateSqlParameter("@TCH_NOTICE_PERIOD", txtNoticePeriod.Text, SqlDbType.VarChar)


            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTCT_H", pParms)

            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim RowCount As Integer, InsCount As Integer = 0
            For RowCount = 0 To PaymentDetails.Rows.Count - 1
                Dim iParms(8) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, PaymentDetails.Rows(RowCount)("TCP_ID"))
                If PaymentDetails.Rows(RowCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@TCP_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@TCP_TCH_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@TCP_RES_ID", PaymentDetails.Rows(RowCount)("TCP_RES_ID"), SqlDbType.Int)
                iParms(4) = Mainclass.CreateSqlParameter("@TCP_CHQDATE", PaymentDetails.Rows(RowCount)("TCP_CHQDATE"), SqlDbType.VarChar)
                iParms(5) = Mainclass.CreateSqlParameter("@TCP_AMOUNT", PaymentDetails.Rows(RowCount)("TCP_AMOUNT"), SqlDbType.Decimal)
                iParms(6) = Mainclass.CreateSqlParameter("@TCP_PAYMENTTYPE", PaymentDetails.Rows(RowCount)("TCP_PAYMENTTYPE"), SqlDbType.VarChar)
                iParms(7) = Mainclass.CreateSqlParameter("@TCP_MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
                iParms(8) = Mainclass.CreateSqlParameter("@TCP_PAYABLETO", PaymentDetails.Rows(RowCount)("TCP_PAYABLETO"), SqlDbType.VarChar)

                If Not IsDBNull(PaymentDetails.Rows(RowCount)("TCP_RES_ID")) > 0 Then
                    InsCount += 1
                    Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETCT_P", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                End If
            Next


            'Recovery Process Data Saving
            Dim RecCount As Integer, RecInsCount As Integer = 0
            For RecCount = 0 To RecoveryDetails.Rows.Count - 1
                Dim iParms(6) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, RecoveryDetails.Rows(RecCount)("TCR_ID"))
                If RecoveryDetails.Rows(RecCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@TCR_ID", RowEntryId, SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@TCR_TCH_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@TCR_RES_ID", RecoveryDetails.Rows(RecCount)("TCR_RES_ID"), SqlDbType.Int)
                iParms(4) = Mainclass.CreateSqlParameter("@TCR_CHQDATE", RecoveryDetails.Rows(RecCount)("TCR_CHQDATE"), SqlDbType.VarChar)
                iParms(5) = Mainclass.CreateSqlParameter("@TCR_AMOUNT", RecoveryDetails.Rows(RecCount)("TCR_AMOUNT"), SqlDbType.Decimal)
                iParms(6) = Mainclass.CreateSqlParameter("@TCR_PAYMENTTYPE", RecoveryDetails.Rows(RecCount)("RES_DESCR"), SqlDbType.VarChar)



                If Not IsDBNull(RecoveryDetails.Rows(RecCount)("TCR_AMOUNT")) > 0 Then
                    InsCount += 1
                    Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETCT_R", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                End If
            Next

            For Each item As DataListItem In RepService.Items
                Dim RESID As Label = CType(item.FindControl("lblRESID"), Label)
                Dim txtRESAmt As TextBox = CType(item.FindControl("txtRESAmount"), TextBox)
                Dim RowID As Label = CType(item.FindControl("lblRowId"), Label)
                Dim DParms(5) As SqlClient.SqlParameter
                If RowID.Text = "" Then RowID.Text = "0"
                DParms(1) = Mainclass.CreateSqlParameter("@TCT_ID", RowID.Text, SqlDbType.Int)
                DParms(2) = Mainclass.CreateSqlParameter("@TCT_TCH_ID", ViewState("EntryId"), SqlDbType.Int)
                DParms(3) = Mainclass.CreateSqlParameter("@TCT_RES_ID", RESID.Text, SqlDbType.Int)
                DParms(4) = Mainclass.CreateSqlParameter("@TCT_AMOUNT", IIf((txtRESAmt.Text = ""), 0, txtRESAmt.Text), SqlDbType.Decimal)
                DParms(5) = Mainclass.CreateSqlParameter("@TCT_MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)

                Dim RetValTCT_D As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETCT_D", DParms)
                If RetValTCT_D = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            Dim Empdetails As DataTable = New DataTable()
            Dim DeletedEmployees As String
            Empdetails = CType(Session("Empdata"), DataTable)
            DeletedEmployees = CType(Session("DeletedEmployees"), String)
            If Not Empdetails Is Nothing Then
                Dim RowCount1 As Integer, InsCount1 As Integer = 0
                For RowCount1 = 0 To Empdetails.Rows.Count - 1
                    Dim eParms(17) As SqlClient.SqlParameter
                    Dim rowState As Integer = ViewState("EntryId")
                    eParms(1) = Mainclass.CreateSqlParameter("@TCE_ID", Empdetails.Rows(RowCount1)("TCE_ID"), SqlDbType.Int)
                    If h_Old_EntryID.Value = 0 Then
                        eParms(2) = Mainclass.CreateSqlParameter("@TCE_TCH_ID", rowState, SqlDbType.Int)
                    Else
                        eParms(2) = Mainclass.CreateSqlParameter("@TCE_TCH_ID", ViewState("EntryId"), SqlDbType.Int)
                    End If
                    eParms(3) = Mainclass.CreateSqlParameter("@TCE_BSU_ID", Empdetails.Rows(RowCount1)("TCE_BSU_ID"), SqlDbType.VarChar)
                    eParms(4) = Mainclass.CreateSqlParameter("@TCE_EMP_ID", Empdetails.Rows(RowCount1)("TCE_EMP_ID"), SqlDbType.Int)
                    eParms(5) = Mainclass.CreateSqlParameter("@TCE_DELETED", Empdetails.Rows(RowCount1)("TCE_DELETED"), SqlDbType.Int)
                    eParms(6) = Mainclass.CreateSqlParameter("@TCE_REU_ID", Empdetails.Rows(RowCount1)("TCE_REU_ID"), SqlDbType.Int)
                    eParms(7) = Mainclass.CreateSqlParameter("@TCE_EREU_ID", Empdetails.Rows(RowCount1)("TCE_EREU_ID"), SqlDbType.Int)
                    eParms(8) = Mainclass.CreateSqlParameter("@TCE_LAMOUNT", Empdetails.Rows(RowCount1)("TCE_LAMOUNT"), SqlDbType.Decimal)
                    eParms(9) = Mainclass.CreateSqlParameter("@TCE_EAMOUNT", Empdetails.Rows(RowCount1)("TCE_EAMOUNT"), SqlDbType.Decimal)
                    eParms(10) = Mainclass.CreateSqlParameter("@TCE_DAMOUNT", Empdetails.Rows(RowCount1)("TCE_DAMOUNT"), SqlDbType.Decimal)
                    eParms(11) = Mainclass.CreateSqlParameter("@TCE_DES_ID", Empdetails.Rows(RowCount1)("TCE_DES_ID"), SqlDbType.VarChar)
                    eParms(12) = Mainclass.CreateSqlParameter("@TCE_MSTATUS", Empdetails.Rows(RowCount1)("TCE_MSTATUS"), SqlDbType.VarChar)
                    eParms(13) = Mainclass.CreateSqlParameter("@TCE_DEPENDANTS", Empdetails.Rows(RowCount1)("TCE_DEPENDANTS"), SqlDbType.Int)
                    eParms(14) = Mainclass.CreateSqlParameter("@TCE_FURN_ID", Empdetails.Rows(RowCount1)("TCE_FURN_ID"), SqlDbType.Int)
                    eParms(15) = Mainclass.CreateSqlParameter("@TCE_MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
                    eParms(16) = Mainclass.CreateSqlParameter("@TCE_UNITNO", Empdetails.Rows(RowCount1)("TCE_UNITNO"), SqlDbType.VarChar)
                    eParms(17) = Mainclass.CreateSqlParameter("@TCE_OLDRENT", Empdetails.Rows(RowCount1)("TCE_OLDRENT"), SqlDbType.Decimal)


                    If Not IsDBNull(Empdetails.Rows(RowCount1)("TCE_TCH_ID")) Then
                        InsCount1 += 1
                        Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETCT_E", eParms)
                        If RetValFooter = "-1" Then
                            lblError.Text = "Unexpected Error !!!"
                            stTrans.Rollback()
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If DeletedEmployees <> "" Then
                Dim deleteEmps() As String = DeletedEmployees.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteEmps.GetUpperBound(0) - 1
                    iParms(1) = Mainclass.CreateSqlParameter("@TCE_ID", deleteEmps(RowCount), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE TCT_E SET TCE_DELETED=1   where TCE_ID= @TCE_ID", iParms)

                    If RetValFooter = "-1" Then
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            If hGridDelete.Value <> "" Then
                Dim deleteId() As String = hGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0) - 1
                    iParms(1) = Mainclass.CreateSqlParameter("@TCP_ID", deleteId(RowCount), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE TCT_P SET TCP_DELETED=1   where TCP_ID=@TCP_ID", iParms)
                    If RetValFooter = "-1" Then
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If
            hGridDelete.Value = ""
            DeletedEmployees = ""
            stTrans.Commit()
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

        If ViewState("MainMnu_code") = "PI02035" Then
            writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), txtComments.Text, 0, "A")
        Else
            writeWorkFlow(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), txtComments.Text, 0, "N")
        End If

        SetDataMode("view")
        setModifyvalues(ViewState("EntryId"))
        lblError.Text = "Data Saved Successfully !!!"
    End Sub
    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        btnSend.Visible = Not btnSave.Visible
        btnPrint.Visible = Not btnSave.Visible
        btnReturn.Visible = Not btnSave.Visible
        grdPayment.DataSource = PaymentDetails
        grdPayment.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        Session("Empdata") = Nothing
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdPayment.DataSource = PaymentDetails
        grdPayment.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(h_EntryId.Value)
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnRenew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRenew.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        RenewContract()
    End Sub
    Private Sub RenewContract()
        trRentalInc.Visible = True
        txtOldContractNo.Text = txtTCH_No.Text
        txtTCH_No.Text = "New"

        h_TCH_Type.Value = "R"
        h_fyear.Value = "2016"
        txtTerms.Text = ""
        txtEmpDetails.Text = ""
        txtRecovery.Text = ""
        h_Old_EntryID.Value = h_EntryId.Value
        h_EntryId.Value = 0
        txtRevrent.Text = txttrent.Text
        Dim RowCount As Integer = 0
        For RowCount = 0 To PaymentDetails.Rows.Count - 1
            If PaymentDetails.Rows(RowCount)("TCP_CHQDATE") <> "" Then
                PaymentDetails.Rows(RowCount)("TCP_CHQDATE") = Format(DateAdd(DateInterval.Year, 1, CDate(PaymentDetails.Rows(RowCount)("TCP_CHQDATE"))), "dd-MMM-yyyy")
            End If
            PaymentDetails.Rows(RowCount)("TCP_ID") = 0
            PaymentDetails.Rows(RowCount)("TCP_TCH_ID") = 0
        Next
        grdPayment.DataSource = PaymentDetails
        grdPayment.DataBind()
        showNoRecordsFound()
        txtContractDate.Text = Now.ToString("dd/MMM/yyyy")
        txtContractSDate.Text = Format(DateAdd(DateInterval.Day, 1, CDate(txtContractEDate.Text)), "dd/MMM/yyyy")
        txtContractEDate.Text = Format(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Year, 1, CDate(txtContractSDate.Text))), "dd/MMM/yyyy")
        DisplayServices(h_Loc_id.Value)
        CalculateTotal()
        btnRenew.Visible = False
        trApproval.Visible = False
        trWorkflow.Visible = False
        trFlow.Visible = False
        trDocument.Visible = False
        trUpload.Visible = False
        Dim Empdetails As DataTable = New DataTable()
        Dim Fillsql As String = "SELECT 0 TCE_ID, TCE_BSU_ID, TCE_EMP_ID,TCE_DELETED, TCE_REU_ID, TCE_LAMOUNT, TCE_EREU_ID, TCE_EAMOUNT, TCE_DAMOUNT, 0 TCE_TCH_ID,case when TCE_EMP_ID=0 then BSU_NAME  else EMPNAME end EMPNAME,BSU_SHORTNAME BSUSHORT,U1.REU_NAME UNITTYPE,U2.REU_NAME EUNITTYPE,TCE_MSTATUS,TCE_DES_ID,DES_DESCR DESIGNATION,TCE_DEPENDANTS,TCE_FURN_ID,FURN_DESCR,TCE_UNITNO,TCE_OLDRENT  FROM TCT_E LEFT OUTER JOIN OASIS..vw_OSO_EMPLOYEEMASTERDETAILS  ON EMP_ID=TCE_EMP_ID  LEFT OUTER JOIN VW_FURNISHINGMASTER  ON FURN_ID=TCE_FURN_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TCE_BSU_ID LEFT OUTER JOIN REUNITTYPE U1 on U1.REU_ID=TCE_REU_ID LEFT OUTER JOIN REUNITTYPE U2 on U2.REU_ID=TCE_EREU_ID LEFT OUTER JOIN OASIS..EMPDESIGNATION_M ON DES_ID =TCE_DES_ID  where TCE_TCH_ID=" & h_Old_EntryID.Value & " and TCE_DELETED=0 order by TCE_ID"
        Empdetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, Fillsql).Tables(0)


        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,0 RowId,0 PERC    from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & h_Old_EntryID.Value & " where  REC_CIT_ID in(select RCM_ID FROM RECITY_M WHERE RCM_DESCR='ALL') AND RES_DELETED<>1   UNION ALL select  CASE WHEN REC_PERCENT=1 THEN RES_DESCR+' ('+ CAST(convert(int,rec_default) as varchar(max)) +' % )' ELSE RES_DESCR END DESCR,ISNULL(isnull(TCT_AMOUNT,REC_DEFAULT),0) AMOUNT, RES_ID ID,0 RowId,0 PERC     from RESERVICES left outer join tct_D on tct_res_id=res_id and TCT_tch_id=" & h_Old_EntryID.Value & " where  REC_CIT_ID ='" & h_Loc_id.Value & "' AND RES_DELETED<>1  order by ID").Tables(0)
        RepService.DataSource = dt
        RepService.DataBind()


        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(Empdetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        Empdetails = mtable

        Session("Empdata") = Empdetails
        Exit Sub
    End Sub
    Protected Sub txtPerc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPerc.TextChanged
        txtRevrent.Text = Val(txttrent.Text) + Val(txttrent.Text) * Val(txtPerc.Text) / 100
        CalculateTotal()
    End Sub
    Protected Sub txttrent_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txttrent.TextChanged
        txtRevrent.Text = Val(txttrent.Text) + Val(txttrent.Text) * Val(txtPerc.Text) / 100
        CalculateTotal()



    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim cmd As New SqlCommand
        If ddlLocation.SelectedItem.Text = "DUBAI" Then
            cmd.CommandText = "rptTenancyContractDXB"
        ElseIf ddlLocation.SelectedItem.Text = "SHARJAH" Then
            cmd.CommandText = "rptTenancyContractSHJ"
        ElseIf ddlLocation.SelectedItem.Text = "ABU DHABI" Then
            cmd.CommandText = "rptTenancyContractAUH"
        ElseIf ddlLocation.SelectedItem.Text = "RAS AL KHAIMAH" Then
            cmd.CommandText = "rptTenancyContractRAK"
        ElseIf ddlLocation.SelectedItem.Text = "FUJAIRAH" Then
            cmd.CommandText = "rptTenancyContractFUJ"
        ElseIf ddlLocation.SelectedItem.Text = "AJMAN" Then
            cmd.CommandText = "rptTenancyContractAJM"
        ElseIf ddlLocation.SelectedItem.Text = "AL AIN" Then
            cmd.CommandText = "rptTenancyContractAAN"
        End If
        Dim sqlParam(1) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int)
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = Mainclass.CreateSqlParameter("@LOCID", h_Loc_id.Value, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(1))
        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        'V1.2 comments start------------
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        If h_TCH_Type.Value = "N" Then
            params("reportCaption") = "New Tenancy Contract For Staff Accomodation"
        Else
            params("reportCaption") = "Renewal Tenancy Contract For Staff Accomodation"
        End If
        Dim repSource As New MyReportClass
        repSource.Command = cmd
        repSource.Parameter = params
        If ddlLocation.SelectedItem.Text = "DUBAI" Then
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractDXB.rpt"
        ElseIf ddlLocation.SelectedItem.Text = "SHARJAH" Then
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractSHJ.rpt"
        ElseIf ddlLocation.SelectedItem.Text = "ABU DHABI" Then
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractAUH.rpt"
        ElseIf ddlLocation.SelectedItem.Text = "RAS AL KHAIMAH" Then
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractRAK.rpt"
        ElseIf ddlLocation.SelectedItem.Text = "FUJAIRAH" Then
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractFUJ.rpt"
        ElseIf ddlLocation.SelectedItem.Text = "AJMAN" Then
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractAJM.rpt"
        ElseIf ddlLocation.SelectedItem.Text = "AL AIN" Then
            repSource.ResourceName = "../../Inventory/Reports/RPT/rptTenancyContractAAN.rpt"
        End If
        Dim subRep(0) As MyReportClass
        subRep(0) = New MyReportClass
        Dim subcmd1 As New SqlCommand
        subcmd1.CommandText = "rptTenancyPaymentDetails"
        Dim sqlParam1(0) As SqlParameter
        sqlParam1(0) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int)
        subcmd1.Parameters.Add(sqlParam1(0))
        subcmd1.CommandType = Data.CommandType.StoredProcedure
        subcmd1.Connection = New SqlConnection(str_conn)
        subRep(0).Command = subcmd1
        subRep(0).ResourceName = "wBTA_Sector"
        subRep(0).Command = subcmd1
        subRep(0).ResourceName = "wBTA_Sector"
        repSource.IncludeBSUImage = True
        repSource.SubReport = subRep
        Session("ReportSource") = repSource
        'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Protected Sub btnTCNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTCNew.Click
        h_TCH_Type.Value = "N"
        pgTitle.Text = "New Tenancy Contract For Staff Accommodation"
        txtPerc.Text = "0"
        trRentalInc.Visible = False
        showNoRecordsFound()
        BindDocuments("N")
    End Sub
    Protected Sub btnTCReNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTCReNew.Click
        h_TCH_Type.Value = "R"
        pgTitle.Text = "Renewal Tenancy Contract For Staff Accommodation"
        trRentalInc.Visible = True
        showNoRecordsFound()
        BindDocuments("R")
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update TCT_H set TCH_DELETED=1, TCH_STATUS='D', TCH_STATUS_STR='Deleted' where TCH_ID=@TCH_ID"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                writeWorkFlow(ViewState("EntryId"), "Deleted", txtApprovals.Text, 0, "D")
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        doApproval()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Return Comments are Mandatory"
            Exit Sub
        End If
        If writeWorkFlow(ViewState("EntryId"), "Returned", txtApprovals.Text, h_Tch_apr_id.Value, "B") = 0 Then
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_STATUS_STR='Returned:by " & Session("sUsr_name") & "', TCH_STATUS='N', TCH_APR_ID=0 where TCH_ID=" & h_EntryId.Value)
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Function doApproval(Optional ByVal tch_id As Integer = 0) As Integer
        If tch_id = 0 Then tch_id = h_EntryId.Value
        Dim strApprover() As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverTCT(" & tch_id & ")").ToString.Split(";")
        doApproval = -1
        If strApprover(0) = 500 Then 'finally approved
            doApproval = writeWorkFlow(tch_id, "Tenancy", "Tenancy Contract archived", 0, "C")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_APR_ID=500, TCH_STATUS='C', TCH_STATUS_STR='Tenancy:Archived' where TCH_ID=" & tch_id)
            End If
        Else
            doApproval = writeWorkFlow(tch_id, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S")
            If doApproval = 0 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_STATUS_STR='Approval:with " & strApprover(1) & "', TCH_STATUS=case when TCH_STATUS='N' then 'S' else TCH_STATUS end, TCH_APR_ID=" & strApprover(0) & " where tch_id=" & tch_id)
            End If
        End If
    End Function
    Private Function writeWorkFlow(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TCH_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", txtApprovals.Text.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveWorkFlowTCT", iParms)
        writeWorkFlow = iParms(8).Value
    End Function
    Protected Sub RepService_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles RepService.ItemDataBound
        Dim txtRESAmount As TextBox = CType(e.Item.FindControl("txtRESAmount"), TextBox)
        txtRESAmount.Attributes.Add("OnKeyUp", "javascript: return calculateRent();")
        ClientScript.RegisterArrayDeclaration("wtgBP", String.Concat("'", txtRESAmount.ClientID, "'"))

    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Tenancy\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename
            If ViewState("MainMnu_code") <> "PI02035" Then
                If h_TCH_Status.Value = "C" Then
                    lblError.Text = "upload not allowed for Archived Contracts!!!!"
                    Exit Sub
                End If
            End If

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Dim sqlParam(6) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@TCI_ID", 0, SqlDbType.Int, True)
                sqlParam(1) = Mainclass.CreateSqlParameter("@TCI_TCH_ID", h_EntryId.Value, SqlDbType.Int)
                sqlParam(2) = Mainclass.CreateSqlParameter("@TCI_CONTENTTYPE", ContentType, SqlDbType.VarChar)
                sqlParam(3) = Mainclass.CreateSqlParameter("@TCI_DOCTYPE", ddlDocType.SelectedValue, SqlDbType.Int)
                sqlParam(4) = Mainclass.CreateSqlParameter("@TCI_NAME", UploadDocPhoto.FileName, SqlDbType.VarChar)
                sqlParam(5) = Mainclass.CreateSqlParameter("@TCI_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                sqlParam(6) = Mainclass.CreateSqlParameter("@TCI_FILENAME", txtTCH_No.Text & "_~" & FileExt, SqlDbType.VarChar)
                Try
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveTCT_I", sqlParam)
                    IO.File.Move(strFilepath, str_img & txtTCH_No.Text & "_" & sqlParam(0).Value & FileExt)
                    showImages()
                    writeWorkFlow(ViewState("EntryId"), "ADDEDIMG", "Added Contract " & UploadDocPhoto.FileName, 0, "O")
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
        trComments.Focus()
    End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select

    End Function
    Protected Sub rptImages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptImages.ItemCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.CommandSource.text = "[x]" Then
                If h_TCH_Status.Value = "C" Or h_TCH_Status.Value = "R" Or h_TCH_Status.Value = "D" Then
                    lblError.Text = "Cannot delete file"
                Else
                    Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete"), LinkButton)
                    Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
                    Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from tct_i where tci_id=" & lblPriId.Text)
                    writeWorkFlow(ViewState("EntryId"), "DELETEDIMG", "Deleted Contract " & btnDocumentLink.Text, 0, "O")
                    showImages()
                End If
            End If
        End If
    End Sub
    Protected Sub rptImages_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptImages.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
            Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriId"), TextBox)
            Dim lblPriFilename As TextBox = CType(e.Item.FindControl("lblPriFilename"), TextBox)
            Dim lblPriContentType As TextBox = CType(e.Item.FindControl("lblPriContentType"), TextBox)
            btnDocumentLink.Attributes.Add("OnClick", "javascript: showDocument(" & lblPriId.Text & ",'" & lblPriFilename.Text & "','" & lblPriContentType.Text & "'); return false;")
        End If
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Rejection Comments are Mandatory"
            Exit Sub
        End If
        If writeWorkFlow(ViewState("EntryId"), "Rejected", txtApprovals.Text, h_Tch_apr_id.Value, "R") = 0 Then
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TCT_H set TCH_STATUS_STR='Rejected:by " & Session("sUsr_name") & "', TCH_STATUS='R' where TCH_ID=" & h_EntryId.Value)
        End If
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If trVisited.Visible And (txtVisitor.Text.Trim.Length < 5 Or txtVisitDate.Text.Length < 5) Then
            lblError.Text = "Enter site visit details"
        Else
            If h_Tch_apr_id.Value = 20 Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update tct_h set TCH_MSO='" & lblMSO.Text & "', TCH_VIEWBY='" & txtVisitor.Text & "', TCH_VIEWDATE='" & txtVisitDate.Text & "' where tch_id=" & h_EntryId.Value)
            End If
            If doApproval() = 0 Then
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub
    Protected Sub btnMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMessage.Click
        If txtApprovals.Text.Trim = "" Then
            lblError.Text = "Message comments are Mandatory"
            Exit Sub
        End If
        writeWorkFlow(ViewState("EntryId"), "Message", txtApprovals.Text, h_Tch_apr_id.Value, "M")
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub txtRevrent_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRevrent.TextChanged
        txtPerc.Text = Convert.ToDouble((Val(txttrent.Text) - Val(txtRevrent.Text)) / Val(txtRevrent.Text) * 100).ToString("######0.00")
        'txtPerc.Text = Convert.ToDouble(((Val(txttrent.Text) / h_EmpCount.Value) - Val(txtRevrent.Text)) / Val(txtRevrent.Text) * 100).ToString("######0.00")
    End Sub
    Protected Sub btnCreatePayments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreatePayments.Click
        Dim EMIAmount As Long = 0.0
        Dim EMIs As Long = 0.0
        Dim noofpayments As Integer = 0
        Dim Rowcount As Integer = 0
        Dim Count As Integer = 0
        Dim RemAmount As Long = 0.0

        If IsNumeric(txtnoofPayments.Text) And Val(txtnoofPayments.Text) > 0 Then
            If PaymentDetails.Rows(0)(1) <> -1 Then
                For Each mrow As DataRow In PaymentDetails.Rows
                    If UCase(mrow("RES_DESCR")) = "RENTAL" Then
                        hGridDelete.Value = hGridDelete.Value & mrow("TCP_ID") & ";"
                        mrow.Delete()
                    End If
                Next
                PaymentDetails.AcceptChanges()
                If PaymentDetails.Rows.Count = 0 Then
                    PaymentDetails.Rows.Add(PaymentDetails.NewRow())
                    PaymentDetails.Rows(0)(1) = -1
                End If
            End If
            EMIAmount = IIf(IsNumeric(txttrent.Text), Val(txttrent.Text) / Val(txtnoofPayments.Text), 0)
            RemAmount = (Val(txttrent.Text)) - (EMIAmount * Val(txtnoofPayments.Text))
            EMIs = 12 / Val(txtnoofPayments.Text)
            For Rowcount = 0 To Val(txtnoofPayments.Text) - 1
                If PaymentDetails.Rows(0)(1) = -1 Then PaymentDetails.Rows.RemoveAt(0)
                Dim mrow As DataRow
                mrow = PaymentDetails.NewRow
                mrow("TCP_ID") = 0
                mrow("TCP_RES_ID") = 0
                mrow("RES_DESCR") = "Rental"
                mrow("TCP_PAYABLETO") = h_PayName.Value


                mrow("TCP_CHQDATE") = DateAdd(DateInterval.Month, Rowcount * EMIs, CDate(txtPSDate.Text)).ToString("dd/MMM/yyyy")
                If Rowcount = Val(txtnoofPayments.Text) - 1 Then
                    mrow("TCP_AMOUNT") = EMIAmount + RemAmount
                Else
                    mrow("TCP_AMOUNT") = EMIAmount
                End If

                mrow("TCP_PAYMENTTYPE") = "Cheque"
                PaymentDetails.Rows.Add(mrow)
            Next
            PaymentDetails.AcceptChanges()
            If PaymentDetails.Rows.Count = 0 Then
                PaymentDetails.Rows.Add(PaymentDetails.NewRow())
                PaymentDetails.Rows(0)(1) = -1
            End If
        End If
        grdPayment.EditIndex = -1
        grdPayment.DataSource = PaymentDetails
        grdPayment.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnRecall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecall.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update TCT_H set TCH_DELETED=0, TCH_STATUS='N', TCH_STATUS_STR='RECALL', TCH_APR_ID=0 where TCH_ID=@TCH_ID"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@TCH_ID", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
                writeWorkFlow(ViewState("EntryId"), "Recalled", txtApprovals.Text, 0, "E")
                Response.Redirect(ViewState("ReferrerUrl"), False)
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnUpload1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload1.Click
        If UploadDocument.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Tenancy\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocument.FileName
            Dim strFilepath As String = str_img & str_tempfilename
            If ViewState("MainMnu_code") <> "PI02035" Then
                If h_TCH_Status.Value <> "C" Then
                    lblError.Text = "upload not allowed for Non Archived Contracts!!!!"
                    Exit Sub
                End If
            End If

            Try
                UploadDocument.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Dim sqlParam(6) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@TCI_ID", 0, SqlDbType.Int, True)
                sqlParam(1) = Mainclass.CreateSqlParameter("@TCI_TCH_ID", h_EntryId.Value, SqlDbType.Int)
                sqlParam(2) = Mainclass.CreateSqlParameter("@TCI_CONTENTTYPE", ContentType, SqlDbType.VarChar)
                sqlParam(3) = Mainclass.CreateSqlParameter("@TCI_DOCTYPE", ddlUploadType.SelectedValue, SqlDbType.Int)
                sqlParam(4) = Mainclass.CreateSqlParameter("@TCI_NAME", UploadDocument.FileName, SqlDbType.VarChar)
                sqlParam(5) = Mainclass.CreateSqlParameter("@TCI_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                sqlParam(6) = Mainclass.CreateSqlParameter("@TCI_FILENAME", txtTCH_No.Text & "_~" & FileExt, SqlDbType.VarChar)
                Try
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "saveTCT_I", sqlParam)
                    IO.File.Move(strFilepath, str_img & txtTCH_No.Text & "_" & sqlParam(0).Value & FileExt)
                    showImagesDocument()
                    writeWorkFlow(ViewState("EntryId"), "ADDEDIMG", "Added Contract " & UploadDocument.FileName, 0, "O")
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If

    End Sub
    Protected Sub rptDocument_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDocument.ItemCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.CommandSource.text = "[x]" Then
                If h_TCH_Status.Value = "C" Or h_TCH_Status.Value = "R" Or h_TCH_Status.Value = "D" Then
                    lblError.Text = "Cannot delete file"
                Else
                    Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete1"), LinkButton)
                    Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriDocumentId"), TextBox)
                    Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink1"), LinkButton)
                    SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from tct_i where tci_id=" & lblPriId.Text)
                    writeWorkFlow(ViewState("EntryId"), "DELETEDIMG", "Deleted Contract " & btnDocumentLink.Text, 0, "O")
                    showImagesDocument()
                End If
            End If
        End If
    End Sub
    Protected Sub rptDocument_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDocument.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink1"), LinkButton)
            Dim lblPriId As TextBox = CType(e.Item.FindControl("lblPriDocumentId"), TextBox)
            Dim lblPriFilename As TextBox = CType(e.Item.FindControl("lblPriDocumentFilename"), TextBox)
            Dim lblPriContentType As TextBox = CType(e.Item.FindControl("lblPriDocumentContentType"), TextBox)
            btnDocumentLink.Attributes.Add("OnClick", "javascript: showDocument(" & lblPriId.Text & ",'" & lblPriFilename.Text & "','" & lblPriContentType.Text & "'); return false;")
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub btnRecoverypayment_Click(sender As Object, e As EventArgs) Handles btnRecoverypayment.Click

        Dim EMIAmount As Long = 0.0
        Dim EMIs As Long = 0.0
        Dim noofpayments As Integer = 0
        Dim Rowcount As Integer = 0
        Dim Count As Integer = 0
        Dim RemAmount As Long = 0.0


        If IsNumeric(txtRecnoofPayments.Text) And Val(txtRecnoofPayments.Text) > 0 Then
            If RecoveryDetails.Rows(0)(1) <> -1 Then
                For Each mrow As DataRow In RecoveryDetails.Rows
                    If UCase(mrow("RES_DESCR")) = "Salary" Then
                        hGridDelete.Value = hGridDelete.Value & mrow("TCR_ID") & ";"
                        mrow.Delete()
                    End If
                Next
                RecoveryDetails.AcceptChanges()
                If RecoveryDetails.Rows.Count = 0 Then
                    RecoveryDetails.Rows.Add(RecoveryDetails.NewRow())
                    RecoveryDetails.Rows(0)(1) = -1
                End If
            End If



            EMIAmount = Math.Abs(IIf(IsNumeric(txtEDed.Text), Val(txtEDed.Text) / Val(txtRecnoofPayments.Text), 0))
            'EMIAmount = 5000
            RemAmount = (Val(txtEDed.Text)) - (EMIAmount * Val(txtRecnoofPayments.Text))
            'EMIs = Val(txtRecnoofPayments.Text)
            'EMIs = 1
            For Rowcount = 0 To Val(txtRecnoofPayments.Text) - 1
                If RecoveryDetails.Rows(0)(1) = -1 Then RecoveryDetails.Rows.RemoveAt(0)
                Dim mrow As DataRow
                mrow = RecoveryDetails.NewRow
                mrow("TCR_ID") = 0
                mrow("TCR_RES_ID") = 0
                mrow("RES_DESCR") = "Salary"
                'mrow("TCR_CHQDATE") = DateAdd(DateInterval.Month, Rowcount + 1, CDate(txtRecPSDate.Text)).ToString("dd/MMM/yyyy")
                mrow("TCR_CHQDATE") = DateAdd(DateInterval.Month, Rowcount, CDate(txtRecPSDate.Text)).ToString("dd/MMM/yyyy")

                If Rowcount = Val(txtnoofPayments.Text) - 1 Then
                    mrow("TCR_AMOUNT") = EMIAmount + RemAmount
                Else
                    mrow("TCR_AMOUNT") = EMIAmount
                End If
                RecoveryDetails.Rows.Add(mrow)
            Next
            RecoveryDetails.AcceptChanges()
            If RecoveryDetails.Rows.Count = 0 Then
                RecoveryDetails.Rows.Add(RecoveryDetails.NewRow())
                RecoveryDetails.Rows(0)(1) = -1
            End If
        End If
        GrdRec.EditIndex = -1
        GrdRec.DataSource = RecoveryDetails
        GrdRec.DataBind()
        showNoRecordsFound1()
    End Sub


    'Private Sub GrdRecovery_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdRecovery.RowCommand
    '    Try
    '        If e.CommandName = "AddNew" Then
    '            Dim txtRChqDate As TextBox = GrdRecovery.FooterRow.FindControl("txtRChqDate")
    '            Dim txtAmount As TextBox = GrdRecovery.FooterRow.FindControl("txtRAmount")
    '            Dim txtProductid As HiddenField = GrdRecovery.FooterRow.FindControl("h_prod_id")
    '            Dim ddlPayment As DropDownList = GrdRecovery.FooterRow.FindControl("ddlRecoveryType")
    '            Dim txtPayable As TextBox = GrdRecovery.FooterRow.FindControl("txtRPayable")

    '            If lblError.Text.Length > 0 Then
    '                showNoRecordsFound()
    '                Exit Sub
    '            End If
    '            If RecoveryDetails.Rows(0)(1) = -1 Then RecoveryDetails.Rows.RemoveAt(0)
    '            Dim mrow As DataRow
    '            mrow = RecoveryDetails.NewRow
    '            mrow("TCR_ID") = 0
    '            mrow("TCR_RES_ID") = ddlPayment.SelectedValue
    '            mrow("TCR_CHQDATE") = IIf(txtRChqDate.Text = "", "01/Jan/1900", txtRChqDate.Text)
    '            mrow("TCR_AMOUNT") = txtAmount.Text
    '            mrow("RES_DESCR") = ddlPayment.SelectedItem.Text
    '            'mrow("TCP_PAYABLETO") = txtPayable.Text
    '            RecoveryDetails.Rows.Add(mrow)
    '            GrdRecovery.EditIndex = -1
    '            GrdRecovery.DataSource = RecoveryDetails
    '            GrdRecovery.DataBind()
    '            showNoRecordsFound1()
    '        Else

    '            Dim txtInv_Qty As TextBox = GrdRecovery.FooterRow.FindControl("txtRChqDate")
    '            Dim txtInv_Amt As TextBox = GrdRecovery.FooterRow.FindControl("txtRAMOUNT")
    '            Dim ddlPayment As DropDownList = GrdRecovery.FooterRow.FindControl("ddlRecoveryType")


    '        End If
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try

    'End Sub

    'Private Sub GrdRecovery_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GrdRecovery.RowDataBound
    '    If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then
    '        Try
    '            If e.Row.RowType = DataControlRowType.Footer Then
    '                Dim txtChqDate_F As TextBox = e.Row.FindControl("txtRChqDate")
    '                Dim txtAmount_F As TextBox = e.Row.FindControl("txtRAMOUNT")
    '                Dim imgchqDate As ImageButton = e.Row.FindControl("imgFRChqDate")
    '                Dim ddlPType As DropDownList = e.Row.FindControl("ddlRecoveryType")
    '            End If
    '            If GrdRecovery.ShowFooter Or GrdRecovery.EditIndex > -1 Then
    '                Dim ddlRType As DropDownList = e.Row.FindControl("ddlRecoveryType")
    '                If Not ddlRType Is Nothing Then
    '                    Dim hProducts As HiddenField = e.Row.FindControl("hProducts")
    '                    Dim sqlStr1 As String = ""
    '                    sqlStr1 &= "SELECT '1' PT_ID,'Cash' TCP_PAYMENTTYPE UNION ALL select '2' PT_ID,'Cheque' TCP_PAYMENTTYPE UNION ALL select '3' PT_ID,'Salary' TCP_PAYMENTTYPE "
    '                    fillDropdown(ddlRType, sqlStr1, "TCP_PAYMENTTYPE", "PT_ID", False)
    '                End If
    '            End If
    '        Catch ex As Exception
    '            lblError.Text = ex.Message
    '        End Try

    '    End If
    'End Sub
    'Private Sub GrdRecovery_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GrdRecovery.RowUpdating
    '    Try
    '        Dim s As String = GrdRecovery.DataKeys(e.RowIndex).Value.ToString()
    '        Dim TCP_id As Label = GrdRecovery.Rows(e.RowIndex).FindControl("lblRCP_ID")
    '        Dim ddlPType As DropDownList = GrdRecovery.Rows(e.RowIndex).FindControl("ddlPType")
    '        Dim txtChqDate As TextBox = GrdRecovery.Rows(e.RowIndex).FindControl("txtRChqDate")
    '        Dim txtAmount As TextBox = GrdRecovery.Rows(e.RowIndex).FindControl("txtRAmount")
    '        Dim txtPayable As TextBox = GrdRecovery.Rows(e.RowIndex).FindControl("txtRPayable")

    '        If Not IsNumeric(txtAmount.Text) Then lblError.Text = "enter numbers only"
    '        If lblError.Text.Length > 0 Then
    '            showNoRecordsFound1()
    '            Exit Sub
    '        End If

    '        Dim mrow As DataRow
    '        mrow = RecoveryDetails.Select("ID=" & s)(0)
    '        mrow("TCR_ID") = TCP_id.Text
    '        mrow("RES_DESCR") = "Recovery"
    '        mrow("TCR_CHQDATE") = txtChqDate.Text
    '        mrow("TCR_PAYMENTTYPE") = ddlPType.SelectedItem
    '        mrow("TCR_AMOUNT") = txtAmount.Text
    '        mrow("TCR_PAYABLETO") = txtPayable.Text

    '        GrdRecovery.EditIndex = -1
    '        GrdRecovery.ShowFooter = True
    '        GrdRecovery.DataSource = RecoveryDetails
    '        GrdRecovery.DataBind()
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try

    'End Sub
    'Private Sub GrdRecovery_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GrdRecovery.RowEditing
    '    Try
    '        GrdRecovery.ShowFooter = False
    '        GrdRecovery.EditIndex = e.NewEditIndex
    '        GrdRecovery.DataSource = RecoveryDetails
    '        GrdRecovery.DataBind()
    '        Dim txtInv_Rate_F As TextBox = GrdRecovery.Rows(e.NewEditIndex).FindControl("txtRChqDate")
    '        Dim txtInv_Qty_F As TextBox = GrdRecovery.Rows(e.NewEditIndex).FindControl("txtRAmount")
    '        Dim h_res_id As HiddenField = GrdRecovery.Rows(e.NewEditIndex).FindControl("hdnRITM_ID")
    '        Dim ddlProduct As DropDownList = GrdRecovery.Rows(e.NewEditIndex).FindControl("ddlRecoveryType")
    '        ddlProduct.Items.FindByValue(h_res_id.Value).Selected = True
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub
    Protected Sub grdPayment_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdPayment.RowEditing
        Try
            grdPayment.ShowFooter = False
            grdPayment.EditIndex = e.NewEditIndex
            grdPayment.DataSource = PaymentDetails
            grdPayment.DataBind()
            Dim txtInv_Rate_F As TextBox = grdPayment.Rows(e.NewEditIndex).FindControl("txtChqDate")
            Dim txtInv_Qty_F As TextBox = grdPayment.Rows(e.NewEditIndex).FindControl("txtAmount")
            Dim h_Service_ID_F As HiddenField = grdPayment.Rows(e.NewEditIndex).FindControl("hdnITM_ID")
            Dim h_ITEMTYPE As HiddenField = grdPayment.Rows(e.NewEditIndex).FindControl("hdnITM_TYPE")
            Dim ddlProduct As DropDownList = grdPayment.Rows(e.NewEditIndex).FindControl("ddlPaymentType")
            Dim ddlPayment As DropDownList = grdPayment.Rows(e.NewEditIndex).FindControl("ddlPType")
            Dim txtPayable_F As TextBox = grdPayment.Rows(e.NewEditIndex).FindControl("txtPayable")
            ddlProduct.Items.FindByValue(h_Service_ID_F.Value).Selected = True
            ddlPayment.Items.FindByText(h_ITEMTYPE.Value).Selected = True
        Catch ex As Exception

        End Try
    End Sub


    'Private Sub GrdRecovery_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GrdRecovery.RowDeleting

    '    Try
    '        Dim mRow() As DataRow = RecoveryDetails.Select("ID=" & GrdRecovery.DataKeys(e.RowIndex).Values(0), "")
    '        If mRow.Length > 0 Then
    '            hGridDelete.Value &= mRow(0)("TCR_ID") & ";"
    '            RecoveryDetails.Select("ID=" & GrdRecovery.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
    '            RecoveryDetails.AcceptChanges()
    '        End If
    '        If RecoveryDetails.Rows.Count = 0 Then
    '            RecoveryDetails.Rows.Add(RecoveryDetails.NewRow())
    '            RecoveryDetails.Rows(0)(1) = -1
    '        End If

    '        GrdRecovery.DataSource = RecoveryDetails
    '        GrdRecovery.DataBind()
    '        showNoRecordsFound1()
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try

    'End Sub

    Private Sub GrdRec_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles GrdRec.RowCancelingEdit
        GrdRec.ShowFooter = True
        GrdRec.EditIndex = -1
        GrdRec.DataSource = RecoveryDetails
        GrdRec.DataBind()
    End Sub

    Private Sub GrdRec_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrdRec.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtChqDate As TextBox = GrdRec.FooterRow.FindControl("txtRChqDate")
            Dim txtAmount As TextBox = GrdRec.FooterRow.FindControl("txtAmount")
            Dim txtProductid As HiddenField = GrdRec.FooterRow.FindControl("h_prod_id")
            Dim ddlPayment As DropDownList = GrdRec.FooterRow.FindControl("ddlPaymentType")
            Dim ddlPType As DropDownList = GrdRec.FooterRow.FindControl("ddlPType")


            If lblError.Text.Length > 0 Then
                showNoRecordsFound1()
                Exit Sub
            End If




            If RecoveryDetails.Rows(0)(1) = -1 Then RecoveryDetails.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = RecoveryDetails.NewRow
            mrow("TCR_ID") = 0
            mrow("TCR_RES_ID") = ddlPayment.SelectedValue
            mrow("RES_DESCR") = ddlPayment.SelectedItem.Text
            mrow("TCR_CHQDATE") = IIf(txtChqDate.Text = "", "01/Jan/1900", txtChqDate.Text)
            mrow("TCR_AMOUNT") = txtAmount.Text


            RecoveryDetails.Rows.Add(mrow)
            GrdRec.EditIndex = -1
            GrdRec.DataSource = RecoveryDetails
            GrdRec.DataBind()
            showNoRecordsFound1()
        Else
            Dim txtInv_Rate As TextBox = GrdRec.FooterRow.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = GrdRec.FooterRow.FindControl("txtRChqDate")
            Dim txtInv_Amt As TextBox = GrdRec.FooterRow.FindControl("txtRAMOUNT")
            Dim ddlPayment As DropDownList = GrdRec.FooterRow.FindControl("ddlRecoveryType")
            'Dim ddlPType As DropDownList = GrdRec.FooterRow.FindControl("ddlPType")
            'Dim txtPayable As TextBox = GrdRec.FooterRow.FindControl("txtPayable")


        End If
    End Sub

    Private Sub GrdRec_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GrdRec.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then
            Dim txtInv_Rate As TextBox = e.Row.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = e.Row.FindControl("txtInv_Qty")
            Dim txtInv As TextBox = e.Row.FindControl("txtInv_Qty")





            If e.Row.RowType = DataControlRowType.Footer Then
                Dim txtChqDate_F As TextBox = e.Row.FindControl("txtRChqDate")
                Dim txtAmount_F As TextBox = e.Row.FindControl("txtAMOUNT")
                Dim hdnITM_ID_F As HiddenField = e.Row.FindControl("hdnITM_ID")
                Dim imgchqDate As ImageButton = e.Row.FindControl("imgChqDate")
                Dim ddlPType As DropDownList = e.Row.FindControl("ddlPaymentType")


            End If
            If GrdRec.ShowFooter Or GrdRec.EditIndex > -1 Then
                Dim ddlPayment As DropDownList = e.Row.FindControl("ddlPaymentType")
                'Dim ddlPType As DropDownList = e.Row.FindControl("ddlPType")
                If Not ddlPayment Is Nothing Then

                    Dim sqlStr As String = ""
                    Dim sqlStr1 As String = ""
                    sqlStr &= "SELECT 0 RES_ID,'Rental'RES_DESCR UNION ALL SELECT RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID  ='" & h_Loc_id.Value & "' AND RES_DELETED<>1 UNION ALL select RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID ='000' UNION ALL select RES_ID,RES_DESCR from RESERVICES WHERE REC_CIT_ID IN(SELECT RCM_ID FROM RECITY_M WHERE RCM_DESCR ='ALL' ) AND RES_DELETED<>1 "
                    sqlStr1 &= "SELECT RT_ID,TCR_PAYMENTTYPE  from VW_GET_RECOVERYTPE order by RT_ID  "
                    'fillDropdown(ddlPayment, sqlStr, "RES_DESCR", "RES_ID", False)
                    fillDropdown(ddlPayment, sqlStr1, "TCR_PAYMENTTYPE", "RT_ID", False)
                    Dim prdTable As DataTable = ddlPayment.DataSource
                    For rowNum As Int16 = 0 To prdTable.Rows.Count - 1
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub GrdRec_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GrdRec.RowDeleting
        Dim mRow() As DataRow = RecoveryDetails.Select("ID=" & GrdRec.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= mRow(0)("TCP_ID") & ";"
            RecoveryDetails.Select("ID=" & GrdRec.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            RecoveryDetails.AcceptChanges()
        End If
        If RecoveryDetails.Rows.Count = 0 Then
            RecoveryDetails.Rows.Add(RecoveryDetails.NewRow())
            RecoveryDetails.Rows(0)(1) = -1
        End If

        GrdRec.DataSource = RecoveryDetails
        GrdRec.DataBind()
        showNoRecordsFound1()
    End Sub

    Private Sub GrdRec_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles GrdRec.RowEditing
        Try
            GrdRec.ShowFooter = False
            GrdRec.EditIndex = e.NewEditIndex
            GrdRec.DataSource = RecoveryDetails
            GrdRec.DataBind()
            Dim txtInv_Rate_F As TextBox = GrdRec.Rows(e.NewEditIndex).FindControl("txtRChqDate")
            Dim txtInv_Qty_F As TextBox = GrdRec.Rows(e.NewEditIndex).FindControl("txtAmount")
            Dim h_Service_ID_F As HiddenField = GrdRec.Rows(e.NewEditIndex).FindControl("hdnITM_ID")
            Dim h_ITEMTYPE As HiddenField = GrdRec.Rows(e.NewEditIndex).FindControl("hdnITM_TYPE")
            Dim ddlPayment As DropDownList = GrdRec.Rows(e.NewEditIndex).FindControl("ddlPaymentType")
            ddlPayment.Items.FindByText(h_ITEMTYPE.Value).Selected = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GrdRec_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GrdRec.RowUpdating
        Dim s As String = GrdRec.DataKeys(e.RowIndex).Value.ToString()
        Dim TCP_id As Label = GrdRec.Rows(e.RowIndex).FindControl("lblTCP_ID")
        Dim ddlPayment As DropDownList = GrdRec.Rows(e.RowIndex).FindControl("ddlPaymentType")

        Dim txtChqDate As TextBox = GrdRec.Rows(e.RowIndex).FindControl("txtRChqDate")
        Dim txtAmount As TextBox = GrdRec.Rows(e.RowIndex).FindControl("txtAmount")
        Dim txtProductid As HiddenField = GrdRec.Rows(e.RowIndex).FindControl("hdnITM_ID")
        Dim lblTCPRECID As Label = GrdRec.Rows(e.RowIndex).FindControl("lblTCP_RES_ID")


        If Not IsNumeric(txtAmount.Text) Then lblError.Text = "enter numbers only"
        If lblError.Text.Length > 0 Then
            showNoRecordsFound1()
            Exit Sub
        End If

        Dim mrow As DataRow
        mrow = RecoveryDetails.Select("ID=" & s)(0)
        mrow("TCR_ID") = TCP_id.Text
        mrow("TCR_RES_ID") = ddlPayment.SelectedValue
        mrow("RES_DESCR") = ddlPayment.SelectedItem
        mrow("TCR_CHQDATE") = txtChqDate.Text
        mrow("TCR_AMOUNT") = txtAmount.Text

        GrdRec.EditIndex = -1
        GrdRec.ShowFooter = True
        GrdRec.DataSource = RecoveryDetails
        GrdRec.DataBind()
    End Sub



    Private Sub ddlBudget_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBudget.SelectedIndexChanged
        Dim Budgetamount As Double = 0.0
        Dim Utilizedamount As Double = 0.0
        Dim currentRent As Double = 0.0

        If txttrent.Text <> "" Then
            currentRent = Val(txttrent.Text)
        Else
            currentRent = 0
        End If

        If h_EntryId.Value = 0 And h_bsu_id.Value = "" Then
            Budgetamount = Mainclass.getDataValue("select BUDAMOUNT from VW_GET_BUDGETS where bsu_id='" & Session("sBsuid") & "' and id=" & ddlBudget.SelectedValue, "OASIS_PUR_INVConnectionString")
            Utilizedamount = Mainclass.getDataValue("select isnull(sum(isnull(TCH_RENT,0)),0)UTI_AMOUNT from tct_h where TCH_BSU_ID='" & Session("sBsuid") & "'  and  isnull(TCH_DELETED,0)=0 and TCH_BUDGET_FYEAR=" & ddlBudget.SelectedValue, "OASIS_PUR_INVConnectionString")
        Else
            Budgetamount = Mainclass.getDataValue("select BUDAMOUNT from VW_GET_BUDGETS where bsu_id='" & h_bsu_id.Value & "' and id=" & ddlBudget.SelectedValue, "OASIS_PUR_INVConnectionString")
            Utilizedamount = Mainclass.getDataValue("select isnull(sum(isnull(TCH_RENT,0)),0)UTI_AMOUNT from tct_h where TCH_BSU_ID='" & h_bsu_id.Value & "' and   isnull(TCH_DELETED,0)=0 and TCH_BUDGET_FYEAR=" & ddlBudget.SelectedValue, "OASIS_PUR_INVConnectionString")
        End If






        If Utilizedamount + currentRent > Budgetamount Then
            radBudgeted.Checked = False
            radUnBudgeted.Checked = True
        Else
            radBudgeted.Checked = True
            radUnBudgeted.Checked = False
        End If
    End Sub
    'Private Sub lnkTRFHISTORY_Click(sender As Object, e As EventArgs) Handles lnkTRFHISTORY.Click


    '    If ViewState("datamode") <> "add" Then
    '        'divHistory.Visible = True
    '        'BindoldTrfs(ViewState("EntryId"))
    '    Else
    '        'divHistory.Visible = False
    '    End If
    '    ' MDefault.Show()

    'End Sub
    'Private Sub lnkTRFHISTORY_Click(sender As Object, e As EventArgs) Handles lnkTRFHISTORY.Click


    '    If ViewState("datamode") <> "add" Then
    '        'divHistory.Visible = True
    '        BindoldTrfs(ViewState("EntryId"))
    '    Else
    '        'divHistory.Visible = False
    '    End If
    '    ' MDefault.Show()

    'End Sub
    'Private Sub BindoldTrfs(ByVal TCH_Id As Integer)
    '    'Try



    '    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    '    '    Dim str_query As String = ""
    '    '    Dim PARAM(1) As SqlParameter

    '    '    'PARAM(1) = New SqlParameter("@TCH_ID", TCH_Id)
    '    '    PARAM(1) = New SqlParameter("@TCH_ID", 179)

    '    '    Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetOldTRFList", PARAM)
    '    '    Dim dvOLDTRF As DataView = New DataView(dsDetails.Tables(0))


    '    '    GrdHistory.DataSource = dvOLDTRF
    '    '    GrdHistory.DataBind()



    '    'Catch ex As Exception
    '    '    lblError.Text = ex.Message
    '    'End Try
    'End Sub

End Class
