﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TenancyContractEmployeeDetails.aspx.vb" Inherits="Inventory_TenancyContractEmployeeDetails" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<!-- Bootstrap core CSS-->
<link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/PHOENIXBETA/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<script type="text/javascript" language="javascript">

    //general close frame 
    function setCloseFrame() {       
        parent.setCloseFrame('done');
    }


    function CloseWindowYesNo() {
        if (confirm('Are you sure?'))
            parent.CloseFrame();
        //window.close();
    }
    //function CloseWindow() {
    //    window.close();
    //}

    function getEmployee(Employee, designation, Empid, Bsu, status, dependants, EUnitType,txtEntileAmount,txtLeaseAmount,txtDeduction) {
        var sFeatures;
        var lstrVal;
        var lintScrVal;
        var pMode;
        var NameandCode;
        var bsuSelected;

        sFeatures = "dialogWidth: 760px; ";
        sFeatures += "dialogHeight: 420px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        //pMode = "TENANCYEMPLOYEE"
        pMode = "TENANCYEMPLOYEENEW"
        var Lbsuid = '<%= Session("sBSUID") %>';
        bsuSelected = document.getElementById(Bsu).value;

       
        url = "../common/PopupSelect.aspx?id=" + pMode + "&SBsuid=" + bsuSelected + "&LBsuid=" + Lbsuid;
        document.getElementById('<%=hf_txtEmployee.ClientID%>').value = Employee;
         document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value = designation;
        document.getElementById('<%=hf_h_Emp_Id.ClientID%>').value = Empid;
        document.getElementById('<%=hf_txtDEPENDANTS.ClientID%>').value = dependants;
        document.getElementById('<%=hf_h_EUnitType.ClientID%>').value = EUnitType;
        document.getElementById('<%=hf_h_EntAmt.ClientID%>').value = txtEntileAmount;
        document.getElementById('<%=hf_h_status.ClientID%>').value = status;
        document.getElementById('<%=hf_h_LeaseAmount.ClientID%>').value = txtLeaseAmount;
        document.getElementById('<%=hf_h_DedAmount.ClientID%>').value = txtDeduction;

        
        

       
        
        var oWnd = radopen(url, "pop_rel");
    }


    function getDesignation(designation, Desid) {
        var sFeatures;
        var lstrVal;
        var lintScrVal;
        var pMode;
        var NameandCode;
        var bsuSelected;

        sFeatures = "dialogWidth: 760px; ";
        sFeatures += "dialogHeight: 420px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        pMode = "TENANCYDESIGNATION"
        var Lbsuid = '<%= Session("sBSUID") %>';
        bsuSelected = document.getElementById("<%=h_bsu_id.ClientID %>").value;
        url = "../common/PopupSelect.aspx?id=" + pMode + "&SBsuid=" + bsuSelected + "&LBsuid=" + Lbsuid;
        var oWnd = radopen(url, "pop_desig");
        
    }


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    function OnClientClose(oWnd, args) {
        var NameandCode;
        var arg = args.get_argument();

        if (arg) {
            NameandCode = arg.NameandCode.split('||');


            if (document.getElementById(document.getElementById('<%=hf_txtEmployee.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_txtEmployee.ClientID%>').value).value = NameandCode[1];

            if (document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value).value = NameandCode[2];

            if (document.getElementById(document.getElementById('<%=hf_h_Emp_Id.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_h_Emp_Id.ClientID%>').value).value = NameandCode[0];

            if (document.getElementById(document.getElementById('<%=hf_txtDEPENDANTS.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_txtDEPENDANTS.ClientID%>').value).value = NameandCode[4];

             if (document.getElementById(document.getElementById('<%=hf_h_status.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_h_status.ClientID%>').value).value = NameandCode[3];

            if (document.getElementById(document.getElementById('<%=hf_h_EUnitType.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_h_EUnitType.ClientID%>').value).value = NameandCode[6];

            if (document.getElementById(document.getElementById('<%=hf_h_EntAmt.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_h_EntAmt.ClientID%>').value).value = NameandCode[5];


            if (document.getElementById(document.getElementById('<%=hf_h_LeaseAmount.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_h_LeaseAmount.ClientID%>').value).value = 0;

            if (document.getElementById(document.getElementById('<%=hf_h_DedAmount.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_h_DedAmount.ClientID%>').value).value = 0;

            


            
            <%--if (document.getElementById(document.getElementById('<%=hf_h_LeaseAmount.ClientID%>').value))
                document.getElementById(document.getElementById('<%=hf_h_LeaseAmount.ClientID%>').value).value = 2500;                      

            var EAmt = NameandCode[5];
            var LeaAmt = document.getElementById(hf_h_LeaseAmount).value;
            var Ded = eval(EAmt) - eval(LeaAmt);

            if (Ded > 0)
                document.getElementById(txtDeductionAmount).value = 0;
            else
                document.getElementById(txtDeductionAmount).value = Ded;--%>


            

            
           document.getElementById("<%=h_dis_status.ClientID%>").value = NameandCode[3];
           document.getElementById("<%=h_dis_designation.ClientID%>").value = NameandCode[2];
           document.getElementById("<%=h_dis_dep.ClientID%>").value = NameandCode[4];
           document.getElementById("<%=h_dis_EmpName.ClientID%>").value  = NameandCode[1];
           document.getElementById("<%=h_dis_Entitlement.ClientID%>").value = NameandCode[5];
            document.getElementById("<%=h_dis_EUnittype.ClientID%>").value = NameandCode[6];


            
              
            
            document.getElementById(document.getElementById("<%=hf_h_status.ClientID%>").value).disabled = true;
            document.getElementById(document.getElementById("<%=hf_txtDESIGNATION.ClientID%>").value).disabled = true;
            document.getElementById(document.getElementById("<%=hf_txtDEPENDANTS.ClientID%>").value).disabled = true;
            document.getElementById(document.getElementById("<%=hf_txtEmployee.ClientID%>").value).disabled = true;
            document.getElementById(document.getElementById("<%=hf_h_EUnitType.ClientID%>").value).disabled = true;
            document.getElementById(document.getElementById("<%=hf_h_EntAmt.ClientID%>").value).disabled = true;
            //document.getElementById(document.getElementById("<%=hf_h_DedAmount.ClientID%>").value).disabled = true;
        }
    }
    function OndesigClientClose(oWnd, args) {

        <%--var NameandCode;
        var arg = args.get_argument();

        if (arg) {
            NameandCode = arg.NameandCode.split('||');
            if (document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_txtDESIGNATION.ClientID%>').value).value = NameandCode[1];
            if (document.getElementById(document.getElementById('<%=hf_h_Des_Id.ClientID%>').value)) document.getElementById(document.getElementById('<%=hf_h_Des_Id.ClientID%>').value).value = NameandCode[0];

            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;

        }--%>
    }

    function calcdeductions(txtEntileAmount, txtDeductionAmount, txtLeaseAmount, e) {
        if (document.getElementById(txtEntileAmount).value == "") document.getElementById(txtEntileAmount).value = 0;
        if (document.getElementById(txtLeaseAmount).value == "") document.getElementById(txtLeaseAmount).value = 0;
        var EAmt = document.getElementById(txtEntileAmount).value;
        var LeaAmt = document.getElementById(txtLeaseAmount).value;
        var Ded = eval(EAmt) - eval(LeaAmt);
        
        document.getElementById("<%=hf_h_DednAmt.ClientID %>").value = Ded;
        
        document.getElementById("<%=hf_h_LeasAmount.ClientID %>").value = LeaAmt;
        document.getElementById("<%=hf_h_EntAmt.ClientID%>").value = EAmt;
        

        if (Ded > 0 )

            document.getElementById(txtDeductionAmount).value = 0;
            
                    else
            document.getElementById(txtDeductionAmount).value = Ded;

        

        

             

            return false;
    }

    


 
</script>

<telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
    ReloadOnShow="true" runat="server" EnableShadow="true">
    <Windows>
        <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
        </telerik:RadWindow>
    </Windows>
    <Windows>
        <telerik:RadWindow ID="pop_desig" runat="server" Behaviors="Close,Move" OnClientClose="OndesigClientClose"
            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
        </telerik:RadWindow>
    </Windows>

</telerik:RadWindowManager>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <base target="_self" />
    <title>Employee Details</title>
</head>
<body>
    <form id="form1" runat="server">
        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                    <%--<asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
                    </asp:ScriptManager>--%>
                    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                    </ajaxToolkit:ToolkitScriptManager>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table border="0" cellpadding="4" cellspacing="0" align="left" width="100%">
                        <tr>
                            <td colspan="6" align="left">
                                <div style="width: 160%; overflow: scroll;">
                                    <asp:GridView ID="grdEmpDetails" runat="server" AutoGenerateColumns="False" PageSize="5" Width="160%" ShowFooter="true"
                                        CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="ID" Style="overflow: scroll;">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <th colspan="5">Employee Details</th>
                                                    <th colspan="3">Lease Details</th>
                                                    <th colspan="4">Entitlement Details</th>
                                                    <th></th>
                                                    <th></th>
                                                    <tr class="title-bg-lite">
                                                        <th width="8%">BSU</th>
                                                        <th width="15%">Employee Name</th>
                                                        <th width="12%">Designation</th>
                                                        <th width="10%">Status</th>
                                                        <th width="5%">Dependants</th>
                                                        <th width="5%">Unit No</th>
                                                        <th width="8%">Type</th>
                                                        <th width="5%">Amount</th>
                                                        <th width="5%">Old Rent</th>
                                                        <th width="8%">Type</th>
                                                        <th width="5%">Amount</th>
                                                        <th width="5%">Deduction</th>
                                                        <th width="15%">Furniture</th>
                                                        <th width="8%">Edit</th>
                                                        <th width="8%">Delete</th>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTCE_ID" runat="server" Visible="false" Text='<%# Bind("TCE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblChqDate" runat="server" Style="text-align: left" Text='<%# Bind("BSUSHORT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="h_BSU_Id" runat="server" Value='<%# Bind("TCE_BSU_ID") %>' />
                                                    <asp:DropDownList ID="ddlBSU" runat="server"></asp:DropDownList>
                                                    <asp:Label ID="lblTCE_ID" runat="server" Visible="false" Text='<%# Bind("TCE_ID") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:HiddenField ID="h_BSU_Id" runat="server" Value='<%# Bind("TCE_BSU_ID") %>' />
                                                    <asp:DropDownList ID="ddlBSU" runat="server"></asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TCE_EMP_ID") %>' />
                                                    <asp:TextBox ID="txtEmployee" Enabled ="false"   runat="server" Text='<%# Bind("EMPNAME") %>'></asp:TextBox>
                                                    <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:HiddenField ID="h_Emp_Id" runat="server" Value='<%# Bind("TCE_EMP_ID") %>' />
                                                    <asp:TextBox ID="txtEmployee" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:TextBox>
                                                    <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDesignation" Enabled ="false" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:TextBox>
                                                    <asp:ImageButton ID="imgDesignation" runat="server" Visible="false" ImageUrl="~/Images/forum_search.gif" />
                                                    <asp:HiddenField ID="h_Des_Id" Visible="false"  runat="server" Value='<%# Bind("TCE_DES_ID") %>' />
                                                </EditItemTemplate>
                                                <FooterTemplate>

                                                    <asp:TextBox ID="txtDesignation"  runat="server" Style="text-align: left" Text='<%# Bind("DESIGNATION") %>'></asp:TextBox>

                                                    <asp:ImageButton ID="imgDesignation" Visible="false"  runat="server" ImageUrl="~/Images/forum_search.gif" />
                                                    <asp:HiddenField ID="h_Des_Id" runat="server" Value='<%# Bind("TCE_DES_ID") %>' />
                                                    <asp:HiddenField ID="h_designation" runat="server" Value='<%# Bind("DESIGNATION")%>' />

                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMStatus" runat="server" Style="text-align: left" Text='<%# Bind("TCE_MSTATUS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="h_MStatus" runat="server" Value='<%# Bind("TCE_MSTATUS") %>' />
                                                    <%--<asp:DropDownList ID="ddlMStatus" Enabled ="false" runat="server"></asp:DropDownList>--%>
                                                    <asp:TextBox ID="txtStatus"  Enabled ="false"  runat="server" Text='<%# Bind("TCE_MSTATUS") %>' ></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:HiddenField ID="h_MStatus"  runat="server" Value='<%# Bind("TCE_MSTATUS") %>' />
                                                    <%--<asp:DropDownList ID="ddlMStatus" Enabled ="false" runat="server"></asp:DropDownList>--%>
                                                    <asp:TextBox ID="txtStatus"   runat="server" Text='<%# Bind("TCE_MSTATUS") %>' ></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDependants" runat="server" Text='<%# Bind("TCE_DEPENDANTS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDEPENDANTS" Enabled ="false"  runat="server" Text='<%# Bind("TCE_DEPENDANTS") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtDEPENDANTS"  runat="server" Style="text-align: left" Text='<%# Bind("TCE_DEPENDANTS") %>'></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUnitNo" runat="server" Text='<%# Bind("TCE_UNITNO") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtUnitNo" runat="server" Text='<%# Bind("TCE_UNITNO") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtUnitNo" runat="server" Text='<%# Bind("TCE_UNITNO") %>'></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaseType" runat="server" Text='<%# Bind("UNITTYPE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="h_UnitType" runat="server" Value='<%# Bind("TCE_REU_ID") %>' />
                                                    <asp:DropDownList ID="ddlUnitType" runat="server"></asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:HiddenField ID="h_UnitType" runat="server" Value='<%# Bind("TCE_REU_ID") %>' />
                                                    <asp:DropDownList ID="ddlUnitType" runat="server"></asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaseAmount" runat="server" Text='<%# Bind("TCE_LAMOUNT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtLeaseAmount" runat="server" Text='<%# Bind("TCE_LAMOUNT") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtLeaseAmount" runat="server" Text='<%# Bind("TCE_LAMOUNT") %>'></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOldRent" runat="server" Text='<%# Bind("TCE_OLDRENT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtOldRent" runat="server" Text='<%# Bind("TCE_OLDRENT") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtOldRent" runat="server" Text='<%# Bind("TCE_OLDRENT") %>'></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaseEType" runat="server" Text='<%# Bind("EUNITTYPE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="h_EUnitType" runat="server" Value='<%# Bind("TCE_EREU_ID") %>' />
                                                    <asp:DropDownList ID="ddlEUnitType" Enabled ="false" Visible ="false"  runat="server"></asp:DropDownList>
                                                    <asp:TextBox ID="txtEUnitType" Enabled ="false" runat="server" Text='<%# Bind("EUNITTYPE") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:HiddenField ID="h_EUnitType" runat="server" Value='<%# Bind("TCE_EREU_ID") %>' />
                                                    <asp:DropDownList ID="ddlEUnitType" Enabled ="false" Visible ="false"  runat="server"></asp:DropDownList>
                                                    <asp:TextBox ID="txtEUnitType" runat="server" Text='<%# Bind("EUNITTYPE") %>'></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOtherDetails" runat="server" Text='<%# Bind("TCE_EAMOUNT") %>'></asp:Label>

                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtEAmount" Enabled ="false"  runat="server" Text='<%# Bind("TCE_EAMOUNT") %>'></asp:TextBox>
                                                    <asp:HiddenField ID="h_OLD_EntAmont" runat="server" Value='<%# Bind("TCE_EAMOUNT")%>' />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:HiddenField ID="h_OLD_EntAmont" runat="server" Value='<%# Bind("TCE_EAMOUNT")%>' />
                                                    <asp:TextBox ID="txtEAmount"  runat="server" Text='<%# Bind("TCE_EAMOUNT") %>'></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDetails" runat="server" Text='<%# Bind("TCE_DAMOUNT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDeduction" Enabled ="false" runat="server" Text='<%# Bind("TCE_DAMOUNT") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtDeduction" Enabled ="false" runat="server" Text='<%# Bind("TCE_DAMOUNT") %>'></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfurnished" runat="server" Text='<%# Bind("FURN_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TCE_FURN_ID") %>' />
                                                    <asp:DropDownList ID="ddlFurnish" runat="server"></asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:HiddenField ID="h_Furn_id" runat="server" Value='<%# Bind("TCE_FURN_ID") %>' />
                                                    <asp:DropDownList ID="ddlFurnish" runat="server"></asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ShowDeleteButton="True" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="matters" colspan="4" align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" OnClientClick="CloseWindowYesNo();" />
                    <asp:Button ID="btnPrint" runat="server" Visible="false" CssClass="button" Text="Print" TabIndex="165" />
                    <asp:HiddenField ID="hGridDelete" runat="server" />
                    <asp:HiddenField ID="h_JT_JHD_ID" runat="server" />
                    <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                    <asp:HiddenField ID="h_bsu_id" runat="server" />

                    <asp:HiddenField ID="hf_txtEmployee" runat="server" />
                    <asp:HiddenField ID="hf_txtDESIGNATION" runat="server" />

                    
                    <asp:HiddenField ID="hf_h_Emp_Id" runat="server" />
                    <asp:HiddenField ID="hf_txtDEPENDANTS" runat="server" />
                    <%--<asp:HiddenField ID="hf_h_Des_Id" runat="server" />--%>
                    <asp:HiddenField ID="hf_h_EUnitType" runat="server" />


                    
                    <asp:HiddenField ID="hf_h_EntAmt" runat="server"  />
                    <asp:HiddenField ID="hf_h_status" runat="server" />

                    <asp:HiddenField ID="hf_h_DednAmt" runat="server" />
                    <asp:HiddenField ID="hf_h_LeasAmount" runat="server" />

                    <asp:HiddenField ID="h_dis_status" runat="server" />
                    <asp:HiddenField ID="h_dis_designation" runat="server" />
                    <asp:HiddenField ID="h_dis_dep" runat="server" />
                    <asp:HiddenField ID="h_dis_EUnittype" runat="server" />
                    <asp:HiddenField ID="h_dis_EmpName" runat="server" />
                    <asp:HiddenField ID="h_dis_Entitlement" runat="server" />
                    <asp:HiddenField ID="hf2_txtDESIGNATION" runat="server" />
                    <asp:HiddenField ID="hf2_h_Des_Id" runat="server" />


                    <asp:HiddenField ID="hf_h_LeaseAmount" runat="server" />
                    <asp:HiddenField ID="hf_h_DedAmount" runat="server" />

                    
                    
                    

                </td>
            </tr>
        </table>
    </form>
</body>
</html>
