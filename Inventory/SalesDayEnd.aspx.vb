Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_SalesDayEnd
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                'ts
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "PI04009") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                refreshScreen()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar1.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Private Sub refreshScreen()
        Dim sqlStr As String = ""
        'sqlStr = "select SAL_CREDITCARD SAP_ID, 'Credit Card' SAP_TYPE, replace(convert(varchar(30),getdate(),106),' ','/') SAP_DATE, sum(sal_cctotal) SAP_Amount "
        'sqlStr &= "from sal_h inner join oasis_fees..CREDITCARD_S on crr_id=SAL_CREDITCARD where sal_deleted=0 and sal_type in ('S','R') and sal_bsu_id='" & Session("sBsuid") & "' and sal_date=DATEADD(day, DATEDIFF(day, 0, getdate()),0) and sal_cctotal>0 "
        'sqlStr &= "group by SAL_CREDITCARD "
        'sqlStr &= "union all "
        'sqlStr &= "select 0, 'Cash', replace(convert(varchar(30),getdate(),106),' ','/'), sum(case when SAL_TYPE='S' THEN SAL_CASHTOTAL-SAL_BALANCE ELSE SAL_TOTAL end)*(CASE when SAL_TYPE='S' THEN 1 ELSE -1 END) "
        'sqlStr &= "from sal_h where sal_deleted=0 and sal_type in ('S','R') and sal_bsu_id='" & Session("sBsuid") & "' and sal_date=DATEADD(day, DATEDIFF(day, 0, getdate()),0) "
        'sqlStr &= "group by sal_type "
        'sqlStr &= "union all "
        'sqlStr &= "select SAL_CREDITCARD SAP_ID, 'Credit Card Charge' SAP_TYPE, replace(convert(varchar(30),getdate(),106),' ','/') SAP_DATE, sum(sal_creditcard_charge) SAP_Amount "
        'sqlStr &= "from sal_h inner join oasis_fees..CREDITCARD_S on crr_id=SAL_CREDITCARD where sal_deleted=0 and sal_type in ('S','R') and sal_bsu_id='" & Session("sBsuid") & "' and sal_date=DATEADD(day, DATEDIFF(day, 0, getdate()),0) and sal_creditcard_charge<>0 "
        'sqlStr &= "group by SAL_CREDITCARD "

        Dim ds As New DataSet
        ds = clsInventory.GET_SALES_DAYEND_AMOUNT(Session("sBsuid").ToString)
        gvDayEnd.DataSource = ds.Tables(0)
        gvDayEnd.DataBind()

        'sqlStr = "SELECT COUNT(*) "
        'sqlStr &= "FROM OASIS_FEES.FEES.FEEOTHCOLLECTION_H inner join OASIS_FEES.FEES.FEEOTHCOLLSUB_D ON FOD_FOC_ID=FOC_ID "
        'sqlStr &= "WHERE FOC_BSU_ID='" & Session("sBsuid") & "' AND FOC_EMP_ID='Auto' and FOC_DATE=DATEADD(day, DATEDIFF(day, 0, getdate()),0) GROUP BY FOC_RECNO"
        'Dim postCnt As Integer = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, sqlStr.ToString)
        btnPost.Visible = Not clsInventory.IsPostedFortheDay(Session("sBsuid").ToString) '(postCnt = 0)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim RetMessage As String = ""
        If POST_DAYEND(RetMessage) Then
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            usrMessageBar1.ShowNotification(RetMessage, UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Public Function POST_DAYEND(ByRef RetMessage As String) As Boolean
        POST_DAYEND = False
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        Try
            Dim trans As SqlTransaction
            conn.Open()
            trans = conn.BeginTransaction("BOOKS")
            Dim cmd As SqlCommand
            Dim RetVal As Integer = 0

            cmd = New SqlCommand("dbo.postSales", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpUSR_ID As New SqlParameter("@USR_ID", SqlDbType.VarChar, 50)
            sqlpUSR_ID.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpUSR_ID)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
            RetVal = retSValParam.Value

            If RetVal <> 0 Then
                trans.Rollback()
                RetMessage = UtilityObj.getErrorMessage(RetVal)
            Else
                trans.Commit()
                POST_DAYEND = True
                RetMessage = ""
            End If
        Catch ex As Exception
            RetMessage = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function
End Class



