﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Net
Imports System.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib.Checksums
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.GZip

Partial Class Inventory_Default2
    Inherits BasePage

    Private Sub DownloadFileNew()
        'Dim fullpath = "//172.16.1.11/oasisphotos/temp/ESSTemp/o2fojfe3mk1a5y5554pjqb555282013 73310 AMPRF-1160001.pdf" ' Path.GetFullPath(fname)
        Dim str_conn As String = "Data Source=172.16.1.15;Initial Catalog=OASIS_PUR_INV;Persist Security Info=True;User ID=sa;Password=xf6mt"
        Dim rdrDocument As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "select PRF_NO+'_'+cast(PRI_ID as varchar) Filename, PRF_I.* from PRF_H inner join PRF_I on PRI_PRF_ID=PRF_ID where pri_id not in (select prf_i from prf_i_new)")

        If rdrDocument.HasRows Then
            Dim FileName As String, NewFileName As String, FileExt As String
            Do While rdrDocument.Read()
                FileExt = rdrDocument("PRI_NAME")
                FileExt.Substring(FileExt.Length - 4)
                If 1 = 1 Then
                    FileName = rdrDocument("Filename")
                    ContentType = rdrDocument("pri_contenttype")
                    Dim fsDataArray As New System.IO.FileStream(WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Purchase\" & FileName & ".zip", System.IO.FileMode.Create)
                    fsDataArray.Write(rdrDocument("pri_file"), 0, rdrDocument("pri_file").Length)
                    fsDataArray.Close() : fsDataArray.Dispose()

                    Dim fileToDelete As New FileInfo(WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Purchase\" & FileName & ".zip")
                    NewFileName = UnZip(WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Purchase\" & FileName & ".zip", WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Purchase\")
                    Dim info As New FileInfo("\\172.16.1.11\oasisphotos\temp\ESSTemp\Purchase\" & NewFileName)
                    FileExt = NewFileName.Substring(NewFileName.Length - 4)
                    If FileExt.Substring(0, 1) <> "." Then FileExt = NewFileName.Substring(NewFileName.Length - 5)
                    If FileExt.Substring(0, 1) <> "." Then FileExt = NewFileName.Substring(NewFileName.Length - 6)
                    Dim infoNew As New FileInfo("\\172.16.1.11\oasisphotos\temp\ESSTemp\Purchase\" & rdrDocument("FileName") & FileExt)
                    If Not infoNew.Exists Then
                        info.MoveTo("\\172.16.1.11\oasisphotos\temp\ESSTemp\Purchase\" & FileName & FileExt)
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "insert into PRF_I_NEW values (" & rdrDocument("PRI_ID") & ",'" & FileName & FileExt & "')")
                    Else
                        info.Delete()
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "insert into PRF_I_NEW values (" & rdrDocument("PRI_ID") & ",'" & rdrDocument("FileName") & FileExt & "')")
                    End If
                    fileToDelete.Delete()
                End If
            Loop
        End If
    End Sub

    Private Sub deleteFiles()
        Dim di As DirectoryInfo = New DirectoryInfo("\\172.16.1.11\oasisphotos\temp\ESSTemp")
        For Each fi As FileInfo In di.GetFiles("*2013*")
            Try
                If fi.CreationTime < Now.AddDays(-0.5) Then
                    fi.Delete()
                End If
            Catch ex As Exception

            End Try
        Next

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'DownloadFileNew()
            'deleteFiles()
        End If
    End Sub

    Public Shared Function UnZip(ByVal SrcFile As String, ByVal DstFile As String) As String
        Dim fileStreamIn As FileStream = New FileStream(SrcFile, FileMode.Open, FileAccess.Read)
        Dim zipInStream As ZipInputStream = New ZipInputStream(fileStreamIn)
        Dim enTry As ZipEntry = zipInStream.GetNextEntry()
        Dim fileStreamOut As FileStream = New FileStream(DstFile + "\" + enTry.Name, FileMode.Create, FileAccess.Write)
        Dim size As Integer
        Dim buffer() As Byte = New Byte(4096) {}
        Do
            size = zipInStream.Read(buffer, 0, buffer.Length)
            fileStreamOut.Write(buffer, 0, size)
        Loop While size > 0
        zipInStream.Close()
        fileStreamOut.Close()
        fileStreamIn.Close()
        Return enTry.Name
    End Function

End Class
