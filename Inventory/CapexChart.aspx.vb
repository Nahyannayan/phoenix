﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class CapexChart
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then
                hfCategory.Value = "display"
                txtFDate.Text = "01/Apr/2013"
                txtTDate.Text = Now.ToString("dd/MMM/yyyy")
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim ReportName As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select mnu_text from oasis..menus_m where mnu_code='" & MainMnu_code & "'")
            Page.Title = OASISConstants.Gemstitle

            pgTitle.Text = ReportName

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim sqlParam(5) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@fromdate", txtFDate.Text, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@todate", txtTDate.Text, SqlDbType.VarChar)
        Dim ds As DataTable = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "CapexChart", sqlParam).Tables(0)
        rptFlow.DataSource = ds
        rptFlow.DataBind()

    End Sub

    Protected Sub rptFlow_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles rptFlow.ItemDataBound

    End Sub
End Class
