﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Partial Class Inventory_ShowOldTRFHistory
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString

    Private Sub Inventory_ShowOldTRFHistory_Load(sender As Object, e As EventArgs) Handles Me.Load
        h_Tch_id.Value = Request.QueryString("viewid")
        BindoldTrfs(h_Tch_id.Value)
    End Sub
    Private Sub BindoldTrfs(ByVal TCH_Id As Integer)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(1) As SqlParameter

            PARAM(1) = New SqlParameter("@TCH_ID", TCH_Id)
            'PARAM(1) = New SqlParameter("@TCH_ID")

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetOldTRFList", PARAM)
            Dim dvOLDTRF As DataView = New DataView(dsDetails.Tables(0))


            GrdHistory.DataSource = dvOLDTRF
            GrdHistory.DataBind()
            If dvOLDTRF.Count = 0 Then
                lblError.Text = "No old TRF data exists"
            End If



        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class
