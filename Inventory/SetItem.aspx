﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="SetItem.aspx.vb" Inherits="Inventory_SetItem" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

     <style>
        .darkPanlAlumini {
            background: rgb(235, 228, 228) !important;
        }

        .completionListElement {
            width: 40% !important;        
            margin: auto;
            list-style: none;
            left: 0px !important;
            position: absolute !important;
            /*width: 350px !important;*/
        }

            .completionListElement .listitem {
                cursor: pointer;
            }

            .completionListElement .highlightedListItem {
                background-color: rgba(0,0,0,0.2);
            }
    </style>


    <script type="text/javascript" language="javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        var myIds = new Array();
        var myDescrs = new Array();



        function ClientItemSelected(sender, e) {
            var index = sender._selectIndex;
            var hdnITM_ID = sender.get_element().id.replace("txtITM_DESCR", "hdnITM_ID");

            $get(hdnITM_ID).value = myIds[index];
            $get(sender.get_element().id).value = myDescrs[index];

        }

        function MultiSelect(sender, e) {
            var comletionList = sender.get_completionList();


            for (i = 0; i < comletionList.childNodes.length; i++) {
                var itemobj = new Object();
                var _data = comletionList.childNodes[i]._value;

                itemobj.descr = _data.substring(_data.lastIndexOf('|') + 1);
                comletionList.childNodes[i]._value = itemobj.name;

                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.itmid = _data.substring(_data.lastIndexOf('|') + 1); // parse name as item value



                if (itemobj.descr) {
                    myIds[i] = itemobj.itmid; // id used in updating hidden file
                    myDescrs[i] = itemobj.descr;

                }
                //comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:80%;float:left'>" + itemobj.descr + "</div>" + "<div class='cloumnspan' style='width:20%;'>" + itemobj.itmid + "</div>";
                //comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:100%;float:left'>" + itemobj.descr + "</div>";
                comletionList.childNodes[i].innerHTML = "<div class='darkPanlAlumini' align='left'> " + itemobj.descr + " </div>"

            }
        }


        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Set Item
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%">
                                <tr>
                                    <td class="matters" width="20%">
                                        <asp:Label ID="lblQUONo" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtBuID" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td colspan="2" width="50%">
                                        <asp:TextBox ID="TxtBUName" runat="server" Enabled="false" Width="88%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" width="20%"><span class="field-label">ISBN<span style="color: #800000"></span></span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="matters" width="20%"><span class="field-label">Description<span style="color: #800000"></span></span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="left" class="matters">
                                        <asp:GridView ID="grdSetItem" runat="server" AutoGenerateColumns="False" PageSize="5"
                                            Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-row table-bordered"
                                            DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSET_ID" runat="server" Text='<%# Bind("SED_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSED_SEH_ID" runat="server" Text='<%# Bind("SED_SEH_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblITM_DESCR" runat="server" Text='<%# Bind("ITM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtITM_DESCR" runat="server" Text='<%# Bind("ITM_DESCR") %>'></asp:TextBox><span
                                                            style="color: #800000">*</span>
                                                        <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx2"
                                                            OnClientPopulated="MultiSelect" CompletionListCssClass="completionListElement"
                                                            CompletionListItemCssClass="listItem" FirstRowSelected="true" CompletionListHighlightedItemCssClass="highlightedListItem"
                                                            OnClientItemSelected="ClientItemSelected" CompletionSetCount="5" EnableCaching="false"
                                                            MinimumPrefixLength="1" UseContextKey="true" ServiceMethod="GetItmDescr" ServicePath="SetItem.aspx"
                                                            TargetControlID="txtITM_DESCR">
                                                        </ajaxToolkit:AutoCompleteExtender>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("SED_ITM_ID") %>' runat="server" />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtITM_DESCR" runat="server" Text='<%# Bind("ITM_DESCR") %>'></asp:TextBox><span
                                                            style="color: #800000">*</span>
                                                        <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx3"
                                                            OnClientPopulated="MultiSelect" CompletionListCssClass="completionListElement"
                                                            CompletionListItemCssClass="listItem" FirstRowSelected="true" CompletionListHighlightedItemCssClass="highlightedListItem"
                                                            OnClientItemSelected="ClientItemSelected" CompletionSetCount="5" EnableCaching="false"
                                                            MinimumPrefixLength="1" UseContextKey="true" ServiceMethod="GetItmDescr" ServicePath="SetItem.aspx"
                                                            TargetControlID="txtITM_DESCR">
                                                        </ajaxToolkit:AutoCompleteExtender>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("SED_ITM_ID") %>' runat="server" />
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblITMQTY" runat="server" Style="text-align: right"
                                                            Text='<%# Bind("SED_QTY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtITM_QTY" runat="server" Style="text-align: right"
                                                            Text='<%# Bind("SED_QTY") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtITM_QTY" runat="server" Style="text-align: right"
                                                            Text='<%# Bind("SED_QTY") %>'></asp:TextBox><span style="color: #800000">*</span>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit"
                                                            Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update"
                                                            Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel"
                                                            Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew"
                                                            Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="6">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Print" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="6">
                                        <asp:HiddenField ID="Seh_Id" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_SetItemGridDelete" runat="server" Value="0" />
                                        <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
