﻿Imports System.Web.Configuration

Partial Class IFrameNew
    Inherits System.Web.UI.Page
    Dim FileName As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ContentType As String, FileName As String, Path As String
        ContentType = Request.QueryString("contenttype").ToString
        FileName = Request.QueryString("filename").ToString
        Path = Request.QueryString("path").ToString

        Dim ViewServer, ViewURL As String, viewDirect As String
        If ContentType.Contains("image") Or FileName.Contains("jpeg") Then
            ViewURL = WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & Path & "/" & FileName
            viewDirect = WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & Path & "/" & FileName
        Else
            ViewServer = Replace(WebConfigurationManager.AppSettings.Item("DocViewerURL"), "*", "&")
            ViewURL = ViewServer & WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & Path & "/" & FileName
            viewDirect = WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & Path & "/" & FileName
        End If
        'iFrame.Attributes("src") = ViewURL

        Dim scriptKey As String = "UniqueKeyForThisScript"
        Dim javaScript As String = "<script type='text/javascript'>foo('" & viewDirect & "','" & ViewURL & "');</script>"
        ClientScript.RegisterStartupScript(Me.GetType(), scriptKey, javaScript)

        'lb.CommandArgument = serverpath + FileName
        Dim serverpath As String = ConfigurationManager.AppSettings("TempFileFolder").ToString() & Path & "\"
        lb.NavigateUrl = "Default2.aspx?name=" + serverpath + FileName
        iFrame.Attributes("width") = "100%"
        iFrame.Attributes("height") = "500px"
        'iFrame.Attributes("width") = "50px"
        'iFrame.Attributes("height") = "50px"
    End Sub

End Class
