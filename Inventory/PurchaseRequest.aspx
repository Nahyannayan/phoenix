﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PurchaseRequest.aspx.vb" Inherits="Inventory_PurchaseRequest" Title="Purchase Requisition Form" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .my-span {
            width: 125px;
            height: 125px;
            display: inline-block;
            vertical-align: top;
            padding: 4px;
            border: 2px solid rgba(0,0,0,0.3);
            margin-right: 2px;
        }
    </style>

    <script type="text/javascript" language="javascript">
        function TabChanged(sender, args) {
            sender.get_clientStateField().value =
        sender.saveClientState();
        }

        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
    1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        var myIds = new Array();
        var myDetails = new Array();
        var myRates = new Array();
        var myActNames = new Array();
        var myActIds = new Array();

        function ClientItemSelected(sender, e) {
            var index = sender._selectIndex;
            var hdnITM_ID = sender.get_element().id.replace("txtPRD_DESCR", "hdnITM_ID");
            var txtPRD_QTY = sender.get_element().id.replace("txtPRD_DESCR", "txtPRD_QTY");
            var txtPRD_RATE = sender.get_element().id.replace("txtPRD_DESCR", "txtPRD_RATE");
            var txtPRD_TOTAL = sender.get_element().id.replace("txtPRD_DESCR", "txtPRD_TOTAL");
            $get(hdnITM_ID).value = myIds[index];
            $get(sender.get_element().id).value = myDetails[index];

            if ($get(txtPRD_RATE)) $get(txtPRD_RATE).value = myRates[index];
            if ($get(txtPRD_QTY).value == "") {
                $get(txtPRD_QTY).value = 1;
                if ($get(txtPRD_TOTAL)) $get(txtPRD_TOTAL).value = myRates[index];
                if (document.getElementById("<%=txtSupplier.ClientID %>"))
                    if (document.getElementById("<%=txtSupplier.ClientID %>").value == "") {
                        ActDetails = myActNames[index].split(';');
                        document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = myActIds[index];
                        if ((myActIds[index] == "063A0111") || (myActIds[index] == "063B0104"))
                            document.getElementById("<%=h_block.ClientID %>").value = "F";
                        else
                            document.getElementById("<%=h_block.ClientID %>").value = document.getElementById("<%=h_blockForm.ClientID %>").value;
                        document.getElementById("<%=txtSupplier.ClientID %>").value = ActDetails[0];
                        document.getElementById("<%=h_term.ClientID %>").value = ActDetails[1];
                        document.getElementById("<%=txtTerms.ClientID %>").value = ActDetails[1];
                        document.getElementById("<%=txtSupplierAddress.ClientID %>").value = ActDetails[2];
                    }
            }
               
            else $get(txtPRD_TOTAL).value = eval(myRates[index]).replace(/,/g, "") * eval($get(txtPRD_QTY).value);
        }

        function MultiSelect(sender, e) {
            var comletionList = sender.get_completionList();
            for (i = 0; i < comletionList.childNodes.length; i++) {
                var itemobj = new Object();
                var _data = comletionList.childNodes[i]._value;
                itemobj.descr = _data.substring(_data.lastIndexOf('|') + 1); // parse name as item value
                comletionList.childNodes[i]._value = itemobj.name;
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.commodity = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.rate = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.id = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.actname = _data.substring(_data.lastIndexOf('|') + 1);
                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.actid = _data.substring(_data.lastIndexOf('|') + 1);
                if (itemobj.id) {
                    myIds[i] = itemobj.id; // id used in updating hidden file
                    myDetails[i] = itemobj.descr;
                    myRates[i] = itemobj.rate;
                    myActNames[i] = itemobj.actname;
                    myActIds[i] = itemobj.actid;
                }
                comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:60%;float:left'>" + itemobj.descr + "</div>"
                                                  + "<div class='cloumnspan' style='width:30%;float:left'>" + itemobj.commodity + "</div>"
                                                  + "<div class='cloumnspan' style='width:10%'>" + itemobj.rate + "</div>";
            }
        }

        function showDocument(id, filename, contenttype) {
            var sFeatures;
            //sFeatures = "dialogWidth: 920px; ";
            //sFeatures += "dialogHeight: 500px; ";
            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";
            //result = window.showModalDialog("IFrame.aspx?id=" + id + "&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            //result = window.showModalDialog("IFrameNew.aspx?id=" + id + "&path=purchase&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            //return false;
            var url = "IFrameNew.aspx?id=" + id + "&path=purchase&filename=" + filename + "&contenttype=" + contenttype;
            var oWnd = radopen(url, "pop_uploaddoc");
        }

        function getItemName(hdnClient, txtClient, txtQty, txtRate, txtTotal) {
            var supplier = 'none';
            if (document.getElementById("<%=hTPT_Client_ID.ClientID %>"))
                if ((document.getElementById("<%=h_PRF_Type.ClientID %>").value == "F") || (document.getElementById("<%=h_PRF_Type.ClientID %>").value == "C"))
                    supplier = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value;
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMITEM"
            ptype = document.getElementById("<%=h_PRF_Type.ClientID %>").value;
            pdate = document.getElementById("<%=txtPRFDate.ClientID %>").value;
            var e = document.getElementById("<%=ddlDepartment.ClientID %>");
            if (e == undefined)
                var dptid = 0;
            else
                var dptid = e.options[e.selectedIndex].value;
            actid = document.getElementById("<%=h_Actid.ClientID %>").value;
            var f = document.getElementById("<%=ddlBSU.ClientID %>");
            if (f == undefined)
                var bsuid = '';
            else
                var bsuid = f.options[f.selectedIndex].value;

            url = "../common/PopupSelect.aspx?id=" + pMode + "&type=" + ptype + "&Supplier=" + supplier + "&Date=" + pdate + "&DptId=" + dptid + "&ActId=" + actid + "&BSUId=" + bsuid;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                return false;
            }
            NameandCode = result.split('||');
            if (document.getElementById(txtQty).value == "0" || document.getElementById(txtQty).value == "") document.getElementById(txtQty).value = "1";
            if (document.getElementById(txtRate)) document.getElementById(txtRate).value = NameandCode[3];
            if (document.getElementById(txtClient)) document.getElementById(txtClient).value = NameandCode[1] + " " + NameandCode[2];
            if (document.getElementById(hdnClient)) document.getElementById(hdnClient).value = NameandCode[0];
            if (document.getElementById(txtTotal)) document.getElementById(txtTotal).value = eval(document.getElementById(txtRate).value) * eval(document.getElementById(txtQty).value);
            if (supplier == '') {
                ActDetails = NameandCode[5].split(';');
                document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = NameandCode[4];
                if ((NameandCode[4] == "063A0111") || (NameandCode[4] == "063B0104"))
                    document.getElementById("<%=h_block.ClientID %>").value = "F";
                else
                    document.getElementById("<%=h_block.ClientID %>").value = document.getElementById("<%=h_blockForm.ClientID %>").value;
                document.getElementById("<%=txtSupplier.ClientID %>").value = ActDetails[0];
                document.getElementById("<%=h_term.ClientID %>").value = ActDetails[1];
                document.getElementById("<%=txtTerms.ClientID %>").value = ActDetails[1];
                document.getElementById("<%=txtSupplierAddress.ClientID %>").value = ActDetails[2];
            }
            //document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        }


        function getItemNameNew(hdnClient, txtClient, txtQty, txtRate, txtTotal) {
            //alert(txtClient);
            var supplier = 'none';
        <%--if (document.getElementById("<%=hTPT_Client_ID.ClientID %>"))
            if ((document.getElementById("<%=h_PRF_Type.ClientID %>").value == "F") || (document.getElementById("<%=h_PRF_Type.ClientID %>").value == "C"))--%>
            supplier = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value;

            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMITEM_NEW"
            ptype = document.getElementById("<%=h_PRF_Type.ClientID %>").value;
            pdate = document.getElementById("<%=txtPRFDate.ClientID %>").value;
            var e = document.getElementById("<%=ddlDepartment.ClientID %>");
            if (e == undefined)
                var dptid = 0;
            else
                var dptid = e.options[e.selectedIndex].value;
            actid = document.getElementById("<%=h_Actid.ClientID %>").value;
            var f = document.getElementById("<%=ddlBSU.ClientID %>");
            if (f == undefined)
                var bsuid = '';
            else
                var bsuid = f.options[f.selectedIndex].value;

            url = "../common/PopupSelect.aspx?title=Item&id=" + pMode + "&type=" + ptype + "&Supplier=" + supplier + "&Date=" + pdate + "&DptId=" + dptid + "&ActId=" + actid + "&BSUId=" + bsuid;
            <%--result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                return false;
            }--%>
            document.getElementById('<%=hf_ITM_ID.ClientID%>').value = hdnClient;
            document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value = txtClient;
            document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value = txtQty;
            document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value = txtRate;
            document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>').value = txtTotal;
            var oWnd = radopen(url, "pop_newitem");

        }




        function getClientName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMCLIENT"
            url = "../common/PopupSelect.aspx?title=Supplier&id=" + pMode + "&jobtype=" + document.getElementById("<%=h_PRF_Type.ClientID %>").value;
            <%-- result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            return false;
        }--%>
            var oWnd = radopen(url, "pop_rel");


        }

        function getAccount() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMACCOUNT";
            var f = document.getElementById("<%=ddlBSU.ClientID %>");
            if (f == undefined)
                var bsu_id = '';
            else
                var bsu_id = f.options[f.selectedIndex].value;

            var e = document.getElementById("<%=ddlDepartment.ClientID %>");
            if (e == undefined)
                var dpt_id = 0;
            else
                var dpt_id = e.options[e.selectedIndex].value;
            url = "../common/PopupSelect.aspx?title=Account&id=" + pMode + "&bsu_id=" + bsu_id + "&dpt_id=" + dpt_id + "&date=" + document.getElementById("<%=txtPRFDate.ClientID %>").value;

            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;

            <%--result = window.showModalDialog(url, "", sFeatures);
        if (result == '' || result == undefined) {
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            return false;
        }--%>
            var oWnd = radopen(url, "pop_acc");

        }

        function getRClientName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMCLIENT"
            url = "../common/PopupSelect.aspx?title=Supplier&id=" + pMode + "&jobtype=Q";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById("<%=txtRSup_Id.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hdnRSup_id.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=hActRefresh.ClientID %>").value = 1;
        }

        function getRAccount() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMACCOUNT"
            url = "../common/PopupSelect.aspx?title=Account&id=" + pMode + "&date=" + document.getElementById("<%=txtPRFDate.ClientID %>").value;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById("<%=hdnRAct_id.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtRAct_id.ClientID %>").value = NameandCode[2];
            document.getElementById("<%=hActRefresh.ClientID %>").value = 1;
        }

        function getClientNameEmail() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "ITMCLIENT"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&jobtype=E";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById("<%=txtSupplierEmail.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hdnSupplierEmail.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtSupplierEmailId.ClientID %>").value = NameandCode[4];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        }

        function calculateItem(txtQty, txtRate, txtTotal) {
            var total;
            var Rate;
            
            if (document.getElementById(txtRate))

            {
                Rate=document.getElementById(txtRate).value.replace(/,/g, "");
                total = eval(Rate) * document.getElementById(txtQty).value;
                //alert(Rate)
                document.getElementById(txtTotal).value = format("#,##0.00", total);
                
            }
        }

        function calculate() {
            var total = 0;
            document.getElementById("<%=txtTotal.ClientID %>").value = format("#,##0.00", total);
            return false;
        }
        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function Numeric_Block() {
            if (document.getElementById("<%=h_block.ClientID %>").value == "T") return false
        };

        function getCCS(hdn, txt, COSTTYPE, DOC_DATE, COSTOTH) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            if (document.getElementById(COSTTYPE).value == "0000") {
                pMode = "SUBLEDGER"
                url = "../common/PopupSelect.aspx?id=" + pMode + "&ACT_ID=" + document.getElementById("<%=h_Actid.ClientID %>").value;
            }
            else {
                pMode = "COSTELEMENT"
                url = "../common/PopupSelect.aspx?id=" + pMode + "&COSTTYPE=" + document.getElementById(COSTTYPE).value + "&DOC_DATE=" + DOC_DATE + "&COSTOTH=0010";
            }
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById(txt).value = NameandCode[1];
            document.getElementById(hdn).value = NameandCode[0];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                document.getElementById("<%=txtSupplier.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtSupplierAddress.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = NameandCode[1];

                if ((NameandCode[1] == "063A0111") || (NameandCode[1] == "063B0104"))
                    document.getElementById("<%=h_block.ClientID %>").value = "F";
                else
                    document.getElementById("<%=h_block.ClientID %>").value = document.getElementById("<%=h_blockForm.ClientID %>").value;

                document.getElementById("<%=h_term.ClientID %>").value = NameandCode[4];
                document.getElementById("<%=txtTerms.ClientID %>").value = NameandCode[4];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }

        }

        function OnClientAccClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');


                document.getElementById("<%=h_Actid.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtACTID.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=txtBudMtd.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=txtBudYtd.ClientID %>").value = NameandCode[4];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                document.getElementById("<%=hActRefresh.ClientID %>").value = 1;
                __doPostBack('<%= hGridRefresh.ClientID%>', '');
            }

        }

        function OnClientNewItemClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                var qty_id = document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value
                if (document.getElementById(qty_id).value == "0" || document.getElementById(qty_id).value == "")
                    document.getElementById(qty_id).value = "1";

                if (document.getElementById('<%=hf_txtSAD_RATE.ClientID%>')) {
               <%-- alert(NameandCode[3]);
                alert(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value)--%>
                    var rate_id = document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value
                    document.getElementById(rate_id).value = NameandCode[3];
                }
                if (document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>')) {
                    var client_id = document.getElementById('<%=hf_txtSAD_DESCR.ClientID%>').value
                    document.getElementById(client_id).value = NameandCode[1] + " " + NameandCode[2];
                }
                if (document.getElementById('<%=hf_ITM_ID.ClientID%>')) {
                    var value_id = document.getElementById('<%=hf_ITM_ID.ClientID%>').value
                document.getElementById(value_id).value = NameandCode[0];
            }
            if (document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>')) {
                    var total_id = document.getElementById('<%=hf_txtSAD_TOTAL.ClientID%>').value
                //alert(total_id)
                //document.getElementById(total_id).value = eval(document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value).value) * eval(document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value).value);

                //document.getElementById(total_id).value = eval(document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value).value.replace(",", "")) * eval(document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value).value);
                document.getElementById(total_id).value = format("#,##0.00", eval(document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value).value.replace(/,/g, "")) * eval(document.getElementById(document.getElementById('<%=hf_txtSAD_QTY.ClientID%>').value).value));

                
                //alert(document.getElementById(document.getElementById('<%=hf_txtSAD_RATE.ClientID%>').value).value.replace(",",""));
            }
            var supplier_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
                if (supplier_id == '') {
                    ActDetails = NameandCode[5].split(';');
                    document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = NameandCode[4];
                    if ((NameandCode[4] == "063A0111") || (NameandCode[4] == "063B0104"))
                        document.getElementById("<%=h_block.ClientID %>").value = "F";
                    else
                        document.getElementById("<%=h_block.ClientID %>").value = document.getElementById("<%=h_blockForm.ClientID %>").value;
                    document.getElementById("<%=txtSupplier.ClientID %>").value = ActDetails[0];
                    document.getElementById("<%=h_term.ClientID %>").value = ActDetails[1];
                    document.getElementById("<%=txtTerms.ClientID %>").value = ActDetails[1];
                    document.getElementById("<%=txtSupplierAddress.ClientID %>").value = ActDetails[2];
                }
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            }

        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
         <Windows>
            <telerik:RadWindow ID="pop_uploaddoc" runat="server" Behaviors="Close,Move" 
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_acc" runat="server" Behaviors="Close,Move" OnClientClose="OnClientAccClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_newitem" runat="server" Behaviors="Close,Move" OnClientClose="OnClientNewItemClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <%--  <Windows>
            <telerik:RadWindow ID="pop_area" runat="server" Behaviors="Close,Move" OnClientClose="OnareaClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>--%>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="pgTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr id="trButtons" runat="server">
                                    <td align="center" colspan="4">
                                        <input type="button" id="btnAcceptTop" class="button" value=" Approve " style="color: White; background-color: #1B80B6; border-style: Solid; font-family: Verdana; font-weight: bold;" runat="server" onclick="this.disabled = true;" onserverclick="btnAccept_Click" />
                                        <input type="button" id="btnBypassTop" class="button" value=" Pass " style="color: White; background-color: #1B80B6; border-style: Solid; font-family: Verdana; font-weight: bold;" runat="server" onclick="this.disabled = true;" onserverclick="btnPass_Click" />
                                        <asp:Button ID="btnRejectTop" runat="server" CausesValidation="False" CssClass="button" Text="Reject" />
                                        <asp:Button ID="btnPrintTop" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print" />
                                        <asp:Button ID="btnCancelTop" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnMessageTop" runat="server" CausesValidation="False" CssClass="button" Text="Message" /></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblPRFNo" runat="server" Text="PRF Number" CssClass="field-label"></asp:Label><span class="text-danger"> * </span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPRFNo" runat="server" Enabled="false"></asp:TextBox></td>
                                    <td align="left" width="20%">  <asp:Label ID="lblPriority" runat="server" Text="Priority" CssClass="field-label"></asp:Label><span class="text-danger"> * </span></td>
                                    <td align="left" width="30%">  <asp:DropDownList ID="ddlPriority" runat="server"></asp:DropDownList></td>
                                </tr>

                                <tr>

                                  
                                    <td align="left"><span class="field-label">Date Requested</span><span class="text-danger"> * </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPRFDate" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:ImageButton Visible="false" ID="lnkPRFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtPRFDate" PopupButtonID="lnkPRFDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:TextBox ID="txtOldNo" runat="server" Enabled="false"></asp:TextBox></td>

                                      <td align="left"><span class="field-label">Charge To</span><span class="text-danger"> * </span>
                                      </td>
                                    <td align="left"> <asp:DropDownList ID="ddlBSU" runat="server"></asp:DropDownList>
                                      </td>
                                </tr>
                                <tr>
                                   
                                    <td align="left"><span class="field-label">Date Required</span><span class="text-danger"> * </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDELDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="lnkDELDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDELDate" PopupButtonID="lnkDELDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:TextBox ID="txtShipment" Visible="false" runat="server"></asp:TextBox></td>
                                     <td align="left"></td>
                                    <td align="left">
                                       </td>
                                </tr>
                                <tr id="trSupplier" runat="server">
                                    <td align="left"><span class="field-label">Supplier</span><span class="text-danger"> * </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSupplier" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getClientName();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                                    </td>
                                    <td align="left"><span class="field-label">Payment Terms</span><span class="text-danger"> * </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTerms" runat="server" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr id="trSupplierAddress" runat="server">
                                    <td align="left"><span class="field-label">Supplier Address</span><br />
                                        <asp:Label ID="lblBuyer" runat="server" Text=""></asp:Label></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSupplierAddress" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Budget Account</span><span class="text-danger"> * </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtACTID" runat="server"></asp:TextBox><asp:HiddenField ID="h_Actid" runat="server"></asp:HiddenField>
                                        <asp:ImageButton ID="imgActid" runat="server" OnClientClick="getAccount();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                </tr>
                                <tr>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                    <td align="left"><span class="field-label">Budget Mtd/Ytd</span></td>
                                    <td align="left">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtBudMtd" runat="server" Text="" Style="text-align: right; width:100% !important;" Enabled="false"></asp:TextBox></td>
                                                <td align="center" width="5%">/</td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtBudYtd" runat="server" Text="" Style="text-align: right; width:100% !important;" Enabled="false"></asp:TextBox></td>
                                                <td align="left" width="35%"></td>
                                            </tr>
                                        </table>


                                    </td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Contact Person for Delivery</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtContact" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">PRF Mtd/Ytd</span></td>
                                    <td align="left">

                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtActMtd" runat="server" Text="" Style="text-align: right; width:100% !important;" Enabled="false"></asp:TextBox></td>
                                                <td align="center" width="5%">/</td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtActYtd" runat="server" Text="" Style="text-align: right; width:100% !important;" Enabled="false"></asp:TextBox></td>
                                                <td align="left" width="35%">
                                                    <asp:Label ID="lblRed" CssClass="text-danger" Visible="false" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="lblGreen" CssClass="text-success" Visible="false" runat="server" Text=""></asp:Label></td>
                                            </tr>
                                           <%-- <tr>
                                                
                                            </tr>--%>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblDepartment" runat="server" Visible="false" Text="Department" CssClass="field-label"></asp:Label></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlDepartment" AutoPostBack="true" Visible="false" runat="server"></asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Approval</span></td>
                                    <td align="left">
                                        <asp:RadioButton ID="radBudgeted" runat="server" GroupName="Budget" Text="Budgeted" Checked="true" />
                                        <asp:RadioButton ID="radUnBudgeted" runat="server" GroupName="Budget" Checked="false" Text="UnBudgeted" /></td>
                                </tr>
                                <tr id="trBuyer" runat="server" visible="false">
                                    <td align="left"><span class="field-label">Procurement Contact</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtProcurement" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                        <asp:Label ID="lblStatus" runat="server" Text="PRF Type" CssClass="field-label"></asp:Label></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlStatus" Enabled="false" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:GridView ID="grdPRF" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="true"
                                            CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRD_ID" runat="server" Text='<%# Bind("PRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="40%" HeaderStyle-Width="40%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRD_DESCR" runat="server" Text='<%# Bind("PRD_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPRD_DESCR" runat="server" Text='<%# Bind("PRD_DESCR") %>'></asp:TextBox><span class="text-danger"> * </span>
                                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                        <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx2" OnClientPopulated="MultiSelect"
                                                            CompletionListCssClass="completionListElement" CompletionListItemCssClass="listItem" FirstRowSelected="true"
                                                            CompletionListHighlightedItemCssClass="highlightedListItem" OnClientItemSelected="ClientItemSelected"
                                                            CompletionSetCount="5" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                            ServiceMethod="GetPrdDescr" ServicePath="PurchaseRequest.aspx" TargetControlID="txtPRD_DESCR">
                                                        </ajaxToolkit:AutoCompleteExtender>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("PRD_ITM_ID") %>' runat="server" />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPRD_DESCR" runat="server" Text='<%# Bind("PRD_DESCR") %>'></asp:TextBox><span class="text-danger"> * </span>
                                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                        <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx3" OnClientPopulated="MultiSelect"
                                                            CompletionListCssClass="completionListElement" CompletionListItemCssClass="listItem" FirstRowSelected="true"
                                                            CompletionListHighlightedItemCssClass="highlightedListItem" OnClientItemSelected="ClientItemSelected"
                                                            CompletionSetCount="5" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                            ServiceMethod="GetPrdDescr" ServicePath="PurchaseRequest.aspx" TargetControlID="txtPRD_DESCR">
                                                        </ajaxToolkit:AutoCompleteExtender>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("PRD_ITM_ID") %>' runat="server" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Additional Details" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRD_DETAILS" runat="server" Text='<%# Bind("PRD_DETAILS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPRD_DETAILS" runat="server" Text='<%# Bind("PRD_DETAILS") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPRD_DETAILS" runat="server" Text='<%# Bind("PRD_DETAILS") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quantity" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRD_QTY" runat="server" Text='<%# Bind("PRD_QTY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPRD_QTY" runat="server" Style="text-align: right" Text='<%# Bind("PRD_QTY") %>'></asp:TextBox><span class="text-danger"> * </span>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPRD_QTY" runat="server" Style="text-align: right" Text='<%# Bind("PRD_QTY") %>'></asp:TextBox><span class="text-danger"> * </span>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unit Rate" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("PRD_RATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPRD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("PRD_RATE") %>'></asp:TextBox><span class="text-danger"> * </span>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPRD_RATE" runat="server" Style="text-align: right" Text='<%# Bind("PRD_RATE") %>'></asp:TextBox><span class="text-danger"> * </span>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <%--<asp:TextBox ID="ccc" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("PRD_TOTAL") %>'></asp:TextBox>--%>
                                                        <asp:TextBox ID="ccc" runat="server" Style="text-align: right" Enabled="false" Text='<%#Eval("PRD_TOTAL", "{0:#,##0.000}")%>'></asp:TextBox>
                                                        
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPRD_TOTAL" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("PRD_TOTAL") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPRD_TOTAL" runat="server" Style="text-align: right" Enabled="false" Text='<%# Bind("PRD_TOTAL") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdatePRF" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelPRF" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddPRF" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditPRF" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" HeaderStyle-Width="5%"/>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trTotal" runat="server">
                                    <td align="left" colspan="4">
                                        <table width="100%" border="0">

                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="field-label"></asp:Label></td>
                                                <td width="30%">
                                                    <asp:DropDownList ID="ddlCurrency" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                    </td>
                                                <td width="20%"></td>
                                                <td width="30%"></td>
                                            </tr>

                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="Label3" runat="server" Text="TAX" CssClass="field-label"></asp:Label>
                                                    </td>
                                                <td width="30%">
                                                    <asp:DropDownList ID="ddlTAX" AutoPostBack="true" runat="server"></asp:DropDownList></td>
                                                <td width="20%"><asp:Label ID="Label2" runat="server" Text="Total Amount" CssClass="field-label"></asp:Label></td>
                                                <td width="30%"><asp:TextBox ID="txtGross" Style="text-align: right" Text="0.0" runat="server" Enabled="false"></asp:TextBox></td>
                                            </tr>

                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblExchRate" runat="server" Text="Exch. Rate" CssClass="field-label"></asp:Label></td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtExgRate" Style="text-align: right" runat="server" Text="1.00"></asp:TextBox></td>
                                                <td width="20%"><asp:Label ID="Label1" runat="server" Text="TAX Amount" CssClass="field-label"></asp:Label></td>
                                                <td width="30%"><asp:TextBox ID="txtTax" Style="text-align: right" Text="0.0" runat="server" Enabled="false"></asp:TextBox></td>
                                            </tr>

                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblExgCurr" runat="server" CssClass="field-label"></asp:Label></td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtAltTotal" Style="text-align: right" runat="server"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnAltTotal" runat="server"></asp:HiddenField>
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblTotal" runat="server" Text="Net Amount (AED)" CssClass="field-label"></asp:Label></td>
                                                <td width="30%">
                                                    <asp:Label ID="lblPrf_Adjust" runat="server" Text="" Style="text-align: right"></asp:Label>
                                                    <asp:TextBox ID="txtTotal" Style="text-align: right" Text="0.0" runat="server" Enabled="false"></asp:TextBox></td>
                                            </tr>

                                        </table>





                                    </td>

                                </tr>
                                <tr id="trCC" runat="server" visible="false">
                                    <td colspan="4">
                                        <asp:GridView ID="grdCCS" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="true"
                                            CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRC_ID" runat="server" Text='<%# Bind("PRC_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRC_TYPE" runat="server" Text='<%# Bind("PRM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlPRC_TYPE" runat="server"></asp:DropDownList>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:DropDownList ID="ddlPRC_TYPE" runat="server"></asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRC_DESCR" runat="server" Text='<%# Bind("PRC_CCS_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPRC_DESCR" runat="server" Text='<%# Bind("PRC_CCS_DESCR") %>'></asp:TextBox><span class="text-danger">*</span>
                                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("PRC_CCS_ID") %>' runat="server" />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPRC_DESCR" runat="server" Text='<%# Bind("PRC_CCS_DESCR") %>'></asp:TextBox><span class="text-danger">*</span>
                                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("PRC_CCS_ID") %>' runat="server" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPRC_AMT" runat="server" Text='<%# Bind("PRC_AMT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPRC_AMT" runat="server" Style="text-align: right" Text='<%# Bind("PRC_AMT") %>'></asp:TextBox><span class="text-danger">*</span>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPRC_AMT" runat="server" Style="text-align: right" Text='<%# Bind("PRC_AMT") %>'></asp:TextBox><span class="text-danger">*</span>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdatePRF" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelPRF" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddPRF" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditPRF" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trChangeRequest1" runat="server" class="title-bg-lite">
                                    <td align="center" colspan="4">
                                        <asp:LinkButton ID="LnkSearch" runat="server" OnClientClick="javascript:return false;">Change Request</asp:LinkButton></td>
                                </tr>
                                <tr id="trChangeRequest2" runat="server">
                                    <td colspan="4">
                                        <asp:Panel runat="server" ID="pnlInvoice">
                                            <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                                                <tr runat="server" id="trChangeRequest3">
                                                    <td align="left" width="20%">
                                                        <asp:Label ID="lblRSup_id" runat="server" Text="Supplier" CssClass="field-label"></asp:Label></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtRSup_id" runat="server"></asp:TextBox><asp:HiddenField ID="hdnRSup_id" runat="server"></asp:HiddenField>
                                                        <asp:ImageButton ID="imgRSup_id" runat="server" OnClientClick="getRClientName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                                    <td align="left" width="20%"><span class="field-label">Budget Account</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtRAct_id" runat="server"></asp:TextBox><asp:HiddenField ID="hdnRAct_id" runat="server"></asp:HiddenField>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="getRAccount();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblRAdjust" runat="server" Text="Adjust Amount" CssClass="field-label"></asp:Label></td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtRAdjust" Style="text-align: right" runat="server"></asp:TextBox><br />
                                                        <asp:Label ID="lblRAdjustMore" runat="server" Text="to reduce PRF Total by 10 enter +10"></asp:Label></td>
                                                    <td align="left"><span class="field-label">Cancellation Request</span></td>
                                                    <td align="left">
                                                        <asp:CheckBox ID="chkRCancel" runat="server"></asp:CheckBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><span class="field-label">Reason for Change</span></td>
                                                    <td colspan="3" align="left">
                                                        <asp:TextBox ID="txtRReason" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:Button ID="btnChange" runat="server" CausesValidation="False" CssClass="button" Text="Save Change Request" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                            CollapseControlID="LnkSearch" CollapsedSize="0" CollapsedText="Show Change Request"
                                            ExpandControlID="LnkSearch" ExpandedText="Hide Change Request" TargetControlID="pnlInvoice"
                                            TextLabelID="LnkSearch" Collapsed="True">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">

                                        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" OnClientActiveTabChanged="TabChanged" AutoPostBack="true">

                                            <ajaxToolkit:TabPanel ID="trFlow" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel2" runat="server" ScrollBars="Vertical">
                                                        <asp:Repeater runat="server" ID="rptFlow">
                                                            <HeaderTemplate></HeaderTemplate>
                                                            <ItemTemplate><span class="my-span" style="background-color: <%#DataBinder.Eval(Container.DataItem, "wfcolor")%>;"><%#DataBinder.Eval(Container.DataItem, "wfdescr")%></span></ItemTemplate>
                                                            <FooterTemplate></FooterTemplate>
                                                        </asp:Repeater>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                            <ajaxToolkit:TabPanel ID="trComments" runat="server" HeaderText="Comments">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                            <ajaxToolkit:TabPanel ID="trDocument" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <asp:FileUpload ID="UploadDocPhoto" runat="server"
                                                        ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                                    <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />

                                                    <asp:Repeater ID="rptImages" runat="server">
                                                        <ItemTemplate>
                                                            [
				<asp:TextBox ID="lblPriContentType" runat="server" Visible="false" Text='<%# Bind("pri_contenttype") %>'></asp:TextBox>
                                                            <asp:TextBox ID="lblPriFilename" runat="server" Visible="false" Text='<%# Bind("pri_filename") %>'></asp:TextBox>
                                                            <asp:TextBox ID="lblPriId" runat="server" Visible="false" Text='<%# Bind("pri_id") %>'></asp:TextBox>
                                                            <asp:LinkButton ID="btnDocumentLink" Text='<%# DataBinder.Eval(Container.DataItem, "pri_name") %>' runat="server"></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDelete" Text='x' Visible='<%# DataBinder.Eval(Container.DataItem, "pri_visible") %>' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>]
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                            <ajaxToolkit:TabPanel ID="trApproval" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtApprovals" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                            <ajaxToolkit:TabPanel ID="trEmail" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <table>
                                                        <tr>
                                                            <td align="left" width="10%">
                                                                <asp:Label ID="lblSupplierEmail" runat="server" Text="Select Supplier" CssClass="field-label"></asp:Label>
                                                            </td>

                                                            <td align="left" width="35%">
                                                                <asp:TextBox ID="txtSupplierEmail" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="imgSupplierEmail" runat="server" OnClientClick="getClientNameEmail();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                                            </td>

                                                            <td align="left" width="10%">
                                                                <asp:Label ID="lblSupplierEmailId" runat="server" Text="Email" CssClass="field-label"></asp:Label>
                                                            </td>

                                                            <td align="left" width="35%">
                                                                <asp:TextBox ID="txtSupplierEmailId" runat="server" Text=""></asp:TextBox>
                                                            </td>
                                                             <td align="left" width="10%">
                                                                <asp:HiddenField ID="hdnSupplierEmail" runat="server" />
                                                                <asp:Button ID="btnSendEmail" runat="server" CausesValidation="False" CssClass="button" Text="Send Email" />
                                                            </td>
                                                        </tr>

                                                     <%--   <tr>
                                                           
                                                        </tr>--%>

                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                            <ajaxToolkit:TabPanel ID="trWorkflow" runat="server" Visible="false">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel1" runat="server"  ScrollBars="Vertical">
                                                        <asp:GridView ID="gvWorkflow" runat="server"
                                                            EmptyDataText="No Data" Width="100%" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                                                            <Columns>
                                                                <asp:BoundField DataField="wrk_Date" HeaderText="Date" />
                                                                <asp:BoundField DataField="wrk_User" HeaderText="User" />
                                                                <asp:BoundField DataField="wrk_Action" HeaderText="Action" />
                                                                <asp:BoundField DataField="wrk_Details" HeaderText="Details" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnConfirm" runat="server" CausesValidation="False" CssClass="button" Text="Confirm" Visible="false" OnClientClick="return confirm('Are you sure you want to Confirm This Supplier ?');" />
                                        <input type="button" id="btnAccept" class="button" value=" Approve " runat="server" onclick="this.disabled = true;" onserverclick="btnAccept_Click" />
                                        <input type="button" id="btnBypass" class="button" value=" Pass " runat="server" onclick="this.disabled = true;" onserverclick="btnPass_Click" />
                                        <asp:Button ID="btnReject" runat="server" CausesValidation="False" CssClass="button" Text="Reject" />
                                        <asp:Button ID="btnCopy" runat="server" CausesValidation="False" CssClass="button" Text="Copy" />
                                        <input type="button" id="btnSend" class="button" value=" Send " runat="server" onclick="this.disabled = true;" onserverclick="btnSend_Click" />
                                        <asp:Button ID="btnBuyer" runat="server" CausesValidation="False" CssClass="button" Visible="false" />
                                        <input type="button" id="btnQuotation" class="button" value=" Quotation " runat="server" onclick="this.disabled = true;" onserverclick="btnQuotation_Click" />
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <input type="button" id="btnSave" class="button" value=" Save " runat="server" onclick="this.disabled = true;" onserverclick="btnSave_Click" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                                        <asp:Button ID="btnMessage" runat="server" CausesValidation="False" CssClass="button" Text="Message" />
                                        <asp:Button ID="btnSuper" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Cancel PRF" OnClientClick="return confirm('Are you sure you want to Cancel this Record ?');" />
                                        <input type="button" id="btnQutSave" class="button" value=" Save and Confirm " visible="false" runat="server" onclick="this.disabled = true;" onserverclick="btnQutSave_Click" />
                                        <asp:Button ID="btnCCSave" runat="server" CausesValidation="False" Visible="false" CssClass="button" Text="Save" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_PRF_Type" runat="server" />
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_empid" runat="server" />
                                        <asp:HiddenField ID="h_term" runat="server" />
                                        <asp:HiddenField ID="h_Prf_apr_id" Value="0" runat="server" />
                                        <asp:HiddenField ID="h_total" runat="server" />
                                        <asp:HiddenField ID="h_QUO_ID1" runat="server" />
                                        <asp:HiddenField ID="h_QUO_ID2" runat="server" />
                                        <asp:HiddenField ID="h_QUO_ID3" runat="server" />
                                        <asp:HiddenField ID="h_ItemID" runat="server" EnableViewState="False" />
                                        <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_PRFGridDelete" Value="0" runat="server" />
                                        <asp:HiddenField ID="h_PRCGridDelete" Value="0" runat="server" />
                                        <asp:HiddenField ID="h_Prf_Status" runat="server" />
                                        <asp:HiddenField ID="h_printed" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_block" runat="server" />
                                        <asp:HiddenField ID="h_blockForm" runat="server" />
                                        <asp:HiddenField ID="hActRefresh" Value="0" runat="server" />
                                        <asp:HiddenField ID="h_PRF_Cur_Id" Value="AED" runat="server" />
                                        <asp:HiddenField ID="h_Dpt_id" Value="0" runat="server" />
                                        <asp:HiddenField ID="h_SubDpt_id" Value="0" runat="server" />
                                        <asp:HiddenField ID="h_unBudgeted" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_TAXPerc" runat="server" Value="0" />
                                        <asp:FileUpload ID="UploadFile" runat="server" Width="250px"
                                            ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' Visible="false" />
                                        <asp:Button ID="btnUploadPrf" runat="server" CssClass="button" Text="Upload" />
                                        <asp:HyperLink ID="btnExport" runat="server" Visible="false">Export to Excel</asp:HyperLink>
                                        <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hf_ITM_ID" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_DESCR" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_QTY" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_RATE" runat="server" />
    <asp:HiddenField ID="hf_txtSAD_TOTAL" runat="server" />
</asp:Content>
