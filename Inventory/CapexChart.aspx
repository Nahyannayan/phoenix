<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"  CodeFile="CapexChart.aspx.vb" Inherits="CapexChart" title="Untitled Page" %>
<%@ Register Src="../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  <script language="javascript" type="text/javascript">

         </script>
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 640px">
        <tr>
         <td align="left"  valign="bottom" style="height: 20px">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" style="text-align: center" Height="16px"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="height: 93px; font-weight: bold;" valign="top">
                <table align="center" border="1" class="BlueTableView" bordercolor="#1b80b6" 
                    cellpadding="5" cellspacing="0"
                     id="TB1" runat="server" width="100%">
                    <tr class="subheader_img">
                        <td align="left" colspan="2" valign="middle">
                            <asp:Label id="pgTitle" runat="server"></asp:Label></td>                        
                        </tr>   
        <tr id="trDateRange" runat="server">
            <td class="matters" align="left">Date Range</td>
            <td align="left">
                <table>
                    <tr>
                        <td class="matters">From Range</td>
                        <td>
                            <asp:TextBox ID="txtFDate" runat="server" Width="110px" CssClass="inputbox"></asp:TextBox>
                            <asp:ImageButton ID="lnkFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFDate" PopupButtonID="lnkFDate"></ajaxToolkit:CalendarExtender></td>
                        <td class="matters">To Date</td>
                        <td>
                            <asp:TextBox ID="txtTDate" runat="server" Width="110px" CssClass="inputbox"></asp:TextBox>
                            <asp:ImageButton ID="lnkTDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTDate" PopupButtonID="lnkTDate"></ajaxToolkit:CalendarExtender></td></tr>
                </table>
            </td></tr>
        <tr id="trSupplier">
            <td class="matters" align="left">Capex Head</td>
            <td class="matters" align="left"><asp:TextBox ID="txtSupplier" runat="server" Width="300px" CssClass="inputbox"></asp:TextBox>
            <asp:ImageButton ID="imgClient" runat="server" OnClientClick="getClientName();return true;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
            <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
             <asp:Button ID="btnGenerateReport" runat="server" CausesValidation="False" CssClass="button"
                    Text="Generate Calendar" UseSubmitBehavior="False" TabIndex="8" /></td>
        </tr>
        <tr id = "trBSUnit" runat = "server">
            <td align="left" valign = "top" class="matters" style="width: 97px" >
                Business Unit</td>
            <td align="left" valign = "top" class="matters"> 
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" /></td></tr>                                       
        </table>
    </td>
        </tr>
        <tr>
            <td class="matters" valign="bottom" style="height: 52px">
                <asp:HiddenField ID="hfMDM_ID" runat="server" />
                 <input id="hfCategory" runat="server" type="hidden" />
                 <input id="hfSubCategory" runat="server" type="hidden" />
          </td>
          
        </tr>
    </table>
     <asp:DataList id="rptFlow"
           BorderColor="black"
           CellPadding="5"
           CellSpacing="5"
           RepeatDirection="Vertical"
           RepeatLayout="Table"
           RepeatColumns="3"
           BorderWidth="1"
           class="matters"
           runat="server">
                    <headertemplate></headertemplate>
                    <ItemStyle VerticalAlign="Top"  HorizontalAlign="Center" Width="200px" Height="150px" BorderWidth="1" BorderColor="Black" />
                    <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "cpx_mth")%><br />
                            <%#DataBinder.Eval(Container.DataItem, "cpx_sum")%><br />
                            <%#DataBinder.Eval(Container.DataItem, "cpx_prfs")%>
                        </ItemTemplate>
                    <footertemplate></footertemplate>
    </asp:DataList>

    <asp:HiddenField ID="h_CATID" runat="server" />
    <asp:HiddenField ID="h_SubCATID" runat="server" />
</asp:Content>



