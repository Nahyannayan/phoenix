
Partial Class SetAlertMsg
    Inherits System.Web.UI.Page

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Application.Remove("alertMessage")
        Application.Remove("alertTime")
        Application.Add("alertMessage", txtAlert.Text)
        Dim dtime As DateTime = Now.AddMinutes(txtTime.Text)
        Application.Add("alertTime", dtime)
    End Sub
End Class
