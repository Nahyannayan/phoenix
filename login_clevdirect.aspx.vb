Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Imports System.IO
Imports System.Security.Cryptography
Partial Class login_clevdirect
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer
    Private m_bIsTerminating As Boolean = False
    Private key() As Byte = {}
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'lblResult.Text = "OASIS WILL BE TEMPORARILY UNAVAILABLE ON 02-JAN-2009 BECAUSE OF SYSTEM UPGRADE!!!"

        LoginDirect()



    End Sub
    Private Sub getSettings(ByVal BusUnit As String)
        Try
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(BusUnit)
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_COUNTRY_ID") = Trim(Convert.ToString(BSUInformation("BSU_COUNTRY_ID")))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                        Dim strformat As String = "0."
                        For i As Integer = 1 To CInt(Session("BSU_ROUNDOFF"))
                            strformat = strformat & "0"
                        Next
                        Session("BSU_DataFormatString") = strformat
                    End While
                End Using
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim SQLStr As String
                SQLStr = "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1"
                Dim dtFinYear As DataTable
                dtFinYear = Mainclass.getDataTable(SQLStr, str_conn)
                If dtFinYear.Rows.Count > 0 Then
                    Session("F_YEAR") = dtFinYear.Rows(0).Item("FYR_ID")
                    Session("F_Descr") = dtFinYear.Rows(0).Item("FYR_DESCR")
                    Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(dtFinYear.Rows(0).Item("FYR_ID"))
                End If
            End If

            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Business Unit Selection")
        End Try
    End Sub
    Public Function Decrypt_CLEV(ByVal stringToDecrypt As String, _
           Optional ByVal sEncryptionKey As String = "!#$a54?CLEV") As String
        Dim inputByteArray(stringToDecrypt.Length) As Byte
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function
    Public Function Encrypt_CLEV(ByVal stringToEncrypt As String, _
              Optional ByVal SEncryptionKey As String = "!#$a54?CLEV") As String
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes( _
                stringToEncrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function
    Protected Sub LoginDirect()
        ' Pass the username password in the format username_password encrypted
        ' The module names store in a table and pass encrypted




        hfExpired.Value = ""
        'Bltm4wWts87nGfffrjd0SFaDDNGequQm
        Session("CLEV_USERNAME") = Decrypt_CLEV(Request.QueryString("U").Replace(" ", "+")) 'Decrypt_CLEV("hzRhYeOdgWKlxsH5+CMSePFqaIc0vlDu")
        Session("CLEV_BSU") = Decrypt_CLEV(Request.QueryString("B").Replace(" ", "+")) 'Decrypt_CLEV("H9n0KjR9Xbo=") 
        Session("CLEV_MODULE") = Decrypt_CLEV(Request.QueryString("M").Replace(" ", "+")) 'Decrypt_CLEV("eGnga/EwHuI=")

         Dim IDs As String() = Split(Session("CLEV_USERNAME"), "$$$", , CompareMethod.Text) ' Session("CLEV_USERNAME").Split("$$$")
        Session("GLGUser") = IDs(0)
        Session("GLGPwd") = IDs(1)

        Dim passwordEncr As New Encryption64
        Dim username As String = Session("GLGUser") 'passwordEncr.Decrypt(Request.QueryString("GLGUser").Replace(" ", "+"))
        Dim password As String = passwordEncr.Encrypt(Session("GLGPwd")) 'Request.QueryString("GLGPwd").Replace(" ", "+")




        Dim tblusername As String = ""
        Dim tblpassword As String = ""
        Dim tblroleid As Integer = 0
        Dim tblbsuper As Boolean = False
        Dim tblbITSupport As Boolean = False
        Dim tblbsuid As String = ""
        Dim tblbUsr_id As String = ""
        Dim tblDisplay_usr As String = ""
        Dim bCount As Integer = 0
        Dim var_F_Year As String = ""
        Dim var_F_Year_ID As String = ""
        Dim var_F_Date As String = ""
        Dim bUSR_MIS As Integer = 0
        Dim USR_bShowMISonLogin As Integer = 0
        Dim tblModuleAccess As String = ""
        Dim tblHRAccess As String = ""
        Dim tblCRMAccess As String = ""


        Dim USR_FCM_ID As String = ""
        Dim tblReqRole As String = ""
        Dim tblDays As Integer = 0

        Dim tblBSU_Name As String = ""
        Dim tblEMPID As String = ""
        Dim tblCOSTEMP As String = ""
        Dim tblCOSTSTU As String = ""
        Dim tblCOSTOTH As String = ""
        Dim tblUSR_ForcePwdChange As Integer = 0
        Session("tot_bsu_count") = "0"
        'Session("sUsr_name") = txtUsername.Text
        Try
            Dim dt As New DataTable
            Dim trycount As Integer
            Dim lstrCount As Integer
            Dim lstrCount_stud As Integer




            ''''''''''' ========================= Check For STUDENT ===========================================
            Dim str_conn_stud As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim strSQL_stud = "SELECT Count(STU_USR_NAME) as usrExist From Student_M WHERE STU_USR_NAME='" & username & "'"
            Dim dsSQL_stud As DataSet = SqlHelper.ExecuteDataset(str_conn_stud, CommandType.Text, strSQL_stud)
            lstrCount_stud = dsSQL_stud.Tables(0).Rows(0).Item("usrExist").ToString().Trim()
            If lstrCount_stud > 0 Then
                Response.Redirect("http://School.gemsoasis.com/StudentLoginbeta/logindirect.aspx?GLGUser=" & Request.QueryString("GLGUser") & "&GLGPwd=" & password & "")

            End If
            ''''''''''  =============================== END OF CHECK FOR STUDENT =========================================


            ''''''''''' ========================= Check For Parent ===========================================
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim strSQL = "SELECT Count(USR_NAME) as usrExist From Users_M WHERE USR_NAME='" & username & "'"
            Dim dsSQL As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
            lstrCount = dsSQL.Tables(0).Rows(0).Item("usrExist").ToString().Trim()
            If lstrCount = 0 Then
                Response.Redirect("http://oasis.gemseducation.com/ParentLogin/logindirect.aspx?GLGUser=" & Request.QueryString("GLGUser") & "&GLGPwd=" & password & "")

            End If
            ''''''''''  =============================== END OF CHECK FOR PARENT =========================================

            'Check Once Mopre
            Dim str_connGLG33 As String = ConnectionManger.GetOASISConnectionString
            Dim pParmsGLG33(3) As SqlClient.SqlParameter
            Dim lstrPWD As String
            lstrPWD = ""
            pParmsGLG33(0) = New SqlClient.SqlParameter("@USR_ID", username)

            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_connGLG33, CommandType.StoredProcedure, "Get_GLG_USER_ROLE", pParmsGLG33)
                While reader.Read
                    lstrPWD = Convert.ToString(reader("Roles"))
                End While
            End Using
            If lstrPWD = "PAR" Then
                Response.Redirect("http://oasis.gemseducation.com/ParentLogin/logindirect.aspx?GLGUser=" & Request.QueryString("GLGUser") & "&GLGPwd=" & password & "")
            End If

            ''''''''''  =============================== END OF CHECK FOR PARENT =========================================
            Dim objConn As New SqlConnection(str_conn) '            
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As String = "1000"
                retval = AccessRoleUser.GetUserDetails_GLG(username, password, "", "", dt, trycount, stTrans)
                stTrans.Commit()
                If retval = "0" Then
                    If dt.Rows.Count > 0 Then
                        tblusername = Convert.ToString(dt.Rows(0)("usr_name"))
                        tblDisplay_usr = Convert.ToString(dt.Rows(0)("Display_Name"))
                        tblpassword = passwordEncr.Decrypt(Convert.ToString(dt.Rows(0)("usr_password")).Replace(" ", "+"))
                        tblroleid = Convert.ToInt32(dt.Rows(0)("usr_rol_id"))
                        tblbsuper = IIf(IsDBNull(dt.Rows(0)("usr_bsuper")), False, dt.Rows(0)("usr_bsuper"))
                        tblbITSupport = IIf(IsDBNull(dt.Rows(0)("USR_bITSupport")), False, dt.Rows(0)("USR_bITSupport"))
                        tblbsuid = Convert.ToString(dt.Rows(0)("usr_bsu_id"))
                        tblbUsr_id = Convert.ToString(dt.Rows(0)("usr_id"))
                        tblModuleAccess = Convert.ToString(dt.Rows(0)("USR_MODULEACCESS"))
                        tblReqRole = Convert.ToString(dt.Rows(0)("USR_REQROLE"))
                        tblEMPID = Convert.ToString(dt.Rows(0)("USR_EMP_ID"))
                        USR_FCM_ID = Convert.ToString(dt.Rows(0)("USR_FCM_ID"))
                        tblHRAccess = Convert.ToString(dt.Rows(0)("USR_bHR"))
                        tblBSU_Name = Convert.ToString(dt.Rows(0)("BSU_NAME"))
                        tblCRMAccess = Convert.ToString(dt.Rows(0)("USR_bTempCRM"))
                        tblCOSTEMP = dt.Rows(0)("COSTcenterEMP").ToString()
                        tblCOSTSTU = dt.Rows(0)("COSTcenterSTU").ToString()
                        tblCOSTOTH = dt.Rows(0)("COSTcenterOTH").ToString()
                        tblUSR_ForcePwdChange = dt.Rows(0)("USR_ForcePwdChange")
                        If IsDBNull(dt.Rows(0)("USR_MIS")) Then
                            bUSR_MIS = 0
                        Else
                            bUSR_MIS = Convert.ToInt32(dt.Rows(0)("USR_MIS"))
                        End If
                        If IsDBNull(dt.Rows(0)("USR_bShowMISonLogin")) Then
                            USR_bShowMISonLogin = 0
                        Else
                            USR_bShowMISonLogin = Convert.ToInt32(dt.Rows(0)("USR_bShowMISonLogin"))
                        End If

                        tblDays = CInt(dt.Rows(0)("Days"))
                    End If
                Else
                    Response.Redirect("login.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                End If
            Catch ex As Exception
                stTrans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try

            password = passwordEncr.Decrypt(password.Replace(" ", "+"))
            'check for  password matches
            If password = tblpassword Then
                'if valid hold the required information into the session
                Session("sUsr_id") = tblbUsr_id
                'to used for all the page
                Session("sUsr_name") = tblusername
                Session("sUsr_Display_Name") = tblDisplay_usr
                Session("sroleid") = tblroleid
                Session("sBusper") = tblbsuper
                Session("sBsuid") = Session("CLEV_BSU") 'tblbsuid
                Session("sBITSupport") = tblbITSupport
                Session("BSU_Name") = tblBSU_Name
                Session("sModuleAccess") = tblModuleAccess
                Session("sReqRole") = tblReqRole
                Session("EmployeeId") = tblEMPID
                Session("counterId") = USR_FCM_ID

                Session("CostEMP") = tblCOSTEMP
                Session("CostSTU") = tblCOSTSTU
                Session("CostOTH") = tblCOSTOTH
                Session("ForcePwdChange") = tblUSR_ForcePwdChange
                If Session("ForcePwdChange") = 1 Then
                    Response.Redirect("Forcepasswordchange.aspx")
                End If
                'insert the session id of the cuurently logged in user
                Session("Sub_ID") = "007"
                If Request.QueryString("hd") = 1 Then
                    Session("hdv2") = "1"
                    Session("ActiveTab") = Request.QueryString("atab")
                    Session("Task_ID") = Request.QueryString("tlid")
                    Response.Redirect("~\HelpDesk\Version2\Pages\hdTabs.aspx")

                End If
                If Request.QueryString("ss") = 1 Then
                    Dim url As String
                    url = String.Format("homepageSS.aspx")
                    Session("sModule") = "SS"
                    getSettings(Session("sBsuid"))
                    Response.Redirect(url)
                End If
                Using RoleReader As SqlDataReader = AccessRoleUser.GetRoles_modules(tblroleid)
                    While RoleReader.Read
                        Session("ROL_MODULE_ACCESS") = Convert.ToString(RoleReader("ROL_MODULE_ACCESS"))
                    End While
                End Using

                'if success write into the table users_m put the sessionID in  usr_session
                Using collectReader As SqlDataReader = AccessRoleUser.getCollectBank(tblbsuid)
                    While collectReader.Read
                        Session("CollectBank") = Convert.ToString(collectReader("CollectBank"))
                        Session("Collect_name") = Convert.ToString(collectReader("Collect_name"))
                    End While
                End Using
                Using CurriculumReader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M_ID(tblbsuid)
                    While CurriculumReader.Read
                        Session("CLM") = Convert.ToString(CurriculumReader("BSU_CLM_ID"))
                    End While
                End Using
                SessionFlag = AccessRoleUser.UpdateSessionID(tblusername, Session.SessionID)
                If SessionFlag <> 0 Then
                    Throw New ArgumentException("Unable to track your request")
                End If

                Using UserreaderFINANCIALYEAR As SqlDataReader = AccessRoleUser.GetFINANCIALYEAR()
                    While UserreaderFINANCIALYEAR.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        var_F_Year_ID = Convert.ToString(UserreaderFINANCIALYEAR("FYR_ID"))
                        var_F_Year = Convert.ToString(UserreaderFINANCIALYEAR("FYR_Descr"))
                        var_F_Date = Convert.ToString(UserreaderFINANCIALYEAR("FYR_TODT"))
                    End While
                    'clear of the resource end using
                End Using
                Session("F_YEAR") = var_F_Year_ID
                Session("F_Descr") = var_F_Year
                Session("F_TODT") = Format(var_F_Date, OASISConstants.DateFormat)
                Dim OasisSettingsCookie As HttpCookie
                If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                    OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
                Else
                    OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
                End If
                OasisSettingsCookie.Values("UsrName") = txtUsername.Text
                OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
                Response.Cookies.Add(OasisSettingsCookie)
                If tblDays <= 0 Then
                    hfExpired.Value = "expired"
                    Exit Sub
                Else
                    hfExpired.Value = ""
                End If
                Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Login", "login", "Login", Page.User.Identity.Name.ToString)
                If auditFlag <> 0 Then
                    Throw New ArgumentException("Unable to track your request")
                End If

                'if super admin allow all businessunit access
                Session("USR_MIS") = bUSR_MIS
                If bUSR_MIS = 1 Then
                    Response.Redirect("~/Management/empManagementMain.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                    Exit Sub
                ElseIf bUSR_MIS = 2 Then
                    Response.Redirect("~/Management/empManagementPrincipal.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                    Exit Sub
                End If
                If tblbsuper = True Then

                    ' Response.Redirect("BusinessUnit.aspx")
                    Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                        While BSUInformation.Read
                            Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                            Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                            Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                            Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                            Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                        End While
                    End Using
                    Session("tot_bsu_count") = "1"
                    Response.Redirect("BusinessUnit.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                    Exit Sub
                Else
                    'if user with single business unit direct the user to module login page else
                    'allow the user to select the business unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        While BUnitreader.Read
                            bCount += 1
                            If bCount = 1 Then
                                Session("BSU_ROUNDOFF") = Convert.ToInt32(BUnitreader("Roundoff"))
                                Session("BSU_CURRENCY") = Convert.ToString(BUnitreader("BSU_CURRENCY"))
                                Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Session("F_YEAR"), BUnitreader("BSU_PAYYEAR")))
                                Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Month(Date.Today), BUnitreader("BSU_PAYMONTH")))
                            End If
                        End While
                    End Using

                    Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                        While BSUInformation.Read
                            Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                            Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                            Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                            Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                            Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                        End While
                    End Using
                End If
                'If tblHRAccess = "1" Then
                '    Dim url As String
                '    url = String.Format("homepageSS.aspx")
                '    Session("sModule") = "SS"
                '    BSUByPass()
                '    StoreSettings()
                '    ''Response.Redirect("http://gemsproject/OASIS_HR/Administrator/Default.aspx")
                '    Response.Redirect(url)
                '    HttpContext.Current.ApplicationInstance.CompleteRequest()
                '    m_bIsTerminating = True
                '    Exit Sub
                'End If

                'If tblCRMAccess = "1" Then
                '    Dim url As String
                '    url = String.Format("HelpDesk/Version2/Pages/hdTabs.aspx?at=1")
                '    Session("sModule") = "HD"
                '    BSUByPass_CRM()
                '    StoreSettings()
                '    ''Response.Redirect("http://gemsproject/OASIS_HR/Administrator/Default.aspx")
                '    Response.Redirect(url)
                '    HttpContext.Current.ApplicationInstance.CompleteRequest()
                '    m_bIsTerminating = True
                '    Exit Sub
                'End If


                'check the number of business unit for the current user
                If bCount > 1 Then
                    Session("tot_bsu_count") = "1"
                    Response.Redirect("Modulelogin.aspx", False) ' Response.Redirect("BusinessUnit.aspx")
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                    Exit Sub
                Else
                    If ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then

                        Session("sModule") = "E0"
                        Response.Redirect("~\eduShield\eduShield_Home.aspx", False)
                    Else
                        If tblHRAccess = "0" Then
                            Response.Redirect("Modulelogin.aspx", False)
                        Else
                            Dim url As String
                            url = String.Format("homepage.aspx")
                            Session("sModule") = "H0"
                            StoreSettings()
                            ''Response.Redirect("http://gemsproject/OASIS_HR/Administrator/Default.aspx")
                            Response.Redirect(url)
                        End If
                    End If

                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                    Exit Sub
                End If
            Else
                If lblResult.Text = "" Then
                    lblResult.Text = "Invalid username or password"
                End If
            End If
        Catch sqlEx As SqlException
            lblResult.Text = "Unable to process your request"
            UtilityObj.Errorlog(sqlEx.Message, "Login1")
        Catch generalEx As Exception
            lblResult.Text = "Invalid username or password"
            UtilityObj.Errorlog(generalEx.Message, "Login2")
        End Try
    End Sub

    Private Sub StoreSettings()
        Dim OasisSettingsCookie As HttpCookie
        If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
        Else
            OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
        End If

        OasisSettingsCookie.Values("MODULEID") = Session("sModule")
        OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
        Response.Cookies.Add(OasisSettingsCookie)

    End Sub
    Private Sub BSUByPass()
        Try
            'Session("BSU_Name") = "GEMS Education Corporate Office"
            'Session("sBsuid") = "999998"
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then

                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(Session("sBsuid"))
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                    End While
                End Using
                Session("F_YEAR") = "2011"
                Session("F_Descr") = "2011-2012"
                Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(Session("F_YEAR"))


            End If

            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Values("BSUID") = Session("sBsuid")
            OasisSettingsCookie.Values("F_YEAR") = Session("F_YEAR")
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)

            Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Business Unit", "Business Unit", "Login", Page.User.Identity.Name.ToString)
            If auditFlag <> 0 Then
                Throw New ArgumentException("Unable to track your request")
            End If


        Catch ex As Exception

        End Try
    End Sub

    Private Sub BSUByPass_CRM()
        Try
            'Session("BSU_Name") = "GEMS Education Corporate Office"
            'Session("sBsuid") = "999998"
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then

                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(Session("sBsuid"))
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_Name") = Convert.ToString(BSUInformation("BSU_NAME"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                    End While
                End Using
                Session("F_YEAR") = "2011"
                Session("F_Descr") = "2011-2012"
                Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(Session("F_YEAR"))


            End If

            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Values("BSUID") = Session("sBsuid")
            OasisSettingsCookie.Values("F_YEAR") = Session("F_YEAR")
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)

            Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Business Unit", "Business Unit", "Login", Page.User.Identity.Name.ToString)
            If auditFlag <> 0 Then
                Throw New ArgumentException("Unable to track your request")
            End If


        Catch ex As Exception

        End Try
    End Sub

    Private Sub StoreDatasForSession(ByVal ds As DataSet)
        For Each dr As DataRow In ds.Tables(0).Rows
            Session("USERDISPLAYNAME") = dr("OLU_DISPLAYNAME")
            Session("OLU_ID") = dr("OLU_ID")
            Session("username") = dr("OLU_NAME")
            Session("OLU_bsuid") = dr("OLU_BSU_ID")
            Session("BSU_NAME") = dr("BSU_NAME")
            Session("lastlogtime") = dr("lastlogtime")
            Session("logincount") = dr("OLU_LoginCount")
            Session("Login_STUID") = Convert.ToString(dr("STU_ID"))
            Session("STU_ID") = Convert.ToString(dr("STU_ID"))
            Session("STU_NAME") = Convert.ToString(dr("STU_NAME"))
            Session("STU_NO") = Convert.ToString(dr("STU_NO"))
            Session("STU_ACD_ID") = Convert.ToString(dr("STU_ACD_ID"))
            Session("sBsuid") = Convert.ToString(dr("STU_BSU_ID"))
            Session("STU_GRD_ID") = Convert.ToString(dr("STU_GRD_ID"))
            Session("sReqRole") = Convert.ToString(dr("STU_NAME"))
            Session("ACY_DESCR") = Convert.ToString(dr("ACY_DESCR"))
            Session("sroleid") = "1"
            Session("STU_BSU_ID") = Convert.ToString(dr("STU_BSU_ID"))
            Session("SUser_Name") = Convert.ToString(dr("STU_NAME"))
            Session("sModule") = "FF"
            Session("bPasswdChanged") = Convert.ToString(dr("OLU_bPasswdChanged"))
            Session("bUpdateContactDetails") = Convert.ToString(dr("OLU_bUpdateContactDetails"))
            Session("Active") = Convert.ToString(dr("ACTIVE"))
        Next
    End Sub

End Class

