﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports InfosoftGlobal
Partial Class StudProfileGeneral
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("STUID") = 0
            Dim USR_NAME As String = String.Empty
            If Convert.ToString(Session("sUsr_name")) <> "" Then
                USR_NAME = Session("sUsr_name").ToString()
            Else
                USR_NAME = ""
            End If

            ' Session("sUsr_name").ToString()
            If USR_NAME = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                If Not Request.QueryString("ID") Is Nothing Then
                    Dim STU_ID As String
                    If Request.QueryString("ID").Replace(" ", "+").Contains("=") Then
                        STU_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                    Else
                        STU_ID = Request.QueryString("ID").Replace(" ", "+")
                    End If
                    ViewState("STUID") = STU_ID
                    hdnStudId.Value = ViewState("STUID")
                End If
            End If
            If Request.Form("MethodName") = "LoadStudentDetails" Then
                ViewState("STUID") = Request.Form("ID").ToString
                LoadStudentDetails()
            Else

                LoadStudentData()
                bindAttChart()
                bindMainAttendance_details()
                PopulateAcademicYear()
                BindTransportData(ViewState("STUID"))

                binEnrolledActivity()

                bindCurriculumPerformance()
                bindHealthInfo()
                bindAchievementsByID()
                bindMEritsByID()
                bindDemeritsByID()
                Dim ds As New DataSet

                Dim ds2 As New DataSet


                ds = bindFeePaidChart(Convert.ToInt32(ViewState("STUID")), 1)

                If Not ds Is Nothing AndAlso Not ds.Tables(0) Is Nothing Then
                    radFeePiechart.DataSource = ds.Tables(0)
                    radFeePiechart.DataBind()
                End If

                Dim dsTransport As New DataSet

                dsTransport = bindTransportPaidChart(Convert.ToInt32(ViewState("STUID")), 1)

                ds2 = bindFeePaidChart(Convert.ToInt32(ViewState("STUID")), 2)

                If Not ds2 Is Nothing AndAlso Not ds2.Tables(0) Is Nothing Then
                    radFeeTotalPaidChart.DataSource = ds2.Tables(0)
                    radFeeTotalPaidChart.DataBind()
                End If

                If Not dsTransport Is Nothing AndAlso Not dsTransport.Tables(0) Is Nothing Then
                    radTransportFeePie.DataSource = dsTransport.Tables(0)
                    radTransportFeePie.DataBind()
                End If


                Dim dsCurriculum As New DataSet
                dsCurriculum = bindCurriculumChart(Convert.ToInt32(ViewState("STUID")))
                If Not dsCurriculum Is Nothing AndAlso Not dsCurriculum.Tables(0) Is Nothing AndAlso dsCurriculum.Tables(0).Rows.Count > 0 Then
                    RadCurriculumPie.DataSource = dsCurriculum.Tables(0)
                    RadCurriculumPie.DataBind()
                    RadCurriculumPie.ChartTitle.Text = "Student Performance Chart For -" + Convert.ToString(dsCurriculum.Tables(0).Rows(0)("RPF_DESCR")).ToString
                End If
                Dim dsSibling As New DataSet
                dsSibling = bindSiblingDetails(Convert.ToInt32(ViewState("SIBID")), Session("Current_ACD_ID"), Convert.ToInt32(ViewState("STUID")))
                If Not dsSibling Is Nothing AndAlso Not dsSibling.Tables(0) Is Nothing AndAlso dsSibling.Tables(0).Rows.Count > 0 Then
                    gvStudChange.DataSource = dsSibling.Tables(0)
                    gvStudChange.DataBind()
                End If

            End If



        End If
    End Sub

    Private Sub LoadStudentData()
        Try
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@STU_ID", ViewState("STUID"))


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "STU.GETSTU_PROFILE_HEADER_DETAILS_byId", param)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        Session("sSTUBSUId") = Convert.ToString(readerStudent_Detail("STUBSUID"))
                        Session("sSTUCLMId") = Convert.ToString(readerStudent_Detail("STU_CLM_ID"))

                        ltStudName.Text = Convert.ToString(readerStudent_Detail("Sname"))
                        ltStudId.Text = Convert.ToString(readerStudent_Detail("stu_no"))
                        'ltCLM.Text = Convert.ToString(readerStudent_Detail("clm"))
                        ltGrd.Text = Convert.ToString(readerStudent_Detail("grm_display")) & "-" & Convert.ToString(readerStudent_Detail("sct_descr"))
                        ltStatus.Text = Convert.ToString(readerStudent_Detail("stu_currstatus"))
                        lblPrimarycontact.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACT"))
                        lblPrimaryname.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACTNAME"))
                        lblPrimaryMobile.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACTNUMBER"))
                        lblPrimaryEmail.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACTEMAIL"))
                         ltHouseStu.Text = Convert.ToString(readerStudent_Detail("HOUSE"))
                        If (Convert.ToString(readerStudent_Detail("stu_currstatus")) <> "ACTIVE") Then
                            rowLastAtt.Visible = True
                            rowLeaveDate.Visible = True
                            ltLastAttDate.Text = Convert.ToString(readerStudent_Detail("STU_LastAttDate"))
                            ltLeaveDate.Text = Convert.ToString(readerStudent_Detail("STU_LastAttDate"))
                        End If

                        ltStudUserName.Text = Convert.ToString(readerStudent_Detail("STU_USR_NAME"))
                        lblParentUsername.Text = Convert.ToString(readerStudent_Detail("Parent_Username"))
                        ViewState("SIBID") = Convert.ToString(readerStudent_Detail("STU_SIBLING_ID"))
                        ''morwe info
                        lblSName.Text = Convert.ToString(readerStudent_Detail("Sname"))
                        lblSID.Text = Convert.ToString(readerStudent_Detail("stu_no"))
                        'ltCLM.Text = Convert.ToString(readerStudent_Detail("clm"))
                        lblSgrade.Text = Convert.ToString(readerStudent_Detail("grm_display")) & "-" & Convert.ToString(readerStudent_Detail("sct_descr"))
                        lblScurrStatus.Text = Convert.ToString(readerStudent_Detail("stu_currstatus"))


                        'ltSct.Text = Convert.ToString(readerStudent_Detail("sct_descr"))
                        lblSShift.Text = Convert.ToString(readerStudent_Detail("shf"))
                        lblsStream.Text = Convert.ToString(readerStudent_Detail("stm"))
                        lblShouse.Text = Convert.ToString(readerStudent_Detail("HOUSE"))

                        lblPprimary.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACT"))
                        lblPFname.Text = Convert.ToString(readerStudent_Detail("fname"))
                        lblMotherName.Text = Convert.ToString(readerStudent_Detail("mname"))
                        lblPusername.Text = Convert.ToString(readerStudent_Detail("ParUserName"))
                        lblFemail.Text = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                        lblMEmail.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                        lblFmobile.Text = Convert.ToString(readerStudent_Detail("STS_FMOBILE"))
                        lblMMobile.Text = Convert.ToString(readerStudent_Detail("STS_GMOBILE"))
                        'ltParUserName.Text = Convert.ToString(readerStudent_Detail("ParUserName"))
                        'ltStuUserName.Text = Convert.ToString(readerStudent_Detail("StudUserName"))
                        'ltminlist.Text = Convert.ToString(readerStudent_Detail("STU_MINLIST"))
                        'ltmintype.Text = Convert.ToString(readerStudent_Detail("STU_MINLISTTYPE"))


                        'If IsDate(readerStudent_Detail("STU_LastAttDate")) = True Then
                        '    ltLDA.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_LastAttDate"))))
                        'End If


                        'If IsDate(readerStudent_Detail("STU_LEAVEDATE")) = True Then
                        '    ltLD.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_LEAVEDATE"))))
                        'End If


                        'If Trim(ltCLM.Text) = "" Then
                        '    trCurr.Visible = False
                        'Else
                        '    trCurr.Visible = True
                        'End If

                        'If Trim(ltShf.Text) = "" Then
                        '    trshf.Visible = False
                        'Else
                        '    trshf.Visible = True
                        'End If
                        'If Trim(ltStm.Text) = "" Then
                        '    trstm.Visible = False
                        'Else
                        '    trstm.Visible = True
                        'End If

                        Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
                        Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                        Dim strImagePath As String = String.Empty
                        If strPath <> "" Then
                            strImagePath = connPath & strPath
                            imgStuImage.ImageUrl = strImagePath
                        End If
                    End While
                End If
            End Using
            ' ContactDetails()
        Catch ex As Exception

        End Try
    End Sub

    Sub bindAttChart()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", ViewState("STUID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_Att_Pattenby_Id", param)
            radAttendanceChart.DataSource = ds.Tables(0)
            radAttendanceChart.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Sub bindMainAttendance_details()
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", ViewState("STUID"))

            Dim tot_mrk As Double
            Dim DayPrs As Double
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStud_Profile_Attendance_by_Id", param)
            rptAttendance.DataSource = ds.Tables(0)
            rptAttendance.DataBind()

            'Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStud_Profile_Attendance_by_Id", param)

            '    If readerStudent_Detail.HasRows = True Then
            '        While readerStudent_Detail.Read

            '            'ltAcdWorkingDays.Text = Convert.ToString(readerStudent_Detail("tot_acddays"))
            '            'ltTotWorkTilldate.Text = readerStudent_Detail("tot_wrkdays").ToString
            '            'ltAttMarkTilldate.Text = readerStudent_Detail("tot_marked").ToString
            '            'ltDayAbsent.Text = readerStudent_Detail("tot_abs").ToString
            '            'ltDayPresent.Text = readerStudent_Detail("tot_att").ToString
            '            'ltDayLeave.Text = readerStudent_Detail("tot_leave").ToString
            '            'ltTitleAcd.Text = "Total working days for the academic year " + readerStudent_Detail("acd_year").ToString
            '            'ltTotTilldate.Text = "Total working days till " + todayDT
            '            'ltMrkTilldate.Text = "Total Attendance marked till " + todayDT
            '            'tot_mrk = Convert.ToDecimal(readerStudent_Detail("tot_marked"))
            '            'DayPrs = Convert.ToDecimal(readerStudent_Detail("tot_att"))
            '            'If tot_mrk = 0 Then
            '            '    tot_mrk = 1
            '            'End If
            '            'ltPerAtt.Text = Math.Round(((DayPrs / tot_mrk) * 100), 2)
            '        End While
            '    Else

            '    End If
            'End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception

        End Try

    End Sub

    Public Sub LoadStudentDetails()
        Try
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@STU_ID", ViewState("STUID"))


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "STU.GETSTU_PROFILE_HEADER_DETAILS_byId", param)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        lblOneName.Text = Convert.ToString(readerStudent_Detail("Sname"))
                        btnReg_Click(Nothing, Nothing)
                        'ltStudId.Text = Convert.ToString(readerStudent_Detail("stu_no"))
                        ''ltCLM.Text = Convert.ToString(readerStudent_Detail("clm"))
                        'ltGrd.Text = Convert.ToString(readerStudent_Detail("grm_display")) & "-" & Convert.ToString(readerStudent_Detail("sct_descr"))
                        'ltStatus.Text = Convert.ToString(readerStudent_Detail("stu_currstatus"))
                        'lblPrimarycontact.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACT"))
                        'lblPrimaryname.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACTNAME"))
                        'lblPrimaryMobile.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACTNUMBER"))
                        'lblPrimaryEmail.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACTEMAIL"))

                        ' ''morwe info
                        'lblSName.Text = Convert.ToString(readerStudent_Detail("Sname"))
                        'lblSID.Text = Convert.ToString(readerStudent_Detail("stu_no"))
                        ''ltCLM.Text = Convert.ToString(readerStudent_Detail("clm"))
                        'lblSgrade.Text = Convert.ToString(readerStudent_Detail("grm_display")) & "-" & Convert.ToString(readerStudent_Detail("sct_descr"))
                        'lblScurrStatus.Text = Convert.ToString(readerStudent_Detail("stu_currstatus"))


                        ''ltSct.Text = Convert.ToString(readerStudent_Detail("sct_descr"))
                        'lblSShift.Text = Convert.ToString(readerStudent_Detail("shf"))
                        'lblsStream.Text = Convert.ToString(readerStudent_Detail("stm"))
                        'lblShouse.Text = Convert.ToString(readerStudent_Detail("HOUSE"))

                        'lblPprimary.Text = Convert.ToString(readerStudent_Detail("PRIMARYCONTACT"))
                        'lblPFname.Text = Convert.ToString(readerStudent_Detail("fname"))
                        'lblMotherName.Text = Convert.ToString(readerStudent_Detail("mname"))
                        'lblPusername.Text = Convert.ToString(readerStudent_Detail("ParUserName"))
                        'lblFemail.Text = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                        'lblMEmail.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                        'lblFmobile.Text = Convert.ToString(readerStudent_Detail("STS_FMOBILE"))
                        'lblMMobile.Text = Convert.ToString(readerStudent_Detail("STS_GMOBILE"))
                        'ltParUserName.Text = Convert.ToString(readerStudent_Detail("ParUserName"))
                        'ltStuUserName.Text = Convert.ToString(readerStudent_Detail("StudUserName"))
                        'ltminlist.Text = Convert.ToString(readerStudent_Detail("STU_MINLIST"))
                        'ltmintype.Text = Convert.ToString(readerStudent_Detail("STU_MINLISTTYPE"))


                        'If IsDate(readerStudent_Detail("STU_LastAttDate")) = True Then
                        '    ltLDA.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_LastAttDate"))))
                        'End If


                        'If IsDate(readerStudent_Detail("STU_LEAVEDATE")) = True Then
                        '    ltLD.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_LEAVEDATE"))))
                        'End If


                        'If Trim(ltCLM.Text) = "" Then
                        '    trCurr.Visible = False
                        'Else
                        '    trCurr.Visible = True
                        'End If

                        'If Trim(ltShf.Text) = "" Then
                        '    trshf.Visible = False
                        'Else
                        '    trshf.Visible = True
                        'End If
                        'If Trim(ltStm.Text) = "" Then
                        '    trstm.Visible = False
                        'Else
                        '    trstm.Visible = True
                        'End If

                        'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
                        'Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                        'Dim strImagePath As String = String.Empty
                        'If strPath <> "" Then
                        '    strImagePath = connPath & strPath
                        '    imgStuImage.ImageUrl = strImagePath
                        'End If
                    End While
                End If
            End Using
            ' ContactDetails()
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btnReg_Click(sender As Object, e As EventArgs)

    End Sub

    ''curriculum starts here

    Sub bindCurriculumPerformance()
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", ViewState("STUID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rpt.getPerformance_tracker_graph", param)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblCatVerbalSAS.Text = ds.Tables(0).Rows(0)("FB_VERBAL").ToString
                    lblQuantitativeSAS.Text = ds.Tables(0).Rows(0)("FB_QUANTITIVE").ToString
                    lblNonVerbalSAS.Text = ds.Tables(0).Rows(0)("FB_NONVERBAL").ToString
                    lblSpatialSAS.Text = ds.Tables(0).Rows(0)("FB_SPATIAL").ToString
                    lblOverallSAS.Text = ds.Tables(0).Rows(0)("FB_MEANCAT").ToString


                    lblEngPercentile.Text = ds.Tables(0).Rows(0)("FB_ENG_PERCENTAILE").ToString
                    lblEngStanine.Text = ds.Tables(0).Rows(0)("FB_ENG_STANINE").ToString
                    lblMathsPercentile.Text = ds.Tables(0).Rows(0)("FB_MAT_PERCENTAILE").ToString
                    lblMathsStenine.Text = ds.Tables(0).Rows(0)("FB_MAT_STANINE").ToString
                    lblSciencePercentile.Text = ds.Tables(0).Rows(0)("FB_SCI_PERCENTAILE").ToString
                    lblScienceStenine.Text = ds.Tables(0).Rows(0)("FB_SCI_STANINE").ToString

                    lblEngMark.Text = ds.Tables(0).Rows(0)("FB_FINAL_ENG").ToString
                    lblEngGrade.Text = ds.Tables(0).Rows(0)("FB_FINAL_ENG_GRADE").ToString
                    lblMathsMark.Text = ds.Tables(0).Rows(0)("FB_FINAL_MAT").ToString
                    lblMathsGrade.Text = ds.Tables(0).Rows(0)("FB_FINAL_MAT_GRADE").ToString
                    lblScienceMark.Text = ds.Tables(0).Rows(0)("FB_FINAL_SCI").ToString
                    lblScienceGrade.Text = ds.Tables(0).Rows(0)("FB_FINAL_SCI_GRADE").ToString

                    lblTerm1EngMark.Text = ds.Tables(0).Rows(0)("FB_T1_ENG").ToString
                    lblTerm1EngGrade.Text = ds.Tables(0).Rows(0)("FB_T1_ENG_GRADE").ToString
                    lblTerm1MathsMark.Text = ds.Tables(0).Rows(0)("FB_T1_MAT").ToString
                    lblTerm1MathsGrade.Text = ds.Tables(0).Rows(0)("FB_T1_MAT_GRADE").ToString
                    lblTerm1ScienceMark.Text = ds.Tables(0).Rows(0)("FB_T1_SCI").ToString
                    lblTerm1ScienceGrade.Text = ds.Tables(0).Rows(0)("FB_T1_SCI_GRADE").ToString
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'If hfType.Value = "ace" Then
        '    BindAceReports()
        'Else
        BindReports()
        'End If

        Session("ReportYear") = ddlAcademicYear.SelectedValue.ToString
        Session("ReportYear_DESC") = ddlAcademicYear.SelectedItem.Text
    End Sub

    Protected Sub lnkReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkReport As LinkButton = DirectCast(sender, LinkButton)
        Dim lblRpf As Label = TryCast(sender.FindControl("lblRpf"), Label)
        Dim lblRsm As Label = TryCast(sender.FindControl("lblRsm"), Label)
        Dim lblDate As Label = TryCast(sender.FindControl("lblDate"), Label)
        Dim lblReport As Label = TryCast(sender.FindControl("lblReport"), Label)
        Dim lblAcdId As Label = TryCast(sender.FindControl("lblAcdId"), Label)
        Dim imgNew As Image = TryCast(sender.FindControl("imgNew"), Image)

        Session("HFrpf_id") = lblRpf.Text
        Session("HFrsm_id") = lblRsm.Text
        Session("HFrpf_date") = lblDate.Text
        Session("RPF_DESCR") = lblReport.Text
        Session("RSM_ACD_ID") = lblAcdId.Text
        imgNew.Visible = False

        If rdView.Checked = True Then
            Session("downloadreport") = "0"

        ElseIf rdDownload.Checked = True Then
            Session("downloadreport") = "1"
        End If


        Dim connection As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction

        sqltran = con.BeginTransaction("trans")
        Try


            Save_Registration(sqltran)

            sqltran.Commit()

            ' ifSibDetail.Src = "progressReportHome.aspx?id=" + Encr_decrData.Encrypt(Session("PRS_ID"))
            ifSibDetail.Src = "https://school.gemsoasis.com/GEMSPARENT/curriculum/progressReportHome.aspx?id=" + Encr_decrData.Encrypt(Session("PRS_ID"))
        Catch ex As Exception


            sqltran.Rollback()
        End Try





        If rdView.Checked = True Then
            Session("downloadreport") = "0"
            mpe.Show()
        ElseIf rdDownload.Checked = True Then
            Session("downloadreport") = "1"
            'ifReport.Attributes("src") = Server.MapPath("~/ParentLogin/progressReportHome.aspx?id=" + Encr_decrData.Encrypt(Session("PRS_ID")))
            ifReport.Attributes("src") = "https://school.gemsoasis.com/GEMSPARENT/curriculum/progressReportHome.aspx?id=" + Encr_decrData.Encrypt(Session("PRS_ID"))
        End If

    End Sub

    Sub PopulateAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String

        If Session("sSTUBSUId") = "123004" And Session("sSTUCLMId") = 50 Then
            str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                       & " ON B.ACD_ACY_ID=A.ACY_ID INNER JOIN STUDENT_PROMO_S ON STP_ACD_ID=ACD_ID " _
                       & " WHERE ACD_BSU_ID='" + Session("sSTUBSUId") + "'" _
                       & " AND STP_STU_ID=" + ViewState("STUID") _
                       & " AND ACD_ACY_ID>=19 ORDER BY ACY_ID"
        Else
            str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                      & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sSTUBSUId") + "' AND ACD_CLM_ID=" + Session("sSTUCLMId") _
                                      & " AND ACD_ACY_ID>=19 ORDER BY ACY_ID"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("sSTUBSUId") + "' AND ACD_CLM_ID=" + Session("sSTUCLMId")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)

        ''commentd by nahyan 
        ''ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True

        If Not (ddlAcademicYear.Items.FindByText(ddlAcademicYear.Items.IndexOf(li))) Is Nothing Then
            ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        End If

    End Sub

    Sub BindReports()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT dbo.title_case(RPF_DESCR) RPF_DESCR,RPF_ID,isnull(RPF_DATE,getdate())RPF_DATE ,RPF_RSM_ID,RPF_DESCR RPF,RSM_ACD_ID,RPP_RELEASEDATE FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                             & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID" _
                             & " INNER JOIN RPT.REPORT_STUDENTS_PUBLISH AS C ON A.RPF_ID=C.RPP_RPF_ID" _
                             & " where C.RPP_STU_ID=" + ViewState("STUID").ToString + "  AND C.RPP_bRELEASEONLINE=1 AND " _
                             & " DATEDIFF(DAY,C.RPP_RELEASEDATE,getdate())>=0 " _
                             & " AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlReports.DataSource = ds
        dlReports.DataBind()
    End Sub

    Sub BindAceReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT dbo.title_case(RPF_DESCR) RPF_DESCR,RPF_ID,isnull(RPF_DATE,getdate())RPF_DATE ,RPF_RSM_ID,RPF_DESCR RPF,RSM_ACD_ID,RPP_RELEASEDATE FROM RPT.ACE_REPORT_PRINTEDFOR_M AS A " _
                           & " INNER JOIN RPT.ACE_REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID" _
                           & " INNER JOIN RPT.ACE_REPORT_STUDENTS_PUBLISH AS C ON A.RPF_ID=C.RPP_RPF_ID" _
                           & " where C.RPP_STU_ID=" + ViewState("STUID").ToString + "  AND C.RPP_bRELEASEONLINE=1 AND " _
                           & " DATEDIFF(DAY,C.RPP_RELEASEDATE,getdate())>=0 " _
                           & " AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlReports.DataSource = ds
        dlReports.DataBind()
    End Sub

    Public Function bindCurriculumChart(ByVal stuId As Integer) As DataSet
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter


            PARAM(0) = New SqlParameter("@STU_ID", stuId)


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[RPT].[RESULT_ANALYSIS_HLAP_GRAPH_new]", PARAM)

            Return dsDetails


        Catch ex As Exception

        End Try
    End Function

    Private Sub Save_Registration(sqltran As SqlTransaction)
        Dim pParms(16) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@PRS_HFrpf_id", Session("HFrpf_id"))
        pParms(1) = New SqlClient.SqlParameter("@PRS_HFrsm_id", Session("HFrsm_id"))
        pParms(2) = New SqlClient.SqlParameter("@PRS_STP_GRD_ID", Session("STP_GRD_ID"))
        pParms(3) = New SqlClient.SqlParameter("@PRS_STU_BSU_ID", Session("STU_BSU_ID"))
        pParms(4) = New SqlClient.SqlParameter("@PRS_RSM_ACD_ID", Session("RSM_ACD_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PRS_STU_ID", Session("STU_ID"))
        pParms(6) = New SqlClient.SqlParameter("@PRS_ReportYear_DESC", Session("ReportYear_DESC"))
        pParms(7) = New SqlClient.SqlParameter("@PRS_bAOLReport", Session("bAOLReport"))
        pParms(8) = New SqlClient.SqlParameter("@PRS_Active_menu", Session("Active_menu"))
        pParms(9) = New SqlClient.SqlParameter("@PRS_RPF_DESCR", Session("RPF_DESCR"))
        pParms(10) = New SqlClient.SqlParameter("@PRS_ReportYear", Session("ReportYear"))
        pParms(11) = New SqlClient.SqlParameter("@PRS_HFrpf_date", Session("HFrpf_date"))
        pParms(12) = New SqlClient.SqlParameter("@PRS_downloadreport", Session("downloadreport"))
        pParms(13) = New SqlClient.SqlParameter("@PRS_USR_NAME", Session("username"))

        pParms(14) = New SqlClient.SqlParameter("@PRS_ID", SqlDbType.VarChar, 200)
        pParms(14).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "rpt.SAVE_PROGRESS_REPORT_SES", pParms)
        Dim PRS_ID As String = Convert.ToString(pParms(14).Value)
        Session("PRS_ID") = PRS_ID

    End Sub

    '' transport main 
    Sub BindTransportData(ByVal stuId As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBL_DESCRIPTION+'-'+A.PNT_DESCRIPTION AS PICKUP,SBL_DESCRIPTION+'-'+B.PNT_DESCRIPTION,ISNULL(PBM.BNO_DESCR,'') SSV_PICKUP_BUSNO,ISNULL(DBM.BNO_DESCR,'') SSV_DROPOFF_BUSNO " _
                                & " FROM dbo.STUDENT_SERVICES_D AS SD WITH(NOLOCK) INNER JOIN OASIS_TRANSPORT.TRANSPORT.SUBLOCATION_M WITH(NOLOCK) ON SBL_ID=SSV_SBL_ID " _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.PICKUPPOINTS_M A WITH(NOLOCK) ON A.PNT_ID=SSV_PICKUP AND SSV_SBL_ID=A.PNT_SBL_ID " _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.PICKUPPOINTS_M B WITH(NOLOCK) ON B.PNT_ID=SSV_DROPOFF AND SSV_SBL_ID=B.PNT_SBL_ID " _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.TRIPS_D AS PTD WITH(NOLOCK) ON PTD.TRD_TRP_ID=SSV_PICKUP_TRP_ID AND PTD.TRD_TODATE IS NULL" _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.BUSNOS_M AS PBM WITH(NOLOCK) ON PBM.BNO_ID=PTD.TRD_BNO_ID" _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.TRIPS_D AS DTD WITH(NOLOCK) ON DTD.TRD_TRP_ID=SSV_PICKUP_TRP_ID AND DTD.TRD_TODATE IS NULL" _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.BUSNOS_M AS DBM WITH(NOLOCK) ON DBM.BNO_ID=DTD.TRD_BNO_ID" _
                                & " WHERE SSV_STU_ID='" + stuId + "' AND SSV_SVC_ID = 1 AND SSV_TODATE IS NULL "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                lblPickupPoint.Text = ds.Tables(0).Rows(0).Item(0)
                lblDropoffPoint.Text = ds.Tables(0).Rows(0).Item(1)
                lblPickupBusNo.Text = ds.Tables(0).Rows(0).Item(2)
                lblDropoffBusNo.Text = ds.Tables(0).Rows(0).Item(3)
            End If
        End If

    End Sub

    ''transport ends here

    ''fee starts here
    Public Function bindFeePaidChart(ByVal stuId As Integer, ByVal feechartType As Integer) As DataSet
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter


            PARAM(0) = New SqlParameter("@STUId", stuId)
            PARAM(1) = New SqlParameter("@FeePaidType", feechartType)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetFeePaidHistoryByStudentId", PARAM)

            Return dsDetails


        Catch ex As Exception

        End Try
    End Function
    ''fee end here

    ''Enrolled activity
    Sub binEnrolledActivity()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", ViewState("STUID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_Enrolled_ActivityByStudentId", param)
            gvActivity.DataSource = ds.Tables(0)
            gvActivity.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    ''Enrolledactivity

    ''health info

    Sub bindHealthInfo()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_HealthInfo_bystudentId", param)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblIsAllergy.Text = ds.Tables(0).Rows(0)("Allergies").ToString
                    lblAllergyDetails.Text = ds.Tables(0).Rows(0)("AllergiesNotes").ToString

                    lblIsDisability.Text = ds.Tables(0).Rows(0)("Disabilities").ToString
                    lblDisabilitydetails.Text = ds.Tables(0).Rows(0)("DisabilitiesNotes").ToString

                    lblIsSpclMed.Text = ds.Tables(0).Rows(0)("Medication").ToString
                    lblMedNotes.Text = ds.Tables(0).Rows(0)("Medication Notes").ToString

                    lblPedRestriction.Text = ds.Tables(0).Rows(0)("PhysicalEducationRestrictions").ToString
                    lblAnyhealthInfo.Text = ds.Tables(0).Rows(0)("Physical EducationRestrictionsNotes").ToString

                    lblLIsTherapy.Text = ds.Tables(0).Rows(0)("Anysortoflearningsupportortherapy").ToString
                    lblTherapyNotes.Text = ds.Tables(0).Rows(0)("Learningsupportortherapynotes").ToString

                    lblIsSpclEduNeeds.Text = ds.Tables(0).Rows(0)("specialeducationneeds").ToString
                    lblSpclEduNeeds.Text = ds.Tables(0).Rows(0)("SpecialEducationNeedsNote").ToString

                    lblIsEngSupport.Text = ds.Tables(0).Rows(0)("Englishsupport").ToString
                    lblEngNotes.Text = ds.Tables(0).Rows(0)("EnglishSupportNotes").ToString

                    lblbehaviourconcern.Text = ds.Tables(0).Rows(0)("Behaviourconcern").ToString
                    lblbehaviournotes.Text = ds.Tables(0).Rows(0)("behaviourcomments").ToString

                    lblIsAnyspecificEnrichment.Text = ds.Tables(0).Rows(0)("enrichmentactivities").ToString
                    lblEnrichmentNotes.Text = ds.Tables(0).Rows(0)("Specificenrichmentactivitiesnotes").ToString

                    lblIsMusicallyProf.Text = ds.Tables(0).Rows(0)("musicallyproficient").ToString
                    lblMusicNotes.Text = ds.Tables(0).Rows(0)("Musicallyproficientnotes").ToString

                    lblExtraCurriculurActivities.Text = ds.Tables(0).Rows(0)("ExtraCurriculars").ToString
                    lblInterestingSports.Text = ds.Tables(0).Rows(0)("Sportschildisinterestedin").ToString
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    ''achievement

    Sub bindAchievementsByID()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))
            param(1) = New SqlClient.SqlParameter("@AChvmntType", 3)


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Achievements_Graph", param)

            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                rptAchievements.DataSource = ds.Tables(0)
                rptAchievements.DataBind()
            End If

        Catch ex As Exception
        End Try

    End Sub

    Sub bindMEritsByID()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))
            param(1) = New SqlClient.SqlParameter("@AChvmntType", 1)


            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Achievements_Graph", param)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                rptBehaviouralMerits.DataSource = ds.Tables(0)
                rptBehaviouralMerits.DataBind()
            End If
        Catch ex As Exception
        End Try

    End Sub

    Sub bindDemeritsByID()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))
            param(1) = New SqlClient.SqlParameter("@AChvmntType", 2)


            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Achievements_Graph", param)

            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                rptDemerit.DataSource = ds.Tables(0)
                rptDemerit.DataBind()
            End If
        Catch ex As Exception
        End Try

    End Sub
    Public Function bindTransportPaidChart(ByVal stuId As Integer, ByVal feechartType As Integer) As DataSet
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter


            PARAM(0) = New SqlParameter("@STUId", stuId)
            PARAM(1) = New SqlParameter("@FeePaidType", feechartType)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetTransportFeePaidHistoryByStudentId", PARAM)

            Return dsDetails


        Catch ex As Exception

        End Try
    End Function
    Function bindSiblingDetails(ByVal SiblingId As Integer, ByVal AcdId As Integer, ByVal StudentId As Integer) As DataSet
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter


            PARAM(0) = New SqlParameter("@SIBId", SiblingId)
            PARAM(1) = New SqlParameter("@ACD_ID", AcdId)
            PARAM(2) = New SqlParameter("@STUD_ID", StudentId)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GETSIBLINGLIST", PARAM)


            Return dsDetails

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class
