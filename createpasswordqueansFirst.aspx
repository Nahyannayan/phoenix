<%@ Page Language="VB" AutoEventWireup="false" CodeFile="createpasswordqueansFirst.aspx.vb" Inherits="createpasswordqueansFirst" %>

<%@ Register Src="createpasswordquestions.ascx" TagName="createpasswordquestions"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<base target="_self" /> 
    <title></title>
    <link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body  style="background-image: url(images/loginImage/background1.jpg); background-repeat: repeat;">
    <form id="form1" runat="server">
    <div class="matters" style="background-image: url(images/loginImage/background1.jpg); background-repeat: repeat;">
    <br />
    &nbsp; &nbsp; &nbsp; GEMS OASIS Password Recovery Process 
    <br />
    <br />
    &nbsp; &nbsp; &nbsp; (1)	Please enter the email address to which you want the new password to be sent &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;in case you forget your password.
    <br />
    <br />
    &nbsp; &nbsp; &nbsp; (2)	Answer the security  question. 
    <center>
    <br />
    <uc1:createpasswordquestions ID="Createpasswordquestions1" closeflag="true" runat="server" />

     <img src="Images/alert.png" width="15px" height="15px" /> Please remember the answer to the security question.
     <br />
     <br />
    </center>
    </div>
    </form>
</body>
</html>
