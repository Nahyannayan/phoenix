Imports Lesnikowski.Barcode
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration

Partial Class Library_Barcode_Barcode
    Inherits BasePage

    Dim Encr_decrData As New Encryption64
    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If HttpContext.Current.Session("sUsr_name") & "" = "" Then
            HttpContext.Current.Response.Redirect("~/login.aspx", False)
            Exit Sub
        End If
        'Dim MainMnu_code As String
        'Dim CurUsr_id As String = Session("sUsr_id")
        'Dim CurRole_id As String = Session("sroleid")
        'Dim CurBsUnit As String = Session("sBsuid")
        'Dim USR_NAME As String = Session("sUsr_name")
        'Dim datamode As String = "none"

        'If Not Request.UrlReferrer Is Nothing Then
        '    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
        'End If
        'If Request.QueryString("datamode") <> "" Then
        '    datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        'Else
        '    datamode = ""
        'End If
        'If Request.QueryString("MainMnu_code") <> "" Then
        '    MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        'Else
        '    MainMnu_code = ""
        'End If
        ''check for the usr_name and the menucode are valid otherwise redirect to login page

        'If USR_NAME = "" Or MainMnu_code <> "F301005" Then
        '    If Not Request.UrlReferrer Is Nothing Then
        '        Response.Redirect(Request.UrlReferrer.ToString())
        '    Else
        '        Response.Redirect("~\noAccess.aspx")
        '    End If
        'End If

        Dim type As String = Request.QueryString("Type")
        Select Case type
            Case "TRANSPORT_STUDENT"
                PrintCard_Transport()
            Case Else
                PrintCard()
        End Select
    End Sub

    Public Sub PrintCard_Transport()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim ids = ""
        Dim SP = ""
        If Request.QueryString("Type") = "TRANSPORT_STUDENT" Then
            SP = "[TRANSPORT_IDENTITY_CARD_ISSUE_STUDENT]"
            ids = ""
        ElseIf Request.QueryString("Type") = "EMPLOYEE" Then
            SP = ""
            ids = ""
        End If
        If Session("CardhashtableAll") = "All" Then
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Bsu", Session("sbsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)
        Else
            Dim hash As New Hashtable
            If Session("Cardhashtable") Is Nothing Then
            Else
                hash = Session("Cardhashtable")
            End If
            Dim idictenum As IDictionaryEnumerator
            idictenum = hash.GetEnumerator()
            While (idictenum.MoveNext())
                ids &= idictenum.Value.ToString & ","
            End While
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@IDS", ids)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            IssueCard(ds, Request.QueryString("Type"))
        End If
    End Sub

    Public Sub PrintCard()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim ds As New DataSet

        Dim ids = ""
        Dim SP = ""

        If Request.QueryString("Type") = "STUDENT" Then

            SP = "IDENTITY_CARD_ISSUE_STUDENT"
            ids = ""

        ElseIf Request.QueryString("Type") = "EMPLOYEE" Then

            str_conn = ConnectionManger.GetOASISConnectionString
            If Session("printIDType") = "GEMS CARD - NEW 2014" Or Session("printIDType") = "INFRACARE" Then
                SP = "GET_EMPLOYEE_IDENTITYCARD_NEWFORMAT"
            Else
                SP = "GET_EMPLOYEE_IDENTITYCARD"
            End If

            ids = ""
            ' Session("CardhashtableAll") = "All"
        ElseIf Request.QueryString("Type") = "EMPLOYEE_STS" Then

            str_conn = ConnectionManger.GetOASISConnectionString
            SP = "GET_EMPLOYEE_IDENTITYCARD_STS"
            ids = ""

        End If




        If Session("CardhashtableAll") = "All" Then

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Bsu", Session("sbsuid"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)

        Else

            Dim hash As New Hashtable

            If Session("Cardhashtable") Is Nothing Then
            Else
                hash = Session("Cardhashtable")
            End If

            Dim idictenum As IDictionaryEnumerator
            idictenum = hash.GetEnumerator()


            Dim i = 0
            Dim comma = ""
            While (idictenum.MoveNext())

                Dim key = idictenum.Key
                Dim id = idictenum.Value

                Dim stuid = idictenum.Value

                ids &= comma & id
                comma = ","
            End While
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@IDS", SqlDbType.VarChar)
            pParms(1) = New SqlClient.SqlParameter("@UPD_USER", SqlDbType.VarChar)
            pParms(0).Value = ids
            pParms(1).Value = Session("sUsr_name")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)


        End If



        If ds.Tables(0).Rows.Count > 0 Then
            If Session("printIDType") = "GEMS CARD - NEW 2014" Or Session("printIDType") = "INFRACARE" Then
                IssueCard_Employee_STS(ds, "EMPLOYEE_STS")
            Else
                IssueCard(ds, Request.QueryString("Type"))
            End If

            'IssueCard(ds, "EMPLOYEE")

        End If



    End Sub

    Public Sub IssueCard_Employee_STS(ByVal ds As DataSet, ByVal type As String)

        If type = "EMPLOYEE_STS" Then

            Dim photopath As String = WebConfigurationManager.AppSettings("StudentPhotoPath").ToString

            Dim dsSet As New dsImageRpt

            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1

                ''Barcode
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                barcode.Number = ds.Tables(0).Rows(i).Item("EMPNO").ToString()
                barcode.ChecksumAdd = True
                barcode.IsNumberVisible = False
                barcode.NarrowBarWidth = 3
                barcode.Height = 300
                barcode.FontHeight = 0.3F
                Dim b As Byte()
                ReDim b(barcode.Render(ImageType.Png).Length)
                b = barcode.Render(ImageType.Png)

                'Dim stupath = photopath & "\" & ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString()
                Dim SImage As Byte()

                'If System.IO.File.Exists(stupath) = False Then
                '    stupath = photopath & "\NOIMG\no_image.jpg"
                'End If
                ' Dim stupath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString()
                If ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString() = "" Then Continue For

                Dim stupath As String = "\\172.25.26.20\oasisphotos\OASIS_HR\ApplicantPhoto\" & ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString()

                Dim fs As New System.IO.FileStream(stupath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                SImage = New Byte(fs.Length - 1) {}
                fs.Read(SImage, 0, Convert.ToInt32(fs.Length))
                fs.Close()

                Dim BSU_badges_Image As Byte()
                Dim BSU_badges_path As String = "\\172.25.26.20\oasisphotos\School_Card_Badges\" & Session("Card_New_2014_BSU_ID") & "\BSU_IMAGE.jpg"
                If System.IO.File.Exists(BSU_badges_path) = False Then
                    BSU_badges_path = "\\172.25.26.20\oasisphotos\School_Card_Badges\NOIMG\NoImage_Blank.jpg"
                End If

                Dim fsBSU As New System.IO.FileStream(BSU_badges_path, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                BSU_badges_Image = New Byte(fsBSU.Length - 1) {}
                fsBSU.Read(BSU_badges_Image, 0, Convert.ToInt32(fsBSU.Length))
                fsBSU.Close()

                Dim dr As dsImageRpt.dsEMP_IDImg_STSRow = dsSet.dsEMP_IDImg_STS.NewdsEMP_IDImg_STSRow
                dr.EMP_ID = ds.Tables(0).Rows(i).Item("EMP_ID").ToString()
                dr.EMP_NO = ds.Tables(0).Rows(i).Item("EMPNO").ToString()
                dr.EMP_DISP_NAME = ds.Tables(0).Rows(i).Item("EMP_NAME").ToString()
                dr.EMP_DESC = ds.Tables(0).Rows(i).Item("DES_DESCR").ToString()
                dr.EMP_PHOTO = SImage
                dr.EMP_BSU_NAME = ds.Tables(0).Rows(i).Item("EMP_BSU_NAME").ToString()
                dr.EMP_CONT_NO = ds.Tables(0).Rows(i).Item("EMP_EMER_CONT_NO").ToString()
                dr.EMP_DOJ = Format(ds.Tables(0).Rows(i).Item("EMP_JOINDT"), OASISConstants.DateFormat)
                dr.EMP_PICKUP_AREA = ds.Tables(0).Rows(i).Item("SBL_DESCRIPTION").ToString()
                dr.EMP_BUS_NO = ds.Tables(0).Rows(i).Item("BNO_DESCR").ToString()
                dr.EMP_BARCODE = b
                dr.EMP_BSU_IMAGE = BSU_badges_Image

                dsSet.dsEMP_IDImg_STS.Rows.Add(dr)
            Next

            repClassVal = New RepClass
            Dim strResourseName As String = String.Empty
            Select Case Session("printIDType")
                Case "GEMS CARD - NEW 2014"

                    strResourseName = "Employeecard_NEW_2014.rpt"

                Case "STS"
                    If Session("printBSUID") = "900501" Or Session("printBSUID") = "900500" Then
                        strResourseName = "Employeecard_STS_EMP.rpt"
                        ''included by nahyan for STSSDET id card emp 27feb
                    ElseIf Session("printBSUID") = "900530" Then
                        strResourseName = "Employeecard_STSSDET.rpt"
                    Else
                        strResourseName = "Employeecard_STS.rpt"
                        'strResourseName = "Employeecard_NEW_2014.rpt"
                    End If
                Case "BBT"
                    If Session("printBSUID") = "900501" Or Session("printBSUID") = "900500" Then
                        strResourseName = "Employeecard_BBT_EMP.rpt"
                    Else
                        strResourseName = "Employeecard_BBT.rpt"
                    End If
                Case "GARAGE"
                    strResourseName = "Employeecard_GARAGE_EMP.rpt"
                Case "HSBC"
                    strResourseName = "Employeecard_HSBC_EMP.rpt"
                Case "INFRACARE"
                    strResourseName = "Employeecard_Infracare.rpt"
            End Select
            repClassVal.ResourceName = strResourseName
            repClassVal.SetDataSource(dsSet)
            CrystalReportViewer1.ReportSource = repClassVal
        End If

        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As Button = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = "completed"
                End If
            End If
        Catch ex As Exception

        Finally
            'repClassVal.Dispose()

        End Try

    End Sub


    Public Sub IssueCard_Employee(ByVal ds As DataSet, ByVal type As String)

        If type = "EMPLOYEE" Then

            Dim photopath As String = WebConfigurationManager.AppSettings("StudentPhotoPath").ToString

            Dim dsSet As New dsImageRpt

            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                ''Student Image
                'Dim stupath = photopath & "\" & ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString()
                Dim SImage As Byte()

                'If System.IO.File.Exists(stupath) = False Then
                '    stupath = photopath & "\NOIMG\no_image.jpg"
                'End If
                ' Dim stupath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString()
                If ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString() = "" Then Continue For

                Dim stupath As String = "\\172.25.26.20\oasisphotos\OASIS_HR\ApplicantPhoto\" & ds.Tables(0).Rows(i).Item("EMD_PHOTO").ToString()

                Dim fs As New System.IO.FileStream(stupath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                SImage = New Byte(fs.Length - 1) {}
                fs.Read(SImage, 0, Convert.ToInt32(fs.Length))
                fs.Close()

                Select Case Session("printIDType")
                    Case "GEMS_SCHOOL", "SAFECOR"
                        Dim barcode As BaseBarcode
                        barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                        barcode.Number = ds.Tables(0).Rows(i).Item("EMPNO").ToString()
                        barcode.ChecksumAdd = True
                        barcode.NarrowBarWidth = 3
                        barcode.Height = 300
                        barcode.FontHeight = 0.3F
                        barcode.ForeColor = Drawing.Color.Black
                        barcode.FontName = "Corisande-Regular"

                        Dim b As Byte()
                        ReDim b(barcode.Render(ImageType.Png).Length)
                        b = barcode.Render(ImageType.Png)

                        Dim dr As dsImageRpt.DSSTUDENT_IDCARDRow = dsSet.DSSTUDENT_IDCARD.NewDSSTUDENT_IDCARDRow
                        dr.STU_ID = ds.Tables(0).Rows(i).Item("EMP_ID").ToString()
                        dr.STU_NO = ds.Tables(0).Rows(i).Item("EMPNO").ToString()
                        dr.STU_NAME = ds.Tables(0).Rows(i).Item("EMP_NAME").ToString()
                        dr.STU_GRD_SEC = ds.Tables(0).Rows(i).Item("DES_DESCR").ToString()
                        dr.STU_IMAGE = SImage
                        dr.STU_BARCODE = b
                        dr.STU_BSU_NAME = Session("BSU_Name").ToString
                        dr.STU_BSU_IMAGE = ds.Tables(0).Rows(i).Item("BSU_IMAGE")
                        dsSet.DSSTUDENT_IDCARD.Rows.Add(dr)
                    Case Else
                        Dim dr As dsImageRpt.dsEMP_IDImgRow = dsSet.dsEMP_IDImg.NewdsEMP_IDImgRow
                        dr.EMP_ID = ds.Tables(0).Rows(i).Item("EMP_ID").ToString()
                        dr.EMP_NO = ds.Tables(0).Rows(i).Item("EMPNO").ToString()
                        dr.EMP_DISP_NAME = ds.Tables(0).Rows(i).Item("EMP_NAME").ToString()
                        dr.EMP_DESC = ds.Tables(0).Rows(i).Item("DES_DESCR").ToString()
                        dr.EMP_PHOTO = SImage
                        dsSet.dsEMP_IDImg.Rows.Add(dr)
                End Select
            Next

            repClassVal = New RepClass
            Dim strResourseName As String = String.Empty
            Select Case Session("printIDType")
                Case "GEMS"
                    If Session("sBsuid") <> "800555" And Session("sBsuid") <> "125018" And Session("sBsuid") <> "800444" Then
                        strResourseName = "EMP_ID_Card.rpt"
                    ElseIf Session("sBsuid") = "125018" Then
                        strResourseName = "EMP_ID_Card_WIS.rpt"
                    ElseIf Session("sBsuid") = "800444" Then
                        strResourseName = "EMP_ID_Card_AAQ.rpt"
                    Else
                        strResourseName = "EMP_ID_Card_GWE.rpt"
                    End If
                Case "STS"
                    strResourseName = "EMP_ID_Card_STS.rpt"
                Case "GEMS CARD - NEW 2014"
                    strResourseName = "Employeecard_New_2014.rpt"
                Case "Visitor"
                    strResourseName = "EMP_ID_Card_VISITOR.rpt"
                Case "Admin"
                    strResourseName = "EMP_ID_Card_ADMIN.rpt"
                Case "Watchman"
                    strResourseName = "EMP_ID_Card_WATCHMAN.rpt"
                Case "GEMS-Principal"
                    strResourseName = "EMP_ID_Card_PRINCIPAL.rpt"
                Case "GEMS-MUSIC"
                    strResourseName = "EMP_ID_Card_MUSIC.rpt"
                Case "GEMS_SCHOOL"
                    If Session("sBsuid") <> "800555" And Session("sBsuid") <> "125018" And Session("sBsuid") <> "800444" And Session("sBsuid") <> "800400" And Session("sBsuid") <> "800401" And Session("sBsuid") <> "800402" And Session("sBsuid") <> "800403" And Session("sBsuid") <> "800404" Then
                        strResourseName = "EMP_ID_Card_SCHOOL.rpt"
                    ElseIf Session("sBsuid") = "125018" Then
                        strResourseName = "EMP_ID_Card_WIS.rpt"
                    ElseIf Session("sBsuid") = "800444" Then
                        strResourseName = "EMP_ID_Card_AAQ.rpt"
                        ''egypt by nahyan
                    ElseIf Session("sBsuid") = "800400" Then
                        strResourseName = "EMP_ID_Card_SCHOOL_EGYPT_BIM.rpt"
                    ElseIf Session("sBsuid") = "800401" Then
                        strResourseName = "EMP_ID_Card_SCHOOL_EGYPT_MIL.rpt"
                    ElseIf Session("sBsuid") = "800402" Then
                        strResourseName = "EMP_ID_Card_SCHOOL_EGYPT_MLS.rpt"
                    ElseIf Session("sBsuid") = "800403" Then
                        strResourseName = "EMP_ID_Card_SCHOOL_EGYPT_TBS.rpt"
                    ElseIf Session("sBsuid") = "800404" Then
                        strResourseName = "EMP_ID_Card_SCHOOL_EGYPT.rpt"

                    Else
                        strResourseName = "EMP_ID_Card_GWE.rpt"
                    End If
                Case "SAFECOR"
                    strResourseName = "EMP_ID_Card_SAFECOR.rpt"
                Case "BBT"
                    strResourseName = "EMP_ID_Card_STS.rpt"
                Case "INFRACARE"
                    strResourseName = "Employeecard_Infracare.rpt"
            End Select
            repClassVal.ResourceName = strResourseName
            repClassVal.SetDataSource(dsSet)
            'Select Case Session("printIDType")
            '    Case "GEMS_SCHOOL"
            '        repClassVal.SetParameterValue("BSU_NAME", Session("BSU_Name").ToString)
            'End Select
            CrystalReportViewer1.ReportSource = repClassVal
        End If

        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As Button = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = "completed"
                End If
            End If
        Catch ex As Exception

        Finally
            'repClassVal.Dispose()

        End Try

    End Sub

    Public Sub IssueCard(ByVal ds As DataSet, ByVal type As String)
        If type = "EMPLOYEE" Then
            IssueCard_Employee(ds, "EMPLOYEE")
        ElseIf type = "EMPLOYEE_STS" Then
            IssueCard_Employee_STS(ds, "EMPLOYEE_STS")
        ElseIf type = "STUDENT" OrElse type = "TRANSPORT_STUDENT" Then

            Dim photopath As String = WebConfigurationManager.AppSettings("StudentPhotoPath").ToString

            Dim dsSet As New dsImageRpt

            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                ''Barcode
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                barcode.Number = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
                barcode.ChecksumAdd = True
                barcode.NarrowBarWidth = 3
                barcode.Height = 300
                barcode.FontHeight = 0.3F
                Dim b As Byte()
                ReDim b(barcode.Render(ImageType.Png).Length)
                b = barcode.Render(ImageType.Png)
                ''Student Image
                Dim stupath = photopath & "\" & ds.Tables(0).Rows(i).Item("STU_IMAGE").ToString()
                Dim SImage As Byte()

                If System.IO.File.Exists(stupath) = False Then
                    stupath = photopath & "\NOIMG\no_image.jpg"
                End If


                Dim fs As New System.IO.FileStream(stupath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                SImage = New Byte(fs.Length - 1) {}
                fs.Read(SImage, 0, Convert.ToInt32(fs.Length))
                fs.Close()

                Dim dr As dsImageRpt.DSSTUDENT_IDCARDRow = dsSet.DSSTUDENT_IDCARD.NewDSSTUDENT_IDCARDRow
                dr.STU_ID = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                dr.STU_NO = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
                dr.STU_NAME = ds.Tables(0).Rows(i).Item("STU_NAME").ToString()
                dr.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("STU_PARENT_NAME").ToString()
                dr.STU_RESIDENCE_NO = ds.Tables(0).Rows(i).Item("STU_RESIDENCE_NO").ToString()
                dr.STU_OFFICE_NO = ds.Tables(0).Rows(i).Item("STU_OFFICE_NO").ToString()
                dr.STU_MOBILE_NO = ds.Tables(0).Rows(i).Item("STU_MOBILE_NO").ToString()
                dr.STU_EMERGENCY_NO = ds.Tables(0).Rows(i).Item("STU_EMERGENCY_NO").ToString()
                dr.STU_PICKUP_POINT = ds.Tables(0).Rows(i).Item("STU_PICKUP_POINT").ToString()
                dr.STU_BUS_NO = ds.Tables(0).Rows(i).Item("STU_BUS_NO").ToString()
                dr.STU_OWN_TRANSPORT = ds.Tables(0).Rows(i).Item("STU_OWN_TRANSPORT")
                dr.STU_BSU_NAME = ds.Tables(0).Rows(i).Item("STU_BSU_NAME").ToString()
                If Not ds.Tables(0).Rows(i).Item("STU_BSU_IMAGE") Is DBNull.Value Then
                    dr.STU_BSU_IMAGE = ds.Tables(0).Rows(i).Item("STU_BSU_IMAGE")
                End If
                dr.STU_DROPUP_POINT = ds.Tables(0).Rows(i).Item("STU_DROPUP_POINT").ToString()
                dr.STU_IMAGE = SImage
                dr.STU_BARCODE = b
                dr.STU_BSU_ADDRESS = ds.Tables(0).Rows(i).Item("STU_BSU_ADDRESS").ToString()
                If type = "TRANSPORT_STUDENT" Then
                    dr.TRANS_BSU_NAME = ds.Tables(0).Rows(i).Item("TRANS_BSU_NAME").ToString()
                    dr.STU_GRD_SEC = ds.Tables(0).Rows(i).Item("STU_GRD_SEC").ToString()
                    dr.ACY_DESCR = ds.Tables(0).Rows(i).Item("ACY_DESCR").ToString()
                End If

                dsSet.DSSTUDENT_IDCARD.Rows.Add(dr)

            Next

            repClassVal = New RepClass
            Dim strResourseName As String = String.Empty
            Select Case type
                Case "STUDENT"
                    strResourseName = "Studentcard.rpt"
                Case "TRANSPORT_STUDENT"
                    strResourseName = "Studentcard_STS.rpt"
            End Select
            repClassVal.ResourceName = strResourseName
            repClassVal.SetDataSource(dsSet)
            CrystalReportViewer1.ReportSource = repClassVal

        End If

        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As Button = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = "completed"
                End If
            End If
        Catch ex As Exception

        Finally
            ' repClassVal.Dispose()

        End Try

    End Sub

    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        CrystalReportViewer1.Dispose()
        CrystalReportViewer1 = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        GC.Collect()
    End Sub

End Class
