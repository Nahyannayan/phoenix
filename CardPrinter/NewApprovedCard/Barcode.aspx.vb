Imports Lesnikowski.Barcode
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration

Partial Class Library_Barcode_Barcode
    Inherits BasePage

    Dim Encr_decrData As New Encryption64
    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If HttpContext.Current.Session("sUsr_name") & "" = "" Then
            HttpContext.Current.Response.Redirect("~/login.aspx", False)
            Exit Sub
        End If
        'Dim MainMnu_code As String
        'Dim CurUsr_id As String = Session("sUsr_id")
        'Dim CurRole_id As String = Session("sroleid")
        'Dim CurBsUnit As String = Session("sBsuid")
        'Dim USR_NAME As String = Session("sUsr_name")
        'Dim datamode As String = "none"

        'If Not Request.UrlReferrer Is Nothing Then
        '    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
        'End If
        'If Request.QueryString("datamode") <> "" Then
        '    datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        'Else
        '    datamode = ""
        'End If
        'If Request.QueryString("MainMnu_code") <> "" Then
        '    MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        'Else
        '    MainMnu_code = ""
        'End If
        ''check for the usr_name and the menucode are valid otherwise redirect to login page

        'If USR_NAME = "" Or MainMnu_code <> "F301005" Then
        '    If Not Request.UrlReferrer Is Nothing Then
        '        Response.Redirect(Request.UrlReferrer.ToString())
        '    Else
        '        Response.Redirect("~\noAccess.aspx")
        '    End If
        'End If
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
        Dim type As String = Request.QueryString("Type")
        Select Case type
            Case "TRANSPORT_STUDENT"
                PrintCard_Transport()
            Case Else
                PrintCard()
        End Select
    End Sub

    Public Sub PrintCard_Transport()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim ids = ""
        Dim SP = ""
        If Request.QueryString("Type") = "TRANSPORT_STUDENT" Then
            SP = "[TRANSPORT_IDENTITY_CARD_ISSUE_STUDENT]"
            ids = ""
        ElseIf Request.QueryString("Type") = "EMPLOYEE" Then
            SP = ""
            ids = ""
        End If
        If Session("CardhashtableAll") = "All" Then
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Bsu", Session("sbsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)
        Else
            Dim hash As New Hashtable
            If Session("Cardhashtable") Is Nothing Then
            Else
                hash = Session("Cardhashtable")
            End If
            Dim idictenum As IDictionaryEnumerator
            idictenum = hash.GetEnumerator()
            While (idictenum.MoveNext())
                ids &= idictenum.Value.ToString & ","
            End While
            If Session("printID_Transport_paper") Then
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@IDS", ids)
                pParms(1) = New SqlClient.SqlParameter("@Bsu", DBNull.Value)
                pParms(2) = New SqlClient.SqlParameter("@Promote_Grade", False)
                pParms(3) = New SqlClient.SqlParameter("@TripType", Session("printID_Tran_paper_TripType"))
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)
            Else
                Dim pParms(1) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@IDS", ids)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)
            End If
            'Dim pParms(1) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@IDS", ids)
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            IssueCard(ds, Request.QueryString("Type"))
        End If
    End Sub

    Public Sub PrintCard()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim ds As New DataSet

        Dim ids = ""
        Dim SP = ""

        If Request.QueryString("Type") = "STUDENT" Then

            SP = "IDENTITY_CARD_ISSUE_STUDENT"
            ids = ""

        ElseIf Request.QueryString("Type") = "EMPLOYEE" Then

            SP = ""
            ids = ""

        End If




        If Session("CardhashtableAll") = "All" Then

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Bsu", Session("sbsuid"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)

        Else

            Dim hash As New Hashtable

            If Session("Cardhashtable") Is Nothing Then
            Else
                hash = Session("Cardhashtable")
            End If

            Dim idictenum As IDictionaryEnumerator
            idictenum = hash.GetEnumerator()


            Dim i = 0

            While (idictenum.MoveNext())

                Dim key = idictenum.Key
                Dim id = idictenum.Value

                Dim stuid = idictenum.Value

                ids &= id & ","

            End While
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@IDS", ids)

            If Request.QueryString("ctype") = "7" Or Request.QueryString("ctype") = "8" Or Request.QueryString("ctype") = "9" Or Request.QueryString("ctype") = "10" Or Request.QueryString("ctype") = "20" Then

            Else
                pParms(1) = New SqlClient.SqlParameter("@Promote_Grade", Session("PromoteGrade"))
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, SP, pParms)


        End If



        If ds.Tables(0).Rows.Count > 0 Then

            IssueCard(ds, Request.QueryString("Type"))

        End If



    End Sub



    Public Sub IssueCard(ByVal ds As DataSet, ByVal type As String)

        If type = "STUDENT" OrElse type = "TRANSPORT_STUDENT" Then

            Dim photopath As String = WebConfigurationManager.AppSettings("StudentPhotoPath").ToString

            Dim dsSet As New dsImageRpt
            Dim BSU_bGEMSSchool As Boolean = False
            Dim STUDBSU As String



            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                ''Barcode
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                If type = "STUDENT" Then
                    barcode.Number = ds.Tables(0).Rows(i).Item("STU_NO").ToString() & ds.Tables(0).Rows(i).Item("DUP_GEN").ToString()
                Else
                    If Request.QueryString("ctype") <> "-1" Then
                        barcode.Number = ds.Tables(0).Rows(i).Item("STU_NO").ToString() & "#" & Request.QueryString("ctype")
                    Else
                        barcode.Number = ds.Tables(0).Rows(i).Item("STU_NO").ToString() & ds.Tables(0).Rows(i).Item("DUP_GEN").ToString()
                    End If

                End If
                'Color.FromArgb(120, 255, 0, 0));#D9EBCB
                STUDBSU = ds.Tables(0).Rows(i).Item("STUDBSU").ToString()

                ''for gaa parent id card 


                If ds.Tables(0).Rows(i).Item("BSU_bGEMSSchool").ToString() = "True" Then
                    BSU_bGEMSSchool = True
                    If Request.QueryString("ctype") = "2" Then
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#D9EBCB")
                    ElseIf (Request.QueryString("ctype") = "7" Or Request.QueryString("ctype") = "8" Or Request.QueryString("ctype") = "9" Or Request.QueryString("ctype") = "10") AndAlso STUDBSU = "114003" Then ''for GAA parent id card 18mar2020
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                        barcode.Rotation = RotationType.Degrees90
                    ElseIf Request.QueryString("ctype") = "5" Then
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                    ElseIf Request.QueryString("ctype") = "4" Then
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#D9EBCB")
                    ElseIf STUDBSU <> "800555" Then
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#BFEDFB")
                    ElseIf STUDBSU = "800555" Then
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                    End If
                Else
                    If Request.QueryString("ctype") = "2" Then
                        barcode.BackColor = Drawing.ColorTranslator.FromHtml("#D9EBCB")
                    End If
                End If

                'added by Mahesh on 28-Aug-2020
                If STUDBSU = "125017" Then
                    barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                End If


                barcode.ChecksumAdd = True

                ''For gaa 7/7/20
                If STUDBSU = "114003" And type = "STUDENT" Then
                    If Request.QueryString("ctype") = "8" Then
                        barcode.CustomText = ds.Tables(0).Rows(i).Item("STU_NO").ToString() & "01"
                    ElseIf Request.QueryString("ctype") = "9" Then
                        barcode.CustomText = ds.Tables(0).Rows(i).Item("STU_NO").ToString() & "02"
                    ElseIf Request.QueryString("ctype") = "10" Then
                        barcode.CustomText = ds.Tables(0).Rows(i).Item("STU_NO").ToString() & "03"
                    Else
                        barcode.CustomText = ds.Tables(0).Rows(i).Item("STU_NO").ToString()

                    End If

                Else
                    barcode.CustomText = ds.Tables(0).Rows(i).Item("STU_NO").ToString()

                End If

                barcode.NarrowBarWidth = 3
                barcode.Height = 300
                barcode.FontHeight = 0.3F
                barcode.ForeColor = Drawing.Color.Black


                Dim b As Byte()
                ReDim b(barcode.Render(ImageType.Png).Length)
                b = barcode.Render(ImageType.Png)


                ''BSU Image
                Dim BSU_badges_Image As Byte()
                Dim STU_BSU_IMAGE As String
                Dim STU_BSU_LOGO As Byte()
                'Dim BSU_badges_path As String = "\\Gcofss01\gemscommon\MARKETING & COMMUNICATIONS\Shameer\school logo n badges"
                Dim BSU_badges_path As String = Server.MapPath("~/Images/SchoolBadges")

                If STUDBSU = "125017" Or STUDBSU = "800555" Then
                    STU_BSU_IMAGE = BSU_badges_path & "\" & ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString() + "_logo.jpg"

                    Dim fsBSU_Logo As New System.IO.FileStream(STU_BSU_IMAGE, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    STU_BSU_LOGO = New Byte(fsBSU_Logo.Length - 1) {}
                    fsBSU_Logo.Read(STU_BSU_LOGO, 0, Convert.ToInt32(fsBSU_Logo.Length))
                    fsBSU_Logo.Close()
                End If

                ''BSU_badges_path = BSU_badges_path & "\DAA.png"
                If Request.QueryString("ctype") = "2" Then
                    BSU_badges_path = BSU_badges_path & "\" & ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString() + "_green.jpg"
                ElseIf Request.QueryString("ctype") = "5" Then
                    BSU_badges_path = BSU_badges_path & "\" & ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString() + ".jpg"
                ElseIf Request.QueryString("ctype") = "4" Then
                    BSU_badges_path = BSU_badges_path & "\" & ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString() + "_green.jpg"
                Else

                    BSU_badges_path = BSU_badges_path & "\" & ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString() + ".jpg"


                End If


                If System.IO.File.Exists(BSU_badges_path) = False Then
                    BSU_badges_path = photopath & "\NOIMG\no_image.jpg"
                End If

                Dim fsBSU As New System.IO.FileStream(BSU_badges_path, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                BSU_badges_Image = New Byte(fsBSU.Length - 1) {}
                fsBSU.Read(BSU_badges_Image, 0, Convert.ToInt32(fsBSU.Length))
                fsBSU.Close()




                ''Transport Bus Image

                Dim TransportImage As Byte()
                'Dim TransportImagePath As String = "\\Gcofss01\gemscommon\MARKETING & COMMUNICATIONS\Shameer\school logo n badges"
                Dim TransportImagePath As String = Server.MapPath("~/Images/SchoolBadges")

                If ds.Tables(0).Rows(i).Item("STU_OWN_TRANSPORT").ToString() <> "False" Then
                    If ds.Tables(0).Rows(i).Item("TRANS_BSU_NAME").ToString() = "SCHOOL TRANSPORT SERVICES" Then
                        'TransportImagePath = TransportImagePath & "\STS.jpg"
                        If Request.QueryString("ctype") = "2" Then
                            If Left(STUDBSU, 2) = "22" Then
                                TransportImagePath = TransportImagePath & "\STS__India_green.jpg"
                            Else
                                TransportImagePath = TransportImagePath & "\STS_green.jpg"
                            End If
                        Else
                            If Left(STUDBSU, 2) = "22" Then
                                TransportImagePath = TransportImagePath & "\STS_India.jpg"
                            Else
                                TransportImagePath = TransportImagePath & "\STS.jpg"
                            End If
                        End If

                    ElseIf ds.Tables(0).Rows(i).Item("TRANS_BSU_NAME").ToString() = "BRIGHT BUS TRANSPORT" Then
                        'TransportImagePath = TransportImagePath & "\BBT.png"

                        If Request.QueryString("ctype") = "2" Then
                            TransportImagePath = TransportImagePath & "\BBT_green.jpg"
                        Else
                            TransportImagePath = TransportImagePath & "\BBT.png"
                        End If
                    End If
                Else
                    If Request.QueryString("ctype") = "2" Then
                        TransportImagePath = TransportImagePath & "\OWEN_green.jpg"
                    Else
                        TransportImagePath = TransportImagePath & "\OWEN.jpg"
                    End If

                    'TransportImagePath = TransportImagePath & "\OWEN.jpg"
                End If

                If System.IO.File.Exists(TransportImagePath) = True Then
                    Dim fsTransportImage As New System.IO.FileStream(TransportImagePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    TransportImage = New Byte(fsTransportImage.Length - 1) {}
                    fsTransportImage.Read(TransportImage, 0, Convert.ToInt32(fsTransportImage.Length))
                    fsTransportImage.Close()

                End If



                ''Student Image
                Dim stupath = photopath & "\" & ds.Tables(0).Rows(i).Item("STU_IMAGE").ToString()
                Dim SImage As Byte()

                If System.IO.File.Exists(stupath) = False Then
                    stupath = photopath & "\NOIMG\no_image.jpg"
                End If


                Dim fs As New System.IO.FileStream(stupath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                SImage = New Byte(fs.Length - 1) {}
                fs.Read(SImage, 0, Convert.ToInt32(fs.Length))
                fs.Close()

                Dim catimage = Server.MapPath(ds.Tables(0).Rows(i).Item("IMAGE_PATH").ToString())
                Dim SImage2 As Byte()

                Dim showcaimg = 0
                If System.IO.File.Exists(catimage) = True Then

                    Dim fs2 As New System.IO.FileStream(catimage, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    SImage2 = New Byte(fs2.Length - 1) {}
                    fs2.Read(SImage2, 0, Convert.ToInt32(fs2.Length))
                    fs2.Close()
                    showcaimg = 1
                End If


                Dim dr As dsImageRpt.DSSTUDENT_IDCARDRow = dsSet.DSSTUDENT_IDCARD.NewDSSTUDENT_IDCARDRow
                dr.STU_ID = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                dr.STU_NO = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
                dr.STU_NAME = ds.Tables(0).Rows(i).Item("STU_NAME").ToString()
                dr.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("STU_PARENT_NAME").ToString()
                dr.STU_RESIDENCE_NO = ds.Tables(0).Rows(i).Item("STU_RESIDENCE_NO").ToString()
                dr.STU_OFFICE_NO = ds.Tables(0).Rows(i).Item("STU_OFFICE_NO").ToString()
                dr.STU_MOBILE_NO = ds.Tables(0).Rows(i).Item("STU_MOBILE_NO").ToString()
                dr.STU_EMERGENCY_NO = ds.Tables(0).Rows(i).Item("STU_EMERGENCY_NO").ToString()
                dr.STU_PICKUP_POINT = ds.Tables(0).Rows(i).Item("STU_PICKUP_POINT").ToString()
                dr.STU_BUS_NO = ds.Tables(0).Rows(i).Item("STU_BUS_NO").ToString()
                dr.STU_OWN_TRANSPORT = ds.Tables(0).Rows(i).Item("STU_OWN_TRANSPORT")
                dr.STU_BSU_NAME = ds.Tables(0).Rows(i).Item("STU_BSU_NAME").ToString()
                If ds.Tables(0).Rows(i).Item("BSU_bGEMSSchool").ToString() = "False" Then
                    dr.STU_BSU_IMAGE = ds.Tables(0).Rows(i).Item("STU_BSU_IMAGE")
                Else
                    dr.STU_BSU_IMAGE = BSU_badges_Image
                End If
                'If Not ds.Tables(0).Rows(i).Item("STU_BSU_IMAGE") Is DBNull.Value Then
                '    dr.STU_BSU_IMAGE = BSU_badges_Image 'ds.Tables(0).Rows(i).Item("STU_BSU_IMAGE")
                'End If
                dr.STU_DROPUP_POINT = ds.Tables(0).Rows(i).Item("STU_DROPUP_POINT").ToString()
                dr.STU_IMAGE = SImage
                dr.STU_BARCODE = b
                dr.STU_BSU_ADDRESS = ds.Tables(0).Rows(i).Item("STU_BSU_ADDRESS").ToString()
                dr.HEADER_COLOR = ds.Tables(0).Rows(i).Item("CT_COLOR_CODE").ToString()
                dr.CAT_IMAGE = SImage2
                dr.SHOW_CAT_IMAGE = showcaimg

                dr.STU_TRANS_IMAGE = TransportImage

                dr.STU_BSU_LOGO = STU_BSU_LOGO
                If Request.QueryString("ayshow") = "true" Then
                    dr.ACY_DESCR = ds.Tables(0).Rows(i).Item("ACY_DESCR").ToString()
                Else
                    dr.ACY_DESCR = ""
                End If

                If type = "TRANSPORT_STUDENT" Then
                    dr.TRANS_BSU_NAME = ds.Tables(0).Rows(i).Item("TRANS_BSU_NAME").ToString()
                    dr.ACY_DESCR = ds.Tables(0).Rows(i).Item("ACY_DESCR").ToString()
                End If

                dr.STU_GRD_SEC = ds.Tables(0).Rows(i).Item("STU_GRD_SEC").ToString()
                dr.FATHER_NAME = ds.Tables(0).Rows(i).Item("FATHER_NAME").ToString()
                dr.MOTHER_NAME = ds.Tables(0).Rows(i).Item("MOTHER_NAME").ToString()
                dr.PARENT_ADDRESS = ds.Tables(0).Rows(i).Item("PARENT_ADDRESS").ToString()
                dr.PARENT_MOBILE = ds.Tables(0).Rows(i).Item("PARENT_MOBILE").ToString()
                dr.PARENT_SIBLINGS = ds.Tables(0).Rows(i).Item("PARENT_SIBLINGS").ToString()
                dsSet.DSSTUDENT_IDCARD.Rows.Add(dr)

                ''nahyan on 8th dec 2014 to display parent card
                If Request.QueryString("ctype") = "7" Or Request.QueryString("ctype") = "8" Or Request.QueryString("ctype") = "9" Or Request.QueryString("ctype") = "10" Or Request.QueryString("ctype") = "20" Then
                    ''added by nahyan on 8th Dec2014 for parent id card
                    Dim drCard As dsImageRpt.DSPARENT_IDCARDRow = dsSet.DSPARENT_IDCARD.NewDSPARENT_IDCARDRow
                    '  drCard.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("STU_PARENT_NAME").ToString()
                    drCard.STU_NO = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
                    drCard.STU_BSU_NAME = ds.Tables(0).Rows(i).Item("STU_BSU_NAME").ToString()
                    drCard.BSU_TEL = ds.Tables(0).Rows(i).Item("BSU_TEL").ToString()
                    '' If ds.Tables(0).Rows(i).Item("BSU_bGEMSSchool").ToString() = "False" Then
                    drCard.STU_BSU_IMAGE = ds.Tables(0).Rows(i).Item("STU_BSU_IMAGE")
                    ''Else
                    ''  drCard.STU_BSU_IMAGE = BSU_badges_Image
                    '' End If

                    ''added by nahyan on 2Jan2017 for GNA
                    drCard.STU_DOJ = ds.Tables(0).Rows(i).Item("STU_DOJ")
                    drCard.STU_BARCODE = b
                    Dim parentPath As String = String.Empty
                    Dim FatherparentPath As String = String.Empty
                    Dim MotherparentPath As String = String.Empty

                    If Request.QueryString("ctype") = "7" Then
                        drCard.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("STU_PARENT_NAME").ToString()
                        parentPath = photopath & "\" & ds.Tables(0).Rows(i).Item("PARENTPHOTO").ToString()
                        drCard.PARENTTYPE = "PARENT"
                    ElseIf Request.QueryString("ctype") = "8" Then
                        drCard.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("FATHER_NAME").ToString()
                        parentPath = photopath & "\" & ds.Tables(0).Rows(i).Item("FATHERPHOTO").ToString()
                        drCard.PARENTTYPE = "FATHER"
                    ElseIf Request.QueryString("ctype") = "9" Then
                        drCard.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("MOTHER_NAME").ToString()
                        parentPath = photopath & "\" & ds.Tables(0).Rows(i).Item("MOTHERPHOTO").ToString()
                        drCard.PARENTTYPE = "MOTHER"
                    ElseIf Request.QueryString("ctype") = "10" Then
                        drCard.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("GUARDIAN_NAME").ToString()
                        parentPath = photopath & "\" & ds.Tables(0).Rows(i).Item("GUARDIANPHOTO").ToString()
                        drCard.PARENTTYPE = "GUARDIAN"
                    ElseIf Request.QueryString("ctype") = "20" Then

                        drCard.FATHER_NAME = ds.Tables(0).Rows(i).Item("FATHER_NAME").ToString()
                        FatherparentPath = photopath & "\" & ds.Tables(0).Rows(i).Item("FATHERPHOTO").ToString()
                        drCard.MOTHER_NAME = ds.Tables(0).Rows(i).Item("MOTHER_NAME").ToString()
                        MotherparentPath = photopath & "\" & ds.Tables(0).Rows(i).Item("MOTHERPHOTO").ToString()

                        drCard.PARENT_SIBLINGS = ds.Tables(0).Rows(i).Item("PARENT_SIBLINGS").ToString()



                        drCard.PARENTTYPE = "FATHER-MOTHER"





                    End If
                    ' parentPath = photopath & "\" & ds.Tables(0).Rows(i).Item("PARENTPHOTO").ToString()
                    Dim PImage As Byte()
                    Dim FImage As Byte()
                    Dim MImage As Byte()

                    If System.IO.File.Exists(parentPath) = False Then
                        parentPath = photopath & "\NOIMG\no_image.jpg"
                    End If

                    If Request.QueryString("ctype") = "20" Then

                        If System.IO.File.Exists(FatherparentPath) = False Then
                            FatherparentPath = photopath & "\NOIMG\no_image.jpg"
                        End If

                        If System.IO.File.Exists(MotherparentPath) = False Then
                            MotherparentPath = photopath & "\NOIMG\no_image.jpg"
                        End If


                    End If



                    Dim fstm As New System.IO.FileStream(parentPath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    PImage = New Byte(fstm.Length - 1) {}
                    fstm.Read(PImage, 0, Convert.ToInt32(fstm.Length))
                    fstm.Close()
                    drCard.PARENT_IMAGE = PImage



                    If Request.QueryString("ctype") = "20" Then


                        Dim fastm1 As New System.IO.FileStream(FatherparentPath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        FImage = New Byte(fastm1.Length - 1) {}
                        fastm1.Read(FImage, 0, Convert.ToInt32(fastm1.Length))
                        fastm1.Close()
                        drCard.FATHER_IMAGE = FImage


                        Dim Mostm2 As New System.IO.FileStream(MotherparentPath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        MImage = New Byte(Mostm2.Length - 1) {}
                        Mostm2.Read(MImage, 0, Convert.ToInt32(Mostm2.Length))
                        Mostm2.Close()
                        drCard.MOTHER_IMAGE = MImage


                    End If




                    Dim whiteImg As String = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                    Dim WhiteImgBinary As Byte()
                    If System.IO.File.Exists(whiteImg) = False Then
                        whiteImg = photopath & "\NOIMG\no_image.jpg"
                    End If
                    Dim fstmWhite As New System.IO.FileStream(whiteImg, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    WhiteImgBinary = New Byte(fstmWhite.Length - 1) {}
                    fstmWhite.Read(WhiteImgBinary, 0, Convert.ToInt32(fstmWhite.Length))
                    fstmWhite.Close()

                    If Convert.ToString(ds.Tables(0).Rows(i).Item("SIBLINGSPHOTO")) <> "" Then

                        Dim resultData As String = Convert.ToString(ds.Tables(0).Rows(i).Item("SIBLINGSPHOTO"))
                        Dim values As String() = resultData.Split(","c)
                        For ig As Integer = 0 To values.Length - 1
                            ''to check if all siblings are available.else display white image
                            If values.Length = 1 Then
                                drCard.SIBLING_IMAGE_2 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_3 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_4 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_5 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_6 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_7 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_8 = WhiteImgBinary
                            ElseIf values.Length = 2 Then
                                drCard.SIBLING_IMAGE_3 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_4 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_5 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_6 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_7 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_8 = WhiteImgBinary
                            ElseIf values.Length = 3 Then

                                drCard.SIBLING_IMAGE_4 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_5 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_6 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_7 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_8 = WhiteImgBinary
                            ElseIf values.Length = 4 Then

                                drCard.SIBLING_IMAGE_5 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_6 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_7 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_8 = WhiteImgBinary
                            ElseIf values.Length = 5 Then

                                drCard.SIBLING_IMAGE_6 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_7 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_8 = WhiteImgBinary
                            ElseIf values.Length = 6 Then

                                drCard.SIBLING_IMAGE_7 = WhiteImgBinary
                                drCard.SIBLING_IMAGE_8 = WhiteImgBinary
                            ElseIf values.Length = 7 Then

                                drCard.SIBLING_IMAGE_8 = WhiteImgBinary
                            ElseIf values.Length = 8 Then
                            End If

                            If ig = 0 Then
                                Dim sibPath = photopath & "\" & values(0).Trim()
                                Dim Sibmage As Byte()

                                If System.IO.File.Exists(sibPath) = False Then
                                    sibPath = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm1 As New System.IO.FileStream(sibPath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage = New Byte(fstm1.Length - 1) {}
                                fstm1.Read(Sibmage, 0, Convert.ToInt32(fstm1.Length))
                                fstm1.Close()
                                drCard.SIBLING_IMAGE_1 = Sibmage

                            End If

                            If ig = 1 Then

                                Dim sibPath2 = photopath & "\" & values(1).Trim()
                                Dim Sibmage2 As Byte()

                                If System.IO.File.Exists(sibPath2) = False Then
                                    sibPath2 = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm2 As New System.IO.FileStream(sibPath2, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage2 = New Byte(fstm2.Length - 1) {}
                                fstm2.Read(Sibmage2, 0, Convert.ToInt32(fstm2.Length))
                                fstm2.Close()
                                drCard.SIBLING_IMAGE_2 = Sibmage2
                            End If

                            If ig = 2 Then

                                Dim sibPath3 = photopath & "\" & values(2).Trim()
                                Dim Sibmage3 As Byte()

                                If System.IO.File.Exists(sibPath3) = False Then
                                    sibPath3 = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm3 As New System.IO.FileStream(sibPath3, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage3 = New Byte(fstm3.Length - 1) {}
                                fstm3.Read(Sibmage3, 0, Convert.ToInt32(fstm3.Length))
                                fstm3.Close()
                                drCard.SIBLING_IMAGE_3 = Sibmage3
                            End If

                            If ig = 3 Then

                                Dim sibPath4 = photopath & "\" & values(3).Trim()
                                Dim Sibmage4 As Byte()

                                If System.IO.File.Exists(sibPath4) = False Then
                                    sibPath4 = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm4 As New System.IO.FileStream(sibPath4, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage4 = New Byte(fstm4.Length - 1) {}
                                fstm4.Read(Sibmage4, 0, Convert.ToInt32(fstm4.Length))
                                fstm4.Close()
                                drCard.SIBLING_IMAGE_4 = Sibmage4
                            End If

                            If ig = 4 Then

                                Dim sibPath5 = photopath & "\" & values(4).Trim()
                                Dim Sibmage5 As Byte()

                                If System.IO.File.Exists(sibPath5) = False Then
                                    sibPath5 = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm5 As New System.IO.FileStream(sibPath5, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage5 = New Byte(fstm5.Length - 1) {}
                                fstm5.Read(Sibmage5, 0, Convert.ToInt32(fstm5.Length))
                                fstm5.Close()
                                drCard.SIBLING_IMAGE_5 = Sibmage5
                            End If

                            If ig = 5 Then

                                Dim sibPath6 = photopath & "\" & values(5).Trim()
                                Dim Sibmage6 As Byte()

                                If System.IO.File.Exists(sibPath6) = False Then
                                    sibPath6 = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm6 As New System.IO.FileStream(sibPath6, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage6 = New Byte(fstm6.Length - 1) {}
                                fstm6.Read(Sibmage6, 0, Convert.ToInt32(fstm6.Length))
                                fstm6.Close()
                                drCard.SIBLING_IMAGE_6 = Sibmage6
                            End If

                            If ig = 6 Then

                                Dim sibPath7 = photopath & "\" & values(6).Trim()
                                Dim Sibmage7 As Byte()

                                If System.IO.File.Exists(sibPath7) = False Then
                                    sibPath7 = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm7 As New System.IO.FileStream(sibPath7, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage7 = New Byte(fstm7.Length - 1) {}
                                fstm7.Read(Sibmage7, 0, Convert.ToInt32(fstm7.Length))
                                fstm7.Close()
                                drCard.SIBLING_IMAGE_7 = Sibmage7
                            End If

                            If ig = 7 Then

                                Dim sibPath8 = photopath & "\" & values(7).Trim()
                                Dim Sibmage8 As Byte()

                                If System.IO.File.Exists(sibPath8) = False Then
                                    sibPath8 = photopath & "\NOIMG\no_image.jpg"
                                End If
                                Dim fstm8 As New System.IO.FileStream(sibPath8, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                                Sibmage8 = New Byte(fstm8.Length - 1) {}
                                fstm8.Read(Sibmage8, 0, Convert.ToInt32(fstm8.Length))
                                fstm8.Close()
                                drCard.SIBLING_IMAGE_8 = Sibmage8
                            End If

                        Next
                    End If

                    If Convert.ToString(ds.Tables(0).Rows(i).Item("SIBLING_STUDENTNAME")) <> "" Then

                        Dim resultData As String = Convert.ToString(ds.Tables(0).Rows(i).Item("SIBLING_STUDENTNAME"))
                        Dim values21 As String() = resultData.Split(","c)
                        For ig1 As Integer = 0 To values21.Length - 1
                            If ig1 = 0 Then
                                drCard.SIBLING_NAME1 = values21(0).Trim()
                            End If
                            If ig1 = 1 Then
                                drCard.SIBLING_NAME2 = values21(1).Trim()
                            End If
                            If ig1 = 2 Then
                                drCard.SIBLING_NAME3 = values21(2).Trim()
                            End If
                            If ig1 = 3 Then
                                drCard.SIBLING_NAME4 = values21(3).Trim()
                            End If
                            If ig1 = 4 Then
                                drCard.SIBLING_NAME5 = values21(4).Trim()
                            End If
                            If ig1 = 5 Then
                                drCard.SIBLING_NAME6 = values21(5).Trim()
                            End If
                            If ig1 = 6 Then
                                drCard.SIBLING_NAME7 = values21(6).Trim()
                            End If
                            If ig1 = 7 Then
                                drCard.SIBLING_NAME8 = values21(7).Trim()

                            End If
                        Next

                    End If

                    If Convert.ToString(ds.Tables(0).Rows(i).Item("PARENT_SIBLINGS")) <> "" Then

                        Dim resultData1 As String = Convert.ToString(ds.Tables(0).Rows(i).Item("PARENT_SIBLINGS"))
                        Dim SibNos As String() = resultData1.Split(" ")
                        For ig1 As Integer = 0 To SibNos.Length - 1
                            If ig1 = 0 Then
                                drCard.SIBLING_NO1 = SibNos(0).Trim()
                            End If
                            If ig1 = 1 Then
                                drCard.SIBLING_NO2 = SibNos(1).Trim()
                            End If
                            If ig1 = 2 Then
                                drCard.SIBLING_NO3 = SibNos(2).Trim()
                            End If
                            If ig1 = 3 Then
                                drCard.SIBLING_NO4 = SibNos(3).Trim()
                            End If
                            If ig1 = 4 Then
                                drCard.SIBLING_NO5 = SibNos(4).Trim()
                            End If
                            If ig1 = 5 Then
                                drCard.SIBLING_NO6 = SibNos(5).Trim()
                            End If
                            If ig1 = 6 Then
                                drCard.SIBLING_NO7 = SibNos(6).Trim()
                            End If
                            If ig1 = 7 Then
                                drCard.SIBLING_NO8 = SibNos(7).Trim()

                            End If
                        Next


                    End If

                    If Request.QueryString("ctype") = "20" Then
                        Dim resultSibNos As String = Convert.ToString(ds.Tables(0).Rows(i).Item("PARENT_SIBLINGS"))
                        Dim SibNos As String() = resultSibNos.Split(" ")
                        For ig1 As Integer = 0 To SibNos.Length - 1
                            If ig1 = 0 Then
                                drCard.SIBLING_NO1 = Right(SibNos(0).Trim(), 5)
                            End If
                            If ig1 = 1 Then
                                drCard.SIBLING_NO2 = Right(SibNos(1).Trim(), 5)
                            End If
                            If ig1 = 2 Then
                                drCard.SIBLING_NO3 = Right(SibNos(2).Trim(), 5)
                            End If
                            If ig1 = 3 Then
                                drCard.SIBLING_NO4 = Right(SibNos(3).Trim(), 5)
                            End If
                            If ig1 = 4 Then
                                drCard.SIBLING_NO5 = Right(SibNos(4).Trim(), 5)
                            End If
                            If ig1 = 5 Then
                                drCard.SIBLING_NO6 = Right(SibNos(5).Trim(), 5)
                            End If
                            If ig1 = 6 Then
                                drCard.SIBLING_NO7 = Right(SibNos(6).Trim(), 5)
                            End If
                            If ig1 = 7 Then
                                drCard.SIBLING_NO8 = Right(SibNos(7).Trim(), 5)

                            End If
                        Next
                    End If


                    dsSet.DSPARENT_IDCARD.Rows.Add(drCard)
                End If



            Next

            repClassVal = New RepClass()
            Dim strResourseName As String = String.Empty
            Select Case type
                Case "STUDENT"
                    If BSU_bGEMSSchool = True Then
                        If STUDBSU = "125017" Then
                            strResourseName = "GWA_StudentcardNew.rpt"
                        ElseIf STUDBSU = "800555" Then
                            strResourseName = "GWE_StudentcardNew.rpt"
                        ElseIf STUDBSU = "900201" Then
                            strResourseName = "GWS_StudentcardNew.rpt"
                            ''added by nahyan to include new id card for EGYPT
                        ElseIf STUDBSU = "800400" Or STUDBSU = "800401" Or STUDBSU = "800402" Or STUDBSU = "800403" Then
                            If Request.QueryString("ctype") = "7" Or Request.QueryString("ctype") = "8" Or Request.QueryString("ctype") = "9" Or Request.QueryString("ctype") = "10" Then
                                strResourseName = "PARENTIDCARD_EGP.rpt"
                            Else
                                strResourseName = "Studentcard_EGP.rpt"
                            End If
                        ElseIf STUDBSU = "225007" Then
                            strResourseName = "ISP_StudentcardNew.rpt"
                        ElseIf Request.QueryString("ctype") = "7" Or Request.QueryString("ctype") = "8" Or Request.QueryString("ctype") = "9" Or Request.QueryString("ctype") = "10" Then
                            ''added by nahyan to include new id card for WPS
                            If STUDBSU = "125004" Then
                                strResourseName = "PARENTIDCARD_WPS.rpt"
                                ''added by nahyan to include new id card for EGYPT
                                'ElseIf STUDBSU = "800400" Or STUDBSU = "800401" Or STUDBSU = "800402" Or STUDBSU = "800403" Then
                                '    strResourseName = "PARENTIDCARD_EGP.rpt"
                            ElseIf STUDBSU = "124002" Then
                                strResourseName = "PARENTIDCARD_GNA.rpt"
                            ElseIf STUDBSU = "114003" Then
                                strResourseName = "PARENTIDCARD_GAA.rpt"
                            Else
                                strResourseName = "PARENTIDCARD_WIS.rpt"
                            End If
                            'added by Mmahesh FOR GFS
                        ElseIf Request.QueryString("ctype") = "20" Then
                            If STUDBSU = "114003" Then
                                strResourseName = "PARENTIDCARD_GAA.rpt"
                            Else

                                strResourseName = "PARENTIDCARD_GFS.rpt"
                            End If

                        Else
                            strResourseName = "StudentcardNew.rpt"
                        End If
                    Else
                        If Request.QueryString("Checked") = "true" Then
                            strResourseName = "Studentcard_pickup.rpt"
                        Else
                            strResourseName = "Studentcard.rpt"
                        End If
                    End If

                Case "TRANSPORT_STUDENT"
                    'strResourseName = "Studentcard_STS.rpt"
                    Dim bPrintOnPaper As Boolean = IIf(Session("printID_Transport_paper") Is Nothing, False, Session("printID_Transport_paper"))
                    If bPrintOnPaper Then
                        If STUDBSU <> "225007" Then
                            strResourseName = "Studentcard_STS_onePage.rpt"
                        Else
                            strResourseName = "Studentcard_ISP_onePage.rpt"
                        End If
                    Else
                        If Session("Suppress_Photo") = "False" Then
                            If Request.QueryString("ctype") = "1" Then
                                strResourseName = "Studentcard_STS_DUP.rpt"
                            ElseIf Request.QueryString("ctype") = "6" And BSU_bGEMSSchool = True And STUDBSU = "125018" Then
                                strResourseName = "Studentcard_WIS.rpt"
                            ElseIf Request.QueryString("ctype") = "6" And BSU_bGEMSSchool = True And STUDBSU = "165010" Then
                                strResourseName = "StudentCard_WSR.rpt"
                            ElseIf Request.QueryString("ctype") = "2" And BSU_bGEMSSchool = True Then
                                strResourseName = "Studentcard_STS Green.rpt"
                            ElseIf Request.QueryString("ctype") = "4" And BSU_bGEMSSchool = True Then
                                strResourseName = "Studentcard_STS Green_OT.rpt"
                            ElseIf Request.QueryString("ctype") = "5" And BSU_bGEMSSchool = True Then
                                strResourseName = "Studentcard_STS_SHUTTLE.rpt"
                                ''by nahyan for gold card on 2may2018
                            ElseIf Request.QueryString("ctype") = "22" And BSU_bGEMSSchool = True Then
                                strResourseName = "Studentcard_STS_GOLD.rpt"
                            ElseIf BSU_bGEMSSchool = False Then
                                If Request.QueryString("ctype") = "2" Then
                                    strResourseName = "Studentcard_STS_GREEN_EXTERNAL.rpt"
                                Else
                                    ''commented and added by nahyan on 7oct2019
                                    'If STUDBSU <> "500905" Then
                                    '    strResourseName = "Studentcard_STS_EXTERNAL.rpt"
                                    'Else
                                    '    strResourseName = "Studentcard_STS_HSBC.rpt"
                                    'End If

                                    If STUDBSU = "500905" Then
                                        strResourseName = "Studentcard_STS_HSBC.rpt"
                                    ElseIf STUDBSU = "500820" Or STUDBSU = "500821" Then
                                        strResourseName = "Studentcard_STS_EXT_GREEN.rpt"
                                    ElseIf STUDBSU = "500953" Then
                                        strResourseName = "STUDENTCARD_AMITY_DXB.rpt"
                                    Else
                                        strResourseName = "Studentcard_STS_EXTERNAL.rpt"
                                    End If
                                End If
                            Else
                                If STUDBSU = "125017" Then
                                    strResourseName = "GWA_Studentcard_STS.rpt"
                                ElseIf STUDBSU = "800555" Then
                                    strResourseName = "GWE_StudentcardNew.rpt"
                                ElseIf STUDBSU = "225007" Then
                                    strResourseName = "ISP_StudentcardNew.rpt"
                                Else
                                    strResourseName = "Studentcard_STS.rpt"
                                End If
                            End If
                        Else
                            strResourseName = "Studentcard_STS_NoPhoto.rpt"
                        End If
                    End If
            End Select
            repClassVal.ResourceName = strResourseName
            repClassVal.SetDataSource(dsSet)
            CrystalReportViewer1.ReportSource = repClassVal

        End If

        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As Button = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = "completed"
                End If
            End If
        Catch ex As Exception

        Finally
            ' repClassVal.Dispose()

        End Try

    End Sub




    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        CrystalReportViewer1.Dispose()
        CrystalReportViewer1 = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        'GC.Collect()
    End Sub

End Class
