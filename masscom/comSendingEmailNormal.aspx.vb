Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Imports System.Data.Oledb
Partial Class masscom_comSendingEmailNormal
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Request.QueryString("method") = "0" Then ''Excel

                Dim Encr_decrData As New Encryption64
                HiddenPath.Value = Request.QueryString("Path")
                HiddenCSE_ID.Value = 0 '' Since we are sending via excel . no groups selected
                HiddenEML_ID.Value = Request.QueryString("templateid").Replace(" ", "+")
                HiddenBsuid.Value = Session("sbsuid")
                HiddenEmpID.Value = Session("EmployeeId")
                'GetTotalrowCountExcel()

            ElseIf Request.QueryString("method") = "1" Then

                Dim Encr_decrData As New Encryption64
                HiddenPath.Value = ""
                HiddenCSE_ID.Value = Request.QueryString("cse_id")
                HiddenEML_ID.Value = ""
                HiddenGroupid.Value = ""
                HiddenBsuid.Value = Session("sbsuid")
                HiddenEmpID.Value = Session("EmployeeId")
                'GetTotalrowCountGroups()


            End If

            'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim sql_query As String = ""

            'str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'sql_query = "update COM_EXCEL_EMAIL_SENDING_LOG set THREAD='False' where SENDING_ID='" & HiddenSendingId.Value & "'"
            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)

            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

        End If

    End Sub

    Public Sub GetTotalrowCountExcel()

        Dim myDataset As New DataSet()

        Try
            Dim HiddenPath As String = Request.QueryString("Path")

            Dim strConn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim Sql_query = "Select count(RECORD_ID) from COM_EXCEL_EMAIL_DATA_LOG WHERE LOG_ID='" & HiddenPath & "' " ' and NO_ERROR_FLAG='True'

            Div4.InnerHtml = SqlHelper.ExecuteScalar(strConn, CommandType.Text, Sql_query)

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TOTAL_MESSAGE", Div4.InnerHtml)
            pParms(1) = New SqlClient.SqlParameter("@DATA_LOG_ID", HiddenPath)
            pParms(2) = New SqlClient.SqlParameter("@EML_ID", HiddenEML_ID.Value)
            pParms(3) = New SqlClient.SqlParameter("@CSE_ID", HiddenCSE_ID.Value)
            pParms(4) = New SqlClient.SqlParameter("@SENDING_USER_EMP_ID", HiddenEmpID.Value)
            pParms(5) = New SqlClient.SqlParameter("@BSU_ID", HiddenBsuid.Value)

            HiddenSendingId.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COM_GET_EMAIL_SENDING_ID", pParms)

        Catch ex As Exception

        End Try



    End Sub

    Public Function SelectContact(ByVal GroupId As String, ByVal Contact As String) As String
        Dim Returnvalue As String = ""

        If Contact <> "" Then

            Dim val As String() = Contact.Split(",")

            Dim i = 0

            If val.Length > 0 Then
                Returnvalue = ""
            End If

            For i = 0 To val.Length - 1
                If Returnvalue = "" Then
                    Returnvalue = "'" & val(i) & "'"
                Else
                    Returnvalue &= ",'" & val(i) & "'"
                End If

            Next

        Else
            ''Select using Groups

        End If

        If Returnvalue <> "" Then

            Returnvalue = " AND CONTACT IN (" & Returnvalue & ")"

        End If


        Return Returnvalue
    End Function

    Public Sub GetTotalrowCountGroups()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = ""
        Dim ds As DataSet

        sql_query = "select CSE_CGR_ID,CSE_EML_ID from COM_SEND_EMAIL where  CSE_ID='" & HiddenCSE_ID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        Dim groupid As String = ds.Tables(0).Rows(0).Item("CSE_CGR_ID").ToString()

        HiddenGroupid.Value = groupid

        Dim Templateid = ds.Tables(0).Rows(0).Item("CSE_EML_ID").ToString()

        HiddenEML_ID.Value = Templateid

        sql_query = "select CGR_CONTACT,CGR_GRP_TYPE,CGR_CONDITION,CGR_TYPE,CGR_REMOVE_EMAIL_IDS from COM_GROUPS_M where CGR_ID='" & groupid & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        Dim condition As String = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
        Dim grptype As String = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()
        Dim Contact = ds.Tables(0).Rows(0).Item("CGR_CONTACT").ToString()

        'If grptype = "GRP" Then  ''Group 

        '    condition = " select count( RowID) as Countvalue  from ( " & condition & " ) a where STU_bRCVMAIL='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString()

        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, condition)
        '    Div4.InnerHtml = ds.Tables(0).Rows(0).Item("Countvalue").ToString()

        'End If

        'If grptype = "AON" Then ''Add On 

        '    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  where CGAO_CGR_ID='" & groupid & "' and STU_bRCVMAIL='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString() & SelectContact("", Contact)

        '    sql_query = " select count(RowID) as Countvalue  from ( " & condition & " ) a "

        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        '    Div4.InnerHtml = ds.Tables(0).Rows(0).Item("Countvalue").ToString()

        'End If

        Dim pParmss(3) As SqlClient.SqlParameter
        pParmss(0) = New SqlClient.SqlParameter("@CGR_ID", groupid)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "MESSAGING_GROUPS_COUNT", pParmss)
        Div4.InnerHtml = ds.Tables(0).Rows(0).Item("COUNTS").ToString()
        str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TOTAL_MESSAGE", Div4.InnerHtml)
        pParms(1) = New SqlClient.SqlParameter("@EML_ID", HiddenEML_ID.Value)
        pParms(2) = New SqlClient.SqlParameter("@CSE_ID", HiddenCSE_ID.Value)
        pParms(3) = New SqlClient.SqlParameter("@SENDING_USER_EMP_ID", HiddenEmpID.Value)
        pParms(4) = New SqlClient.SqlParameter("@BSU_ID", HiddenBsuid.Value)


        HiddenSendingId.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COM_GET_EMAIL_SENDING_ID", pParms)


    End Sub


    Protected Sub BtnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSend.Click
        Image1.Visible = True
        If Request.QueryString("method") = "0" Then ''Excel
            GetTotalrowCountExcel()
        ElseIf Request.QueryString("method") = "1" Then
            GetTotalrowCountGroups()
        End If

        BtnSend.Visible = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = ""
        str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        sql_query = "update COM_EXCEL_EMAIL_SENDING_LOG set THREAD='False' where SENDING_ID='" & HiddenSendingId.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)

    End Sub

End Class
