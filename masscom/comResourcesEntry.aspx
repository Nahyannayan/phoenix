﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="comResourcesEntry.aspx.vb" Inherits="masscom_comResourcesEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <link href="../FolderGallery/css/main-teacher.css" rel="stylesheet" />
      <script language="javascript" type="text/javascript">
          function ValidateFileUploadExtension(Source, args) {
              var fupData = document.getElementById('<%= FileUploadControl.ClientID%>');
            var FileUploadPath = fupData.value;          
            if (FileUploadPath == '') {
                // There is no file selected
               //alert("Please select file to upload");
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "pdf" || Extension == "PDF" ) {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
          }

          function ValidateFileUploadExtension2(Source, args) {
            var fupData = document.getElementById('<%= FileUploadTHUMB.ClientID%>');
            var FileUploadPath = fupData.value;
            
            if (FileUploadPath == '') {
                // There is no file selected
                //alert("Please select file to upload");
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "gif" || Extension == "jpeg" || Extension == "jpg" || Extension == "png" ) {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
</script>

    <style>
        .text-bold {
    font-weight: 600 !important;
}
        table td input[type=image] {
            border-radius:4px !important;
        }
        .grid-thumbnail {
    height: 50px;
    width: 50px;
    border: 2px solid #8dc24c !important;
    border-radius: 4px !important;
    max-height: 50px !important;
    overflow: hidden !important;
}
    </style>
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>   Resources Entry
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">


            <div class="container-fluid p-0 mt-2 mb-4">

                <div class="container-fluid p-0 mt-2 mb-4 clearfix position-relative teachers-lockers">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div class="card min-height">
                                <div class="card-body">


                                    <!-- library content start -->
                                    <div class="library-data column-padding pt-3 pb-3">
                                        <div id="library" class="row view-group">
                                            <div class="col-lg-12 col-md-12 col-12">

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 text-left">
                                                        <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblError" runat="server"   EnableViewState="False"></asp:Label></td>
                    </tr>
                </table>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Basic Details</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12 mb-3" style="padding: 10px; border: 1px solid #e5ebdd;">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table width="100%">
                                                                    <tr>
                          <td align="left" width="20%"><span class="field-label">Category</span><asp:Label ID="lblasterick1" runat="server" text="*" cssclass="text-danger" ></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlCategory" runat="server"  AutoPostBack="true">
                                        </asp:DropDownList><br />
                            <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator1" controltovalidate="ddlCategory" errormessage="Select a category" validationgroup="preview"/>                               
                            </td>
                        
                      
                   
                          <td align="left" width="20%"><span class="field-label">Resource Title</span><asp:Label ID="Label1" runat="server" text="*" cssclass="text-danger" ></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtResourceTitle" runat="server" ></asp:TextBox><br />
                            <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" controltovalidate="txtResourceTitle" errormessage="Enter Title" validationgroup="preview"/>                               
                            </td>
                      
                      
                    </tr>

                      <tr>
                          <td align="left" width="20%"><span class="field-label">Resource Description</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtResourceDescription" runat="server"  TextMode="MultiLine"></asp:TextBox>
                            </td>
                    
                      
                    
                          <td align="left" width="20%"><span class="field-label">Resource Date</span><asp:Label ID="Label2" runat="server" text="*" cssclass="text-danger" ></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtResourceDate" runat="server" ></asp:TextBox><br />
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgEventDate" TargetControlID="txtResourceDate">
                                        </ajaxToolkit:CalendarExtender>
                             <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator3" controltovalidate="txtResourceDate" errormessage="Select a date" validationgroup="preview"/>
                            </td>
                     
                    </tr>
                                                                </table>
                                                            </div>
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Upload Files</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12 mb-3" style="padding: 10px; border: 1px solid #e5ebdd;">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table width="100%">
                                                                    <tr>
                          <td align="left" width="20%"><span class="field-label">Attachment File</span><asp:Label ID="Label3" runat="server" text="*" cssclass="text-danger" ></asp:Label></td>
                        <td align="left" width="30%">
                           
                             <asp:UpdatePanel ID="updpnl" runat="server">
                                                            <ContentTemplate>
                            <asp:FileUpload id="FileUploadControl" runat="server" />
                                                                 <asp:Label ID="lblattachfilename" runat="server"  align="left" visible="false" ></asp:Label>
                                                               </br>  <asp:Label ID="lblfilemsgfor" runat="server" text="File should be PDF format  " cssclass="text-warning" ></asp:Label>
                                                                </br><asp:Label ID="lblfilemsgSiz" runat="server" text="File size be less than 10MB   " cssclass="text-warning" ></asp:Label>
                               <asp:Button runat="server" id="UploadButtonbtn" text="Upload"   visible="false"/>
                            <asp:LinkButton ID="lnkUpload" runat="server" Visible="false">Upload</asp:LinkButton><br />
                                                                
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$"
    ControlToValidate="FileUploadControl" runat="server" cssclass="text-warning" ErrorMessage="Please select a valid pdf  file."
    Display="Dynamic" />             
                                   <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator4" controltovalidate="FileUploadControl" errormessage="Please upload a file" validationgroup="preview"/>                         
                            </td>
                      
                      
                   
                          <td align="left" width="20%"><span class="field-label">Thumbnail Image</span><asp:Label ID="Label5" runat="server" text="*" cssclass="text-warning" ></asp:Label></td>
                        <td align="left" width="30%">
                <%--             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>--%>
                            
                            <asp:FileUpload id="FileUploadTHUMB" runat="server" />
                            <asp:Image ID="ImgThumbnail" runat="server" Height="50px" Width="50px" Visible="false" />
                                                               </br>  <asp:Label ID="lblmsgformat" runat="server" text="File should be JPEG/PNG format  " cssclass="text-warning" ></asp:Label>
                                                                </br><asp:Label ID="lblmsgsize" runat="server" text="File size be less than 50KB" cssclass="text-warning" ></asp:Label>
                               <asp:Button runat="server" id="btnFileUploadIMG" text="Upload"   visible="false"/>
                            <asp:LinkButton ID="LNKFileUploadTHUMB" runat="server" Visible="false">Upload</asp:LinkButton><br />
                                                                
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$"
    ControlToValidate="FileUploadTHUMB" runat="server" cssclass="text-warning" ErrorMessage="Please select a valid jpg or png  file."
    Display="Dynamic" />             
                          <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator5" controltovalidate="FileUploadTHUMB" errormessage="Please upload a file" validationgroup="preview"/>  
                                <%--   </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkUpload" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>                                
                            </td>
                       
                      
                    </tr>

                                                                <tr style="display:none;">
                    <td align="left" width="20%"><span class="field-label">Active</span></td>
                    <td  align="left" width="30%"><asp:CheckBox ID="chkActive" runat="server"  Visible="false"/>
                    </td>
                 <td width="20%"></td>
                        <td width="30%"></td>
                </tr>
                                                                

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12">
                                                        <table width="100%">
                                                            <tr>
                                    <td align="center"  colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" validationgroup="preview"   />&nbsp;&nbsp;
                                       <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  />
                                           </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSave" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>  
                                    </td>
                         </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                        </div>
                                                    </div>

                                                <div class="row">
                                                <div class="col-lg-12 col-md-12">
                                                    <table width="100%">                  
                    
                                           <tr>                                                           
                                        <td align="center" colspan="4"  valign="top">
                                          
                                        <asp:GridView ID="gvResourcesEntry" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20" Width="100%" OnPageIndexChanging="gvResourcesEntry_PageIndexChanging">
                                            <RowStyle CssClass="griditem"  HorizontalAlign="Center" />
                                          
                                            <Columns>
                                         

                                                <asp:TemplateField HeaderText="SNO" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRcatID" runat="server" Text='<%# Bind("RST_ID")%>' visible="false"></asp:Label>
                                                        <asp:Label ID="lblSNO" runat="server" Text='<%# Bind("SNo")%>'  visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Category" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("RCM_DESCR")%>' align="left"></asp:Label>
                                                        
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="text-bold" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Title" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("RST_TITLE")%>' align="left"></asp:Label>
                                                       
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="text-bold" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblDescrip" runat="server" Text='<%# Bind("RST_DESCR")%>' align="left"></asp:Label>
                                                       
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("RST_CREATED_DT", "{0:dd/MMM/yyyy}")%>' align="left"></asp:Label>
                                                       <asp:Label ID="LblResourceEntrypath" runat="server" Text='<%# Eval("RST_THUMBNAIL_URL")%>' visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <HeaderStyle HorizontalAlign="center" Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Attachments">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="updpnl" runat="server">
                                                            <ContentTemplate>  
                                                      <%--<asp:LinkButton ID="lnkFileAttachments"  runat="server" align="center"  onclick="lnkFileAttachments_Click1" ></asp:LinkButton>--%>
                                                   <asp:Label ID="LblResourceEntryattachPath" runat="server" Text='<%# Eval("RST_FILE_URL")%>' visible="false"></asp:Label>
                                                      <asp:LinkButton ID="lnkBtnPrint" runat="server" align="center" OnClick="lnkBtnPrint_Click" Text="Download" CommandName="Print"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkBtnPrint" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                                   </ItemTemplate>
                                                    <HeaderStyle Width="10%" />
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                 
                                                <asp:TemplateField HeaderText="Thumbnail">
                                                    <ItemTemplate>
                                                         <asp:UpdatePanel ID="updpnl2" runat="server">
                                                            <ContentTemplate>  
                                                      <asp:ImageButton ID="ImgResourceEntry" runat="server" CssClass="grid-thumbnail" onclick="ImgResourceEntry_Click"  data-toggle="tooltip" Title="Click To Download" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="ImgResourceEntry" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                    <HeaderStyle Width="10%" />
                                                </asp:TemplateField>
                                               
                                             <asp:TemplateField HeaderText="Status" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("StatusACTIVE")%>'  align="left"></asp:Label>
                                                       
                                                    </ItemTemplate>
                                                 <HeaderStyle Width="10%" />
                                                </asp:TemplateField>
                                               
                                                     <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                        <asp:LinkButton ID="lnkBtnEdit" text="Edit" runat="server" align="center"  OnClick="lnkBtnEdit_Click1" ></asp:LinkButton> &nbsp;
                                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" align="center" OnClick="lnkbtnDelete_Click" Text="Delete"></asp:LinkButton>
                                                                 </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkBtnEdit" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>    
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10%" />
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                          

                                    </td>

                                    </tr>
                </table>
                                                </div>
                                            </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- library content ends -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            
                
                
               <script type="text/javascript" lang="javascript">
                   function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                       $.fancybox({
                           type: 'iframe',
                           //maxWidth: 300,
                           href: gotourl,
                           //maxHeight: 600,
                           fitToView: true,
                           padding: 6,
                           width: w,
                           height: h,
                           autoSize: false,
                           openEffect: 'none',
                           showLoading: true,
                           closeClick: true,
                           closeEffect: 'fade',
                           'closeBtn': true,
                           afterLoad: function () {
                               this.title = '';//ShowTitle(pageTitle);
                           },
                           helpers: {
                               overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                               title: { type: 'inside' }
                           },
                           onComplete: function () {
                               $("#fancybox-wrap").css({ 'top': '90px' });
                           },
                           onCleanup: function () {
                               var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                               if (hfPostBack == "Y")
                                   window.location.reload(true);
                           }
                       });

                       return false;
                   }
    </script>
            
        </div>
        <%--<uc2:usrMessageBar runat="server" ID="usrMessageBar" />--%>
    </div>
  
</asp:Content>

