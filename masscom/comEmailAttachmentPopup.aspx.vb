﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_comEmailAttachmentPopup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            ViewState("templateid") = Request.QueryString("templateid") 

            'Dim GrdAttachment As GridView = DirectCast(row.FindControl("GrdAttachment"), GridView)
            BindAttachments()
            BindBanner()
            BindThumbnail()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Private Sub BindAttachments()
        Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
        Dim val As String = ViewState("templateid")
        Dim d As New DirectoryInfo(serverpath + val + "/Attachments/")
        GrdAttachment.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
        GrdAttachment.DataBind()

        'Dim lnkdeletedir As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
        'lnkdeletedir.CommandArgument = val

        For Each arow As GridViewRow In GrdAttachment.Rows
            Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
            lnkattachment.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkattachment)
            Dim lnkdelete As LinkButton = DirectCast(arow.FindControl("lnkdelete"), LinkButton)
            lnkdelete.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
        Next
    End Sub
    Private Sub BindBanner()
        Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
        Dim val As String = ViewState("templateid")
        Dim d As New DirectoryInfo(serverpath + val + "/Banner/")
        GrdBanner.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
        GrdBanner.DataBind()

        'Dim lnkdeletedir As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
        'lnkdeletedir.CommandArgument = val

        For Each arow As GridViewRow In GrdBanner.Rows
            Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
            lnkattachment.CommandArgument = serverpath + val + "/Banner/" + lnkattachment.Text.Trim()
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkattachment)
            Dim lnkdelete As LinkButton = DirectCast(arow.FindControl("lnkdelete"), LinkButton)
            lnkdelete.CommandArgument = serverpath + val + "/Banner/" + lnkattachment.Text.Trim()
        Next
    End Sub
    Private Sub BindThumbnail()
        Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
        Dim val As String = ViewState("templateid")
        Dim d As New DirectoryInfo(serverpath + val + "/Thumbnail/")
        GridThumbnail.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
        GridThumbnail.DataBind()

        'Dim lnkdeletedir As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
        'lnkdeletedir.CommandArgument = val

        For Each arow As GridViewRow In GridThumbnail.Rows
            Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
            lnkattachment.CommandArgument = serverpath + val + "/Thumbnail/" + lnkattachment.Text.Trim()
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkattachment)
            Dim lnkdelete As LinkButton = DirectCast(arow.FindControl("lnkdelete"), LinkButton)
            lnkdelete.CommandArgument = serverpath + val + "/Thumbnail/" + lnkattachment.Text.Trim()
        Next
    End Sub
    Protected Sub GrdTextAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAttachment.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If
        If e.CommandName = "Remove" Then
            Dim f As New FileInfo(e.CommandArgument)
            If f.Exists Then
                f.Delete()
            End If
            'BindEmailText()
            lblmessage.Text = "Attachment Deleted"
            BindAttachments()
        End If
        If e.CommandName = "search" Then
            'BindEmailText()
        End If


    End Sub

    Protected Sub GrdBanner_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdBanner.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If
        If e.CommandName = "Remove" Then
            Dim f As New FileInfo(e.CommandArgument)
            If f.Exists Then
                f.Delete()
            End If
            'BindEmailText()
            lblmessage.Text = "Banner Deleted"
            BindBanner()
        End If
        If e.CommandName = "search" Then
            'BindEmailText()
        End If


    End Sub
    Protected Sub Thumbnail_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridThumbnail.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If
        If e.CommandName = "Remove" Then
            Dim f As New FileInfo(e.CommandArgument)
            If f.Exists Then
                f.Delete()
            End If
            'BindEmailText()
            lblmessage.Text = "Thumbnail Deleted"
            BindBanner()
        End If
        If e.CommandName = "search" Then
            'BindEmailText()
        End If


    End Sub

End Class
