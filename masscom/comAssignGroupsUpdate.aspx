﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsUpdate.aspx.vb" Inherits="masscom_comAssignGroupsUpdate" %>

<%@ Register Src="UserControls/comAssignGroupsUpdate.ascx" TagName="comAssignGroupsUpdate" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../cssfiles/Popup.css" rel="stylesheet" />

<script language="javascript" type="text/javascript">
    function SetCloseNewGRPValuetoParent(msg) {
        //alert(msg);



        $("#<%=hdn_msg.ClientID%>").val(msg);
             __doPostBack('<%=hdn_msg.ClientID%>', "");

         }
</script>
<body>
    <form id="form1" runat="server">
        <div>
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <uc1:comAssignGroupsUpdate ID="ComAssignGroupsUpdate1" runat="server" />
        </div>
        <asp:HiddenField ID="hdn_msg" runat="server" />

    </form>
</body>
</html>
