Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_comManageSMS
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Hiddenbsuid.Value = Session("sBsuid")
            CheckMenuRights()
            BindGrid()
            BindHrsMins()
            Btn_grp.Attributes.Add("onclick", "javascript: AssignGroup(); return true;")
            Btn_exl.Attributes.Add("onclick", "javascript: AssignExcel(); return true;")
            Me.hfMenuCode.Value = ViewState("MainMnu_code")
        End If
        AssignRights()
    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
        'directory.Add("Add", lnkAddNew)
        'directory.Add("Add", btn_newemail)
        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))


        For Each row As GridViewRow In GridMSMS.Rows
            Dim editlink As Image = DirectCast(row.FindControl("Image1"), Image)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Edit", editlink)
            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next


    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Me.hfMenuCode.Value = Request.QueryString("MainMnu_code")
            If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "M000040") And (ViewState("MainMnu_code") <> "M0000100")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
    End Sub


    Protected Sub lblbtn_Click(sender As Object, e As EventArgs) Handles lblbtn.Click


    End Sub
    Public Shared Function GET_EMAIL_SMS_TEMPLATES(ByVal OPTIONS As Integer, Optional ByVal TEMPLATE_ID As String = "") As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@TEMPLATE_ID", SqlDbType.VarChar)
        pParms(1).Value = TEMPLATE_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[DBO].[GET_EMAIL_SMS_TEMPLATES]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function GET_GROUPS_TEMPLATES_M(ByVal OPTIONS As Integer, Optional ByVal CGR_ID As String = "") As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@CGR_ID", SqlDbType.VarChar)
        pParms(1).Value = CGR_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[DBO].[GET_GROUPS_TEMPLATES_M]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Protected Sub grptemplateid_ValueChanged(sender As Object, e As EventArgs) Handles grptemplateid.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        TR1.Attributes.Add("style", "display:grid")
        TR2.Attributes.Add("style", "display:grid")
        TR3.Attributes.Add("style", "display:none")
        Dim c_date As String = DateTime.Now.ToString("dd/MMM/yyyy")
        txtdate.Text = c_date
        BindHrsMins()

        If DateTime.Now.Hour < 10 Then
            Dim hrr As String = "0" & DateTime.Now.Hour
            ddhour.SelectedValue = hrr
        Else
            ddhour.SelectedValue = DateTime.Now.Hour
        End If
        If DateTime.Now.Minute < 10 Then
            Dim minn As String = "0" & DateTime.Now.Minute
            ddmins.SelectedValue = minn
        Else
            ddmins.SelectedValue = DateTime.Now.Minute
        End If

        grptemplateid.Value = ERR_MSG

        Dim grp_name As String = ""
        Dim dt5 As DataTable = GET_GROUPS_TEMPLATES_M(1, grptemplateid.Value)

        For Each row2 As DataRow In dt5.Rows
            If grp_name = "" Then
                grp_name = row2.Item("CGR_DES")
            Else
                grp_name = grp_name + " || " + row2.Item("CGR_DES")
            End If


        Next
        popupmsg.Text = "Selected Group/Excel Templates is/are: " + grp_name 'grptemplateid.Value

    End Sub

    Protected Sub exltemplateid_ValueChanged(sender As Object, e As EventArgs) Handles exltemplateid.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        TR1.Attributes.Add("style", "display:grid")
        TR2.Attributes.Add("style", "display:grid")
        TR3.Attributes.Add("style", "display:none")
        Dim c_date As String = DateTime.Now.ToString("dd/MMM/yyyy")
        txtdate.Text = c_date
        BindHrsMins()

        If DateTime.Now.Hour < 10 Then
            Dim hrr As String = "0" & DateTime.Now.Hour
            ddhour.SelectedValue = hrr
        Else
            ddhour.SelectedValue = DateTime.Now.Hour
        End If
        If DateTime.Now.Minute < 10 Then
            Dim minn As String = "0" & DateTime.Now.Minute
            ddmins.SelectedValue = minn
        Else
            ddmins.SelectedValue = DateTime.Now.Minute
        End If
        exltemplateid.Value = ERR_MSG

        Dim tmplt_name As String = ""
        Dim dt5 As DataTable = GET_GROUPS_TEMPLATES_M(3, exltemplateid.Value)

        For Each row2 As DataRow In dt5.Rows
            If tmplt_name = "" Then
                tmplt_name = row2.Item("TITLE")
            Else
                tmplt_name = tmplt_name + " || " + row2.Item("TITLE")
            End If



        Next
        popupmsg.Text = "Selected Templates is/are: " + tmplt_name 'exltemplateid.Value

    End Sub

    Protected Sub h_MSG_ValueChanged(sender As Object, e As EventArgs) Handles h_MSG.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        BindGrid()
        lblmessage.Text = ERR_MSG
        h_MSG.Value = ""
    End Sub

    Protected Sub h_MSG2_ValueChanged(sender As Object, e As EventArgs) Handles h_MSG2.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value

        lblmessage.Text = ERR_MSG
        h_MSG2.Value = ""
    End Sub


    Public Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim itemBsuId As String = Hiddenbsuid.Value
            Dim str_query = " select cms_id,cms_date, '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(cms_sms_text,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,cms_sms_text,substring(cms_sms_text,0,150)+'....' as cms_sms_text_1, " &
                            " 'javascript:EditTemplate('''+ CONVERT(VARCHAR,cms_id) +'''); return false;' EditTemplate from com_manage_sms WITH (NOLOCK) where CMS_BSU_ID ='" & itemBsuId & "' AND ISNULL(CMS_DELETED,'False')='False' AND CMS_DATE>='01/JAN/2014'"

            Dim Txt1 As String = ""
            Dim Txt2 As String = ""
            Dim Txt3 As String = ""

            If GridMSMS.Rows.Count > 0 Then
                'Txt1 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
                'Txt2 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
                'Txt3 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

                If Txt1.Trim() <> "" Then
                    str_query &= " and replace(cms_id,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                End If

                If Txt2.Trim() <> "" Then
                    str_query &= " and REPLACE(CONVERT(VARCHAR(11), cms_date, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
                End If

                If Txt_SMSTitle.Text.Trim() <> "" Then
                    str_query &= " and replace(cms_sms_text,' ','') like '%" & Txt_SMSTitle.Text.Replace(" ", "") & "%' "
                End If

            End If

            str_query &= " order by CMS_ID desc "


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count = 0 Then
                Dim dt As New DataTable
                dt.Columns.Add("cms_id")
                dt.Columns.Add("cms_date")
                dt.Columns.Add("hide")
                dt.Columns.Add("tempview")
                dt.Columns.Add("cms_sms_text_1")
                dt.Columns.Add("EditTemplate")


                Dim dr As DataRow = dt.NewRow()
                dr("cms_id") = ""
                dr("cms_date") = ""
                dr("hide") = ""
                dr("tempview") = ""
                dr("cms_sms_text_1") = ""
                dr("EditTemplate") = ""

                dt.Rows.Add(dr)
                GridMSMS.DataSource = dt
                GridMSMS.DataBind()

                DirectCast(GridMSMS.Rows(0).FindControl("Image1"), Image).Visible = False
            Else
                GridMSMS.DataSource = ds
                GridMSMS.DataBind()

            End If

            If GridMSMS.Rows.Count > 0 Then

                'DirectCast(GridMSMS.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
                'DirectCast(GridMSMS.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
                'DirectCast(GridMSMS.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

            End If
            'lblmessage.Text = "Page Refreshed..."
            lnkreports.Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "masscomeditnews")
            'lblmessage.Text = ex.ToString
        End Try
    End Sub

    Protected Sub GridMSMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridMSMS.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('../comViewSmsText.aspx?cmsid={0}', '','dialogHeight:600px;dialogWidth:600px;scroll:yes;resizable:no;');", pId)
    End Function
    Protected Function GetUrl(ByVal pId As String) As String
        Return String.Format("javascript:openPopup('../comViewSmsText.aspx?cmsid={0}')", pId)
    End Function

    Protected Sub GridMSMS_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "sendmail" Then
            divAge.Visible = True
            'divAge.Style.Add("display", "block")
            grpemailtemplateid.Value = e.CommandArgument
            TR1.Attributes.Add("style", "display:none")
            TR2.Attributes.Add("style", "display:none")
            TR3.Attributes.Add("style", "display:grid")
            popupmsg.Text = ""
            grptemplateid.Value = ""
            exltemplateid.Value = ""
            lnkreports.Visible = False

            Dim tmplt_title As String = ""
            Dim dt5 As DataTable = GET_EMAIL_SMS_TEMPLATES(2, grpemailtemplateid.Value)

            For Each row2 As DataRow In dt5.Rows
                If tmplt_title = "" Then
                    tmplt_title = row2.Item("cms_sms_text_1")
                Else
                    tmplt_title = tmplt_title + " || " + row2.Item("cms_sms_text_1")
                End If


            Next
            popuptitlemsg.Text = "Selected SMS Templates text is: " + tmplt_title 'grptemplateid.Value

        End If
        If e.CommandName = "Editing" Then
            HiddenFieldTemplateid.Value = e.CommandArgument
            h_MSG.Value = ""
            lnkreports.Visible = False
        End If
        If e.CommandName = "report" Then
            Dim EncDec As New Encryption64
            Dim cmsid As String = e.CommandArgument
            cmsid = EncDec.Encrypt(cmsid)
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
            Response.Redirect("comManageSmsReports.aspx?cms_id=" & cmsid & mInfo)
        End If
        If e.CommandName = "search" Then
            BindGrid()
        End If

    End Sub
    Public Sub PopUpSMSWindow()
        Dim url As String = "comCreateSmsText.aspx?mnu=" & Me.hfMenuCode.Value
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "PopUpSMSWindow", "javascript:openPopup('" & url & "');", True)

    End Sub
    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False
        'divAge.Style.Add("display", "none")
    End Sub
    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divAge.Visible = False
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Dim k As Boolean = checkdatetime(txtdate.Text, ddhour.SelectedValue, ddmins.SelectedValue)
        If k = True Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CMS_ID", grpemailtemplateid.Value)
            pParms(1) = New SqlClient.SqlParameter("@HOUR", Convert.ToInt16(ddhour.SelectedValue))
            pParms(2) = New SqlClient.SqlParameter("@MINUTE", ddmins.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@BSU_COUNTRY_ID", Session("BSU_COUNTRY_ID"))
            pParms(4) = New SqlClient.SqlParameter("@RETURN_MSG", SqlDbType.VarChar, 100)
            pParms(4).Direction = ParameterDirection.Output
            SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SMS].[CHECK_SMS_SCHEDULER_TIME]", pParms)
            Dim outvalue As String = pParms(4).Value
            If outvalue = "" Then
                popuperrmsg.Text = ""
                If grp_exl_btn_sel_id.Value = 1 Then

                    '''''groupemailapprovalentry()
                    btngrpemailok_Click()
                    TR1.Attributes.Add("style", "display:none")
                    TR2.Attributes.Add("style", "display:none")
                    TR3.Attributes.Add("style", "display:grid")
                    lblmessage.Text = "Schedule has been successfully done"
                    lnkreports.Visible = True
                ElseIf grp_exl_btn_sel_id.Value = 2 Then
                    btnexlemailok_Click()
                    TR1.Attributes.Add("style", "display:none")
                    TR2.Attributes.Add("style", "display:none")
                    TR3.Attributes.Add("style", "display:grid")
                    lblmessage.Text = "Schedule has been successfully done"
                    lnkreports.Visible = True
                End If

                divAge.Visible = False
            Else
                popuperrmsg.Text = outvalue
            End If


        Else
            'lblmessage.Text = "Selected Date and Time should be greater than Current Date and Time"
            popuperrmsg.Text = "Selected Date and Time should be greater than Current Date and Time"
        End If
    End Sub
    Public Function checkdatetime(ByRef p_date As String, ByRef p_hour As String, ByRef p_minute As String) As Boolean
        Dim c_date As String
        Dim ps_date As String
        ps_date = p_date + " " + p_hour + ":" + p_minute
        c_date = DateTime.Now.ToString("dd/MM/yyyy HH:mm")

        Dim pass_date As DateTime = DateTime.ParseExact(ps_date, "dd/MMM/yyyy HH:mm", Nothing)
        Dim cur_date As DateTime = DateTime.ParseExact(c_date, "dd/MM/yyyy HH:mm", Nothing)

        If pass_date > cur_date Then
            Return True

        Else
            Return False
        End If

    End Function
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    'excel email schedule
    Protected Sub btnexlemailok_Click()
        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        'If hours < 0 Then

        Dim pParms(8) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@EML_ID", grpemailtemplateid.Value)
        'pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", "Using Previous Template") '' We are taking data log id data
        'pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
        'pParms(3) = New SqlClient.SqlParameter("@CSE_ID", 0)
        'pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
        'pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
        'pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
        'pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", exltemplateid.Value)


        pParms(0) = New SqlClient.SqlParameter("@CMS_ID", grpemailtemplateid.Value)
        pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", "Using Previous Template") '' We are taking data log id data
        pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
        pParms(3) = New SqlClient.SqlParameter("@CSM_ID", 0)
        pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
        pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
        pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
        pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", exltemplateid.Value)


        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_SCHEDULE_INSERT", pParms)
        'lblmessage.Text = "Schedule has been successfully done"
        'lblUerror.Text = "Schedule has been successfully done"
        txtdate.Text = ""
        'Else
        ''lblsmessage.Text = "Date time is past"

        'End If


    End Sub

    'grup email approaval entry
    Protected Sub groupemailapprovalentry()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CSE_CGR_ID", grptemplateid.Value)
        pParms(1) = New SqlClient.SqlParameter("@CSE_EML_ID", grpemailtemplateid.Value)
        pParms(2) = New SqlClient.SqlParameter("@CSE_BSU_ID", Hiddenbsuid.Value) ''---------------------|  (grpbsuid)
        pParms(3) = New SqlClient.SqlParameter("@CSE_TYPE", "PT")
        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COM_SEND_EMAIL_INSERT", pParms)


    End Sub

    'group email
    Protected Sub btngrpemailok_Click()

        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        'If hours < 0 Then


        Dim str_Data0 As String
        Dim strArr0() As String
        str_Data0 = grptemplateid.Value '"6||A||4"
        strArr0 = str_Data0.Split("||")

        If strArr0.Length > 0 Then
            For i As Int16 = 0 To strArr0.Length - 1
                If strArr0(i).ToString <> "" AndAlso IsNumeric(strArr0(i).ToString) Then
                    'System.Console.WriteLine("--" & strArr0(i))

                    Dim pParms(9) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@CMS_ID", grpemailtemplateid.Value) 'selected email template id
                    pParms(1) = New SqlClient.SqlParameter("@CGR_ID", strArr0(i)) 'selected email group id
                    pParms(2) = New SqlClient.SqlParameter("@CSM_TYPE", "SMS")
                    pParms(3) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
                    pParms(4) = New SqlClient.SqlParameter("@EXCEL_PATH", "")
                    pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value) 'selected bsu id
                    pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
                    pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", "")
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.COM_AUTO_APPROVED_SMS_SCHEDULE_INSERT", pParms)

                End If
            Next
        End If



        'Dim ds As DataSet
        'Dim str_query = "select CSE_CGR_ID,CSE_EML_ID from COM_SEND_EMAIL where CSE_ID='" & grptemplateid.Value & "'"
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'If ds.Tables(0).Rows.Count > 0 Then
        '    HiddenCGR_ID = ds.Tables(0).Rows(0).Item("CSE_CGR_ID").ToString()
        '    HiddenEML_ID = ds.Tables(0).Rows(0).Item("CSE_EML_ID").ToString()

        '    str_query = "select * from COM_MANAGE_EMAIL_SCHEDULE where CSE_ID='" & grptemplateid.Value & "'"
        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        lblmessage.Text = "You have already scheduled this template"
        '        lblUerror.Text = "You have already scheduled this template"
        '    Else
        '        Dim pParms(6) As SqlClient.SqlParameter
        '        pParms(0) = New SqlClient.SqlParameter("@EML_ID", HiddenEML_ID)
        '        pParms(1) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
        '        pParms(2) = New SqlClient.SqlParameter("@CSE_ID", grptemplateid.Value)
        '        pParms(3) = New SqlClient.SqlParameter("@CGR_ID", HiddenCGR_ID)
        '        pParms(4) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
        '        pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))

        '        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_SCHEDULE_INSERT", pParms)
        '        lblmessage.Text = "Schedule has been successfully done"
        '        lblUerror.Text = "Schedule has been successfully done"
        '    End If

        'End If


        'Else
        ''lblsmessage.Text = "Date time is past"

        'End If


    End Sub
    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub
End Class
