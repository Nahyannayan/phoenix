﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic

Partial Class masscom_comResourcesEntry
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        ' Dim smscript As ScriptManager = Master.FindControl("ScriptManager1")
        ' smscript.RegisterPostBackControl(btnSave)
        If Page.IsPostBack = False Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            'btnSave.Attributes.Add("onclick", "return UnpostOthFeecharge(" & h_UnpostFChg.ClientID & ") ")


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ' gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                If Session("sUsr_name") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_DAYEND_PROCESS
                            lblHead.Text = "Day End Process"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    ' Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    
                    Page.Form.Attributes.Add("enctype", "multipart/form-data")

                    Call bindCategory()
                    Call BinGrid()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try

        End If
    End Sub
    
    
    Protected Sub ClearData()
        Call bindCategory()
        txtResourceTitle.Text = ""
        txtResourceDescription.Text = ""
        chkActive.Checked = False
        txtResourceDate.Text = ""
        ImgThumbnail.Visible = False
        lblattachfilename.Visible = False
    End Sub

    Private Sub bindCategory()
        Try

            Dim sqlCon_img As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim Param_img(0) As SqlParameter
            Param_img(0) = Mainclass.CreateSqlParameter("@bsuid", Session("sBsuid"), SqlDbType.VarChar)
            ds = SqlHelper.ExecuteDataset(sqlCon_img, "dbo.[GetResourcesCategory_NEW1]", Param_img)
            dt = ds.Tables(0)
            '  If dt.Rows.Count > 0 Then
            ddlCategory.Items.Clear()
            ddlCategory.DataSource = ds.Tables(0)
            ddlCategory.DataTextField = "RCM_DESCR"
            ddlCategory.DataValueField = "RCM_ID"
            ddlCategory.DataBind()

        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub

    Private Sub BinGrid()


        Dim sqlCon1 As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable
        Dim Param1(0) As SqlParameter
        Param1(0) = Mainclass.CreateSqlParameter("@bsuid", Session("sBsuid"), SqlDbType.VarChar)
        ds1 = SqlHelper.ExecuteDataset(sqlCon1, "dbo.[GetResourcesALLEntry]", Param1)
        dt1 = ds1.Tables(0)
        gvResourcesEntry.DataSource = dt1

        gvResourcesEntry.DataBind()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("Resources").ToString()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim catID As Integer = ddlCategory.SelectedValue
        ViewState("catID") = CStr(catID)
        Dim thumbnailurl As String = "~/Resources/" + ViewState("catID") + "/"
        If btnSave.Text = "Update" Then
            If FileUploadControl.HasFile Then 'pdf file attachment
                Dim filesize As Integer = FileUploadControl.PostedFile.ContentLength
                If filesize < 10485760 Then '10485760 bytes= 10MB

                    If Not Directory.Exists(PhyPath + ViewState("catID")) Then
                        ' Create the directory.
                        Directory.CreateDirectory(PhyPath + ViewState("catID"))
                    End If
                    Dim filename As String = Path.GetFileName(FileUploadControl.FileName)
                    Dim path1 As String = PhyPath

                    ViewState("filename") = filename
                    Dim newFilepath As String = PhyPath + ViewState("catID") + "\" + ViewState("catID") + "_" + filename
                    FileUploadControl.SaveAs(newFilepath)
                Else
                    lblError.Text = "File Size should be less than 10MB"
                    Exit Sub
                End If
            Else

            End If

            If FileUploadTHUMB.HasFile Then
                Dim filesize As Integer = FileUploadTHUMB.PostedFile.ContentLength

                If filesize < 50000 Then '5242880 bytes= 5MB

                    If Not Directory.Exists(PhyPath + ViewState("catID")) Then
                        ' Create the directory.
                        Directory.CreateDirectory(PhyPath + ViewState("catID"))
                    End If
                    Dim filenamethumb As String = Path.GetFileName(FileUploadTHUMB.FileName)
                    Dim path1 As String = PhyPath

                    ViewState("filenameThumb") = filenamethumb
                    Dim newthumbnailpath As String = PhyPath + ViewState("catID") + "\" + ViewState("catID") + "_" + filenamethumb
                    FileUploadTHUMB.SaveAs(newthumbnailpath)
                Else
                    lblError.Text = "File Size should be less than 50KB"
                    Exit Sub
                End If

            End If
            Dim Param(8) As SqlParameter

            Dim strans As SqlTransaction = Nothing

            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction

            Param(0) = Mainclass.CreateSqlParameter("@RST_ID", Convert.ToInt32(ViewState("RST_ID")), SqlDbType.BigInt)
            Param(1) = Mainclass.CreateSqlParameter("@RST_RCM_ID", Convert.ToInt32(ViewState("catID")), SqlDbType.BigInt)
            Param(2) = Mainclass.CreateSqlParameter("@RST_TITLE", txtResourceTitle.Text, SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@RST_DESCR", txtResourceDescription.Text, SqlDbType.VarChar)
            If ViewState("filename") <> "" Then
                Param(4) = Mainclass.CreateSqlParameter("@RST_FILE_URL", thumbnailurl + ViewState("catID") + "_" + ViewState("filename"), SqlDbType.VarChar)
            Else
                Param(4) = Mainclass.CreateSqlParameter("@RST_FILE_URL", ViewState("fileUrL"), SqlDbType.VarChar)

            End If
            If ViewState("filenameThumb") <> "" Then
                Param(5) = Mainclass.CreateSqlParameter("@RST_THUMBNAIL_URL", thumbnailurl + ViewState("catID") + "_" + ViewState("filenameThumb"), SqlDbType.VarChar)
            Else
                Param(5) = Mainclass.CreateSqlParameter("@RST_THUMBNAIL_URL", ViewState("filenameThumb"), SqlDbType.VarChar)
            End If

            Param(6) = Mainclass.CreateSqlParameter("@RST_IS_ACTIVE", DBNull.Value, SqlDbType.Bit)
            Param(7) = Mainclass.CreateSqlParameter("@RST_LAST_MODIFIED_DT", Format(Date.Now, "dd/MMM/yyyy"), SqlDbType.Date)
            Param(8) = Mainclass.CreateSqlParameter("@RST_LAST_MODIFIED_BY", Session("sUsr_name"), SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[EditResources_TranBYID]", Param)
            strans.Commit()
            lblError.Text = "Updated Successfully"
            'Call BINDGRID()
            'Call ClearData()
            btnSave.Text = "Save"
        Else


            If FileUploadControl.HasFile Then 'pdf file attachment
                Dim filesize As Integer = FileUploadControl.PostedFile.ContentLength
                If filesize < 10485760 Then '10485760 bytes= 10MB

                    If Not Directory.Exists(PhyPath + ViewState("catID")) Then
                        ' Create the directory.
                        Directory.CreateDirectory(PhyPath + ViewState("catID"))
                    End If
                    Dim filename As String = Path.GetFileName(FileUploadControl.FileName)
                    Dim path1 As String = PhyPath

                    ViewState("filename") = filename
                    Dim newFilepath As String = PhyPath + ViewState("catID") + "\" + ViewState("catID") + "_" + filename
                    FileUploadControl.SaveAs(newFilepath)
                Else
                    lblError.Text = "File Size should be less than 10MB"
                    Exit Sub
                End If

            End If
            'thumbnail

            If FileUploadTHUMB.HasFile Then

                Dim filesize As Integer = FileUploadTHUMB.PostedFile.ContentLength

                If filesize < 5242880 Then '5242880 bytes= 5MB

                    If Not Directory.Exists(PhyPath + ViewState("catID")) Then
                        ' Create the directory.
                        Directory.CreateDirectory(PhyPath + ViewState("catID"))
                    End If
                    Dim filenamethumb As String = Path.GetFileName(FileUploadTHUMB.FileName)
                    Dim path1 As String = PhyPath

                    ViewState("filenameThumb") = filenamethumb
                    Dim newthumbnailpath As String = PhyPath + ViewState("catID") + "\" + ViewState("catID") + "_" + filenamethumb
                    FileUploadTHUMB.SaveAs(newthumbnailpath)

                Else
                    lblError.Text = "File Size should be less than 5MB"
                    Exit Sub
                End If
            End If

            Dim resourcesdate As String = txtResourceDate.Text


            '  Dim catID As Integer = ddlCategory.SelectedItem.Value

            Dim Param(10) As SqlParameter

            Dim strans As SqlTransaction = Nothing

            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction

            Param(0) = Mainclass.CreateSqlParameter("@RST_RCM_ID", ddlCategory.SelectedItem.Value, SqlDbType.BigInt)
            Param(1) = Mainclass.CreateSqlParameter("@RST_TITLE", txtResourceTitle.Text, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@RST_DESCR", txtResourceDescription.Text, SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@RST_FILE_URL ", thumbnailurl + ViewState("catID") + "_" + ViewState("filename"), SqlDbType.VarChar)
            Param(4) = Mainclass.CreateSqlParameter("@RST_THUMBNAIL_URL", thumbnailurl + ViewState("catID") + "_" + ViewState("filenameThumb"), SqlDbType.VarChar)
            Param(5) = Mainclass.CreateSqlParameter("@RST_BANNER_URL", DBNull.Value, SqlDbType.VarChar)
            Param(6) = Mainclass.CreateSqlParameter("@RST_IS_ACTIVE", DBNull.Value, SqlDbType.Bit)
            Param(7) = Mainclass.CreateSqlParameter("@RST_CREATED_DT", CType(resourcesdate, DateTime), SqlDbType.Date)
            Param(8) = Mainclass.CreateSqlParameter("@RST_LAST_MODIFIED_DT", DBNull.Value, SqlDbType.Date)
            Param(9) = Mainclass.CreateSqlParameter("@RST_CREATED_BY", Session("sUsr_name"), SqlDbType.VarChar)
            Param(10) = Mainclass.CreateSqlParameter("@RST_LAST_MODIFIED_BY ", DBNull.Value, SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[SaveResourcesTRAN_ENTRy1_NEW]", Param)
            strans.Commit()
            lblError.Text = "Saved Successfully"
        End If
        Call BinGrid()
        Call ClearData()

    End Sub

    Protected Sub lnkBtnEdit_Click(sender As Object, e As EventArgs)

        Dim lblRST_ID As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                        GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        lblRST_ID = TryCast(gvRow.Cells(0).FindControl("RST_ID"), Label)
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        ViewState("RST_ID") = lblRST_ID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable
        Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("Resources").ToString()
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@RST_ID", Convert.ToInt32(lblRST_ID.Text), SqlDbType.BigInt)

        ds1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetResourcesTranByID", Param)
        dt1 = ds1.Tables(0)
        If dt1.Rows.Count > 0 Then
            'txtCategory.Text = dt1.Rows(0).Item("RCM_DESCR")
            'chkActive.Checked = dt1.Rows(0).Item("RCM_IS_ACTIVE")
            ddlCategory.SelectedItem.Value = dt1.Rows(0).Item("RST_RCM_ID")
            txtResourceTitle.Text = dt1.Rows(0).Item("RST_TITLE")
            txtResourceDescription.Text = dt1.Rows(0).Item("RST_DESCR")
            txtResourceDate.Text = dt1.Rows(0).Item("RST_CREATED_DT")
            chkActive.Checked = dt1.Rows(0).Item("RST_IS_ACTIVE")
            ViewState("thumbnailUrL") = dt1.Rows(0).Item("RST_THUMBNAIL_URL")
            ViewState("fileUrL") = dt1.Rows(0).Item("RST_FILE_URL")
            ' ImgThumbnail.Visible = True
            Dim thumbnailpath As String = ViewState("thumbnailUrL")
            Dim attachfilename As String = Path.GetFileName(thumbnailpath)
            Dim attachmentsf() As String = attachfilename.ToString.Split("_")
            Dim BYTES() As Byte = File.ReadAllBytes(PhyPath + attachmentsf(0) + "\" + attachfilename) 'for display img to button converted to bytes
            Dim base64String As String = Convert.ToBase64String(BYTES, 0, BYTES.Length)
            ImgThumbnail.ImageUrl = "data:image/jpeg;base64," + base64String
            lblattachfilename.Visible = True
            Dim attFilename As String = Path.GetFileName(ViewState("fileUrL"))
            lblattachfilename.Text = attachfilename
        End If
        btnSave.Text = "Update"
    End Sub

    Protected Sub lnkBtnEdit_Click1(sender As Object, e As EventArgs)
        Dim lblRST_ID As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent.Parent.Parent,  _
                                        GridViewRow)
        bindCategory()
        Dim index As Integer = gvRow.RowIndex
        lblRST_ID = TryCast(gvRow.Cells(0).FindControl("lblRcatID"), Label)
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        ViewState("RST_ID") = lblRST_ID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable
        Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("Resources").ToString()
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@RST_ID", Convert.ToInt32(lblRST_ID.Text), SqlDbType.BigInt)

        ds1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetResourcesTranByID", Param)
        dt1 = ds1.Tables(0)
        If dt1.Rows.Count > 0 Then
            'txtCategory.Text = dt1.Rows(0).Item("RCM_DESCR")
            'chkActive.Checked = dt1.Rows(0).Item("RCM_IS_ACTIVE")
            ddlCategory.Items.FindByValue(dt1.Rows(0).Item("RST_RCM_ID")).Selected = True
            txtResourceTitle.Text = dt1.Rows(0).Item("RST_TITLE")
            txtResourceDescription.Text = dt1.Rows(0).Item("RST_DESCR")
            txtResourceDate.Text = Format(dt1.Rows(0).Item("RST_CREATED_DT"), "dd/MMM/yyyy")
            chkActive.Checked = dt1.Rows(0).Item("RST_IS_ACTIVE")
            ViewState("thumbnailUrL") = dt1.Rows(0).Item("RST_THUMBNAIL_URL")
            ViewState("fileUrL") = dt1.Rows(0).Item("RST_FILE_URL")
            ImgThumbnail.Visible = True
            Dim thumbnailpath As String = ViewState("thumbnailUrL")
            Dim attachfilename As String = Path.GetFileName(thumbnailpath)
            Dim attachmentsf() As String = attachfilename.ToString.Split("_")
            Dim BYTES() As Byte = File.ReadAllBytes(PhyPath + attachmentsf(0) + "\" + attachfilename) 'for display img to button converted to bytes
            Dim base64String As String = Convert.ToBase64String(BYTES, 0, BYTES.Length)
            ImgThumbnail.ImageUrl = "data:image/jpeg;base64," + base64String
            lblattachfilename.Visible = True
            Dim attFilename As String = Path.GetFileName(ViewState("fileUrL"))
            lblattachfilename.Text = attFilename
        End If
        btnSave.Text = "Update"
    End Sub

    Protected Sub lnkbtnDelete_Click(sender As Object, e As EventArgs)
        Dim lblRST_ID As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent.Parent.Parent,  _
                                        GridViewRow)
        bindCategory()
        Dim index As Integer = gvRow.RowIndex
        lblRST_ID = TryCast(gvRow.Cells(0).FindControl("lblRcatID"), Label)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConnc As New SqlConnection(str_conn)
        objConnc.Open()
        ViewState("RST_ID") = lblRST_ID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable

        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@RST_ID", Convert.ToInt32(lblRST_ID.Text), SqlDbType.BigInt)



        SqlHelper.ExecuteNonQuery(objConnc, "[dbo].[DeleteResources_tranBYID]", Param)
        objConnc.Close()
        Call BinGrid()
    End Sub


    Protected Sub gvResourcesEntry_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvResourcesEntry.RowDataBound
        Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("Resources").ToString()
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ImgResourceEntry As ImageButton = TryCast(e.Row.FindControl("ImgResourceEntry"), ImageButton)
            Dim LblResourceEntrypath As Label = TryCast(e.Row.FindControl("LblResourceEntrypath"), Label)
            Dim attachfilename As String = Path.GetFileName(LblResourceEntrypath.Text)
            Dim attachmentsf() As String = attachfilename.ToString.Split("_")
            Dim BYTES() As Byte = File.ReadAllBytes(PhyPath + attachmentsf(0) + "\" + attachfilename) 'for display img to button converted to bytes
            Dim base64String As String = Convert.ToBase64String(BYTES, 0, BYTES.Length)
            ImgResourceEntry.ImageUrl = "data:image/jpeg;base64," + base64String

        End If
    End Sub

    'Protected Sub lnkFileAttachments_Click(sender As Object, e As EventArgs)
    '    Dim filePath As String = TryCast(sender, LinkButton).CommandArgument
    '    Dim newfilepath As String = filePath.ToString.Replace("/", "\")
    '    Dim testpath As String = newfilepath.TrimStart("~")
    '    Response.ContentType = ContentType
    '    Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(testpath.Substring(0)))
    '    Response.WriteFile(filePath)
    '    Response.[End]()
    'End Sub



    Protected Sub lnkBtnPrint_Click(sender As Object, e As EventArgs)
        Try

            Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("Resources").ToString()
            Dim LblResourceEntryattachPath As Label
            Dim lblStudno As Label

            Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent.Parent.Parent,  _
                                           GridViewRow)
            Dim index As Integer = gvRow.RowIndex
            LblResourceEntryattachPath = TryCast(gvRow.Cells(0).FindControl("LblResourceEntryattachPath"), Label)
            'lnkFileAttachments = TryCast(gvRow.Cells(0).FindControl("lnkFileAttachments"), LinkButton)
            Dim filename As String = Path.GetFileName(LblResourceEntryattachPath.Text)
            Dim Fpath1 As String = LblResourceEntryattachPath.Text.Remove(0, 1)
            Dim pathArr() As String = Fpath1.Split("/")
            Dim Mainfolder As String = pathArr(1) 'Resources
            Dim Subfolder As String = pathArr(2) '9 foldername
            Dim fileN As String = pathArr(3)

            Dim BYTES() As Byte = File.ReadAllBytes(PhyPath + pathArr(2) + "\" + filename)
            HttpContext.Current.Response.Clear()
            Response.ContentType = ContentType
            Response.AddHeader("content-disposition", "attachment; filename=" & filename)

            Response.CacheControl = "No-cache"
            Response.BinaryWrite(BYTES)
            Response.Flush()
            Response.SuppressContent = True
            HttpContext.Current.ApplicationInstance.CompleteRequest()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        '------------------------------------------------------------------------------------------------------------

    End Sub


    Protected Sub ImgResourceEntry_Click(sender As Object, e As ImageClickEventArgs)
        Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("Resources").ToString()
        Dim LblResourceEntrypath As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent.Parent.Parent,  _
                                          GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        'lnkFileAttachments = TryCast(gvRow.Cells(0).FindControl("lnkFileAttachments"), LinkButton)
        LblResourceEntrypath = TryCast(gvRow.Cells(0).FindControl("LblResourceEntrypath"), Label)
        ' Dim attachfilename1 As String = Path.GetFileName(LblResourceEntrypath.Text)

        Dim filename As String = Path.GetFileName(LblResourceEntrypath.Text)
        Dim Fpath1 As String = LblResourceEntrypath.Text.Remove(0, 1)
        Dim pathArr() As String = Fpath1.Split("/")
        Dim Mainfolder As String = pathArr(1) 'Resources
        Dim Subfolder As String = pathArr(2) '9 foldername
        Dim fileN As String = pathArr(3)

        Dim BYTES() As Byte = File.ReadAllBytes(PhyPath + pathArr(2) + "\" + filename)
        HttpContext.Current.Response.Clear()
        Response.ContentType = ContentType
        Response.AddHeader("content-disposition", "attachment; filename=" & filename)

        Response.CacheControl = "No-cache"
        Response.BinaryWrite(BYTES)
        Response.Flush()
        Response.SuppressContent = True
        HttpContext.Current.ApplicationInstance.CompleteRequest()


    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged
        Call BinGrid()
    End Sub

    
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Call ClearData()
        btnSave.Text = "Save"
    End Sub

 

    Protected Sub gvResourcesEntry_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            gvResourcesEntry.PageIndex = e.NewPageIndex
            BinGrid()
        Catch ex As Exception

        End Try
    End Sub
End Class
