﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comPlainTextView.aspx.vb" EnableTheming="false" Inherits="masscom_TabPages_comPlainTextView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="template" runat="server" >
    
    </div>
    <asp:GridView ID="GrdAttachment" runat="server" AutoGenerateColumns="false" 
        OnRowCommand="GrdAttachment_RowCommand" ShowHeader="false" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" 
                        CommandName="select" Text=' <%# Eval("Name") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Size (Bytes)">
                <ItemTemplate>
                    <div align="right">
                        -(Size
                                                    <%#Eval("length")%>
                                                    Bytes)</div>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
    </form>
</body>
</html>
