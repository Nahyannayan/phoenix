﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class masscom_TabPages_comPlainTextView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim temp_id As String = Request.QueryString("temp_id")
            ShowTemplate(temp_id)
            BindAttachments(temp_id)
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindAttachments(ByVal temp_id As String)
        Dim serverpath As String = ConfigurationManager.AppSettings("EmailAttachments").ToString()
        Dim d As New DirectoryInfo(serverpath + temp_id + "/Attachments/")
        If d.Exists() Then
            GrdAttachment.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            GrdAttachment.DataBind()
        End If

        For Each arow As GridViewRow In GrdAttachment.Rows
            Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
            lnkattachment.CommandArgument = serverpath + temp_id + "/Attachments/" + lnkattachment.Text.Trim()
        Next

    End Sub

    Public Sub ShowTemplate(ByVal temp_id As String)

        Try
            If temp_id <> "" Then

                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

                Dim str_query = "select EML_BODY,EML_NEWS_LETTER,EML_NEWS_LETTER_FILE_NAME from COM_MANAGE_EMAIL where EML_ID='" & temp_id & "'"

                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("EML_NEWS_LETTER") = "false" Then
                        template.InnerHtml = ds.Tables(0).Rows(0).Item("EML_BODY")
                    Else
                        Dim serverpath As String = ConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString()
                        Dim filename As String = ds.Tables(0).Rows(0).Item("EML_NEWS_LETTER_FILE_NAME").ToString()

                        Dim path = serverpath + temp_id + "/News Letters/" + filename
                        Response.Redirect(path)
                    End If

                End If
            End If
        Catch ex As Exception
            template.InnerHtml = "Error :" & ex.Message
        End Try



    End Sub

    Public Shared Function ScreenScrapeHtml(ByVal url As String) As String
        On Error Resume Next
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream(), System.Text.Encoding.Default)
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml

    Protected Sub GrdAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAttachment.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        End If

    End Sub
End Class
