<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="comSurAnstypeList.aspx.vb" Inherits="masscom_comAnstypeList" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <table id="Table1" runat="server" align="center" border="1" bordercolor="#1b80b6"
        cellpadding="5" cellspacing="0">
        <tr class="subheader_img">
            <td align="left" colspan="18" style="height: 25px" valign="middle">
                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                    Survey Question Type List</span></font></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="3" style="height: 14px; width: 489px;" valign="top">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" colspan="3" valign="top">
                <table id="Table2" runat="server" align="center" border="0" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left" colspan="3" valign="top">
                            <asp:LinkButton id="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                            <asp:GridView id="gvTypeList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                BorderStyle="None" CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" Width="462px">
                                <emptydatarowstyle wrap="False" />
                                <columns>
<asp:TemplateField Visible="False" HeaderText="grd_id">
<ItemStyle HorizontalAlign="Left"></ItemStyle>
<ItemTemplate>
<asp:Label id="lblInitId" runat="server" Text='<%# Bind("ANS_TYPEID") %>' __designer:wfdid="w4"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="TypeCode"><ItemTemplate>
<asp:Label id="lblTyepcode" runat="server" __designer:wfdid="w5" text='<%# Bind("ANS_CODE") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Type"><HeaderTemplate>
<TABLE style="WIDTH: 66%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="WIDTH: 31px"><DIV id="Div1" class="chromestyle"><UL><LI><A href="#" rel="dropmenu1"></A><IMG id="mnu_1_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /> </LI></UL></DIV></TD><TD style="WIDTH: 129px">Type<asp:TextBox id="txtType" runat="server" Width="100px" __designer:wfdid="w12"></asp:TextBox></TD><TD></TD><TD style="WIDTH: 55px" vAlign=middle><asp:ImageButton id="btnPse_Search" onclick="btnPse_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w13"></asp:ImageButton></TD></TR></TBODY></TABLE>
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="lblType" runat="server" __designer:wfdid="w85" text='<%# Bind("ANS_TYPE") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:ButtonField HeaderText="View" Text="View" CommandName="View">
<ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
</asp:ButtonField>
</columns>
                                <rowstyle cssclass="griditem" height="17px" wrap="False" />
                                <editrowstyle wrap="False" />
                                <selectedrowstyle cssclass="Green" wrap="False" />
                                <headerstyle cssclass="gridheader_pop" height="15px" wrap="False" />
                                <alternatingrowstyle cssclass="griditem_alternative" wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /></td>
        </tr>
    </table>
</asp:Content>

