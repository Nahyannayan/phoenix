
Partial Class masscom_comAssignGroups
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim menucode As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If menucode = "M000030" Then
                lblTopTitle.Text = "Groups"
            Else
                lblTopTitle.Text = "Groups"
            End If

        End If
    End Sub

    Protected Sub hdn_msg_ValueChanged(sender As Object, e As EventArgs) Handles hdn_msg.ValueChanged
        Dim Encr_decrData As New Encryption64
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Encr_decrData.Encrypt("none") ''& Request.QueryString("datamode").ToString()
        Response.Redirect("comListGroups.aspx" & mInfo)
        hdn_msg.Value = ""
    End Sub
End Class
