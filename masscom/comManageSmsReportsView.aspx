<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comManageSmsReportsView.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="masscom_comManageSmsReportsView" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr class="title">
            <td align="left" >
            MESSAGE SEND STATUS
            </td>
        </tr>
    </table>


<table border="0" cellpadding="0" class="matters" cellspacing="0">
            <tr >
                <td align="left"> 
<div>
<br />
 <table border="1" bordercolor="#1b80b6" cellpadding="5" 

cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Message Send Status</td>
                        </tr>
                        <tr>
                            <td >

        <asp:GridView ID="Grdstatus" AutoGenerateColumns="false" EmptyDataText="No Records Found from your selected search" width="650px" runat="server" AllowPaging="True" PageSize="20">
                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                     <EmptyDataRowStyle Wrap="False" />
        <Columns>
<asp:TemplateField HeaderText="Student No"><HeaderTemplate>
           <table>
                                      <tr>
                                       <td align="center">
                                      Student Id</td>
                                    </tr>
                                    <tr>
                                 <td >
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                   <td >
                                   <div id="Div1" class="chromestyle">
                                    <ul>
                                   <li><a href="#" rel="dropmenu1"></a>
                                   <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                      </li>
                                    </ul>
                                   </div>
                                    </td>
                                      <td >
                                <asp:TextBox ID="txtSearch1" runat="server" Width="60px"></asp:TextBox></td>
                                                              <td >
                                   <td  valign="middle">
                                   <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearch_Click" /></td>
                                   </tr>
                                  </table>
                                  </td>
                                  </tr>
                                 </table>          
           
           
            
</HeaderTemplate>
<ItemTemplate>
        <asp:HiddenField ID="Hiddenstuid" Value='<%#Eval("STU_NO")%>' runat="server" />
        <%#Eval("STU_NO")%>
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Name"><HeaderTemplate>
           <table>
                                      <tr>
                                       <td align="center">
                                      Student Name</td>
                                    </tr>
                                    <tr>
                                 <td >
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                   <td >
                                   <div id="Div2" class="chromestyle">
                                    <ul>
                                   <li><a href="#" rel="dropmenu2"></a>
                                   <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                      </li>
                                    </ul>
                                   </div>
                                    </td>
                                      <td >
                                <asp:TextBox ID="txtSearch2" runat="server" Width="60px"></asp:TextBox></td>
                                                              <td >
                                   <td  valign="middle">
                                   <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearch_Click" /></td>
                                   </tr>
                                  </table>
                                  </td>
                                  </tr>
                                 </table>          
           
           
            
</HeaderTemplate>
<ItemTemplate>
         <%#Eval("STU_FIRSTNAME")%> <%#Eval("STU_MIDNAME")%> <%#Eval("STU_LASTNAME")%>
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Parent Name"><HeaderTemplate>
           <table>
                                      <tr>
                                       <td align="center">
                                      Parent Name</td>
                                    </tr>
                                    <tr>
                                 <td >
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                   <td >
                                   <div id="Div3" class="chromestyle">
                                    <ul>
                                   <li><a href="#" rel="dropmenu3"></a>
                                   <img id="mnu_3_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                      </li>
                                    </ul>
                                   </div>
                                    </td>
                                      <td >
                                <asp:TextBox ID="txtSearch3" runat="server" Width="60px"></asp:TextBox></td>
                                                              <td >
                                   <td  valign="middle">
                                   <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearch_Click" /></td>
                                   </tr>
                                  </table>
                                  </td>
                                  </tr>
                                 </table>          
           
           
            
</HeaderTemplate>
<ItemTemplate>
         <%#Eval("parent_name")%> 
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Mobile"><HeaderTemplate>
           <table>
                                      <tr>
                                       <td align="center">
                                      Mobile</td>
                                    </tr>
                                    <tr>
                                 <td >
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                   <td >
                                   <div id="Div5" class="chromestyle">
                                    <ul>
                                   <li><a href="#" rel="dropmenu5"></a>
                                   <img id="mnu_5_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                      </li>
                                    </ul>
                                   </div>
                                    </td>
                                      <td >
                                <asp:TextBox ID="txtSearch5" runat="server" Width="60px"></asp:TextBox></td>
                                                              <td >
                                   <td  valign="middle">
                                   <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearch_Click" /></td>
                                   </tr>
                                  </table>
                                  </td>
                                  </tr>
                                 </table>          
           
           
            
</HeaderTemplate>
<ItemTemplate>
         <%#Eval("ParentMobile")%> 
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Company Name"><HeaderTemplate>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td >
                    Company Name</td>
            </tr>
            <tr>
                <td >
                    <asp:DropDownList ID="DropSearch1"  AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged" runat="server">
        </asp:DropDownList></td>
            </tr>
        </table>
      
        
</HeaderTemplate>
<ItemTemplate>
         <%#Eval("comp_name")%> 
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Status"><ItemTemplate>

<asp:LinkButton ID="LinkView" Text="View" OnClientClick="javascript:return false;" runat="server"></asp:LinkButton>
 <asp:Panel id="Show" BorderColor="Black" BackColor="#ffff99"  runat="server" Height="50px" Width="200px">
<asp:Label id="lberror" text='<%#Eval("log_status")%>' runat="server"></asp:Label>
            </asp:Panel>
            <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" TargetControlID="LinkView" PopupControlID="Show" PopupPosition="Left" />


 
</ItemTemplate>
</asp:TemplateField>
</Columns>
                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                     <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                     <EditRowStyle Wrap="False" />
                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
        </asp:GridView>
        
 </td>
                    </tr>
               </table>
        <asp:HiddenField ID="Hiddensmsid" runat="server" />
        <asp:HiddenField ID="Hiddengroupid" runat="server" />
    
    </div>
    <input id="h_Selected_menu_1" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_2" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_3" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_4" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_5" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
</td>
            </tr>
        </table>
<script type="text/javascript">

    cssdropdown.startchrome("Div1")
     cssdropdown.startchrome("Div2")
      cssdropdown.startchrome("Div3")
     cssdropdown.startchrome("Div4")
      cssdropdown.startchrome("Div5")
    

    </script>
    <div id="dropmenu1" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test1('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test1('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test1('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test1('NSW');">
                        <img alt="Not Start With" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test1('EW');">
                            <img alt="Ends With" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test1('NEW');">
                                <img alt="Not Ends With" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>    
     <div id="dropmenu2" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test2('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test2('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test2('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test2('NSW');">
                        <img alt="Not Start With" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test2('EW');">
                            <img alt="Ends With" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test2('NEW');">
                                <img alt="Not Ends With" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>  
    <div id="dropmenu3" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test3('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test3('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test3('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test3('NSW');">
                        <img alt="Not Start With" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test3('EW');">
                            <img alt="Ends With" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test3('NEW');">
                                <img alt="Not Ends With" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>     
    <div id="dropmenu4" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test4('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test4('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test4('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test4('NSW');">
                        <img alt="Not Start With" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test4('EW');">
                            <img alt="Ends With" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test4('NEW');">
                                <img alt="Not Ends With" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>     
    
     <div id="dropmenu5" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test5('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test5('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test5('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test5('NSW');">
                        <img alt="Not Start With" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test5('EW');">
                            <img alt="Ends With" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test5('NEW');">
                                <img alt="Not Ends With" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>  
    <script language="javascript" type="text/javascript">
       function test1(val)
                    {
                    var path;
                    if (val=='LI')
                    {
                    path='../Images/operations/like.gif';
                    }else if (val=='NLI')
                    {
                    path='../Images/operations/notlike.gif';
                    }else if (val=='SW')
                    {
                    path='../Images/operations/startswith.gif';
                    }else if (val=='NSW')
                    {
                    path='../Images/operations/notstartwith.gif';
                    }else if (val=='EW')
                    {
                    path='../Images/operations/endswith.gif';
                    }else if (val=='NEW')
                    {
                    path='../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid1()%>").src = path;
                    document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                   
                    }
                    function test2(val)
                    {
                    var path;
                    if (val=='LI')
                    {
                    path='../Images/operations/like.gif';
                    }else if (val=='NLI')
                    {
                    path='../Images/operations/notlike.gif';
                    }else if (val=='SW')
                    {
                    path='../Images/operations/startswith.gif';
                    }else if (val=='NSW')
                    {
                    path='../Images/operations/notstartwith.gif';
                    }else if (val=='EW')
                    {
                    path='../Images/operations/endswith.gif';
                    }else if (val=='NEW')
                    {
                    path='../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid2()%>").src = path;
                    document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                   
                    }
                     function test3(val)
                    {
                    var path;
                    if (val=='LI')
                    {
                    path='../Images/operations/like.gif';
                    }else if (val=='NLI')
                    {
                    path='../Images/operations/notlike.gif';
                    }else if (val=='SW')
                    {
                    path='../Images/operations/startswith.gif';
                    }else if (val=='NSW')
                    {
                    path='../Images/operations/notstartwith.gif';
                    }else if (val=='EW')
                    {
                    path='../Images/operations/endswith.gif';
                    }else if (val=='NEW')
                    {
                    path='../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid3()%>").src = path;
                    document.getElementById("<%=h_selected_menu_3.ClientID %>").value=val+'__'+path;
                   
                    }
                     function test4(val)
                    {
                    var path;
                    if (val=='LI')
                    {
                    path='../Images/operations/like.gif';
                    }else if (val=='NLI')
                    {
                    path='../Images/operations/notlike.gif';
                    }else if (val=='SW')
                    {
                    path='../Images/operations/startswith.gif';
                    }else if (val=='NSW')
                    {
                    path='../Images/operations/notstartwith.gif';
                    }else if (val=='EW')
                    {
                    path='../Images/operations/endswith.gif';
                    }else if (val=='NEW')
                    {
                    path='../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid4()%>").src = path;
                    document.getElementById("<%=h_selected_menu_4.ClientID %>").value=val+'__'+path;
                   
                    }
                     function test5(val)
                    {
                    var path;
                    if (val=='LI')
                    {
                    path='../Images/operations/like.gif';
                    }else if (val=='NLI')
                    {
                    path='../Images/operations/notlike.gif';
                    }else if (val=='SW')
                    {
                    path='../Images/operations/startswith.gif';
                    }else if (val=='NSW')
                    {
                    path='../Images/operations/notstartwith.gif';
                    }else if (val=='EW')
                    {
                    path='../Images/operations/endswith.gif';
                    }else if (val=='NEW')
                    {
                    path='../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid5()%>").src = path;
                    document.getElementById("<%=h_selected_menu_5.ClientID %>").value=val+'__'+path;
                   
                    }
       
       </script>   


</asp:Content>
