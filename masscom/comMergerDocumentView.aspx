﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comMergerDocumentView.aspx.vb" Inherits="masscom_comMergerDocumentView" %>

<%@ Register src="UserControls/comMergerDocumentView.ascx" tagname="comMergerDocumentView" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    
    <!-- Bootstrap core CSS-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

    <!-- Bootstrap header files ends here -->

   <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:comMergerDocumentView ID="comMergerDocumentView1" runat="server" />
    
    </div>
    </form>
</body>
</html>
