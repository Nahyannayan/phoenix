<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comViewSmsText.aspx.vb" Inherits="masscom_comViewSmsText" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>SMS Text</title>


    <!-- Bootstrap core CSS-->
    <script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>


    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />


    <script type="text/javascript" language="JavaScript">

        function opneWindow() {

            var sFeatures;
            sFeatures = "dialogWidth: 900px; ";
            sFeatures += "dialogHeight: 700px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var strOpen = "comMergerDocument.aspx?Type=SMS"

            var result;
            //result = window.showModalDialog(strOpen, "", sFeatures);
            //window.location.reload(true);
            return ShowWindowWithClose(strOpen, 'search', '90%', '85%')
            return false;

        }

        function textCounter() {
            var field = document.getElementById("txtsmstext");
            var countfield = document.getElementById("txtcount");
            var maxlimit = document.getElementById("HiddenCount").value;

            var v = document.getElementById('<%= Me.hfMenuCode.ClientID%>').value;

            if (v == 'M0000100')
            { return false };

            if (field.value.length > maxlimit) // if too long...trim it!
                field.value = field.value.substring(0, maxlimit);
                // otherwise, update 'characters left' counter
            else
                countfield.innerHTML = maxlimit - field.value.length;

            document.getElementById("<%=txttaken.ClientID %>").innerHTML = field.value.length
        }


    </script>
    <script type="text/javascript">
        function dec2hex(textString) {
            return (textString + 0).toString(16).toUpperCase();
        }

        function dec2hex4(textString) {
            var hexequiv = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
            return hexequiv[(textString >> 12) & 0xF] + hexequiv[(textString >> 8) & 0xF] + hexequiv[(textString >> 4) & 0xF] + hexequiv[textString & 0xF];
        }
        function convertCP2UTF16() {
            var outputString = "";

            var textString = convertChar2CP(document.getElementById("txtsmstext").value);

            textString = textString.replace(/^\s+/, '');



            if (textString.length == 0) { return ""; }
            textString = textString.replace(/\s+/g, ' ');

            var listArray = textString.split(' ');



            for (var i = 0; i < listArray.length; i++) {
                var n = parseInt(listArray[i], 16);



                if (i > 0) { outputString += ' '; }

                if (n <= 0xFFFF) {
                    outputString += dec2hex4(n);

                } else if (n <= 0x10FFFF) {
                    n -= 0x10000
                    outputString += dec2hex4(0xD800 | (n >> 10)) + ' ' + dec2hex4(0xDC00 | (n & 0x3FF));
                } else {
                    outputString += 'HAI BOSS' + dec2hex(n) + '!';
                }
            }
            document.getElementById("HiddenUTF16").value = outputString
            return (outputString);
        }
        function convertChar2CP(textString) {
            var outputString = "";
            var haut = 0;
            var n = 0;
            for (var i = 0; i < textString.length; i++) {
                var b = textString.charCodeAt(i);  // alert('b:'+dec2hex(b));
                if (b < 0 || b > 0xFFFF) {
                    outputString += 'HAI BOSS' + dec2hex(b) + '!';
                }
                if (haut != 0) {
                    if (0xDC00 <= b && b <= 0xDFFF) {
                        outputString += dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00)) + ' ';
                        haut = 0;
                        continue;
                    } else {
                        outputString += 'HAI BOSS' + dec2hex(haut) + '!';
                        haut = 0;
                    }
                }
                if (0xD800 <= b && b <= 0xDBFF) {
                    haut = b;
                } else {
                    outputString += dec2hex(b) + ' ';
                }
            }
            return (outputString.replace(/ $/, ''));
        }


        function point() {

            var tempval;
            tempval = document.getElementById('<%=txtsmstext.ClientID %>').value

            tempval = tempval + document.getElementById('<%=ddfields.ClientID %>').value
            document.getElementById('<%=txtsmstext.ClientID %>').value = tempval
            textCounter();

        }

    </script>

    <script language="javascript" type="text/javascript">
        function SetCloseFrame() {
            //alert('close1');
            parent.setCloseFrame();
            return false;
        }
    </script>

    <script language="javascript" type="text/javascript">
        function SetCloseValuetoParent(msg) {
            //alert('close1');
            parent.setCloseEditTemplateValue(msg);
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <center>
            <%--   <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>--%>
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left">
                        <div>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="title-bg-lite" colspan="2">Message Text</td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:CheckBox ID="CheckArabic" runat="server" AutoPostBack="True" Text="Arabic" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtsmstext" runat="server" TextMode="MultiLine" onKeyDown="textCounter();" onKeyUp="textCounter();" EnableTheming="False"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr id="trCharsCount" runat="server" visible="true">
                                    <td align="left">
                                        <%--</td>--%>

                                        <%--<td align="left">--%>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <span>Characters Used - </span>
                                                            <asp:Label ID="txttaken" runat="server" ReadOnly="True" CssClass="field-label" placeholder="0"></asp:Label>
                                                            Out Of 
                                                                 <asp:Label ID="txtcount" runat="server" ReadOnly="True" CssClass="field-label"></asp:Label>
                                                            <%--<asp:TextBox ID="txttaken" runat="server" ReadOnly="True"></asp:TextBox>--%>

                                                        </td>
                                                        <td>
                                                            <%-- <asp:TextBox ID="txtcount" runat="server" ReadOnly="True"></asp:TextBox>--%>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">

                                        <div align="left">

                                            <asp:LinkButton ID="Linkmerge" runat="server" CausesValidation="false" OnClientClick="opneWindow(); return false;">Upload Merge Document</asp:LinkButton>

                                        </div>
                                        <br />
                                        <br />
                                        <div id="TRDynamic" runat="server">
                                            <asp:LinkButton ID="LinkDynamic" runat="server" OnClientClick="javascript:return false;">Dynamic Text</asp:LinkButton>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="Panel1T" runat="server">
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left" width="20%">
                                                                    <span class="field-label">Template</span>
                                                                </td>

                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddtemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddtemplate_SelectedIndexChanged">
                                                                    </asp:DropDownList></td>
                                                                <td align="left" width="20%">
                                                                    <span class="field-label">Fields</span></td>

                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddfields" runat="server">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Button ID="btninsert" runat="server" CssClass="button" OnClientClick="point(); return false;"
                                                                        Text="Insert" /></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>

                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkDynamic"
                                                        Collapsed="true" CollapsedSize="0" CollapsedText="Dynamic Text" ExpandControlID="LinkDynamic"
                                                        ExpandedSize="100" ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1T"
                                                        TextLabelID="LinkDynamic">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </td>
                                </tr>

                                <%--            <tr  >
                <td align="right" style="width: 100px">
                    User Name</td>
                <td align="center" style="width: 2px">
                    :</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtusername" runat="server" Width="121px"></asp:TextBox></td>
            </tr>
            <tr >
                <td align="right" style="width: 100px">
                    Password</td>
                <td align="center" style="width: 2px">
                    :</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtpassword" runat="server" Width="121px"></asp:TextBox></td>
            </tr>--%>
                                <tr id="from_id" runat="server">
                                    <td align="left">
                                        <span class="field-label">From</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtfrom" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <center>
                                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" />
                                                <asp:Button ID="btnedit" runat="server" Text="Edit" CausesValidation="False" CssClass="button" />
                                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="button" />
                                                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                                            </asp:Panel>
                                            <asp:Panel ID="Panel2" runat="server" Width="100%">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="convertCP2UTF16 ()" CssClass="button" />
                                                <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="button" />
                                                <asp:Button ID="btnecancel" runat="server" Text="Cancel" CssClass="button" />
                                            </asp:Panel>
                                            &nbsp;
                                        </center>
                                        <center>
                                            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>&nbsp;</center>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <input id="HiddenCount" runat="server" type="hidden" />
                        <input id="Hiddensmstext" runat="server" type="hidden" /></td>
                </tr>
            </table>

        </center>

        <asp:HiddenField ID="Hiddenmessageid" runat="server" />
        <asp:HiddenField ID="HiddenUTF16" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />

        <asp:HiddenField ID="hfMenuCode" runat="server" />
        <asp:HiddenField ID="hfEditCode" runat="server" />

        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtsmstext"
            Display="None" ErrorMessage="Please enter message text" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:RequiredFieldValidator
                ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtfrom" Display="None"
                ErrorMessage="Please enter From details" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" DisplayMode="List" ShowMessageBox="True"
                    ShowSummary="False" />
         <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                margin: 0,
                padding: 0,
                showCloseButton: false,

                //maxHeight: 600,
                //fitToView: true,
                //padding: 6,
                //width: w,
                //height: h,
                //autoSize: false,
                //openEffect: 'none',
                //showLoading: true,
                //closeClick: true,
                //closeEffect: 'fade',
                //'closeBtn': true,
                //afterLoad: function () {
                //    this.title = '';//ShowTitle(pageTitle);
                //},
                //helpers: {
                //    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                //    title: { type: 'inside' }
                //},
                //onComplete: function () {
                //    $("#fancybox-wrap").css({ 'top': '90px' });

                //},
                //onCleanup: function () {
                //    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                //    if (hfPostBack == "Y")
                //        window.location.reload(true);
                //}
            });

            return false;
        }
    </script>

    </form>
</body>
</html>
