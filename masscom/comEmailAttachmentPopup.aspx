﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comEmailAttachmentPopup.aspx.vb" Inherits="masscom_comEmailAttachmentPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

    <!-- Bootstrap header files ends here -->
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
             <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
            <table width="100%">
                <tr>
                    <td class="title-bg-lite">
                        <asp:Label ID="lbltitle" runat="server" Text="Attachments"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GrdAttachment" runat="server" AutoGenerateColumns="false" OnRowCommand="GrdTextAttachment_RowCommand"
                            ShowHeader="false" Width="100%" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" CommandName="select"
                                            Text=' <%# Eval("Name") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Size (Bytes)">
                                    <ItemTemplate>
                                        <div align="right">
                                            -(Size
                                                    <%#Eval("length")%>
                                                    Bytes)
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Remove">Delete</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this attachment?"
                                            TargetControlID="lnkdelete">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="title-bg-lite">
                        <asp:Label ID="Label1" runat="server" Text="Banner"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GrdBanner" runat="server" AutoGenerateColumns="false" OnRowCommand="GrdBanner_RowCommand"
                            ShowHeader="false" Width="100%" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" CommandName="select"
                                            Text=' <%# Eval("Name") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Size (Bytes)">
                                    <ItemTemplate>
                                        <div align="right">
                                            -(Size
                                                    <%#Eval("length")%>
                                                    Bytes)
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Remove">Delete</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this attachment?"
                                            TargetControlID="lnkdelete">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="title-bg-lite">
                        <asp:Label ID="Label2" runat="server" Text="Thumbnail"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridThumbnail" runat="server" AutoGenerateColumns="false" OnRowCommand="Thumbnail_RowCommand"
                            ShowHeader="false" Width="100%" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" CommandName="select"
                                            Text=' <%# Eval("Name") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Size (Bytes)">
                                    <ItemTemplate>
                                        <div align="right">
                                            -(Size
                                                    <%#Eval("length")%>
                                                    Bytes)
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Remove">Delete</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this attachment?"
                                            TargetControlID="lnkdelete">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
