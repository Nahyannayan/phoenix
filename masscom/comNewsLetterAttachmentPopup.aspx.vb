﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_comNewsLetterAttachmentPopup
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ViewState("templateid") = Request.QueryString("templateid")
            Dim val As String = ViewState("templateid")
            'Dim Filename As String = DirectCast(row.FindControl("HiddenFieldFileName"), HiddenField).Value

            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
            'Dim lnknewsletter As LinkButton = DirectCast(row.FindControl("lnknewsletter"), LinkButton)

            'lnknewsletter.CommandArgument = val + "/News Letters/" & Filename
            'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnknewsletter)
            'Dim GrdAttachment As GridView = DirectCast(row.FindControl("GrdAttachment"), GridView)
            Dim d As New DirectoryInfo(serverpath + val + "/Attachments/")

            If d.Exists Then
                ' If (d.GetFiles("*.*", SearchOption.TopDirectoryOnly).Length > 0) Then
                GrdAttachment.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                GrdAttachment.DataBind()
                'End If
            End If


            'Dim lnkdeletedir As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
            'lnkdeletedir.CommandArgument = val

            For Each arow As GridViewRow In GrdAttachment.Rows
                Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
                Dim lnkVal As HiddenField = DirectCast(arow.FindControl("lnkVal"), HiddenField)
                lnkVal.Value = val
                'lnkattachment.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
                'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkattachment)
                Dim lnkdelete As LinkButton = DirectCast(arow.FindControl("lnkdelete"), LinkButton)
                lnkdelete.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
            Next
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub
    Protected Sub GrdAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAttachment.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If
        If e.CommandName = "Remove" Then
            Dim f As New FileInfo(e.CommandArgument)
            If f.Exists Then
                f.Delete()
            End If
            'BindNewsletters()
            lblmessage.Text = "Attachment Deleted Successfully"

        End If
    End Sub
    Protected Sub lnkAtt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbtnEm As LinkButton = DirectCast(sender, LinkButton)
        Try


            Dim lnkVal As New HiddenField()
            lnkVal = TryCast(sender.FindControl("lnkVal"), HiddenField)
            Dim val As String = lnkVal.Value



            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

            Dim Path As String = serverpath + val + "/Attachments/" + lbtnEm.Text.Trim()

            Dim bytes() As Byte = File.ReadAllBytes(Path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(Path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message.ToString + ex.StackTrace, "lnkAtt")
        End Try

    End Sub
End Class
