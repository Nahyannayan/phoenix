Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Reflection
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class masscom_UserControls_comExportMembers
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim EncDec As New Encryption64
            Dim Groupid As String = EncDec.Decrypt(Request.QueryString("Groupid").Replace(" ", "+"))
            HiddenGroupid.Value = Groupid
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExport)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Export(HiddenGroupid.Value, RadioData.SelectedValue)
    End Sub

    Public Sub Export(ByVal groupid As String, ByVal DataOption As String)
        ''DataOption M-Mobile , E-Email 
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query = ""
        Dim ds As DataSet
        sql_query = "select CGR_GRP_TYPE,CGR_CONDITION,CGR_TYPE,CGR_REMOVE_EMAIL_IDS,CGR_REMOVE_SMS_IDS from COM_GROUPS_M where CGR_ID='" & groupid & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        Dim condition As String = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
        Dim grptype As String = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()

        Dim dt As New DataTable()

        If grptype = "GRP" Then  ''Group 

            If DataOption = "M" Then

               '' If Session("sbsuid") <> "315888" Then
               ''     condition = " select isnull(uniqueid,'') as ID , Name, isnull('971'+ right(replace(Mobile,'-',''),9),'') as MobileNo   from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
               '' Else
                ''    condition = " select isnull(uniqueid,'') as ID , Name, isnull('012'+ right(replace(Mobile,'-',''),7),'') as MobileNo   from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                ''End If
				
			   If Session("BSU_COUNTRY_ID") = "172" Then
                    condition = " select isnull(uniqueid,'') as ID , Name, isnull('971'+ right(replace(Mobile,'-',''),9),'') as MobileNo   from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
               Else If Session("BSU_COUNTRY_ID") = "6" Then
                    condition = " select isnull(uniqueid,'') as ID , Name, isnull('91'+ right(replace(Mobile,'-',''),10),'') as MobileNo   from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                    condition = " select isnull(uniqueid,'') as ID , Name, isnull('002'+ right(replace(Mobile,'-',''),11),'') as MobileNo   from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                    condition = " select isnull(uniqueid,'') as ID , Name, isnull('91'+ right(replace(Mobile,'-',''),9),'') as MobileNo   from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                
				Else
                   condition = " select isnull(uniqueid,'') as ID , Name, Mobile as MobileNo   from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                End If
            Else
                condition = " select isnull(uniqueid,'') as ID , Name, isnull(Email,'') as EmailID   from ( " & condition & " ) a  where STU_bRCVMAIL='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString() & " "
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, condition)


        End If

        If grptype = "AON" Then ''Add On 

            If DataOption = "M" Then

                ''If Session("sbsuid") <> "315888" Then
                ''    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                ''    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('971'+ right(replace(Mobile,'-',''),9),'') as MobileNo  from ( " & condition & " ) a  "

                ''Else
                ''    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                ''    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('012'+ right(replace(Mobile,'-',''),7),'') as MobileNo  from ( " & condition & " ) a  "

                ''End If
				
			    If Session("BSU_COUNTRY_ID") = "172" Then
                    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('971'+ right(replace(Mobile,'-',''),9),'') as MobileNo  from ( " & condition & " ) a  "
                Else If Session("BSU_COUNTRY_ID") = "6" Then
				    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('91'+ right(replace(Mobile,'-',''),10),'') as MobileNo  from ( " & condition & " ) a  "

                ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('002'+ right(replace(Mobile,'-',''),11),'') as MobileNo  from ( " & condition & " ) a  "

                ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('60'+ right(replace(Mobile,'-',''),9),'') as MobileNo  from ( " & condition & " ) a  "
                Else
				    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, Mobile as MobileNo  from ( " & condition & " ) a  "
                End If
            Else

                condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and STU_bRCVMAIL='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString()
                sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull(Email,'') as EmailID  from ( " & condition & " ) a  "

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)


        End If





        If DataOption = "M" Then
            dt.Columns.Add("ID", GetType(String))
            dt.Columns.Add("NAME", GetType(String))
            dt.Columns.Add("MOBILENO", GetType(String))

            Dim i = 0

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = ds.Tables(0).Rows(i).Item("ID").ToString()
                dr("NAME") = ds.Tables(0).Rows(i).Item("Name").ToString()
                dr("MOBILENO") = ds.Tables(0).Rows(i).Item("MobileNo").ToString()
                dt.Rows.Add(dr)
            Next
        Else
            dt.Columns.Add("ID", GetType(String))
            dt.Columns.Add("NAME", GetType(String))
            dt.Columns.Add("EMAILID", GetType(String))

            Dim i = 0

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim dr As DataRow = dt.NewRow()
                dr("ID") = ds.Tables(0).Rows(i).Item("ID").ToString()
                dr("NAME") = ds.Tables(0).Rows(i).Item("Name").ToString()
                dr("EMAILID") = ds.Tables(0).Rows(i).Item("EmailID").ToString()
                dt.Rows.Add(dr)
            Next
        End If



        ' Create excel file.


        SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()

        Dim pathSave As String
        pathSave = Session("EmployeeId") + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

        ef.SaveXls(cvVirtualPath & "GroupMembers\" + pathSave)

        Dim path = cvVirtualPath & "\GroupMembers\" + pathSave

        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()


    End Sub

   

End Class
