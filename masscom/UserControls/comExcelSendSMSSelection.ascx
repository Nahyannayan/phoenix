﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSendSMSSelection.ascx.vb" Inherits="masscom_UserControls_comExcelSendSMSSelection" %>

<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->

<script type="text/javascript">
    function openPopup(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 1024px; ";
        sFeatures += "dialogHeight: 768px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
        strOpen += "&smsid=" + document.getElementById("<%=Hiddensmsid.ClientID %>").value;
        document.getElementById('div2').style.display = 'none';
        window.showModalDialog(strOpen, "", sFeatures);

    }

    function openPopup2(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 500px; ";
        sFeatures += "dialogHeight:  500px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
        strOpen += "&smsid=" + document.getElementById("<%=Hiddensmsid.ClientID %>").value;
        strOpen += "&method=0";
        document.getElementById('div2').style.display = 'none';
        window.showModalDialog(strOpen, "", sFeatures);



    }



    function openTemplate(tab) {


        var sFeatures;
        var strOpen;

        strOpen = 'comExcelSMSData.aspx'

        sFeatures = "dialogWidth: 1024px; ";
        sFeatures += "dialogHeight: 768px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?templateid=" + document.getElementById("<%=Hiddensmsid.ClientID %>").value;
        strOpen += "&Tab=" + tab + "&DataId=0";

        window.showModalDialog(strOpen, "", sFeatures);



    }

    function vali_Filetype() {

        var id_value = document.getElementById('<%= FileUpload1.ClientID %>').value;

        if (id_value != '') {
            var valid_extensions = /(.xls|.xlsx|.XLS|.XLSX)$/i;

            if (valid_extensions.test(id_value)) {

                return true;
            } else {
                alert('Please upload a document in one of the following formats:  .xls,.xlsx')
                document.getElementById('<%= FileUpload1.ClientID %>').focus()
                return false;
            }
        }

    }
</script>
<script language="javascript" type="text/javascript">
    function SetSelValuetoParent(id) {
        //alert(id);
        parent.SetSelValuetoParent(id);
        return false;
    }
</script>
<div>
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>



            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label><br />
            <uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
            <br />


            <br />
            <asp:Panel ID="Panel1" runat="server" Visible="True">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Upload Excel File 
                                      
                                <asp:LinkButton ID="LinkButton1" OnClientClick="openTemplate(2); return false;" runat="server" Visible="false">Get Previous Template(s)</asp:LinkButton>
                            <asp:LinkButton
                                ID="LinkButton2" runat="server" CausesValidation="False" Visible="false">Cancel</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Title</span></td>
                                    <td align="left" width="80%" colspan="2">
                                        <asp:TextBox ID="txttitle" runat="server"></asp:TextBox></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Type</span></td>
                                    <td align="left" width="30%">
                                        <asp:RadioButtonList ID="RadioType" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Text="Student" Value="STUDENT" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Staff" Value="STAFF"></asp:ListItem>
                                            <asp:ListItem Text="Others" Value="OTHERS"></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td align="left" width="50%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Excel File</span></td>
                                    <td align="left" width="30%">

                                        <asp:FileUpload ID="FileUpload1" onBlur="vali_Filetype()" runat="server" /></td>
                                    <td align="center" width="50%">
                                        <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button" /></td>
                                </tr>

                            </table>

                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Important Notes Regarding Uploading Excel Sheet</td>
                    </tr>
                    <tr>
                        <td>

                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="field-label">Field Names</span>
                                    </td>

                                    <td align="left" width="70%">
                                        <span class="field-value">ID,MobileNo,Name</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="field-label">Sheet Name</span>
                                    </td>

                                    <td align="left" width="70%">
                                        <span class="field-value">Sheet1</span></td>

                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </asp:Panel>



            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttitle"
                Display="None" ErrorMessage="Please Enter a Title" SetFocusOnError="True"></asp:RequiredFieldValidator>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUpload1"
                Display="None" ErrorMessage="Please upload excel file" SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
            <%--<asp:RegularExpressionValidator Style="position: static" ID="RegularExpressionValidator3"
    runat="server" SetFocusOnError="true" ErrorMessage="Please upload a Excel document (*.xls)."
    Display="None" ControlToValidate="FileUpload1" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.xls|.xlsx|.XLS|.XLSX)$"></asp:RegularExpressionValidator>--%>
            <br />

            <asp:HiddenField ID="Hiddensmsid" runat="server" />
            <asp:HiddenField ID="HiddenPathEncrypt" runat="server" />
            <asp:HiddenField ID="Hiddenlogid" runat="server" />
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />

            <input id="Hiddenpath" runat="server" value="00" type="hidden" />

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />

        </Triggers>
    </asp:UpdatePanel>
</div>
