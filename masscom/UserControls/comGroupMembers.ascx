<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comGroupMembers.ascx.vb"
    Inherits="masscom_UserControls_comGroupMembers" %>
<%@ Register Src="comExportMembers.ascx" TagName="comExportMembers" TagPrefix="uc1" %>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->


<div class="card mb-3">
    <div class="card-header letter-space">
        <i class="fa fa-email mr-3"></i>Group Members - Groups
    </div>
    <div class="card-body">
        <div class="table-responsive">

            <%--<table border="0" cellpadding="0" cellspacing="0" style="width: 700px">
    <tr class="title-bg-lite">
        <td align="left">
            GROUP MEMBERS -GROUPS</td>
    </tr>
</table>--%>
            <%--<br />--%>
            <div>
                <asp:HiddenField ID="HiddenGroupid" runat="server" />
                <asp:HiddenField ID="Hiddengroupcondition" runat="server" />
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Group Members
                <asp:Label ID="lblname" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="lbltotal" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GrdMembers" EmptyDataText="Query does not produce any results"
                                runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" ShowFooter="True"
                                OnPageIndexChanging="GrdMembers_PageIndexChanging" PageSize="20" OnPageIndexChanged="GrdMembers_PageIndexChanged"
                                OnRowCommand="GrdMembers_RowCommand"  CssClass="table table-bordered table-row">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Unique No">
                                        <HeaderTemplate>
                                            Unique No
                                            <br />
                                            <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddenuniqueid" Value='<%#Eval("uniqueid")%>' runat="server" />
                                            <%#Eval("uniqueid")%>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name
                                            <br />
                                            <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("Name")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Parent Name">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                               Parent Name
                                <br />
                                <asp:TextBox ID="Txt1" width="40px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                               
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>


<ItemTemplate>
         <%#Eval("parent_name")%> 
        
</ItemTemplate>
</asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Email">
                                        <HeaderTemplate>
                                            Email
                                            <br />
                                            <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("Email")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile">
                                        <HeaderTemplate>
                                            Mobile
                                            <br />
                                            <asp:TextBox ID="Txt4" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch4" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("Mobile")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--                        <asp:TemplateField HeaderText="Company Name">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Company Name
                                            <br />
                                            <asp:TextBox ID="Txt5" Width="70px" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch5" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("comp_name")%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Contact">
                                        <HeaderTemplate>

                                            <br />
                                            Contact   
                               <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center> <%#Eval("Contact")%> </center>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remove">
                                        <HeaderTemplate>

                                            <br />
                                            Remove
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:CheckBox ID="CheckEmailRemove"  Visible=' <%#Eval("s1")%>' ToolTip="Email- Remove from Group" runat="server" />
                                    <asp:CheckBox ID="CheckSmsRemove" Visible=' <%#Eval("s1")%>' ToolTip="SMS-Remove from Group" runat="server" />
                                </center>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <center>
                                    <asp:Button ID="btnupdate" CommandName="remove" CssClass="button" runat="server"
                                         Text="Update" /></center>
                                            <ajaxToolkit:ConfirmButtonExtender ID="CBE1" runat="server" ConfirmText="Selected members will be removed from group .Do you want to continue ?"
                                                TargetControlID="btnupdate">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SMS">
                                        <HeaderTemplate>

                                            <br />
                                            SMS
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:CheckBox ID="CheckBox1" Checked='<%#Eval("STU_bRCVSMS")%>' Enabled="false" runat="server" /></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <HeaderTemplate>

                                            <br />
                                            Email
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:CheckBox ID="CheckBox2" Checked='<%#Eval("STU_bRCVMAIL")%>' Enabled="false"
                                        runat="server" /></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <EmptyDataRowStyle />
                                <EditRowStyle />
                            </asp:GridView>
                            <%#Eval("uniqueid")%>
                            <br />
                            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label><br />
                            <asp:Button ID="Button1" runat="server" Text="Back" Visible="false" CssClass="button" OnClick="Button1_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>
