<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comCreatePlainText.ascx.vb"
    Inherits="masscom_UserControls_comCreatePlainText" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!-- Bootstrap core CSS-->
<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../cssfiles/Popup.css" rel="stylesheet" />



<script type="text/javascript">

    function opneWindow() {

        var sFeatures;
        sFeatures = "dialogWidth: 900px; ";
        sFeatures += "dialogHeight: 700px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "../comMergerDocument.aspx?Type=EMAIL"

        var result;
        //result = window.showModalDialog(strOpen, "", sFeatures);
        //window.location.reload(true);
        return ShowWindowWithClose(strOpen, 'search', '90%', '85%')
        return false;

    }

    function opnehtmlWindow() {

        var sFeatures;
        sFeatures = "dialogWidth: 1000px; ";
        sFeatures += "dialogHeight: 700px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "../TabPages/comPlainTextHtmlUpload.aspx"

        var result;
        //result = window.showModalDialog(strOpen, "", sFeatures);
        //window.location.reload(true);
        return ShowWindowWithClose(strOpen, 'search', '90%', '85%')
        return false;

    }

</script>

<script language="javascript" type="text/javascript">
    function SetCloseValuetoParent(msg) {
        //alert('close1');
        parent.setCloseValue(msg);
        return false;
    }
</script>

<script language="javascript" type="text/javascript">
    function SetCloseFrame() {
     
        CloseFrame();
       
    }
    function CloseFrame() {
        jQuery.fancybox.close();
    }

</script>

<div>
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
      
            <asp:LinkButton ID="lnkhtmlupload" OnClientClick="javascript:opnehtmlWindow(); return false;" runat="server" Visible="false">Plain Text with image ? Please click here.</asp:LinkButton>
       
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Enter Email Text
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td rowspan="2">
                                    <table width="100%">
                                         <tr id="id_type" runat="server">
                                            <td align="left" width="15%"><span class="field-label">Type</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:DropDownList ID="ddlType" runat="server"> 
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="15%"><span class="field-label">Title</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="15%"><span class="field-label">From Email Id</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="15%"><span class="field-label">Subject</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txtsubject" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="15%"><span class="field-label">Display</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txtdisplay" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_host" runat="server">
                                            <td align="left" width="15%"><span class="field-label">Host</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txthost" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_port" runat="server">
                                            <td align="left" width="15%"><span class="field-label">Port</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txtport" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_username" runat="server">
                                            <td align="left" width="15%"><span class="field-label">Username</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txtusername" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_password" runat="server">
                                            <td align="left" width="15%"><span class="field-label">Password</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox>
                                                <asp:Label ID="Label1" runat="server" Text="* Please provide if any change in Password"></asp:Label>
                                            </td>
                                        </tr>

                                        <tr id="id_priority" runat="server">
                                            <td align="left" width="15%"><span class="field-label">Priority</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:DropDownList ID="ddlPriority" runat="server">
                                                    <asp:ListItem Value="1">NORMAL</asp:ListItem>
                                                    <asp:ListItem Value="2">CRITICAL</asp:ListItem>

                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="15%"></td>

                                            <td align="left" width="85%">
                                                <telerik:RadEditor ID="txtEmailText" runat="server" EditModes="All"
                                                    Height="600px" ToolsFile="../xml/FullSetOfTools.xml" Width="750px">
                                                </telerik:RadEditor>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="15%"></td>

                                            <td align="left" width="85%">
                                                <div align="left">

                                                    <asp:LinkButton ID="Linkmerge" runat="server" CausesValidation="false" OnClientClick="opneWindow(); return false;">Upload Merge Document</asp:LinkButton>

                                                </div>
                                                <br />
                                                <br />
                                                <div id="TRDynamic" runat="server">
                                                    <asp:LinkButton ID="LinkDynamic" runat="server"
                                                        OnClientClick="javascript:return false;">Dynamic Text</asp:LinkButton>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="Panel1" runat="server">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" width="20%"><span class="field-label">Template</span>
                                                                        </td>
                                                                        <td align="left" width="30%">
                                                                            <asp:DropDownList ID="ddtemplate" runat="server" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="ddtemplate_SelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left" width="20%"><span class="field-label">Fields</span>
                                                                        </td>
                                                                        <td align="left" width="30%">
                                                                            <asp:DropDownList ID="ddfields" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <%-- <tr>
                                                                       
                                                                    </tr>--%>
                                                                    <tr>
                                                                        <td align="center" colspan="4">
                                                                            <asp:Button ID="btninsert" runat="server" CssClass="button"
                                                                                OnClick="btninsert_Click" Text="Insert" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1"
                                                                runat="server" AutoCollapse="False" AutoExpand="False"
                                                                CollapseControlID="LinkDynamic" Collapsed="true" CollapsedSize="0"
                                                                CollapsedText="Dynamic Text" ExpandControlID="LinkDynamic" ExpandedSize="100"
                                                                ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1"
                                                                TextLabelID="LinkDynamic">
                                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="15%"><span class="field-label">Attachments</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:FileUpload ID="FileUploadOrdinaryFile" runat="server" />
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trThumbnail" visible="false" >
                                            <td align="left" width="15%"><span class="field-label">Thumbnail Image</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:FileUpload ID="imgThumbnail" runat="server" />
                                            </td>
                                        </tr>

                                        <tr runat="server" id="trBanner" visible="false" >
                                            <td align="left" width="15%"><span class="field-label">Banner Image</span>
                                            </td>

                                            <td align="left" width="85%">
                                                <asp:FileUpload ID="imgBanner" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" width="100%">
                                                <asp:Button ID="btnsaveordinary" runat="server" CssClass="button" Text="Save"
                                                    ValidationGroup="EText" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmailText"
        Display="None" ErrorMessage="Please Enter Email Text" SetFocusOnError="True"
        ValidationGroup="EText"></asp:RequiredFieldValidator>
    <asp:HiddenField ID="HiddenPassword" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />

    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="EText" />

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                margin: 0,
                padding: 0,
                showCloseButton: false,

                //maxHeight: 600,
                //fitToView: true,
                //padding: 6,
                //width: w,
                //height: h,
                //autoSize: false,
                //openEffect: 'none',
                //showLoading: true,
                //closeClick: true,
                //closeEffect: 'fade',
                //'closeBtn': true,
                //afterLoad: function () {
                //    this.title = '';//ShowTitle(pageTitle);
                //},
                //helpers: {
                //    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                //    title: { type: 'inside' }
                //},
                //onComplete: function () {
                //    $("#fancybox-wrap").css({ 'top': '90px' });

                //},
                //onCleanup: function () {
                //    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                //    if (hfPostBack == "Y")
                //        window.location.reload(true);
                //}
            });

            return false;
        }
    </script>
</div>
