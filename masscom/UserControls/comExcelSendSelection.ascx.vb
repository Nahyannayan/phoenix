﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Imports System.Xml
'Imports System.Data.Oledb
Imports GemBox.Spreadsheet
Partial Class masscom_UserControls_comExcelSendSelection
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            CheckMenuRights()
            BindEmailText()
            BindHrsMins()
        End If


        AssignRights()
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        For Each row As GridViewRow In GrdEmailText.Rows
            Dim lnkselect As LinkButton = DirectCast(row.FindControl("lnkselect"), LinkButton)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Add", lnkselect)
            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next

        Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
        directory.Add("Add", LinkButton3)
        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))


    End Sub
    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000090" And ViewState("MainMnu_code") <> "M000092") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
    Public Sub BindEmailText()
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim CurBsUnit As String = Hiddenbsuid.Value
            Dim str_query = "select *,'javascript:openview('''+ CONVERT(VARCHAR,EML_ID) +'''); return false;' openview,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('<span style=''color: red;font-weight:bold''> more... </span>')tempview from COM_MANAGE_EMAIL WITH (NOLOCK) where EML_NEWS_LETTER='False' and EML_BSU_ID='" & CurBsUnit & "' AND ISNULL(EML_DELETED,'False')='False'"
            Dim Txt1 As String
            Dim Txt2 As String
            Dim Txt3 As String

            If GrdEmailText.Rows.Count > 0 Then
                Txt1 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
                Txt2 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
                Txt3 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

                If Txt1.Trim() <> "" Then
                    str_query &= " and replace(EML_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                End If

                If Txt2.Trim() <> "" Then
                    str_query &= " and REPLACE(CONVERT(VARCHAR(11), EML_DATE, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
                End If

                If Txt3.Trim() <> "" Then
                    str_query &= " and replace(EML_BODY,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                End If

            End If

            str_query &= " order by EML_DATE desc "


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count = 0 Then
                Dim dt As New DataTable
                dt.Columns.Add("EML_ID")
                dt.Columns.Add("EML_DATE")
                dt.Columns.Add("tempview")
                dt.Columns.Add("EML_TITLE")
                dt.Columns.Add("hide")
                dt.Columns.Add("openview")


                Dim dr As DataRow = dt.NewRow()
                dr("EML_ID") = ""
                dr("EML_DATE") = ""
                dr("tempview") = ""
                dr("EML_TITLE") = ""
                dr("hide") = ""
                dr("openview") = ""

                dt.Rows.Add(dr)
                GrdEmailText.DataSource = dt
                GrdEmailText.DataBind()

                DirectCast(GrdEmailText.Rows(0).FindControl("lnkselect"), LinkButton).Visible = False

            Else
                GrdEmailText.DataSource = ds
                GrdEmailText.DataBind()

            End If

            If GrdEmailText.Rows.Count > 0 Then

                DirectCast(GrdEmailText.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
                DirectCast(GrdEmailText.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
                DirectCast(GrdEmailText.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

            End If


        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GrdEmailText_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdEmailText.PageIndexChanging
        GrdEmailText.PageIndex = e.NewPageIndex
        BindEmailText()
    End Sub
    Protected Sub GrdAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdEmailText.RowCommand
        Session("MainETabIndex") = 1
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        End If
        If e.CommandName = "send" Then
            HiddenTempid.Value = e.CommandArgument
            ''lblmessage.Text = "Selected Template id :" & e.CommandArgument
            Panel1.Visible = True
            Panel3.Visible = False
            Panel2.Visible = False
        End If
        If e.CommandName = "search" Then
            BindEmailText()
        End If
    End Sub

    Public Function CheckPreviousTemplateName() As Boolean
        lblmessage.Text = ""
        Dim title As String = txttitle.Text.Trim()
        Dim bsuId As String = Hiddenbsuid.Value
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_text = "SELECT * FROM COM_EXCEL_EMAIL_DATA_MASTER WITH (NOLOCK)  WHERE TITLE='" & title & "' AND BSU_ID='" & bsuId & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_text)
        Dim Returnvalue = True
        If ds.Tables(0).Rows.Count > 0 Then
            lblmessage.Text = "Title already exists. Please enter a new title."
            Returnvalue = False
        End If
        Return Returnvalue
    End Function

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        If CheckPreviousTemplateName() Then


            Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
            Dim filename As String = FileUpload1.FileName.ToString()
            Dim savepath As String = path + "PlainTextEmails\" + Session("EmployeeId") + "_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)
            FileUpload1.SaveAs(savepath)
            lblmessage.Text = "Uploaded File Name  <br>" & filename
            Dim Encr_decrData As New Encryption64
            HiddenPathEncrypt.Value = Encr_decrData.Encrypt(savepath)
            Dim useexe = WebConfigurationManager.AppSettings("useexe").ToString()

            Dim myDataset As New DataSet()

            'Dim strConnE As String = ""
            'Dim mySelectStatement As String = ""
            'Dim ext = GetExtension(filename)

            'If ext = "xls" Then
            '    strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savepath & ";Extended Properties=""Excel 8.0;"""
            'ElseIf ext = "xlsx" Then
            '    strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savepath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
            'End If

            Try

                'Dim myData As New OleDbDataAdapter("SELECT ID,EmailID,Name FROM [Sheet1$] ", strConnE)
                'myData.TableMappings.Add("Table", "ExcelTest")
                'myData.Fill(myDataset)

                'mySelectStatement = "SELECT ID,EmailID,Name FROM [Sheet1$]"
                'Using myConnection As New OleDbConnection(strConnE)
                '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                '        myAdapter.Fill(myDataset)
                '    End Using
                'End Using


                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")

                Dim mObj As ExcelRowCollection
                Dim iRowRead As Boolean
                '  Dim ef As ExcelFile = New ExcelFile
                Dim mRowObj As ExcelRow
                Dim iRow As Integer = 1
                Dim dataTable As New DataTable
                Dim dr As DataRow
                Dim lstrData As String

                iRowRead = True
                dataTable.Columns.Add("ID", GetType(Int64))
                dataTable.Columns.Add("EmailID", GetType(String))
                dataTable.Columns.Add("Name", GetType(String))
                Dim ef = ExcelFile.Load(savepath)
                ' ef.LoadXls(savepath)
                mObj = ef.Worksheets(0).Rows


                'Dim ws As ExcelWorksheet = ef.Worksheets(0)
                'ws.ExtractToDataTable(dataTable, ws.Rows.Count, ExtractDataOptions.StopAtFirstEmptyRow, ws.Rows(0), ws.Columns(0))

                While iRowRead
                    mRowObj = mObj(iRow)
                    If mRowObj.Cells(0).Value Is Nothing Then
                        Exit While
                    End If
                    If mRowObj.Cells(0).Value.ToString = "" Then
                        Exit While
                    End If

                    If Not mRowObj.Cells(1).Value Is Nothing Then
                        dr = dataTable.NewRow
                        dr.Item(0) = Convert.ToInt64(mRowObj.Cells(0).Value)
                        dr.Item(1) = mRowObj.Cells(1).Value.ToString
                        'dr.Item(2) = mRowObj.Cells(0).Value.ToString
                        'dr.Item(3) = mRowObj.Cells(1).Value.ToString
                        'dr.Item(4) = "add"
                        'dr.Item(5) = iRow
                        'dr.Item(6) = "0"
                        'dr.Item(7) = "false"
                        dataTable.Rows.Add(dr)
                    End If
                    iRow += 1
                End While

                myDataset.Tables.Add(dataTable)


            Catch ex As Exception

                'mySelectStatement = "SELECT ID,EmailID,Name FROM [Sheet1$]"
                'Using myConnection As New OleDbConnection(strConnE)
                '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                '        myAdapter.Fill(myDataset)
                '    End Using
                'End Using


            End Try



            If useexe = "1" Then
                lnksend2.Visible = True
                lnksend.Visible = False
            Else
                lnksend.Visible = True
                lnksend2.Visible = False
            End If


            Panel2.Visible = False 'changing from true to false we are opening the schedule in parent panel popup
            Panel1.Visible = False
            Panel3.Visible = False

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TITLE", txttitle.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@TYPE", RadioType.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@EXCEL_PATH", savepath)
                pParms(3) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)


                Dim Logid = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "COM_EXCEL_EMAIL_DATA_MASTER_INSERT", pParms)
                Hiddenlogid.Value = Logid
                'Session("Hiddenlogid") = Logid
                'Session("Hiddensavepath") = Encr_decrData.Encrypt(savepath)


                ''Dim HiddenPath = Encr_decrData.Decrypt(path.Replace(" ", "+"))
                'Dim strConnE As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                '                        "Data Source=" & savepath & ";" & _
                '                        "Extended Properties=""Excel 8.0;"""


                Dim i = 0

                Dim student_id As String
                Dim EMAIL_ID As String
                Dim name As String
                Dim errorflag As Boolean
                Dim str_query As String

                For i = 0 To myDataset.Tables(0).Rows.Count - 1

                    student_id = myDataset.Tables(0).Rows(i).Item("ID").ToString()
                    EMAIL_ID = myDataset.Tables(0).Rows(i).Item("EmailID").ToString().Replace("'", "").Trim()
                    name = myDataset.Tables(0).Rows(i).Item("Name").ToString().Replace("'", "")

                    If student_id <> "" And EMAIL_ID <> "" Then
                        If isEmail(EMAIL_ID) Then
                            errorflag = True
                        Else
                            errorflag = False
                        End If

                        str_query = "insert into  COM_EXCEL_EMAIL_DATA_LOG (LOG_ID,ID,EMAIL_ID,NAME,NO_ERROR_FLAG) values('" & Logid & "','" & student_id & "','" & EMAIL_ID & "','" & name & "','" & errorflag & "') "
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                    End If

                Next

                transaction.Commit()
                BindLog()
                Dim jsfunc As String = "SetSelValuetoParent('" & CleanupStringForJavascript(Hiddenlogid.Value) & "')"
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsfunc, True)
            Catch ex As Exception
                transaction.Rollback()
                lblmessage.Text = "Error : " & ex.Message
            Finally
                connection.Close()

            End Try

        End If

    End Sub
    Public Shared Function CleanupStringForJavascript(ByVal Str_TexttoCleanup As String) As String
        Str_TexttoCleanup = HttpContext.Current.Server.UrlEncode(Str_TexttoCleanup)
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("'", "\'")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d%0a", " ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0a", "  ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d", " ")
        Return HttpContext.Current.Server.UrlDecode(Str_TexttoCleanup)
    End Function
    Public Sub BindLog()



        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim logId As String = Hiddenlogid.Value
        Dim str_query = "Select count(LOG_ID) from COM_EXCEL_EMAIL_DATA_LOG WITH (NOLOCK) where LOG_ID='" & logId & "' "
        lblmessage.Text = lblmessage.Text & "<br>Total Numbers :" & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_query = "Select count(LOG_ID) from COM_EXCEL_EMAIL_DATA_LOG WITH (NOLOCK) where NO_ERROR_FLAG='false' and LOG_ID='" & logId & "' "
        lblmessage.Text = lblmessage.Text & "<br>Error in Numbers :" & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    End Sub

    Public Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            'Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim re As New Regex(strRegex)
            'If re.IsMatch(inputEmail) Then
            '    Return (True)
            'Else
            '    Return (False)
            'End If
            Dim pattern As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" '"(.+@.+\.[a-z]+)"
            Dim expression As Regex = New Regex(pattern)
            Return expression.IsMatch(inputEmail)


        End If

    End Function

    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        If hours < 0 Then

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EML_ID", HiddenTempid.Value)
            pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", Encr_decrData.Decrypt(HiddenPathEncrypt.Value.Replace(" ", "+")))
            pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
            pParms(3) = New SqlClient.SqlParameter("@CSE_ID", 0)
            pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
            pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
            pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", Hiddenlogid.Value)

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_SCHEDULE_INSERT", pParms)
            lblmessage.Text = "Schedule has been successfully done"
            txtdate.Text = ""
            'lnksend.Visible = False
            'lnkschedule.Visible = False
            MO1.Hide()
        Else
            lblsmessage.Text = "Date time is past"
            MO1.Show()
        End If


    End Sub

    Protected Sub lnkcancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkcancel.Click
        lblmessage.Text = ""
        Panel3.Visible = True
        Panel1.Visible = False
        Panel2.Visible = False
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        lblmessage.Text = ""
        Panel3.Visible = True
        Panel1.Visible = False
        Panel2.Visible = False
    End Sub

End Class
