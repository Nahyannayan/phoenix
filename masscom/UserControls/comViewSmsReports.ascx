<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comViewSmsReports.ascx.vb" Inherits="masscom_UserControls_comViewSmsReports" %>
<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!-- Bootstrap core CSS-->
<link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/PHOENIXBETA/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


<!-- Bootstrap header files ends here -->
  <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
    <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
   <link href="/PHOENIXBETA/cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">

    function openTemplate() {


        var sFeatures;
        var strOpen;

        strOpen = '../masscom/comExcelSMSData.aspx'

        sFeatures = "dialogWidth: 1024px; ";
        sFeatures += "dialogHeight: 768px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?templateid=0";
        strOpen += "&Tab=1&DataId=" + document.getElementById("<%=HiddenLogid.ClientID %>").value;

        //window.showModalDialog(strOpen, "", sFeatures);
         return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
                   return false;


    }



</script>
<div class="matters">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="LinkSearch" OnClientClick="javascript:return false;" runat="server">Search </asp:LinkButton>
            <asp:Panel ID="Panel1" runat="server">
                <table>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Unique ID.</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%">
                            <span class="field-label">Name</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtname" runat="server"></asp:TextBox></td>

                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Mobile Number</span></td>

                        <td>
                            <asp:TextBox ID="txtmobilenumber" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%">
                            <span class="field-label">Status</span></td>

                        <td>
                            <asp:DropDownList ID="dderror" runat="server">
                                <asp:ListItem Text="All" Value="ALL" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Error" Value="ERROR"></asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnsearch" CssClass="button" CausesValidation="false" runat="server" Text="Search" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">SMS Status Reports -
                                <asp:Label ID="lblmessage" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></asp:Label>
                        <asp:Label ID="lbldatalog" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></asp:Label>
                        <asp:LinkButton ID="LinkTemplate" OnClientClick="javascript:openTemplate();return false;" runat="server">Excel Template</asp:LinkButton></td>
                </tr>
                <tr>
                    <td valign="top">

                        <asp:GridView ID="GridReports" AutoGenerateColumns="false" runat="server" Width="100%" AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row">
                            <Columns>

                                <asp:TemplateField HeaderText="Unique ID">
                                    <HeaderTemplate>
                                        Unique ID
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("LOG_UNIQUE_ID")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderTemplate>
                                        Name
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("NAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email ID">
                                    <HeaderTemplate>
                                        Mobile Number
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("LOG_MOBILE_NUMBER")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <HeaderTemplate>
                                        Status
                                                
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
<asp:LinkButton ID="LinkView" Text="View" OnClientClick="javascript:return false;" runat="server"></asp:LinkButton>
 <asp:Panel id="Show" BorderColor="Black" class="panel-cover"  runat="server" Height="150px" Width="275px">
<asp:Label id="lberror" text='<%#Eval("log_status")%>' runat="server"></asp:Label>
            </asp:Panel>
            <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" TargetControlID="LinkView" PopupControlID="Show" PopupPosition="Left" />

</center>

                                        <%--<center><%#Eval("LOG_STATUS")%></center>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                                    <HeaderTemplate>
                                        Date
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("LOG_ENTRY_DATE", "{0:dd/MMM/yyyy HH:mm:ss}")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                        <asp:Button ID="btnExport" runat="server" Text="Export-Non Sent/Errors" OnClick="btnExport_Click" CssClass="button" />
                        <asp:Button ID="btnResent" runat="server" OnClick="btnResent_Click" Visible="false" Text="Resent to Non Sent" OnClientClick="javascript:alert('Resent has been succesfully done. Please see the Reports for details.')" CssClass="button" />
                        <br />
                        <asp:Label ID="lblusermessage" runat="server" CssClass="error"></asp:Label></td>
                </tr>
            </table>
            <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="This report generate list of data which are not used for sending and also errors occured during sending."
                TargetControlID="btnExport">
            </ajaxToolkit:ConfirmButtonExtender>
            <ajaxToolkit:ConfirmButtonExtender ID="C2" runat="server" ConfirmText="Proceed with Resent?"
                TargetControlID="btnResent">
            </ajaxToolkit:ConfirmButtonExtender>
            <asp:HiddenField ID="HiddenSendingID" runat="server" />
            <asp:HiddenField ID="HiddenScheduleID" runat="server" />
            <asp:HiddenField ID="HiddenLogid" runat="server" />
            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" TargetControlID="Panel1" CollapsedSize="0" ExpandedSize="150"
                Collapsed="true" ExpandControlID="LinkSearch" CollapseControlID="LinkSearch"
                AutoCollapse="False" AutoExpand="False" ScrollContents="false" TextLabelID="LinkSearch" CollapsedText="Search" ExpandedText="Hide Search" runat="server">
            </ajaxToolkit:CollapsiblePanelExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        </script>