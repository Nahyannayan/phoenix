﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comSendMessage.ascx.vb"
    Inherits="Transport_GPS_Tracking_Communication_comSendMessage" %>



<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<div align="center" class="matters">

<br />
<br />
<table id="TBL1" runat="server" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="250">
    <tr>
        <td class="subheader_img">
            Send Message
        </td>
    </tr>
    <tr>
        <td>
<%--            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>--%>
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:RadioButtonList ID="RadioSend" runat="server" RepeatDirection="Horizontal" 
                                    AutoPostBack="True">
                                    <asp:ListItem Selected="True" Text="Now" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Schedule" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr id="TR1" runat="server" visible="false">
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtdate" runat="server" Width="100px" ValidationGroup="ss"></asp:TextBox>
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Time
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddhour" runat="server">
                                            </asp:DropDownList>
                                            &nbsp;<asp:DropDownList ID="ddmins" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnok2" runat="server" CssClass="button" Text="Proceed"
                                    ValidationGroup="ss" Width="80px" />
                                <asp:Button ID="btncancel2" runat="server" CausesValidation="False" CssClass="button"
                                    OnClientClick="window.close();" Text="Cancel" Width="80px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                             
                                <asp:Label ID="lblsmessage" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                        TargetControlID="txtdate">
                    </ajaxToolkit:CalendarExtender>
<asp:HiddenField ID="HiddenFlag" runat="server" />
<asp:HiddenField ID="Hiddencsm_id" runat="server" />


<%--                </ContentTemplate>
            </asp:UpdatePanel>--%>
        </td>
    </tr>
</table>

</div>

<script type="text/javascript" >

    if (document.getElementById('<%= HiddenFlag.ClientID %>').value == '1') {
        document.getElementById('<%= HiddenFlag.ClientID %>').value = 0;

        //alert(document.getElementById('<%= Hiddencsm_id.ClientID %>').value);

        window.showModalDialog("../comSendingSMSNormal.aspx?method=1&csm_id=" + document.getElementById('<%= Hiddencsm_id.ClientID %>').value, "", "dialogWidth:550px; dialogHeight:500px; center:yes");
        window.close();
    }



</script>