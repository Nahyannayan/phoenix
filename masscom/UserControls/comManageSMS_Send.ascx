<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comManageSMS_Send.ascx.vb"
    Inherits="masscom_UserControls_comManageSMS" %>

<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>

<script language="javascript">
    function openPopup(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 600px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";


        var result;
        result = window.showModalDialog(strOpen, "", sFeatures);
        if (result == "1") {
            window.location.reload(true);
        }

    }
     
</script>
    <script type="text/javascript" >

        function opneWindow() {
            
            var sFeatures;
            sFeatures = "dialogWidth: 900px; ";
            sFeatures += "dialogHeight: 700px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var strOpen = "../comMergerDocument.aspx?Type=SMS"

            var result;
            result = window.showModalDialog(strOpen, "", sFeatures);


        }
    </script>
    
   <table class="style1">
       <tr>
           <td align="left">

   <asp:LinkButton ID="lnkAddNew" OnClientClick="javascript:openPopup('comCreateSmsText.aspx')"
                            runat="server">Add New</asp:LinkButton>
           </td>
           <td align="right" >
                  <asp:LinkButton ID="Linkmerge" OnClientClick="javascript:opneWindow();return false;" CausesValidation="false"  Visible="false" runat="server">Upload Merge Document</asp:LinkButton>
           </td>
       </tr>
</table>
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
    <tr>
        <td class="subheader_img">
            Select
            SMS Templates
        </td>
    </tr>
    <tr>
        <td> <asp:GridView ID="GridMSMS" AutoGenerateColumns="false" Width="100%" runat="server"
                            AllowPaging="True" EmptyDataText="No SMS templates added yet (or) Search query did not produce any results"
                            OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="20" OnRowCommand="GridMSMS_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Sl No">
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Sl No
                                                    <br />
                                                    <asp:TextBox ID="Txt1" Width="50px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                        ImageUrl="~/Images/forum_search.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="Hiddenid" Value='<%# Eval("CMS_ID") %>' runat="server" />
                                        <center>
                                            <%# Eval("CMS_ID") %></center>
                                    </ItemTemplate>
                                 
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Create Date">
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Create Date
                                                    <br />
                                                    <asp:TextBox ID="Txt2" Width="80px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                        ImageUrl="~/Images/forum_search.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                        TargetControlID="Txt2">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("CMS_DATE", "{0:dd/MMM/yyyy}")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sms Text">
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Sms Text
                                                    <br />
                                                    <asp:TextBox ID="Txt3" Width="300px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                        ImageUrl="~/Images/forum_search.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                        <asp:Panel ID="T12Panel1" runat="server" Height="50px">
                                            <%#Eval("cms_sms_text")%>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                            CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                            TextLabelID="T12lblview">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="View">
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    <br />
                                                    View
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <center>
                                        <a href="javascript:openPopup('comViewSmsText.aspx?cmsid=<%# Eval("CMS_ID") %>')">
                                            <asp:Image ID="Image1" ImageUrl="~/Images/View.png" Height="25" Width="50" runat="server" /></a>
                                    </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="SMS">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        SMS
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnksmslist" Text="Send" CommandName="sms" CommandArgument='<%# Eval("CMS_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                            </Columns>
                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <EditRowStyle Wrap="False" />
                        </asp:GridView>

        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="HiddenFlag" runat="server" />
<asp:HiddenField ID="Hiddencsm_id" runat="server" />


<script type="text/javascript" >

    if (document.getElementById('<%= HiddenFlag.ClientID %>').value == '1') {
        document.getElementById('<%= HiddenFlag.ClientID %>').value = 0;

        //alert(document.getElementById('<%= Hiddencsm_id.ClientID %>').value);

        window.showModalDialog("UserControls/comSendMessagePage.aspx?csm_id=" + document.getElementById('<%= Hiddencsm_id.ClientID %>').value, "", "dialogWidth:550px; dialogHeight:400px; center:yes");

    }



</script>