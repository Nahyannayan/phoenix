﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comMergerDocumentView.ascx.vb" Inherits="masscom_UserControls_comMergerDocumentView" %>
<!-- Bootstrap core CSS-->
<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->

<div>

    <table>

        <tr>
            <td align="left"><span class="field-label">Select a Merge Document</span>

            </td>
            <td align="left">
                <asp:DropDownList ID="DropMergeTitle" runat="server" AutoPostBack="True">
                </asp:DropDownList>

            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btndelete" runat="server" CssClass="button" Text="Delete"
                    Visible="False" />

            </td>
        </tr>

        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>



    <br />

    <asp:GridView ID="GridData" runat="server" CssClass="table table-bordered table-row"
        EnableTheming="false" Width="100%">
        <HeaderStyle BackColor="LightGray" />
    </asp:GridView>
</div>
<asp:HiddenField ID="HiddenData" runat="server" />
<asp:HiddenField ID="HiddenBsu" runat="server" />
<asp:HiddenField ID="Hiddenemp_id" runat="server" />
<asp:HiddenField ID="Hiddentype" runat="server" />
