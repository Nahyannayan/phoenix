<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comEditPlainText.ascx.vb"
    Inherits="masscom_UserControls_comEditPlainText" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script type="text/javascript">

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false; 
        
    }

</script>

<div class="matters">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>        </ContentTemplate>
    </asp:UpdatePanel>
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
            <asp:Panel ID="Panel3" runat="server">
                <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
                    <tr>
                        <td class="subheader_img">
                            Email Plain Text Templates
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GrdEmailText" runat="server" EmptyDataText="Email Plain Text not added yet."
                                Width="100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Template ID">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Template&nbsp;ID
                                                        <br />
                                                        <asp:TextBox ID="Txt1" Width="50px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                            <center>
                                                <%# Eval("EML_ID") %></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Date
                                                        <br />
                                                        <asp:TextBox ID="Txt2" Width="60px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                            TargetControlID="Txt2">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email Text">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Email Title
                                                        <br />
                                                        <asp:TextBox ID="Txt3" Width="100px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkView" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                                runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                      
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Attachments">
                         <HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">

                                 <br />
                                  Attachments
                                 <br />
                                 <br />
                                 </td>
                              </tr>
                          </table>
</HeaderTemplate>
                            <ItemTemplate>
                                <asp:GridView ID="GrdAttachment" runat="server" AutoGenerateColumns="false" OnRowCommand="GrdTextAttachment_RowCommand"
                                    ShowHeader="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" CommandName="select"
                                                    Text=' <%# Eval("Name") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Size (Bytes)">
                                            <ItemTemplate>
                                                <div align="right">
                                                    -(Size
                                                    <%#Eval("length")%>
                                                    Bytes)</div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Remove">Delete</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this attachment?"
                                                    TargetControlID="lnkdelete">
                                                </ajaxToolkit:ConfirmButtonExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        Delete
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>' CommandName="RemoveDir">Delete</asp:LinkButton></center>
                                            <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this template?"
                                                TargetControlID="lnkdelete">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        Edit
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                                    CommandName="Editing">Edit</asp:LinkButton></center>
                                            <ajaxToolkit:ConfirmButtonExtender ID="C2" runat="server" ConfirmText="Are you sure you want to edit this template?"
                                                TargetControlID="lnkEdit">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="Panel4" runat="server" Visible="False">
                <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
                    <tr>
                        <td class="subheader_img">
                            Update Plain Text Templates
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    Title
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td width="100%">
                                                    <asp:TextBox ID="txtTitle" Width="50%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    From&nbsp;Email&nbsp;Id
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFrom" Width="50%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Subject
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtsubject" Width="50%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Display
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtdisplay" Width="50%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Host
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txthost" Width="50%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Port
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtport" Width="50%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Username
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtusername" Width="50%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Password
                                                </td>
                                                <td>
                                                    &nbsp;:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtpassword" Width="50%" runat="server" TextMode="Password"></asp:TextBox>
                                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" 
                                                        Text="* Please provide if any change in Password"></asp:Label>
                                                </td>
                                            </tr>
                                             <tr>
                                            <td>
                                                Priority
                                            </td>
                                            <td>
                                                &nbsp;:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPriority" SKINID="smallcmb" runat="server" Width="108px" >
                                <asp:ListItem Value="1">NORMAL</asp:ListItem>
                                <asp:ListItem Value="2">CRITICAL</asp:ListItem>
                             
                            </asp:DropDownList>
                                            </td>
                                        </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <telerik:RadEditor ID="txtEditEmailText" runat="server" EditModes="All" Height="600px"
                                                        ToolsFile="../xml/FullSetOfTools.xml" Width="750px">
                                                    </telerik:RadEditor>
                                                    <div id="TRDynamic" runat="server">
                                                        <asp:LinkButton ID="LinkDynamic" OnClientClick="javascript:return false;" runat="server">Dynamic Text</asp:LinkButton>
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="Panel1" runat="server">
                                                                    <table>
                                                                        <tr>
                                                                            <td colspan="1">
                                                                                Template
                                                                            </td>
                                                                            <td colspan="1">
                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddtemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddtemplate_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="1">
                                                                                Fields
                                                                            </td>
                                                                            <td colspan="1">
                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddfields" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" colspan="1">
                                                                            </td>
                                                                            <td align="center" colspan="1">
                                                                            </td>
                                                                            <td align="center">
                                                                                <asp:Button ID="btninsert" runat="server" CssClass="button" Text="Insert" OnClick="btninsert_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkDynamic" Collapsed="true"
                                                                    CollapsedSize="0" CollapsedText="Dynamic Text" ExpandControlID="LinkDynamic"
                                                                    ExpandedSize="100" ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1"
                                                                    TextLabelID="LinkDynamic">
                                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Attachments
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="FileUploadEditEmailtext" runat="server" />
                                                  
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btneditplaintextsave" runat="server" CssClass="button" Text="Save"
                                            ValidationGroup="EdText" Width="118px" />
                                        <asp:Button ID="btnplaintextcancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" Width="113px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEditEmailText"
                    Display="None" ErrorMessage="Please Enter Email Text" SetFocusOnError="True"
                    ValidationGroup="EdText"></asp:RequiredFieldValidator>
                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
                    ShowSummary="False" ValidationGroup="EdText" />
            </asp:Panel>
            <asp:HiddenField ID="HiddenFieldTemplateid" runat="server" />
            <asp:HiddenField ID="HiddenPassword" runat="server" />
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />

</div>
