Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comManageSMS
    Inherits System.Web.UI.UserControl

   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenFlag.Value = 0
            Hiddenbsuid.Value = Session("sBsuid")
            'CheckMenuRights()
            BindGrid()
        End If
        'AssignRights()
    End Sub

    'Public Sub AssignRights()
    '    Dim Encr_decrData As New Encryption64
    '    Dim CurBsUnit As String = Hiddenbsuid.Value
    '    Dim USR_NAME As String = Session("sUsr_name")

    '    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
    '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
    '    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

    '    Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
    '    directory.Add("Add", lnkAddNew)
    '    Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))


    '    For Each row As GridViewRow In GridMSMS.Rows
    '        Dim editlink As Image = DirectCast(row.FindControl("Image1"), Image)
    '        Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
    '        directory2.Add("Edit", editlink)
    '        Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
    '    Next


    'End Sub

    'Private Function isPageExpired() As Boolean

    '    If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
    '        Return False
    '    ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    'Public Sub CheckMenuRights()
    '    Dim Encr_decrData As New Encryption64
    '    Dim CurBsUnit As String = Hiddenbsuid.Value
    '    Dim USR_NAME As String = Session("sUsr_name")
    '    If isPageExpired() Then
    '        Response.Redirect("expired.htm")
    '    Else
    '        Session("TimeStamp") = Now.ToString
    '        ViewState("TimeStamp") = Now.ToString
    '    End If

    '    Try
    '        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
    '        If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000040") Then
    '            If Not Request.UrlReferrer Is Nothing Then
    '                Response.Redirect(Request.UrlReferrer.ToString())
    '            Else

    '                Response.Redirect("~\noAccess.aspx")
    '            End If
    '        End If

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '        Response.Redirect("~\noAccess.aspx")

    '    End Try
    '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    'End Sub


    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " select cms_id,cms_date, '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(cms_sms_text,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,cms_sms_text " & _
                        " from com_manage_sms where CMS_BSU_ID ='" & Hiddenbsuid.Value & "' AND ISNULL(CMS_DELETED,'False')='False' "

        Dim Txt1 As String
        Dim Txt2 As String
        Dim Txt3 As String

        If GridMSMS.Rows.Count > 0 Then
            Txt1 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            Txt2 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            Txt3 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(cms_id,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt2.Trim() <> "" Then
                str_query &= " and REPLACE(CONVERT(VARCHAR(11), cms_date, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
            End If

            If Txt3.Trim() <> "" Then
                str_query &= " and replace(cms_sms_text,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by CMS_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("cms_id")
            dt.Columns.Add("cms_date")
            dt.Columns.Add("hide")
            dt.Columns.Add("tempview")
            dt.Columns.Add("cms_sms_text")


            Dim dr As DataRow = dt.NewRow()
            dr("cms_id") = ""
            dr("cms_date") = ""
            dr("hide") = ""
            dr("tempview") = ""
            dr("cms_sms_text") = ""

            dt.Rows.Add(dr)
            GridMSMS.DataSource = dt
            GridMSMS.DataBind()

            DirectCast(GridMSMS.Rows(0).FindControl("Image1"), Image).Visible = False
        Else
            GridMSMS.DataSource = ds
            GridMSMS.DataBind()

        End If

        If GridMSMS.Rows.Count > 0 Then

            DirectCast(GridMSMS.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GridMSMS.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(GridMSMS.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

        End If


    End Sub

    Protected Sub GridMSMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridMSMS.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('comViewSmsText.aspx?cmsid={0}', '','dialogHeight:600px;dialogWidth:600px;scroll:yes;resizable:no;');", pId)
    End Function
    Protected Function GetUrl(ByVal pId As String) As String
        Return String.Format("javascript:openPopup('comViewSmsText.aspx?cmsid={0}')", pId)
    End Function

    Protected Sub GridMSMS_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        'If e.CommandName = "Deleting" Then

        'End If
        'If e.CommandName = "report" Then
        '    Dim EncDec As New Encryption64
        '    Dim cmsid As String = e.CommandArgument
        '    cmsid = EncDec.Encrypt(cmsid)
        '    Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        '    Response.Redirect("comManageSmsReports.aspx?cms_id=" & cmsid & mInfo)
        'End If
        If e.CommandName = "search" Then
            BindGrid()
        End If

        If e.CommandName = "sms" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim groupid As String = Request.QueryString("GRP_ID")
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CSM_CGR_ID", groupid)
            pParms(1) = New SqlClient.SqlParameter("@CSM_CMS_ID", e.CommandArgument)
            pParms(2) = New SqlClient.SqlParameter("@CSM_BSU_ID", Hiddenbsuid.Value)
            pParms(3) = New SqlClient.SqlParameter("@CSM_TYPE", "SMS")
            Dim ID As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COM_SEND_SMS_INSERT", pParms)
            SendApproval(ID.Split("-")(0))

        End If

    End Sub


    Public Sub SendApproval(ByVal id As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Update COM_SEND_SMS set CSM_APPROVED='YES',CSM_APPROVE_DATE=getdate() where CSM_ID='" & id & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        HiddenFlag.Value = 1
        Hiddencsm_id.Value = id

    End Sub

End Class
