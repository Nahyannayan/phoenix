Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet

Partial Class masscom_UserControls_comViewSmsReports
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExport)
        If Not IsPostBack Then
            Dim SendingiD = Request.QueryString("SendingId").ToString()
            Dim ScheduleID = Request.QueryString("ScheduleId").ToString()

            HiddenSendingID.Value = SendingiD
            HiddenScheduleID.Value = ScheduleID

            BindGrid()
            BindGroupDetails()

            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If


    End Sub


    Public Sub BindGroupDetails()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        If HiddenSendingID.Value.Trim() <> 0 Then

            str_query = " SELECT SENDING_ID,ISNULL(D.LOG_ID,'')LOG_ID,A.TYPE,ISNULL(TITLE,'')TITLE,ISNULL(CGR_DES,'')CGR_DES FROM dbo.COM_EXCEL_SMS_SENDING_LOG A " & _
                            " LEFT JOIN  dbo.COM_SEND_SMS B ON B.CSM_ID=A.CSM_ID " & _
                            " LEFT JOIN dbo.COM_GROUPS_M C ON C.CGR_ID= B.CSM_CGR_ID " & _
                            " LEFT JOIN  dbo.COM_EXCEL_SMS_DATA_MASTER D ON D.LOG_ID= A.DATA_LOG_ID " & _
                            " WHERE SENDING_ID='" & HiddenSendingID.Value.Trim() & "'"

        ElseIf HiddenScheduleID.Value <> 0 Then

            str_query = " SELECT RECORD_ID,ISNULL(D.LOG_ID,'')LOG_ID,ISNULL(TITLE,'')TITLE,ISNULL(CGR_DES,'')CGR_DES FROM dbo.COM_MANAGE_SMS_SCHEDULE A " & _
                        " LEFT JOIN  dbo.COM_SEND_SMS B ON B.CSM_ID=A.CSM_ID " & _
                        " LEFT JOIN dbo.COM_GROUPS_M C ON C.CGR_ID= B.CSM_CGR_ID " & _
                        " LEFT JOIN  dbo.COM_EXCEL_SMS_DATA_MASTER D ON D.LOG_ID= A.DATA_LOG_ID " & _
                        " WHERE RECORD_ID='" & HiddenScheduleID.Value & "'"

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        If ds.Tables(0).Rows.Count > 0 Then

            Dim logid = ds.Tables(0).Rows(0).Item("LOG_ID").ToString()
            Dim exceltitile = ds.Tables(0).Rows(0).Item("TITLE").ToString()
            Dim grpdes = ds.Tables(0).Rows(0).Item("CGR_DES").ToString()

            If logid <> 0 Then ''Excel

                lbldatalog.Text = "Type : Excel - " & exceltitile
                LinkTemplate.Visible = True
                HiddenLogid.Value = logid
            Else '' By Group

                lbldatalog.Text = "Type : Group - " & grpdes
                LinkTemplate.Visible = False
            End If


        End If




    End Sub

    Public Sub HeaderAlert()
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            ''Success
            Dim str_query = "select count(*) from dbo.COM_LOG_SMS_TABLE "

            If HiddenSendingID.Value.Trim() <> 0 Then
                str_query = str_query & " where LOG_SENDING_ID =" & HiddenSendingID.Value & " "
            Else
                str_query = str_query & " where LOG_SCHEDULE_ID =" & HiddenScheduleID.Value & " "
            End If

            str_query = str_query & " and CHARINDEX('Error',log_status) = 0 "

            Dim SuccessCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            ''Error

            str_query = "select count(*) from dbo.COM_LOG_SMS_TABLE"

            If HiddenSendingID.Value.Trim() <> 0 Then
                str_query = str_query & " where LOG_SENDING_ID =" & HiddenSendingID.Value & " "
            Else
                str_query = str_query & " where LOG_SCHEDULE_ID =" & HiddenScheduleID.Value & " "
            End If

            str_query = str_query & " and CHARINDEX('Error',log_status) = 1 "

            Dim ErrorCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            lblmessage.Text = "Success : " & SuccessCount.ToString() & " / Errors : " & ErrorCount.ToString()

           

        Catch ex As Exception

        End Try


    End Sub

    Public Function FindGroupType() As String
        '' Student or Staff
        Dim ReturnValue = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        If HiddenSendingID.Value.Trim() <> 0 Then
            str_query = " select top 1 (case log_cgr_id  when  0 then ( " & _
                        " select TYPE from dbo.COM_EXCEL_SMS_DATA_MASTER where LOG_ID= " & _
                        " (select DATA_LOG_ID from dbo.COM_EXCEL_SMS_SENDING_LOG where SENDING_ID='" & HiddenSendingID.Value & "') " & _
                        " )else(select CGR_TYPE from dbo.COM_GROUPS_M where CGR_ID=log_cgr_id ) end ) Type " & _
                        " from dbo.COM_LOG_SMS_TABLE where LOG_SENDING_ID='" & HiddenSendingID.Value & "' "

        ElseIf HiddenScheduleID.Value <> 0 Then
            str_query = " select top 1 (case log_cgr_id  when  0 then ( " & _
                        " select TYPE from dbo.COM_EXCEL_SMS_DATA_MASTER where LOG_ID= " & _
                        " (select DATA_LOG_ID from dbo.COM_MANAGE_SMS_SCHEDULE where RECORD_ID='" & HiddenScheduleID.Value & "') " & _
                        " )else(select CGR_TYPE from dbo.COM_GROUPS_M where CGR_ID=log_cgr_id ) end ) Type " & _
                        " from dbo.COM_LOG_SMS_TABLE where LOG_SCHEDULE_ID='" & HiddenScheduleID.Value & "' "

        End If

        ReturnValue = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Return ReturnValue
    End Function
    Public Sub BindGrid()
        lblusermessage.Text = ""
        Try

            Dim Type = FindGroupType()

            If Type <> "" Then

                HeaderAlert()

                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_query = ""
                If Type = "STUDENT" Then
                    str_query = " select LOG_UNIQUE_ID,LOG_MOBILE_NUMBER,LOG_STATUS,LOG_ENTRY_DATE , " & _
                                               " isnull(STU_FIRSTNAME,'')+ ' '+isnull(STU_MIDNAME,'')+ ' ' +isnull(STU_LASTNAME,'') NAME " & _
                                               " from dbo.COM_LOG_SMS_TABLE A " & _
                                               " left join dbo.STUDENT_M b on a.LOG_UNIQUE_ID=b.STU_NO "
                ElseIf Type = "STAFF" Then
                    str_query = " select LOG_UNIQUE_ID,LOG_MOBILE_NUMBER,LOG_STATUS,LOG_ENTRY_DATE , " & _
                                                              " isnull(EMP_FNAME,'')+ ' '+isnull(EMP_MNAME,'')+ ' ' +isnull(EMP_LNAME,'') NAME " & _
                                                              " from dbo.COM_LOG_SMS_TABLE A " & _
                                                              " left join dbo.EMPLOYEE_M b on a.LOG_UNIQUE_ID=b.EMPNO "

                Else
                    str_query = " select LOG_UNIQUE_ID,LOG_MOBILE_NUMBER,LOG_STATUS,LOG_ENTRY_DATE , " & _
                                " CASE WHEN ISNULL(LOG_SENDING_ID,'') != '' THEN " & _
                                " (SELECT TOP 1 NAME FROM dbo.COM_EXCEL_SMS_DATA_LOG A INNER JOIN dbo.COM_EXCEL_SMS_SENDING_LOG B ON A.LOG_ID=B.DATA_LOG_ID  WHERE A.ID=LOG_UNIQUE_ID) " & _
                                " ELSE " & _
                                " (SELECT TOP 1 NAME FROM dbo.COM_EXCEL_SMS_DATA_LOG A INNER JOIN dbo.COM_MANAGE_SMS_SCHEDULE B ON A.LOG_ID=B.DATA_LOG_ID WHERE A.ID=LOG_UNIQUE_ID ) " & _
                                " END " & _
                                " as  NAME " & _
                                " from dbo.COM_LOG_SMS_TABLE A "



                End If


                If HiddenSendingID.Value.Trim() <> 0 Then
                    str_query = str_query & " where LOG_SENDING_ID =" & HiddenSendingID.Value & " "
                Else
                    str_query = str_query & " where LOG_SCHEDULE_ID =" & HiddenScheduleID.Value & " "
                End If

                If txtnumber.Text.Trim() <> "" Then
                    str_query = str_query & " and  LOG_UNIQUE_ID like '%" & txtnumber.Text.Trim() & "%' "
                End If

                If Type = "STUDENT" Then
                    If txtname.Text.Trim() <> "" Then
                        str_query = str_query & " and  (isnull(STU_FIRSTNAME,'')+isnull(STU_MIDNAME,'')+isnull(STU_LASTNAME,'')) like '%" & txtname.Text.Trim() & "%' "
                    End If
                ElseIf Type = "STAFF" Then
                    If txtname.Text.Trim() <> "" Then
                        str_query = str_query & " and  (isnull(EMP_FNAME,'')+isnull(EMP_MNAME,'')+isnull(EMP_LNAME,'')) like '%" & txtname.Text.Trim() & "%' "
                    End If

                End If


                If txtmobilenumber.Text.Trim() <> "" Then
                    str_query = str_query & " and  LOG_MOBILE_NUMBER like '%" & txtmobilenumber.Text.Trim() & "%' "
                End If


                If dderror.SelectedValue = "ERROR" Then
                    str_query = str_query & " and CHARINDEX('Error',log_status) =1 "
                End If

                str_query = str_query & " ORDER BY LOG_ENTRY_DATE "

                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                GridReports.DataSource = ds
                GridReports.DataBind()

            End If

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub GridReports_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridReports.PageIndexChanging

        GridReports.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        BindGrid()
    End Sub


    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim ReturnValue = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""

        If HiddenSendingID.Value.Trim() <> 0 Then
            str_query = " SELECT CGR_ID,A.CSM_ID,CGR_TYPE,CGR_CONDITION,CGR_REMOVE_SMS_IDS,CGR_GRP_TYPE,DATA_LOG_ID FROM COM_EXCEL_SMS_SENDING_LOG  A" & _
                        " LEFT JOIN dbo.COM_SEND_SMS B ON A.CSM_ID=B.CSM_ID " & _
                        " LEFT JOIN dbo.COM_GROUPS_M C ON B.CSM_CGR_ID=C.CGR_ID " & _
                        " WHERE SENDING_ID='" & HiddenSendingID.Value.Trim() & "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then

                ''Check if - Groups or  Excel 
                Dim csm_id = ds.Tables(0).Rows(0).Item("CSM_ID").ToString()
                Dim log_id = ds.Tables(0).Rows(0).Item("DATA_LOG_ID").ToString()
                Dim condition As String = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
                Dim grp_id = ds.Tables(0).Rows(0).Item("CGR_ID").ToString()
                Dim type As String = ds.Tables(0).Rows(0).Item("CGR_TYPE").ToString()

                If csm_id = 0 Then '' By Excel 
                    If type = "STAFF" Then
                        str_query = " SELECT DISTINCT LOG_UNIQUE_ID as uniqueid ,LOG_MOBILE_NUMBER as Mobile FROM dbo.COM_EXCEL_SMS_DATA_LOG  A " & _
                                                       " INNER JOIN dbo.COM_LOG_SMS_TABLE B ON A.ID=B.LOG_UNIQUE_ID " & _
                                                       " WHERE A.LOG_ID='" & log_id & "' AND LOG_SENDING_ID='" & HiddenSendingID.Value.Trim() & "' AND CHARINDEX('Error',log_status) > 0 "


                    Else

                        str_query = " SELECT DISTINCT LOG_UNIQUE_ID as uniqueid ,LOG_MOBILE_NUMBER as Mobile,stu_name,GRM_DISPLAY AS GRADE,SCT_DESCR AS SECTION FROM dbo.COM_EXCEL_SMS_DATA_LOG  A " & _
                                                     " INNER JOIN dbo.COM_LOG_SMS_TABLE B ON A.ID=B.LOG_UNIQUE_ID " & _
                                                     " inner join vv_students c ON c.STU_NO = B.LOG_UNIQUE_ID  " & _
                                                     " WHERE A.LOG_ID='" & log_id & "' AND LOG_SENDING_ID='" & HiddenSendingID.Value.Trim() & "' AND CHARINDEX('Error',log_status) > 0 "


                    End If

                End If
                If csm_id <> 0 Then '' By Groups 
                    ''Check for Add On or Groups
                    Dim Grp_type = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()

                    If type = "STAFF" Then

                        If Grp_type = "GRP" Then
                            If Session("BSU_COUNTRY_ID") = "172" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            End If


                            str_query = " SELECT uniqueid,Mobile FROM " & _
                                      " ( " & _
                                        condition & _
                                      " )A" & _
                                      " WHERE uniqueid not in " & _
                                      " ( " & _
                                      " SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SENDING_ID='" & HiddenSendingID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                                      " )"

                        ElseIf Grp_type = "AON" Then

                            condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on  b.CGAO_UNIQUE_ID =a.uniqueid and CGAO_CGR_ID='" & grp_id & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()

                            Dim val
                            If Session("BSU_COUNTRY_ID") = "172" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "
                            Else
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  "
                            End If
                            str_query = " SELECT uniqueid,Mobile FROM " & _
                                                              " ( " & _
                                                                val & _
                                                              " )A" & _
                                                              " WHERE uniqueid not in " & _
                                                              " ( " & _
                                                              " SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SENDING_ID='" & HiddenSendingID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                                                              " )"

                        End If

                    Else


                        If Grp_type = "GRP" Then

                            If Session("BSU_COUNTRY_ID") = "172" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "


                            Else
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            End If
                            str_query = " SELECT STU_NAME,uniqueid,Mobile,GRM_DISPLAY AS GRADE,SCT_DESCR AS SECTION FROM " & _
                                      " ( " & _
                                        condition & _
                                      " )A INNER JOIN vv_students B ON B.STU_NO = UNIQUEID " & _
                                      " WHERE uniqueid not in " & _
                                      " ( " & _
                                      " SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SENDING_ID='" & HiddenSendingID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                                      " )"

                        ElseIf Grp_type = "AON" Then

                            condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on  b.CGAO_UNIQUE_ID =a.uniqueid and CGAO_CGR_ID='" & grp_id & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()

                            Dim val
                            If Session("BSU_COUNTRY_ID") = "172" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "

                            Else
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  "
                            End If

                            str_query = " SELECT STU_NAME,uniqueid,Mobile,GRM_DISPLAY AS GRADE,SCT_DESCR AS SECTION FROM " & _
                                                              " ( " & _
                                                                val & _
                                                              " )A INNER JOIN vv_students B ON B.STU_NO = UNIQUEID " & _
                                                              " WHERE uniqueid not in " & _
                                                              " ( " & _
                                                              " SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SENDING_ID='" & HiddenSendingID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                                                              " )"

                        End If


                    End If




                End If


            End If


        ElseIf HiddenScheduleID.Value <> 0 Then
            str_query = " SELECT A.CGR_ID,A.CSM_ID,CGR_TYPE,CGR_CONDITION,CGR_REMOVE_SMS_IDS,CGR_GRP_TYPE,DATA_LOG_ID FROM COM_MANAGE_SMS_SCHEDULE  A" & _
                                   " LEFT JOIN dbo.COM_SEND_SMS B ON A.CSM_ID=B.CSM_ID " & _
                                   " LEFT JOIN dbo.COM_GROUPS_M C ON B.CSM_CGR_ID=C.CGR_ID " & _
                                   " WHERE RECORD_ID='" & HiddenScheduleID.Value.Trim() & "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                ''Check if - Groups or  Excel 
                Dim csm_id = ds.Tables(0).Rows(0).Item("CSM_ID").ToString()
                Dim log_id = ds.Tables(0).Rows(0).Item("DATA_LOG_ID").ToString()
                Dim condition As String = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
                Dim grp_id = ds.Tables(0).Rows(0).Item("CGR_ID").ToString()
                Dim type As String = ds.Tables(0).Rows(0).Item("CGR_TYPE").ToString()

                If csm_id = 0 Then '' By Excel 
                    If type = "STAFF" Then

                        str_query = " SELECT ID as uniqueid,MOBILE_NO as Mobile FROM dbo.COM_EXCEL_SMS_DATA_LOG WHERE LOG_ID='" & log_id & "' " & _
                                                       " AND ID  NOT IN " & _
                                                       " (" & _
                                                       " SELECT A.ID FROM dbo.COM_EXCEL_SMS_DATA_LOG  A " & _
                                                       " LEFT JOIN dbo.COM_LOG_SMS_TABLE B ON A.ID=B.LOG_UNIQUE_ID " & _
                                                       " WHERE A.LOG_ID='" & log_id & "' AND LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' AND CHARINDEX('Error',log_status) =0 " & _
                                                       " ) "

                    Else

                        'str_query = " SELECT ID as uniqueid,MOBILE_NO as Mobile,stu_name,GRM_DISPLAY AS GRADE,SCT_DESCR AS SECTION FROM dbo.COM_EXCEL_SMS_DATA_LOG inner join vv_students c ON c.STU_NO =ID WHERE LOG_ID='" & log_id & "' " & _
                        '                            " AND ID  NOT IN " & _
                        '                            " (" & _
                        '                            " SELECT A.ID FROM dbo.COM_EXCEL_SMS_DATA_LOG  A " & _
                        '                            " LEFT JOIN dbo.COM_LOG_SMS_TABLE B ON A.ID=B.LOG_UNIQUE_ID " & _
                        '                            " WHERE A.LOG_ID='" & log_id & "' AND LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' AND CHARINDEX('Error',log_status) =0 " & _
                        '                            " ) "

                        str_query = "SELECT ID as uniqueid,MOBILE_NO as Mobile, ISNULL(Name,'') Name FROM dbo.COM_EXCEL_SMS_DATA_LOG A " & _
                            "LEFT JOIN dbo.COM_LOG_SMS_TABLE B ON A.ID=B.LOG_UNIQUE_ID  WHERE A.LOG_ID='" & log_id & "' AND " & _
                            "LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' AND log_status like 'Error%'"

                    End If

                End If

                If csm_id <> 0 Then '' By Groups 
                    ''Check for Add On or Groups
                    Dim Grp_type = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()

                    If type = "STAFF" Then

                        If Grp_type = "GRP" Then

                            If Session("BSU_COUNTRY_ID") = "172" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "


                            Else
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            End If
                            str_query = " SELECT uniqueid,Mobile FROM " & _
                                      " ( " & _
                                        condition & _
                                      " )A" & _
                                  " INNER JOIN  COM_LOG_SMS_TABLE COMS ON uniqueid=LOG_UNIQUE_ID  AND CHARINDEX('Error',log_status) > 0 " & _
                                  " WHERE  LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' "
                            '" WHERE uniqueid not in " & _
                            '" ( " & _
                            '" SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                            '" )"

                        ElseIf Grp_type = "AON" Then

                            condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on  b.CGAO_UNIQUE_ID =a.uniqueid and CGAO_CGR_ID='" & grp_id & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()

                            Dim val
                            If Session("BSU_COUNTRY_ID") = "172" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "

                            Else
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  "
                            End If
                            str_query = " SELECT uniqueid,Mobile FROM " & _
                                                              " ( " & _
                                                                val & _
                                                              " )A" & _
                                                              " INNER JOIN  COM_LOG_SMS_TABLE COMS ON uniqueid=LOG_UNIQUE_ID  AND CHARINDEX('Error',log_status) > 0 " & _
                                                              " WHERE  LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' "
                            '" WHERE uniqueid not in " & _
                            '" ( " & _
                            '" SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                            '" )"

                        End If

                    Else


                        If Grp_type = "GRP" Then

                            If Session("BSU_COUNTRY_ID") = "172" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                            Else
                                condition = " select isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  where STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                            End If
                            str_query = " SELECT STU_NAME,uniqueid,Mobile,GRM_DISPLAY AS GRADE,SCT_DESCR AS SECTION FROM " & _
                                      " ( " & _
                                        condition & _
                                      " )A INNER JOIN vv_students B ON B.STU_NO = UNIQUEID  " & _
                                      " INNER JOIN  COM_LOG_SMS_TABLE COMS ON uniqueid=LOG_UNIQUE_ID  AND CHARINDEX('Error',log_status) > 0 " & _
                                      " WHERE  LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' "
                            '" WHERE uniqueid not in " & _
                            '" ( " & _
                            '" SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                            '" )"

                        ElseIf Grp_type = "AON" Then

                            condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on  b.CGAO_UNIQUE_ID =a.uniqueid and CGAO_CGR_ID='" & grp_id & "' and STU_bRCVSMS='True' " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()

                            Dim val
                            If Session("BSU_COUNTRY_ID") = "172" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('971'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "
                            ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('91'+ right(replace(Mobile,'-',''),10),'') as Mobile,RowID from ( " & condition & " ) a  "

                            ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('002'+ right(replace(Mobile,'-',''),11),'') as Mobile,RowID from ( " & condition & " ) a  "

                            ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('60'+ right(replace(Mobile,'-',''),9),'') as Mobile,RowID from ( " & condition & " ) a  "

                            Else
                                val = " select  isnull(uniqueid,'') as uniqueid,isnull('012'+ right(replace(Mobile,'-',''),7),'') as Mobile,RowID from ( " & condition & " ) a  "
                            End If
                            str_query = " SELECT STU_NAME,uniqueid,Mobile,GRM_DISPLAY AS GRADE,SCT_DESCR AS SECTION FROM " & _
                                                              " ( " & _
                                                                val & _
                                                              " )A INNER JOIN vv_students B ON B.STU_NO = UNIQUEID " & _
                                                              " INNER JOIN  COM_LOG_SMS_TABLE COMS ON uniqueid=LOG_UNIQUE_ID  AND CHARINDEX('Error',log_status) > 0 " & _
                                                              " WHERE  LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' "
                            '" WHERE uniqueid not in " & _
                            '" ( " & _
                            '" SELECT LOG_UNIQUE_ID FROM COM_LOG_SMS_TABLE WHERE LOG_SCHEDULE_ID='" & HiddenScheduleID.Value.Trim() & "' AND CHARINDEX('Error',log_status) = 0 " & _
                            '" )"

                        End If



                    End If



                End If

            End If
        End If


        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds1.Tables(0).Rows.Count > 0 Then
            Dim dt As DataTable = ds1.Tables(0)

            ' Create excel file.

            'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
            ws.InsertDataTable(dt, "A1", True)
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
            ef.SaveXls(Response.OutputStream)

        Else
            lblusermessage.Text = "No Invalid Data Found.No Data to retrieve."
        End If


    End Sub

    Protected Sub btnResent_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        If HiddenSendingID.Value.Trim() <> 0 Then
            str_query = "UPDATE COM_EXCEL_SMS_SENDING_LOG SET RESENT_ACTIVE='True', RESENT_COMPLETED=NULL WHERE SENDING_ID='" & HiddenSendingID.Value.Trim() & "'"
        ElseIf HiddenScheduleID.Value <> 0 Then
            str_query = "UPDATE COM_MANAGE_SMS_SCHEDULE SET RESENT_ACTIVE='True' , RESENT_COMPLETED=NULL WHERE RECORD_ID='" & HiddenScheduleID.Value.Trim() & "'"
        End If

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        btnResent.Visible = False

    End Sub

End Class
