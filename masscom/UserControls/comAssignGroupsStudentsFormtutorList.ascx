﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsStudentsFormtutorList.ascx.vb" Inherits="masscom_UserControls_comAssignGroupsStudentsFormtutorList" %>

<div class="matters">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr class="title">
            <td align="left">
                FORM TUTOR
                GROUP LIST
            </td>
        </tr>
    </table>
    <br />
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="750px">
        <tr>
            <td class="subheader_img">
                Group List &nbsp;&nbsp;
                    <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACD_ID_SelectedIndexChanged">
                    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="750px">
                    <tr>
                        <td align="left">
<asp:LinkButton ID="lnkadd" runat="server">Add New</asp:LinkButton>

                            <asp:GridView ID="GrdGroups" AutoGenerateColumns="false" runat="server" AllowPaging="True"
                                OnPageIndexChanging="GrdGroups_PageIndexChanging" PageSize="20" Width="100%"
                                EmptyDataText="No Groups added yet. or Search Query did not produce any results">
                                <RowStyle CssClass="griditem" Height="25px" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Group Id">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Group&nbsp;Id
                                                        <br />
                                                        <asp:TextBox ID="Txt1" Width="40px" runat="server"></asp:TextBox>&nbsp;
                                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddengrpid" Value='<%#Eval("CGR_ID")%>' runat="server" />
                                            <center>
                                                <%#Eval("CGR_ID")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Name">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Group&nbsp;Name
                                                        <br />
                                                        <asp:TextBox ID="Txt2" Width="60px" runat="server"></asp:TextBox>&nbsp;
                                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("CGR_DES")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Type
                                                        <br />
                                                        <asp:DropDownList ID="DropSearch1" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                            runat="server">
                                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                            <asp:ListItem Value="STUDENT" Text="STUDENT"></asp:ListItem>
                                                            <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("CGR_TYPE")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Type">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Group&nbsp;Type
                                                        <br />
                                                        <asp:DropDownList ID="DropSearch2" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                            runat="server">
                                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                            <asp:ListItem Value="GRP" Text="Group"></asp:ListItem>
                                                            <asp:ListItem Value="AON" Text="Add On"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddentype" Value='<%#Eval("gtype")%>' runat="server" />
                                            <center>
                                                <%#Eval("gtype")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        Business&nbsp;Unit
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddengrpbsu" Value='<%#Eval("cgr_grp_bsu_id")%>' runat="server" />
                                       <center><%#Eval("BSU_NAME")%></center>   
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Academic Year">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                       Academic&nbsp;Year
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                          <center> <%#Eval("ACY_DESCR")%></center>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField HeaderText="Academic Year">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                       Contact
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                         <center> <%#Eval("CGR_CONTACT_C")%></center>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    
         <%--                           
                                    <asp:TemplateField HeaderText="Edit">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        Edit
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkview" Text="Edit" CommandName="Edit" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Delete">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        Delete
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkDelete" Text="Delete" CommandName="Deleted" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                            <ajaxToolkit:ConfirmButtonExtender ID="C1" TargetControlID="lnkDelete" ConfirmText="Do you wish to continue?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="List">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        List
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnklist" Text="List" CommandName="list" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="SMS">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        SMS
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnksmslist" Text="Select" CommandName="sms" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>


<%--                                    <asp:TemplateField HeaderText="Export">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        Export
                                                        <br />
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkExportMobile" Text="Mobile" CommandName="ExportMobile" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton>&nbsp;<asp:LinkButton ID="lnkExportEmail" Text="Email"
                                                        CommandName="ExportEmail" CommandArgument='<%# Eval("CGR_ID") %>' runat="server"></asp:LinkButton>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />