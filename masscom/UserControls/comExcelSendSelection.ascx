﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSendSelection.ascx.vb" Inherits="masscom_UserControls_comExcelSendSelection" %>

<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<link href="~/cssfiles/title.css" rel="stylesheet" type="text/css" />--%>



<!-- Bootstrap core CSS-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="//cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../cssfiles/Popup.css" rel="stylesheet" />



<script type="text/javascript">
    function openPopup(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 1024px; ";
        sFeatures += "dialogHeight: 768px; ";


        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
        strOpen += "&EType=PT";
        strOpen += "&templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
        strOpen += "&method=0";
        document.getElementById('div2').style.display = 'none';
        window.showModalDialog(strOpen, "", sFeatures);



    }
    function openPopup2(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 500px; ";
        sFeatures += "dialogHeight: 500px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
        strOpen += "&EType=PT";
        strOpen += "&templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
        strOpen += "&method=0";
        document.getElementById('div2').style.display = 'none';

        window.showModalDialog(strOpen, "", sFeatures);


    }
    function openTemplate(tab) {

        //alert(7);
        var sFeatures;
        var strOpen;

        strOpen = 'comExcelEmailData.aspx'

        //sFeatures = "dialogWidth: 1024px; ";
        //sFeatures += "dialogHeight: 768px; ";

        //sFeatures += "help: no; ";
        //sFeatures += "resizable: no; ";
        //sFeatures += "scroll: yes; ";
        //sFeatures += "status: no; ";
        //sFeatures += "unadorned: no; ";

        strOpen += "?templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
        strOpen += "&EType=PT";
        strOpen += "&Tab=" + tab + "&DataId=0";

        //window.showModalDialog(strOpen, "", sFeatures);
        return ShowWindowWithClose(strOpen, 'search', '75%', '85%')
        return false;


    }
    function CloseFrame() {
        jQuery.fancybox.close();
    }



    function vali_Filetype() {

        var id_value = document.getElementById('<%= FileUpload1.ClientID %>').value;

        if (id_value != '') {
            var valid_extensions = /(.xls|.xlsx|.XLS|.XLSX)$/i;

            if (valid_extensions.test(id_value)) {

                return true;
            } else {
                alert('Please upload a document in one of the following formats:  .xls,.xlsx')
                document.getElementById('<%= FileUpload1.ClientID %>').focus()
                return false;
            }
        }

    }

</script>

<script type="text/javascript">

    function openview(val) {
        window.open('TabPages/comPlainTextView.aspx?temp_id=' + val);
        return false;

    }

</script>
<script language="javascript" type="text/javascript">
    function SetSelValuetoParent(id) {
        //alert(id);
        parent.SetSelValuetoParent(id);
        return false;
    }
</script>

<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            margin: 0,
            padding: 0,
            showCloseButton: false,

            //maxHeight: 600,
            //fitToView: true,
            //padding: 6,
            //width: w,
            //height: h,
            //autoSize: false,
            //openEffect: 'none',
            //showLoading: true,
            //closeClick: true,
            //closeEffect: 'fade',
            //'closeBtn': true,
            //afterLoad: function () {
            //    this.title = '';//ShowTitle(pageTitle);
            //},
            //helpers: {
            //    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
            //    title: { type: 'inside' }
            //},
            //onComplete: function () {
            //    $("#fancybox-wrap").css({ 'top': '90px' });

            //},
            //onCleanup: function () {
            //    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

            //    if (hfPostBack == "Y")
            //        window.location.reload(true);
            //}
        });

        return false;
    }
</script>

<div>
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>


            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
            <br />
            <uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
            <br />
            <asp:Panel ID="Panel3" runat="server" Visible="false">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Step 1-Select Plain Text Template
                <asp:LinkButton ID="LinkButton3" OnClientClick="openTemplate(1); return false;" Visible="false" runat="server">Previous Excel Template(s)</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GrdEmailText" runat="server" AutoGenerateColumns="False" EmptyDataText="Email Plain Text not added yet."
                                Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Template ID">
                                        <HeaderTemplate>
                                            Template ID
                                            <br />
                                            <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                            <center>
                                    <%# Eval("EML_ID") %>
                                </center>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date
                                            <br />
                                            <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                TargetControlID="Txt2">
                                            </ajaxToolkit:CalendarExtender>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email Text">
                                        <HeaderTemplate>
                                            Email Title
                                            <br />
                                            <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkView" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                                runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>

                                            <br />
                                            Select
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkselect" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                        CommandName="send">Select</asp:LinkButton></center>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <EmptyDataRowStyle />
                                <EditRowStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Panel ID="Panel1" runat="server" Visible="True">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Step 2-Upload Excel File 
                <asp:LinkButton ID="LinkButton1" OnClientClick="openTemplate(2); return false;" runat="server" CausesValidation="false" Visible="false">Get Previous Template(s)</asp:LinkButton><asp:LinkButton
                    ID="LinkButton2" runat="server" CausesValidation="False" Visible="false">Cancel</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Title</span>
                                    </td>
                                    <td align="left" width="80%" colspan="2">
                                        <asp:TextBox ID="txttitle" runat="server" ValidationGroup="v1"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Type</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:RadioButtonList ID="RadioType" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Text="Student" Value="STUDENT" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Staff" Value="STAFF"></asp:ListItem>
                                            <asp:ListItem Text="Others" Value="OTHERS"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="left" width="50%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Excel File</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="FileUpload1" onBlur="javascript:vali_Filetype();" runat="server" />
                                    </td>
                                    <td align="center" width="50%">
                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" ValidationGroup="v1" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="title-bg-lite" align="left">Important Notes Regarding Uploading Excel Sheet
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="30%"><span class="field-label">Field Names</span>
                                                </td>

                                                <td align="left" width="70%">ID,EmailID,Name
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="30%"><span class="field-label">Sheet Name</span>
                                                </td>

                                                <td align="left" width="70%">Sheet1
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="div2">
                <asp:Panel ID="Panel2" runat="server" Visible="False">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">Step 3-Send Plain Text Email
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:LinkButton ID="lnksend" runat="server" CausesValidation="False" OnClientClick="javascript:openPopup('comSendingPlainTextEmailsExcel.aspx')"
                                    Visible="False">Proceed</asp:LinkButton>
                                <asp:LinkButton ID="lnksend2" runat="server" CausesValidation="False" OnClientClick="javascript:openPopup2('comSendingEmailNormal.aspx');  return false;"
                                    Visible="False">Proceed</asp:LinkButton>&nbsp;||
                    <asp:LinkButton ID="lnkschedule" runat="server">Schedule</asp:LinkButton>

                                ||
                    <asp:LinkButton ID="lnkcancel" runat="server" CausesValidation="False">Cancel</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <div>
                <asp:Panel ID="PanelSchedule" runat="server" CssClass="modalPopup" Style="display: none">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">Set Schedule Time
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td>Date
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtdate" runat="server" ValidationGroup="s"></asp:TextBox>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Hour
                                                </td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddhour" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Mins
                                                </td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddmins" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <asp:Button ID="btnok" runat="server" CssClass="button" OnClick="btnok_Click" Text="Ok"
                                                        ValidationGroup="s" />
                                                    <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <asp:Label ID="lblsmessage" runat="server" CssClass="error"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                                            TargetControlID="txtdate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                                            Display="None" ErrorMessage="Please Enter Date" SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                                            ShowSummary="False" ValidationGroup="s" />
                                        &nbsp;
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>




            </div>
            <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelSchedule"
                RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="lnkschedule">
            </ajaxToolkit:ModalPopupExtender>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttitle"
                Display="None" ErrorMessage="Please enter title" SetFocusOnError="True"></asp:RequiredFieldValidator>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUpload1"
                Display="None" ErrorMessage="Please upload excel file" SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUpload1"
    Display="None" ErrorMessage="Please upload a Excel document (*.xls)." SetFocusOnError="true"
    Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.xls|.xlsx|.XLS|.XLSX)$"></asp:RegularExpressionValidator>--%>
            <br />
            <asp:HiddenField ID="HiddenTempid" runat="server" />
            <asp:HiddenField ID="HiddenPathEncrypt" runat="server" />
            <asp:HiddenField ID="Hiddenlogid" runat="server" />
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />

        </Triggers>
    </asp:UpdatePanel>
</div>
