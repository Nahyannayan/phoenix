<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comSendingReports.ascx.vb" Inherits="masscom_UserControls_comSendingReports" %>
<%@ Register Src="../Reports/UserControls/SendingReportFilter.ascx" TagName="SendingReportFilter"
    TagPrefix="uc1" %>

   <script type="text/javascript" >
   
    window.setTimeout('setpath()',1000);
    
     function setpath()
        {
           var path = window.location.href
           var Rpath=path.substring(path.indexOf('?'),path.length)
           var objFrame

           //SMS
         
               objFrame = document.getElementById("F1");
               objFrame.src = "TabPages/comReportsSms.aspx" + Rpath
        

           //Email Newsletters
         
               objFrame = document.getElementById("F2");
               objFrame.src = "TabPages/comReportsNewsletter.aspx" + Rpath
         

           //Email Plain Text
         
               objFrame = document.getElementById("F3");
               objFrame.src = "TabPages/comReportsPlainText.aspx" + Rpath
          
           

        }
   
   </script> 

<div align="left" >
    
        <ajaxToolkit:TabContainer id="Tab1" Width="100%" runat="server" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="T1" runat="server">
                <ContentTemplate>
               <iframe id="F1" height="650" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                   SMS
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="T2" runat="server">
                <ContentTemplate>
                <iframe id="F2" height="650" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                 Email Newsletters
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="T3" runat="server">
                <ContentTemplate>
                <iframe id="F3" height="650" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
               Email Plain Text 
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            <ajaxToolkit:TabPanel ID="T4" runat="server">
                <ContentTemplate>
                 <uc1:SendingReportFilter ID="SendingReportFilter1" runat="server" />
                </ContentTemplate>
                <HeaderTemplate>
                   Crystal Reports
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
        </ajaxToolkit:TabContainer>
   
        <asp:HiddenField ID="HiddenTempid" runat="server" />
</div>