Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Collections.Generic
Imports System.Linq

Partial Class masscom_UserControls_comReportsSms
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            'BindGridNormal()
            BindGridScheduled()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub


    Public Sub BindGridNormal()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT SENDING_ID,TYPE,B.CMS_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('</br><span style=''color: red;font-weight:bold''> Show... </span>')tempview,CMS_FROM,CMS_SMS_TEXT," & _
                        " SENDING_ID,TOTAL_MESSAGE,SUCCESSFULLY_SEND,SENDING_FAILED, " & _
                        " START_TIME,END_TIME,B.ENTRY_DATE,ISNULL(CREDITS,'0')CREDITS,ISNULL(TOTAL_CREDITS_TAKEN,'0')TOTAL_CREDITS_TAKEN,ISNULL(TOTAL_CREDITS_LEFT,'0')TOTAL_CREDITS_LEFT," & _
                        " ISNULL(EMP_FNAME,'')+ISNULL(EMP_MNAME,'')+ISNULL(EMP_LNAME,'')EMP_NAME " & _
                        " ,'javascript:openWindow(''' + CONVERT(VARCHAR,SENDING_ID) + ''',''0'');return false;' VIEWDETAILS " & _
                        " FROM dbo.COM_MANAGE_SMS A  " & _
                        " INNER JOIN dbo.COM_EXCEL_SMS_SENDING_LOG B ON A.CMS_ID=B.CMS_ID  " & _
                        " LEFT JOIN dbo.EMPLOYEE_M C WITH(NOLOCK) ON C.EMP_ID=B.SENDING_USER_EMP_ID  " & _
                        " WHERE B.BSU_ID='" & Hiddenbsuid.Value & "'"

        If txtFromDate.Text.Trim() <> "" Then

            str_query = str_query & "AND ENTRY_DATE  >='" & txtFromDate.Text.Trim() & "'"

        End If

        If txtToDate.Text.Trim() <> "" Then

            str_query = str_query & "AND  ENTRY_DATE  <= DATEADD(dd, 1, '" & txtToDate.Text.Trim() & "')"

        End If


        str_query = str_query & " ORDER BY B.SENDING_ID DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GrdReports.DataSource = ds
        GrdReports.DataBind()
        Dim bShowAmount As Boolean = True
        If Convert.ToString(Session("BSU_COUNTRY_ID")).Trim <> "172" Then '---hide amount column of grid if not UAE.
            bShowAmount = False
        End If
        Dim columns As List(Of DataControlField) = GrdReports.Columns.Cast(Of DataControlField)().ToList()
        columns.Find(Function(col) col.HeaderText = "Total Credits Left").Visible = bShowAmount
        columns.Find(Function(col) col.HeaderText = "Credits").Visible = bShowAmount
        columns.Find(Function(col) col.HeaderText = "Total Credits Taken").Visible = bShowAmount
        columns.Find(Function(col) col.HeaderText = "Amount").Visible = bShowAmount

        If GrdReports.Rows.Count > 0 Then

            Dim Price As Double = DirectCast(GrdReports.FooterRow.FindControl("TxtAmount"), TextBox).Text.Trim()
            If Price.ToString() = "" Then
                Price = 0
                DirectCast(GrdReports.FooterRow.FindControl("TxtAmount"), TextBox).Text = 0
            End If

            Dim total = 0
            For Each row As GridViewRow In GrdReports.Rows
                Dim tct = DirectCast(row.FindControl("lbltct"), Label).Text.Trim()
                DirectCast(row.FindControl("lblAmount"), Label).Text = tct * Price & " " & Session("BSU_CURRENCY") '" AED"
                total = total + (tct * Price)
            Next

            DirectCast(GrdReports.FooterRow.FindControl("lblAmountTotal"), Label).Text = total & " " & Session("BSU_CURRENCY") ' " AED"

        End If

    End Sub

    Public Sub BindGridScheduled()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT RECORD_ID,(CASE CGR_ID WHEN 0 THEN 'Excel' ELSE 'Groups/Add on' end ) as  TYPE,B.CMS_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('</br><span style=''color: red;font-weight:bold''> Show... </span>')tempview,CMS_FROM,CMS_SMS_TEXT,TOTAL_COUNT,SUCCESSFULLY_SENT,SENT_FAILED, " & _
                        " (SELECT CONVERT(Datetime, START_TIME, 21)) AS START_TIME, (SELECT CONVERT(Datetime, END_TIME, 21)) AS END_TIME, (SELECT CONVERT(Datetime, B.ENTRY_DATE, 21)) AS ENTRY_DATE,STATUS, (SELECT CONVERT(Datetime, SCHEDULE_DATE_TIME, 21)) AS SCHEDULE_DATE_TIME, " & _
                        " ISNULL(CREDITS,'0')CREDITS,ISNULL(TOTAL_CREDITS_TAKEN,'0')TOTAL_CREDITS_TAKEN,ISNULL(TOTAL_CREDITS_LEFT,'0')TOTAL_CREDITS_LEFT, " & _
                        " ISNULL(EMP_FNAME,'')+ISNULL(EMP_MNAME,'')+ISNULL(EMP_LNAME,'')EMP_NAME  , " & _
                        " 'javascript:openWindow(''0'',''' + CONVERT(VARCHAR,RECORD_ID) + ''');return false;' VIEWDETAILS,   " & _
                        " 'javascript:openWindowActions(''' + CONVERT(VARCHAR,RECORD_ID) + ''');return false;' ACTIONS   " & _
                        " FROM dbo.COM_MANAGE_SMS A  " & _
                        " INNER JOIN dbo.COM_MANAGE_SMS_SCHEDULE B ON A.CMS_ID=B.CMS_ID  " & _
                        " LEFT JOIN dbo.EMPLOYEE_M C WITH(NOLOCK) ON C.EMP_ID=B.ENTRY_EMP_ID  " & _
                        " WHERE B.ENTRY_BSU_ID='" & Hiddenbsuid.Value & "'"

        If txtFromDate.Text.Trim() <> "" Then

            str_query = str_query & "AND ENTRY_DATE  >='" & txtFromDate.Text.Trim() & "'"

        End If

        If txtToDate.Text.Trim() <> "" Then

            str_query = str_query & "AND  ENTRY_DATE  <= DATEADD(dd, 1, '" & txtToDate.Text.Trim() & "')"

        End If


        str_query = str_query & "  ORDER BY B.RECORD_ID DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridScheduled.DataSource = ds
        GridScheduled.DataBind()
        Dim bShowAmount As Boolean = True
        If Convert.ToString(Session("BSU_COUNTRY_ID")).Trim <> "172" Then '---hide amount column of grid if not UAE.
            bShowAmount = False
        End If
        Dim columns As List(Of DataControlField) = GridScheduled.Columns.Cast(Of DataControlField)().ToList()
        columns.Find(Function(col) col.HeaderText = "Total Credits Left").Visible = bShowAmount
        columns.Find(Function(col) col.HeaderText = "Credits").Visible = bShowAmount
        columns.Find(Function(col) col.HeaderText = "Total Credits Taken").Visible = bShowAmount
        columns.Find(Function(col) col.HeaderText = "Amount").Visible = bShowAmount

        If GridScheduled.Rows.Count > 0 Then

            Dim Price As Double = DirectCast(GridScheduled.FooterRow.FindControl("TxtAmount"), TextBox).Text.Trim()
            If Price.ToString() = "" Then
                Price = 0
                DirectCast(GridScheduled.FooterRow.FindControl("TxtAmount"), TextBox).Text = 0
            End If

            Dim total = 0
            For Each row As GridViewRow In GridScheduled.Rows
                Dim tct = DirectCast(row.FindControl("lbltct"), Label).Text.Trim()
                DirectCast(row.FindControl("lblAmount"), Label).Text = tct * Price & " " & Session("BSU_CURRENCY") ' " AED"
                total = total + (tct * Price)
            Next

            DirectCast(GridScheduled.FooterRow.FindControl("lblAmountTotal"), Label).Text = total & " " & Session("BSU_CURRENCY") ' " AED"

        End If

    End Sub

    Protected Sub GrdReports_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdReports.PageIndexChanging
        GrdReports.PageIndex = e.NewPageIndex
        BindGridNormal()
    End Sub

    Protected Sub BtnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If GrdReports.Rows.Count > 0 Then

            Dim Price As Double = DirectCast(GrdReports.FooterRow.FindControl("TxtAmount"), TextBox).Text.Trim()
            If Price.ToString() = "" Then
                Price = 0
                DirectCast(GrdReports.FooterRow.FindControl("TxtAmount"), TextBox).Text = 0
            End If

            Dim total = 0
            For Each row As GridViewRow In GrdReports.Rows
                Dim tct = DirectCast(row.FindControl("lbltct"), Label).Text.Trim()
                DirectCast(row.FindControl("lblAmount"), Label).Text = tct * Price & " " & Session("BSU_CURRENCY") ' " AED"
                total = total + (tct * Price)
            Next

            DirectCast(GrdReports.FooterRow.FindControl("lblAmountTotal"), Label).Text = total & " " & Session("BSU_CURRENCY") ' " AED"

        End If
    End Sub
    Protected Sub BtnCalculate2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If GridScheduled.Rows.Count > 0 Then

            Dim Price As Double = DirectCast(GridScheduled.FooterRow.FindControl("TxtAmount"), TextBox).Text.Trim()
            If Price.ToString() = "" Then
                Price = 0
                DirectCast(GridScheduled.FooterRow.FindControl("TxtAmount"), TextBox).Text = 0
            End If

            Dim total = 0
            For Each row As GridViewRow In GridScheduled.Rows
                Dim tct = DirectCast(row.FindControl("lbltct"), Label).Text.Trim()
                DirectCast(row.FindControl("lblAmount"), Label).Text = tct * Price & " " & Session("BSU_CURRENCY") ' " AED"
                total = total + (tct * Price)
            Next

            DirectCast(GridScheduled.FooterRow.FindControl("lblAmountTotal"), Label).Text = total & " " & Session("BSU_CURRENCY") ' " AED"

        End If

    End Sub

    Protected Sub btnrefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnrefresh.Click
        BindGridScheduled()
    End Sub

    Protected Sub GridScheduled_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridScheduled.PageIndex = e.NewPageIndex
        BindGridScheduled()
    End Sub

    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        BindGridNormal()
        BindGridScheduled()
    End Sub

    Protected Sub btnrefresh2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGridNormal()
    End Sub


    Public Sub ExportSmsNormal()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT SENDING_ID,TYPE,CMS_FROM AS MESSAGE_FROM ,CMS_SMS_TEXT AS MESSAGE," & _
                        " TOTAL_MESSAGE,SUCCESSFULLY_SEND,SENDING_FAILED, " & _
                        " START_TIME,END_TIME,ISNULL(TOTAL_CREDITS_TAKEN,'0')TOTAL_CREDITS_TAKEN," & _
                        " ISNULL(EMP_FNAME,'')+ISNULL(EMP_MNAME,'')+ISNULL(EMP_LNAME,'')SENT_BY " & _
                        " FROM dbo.COM_MANAGE_SMS A  " & _
                        " INNER JOIN dbo.COM_EXCEL_SMS_SENDING_LOG B ON A.CMS_ID=B.CMS_ID  " & _
                        " LEFT JOIN dbo.EMPLOYEE_M C ON C.EMP_ID=B.SENDING_USER_EMP_ID  " & _
                        " AND B.BSU_ID='" & Hiddenbsuid.Value & "'"

        Dim Grid As New GridView
        Grid.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Grid.DataBind()

        ExportGridToExcel(Grid, "SMS Report")
        ' Create excel file.
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'Dim dt As DataTable
        'dt = ds.Tables(0)

        'GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        'Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        'Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        'ws.InsertDataTable(dt, "A1", True)
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        'ef.SaveXls(Response.OutputStream)

    End Sub
    Public Sub ExportGridToExcel(ByVal GrdView As GridView, ByVal fileName As String)
        Response.Clear()
        Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", fileName))
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New StringWriter()
        Dim htmlWrite As New HtmlTextWriter(stringWrite)
        GrdView.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub


    Protected Sub btnnormalexport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ExportSmsNormal()
    End Sub

End Class
