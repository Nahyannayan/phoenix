<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsNewsLetters.ascx.vb"
    Inherits="masscom_UserControls_comAssignGroupsNewsLetters" %>

<script type="text/javascript">

    function change_chk_state(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {
                //if (document.forms[0].elements[i].type=='checkbox' )
                //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click(); //fire the click event of the child element
            }
        }
    }

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false;

    }

</script>


<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>

<asp:Label ID="lblmessage" runat="server" ForeColor="Red" CssClass="matters"></asp:Label><br />
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div class="matters">
                <asp:Panel ID="Panel3" runat="server">
                    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
                        <tr>
                            <td class="subheader_img">
                                Step 1-Select News Letter
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GrdNewsletterView" runat="server" EmptyDataText="No News letters added yet."
                                    AutoGenerateColumns="False" Width="100%" AllowPaging="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Template ID">
                                            <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            Template ID
                                                            <br />
                                                            <asp:TextBox ID="Txt1" Width="50px" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                                ImageUrl="~/Images/forum_search.gif" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                                <asp:HiddenField ID="HiddenFieldFileName" runat="server" Value='<%# Eval("EML_NEWS_LETTER_FILE_NAME") %>' />
                                                <center>
                                                    <%# Eval("EML_ID") %></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            Date
                                                            <br />
                                                            <asp:TextBox ID="Txt2" Width="100px" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" CausesValidation="false"
                                                                ImageUrl="~/Images/forum_search.gif" />
                                                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                                TargetControlID="Txt2">
                                                            </ajaxToolkit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NewsLetter">
                                            <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            NewsLetter
                                                            <br />
                                                            <asp:TextBox ID="Txt3" Width="100px" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" CausesValidation="false" 
                                                                ImageUrl="~/Images/forum_search.gif" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnknewsletter" runat="server" CausesValidation="false" OnClientClick=' <%# Eval("openview") %>'
                                                    Text=' <%# Eval("EML_NEWS_LETTER_FILE_NAME") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                          
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Send">
                                            <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            <br />
                                                            Send
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <asp:LinkButton ID="Lnksend" CommandArgument='<%# Eval("EML_ID") %>' CausesValidation="false"
                                                        CommandName="sendNews" runat="server">Select</asp:LinkButton></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="Panel1" Visible="false" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center">
                                <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
                                    <tr>
                                        <td class="subheader_img">
                                            Step 2-Select Group&nbsp;&nbsp;
                                            <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACD_ID_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GrdGroups" runat="server" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                AllowPaging="True" AutoGenerateColumns="false" Width="100%" OnPageIndexChanging="GrdGroups_PageIndexChanging">
                                                <RowStyle CssClass="griditem" Height="25px" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        All
                                                                        <br />
                                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                            ToolTip="Click here to select/deselect all rows" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <center>
                                                                <asp:CheckBox ID="ch1" Text="" runat="server" /></center>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Group Id">
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        Group Id
                                                                        <br />
                                                                        <asp:TextBox ID="Txt1" Width="40px" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                                            ImageUrl="~/Images/forum_search.gif" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="Hiddengrpid" Value='<%#Eval("CGR_ID")%>' runat="server" />
                                                          <center> <%#Eval("CGR_ID")%></center>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Group Name">
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        Group Name
                                                                        <br />
                                                                        <asp:TextBox ID="Txt2" Width="60px" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" CausesValidation="false"
                                                                            ImageUrl="~/Images/forum_search.gif" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%#Eval("CGR_DES")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type">
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        Type
                                                                        <br />
                                                                        <asp:DropDownList ID="DropSearch1" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                                            runat="server">
                                                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                                            <asp:ListItem Value="STUDENT" Text="STUDENT"></asp:ListItem>
                                                                            <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                         <center>   <%#Eval("CGR_TYPE")%></center>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Group Type">
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        Group Type
                                                                        <br />
                                                                        <asp:DropDownList ID="DropSearch2" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                                            runat="server">
                                                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                                            <asp:ListItem Value="GRP" Text="Group"></asp:ListItem>
                                                                            <asp:ListItem Value="AON" Text="Add On"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="Hiddentype" Value='<%#Eval("gtype")%>' runat="server" />
                                                        <center>    <%#Eval("gtype")%></center>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Business Unit">
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        <br />
                                                                        Business Unit
                                                                        <br />
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="Hiddengrpbsu" Value='<%#Eval("cgr_grp_bsu_id")%>' runat="server" />
                                                            <%#Eval("BSU_NAME")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Academic Year">
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        <br />
                                                                        Academic Year
                                                                        <br />
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <center>
                                                                <%#Eval("ACY_DESCR")%></center>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Academic Year">
                                                        <HeaderTemplate>
                                                            <table class="BlueTable" width="100%">
                                                                <tr class="matterswhite">
                                                                    <td align="center" colspan="2">
                                                                        <br />
                                                                        Contact
                                                                        <br />
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <center>
                                                                <%#Eval("CGR_CONTACT_C")%></center>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>

                                                </Columns>
                                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                <EmptyDataRowStyle Wrap="False" />
                                                <EditRowStyle Wrap="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:Button ID="btncontinue" runat="server" CssClass="button" 
                                    Text="Continue" CausesValidation="False" Width="100px" />
                                <asp:Button ID="btnback" runat="server" CssClass="button" Text="Back" Width="100px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Do you wish to continue?"
                                            TargetControlID="btncontinue" runat="server">
                                        </ajaxToolkit:ConfirmButtonExtender>
                <asp:HiddenField ID="Hiddensmsid" runat="server" />
                <asp:HiddenField ID="HiddenTempid" runat="server" />
                &nbsp;
                <asp:HiddenField ID="Hiddenbsuid" runat="server" />
            </div>
        </td>
    </tr>
</table>
