Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class masscom_UserControls_comListGroups
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            CheckMenuRights()
            BindBsu()
            acadamicyear_bind()
            BindGrid()
        End If
        AssignRights()
    End Sub
    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
        directory.Add("Add", lnkaddnew)
        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))


        For Each row As GridViewRow In GrdGroups.Rows
            Dim editlink As LinkButton = DirectCast(row.FindControl("lnkview"), LinkButton)
            Dim viewlink As LinkButton = DirectCast(row.FindControl("lnklist"), LinkButton)
            Dim viewdelete As LinkButton = DirectCast(row.FindControl("lnkDelete"), LinkButton)

            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Edit", editlink)
            directory2.Add("Delete", viewdelete)
            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub

    Public Function SelectContact(ByVal GroupId As String, ByVal Contact As String) As String
        Dim Returnvalue As String = ""

        If Contact <> "" Then

            Dim val As String() = Contact.Split(",")

            Dim i = 0

            If val.Length > 0 Then
                Returnvalue = ""
            End If

            For i = 0 To val.Length - 1
                If Returnvalue = "" Then
                    Returnvalue = "'" & val(i) & "'"
                Else
                    Returnvalue &= ",'" & val(i) & "'"
                End If

            Next

        Else
            ''Select using Groups

        End If

        If Returnvalue <> "" Then

            Returnvalue = " AND CONTACT IN (" & Returnvalue & ")"

        End If


        Return Returnvalue
    End Function

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT * FROM ( select * , " & _
                       " CASE CGR_TYPE WHEN 'STUDENT' THEN (select ACY_DESCR from dbo.ACADEMICYEAR_M where ACY_ID=(select top 1 CGA_ACY_ID from dbo.COM_GROUPS_A where CGA_CGR_ID= cgr_id)) ELSE '' END ACY_DESCR, " & _
                       " (case a.cgr_grp_type when 'GRP' then 'Group' else 'Add On' end)as gtype, " & _
                       " ( case a.cgr_grp_bsu_id when '0' then 'All' else (select c.BSU_SHORTNAME from BUSINESSUNIT_M c where  c.BSU_ID= a.cgr_grp_bsu_id ) end ) bsu_name,  " & _
                       " isnull(CGR_CONTACT,'P') CGR_CONTACT_C " & _
                       " from com_groups_m a ) AA  where CGR_BSU_ID='" & ddlSchool.SelectedValue & "' and  isnull(CGR_DELETED,'false') ='false' "

        Dim Txt1 As String
        Dim Txt2 As String
        Dim DropSearch1 As String
        Dim DropSearch2 As String

        If GrdGroups.Rows.Count > 0 Then
            Txt1 = DirectCast(GrdGroups.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            Txt2 = DirectCast(GrdGroups.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            DropSearch1 = DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue
            DropSearch2 = DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(CGR_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt2.Trim() <> "" Then
                str_query &= " and replace(CGR_DES,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
            End If

            If DropSearch1.Trim() <> "0" Then
                str_query &= " and replace(CGR_TYPE,' ','') like '%" & DropSearch1.Replace(" ", "") & "%' "
            End If

            If DropSearch2.Trim() <> "0" Then
                str_query &= " and replace(cgr_grp_type,' ','') like '%" & DropSearch2.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " and CGR_BSU_ID='" & ddlSchool.SelectedValue & "'"

        If ddlACD_ID.Items.Count > 0 Then

            str_query &= " and (ACY_DESCR='" & ddlACD_ID.SelectedItem.Text & "' or ACY_DESCR='') "

        End If

        str_query &= " order by cgr_id desc"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("CGR_ID")
            dt.Columns.Add("CGR_DES")
            dt.Columns.Add("CGR_TYPE")
            dt.Columns.Add("gtype")
            dt.Columns.Add("cgr_grp_bsu_id")
            dt.Columns.Add("BSU_NAME")
            dt.Columns.Add("ACY_DESCR")
            dt.Columns.Add("CGR_CONTACT_C")

            Dim dr As DataRow = dt.NewRow()
            dr("CGR_ID") = ""
            dr("CGR_DES") = ""
            dr("CGR_TYPE") = ""
            dr("gtype") = ""
            dr("cgr_grp_bsu_id") = ""
            dr("BSU_NAME") = ""
            dr("ACY_DESCR") = ""
            dr("CGR_CONTACT_C") = ""

            dt.Rows.Add(dr)
            GrdGroups.DataSource = dt
            GrdGroups.DataBind()

            DirectCast(GrdGroups.Rows(0).FindControl("lnkview"), LinkButton).Visible = False
            DirectCast(GrdGroups.Rows(0).FindControl("lnklist"), LinkButton).Visible = False
            DirectCast(GrdGroups.Rows(0).FindControl("lnkDelete"), LinkButton).Visible = False
            DirectCast(GrdGroups.Rows(0).FindControl("lnkExportMobile"), LinkButton).Visible = False
            DirectCast(GrdGroups.Rows(0).FindControl("lnkExportEmail"), LinkButton).Visible = False
        Else


            GrdGroups.DataSource = ds
            GrdGroups.DataBind()

        End If

        If GrdGroups.Rows.Count > 0 Then

            DirectCast(GrdGroups.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GrdGroups.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue = DropSearch1
            DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue = DropSearch2

        End If

    End Sub
    Protected Sub ddlsearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub
    Protected Sub GrdGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdGroups.RowCommand
        Dim EncDec As New Encryption64
        Dim Groupid As String = e.CommandArgument
        Groupid = EncDec.Encrypt(Groupid)

        If e.CommandName = "Edit" Then
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & EncDec.Encrypt("edit") 'Request.QueryString("datamode").ToString()
            Response.Redirect("comAssignGroupsEdit.aspx?Groupid=" & Groupid & mInfo)
        End If
        If e.CommandName = "list" Then
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & EncDec.Encrypt("view") '& Request.QueryString("datamode").ToString()
            Session("membersview") = "groups"
            Response.Redirect("comGroupMembers.aspx?Groupid=" & Groupid & mInfo)
        End If
        If e.CommandName = "Deleted" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "UPDATE COM_GROUPS_M SET CGR_DELETED='True' where CGR_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            BindGrid()
        End If

        If e.CommandName = "ExportMobile" Then

            Export(e.CommandArgument, "M")

        End If

        If e.CommandName = "ExportEmail" Then

            Export(e.CommandArgument, "E")

        End If



        If e.CommandName = "search" Then
            BindGrid()
        End If
    End Sub

    Public Sub Export(ByVal groupid As String, ByVal DataOption As String)
        ''DataOption M-Mobile , E-Email 
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query = ""
        Dim ds As DataSet
        sql_query = "select CGR_CONTACT,CGR_GRP_TYPE,CGR_CONDITION,CGR_TYPE,CGR_REMOVE_EMAIL_IDS,CGR_REMOVE_SMS_IDS from COM_GROUPS_M where CGR_ID='" & groupid & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        Dim condition As String = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
        Dim grptype As String = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()
        Dim contact As String = ds.Tables(0).Rows(0).Item("CGR_CONTACT").ToString()
        Dim type As String = ds.Tables(0).Rows(0).Item("CGR_TYPE").ToString()

        Dim dt As New DataTable()

        If grptype = "GRP" Then  ''Group 

            If type = "STUDENT" Then

                If DataOption = "M" Then
                    If Session("BSU_COUNTRY_ID") = "172" Then
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('971'+ right(replace(Mobile,'-',''),9),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR,BCode as BSU_Code,BName as BSU_Name from ( " & condition & " ) a  " & _
                                    " INNER JOIN vv_students B ON B.STU_NO = UNIQUEID where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                    ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('91'+ right(replace(Mobile,'-',''),10),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR,BCode as BSU_Code,BName as BSU_Name from ( " & condition & " ) a  " & _
                                   " INNER JOIN vv_students B ON B.STU_NO = UNIQUEID where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                    ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('002'+ right(replace(Mobile,'-',''),11),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR,BCode as BSU_Code,BName as BSU_Name from ( " & condition & " ) a  " & _
                                   " INNER JOIN vv_students B ON B.STU_NO = UNIQUEID where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                    ElseIf Session("BSU_COUNTRY_ID") = "47" Then
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('012'+ right(replace(Mobile,'-',''),7),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR,BCode as BSU_Code,BName as BSU_Name  from ( " & condition & " ) a  " & _
                                    " INNER JOIN vv_students B ON B.STU_NO = UNIQUEID where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                    End If
                Else
                    condition = " select isnull(uniqueid,'') as ID , Name, isnull(Email,'') as EmailID,GRM_DISPLAY AS GRADE,SCT_DESCR ,BCode as BSU_Code,BName as BSU_Name  from ( " & condition & " ) a INNER JOIN vv_students B ON B.STU_NO = UNIQUEID where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString() & " "
                End If

            Else

                If DataOption = "M" Then
                    If Session("BSU_COUNTRY_ID") = "172" Then
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('971'+ right(replace(Mobile,'-',''),9),'') as MobileNo   from ( " & condition & " ) a  where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "
                    ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('91'+ right(replace(Mobile,'-',''),10),'') as MobileNo   from ( " & condition & " ) a  where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                    ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('002'+ right(replace(Mobile,'-',''),11),'') as MobileNo   from ( " & condition & " ) a  where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                    Else
                        condition = " select isnull(uniqueid,'') as ID , Name, isnull('012'+ right(replace(Mobile,'-',''),7),'') as MobileNo   from ( " & condition & " ) a  where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString() & " "

                    End If


                Else
                    condition = " select isnull(uniqueid,'') as ID , Name, isnull(Email,'') as EmailID   from ( " & condition & " ) a  where 1=1 " & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString() & " "

                End If

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, condition)

        End If

        If grptype = "AON" Then ''Add On 

            If DataOption = "M" Then
                If Session("BSU_COUNTRY_ID") = "172" Then
                    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and 1=1 " & SelectContact("", contact) & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('971'+ right(replace(Mobile,'-',''),9),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR ,BCode as BSU_Code,BName as BSU_Name  from ( " & condition & " ) a INNER JOIN vv_students B ON B.STU_NO = UNIQUEID "
                ElseIf Session("BSU_COUNTRY_ID") = "6" Then
                    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and 1=1 " & SelectContact("", contact) & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('91'+ right(replace(Mobile,'-',''),10),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR ,BCode as BSU_Code,BName as BSU_Name  from ( " & condition & " ) a INNER JOIN vv_students B ON B.STU_NO = UNIQUEID "
                ElseIf Session("BSU_COUNTRY_ID") = "217" Then
                    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and 1=1 " & SelectContact("", contact) & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('002'+ right(replace(Mobile,'-',''),11),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR ,BCode as BSU_Code,BName as BSU_Name  from ( " & condition & " ) a INNER JOIN vv_students B ON B.STU_NO = UNIQUEID "

                Else
                    condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and 1=1 " & SelectContact("", contact) & ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()
                    sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull('012'+ right(replace(Mobile,'-',''),7),'') as MobileNo,GRM_DISPLAY AS GRADE,SCT_DESCR ,BCode as BSU_Code,BName as BSU_Name  from ( " & condition & " ) a INNER JOIN vv_students B ON B.STU_NO = UNIQUEID "


                End If

            Else

                condition = "select * from (" & condition & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid  and CGAO_CGR_ID='" & groupid & "' and 1=1 " & SelectContact("", contact) & ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString()
                sql_query = " select  isnull(uniqueid,'') as ID , Name, isnull(Email,'') as EmailID,GRM_DISPLAY AS GRADE,SCT_DESCR,BCode as BSU_Code,BName as BSU_Name  from ( " & condition & " ) a INNER JOIN vv_students B ON B.STU_NO = UNIQUEID "

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)


        End If


        dt = ds.Tables(0)

        ' Create excel file.


        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()

        Dim pathSave As String
        pathSave = Session("EmployeeId") + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

        ef.Save(cvVirtualPath & "GroupMembers\" + pathSave)

        Dim path = cvVirtualPath & "\GroupMembers\" + pathSave

        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()


    End Sub


    Protected Sub lnkaddnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkaddnew.Click
        Dim EncDec As New Encryption64
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & EncDec.Encrypt("add")  ''& Request.QueryString("datamode").ToString()
        Response.Redirect("comAssignGroups.aspx" & mInfo)
    End Sub

    Protected Sub GrdGroups_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

        GrdGroups.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Public Sub BindBsu()

        Try

            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                                 CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
                                 & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
            ddlSchool.DataSource = ds.Tables(0)
            ddlSchool.DataTextField = "BSU_NAME"
            ddlSchool.DataValueField = "BSU_ID"
            ddlSchool.DataBind()
            ddlSchool.SelectedIndex = -1
            If Not ddlSchool.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                ddlSchool.Items.FindByValue(Session("sBsuid")).Selected = True
                ddlSchool_SelectedIndexChanged(ddlSchool, Nothing)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Sub

    Sub acadamicyear_bind()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedItem.Value
            Dim currACY_ID As String = String.Empty
            str_Sql = "SELECT  distinct   ACADEMICYEAR_D.ACD_ACY_ID, ACADEMICYEAR_M.ACY_DESCR," & _
" (select top 1 ACD_ACY_ID from ACADEMICYEAR_D where ACD_BSU_ID = '" & BSU_ID & "' and acd_current=1) as CurrACY_ID " & _
" FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID " & _
" = ACADEMICYEAR_M.ACY_ID WHERE(ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')" & _
" ORDER BY ACADEMICYEAR_D.ACD_ACY_ID "

            Using readerAcademic As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerAcademic.HasRows = True Then
                    While readerAcademic.Read
                        ddlACD_ID.Items.Add(New ListItem(readerAcademic("ACY_DESCR"), readerAcademic("ACD_ACY_ID")))
                        currACY_ID = readerAcademic("CurrACY_ID")
                    End While
                End If
            End Using
            ddlACD_ID.ClearSelection()
            ddlACD_ID.Items.FindByValue(currACY_ID).Selected = True
            ddlACD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        If ddlACD_ID.Items.Count > 0 Then
            ddlACD_ID.Visible = True
        Else
            ddlACD_ID.Visible = False
        End If
    End Sub

    Protected Sub ddlSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        acadamicyear_bind()
        BindGrid()
    End Sub

    Protected Sub ddlACD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub

End Class
