﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsUpdate.ascx.vb" Inherits="masscom_UserControls_comAssignGroupsUpdate" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->
       <script type="text/javascript" >

           window.setTimeout('setpathExt()', 100);

           function setpathExt() {
               var path = window.location.href
               var Rpath = path.substring(path.indexOf('?'), path.length)
               var objFrame

               //Student
               objFrame = document.getElementById("FEx1");
               objFrame.src = "TabPages/comAssignGroupsStudentsUpdate.aspx" + Rpath


               //Staff
               objFrame = document.getElementById("FEx2");
               objFrame.src = "TabPages/comAssignGroupsStaffsUpdate.aspx" + Rpath

           }

   </script> 
    
<div>
    <div align="left" >

        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                   <iframe id="FEx1" height="1100" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
                 
                </ContentTemplate>
                <HeaderTemplate>
                  Student
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                     <iframe id="FEx2" height="1100" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
                   
                </ContentTemplate>
                <HeaderTemplate>
                   Staff
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
     
        
    </div>
</div>
