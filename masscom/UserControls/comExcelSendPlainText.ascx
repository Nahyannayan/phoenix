<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSendPlainText.ascx.vb"
    Inherits="masscom_UserControls_comExcelSendPlainText" %>
<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<link href="~/cssfiles/title.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    function openPopup(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 1024px; ";
        sFeatures += "dialogHeight: 768px; ";


        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
        strOpen += "&EType=PT";
        strOpen += "&templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
        strOpen += "&method=0";
        document.getElementById('div2').style.display = 'none';
        window.showModalDialog(strOpen, "", sFeatures);



    }
    function openPopup2(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 500px; ";
        sFeatures += "dialogHeight: 500px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
        strOpen += "&EType=PT";
        strOpen += "&templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
        strOpen += "&method=0";
        document.getElementById('div2').style.display = 'none';

        window.showModalDialog(strOpen, "", sFeatures);


    }
    function openTemplate(tab) {


        var sFeatures;
        var strOpen;

        strOpen = 'comExcelEmailData.aspx'

        sFeatures = "dialogWidth: 1024px; ";
        sFeatures += "dialogHeight: 768px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
        strOpen += "&EType=PT";
        strOpen += "&Tab=" + tab + "&DataId=0";

        window.showModalDialog(strOpen, "", sFeatures);


    }
    function vali_Filetype() {

        var id_value = document.getElementById('<%= FileUpload1.ClientID %>').value;

        if (id_value != '') {
            var valid_extensions = /(.xls|.xlsx|.XLS|.XLSX)$/i;

            if (valid_extensions.test(id_value)) {

                return true;
            } else {
                alert('Please upload a document in one of the following formats:  .xls,.xlsx')
                document.getElementById('<%= FileUpload1.ClientID %>').focus()
                return false;
            }
        }

    }
    
</script>

<script type="text/javascript">

    function openview(val) {
        window.open('TabPages/comPlainTextView.aspx?temp_id=' + val);
        return false;

    }

</script>

<asp:Label ID="lblmessage" runat="server" CssClass="matters" ForeColor="Red"></asp:Label>
<br />
<uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
<br />
<asp:Panel ID="Panel3" runat="server">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Step 1-Select Plain Text Template&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:LinkButton ID="LinkButton3" OnClientClick="openTemplate(1); return false;" runat="server">Previous Excel Template(s)</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GrdEmailText" runat="server" AutoGenerateColumns="False" EmptyDataText="Email Plain Text not added yet."
                    Width="100%" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Template ID">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Template ID
                                            <br />
                                            <asp:TextBox ID="Txt1" Width="40px" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                <center>
                                    <%# Eval("EML_ID") %>
                                </center>
                            </ItemTemplate>
                            <ItemStyle Width="40px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Date
                                            <br />
                                            <asp:TextBox ID="Txt2" Width="60px" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                TargetControlID="Txt2">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center>
                            </ItemTemplate>
                            <ItemStyle Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email Text">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Email Title
                                            <br />
                                            <asp:TextBox ID="Txt3" Width="100px" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkView" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                    runat="server"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Select">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            <br />
                                            Select
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="lnkselect" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                        CommandName="send">Select</asp:LinkButton></center>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<asp:Panel ID="Panel1" runat="server" Visible="False">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Step 2-Upload Excel File &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:LinkButton ID="LinkButton1" OnClientClick="openTemplate(2); return false;" runat="server">Get Previous Template(s)</asp:LinkButton>||<asp:LinkButton
                    ID="LinkButton2" runat="server" CausesValidation="False">Cancel</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table class="matters">
                    <tr>
                        <td align="left">
                            Title
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Type
                        </td>
                        <td align="left">
                            <asp:RadioButtonList ID="RadioType" RepeatDirection="Horizontal" runat="server">
                                <asp:ListItem Text="Student" Value="STUDENT" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Staff" Value="STAFF"></asp:ListItem>
                                <asp:ListItem Text="Others" Value="OTHERS"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Excel File
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload1" onBlur="javascript:vali_Filetype();" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />
                        </td>
                    </tr>
                </table>
                <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" class="matters"
                    width="700">
                    <tr>
                        <td class="subheader_img">
                            Important Notes Regarding Uploading Excel Sheet
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        Field Names
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        ID,EmailID,Name
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Sheet Name
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        Sheet1
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<div id="div2">
    <asp:Panel ID="Panel2" runat="server" Visible="False">
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
            <tr>
                <td class="subheader_img">
                    Step 3-Send Plain Text Email
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:LinkButton ID="lnksend" runat="server" CausesValidation="False" OnClientClick="javascript:openPopup('comSendingPlainTextEmailsExcel.aspx')"
                        Visible="False">Proceed</asp:LinkButton>
                    <asp:LinkButton ID="lnksend2" runat="server" CausesValidation="False" OnClientClick="javascript:openPopup2('comSendingEmailNormal.aspx');  return false;"
                        Visible="False">Proceed</asp:LinkButton>&nbsp;||
                    <asp:LinkButton ID="lnkschedule" runat="server">Schedule</asp:LinkButton>

                    ||
                    <asp:LinkButton ID="lnkcancel" runat="server" CausesValidation="False">Cancel</asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<div class="matters">
    <asp:Panel ID="PanelSchedule" runat="server" CssClass="modalPopup" Style="display: none">
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
            <tr>
                <td class="subheader_img">
                    Set Schedule Time
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        Date
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="s"></asp:TextBox>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Hour
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddhour" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mins
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddmins" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:Button ID="btnok" runat="server" CssClass="button" OnClick="btnok_Click" Text="Ok"
                                            ValidationGroup="s" Width="80px" />
                                        <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" Width="80px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:Label ID="lblsmessage" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                                TargetControlID="txtdate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                                Display="None" ErrorMessage="Please Enter Date" SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                                ShowSummary="False" ValidationGroup="s" />
                            &nbsp;
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelSchedule"
    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="lnkschedule">
</ajaxToolkit:ModalPopupExtender>
<br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttitle"
    Display="None" ErrorMessage="Please enter title" SetFocusOnError="True"></asp:RequiredFieldValidator>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUpload1"
    Display="None" ErrorMessage="Please upload excel file" SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" />
<%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUpload1"
    Display="None" ErrorMessage="Please upload a Excel document (*.xls)." SetFocusOnError="true"
    Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.xls|.xlsx|.XLS|.XLSX)$"></asp:RegularExpressionValidator>--%>
<br />
<asp:HiddenField ID="HiddenTempid" runat="server" />
<asp:HiddenField ID="HiddenPathEncrypt" runat="server" />
<asp:HiddenField ID="Hiddenlogid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />

