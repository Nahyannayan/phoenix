Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comAssignGroupsStudentsEdit
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Hiddenbsuid.Value = Session("sBsuid")
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                CheckMenuRights()

                Dim Encr_decrData As New Encryption64
                Dim Groupid = Encr_decrData.Decrypt(Request.QueryString("Groupid").Replace(" ", "+"))
                HiddenGroupid.Value = Groupid

                Dim str_query = "Select CGR_TYPE from COM_GROUPS_M where CGR_ID='" & Groupid & "'"
                Dim type = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

                If type = "STUDENT" Then
                    Hiddenacyid.Value = Session("Current_ACY_ID")
                    Hiddenacdid.Value = Session("Current_ACD_ID")
                    Hiddenbsuid.Value = Hiddenbsuid.Value
                    BindPartAControls()

                End If

            Else

                BindPartBControls()

            End If
            parthdr_id.Visible = False
            partdtl_id.Visible = False

        Catch ex As Exception
            lblmessage.Text = "Error" & ex.Message
        End Try
        AssignRights()
    End Sub
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        'Dim Encr_decrData As New Encryption64
        'Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Encr_decrData.Encrypt("none") ''& Request.QueryString("datamode").ToString()
        'Response.Redirect("../comListGroups.aspx" & mInfo)
        Dim jsFunc As String = ""
        jsFunc = "SetCloseNewGRPValuetoParent('Back')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
    End Sub
    Protected Sub OnCheckBox_Changed(sender As Object, e As EventArgs)

        Dim flag As Boolean = False
        For Each item As ListItem In CheckContact.Items
            If item.Selected Then
                If item.Value = "F" Or item.Value = "M" Or item.Value = "G" Then
                    flag = True
                Else
                    flag = False
                End If
            End If
        Next

        If flag Then
            CheckContact.Items.FindByValue("P").Selected = False
        Else
            CheckContact.Items.FindByValue("P").Selected = True
        End If

    End Sub
    Public Function SetNodeValue(ByVal ParentNode As TreeNode, ByVal sectionarray As String()) As String
        Dim i = 0

        For i = 0 To sectionarray.Length - 1
            Dim expand = 0
            For Each node As TreeNode In ParentNode.ChildNodes
                Dim nodevalue = sectionarray(i).ToString().Trim().Replace("'", "")
                If node.Value = nodevalue Then
                    node.Checked = True
                    expand = 1
                End If
            Next
            If expand = 1 Then
                ParentNode.Expand()
            End If

        Next



    End Function

    Public Sub BindPartAControls()
        Try
            Dim Encr_decrData As New Encryption64
            Dim Groupid = Encr_decrData.Decrypt(Request.QueryString("Groupid").Replace(" ", "+"))

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "Select * from COM_GROUPS_M where CGR_ID='" & Groupid & "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then

                ''Bind Main Data
                txtgroups.Text = ds.Tables(0).Rows(0).Item("CGR_DES").ToString()
                Dim type = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()
                Dim AddOnQuery = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
                Dim contact As String() = ds.Tables(0).Rows(0).Item("CGR_CONTACT").ToString().Split(",")

                Dim falg = 0
                Dim j = 0

                For j = 0 To contact.Length - 1
                    If contact(j) = "P" Then
                        CheckContact.Items(0).Selected = True
                        falg = 1
                    End If
                    If contact(j) = "F" Then
                        CheckContact.Items(1).Selected = True
                        falg = 1
                    End If
                    If contact(j) = "M" Then
                        CheckContact.Items(2).Selected = True
                        falg = 1
                    End If
                    If contact(j) = "G" Then
                        CheckContact.Items(3).Selected = True
                        falg = 1
                    End If
                Next


                If falg = 0 Then
                    CheckContact.Items(0).Selected = True
                End If

                Hiddenquery.Value = AddOnQuery

                BindBsu()

                ddlSchool.SelectedValue = ds.Tables(0).Rows(0).Item("CGR_GRP_BSU_ID").ToString()

                If type = "GRP" Then
                    RadioButtonList1.SelectedValue = "0" ' Group
                    Panel1.Visible = True
                    Panel2.Visible = False
                Else
                    RadioButtonList1.SelectedValue = "1" ''Add on
                    Panel2.Visible = True
                    Panel1.Visible = False
                    btnsearch.Visible = False
                    btnadonCancel.Visible = True
                    btnadonSave.Visible = True
                    GrdAddOn.Visible = True
                    'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, AddOnQuery)
                    'GrdAddOn.DataSource = ds
                    'GrdAddOn.DataBind()
                    BindGrid()

                    str_query = "Select CGAO_UNIQUE_ID from COM_GROUPS_ADD_ON where CGAO_CGR_ID='" & Groupid & "'"
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)



                    Dim hash As New Hashtable

                    Session("hashtable") = Nothing

                    Dim i = 0
                    For i = 0 To ds.Tables(0).Rows.Count - 1

                        Dim key = ds.Tables(0).Rows(i).Item("CGAO_UNIQUE_ID").ToString()
                        If hash.ContainsKey(key) Then
                        Else
                            hash.Add(key, key)
                        End If

                    Next


                    For Each row As GridViewRow In GrdAddOn.Rows
                        Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                        Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                        Dim key = Hid.Value

                        If hash.ContainsValue(Hid.Value) Then
                            ch.Checked = True
                        End If

                    Next

                    Session("hashtable") = hash
                    GrdAddOn.Visible = True

                End If


                txtgroups.Enabled = False
                RadioButtonList1.Enabled = False


                ''Bind Group A
                str_query = "Select * from COM_GROUPS_A where CGA_CGR_ID='" & Groupid & "'"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


                If ds.Tables(0).Rows.Count > 0 Then
                    acadamicyear_bind()
                    ddlACD_ID.SelectedValue = ds.Tables(0).Rows(0).Item("CGA_ACY_ID").ToString()

                    'ddlACD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)
                    'CURRICULUM_BSU_4_Student()
                    'bind_Shift()
                    'bind_Stream()
                    'BindTree_GradeSection()


                    ''Bind CLM
                    CURRICULUM_BSU_4_Student()
                    Dim clm = ds.Tables(0).Rows(0).Item("CGA_CLM_ID").ToString()

                    If clm <> "" Then
                        ddlCLM_ID.SelectedValue = ds.Tables(0).Rows(0).Item("CGA_CLM_ID").ToString()
                    Else
                        trCurr.Visible = False
                    End If

                    bind_Shift()
                    Dim shift As String = ds.Tables(0).Rows(0).Item("CGA_SHF_ID").ToString()
                    If shift <> "" Then
                        ddlSHF_ID.SelectedValue = ds.Tables(0).Rows(0).Item("CGA_SHF_ID").ToString()
                    Else
                        trshf.Visible = False
                    End If

                    bind_Stream()

                    Dim stream As String = ds.Tables(0).Rows(0).Item("CGA_STM_ID").ToString()
                    If stream <> "" Then
                        ddlSTM_ID.SelectedValue = ds.Tables(0).Rows(0).Item("CGA_STM_ID").ToString()
                    Else
                        trstm.Visible = False
                    End If

                    BindTree_GradeSection()

                    Dim section As String = ds.Tables(0).Rows(0).Item("CGA_SEC_ID").ToString().Replace("(", "").Replace(")", "")

                    If section <> "" Then

                        Dim sectionarray As String() = section.Split(",")

                        For Each node As TreeNode In tvGrade.Nodes

                            For Each cnode As TreeNode In node.ChildNodes
                                SetNodeValue(cnode, sectionarray)
                            Next


                        Next


                    End If


                End If

                BindPartBControls()

                ''Bind Part B
                For Each item As DataListItem In DataListPartB.Items
                    Dim cgi_id = DirectCast(item.FindControl("HiddenCGC_ID"), HiddenField).Value
                    Dim dc As DataSet
                    Dim CBLDynamic As New CheckBoxList
                    str_query = "Select * from COM_GROUPS_B where CGB_CGR_ID='" & Groupid & "' and CGB_CGC_ID_STF_FT_ID='" & cgi_id & "'"
                    dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    CBLDynamic = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)
                    Dim i = 0
                    For i = 0 To dc.Tables(0).Rows.Count - 1
                        Dim value = dc.Tables(0).Rows(i).Item("CGB_VALUE").ToString()
                        If value = "0" Then
                            For Each clist1 As ListItem In CBLDynamic.Items
                                clist1.Selected = True
                            Next

                        Else
                            For Each clist As ListItem In CBLDynamic.Items
                                If clist.Value = value Then
                                    clist.Selected = True
                                End If

                            Next
                        End If


                    Next

                Next




            End If



        Catch ex As Exception

            lblmessage.Text = "Error :" & ex.Message
        End Try



    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
        directory.Add("Save", btnsave)
        'directory.Add("Cancel", btncancel)

        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))

        Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
        directory2.Add("Save", btnadonSave)
        directory2.Add("Cancel", btnadonCancel)
        directory2.Add("Add", btnsearch)

        Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub

    Public Sub BindPartBControls()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select * from COM_GROUP_CRITERIA_M"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DataListPartB.DataSource = ds
        DataListPartB.DataBind()
        For Each row As DataListItem In DataListPartB.Items
            Dim cgi_id = DirectCast(row.FindControl("HiddenCGC_ID"), HiddenField).Value
            Dim dc As DataSet
            Dim CBLDynamic As New CheckBoxList
            CBLDynamic.ID = "|" & cgi_id & "|"
            Dim condition As String = DirectCast(row.FindControl("HiddenCondition"), HiddenField).Value.Replace("B$", ddlSchool.SelectedValue)

            dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, condition)
            CBLDynamic.DataSource = dc
            CBLDynamic.DataTextField = DirectCast(row.FindControl("HiddenTextField"), HiddenField).Value
            CBLDynamic.DataValueField = DirectCast(row.FindControl("HiddenValueField"), HiddenField).Value
            CBLDynamic.DataBind()
            For Each clist As ListItem In CBLDynamic.Items
                clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
            Next
            Dim list As New ListItem
            'list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
            list.Text = "All"
            list.Value = "0"
            list.Attributes.Add("onclick", "javascript:change_chk_state(this);")

            CBLDynamic.Items.Insert(0, list)

            Dim PanelHolder As Panel = DirectCast(row.FindControl("PanelControl"), Panel)
            PanelHolder.Controls.Add(CBLDynamic)
        Next
    End Sub


    Public Sub BindBsu()

        Try
            'Using BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_Student()

            '    ddlSchool.Items.Clear()

            '    If BSU_4_Student_reader.HasRows = True Then
            '        While BSU_4_Student_reader.Read

            '            ddlSchool.Items.Add(New ListItem(BSU_4_Student_reader("bsu_name"), BSU_4_Student_reader("bsu_id")))
            '        End While

            '    End If
            'End Using
            ''ddlSchool.ClearSelection()
            ''ddlSchool.Items.FindByValue(Session("sBsuid")).Selected = True
            ''ddlSchool_SelectedIndexChanged(ddlSchool, Nothing)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
& " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
            ddlSchool.DataSource = ds.Tables(0)
            ddlSchool.DataTextField = "BSU_NAME"
            ddlSchool.DataValueField = "BSU_ID"
            ddlSchool.DataBind()
            ddlSchool.SelectedIndex = -1
            If Not ddlSchool.Items.FindByValue(Hiddenbsuid.Value) Is Nothing Then
                ddlSchool.Items.FindByValue(Hiddenbsuid.Value).Selected = True
                ddlSchool_SelectedIndexChanged(ddlSchool, Nothing)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ddlSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSchool.SelectedIndexChanged

        Call acadamicyear_bind()
        CURRICULUM_BSU_4_Student()
        bind_Shift()
        bind_Stream()
        BindTree_GradeSection()

        HideADDon()
    End Sub

    Sub acadamicyear_bind()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedItem.Value
            Dim currACY_ID As String = String.Empty
            str_Sql = "SELECT  distinct   ACADEMICYEAR_D.ACD_ACY_ID, ACADEMICYEAR_M.ACY_DESCR," & _
" (select top 1 ACD_ACY_ID from ACADEMICYEAR_D where ACD_BSU_ID = '" & BSU_ID & "' and acd_current=1) as CurrACY_ID " & _
" FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID " & _
" = ACADEMICYEAR_M.ACY_ID WHERE(ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')" & _
" ORDER BY ACADEMICYEAR_D.ACD_ACY_ID "

            Using readerAcademic As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerAcademic.HasRows = True Then
                    While readerAcademic.Read
                        ddlACD_ID.Items.Add(New ListItem(readerAcademic("ACY_DESCR"), readerAcademic("ACD_ACY_ID")))
                        currACY_ID = readerAcademic("CurrACY_ID")
                    End While
                End If
            End Using
            'ddlACD_ID.ClearSelection()
            'ddlACD_ID.Items.FindByValue(currACY_ID).Selected = True
            'ddlACD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlACD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACD_ID.SelectedIndexChanged
        CURRICULUM_BSU_4_Student()
        bind_Shift()
        bind_Stream()
        BindTree_GradeSection()
        HideADDon()
    End Sub

    Sub CURRICULUM_BSU_4_Student()

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_Count As Integer
            str_Sql = "SELECT DISTINCT ACADEMICYEAR_D.ACD_CLM_ID, CURRICULUM_M.CLM_DESCR," & _
"  (select count(distinct acd_clm_id) from academicyear_d where acd_acy_id " & _
" =ACADEMICYEAR_D.ACD_ACY_ID and acd_bsu_id= '" & BSU_ID & "') as tot_count " & _
" FROM ACADEMICYEAR_D INNER JOIN  CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = " & _
" CURRICULUM_M.CLM_ID WHERE(ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "') And " & _
" (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')"
            ddlCLM_ID.Items.Clear()

            Using readerCLM As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerCLM.HasRows = True Then
                    While readerCLM.Read
                        ddlCLM_ID.Items.Add(New ListItem(readerCLM("CLM_DESCR"), readerCLM("ACD_CLM_ID")))
                        CLM_Count = readerCLM("tot_count")
                    End While
                End If
            End Using
            If CLM_Count > 1 Then
                ddlCLM_ID.Items.Add(New ListItem("ALL", "0"))
                ddlCLM_ID.ClearSelection()
                ddlCLM_ID.Items.FindByValue("0").Selected = True
            Else

            End If
            If ddlCLM_ID.Items.Count = 1 Then
                trCurr.Visible = False
            Else
                trCurr.Visible = True
            End If

            'ddlCLM_ID_SelectedIndexChanged(ddlCLM_ID, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCLM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCLM_ID.SelectedIndexChanged
        bind_Shift()
        bind_Stream()
        BindTree_GradeSection()
        HideADDon()
    End Sub

    Sub bind_Shift()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_ID As String = ddlCLM_ID.SelectedValue
            ddlSHF_ID.Items.Clear()
            If CLM_ID <> "0" Then
                str_Sql = " SELECT   distinct  SHIFTS_M.SHF_ID, SHIFTS_M.SHF_DESCR " & _
" FROM  SHIFTS_M INNER JOIN  GRADE_BSU_M ON SHIFTS_M.SHF_ID = GRADE_BSU_M.GRM_SHF_ID " & _
" and GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d " & _
" where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "')"

                Using readerSHF As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    If readerSHF.HasRows = True Then
                        While readerSHF.Read
                            ddlSHF_ID.Items.Add(New ListItem(readerSHF("SHF_DESCR"), readerSHF("SHF_ID")))
                        End While
                    End If
                End Using
                If ddlSHF_ID.Items.Count > 1 Then
                    ddlSHF_ID.Items.Add(New ListItem("ALL", "0"))
                    ddlSHF_ID.ClearSelection()
                    ddlSHF_ID.Items.FindByValue("0").Selected = True
                End If

            Else
                ddlSHF_ID.Items.Add(New ListItem("ALL", "0"))
                ddlSHF_ID.ClearSelection()
                ddlSHF_ID.Items.FindByValue("0").Selected = True
            End If

            If ddlSHF_ID.Items.Count = 1 Then
                trshf.Visible = False
            Else
                trshf.Visible = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bind_Stream()

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_ID As String = ddlCLM_ID.SelectedValue
            ddlSTM_ID.Items.Clear()
            If CLM_ID <> "0" Then
                str_Sql = "SELECT DISTINCT STREAM_M.STM_ID, STREAM_M.STM_DESCR " & _
               " FROM GRADE_BSU_M INNER JOIN  STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID " & _
               " and GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d " & _
               " where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "')"


                Using readerSTM As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    If readerSTM.HasRows = True Then
                        While readerSTM.Read
                            ddlSTM_ID.Items.Add(New ListItem(readerSTM("STM_DESCR"), readerSTM("STM_ID")))
                        End While
                    End If
                End Using
                If ddlSTM_ID.Items.Count > 1 Then
                    ddlSTM_ID.Items.Add(New ListItem("ALL", "0"))
                    ddlSTM_ID.ClearSelection()
                    ddlSTM_ID.Items.FindByValue("0").Selected = True
                End If

            Else
                ddlSTM_ID.Items.Add(New ListItem("ALL", "0"))
                ddlSTM_ID.ClearSelection()
                ddlSTM_ID.Items.FindByValue("0").Selected = True

            End If

            If ddlSTM_ID.Items.Count = 1 Then
                trstm.Visible = False
            Else
                trstm.Visible = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlSHF_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHF_ID.SelectedIndexChanged
        BindTree_GradeSection()
        HideADDon()
    End Sub

    Protected Sub ddlSTM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTM_ID.SelectedIndexChanged
        BindTree_GradeSection()
        HideADDon()
    End Sub

    Sub BindTree_GradeSection() 'for orginating tree view for the grade and section
        Dim dsData As DataSet = Nothing
        Dim dtTable As DataTable = Nothing

        Dim ACY_ID As String = ddlACD_ID.SelectedValue
        Dim BSU_ID As String = ddlSchool.SelectedValue
        Dim CLM_ID As String = ddlCLM_ID.SelectedValue
        Dim SHF_ID As String = ddlSHF_ID.SelectedValue
        Dim STM_ID As String = ddlSTM_ID.SelectedValue
        Dim SHF_STM As String = String.Empty
        SHF_STM = " AND GRADE_BSU_M.GRM_ID<>'' "
        If SHF_ID <> "0" Then
            SHF_STM += " AND GRADE_BSU_M.GRM_SHF_ID = '" & SHF_ID & "'"
        End If
        If STM_ID <> "0" Then
            SHF_STM += " AND GRADE_BSU_M.GRM_STM_ID = '" & STM_ID & "'"

        End If



        If CLM_ID <> "0" Then

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = " with CTE_GRD (GRD_DESCR, SCT_DESCR, GRD_ID, SCT_ID,DISPLAYORDER)" & _
 "  AS ( SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR,  " & _
 "  GRADE_BSU_M.GRM_GRD_ID as  GRD_ID,SECTION_M.SCT_ID as SCT_ID,GRADE_M.GRD_DISPLAYORDER as " & _
 " DISPLAYORDER FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = " & _
 " SECTION_M.SCT_ACD_ID AND  GRADE_BSU_M.GRM_ID = SECTION_M.SCT_GRM_ID INNER JOIN  " & _
 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID  where  GRADE_BSU_M.GRM_ACD_ID= " & _
 " (select acd_id from academicyear_d where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and " & _
 " acd_bsu_id='" & BSU_ID & "') AND SECTION_M.SCT_DESCR<>'TEMP'  " & SHF_STM & ") " & _
 " SELECT GRD_ID,GRD_DESCR, SCT_ID,SCT_DESCR FROM   CTE_GRD ORDER BY DISPLAYORDER"






                '"declare  @temptable table(GRD_DESCR varchar(20), SCT_DESCR varchar(20), GRD_ID varchar(100), SCT_ID varchar(10),DISPLAYORDER int) " & _
                '     " insert @temptable (GRD_DESCR, SCT_DESCR, GRD_ID, SCT_ID,DISPLAYORDER) " & _
                '   " SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR, " & _
                '   "  GRADE_BSU_M.GRM_GRD_ID as  GRD_ID,SECTION_M.SCT_ID as SCT_ID,GRADE_M.GRD_DISPLAYORDER as DISPLAYORDER" & _
                '   " FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND " & _
                '   " GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
                '   " where  GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "') AND SECTION_M.SCT_DESCR<>'TEMP' " & SHF_STM & " order by GRADE_M.GRD_DISPLAYORDER  ASC" & _
                '   " SELECT GRD_ID,GRD_DESCR, SCT_ID,SCT_DESCR FROM   @temptable ORDER BY DISPLAYORDER "

                dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            End Using
            If Not dsData Is Nothing Then
                dtTable = dsData.Tables(0)
            End If

            Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("All Grades", "ALL")
            Dim drGRD_DESCR As DataRow
            For i As Integer = 0 To dtTable.Rows.Count - 1
                drGRD_DESCR = dtTable.Rows(i)
                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                        contains = True
                    End If
                End While
                Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRD_ID"))
                If contains Then
                    Continue For
                End If
                Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
                drGRD_DESCR("GRD_DESCR") & "'"
                Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
                Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
                While (ienumGRADE_SECT.MoveNext())
                    Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                    Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                    trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
                End While
                trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
            Next

            tvGrade.Nodes.Clear()
            tvGrade.Nodes.Add(trSelectAll)
            tvGrade.DataBind()

        Else
            Dim trGRDAll As New TreeNode("All Grades", "ALL")
            tvGrade.Nodes.Clear()
            tvGrade.Nodes.Add(trGRDAll)
            tvGrade.DataBind()


        End If

        ' PROCESS Filter

    End Sub


    Public Function GetFilterQuery() As String
        Dim ReturnQuery = ""


        If CheckContact.Items(0).Selected = True And CheckContact.Items(0).Value = "P" Then
            ReturnQuery &= FilterQuery("P")
            CheckContact.Items(0).Selected = True
            CheckContact.Items(1).Selected = False
            CheckContact.Items(2).Selected = False
            CheckContact.Items(3).Selected = False

        Else
            ReturnQuery = ""
            CheckContact.Items(0).Selected = False

            If CheckContact.Items(1).Selected = True Then '' Father
                ReturnQuery &= FilterQuery("F")
            End If
            If CheckContact.Items(2).Selected = True Then '' Mother

                If ReturnQuery <> "" Then
                    ReturnQuery &= " union all "
                End If

                ReturnQuery &= FilterQuery("M")
            End If
            If CheckContact.Items(3).Selected = True Then '' Guardian
                If ReturnQuery <> "" Then
                    ReturnQuery &= " union all "
                End If

                ReturnQuery &= FilterQuery("G")
            End If


        End If

        If ReturnQuery = "" Then
            ReturnQuery &= FilterQuery("P")
            CheckContact.Items(0).Selected = True
            CheckContact.Items(1).Selected = False
            CheckContact.Items(2).Selected = False
            CheckContact.Items(3).Selected = False
        End If


        Return ReturnQuery
    End Function

    Public Function FilterQuery(ByVal PrimaryContact As String) As String

        Dim CommonQuery As String = " ROW_NUMBER() over(order by STU_SIBLING_ID) RowId ,student_m.stu_no as uniqueid, student_m.STU_SIBLING_ID, isnull(student_m.STU_FIRSTNAME,'''')+'' ''+ isnull(student_m.STU_MIDNAME,'''') + '' '' + isnull(student_m.STU_LASTNAME,'''') as Name,student_m.STU_bRCVSMS,student_m.STU_bRCVMAIL " & _
        " ,student_m.STU_PRIMARYCONTACT ,"

        If PrimaryContact = "F" Then ''Father
            CommonQuery &= " (select STS_FEMAIL from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as Email, "
            CommonQuery &= " (select STS_FMOBILE from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as Mobile, "
            CommonQuery &= " (select ISNULL(STS_FFIRSTNAME,'''') + '' '' + ISNULL(STS_FLASTNAME,'''')  from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as parent_name "

        End If

        If PrimaryContact = "M" Then ''Mother
            CommonQuery &= " (select STS_MEMAIL from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as Email, "
            CommonQuery &= " (select STS_MMOBILE from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as Mobile, "
            CommonQuery &= " (select ISNULL(STS_MFIRSTNAME,'''') + '' '' + ISNULL(STS_MLASTNAME,'''')  from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as parent_name "

        End If

        If PrimaryContact = "G" Then ''Guardian
            CommonQuery &= " (select STS_GEMAIL from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as Email, "
            CommonQuery &= " (select STS_GMOBILE from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as Mobile, "
            CommonQuery &= " (select ISNULL(STS_GFIRSTNAME,'''') + '' '' + ISNULL(STS_GLASTNAME,'''')  from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) as parent_name "
        End If

        If PrimaryContact = "P" Then ''Primary Contact
            CommonQuery &= " (case student_m.STU_PRIMARYCONTACT when ''F'' then (select STS_FEMAIL from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID)  when ''M'' then (select STS_MEMAIL from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID)  when ''G'' then (select STS_GEMAIL from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID)  end) as Email  ,  " & _
                            " (case student_m.STU_PRIMARYCONTACT when ''F'' then (select STS_FMOBILE from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID)  when ''M'' then (select STS_MMOBILE from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID)  when ''G'' then (select STS_GMOBILE from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID)  end) as Mobile ,  " & _
                            " (case student_m.STU_PRIMARYCONTACT when ''F'' then (select ISNULL(STS_FFIRSTNAME,'''') + '' '' + ISNULL(STS_FLASTNAME,'''')  from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) when ''M'' then (select ISNULL(STS_MFIRSTNAME,'''') + '' '' + ISNULL(STS_MLASTNAME,'''')  from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) when ''G'' then (select ISNULL(STS_GFIRSTNAME,'''') + '' '' + ISNULL(STS_GLASTNAME,'''')  from student_d where student_d.sts_stu_id=Student_m.STU_SIBLING_ID) end) as parent_name  "

        End If

        CommonQuery &= " ,cm.comp_name  as comp_name, ''" & PrimaryContact & "'' as Contact  "


        Dim PartAFilter = ""

        ''Part A

        If ddlSchool.SelectedItem.Text <> "" Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_bsu_id in(''" & ddlSchool.SelectedValue & "'')"
            Else
                PartAFilter = "student_m.stu_bsu_id in(''" & ddlSchool.SelectedValue & "'')"
            End If

        End If


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sqlquery = ""
        If trCurr.Visible = True Then
            ''Get the Acd_id
            If ddlCLM_ID.SelectedItem.Text <> "ALL" Then
                sqlquery = "select ACD_ID from ACADEMICYEAR_D where ACD_BSU_ID='" & ddlSchool.SelectedValue & "' and ACD_ACY_ID='" & ddlACD_ID.SelectedValue & "' and ACD_CLM_ID='" & ddlCLM_ID.SelectedValue & "'"
            Else
                'sqlquery = "select ACD_ID from ACADEMICYEAR_D where ACD_BSU_ID='" & ddlSchool.SelectedValue & "' and ACD_ACY_ID='" & ddlACD_ID.SelectedValue & "'"
                sqlquery = "SELECT ISNULL(STUFF(( SELECT ',' + CONVERT(VARCHAR(100),ACD_ID)  FROM  dbo.ACADEMICYEAR_D AS t2   WHERE   t2.ACD_ID  in (select ACD_ID from  ACADEMICYEAR_D t1 WHERE ACD_BSU_ID='" & ddlSchool.SelectedValue & "'  and ACD_ACY_ID='" & ddlACD_ID.SelectedValue & "')  FOR XML PATH('')), 1, 1, ''),'') as ACD_ID"
            End If

        Else
            'sqlquery = "select ACD_ID from ACADEMICYEAR_D where ACD_BSU_ID='" & ddlSchool.SelectedValue & "' and ACD_ACY_ID='" & ddlACD_ID.SelectedValue & "'"
            sqlquery = "SELECT ISNULL(STUFF(( SELECT ',' + CONVERT(VARCHAR(100),ACD_ID)  FROM  dbo.ACADEMICYEAR_D AS t2   WHERE   t2.ACD_ID  in (select ACD_ID from  ACADEMICYEAR_D t1 WHERE ACD_BSU_ID='" & ddlSchool.SelectedValue & "'  and ACD_ACY_ID='" & ddlACD_ID.SelectedValue & "')  FOR XML PATH('')), 1, 1, ''),'') as ACD_ID"
        End If

        Dim acd_id = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sqlquery)

        If PartAFilter <> "" Then
            PartAFilter += " AND " & "student_m.stu_acd_id in(" & acd_id & ")"
        Else
            PartAFilter = "student_m.stu_acd_id in(" & acd_id & ")"
        End If




        If trshf.Visible = True Then
            If ddlSHF_ID.SelectedItem.Text <> "ALL" Then
                If PartAFilter <> "" Then
                    PartAFilter += " AND " & "student_m.stu_shf_id in (''" & ddlSHF_ID.SelectedValue & "'')"
                Else
                    PartAFilter = "student_m.stu_shf_id in (''" & ddlSHF_ID.SelectedValue & "'')"
                End If
            End If
        End If

        If trstm.Visible = True Then
            If ddlSTM_ID.SelectedItem.Text <> "ALL" Then
                If PartAFilter <> "" Then
                    PartAFilter += " AND " & "student_m.stu_stm_id in (''" & ddlSTM_ID.SelectedValue & "'')"
                Else
                    PartAFilter = "student_m.stu_stm_id in (''" & ddlSTM_ID.SelectedValue & "'')"
                End If
            End If
        End If




        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
        Dim temp1 As String = String.Empty
        If tvGrade.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvGrade.CheckedNodes
                If Not node Is Nothing Then
                    If node.Text <> "All Grades" Then

                        If node.Parent.Text <> "All Grades" Then
                            temp1 += "''" & node.Value & "'',"
                            'node.Parent.Value, node.Value
                        End If
                    End If
                End If
            Next
        End If


        temp1 = temp1.TrimEnd(",").TrimStart(",")

        If Trim(temp1) <> "" Then

            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_acd_id in(''" & acd_id & "'')"
            Else
                PartAFilter = "student_m.stu_acd_id in(''" & acd_id & "'')"
            End If


            If PartAFilter <> "" Then
                PartAFilter += " AND student_m.STU_SCT_ID IN (" & temp1 & ")"
            Else
                PartAFilter = "  student_m.STU_SCT_ID IN (" & temp1 & ")"
            End If

        End If



        Dim PBfilter As String = ""
        Dim TJoinFilter As String = ""

        'Part B
        For Each item As DataListItem In DataListPartB.Items
            Dim cboxlist As New CheckBoxList
            Dim cgi_id As String = DirectCast(item.FindControl("HiddenCGC_ID"), HiddenField).Value
            cboxlist = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)


            If cboxlist.Items(0).Selected = False Then

                Dim temp As String = ""
                Dim i = 0

                For i = 0 To cboxlist.Items.Count - 1
                    Dim filter As String = ""
                    If cboxlist.Items(i).Selected Then

                        If i = 0 Then
                            filter += "''" & cboxlist.Items(i).Value & "''"
                        Else
                            filter += "''" & cboxlist.Items(i).Value & "'',"
                        End If

                    End If
                    temp += filter
                Next
                If temp <> "" Then

                    temp = temp.Substring(0, temp.Length - 1)
                    Dim FilterField As String = DirectCast(item.FindControl("HiddenFilterField"), HiddenField).Value
                    Dim TjoinField As String = DirectCast(item.FindControl("HiddenTJoinField"), HiddenField).Value

                    If FilterField <> "" Then

                        FilterField = FilterField.Replace("$", temp)
                       
                        If PBfilter <> "" Then
                            PBfilter += " AND "
                        End If
                        PBfilter += FilterField
                    End If
                    If TjoinField <> "" Then
                        TjoinField = TjoinField.Replace("$", temp)

                        TJoinFilter += " " & TjoinField
                    End If

                End If

            End If

        Next

        Dim ReturnQuery As String = ""

        Dim FromQuery As String = " from Student_m  inner join student_d sd on  Student_m.STU_SIBLING_ID=sd.STS_STU_ID AND (CONVERT(DATETIME, STUDENT_M.STU_LEAVEDATE) IS NULL OR CONVERT(DateTime, CONVERT(VARCHAR(100), STUDENT_M.STU_LEAVEDATE, 106)) >= CONVERT(DateTime, CONVERT(VARCHAR(100), GETDATE(), 106))) and STU_CURRSTATUS <>''CN''" & _
                    " left join comp_listed_m cm on cm.comp_id in " & _
                    " (select (case Student_m.STU_PRIMARYCONTACT when ''F'' then sd.sts_f_comp_id when ''M'' then sd.sts_m_comp_id when ''G'' then sd.sts_g_comp_id end ) as id ) "

        ' Dim FromQuery As String = " from student_m inner join student_d sd on student_m.stu_id =sd.sts_stu_id left join comp_listed_m cm on cm.comp_id in (select (case student_m.STU_PRIMARYCONTACT when ''F'' then sd.sts_f_comp_id when ''M'' then sd.sts_m_comp_id when ''G'' then sd.sts_g_comp_id end ) as id ) "

        If TJoinFilter <> "" Or PBfilter <> "" Then
            If TJoinFilter <> "" Then
                If PBfilter <> "" Then
                    If PartAFilter <> "" Then
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter & " where " & PBfilter & " AND " & PartAFilter
                    Else
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter & " where " & PBfilter
                    End If
                Else
                    If PartAFilter <> "" Then
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter & " AND " & PartAFilter
                    Else
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter
                    End If
                End If
            Else
                If PartAFilter <> "" Then
                    ReturnQuery = "select " & CommonQuery & FromQuery & " where " & PBfilter & " AND " & PartAFilter
                Else
                    ReturnQuery = "select " & CommonQuery & FromQuery & " where " & PBfilter
                End If

            End If
        Else
            If PartAFilter <> "" Then
                ReturnQuery = "select " & CommonQuery & FromQuery & " where " & PartAFilter
            Else
                ReturnQuery = "select " & CommonQuery & FromQuery
            End If

        End If


        Return ReturnQuery
    End Function

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim contact As String = ""
            If CheckContact.Items(0).Selected = True And CheckContact.Items(0).Value = "P" Then
                contact = "P"
            Else
                If CheckContact.Items(1).Selected = True Then '' Father
                    contact = "F"
                End If
                If CheckContact.Items(2).Selected = True Then '' Mother

                    If contact <> "" Then
                        contact &= ","
                    End If

                    contact &= "M"
                End If
                If CheckContact.Items(3).Selected = True Then '' Guardian
                    If contact <> "" Then
                        contact &= ","
                    End If

                    contact &= "G"
                End If


            End If

            If contact = "" Then
                contact = "P"
            End If

            ''Update the Contact Details

            Dim sql_query = "Update COM_GROUPS_M SET CGR_CONTACT='" & contact & "' WHERE CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)


            ''Update Part A Values
            Dim partA_BSU As String = ddlSchool.SelectedValue
            Dim partA_Acy_ID As String = ddlACD_ID.SelectedValue
            Dim partA_CLM As String = ""

            If trCurr.Visible = True Then
                partA_CLM = ddlCLM_ID.SelectedValue
            End If

            Dim partA_Shift As String = ""
            If trshf.Visible = True Then
                partA_Shift = ddlSHF_ID.SelectedValue
            End If

            Dim partA_Stream As String = ""
            If trstm.Visible = True Then
                partA_Stream = ddlSTM_ID.SelectedValue
            End If


            Dim partA_Grd As String = ""  '' Only the value selected is section


            Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
            Dim temp1 As String = String.Empty
            If tvGrade.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvGrade.CheckedNodes
                    If Not node Is Nothing Then
                        If node.Text <> "All Grades" Then

                            If node.Parent.Text <> "All Grades" Then
                                temp1 += "'" & node.Value & "',"
                                'node.Parent.Value, node.Value
                            End If
                        End If
                    End If
                Next
            End If


            temp1 = temp1.TrimEnd(",").TrimStart(",")
            Dim partA_Section As String = ""
            If temp1.Trim() <> "" Then
                partA_Section = "(" & temp1 & ")"
            End If

            Dim pParmsA(8) As SqlClient.SqlParameter
            pParmsA(0) = New SqlClient.SqlParameter("@CGA_CGR_ID", HiddenGroupid.Value)
            pParmsA(1) = New SqlClient.SqlParameter("@CGA_BSU_ID", partA_BSU)
            pParmsA(2) = New SqlClient.SqlParameter("@CGA_ACY_ID", partA_Acy_ID)
            pParmsA(3) = New SqlClient.SqlParameter("@CGA_CLM_ID", partA_CLM)
            pParmsA(4) = New SqlClient.SqlParameter("@CGA_SHF_ID", partA_Shift)
            pParmsA(5) = New SqlClient.SqlParameter("@CGA_STM_ID", partA_Stream)
            pParmsA(6) = New SqlClient.SqlParameter("@CGA_GRD_ID", partA_Grd)
            pParmsA(7) = New SqlClient.SqlParameter("@CGA_SEC_ID", partA_Section)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_A_UPDATE", pParmsA)

            ''Delete PART B Values
            sql_query = "DELETE COM_GROUPS_B WHERE CGB_CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

            ''Insert Part B Values
            For Each item As DataListItem In DataListPartB.Items
                Dim cboxlist As New CheckBoxList
                Dim cgi_id As String = DirectCast(item.FindControl("HiddenCGC_ID"), HiddenField).Value
                cboxlist = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)
                Dim i = 0
                If cboxlist.Items(0).Selected Then
                    Dim value As String = cboxlist.Items(0).Value
                    Dim pParmsB(3) As SqlClient.SqlParameter
                    pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                    pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                    pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)

                Else

                    For i = 0 To cboxlist.Items.Count - 1
                        If cboxlist.Items(i).Selected Then
                            Dim value As String = cboxlist.Items(i).Value
                            Dim pParmsB(3) As SqlClient.SqlParameter
                            pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                            pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                            pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)
                        End If
                    Next

                End If

            Next

            ''Update Query
            Dim query = "Update COM_GROUPS_M set CGR_CONDITION='" & GetFilterQuery() & "' , CGR_GRP_BSU_ID='" & ddlSchool.SelectedValue & "' where CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)

            lblmessage.Text = "Group updated successfully."

            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            lblmessage.Text = "Error occured while saving . " & ex.Message
        Finally

            connection.Close()
        End Try


      
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Encr_decrData As New Encryption64
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Encr_decrData.Encrypt("none") ''& Request.QueryString("datamode").ToString()
        Response.Redirect("comListGroups.aspx" & mInfo)
    End Sub

    Public Sub SearchShow()
        If RadioButtonList1.SelectedValue = 0 Then
            Panel1.Visible = True
            Panel2.Visible = False
        Else
            Panel2.Visible = True
            Panel1.Visible = False
            btnsearch.Visible = True
            btnadonCancel.Visible = True 'False
            btnadonSave.Visible = False
            GrdAddOn.Visible = False
        End If
        lblmessage.Text = ""
    End Sub

    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        SearchShow()
    End Sub

    Public Sub BindGrid()
        Try

            GrdAddOn.Visible = True
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = ""

            Dim ds As DataSet

            Dim Txt1 As String
            Dim Txt2 As String
            Dim Txt3 As String
            Dim Txt4 As String
            Dim Txt5 As String


            str_query = "select *,case ROW_NUMBER() over(PARTITION BY STU_SIBLING_ID order by STU_SIBLING_ID)  when '1' then 'True' else 'False' end  S1  from (" & Hiddenquery.Value & ") a"

            Dim sqlwhere As String = ""

            If GrdAddOn.Rows.Count > 0 Then
                Txt1 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim() 'Unique No
                Txt2 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim() 'Name
                Txt3 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim() 'Email
                Txt4 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim() 'Mobile
                'Txt5 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt5"), TextBox).Text.Trim() 'Company Name



                If Txt1.Trim() <> "" Then
                    If sqlwhere.Length = 0 Then
                        sqlwhere &= "  replace(uniqueid,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                    Else
                        sqlwhere &= " and replace(uniqueid,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                    End If

                End If

                If Txt2.Trim() <> "" Then
                    If sqlwhere.Length = 0 Then
                        sqlwhere &= "  replace(Name,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
                    Else
                        sqlwhere &= " and replace(Name,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
                    End If

                End If

                If Txt3.Trim() <> "" Then
                    If sqlwhere.Length = 0 Then
                        sqlwhere &= "  replace(Email,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                    Else
                        sqlwhere &= " and replace(Email,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                    End If

                End If

                If Txt4.Trim() <> "" Then
                    If sqlwhere.Length = 0 Then
                        sqlwhere &= "  replace(Mobile,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
                    Else
                        sqlwhere &= " and replace(Mobile,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
                    End If

                End If

                'If Txt5.Trim() <> "" Then
                '    If sqlwhere.Length = 0 Then
                '        sqlwhere &= "  replace(comp_name,' ','') like '%" & Txt5.Replace(" ", "") & "%' "
                '    Else
                '        sqlwhere &= " and replace(comp_name,' ','') like '%" & Txt5.Replace(" ", "") & "%' "
                '    End If

                'End If

                If Txt1 <> "" Or Txt2 <> "" Or Txt3 <> "" Or Txt4 <> "" Or Txt5 <> "" Then
                    sqlwhere = " Where" & sqlwhere
                End If

            End If


            If sqlwhere.Length > 6 Then
                str_query = str_query & sqlwhere
            End If

            str_query &= " order by uniqueid "

           
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count = 0 Then
                Dim dt As New DataTable
                dt.Columns.Add("uniqueid")
                dt.Columns.Add("Name")
                dt.Columns.Add("Email")
                dt.Columns.Add("Mobile")
                dt.Columns.Add("comp_name")
                dt.Columns.Add("STU_bRCVSMS")
                dt.Columns.Add("STU_bRCVMAIL")
                dt.Columns.Add("Contact")
                dt.Columns.Add("s1")
                Dim dr As DataRow = dt.NewRow()
                dr("uniqueid") = ""
                dr("Name") = ""
                dr("Email") = ""
                dr("Mobile") = ""
                dr("comp_name") = ""
                dr("STU_bRCVSMS") = False
                dr("STU_bRCVMAIL") = False
                dr("Contact") = ""
                dr("s1") = False

                dt.Rows.Add(dr)
                GrdAddOn.DataSource = dt
                GrdAddOn.DataBind()




            Else


                GrdAddOn.DataSource = ds
                GrdAddOn.DataBind()

            End If

            If GrdAddOn.Rows.Count > 0 Then

                DirectCast(GrdAddOn.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
                DirectCast(GrdAddOn.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
                DirectCast(GrdAddOn.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3
                DirectCast(GrdAddOn.HeaderRow.FindControl("Txt4"), TextBox).Text = Txt4
                'DirectCast(GrdAddOn.HeaderRow.FindControl("Txt5"), TextBox).Text = Txt5


            End If


            Dim hash As New Hashtable
            If Session("hashtable") Is Nothing Then
            Else
                hash = Session("hashtable")
            End If
            ''Dim i = 0
            For Each row As GridViewRow In GrdAddOn.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                Dim key = Hid.Value

                If hash.ContainsValue(Hid.Value) Then
                    ch.Checked = True
                End If

            Next

            Session("hashtable") = hash

        Catch ex As Exception
            lblmessage.Text = "Please save this group again."
            btnadonSave.Visible = False
            btnsave.Visible = False
            btnadonCancel.Visible = True
        End Try

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Try
            lblmessage.Text = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim Query As String = GetFilterQuery()
            Query = Query.Replace("''", "'")

            Hiddenquery.Value = Query
            BindGrid()

        Catch ex As Exception
            lblmessage.Text = "Error in Query"
        End Try
    End Sub

    Protected Sub btnadonSave_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadonSave.Click
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            lblmessage.Text = ""

            Dim contact As String = ""
            If CheckContact.Items(0).Selected = True And CheckContact.Items(0).Value = "P" Then
                contact = "P"
            Else
                If CheckContact.Items(1).Selected = True Then '' Father
                    contact = "F"
                End If
                If CheckContact.Items(2).Selected = True Then '' Mother

                    If contact <> "" Then
                        contact &= ","
                    End If

                    contact &= "M"
                End If
                If CheckContact.Items(3).Selected = True Then '' Guardian
                    If contact <> "" Then
                        contact &= ","
                    End If

                    contact &= "G"
                End If


            End If

            If contact = "" Then
                contact = "P"
            End If

            ''Update the Contact Details

            Dim sql_query = "Update COM_GROUPS_M SET CGR_CONTACT='" & contact & "' WHERE CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)



            ''Delete Add On Members Values
            sql_query = "DELETE COM_GROUPS_ADD_ON WHERE CGAO_CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)


            Dim hash As New Hashtable


            If Session("hashtable") Is Nothing Then
            Else
                hash = Session("hashtable")
            End If
            For Each row As GridViewRow In GrdAddOn.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                If ch.Visible = True Then
                    Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                    Dim key = Hid.Value
                    If ch.Checked Then
                        If hash.ContainsKey(key) Then
                        Else
                            hash.Add(key, Hid.Value)
                        End If

                    Else

                        If hash.ContainsKey(key) Then
                            hash.Remove(key)
                        Else
                        End If
                    End If
                End If
            Next


            Dim idictenum As IDictionaryEnumerator
            idictenum = hash.GetEnumerator()
            While (idictenum.MoveNext())
                Dim key = idictenum.Key
                Dim stuid = idictenum.Value
                Dim pParmsAon(3) As SqlClient.SqlParameter
                pParmsAon(0) = New SqlClient.SqlParameter("@CGAO_CGR_ID", HiddenGroupid.Value)
                pParmsAon(1) = New SqlClient.SqlParameter("@CGAO_UNIQUE_ID", stuid)
                pParmsAon(2) = New SqlClient.SqlParameter("@CGAO_BSU_ID", ddlSchool.SelectedValue)
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_ADD_ON_INSERT", pParmsAon)

            End While

            ''Insert Part A Values
            Dim partA_BSU As String = ddlSchool.SelectedValue
            Dim partA_Acy_ID As String = ddlACD_ID.SelectedValue
            Dim partA_CLM As String = ""

            If trCurr.Visible = True Then
                partA_CLM = ddlCLM_ID.SelectedValue
            End If

            Dim partA_Shift As String = ""
            If trshf.Visible = True Then
                partA_Shift = ddlSHF_ID.SelectedValue
            End If

            Dim partA_Stream As String = ""
            If trstm.Visible = True Then
                partA_Stream = ddlSTM_ID.SelectedValue
            End If


            Dim partA_Grd As String = ""  '' Only the value selected is section


            Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
            Dim temp1 As String = String.Empty
            If tvGrade.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvGrade.CheckedNodes
                    If Not node Is Nothing Then
                        If node.Text <> "All Grades" Then

                            If node.Parent.Text <> "All Grades" Then
                                temp1 += "'" & node.Value & "',"
                                'node.Parent.Value, node.Value
                            End If
                        End If
                    End If
                Next
            End If


            temp1 = temp1.TrimEnd(",").TrimStart(",")
            Dim partA_Section As String = ""
            If temp1.Trim() <> "" Then
                partA_Section = "(" & temp1 & ")"
            End If

            Dim pParmsA(8) As SqlClient.SqlParameter
            pParmsA(0) = New SqlClient.SqlParameter("@CGA_CGR_ID", HiddenGroupid.Value)
            pParmsA(1) = New SqlClient.SqlParameter("@CGA_BSU_ID", partA_BSU)
            pParmsA(2) = New SqlClient.SqlParameter("@CGA_ACY_ID", partA_Acy_ID)
            pParmsA(3) = New SqlClient.SqlParameter("@CGA_CLM_ID", partA_CLM)
            pParmsA(4) = New SqlClient.SqlParameter("@CGA_SHF_ID", partA_Shift)
            pParmsA(5) = New SqlClient.SqlParameter("@CGA_STM_ID", partA_Stream)
            pParmsA(6) = New SqlClient.SqlParameter("@CGA_GRD_ID", partA_Grd)
            pParmsA(7) = New SqlClient.SqlParameter("@CGA_SEC_ID", partA_Section)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_A_UPDATE", pParmsA)

            ''Delete PART B Values
            sql_query = "DELETE COM_GROUPS_B WHERE CGB_CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)


            ''Insert Part B Values
            For Each item As DataListItem In DataListPartB.Items
                Dim cboxlist As New CheckBoxList
                Dim cgi_id As String = DirectCast(item.FindControl("HiddenCGC_ID"), HiddenField).Value
                cboxlist = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)
                Dim i = 0
                If cboxlist.Items(0).Selected Then
                    Dim value As String = cboxlist.Items(0).Value
                    Dim pParmsB(3) As SqlClient.SqlParameter
                    pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                    pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                    pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)

                Else

                    For i = 0 To cboxlist.Items.Count - 1
                        If cboxlist.Items(i).Selected Then
                            Dim value As String = cboxlist.Items(i).Value
                            Dim pParmsB(3) As SqlClient.SqlParameter
                            pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                            pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                            pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)
                        End If
                    Next

                End If

            Next


            ''Update Query
            Dim query = "Update COM_GROUPS_M set CGR_CONDITION='" & GetFilterQuery() & "' , CGR_GRP_BSU_ID='" & ddlSchool.SelectedValue & "' where CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)

            lblmessage.Text = "Add On Group updated Successfully"
            Session.Remove("hashtable")

            GrdAddOn.Visible = False
            btnadonCancel.Visible = True 'False
            btnadonSave.Visible = False
            btnsearch.Visible = True

            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            lblmessage.Text = "Error occured while saving . " & ex.Message
        Finally

            connection.Close()
        End Try


    

    End Sub
    Public Sub HideADDon()
        GrdAddOn.Visible = False
        btnadonCancel.Visible = True 'False
        btnadonSave.Visible = False
        btnsearch.Visible = True
    End Sub

    Protected Sub btnadonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadonCancel.Click

        'HideADDon()
        Dim jsFunc As String = ""
        jsFunc = "SetCloseNewGRPValuetoParent('Back')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
    End Sub

    Protected Sub GrdAddOn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Dim hash As New Hashtable
        If Session("hashtable") Is Nothing Then
        Else
            hash = Session("hashtable")
        End If
        ''Dim i = 0
        For Each row As GridViewRow In GrdAddOn.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
            If ch.Visible = True Then

                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                Dim key = Hid.Value
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, Hid.Value)
                    End If

                Else

                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If

            End If
        Next

        Session("hashtable") = hash
        GrdAddOn.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub GrdAddOn_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As New Hashtable
        hash = Session("hashtable")
        For Each row As GridViewRow In GrdAddOn.Rows
            Dim i = 0
            Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

            If hash.ContainsValue(Hid.Value) Then
                ch.Checked = True
            End If

        Next
        Session("hashtable") = hash

    End Sub

    Protected Sub GrdAddOn_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAddOn.RowCommand

        If e.CommandName = "search" Then

            Dim hash As New Hashtable
            If Session("hashtable") Is Nothing Then
            Else
                hash = Session("hashtable")
            End If
            ''Dim i = 0
            For Each row As GridViewRow In GrdAddOn.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                If ch.Visible = True Then

                    Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                    Dim key = Hid.Value
                    If ch.Checked Then
                        If hash.ContainsKey(key) Then
                        Else
                            hash.Add(key, Hid.Value)
                        End If

                    Else

                        If hash.ContainsKey(key) Then
                            hash.Remove(key)
                        Else
                        End If
                    End If

                End If
            Next

            BindGrid()
        End If

    End Sub


End Class
