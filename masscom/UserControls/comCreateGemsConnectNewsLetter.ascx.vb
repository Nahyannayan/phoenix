﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_UserControls_comCreateGemsConnectNewsLetter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnsaveordinary)


        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            BindDDLType()
            BindTemplate()
            BindDefaultValues()
            ShowBannerThumbnail()
        End If
        ddlType.SelectedValue = 2 'Default News Letter
        ddlPriority.SelectedValue = 1 'Default Normal
        id_type.Attributes.Add("style", "display:none")
        id_fromemail.Attributes.Add("style", "display:none")
        id_priority.Attributes.Add("style", "display:none")
        id_dynamictext.Attributes.Add("style", "display:none")

        AssignRights()
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        tr_host.Visible = False
        tr_port.Visible = False
        tr_username.Visible = False
        tr_password.Visible = False

    End Sub
    Public Sub BindDDLType()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim str_query = "Select * from [dbo].[NOTIFICATION_TYPES]  where NT_TYPE_ACTIVE=1  order by NT_TYPE_DISPLAY_ORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlType.DataSource = ds
            ddlType.DataTextField = "NT_TYPE_DESC"
            ddlType.DataValueField = "NT_TYPE_ID"
            ddlType.DataBind()

            Dim item As New ListItem
            item.Text = "Select"
            item.Value = "-1"
            ddlType.Items.Insert(0, item)
        End If
    End Sub

    Public Sub ShowBannerThumbnail()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim pParms(20) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_VALIDATE_BANNER", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("IsBanner") = "1" Then
                trBanner.Visible = True
            Else
                trBanner.Visible = False
            End If

            If ds.Tables(0).Rows(0)("IsThumbnail") = "1" Then
                trThumbnail.Visible = True
            Else
                trThumbnail.Visible = False
            End If
        Else
            trBanner.Visible = False
            trThumbnail.Visible = False
        End If
    End Sub

    Public Sub BindDefaultValues()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim str_query = "select * from BSU_COMMUNICATION_M where BSC_BSU_ID='" & BsuId & "' AND BSC_TYPE='COM'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            txtFrom.Text = ds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
            txthost.Text = ds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            txtport.Text = ds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
            txtusername.Text = ds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
            HiddenPassword.Value = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
            txtpassword.Text = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
        End If


    End Sub
    Public Sub BindTemplate()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim str_query = "select MERGE_ID,MERGE_TITLE_DES from COM_MERGE_TABLES where MERGE_BSU='" & BsuId & "' AND MERGE_TYPE='EMAIL'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtemplate.DataSource = ds
            ddtemplate.DataTextField = "MERGE_TITLE_DES"
            ddtemplate.DataValueField = "MERGE_ID"
            ddtemplate.DataBind()

            Dim item As New ListItem
            item.Text = "Select a Template"
            item.Value = "-1"
            ddtemplate.Items.Insert(0, item)
            Fields()
        Else
            TRDynamic.Visible = False
        End If




    End Sub
    Public Sub Fields()
        ''Clear the Previous message
        'txtEmailText.Content = ""


        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim template As String = ddtemplate.SelectedValue
        Dim str_query = "select MERGE_FIELDS from COM_MERGE_TABLES where MERGE_ID='" & template & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim fields As String = ds.Tables(0).Rows(0).Item("MERGE_FIELDS").ToString()

            Dim Afields() As String = fields.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("Data")
            dt.Columns.Add("Value")

            Dim i = 0
            For i = 0 To Afields.Length - 1
                Dim cval As String = Afields(i)


                Dim csplit() As String = cval.Split(">")

                Dim dr As DataRow = dt.NewRow()

                dr("Value") = "$" & csplit(0) & "$"
                dr("Data") = csplit(1).Replace("$", "").Replace("<", "")

                dt.Rows.Add(dr)
            Next

            ddfields.DataSource = dt
            ddfields.DataTextField = "Data"
            ddfields.DataValueField = "Value"
            ddfields.DataBind()
            ddfields.Visible = True
            btninsert.Visible = True
        Else
            ddfields.Visible = False
            btninsert.Visible = False
        End If



    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)

        directory2.Add("Add", btnsaveordinary)

        'Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))


    End Sub

    Public Function CheckFileSize(ByVal path As String, ByVal templateid As String) As Boolean
        Dim val = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pVal As String = 1
        Dim str_query = "SELECT P_VAL FROM COM_PARAMETER WHERE ID= " & pVal

        Dim f2 = New FileInfo(path)
        Dim len = f2.Length
        Dim mb = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If len > (mb * 1024 * 1024) Then
            val = False
            lblmessage.Text = "File size must be less than " & mb & "MB."
            File.Delete(path)
        End If


        Return val
    End Function

    Protected Sub btnsaveordinary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsaveordinary.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "insert into COM_MANAGE_EMAIL (EML_GUID) values(NEWID()) SELECT scope_identity() as id "
        Dim save = 1
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim attachment = False
        Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
        Dim val As String = ""
        If ds.Tables(0).Rows(0).Item("id").ToString() <> "" Then
            val = ds.Tables(0).Rows(0).Item("id").ToString()
        Else
            val = "1"
        End If

        Directory.CreateDirectory(serverpath + val)
        Directory.CreateDirectory(serverpath + val + "/Attachments")
        Directory.CreateDirectory(serverpath + val + "/News Letters")
        Directory.CreateDirectory(serverpath + val + "/Thumbnail")
        Directory.CreateDirectory(serverpath + val + "/Banner")

        Dim EML_ATTACHMENT_PATHS As String = ""
        If FileUploadOrdinaryFile.HasFile Then
            attachment = True
            Dim filen As String()
            Dim FileName As String = FileUploadOrdinaryFile.PostedFile.FileName
            FileName = FileName.Replace(" ", "_")
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)
            FileUploadOrdinaryFile.SaveAs(serverpath + val + "/Attachments/" + FileName)
            EML_ATTACHMENT_PATHS = WebConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString + val + "/Attachments/" + FileName
            If CheckFileSize(serverpath + val + "/Attachments/" + FileName, val) Then
                save = 1
            Else
                save = 0
            End If
            attachment = True

        End If

        Dim thumbnailPath As String = String.Empty

        If imgThumbnail.HasFile Then
            Dim filen As String()
            Dim FileName As String = imgThumbnail.PostedFile.FileName
            FileName = FileName.Replace(" ", "_")
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)
            imgThumbnail.SaveAs(serverpath + val + "/Thumbnail/" + FileName)
            thumbnailPath = WebConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString + val + "/Thumbnail/" + FileName
            If CheckFileSize(serverpath + val + "/Thumbnail/" + FileName, val) Then
                save = 1
            Else
                save = 0
            End If
        Else '
            thumbnailPath = WebConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString + "/noimageThumbnail.jpg"
        End If

        Dim bannerPath As String = String.Empty
        If imgBanner.HasFile Then

            Dim filen As String()
            Dim FileName As String = imgBanner.PostedFile.FileName
            FileName = FileName.Replace(" ", "_")
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)
            imgBanner.SaveAs(serverpath + val + "/Banner/" + FileName)
            bannerPath = WebConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString + val + "/Banner/" + FileName
            If CheckFileSize(serverpath + val + "/Banner/" + FileName, val) Then
                save = 1
            Else
                save = 0
            End If
        Else
            bannerPath = WebConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString + "/noimageBanner.jpg"
        End If


        Dim pParms(20) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EML_ID", val)
        pParms(1) = New SqlClient.SqlParameter("@EML_NEWS_LETTER", "False")
        pParms(2) = New SqlClient.SqlParameter("@EML_BODY", txtEmailText.Content)
        pParms(3) = New SqlClient.SqlParameter("@EML_ATTACHMENT", attachment)
        pParms(4) = New SqlClient.SqlParameter("@EML_FROM", txtFrom.Text.Trim())
        pParms(5) = New SqlClient.SqlParameter("@EML_TITLE", txtTitle.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@EML_SUBJECT", txtsubject.Text.Trim())
        pParms(7) = New SqlClient.SqlParameter("@EML_DISPLAY", txtdisplay.Text.Trim())

        pParms(8) = New SqlClient.SqlParameter("@EML_USERNAME", txtusername.Text.Trim())
        If txtpassword.Text.Trim() <> "" Then
            pParms(9) = New SqlClient.SqlParameter("@EML_PASSWORD", txtpassword.Text.Trim())
        Else
            pParms(9) = New SqlClient.SqlParameter("@EML_PASSWORD", HiddenPassword.Value)
        End If

        If (ddlPriority.SelectedItem.Value = "2") Then
            Dim criticalEmailPath As String = WebConfigurationManager.AppSettings("CriticalEmailHost").ToString
            pParms(10) = New SqlClient.SqlParameter("@EML_HOST", criticalEmailPath.ToString)
        Else
            pParms(10) = New SqlClient.SqlParameter("@EML_HOST", txthost.Text.Trim())
        End If

        pParms(11) = New SqlClient.SqlParameter("@EML_PORT", txtport.Text.Trim())
        pParms(12) = New SqlClient.SqlParameter("@EML_BSU_ID", Hiddenbsuid.Value)
        pParms(13) = New SqlClient.SqlParameter("@EML_EMP_ID", Session("EmployeeId"))

        If ddtemplate.SelectedIndex > 0 Then
            pParms(14) = New SqlClient.SqlParameter("@EML_MERGE_ID", ddtemplate.SelectedValue)
        End If
        If (ddlPriority.SelectedItem.Value = "2") Then
            pParms(15) = New SqlClient.SqlParameter("@EML_CRITICAL", 1)
        Else
            pParms(15) = New SqlClient.SqlParameter("@EML_CRITICAL", 0)
        End If

        pParms(16) = New SqlClient.SqlParameter("@EML_THUMBNAIL", thumbnailPath)
        pParms(17) = New SqlClient.SqlParameter("@EML_BANNER", bannerPath)
        pParms(18) = New SqlClient.SqlParameter("@EML_TYPE", ddlType.SelectedItem.Text)
        pParms(19) = New SqlClient.SqlParameter("@EML_ATTACHMENT_PATHS", EML_ATTACHMENT_PATHS)
        pParms(20) = New SqlClient.SqlParameter("@EML_NEWSLETTERCONNECT", 1)
        'pParms(16) = New SqlClient.SqlParameter("@OUT_VALUE", SqlDbType.Int)
        'pParms(16).Direction = ParameterDirection.Output
        'Dim outvalue As String = ""
        If save = 1 Then
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_INSERT", pParms)
            'outvalue = pParms(16).Value
            txtEmailText.Content = ""
            txtFrom.Text = ""
            txtTitle.Text = ""
            txtsubject.Text = ""
            txtdisplay.Text = ""
            txtusername.Text = ""
            txtpassword.Text = ""
            txthost.Text = ""
            txtport.Text = ""
            lblmessage.Text = "Plain Text Email Template Successfully Created with ID: "


        End If

        Dim jsFunc As String = "SetCloseValuetoParent('" + lblmessage.Text.ToString + val + "')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)

    End Sub


    Protected Sub ddtemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Fields()
    End Sub

    Protected Sub btninsert_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtEmailText.Content = txtEmailText.Content & ddfields.SelectedValue
    End Sub



End Class
