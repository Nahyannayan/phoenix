Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comGroupAddOnMembers
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                Session("hashtable1") = Nothing
                Session("hashtable2") = Nothing

                Dim EncDec As New Encryption64
                Dim Groupid As String = EncDec.Decrypt(Request.QueryString("Groupid").Replace(" ", "+"))
                HiddenGroupid.Value = Groupid

                BindGrid()
                BindRemoveEmailSMS()
            Catch ex As Exception

                lblmessage.Text = "Error :" & ex.Message
            End Try

        End If
    End Sub

    Public Sub BindRemoveEmailSMS()
        Session("hashtable1") = Nothing
        Session("hashtable2") = Nothing
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        str_query = "Select CGR_REMOVE_EMAIL_IDS ,CGR_REMOVE_SMS_IDS from COM_GROUPS_M where CGR_ID='" & HiddenGroupid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim emailidsremove As String = ds.Tables(0).Rows(0).Item("CGR_REMOVE_EMAIL_IDS").ToString()
            Dim smsidsremove As String = ds.Tables(0).Rows(0).Item("CGR_REMOVE_SMS_IDS").ToString()

            Dim hash1 As New Hashtable
            Dim hash2 As New Hashtable

            If emailidsremove <> "" Then

                Dim query = "select SUBSTRING('" & emailidsremove & "', (select charindex ('(', '" & emailidsremove & "'))+1, ((select len ('" & emailidsremove & "')) - (select charindex ('(', '" & emailidsremove & "'))  )-1) "

                emailidsremove = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)

                Dim val As String() = emailidsremove.Split(",")

                Dim i = 0
                For i = 0 To val.Length - 1
                    Dim key = val(i)  'GrdMembers.PageIndex & "_" &
                    If hash1.ContainsKey(key) Then
                    Else
                        hash1.Add(key, val(i))
                    End If

                Next

            End If

            If smsidsremove <> "" Then

                Dim query = "select SUBSTRING('" & smsidsremove & "', (select charindex ('(', '" & smsidsremove & "'))+1, ((select len ('" & smsidsremove & "')) - (select charindex ('(', '" & smsidsremove & "'))  )-1) "

                smsidsremove = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)

                Dim val As String() = smsidsremove.Split(",")

                Dim i = 0
                For i = 0 To val.Length - 1
                    Dim key = val(i)  'GrdMembers.PageIndex & "_" &
                    If hash2.ContainsKey(key) Then
                    Else
                        hash2.Add(key, val(i))
                    End If

                Next

            End If

            Session("hashtable1") = hash1
            Session("hashtable2") = hash2

            ''Bind to grid
            For Each row As GridViewRow In GrdMembers.Rows
                Dim i = 0
                Dim ch1 As CheckBox = DirectCast(row.FindControl("CheckEmailRemove"), CheckBox)
                Dim ch2 As CheckBox = DirectCast(row.FindControl("CheckSmsRemove"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

                If hash1.ContainsValue(Hid.Value) Then
                    ch1.Checked = True
                End If

                If hash2.ContainsValue(Hid.Value) Then
                    ch2.Checked = True
                End If

            Next

        End If

    End Sub

    Public Function SelectContact(ByVal GroupId As String, ByVal Contact As String) As String
        Dim Returnvalue As String = "'P'"

        If Contact <> "" Then

            Dim val As String() = Contact.Split(",")

            Dim i = 0

            If val.Length > 0 Then
                Returnvalue = ""
            End If

            For i = 0 To val.Length - 1
                If Returnvalue = "" Then
                    Returnvalue = "'" & val(i) & "'"
                Else
                    Returnvalue &= ",'" & val(i) & "'"
                End If

            Next

        Else
            ''Select using Groups

        End If

        Returnvalue = " AND CONTACT IN (" & Returnvalue & ")"


        Return Returnvalue
    End Function
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        Dim ds As DataSet
        str_query = "Select CGR_DES,CGR_CONDITION,CGR_GRP_TYPE,CGR_TYPE,CGR_CONTACT from COM_GROUPS_M where CGR_ID='" & HiddenGroupid.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim CGR_CONDITION = ""
        Dim CGR_GRP_TYPE = ""
        Dim CGR_TYPE = ""
        Dim Contact = ""
        If ds.Tables(0).Rows.Count > 0 Then

            CGR_CONDITION = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
            CGR_GRP_TYPE = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()
            CGR_TYPE = ds.Tables(0).Rows(0).Item("CGR_TYPE").ToString()
            Contact = ds.Tables(0).Rows(0).Item("CGR_CONTACT").ToString()
            lblname.Text = " : " & ds.Tables(0).Rows(0).Item("CGR_DES").ToString()

        End If

        If ds.Tables(0).Rows(0).Item("CGR_TYPE").ToString() = "STUDENT" Then

            str_query = "select *,case ROW_NUMBER() over(PARTITION BY STU_SIBLING_ID order by STU_SIBLING_ID)  when '1' then 'True' else 'False' end  S1 from (" & CGR_CONDITION & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid where b.CGAO_CGR_ID='" & HiddenGroupid.Value & "' " & SelectContact("", Contact)

        Else

            str_query = "select *, 'True' as S1, '' as contact from (" & CGR_CONDITION & ") a inner join COM_GROUPS_ADD_ON b on b.CGAO_UNIQUE_ID =a.uniqueid where b.CGAO_CGR_ID='" & HiddenGroupid.Value & "' "

        End If





        Dim Txt1 As String
        Dim Txt2 As String
        Dim Txt3 As String
        Dim Txt4 As String
        Dim Txt5 As String

        str_query = "select * from (" & str_query & ") a"

        Dim sqlwhere As String = ""

        Hiddenquery.Value = str_query



        If GrdMembers.Rows.Count > 0 Then
            Txt1 = DirectCast(GrdMembers.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim() 'Unique No
            Txt2 = DirectCast(GrdMembers.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim() 'Name
            Txt3 = DirectCast(GrdMembers.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim() 'Email
            Txt4 = DirectCast(GrdMembers.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim() 'Mobile
            'Txt5 = DirectCast(GrdMembers.HeaderRow.FindControl("Txt5"), TextBox).Text.Trim() 'Company Name



            If Txt1.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(uniqueid,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(uniqueid,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                End If

            End If

            If Txt2.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(Name,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(Name,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
                End If

            End If

            If Txt3.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(Email,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(Email,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                End If

            End If

            If Txt4.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(Mobile,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(Mobile,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
                End If

            End If

            'If Txt5.Trim() <> "" Then
            '    If sqlwhere.Length = 0 Then
            '        sqlwhere &= "  replace(comp_name,' ','') like '%" & Txt5.Replace(" ", "") & "%' "
            '    Else
            '        sqlwhere &= " and replace(comp_name,' ','') like '%" & Txt5.Replace(" ", "") & "%' "
            '    End If

            'End If

            If Txt1 <> "" Or Txt2 <> "" Or Txt3 <> "" Or Txt4 <> "" Or Txt5 <> "" Then
                sqlwhere = " Where" & sqlwhere
            End If

        End If


        If sqlwhere.Length > 6 Then
            str_query = str_query & sqlwhere
        End If

        str_query &= " order by uniqueid "




        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If lbltotal.Text = "" Then
            lbltotal.Text = " :    Total Members in this Group : " & ds.Tables(0).Rows.Count
        End If

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("uniqueid")
            dt.Columns.Add("Name")
            dt.Columns.Add("Email")
            dt.Columns.Add("Mobile")
            dt.Columns.Add("comp_name")
            dt.Columns.Add("STU_bRCVSMS")
            dt.Columns.Add("STU_bRCVMAIL")
            dt.Columns.Add("Contact")
            dt.Columns.Add("s1")


            Dim dr As DataRow = dt.NewRow()
            dr("uniqueid") = ""
            dr("Name") = ""
            dr("Email") = ""
            dr("Mobile") = ""
            dr("comp_name") = ""
            dr("STU_bRCVSMS") = False
            dr("STU_bRCVMAIL") = False
            dr("Contact") = ""
            dr("s1") = False

            dt.Rows.Add(dr)
            GrdMembers.DataSource = dt
            GrdMembers.DataBind()

            DirectCast(GrdMembers.FooterRow.FindControl("btnupdate"), Button).Visible = False


        Else


            GrdMembers.DataSource = ds
            GrdMembers.DataBind()

        End If

        If GrdMembers.Rows.Count > 0 Then

            DirectCast(GrdMembers.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GrdMembers.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(GrdMembers.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3
            DirectCast(GrdMembers.HeaderRow.FindControl("Txt4"), TextBox).Text = Txt4
            ' DirectCast(GrdMembers.HeaderRow.FindControl("Txt5"), TextBox).Text = Txt5


        End If


        Dim hash1 As New Hashtable
        Dim hash2 As New Hashtable
        hash1 = Session("hashtable1")
        hash2 = Session("hashtable2")

        For Each row As GridViewRow In GrdMembers.Rows
            Dim i = 0
            Dim ch1 As CheckBox = DirectCast(row.FindControl("CheckEmailRemove"), CheckBox)
            Dim ch2 As CheckBox = DirectCast(row.FindControl("CheckSmsRemove"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

            If hash1 Is Nothing Then
            Else
                If hash1.ContainsValue(Hid.Value) Then
                    ch1.Checked = True
                End If
            End If

            If hash1 Is Nothing Then
            Else
                If hash2.ContainsValue(Hid.Value) Then
                    ch2.Checked = True
                End If
            End If


        Next
        Session("hashtable1") = hash1
        Session("hashtable2") = hash2





    End Sub

    Protected Sub GrdMembers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdMembers.PageIndexChanging
        Dim hash1 As New Hashtable
        Dim hash2 As New Hashtable

        If Session("hashtable1") Is Nothing Then
        Else
            hash1 = Session("hashtable1")
        End If

        If Session("hashtable2") Is Nothing Then
        Else
            hash2 = Session("hashtable2")
        End If



        For Each row As GridViewRow In GrdMembers.Rows
            Dim ch1 As CheckBox = DirectCast(row.FindControl("CheckEmailRemove"), CheckBox)
            Dim ch2 As CheckBox = DirectCast(row.FindControl("CheckSmsRemove"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

            Dim key = Hid.Value 'GrdMembers.PageIndex & "_" &

            If ch1.Visible = True And ch2.Visible = True Then

                If ch1.Checked Then
                    If hash1.ContainsKey(key) Then
                    Else
                        hash1.Add(key, Hid.Value)
                    End If

                Else

                    If hash1.ContainsKey(key) Then
                        hash1.Remove(key)
                    Else
                    End If
                End If

                If ch2.Checked Then
                    If hash2.ContainsKey(key) Then
                    Else
                        hash2.Add(key, Hid.Value)
                    End If

                Else

                    If hash2.ContainsKey(key) Then
                        hash2.Remove(key)
                    Else
                    End If
                End If

            End If
        Next

        Session("hashtable1") = hash1
        Session("hashtable2") = hash2

        GrdMembers.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        If Session("membersview") = "groups" Then
            Response.Redirect("comListGroups.aspx" & mInfo)
        End If
        If Session("membersview") = "sendsms" Then
            Response.Redirect("comSendSms.aspx" & mInfo)
        End If

    End Sub

    Protected Sub GrdMembers_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrdMembers.PageIndexChanged

        lblmessage.Text = ""
        Dim hash1 As New Hashtable
        Dim hash2 As New Hashtable
        hash1 = Session("hashtable1")
        hash2 = Session("hashtable2")

        For Each row As GridViewRow In GrdMembers.Rows
            Dim i = 0
            Dim ch1 As CheckBox = DirectCast(row.FindControl("CheckEmailRemove"), CheckBox)
            Dim ch2 As CheckBox = DirectCast(row.FindControl("CheckSmsRemove"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

            If hash1.ContainsValue(Hid.Value) Then
                ch1.Checked = True
            End If

            If hash2.ContainsValue(Hid.Value) Then
                ch2.Checked = True
            End If



        Next

    End Sub


    Protected Sub GrdMembers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdMembers.RowCommand
        If e.CommandName = "remove" Then
            Dim hash1 As New Hashtable
            Dim hash2 As New Hashtable

            If Session("hashtable1") Is Nothing Then
            Else
                hash1 = Session("hashtable1")
            End If

            If Session("hashtable2") Is Nothing Then
            Else
                hash2 = Session("hashtable2")
            End If


            For Each row As GridViewRow In GrdMembers.Rows

                Dim ch1 As CheckBox = DirectCast(row.FindControl("CheckEmailRemove"), CheckBox)
                Dim ch2 As CheckBox = DirectCast(row.FindControl("CheckSmsRemove"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

                Dim key = Hid.Value 'GrdMembers.PageIndex & "_" &

                If ch1.Visible = True And ch2.Visible = True Then

                    If ch1.Checked Then
                        If hash1.ContainsKey(key) Then
                        Else
                            hash1.Add(key, Hid.Value)
                        End If

                    Else

                        If hash1.ContainsKey(key) Then
                            hash1.Remove(key)
                        Else
                        End If
                    End If

                    If ch2.Checked Then
                        If hash2.ContainsKey(key) Then
                        Else
                            hash2.Add(key, Hid.Value)
                        End If

                    Else

                        If hash2.ContainsKey(key) Then
                            hash2.Remove(key)
                        Else
                        End If
                    End If

                End If


            Next

            Session("hashtable1") = hash1
            Session("hashtable2") = hash2

            Dim idictenum As IDictionaryEnumerator
            Dim emailremoveids As String
            Dim smsremoveids As String

            If hash1.Count > 0 Then
                emailremoveids = " AND uniqueid NOT IN ("
                idictenum = hash1.GetEnumerator()
                Dim i = 0
                While (idictenum.MoveNext())
                    Dim key = idictenum.Key
                    Dim stuid = idictenum.Value
                    i = i + 1
                    If hash1.Count = i Then
                        emailremoveids &= stuid & ")"
                    Else
                        emailremoveids &= stuid & ","
                    End If


                End While



            End If

            If hash2.Count > 0 Then
                smsremoveids = " AND uniqueid NOT IN ("
                idictenum = hash2.GetEnumerator()
                Dim i = 0
                While (idictenum.MoveNext())
                    Dim key = idictenum.Key
                    Dim stuid = idictenum.Value
                    i = i + 1
                    If hash2.Count = i Then
                        smsremoveids &= stuid & ")"
                    Else
                        smsremoveids &= stuid & ","
                    End If


                End While



            End If


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sql_query = "UPDATE COM_GROUPS_M SET CGR_REMOVE_EMAIL_IDS='" & emailremoveids & "'  , CGR_REMOVE_SMS_IDS='" & smsremoveids & "' WHERE CGR_ID='" & HiddenGroupid.Value & "' "
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
            lblmessage.Text = "Successfully Updated"

        End If
        If e.CommandName = "search" Then

            Dim hash1 As New Hashtable
            Dim hash2 As New Hashtable

            If Session("hashtable1") Is Nothing Then
            Else
                hash1 = Session("hashtable1")
            End If

            If Session("hashtable2") Is Nothing Then
            Else
                hash2 = Session("hashtable2")
            End If


            For Each row As GridViewRow In GrdMembers.Rows
                Dim ch1 As CheckBox = DirectCast(row.FindControl("CheckEmailRemove"), CheckBox)
                Dim ch2 As CheckBox = DirectCast(row.FindControl("CheckSmsRemove"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

                Dim key = Hid.Value 'GrdMembers.PageIndex & "_" &

                If ch1.Checked Then
                    If hash1.ContainsKey(key) Then
                    Else
                        hash1.Add(key, Hid.Value)
                    End If

                Else

                    If hash1.ContainsKey(key) Then
                        hash1.Remove(key)
                    Else
                    End If
                End If

                If ch2.Checked Then
                    If hash2.ContainsKey(key) Then
                    Else
                        hash2.Add(key, Hid.Value)
                    End If

                Else

                    If hash2.ContainsKey(key) Then
                        hash2.Remove(key)
                    Else
                    End If
                End If

            Next

            Session("hashtable1") = hash1
            Session("hashtable2") = hash2

            BindGrid()
        End If


    End Sub


End Class
