<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comPlainText.ascx.vb" Inherits="masscom_UserControls_comPlainText" %>
<%@ Register Src="comEditPlainText.ascx" TagName="comEditPlainText" TagPrefix="uc2" %>
<%@ Register Src="comCreatePlainText.ascx" TagName="comCreatePlainText" TagPrefix="uc1" %>
<div class="matters" >
 <asp:RadioButtonList ID="RadioButtonListSelect" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                            <asp:ListItem Value="0">Create Plain Text</asp:ListItem>
                            <asp:ListItem Value="1">View/Edit Plain Text</asp:ListItem>
                        </asp:RadioButtonList>
                        <hr />
                        
                     
                        <asp:Panel ID="Panel1" runat="server" Visible="False" >
                       
                            <uc1:comCreatePlainText ID="ComCreatePlainText1" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="Panel2" runat="server" Visible="False" >
                            <uc2:comEditPlainText ID="ComEditPlainText1" runat="server" />
                        </asp:Panel>
</div>