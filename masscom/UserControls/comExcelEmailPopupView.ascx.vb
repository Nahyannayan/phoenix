﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb
Partial Class masscom_UserControls_comExcelEmailPopupView
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTempid.Value = Request.QueryString("templateid").ToString()
            Hiddenbsuid.Value = Session("sBsuid")
            HiddenEType.Value = Request.QueryString("EType").ToString()
            BindGridView()

            BindHrsMins()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub


    Public Sub BindGridView()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_query = "Select LOG_ID,TITLE,TYPE " & _
        '                ",(select count(*) from COM_EXCEL_EMAIL_DATA_LOG B where B.LOG_ID=A.LOG_ID ) TOTAL " & _
        '                ",(select count(*) from COM_EXCEL_EMAIL_DATA_LOG C where C.LOG_ID=A.LOG_ID AND NO_ERROR_FLAG='True' ) VALID " & _
        '                ",(select count(*) from COM_EXCEL_EMAIL_DATA_LOG D where D.LOG_ID=A.LOG_ID AND NO_ERROR_FLAG='False') ERROR " & _
        '                ",'openPopup('''+ convert(varchar, LOG_ID) +''')' OPENW" & _
        '                ",'openPopup2('''+ convert(varchar, LOG_ID) +''')' OPENW2" & _
        '                " from COM_EXCEL_EMAIL_DATA_MASTER A where BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim str_query = "Select LOG_ID,TITLE,TYPE " & _
                              ",'' TOTAL " & _
                              ",''VALID " & _
                              ",'' ERROR " & _
                              ",'openPopup('''+ convert(varchar, LOG_ID) +''')' OPENW" & _
                              ",'openPopup2('''+ convert(varchar, LOG_ID) +''')' OPENW2" & _
                              " from COM_EXCEL_EMAIL_DATA_MASTER A where BSU_ID='" & Hiddenbsuid.Value & "' "


        Dim txttitle As String
        Dim txttype As String


        If GridView.Rows.Count > 0 Then
            txttitle = DirectCast(GridView.HeaderRow.FindControl("txttitle"), TextBox).Text.Trim()
            txttype = DirectCast(GridView.HeaderRow.FindControl("txttype"), TextBox).Text.Trim()

            If txttitle.Trim() <> "" Then
                str_query &= " and replace(TITLE,' ','') like '%" & txttitle.Replace(" ", "") & "%' "
            End If

            If txttype.Trim() <> "" Then
                str_query &= " and replace(TYPE,' ','') like '%" & txttype.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by LOG_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("LOG_ID")
            dt.Columns.Add("TITLE")
            dt.Columns.Add("TYPE")
            dt.Columns.Add("TOTAL")
            dt.Columns.Add("VALID")
            dt.Columns.Add("ERROR")
            dt.Columns.Add("OPENW")
            dt.Columns.Add("OPENW2")

            Dim dr As DataRow = dt.NewRow()
            dr("LOG_ID") = ""
            dr("TITLE") = ""
            dr("TYPE") = ""
            dr("TOTAL") = ""
            dr("VALID") = ""
            dr("ERROR") = ""
            dr("OPENW") = ""
            dr("OPENW2") = ""

            dt.Rows.Add(dr)
            GridView.DataSource = dt
            GridView.DataBind()

        Else
            GridView.DataSource = ds
            GridView.DataBind()

            For Each row As GridViewRow In GridView.Rows

                Dim LinkSent As LinkButton = DirectCast(row.FindControl("LinkSent"), LinkButton)
                Dim LinkSent2 As LinkButton = DirectCast(row.FindControl("LinkSent2"), LinkButton)

                Dim useexe = WebConfigurationManager.AppSettings("useexe").ToString()

                If useexe = "1" Then
                    LinkSent2.Visible = True
                    LinkSent.Visible = False
                Else
                    LinkSent.Visible = True
                    LinkSent2.Visible = False
                End If

                ''Show Count
                If ds.Tables(0).Rows.Count > 0 Then

                    Dim logid = DirectCast(row.FindControl("HiddenLogid"), HiddenField).Value

                    str_query = "select count(LOG_ID) from COM_EXCEL_EMAIL_DATA_LOG B where B.LOG_ID='" & logid & "'"
                    DirectCast(row.FindControl("lbltotal"), Label).Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

                    str_query = "select count(LOG_ID) from COM_EXCEL_EMAIL_DATA_LOG B where B.LOG_ID='" & logid & "'  AND NO_ERROR_FLAG='True'"
                    DirectCast(row.FindControl("lblvalid"), Label).Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

                    str_query = "select count(LOG_ID) from COM_EXCEL_EMAIL_DATA_LOG B where B.LOG_ID='" & logid & "' AND NO_ERROR_FLAG='False'"
                    DirectCast(row.FindControl("lblerror"), Label).Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

                End If




            Next


        End If

        If GridView.Rows.Count > 0 Then

            DirectCast(GridView.HeaderRow.FindControl("txttitle"), TextBox).Text = txttitle
            DirectCast(GridView.HeaderRow.FindControl("txttype"), TextBox).Text = txttype

        End If

    End Sub



    Protected Sub GridView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView.PageIndexChanging
        GridView.PageIndex = e.NewPageIndex
        BindGridView()
    End Sub

    Protected Sub GridView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView.RowCommand
        If e.CommandName = "search" Then
            BindGridView()
        End If

        If e.CommandName = "Schedule" Then
            lblsmessage2.Text = ""
            HiddenDatalogid.Value = e.CommandArgument
            MO2.Show()

        End If

    End Sub

    Protected Sub GridView_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim lnkDESCR As LinkButton = DirectCast(e.Row.FindControl("Txt2Val"), LinkButton)
            Dim HidnLogid As HiddenField = DirectCast(e.Row.FindControl("HiddenLogid"), HiddenField)
            If Not lnkDESCR Is Nothing AndAlso Not HidnLogid Is Nothing Then

                'lnkDESCR.Attributes.Add("onClick", "return SetValuetoParent('" & CleanupStringForJavascript(lblid.Value & "||" & lnkDESCR.Text) & "');")
                lnkDESCR.Attributes.Add("onClick", "return SetSelValuetoParent('" & CleanupStringForJavascript(HidnLogid.Value) & "');")
            End If
        End If
    End Sub
    Public Shared Function CleanupStringForJavascript(ByVal Str_TexttoCleanup As String) As String
        Str_TexttoCleanup = HttpContext.Current.Server.UrlEncode(Str_TexttoCleanup)
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("'", "\'")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d%0a", " ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0a", "  ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d", " ")
        Return HttpContext.Current.Server.UrlDecode(Str_TexttoCleanup)
    End Function


    Protected Sub btnok2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        If hours < 0 Then

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EML_ID", HiddenTempid.Value)
            pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", "Using Previous Template") '' We are taking data log id data
            pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
            pParms(3) = New SqlClient.SqlParameter("@CSE_ID", 0)
            pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
            pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
            pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", HiddenDatalogid.Value)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_SCHEDULE_INSERT", pParms)

            lblsmessage2.Text = "Schedule has been successfully done"
            lblsmessage.Text = ""
            txtdate.Text = ""

            MO2.Hide()
        Else
            lblsmessage.Text = "Date time is past"
            MO2.Show()
        End If


    End Sub

    Protected Sub btncancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO2.Hide()
    End Sub
End Class
