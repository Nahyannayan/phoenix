Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comSendSms
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            CheckMenuRights()
            BindBsu()
            acadamicyear_bind()
            BindSmsText()
            BindGroups()
            Panel1.Visible = False
        End If
        AssignRights()
    End Sub
    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        For Each row As GridViewRow In Grdsmssend.Rows
            Dim addlink As LinkButton = DirectCast(row.FindControl("Lnksend"), LinkButton)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Add", addlink)
            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next


    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "M000040") And (ViewState("MainMnu_code") <> "M0000100")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub


    Public Sub BindSmsText()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " select cms_id,cms_date, '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(cms_sms_text,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,cms_sms_text " & _
                        " from com_manage_sms where CMS_BSU_ID ='" & Hiddenbsuid.Value & "' AND ISNULL(CMS_DELETED,'False')='False' "

        Dim Txt1 As String
        Dim Txt2 As String
        Dim Txt3 As String

        If Grdsmssend.Rows.Count > 0 Then
            Txt1 = DirectCast(Grdsmssend.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            Txt2 = DirectCast(Grdsmssend.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            Txt3 = DirectCast(Grdsmssend.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(cms_id,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt2.Trim() <> "" Then
                str_query &= " and REPLACE(CONVERT(VARCHAR(11), cms_date, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
            End If

            If Txt3.Trim() <> "" Then
                str_query &= " and replace(cms_sms_text,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by CMS_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("cms_id")
            dt.Columns.Add("cms_date")
            dt.Columns.Add("hide")
            dt.Columns.Add("tempview")
            dt.Columns.Add("cms_sms_text")


            Dim dr As DataRow = dt.NewRow()
            dr("cms_id") = ""
            dr("cms_date") = ""
            dr("hide") = ""
            dr("tempview") = ""
            dr("cms_sms_text") = ""

            dt.Rows.Add(dr)
            Grdsmssend.DataSource = dt
            Grdsmssend.DataBind()

            DirectCast(Grdsmssend.Rows(0).FindControl("Lnksend"), LinkButton).Visible = False
        Else
            Grdsmssend.DataSource = ds
            Grdsmssend.DataBind()

        End If

        If Grdsmssend.Rows.Count > 0 Then

            DirectCast(Grdsmssend.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(Grdsmssend.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(Grdsmssend.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

        End If
    End Sub
    Public Sub BindGroups()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT * FROM ( select * , " & _
                        " CASE CGR_TYPE WHEN 'STUDENT' THEN (select ACY_DESCR from dbo.ACADEMICYEAR_M where ACY_ID=(select top 1 CGA_ACY_ID from dbo.COM_GROUPS_A where CGA_CGR_ID= cgr_id)) ELSE '' END ACY_DESCR, " & _
                       " (case a.cgr_grp_type when 'GRP' then 'Group' else 'Add On' end)as gtype, " & _
                       " ( case a.cgr_grp_bsu_id when '0' then 'All' else (select c.BSU_SHORTNAME from BUSINESSUNIT_M c where  c.BSU_ID= a.cgr_grp_bsu_id ) end ) bsu_name,  " & _
                       " isnull(CGR_CONTACT,'P') CGR_CONTACT_C " & _
                       " from com_groups_m a ) AA where CGR_BSU_ID='" & ddlSchool.SelectedValue & "' and isnull(CGR_DELETED,'false')='false'"


        Dim Txt1 As String
        Dim Txt2 As String
        Dim DropSearch1 As String
        Dim DropSearch2 As String

        If GrdGroups.Rows.Count > 0 Then
            Txt1 = DirectCast(GrdGroups.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            Txt2 = DirectCast(GrdGroups.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            DropSearch1 = DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue
            DropSearch2 = DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(CGR_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt2.Trim() <> "" Then
                str_query &= " and replace(CGR_DES,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
            End If

            If DropSearch1.Trim() <> "0" Then
                str_query &= " and replace(CGR_TYPE,' ','') like '%" & DropSearch1.Replace(" ", "") & "%' "
            End If

            If DropSearch2.Trim() <> "0" Then
                str_query &= " and replace(cgr_grp_type,' ','') like '%" & DropSearch2.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " and CGR_BSU_ID='" & ddlSchool.SelectedValue & "'"

        If ddlACD_ID.Items.Count > 0 Then

            str_query &= " and (ACY_DESCR='" & ddlACD_ID.SelectedItem.Text & "' or ACY_DESCR='') "

        End If

        str_query &= " order by cgr_id desc"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("CGR_ID")
            dt.Columns.Add("CGR_DES")
            dt.Columns.Add("CGR_TYPE")
            dt.Columns.Add("gtype")
            dt.Columns.Add("cgr_grp_bsu_id")
            dt.Columns.Add("BSU_NAME")
            dt.Columns.Add("ACY_DESCR")
            dt.Columns.Add("CGR_CONTACT_C")


            Dim dr As DataRow = dt.NewRow()
            dr("CGR_ID") = ""
            dr("CGR_DES") = ""
            dr("CGR_TYPE") = ""
            dr("gtype") = ""
            dr("cgr_grp_bsu_id") = ""
            dr("BSU_NAME") = ""
            dr("ACY_DESCR") = ""
            dr("CGR_CONTACT_C") = ""

            dt.Rows.Add(dr)
            GrdGroups.DataSource = dt
            GrdGroups.DataBind()

            'DirectCast(GrdGroups.Rows(0).FindControl("lnklist"), LinkButton).Visible = False
            btncontinue.Visible = False
        Else

            btncontinue.Visible = True
            GrdGroups.DataSource = ds
            GrdGroups.DataBind()

        End If

        If GrdGroups.Rows.Count > 0 Then

            DirectCast(GrdGroups.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GrdGroups.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue = DropSearch1
            DirectCast(GrdGroups.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue = DropSearch2

        End If
    End Sub
    Protected Sub ddlsearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGroups()
    End Sub
    Protected Sub Grdsmssend_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grdsmssend.RowCommand
        lblmessage.Text = ""
        If e.CommandName = "send" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "select cms_sms_text  from com_manage_sms where cms_id ='" & e.CommandArgument & "'"
            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
            Hiddensmsid.Value = e.CommandArgument
            BindGroups()
            Panel1.Visible = True
            Panel3.Visible = False
        End If
        If e.CommandName = "search" Then
            BindSmsText()
        End If

    End Sub

    Protected Sub GrdGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdGroups.RowCommand
        Dim EncDec As New Encryption64
        lblmessage.Text = ""
        If e.CommandName = "list" Then
            Dim Groupid As String = e.CommandArgument
            Groupid = EncDec.Encrypt(Groupid)
            Session("membersview") = "sendsms"
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
            Response.Redirect("../comGroupMembers.aspx?Groupid=" & Groupid & mInfo)
        End If
        If e.CommandName = "search" Then
            BindGroups()
        End If

    End Sub

    Protected Sub GrdGroups_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GrdGroups.PageIndex = e.NewPageIndex
        BindGroups()

    End Sub

    Protected Sub Grdsmssend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Grdsmssend.PageIndex = e.NewPageIndex
        BindSmsText()
    End Sub

    Protected Sub btncontinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncontinue.Click
        Dim flag = 0
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        If Hiddensmsid.Value <> "" Then
            For Each row As GridViewRow In GrdGroups.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                If check.Checked Then
                    flag = 1
                    Dim groupid As String = DirectCast(row.FindControl("Hiddengrpid"), HiddenField).Value
                    Dim grpbsuid As String = DirectCast(row.FindControl("Hiddengrpbsu"), HiddenField).Value '' Not been Used 
                    Dim pParms(4) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@CSM_CGR_ID", groupid)
                    pParms(1) = New SqlClient.SqlParameter("@CSM_CMS_ID", Hiddensmsid.Value)
                    pParms(2) = New SqlClient.SqlParameter("@CSM_BSU_ID", Hiddenbsuid.Value) ''<-------------------|    (grpbsuid)
                    pParms(3) = New SqlClient.SqlParameter("@CSM_TYPE", "SMS")
                    lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COM_SEND_SMS_INSERT", pParms)
                    lblmessage.Text = lblmessage.Text.Split("-")(1)
                End If
            Next
            If flag = 0 Then
                lblmessage.Text = "Step 2-Please Select Group(s)"
            End If
            If lblmessage.Text.Trim() = "" Then
                lblmessage.Text = "Some record(s) already exists in database."
            End If

            If lblmessage.Text.IndexOf("Inserted") > -1 Then
                lblmessage.Text = "Groups assigned successfully."
            End If

        Else
            lblmessage.Text = "Step 1-Please select a message"
            lblmessage.Focus()
        End If

    End Sub

    Protected Sub Btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnback.Click
        Panel1.Visible = False
        Panel3.Visible = True
        lblmessage.Text = ""
    End Sub

    Public Sub BindBsu()

        Try

            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                                 CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
                                 & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
            ddlSchool.DataSource = ds.Tables(0)
            ddlSchool.DataTextField = "BSU_NAME"
            ddlSchool.DataValueField = "BSU_ID"
            ddlSchool.DataBind()
            ddlSchool.SelectedIndex = -1
            If Not ddlSchool.Items.FindByValue(Hiddenbsuid.Value) Is Nothing Then
                ddlSchool.Items.FindByValue(Hiddenbsuid.Value).Selected = True
                ddlSchool_SelectedIndexChanged(ddlSchool, Nothing)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Sub

    Sub acadamicyear_bind()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedItem.Value
            Dim currACY_ID As String = String.Empty
            str_Sql = "SELECT  distinct   ACADEMICYEAR_D.ACD_ACY_ID, ACADEMICYEAR_M.ACY_DESCR," & _
" (select top 1 ACD_ACY_ID from ACADEMICYEAR_D where ACD_BSU_ID = '" & BSU_ID & "' and acd_current=1) as CurrACY_ID " & _
" FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID " & _
" = ACADEMICYEAR_M.ACY_ID WHERE(ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')" & _
" ORDER BY ACADEMICYEAR_D.ACD_ACY_ID "

            Using readerAcademic As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerAcademic.HasRows = True Then
                    While readerAcademic.Read
                        ddlACD_ID.Items.Add(New ListItem(readerAcademic("ACY_DESCR"), readerAcademic("ACD_ACY_ID")))
                        currACY_ID = readerAcademic("CurrACY_ID")
                    End While
                End If
            End Using
            ddlACD_ID.ClearSelection()
            ddlACD_ID.Items.FindByValue(currACY_ID).Selected = True
            ddlACD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        If ddlACD_ID.Items.Count > 0 Then
            ddlACD_ID.Visible = True
        Else
            ddlACD_ID.Visible = False
        End If
    End Sub

    Protected Sub ddlSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        acadamicyear_bind()
        BindGroups()
    End Sub

    Protected Sub ddlACD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGroups()
    End Sub
End Class
