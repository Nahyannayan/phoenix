﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
'Imports System.Data.Oledb
Imports GemBox.Spreadsheet
Partial Class masscom_UserControls_comExcelSendSMSSelection
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            CheckMenuRights()
            
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
        AssignRights()
    End Sub
    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

   
        Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)

        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000090" And ViewState("MainMnu_code") <> "M000091") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
 




    Public Function CheckPreviousTemplateName() As Boolean
        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim title As String = txttitle.Text.Trim()
        Dim sql_text = "SELECT * FROM COM_EXCEL_SMS_DATA_MASTER WITH (NOLOCK) WHERE TITLE='" & title & "' AND BSU_ID='" & BsuId & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_text)
        Dim Returnvalue = True
        If ds.Tables(0).Rows.Count > 0 Then
            lblmessage.Text = "Title already exists. Please enter a new title."
            Returnvalue = False
        End If
        Return Returnvalue
    End Function

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        If CheckPreviousTemplateName() Then


            Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
            Dim filename As String = FileUpload1.FileName.ToString()
            Dim savepath As String = path + "SMS\" + Session("EmployeeId") + "_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)

            FileUpload1.SaveAs(savepath)
            lblmessage.Text = "Uploaded File Name :" & filename
            Dim Encr_decrData As New Encryption64
            HiddenPathEncrypt.Value = Encr_decrData.Encrypt(savepath)

            Dim useexe = WebConfigurationManager.AppSettings("useexe").ToString()

            If useexe = "1" Then
               
            Else
              
            End If


         

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TITLE", txttitle.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@TYPE", RadioType.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@EXCEL_PATH", savepath)
                pParms(3) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)


                Dim Logid = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "COM_EXCEL_SMS_DATA_MASTER_INSERT", pParms)
                Hiddenlogid.Value = Logid

                Dim myDataset As New DataSet()
                Dim Xdoc As New XmlDocument

                ''Dim HiddenPath = Encr_decrData.Decrypt(path.Replace(" ", "+"))
                'Dim strConnE As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                '                        "Data Source=" & savepath & ";" & _
                '                        "Extended Properties=""Excel 8.0;"""

                'Dim strConnE As String = ""
                'Dim mySelectStatement As String = ""

                'Dim ext = GetExtension(filename)

                'If ext = "xls" Then
                '    strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savepath & ";Extended Properties=""Excel 8.0;"""
                'ElseIf ext = "xlsx" Then
                '    strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savepath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                'End If


                Try
                    'Dim myData As New OleDbDataAdapter("SELECT ID,MobileNo,Name FROM [Sheet1$] ", strConnE)
                    'myData.TableMappings.Add("Table", "ExcelTest")
                    'myData.Fill(myDataset)


                    'mySelectStatement = "SELECT ID,MobileNo,Name FROM [Sheet1$] "
                    'Using myConnection As New OleDbConnection(strConnE)
                    '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                    '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                    '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                    '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                    '        myAdapter.Fill(myDataSet)
                    '    End Using
                    'End Using

                    'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                    SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                    Dim mObj As ExcelRowCollection
                    Dim iRowRead As Boolean
                    Dim ef As ExcelFile = New ExcelFile
                    Dim mRowObj As ExcelRow
                    Dim iRow As Integer = 1
                    Dim dataTable As New DataTable
                    Dim dr As DataRow

                    iRowRead = True

                    dataTable.Columns.Add("ID", GetType(Int64))
                    dataTable.Columns.Add("MobileNo", GetType(String))
                    dataTable.Columns.Add("Name", GetType(String))
                    ef = ExcelFile.Load(savepath)
                    'ef.LoadXls(savepath)
                    mObj = ef.Worksheets(0).Rows


                    'Dim ws As ExcelWorksheet = ef.Worksheets(0)
                    'ws.ExtractToDataTable(dataTable, ws.Rows.Count, ExtractDataOptions.StopAtFirstEmptyRow, ws.Rows(0), ws.Columns(0))

                    While iRowRead
                        mRowObj = mObj(iRow)
                        If mRowObj.Cells(0).Value Is Nothing Then
                            Exit While
                        End If
                        If mRowObj.Cells(0).Value.ToString = "" Then
                            Exit While
                        End If

                        If Not mRowObj.Cells(1).Value Is Nothing Then
                            dr = dataTable.NewRow
                            dr.Item(0) = Convert.ToInt64(mRowObj.Cells(0).Value)
                            dr.Item(1) = mRowObj.Cells(1).Value.ToString
                            'dr.Item(2) = mRowObj.Cells(0).Value.ToString
                            'dr.Item(3) = mRowObj.Cells(1).Value.ToString
                            'dr.Item(4) = "add"
                            'dr.Item(5) = iRow
                            'dr.Item(6) = "0"
                            'dr.Item(7) = "false"
                            dataTable.Rows.Add(dr)
                        End If
                        iRow += 1
                    End While

                    myDataset.Tables.Add(dataTable)

                Catch ex As Exception

                    'Dim myData As New OleDbDataAdapter("SELECT ID,MobileNo,'' as Name FROM [Sheet1$] ", strConnE)
                    'myData.TableMappings.Add("Table", "ExcelTest")
                    'myData.Fill(myDataset)

                    'mySelectStatement = "SELECT ID,MobileNo,Name FROM [Sheet1$] "
                    'Using myConnection As New OleDbConnection(strConnE)
                    '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                    '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                    '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                    '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                    '        myAdapter.Fill(myDataset)
                    '    End Using
                    'End Using

                End Try


                Dim i = 0

                Dim student_id As String
                Dim mobilenumber As String
                Dim errorflag As Boolean
                Dim str_query As String
                Dim name As String


                For i = 0 To myDataset.Tables(0).Rows.Count - 1

                    student_id = myDataset.Tables(0).Rows(i).Item("ID").ToString().Trim()
                    name = myDataset.Tables(0).Rows(i).Item("Name").ToString().Replace("'", "")

                    mobilenumber = myDataset.Tables(0).Rows(i).Item("MobileNo").ToString().Trim()
                    mobilenumber = mobilenumber.Replace("-", "").Trim()
                    'If Session("sbsuid") <> "315888" Then
                    '    mobilenumber = "971" & Right(mobilenumber, 9)
                    'Else
                    '    mobilenumber = "012" & Right(mobilenumber, 7)
                    'End If

                    If student_id <> "" And mobilenumber <> "" Then
                        If mobilenumber.Length < 10 Then
                            errorflag = False
                        Else
                            errorflag = True
                        End If

                        str_query = "insert into  COM_EXCEL_SMS_DATA_LOG (LOG_ID,ID,MOBILE_NO,NAME,NO_ERROR_FLAG) values('" & Logid & "','" & student_id & "','" & mobilenumber & "','" & name & "','" & errorflag & "') "
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End If


                Next

                transaction.Commit()
                BindLog()
                Dim jsfunc As String = "SetSelValuetoParent('" & CleanupStringForJavascript(Hiddenlogid.Value) & "')"
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsfunc, True)
            Catch ex As Exception
                transaction.Rollback()
                lblmessage.Text = "Error : " & ex.Message
            Finally
                connection.Close()

            End Try

        End If


    End Sub
    Public Shared Function CleanupStringForJavascript(ByVal Str_TexttoCleanup As String) As String
        Str_TexttoCleanup = HttpContext.Current.Server.UrlEncode(Str_TexttoCleanup)
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("'", "\'")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d%0a", " ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0a", "  ")
        Str_TexttoCleanup = Str_TexttoCleanup.Replace("%0d", " ")
        Return HttpContext.Current.Server.UrlDecode(Str_TexttoCleanup)
    End Function
    Public Sub BindLog()


        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim logId As String = Hiddenlogid.Value
        Dim str_query = "Select count(LOG_ID) from COM_EXCEL_SMS_DATA_LOG WITH (NOLOCK) where LOG_ID='" & logId & "' "
        lblmessage.Text = lblmessage.Text & "<br>Total Numbers :" & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_query = "Select count(LOG_ID) from COM_EXCEL_SMS_DATA_LOG WITH (NOLOCK)  where NO_ERROR_FLAG='false' and LOG_ID='" & logId & "' "
        lblmessage.Text = lblmessage.Text & "<br>Error in Numbers :" & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    End Sub


    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

    'Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
    '    Dim Encr_decrData As New Encryption64
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '    Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

    '    Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
    '    Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
    '    Dim hours = ts.TotalHours
    '    If hours < 0 Then

    '        Dim pParms(8) As SqlClient.SqlParameter
    '        pParms(0) = New SqlClient.SqlParameter("@CMS_ID", Hiddensmsid.Value)
    '        pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", Encr_decrData.Decrypt(HiddenPathEncrypt.Value.Replace(" ", "+")))
    '        pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
    '        pParms(3) = New SqlClient.SqlParameter("@CSM_ID", 0)
    '        pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
    '        pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
    '        pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
    '        pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", Hiddenlogid.Value)

    '        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_SCHEDULE_INSERT", pParms)
    '        lblmessage.Text = "Schedule has been successfully done"
    '        txtdate.Text = ""
    '        MO1.Hide()
    '        'lnksend.Visible = False
    '        'lnkschedule.Visible = False
    '    Else
    '        lblsmessage.Text = "Date time is past"
    '        MO1.Show()
    '    End If


    'End Sub


    'Protected Sub lnkcancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkcancel.Click
    '    lblmessage.Text = ""

    'End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        lblmessage.Text = ""
    
    End Sub
End Class
