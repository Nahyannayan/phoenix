Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_UserControls_comCreateNewsLetters
    Inherits System.Web.UI.UserControl

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            BindDefaultValues()
        End If
        port_id.Visible = False
        host_id.Visible = False
        user_id.Visible = False
        password_id.Visible = False

        AssignRights()
    End Sub
    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
        'directory2.Add("Add", btnsave)
        Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))



    End Sub

    Public Sub BindDefaultValues()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select * from BSU_COMMUNICATION_M where BSC_BSU_ID='" & Hiddenbsuid.Value & "' AND BSC_TYPE='COM'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            txtFrom.Text = ds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
            txthost.Text = ds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            txtport.Text = ds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
            txtusername.Text = ds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
            HiddenPassword.value = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
            txtpassword.Text = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
        End If


    End Sub

    Public Function CheckFileSize(ByVal path As String, ByVal templateid As String) As Boolean
        Dim val = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "SELECT P_VAL FROM COM_PARAMETER WHERE ID=1 "

        Dim f2 = New FileInfo(path)
        Dim len = f2.Length
        Dim mb = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If len > (mb * 1024 * 1024) Then
            val = False
            lblmessage.Text = "File size must be less than " & mb & "MB."
            File.Delete(path)
        End If


        Return val
    End Function

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try


            If Not FileUploadNewsletters.HasFile Then

                lblmessage.Text = "NewsLetter Attachment is required!!!"
                Return
            End If

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim str_query = "select max(EML_ID) +1 as id from COM_MANAGE_EMAIL "
            Dim str_query = "insert into COM_MANAGE_EMAIL (EML_GUID) values(NEWID()) SELECT scope_identity() as id "
            Dim save = 1
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim attachment = False
            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
            Dim val As String = ""
            If ds.Tables(0).Rows(0).Item("id").ToString() <> "" Then
                val = ds.Tables(0).Rows(0).Item("id").ToString()
            Else
                val = "1"
            End If

            Directory.CreateDirectory(serverpath + val)
            Directory.CreateDirectory(serverpath + val + "/Attachments")
            Directory.CreateDirectory(serverpath + val + "/News Letters")


            If FileUploadAttachments.HasFile Then
                Dim filen As String()
                Dim FileName As String = FileUploadAttachments.PostedFile.FileName
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)
                FileUploadAttachments.SaveAs(serverpath + val + "/Attachments/" + FileName)
                If CheckFileSize(serverpath + val + "/Attachments/" + FileName, val) Then
                    save = 1
                Else
                    save = 0
                End If
                attachment = True
            End If
            If FileUploadNewsletters.HasFile Then
                Dim filen As String()
                Dim FileName As String = FileUploadNewsletters.PostedFile.FileName
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)
                FileUploadNewsletters.SaveAs(serverpath + val + "/News Letters/" + FileName)

                Dim c As New Zip.FastZip
                c.ExtractZip(serverpath + val + "/News Letters/" + FileName, serverpath + val + "/News Letters/", String.Empty)
            Else
                lblmessage.Text = "Attachment missing"
                Return
            End If

            Dim d As New DirectoryInfo(serverpath + val + "/News Letters/")
            RadioButtonListHtml.Visible = True
            RadioButtonListHtml.DataSource = d.GetFiles("*.htm", SearchOption.TopDirectoryOnly)
            RadioButtonListHtml.DataTextField = "Name"
            RadioButtonListHtml.DataValueField = "Name"
            RadioButtonListHtml.DataBind()
            If RadioButtonListHtml.Items.Count > 0 Then
                RadioButtonListHtml.SelectedIndex = 0
            End If

            Dim pParms(15) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EML_ID", val)
            pParms(1) = New SqlClient.SqlParameter("@EML_NEWS_LETTER", "True")
            If RadioButtonListHtml.Items.Count > 0 Then
                pParms(2) = New SqlClient.SqlParameter("@EML_NEWS_LETTER_FILE_NAME", RadioButtonListHtml.SelectedItem.Text)
            End If
            pParms(3) = New SqlClient.SqlParameter("@EML_ATTACHMENT", attachment)
            pParms(4) = New SqlClient.SqlParameter("@EML_FROM", txtFrom.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@EML_TITLE", txtTitle.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@EML_SUBJECT", txtsubject.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@EML_DISPLAY", txtdisplay.Text.Trim())

            pParms(8) = New SqlClient.SqlParameter("@EML_USERNAME", txtusername.Text.Trim())
            If txtpassword.Text.Trim() <> "" Then
                pParms(9) = New SqlClient.SqlParameter("@EML_PASSWORD", txtpassword.Text.Trim())
            Else
                pParms(9) = New SqlClient.SqlParameter("@EML_PASSWORD", HiddenPassword.Value)
            End If

            pParms(10) = New SqlClient.SqlParameter("@EML_HOST", txthost.Text.Trim())
            pParms(11) = New SqlClient.SqlParameter("@EML_PORT", txtport.Text.Trim())
            'If ddSurvey.SelectedIndex > 0 Then
            '    pParms(12) = New SqlClient.SqlParameter("@EML_SURVEY_ID", ddSurvey.SelectedValue.ToString())
            'End If
            pParms(13) = New SqlClient.SqlParameter("@EML_BSU_ID", Hiddenbsuid.Value)
            pParms(14) = New SqlClient.SqlParameter("@EML_EMP_ID", Session("EmployeeId"))
            pParms(15) = New SqlClient.SqlParameter("@EML_SCHOOL_SURVEY", False)

            If save = 1 Then
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_INSERT", pParms)

                RadioButtonListHtml.Controls.Clear()
                RadioButtonListHtml.DataSource = Nothing
                RadioButtonListHtml.DataBind()
                lblmessage.Text = "Newsletter created successfully with ID "

            End If
            Dim jsFunc As String = "SetCloseValuetoParent('" + lblmessage.Text.ToString + val + "')"
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
        Catch ex As Exception
            lblmessage.TabIndex = "Error : " & ex.Message
            lblmessage.Text = ex.Message
            Dim jsFunc As String = "SetCloseValuetoParent('" + lblmessage.Text.ToString + "')"
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
        End Try

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", "SetCloseFrame()", True)
        Catch ex As Exception

        End Try

    End Sub
End Class
