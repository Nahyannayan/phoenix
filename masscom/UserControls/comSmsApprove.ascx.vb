Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports SmsService
Imports System.Threading
Partial Class masscom_UserControls_comSmsApprove
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            CheckMenuRights()
            BindApproveGrid()
            BindHrsMins()
        End If

        AssignRights()
    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        For Each row As GridViewRow In GrdSmsApprove.Rows
            Dim addlink As ImageButton = DirectCast(row.FindControl("ImageApprove"), ImageButton)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Add", addlink)
            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "M000040") And (ViewState("MainMnu_code") <> "M0000100")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")
        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
    End Sub

    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub

    Public Function VisibleSendButton(ByVal Csm_id As String) As Boolean
        Dim ReturnValue = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select count(*) from COM_LOG_SMS_TABLE where LOG_CSM_ID='" & Csm_id & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If val = 0 Then
            ReturnValue = True
        End If

        str_query = "select count(*) from COM_MANAGE_SMS_SCHEDULE where CSM_ID='" & Csm_id & "'"
        val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If val = 0 And ReturnValue = True Then
            ReturnValue = True
        Else
            ReturnValue = False
        End If

        'If Session("sBusper") = "True" Then
        '    ReturnValue = True
        'End If


        Return ReturnValue

    End Function

    Public Sub BindApproveGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select *,(case CSM_APPROVED WHEN 'NO' THEN 'True' ELSE 'False' END) DELVISBLE,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(cms_sms_text,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, " & _
        " (case c.cgr_grp_type when 'GRP' then 'Groups' else 'Add On' end) as Gtype " & _
        " from com_send_sms a WITH (NOLOCK)" & _
        " inner join com_manage_sms b WITH (NOLOCK)on  a.csm_cms_id=b.cms_id " & _
        " inner join com_groups_m c WITH (NOLOCK) on c.cgr_id= a.csm_cgr_id  where CSM_ENTRY_DATE>='01/NOV/2014' AND cgr_bsu_id='" & Hiddenbsuid.Value & "' "
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'GrdSmsApprove.DataSource = ds
        'GrdSmsApprove.DataBind()
        Dim Txt1 As String
        Dim Txt2 As String
        Dim Txt3 As String
        Dim Txt4 As String
        Dim DropSearch1 As String
        Dim DropSearch2 As String

        If GrdSmsApprove.Rows.Count > 0 Then
            Txt1 = DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim() 'Id
            Txt2 = DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim() 'Requested Date
            Txt3 = DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim() 'SMS Text
            Txt4 = DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim() 'Group
            DropSearch1 = DirectCast(GrdSmsApprove.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue
            DropSearch2 = DirectCast(GrdSmsApprove.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(CSM_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt2.Trim() <> "" Then
                str_query &= " and REPLACE(CONVERT(VARCHAR(11), CSM_ENTRY_DATE, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
            End If

            If Txt3.Trim() <> "" Then
                str_query &= " and replace(cms_sms_text,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
            End If

            If Txt4.Trim() <> "" Then
                str_query &= " and replace(CGR_DES,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
            End If

            If DropSearch1.Trim() <> "0" Then
                str_query &= " and replace(CGR_TYPE,' ','') like '%" & DropSearch1.Replace(" ", "") & "%' "
            End If

            If DropSearch2.Trim() <> "0" Then
                str_query &= " and replace(cgr_grp_type,' ','') like '%" & DropSearch2.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by a.CSM_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("CSM_ID")
            dt.Columns.Add("CSM_ENTRY_DATE")
            dt.Columns.Add("cms_sms_text")
            dt.Columns.Add("tempview")
            dt.Columns.Add("hide")
            dt.Columns.Add("CGR_DES")
            dt.Columns.Add("CGR_TYPE")
            dt.Columns.Add("Gtype")
            dt.Columns.Add("CSM_APPROVED")
            dt.Columns.Add("DELVISBLE")

            Dim dr As DataRow = dt.NewRow()
            dr("CSM_ID") = ""
            dr("CSM_ENTRY_DATE") = ""
            dr("cms_sms_text") = ""
            dr("tempview") = ""
            dr("hide") = ""
            dr("CGR_DES") = ""
            dr("CGR_TYPE") = ""
            dr("Gtype") = ""
            dr("CSM_APPROVED") = ""
            dr("DELVISBLE") = "False"

            dt.Rows.Add(dr)
            GrdSmsApprove.DataSource = dt
            GrdSmsApprove.DataBind()

            DirectCast(GrdSmsApprove.Rows(0).FindControl("Linksend"), LinkButton).Visible = False
            DirectCast(GrdSmsApprove.Rows(0).FindControl("LinkSchedule"), LinkButton).Visible = False
            DirectCast(GrdSmsApprove.Rows(0).FindControl("ImageApprove"), ImageButton).Visible = False

        Else


            GrdSmsApprove.DataSource = ds
            GrdSmsApprove.DataBind()

        End If

        If GrdSmsApprove.Rows.Count > 0 Then

            DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3
            DirectCast(GrdSmsApprove.HeaderRow.FindControl("Txt4"), TextBox).Text = Txt4
            DirectCast(GrdSmsApprove.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue = DropSearch1
            DirectCast(GrdSmsApprove.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue = DropSearch2

        End If






        For Each row As GridViewRow In GrdSmsApprove.Rows
            Dim val As String = DirectCast(row.FindControl("HiddenApproved"), HiddenField).Value
            Dim img As ImageButton = DirectCast(row.FindControl("ImageApprove"), ImageButton)
            If val = "YES" Then
                img.ImageUrl = "~/Images/tick.gif"
                Dim Csm_id As String = DirectCast(row.FindControl("HiddenCsm_id"), HiddenField).Value
                If VisibleSendButton(Csm_id) Then

                    Dim useexe = WebConfigurationManager.AppSettings("useexe").ToString()

                    If useexe = "1" Then
                        DirectCast(row.FindControl("Linksend2"), LinkButton).Visible = True
                        DirectCast(row.FindControl("Linksend"), LinkButton).Visible = False
                    Else
                        DirectCast(row.FindControl("Linksend"), LinkButton).Visible = True
                        DirectCast(row.FindControl("Linksend2"), LinkButton).Visible = False
                    End If
                    DirectCast(row.FindControl("LinkSchedule"), LinkButton).Visible = True
                Else
                    DirectCast(row.FindControl("Linksend"), LinkButton).Enabled = False
                    DirectCast(row.FindControl("Linksend2"), LinkButton).Enabled = False
                    DirectCast(row.FindControl("LinkSchedule"), LinkButton).Enabled = False
                End If
                img.Enabled = False
            Else
                img.ImageUrl = "~/Images/cross.png"
                DirectCast(row.FindControl("Linksend"), LinkButton).Visible = False
                DirectCast(row.FindControl("Linksend2"), LinkButton).Visible = False
                DirectCast(row.FindControl("LinkSchedule"), LinkButton).Visible = False
            End If
        Next


    End Sub

    Protected Sub GrdSmsApprove_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

        GrdSmsApprove.PageIndex = e.NewPageIndex
        BindApproveGrid()

    End Sub
    Protected Function GetNavigateUrl(ByVal pId As String) As String

        Return String.Format("javascript:var popup = window.showModalDialog('../comSendingSMSNormal.aspx?csm_id={0}&method={1} ', '','dialogHeight:500px;dialogWidth:500px;scroll:yes;resizable:no;'); location.reload(true); ", pId, 1)

    End Function

    Protected Sub GrdSmsApprove_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        If e.CommandName = "approve" Then
            Dim str_query = "Update COM_SEND_SMS set CSM_APPROVED='YES',CSM_APPROVE_DATE=getdate() where CSM_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            BindApproveGrid()
        End If
        If e.CommandName = "Schedule" Then
            Dim csm_id As String = e.CommandArgument
            HiddenCSM_ID.Value = csm_id
            Dim ds As DataSet
            Dim str_query = "select CSM_CGR_ID,CSM_CMS_ID from COM_SEND_SMS where CSM_ID='" & e.CommandArgument & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                HiddenCGR_ID.Value = ds.Tables(0).Rows(0).Item("CSM_CGR_ID").ToString()
                HiddenCMS_ID.Value = ds.Tables(0).Rows(0).Item("CSM_CMS_ID").ToString()

                str_query = "select * from COM_MANAGE_SMS_SCHEDULE where CSM_ID='" & e.CommandArgument & "'"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count > 0 Then
                    lblmessage.Text = "You have already scheduled this template"
                Else
                    MO1.Show()
                End If

            End If

        End If

        If e.CommandName = "Deleting" Then
            Dim str_query = "delete COM_SEND_SMS where CSM_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            BindApproveGrid()
        End If

        If e.CommandName = "search" Then
            BindApproveGrid()
        End If

    End Sub


    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        If hours < 0 Then

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CSM_ID", HiddenCSM_ID.Value)
            pParms(1) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
            pParms(2) = New SqlClient.SqlParameter("@CMS_ID", HiddenCMS_ID.Value)
            pParms(3) = New SqlClient.SqlParameter("@CGR_ID", HiddenCGR_ID.Value)
            pParms(4) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
            pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_SCHEDULE_INSERT", pParms)
            lnkschedule.Visible = False
            BindApproveGrid()
            MO1.Hide()
            lblmessage.Text = "Schedule has been successfully done"
        Else
            lblsmessage.Text = "Date time is past"
            MO1.Show()
        End If



    End Sub
    Protected Sub ddlsearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindApproveGrid()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        MO1.Hide()
    End Sub
End Class
