﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class masscom_UserControls_comMergerDocumentView
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsu.Value = Session("sBsuid")
            Hiddenemp_id.Value = Session("EmployeeId")
            Hiddentype.Value = Request.QueryString("Type")
            BindMergeTitle()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub


    Public Sub BindMergeTitle()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT MERGE_ID,MERGE_TITLE_DES FROM COM_MERGE_TABLES WHERE MERGE_BSU='" & HiddenBsu.Value & "' AND MERGE_TYPE='" & Hiddentype.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        DropMergeTitle.DataSource = ds
        DropMergeTitle.DataTextField = "MERGE_TITLE_DES"
        DropMergeTitle.DataValueField = "MERGE_ID"
        DropMergeTitle.DataBind()

        Dim list As New ListItem
        list.Text = "Select a Template"
        list.Value = "-1"
        DropMergeTitle.Items.Insert(0, list)

    End Sub

    Protected Sub DropMergeTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropMergeTitle.SelectedIndexChanged

        BindGrid(DropMergeTitle.SelectedValue)

    End Sub


    Public Sub BindGrid(ByVal mergeid As String)
        btndelete.Visible = False
        Label1.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT MERGE_UNIQUE_FIELD_NAME,MERGE_TABLE FROM COM_MERGE_TABLES WHERE MERGE_ID='" & mergeid & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            GridData.Visible = True
            Dim strConnE = ""

            Dim table = ds.Tables(0).Rows(0).Item("MERGE_TABLE").ToString()

            If System.IO.File.Exists(table) Then
                Dim myDataset As New DataSet()
                Try
                    strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & table & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                    Dim conn As New OleDbConnection(strConnE)
                    Dim myData As New OleDbDataAdapter
                    myData.SelectCommand = New OleDbCommand("SELECT * FROM [Sheet1$] ", conn)

                    myData.TableMappings.Add("Table", "ExcelTest")
                    myData.Fill(myDataset)

                Catch ex As Exception
                    strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & table & ";Extended Properties=""Excel 8.0;"""
                    Dim conn As New OleDbConnection(strConnE)
                    Dim myData As New OleDbDataAdapter
                    myData.SelectCommand = New OleDbCommand("SELECT * FROM [Sheet1$] ", conn)

                    myData.TableMappings.Add("Table", "ExcelTest")
                    myData.Fill(myDataset)

                End Try

                If myDataset.Tables(0).Rows.Count > 0 Then
                    GridData.DataSource = myDataset
                    GridData.DataBind()
                    GridData.Visible = True

                Else
                    GridData.Visible = False
                    Label1.Text = "No data found"
                End If
                btndelete.Visible = True
            Else
                Label1.Text = "File does not exists."
                btndelete.Visible = True
                GridData.Visible = False
            End If

        Else
            Label1.Text = ""
            GridData.Visible = False
        End If
    End Sub


    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        Dim count = 0
        Dim deletefalg = 0
        If Hiddentype.Value = "SMS" Then

            str_query = " select COUNT(*) from dbo.COM_LOG_SMS_TABLE A " & _
                        " INNER JOIN dbo.COM_MANAGE_SMS B  ON A.LOG_CMS_ID= B.CMS_ID " & _
                        " WHERE B.CMS_MERGE_ID='" & DropMergeTitle.SelectedValue & "'"
            count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If count = 0 Then
                deletefalg = 1
            End If

        End If

        If Hiddentype.Value = "EMAIL" Then
            str_query = " select COUNT(*) from dbo.COM_LOG_EMAIL_TABLE A " & _
                        " INNER JOIN dbo.COM_MANAGE_EMAIL B  ON A.LOG_EML_ID= B.EML_ID " & _
                        " WHERE B.EML_MERGE_ID='" & DropMergeTitle.SelectedValue & "'"
            count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If count = 0 Then
                deletefalg = 1
            End If

        End If

        If deletefalg = 1 Then
            str_query = "DELETE COM_MERGE_TABLES WHERE MERGE_ID='" & DropMergeTitle.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            BindMergeTitle()
            BindGrid(DropMergeTitle.SelectedValue)
            Label1.Text = "Merge document deleted successfully."
        Else
            Label1.Text = "This merge document has been used. You cannot delete this document."

        End If

    End Sub

End Class
