﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsSelection.ascx.vb" Inherits="masscom_UserControls_comAssignGroupsSelection" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!-- Bootstrap core CSS-->
<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->
<script type="text/javascript">

    function change_chk_state(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {
                //if (document.forms[0].elements[i].type=='checkbox' )
                //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
        }
    }

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false;

    }

    
</script>

 <script language="javascript" type="text/javascript">
     function SetCloseGrouptoParent() {
         //alert('close1');
         parent.setCloseGroupValue();
         return false;
     }
            </script>
 <script language="javascript" type="text/javascript">
     function SetSelValuetoParent(id) {
         //alert(id);
         parent.setValue(id);
         return false;
     }
            </script>

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div >
                        <asp:Panel ID="Panel3" runat="server" Visible="false">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="title-bg-lite">Step 1-Select Plain Text
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GrdEmailText" runat="server" EmptyDataText="Email Plain Text not added yet."
                                            AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Template ID">
                                                    <HeaderTemplate>
                                                        Template ID
                                                                    <br />
                                                        <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                                        <center>
                                                            <%# Eval("EML_ID") %></center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <HeaderTemplate>
                                                        Date
                                                                    <br />
                                                        <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                            TargetControlID="Txt2">
                                                        </ajaxToolkit:CalendarExtender>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <center>
                                                            <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email Text">
                                                    <HeaderTemplate>
                                                        Email Title
                                                                    <br />
                                                        <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkView" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                                            runat="server"></asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select">
                                                    <HeaderTemplate>
                                                        Select
                                                                    <br />
                                                        <br />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <center>   <asp:LinkButton ID="lnkselect" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                                            CommandName="send">Select</asp:LinkButton></center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel1" runat="server" Visible="true">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="title-bg-lite" colspan="2">Select Group
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="80%">
                                                    <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                 <td align="left" width="20%">
                                                    <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACD_ID_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="2">
                                                    <asp:GridView ID="GrdGroups" runat="server" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        AllowPaging="True" AutoGenerateColumns="false" Width="100%" OnPageIndexChanging="GrdGroups_PageIndexChanging" CssClass="table table-bordered table-row">
                                                        <RowStyle CssClass="griditem" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="true">
                                                                <HeaderTemplate>
                                                                    All
                                                                                <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <center>
                                                                        <asp:CheckBox ID="ch1" Text="" runat="server" /></center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Group Id">
                                                                <HeaderTemplate>
                                                                    Group Id
                                                                                <br />
                                                                    <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                                        ImageUrl="~/Images/forum_search.gif" />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="Hiddengrpid" Value='<%#Eval("CGR_ID")%>' runat="server" />
                                                                    <center><%#Eval("CGR_ID")%></center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Group Name">
                                                                <HeaderTemplate>
                                                                    Group Name
                                                                                <br />
                                                                    <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                                        ImageUrl="~/Images/forum_search.gif" />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                  
                                                                     <asp:LinkButton ID="Txt2Val"  Text=' <%# Eval("CGR_DES")%>' runat="server"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type">
                                                                <HeaderTemplate>
                                                                    Type
                                                                                <br />
                                                                    <asp:DropDownList ID="DropSearch1" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                                        runat="server">
                                                                        <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                                        <asp:ListItem Value="STUDENT" Text="STUDENT"></asp:ListItem>
                                                                        <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <center>     <%#Eval("CGR_TYPE")%></center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Group Type">
                                                                <HeaderTemplate>
                                                                    Group Type
                                                                                <br />
                                                                    <asp:DropDownList ID="DropSearch2" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                                        runat="server">
                                                                        <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                                        <asp:ListItem Value="GRP" Text="Group"></asp:ListItem>
                                                                        <asp:ListItem Value="AON" Text="Add On"></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="Hiddentype" Value='<%#Eval("gtype")%>' runat="server" />
                                                                    <center>   <%#Eval("gtype")%></center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Business Unit">
                                                                <HeaderTemplate>
                                                                    Business Unit
                                                                                <br />
                                                                    <br />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="Hiddengrpbsu" Value='<%#Eval("cgr_grp_bsu_id")%>' runat="server" />
                                                                    <%#Eval("BSU_NAME")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Academic Year">
                                                                <HeaderTemplate>
                                                                    Academic Year
                                                                                <br />
                                                                    <br />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <center>
                                                                        <%#Eval("ACY_DESCR")%></center>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="100px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Academic Year">
                                                                <HeaderTemplate>
                                                                    Contact
                                                                                <br />
                                                                    <br />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <center>
                                                                        <%#Eval("CGR_CONTACT_C")%></center>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="100px" />
                                                            </asp:TemplateField>
                                                            <%--                                                            <asp:TemplateField HeaderText="List">
                                                                <HeaderTemplate>
                                                                    <table class="BlueTable" width="100%">
                                                                        <tr class="matterswhite">
                                                                            <td align="center" colspan="2">
                                                                                <br />
                                                                                List
                                                                                <br />
                                                                                <br />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                             <center><asp:LinkButton ID="lnklist" Text="List" CommandName="list" CommandArgument='<%# Eval("CGR_ID") %>'
                                                                        runat="server"></asp:LinkButton></center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        </Columns>
                                                        <HeaderStyle />
                                                        <RowStyle CssClass="griditem" />
                                                        <SelectedRowStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <asp:Button ID="btncontinue" runat="server" CssClass="button" Text="Select" Visible="false" />
                                        <asp:Button ID="btnback" runat="server" CssClass="button" Text="Back" Visible="true"/>
                                       <%-- <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Do you wish to continue?"
                                            TargetControlID="btncontinue" runat="server">
                                        </ajaxToolkit:ConfirmButtonExtender>--%>
                                    </td>
            </tr>
            </table>
                        </asp:Panel>
                       <%-- <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" ConfirmText="Do you wish to continue?"
                            TargetControlID="btncontinue" runat="server">
                        </ajaxToolkit:ConfirmButtonExtender>--%>
            <asp:HiddenField ID="Hiddensmsid" runat="server" />
            <asp:HiddenField ID="HiddenTempid" runat="server" />
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />
            &nbsp;
                    </div>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
