﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comUpdateNewsLetters.ascx.vb" Inherits="masscom_UserControls_comUpdateNewsLetters" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>



<!-- Bootstrap core CSS-->
<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../cssfiles/Popup.css" rel="stylesheet" />

<script language="javascript" type="text/javascript">
    function SetCloseEditTemplatetoParent(msg) {
        //alert('close1');
        parent.setCloseEditTemplateValue(msg);
        return false;
    }

    function SetCloseFrametoParent(msg) {
        //alert('close1');
        parent.setCloseFrame(msg);
        return false;
    }
</script>

<script type="text/javascript">

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false;

    }

</script>
<div>

    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
    <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">


        <ContentTemplate>

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Update News Letter Template</td>
                </tr>
                <tr>
                    <td align="left" width="100%">
                        <table width="100%">
                            <tr>
                                <td width="100%" colspan="2">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="30%"><span class="field-label">Title</span></td>

                                            <td align="left" width="70%">
                                                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td><span class="field-label">From Email Id</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td><span class="field-label">Subject </span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtsubject" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td><span class="field-label">Display </span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtdisplay" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr id="host_id" runat="server">
                                            <td><span class="field-label">Host</span></td>

                                            <td>
                                                <asp:TextBox ID="txthost" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr id="port_id" runat="server">
                                            <td><span class="field-label">Port</span></td>

                                            <td>
                                                <asp:TextBox ID="txtport" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr id="user_id" runat="server">
                                            <td><span class="field-label">Username</span></td>

                                            <td>
                                                <asp:TextBox ID="txtusername" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr id="password_id" runat="server">
                                            <td><span class="field-label">Password</span></td>

                                            <td>
                                                <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox><br />
                                                <asp:Label ID="Label2" runat="server" CssClass="error"
                                                    Text="* Please provide if any change in Password"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="30%"><span class="field-label">Attachments</span></td>
                                <td align="left" width="70%">
                                    <asp:FileUpload ID="FileUploadEditAttachments" runat="server" />
                                    <%--  <asp:Button ID="btnEditsaveNewsLetters" runat="server" CausesValidation="False" Text="Add" CssClass="button" />
                                    --%>
                                </td>
                            </tr>

                            <tr>
                                <td><span class="field-label">News Letter</span>
                                </td>
                                <td>
                                    <asp:FileUpload ID="FileUploadEditNewsLetters" runat="server" />
                                </td>
                            </tr>

                            <tr>
                                <td align="left" colspan="2">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="left" colspan="2">
                                    <asp:RadioButtonList ID="RadioButtonListEditNewsLetters" runat="server">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btnsaveEditTemplates" runat="server" CssClass="button"
                                        Text="Save" />
                                    <asp:Button ID="btnedittemplatecancel" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnsaveEditTemplates" />

        </Triggers>
    </asp:UpdatePanel>

    <asp:HiddenField ID="HiddenFieldTemplateid" runat="server" />
    <asp:HiddenField ID="HiddenPassword" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUploadEditNewsLetters"
        Display="None" ErrorMessage="Please upload (*.zip) Files" SetFocusOnError="True"
        Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.zip)$"></asp:RegularExpressionValidator><asp:ValidationSummary
            ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
</div>
