﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSMSPopupView.ascx.vb" Inherits="masscom_UserControls_comExcelSMSPopupView" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->
<script type="text/javascript">

    function openPopup(logid) {

        if (logid != 0) {
            var sFeatures;

            var strOpen = '../comSendingSMSExcel.aspx'

            sFeatures = "dialogWidth: 1024px; ";
            sFeatures += "dialogHeight: 768px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            strOpen += "?Path=" + logid;

            strOpen += "&smsid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;

            window.showModalDialog(strOpen, "", sFeatures);

        }
        else {
            alert('Message is not selected.')
        }



    }


    function openPopup2(logid) {

        if (logid != 0) {
            var sFeatures;

            var strOpen = '../comSendingSMSNormal.aspx'

            sFeatures = "dialogWidth: 500px; ";
            sFeatures += "dialogHeight: 500px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            strOpen += "?Path=" + logid;
            strOpen += "&smsid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
            strOpen += "&method=0";
            window.showModalDialog(strOpen, "", sFeatures);

        }
        else {
            alert('Message is not selected.')
        }



    }


</script>
<script language="javascript" type="text/javascript">
    function SetSelValuetoParent(id) {
        //alert(id);
        parent.SetSelValuetoParent(id);
        return false;
    }
</script>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:Label ID="lblsmessage2" runat="server" CssClass="error"></asp:Label>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Template Collections</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView" AutoGenerateColumns="False" runat="server" AllowPaging="True"
                Width="100%" CssClass="table table-bordered table-row">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Title
                                                                <br />
                            <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenLogid" Value='<% #Eval("LOG_ID")%>' runat="server" />
                            <asp:LinkButton ID="Txt2Val" Text=' <%#Eval("TITLE")%>' runat="server"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Type
                                                                <br />
                            <asp:TextBox ID="txttype" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                                        <%#Eval("TYPE")%>
                                                    </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Count
                                                                <br />
                            Total/Valid/Error
                                                                <br />
                            <br />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                                    
                                                        <asp:Label ID="lbltotal"  Text='<% #Eval("TOTAL")%>' runat="server" ></asp:Label>
                                                   <b>/</b>
                                                         <asp:Label ID="lblvalid" runat="server" ></asp:Label>
                                                        <b>/</b>
                                                         <asp:Label ID="lblerror" runat="server" ></asp:Label>
                                                    </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false">
                        <HeaderTemplate>

                            <br />
                            Send
                                                                <br />
                            <br />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                                        <asp:LinkButton ID="LinkSent" OnClientClick='<%#Eval("OPENW")%>' runat="server">Proceed</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkSent2" OnClientClick='<%#Eval("OPENW2")%>' runat="server">Proceed</asp:LinkButton>
                                                    </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false">
                        <HeaderTemplate>

                            <br />
                            Schedule
                                                                <br />
                            <br />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                                        <asp:LinkButton ID="LinkSchedule" CommandArgument='<% #Eval("LOG_ID")%>' CommandName="Schedule"
                                                            runat="server">Schedule</asp:LinkButton>
                                                    </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>



<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="HiddenTempid" runat="server" />
<asp:HiddenField ID="HiddenDatalogid" runat="server" />
