Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_UserControls_comEditPlainText
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            BindTemplate()
            BindEmailText()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btneditplaintextsave)
        AssignRights()

    End Sub

    Public Sub BindTemplate()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim str_query = "select MERGE_ID,MERGE_TITLE_DES from COM_MERGE_TABLES where MERGE_BSU='" & BsuId & "' AND MERGE_TYPE='EMAIL'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtemplate.DataSource = ds
            ddtemplate.DataTextField = "MERGE_TITLE_DES"
            ddtemplate.DataValueField = "MERGE_ID"
            ddtemplate.DataBind()

            Dim item As New ListItem
            item.Text = "Select a Template"
            item.Value = "-1"
            ddtemplate.Items.Insert(0, item)
            Fields()
        Else
            TRDynamic.Visible = False
        End If



    End Sub
    Public Sub Fields()
        ''Clear the Previous message



        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim template As String = ddtemplate.SelectedValue
        Dim str_query = "select MERGE_FIELDS from COM_MERGE_TABLES where MERGE_ID='" & template & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim fields As String = ds.Tables(0).Rows(0).Item("MERGE_FIELDS").ToString()

            Dim Afields() As String = fields.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("Data")
            dt.Columns.Add("Value")

            Dim i = 0
            For i = 0 To Afields.Length - 1
                Dim cval As String = Afields(i)


                Dim csplit() As String = cval.Split(">")

                Dim dr As DataRow = dt.NewRow()

                dr("Value") = "$" & csplit(0) & "$"
                dr("Data") = csplit(1).Replace("$", "").Replace("<", "")

                dt.Rows.Add(dr)
            Next

            ddfields.DataSource = dt
            ddfields.DataTextField = "Data"
            ddfields.DataValueField = "Value"
            ddfields.DataBind()
            ddfields.Visible = True
            btninsert.Visible = True
        Else
            ddfields.Visible = False
            btninsert.Visible = False
        End If



    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        For Each row As GridViewRow In GrdEmailText.Rows
            Dim lnkdelete As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
            Dim lnkEdit As LinkButton = DirectCast(row.FindControl("lnkEdit"), LinkButton)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Delete", lnkdelete)
            directory2.Add("Edit", lnkEdit)

            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next

    End Sub

    Public Sub BindEmailText()
        Try

      
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "select *,'javascript:openview('''+ CONVERT(VARCHAR,EML_ID) +''')' openview,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('<span style=''color: red;font-weight:bold''> more... </span>')tempview from COM_MANAGE_EMAIL where EML_NEWS_LETTER='False' and EML_BSU_ID='" & Hiddenbsuid.Value & "' AND ISNULL(EML_DELETED,'False')='False'"
            Dim Txt1 As String
            Dim Txt2 As String
            Dim Txt3 As String

            If GrdEmailText.Rows.Count > 0 Then
                Txt1 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
                Txt2 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
                Txt3 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

                If Txt1.Trim() <> "" Then
                    str_query &= " and replace(EML_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                End If

                If Txt2.Trim() <> "" Then
                    str_query &= " and REPLACE(CONVERT(VARCHAR(11), EML_DATE, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
                End If

                If Txt3.Trim() <> "" Then
                    str_query &= " and replace(EML_TITLE,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                End If

            End If

            str_query &= " order by EML_DATE desc "


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count = 0 Then
                Dim dt As New DataTable
                dt.Columns.Add("EML_ID")
                dt.Columns.Add("EML_DATE")
                dt.Columns.Add("tempview")
                dt.Columns.Add("EML_TITLE")
                dt.Columns.Add("hide")
                dt.Columns.Add("openview")


                Dim dr As DataRow = dt.NewRow()
                dr("EML_ID") = ""
                dr("EML_DATE") = ""
                dr("tempview") = ""
                dr("EML_TITLE") = ""
                dr("hide") = ""
                dr("openview") = ""

                dt.Rows.Add(dr)
                GrdEmailText.DataSource = dt
                GrdEmailText.DataBind()

                DirectCast(GrdEmailText.Rows(0).FindControl("lnkdelete"), LinkButton).Visible = False
                DirectCast(GrdEmailText.Rows(0).FindControl("lnkEdit"), LinkButton).Visible = False
                DirectCast(GrdEmailText.Rows(0).FindControl("LinkView"), ImageButton).Visible = False
            Else
                GrdEmailText.DataSource = ds
                GrdEmailText.DataBind()

            End If

            If GrdEmailText.Rows.Count > 0 Then

                DirectCast(GrdEmailText.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
                DirectCast(GrdEmailText.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
                DirectCast(GrdEmailText.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

            End If
            For Each row As GridViewRow In GrdEmailText.Rows
                Dim val As String = DirectCast(row.FindControl("HiddenFieldId"), HiddenField).Value
                Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

                Dim GrdAttachment As GridView = DirectCast(row.FindControl("GrdAttachment"), GridView)
                Dim d As New DirectoryInfo(serverpath + val + "/Attachments/")
                GrdAttachment.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                GrdAttachment.DataBind()

                Dim lnkdeletedir As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
                lnkdeletedir.CommandArgument = val

                For Each arow As GridViewRow In GrdAttachment.Rows
                    Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
                    lnkattachment.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
                    ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkattachment)
                    Dim lnkdelete As LinkButton = DirectCast(arow.FindControl("lnkdelete"), LinkButton)
                    lnkdelete.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
                Next

            Next
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GrdTextAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdEmailText.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If
        If e.CommandName = "Remove" Then
            Dim f As New FileInfo(e.CommandArgument)
            If f.Exists Then
                f.Delete()
            End If
            BindEmailText()
            lblmessage.Text = "Attachment Deleted"
            BindEmailText()
        End If
        If e.CommandName = "search" Then
            BindEmailText()
        End If


    End Sub
    Protected Sub GrdEmailText_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdEmailText.RowCommand
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        If e.CommandName = "RemoveDir" Then
            If CheckActive(e.CommandArgument) Then
                Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
                Dim str_query = "UPDATE COM_MANAGE_EMAIL SET EML_DELETED='True' where EML_ID='" & e.CommandArgument & "'"
                SqlHelper.ExecuteNonQuery(str_conn.ToString, CommandType.Text, str_query)
                Dim path As String = (serverpath + e.CommandArgument)

                If System.IO.Directory.Exists(path) Then
                    Try
                        System.IO.Directory.Delete(path, True)
                    Catch ex As Exception

                    End Try

                End If


                lblmessage.Text = "Template Deleted"
                BindEmailText()
            Else
                lblmessage.Text = "Template cannot be deleted. This template has been sent."
            End If


        End If
        If e.CommandName = "Editing" Then
            If CheckActive(e.CommandArgument) Then
                Panel3.Visible = False
                Panel4.Visible = True
                HiddenFieldTemplateid.Value = e.CommandArgument
                Dim str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & e.CommandArgument & "'"
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)
                txtEditEmailText.Content = ds.Tables(0).Rows(0).Item("EML_BODY").ToString()
                txtFrom.Text = ds.Tables(0).Rows(0).Item("EML_FROM").ToString()
                txtTitle.Text = ds.Tables(0).Rows(0).Item("EML_TITLE").ToString()
                txtsubject.Text = ds.Tables(0).Rows(0).Item("EML_SUBJECT").ToString()
                txtdisplay.Text = ds.Tables(0).Rows(0).Item("EML_DISPLAY").ToString()
                txthost.Text = ds.Tables(0).Rows(0).Item("EML_HOST").ToString()
                txtport.Text = ds.Tables(0).Rows(0).Item("EML_PORT").ToString()
                txtusername.Text = ds.Tables(0).Rows(0).Item("EML_USERNAME").ToString()
                HiddenPassword.Value = ds.Tables(0).Rows(0).Item("EML_USERNAME").ToString()
                txtpassword.Text = ds.Tables(0).Rows(0).Item("EML_PASSWORD").ToString()
                If (Convert.ToString(ds.Tables(0).Rows(0).Item("isCriticalEmail")) = "2") Then
                    ddlPriority.SelectedItem.Value = "2"


                Else
                    ddlPriority.SelectedItem.Value = "1"
                End If
                Dim mergeid = ds.Tables(0).Rows(0).Item("EML_MERGE_ID").ToString()

                If mergeid <> "" Then
                    ddtemplate.SelectedValue = mergeid
                    Fields()
                End If

                lblmessage.Text = "You can edit the Email text and add new attachments."
            Else
                lblmessage.Text = "You cannot edit this template.Message has been sent."
            End If

        End If
    End Sub
    Public Function CheckActive(ByVal id As String) As Boolean
        Dim returnvalue As Boolean = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select count(*) from COM_LOG_EMAIL_TABLE where LOG_EML_ID='" & id & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If val = 0 Then
            returnvalue = True
        End If

        If Session("sBusper") = "True" Then
            returnvalue = True
        End If

        Return returnvalue
    End Function

    Protected Sub btneditplaintextsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btneditplaintextsave.Click
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim attachment = False
            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
            Dim val As String = HiddenFieldTemplateid.Value
            Dim save = 1
            If FileUploadEditEmailtext.HasFile Then
                Dim filen As String()
                Dim FileName As String = FileUploadEditEmailtext.PostedFile.FileName
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)
                FileUploadEditEmailtext.SaveAs(serverpath + val + "/Attachments/" + FileName)

                If CheckFileSize(serverpath + val + "/Attachments/" + FileName, val) Then
                    save = 1
                Else
                    save = 0
                End If
                attachment = True
            End If

            Dim pParms(15) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EML_ID", val)
            pParms(1) = New SqlClient.SqlParameter("@EML_NEWS_LETTER", "False")
            pParms(2) = New SqlClient.SqlParameter("@EML_BODY", txtEditEmailText.Content)
            If attachment = True Then
                pParms(3) = New SqlClient.SqlParameter("@EML_ATTACHMENT", attachment)
            End If
            pParms(4) = New SqlClient.SqlParameter("@OPTION", 2)
            pParms(5) = New SqlClient.SqlParameter("@EML_FROM", txtFrom.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@EML_TITLE", txtTitle.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@EML_SUBJECT", txtsubject.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@EML_DISPLAY", txtdisplay.Text.Trim())

            pParms(9) = New SqlClient.SqlParameter("@EML_USERNAME", txtusername.Text.Trim())
            If txtpassword.Text.Trim() <> "" Then
                pParms(10) = New SqlClient.SqlParameter("@EML_PASSWORD", txtpassword.Text.Trim())
            Else
                pParms(10) = New SqlClient.SqlParameter("@EML_PASSWORD", HiddenPassword.Value)
            End If

            If (ddlPriority.SelectedItem.Value = "2") Then
                Dim criticalEmailPath As String = WebConfigurationManager.AppSettings("CriticalEmailHost").ToString
                pParms(11) = New SqlClient.SqlParameter("@EML_HOST", criticalEmailPath.ToString)
            Else
                pParms(11) = New SqlClient.SqlParameter("@EML_HOST", txthost.Text.Trim())
            End If

            pParms(12) = New SqlClient.SqlParameter("@EML_PORT", txtport.Text.Trim())

            If ddtemplate.SelectedIndex > 0 Then
                pParms(13) = New SqlClient.SqlParameter("@EML_MERGE_ID", ddtemplate.SelectedValue)
            End If
            If (ddlPriority.SelectedItem.Value = "2") Then
                pParms(145) = New SqlClient.SqlParameter("@EML_CRITICAL", 1)
            Else
                pParms(14) = New SqlClient.SqlParameter("@EML_CRITICAL", 0)
            End If
            If save = 1 Then
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_UPDATE", pParms)


                txtEditEmailText.Content = ""
                lblmessage.Text = "Email Template Updated"
                BindEmailText()
                Panel3.Visible = True
                Panel4.Visible = False
            End If

        Catch ex As Exception

        End Try
     
    End Sub
    Public Function CheckFileSize(ByVal path As String, ByVal templateid As String) As Boolean
        Dim val = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pVal As String = 1
        Dim str_query = "SELECT P_VAL FROM COM_PARAMETER WHERE ID= " & pVal

        Dim f2 = New FileInfo(path)
        Dim len = f2.Length
        Dim mb = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If len > (mb * 1024 * 1024) Then
            val = False
            lblmessage.Text = "File size must be less than " & mb & "MB."
            File.Delete(path)
        End If


        Return val
    End Function

    Protected Sub btnplaintextcancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnplaintextcancel.Click
        Panel3.Visible = True
        Panel4.Visible = False
        lblmessage.Text = ""
    End Sub

  
    Protected Sub GrdEmailText_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdEmailText.PageIndexChanging
        GrdEmailText.PageIndex = e.NewPageIndex
        BindEmailText()
    End Sub


    Protected Sub ddtemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtEditEmailText.Content = ""
        Fields()
    End Sub

    Protected Sub btninsert_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtEditEmailText.Content = txtEditEmailText.Content & ddfields.SelectedValue
    End Sub


End Class
