<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comReportsPlainText.ascx.vb" Inherits="masscom_UserControls_comReportsPlainText" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!-- Bootstrap core CSS-->
<link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/PHOENIXBETA/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


<!-- Bootstrap header files ends here -->

<script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
<script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="/PHOENIXBETA/cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">

    function openWindow(a, b) {
        window.open('../comViewEmailReports.aspx?SendingId=' + a + '&ScheduleId=' + b, '', 'Height=710px,Width=1020px,scrollbars=yes,resizable=no,directories=no'); return false;
    }

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false;

    }

    function openWindowActions(rcid) {

        var sFeatures;
        sFeatures = "dialogWidth: 300px; ";
        sFeatures += "dialogHeight: 300px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "../comChangeActions.aspx?stype=EMAIL" + '&recordid=' + rcid

        var result;
        //result = window.showModalDialog(strOpen, "", sFeatures);
        //window.location.reload(true)
        return ShowWindowWithClose(strOpen, 'search', '50%', '50%')
    }

    //general close frame used in edit template
    function setCloseFrame() {
        //alert('x');
        CloseFrame();
    }

    function CloseFrame() {
        jQuery.fancybox.close();
    }

</script>
<div>
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


             <table border="0" cellpadding="0" cellspacing="0" width="100%">
                         <%--   <tr>
                                <td class="title-bg-lite">Plain Text Reports-(Scheduled)</td>
                            </tr>--%>
                            <tr>
                                <td>
                                    <asp:GridView ID="GrdScheduled" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" OnPageIndexChanging="GrdScheduled_PageIndexChanging" CssClass="table table-bordered table-row">
                                        <HeaderStyle />
                                        <SelectedRowStyle />
                                        <EmptyDataRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EditRowStyle />
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID">
                                                <HeaderTemplate>
                                                    ID
                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />

                                                    <center>
                                       <%#Eval("SCHEDULE_ID")%>
                                   </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <HeaderTemplate>
                                                    Type
                                        
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                       <%#Eval("TYPE")%>
                                   </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NewsLetter">
                                                <HeaderTemplate>
                                                    Plain&nbsp;Text
                                         
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkView1" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                                        runat="server" Style="white-space: pre-wrap;" Width="180"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Count">
                                                <HeaderTemplate>
                                                    Count
                                          
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                       <%#Eval("TOTAL_COUNT")%>
                                   </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sent">
                                                <HeaderTemplate>
                                                    Sent
                                          
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                       <%#Eval("SUCCESSFULLY_SENT")%>
                                   </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Failed">
                                                <HeaderTemplate>
                                                    Failed
                                        
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                       <%#Eval("SENT_FAILED")%>
                                   </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule Date">
                                                <HeaderTemplate>
                                                    Scheduled&nbsp;Date
                                         
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("SCHEDULE_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("SCHEDULE_DATE_TIME", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start Time">
                                                <HeaderTemplate>
                                                    Start&nbsp;Time
                                         
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("START_TIME", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("START_TIME", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Time">
                                                <HeaderTemplate>
                                                    End&nbsp;Time
                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("END_TIME", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("END_TIME", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User Sent">
                                                <HeaderTemplate>
                                                    Sent&nbsp;By
                                          
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                       <%#Eval("EMP_NAME")%>
                                   </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Detail">
                                                <HeaderTemplate>
                                                    Detail
                                        
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                       <asp:LinkButton ID="LinkView" runat="server" OnClientClick='<%#Eval("VIEWDETAILS")%>'>View</asp:LinkButton></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions" Visible="true">
                                                <HeaderTemplate>
                                                    Actions
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><asp:LinkButton ID="LinkActions" OnClientClick='<%#Eval("ACTIONS")%>' runat="server">Actions</asp:LinkButton></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="griditem" />
                                    </asp:GridView>
                                    <asp:Button ID="btnrefresh" runat="server" CssClass="button" Text="Refresh" /></td>
                            </tr>
                        </table>

            <ajaxToolkit:TabContainer ID="Tab1" Width="100%" runat="server" ActiveTabIndex="0" ScrollBars="Horizontal" Visible="false">
                <ajaxToolkit:TabPanel ID="T1" Width="100%" runat="server">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg-lite">Plain Text Reports-(Normal)</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GrdReports" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID">
                                                <HeaderTemplate>
                                                    ID
                                     
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />

                                                    <center><%#Eval("SENDING_ID")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <HeaderTemplate>
                                                    Type
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("TYPE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Plain Text">
                                                <HeaderTemplate>
                                                    Plain&nbsp;Text
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="LinkView1" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                                        runat="server" Style="white-space: pre-wrap;" Width="180"></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Count">
                                                <HeaderTemplate>
                                                    Count
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("TOTAL_MESSAGE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sent">
                                                <HeaderTemplate>
                                                    Sent
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("SUCCESSFULLY_SEND")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Failed">
                                                <HeaderTemplate>
                                                    Failed
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("SENDING_FAILED")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start Time">
                                                <HeaderTemplate>
                                                    Start&nbsp;Time
                                     
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("START_TIME", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("START_TIME", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Time">
                                                <HeaderTemplate>
                                                    End&nbsp;Time
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("END_TIME", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("END_TIME", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User Sent">
                                                <HeaderTemplate>
                                                    Sent&nbsp;By
                                     
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("EMP_NAME")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <HeaderTemplate>
                                                    Date
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("ENTRY_DATE", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Detail">
                                                <HeaderTemplate>
                                                    Detail
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><asp:LinkButton ID="LinkView" OnClientClick='<%#Eval("VIEWDETAILS")%>' runat="server">View</asp:LinkButton></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle />
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EmptyDataRowStyle />
                                        <EditRowStyle />
                                    </asp:GridView>
                                    <asp:Button ID="btnrefresh2" runat="server" CssClass="button" OnClick="btnrefresh2_Click"
                                        Text="Refresh" /></td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                    </ContentTemplate>
                    <HeaderTemplate>
                        Normal
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="T2" Width="100%" runat="server">
                    <ContentTemplate>
                       
                    </ContentTemplate>
                    <HeaderTemplate>
                        Scheduled
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>

     <script type="text/javascript" lang="javascript">
         function ShowWindowWithClose(gotourl, pageTitle, w, h) {
             $.fancybox({
                 type: 'iframe',
                 //maxWidth: 300,
                 href: gotourl,
                 //maxHeight: 600,
                 fitToView: true,
                 padding: 6,
                 width: w,
                 height: h,
                 autoSize: false,
                 openEffect: 'none',
                 showLoading: true,
                 closeClick: true,
                 closeEffect: 'fade',
                 'closeBtn': true,
                 afterLoad: function () {
                     this.title = '';//ShowTitle(pageTitle);
                 },
                 helpers: {
                     overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                     title: { type: 'inside' }
                 },
                 onComplete: function () {
                     $("#fancybox-wrap").css({ 'top': '90px' });

                 },
                 onCleanup: function () {
                     var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                     if (hfPostBack == "Y")
                         window.location.reload(true);
                 }
             });

             return false;
         }
    </script>



</div>
