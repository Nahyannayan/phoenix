Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class masscom_UserControls_comReportsPlainText
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            'BindGridNormal()
            BindGridScheduled()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub


    Public Sub BindGridNormal()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT 'javascript:openview('''+ CONVERT(VARCHAR,A.EML_ID) +''')' openview,SENDING_ID,TYPE,B.EML_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('</br><span style=''color: red;font-weight:bold''> Show... </span>')tempview,EML_FROM,EML_TITLE,EML_SUBJECT,EML_BODY,EML_ATTACHMENT," & _
                        " SENDING_ID,CSE_ID,TOTAL_MESSAGE,SUCCESSFULLY_SEND,SENDING_FAILED, " & _
                        " START_TIME, END_TIME, B.ENTRY_DATE, " & _
                        " ISNULL(EMP_FNAME,'')+ISNULL(EMP_MNAME,'')+ISNULL(EMP_LNAME,'')EMP_NAME " & _
                        " ,'javascript:openWindow(''' + CONVERT(VARCHAR,SENDING_ID) + ''',''0'');return false;' VIEWDETAILS " & _
                        " FROM dbo.COM_MANAGE_EMAIL A " & _
                        " INNER JOIN dbo.COM_EXCEL_EMAIL_SENDING_LOG B ON A.EML_ID=B.EML_ID " & _
                        " LEFT JOIN dbo.EMPLOYEE_M C ON C.EMP_ID=B.SENDING_USER_EMP_ID " & _
                        " WHERE EML_NEWS_LETTER='False'" & _
                        " AND B.BSU_ID='" & Hiddenbsuid.Value & "' ORDER BY B.SENDING_ID DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GrdReports.DataSource = ds
        GrdReports.DataBind()
    End Sub
    Public Sub BindGridScheduled()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT 'javascript:openview('''+ CONVERT(VARCHAR,A.EML_ID) +''')' openview,RECORD_ID as SCHEDULE_ID, " & _
                        " (CASE CGR_ID WHEN 0 THEN 'Excel' ELSE 'Groups/Add on' end ) as  TYPE " & _
                        " ,B.EML_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('</br><span style=''color: red;font-weight:bold''> Show... </span>')tempview,EML_TITLE,EML_FROM,EML_SUBJECT,EML_BODY,EML_ATTACHMENT, " & _
                        " CSE_ID,TOTAL_COUNT,SUCCESSFULLY_SENT,SENT_FAILED,  " & _
                        " (SELECT CONVERT(Datetime, START_TIME, 21)) AS START_TIME, (SELECT CONVERT(Datetime, END_TIME, 21)) AS END_TIME, (SELECT CONVERT(Datetime, SCHEDULE_DATE_TIME, 21)) AS SCHEDULE_DATE_TIME, (SELECT CONVERT(Datetime, B.ENTRY_DATE, 21)) AS ENTRY_DATE, " & _
                        " ISNULL(EMP_FNAME,'')+ISNULL(EMP_MNAME,'')+ISNULL(EMP_LNAME,'')EMP_NAME  , " & _
                        " 'javascript:openWindow(''0'',''' + CONVERT(VARCHAR,RECORD_ID) + ''');return false;' VIEWDETAILS,   " & _
                        " 'javascript:openWindowActions(''' + CONVERT(VARCHAR,RECORD_ID) + ''');return false;' ACTIONS   " & _
                        " FROM dbo.COM_MANAGE_EMAIL A  " & _
                        " INNER JOIN dbo.COM_MANAGE_EMAIL_SCHEDULE B ON A.EML_ID=B.EML_ID " & _
                        " LEFT JOIN dbo.EMPLOYEE_M C ON C.EMP_ID=B.ENTRY_EMP_ID " & _
                        " WHERE EML_NEWS_LETTER='False'" & _
                        " AND B.ENTRY_BSU_ID='" & Hiddenbsuid.Value & "' ORDER BY B.RECORD_ID DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GrdScheduled.DataSource = ds
        GrdScheduled.DataBind()

    End Sub


    Protected Sub GrdReports_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdReports.PageIndexChanging
        GrdReports.PageIndex = e.NewPageIndex
        BindGridNormal()
    End Sub

    Protected Sub GrdScheduled_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GrdScheduled.PageIndex = e.NewPageIndex
        BindGridScheduled()
    End Sub

    Protected Sub btnrefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnrefresh.Click
        BindGridScheduled()
    End Sub

    Protected Sub btnrefresh2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGridNormal()
    End Sub

   
End Class
