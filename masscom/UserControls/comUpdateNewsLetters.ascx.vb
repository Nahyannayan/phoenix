﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_UserControls_comUpdateNewsLetters
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnsaveEditTemplates)
        ScriptManager1.RegisterPostBackControl(btnsaveEditTemplates)

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            'BindNewsletters()
            'BindTemplate()
            'BindDefaultValues()
            Dim Encr_decrData As New Encryption64
            ViewState("hdn_Templateid") = Request.QueryString("hdn_Templateid")

            If CheckActive(ViewState("hdn_Templateid")) Then
                'Panel1.Visible = False
                'Panel2.Visible = True
                HiddenFieldTemplateid.Value = ViewState("hdn_Templateid")
                Dim str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & ViewState("hdn_Templateid") & "'"
                Dim ds As DataSet
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                ds = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtFrom.Text = ds.Tables(0).Rows(0).Item("EML_FROM").ToString()
                    txtTitle.Text = ds.Tables(0).Rows(0).Item("EML_TITLE").ToString()
                    txtsubject.Text = ds.Tables(0).Rows(0).Item("EML_SUBJECT").ToString()
                    txtdisplay.Text = ds.Tables(0).Rows(0).Item("EML_DISPLAY").ToString()
                    txthost.Text = ds.Tables(0).Rows(0).Item("EML_HOST").ToString()
                    txtport.Text = ds.Tables(0).Rows(0).Item("EML_PORT").ToString()
                    txtusername.Text = ds.Tables(0).Rows(0).Item("EML_USERNAME").ToString()
                    txtpassword.Text = ds.Tables(0).Rows(0).Item("EML_PASSWORD").ToString()
                    HiddenPassword.Value = ds.Tables(0).Rows(0).Item("EML_PASSWORD").ToString()

                End If
                lblmessage.Text = "You can add attachments and change the existing news letter"
            Else
                lblmessage.Text = "You cannot edit this template.Message has been sent."
            End If
        End If
        host_id.Visible = False
        port_id.Visible = False
        user_id.Visible = False
        password_id.Visible = False


        AssignRights()
    End Sub
    Protected Sub lnkAtt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbtnEm As LinkButton = DirectCast(sender, LinkButton)
        Try


            Dim lnkVal As New HiddenField()
            lnkVal = TryCast(sender.FindControl("lnkVal"), HiddenField)
            Dim val As String = lnkVal.Value



            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

            Dim Path As String = serverpath + val + "/Attachments/" + lbtnEm.Text.Trim()

            Dim bytes() As Byte = File.ReadAllBytes(Path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(Path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message.ToString + ex.StackTrace, "lnkAtt")
        End Try

    End Sub



    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        'For Each row As GridViewRow In GrdNewsletterView.Rows
        '    Dim lnkdelete As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
        '    Dim lnkEdit As LinkButton = DirectCast(row.FindControl("lnkEdit"), LinkButton)
        '    Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
        '    directory2.Add("Delete", lnkdelete)
        '    directory2.Add("Edit", lnkEdit)
        '    Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        'Next


    End Sub

    'Public Sub BindNewsletters()
    '    Try


    '        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        Dim str_query = "select *,'javascript:openview('''+ CONVERT(VARCHAR,EML_ID) +''')' openview from COM_MANAGE_EMAIL where EML_NEWS_LETTER='True' and EML_BSU_ID='" & Hiddenbsuid.Value & "' AND ISNULL(EML_DELETED,'False')='False'"
    '        Dim Txt1 As String
    '        Dim Txt2 As String
    '        Dim Txt3 As String

    '        If GrdNewsletterView.Rows.Count > 0 Then
    '            Txt1 = DirectCast(GrdNewsletterView.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
    '            Txt2 = DirectCast(GrdNewsletterView.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
    '            Txt3 = DirectCast(GrdNewsletterView.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

    '            If Txt1.Trim() <> "" Then
    '                str_query &= " and replace(EML_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
    '            End If

    '            If Txt2.Trim() <> "" Then
    '                str_query &= " and REPLACE(CONVERT(VARCHAR(11), EML_DATE, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
    '            End If

    '            If Txt3.Trim() <> "" Then
    '                str_query &= " and replace(EML_NEWS_LETTER_FILE_NAME,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
    '            End If

    '        End If

    '        str_query &= " order by EML_DATE desc "


    '        Dim ds As DataSet
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '        If ds.Tables(0).Rows.Count = 0 Then
    '            Dim dt As New DataTable
    '            dt.Columns.Add("EML_ID")
    '            dt.Columns.Add("EML_NEWS_LETTER_FILE_NAME")
    '            dt.Columns.Add("EML_DATE")
    '            dt.Columns.Add("openview")

    '            Dim dr As DataRow = dt.NewRow()
    '            dr("EML_ID") = ""
    '            dr("EML_NEWS_LETTER_FILE_NAME") = ""
    '            dr("EML_DATE") = ""
    '            dr("openview") = ""

    '            dt.Rows.Add(dr)
    '            GrdNewsletterView.DataSource = dt
    '            GrdNewsletterView.DataBind()

    '            DirectCast(GrdNewsletterView.Rows(0).FindControl("lnkdelete"), LinkButton).Visible = False
    '            DirectCast(GrdNewsletterView.Rows(0).FindControl("lnkEdit"), LinkButton).Visible = False
    '        Else
    '            GrdNewsletterView.DataSource = ds
    '            GrdNewsletterView.DataBind()

    '        End If

    '        If GrdNewsletterView.Rows.Count > 0 Then

    '            DirectCast(GrdNewsletterView.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
    '            DirectCast(GrdNewsletterView.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
    '            DirectCast(GrdNewsletterView.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

    '        End If

    '        For Each row As GridViewRow In GrdNewsletterView.Rows

    '            Dim val As String = DirectCast(row.FindControl("HiddenFieldId"), HiddenField).Value
    '            Dim Filename As String = DirectCast(row.FindControl("HiddenFieldFileName"), HiddenField).Value

    '            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
    '            Dim lnknewsletter As LinkButton = DirectCast(row.FindControl("lnknewsletter"), LinkButton)

    '            lnknewsletter.CommandArgument = val + "/News Letters/" & Filename
    '            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnknewsletter)
    '            Dim GrdAttachment As GridView = DirectCast(row.FindControl("GrdAttachment"), GridView)
    '            Dim d As New DirectoryInfo(serverpath + val + "/Attachments/")

    '            If d.Exists Then
    '                ' If (d.GetFiles("*.*", SearchOption.TopDirectoryOnly).Length > 0) Then
    '                GrdAttachment.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
    '                GrdAttachment.DataBind()
    '                'End If
    '            End If


    '            Dim lnkdeletedir As LinkButton = DirectCast(row.FindControl("lnkdelete"), LinkButton)
    '            lnkdeletedir.CommandArgument = val

    '            For Each arow As GridViewRow In GrdAttachment.Rows
    '                Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
    '                Dim lnkVal As HiddenField = DirectCast(arow.FindControl("lnkVal"), HiddenField)
    '                lnkVal.Value = val
    '                'lnkattachment.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
    '                'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkattachment)
    '                Dim lnkdelete As LinkButton = DirectCast(arow.FindControl("lnkdelete"), LinkButton)
    '                lnkdelete.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
    '            Next

    '        Next
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, "masscomeditnews")

    '    End Try
    'End Sub
    Public Function CheckFileSize(ByVal path As String, ByVal templateid As String) As Boolean
        Dim val = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "SELECT P_VAL FROM COM_PARAMETER WHERE ID=1 "

        Dim f2 = New FileInfo(path)
        Dim len = f2.Length
        Dim mb = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If len > (mb * 1024 * 1024) Then
            val = False
            lblmessage.Text = "File size must be less than " & mb & "MB."
            File.Delete(path)
        End If


        Return val
    End Function

    Protected Sub btnsaveEditTemplates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsaveEditTemplates.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim Val As String = HiddenFieldTemplateid.Value
        Dim attachment = False
        Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
        Dim save = 1
        If FileUploadEditAttachments.HasFile Then
            Dim filen As String()
            Dim FileName As String = FileUploadEditAttachments.PostedFile.FileName
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)
            FileUploadEditAttachments.SaveAs(serverpath + Val + "/Attachments/" + FileName)
            If CheckFileSize(serverpath + Val + "/Attachments/" + FileName, Val) Then
                save = 1
            Else
                save = 0
            End If
            attachment = True
        End If

        If FileUploadEditNewsLetters.HasFile Then
            '' Deleting the previous files

            Dim path As String = (serverpath + Val + "/News Letters")

            If System.IO.Directory.Exists(path) Then
                Try
                    System.IO.Directory.Delete(path, True)
                Catch ex As Exception

                End Try

            End If
            '' Create a new Directory

            Dim strDir As String = serverpath + Val + "/News Letters"
            Dim dir As New System.IO.DirectoryInfo(strDir)
            If dir.Exists = False Then
                dir.Create()
            End If


            Dim filen As String()
            Dim FileName As String = FileUploadEditNewsLetters.PostedFile.FileName
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)
            FileUploadEditNewsLetters.SaveAs(serverpath + Val + "/News Letters/" + FileName)

            Dim c As New Zip.FastZip
            c.ExtractZip(serverpath + Val + "/News Letters/" + FileName, serverpath + Val + "/News Letters/", String.Empty)

        End If

        Dim d As New DirectoryInfo(serverpath + Val + "/News Letters/")
        RadioButtonListEditNewsLetters.Visible = True
        RadioButtonListEditNewsLetters.DataSource = d.GetFiles("*.htm", SearchOption.TopDirectoryOnly)
        RadioButtonListEditNewsLetters.DataTextField = "Name"
        RadioButtonListEditNewsLetters.DataValueField = "Name"
        RadioButtonListEditNewsLetters.DataBind()

        If RadioButtonListEditNewsLetters.Items.Count > 0 Then
            RadioButtonListEditNewsLetters.SelectedIndex = 0
        End If


        Dim pParms(14) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EML_ID", Val)
        pParms(1) = New SqlClient.SqlParameter("@EML_NEWS_LETTER", "True")

        If RadioButtonListEditNewsLetters.Items.Count > 0 And FileUploadEditNewsLetters.HasFile Then
            pParms(2) = New SqlClient.SqlParameter("@EML_NEWS_LETTER_FILE_NAME", RadioButtonListEditNewsLetters.SelectedItem.Text)
        End If

        If attachment = True Then
            pParms(3) = New SqlClient.SqlParameter("@EML_ATTACHMENT", attachment)
        End If
        pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
        pParms(5) = New SqlClient.SqlParameter("@EML_FROM", txtFrom.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@EML_TITLE", txtTitle.Text.Trim())
        pParms(7) = New SqlClient.SqlParameter("@EML_SUBJECT", txtsubject.Text.Trim())
        pParms(8) = New SqlClient.SqlParameter("@EML_DISPLAY", txtdisplay.Text.Trim())

        pParms(9) = New SqlClient.SqlParameter("@EML_USERNAME", txtusername.Text.Trim())

        If txtpassword.Text.Trim() <> "" Then
            pParms(10) = New SqlClient.SqlParameter("@EML_PASSWORD", txtpassword.Text.Trim())
        Else
            pParms(10) = New SqlClient.SqlParameter("@EML_PASSWORD", HiddenPassword.Value)
        End If


        pParms(11) = New SqlClient.SqlParameter("@EML_HOST", txthost.Text.Trim())
        pParms(12) = New SqlClient.SqlParameter("@EML_PORT", txtport.Text.Trim())
        If save = 1 Then
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_UPDATE", pParms)
            lblmessage.Text = "Newsletter Template updated Successfully for ID "
        Else
            lblmessage.Text = "Newsletter Template updated Successfully for ID "
        End If

        Dim jsFunc As String = "SetCloseEditTemplatetoParent('" + lblmessage.Text.ToString + Val + "')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
    End Sub

    Protected Sub btnedittemplatecancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnedittemplatecancel.Click
        'Panel1.Visible = True
        'Panel2.Visible = False
        lblmessage.Text = ""
        Dim jsFunc As String = "SetCloseFrametoParent('" + lblmessage.Text.ToString + "')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
    End Sub

    'Protected Sub GrdNewsletterView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdNewsletterView.RowCommand
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '    If e.CommandName = "selectnews" Then
    '        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString()
    '        Dim path = cvVirtualPath & e.CommandArgument.ToString()
    '        Response.Redirect(path)
    '    End If
    '    If e.CommandName = "RemoveDir" Then
    '        Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
    '        Dim str_query = "UPDATE COM_MANAGE_EMAIL SET EML_DELETED='True' where EML_ID='" & e.CommandArgument & "'"
    '        SqlHelper.ExecuteNonQuery(str_conn.ToString, CommandType.Text, str_query)
    '        Dim path As String = serverpath + e.CommandArgument

    '        If System.IO.Directory.Exists(path) Then
    '            Try
    '                System.IO.Directory.Delete(path, True)
    '            Catch ex As Exception

    '            End Try

    '        End If

    '        BindNewsletters()
    '        lblmessage.Text = "Template Deleted Successfully"

    '    End If
    '    If e.CommandName = "Editing" Then
    '        If CheckActive(e.CommandArgument) Then
    '            Panel1.Visible = False
    '            Panel2.Visible = True
    '            HiddenFieldTemplateid.Value = e.CommandArgument
    '            Dim str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & e.CommandArgument & "'"
    '            Dim ds As DataSet
    '            ds = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                txtFrom.Text = ds.Tables(0).Rows(0).Item("EML_FROM").ToString()
    '                txtTitle.Text = ds.Tables(0).Rows(0).Item("EML_TITLE").ToString()
    '                txtsubject.Text = ds.Tables(0).Rows(0).Item("EML_SUBJECT").ToString()
    '                txtdisplay.Text = ds.Tables(0).Rows(0).Item("EML_DISPLAY").ToString()
    '                txthost.Text = ds.Tables(0).Rows(0).Item("EML_HOST").ToString()
    '                txtport.Text = ds.Tables(0).Rows(0).Item("EML_PORT").ToString()
    '                txtusername.Text = ds.Tables(0).Rows(0).Item("EML_USERNAME").ToString()
    '                txtpassword.Text = ds.Tables(0).Rows(0).Item("EML_PASSWORD").ToString()
    '                HiddenPassword.Value = ds.Tables(0).Rows(0).Item("EML_PASSWORD").ToString()

    '            End If
    '            lblmessage.Text = "You can add attachments and change the existing news letter"
    '        Else
    '            lblmessage.Text = "You cannot edit this template.Message has been sent."
    '        End If
    '    End If
    '    If e.CommandName = "search" Then
    '        BindNewsletters()
    '    End If


    'End Sub
    Public Function CheckActive(ByVal id As String) As Boolean
        Dim returnvalue As Boolean = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select count(*) from COM_LOG_EMAIL_TABLE where LOG_EML_ID='" & id & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If val = 0 Then
            returnvalue = True
        End If

        If Session("sBusper") = "True" Then
            returnvalue = True
        End If

        Return returnvalue
    End Function


    'Protected Sub GrdAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdNewsletterView.RowCommand
    '    If e.CommandName = "select" Then
    '        Dim path = e.CommandArgument.ToString()
    '        'HttpContext.Current.Response.ContentType = "application/octect-stream"
    '        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
    '        'HttpContext.Current.Response.Clear()
    '        'HttpContext.Current.Response.WriteFile(path)
    '        'HttpContext.Current.Response.End()

    '        Dim bytes() As Byte = File.ReadAllBytes(path)
    '        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '        Response.Clear()
    '        Response.ClearHeaders()
    '        Response.ContentType = "application/octect-stream"
    '        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
    '        Response.BinaryWrite(bytes)
    '        Response.Flush()
    '        Response.End()
    '    End If
    '    If e.CommandName = "Remove" Then
    '        Dim f As New FileInfo(e.CommandArgument)
    '        If f.Exists Then
    '            f.Delete()
    '        End If
    '        BindNewsletters()
    '        lblmessage.Text = "Attachment Deleted Successfully"

    '    End If
    'End Sub

    'Protected Sub GrdNewsletterView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdNewsletterView.PageIndexChanging
    '    GrdNewsletterView.PageIndex = e.NewPageIndex
    '    BindNewsletters()
    'End Sub
End Class
