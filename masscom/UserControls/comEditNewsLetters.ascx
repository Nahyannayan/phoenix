<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comEditNewsLetters.ascx.vb" Inherits="masscom_UserControls_comEditNewsLetters" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <script type="text/javascript">

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false; 
        
    }

</script>
    <div class="matters">

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
<asp:Panel ID="Panel1" runat="server">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
        <tr>
            <td class="subheader_img">
                Email News Letter Templates</td>
        </tr>
        
        <tr>
            <td>
                <asp:GridView ID="GrdNewsletterView" runat="server" EmptyDataText="No News letters added yet." AutoGenerateColumns="False"  Width="100%" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Template ID">
                        <HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                Template ID
                                <br />
                                <asp:TextBox ID="Txt1" Width="50px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                
                                </td>
                              </tr>
                          </table>
                        </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                <asp:HiddenField ID="HiddenFieldFileName" runat="server" Value='<%# Eval("EML_NEWS_LETTER_FILE_NAME") %>' />
                              <center><%# Eval("EML_ID") %></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                          <HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                               Date
                                <br />
                                <asp:TextBox ID="Txt2" width="100px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2" TargetControlID="Txt2"></ajaxToolkit:CalendarExtender>

                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>
                          
                                            <ItemTemplate>
                                              <center> <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NewsLetter">
                        <HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                  NewsLetter
                                <br />
                                <asp:TextBox ID="Txt3" width="100px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>
                            <ItemTemplate>
                             <asp:LinkButton ID="lnknewsletter" runat="server" CausesValidation="false" Text=' <%# Eval("EML_NEWS_LETTER_FILE_NAME") %>' OnClientClick=' <%# Eval("openview") %>' ></asp:LinkButton>
                            </ItemTemplate>
                          
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attachments">
                        <HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">

                                 <br />
                                  Attachments
                                 <br />
                                 <br />
                                 </td>
                              </tr>
                          </table>
</HeaderTemplate>
                            <ItemTemplate>
                                <asp:GridView ID="GrdAttachment" runat="server" AutoGenerateColumns="false" OnRowCommand="GrdAttachment_RowCommand"
                                    ShowHeader="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" CommandName="select"
                                                    Text=' <%# Eval("Name") %>' OnClick="lnkAtt_Click"></asp:LinkButton>
                                                <asp:HiddenField ID="lnkVal" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Size (Bytes)">
                                            <ItemTemplate>
                                                <div align="right">
                                                    -(Size
                                                    <%#Eval("length")%>
                                                    Bytes)</div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                             <center>   <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Remove">Delete</asp:LinkButton></center>
                                                <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this attachment?"
                                                    TargetControlID="lnkdelete">
                                                </ajaxToolkit:ConfirmButtonExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                        <HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">

                                 <br />
                                 Delete
                                 <br />
                                 <br />
                                 </td>
                              </tr>
                          </table>
</HeaderTemplate>
                            <ItemTemplate>
                              <center>  <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'  CommandName="RemoveDir">Delete</asp:LinkButton></center>
                                <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this template?"
                                    TargetControlID="lnkdelete">
                                </ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                        <HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">

                                 <br />
                                  Edit
                                 <br />
                                 <br />
                                 </td>
                              </tr>
                          </table>
</HeaderTemplate>
                            <ItemTemplate>
                           <center><asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                    CommandName="Editing">Edit</asp:LinkButton></center>
                                <ajaxToolkit:ConfirmButtonExtender ID="C2" runat="server" ConfirmText="Are you sure you want to edit this template?"
                                    TargetControlID="lnkEdit">
                                </ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                     <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                     <EmptyDataRowStyle Wrap="False" />
                     <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server" Visible="False">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900">
        <tr>
            <td class="subheader_img">
                Update News Letter Template</td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        Title</td>
                                    <td>
                                        &nbsp;:</td>
                                    <td width="100%">
                                        <asp:TextBox ID="txtTitle"  Width="50%" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                             From&nbsp;Email&nbsp;Id
                                    </td>
                                    <td>
                                        &nbsp;:</td>
                                    <td>
                                        <asp:TextBox ID="txtFrom"  Width="50%" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                            Subject 
                                    </td>
                                    <td>
                                        &nbsp;:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtsubject"  Width="50%" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                            Display 
                                    </td>
                                    <td>
                                        &nbsp;:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtdisplay"  Width="50%" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                    <td>
                        Host</td>
                    <td>
                        &nbsp;:</td>
                    <td>
                        <asp:TextBox ID="txthost"  Width="50%" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Port</td>
                    <td>
                        &nbsp;:</td>
                    <td>
                        <asp:TextBox ID="txtport"  Width="50%" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Username</td>
                    <td>
                        &nbsp;:</td>
                    <td>
                        <asp:TextBox ID="txtusername"  Width="50%" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Password</td>
                    <td>
                        &nbsp;:</td>
                    <td>
                        <asp:TextBox ID="txtpassword"  Width="50%" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:Label ID="Label2" runat="server" ForeColor="Red" 
                            Text="* Please provide if any change in Password"></asp:Label></td>
                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Attachments</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:FileUpload ID="FileUploadEditAttachments" runat="server" />
                            <%--  <asp:Button ID="btnEditsaveNewsLetters" runat="server" CausesValidation="False" Text="Add" CssClass="button" />
--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            News Letter
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:FileUpload ID="FileUploadEditNewsLetters" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>

                    <tr>
                        <td>
                            <asp:RadioButtonList ID="RadioButtonListEditNewsLetters" runat="server">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnsaveEditTemplates" runat="server" CssClass="button" 
                                Text="Save" Width="100px" />
                            <asp:Button ID="btnedittemplatecancel" runat="server" CausesValidation="False" 
                                CssClass="button" Text="Cancel" Width="100px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID="HiddenFieldTemplateid" runat="server" />
        <asp:HiddenField ID="HiddenPassword" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUploadEditNewsLetters"
    Display="None" ErrorMessage="Please upload (*.zip) Files" SetFocusOnError="True"
    Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.zip)$"></asp:RegularExpressionValidator><asp:ValidationSummary
        ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
          </div>   