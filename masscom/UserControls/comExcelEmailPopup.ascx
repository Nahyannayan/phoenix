﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelEmailPopup.ascx.vb" Inherits="masscom_UserControls_comExcelEmailPopup" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../cssfiles/Popup.css" rel="stylesheet" />
       <script type="text/javascript" >

           window.setTimeout('setpathExt()', 100);

           function setpathExt() {
               var path = window.location.href
               var Rpath = path.substring(path.indexOf('?'), path.length)
               var objFrame
               //Staff
               objFrame = document.getElementById("FEx3");
               objFrame.src = "../masscom/comExcelSendSelection.aspx" + Rpath

               var strOpen;
               var tab=0;
               strOpen += "&templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
               strOpen += "&EType=PT";
               strOpen += "&Tab=" + tab + "&DataId=0";

               //Student
               objFrame = document.getElementById("FEx1");
               objFrame.src = "../masscom/TabPages/comExcelEmailPopupView.aspx" + Rpath + strOpen

               //Staff
               objFrame = document.getElementById("FEx2");
               objFrame.src = "../masscom/TabPages/comExcelEmailDataAdd.aspx" + Rpath + strOpen

           }
           function refreshPage() {
               //setpathExt()
               var path = window.location.href
               alert(path)
           }
       </script> 
   
 <script language="javascript" type="text/javascript">
     function SetSelValuetoParent(id) {
         //alert(id);
         parent.setSelExcelFileValue(id);
         return false;
     }
            </script>

<div >

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
  <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0"  ><%--OnActiveTabChanged="Tab1_ActiveTabChanged" AutoPostBack="true"--%>

         <ajaxToolkit:TabPanel ID="HT3" runat="server">
                <ContentTemplate>
                
                <iframe id="FEx3" height="900" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%" ></iframe>
               
                </ContentTemplate>
                <HeaderTemplate>
                  Upload Template
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>

  
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                   
                <iframe id="FEx1" height="900" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
               
                   
                </ContentTemplate>
                <HeaderTemplate>
                  Template Collections
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                
                <iframe id="FEx2" height="1100" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
               
                </ContentTemplate>
                <HeaderTemplate>
                  Add/Edit/Delete Template 
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
  </ajaxToolkit:TabContainer>
        <asp:HiddenField ID="HiddenTempid" runat="server" />

</ContentTemplate>
</asp:UpdatePanel>



  </div>
