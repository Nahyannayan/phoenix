<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comViewEmailReports.ascx.vb" Inherits="masscom_UserControls_comViewEmailReports" %>
<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


<!-- Bootstrap header files ends here -->

<script type="text/javascript">
    function openTemplate() {


        var sFeatures;
        var strOpen;

        strOpen = 'comExcelEmailData.aspx'

        sFeatures = "dialogWidth: 1024px; ";
        sFeatures += "dialogHeight: 768px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";

        strOpen += "?templateid=0";
        strOpen += "&EType=NL";
        strOpen += "&Tab=1&DataId=" + document.getElementById("<%=HiddenLogid.ClientID %>").value;

        window.showModalDialog(strOpen, "", sFeatures);



    }
</script>
<div>
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="LinkSearch" OnClientClick="javascript:return false;" runat="server">Search </asp:LinkButton>
            <asp:Panel ID="Panel1" runat="server">
                <table>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Unique ID.</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%"><span class="field-label">Name</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Email ID</span></td>

                        <td>
                            <asp:TextBox ID="txtemailid" runat="server"></asp:TextBox></td>
                        <td><span class="field-label">Status</span></td>

                        <td>
                            <asp:DropDownList ID="dderror" runat="server">
                                <asp:ListItem Text="All" Value="ALL" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Error" Value="ERROR"></asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Button ID="btnsearch" CssClass="button" runat="server" Text="Search" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Email Status Reports -
                                <asp:Label ID="lblmessage" runat="server" BorderColor="Black" BorderStyle="Solid"
                                    BorderWidth="1px"></asp:Label>
                        <asp:Label ID="lbldatalog" runat="server" BorderColor="Black" BorderStyle="Solid"
                            BorderWidth="1px"></asp:Label>
                        <asp:LinkButton ID="LinkTemplate" runat="server" OnClientClick="javascript:openTemplate();return false;">Excel Template</asp:LinkButton></td>
                </tr>
                <tr>
                    <td valign="top">

                        <asp:GridView ID="GridReports" AutoGenerateColumns="false" runat="server" AllowPaging="True" Width="100%" PageSize="20" CssClass="table table-bordered table-row">
                            <Columns>

                                <asp:TemplateField HeaderText=" Unique ID">
                                    <HeaderTemplate>
                                        Unique ID
                                                
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("LOG_UNIQUE_ID")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderTemplate>
                                        Name
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("NAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email ID">
                                    <HeaderTemplate>
                                        Email ID
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("LOG_EMAIL_ID")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <HeaderTemplate>
                                        Status
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("LOG_STATUS")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                                    <HeaderTemplate>
                                        Date
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("LOG_ENTRY_DATE", "{0:dd/MMM/yyyy HH:mm:ss}")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                        <asp:Button ID="btnExport" runat="server" CssClass="button" OnClick="btnExport_Click"
                            Text="Export-Non Sent/Errors" />
                        <asp:Button ID="btnResent" runat="server" CssClass="button" OnClick="btnResent_Click"
                            OnClientClick="javascript:alert('Resent has been succesfully done. Please see the Reports for details.')"
                            Text="Resent to Non Sent" />
                        <br />
                        <asp:Label ID="lblusermessage" runat="server" CssClass="error"></asp:Label></td>
                </tr>
            </table>
            <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="This report generate list of data which are not used for sending and also errors occured during sending." TargetControlID="btnExport" runat="server"></ajaxToolkit:ConfirmButtonExtender>
            <ajaxToolkit:ConfirmButtonExtender ID="C2" ConfirmText="Proceed with Resent?" TargetControlID="btnResent" runat="server">
            </ajaxToolkit:ConfirmButtonExtender>
            <asp:HiddenField ID="HiddenSendingID" runat="server" />
            <asp:HiddenField ID="HiddenScheduleID" runat="server" />
            <asp:HiddenField ID="HiddenLogid" runat="server" />
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />
            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" TargetControlID="Panel1" CollapsedSize="0" ExpandedSize="150"
                Collapsed="true" ExpandControlID="LinkSearch" CollapseControlID="LinkSearch"
                AutoCollapse="False" AutoExpand="False" ScrollContents="false" TextLabelID="LinkSearch" CollapsedText="Search" ExpandedText="Hide Search" runat="server">
            </ajaxToolkit:CollapsiblePanelExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
