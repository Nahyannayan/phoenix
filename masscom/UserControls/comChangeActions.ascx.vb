﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comChangeActions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenType.Value = Request.QueryString("stype")
            HiddenRecordid.Value = Request.QueryString("recordid")
            BindHrsMins()
            If CheckRecordExists() Then
                CheckMessageSending()
            Else
                lblsmessage.Text = "Schedule does not exists."
                btnok0.Visible = True
            End If

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Function CheckRecordExists() As Boolean
        Dim returnval = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        If HiddenType.Value = "SMS" Then
            Dim sql_query = "SELECT * from COM_MANAGE_SMS_SCHEDULE WHERE RECORD_ID='" & HiddenRecordid.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            If ds.Tables(0).Rows.Count > 0 Then

                Dim pass_date As DateTime = DateTime.Parse(ds.Tables(0).Rows(0).Item("SCHEDULE_DATE_TIME").ToString())
                Dim c_date As String = pass_date.ToString("dd/MMM/yyyy")
                txtdate.Text = c_date
                'Dim dat As String = pass_date.Date
                'Dim hrr As String = pass_date.Hour
                'Dim min As String = pass_date.Minute
                If pass_date.Hour < 10 Then
                    Dim hrr As String = "0" & pass_date.Hour
                    ddhour.SelectedValue = hrr
                Else
                    ddhour.SelectedValue = pass_date.Hour
                End If
                If pass_date.Minute < 10 Then
                    Dim minn As String = "0" & pass_date.Minute
                    ddmins.SelectedValue = minn
                Else
                    ddmins.SelectedValue = pass_date.Minute
                End If
                returnval = True
            End If

        End If
        If HiddenType.Value = "EMAIL" Then
            Dim sql_query = "SELECT * from COM_MANAGE_EMAIL_SCHEDULE WHERE RECORD_ID='" & HiddenRecordid.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            If ds.Tables(0).Rows.Count > 0 Then

                Dim pass_date As DateTime = DateTime.Parse(ds.Tables(0).Rows(0).Item("SCHEDULE_DATE_TIME").ToString())
                Dim c_date As String = pass_date.ToString("dd/MMM/yyyy")
                txtdate.Text = c_date
           
                If pass_date.Hour < 10 Then
                    Dim hrr As String = "0" & pass_date.Hour
                    ddhour.SelectedValue = hrr
                Else
                    ddhour.SelectedValue = pass_date.Hour
                End If
                If pass_date.Minute < 10 Then
                    Dim minn As String = "0" & pass_date.Minute
                    ddmins.SelectedValue = minn
                Else
                    ddmins.SelectedValue = pass_date.Minute
                End If
                returnval = True
            End If
        End If

        Return returnval
    End Function
    Public Function CheckMessageSending() As Boolean
        lblsmessage.Text = ""
        Dim returnsent As Boolean = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        If HiddenType.Value = "SMS" Then
            Dim sql_query = "SELECT * from COM_MANAGE_SMS_SCHEDULE WHERE THREAD_STARTED='False' AND SCHEDULE_DATE_TIME > GETDATE() AND RECORD_ID='" & HiddenRecordid.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnsent = False
            End If
        End If
        If HiddenType.Value = "EMAIL" Then
            Dim sql_query = "SELECT * from COM_MANAGE_EMAIL_SCHEDULE WHERE THREAD_STARTED='False' AND SCHEDULE_DATE_TIME > GETDATE() AND RECORD_ID='" & HiddenRecordid.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnsent = False
            End If
        End If

        If returnsent = True Then
            Panel1.Visible = False
            lblsmessage.Text = "Message has been sent. Transaction cannot be done."
            btnok0.Visible = True
        Else
            Panel1.Visible = True
        End If

        Return returnsent
    End Function

    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
        If CheckMessageSending() = False Then


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

            Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
            Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
            Dim hours = ts.TotalHours
            If hours < 0 Then
                ''Update Record
                If HiddenType.Value = "SMS" Then
                    Dim sql_query = "UPDATE COM_MANAGE_SMS_SCHEDULE SET SCHEDULE_DATE_TIME= CONVERT(DATETIME,'" & dt & "') WHERE RECORD_ID='" & HiddenRecordid.Value & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
                    lblsmessage.Text = "SMS schedule time changed successfully."
                End If

                If HiddenType.Value = "EMAIL" Then
                    Dim sql_query = "UPDATE COM_MANAGE_EMAIL_SCHEDULE SET SCHEDULE_DATE_TIME= CONVERT(DATETIME,'" & dt & "') WHERE RECORD_ID='" & HiddenRecordid.Value & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
                    lblsmessage.Text = "Email schedule time changed successfully."
                End If
                Panel1.Visible = False
                btnok0.Visible = True
            Else
                lblsmessage.Text = "Date time is past"
                Panel1.Visible = True
            End If


        End If

        'Dim jsFunc As String = "SetCloseValuetoParent()"
        'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)

    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If CheckMessageSending() = False Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            If HiddenType.Value = "SMS" Then
                Dim sql_query = "DELETE COM_MANAGE_SMS_SCHEDULE WHERE RECORD_ID='" & HiddenRecordid.Value & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
                lblsmessage.Text = "SMS schedule deleted successfully."
                Panel1.Visible = False
            End If

            If HiddenType.Value = "EMAIL" Then
                Dim sql_query = "DELETE COM_MANAGE_EMAIL_SCHEDULE WHERE RECORD_ID='" & HiddenRecordid.Value & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
                lblsmessage.Text = "Email schedule deleted successfully."
                Panel1.Visible = False
            End If
            btnok0.Visible = True
        End If

    End Sub

End Class
