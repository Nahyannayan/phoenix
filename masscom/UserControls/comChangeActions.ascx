﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comChangeActions.ascx.vb" Inherits="masscom_UserControls_comChangeActions" %>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


<!-- Bootstrap header files ends here -->


<script language="javascript" type="text/javascript">
    function SetCloseValuetoParent() {
        //alert('close1');
        parent.setCloseFrame();
        return false;
    }
</script>
<div align="center">

    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <center>
               <br />
               <asp:Label ID="lblsmessage" runat="server" CssClass="error"></asp:Label>
               <br />
               <asp:Button ID="btnok0" runat="server" CssClass="button" CausesValidation="false"  OnClientClick="javascript:SetCloseValuetoParent();" Text="Ok" 
                   Visible="False" />
                            </center>
            <asp:Panel ID="Panel1" runat="server">
                <table border="0" cellpadding="0" cellspacing="0"
                    width="100%">
                    <tr>
                        <td class="title-bg-lite">Set Schedule Time</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left"><span class="field-label">Date</span></td>

                                    <td>
                                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="s"></asp:TextBox>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Hour</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddhour" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Mins</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddmins" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnok" runat="server" CssClass="button" Text="Ok"
                                            ValidationGroup="s" />
                                        <asp:Button ID="btncancel" runat="server" OnClientClick="javascript:SetCloseValuetoParent();" CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btndelete" runat="server" CssClass="button" Text="Delete" />
                                        <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Are you sure ?" TargetControlID="btndelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                    </td>
                                </tr>

                            </table>
                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="Image1" TargetControlID="txtdate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="txtdate" Display="None" ErrorMessage="Please Enter Date"
                                SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server"
                                ShowMessageBox="True" ShowSummary="False" ValidationGroup="s" />
                            <asp:HiddenField ID="HiddenType" runat="server" />
                            <asp:HiddenField ID="HiddenRecordid" runat="server" />


                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>

