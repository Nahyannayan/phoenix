﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsStaffUpdate.ascx.vb" Inherits="masscom_UserControls_comAssignGroupsStaffUpdate" %>
<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->
<script type="text/javascript">
    function change_chk_state(chkThis) {
        var ids = chkThis.id

        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                document.forms[0].elements[i].checked = state;
            }
        }
        return false;
    }


    function uncheckall(chkThis) {
        var ids = chkThis.id
        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
        var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }
        var uncheckflag = 0

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                if (currentid.indexOf(value) == -1) {
                    if (document.forms[0].elements[i].checked == false) {
                        uncheckflag = 1
                    }
                }
            }
        }

        if (uncheckflag == 1) {
            // uncheck parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }
        else {
            // Check parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = true;
                }
            }
        }


        return false;
    }
    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }
</script>
<script language="javascript" type="text/javascript">
    function SetCloseNewGRPValuetoParent(msg) {
        //alert('close1');
        parent.SetCloseNewGRPValuetoParent(msg);
        return false;
    }
            </script>

     
<div>

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <div>

                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="4" class="title-bg-lite">Groups</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Group</span></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtgroups" runat="server"></asp:TextBox></td>
                            <td align="left" width="20%"><span class="field-label">Group Type</span></td>
                            <td align="left" width="30%">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                     AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="0">New Group</asp:ListItem>
                                    <asp:ListItem Value="1">Add On</asp:ListItem>
                                </asp:RadioButtonList></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                            <td colspan="3">
                                <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr id="filtr_hdr_id" runat="server">
                            <td colspan="4" class="title-bg-lite">Filter Block</td>
                        </tr>
                        <tr id="filtr_dtl_id" runat="server">
                            <td colspan="4">
                                <asp:DataList ID="DataStaffFilter" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" ShowHeader="False" Width="100%">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenSTF_FT_ID" runat="server" Value='<%# Eval("STF_FT_ID") %>' />
                                        <asp:HiddenField ID="HiddenSTF_FT_PARAMETER_FIELD" runat="server" Value='<%# Eval("STF_FT_PARAMETER_FIELD") %>' />
                                        <asp:HiddenField ID="HiddenSTF_FT_CONDITION" runat="server" Value='<%# Eval("STF_FT_CONDITION") %>' />
                                        <asp:HiddenField ID="HiddenSTF_FT_VALUE_FIELD" runat="server" Value='<%# Eval("STF_FT_VALUE_FIELD") %>' />
                                        <asp:HiddenField ID="HiddenSTF_FT_TEXT_FIELD" runat="server" Value='<%# Eval("STF_FT_TEXT_FIELD") %>' />
                                        <asp:HiddenField ID="HiddenSTF_FT_FIELDS" runat="server" Value='<%# Eval("STF_FT_FIELDS") %>' />
                                        <asp:HiddenField ID="HiddenSTF_FT_CASE" runat="server" Value='<%# Eval("STF_FT_CASE") %>' />
                                        <asp:HiddenField ID="HiddenSTS_TABLES" runat="server" Value='<%# Eval("STS_TABLES") %>' />
                                        <asp:HiddenField ID="HiddenSTF_SHOW_ALL_FLAG" runat="server" Value='<%# Eval("STF_SHOW_ALL_FLAG") %>' />
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="title-bg">
                                                    <%#Eval("STF_FT_DES")%>
                                      
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox-list-full">
                                                        <asp:Panel ID="PanelControl" runat="server">
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <center>
                  <asp:Panel id="Panel1" runat="server" Width="100%"><asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                        <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" Visible="true"  />
                    </asp:Panel></center>
                                <asp:Panel ID="Panel2" runat="server" Width="100%" Visible="False">
                                    <asp:GridView ID="GrdAddOn" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GrdAddOn_PageIndexChanging"
                                        OnPageIndexChanged="GrdAddOn_PageIndexChanged" PageSize="20" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Check">
                                                <HeaderTemplate>

                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                        ToolTip="Click here to select/deselect all rows" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ch1" Text="" runat="server" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unique No">
                                                <HeaderTemplate>
                                                    Unique No
                                <br />
                                                    <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <asp:HiddenField ID="Hiddenuniqueid" Value='<%#Eval("uniqueid")%>' runat="server" />
                                                    <%#Eval("uniqueid")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <HeaderTemplate>
                                                    Name
                                <br />
                                                    <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <%#Eval("Name")%>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <HeaderTemplate>
                                                    Email
                                <br />
                                                    <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />

                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <%#Eval("Email")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile">
                                                <HeaderTemplate>
                                                    Mobile
                                <br />
                                                    <asp:TextBox ID="Txt4" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch4" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <%#Eval("Mobile")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Company Name">
                                                <HeaderTemplate>
                                                    Company Name
                                <br />
                                                    <asp:TextBox ID="Txt5" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch5" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("comp_name")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle />
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EmptyDataRowStyle />
                                        <EditRowStyle />
                                    </asp:GridView>
                                    <center>
                        <asp:Button id="btnsearch" runat="server" CssClass="button" Text="Search"  />
                        <asp:Button id="btnadonSave" runat="server" CssClass="button" Text="Save"  />
                        <asp:Button id="btnadonCancel" runat="server" CssClass="button" Text="Cancel"  /></center>
                                    &nbsp;<br />
                                </asp:Panel>
                                &nbsp;
                    <center>
                        <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>&nbsp;</center>
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                    <asp:HiddenField
                        ID="Hiddenquery" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="HiddenGroupid" runat="server" EnableViewState="False" />

                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtgroups"
                        Display="None" ErrorMessage="Please enter Group Description" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </td>
        </tr>
    </table>

</div>

