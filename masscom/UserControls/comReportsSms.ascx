<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comReportsSms.ascx.vb" Inherits="masscom_UserControls_comReportsSms" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="/phoenixbeta/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/phoenixbeta/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/phoenixbeta/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/phoenixbeta/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/phoenixbeta/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/phoenixbeta/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->

<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="/cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">

    function openWindow(a, b) {
        window.open('../comViewSmsReports.aspx?SendingId=' + a + '&ScheduleId=' + b, '', 'Height=710px,Width=1020px,scrollbars=yes,resizable=no,directories=no'); return false;
    }


    function openWindowActions(rcid) {

        var sFeatures;
        sFeatures = "dialogWidth: 300px; ";
        sFeatures += "dialogHeight: 300px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "../comChangeActions.aspx?stype=SMS" + '&recordid=' + rcid

        var result;
        //result = window.showModalDialog(strOpen, "", sFeatures);
        //window.location.reload(true)
        return ShowWindowWithClose(strOpen, 'search', '50%', '50%')

    }
    //general close frame used in edit template
    function setCloseFrame() {
        //alert('x');
        CloseFrame();
    }

    function CloseFrame() {
        jQuery.fancybox.close();
    }

</script>
<div class="matters">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <br />
            <br />
            <asp:LinkButton ID="LinkSearch" OnClientClick="javascript:return false;" runat="server" Visible="false">Search By Date</asp:LinkButton>
            <asp:Panel ID="Panel1" runat="server">
                <table>
                    <tr>
                        <td align="left" width="10%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" onfocus="javascript:callblur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>

                        <td align="left" width="10%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" onfocus="javascript:callblur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td align="left" width="20%">
                            <asp:Button ID="btnShow" runat="server" CssClass="button" Text="Show" /></td>
                    </tr>

                    <%--<tr>
                        <td></td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>--%>
                </table>
            </asp:Panel>


            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <%-- <tr>
                                <td class="title-bg-lite">SMS Reports-(Scheduled)</td>
                            </tr>--%>
                <tr>
                    <td>

                        <asp:GridView ID="GridScheduled" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" ShowFooter="True" OnPageIndexChanging="GridScheduled_PageIndexChanging" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField HeaderText="ID">
                                    <HeaderTemplate>
                                        ID
                                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("CMS_ID") %>' />

                                        <center><%#Eval("RECORD_ID")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <HeaderTemplate>
                                        Type
                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("TYPE")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SMS Text">
                                    <HeaderTemplate>
                                        SMS&nbsp;Text
                                      
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <asp:Label ID="Elblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>

                                        <asp:Panel ID="E4Panel1" runat="server" Height="50px">
                                            <%#Eval("CMS_SMS_TEXT")%>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview" Collapsed="true"
                                            CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="Elblview"
                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel1"
                                            TextLabelID="Elblview">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Count">
                                    <HeaderTemplate>
                                        Count
                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("TOTAL_COUNT")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sent">
                                    <HeaderTemplate>
                                        Sent
                                                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("SUCCESSFULLY_SENT")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Failed">
                                    <HeaderTemplate>
                                        Failed
                                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("SENT_FAILED")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Start Time">
                                    <HeaderTemplate>
                                        Start
                                     
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("START_TIME", "{0:dd/MMM/yyyy}")%>
                                        <br />
                                        <%#Eval("START_TIME", "{0:HH:mm:ss}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Time">
                                    <HeaderTemplate>
                                        End
                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("END_TIME", "{0:dd/MMM/yyyy}")%>
                                        <br />
                                        <%#Eval("END_TIME", "{0:HH:mm:ss}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Sent">
                                    <HeaderTemplate>
                                        Sent&nbsp;By
                                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("EMP_NAME")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scheduled Date">
                                    <HeaderTemplate>
                                        Scheduled&nbsp;Date
                                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("SCHEDULE_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                                        <br />
                                        <%#Eval("SCHEDULE_DATE_TIME", "{0:HH:mm:ss}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Total Credits Left">
                                    <HeaderTemplate>
                                        TCL
                                     
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                        <asp:Label ID="lbltcl" runat="server" Text='<%#Eval("TOTAL_CREDITS_LEFT")%>'></asp:Label>
                        
                        </center>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Credits">
                                    <HeaderTemplate>
                                        Credits
                                     
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("CREDITS")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Total Credits Taken" HeaderStyle-Width="10%">
                                    <HeaderTemplate>
                                        TCT
                                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                            <asp:Label ID="lbltct" runat="server" Text='<%#Eval("TOTAL_CREDITS_TAKEN")%>'></asp:Label>
                           
                        </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                                    <asp:TextBox ID="TxtAmount" Text=".08" MaxLength="5" width="75%" runat="server"></asp:TextBox>
                                                    <asp:Button ID="BtnCalculate" CssClass="button" runat="server" OnClick="BtnCalculate2_Click" Text="Ok" />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="F1" TargetControlID="TxtAmount" FilterType="Custom,Numbers" ValidChars="." runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                                               </center>
                                    </FooterTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Amount">
                                    <HeaderTemplate>
                                        AM
                                     
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                        </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center><asp:Label ID="lblAmountTotal" runat="server" CssClass="field-label"></asp:Label></center>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Detail">
                                    <HeaderTemplate>
                                        Detail
                                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><asp:LinkButton ID="LinkView" OnClientClick='<%#Eval("VIEWDETAILS")%>' runat="server">View</asp:LinkButton></center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Actions" Visible="true">
                                    <HeaderTemplate>
                                        Actions
                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><asp:LinkButton ID="LinkActions" OnClientClick='<%#Eval("ACTIONS")%>' runat="server">Actions</asp:LinkButton></center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>



                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Button ID="btnrefresh" runat="server" CssClass="button" Text="Refresh" />
                        <br />
                        <div class="matters">TCL- Total Credits Left &nbsp;, TCT- Total Credits Taken &nbsp;, AM- Amount</div>
                    </td>
                </tr>
            </table>


            <%-- <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" TargetControlID="Panel1" CollapsedSize="0" ExpandedSize="70"
                Collapsed="true" ExpandControlID="LinkSearch" CollapseControlID="LinkSearch"
                AutoCollapse="False" AutoExpand="False" ScrollContents="false" TextLabelID="LinkSearch" CollapsedText="Search By Date" ExpandedText="Hide Search" runat="server">
            </ajaxToolkit:CollapsiblePanelExtender>--%>

            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                TargetControlID="txtFromDate">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy"
                PopupButtonID="Image2" TargetControlID="txtToDate">
            </ajaxToolkit:CalendarExtender>


            <ajaxToolkit:TabContainer ID="Tab1" Width="100%" runat="server" ActiveTabIndex="0" ScrollBars="Horizontal" Visible="false">
                <ajaxToolkit:TabPanel ID="T1" Width="100%" runat="server">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg-lite">SMS Reports-(Normal)</td>
                            </tr>
                            <tr>
                                <td>

                                    <asp:GridView ID="GrdReports" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" ShowFooter="true" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID">
                                                <HeaderTemplate>
                                                    ID                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("CMS_ID") %>' />

                                                    <center><%#Eval("SENDING_ID")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <HeaderTemplate>
                                                    Type
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("TYPE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SMS Text">
                                                <HeaderTemplate>
                                                    SMS&nbsp;Text
                                        
                                                </HeaderTemplate>
                                                <ItemTemplate>

                                                    <asp:Label ID="Elblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>

                                                    <asp:Panel ID="E4Panel1" runat="server" Height="50px">
                                                        <%#Eval("CMS_SMS_TEXT")%>
                                                    </asp:Panel>
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                        AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview" Collapsed="true"
                                                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="Elblview"
                                                        ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel1"
                                                        TextLabelID="Elblview">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Count">
                                                <HeaderTemplate>
                                                    Count
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("TOTAL_MESSAGE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sent">
                                                <HeaderTemplate>
                                                    Sent
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("SUCCESSFULLY_SEND")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Failed">
                                                <HeaderTemplate>
                                                    Failed
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("SENDING_FAILED")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start Time">
                                                <HeaderTemplate>
                                                    Start
                                        
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("START_TIME", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("START_TIME", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Time">
                                                <HeaderTemplate>
                                                    End
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("END_TIME", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("END_TIME", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User Sent">
                                                <HeaderTemplate>
                                                    Sent&nbsp;By
                                        
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("EMP_NAME")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <HeaderTemplate>
                                                    Date
                                     
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                                    <br />
                                                    <%#Eval("ENTRY_DATE", "{0:HH:mm:ss}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total Credits Left">
                                                <HeaderTemplate>
                                                    TCL
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                        <asp:Label ID="lbltcl" runat="server" Text='<%#Eval("TOTAL_CREDITS_LEFT")%>'></asp:Label>
                        
                        </center>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Credits">
                                                <HeaderTemplate>
                                                    Credits
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("CREDITS")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total Credits Taken" HeaderStyle-Width="10%">
                                                <HeaderTemplate>
                                                    TCT
                                     
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                            <asp:Label ID="lbltct" runat="server" Text='<%#Eval("TOTAL_CREDITS_TAKEN")%>'></asp:Label>
                           
                        </center>
                                                </ItemTemplate>
                                                <FooterTemplate>

                                                    <center>
                                                                <asp:TextBox ID="TxtAmount" Text=".08" Width="75%" MaxLength="5" runat="server"></asp:TextBox>
                                                                <asp:Button ID="BtnCalculate" CssClass="button" runat="server" OnClick="BtnCalculate_Click" Text="Ok" />
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="F1" TargetControlID="TxtAmount" FilterType="Custom,Numbers" ValidChars="." runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                                                          </center>
                                                </FooterTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Amount">
                                                <HeaderTemplate>
                                                    AM
                                       
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                        </center>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <center><asp:Label ID="lblAmountTotal" runat="server" CssClass="field-label"></asp:Label></center>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Detail">
                                                <HeaderTemplate>
                                                    Detail
                                      
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><asp:LinkButton ID="LinkView" OnClientClick='<%#Eval("VIEWDETAILS")%>' runat="server">View</asp:LinkButton></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle />
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EmptyDataRowStyle />
                                        <EditRowStyle />
                                    </asp:GridView>
                                    <asp:Button ID="btnrefresh2" runat="server" CssClass="button" OnClick="btnrefresh2_Click"
                                        Text="Refresh" /></td>
                            </tr>
                        </table>
                        <br />
                        <div>
                            <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                            TCL- Total Credits Left &nbsp;, TCT- Total Credits Taken &nbsp;, AM- Amount
                        </div>
                    </ContentTemplate>
                    <HeaderTemplate>
                        Normal
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="T2" Width="100%" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        Scheduled
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>


</div>
