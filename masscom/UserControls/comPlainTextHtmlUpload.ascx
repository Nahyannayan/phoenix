﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comPlainTextHtmlUpload.ascx.vb" Inherits="masscom_UserControls_comPlainTextHtmlUpload" %>
<!-- Bootstrap core CSS-->
<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->
<br />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite" colspan="2">Upload Zip File (Html File and its supporting files)&nbsp;</td>
    </tr>
    <tr>
        <td align="left" >
            <asp:FileUpload ID="FileUploadNewsletters" runat="server" /></td>
            <td align="left">
            <asp:Button ID="btnupload" runat="server" CssClass="button" Text="Upload" />
           </td>
    </tr>

            <tr>
        <td align="left" colspan="2">
            <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
         
              </td>
    </tr>
            <tr>
        <td align="center" colspan="2">
            <span style="font-weight: bold; color: #808000">Note: Please upload a zip file which contains the html file and its supporting 
            files (images, css files etc). Once uploaded you can see the preview of the html 
            file. Below you can find the html tags which will be usefull to create the plain 
            text email.
            </span>


        </td>
    </tr>
    <tr>
        <td align="left" colspan="2">



            <div id="template" runat="server">
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:TextBox ID="txthtmlcode" TextMode="MultiLine" EnableTheming="false" runat="server"></asp:TextBox>
        </td>
    </tr>
</table>



