<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelEmailDataView.ascx.vb"
    Inherits="masscom_UserControls_comExcelEmailDataView" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->
<script type="text/javascript">

 function openPopup(logid)
    {
  
       if (logid != 0)
       {
            var sFeatures;
            var strOpen='';
             
            if(document.getElementById("<%=HiddenEType.ClientID %>").value =='NL')
            {
             strOpen='../comSendingNewsLettersExcel.aspx'
            }
            else
            {
            strOpen='../comSendingPlainTextEmailsExcel.aspx'
            }
       
            
            sFeatures="dialogWidth: 1024px; ";
            sFeatures+="dialogHeight: 768px; ";
          
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
           
            strOpen+="?Path=" + logid;
           
            strOpen+="&templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;

            window.showModalDialog(strOpen,"", sFeatures);
            
        }
        else
        {
        alert('Message is not selected.')
        }
        
        
            
    }
    
function openPopup2(logid)
    {
   
       if (logid != 0)
       {
            var sFeatures;
            var strOpen='';
             
           
            strOpen='../comSendingEmailNormal.aspx'
        
        
            sFeatures="dialogWidth: 500px; ";
            sFeatures+="dialogHeight: 500px; ";
          
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
           
            strOpen+="?Path=" + logid;
           
            strOpen+="&templateid=" + document.getElementById("<%=HiddenTempid.ClientID %>").value;
            strOpen+="&method=0";
            window.showModalDialog(strOpen,"", sFeatures);
            
        }
        else
        {
        alert('Message is not selected.')
        }
        
        
            
    }
</script>



<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>


<asp:Label ID="lblsmessage2" runat="server" ForeColor="Red"></asp:Label>

<table border="0"  cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">
            Template Collections</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView" AutoGenerateColumns="False" runat="server" AllowPaging="True"
                Width="100%" CssClass="table table-bordered table-row">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Title
                                        <br />
                                        <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                         <asp:HiddenField ID="HiddenLogid" Value='<% #Eval("LOG_ID") %>' runat="server" />
                            <%#Eval("TITLE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Type
                                        <br />
                                        <asp:TextBox ID="txttype" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("TYPE")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField>
                        <HeaderTemplate>
                           
                                        Count
                                        <br />
                                        Total/Valid/Error
                                        <br />
                                        <br />
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                               
                                <asp:Label ID="lbltotal" Text='<% #Eval("TOTAL") %>' runat="server" ></asp:Label>
                              <b>/</b>
                                 <asp:Label ID="lblvalid" runat="server" ></asp:Label>
                                <b>/</b>
                                 <asp:Label ID="lblerror" runat="server" ></asp:Label>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                           
                                        <br />
                                        Send
                                        <br />
                                        <br />
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkSent" OnClientClick='<%#Eval("OPENW")%>' runat="server">Proceed</asp:LinkButton>
                                <asp:LinkButton ID="LinkSent2" OnClientClick='<%#Eval("OPENW2")%>' runat="server">Proceed</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        <br />
                                        Schedule
                                        <br />
                                        <br />
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkSchedule" CommandArgument='<% #Eval("LOG_ID") %>' CommandName="Schedule"
                                    runat="server">Schedule</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem"  />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle  />
                <HeaderStyle  />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>
<div >
    <asp:Panel ID="PanelSchedule" runat="server"  CssClass="panel-cover"     Style="display: none">
        <table border="0" cellpadding="5" cellspacing="0" width="250">
            <tr>
                <td class="title-bg-lite">
                    Set Schedule Time</td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <contenttemplate>
                                <table>
                                    <tr>
                                        <td>
                                            Date</td>
                                       
                                        <td>
                                            <asp:TextBox ID="txtdate" runat="server" ValidationGroup="ss"></asp:TextBox>
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Hour</td>
                                        
                                        <td align="left">
                                            <asp:DropDownList ID="ddhour" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mins</td>
                                        
                                        <td align="left">
                                            <asp:DropDownList ID="ddmins" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <asp:Button ID="btnok2" runat="server" CssClass="button"  Text="Ok"
                                                ValidationGroup="ss" Width="80px" OnClick="btnok2_Click" />
                                            <asp:Button ID="btncancel2" runat="server" CssClass="button" Text="Cancel" Width="80px" CausesValidation="False" OnClick="btncancel2_Click" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <asp:Label ID="lblsmessage" runat="server" ForeColor="Red"></asp:Label></td>
                                    </tr>
                                </table>
                                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                                    TargetControlID="txtdate">
                                </ajaxToolkit:CalendarExtender>
                               
                                &nbsp;
                            </contenttemplate>
                    </asp:UpdatePanel>
                    <asp:HiddenField ID="HiddenDatalogid" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<asp:LinkButton ID="lnkschedule" runat="server" Style="display: none"></asp:LinkButton>
<ajaxToolkit:ModalPopupExtender ID="MO2" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="btncancel2" DropShadow="True" PopupControlID="PanelSchedule"
    TargetControlID="lnkschedule" DynamicServicePath="" Enabled="True">
</ajaxToolkit:ModalPopupExtender>
<asp:HiddenField ID="HiddenTempid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="HiddenEType" runat="server" />
 <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtdate"
                                    Display="None" ErrorMessage="Please Enter Date" SetFocusOnError="True" ValidationGroup="ss"></asp:RequiredFieldValidator>
                                <asp:ValidationSummary ID="ValidationSummary22" runat="server" ShowMessageBox="True"
                                    ShowSummary="False" ValidationGroup="ss" />