﻿Imports System.IO
Imports ICSharpCode.SharpZipLib
Imports System.Web.Configuration

Partial Class masscom_UserControls_comPlainTextHtmlUpload
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupload.Click

        Try
            If FileUploadNewsletters.HasFile Then

                Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
                Dim val As String = "PlainTextHtml_" & System.Guid.NewGuid().ToString()

                Dim filen As String()
                Dim FileName As String = FileUploadNewsletters.PostedFile.FileName
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)

                Directory.CreateDirectory(serverpath + val)

                FileUploadNewsletters.SaveAs(serverpath + val + "/" + FileName)

                Dim c As New Zip.FastZip
                c.ExtractZip(serverpath + val + "/" + FileName, serverpath + val + "/", String.Empty)

                Dim d As New DirectoryInfo(serverpath + val + "/")

                If d.GetFiles("*.htm", SearchOption.TopDirectoryOnly).Length > 1 Then
                    lblmessage.Text = -"Multiple HTML files found.Only one HTML file allowed."
                End If
                If d.GetFiles("*.htm", SearchOption.TopDirectoryOnly).Length = 0 Then
                    lblmessage.Text = -"No HTML files found.Please add HTML file and its supporting files."
                End If

                If d.GetFiles("*.htm", SearchOption.TopDirectoryOnly).Length = 1 Then
                    '' Open File
                    Dim url = serverpath + val + "/" + d.GetFiles("*.htm", SearchOption.TopDirectoryOnly)(0).ToString()
                    template.InnerHtml = ScreenScrapeHtml(url, val)
                    txthtmlcode.Text = ScreenScrapeHtml(url, val)

                End If


            End If
        Catch ex As Exception
            lblmessage.Text = "Error : " & ex.Message
        End Try



    End Sub


    Public Shared Function ScreenScrapeHtml(ByVal url As String, ByVal val As String) As String
        On Error Resume Next

        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream(), System.Text.Encoding.Default)
        Dim emailHtml As String = sr.ReadToEnd()
        sr.Close()

        Dim ResourceUrl As String = ConfigurationManager.AppSettings("WebsiteURLResource").ToString()
        Dim webSiteUrl = ResourceUrl + val + "/"

        ''''Comment 1------------------------------------------------------------
        emailHtml = emailHtml.Replace("src=""", "src=""" & webSiteUrl)
        ''-----------------------------------------------------------------------
        Return emailHtml



    End Function 'ScreenScrapeHtml


End Class
