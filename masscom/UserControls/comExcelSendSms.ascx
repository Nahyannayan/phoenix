<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSendSms.ascx.vb"
    Inherits="masscom_UserControls_comExcelSendSms" %>
<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <script type="text/javascript" >
    function openPopup(strOpen)
    {
   
       
         var sFeatures;
            sFeatures="dialogWidth: 1024px; ";
            sFeatures+="dialogHeight: 768px; ";
          
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            
            strOpen+="?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
            strOpen+="&smsid=" + document.getElementById("<%=Hiddensmsid.ClientID %>").value;
            document.getElementById('div2').style.display='none';
            window.showModalDialog(strOpen,"", sFeatures);
  
    }
    
    function openPopup2(strOpen)
    {
   
       
         var sFeatures;
            sFeatures="dialogWidth: 500px; ";
            sFeatures+="dialogHeight:  500px; ";
          
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            
            strOpen+="?Path=" + document.getElementById("<%=Hiddenlogid.ClientID %>").value;
            strOpen+="&smsid=" + document.getElementById("<%=Hiddensmsid.ClientID %>").value;
            strOpen+="&method=0";
            document.getElementById('div2').style.display='none';
            window.showModalDialog(strOpen,"", sFeatures);
          

            
    }
    
    
    
     function openTemplate(tab)
    {
   
       
            var sFeatures;
            var strOpen ;
            
            strOpen='comExcelSMSData.aspx'
            
            sFeatures="dialogWidth: 1024px; ";
            sFeatures+="dialogHeight: 768px; ";
          
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            
            strOpen+="?templateid=" + document.getElementById("<%=Hiddensmsid.ClientID %>").value;
            strOpen+="&Tab="+ tab + "&DataId=0" ;
            
            window.showModalDialog(strOpen,"", sFeatures);
             

            
    }
        
          function vali_Filetype()
        {  
           
            var id_value = document.getElementById('<%= FileUpload1.ClientID %>').value;  
          
                if(id_value != '') 
                {   
                    var valid_extensions = /(.xls|.xlsx|.XLS|.XLSX)$/i;     
                    
                    if(valid_extensions.test(id_value))  
                    {    
                       
                        return true;
                    }  else 
                    {   
                        alert('Please upload a document in one of the following formats:  .xls,.xlsx')  
                        document.getElementById('<%= FileUpload1.ClientID %>').focus()
                        return false;
                    } 
                } 
                
        }
    </script>

<asp:Label ID="lblmessage" runat="server" CssClass="matters" ForeColor="Red"></asp:Label><br />
<uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
<br />

<asp:Panel ID="Panel3" runat="server">
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="650">
    <tr>
        <td class="subheader_img">
            Step 1-Select Message&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;       
                                <asp:LinkButton ID="LinkButton3" OnClientClick="openTemplate(1); return false;" runat="server">Previous Excel Template(s)</asp:LinkButton></td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="Grdsmssend" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                OnPageIndexChanging="Grdsmssend_PageIndexChanging" Width="650px">
                <Columns>
<asp:TemplateField HeaderText="Sl No">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                               Sl No
                                <br />
                                <asp:TextBox ID="Txt1" Width="50px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                
                                </td>
                              </tr>
                          </table>
</HeaderTemplate>

<ItemTemplate>
         <asp:HiddenField ID="Hiddenid" Value='<%# Eval("CMS_ID") %>' runat="server" />
    <center><%# Eval("CMS_ID") %></center>  
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Create Date">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                Create Date
                                <br />
                                <asp:TextBox ID="Txt2" width="100px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2" TargetControlID="Txt2"></ajaxToolkit:CalendarExtender>

                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
     <center>   <%#Eval("CMS_DATE", "{0:dd/MMM/yyyy}")%></center> 
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Sms Text">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                Sms Text
                                <br />
                                <asp:TextBox ID="Txt3" width="300px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
      
        <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                            <asp:Panel ID="T12Panel1" runat="server" Height="50px">
                                <%#Eval("cms_sms_text")%></asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                TextLabelID="T12lblview">
                            </ajaxToolkit:CollapsiblePanelExtender>
</ItemTemplate>
</asp:TemplateField>
                    <asp:TemplateField HeaderText="Send">
                    <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            <br />
                                                          Send
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                        <ItemTemplate>
                        <center>
                            <asp:LinkButton ID="Lnksend" runat="server" CausesValidation="false" CommandArgument='<%# Eval("CMS_ID") %>'
                                CommandName="send">Select</asp:LinkButton></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Panel>
<br />
<asp:Panel ID="Panel1" runat="server" Visible="False">
 <table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Step 2-Upload Excel File &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                      
                                <asp:LinkButton ID="LinkButton1" OnClientClick="openTemplate(2); return false;" runat="server">Get Previous Template(s)</asp:LinkButton>||<asp:LinkButton
                                    ID="LinkButton2" runat="server" CausesValidation="False">Cancel</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td align="center" >
                                <table>
                                    <tr>
                                        <td align="left" >
                                            Title</td>
                                        <td align="left">
                                            <asp:TextBox ID="txttitle" runat="server"></asp:TextBox></td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            Type</td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="RadioType"  RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Text="Student" Value="STUDENT" Selected="True"></asp:ListItem> 
                                            <asp:ListItem Text="Staff" Value="STAFF" ></asp:ListItem> 
                                            <asp:ListItem Text="Others" Value="OTHERS" ></asp:ListItem> 
                                            </asp:RadioButtonList></td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            Excel File</td>
                                        <td align="left">

    <asp:FileUpload ID="FileUpload1"  onBlur="vali_Filetype()" runat="server" /></td>
                                        <td align="left" >
    <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button" /></td>
                                    </tr>
                                    
                                </table>

     </td>
                    </tr>
               </table>
<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Important Notes Regarding Uploading Excel Sheet</td>
                        </tr>
                        <tr>
                            <td >

                                <table>
                                    <tr>
                                        <td >
                                            Field Names
                                        </td>
                                        <td>
                                            :</td>
                                        <td colspan="2">
                                            ID,MobileNo,Name
                                            </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Sheet Name
                                        </td>
                                        <td>
                                            :</td>
                                        <td >
                                            Sheet1</td>
                                       
                                    </tr>
                                </table>
                             
 </td>
                    </tr>
               </table>
    </asp:Panel>
    <div id="div2">
<asp:Panel ID="Panel2" runat="server" Visible="False">
     <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Step 3-Send SMS</td>
                        </tr>
                        <tr>
                            <td align="center" >
    <asp:LinkButton ID="lnksend" runat="server" CausesValidation="False" OnClientClick="javascript:openPopup('comSendingSMSExcel.aspx');  return false;"  Visible="False">Proceed</asp:LinkButton>
    <asp:LinkButton ID="lnksend2" runat="server" CausesValidation="False" OnClientClick="javascript:openPopup2('comSendingSMSNormal.aspx');  return false;"  Visible="False">Proceed</asp:LinkButton>
                                ||
                                <asp:LinkButton ID="lnkschedule" runat="server">Schedule</asp:LinkButton>
                                ||
                                <asp:LinkButton ID="lnkcancel"  runat="server" CausesValidation="False">Cancel</asp:LinkButton><br />
                            </td>
                    </tr>
               </table>
</asp:Panel>
<div class="matters">
        <asp:Panel ID="PanelSchedule" runat="server" CssClass="modalPopup"  Style="display: none" >
         <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="240" >
                        <tr>
                            <td class="subheader_img">
                                Set Schedule Time</td>
                        </tr>
                        <tr>
                            <td >
                                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                    <contenttemplate>

            <table>
                <tr>
                    <td >
                        Date</td>
                    <td >
                        :</td>
                    <td >
                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="s"></asp:TextBox>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                </tr>
                <tr>
                    <td >
                        Hour</td>
                    <td >
                        :</td>
                    <td align="left" >
                        <asp:DropDownList ID="ddhour" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        Mins</td>
                    <td >
                        :</td>
                    <td align="left"  >
                        <asp:DropDownList ID="ddmins" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
            <asp:Button ID="btnok" CssClass="button" runat="server" Text="Ok" Width="80px" ValidationGroup="s" />
            <asp:Button ID="btncancel" CssClass="button" runat="server" Text="Cancel" Width="80px" /></td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Label ID="lblsmessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
            </table>
                                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                                    TargetControlID="txtdate">
                                </ajaxToolkit:CalendarExtender><asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" SetFocusOnError="True" ErrorMessage="Please Enter Date" Display="None" ControlToValidate="txtdate" ValidationGroup="s"></asp:RequiredFieldValidator> <asp:ValidationSummary id="ValidationSummary2" runat="server" ValidationGroup="s" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>&nbsp;
</contenttemplate>
                                </asp:UpdatePanel>&nbsp;
            
            
 </td>
                    </tr>
               </table>
           </asp:Panel>
</div>
<ajaxToolkit:ModalPopupExtender ID ="MO1" TargetControlID="lnkschedule" PopupControlID="PanelSchedule"  CancelControlID="btncancel" BackgroundCssClass="modalBackground" DropShadow=true RepositionMode="RepositionOnWindowResizeAndScroll"   runat="server">

</ajaxToolkit:ModalPopupExtender>
</div>
<br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttitle"
    Display="None" ErrorMessage="Please Enter a Title" SetFocusOnError="True"></asp:RequiredFieldValidator>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUpload1"
    Display="None" ErrorMessage="Please upload excel file" SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" />
<%--<asp:RegularExpressionValidator Style="position: static" ID="RegularExpressionValidator3"
    runat="server" SetFocusOnError="true" ErrorMessage="Please upload a Excel document (*.xls)."
    Display="None" ControlToValidate="FileUpload1" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.xls|.xlsx|.XLS|.XLSX)$"></asp:RegularExpressionValidator>--%>
<br />

<asp:HiddenField ID="Hiddensmsid" runat="server" />
<asp:HiddenField ID="HiddenPathEncrypt" runat="server" /><asp:HiddenField ID="Hiddenlogid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />

<input id="Hiddenpath" runat="server" value="00" type="hidden" />

