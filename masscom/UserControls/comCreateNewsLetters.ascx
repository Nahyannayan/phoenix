<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comCreateNewsLetters.ascx.vb" Inherits="masscom_UserControls_comCreateNewsLetters" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->

<script language="javascript" type="text/javascript">
    function SetCloseFrame() {
        //alert('close1');
        parent.setCloseFrame();
        return false;
    }
</script>
   <script language="javascript" type="text/javascript">
       function SetCloseValuetoParent(msg) {
           //alert('close1');
           parent.setCloseValue(msg);
           return false;
       }
            </script>

<div>
    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Create News Letters</td>
        </tr>
        <tr>
            <td align="left">

                <table width="100%">
                    <tr>
                        <td align="left" width="100%" colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="field-label">Title</span></td>

                                    <td align="left" width="70%">
                                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">From Email Id</span>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Subject </span>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtsubject" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Display </span>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtdisplay" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr id="host_id" runat="server">
                                    <td>
                                        <span class="field-label">Host</span></td>

                                    <td>
                                        <asp:TextBox ID="txthost" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr id="port_id" runat="server">
                                    <td>
                                        <span class="field-label">Port</span></td>

                                    <td>
                                        <asp:TextBox ID="txtport" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr id="user_id" runat="server">
                                    <td>
                                        <span class="field-label">Username</span></td>

                                    <td>
                                        <asp:TextBox ID="txtusername" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr id="password_id" runat="server">
                                    <td>
                                        <span class="field-label">Password</span></td>

                                    <td>
                                        <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox><br />
                                        <asp:Label ID="Label1" runat="server" Text="* Please provide if any change in Password" CssClass="error"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="30%">
                            <span class="field-label">Attachments</span></td>
                        <td align="left" width="70%">
                            <asp:FileUpload ID="FileUploadAttachments" runat="server" />
                            <br />
                            After creating the template, you can add more attachments using edit options.
            <%--<asp:Button ID="btnattachmentadd" runat="server" CausesValidation="False" Text="Add" CssClass="button" />
            --%>        </td>
                    </tr>

                    <tr>
                        <td>
                            <span class="field-label">News Letter</span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUploadNewsletters" runat="server" />
                            <br />
                            Newsletter html and its supporting files must be compressed using Win Zip (*.zip). 
                        </td>
                    </tr>


                    <tr>
                        <td align="left" colspan="2">
                            <asp:RadioButtonList ID="RadioButtonListHtml" runat="server">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="button" ValidationGroup="v1" />
                             <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUploadNewsletters" ValidationGroup="v1"
        Display="None" ErrorMessage="Please Upload a Zip File Containing Html file and its associated support files(Images and Javascript)."
        SetFocusOnError="True"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
            ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUploadNewsletters"
            Display="None" ErrorMessage="Please upload (*.zip) Files" SetFocusOnError="True" ValidationGroup="v1"
            Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.zip)$"></asp:RegularExpressionValidator>
    <asp:HiddenField ID="HiddenPassword" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:ValidationSummary
        ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
</div>
