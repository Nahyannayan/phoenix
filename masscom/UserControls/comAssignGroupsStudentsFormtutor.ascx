<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsStudentsFormtutor.ascx.vb" Inherits="masscom_UserControls_comAssignGroupsStudents" %>
   <script type="text/javascript"> 
   
   function client_OnTreeNodeChecked()
        { 
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
            var treeNode = obj;
            checkedState = treeNode.checked;
            do {
                obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
            var parentTreeLevel = obj.rows[0].cells.length;
            var parentTreeNode = obj.rows[0].cells[0];
            var tables = obj.parentElement.getElementsByTagName("TABLE");
            var numTables = tables.length
            if (numTables >= 1)
            {
                for (i=0; i < numTables; i++)
                {
                    if (tables[i] == obj)
                    {
                        treeNodeFound = true;
                        i++;
                        if (i == numTables)
                            {
                            return;
                            }
                    }
                    if (treeNodeFound == true)
                    {
                    var childTreeLevel = tables[i].rows[0].cells.length;
                    if (childTreeLevel > parentTreeLevel)
                        {
                        var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                        var inputs = cell.getElementsByTagName("INPUT");
                        inputs[0].checked = checkedState;
                        }
                    else
                        {
                        return;
                        }
                    }
                }
            }
        }
        }
        
    function change_chk_state(chkThis)
             {
                var ids =chkThis.id
             
                var value= ids.slice(ids.indexOf("_|"),ids.indexOf("|_")+2)
                    var state=false
                    if (chkThis.checked)
                    {
                    state= true
                    }
                    else
                    {
                    state=false
                    }
                  
                     for(i=0; i<document.forms[0].elements.length; i++)
                           {
                           var currentid =document.forms[0].elements[i].id; 
                           if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf(value)!=-1)
                              {
                                document.forms[0].elements[i].checked=state;
                              }
                            }
                   return false;
              }


    function uncheckall(chkThis)
    {
                var ids =chkThis.id
                var value= ids.slice(ids.indexOf("_|"),ids.indexOf("|_")+2) +'0' //Parent
                var value1= ids.slice(ids.indexOf("_|"),ids.indexOf("|_")+2)  //Child
                 var state=false
                    if (chkThis.checked)
                    {
                    state= true
                    }
                    else
                    {
                    state=false
                    }
                     var uncheckflag=0
                     
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                           var currentid =document.forms[0].elements[i].id; 
                           if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf(value1)!=-1 )
                              {
                                   if ( currentid.indexOf(value)==-1 )
                                    {
                                         if (document.forms[0].elements[i].checked==false)
                                         {
                                         uncheckflag=1
                                         }
                                    }
                              }
                            }
                           
                            if (uncheckflag==1)
                            {
                            // uncheck parent
                                    for(i=0; i<document.forms[0].elements.length; i++)
                                   {
                                   var currentid =document.forms[0].elements[i].id; 
                                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf(value)!=-1)
                                      {
                                        document.forms[0].elements[i].checked=false;
                                      }
                                    }
                            }
                            else
                            {
                            // Check parent
                                    for(i=0; i<document.forms[0].elements.length; i++)
                                   {
                                   var currentid =document.forms[0].elements[i].id; 
                                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf(value)!=-1)
                                      {
                                        document.forms[0].elements[i].checked=true;
                                      }
                                    }
                            }
                            
        
        return false;
    }
    function change_chk_stateg(chkThis)
             {
            var chk_state= ! chkThis.checked ;
             for(i=0; i<document.forms[0].elements.length; i++)
                   {
                   var currentid =document.forms[0].elements[i].id; 
                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("ch1")!=-1)
                 {
                  
                        document.forms[0].elements[i].checked=chk_state;
                         document.forms[0].elements[i].click();
                     }
                  }
              }
    </script>
    <div class="matters">
   
 <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left"> 
                <div class="matters" >
                   
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" >
            <tr>
                <td colspan="6" class="subheader_img">
                    Groups</td>
            </tr>
            <tr>
                <td style="width: 100px; height: 47px;">
                    Group</td>
                <td style="width: 4px; height: 47px;">
                    :</td>
                <td style="width: 4px; height: 47px;">
                    <asp:TextBox ID="txtgroups" runat="server"></asp:TextBox></td>
                <%--<td style="width: 4px; height: 47px">
                    Group
                    Type</td>
                <td style="width: 4px; height: 47px">
                    :</td>
                <td style="width: 51px; height: 47px;">
                    <asp:RadioButtonList id="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                        Width="155px" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                        <asp:ListItem Selected="True" Value="0">New Group</asp:ListItem>
                        <asp:ListItem Value="1">Add On</asp:ListItem>
                    </asp:RadioButtonList></td>--%>
            </tr>
            <tr>
                <td style="width: 100px; height: 47px;">
                    Contact</td>
                <td style="width: 4px; height: 47px;">
                    :</td>
                <td style="height: 47px;" colspan="4">
                    <asp:CheckBoxList ID="CheckContact" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem Text="<span style='color:red'>Primary Contact  (or)</span>" Selected="True" Value="P" ></asp:ListItem>
                    <asp:ListItem Text="Father" Value="F" ></asp:ListItem>
                    <asp:ListItem Text="Mother" Value="M" ></asp:ListItem>
                    <asp:ListItem Text="Guardian" Value="G" ></asp:ListItem>
                    </asp:CheckBoxList>
                    
                </td>
            </tr>
            <tr>
                <td colspan="6" class="subheader_img">
                    Part A</td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Business Unit</td>
                <td style="width: 4px">
                    :</td>
                <td colspan="4">
                    <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Academic Year</td>
                <td style="width: 4px">
                    :</td>
                <td colspan="4">
                    <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACD_ID_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr id="trCurr" runat="server">
                <td style="width: 100px">
                    Curriculum</td>
                <td style="width: 4px">
                    :</td>
                <td colspan="4">
                    <asp:DropDownList ID="ddlCLM_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCLM_ID_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr id="trshf" runat="server">
                <td style="width: 100px">
                    Shift</td>
                <td style="width: 4px">
                    :</td>
                <td colspan="4">
                    <asp:DropDownList ID="ddlSHF_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSHF_ID_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr id="trstm" runat="server">
                <td style="width: 100px">
                                  
                                Stream</td>
                <td style="width: 4px">
                    :</td>
                <td colspan="4">
                    <asp:DropDownList ID="ddlSTM_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSTM_ID_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Grade &amp; Section</td>
                <td style="width: 4px">
                    :</td>
                <td colspan="4">
                    <asp:Panel ID="plGrade" runat="server" Height="120px" ScrollBars="Vertical">
                        <asp:TreeView ID="tvGrade" runat="server" ExpandDepth="1" onclick="client_OnTreeNodeChecked();"
                            ShowCheckBoxes="All">
                            <DataBindings>
                                <asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand" TextField="Text"
                                    ValueField="ValueField" />
                            </DataBindings>
                        </asp:TreeView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="6" >
                </td>
            </tr>
            <tr >
                <td colspan="6" class="subheader_img">
                    Part B</td>
            </tr>
            <tr class="matters" >
                <td colspan="6">
                   
                    <asp:DataList ID="DataListPartB" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" ShowHeader="False">
                    <ItemTemplate>
                    <asp:HiddenField ID="HiddenCGC_ID" Value='<%# Eval("CGC_ID") %>' runat="server" />
                    <asp:HiddenField ID="HiddenCondition" Value='<%# Eval("CGC_CONDITION") %>' runat="server" />
                    <asp:HiddenField ID="HiddenValueField" Value='<%# Eval("CGC_VALUE_FIELD") %>' runat="server" />
                    <asp:HiddenField ID="HiddenTextField" Value='<%# Eval("CGC_TEXT_FIELD") %>' runat="server" />
                    <asp:HiddenField ID="HiddenFilterField" Value='<%# Eval("CGA_FILTER_FIELD") %>' runat="server" />
                     <asp:HiddenField ID="HiddenTJoinField" Value='<%# Eval("CGA_TABLES") %>' runat="server" />
                     <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" >
                        <tr class="subheader_img">
                            <td align="left" colspan="12" style="height: 16px" valign="middle">
                             <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                <%# Eval("CGC_DES") %></span></font>
                            </td>
                        </tr>
                        <tr class="matters">
                            <td>
                            <asp:Panel ID="PanelControl" runat="server" ScrollBars="Auto" BorderStyle="Solid" BorderColor="#1b80b6"   Height="200px" Width="200px">
                            </asp:Panel>
                            </td>
                        </tr>
                    </table>
                   
                    </ItemTemplate>
                  
                    </asp:DataList>
                    
                    </td>
            </tr>
            <tr >
                <td colspan="6">
                <center>
                    &nbsp;<asp:Panel id="Panel1" runat="server" Width="100%">
                        <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" 
                            Width="88px" />
                    </asp:Panel></center>
                    <%--<asp:Panel id="Panel2" runat="server" Width="100%" Visible="False">
                    <asp:GridView id="GrdAddOn" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GrdAddOn_PageIndexChanging" OnPageIndexChanged="GrdAddOn_PageIndexChanged" PageSize="20">
                            <columns>
<asp:TemplateField HeaderText="Check"><HeaderTemplate>
       
          <asp:CheckBox ID="chkAll"  runat="server"  onclick="javascript:change_chk_stateg(this);"
                         ToolTip="Click here to select/deselect all rows" />
        
</HeaderTemplate>
<ItemTemplate>
         <asp:CheckBox ID="ch1" Visible='<%#Eval("s1")%>'  Text="" runat="server" />
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Unique No">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                               Unique No
                                <br />
                                <asp:TextBox ID="Txt1" width="40px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                               
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
        <asp:HiddenField ID="Hiddenuniqueid" Value='<%#Eval("uniqueid")%>' runat="server" />
        <%#Eval("uniqueid")%>
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Name">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                              Student Name
                                <br />
                                <asp:TextBox ID="Txt2" width="120px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                               
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
         <%#Eval("Name")%> 
        
</ItemTemplate>

</asp:TemplateField>
<asp:TemplateField HeaderText="Email">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                 Email
                                <br />
                                <asp:TextBox ID="Txt3" width="100px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                               
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
         <%#Eval("Email")%> 
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Mobile">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                              Mobile
                                <br />
                                <asp:TextBox ID="Txt4" width="40px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch4" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                               
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
         <%#Eval("Mobile")%> 
        
</ItemTemplate>
</asp:TemplateField>

<%--<asp:TemplateField HeaderText="Company Name">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                               Company Name
                                <br />
                                <asp:TextBox ID="Txt5" width="70px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch5" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                               
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>
<ItemTemplate>
         <%#Eval("comp_name")%> 
        
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Contact">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                  <br />      
                               Contact   
                               <br /> 
                                <br />                             
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>
<ItemTemplate>
   <center> <%#Eval("Contact")%> </center> 
        
</ItemTemplate>
</asp:TemplateField>

</columns>
 <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                     <EmptyDataRowStyle Wrap="False" />
                     <EditRowStyle Wrap="False" />
                        </asp:GridView>
                        <center>
                        <asp:Button id="btnsearch" runat="server" CssClass="button" Text="Search"  />
                        <asp:Button id="btnadonSave" runat="server" CssClass="button" Text="Save"  />
                        <asp:Button id="btnadonCancel" runat="server" CssClass="button" Text="Cancel"  /></center>&nbsp;<br /> 
                    </asp:Panel>--%>&nbsp;
                    <center>
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>&nbsp;</center>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="Hiddenacyid" runat="server" /><asp:HiddenField ID="Hiddenacdid" runat="server" /><asp:HiddenField ID="Hiddenbsuid" runat="server" /><asp:HiddenField
            id="Hiddenquery" runat="server"></asp:HiddenField>
       
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtgroups"
            Display="None" ErrorMessage="Please enter Group Description" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
                </td>
            </tr>
        </table>
  
    </div>   
        
      