<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comSmsApprove.ascx.vb"
    Inherits="masscom_UserControls_comSmsApprove" %>
<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <contenttemplate>
<div class="matters">
<uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
<br />
<asp:Label ID="lblmessage" runat="server" CssClass="matters" ForeColor="Red"></asp:Label>
<br />
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
        <tr>
            <td class="subheader_img">
                Approve SMS and Send Message</td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:GridView ID="GrdSmsApprove" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found for Sms Approval (or) Search query did not produce any results"
                                AllowPaging="True" OnPageIndexChanging="GrdSmsApprove_PageIndexChanging" PageSize="10"
                                Width="100%" OnRowCommand="GrdSmsApprove_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Id
                                                        <br />
                                                        <asp:TextBox ID="Txt1" Width="40px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenCsm_id" Value='<%#Eval("CSM_ID")%>' runat="server"></asp:HiddenField>
                                            <center>
                                                <%#Eval("CSM_ID")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Requested Date">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Date
                                                        <br />
                                                        <asp:TextBox ID="Txt2" Width="60px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2" TargetControlID="Txt2"></ajaxToolkit:CalendarExtender>

                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                        <center>    <%# Eval("CSM_ENTRY_DATE", "{0:dd/MMM/yyyy}") %></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Message">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Sms Text
                                                        <br />
                                                        <asp:TextBox ID="Txt3" Width="100px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T12Panel1" runat="server" Height="50px">
                                                <%#Eval("cms_sms_text")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                TextLabelID="T12lblview">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Group
                                                        <br />
                                                        <asp:TextBox ID="Txt4" Width="40px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageSearch4" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("CGR_DES")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Type
                                                        <br />
                                                        <asp:DropDownList ID="DropSearch1" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                            runat="server">
                                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                            <asp:ListItem Value="STUDENT" Text="STUDENT"></asp:ListItem>
                                                            <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%# Eval("CGR_TYPE") %>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Type">
                                        <HeaderTemplate>
                                            <table class="BlueTable" width="100%">
                                                <tr class="matterswhite">
                                                    <td align="center" colspan="2">
                                                        Group Type
                                                        <br />
                                                        <asp:DropDownList ID="DropSearch2" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                            runat="server">
                                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                            <asp:ListItem Value="GRP" Text="Group"></asp:ListItem>
                                                            <asp:ListItem Value="AON" Text="Add On"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("Gtype")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<span style='font-size: smaller'>Approved</span>">
                                      <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            <br />
                                                           Approved
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenApproved" Value='<%#Eval("CSM_APPROVED")%>' runat="server" />
                                            <center>
                                                <asp:ImageButton ID="ImageApprove" CommandName="approve" CommandArgument='<%#Eval("CSM_ID")%>'
                                                    ImageUrl="~/Images/cross.png" runat="server" /></center>
                                            <ajaxToolkit:ConfirmButtonExtender ID="cfm1" ConfirmText="Do you wish to approve? "
                                                TargetControlID="ImageApprove" runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<span style='font-size: smaller'>Send</span>">
                                     <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            <br />
                                                          Send
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                        <ItemTemplate>
                                        <center>    <asp:LinkButton ID="Linksend" CommandName="Send" Visible="false" OnClientClick='<%# GetNavigateUrl(Eval("CSM_ID").ToString()) %>'
                                                runat="server">Proceed</asp:LinkButton>
                                                <asp:LinkButton ID="Linksend2" CommandName="Send" Visible="false" OnClientClick='<%# GetNavigateUrl(Eval("CSM_ID").ToString()) %>'
                                                runat="server">Proceed</asp:LinkButton>
                                        </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<span style='font-size: smaller'>Schedule</span>">
                                     <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            <br />
                                                         Schedule
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                        <ItemTemplate>
                                         <center> <asp:LinkButton ID="LinkSchedule" CommandName="Schedule" Visible="false" CommandArgument='<%#Eval("CSM_ID")%>'
                                                runat="server">Schedule</asp:LinkButton></center>  
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="<span style='font-size: smaller'>Delete</span>">
                                     <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            <br />
                                                         Delete
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                        <ItemTemplate>
                                         <center> <asp:LinkButton ID="Linkdelete" CommandName="Deleting" Visible='<%#Eval("DELVISBLE")%>' CommandArgument='<%#Eval("CSM_ID")%>'
                                                runat="server">Delete</asp:LinkButton></center>  
                                                <ajaxToolkit:ConfirmButtonExtender ID="C45" ConfirmText="Do you wish to continue ?" TargetControlID="Linkdelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="HiddenCSM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="HiddenCGR_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="HiddenCMS_ID" runat="server"></asp:HiddenField>
                <div class="matters">
                    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                    <asp:Panel ID="PanelSchedule" runat="server" CssClass="modalPopup" Style="display: none">
                        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="250">
                            <tr>
                                <td class="subheader_img">
                                    Set Schedule Time</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        Date</td>
                                                    <td>
                                                        :</td>
                                                    <td>
                                                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="s"></asp:TextBox>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:Image></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Hour</td>
                                                    <td>
                                                        :</td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddhour" runat="server">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Mins</td>
                                                    <td>
                                                        :</td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddmins" runat="server">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        <asp:Button ID="btnok" OnClick="btnok_Click" runat="server" Text="Ok" CssClass="button"
                                                            Width="80px" ValidationGroup="s"></asp:Button>
                                                            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" Width="80px">
                                                        </asp:Button>
                                                            
                                                      </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        <asp:Label ID="lblsmessage" runat="server" ForeColor="Red"></asp:Label></td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" TargetControlID="txtdate" PopupButtonID="Image1"
                                                Format="dd/MMM/yyyy">
                                            </ajaxToolkit:CalendarExtender>
                                           
                                            &nbsp;
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <asp:LinkButton ID="lnkschedule" runat="server" Style="display: none">
                </asp:LinkButton>
                <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelSchedule"
                    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="lnkschedule">
                </ajaxToolkit:ModalPopupExtender>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="s"
                                                SetFocusOnError="True" ErrorMessage="Please Enter Date" Display="None" ControlToValidate="txtdate"></asp:RequiredFieldValidator>
                                         
                  <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="s"
                                                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            </td>
        </tr>
    </table>
</div>                                    
                                        </contenttemplate>
</asp:UpdatePanel>
