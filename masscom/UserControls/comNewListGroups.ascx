﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comNewListGroups.ascx.vb" Inherits="masscom_UserControls_comNewListGroups" %>
<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->

<div>
    <%--<table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr class="title">
            <td align="left">
                GROUP LIST
            </td>
        </tr>
    </table>
    <br />--%>

    
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" width="20%"><span class="field-label">Group List </span></td>
            <td align="left" width="50%">
                <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACD_ID_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkaddnew" runat="server">Add New</asp:LinkButton><br />
                            <asp:GridView ID="GrdGroups" AutoGenerateColumns="false" runat="server" AllowPaging="True"
                                OnPageIndexChanging="GrdGroups_PageIndexChanging" PageSize="20" Width="100%" CssClass="table table-bordered table-row"
                                EmptyDataText="No Groups added yet. or Search Query did not produce any results">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Group Id">
                                        <HeaderTemplate>
                                            Group Id
                                                        <br />
                                            <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>&nbsp;
                                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddengrpid" Value='<%#Eval("CGR_ID")%>' runat="server" />
                                            <center>
                                                <%#Eval("CGR_ID")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Name">
                                        <HeaderTemplate>
                                            Group Name
                                                        <br />
                                            <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>&nbsp;
                                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                            ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("CGR_DES")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <HeaderTemplate>
                                            Type
                                             <br />
                                            <asp:DropDownList ID="DropSearch1" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                runat="server">
                                                <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                <asp:ListItem Value="STUDENT" Text="STUDENT"></asp:ListItem>
                                                <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                            </asp:DropDownList>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("CGR_TYPE")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Type">
                                        <HeaderTemplate>
                                            Group Type
                                             <br />
                                            <asp:DropDownList ID="DropSearch2" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                runat="server">
                                                <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                <asp:ListItem Value="GRP" Text="Group"></asp:ListItem>
                                                <asp:ListItem Value="AON" Text="Add On"></asp:ListItem>
                                            </asp:DropDownList>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddentype" Value='<%#Eval("gtype")%>' runat="server" />
                                            <center>
                                                <%#Eval("gtype")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>

                                            <br />
                                            Business Unit
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddengrpbsu" Value='<%#Eval("cgr_grp_bsu_id")%>' runat="server" />
                                            <center><%#Eval("BSU_NAME")%></center>
                                        </ItemTemplate>
                                        <ItemStyle  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Academic Year">
                                        <HeaderTemplate>

                                            <br />
                                            Academic Year
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center> <%#Eval("ACY_DESCR")%></center>
                                        </ItemTemplate>
                                        <ItemStyle  />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Academic Year">
                                        <HeaderTemplate>

                                            <br />
                                            Contact
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center> <%#Eval("CGR_CONTACT_C")%></center>
                                        </ItemTemplate>
                                        <ItemStyle  />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Edit">
                                        <HeaderTemplate>

                                            <br />
                                            Edit
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkview" Text="Edit" CommandName="Edit" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" Visible="false">
                                        <HeaderTemplate>

                                            <br />
                                            Delete
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkDelete" Text="Delete" CommandName="Deleted" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                            <ajaxToolkit:ConfirmButtonExtender ID="C1" TargetControlID="lnkDelete" ConfirmText="Do you wish to continue?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="List" Visible="false">
                                        <HeaderTemplate>

                                            <br />
                                            List
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnklist" Text="List" CommandName="list" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Export" Visible="false">
                                        <HeaderTemplate>

                                            <br />
                                            Export
                                            <br />
                                            <br />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="lnkExportMobile" Text="Mobile" CommandName="ExportMobile" CommandArgument='<%# Eval("CGR_ID") %>'
                                                    runat="server"></asp:LinkButton>&nbsp;<asp:LinkButton ID="lnkExportEmail" Text="Email"
                                                        CommandName="ExportEmail" CommandArgument='<%# Eval("CGR_ID") %>' runat="server"></asp:LinkButton>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <EmptyDataRowStyle />
                                <EditRowStyle />
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
