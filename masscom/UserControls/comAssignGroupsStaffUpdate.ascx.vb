﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comAssignGroupsStaffUpdate
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            BindPartBControls()
            If Not IsPostBack Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                CheckMenuRights()
                Hiddenbsuid.Value = Session("sBsuid")
                Session("hashtable") = Nothing
                Dim Encr_decrData As New Encryption64
                Dim Groupid = Encr_decrData.Decrypt(Request.QueryString("Groupid").Replace(" ", "+"))
                HiddenGroupid.Value = Groupid

                Dim str_query = "Select CGR_TYPE from COM_GROUPS_M where CGR_ID='" & Groupid & "'"
                Dim type = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

                If type = "STAFF" Then
                    BindPartAControls()
                    BindActualData()
                End If


            End If

            filtr_hdr_id.Visible = False
            filtr_dtl_id.Visible = False

        Catch ex As Exception
            lblmessage.Text = "Error" & ex.Message
        End Try
        AssignRights()
    End Sub

    Public Sub BindActualData()
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "Select * from COM_GROUPS_M where CGR_ID='" & HiddenGroupid.Value & "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                ''Bind Main Data
                txtgroups.Text = ds.Tables(0).Rows(0).Item("CGR_DES").ToString()
                Dim type = ds.Tables(0).Rows(0).Item("CGR_GRP_TYPE").ToString()
                Dim AddOnQuery = ds.Tables(0).Rows(0).Item("CGR_CONDITION").ToString()
                Hiddenquery.Value = AddOnQuery
                ddbsu.SelectedValue = ds.Tables(0).Rows(0).Item("CGR_GRP_BSU_ID").ToString()
                If type = "GRP" Then
                    RadioButtonList1.SelectedValue = "0" ' Group
                    Panel1.Visible = True
                    Panel2.Visible = False
                Else
                    RadioButtonList1.SelectedValue = "1" ''Add on
                    Panel2.Visible = True
                    Panel1.Visible = False
                    btnsearch.Visible = False
                    btnadonCancel.Visible = True
                    btnadonSave.Visible = True
                    GrdAddOn.Visible = True
                    'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, AddOnQuery)
                    'GrdAddOn.DataSource = ds
                    'GrdAddOn.DataBind()
                    BindGrid()

                    str_query = "Select CGAO_UNIQUE_ID from COM_GROUPS_ADD_ON where CGAO_CGR_ID='" & HiddenGroupid.Value & "'"
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)



                    Dim hash As New Hashtable

                    Session("hashtable") = Nothing

                    Dim i = 0
                    For i = 0 To ds.Tables(0).Rows.Count - 1

                        Dim key = ds.Tables(0).Rows(i).Item("CGAO_UNIQUE_ID").ToString()
                        If hash.ContainsKey(key) Then
                        Else
                            hash.Add(key, key)
                        End If

                    Next


                    For Each row As GridViewRow In GrdAddOn.Rows
                        Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                        Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                        Dim key = Hid.Value

                        If hash.ContainsValue(Hid.Value) Then
                            ch.Checked = True
                        End If

                    Next

                    Session("hashtable") = hash

                End If


                txtgroups.Enabled = False
                RadioButtonList1.Enabled = False


                ''Bind Part B
                For Each item As DataListItem In DataStaffFilter.Items
                    Dim cgi_id = DirectCast(item.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
                    Dim dc As DataSet
                    Dim CBLDynamic As New CheckBoxList
                    str_query = "Select * from COM_GROUPS_B where CGB_CGR_ID='" & HiddenGroupid.Value & "' and CGB_CGC_ID_STF_FT_ID='" & cgi_id & "'"
                    dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    CBLDynamic = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)
                    Dim i = 0
                    For i = 0 To dc.Tables(0).Rows.Count - 1
                        Dim value = dc.Tables(0).Rows(i).Item("CGB_VALUE").ToString()
                        If value = "0" Then
                            For Each clist1 As ListItem In CBLDynamic.Items
                                clist1.Selected = True
                            Next

                        Else
                            For Each clist As ListItem In CBLDynamic.Items
                                If clist.Value = value Then
                                    clist.Selected = True
                                End If

                            Next
                        End If


                    Next

                Next

            End If
        Catch ex As Exception

            lblmessage.Text = "Error :" & ex.Message

        End Try
    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
        directory.Add("Save", btnsave)
        directory.Add("Cancel", btncancel)

        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))

        Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
        directory2.Add("Save", btnadonSave)
        directory2.Add("Cancel", btnadonCancel)
        directory2.Add("Add", btnsearch)

        Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub

    Public Sub BindPartAControls()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Hiddenbsuid.Value

        ''if user is super admin give access to all the Business Unit
        'If tblbUSuper = True Then
        '    Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
        '        'create a list item to bind records from reader to dropdownlist ddlBunit
        '        Dim di As ListItem
        '        ddbsu.Items.Clear()
        '        'check if it return rows or not
        '        If BUnitreaderSuper.HasRows = True Then
        '            While BUnitreaderSuper.Read
        '                di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
        '                'adding listitems into the dropdownlist
        '                ddbsu.Items.Add(di)

        '                For ItemTypeCounter = 0 To ddbsu.Items.Count - 1
        '                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
        '                    If ddbsu.Items(ItemTypeCounter).Value = buser_id Then
        '                        ddbsu.SelectedIndex = ItemTypeCounter
        '                    End If
        '                Next
        '            End While
        '        End If
        '    End Using

        'Else
        '    'if user is not super admin get access to the selected  Business Unit
        '    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
        '        Dim di As ListItem
        '        ddbsu.Items.Clear()
        '        If BUnitreader.HasRows = True Then
        '            While BUnitreader.Read
        '                di = New ListItem(BUnitreader(2), BUnitreader(0))
        '                ddbsu.Items.Add(di)
        '                For ItemTypeCounter = 0 To ddbsu.Items.Count - 1
        '                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
        '                    If ddbsu.Items(ItemTypeCounter).Value = buser_id Then
        '                        ddbsu.SelectedIndex = ItemTypeCounter
        '                    End If
        '                Next
        '            End While
        '        End If
        '    End Using
        'End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
     CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
     & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddbsu.DataSource = ds.Tables(0)
        ddbsu.DataTextField = "BSU_NAME"
        ddbsu.DataValueField = "BSU_ID"
        ddbsu.DataBind()
        ddbsu.SelectedIndex = -1
        If Not ddbsu.Items.FindByValue(Hiddenbsuid.Value) Is Nothing Then
            ddbsu.Items.FindByValue(Hiddenbsuid.Value).Selected = True
        End If

    End Sub

    Public Sub BindPartBControls()
        ''Try
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select * from STF_REPORT_FILTER_TABLE where STF_FT_SHOW_FLAG='1' and STF_FT_ID NOT IN (10) ORDER BY STF_ORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DataStaffFilter.DataSource = ds
        DataStaffFilter.DataBind()

        For Each row As DataListItem In DataStaffFilter.Items
            Dim STF_FT_ID = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            Dim dc As DataSet
            Dim CBLDynamic As New CheckBoxList
            CBLDynamic.ID = "|" & STF_FT_ID & "|"

            Dim query = DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField).Value
            Dim tempquery = BindLoginUser(STF_FT_ID, query)

            If tempquery <> "" Then
                query = tempquery
            End If
            dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

            CBLDynamic.DataSource = dc
            CBLDynamic.DataValueField = DirectCast(row.FindControl("HiddenSTF_FT_VALUE_FIELD"), HiddenField).Value.Trim()
            CBLDynamic.DataTextField = DirectCast(row.FindControl("HiddenSTF_FT_TEXT_FIELD"), HiddenField).Value.Trim()
            CBLDynamic.DataBind()

            Dim showflag As String = DirectCast(row.FindControl("HiddenSTF_SHOW_ALL_FLAG"), HiddenField).Value.Trim()
            If showflag = "" Then
                showflag = True
            End If

            If showflag = True Then
                For Each clist As ListItem In CBLDynamic.Items
                    clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
                Next

                Dim list As New ListItem
                'list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
                list.Text = "All"
                list.Value = "0"
                list.Attributes.Add("onclick", "javascript:change_chk_state(this);")
                CBLDynamic.Items.Insert(0, list)
            End If


            Dim PanelHolder As Panel = DirectCast(row.FindControl("PanelControl"), Panel)
            PanelHolder.Controls.Add(CBLDynamic)

        Next

    End Sub

    Public Function BindLoginUser(ByVal STF_FT_ID As String, ByVal O_query As String) As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select STF_LOGIN_USER_CHECK_QUERY from STF_REPORT_FILTER_TABLE where STF_FT_ID='" & STF_FT_ID & "' "

        Dim qry As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()

        str_query = ""
        If qry <> "" Then
            str_query = "SELECT * FROM ( " & O_query & ") A " & qry

            str_query = str_query.Replace("$UID", Session("EmployeeId"))
            str_query = str_query.Replace("$BSUID", Hiddenbsuid.Value)



        End If

        Return str_query
    End Function


    Public Sub ShowSearch()
        If RadioButtonList1.SelectedValue = 0 Then
            Panel1.Visible = True
            Panel2.Visible = False
        Else
            Panel2.Visible = True
            Panel1.Visible = False
            btnsearch.Visible = True
            btnadonCancel.Visible = True 'False
            btnadonSave.Visible = False
            GrdAddOn.Visible = False
        End If
        lblmessage.Text = ""
    End Sub
    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        ShowSearch()
    End Sub

    Protected Sub GrdAddOn_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrdAddOn.PageIndexChanged
        Dim hash As New Hashtable
        hash = Session("hashtable")
        For Each row As GridViewRow In GrdAddOn.Rows
            Dim i = 0
            Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)

            If hash.ContainsValue(Hid.Value) Then
                ch.Checked = True
            End If

        Next
        Session("hashtable") = hash
    End Sub

    Protected Sub GrdAddOn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdAddOn.PageIndexChanging
        Dim hash As New Hashtable
        If Session("hashtable") Is Nothing Then
        Else
            hash = Session("hashtable")
        End If
        ''Dim i = 0
        For Each row As GridViewRow In GrdAddOn.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
            Dim key = Hid.Value
            If ch.Checked Then
                If hash.ContainsKey(key) Then
                Else
                    hash.Add(key, Hid.Value)
                End If

            Else

                If hash.ContainsKey(key) Then
                    hash.Remove(key)
                Else
                End If
            End If
        Next

        Session("hashtable") = hash
        GrdAddOn.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GrdAddOn_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAddOn.RowCommand

        If e.CommandName = "search" Then

            Dim hash As New Hashtable

            If Session("hashtable") Is Nothing Then
            Else
                hash = Session("hashtable")
            End If
            For Each row As GridViewRow In GrdAddOn.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                Dim key = Hid.Value
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, Hid.Value)
                    End If

                Else

                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If
            Next


            BindGrid()
        End If

    End Sub

    Protected Sub btnadonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadonCancel.Click
        'GrdAddOn.Visible = False
        'btnadonCancel.Visible = True 'False
        'btnadonSave.Visible = False
        'btnsearch.Visible = True

        Dim jsFunc As String = ""
        jsFunc = "SetCloseNewGRPValuetoParent('Back')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Try
            lblmessage.Text = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim Query As String = FilterQuery()
            Query = Query + " and EMPLOYEE_M.EMP_bACTIVE=''True'' and EMPLOYEE_M.EMP_STATUS <> 4 "

            Query = Query.Replace("''", "'")

            Hiddenquery.Value = Query
            BindGrid()

        Catch ex As Exception
            lblmessage.Text = "Error in Query"
        End Try
    End Sub

    Public Function FilterQuery() As String
        Dim BSU As String = ddbsu.SelectedValue


        Dim CommonQuery As String = " ROW_NUMBER() over(order by EMP_ID) RowId ,EMPLOYEE_M.EMPNO as uniqueid, EMPLOYEE_M.EMP_ID, isnull(EMPLOYEE_M.EMP_FNAME,'''')+'' ''+ isnull(EMPLOYEE_M.EMP_MNAME,'''') + '' '' + isnull(EMPLOYEE_M.EMP_LNAME,'''') as Name, ''True'' as STU_bRCVSMS, ''True'' as STU_bRCVMAIL  " & _
        " ,EMD_CUR_MOBILE as Mobile, " & _
        " EMD_EMAIL as Email, " & _
        " '' ''  as comp_name  "

        Dim PartAFilter = ""

        ''Part A


        Dim hasrights As Boolean = False
        hasrights = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT * FROM COM_ADDON_USR_BSU " & _
                        "WHERE CAU_USR_NAME='" & Session("sUsr_name") & "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            hasrights = True
        End If




        'If ddbsu.SelectedIndex > 0 Then
        '    If PartAFilter <> "" Then
        '        PartAFilter += " AND " & "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
        '    Else
        '        PartAFilter = "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
        '    End If

        'End If

        If hasrights = False And RadioButtonList1.SelectedValue = 1 Then
            If ddbsu.SelectedValue <> 0 Then
                If PartAFilter <> "" Then
                    PartAFilter += " AND " & "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
                Else
                    PartAFilter = "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
                End If
            End If
        ElseIf hasrights = False And RadioButtonList1.SelectedValue = 0 Then
            If ddbsu.SelectedValue <> 0 Then
                If PartAFilter <> "" Then
                    PartAFilter += " AND " & "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
                Else
                    PartAFilter = "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
                End If
            End If
        ElseIf hasrights = True And RadioButtonList1.SelectedValue = 1 Then
            'If ddbsu.SelectedValue <> 0 Then
            '    If PartAFilter <> "" Then
            '        PartAFilter += " AND " & "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
            '    Else
            '        PartAFilter = "EMPLOYEE_M.EMP_BSU_ID in(''" & ddbsu.SelectedValue & "'')"
            '    End If
            'End If
        End If



        Dim PBfilter As String = ""
        Dim TJoinFilter As String = ""

        'Part B
        For Each item As DataListItem In DataStaffFilter.Items
            Dim cboxlist As New CheckBoxList
            Dim cgi_id As String = DirectCast(item.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            Dim showflag As String = DirectCast(item.FindControl("HiddenSTF_SHOW_ALL_FLAG"), HiddenField).Value
            cboxlist = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)

            If showflag = "" Then
                showflag = "True"
            End If

            If showflag = "True" Then
                If cboxlist.Items(0).Selected = False Then

                    Dim temp As String = ""
                    Dim i = 0

                    For i = 0 To cboxlist.Items.Count - 1
                        Dim filter As String = ""
                        If cboxlist.Items(i).Selected Then

                            If i = 0 Then
                                filter += "''" & cboxlist.Items(i).Value & "''"
                            Else
                                filter += "''" & cboxlist.Items(i).Value & "'',"
                            End If

                        End If
                        temp += filter
                    Next
                    If temp <> "" Then

                        temp = temp.Substring(0, temp.Length - 1)
                        Dim FilterField As String = DirectCast(item.FindControl("HiddenSTF_FT_FIELDS"), HiddenField).Value
                        Dim TjoinField As String = DirectCast(item.FindControl("HiddenSTS_TABLES"), HiddenField).Value

                        If FilterField <> "" Then

                            FilterField = FilterField.Replace("$", temp)

                            If PBfilter <> "" Then
                                PBfilter += " AND "
                            End If
                            PBfilter += FilterField
                        End If
                        If TjoinField <> "" Then
                            TjoinField = TjoinField.Replace("$", temp)

                            TJoinFilter += " " & TjoinField
                        End If

                    End If

                End If

            Else

                Dim temp As String = ""
                Dim i = 0

                For i = 0 To cboxlist.Items.Count - 1
                    Dim filter As String = ""
                    If cboxlist.Items(i).Selected Then


                        If i = 0 Then
                            filter += "''" & cboxlist.Items(i).Value & "''"
                        Else
                            filter += ",''" & cboxlist.Items(i).Value & "''"
                        End If

                    End If
                    temp += filter
                Next

                If temp <> "" Then
                    If i > 1 Then

                        temp = temp.Substring(0, temp.Length)
                    End If

                    Dim FilterField As String = DirectCast(item.FindControl("HiddenSTF_FT_FIELDS"), HiddenField).Value
                    Dim TjoinField As String = DirectCast(item.FindControl("HiddenSTS_TABLES"), HiddenField).Value

                    If FilterField <> "" Then

                        FilterField = FilterField.Replace("$", temp)

                        If PBfilter <> "" Then
                            PBfilter += " AND "
                        End If
                        PBfilter += FilterField
                    End If
                    If TjoinField <> "" Then
                        TjoinField = TjoinField.Replace("$", temp)

                        TJoinFilter += " " & TjoinField
                    End If

                End If

            End If


        Next

        Dim ReturnQuery As String = ""

        Dim FromQuery As String = " from EMPLOYEE_M  inner join EMPLOYEE_D sd on  EMPLOYEE_M.EMP_ID=sd.EMD_EMP_ID "


        If TJoinFilter <> "" Or PBfilter <> "" Then
            If TJoinFilter <> "" Then
                If PBfilter <> "" Then
                    If PartAFilter <> "" Then
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter & " where " & PBfilter & " AND " & PartAFilter
                    Else
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter & " where " & PBfilter
                    End If
                Else
                    If PartAFilter <> "" Then
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter & " AND " & PartAFilter
                    Else
                        ReturnQuery = "select " & CommonQuery & FromQuery & TJoinFilter
                    End If
                End If
            Else
                If PartAFilter <> "" Then
                    ReturnQuery = "select " & CommonQuery & FromQuery & " where " & PBfilter & " AND " & PartAFilter
                Else
                    ReturnQuery = "select " & CommonQuery & FromQuery & " where " & PBfilter
                End If

            End If
        Else
            If PartAFilter <> "" Then
                ReturnQuery = "select " & CommonQuery & FromQuery & " where " & PartAFilter
            Else
                ReturnQuery = "select " & CommonQuery & FromQuery
            End If

        End If


        Return ReturnQuery

    End Function

    Public Sub BindGrid()
        GrdAddOn.Visible = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""

        Dim ds As DataSet

        Dim Txt1 As String
        Dim Txt2 As String
        Dim Txt3 As String
        Dim Txt4 As String
        Dim Txt5 As String


        str_query = "select * from (" & Hiddenquery.Value & ") a"

        Dim sqlwhere As String = ""

        If GrdAddOn.Rows.Count > 0 Then
            Txt1 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim() 'Unique No
            Txt2 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim() 'Name
            Txt3 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim() 'Email
            Txt4 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim() 'Mobile
            Txt5 = DirectCast(GrdAddOn.HeaderRow.FindControl("Txt5"), TextBox).Text.Trim() 'Company Name



            If Txt1.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(uniqueid,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(uniqueid,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                End If

            End If

            If Txt2.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(Name,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(Name,' ','') like '%" & Txt2.Replace(" ", "") & "%' "
                End If

            End If

            If Txt3.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(Email,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(Email,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                End If

            End If

            If Txt4.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(Mobile,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(Mobile,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
                End If

            End If

            If Txt5.Trim() <> "" Then
                If sqlwhere.Length = 0 Then
                    sqlwhere &= "  replace(comp_name,' ','') like '%" & Txt5.Replace(" ", "") & "%' "
                Else
                    sqlwhere &= " and replace(comp_name,' ','') like '%" & Txt5.Replace(" ", "") & "%' "
                End If

            End If

            If Txt1 <> "" Or Txt2 <> "" Or Txt3 <> "" Or Txt4 <> "" Or Txt5 <> "" Then
                sqlwhere = " Where" & sqlwhere
            End If

        End If


        If sqlwhere.Length > 6 Then
            str_query = str_query & sqlwhere
        End If

        str_query &= " order by uniqueid "

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("uniqueid")
            dt.Columns.Add("Name")
            dt.Columns.Add("Email")
            dt.Columns.Add("Mobile")
            dt.Columns.Add("comp_name")
            dt.Columns.Add("STU_bRCVSMS")
            dt.Columns.Add("STU_bRCVMAIL")


            Dim dr As DataRow = dt.NewRow()
            dr("uniqueid") = ""
            dr("Name") = ""
            dr("Email") = ""
            dr("Mobile") = ""
            dr("comp_name") = ""
            dr("STU_bRCVSMS") = False
            dr("STU_bRCVMAIL") = False


            dt.Rows.Add(dr)
            GrdAddOn.DataSource = dt
            GrdAddOn.DataBind()




        Else


            GrdAddOn.DataSource = ds
            GrdAddOn.DataBind()

        End If

        If GrdAddOn.Rows.Count > 0 Then

            DirectCast(GrdAddOn.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GrdAddOn.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(GrdAddOn.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3
            DirectCast(GrdAddOn.HeaderRow.FindControl("Txt4"), TextBox).Text = Txt4
            DirectCast(GrdAddOn.HeaderRow.FindControl("Txt5"), TextBox).Text = Txt5


        End If


        Dim hash As New Hashtable
        If Session("hashtable") Is Nothing Then
        Else
            hash = Session("hashtable")
        End If
        ''Dim i = 0
        For Each row As GridViewRow In GrdAddOn.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
            Dim key = Hid.Value

            If hash.ContainsValue(Hid.Value) Then
                ch.Checked = True
            End If

        Next

        Session("hashtable") = hash

    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Dim jsFunc As String = ""
        Try
            ''Insert Part A Values
            Dim partA_BSU As String = ddbsu.SelectedValue
            Dim pParmsA(2) As SqlClient.SqlParameter
            pParmsA(0) = New SqlClient.SqlParameter("@CGA_CGR_ID", HiddenGroupid.Value)
            pParmsA(1) = New SqlClient.SqlParameter("@CGA_BSU_ID", partA_BSU)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_A_UPDATE", pParmsA)


            ''Delete PART B Values
            Dim sql_query = "DELETE COM_GROUPS_B WHERE CGB_CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)


            ''Insert Part B Values
            For Each item As DataListItem In DataStaffFilter.Items
                Dim cboxlist As New CheckBoxList
                Dim cgi_id As String = DirectCast(item.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
                Dim showflag As String = DirectCast(item.FindControl("HiddenSTF_SHOW_ALL_FLAG"), HiddenField).Value
                cboxlist = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)
                Dim i = 0
                If showflag = "" Then
                    showflag = "True"
                End If

                If showflag = "True" Then
                    If cboxlist.Items(0).Selected Then
                        Dim value As String = cboxlist.Items(0).Value
                        Dim pParmsB(3) As SqlClient.SqlParameter
                        pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                        pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                        pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)

                    Else

                        For i = 0 To cboxlist.Items.Count - 1
                            If cboxlist.Items(i).Selected Then
                                Dim value As String = cboxlist.Items(i).Value
                                Dim pParmsB(3) As SqlClient.SqlParameter
                                pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                                pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                                pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)
                            End If
                        Next

                    End If
                Else

                    For i = 0 To cboxlist.Items.Count - 1
                        If cboxlist.Items(i).Selected Then
                            Dim value As String = cboxlist.Items(i).Value
                            Dim pParmsB(3) As SqlClient.SqlParameter
                            pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                            pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                            pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)
                        End If
                    Next

                End If


            Next

            ''Insert Query
            Dim query = "Update COM_GROUPS_M set CGR_CONDITION='" & FilterQuery() & "' , CGR_GRP_BSU_ID='" & ddbsu.SelectedValue & "' where CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)

            lblmessage.Text = "Group updated successfully."

            jsFunc = "SetCloseNewGRPValuetoParent('" + lblmessage.Text.ToString + "')"
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            lblmessage.Text = "Error occured while saving . " & ex.Message
            jsFunc = "SetCloseNewGRPValuetoParent('" + lblmessage.Text.ToString + "')"
        Finally

            connection.Close()
        End Try


    End Sub

    Protected Sub btnadonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadonSave.Click

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            lblmessage.Text = ""

            ''Delete Add On Members Values
            Dim sql_query = "DELETE COM_GROUPS_ADD_ON WHERE CGAO_CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)


            Dim hash As New Hashtable

            If Session("hashtable") Is Nothing Then
            Else
                hash = Session("hashtable")
            End If
            For Each row As GridViewRow In GrdAddOn.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenuniqueid"), HiddenField)
                Dim key = Hid.Value
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, Hid.Value)
                    End If

                Else

                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If
            Next


            Dim idictenum As IDictionaryEnumerator
            idictenum = hash.GetEnumerator()
            While (idictenum.MoveNext())
                Dim key = idictenum.Key
                Dim stuid = idictenum.Value
                Dim pParmsAon(3) As SqlClient.SqlParameter
                pParmsAon(0) = New SqlClient.SqlParameter("@CGAO_CGR_ID", HiddenGroupid.Value)
                pParmsAon(1) = New SqlClient.SqlParameter("@CGAO_UNIQUE_ID", stuid)
                pParmsAon(2) = New SqlClient.SqlParameter("@CGAO_BSU_ID", ddbsu.SelectedValue)
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_ADD_ON_INSERT", pParmsAon)

            End While


            ''Delete PART B Values
            sql_query = "DELETE COM_GROUPS_B WHERE CGB_CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

            ''Insert Part B Values
            For Each item As DataListItem In DataStaffFilter.Items
                Dim cboxlist As New CheckBoxList
                Dim cgi_id As String = DirectCast(item.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
                Dim showflag As String = DirectCast(item.FindControl("HiddenSTF_SHOW_ALL_FLAG"), HiddenField).Value
                cboxlist = DirectCast(item.FindControl("|" & cgi_id & "|"), CheckBoxList)
                Dim i = 0
                If showflag = "" Then
                    showflag = "True"
                End If

                If showflag = "True" Then
                    If cboxlist.Items(0).Selected Then
                        Dim value As String = cboxlist.Items(0).Value
                        Dim pParmsB(3) As SqlClient.SqlParameter
                        pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                        pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                        pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)

                    Else

                        For i = 0 To cboxlist.Items.Count - 1
                            If cboxlist.Items(i).Selected Then
                                Dim value As String = cboxlist.Items(i).Value
                                Dim pParmsB(3) As SqlClient.SqlParameter
                                pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                                pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                                pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)
                            End If
                        Next

                    End If
                Else

                    For i = 0 To cboxlist.Items.Count - 1
                        If cboxlist.Items(i).Selected Then
                            Dim value As String = cboxlist.Items(i).Value
                            Dim pParmsB(3) As SqlClient.SqlParameter
                            pParmsB(0) = New SqlClient.SqlParameter("@CGB_CGR_ID", HiddenGroupid.Value)
                            pParmsB(1) = New SqlClient.SqlParameter("@CGB_CGC_ID_STF_FT_ID", cgi_id)
                            pParmsB(2) = New SqlClient.SqlParameter("@CGB_VALUE", value)
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COM_GROUPS_B_INSERT", pParmsB)
                        End If
                    Next

                End If


            Next


            ''Insert Query
            Dim query = "Update COM_GROUPS_M set CGR_CONDITION='" & FilterQuery() & "' , CGR_GRP_BSU_ID='" & ddbsu.SelectedValue & "' where CGR_ID='" & HiddenGroupid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)

            lblmessage.Text = "Add On Group Updated Successfully"
            Session.Remove("hashtable")

            GrdAddOn.Visible = False
            btnadonCancel.Visible = True 'False
            btnadonSave.Visible = False
            btnsearch.Visible = True

            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            lblmessage.Text = "Error occured while saving . " & ex.Message
        Finally

            connection.Close()
        End Try

    End Sub
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Dim jsFunc As String = ""
        jsFunc = "SetCloseNewGRPValuetoParent('Back')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
    End Sub
    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        GrdAddOn.Visible = False
        btnadonCancel.Visible = True 'False
        btnadonSave.Visible = False
        btnsearch.Visible = True
    End Sub
End Class
