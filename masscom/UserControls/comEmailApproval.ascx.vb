Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService

Partial Class masscom_UserControls_comEmailApproval
    Inherits System.Web.UI.UserControl
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            BindApproveGrid()
            BindHrsMins()
        End If
        AssignRights()
    End Sub
    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        For Each row As GridViewRow In GrdEmailApprove.Rows
            Dim addlink As ImageButton = DirectCast(row.FindControl("ImageApprove"), ImageButton)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Add", addlink)
            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next


    End Sub
    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub
    Public Sub BindApproveGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select *,(case CSE_APPROVED WHEN 'NO' THEN 'True' ELSE 'False' END) DELVISBLE ,'javascript:openview('''+ CONVERT(VARCHAR,b.eml_id) +''')' openview, " & _
        " (case c.cgr_grp_type when 'GRP' then 'Groups' else 'Add On' end) as Gtype " & _
        " from COM_SEND_EMAIL a" & _
        " inner join com_manage_email b on  a.cse_eml_id=b.eml_id " & _
        " inner join com_groups_m c on c.cgr_id= a.cse_cgr_id and a.CSE_TYPE='NL' and CSE_BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim Txt1 As String
        Dim Txt2 As String
        Dim Txt3 As String
        Dim Txt4 As String
        Dim DropSearch1 As String
        Dim DropSearch2 As String

        If GrdEmailApprove.Rows.Count > 0 Then
            Txt1 = DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim() 'Id
            Txt2 = DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim() 'Requested Date
            Txt3 = DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim() 'News Letter
            Txt4 = DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim() 'Group
            DropSearch1 = DirectCast(GrdEmailApprove.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue
            DropSearch2 = DirectCast(GrdEmailApprove.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(CSE_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt2.Trim() <> "" Then
                str_query &= " and REPLACE(CONVERT(VARCHAR(11), CSE_ENTRY_DATE, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
            End If

            If Txt3.Trim() <> "" Then
                str_query &= " and replace(EML_NEWS_LETTER_FILE_NAME,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
            End If

            If Txt4.Trim() <> "" Then
                str_query &= " and replace(CGR_DES,' ','') like '%" & Txt4.Replace(" ", "") & "%' "
            End If

            If DropSearch1.Trim() <> "0" Then
                str_query &= " and replace(CGR_TYPE,' ','') like '%" & DropSearch1.Replace(" ", "") & "%' "
            End If

            If DropSearch2.Trim() <> "0" Then
                str_query &= " and replace(cgr_grp_type,' ','') like '%" & DropSearch2.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= "  order by a.CSE_ENTRY_DATE desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("CSE_ID")
            dt.Columns.Add("EML_ID")
            dt.Columns.Add("CSE_ENTRY_DATE")
            dt.Columns.Add("EML_NEWS_LETTER_FILE_NAME")
            dt.Columns.Add("CGR_DES")
            dt.Columns.Add("CGR_TYPE")
            dt.Columns.Add("Gtype")
            dt.Columns.Add("CSE_APPROVED")
            dt.Columns.Add("openview")
            dt.Columns.Add("DELVISBLE")

            Dim dr As DataRow = dt.NewRow()
            dr("CSE_ID") = ""
            dr("EML_ID") = ""
            dr("CSE_ENTRY_DATE") = ""
            dr("EML_NEWS_LETTER_FILE_NAME") = ""
            dr("CGR_DES") = ""
            dr("CGR_TYPE") = ""
            dr("Gtype") = ""
            dr("CSE_APPROVED") = ""
            dr("openview") = ""
            dr("DELVISBLE") = "False"

            dt.Rows.Add(dr)
            GrdEmailApprove.DataSource = dt
            GrdEmailApprove.DataBind()

            DirectCast(GrdEmailApprove.Rows(0).FindControl("Linksend"), LinkButton).Visible = False
            DirectCast(GrdEmailApprove.Rows(0).FindControl("LinkSchedule"), LinkButton).Visible = False
            DirectCast(GrdEmailApprove.Rows(0).FindControl("ImageApprove"), ImageButton).Visible = False

        Else


            GrdEmailApprove.DataSource = ds
            GrdEmailApprove.DataBind()

        End If

        If GrdEmailApprove.Rows.Count > 0 Then

            DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3
            DirectCast(GrdEmailApprove.HeaderRow.FindControl("Txt4"), TextBox).Text = Txt4
            DirectCast(GrdEmailApprove.HeaderRow.FindControl("DropSearch1"), DropDownList).SelectedValue = DropSearch1
            DirectCast(GrdEmailApprove.HeaderRow.FindControl("DropSearch2"), DropDownList).SelectedValue = DropSearch2

        End If


        For Each row As GridViewRow In GrdEmailApprove.Rows

            Dim val As String = DirectCast(row.FindControl("Hiddentempid"), HiddenField).Value
            Dim Filename As String = DirectCast(row.FindControl("HiddenFieldFileName"), HiddenField).Value

            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
            Dim lnknewsletter As LinkButton = DirectCast(row.FindControl("lnknewsletter"), LinkButton)

            lnknewsletter.CommandArgument = val + "/News Letters/" & Filename
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnknewsletter)

        Next

        For Each row As GridViewRow In GrdEmailApprove.Rows
            Dim val As String = DirectCast(row.FindControl("HiddenApproved"), HiddenField).Value
            Dim img As ImageButton = DirectCast(row.FindControl("ImageApprove"), ImageButton)
            If val = "YES" Then
                img.ImageUrl = "~/Images/tick.gif"
                Dim Cse_id As String = DirectCast(row.FindControl("HiddenCSE_ID"), HiddenField).Value
                If VisibleSendButton(Cse_id) Then
                    DirectCast(row.FindControl("Linksend"), LinkButton).Visible = True
                    DirectCast(row.FindControl("LinkSchedule"), LinkButton).Visible = True
                End If
                img.Enabled = False
            Else
                img.ImageUrl = "~/Images/cross.png"
                DirectCast(row.FindControl("Linksend"), LinkButton).Visible = False
                DirectCast(row.FindControl("LinkSchedule"), LinkButton).Visible = False
            End If
        Next


    End Sub
    Public Function VisibleSendButton(ByVal Cse_id As String) As Boolean
        Dim ReturnValue = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select count(*) from COM_LOG_EMAIL_TABLE where LOG_CSE_ID='" & Cse_id & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If val = 0 Then
            ReturnValue = True
        End If

        str_query = "select count(*) from COM_MANAGE_EMAIL_SCHEDULE where CSE_ID='" & Cse_id & "'"
        val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If val = 0 And ReturnValue = True Then
            ReturnValue = True
        Else
            ReturnValue = False
        End If

        'If Session("sBusper") = "True" Then
        '    ReturnValue = True
        'End If

        Return ReturnValue
    End Function

    Protected Sub ddlsearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindApproveGrid()
    End Sub

    Protected Sub GrdEmailApprove_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GrdEmailApprove.PageIndex = e.NewPageIndex
        BindApproveGrid()

    End Sub

    Protected Function GetNavigateUrl(ByVal pId As String) As String

        Return String.Format("javascript:window.showModalDialog('../comSendingEmailNormal.aspx?cse_id={0}&method={1}', '','dialogHeight:500px;dialogWidth:500px;scroll:yes;resizable:no;'); location.reload(true); ", pId, 1)


    End Function

    Protected Sub GrdEmailApprove_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdEmailApprove.RowCommand
        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        If e.CommandName = "selectnews" Then
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("EmailAttachmentsPathVirtual").ToString()
            Dim path = cvVirtualPath & e.CommandArgument.ToString()
            Response.Redirect(path)
        End If
        If e.CommandName = "Schedule" Then
            Dim cse_id As String = e.CommandArgument
            HiddenCSE_ID.Value = cse_id
            Dim ds As DataSet
            Dim str_query = "select CSE_CGR_ID,CSE_EML_ID from COM_SEND_EMAIL where CSE_ID='" & e.CommandArgument & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                HiddenCGR_ID.Value = ds.Tables(0).Rows(0).Item("CSE_CGR_ID").ToString()
                HiddenEML_ID.Value = ds.Tables(0).Rows(0).Item("CSE_EML_ID").ToString()

                str_query = "select * from COM_MANAGE_EMAIL_SCHEDULE where CSE_ID='" & e.CommandArgument & "'"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count > 0 Then
                    lblmessage.Text = "You have already scheduled this template"
                Else
                    MO1.Show()
                End If


            End If

         
        End If
        If e.CommandName = "approve" Then
            Dim str_query = "Update COM_SEND_EMAIL set CSE_APPROVED='YES',CSE_APPROVE_DATE=getdate() where CSE_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            BindApproveGrid()
        End If

        If e.CommandName = "Deleting" Then
            Dim str_query = "delete COM_SEND_EMAIL where CSE_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            BindApproveGrid()
        End If

        If e.CommandName = "search" Then
            BindApproveGrid()
        End If

    End Sub
    Protected Sub GrdAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdEmailApprove.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        End If
    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        If hours < 0 Then

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EML_ID", HiddenEML_ID.Value)
            pParms(1) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
            pParms(2) = New SqlClient.SqlParameter("@CSE_ID", HiddenCSE_ID.Value)
            pParms(3) = New SqlClient.SqlParameter("@CGR_ID", HiddenCGR_ID.Value)
            pParms(4) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
            pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_SCHEDULE_INSERT", pParms)
            lblmessage.Text = "Schedule has been successfully done"
            lnkschedule.Visible = False
            BindApproveGrid()
            MO1.Hide()
        Else
            lblsmessage.Text = "Date time is past"
            MO1.Show()
        End If


    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        MO1.Hide()
    End Sub
End Class
