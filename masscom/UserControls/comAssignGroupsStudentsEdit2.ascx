<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsStudentsEdit2.ascx.vb" Inherits="masscom_UserControls_comAssignGroupsStudentsEdit" %>
<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->

<script type="text/javascript">
    function client_OnTreeNodeChecked() {
        var obj = window.event.srcElement;
        var treeNodeFound = false;
        var checkedState;
        if (obj.tagName == "INPUT" && obj.type == "checkbox") {
            var treeNode = obj;
            checkedState = treeNode.checked;
            do {
                obj = obj.parentElement;
            } while (obj.tagName != "TABLE")
            var parentTreeLevel = obj.rows[0].cells.length;
            var parentTreeNode = obj.rows[0].cells[0];
            var tables = obj.parentElement.getElementsByTagName("TABLE");
            var numTables = tables.length
            if (numTables >= 1) {
                for (i = 0; i < numTables; i++) {
                    if (tables[i] == obj) {
                        treeNodeFound = true;
                        i++;
                        if (i == numTables) {
                            return;
                        }
                    }
                    if (treeNodeFound == true) {
                        var childTreeLevel = tables[i].rows[0].cells.length;
                        if (childTreeLevel > parentTreeLevel) {
                            var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                            var inputs = cell.getElementsByTagName("INPUT");
                            inputs[0].checked = checkedState;
                        }
                        else {
                            return;
                        }
                    }
                }
            }
        }
    }
    function change_chk_state(chkThis) {
        var ids = chkThis.id

        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                document.forms[0].elements[i].checked = state;
            }
        }
        return false;
    }


    function uncheckall(chkThis) {
        var ids = chkThis.id
        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
        var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }
        var uncheckflag = 0

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                if (currentid.indexOf(value) == -1) {
                    if (document.forms[0].elements[i].checked == false) {
                        uncheckflag = 1
                    }
                }
            }
        }

        if (uncheckflag == 1) {
            // uncheck parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }
        else {
            // Check parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = true;
                }
            }
        }


        return false;
    }
    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }
</script>

<script language="javascript" type="text/javascript">
    function SetCloseNewGRPValuetoParent(msg) {
        //alert('close1');
        parent.SetCloseNewGRPValuetoParent(msg);
        return false;
    }
            </script>
<div>

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <div>

                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="4" class="title-bg-lite">Groups</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Group</span></td>

                            <td align="left" width="30%">
                                <asp:TextBox ID="txtgroups" runat="server"></asp:TextBox></td>
                            <td align="left" width="20%"><span class="field-label">Group Type</span></td>

                            <td align="left" width="30%">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                     AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="0">New Group</asp:ListItem>
                                    <asp:ListItem Value="1">Add On</asp:ListItem>
                                </asp:RadioButtonList></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Contact</span></td>

                            <td colspan="3">

                                <asp:CheckBoxList ID="CheckContact" RepeatDirection="Horizontal" runat="server" OnSelectedIndexChanged = "OnCheckBox_Changed" AutoPostBack="true">
                                    <asp:ListItem Text="<span class='field-label'>Primary Contact  (or)</span>" Value="P"></asp:ListItem>
                                    <asp:ListItem Text="Father" Value="F"></asp:ListItem>
                                    <asp:ListItem Text="Mother" Value="M"></asp:ListItem>
                                    <asp:ListItem Text="Guardian" Value="G"></asp:ListItem>
                                </asp:CheckBoxList>

                            </td>
                        </tr>
                        <tr>
                               <td align="left" width="20%"><span class="field-label">Gender</span></td>
                             <td  colspan="3">
                                <asp:CheckBoxList ID="chkGender" RepeatDirection="Horizontal" runat="server" OnSelectedIndexChanged = "OnCheckBox_Changed" AutoPostBack="true">
                                    <asp:ListItem Text="<span class='field-label'>Boy</span>"  Value="M"></asp:ListItem>
                                    <asp:ListItem Text="<span class='field-label'>Girl</span>"  Value="F"></asp:ListItem>
                                    
                                </asp:CheckBoxList>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="title-bg-lite">Recipients</td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                            <td colspan="3">
                                <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                            <td colspan="3">
                                <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACD_ID_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr id="trCurr" runat="server">
                            <td align="left" width="20%"><span class="field-label">Curriculum</span></td>

                            <td colspan="3">
                                <asp:DropDownList ID="ddlCLM_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCLM_ID_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr id="trshf" runat="server">
                            <td align="left" width="20%"><span class="field-label">Shift</span></td>

                            <td colspan="3">
                                <asp:DropDownList ID="ddlSHF_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSHF_ID_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr id="trstm" runat="server">
                            <td align="left" width="20%"><span class="field-label">Stream</span></td>

                            <td colspan="3">
                                <asp:DropDownList ID="ddlSTM_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSTM_ID_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Grade &amp; Section</span></td>

                            <td colspan="3">
                                <div class="checkbox-list">
                                    <asp:Panel ID="plGrade" runat="server">
                                        <asp:TreeView ID="tvGrade" runat="server" ExpandDepth="1" onclick="client_OnTreeNodeChecked();"
                                            ShowCheckBoxes="All">
                                            <DataBindings>
                                                <asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand" TextField="Text"
                                                    ValueField="ValueField" />
                                            </DataBindings>
                                        </asp:TreeView>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="4"></td>
                        </tr>
                        <tr id="parthdr_id" runat="server">
                            <td colspan="4" class="title-bg-lite">Part B</td>
                        </tr>
                        <tr id="partdtl_id" runat="server">
                            <td colspan="4">

                                <asp:DataList ID="DataListPartB" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" ShowHeader="False" Width="100%">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenCGC_ID" Value='<%# Eval("CGC_ID") %>' runat="server" />
                                        <asp:HiddenField ID="HiddenCondition" Value='<%# Eval("CGC_CONDITION") %>' runat="server" />
                                        <asp:HiddenField ID="HiddenValueField" Value='<%# Eval("CGC_VALUE_FIELD") %>' runat="server" />
                                        <asp:HiddenField ID="HiddenTextField" Value='<%# Eval("CGC_TEXT_FIELD") %>' runat="server" />
                                        <asp:HiddenField ID="HiddenFilterField" Value='<%# Eval("CGA_FILTER_FIELD") %>' runat="server" />
                                        <asp:HiddenField ID="HiddenTJoinField" Value='<%# Eval("CGA_TABLES") %>' runat="server" />
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="title-bg">
                                                    <%# Eval("CGC_DES") %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox-list-full">
                                                        <asp:Panel ID="PanelControl" runat="server">
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                    </ItemTemplate>

                                </asp:DataList>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <center>
                 <asp:Panel id="Panel1" runat="server" Width="100%"><asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                        <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" Visible="true"  />
                    </asp:Panel></center>
                                <asp:Panel ID="Panel2" runat="server" Width="100%" Visible="False">
                                    <asp:GridView ID="GrdAddOn" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GrdAddOn_PageIndexChanging"
                                        OnPageIndexChanged="GrdAddOn_PageIndexChanged" PageSize="20" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Check">
                                                <HeaderTemplate>

                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                        ToolTip="Click here to select/deselect all rows" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ch1" Visible='<%#Eval("s1")%>' Text="" runat="server" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unique No">
                                                <HeaderTemplate>
                                                    Unique No
                                <br />
                                                    <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <asp:HiddenField ID="Hiddenuniqueid" Value='<%#Eval("uniqueid")%>' runat="server" />
                                                    <%#Eval("uniqueid")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <HeaderTemplate>
                                                    Name
                                <br />
                                                    <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <%#Eval("Name")%>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <HeaderTemplate>
                                                    Email
                                <br />
                                                    <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <%#Eval("Email")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile">
                                                <HeaderTemplate>
                                                    Mobile
                                <br />
                                                    <asp:TextBox ID="Txt4" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch4" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <%#Eval("Mobile")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Company Name">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                               Company Name
                                <br />
                                <asp:TextBox ID="Txt5" width="50px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch5" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                               
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>
<ItemTemplate>
         <%#Eval("comp_name")%> 
        
</ItemTemplate>
</asp:TemplateField>--%>

                                            <asp:TemplateField HeaderText="Contact">
                                                <HeaderTemplate>

                                                    <br />
                                                    Contact   
                               <br />
                                                    <br />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center> <%#Eval("Contact")%> </center>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle />
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EmptyDataRowStyle />
                                        <EditRowStyle />
                                    </asp:GridView>
                                    <center>
                        <asp:Button id="btnsearch" runat="server" CssClass="button" Text="Search"  />
                        <asp:Button id="btnadonSave" runat="server" CssClass="button" Text="Save"  />
                        <asp:Button id="btnadonCancel" runat="server" CssClass="button" Text="Cancel"  /></center>
                                    &nbsp;<br />
                                </asp:Panel>
                                &nbsp;
                    <center>
                        <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>&nbsp;</center>
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="Hiddenacyid" runat="server" />
                    <asp:HiddenField ID="Hiddenacdid" runat="server" />
                    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                    <asp:HiddenField
                        ID="Hiddenquery" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="HiddenGroupid" runat="server" />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtgroups"
                        Display="None" ErrorMessage="Please enter Group Description" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </td>
        </tr>
    </table>

</div>
