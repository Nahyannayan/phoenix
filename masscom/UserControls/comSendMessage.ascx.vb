﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_Communication_comSendMessage
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenFlag.Value = 0
            Hiddencsm_id.Value = Request.QueryString("csm_id")
            'notification()
            BindHrsMins()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub notification()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select DATEDIFF(second,SCHEDULE_ON_TIME,GETDATE())SEC from COM_NOTIFICATIONS"

        Dim sec As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If sec > 5 Then
            lblsmessage.Text = "Messaging System is offline.Please contact the administrator."
            btnok2.Visible = False
        Else
            btnok2.Visible = True
        End If

    End Sub

    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub

    Protected Sub RadioSend_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioSend.SelectedIndexChanged
        lblsmessage.Text = ""
        If RadioSend.SelectedValue = "1" Then
            TR1.Visible = False
        Else
            TR1.Visible = True
        End If
    End Sub

    Public Function ValidateControls() As Boolean
        Dim rval = True

        If Not IsDate(txtdate.Text.Trim) And RadioSend.SelectedValue = "2" Then
            lblsmessage.Text = "Please enter valid date"
            rval = False
        End If

        Return rval
    End Function

    Protected Sub btnok2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok2.Click

        If RadioSend.SelectedValue = "2" Then
            Dim Encr_decrData As New Encryption64
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

            Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
            Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
            Dim hours = ts.TotalHours
            If hours < 0 Then

                Dim Query = "select * from COM_SEND_SMS where CSM_ID='" & Request.QueryString("csm_id") & "'"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)

                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@CSM_ID", Request.QueryString("csm_id"))
                pParms(1) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
                pParms(2) = New SqlClient.SqlParameter("@CMS_ID", ds.Tables(0).Rows(0).Item("CSM_CMS_ID").ToString())
                pParms(3) = New SqlClient.SqlParameter("@CGR_ID", ds.Tables(0).Rows(0).Item("CSM_CGR_ID").ToString())
                pParms(4) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Session("sBsuid"))
                pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_SCHEDULE_INSERT", pParms)
                btnok2.Visible = False

                lblsmessage.Text = "Schedule has been successfully done"

            Else
                lblsmessage.Text = "Date time is past"

            End If
        Else
            HiddenFlag.Value = 1
        End If
       

    End Sub

End Class
