<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comManageSMS.ascx.vb"
    Inherits="masscom_UserControls_comManageSMS" %>
<!-- Bootstrap core CSS-->
<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../cssfiles/Popup.css" rel="stylesheet" />

<style type="text/css">
    .style1 {
        width: 100%;
    }
</style>

<script language="javascript">
    function openPopup(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 600px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";


        var result;
        result = window.showModalDialog(strOpen, "", sFeatures);
        if (result == "1") {
            window.location.reload(true);
        }

    }

</script>
<script type="text/javascript">

    function opneWindow() {

        var sFeatures;
        sFeatures = "dialogWidth: 900px; ";
        sFeatures += "dialogHeight: 700px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "../comMergerDocument.aspx?Type=SMS"

        var result;
        result = window.showModalDialog(strOpen, "", sFeatures);


    }
</script>

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>

<table >
    <tr>
        <td align="left">

            <asp:LinkButton ID="lnkAddNew" runat="server" OnClick="PopUpSMSWindow">Add New</asp:LinkButton>
        </td>
        <td align="right">
            <asp:LinkButton ID="Linkmerge" OnClientClick="javascript:opneWindow();return false;" CausesValidation="false" runat="server">Upload Merge Document</asp:LinkButton>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">SMS Templates
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridMSMS" AutoGenerateColumns="false" Width="100%" runat="server"
                AllowPaging="True" EmptyDataText="No SMS templates added yet (or) Search query did not produce any results"
                OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="20" OnRowCommand="GridMSMS_RowCommand" CssClass="table table-bordered table-row table-striped">
                <Columns>
                    <asp:TemplateField HeaderText="Sl No">
                        <HeaderTemplate>
                            Sl No
                                                    <br />
                            <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="Hiddenid" Value='<%# Eval("CMS_ID") %>' runat="server" />
                            <center>
                                            <%# Eval("CMS_ID") %></center>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Create Date">
                        <HeaderTemplate>
                            Create Date
                                                    <br />
                            <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                ImageUrl="~/Images/forum_search.gif" />
                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                TargetControlID="Txt2">
                            </ajaxToolkit:CalendarExtender>

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                            <%#Eval("CMS_DATE", "{0:dd/MMM/yyyy}")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sms Text">
                        <HeaderTemplate>
                            Sms Text
                                                    <br />
                            <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                            <asp:Panel ID="T12Panel1" runat="server" Height="50px">
                                <%#Eval("cms_sms_text")%>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                TextLabelID="T12lblview">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="View">
                        <HeaderTemplate>

                            <br />
                            View
                                                    <br />
                            <br />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                        <a href="javascript:openPopup('../comViewSmsText.aspx?cmsid=<%# Eval("CMS_ID") %>&mnu=<%# Me.hfMenuCode.Value%>')">
                                            <asp:Image ID="Image1" ImageUrl="~/Images/View.png"  Height="25" Width="50" runat="server" /></a>
                                    </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle />
                <RowStyle CssClass="griditem" />
                <SelectedRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
                <EmptyDataRowStyle />
                <EditRowStyle />
            </asp:GridView>

        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />

<asp:HiddenField ID="hfMenuCode" runat="server" />

