<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comMailScheduleNotification.ascx.vb"
    Inherits="masscom_UserControls_comMailScheduleNotification" %>
<!-- Bootstrap core CSS-->
<%--<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">--%>
<%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

<!-- Bootstrap header files ends here -->
<div id="OffAlert" runat="server" class="panel-cover" style="width: 60%; margin: auto;" >
<center>
 <table border="0"  cellpadding="0" cellspacing="0" Width="100%" >
                        <tr>
                            <td class="title-bg-lite">
                                Messaging Alert</td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Image ID="Image1" runat="server"  height="25px" AlternateText="Messaging System is offline" runat="server" />&nbsp; <u>Messaging System is offline</u><br />
    Emails and SMS will not be delivered at this time.Please contact the administrator.
    <br />
    <br />
    You have the following Options<br />
    1. You may proceed or schedule with the messaging task, the message will be delivered
    when the system is online.<br />
    2. Please try again later.



 </td>
                    </tr>
               </table>
   </center>
</div>
