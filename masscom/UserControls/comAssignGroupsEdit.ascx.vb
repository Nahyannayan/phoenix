Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comAssignGroupsEdit
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim Encr_decrData As New Encryption64
            Dim Groupid = Encr_decrData.Decrypt(Request.QueryString("Groupid").Replace(" ", "+"))

            Dim str_query = "Select CGR_TYPE from COM_GROUPS_M where CGR_ID='" & Groupid & "'"
            Dim type = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            If type = "STUDENT" Then
                Tab1.ActiveTabIndex = 0
                HT1.Enabled = True
                HT2.Enabled = False

            Else
                Tab1.ActiveTabIndex = 1
                HT1.Enabled = False
                HT2.Enabled = True

            End If

        End If

    End Sub

End Class
