<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comEmailApproval.ascx.vb"
    Inherits="masscom_UserControls_comEmailApproval" %>
<%@ Register Src="comMailScheduleNotification.ascx" TagName="comMailScheduleNotification"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script type="text/javascript">

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false;

    }

</script>

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel11" runat="server">
    <contenttemplate>
<uc1:comMailScheduleNotification ID="ComMailScheduleNotification1" runat="server" />
<br />
<asp:Label ID="lblmessage" runat="server" CssClass="matters" ForeColor="Red"></asp:Label>
<br />
<div class="matters">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
        <tr>
            <td class="subheader_img">
                Approve and Send &nbsp;News Letter Emails&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left">
                                    <asp:GridView ID="GrdEmailApprove" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found for Sms Approval"
                                        AllowPaging="True" OnPageIndexChanging="GrdEmailApprove_PageIndexChanging" PageSize="20"
                                        Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Id">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Id
                                                                <br />
                                                                <asp:TextBox ID="Txt1" Width="40px" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                                    ImageUrl="~/Images/forum_search.gif" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("CSE_ID")%>
                                                        <asp:HiddenField ID="HiddenCSE_ID" Value='<%#Eval("CSE_ID")%>' runat="server" />
                                                        <asp:HiddenField ID="Hiddentempid" Value='<%#Eval("EML_ID")%>' runat="server" />
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Date
                                                                <br />
                                                                <asp:TextBox ID="Txt2" Width="60px" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                                    ImageUrl="~/Images/forum_search.gif" />
                                                                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                                    TargetControlID="Txt2">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%# Eval("CSE_ENTRY_DATE", "{0:dd/MMM/yyyy}") %></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="News Letter">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                News Letter
                                                                <br />
                                                                <asp:TextBox ID="Txt3" Width="80px" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                                    ImageUrl="~/Images/forum_search.gif" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddenFieldFileName" Value='<%#Eval("EML_NEWS_LETTER_FILE_NAME")%>'
                                                        runat="server" />
                                                    <%--       <%#Eval("EML_ID")%>--%>
                                                    <asp:LinkButton ID="lnknewsletter" runat="server" CausesValidation="false" OnClientClick=' <%# Eval("openview") %>'
                                                        Text=' <%# Eval("EML_NEWS_LETTER_FILE_NAME") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="120px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Group">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Group
                                                                <br />
                                                                <asp:TextBox ID="Txt4" Width="40px" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch4" runat="server" CausesValidation="false" CommandName="search"
                                                                    ImageUrl="~/Images/forum_search.gif" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("CGR_DES")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Type
                                                                <br />
                                                                <asp:DropDownList ID="DropSearch1" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                                    runat="server">
                                                                    <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                                    <asp:ListItem Value="STUDENT" Text="STUDENT"></asp:ListItem>
                                                                    <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%# Eval("CGR_TYPE") %></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Group Type">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Group Type
                                                                <br />
                                                                <asp:DropDownList ID="DropSearch2" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged"
                                                                    runat="server">
                                                                    <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                                    <asp:ListItem Value="GRP" Text="Group"></asp:ListItem>
                                                                    <asp:ListItem Value="AON" Text="Add On"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("Gtype")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Approved">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                <br />
                                                                Approved
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddenApproved" Value='<%#Eval("CSE_APPROVED")%>' runat="server" />
                                                    <center>
                                                        <asp:ImageButton ID="ImageApprove" CommandName="approve" CommandArgument='<%#Eval("CSE_ID")%>'
                                                            ImageUrl="~/Images/cross.png" runat="server" /></center>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cfm1" ConfirmText="Do you wish to approve? "
                                                        TargetControlID="ImageApprove" runat="server">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Send">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                <br />
                                                                Send
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:LinkButton ID="Linksend" CommandName="Send" Visible="false" OnClientClick='<%# GetNavigateUrl(Eval("CSE_ID").ToString()) %>'
                                                            runat="server">Proceed</asp:LinkButton></center>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cfm2" ConfirmText="Proceed with email sending ?"
                                                        TargetControlID="Linksend" runat="server">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                <br />
                                                                Schedule
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:LinkButton ID="LinkSchedule" CommandName="Schedule" Visible="false" CommandArgument='<%#Eval("CSE_ID")%>'
                                                            runat="server">Schedule</asp:LinkButton></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<span style='font-size: smaller'>Delete</span>">
                                     <HeaderTemplate>
                                                <table class="BlueTable" width="100%">
                                                    <tr class="matterswhite">
                                                        <td align="center" colspan="2">
                                                            <br />
                                                         Delete
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                        <ItemTemplate>
                                         <center> <asp:LinkButton ID="Linkdelete" CommandName="Deleting" Visible='<%#Eval("DELVISBLE")%>' CommandArgument='<%#Eval("CSE_ID")%>'
                                                runat="server">Delete</asp:LinkButton></center>  
                                                <ajaxToolkit:ConfirmButtonExtender ID="C45" ConfirmText="Do you wish to continue ?" TargetControlID="Linkdelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                        </Columns>
                                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <EditRowStyle Wrap="False" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:HiddenField ID="HiddenCSE_ID" runat="server" />
                <asp:HiddenField ID="HiddenCGR_ID" runat="server" />
                <asp:HiddenField ID="HiddenEML_ID" runat="server" />
                <div class="matters">
                    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                    <asp:Panel ID="PanelSchedule" runat="server" CssClass="modalPopup" Style="display: none">
                        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="250">
                            <tr>
                                <td class="subheader_img">
                                    Set Schedule Time
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        Date
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="s"></asp:TextBox>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Hour
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddhour" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Mins
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddmins" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        <asp:Button ID="btnok" runat="server" CssClass="button" OnClick="btnok_Click" Text="Ok"
                                                            ValidationGroup="s" Width="80px" />
                                                        <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" Width="80px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        <asp:Label ID="lblsmessage" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                                                TargetControlID="txtdate">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                                                Display="None" ErrorMessage="Please Enter Date" SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                                                ShowSummary="False" ValidationGroup="s" />
                                            &nbsp;
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <asp:LinkButton ID="lnkschedule" Style="display: none" runat="server"></asp:LinkButton>
                <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelSchedule"
                    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="lnkschedule">
                </ajaxToolkit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</div>
</contenttemplate>
</asp:UpdatePanel>
