<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comNewsLetters.ascx.vb" Inherits="masscom_UserControls_comNewsLetters" %>
<%@ Register Src="comCreateNewsLetters.ascx" TagName="comCreateNewsLetters" TagPrefix="uc1" %>
<%@ Register Src="comEditNewsLetters.ascx" TagName="comEditNewsLetters" TagPrefix="uc2" %>
<div class="matters">
<asp:RadioButtonList ID="RadioButtonListSelection" runat="server" AutoPostBack="True" BorderColor="Silver" BorderWidth="1px" RepeatDirection="Horizontal">
    <asp:ListItem Value="0">Create News Letters</asp:ListItem>
    <asp:ListItem Value="1">View/Edit News Letters</asp:ListItem>
</asp:RadioButtonList>
<hr />
<asp:Panel ID="Panel1" runat="server" Visible="False">
    <uc1:comCreateNewsLetters id="ComCreateNewsLetters1" runat="server">
    </uc1:comCreateNewsLetters>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server" Visible="False">
    <uc2:comEditNewsLetters ID="ComEditNewsLetters1" runat="server" />
</asp:Panel>
</div>