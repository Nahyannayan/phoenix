Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
'Imports System.Data.Oledb
Imports GemBox.Spreadsheet


Partial Class masscom_UserControls_comExcelSendSms
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            CheckMenuRights()
            BindTemplates()
            BindHrsMins()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
        AssignRights()
    End Sub
    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        For Each row As GridViewRow In Grdsmssend.Rows
            Dim Lnksend As LinkButton = DirectCast(row.FindControl("Lnksend"), LinkButton)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Add", Lnksend)
            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next
        Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
        directory.Add("Add", LinkButton3)
        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000090" And ViewState("MainMnu_code") <> "M000091") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
    Public Sub BindTemplates()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select cms_id,cms_date,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(cms_sms_text,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,cms_sms_text  from com_manage_sms WITH (NOLOCK) where CMS_BSU_ID='" & Hiddenbsuid.Value & "' AND ISNULL(CMS_DELETED,'False')='False'"
        Dim Txt1 As String
        Dim Txt2 As String
        Dim Txt3 As String

        If Grdsmssend.Rows.Count > 0 Then
            Txt1 = DirectCast(Grdsmssend.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            Txt2 = DirectCast(Grdsmssend.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            Txt3 = DirectCast(Grdsmssend.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(cms_id,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt2.Trim() <> "" Then
                str_query &= " and REPLACE(CONVERT(VARCHAR(11), cms_date, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
            End If

            If Txt3.Trim() <> "" Then
                str_query &= " and replace(cms_sms_text,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by CMS_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("cms_id")
            dt.Columns.Add("cms_date")
            dt.Columns.Add("hide")
            dt.Columns.Add("tempview")
            dt.Columns.Add("cms_sms_text")


            Dim dr As DataRow = dt.NewRow()
            dr("cms_id") = ""
            dr("cms_date") = ""
            dr("hide") = ""
            dr("tempview") = ""
            dr("cms_sms_text") = ""

            dt.Rows.Add(dr)
            Grdsmssend.DataSource = dt
            Grdsmssend.DataBind()

            DirectCast(Grdsmssend.Rows(0).FindControl("Lnksend"), LinkButton).Visible = False
        Else
            Grdsmssend.DataSource = ds
            Grdsmssend.DataBind()

        End If

        If Grdsmssend.Rows.Count > 0 Then

            DirectCast(Grdsmssend.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(Grdsmssend.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
            DirectCast(Grdsmssend.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

        End If

    End Sub

    Protected Sub Grdsmssend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grdsmssend.PageIndexChanging
        Grdsmssend.PageIndex = e.NewPageIndex
        BindTemplates()
       
    End Sub

    Protected Sub Grdsmssend_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grdsmssend.RowCommand
        lblmessage.Text = ""
        If e.CommandName = "send" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "select cms_sms_text  from com_manage_sms WITH (NOLOCK) where cms_id ='" & e.CommandArgument & "'"
            lblmessage.Text = "SMS Message<br>" & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
            Hiddensmsid.Value = e.CommandArgument
            Panel1.Visible = True
            Panel3.Visible = False
            Panel2.Visible = False
        End If
        If e.CommandName = "search" Then
            BindTemplates()
        End If
    End Sub

    Public Function CheckPreviousTemplateName() As Boolean
        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim title As String = txttitle.Text.Trim()
        Dim sql_text = "SELECT * FROM COM_EXCEL_SMS_DATA_MASTER WITH (NOLOCK) WHERE TITLE='" & title & "' AND BSU_ID='" & BsuId & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_text)
        Dim Returnvalue = True
        If ds.Tables(0).Rows.Count > 0 Then
            lblmessage.Text = "Title already exists. Please enter a new title."
            Returnvalue = False
        End If
        Return Returnvalue
    End Function

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        If CheckPreviousTemplateName() Then


            Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
            Dim filename As String = FileUpload1.FileName.ToString()
            Dim savepath As String = path + "SMS/" + Session("EmployeeId") + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)

            FileUpload1.SaveAs(savepath)
            lblmessage.Text = "Uploaded File Name :" & filename
            Dim Encr_decrData As New Encryption64
            HiddenPathEncrypt.Value = Encr_decrData.Encrypt(savepath)

            Dim useexe = WebConfigurationManager.AppSettings("useexe").ToString()

            If useexe = "1" Then
                lnksend2.Visible = True
                lnksend.Visible = False
            Else
                lnksend.Visible = True
                lnksend2.Visible = False
            End If


            Panel2.Visible = True
            Panel1.Visible = False
            Panel3.Visible = False

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TITLE", txttitle.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@TYPE", RadioType.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@EXCEL_PATH", savepath)
                pParms(3) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)


                Dim Logid = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "COM_EXCEL_SMS_DATA_MASTER_INSERT", pParms)
                Hiddenlogid.Value = Logid

                Dim myDataset As New DataSet()
                Dim Xdoc As New XmlDocument

                ''Dim HiddenPath = Encr_decrData.Decrypt(path.Replace(" ", "+"))
                'Dim strConnE As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                '                        "Data Source=" & savepath & ";" & _
                '                        "Extended Properties=""Excel 8.0;"""

                'Dim strConnE As String = ""
                'Dim mySelectStatement As String = ""

                'Dim ext = GetExtension(filename)

                'If ext = "xls" Then
                '    strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savepath & ";Extended Properties=""Excel 8.0;"""
                'ElseIf ext = "xlsx" Then
                '    strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savepath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                'End If


                Try
                    'Dim myData As New OleDbDataAdapter("SELECT ID,MobileNo,Name FROM [Sheet1$] ", strConnE)
                    'myData.TableMappings.Add("Table", "ExcelTest")
                    'myData.Fill(myDataset)


                    'mySelectStatement = "SELECT ID,MobileNo,Name FROM [Sheet1$] "
                    'Using myConnection As New OleDbConnection(strConnE)
                    '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                    '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                    '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                    '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                    '        myAdapter.Fill(myDataSet)
                    '    End Using
                    'End Using
                    
                    SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                    Dim mObj As ExcelRowCollection
                    Dim iRowRead As Boolean
                    Dim ef As ExcelFile = New ExcelFile
                    Dim mRowObj As ExcelRow
                    Dim iRow As Integer = 1
                    Dim dataTable As New DataTable
                    Dim dr As DataRow

                    iRowRead = True

                    dataTable.Columns.Add("ID", GetType(Int64))
                    dataTable.Columns.Add("MobileNo", GetType(String))
                    dataTable.Columns.Add("Name", GetType(String))

                    ef.LoadXls(savepath)
                    mObj = ef.Worksheets(0).Rows
                   
                    
                    'Dim ws As ExcelWorksheet = ef.Worksheets(0)
                    'ws.ExtractToDataTable(dataTable, ws.Rows.Count, ExtractDataOptions.StopAtFirstEmptyRow, ws.Rows(0), ws.Columns(0))

                    While iRowRead
                        mRowObj = mObj(iRow)
                        If mRowObj.Cells(0).Value Is Nothing Then
                            Exit While
                        End If
                        If mRowObj.Cells(0).Value.ToString = "" Then
                            Exit While
                        End If

                        If Not mRowObj.Cells(1).Value Is Nothing Then
                            dr = dataTable.NewRow
                            dr.Item(0) = Convert.ToInt64(mRowObj.Cells(0).Value)
                            dr.Item(1) = mRowObj.Cells(1).Value.ToString
                            'dr.Item(2) = mRowObj.Cells(0).Value.ToString
                            'dr.Item(3) = mRowObj.Cells(1).Value.ToString
                            'dr.Item(4) = "add"
                            'dr.Item(5) = iRow
                            'dr.Item(6) = "0"
                            'dr.Item(7) = "false"
                            dataTable.Rows.Add(dr)
                        End If
                        iRow += 1
                    End While

                    myDataset.Tables.Add(dataTable)

                Catch ex As Exception

                    'Dim myData As New OleDbDataAdapter("SELECT ID,MobileNo,'' as Name FROM [Sheet1$] ", strConnE)
                    'myData.TableMappings.Add("Table", "ExcelTest")
                    'myData.Fill(myDataset)

                    'mySelectStatement = "SELECT ID,MobileNo,Name FROM [Sheet1$] "
                    'Using myConnection As New OleDbConnection(strConnE)
                    '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                    '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                    '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                    '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                    '        myAdapter.Fill(myDataset)
                    '    End Using
                    'End Using

                End Try


                Dim i = 0

                Dim student_id As String
                Dim mobilenumber As String
                Dim errorflag As Boolean
                Dim str_query As String
                Dim name As String


                For i = 0 To myDataset.Tables(0).Rows.Count - 1

                    student_id = myDataset.Tables(0).Rows(i).Item("ID").ToString().Trim()
                    name = myDataset.Tables(0).Rows(i).Item("Name").ToString().Replace("'", "")

                    mobilenumber = myDataset.Tables(0).Rows(i).Item("MobileNo").ToString().Trim()
                    mobilenumber = mobilenumber.Replace("-", "").Trim()
                    If Session("sbsuid") <> "315888" Then
                        mobilenumber = "971" & Right(mobilenumber, 9)
                    Else
                        mobilenumber = "012" & Right(mobilenumber, 7)
                    End If

                    If student_id <> "" And mobilenumber <> "" Then
                        If mobilenumber.Length < 10 Then
                            errorflag = False
                        Else
                            errorflag = True
                        End If

                        str_query = "insert into  COM_EXCEL_SMS_DATA_LOG (LOG_ID,ID,MOBILE_NO,NAME,NO_ERROR_FLAG) values('" & Logid & "','" & student_id & "','" & mobilenumber & "','" & name & "','" & errorflag & "') "
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End If


                Next

                transaction.Commit()
                BindLog()

            Catch ex As Exception
                transaction.Rollback()
                lblmessage.Text = "Error : " & ex.Message
            Finally
                connection.Close()

            End Try

        End If


    End Sub

    Public Sub BindLog()


        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim logId As String = Hiddenlogid.Value
        Dim str_query = "Select count(LOG_ID) from COM_EXCEL_SMS_DATA_LOG WITH (NOLOCK) where LOG_ID='" & logId & "' "
        lblmessage.Text = lblmessage.Text & "<br>Total Numbers :" & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        
        str_query = "Select count(LOG_ID) from COM_EXCEL_SMS_DATA_LOG WITH (NOLOCK)  where NO_ERROR_FLAG='false' and LOG_ID='" & logId & "' "
        lblmessage.Text = lblmessage.Text & "<br>Error in Numbers :" & SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    End Sub


    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        If hours < 0 Then

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CMS_ID", Hiddensmsid.Value)
            pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", Encr_decrData.Decrypt(HiddenPathEncrypt.Value.Replace(" ", "+")))
            pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
            pParms(3) = New SqlClient.SqlParameter("@CSM_ID", 0)
            pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
            pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
            pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", Hiddenlogid.Value)

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_SCHEDULE_INSERT", pParms)
            lblmessage.Text = "Schedule has been successfully done"
            txtdate.Text = ""
            MO1.Hide()
            'lnksend.Visible = False
            'lnkschedule.Visible = False
        Else
            lblsmessage.Text = "Date time is past"
            MO1.Show()
        End If


    End Sub


    Protected Sub lnkcancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkcancel.Click
        lblmessage.Text = ""
        Panel3.Visible = True
        Panel1.Visible = False
        Panel2.Visible = False
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        lblmessage.Text = ""
        Panel3.Visible = True
        Panel1.Visible = False
        Panel2.Visible = False
    End Sub
End Class
