﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_comManageGemsConnectNewsLetter
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            BindTemplate()
            BindEmailText()
            BindHrsMins()
            Btn_grp.Attributes.Add("onclick", "javascript: AssignGroup(); return true;")
            Btn_exl.Attributes.Add("onclick", "javascript: AssignExcel(); return true;")
            'lnkreports.Visible = False
        End If
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btneditplaintextsave)
        AssignRights()


    End Sub

    Public Sub BindTemplate()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim BsuId As String = Hiddenbsuid.Value
        Dim str_query = "select MERGE_ID,MERGE_TITLE_DES from COM_MERGE_TABLES where MERGE_BSU='" & BsuId & "' AND MERGE_TYPE='EMAIL'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtemplate.DataSource = ds
            ddtemplate.DataTextField = "MERGE_TITLE_DES"
            ddtemplate.DataValueField = "MERGE_ID"
            ddtemplate.DataBind()

            Dim item As New ListItem
            item.Text = "Select a Template"
            item.Value = "-1"
            ddtemplate.Items.Insert(0, item)
            Fields()
        Else
            TRDynamic.Visible = False
        End If



    End Sub
    Public Sub Fields()
        ''Clear the Previous message



        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim template As String = ddtemplate.SelectedValue
        Dim str_query = "select MERGE_FIELDS from COM_MERGE_TABLES where MERGE_ID='" & template & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim fields As String = ds.Tables(0).Rows(0).Item("MERGE_FIELDS").ToString()

            Dim Afields() As String = fields.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("Data")
            dt.Columns.Add("Value")

            Dim i = 0
            For i = 0 To Afields.Length - 1
                Dim cval As String = Afields(i)


                Dim csplit() As String = cval.Split(">")

                Dim dr As DataRow = dt.NewRow()

                dr("Value") = "$" & csplit(0) & "$"
                dr("Data") = csplit(1).Replace("$", "").Replace("<", "")

                dt.Rows.Add(dr)
            Next

            ddfields.DataSource = dt
            ddfields.DataTextField = "Data"
            ddfields.DataValueField = "Value"
            ddfields.DataBind()
            ddfields.Visible = True
            btninsert.Visible = True
        Else
            ddfields.Visible = False
            btninsert.Visible = False
        End If



    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        For Each row As GridViewRow In GrdEmailText.Rows
            Dim lnkdelete As ImageButton = DirectCast(row.FindControl("lnkdelete"), ImageButton)
            Dim lnkEdit As ImageButton = DirectCast(row.FindControl("lnkEdit"), ImageButton)
            Dim directory2 As New System.Collections.Generic.Dictionary(Of String, Object)
            directory2.Add("Delete", lnkdelete)
            directory2.Add("Edit", lnkEdit)

            Call AccessRight3.setpage(directory2, ViewState("menu_rights"), ViewState("datamode"))
        Next

    End Sub
    Protected Sub lblbtn_Click(sender As Object, e As EventArgs) Handles lblbtn.Click


    End Sub
    Public Shared Function GET_EMAIL_SMS_TEMPLATES(ByVal OPTIONS As Integer, Optional ByVal TEMPLATE_ID As String = "") As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@TEMPLATE_ID", SqlDbType.VarChar)
        pParms(1).Value = TEMPLATE_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[DBO].[GET_EMAIL_SMS_TEMPLATES]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function GET_GROUPS_TEMPLATES_M(ByVal OPTIONS As Integer, Optional ByVal CGR_ID As String = "") As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@CGR_ID", SqlDbType.VarChar)
        pParms(1).Value = CGR_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[DBO].[GET_GROUPS_TEMPLATES_M]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Protected Sub grptemplateid_ValueChanged(sender As Object, e As EventArgs) Handles grptemplateid.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        TR1.Attributes.Add("style", "display:grid")
        TR2.Attributes.Add("style", "display:grid")
        TR3.Attributes.Add("style", "display:none")
        Dim c_date As String = DateTime.Now.ToString("dd/MMM/yyyy")
        txtdate.Text = c_date
        BindHrsMins()

        If DateTime.Now.Hour < 10 Then
            Dim hrr As String = "0" & DateTime.Now.Hour
            ddhour.SelectedValue = hrr
        Else
            ddhour.SelectedValue = DateTime.Now.Hour
        End If
        If DateTime.Now.Minute < 10 Then
            Dim minn As String = "0" & DateTime.Now.Minute
            ddmins.SelectedValue = minn
        Else
            ddmins.SelectedValue = DateTime.Now.Minute
        End If

        grptemplateid.Value = ERR_MSG
        Dim grp_name As String = ""
        Dim dt5 As DataTable = GET_GROUPS_TEMPLATES_M(1, grptemplateid.Value)

        For Each row2 As DataRow In dt5.Rows
            If grp_name = "" Then
                grp_name = row2.Item("CGR_DES")
            Else
                grp_name = grp_name + " || " + row2.Item("CGR_DES")
            End If


        Next
        popupmsg.Text = "Selected Groups is/are: " + grp_name 'grptemplateid.Value
    End Sub

    Protected Sub exltemplateid_ValueChanged(sender As Object, e As EventArgs) Handles exltemplateid.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        TR1.Attributes.Add("style", "display:grid")
        TR2.Attributes.Add("style", "display:grid")
        TR3.Attributes.Add("style", "display:none")
        Dim c_date As String = DateTime.Now.ToString("dd/MMM/yyyy")
        txtdate.Text = c_date
        BindHrsMins()

        If DateTime.Now.Hour < 10 Then
            Dim hrr As String = "0" & DateTime.Now.Hour
            ddhour.SelectedValue = hrr
        Else
            ddhour.SelectedValue = DateTime.Now.Hour
        End If
        If DateTime.Now.Minute < 10 Then
            Dim minn As String = "0" & DateTime.Now.Minute
            ddmins.SelectedValue = minn
        Else
            ddmins.SelectedValue = DateTime.Now.Minute
        End If
        exltemplateid.Value = ERR_MSG

        Dim tmplt_name As String = ""
        Dim dt5 As DataTable = GET_GROUPS_TEMPLATES_M(2, exltemplateid.Value)

        For Each row2 As DataRow In dt5.Rows
            If tmplt_name = "" Then
                tmplt_name = row2.Item("TITLE")
            Else
                tmplt_name = tmplt_name + " || " + row2.Item("TITLE")
            End If



        Next
        popupmsg.Text = "Selected Excel Templates is/are: " + tmplt_name 'exltemplateid.Value
        popuptitlemsg.Text = lblmessage.Text
    End Sub

    Protected Sub h_MSG_ValueChanged(sender As Object, e As EventArgs) Handles h_MSG.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        BindEmailText()
        lblmessage.Text = ERR_MSG
        h_MSG.Value = ""

    End Sub

    Protected Sub h_MSG2_ValueChanged(sender As Object, e As EventArgs) Handles h_MSG2.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value

        lblmessage.Text = ERR_MSG
        h_MSG2.Value = ""
    End Sub
    Public Sub BindEmailText()
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "select *,'javascript:openview('''+ CONVERT(VARCHAR,EML_ID) +''')' openview,'javascript:AttachmentPopup('''+ CONVERT(VARCHAR,EML_ID) +'''); return false;' AttachmentPopup,'javascript:EditTemplate('''+ CONVERT(VARCHAR,EML_ID) +'''); return false;' EditTemplate,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('<span style=''color: red;font-weight:bold''> more... </span>')tempview from COM_MANAGE_EMAIL where EML_NEWS_LETTER='False' and EML_BSU_ID='" & Hiddenbsuid.Value & "' AND ISNULL(EML_DELETED,'False')='False' AND EML_NEWSLETTERCONNECT=1"
            Dim Txt1 As String = ""
            Dim Txt2 As String = ""
            Dim Txt3 As String = ""
            Txt3 = Txt_EmailTitle.Text.Trim()
            If GrdEmailText.Rows.Count > 0 Then
                'Txt1 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
                'Txt2 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
                'Txt3 = DirectCast(GrdEmailText.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

                If Txt1.Trim() <> "" Then
                    str_query &= " and replace(EML_ID,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
                End If

                If Txt2.Trim() <> "" Then
                    str_query &= " and REPLACE(CONVERT(VARCHAR(11), EML_DATE, 106), ' ', '/') like '%" & Txt2.Replace(" ", "") & "%' "
                End If

                If Txt3.Trim() <> "" Then
                    str_query &= " and replace(EML_TITLE,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
                End If

            End If

            str_query &= " order by EML_DATE desc "


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count = 0 Then
                Dim dt As New DataTable
                dt.Columns.Add("EML_ID")
                dt.Columns.Add("EML_DATE")
                dt.Columns.Add("EML_FROM")
                dt.Columns.Add("tempview")
                dt.Columns.Add("EML_TITLE")
                dt.Columns.Add("hide")
                dt.Columns.Add("openview")
                dt.Columns.Add("AttachmentPopup")
                dt.Columns.Add("EditTemplate")


                Dim dr As DataRow = dt.NewRow()
                dr("EML_ID") = ""
                dr("EML_DATE") = ""
                dr("EML_FROM") = ""
                dr("tempview") = ""
                dr("EML_TITLE") = ""
                dr("hide") = ""
                dr("openview") = ""
                dr("AttachmentPopup") = ""
                dr("EditTemplate") = ""

                dt.Rows.Add(dr)
                GrdEmailText.DataSource = dt
                GrdEmailText.DataBind()

                DirectCast(GrdEmailText.Rows(0).FindControl("lnkdelete"), ImageButton).Visible = False
                DirectCast(GrdEmailText.Rows(0).FindControl("lnkEdit"), ImageButton).Visible = False
                DirectCast(GrdEmailText.Rows(0).FindControl("LinkView"), LinkButton).Visible = False
            Else
                GrdEmailText.DataSource = ds
                GrdEmailText.DataBind()

            End If

            If GrdEmailText.Rows.Count > 1 Then

                'DirectCast(GrdEmailText.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
                'DirectCast(GrdEmailText.HeaderRow.FindControl("Txt2"), TextBox).Text = Txt2
                'DirectCast(GrdEmailText.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3


                For Each row As GridViewRow In GrdEmailText.Rows
                    Dim val As String = DirectCast(row.FindControl("HiddenFieldId"), HiddenField).Value
                    Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

                    Dim GrdAttachment As GridView = DirectCast(row.FindControl("GrdAttachment"), GridView)
                    Dim d As New DirectoryInfo(serverpath + val + "/Attachments/")
                    GrdAttachment.DataSource = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    GrdAttachment.DataBind()

                    Dim lnkdeletedir As ImageButton = DirectCast(row.FindControl("lnkdelete"), ImageButton)
                    lnkdeletedir.CommandArgument = val

                    For Each arow As GridViewRow In GrdAttachment.Rows
                        Dim lnkattachment As LinkButton = DirectCast(arow.FindControl("lnkattachment"), LinkButton)
                        lnkattachment.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
                        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkattachment)
                        Dim lnkdelete As LinkButton = DirectCast(arow.FindControl("lnkdelete"), LinkButton)
                        lnkdelete.CommandArgument = serverpath + val + "/Attachments/" + lnkattachment.Text.Trim()
                    Next

                Next
            End If
            'lblmessage.Text = "Page Refreshed..."
            lnkreports.Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "masscomeditnews")
            'lblmessage.Text = ex.ToString
        End Try
    End Sub
    Protected Sub GrdTextAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdEmailText.RowCommand
        If e.CommandName = "select" Then
            Dim path = e.CommandArgument.ToString()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If
        If e.CommandName = "Remove" Then
            Dim f As New FileInfo(e.CommandArgument)
            If f.Exists Then
                f.Delete()
            End If
            BindEmailText()
            lblmessage.Text = "Attachment Deleted"
            BindEmailText()
        End If
        If e.CommandName = "search" Then
            BindEmailText()
        End If


    End Sub
    Protected Sub GrdEmailText_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdEmailText.RowCommand
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        If e.CommandName = "sendmail" Then
            divAge.Visible = True
            'divAge.Style.Add("display", "block")
            grpemailtemplateid.Value = e.CommandArgument
            TR1.Attributes.Add("style", "display:none")
            TR2.Attributes.Add("style", "display:none")
            TR3.Attributes.Add("style", "display:grid")
            popupmsg.Text = ""
            grptemplateid.Value = ""
            exltemplateid.Value = ""
            lnkreports.Visible = False

            Dim tmplt_title As String = ""
            Dim dt5 As DataTable = GET_EMAIL_SMS_TEMPLATES(1, grpemailtemplateid.Value)

            For Each row2 As DataRow In dt5.Rows
                If tmplt_title = "" Then
                    tmplt_title = row2.Item("EML_TITLE")
                Else
                    tmplt_title = tmplt_title + " || " + row2.Item("EML_TITLE")
                End If


            Next
            popuptitlemsg.Text = "Selected Newsletter Templates title is: " + tmplt_title 'grptemplateid.Value

        End If
        If e.CommandName = "attachment" Then
            lnkreports.Visible = False
            'If CheckActive(e.CommandArgument) Then
            '    Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
            '    Dim str_query = "UPDATE COM_MANAGE_EMAIL SET EML_DELETED='True' where EML_ID='" & e.CommandArgument & "'"
            '    SqlHelper.ExecuteNonQuery(str_conn.ToString, CommandType.Text, str_query)
            '    Dim path As String = (serverpath + e.CommandArgument)

            '    If System.IO.Directory.Exists(path) Then
            '        Try
            '            System.IO.Directory.Delete(path, True)
            '        Catch ex As Exception

            '        End Try

            '    End If


            '    lblmessage.Text = "Template Deleted"
            '    BindEmailText()
            'Else
            '    lblmessage.Text = "Template cannot be deleted. This template has been sent."
            'End If
            'Response.Redirect("TabPages/comAssignGroupsPlainText.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view"), False)


        End If

        If e.CommandName = "Deleting" Then
            lnkreports.Visible = False
            If CheckActive(e.CommandArgument) Then
                Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
                Dim str_query = "UPDATE COM_MANAGE_EMAIL SET EML_DELETED='True' where EML_ID='" & e.CommandArgument & "'"
                SqlHelper.ExecuteNonQuery(str_conn.ToString, CommandType.Text, str_query)
                Dim path As String = (serverpath + e.CommandArgument)

                If System.IO.Directory.Exists(path) Then
                    Try
                        System.IO.Directory.Delete(path, True)
                    Catch ex As Exception

                    End Try

                End If


                lblmessage.Text = "Template Deleted of ID " + e.CommandArgument
                BindEmailText()
            Else
                lblmessage.Text = "Template of ID " + e.CommandArgument + " cannot be deleted. This template has been sent."
            End If
            'Response.Redirect("TabPages/comAssignGroupsPlainText.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view"), False)


        End If
        If e.CommandName = "Editing" Then
            HiddenFieldTemplateid.Value = e.CommandArgument
            h_MSG.Value = ""
            lnkreports.Visible = False
            'If CheckActive(e.CommandArgument) Then
            '    Panel3.Visible = False
            '    Panel4.Visible = True
            '    tr_host.Visible = False
            '    tr_port.Visible = False
            '    tr_username.Visible = False
            '    tr_password.Visible = False

            '    HiddenFieldTemplateid.Value = e.CommandArgument
            '    Dim str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & e.CommandArgument & "'"
            '    Dim ds As DataSet
            '    ds = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)
            '    txtEditEmailText.Content = ds.Tables(0).Rows(0).Item("EML_BODY").ToString()
            '    txtFrom.Text = ds.Tables(0).Rows(0).Item("EML_FROM").ToString()
            '    txtTitle.Text = ds.Tables(0).Rows(0).Item("EML_TITLE").ToString()
            '    txtsubject.Text = ds.Tables(0).Rows(0).Item("EML_SUBJECT").ToString()
            '    txtdisplay.Text = ds.Tables(0).Rows(0).Item("EML_DISPLAY").ToString()
            '    txthost.Text = ds.Tables(0).Rows(0).Item("EML_HOST").ToString()
            '    txtport.Text = ds.Tables(0).Rows(0).Item("EML_PORT").ToString()
            '    txtusername.Text = ds.Tables(0).Rows(0).Item("EML_USERNAME").ToString()
            '    HiddenPassword.Value = ds.Tables(0).Rows(0).Item("EML_USERNAME").ToString()
            '    txtpassword.Text = ds.Tables(0).Rows(0).Item("EML_PASSWORD").ToString()
            '    If (Convert.ToString(ds.Tables(0).Rows(0).Item("isCriticalEmail")) = "2") Then
            '        ddlPriority.SelectedItem.Value = "2"


            '    Else
            '        ddlPriority.SelectedItem.Value = "1"
            '    End If
            '    Dim mergeid = ds.Tables(0).Rows(0).Item("EML_MERGE_ID").ToString()

            '    If mergeid <> "" Then
            '        ddtemplate.SelectedValue = mergeid
            '        Fields()
            '    End If

            '    lblmessage.Text = "You can edit the Email text and add new attachments."
            'Else
            '    lblmessage.Text = "You cannot edit this template.Message has been sent."
            'End If

        End If
    End Sub
    Public Function CheckActive(ByVal id As String) As Boolean
        Dim returnvalue As Boolean = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select count(*) from COM_LOG_EMAIL_TABLE (NoLOCK) where LOG_EML_ID='" & id & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If val = 0 Then
            returnvalue = True
        End If

        If Session("sBusper") = "True" Then
            returnvalue = True
        End If

        Return returnvalue
    End Function

    Protected Sub btneditplaintextsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btneditplaintextsave.Click
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim attachment = False
            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
            Dim val As String = HiddenFieldTemplateid.Value
            Dim save = 1
            If FileUploadEditEmailtext.HasFile Then
                Dim filen As String()
                Dim FileName As String = FileUploadEditEmailtext.PostedFile.FileName
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)
                FileUploadEditEmailtext.SaveAs(serverpath + val + "/Attachments/" + FileName)

                If CheckFileSize(serverpath + val + "/Attachments/" + FileName, val) Then
                    save = 1
                Else
                    save = 0
                End If
                attachment = True
            End If

            Dim pParms(15) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EML_ID", val)
            pParms(1) = New SqlClient.SqlParameter("@EML_NEWS_LETTER", "False")
            pParms(2) = New SqlClient.SqlParameter("@EML_BODY", txtEditEmailText.Content)
            If attachment = True Then
                pParms(3) = New SqlClient.SqlParameter("@EML_ATTACHMENT", attachment)
            End If
            pParms(4) = New SqlClient.SqlParameter("@OPTION", 2)
            pParms(5) = New SqlClient.SqlParameter("@EML_FROM", txtFrom.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@EML_TITLE", txtTitle.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@EML_SUBJECT", txtsubject.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@EML_DISPLAY", txtdisplay.Text.Trim())

            pParms(9) = New SqlClient.SqlParameter("@EML_USERNAME", txtusername.Text.Trim())
            If txtpassword.Text.Trim() <> "" Then
                pParms(10) = New SqlClient.SqlParameter("@EML_PASSWORD", txtpassword.Text.Trim())
            Else
                pParms(10) = New SqlClient.SqlParameter("@EML_PASSWORD", HiddenPassword.Value)
            End If

            If (ddlPriority.SelectedItem.Value = "2") Then
                Dim criticalEmailPath As String = WebConfigurationManager.AppSettings("CriticalEmailHost").ToString
                pParms(11) = New SqlClient.SqlParameter("@EML_HOST", criticalEmailPath.ToString)
            Else
                pParms(11) = New SqlClient.SqlParameter("@EML_HOST", txthost.Text.Trim())
            End If

            pParms(12) = New SqlClient.SqlParameter("@EML_PORT", txtport.Text.Trim())

            If ddtemplate.SelectedIndex > 0 Then
                pParms(13) = New SqlClient.SqlParameter("@EML_MERGE_ID", ddtemplate.SelectedValue)
            End If
            If (ddlPriority.SelectedItem.Value = "2") Then
                pParms(145) = New SqlClient.SqlParameter("@EML_CRITICAL", 1)
            Else
                pParms(14) = New SqlClient.SqlParameter("@EML_CRITICAL", 0)
            End If
            If save = 1 Then
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_UPDATE", pParms)


                txtEditEmailText.Content = ""
                lblmessage.Text = "Newsletter Template Updated"
                BindEmailText()
                Panel3.Visible = True
                Panel4.Visible = False
            End If

        Catch ex As Exception

        End Try

    End Sub
    Public Function CheckFileSize(ByVal path As String, ByVal templateid As String) As Boolean
        Dim val = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pVal As String = 1
        Dim str_query = "SELECT P_VAL FROM COM_PARAMETER WHERE ID= " & pVal

        Dim f2 = New FileInfo(path)
        Dim len = f2.Length
        Dim mb = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If len > (mb * 1024 * 1024) Then
            val = False
            lblmessage.Text = "File size must be less than " & mb & "MB."
            File.Delete(path)
        End If


        Return val
    End Function

    Protected Sub btnplaintextcancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnplaintextcancel.Click
        Panel3.Visible = True
        Panel4.Visible = False
        lblmessage.Text = ""
    End Sub


    Protected Sub GrdEmailText_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdEmailText.PageIndexChanging
        GrdEmailText.PageIndex = e.NewPageIndex
        BindEmailText()

    End Sub


    Protected Sub ddtemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtEditEmailText.Content = ""
        Fields()
    End Sub

    Protected Sub btninsert_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtEditEmailText.Content = txtEditEmailText.Content & ddfields.SelectedValue

    End Sub

    'Protected Sub RadioSend_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles RadioSend.SelectedIndexChanged

    '    If RadioSend.SelectedValue = "1" Then
    '        'TR1.Visible = False
    '        divAge.Visible = False
    '        RadioSend.Attributes.Add("onclick", "javascript: CreateAssignGroup(); return false;")
    '    Else
    '        'TR1.Visible = True
    '        divAge.Visible = False
    '        RadioSend.Attributes.Add("onclick", "javascript: AssignGroup(); return false;")
    '    End If
    'End Sub
    'Protected Sub rbHigher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles rbHigher.SelectedIndexChanged

    '    'divAge.Visible = False
    '    'divAge.Style.Add("display", "none")

    'End Sub
    'Protected Sub rbLower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles rbLower.SelectedIndexChanged

    '    'divAge.Visible = False
    '    'divAge.Style.Add("display", "none")

    'End Sub
    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False
        'divAge.Style.Add("display", "none")
    End Sub
    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divAge.Visible = False
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Dim k As Boolean = checkdatetime(txtdate.Text, ddhour.SelectedValue, ddmins.SelectedValue)
        If k = True Then
            If grp_exl_btn_sel_id.Value = 1 Then

                '''''groupemailapprovalentry()
                btngrpemailok_Click()
                TR1.Attributes.Add("style", "display:none")
                TR2.Attributes.Add("style", "display:none")
                TR3.Attributes.Add("style", "display:grid")
                lblmessage.Text = "Schedule has been successfully done"
                lnkreports.Visible = True
            ElseIf grp_exl_btn_sel_id.Value = 2 Then
                btnexlemailok_Click()
                TR1.Attributes.Add("style", "display:none")
                TR2.Attributes.Add("style", "display:none")
                TR3.Attributes.Add("style", "display:grid")
                lblmessage.Text = "Schedule has been successfully done"
                lnkreports.Visible = True
            End If

            divAge.Visible = False

        Else
            'lblmessage.Text = "Selected Date and Time should be greater than Current Date and Time"
            popuperrmsg.Text = "Selected Date and Time should be greater than Current Date and Time"
        End If


    End Sub

    Public Function checkdatetime(ByRef p_date As String, ByRef p_hour As String, ByRef p_minute As String) As Boolean
        Dim c_date As String
        Dim ps_date As String
        ps_date = p_date + " " + p_hour + ":" + p_minute
        c_date = DateTime.Now.ToString("dd/MM/yyyy HH:mm")

        Dim pass_date As DateTime = DateTime.ParseExact(ps_date, "dd/MMM/yyyy HH:mm", Nothing)
        Dim cur_date As DateTime = DateTime.ParseExact(c_date, "dd/MM/yyyy HH:mm", Nothing)

        If pass_date > cur_date Then
            Return True

        Else
            Return False
        End If

    End Function

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    'excel email schedule
    Protected Sub btnexlemailok_Click()
        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        'If hours < 0 Then

        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EML_ID", grpemailtemplateid.Value)
        pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", "Using Previous Template") '' We are taking data log id data
        pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
        pParms(3) = New SqlClient.SqlParameter("@CSE_ID", 0)
        pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
        pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
        pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
        pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", exltemplateid.Value)

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_SCHEDULE_INSERT", pParms)
        'lblmessage.Text = "Schedule has been successfully done"
        'lblUerror.Text = "Schedule has been successfully done"
        txtdate.Text = ""
        'Else
        ''lblsmessage.Text = "Date time is past"

        'End If


    End Sub

    'grup email approaval entry
    Protected Sub groupemailapprovalentry()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CSE_CGR_ID", grptemplateid.Value)
        pParms(1) = New SqlClient.SqlParameter("@CSE_EML_ID", grpemailtemplateid.Value)
        pParms(2) = New SqlClient.SqlParameter("@CSE_BSU_ID", Hiddenbsuid.Value) ''---------------------|  (grpbsuid)
        pParms(3) = New SqlClient.SqlParameter("@CSE_TYPE", "PT")
        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COM_SEND_EMAIL_INSERT", pParms)


    End Sub

    'group email
    Protected Sub btngrpemailok_Click()

        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        'If hours < 0 Then


        Dim str_Data0 As String
        Dim strArr0() As String
        str_Data0 = grptemplateid.Value '"6||A||4"
        strArr0 = str_Data0.Split("||")

        If strArr0.Length > 0 Then
            For i As Int16 = 0 To strArr0.Length - 1
                If strArr0(i).ToString <> "" AndAlso IsNumeric(strArr0(i).ToString) Then
                    'System.Console.WriteLine("--" & strArr0(i))

                    Dim pParms(9) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EML_ID", grpemailtemplateid.Value) 'selected email template id
                    pParms(1) = New SqlClient.SqlParameter("@CGR_ID", strArr0(i)) 'selected email group id
                    pParms(2) = New SqlClient.SqlParameter("@CSE_TYPE", "PT")
                    pParms(3) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
                    pParms(4) = New SqlClient.SqlParameter("@EXCEL_PATH", "")
                    pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value) 'selected bsu id
                    pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
                    pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", "")
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.COM_AUTO_APPROVED_GC_NEWSLETTER", pParms)

                End If
            Next
        End If



        'Dim ds As DataSet
        'Dim str_query = "select CSE_CGR_ID,CSE_EML_ID from COM_SEND_EMAIL where CSE_ID='" & grptemplateid.Value & "'"
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'If ds.Tables(0).Rows.Count > 0 Then
        '    HiddenCGR_ID = ds.Tables(0).Rows(0).Item("CSE_CGR_ID").ToString()
        '    HiddenEML_ID = ds.Tables(0).Rows(0).Item("CSE_EML_ID").ToString()

        '    str_query = "select * from COM_MANAGE_EMAIL_SCHEDULE where CSE_ID='" & grptemplateid.Value & "'"
        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        lblmessage.Text = "You have already scheduled this template"
        '        lblUerror.Text = "You have already scheduled this template"
        '    Else
        '        Dim pParms(6) As SqlClient.SqlParameter
        '        pParms(0) = New SqlClient.SqlParameter("@EML_ID", HiddenEML_ID)
        '        pParms(1) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
        '        pParms(2) = New SqlClient.SqlParameter("@CSE_ID", grptemplateid.Value)
        '        pParms(3) = New SqlClient.SqlParameter("@CGR_ID", HiddenCGR_ID)
        '        pParms(4) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
        '        pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))

        '        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_SCHEDULE_INSERT", pParms)
        '        lblmessage.Text = "Schedule has been successfully done"
        '        lblUerror.Text = "Schedule has been successfully done"
        '    End If

        'End If


        'Else
        ''lblsmessage.Text = "Date time is past"

        'End If


    End Sub
    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub
End Class
