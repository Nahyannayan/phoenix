﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comMergerDocument.aspx.vb" Inherits="masscom_comMergerDocument" %>

<%@ Register Src="UserControls/comMergerDocument.ascx" TagName="comMergerDocument" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Merge Document</title>

    <!-- Bootstrap core CSS-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <%--<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>

    <!-- Bootstrap header files ends here -->


    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <base target="_self" />

</head>
<script type="text/javascript">

    window.setTimeout('setpath()', 100);

    function setpath() {
        var path = window.location.href
        var Rpath = path.substring(path.indexOf('?'), path.length)
        var objFrame


        objFrame = document.getElementById("FEx2");
        objFrame.src = "comMergerDocumentView.aspx" + Rpath


    }

</script>
<body class="m-3">
    <form id="form1" runat="server">
        <div >
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0" >
                <ajaxToolkit:TabPanel ID="HT1" runat="server">
                    <ContentTemplate>

                        <uc1:comMergerDocument ID="comMergerDocument1" runat="server" />

                    </ContentTemplate>
                    <HeaderTemplate>
                        Upload Merge Document
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="HT2" runat="server">
                    <ContentTemplate>
                        <iframe id="FEx2" height="600" scrolling="auto" marginwidth="0px" frameborder="0" width="750"></iframe>

                    </ContentTemplate>
                    <HeaderTemplate>
                        View Merge Document
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>


        </div>
    </form>
</body>
</html>
