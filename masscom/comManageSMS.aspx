<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comManageSMS.aspx.vb" MasterPageFile="~/mainMasterPage.master"
    Inherits="masscom_comManageSMS" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 25%;
            top: 40%;
            position: fixed;
            width: 50%;
        }
          .gridrow_bold {
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />


    <script language="javascript">

        function openWindow() {
            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            window.open('comSendingReports.aspx' + Rpath + "&tabid=0", '_self');
            return false;
        }

        function openview(val) {
            window.open('comPlainTextView.aspx?temp_id=' + val);
            return false;

        }

        function openPopup(strOpen) {
            //alert(2);

            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 600px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";


            //var result;
            //result = window.showModalDialog(strOpen, "", sFeatures);
            //if (result == "1") {
            //    window.location.reload(true);
            //}
            return ShowWindowWithClose(strOpen, 'search', '75%', '85%')
            //return false;
        }


        //create a new email template
        function CreatePlainText() {
            //alert(id);
            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            // var page_url = "comCreateSmsText.aspx" + "?mnu='" + $("#<%=hfMenuCode.ClientID%>").val + "'";
            var page_url = "comCreateSmsText.aspx?mnu=M000040";
            //M000040
            return ShowWindowWithClose(page_url, 'search', '75%', '85%')
            return false;

        }

        //after creating a new email template
        function setCloseValue(msg) {

            CloseFrame();
            $("#<%=lblbtn.ClientID%>").click();

            $("#<%=h_MSG.ClientID%>").val(msg);
            __doPostBack('<%=h_MSG.ClientID%>', "");
        }

        //assign group popup back button
        function setCloseGroupValue() {
            //alert('x');
            CloseFrame();

        }
        //general close frame used in edit template
        function setCloseFrame() {
            //alert('x');
            CloseFrame();
        }
        //close edit template popup
        function setCloseEditTemplateValue(msg) {

            CloseFrame();
            $("#<%=h_MSG.ClientID%>").val(msg);
            __doPostBack('<%=h_MSG.ClientID%>', "");
        }

        //close new group popup after save
        function setCloseNewGRPValue(msg) {

            CloseFrame();
            $("#<%=h_MSG2.ClientID%>").val(msg);
            __doPostBack('<%=h_MSG2.ClientID%>', "");
        }

        //group selection return value
        function setValue(id) {

            CloseFrame();
           <%-- document.getElementById('<%=TR3.ClientID  %>').style.display = "none";
            document.getElementById('<%=TR1.ClientID  %>').style.display = "grid";
            document.getElementById('<%=TR2.ClientID  %>').style.display = "grid";
            document.getElementById('<%=grptemplateid.ClientID  %>').value = NameandCode[0]--%>
            $("#<%=grptemplateid.ClientID%>").val(id);
            __doPostBack('<%=grptemplateid.ClientID%>', "");

        }


        //email excel file selection return value
        function setSelExcelFileValue(id) {
            //alert(id);
            CloseFrame();
            $("#<%=exltemplateid.ClientID%>").val(id);
            __doPostBack('<%=exltemplateid.ClientID%>', "");

        }       

        function CloseFrame() {
            jQuery.fancybox.close();
        }




        //edit the selected template
        function EditTemplate(id) {

            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            // var page_url = "comViewSmsText.aspx?mnu='" + $("#<%=hfMenuCode.ClientID%>").val + "'&editCode=1&cmsid=" + id;
            var page_url = "comViewSmsText.aspx?mnu=M000040&editCode=1&cmsid=" + id;
            return ShowWindowWithClose(page_url, 'search', '75%', '85%')
            return false;

        }

        //send the email by assigning group
        function AssignGroup(id) {
            document.getElementById('<%=grp_exl_btn_sel_id.ClientID  %>').value = 1
            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            var page_url = "TabPages/comAssignGroupsSelection.aspx" + Rpath  //+"&SELTEMPLT="+id;
            return ShowWindowWithClose(page_url, 'search', '75%', '85%')
            return false;

        }

        //create a new email group
        function CreateAssignGroup() {

            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            //var page_url = "comCreateAssignGroups.aspx" + "?MainMnu_code=kuz/F%20JuQII=&datamode=bW5AEI9plJ4=";
            var page_url = "comNewListGroups.aspx" + "?MainMnu_code=kuz/F%20JuQII=&datamode=bW5AEI9plJ4=";
            return ShowWindowWithClose(page_url, 'search', '75%', '85%')
            return false;

        }

        //send the email by assigning excel upload or previous excel template
        function AssignExcel() {
            document.getElementById('<%=grp_exl_btn_sel_id.ClientID  %>').value = 2
            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            //var page_url = "comExcelSendSelection.aspx" + "?MainMnu_code=DxlxayjdDu8=&datamode=Zo4HhpVNpXc=";
            var page_url = "comExcelSMSPopup.aspx" + "?MainMnu_code=hD1cncIiAMM=&datamode=Zo4HhpVNpXc=";
            return ShowWindowWithClose(page_url, 'search', '75%', '85%')
            return false;

        }


    </script>
    <script type="text/javascript">

        function opneWindow() {

            var sFeatures;
            sFeatures = "dialogWidth: 900px; ";
            sFeatures += "dialogHeight: 700px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var strOpen = "../comMergerDocument.aspx?Type=SMS"

            var result;
            result = window.showModalDialog(strOpen, "", sFeatures);


        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-email mr-3"></i>SMS Services
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>



                <table width="100%">
                    <tr>
                        <td align="left" width="10%">
                            <button id="btn_newemail" class="btn btn-primary button" onclick="CreatePlainText(); return false;"><i class="fa fa-fw fa-envelope"></i>New SMS</button></td>
                        <td align="left" width="10%">
                            <button id="btn_assign_grp" class="btn btn-primary button" onclick="CreateAssignGroup(); return false;"><i class="fa fa-fw fa-edit"></i>View/Create Groups</button></td>
                        <td align="left" width="30%"><span class="field-label">Search</span>
                            <asp:TextBox ID="Txt_SMSTitle" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search" OnClick="BindGrid" ImageUrl="~/Images/forum_search.gif" /></td>

                        <td align="left" width="10%">
                            <asp:LinkButton ID="Linkmerge" OnClientClick="javascript:opneWindow();return false;" CausesValidation="false" Visible="false" runat="server">Upload Merge Document</asp:LinkButton>
                        </td>
                        <td width="40%" align="right">
                             <asp:LinkButton ID="lnkreports" OnClientClick="javascript:openWindow();" runat="server" Visible="false">Please click here to move to reports page</asp:LinkButton>
                            <%--<span class="float-left">Emails in Queue</span> <span class="float-right">(55% of the daily capacity)</span><br />
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 55%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">55%</div>
                            </div>--%>
                        </td>
                    </tr>
                </table>
                <div class="mb-2"></div>


                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg">SMS Templates
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridMSMS" AutoGenerateColumns="false" Width="100%" runat="server"
                                AllowPaging="True" EmptyDataText="No SMS templates added yet (or) Search query did not produce any results"
                                OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="20" OnRowCommand="GridMSMS_RowCommand" CssClass="table table-bordered table-row table-striped">
                                <Columns>
                                    <%--  <asp:TemplateField HeaderText="Sl No">
                                        <HeaderTemplate>
                                            Sl No--%>
                                    <%--   <br />
                                           <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />--%>
                                    <%-- </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="Hiddenid" Value='<%# Eval("CMS_ID") %>' runat="server" />
                                            <center>
                                            <%# Eval("CMS_ID") %></center>
                                        </ItemTemplate>

                                    </asp:TemplateField>--%>
                                    <%-- <asp:TemplateField HeaderText="Create Date">
                                        <HeaderTemplate>
                                            Create Date--%>
                                    <%-- <br />
                                            <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                                TargetControlID="Txt2">
                                            </ajaxToolkit:CalendarExtender>--%>
                                    <%-- </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                           </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="SMS Text">
                                        <HeaderTemplate>
                                            Sms Text
                                                   <%-- <br />
                                            <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>' Visible="false"></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T12Panel1" runat="server">
                                                <%#Eval("cms_sms_text_1")%>
                                                <br />
                                                <span class="text-left">
                                                    <asp:HiddenField ID="Hiddenid" Value='<%# Eval("CMS_ID") %>' runat="server" />

                                                  <span class="gridrow_bold"> ID :</span>&nbsp;  <%# Eval("CMS_ID") %>
                                                 <span class="gridrow_bold"> Date :</span>&nbsp;   <%#Eval("CMS_DATE", "{0:dd/MMM/yyyy}")%></span>
                                            </asp:Panel>
                                            <%--  <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                TextLabelID="T12lblview">
                                            </ajaxToolkit:CollapsiblePanelExtender>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="View">
                                        <HeaderTemplate>
                                            <center>  Options</center>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                              
                                               <%-- <br />--%>
                                   <a href="javascript:openPopup('comViewSmsText.aspx?editCode=0&cmsid=<%# Eval("CMS_ID") %>&mnu=<%# Me.hfMenuCode.Value%>') " data-toggle="tooltip" title="view">
                                            <asp:Image ID="Image1" ImageUrl="~/Images/email-view.png" runat="server" style="display:none;"/> </a>
                                                <asp:ImageButton ID="lnkEdit" runat="server" CausesValidation="false" OnClientClick='<%# Eval("EditTemplate")%>' CommandArgument='<%# Eval("cms_id") %>'
                                                    CommandName="Editing" ImageUrl="~/Images/email-edit.png"  data-toggle="tooltip" title="edit"></asp:ImageButton>
                                            
                                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="false" CommandArgument='<%# Eval("cms_id")%>'
                                                    CommandName="sendmail" ImageUrl="~/Images/email-send.png" data-toggle="tooltip" title="send"></asp:ImageButton>
                                    </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <EmptyDataRowStyle />
                                <EditRowStyle />
                            </asp:GridView>

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                <asp:HiddenField ID="hfMenuCode" runat="server" />

                <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <%--style="display:none;"--%>
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: black; border: 1px solid black; border-radius: 10px 10px; background-color: lightgray;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="CENTER" class="title-bg-lite">
                                    <asp:Label ID="lblpnltitle" runat="server" EnableViewState="True">SMS Send Option Selection</asp:Label>
                                </div>
                                <div align="CENTER">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:Label ID="popuptitlemsg" runat="server" CssClass="error" EnableViewState="true"></asp:Label>
                                </div>


                                <table align="center" width="100%" cellpadding="2" cellspacing="0">
                                    <tr id="TR3" runat="server" style="display: table; width: 100%;">
                                        <td align="center" width="100%">
                                            <table align="center" width="100%" cellpadding="2" cellspacing="0">
                                                <tr>
                                                    <td align="center" width="100%"><span class="field-label">How would you like to send?</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" width="100%">
                                                        <%-- <asp:RadioButtonList ID="RadioSend" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioSend_SelectedIndexChanged"
                                                AutoPostBack="True">
                                                <asp:ListItem  Text=" <span class='field-label'>Using Groups</span>" Value="1"></asp:ListItem>
                                                <asp:ListItem Text=" <span class='field-label'>Using Excel Upload</span>" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>--%>
                                                        <%--<asp:RadioButton ID="rbHigher" runat="server" GroupName="ro" Text="<span class='field-label'>Using Groups</span>"
                                                AutoPostBack="True" OnCheckedChanged="rbHigher_SelectedIndexChanged" />
                                            <asp:RadioButton ID="rbLower" runat="server" GroupName="ro"
                                                Text="<span class='field-label'>Using Excel Upload</span>" AutoPostBack="True" OnCheckedChanged="rbLower_SelectedIndexChanged" />--%>
                                                        <asp:Button ID="Btn_grp" Text="Using Groups" CssClass="button" runat="server" />
                                                        <asp:Button ID="Btn_exl" Text="Using Excel Upload" CssClass="button" runat="server" />

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="TR1" runat="server" style="display: none">
                                        <td colspan="2">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="20%">
                                                        <span class="field-label">Date</span>
                                                    </td>
                                                    <td align="left" width="40%">
                                                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="ss"></asp:TextBox>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                                            Format="dd/MMM/yyyy" PopupButtonID="Image1" TargetControlID="txtdate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </td>
                                                    <td align="left" width="40%"></td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <span class="field-label">Time</span>
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddhour" runat="server">
                                                        </asp:DropDownList></td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddmins" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="TR2" runat="server" style="display: none">
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnUpdate" Text="Send" CssClass="button" runat="server" />
                                            <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="left" colspan="4">
                                            <asp:Label ID="popuperrmsg" runat="server" Text="" EnableViewState="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <asp:Label ID="popupmsg" runat="server" Text="" EnableViewState="true"></asp:Label>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>


   
    <asp:HiddenField ID="exltemplateid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="grptemplateid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="grpemailtemplateid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="grp_exl_btn_sel_id" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HiddenFieldTemplateid" runat="server" />
    <asp:Button ID="lblbtn" runat="server" Style="display: none" />
    <asp:HiddenField ID="h_MSG" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="h_MSG2" runat="server" EnableViewState="true" />
    <script type="text/javascript" lang="javascript">
              function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                  $.fancybox({
                      type: 'iframe',
                      //maxWidth: 300,
                      href: gotourl,
                      //maxHeight: 600,
                      fitToView: true,
                      padding: 6,
                      width: w,
                      height: h,
                      autoSize: false,
                      openEffect: 'none',
                      showLoading: true,
                      closeClick: true,
                      closeEffect: 'fade',
                      'closeBtn': true,
                      afterLoad: function () {
                          this.title = '';//ShowTitle(pageTitle);
                      },
                      helpers: {
                          overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                          title: { type: 'inside' }
                      },
                      onComplete: function () {
                          $("#fancybox-wrap").css({ 'top': '90px' });

                      },
                      onCleanup: function () {
                          var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                          if (hfPostBack == "Y")
                              window.location.reload(true);
                      }
                  });

                  return false;
              }
    </script>



</asp:Content>
