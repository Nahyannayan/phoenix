﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="comResources.aspx.vb" Inherits="masscom_comResources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">


        function ValidateFileUploadExtension(Source, args) {
            var fupData = document.getElementById('<%= FileUploadControl.ClientID%>');
            var FileUploadPath = fupData.value;
            
            if (FileUploadPath == '') {
                // There is no file selected
               alert(1)
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "gif" || Extension == "jpeg" || Extension == "jpg" || Extension == "png" ) {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
</script>

    <style>
        .text-bold {
    font-weight: 600 !important;
}
        table td input[type=image] {
            border-radius:4px !important;
        }
        .grid-thumbnail {
    height: 50px;
    width: 50px;
    border: 2px solid #8dc24c !important;
    border-radius: 4px !important;
    max-height: 50px !important;
    overflow: hidden !important;
}
    </style>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>   Resources Category
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblError" runat="server"   CssClass="error"></asp:Label></td>
                    </tr>
                </table>
                <table width="100%">
                    
            <tr>
                <%--<asp:Label ID="lblmandatory" runat="server" text="* Mandatory Fields" ForeColor="red" ></asp:Label>--%>
            </tr>
                    <tr>
                          <td  align="left" width="20%"><span class="field-label">Category</span><asp:Label ID="lblasterick" runat="server" text="*" cssclass="text-danger" ></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCategory" runat="server" ></asp:TextBox><br />
                           <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator1" controltovalidate="txtCategory" errormessage="Enter a category" validationgroup="preview"/>  
                                   
                            </td>
                        
                          <td  align="left" width="20%"><span class="field-label">Thumbnail Image</span><asp:Label ID="Label1" runat="server" text="*" cssclass="text-danger" ></asp:Label></td>
                        <td align="left" width="30%">
                       <asp:UpdatePanel ID="updpnl" runat="server">
                                                            <ContentTemplate>
                                                                
                            <asp:FileUpload id="FileUploadControl" runat="server" />
                                                                 <asp:Image ID="ImgThumbnail" runat="server" Height="50px" Width="50px" Visible="false" />
                                                              <br/> <asp:Label ID="lblmsgformat" runat="server" text="File should be JPEG/PNG format  " cssclass="text-warning" ></asp:Label>
                                                                <br/><asp:Label ID="lblmsgsize" runat="server" text="File size be less than 50KB   " cssclass="text-warning" ></asp:Label>
                               <asp:Button runat="server" id="UploadButtonbtn" text="Upload"   visible="false"/>
                            <asp:LinkButton ID="lnkUpload" runat="server" Visible="false">Upload</asp:LinkButton><br />
                                                                <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" controltovalidate="FileUploadControl" errormessage="Please Upload File" validationgroup="preview"/>  
                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$"
    ControlToValidate="FileUploadControl" runat="server" ForeColor="Red" ErrorMessage="Please select a valid jpg or png File file."
    Display="Dynamic" />    
                            </td>
                         
                       
                    </tr>
                <tr style="display:none;">
                    <td  align="left" width="20%" ><span class="field-label">Active</span></td>
                    <td  align="left" width="30%"><asp:CheckBox ID="chkActive" runat="server"  Visible="false"/></td>
                 <td></td>
                        <td></td>
                </tr>
                  <tr>
                                    <td align="center"  colspan="5">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"  validationgroup="preview"  />&nbsp;&nbsp;<asp:Button ID="btndownload" runat="server" CssClass="button" Text="download"  visible="false"  />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  />
                                         </td>
                         </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSave" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                       <td></td>
                        <td></td><td></td>
                                </tr><tr>
                                                           
                                        <td align="center" colspan="4"  valign="top">
                                        <asp:GridView ID="gvResourcesCategory" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"  HorizontalAlign="Center" />
                                          
                                            <Columns>

                                         

                                                <asp:TemplateField HeaderText="SNO" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRcatID" runat="server" Text='<%# Bind("RCM_ID")%>' visible="false"></asp:Label>
                                                        <asp:Label ID="lblSNO" runat="server" Text='<%# Bind("SNo")%>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Category" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("RCM_DESCR")%>' align="left"></asp:Label>
                                                        <asp:Label ID="lblResourceCategorypath" runat="server" Text='<%# Eval("RCM_THUMBNAIL_PATH")%>' visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"  CssClass="text-bold" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Image">
                                                    <ItemTemplate>
                                                     
                                                         <asp:UpdatePanel ID="updpnl" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblnotavli" runat="server" Text="No Image" visible="false"></asp:Label>
                                                        <asp:ImageButton ID="Imgbtn" runat="server" AlternateText="Download"  OnClick="Imgbtn_Click" CssClass="grid-thumbnail" data-toggle="tooltip" Title="Click To Download"
             />
                                                                 </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="Imgbtn" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:HiddenField ID="hfFileName" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusACTIVE")%>'  align="left"></asp:Label>
                                                      
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Created On" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Bind("RCM_CREATED_DT", "{0:dd/MMM/yyyy}")%>'  align="left"></asp:Label>
                                                      
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               
                                                     <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkBtnEdit" text="Edit" runat="server" align="center" onclick="lnkBtnEdit_Click" ></asp:LinkButton>
                                                   
                                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" align="center" OnClick="lnkbtnDelete_Click" Text="Delete"></asp:LinkButton> </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                    </td>

                                    </tr>
                </table>
               <script type="text/javascript" lang="javascript">
                   function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                       $.fancybox({
                           type: 'iframe',
                           //maxWidth: 300,
                           href: gotourl,
                           //maxHeight: 600,
                           fitToView: true,
                           padding: 6,
                           width: w,
                           height: h,
                           autoSize: false,
                           openEffect: 'none',
                           showLoading: true,
                           closeClick: true,
                           closeEffect: 'fade',
                           'closeBtn': true,
                           afterLoad: function () {
                               this.title = '';//ShowTitle(pageTitle);
                           },
                           helpers: {
                               overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                               title: { type: 'inside' }
                           },
                           onComplete: function () {
                               $("#fancybox-wrap").css({ 'top': '90px' });
                           },
                           onCleanup: function () {
                               var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                               if (hfPostBack == "Y")
                                   window.location.reload(true);
                           }
                       });

                       return false;
                   }
    </script>
            </div>
        </div>
        <%--<uc2:usrMessageBar runat="server" ID="usrMessageBar" />--%>
    </div>
  
</asp:Content>

