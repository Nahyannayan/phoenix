﻿// JScript File

    var XMLHTTPRequestObject =false;
    var timeout;

    
////    if(window.XMLHttpRequest)
////    {
////    XMLHTTPRequestObject =new XMLHTTPRequest();
////    }
////   
 
    //First Time Load 
    window.setTimeout('BindData()',1000);
  
    if(window.ActiveXObject)
    {
    XMLHTTPRequestObject =new ActiveXObject('Microsoft.XMLHTTP');
    }
    
    function ProgressBar()
    {
    
    var ProgressBar=document.getElementById('TxtProgressBar')
    var totalmessagesend =document.getElementById('Div2').innerHTML;
    var totalmessage=document.getElementById('Div4').innerHTML;

    var PWidth= ((totalmessagesend)/(totalmessage))*100
    PWidth =PWidth.toPrecision(3)
    if (PWidth <50)
    {
        ProgressBar.style.backgroundColor= 'Red'
    }
    if (PWidth >=50 && PWidth <80)
    {
     ProgressBar.style.backgroundColor= 'Orange'
    }
    if (PWidth >=80)
    {
     ProgressBar.style.backgroundColor= 'Yellow'
    }
    
    document.getElementById('TxtProgressBar').style.width=PWidth
    document.getElementById('TxtProgressBar').value=PWidth + '% Completed'
    }
    
    

   function BindData()
   {
 
           
            //Update Progress bar
            ProgressBar()
            
            var datasource ='Webservice/GetMessageStatus.asmx/GetEmailNormalStatus?Sending_id=' + document.getElementById('HiddenSendingId').value 
            
            XMLHTTPRequestObject.open('GET',datasource,true);
            XMLHTTPRequestObject.onreadystatechange =function()
            {
                    if(XMLHTTPRequestObject.readystate==4 && XMLHTTPRequestObject.status==200)
                    {
                    var xmlDoc =XMLHTTPRequestObject.responseXML;
                    var xmlDocc = new ActiveXObject("Microsoft.XMLDOM"); 
                    xmlDocc.load(xmlDoc); 
                    xmlObj=xmlDocc.documentElement
                    //Total Message
                    document.getElementById('Div4').innerHTML=xmlObj.childNodes.item(0).childNodes.item(0).text
                    //Total Sent
                    document.getElementById('Div2').innerHTML=xmlObj.childNodes.item(0).childNodes.item(1).text
                    //Success
                    document.getElementById('Div3').innerHTML=xmlObj.childNodes.item(0).childNodes.item(2).text
                    //Failed
                    document.getElementById('Div6').innerHTML=xmlObj.childNodes.item(0).childNodes.item(3).text
                    //Start Time
                    document.getElementById('Div5').innerHTML=xmlObj.childNodes.item(0).childNodes.item(4).text
                    //End Time
                    document.getElementById('Div7').innerHTML=xmlObj.childNodes.item(0).childNodes.item(5).text

                    
                    
                    }
                    else
                    {
                    
                     if (XMLHTTPRequestObject.status ==503 || XMLHTTPRequestObject.status ==504)
                       {   
                          BindData()
                       }
                 
                    }
                    
            }
              
           
            XMLHTTPRequestObject.send(null);

            timeout =  window.setTimeout('BindData();', 100); // 1 sec
            return false;
 
    
   }
   
   
   //Error Handler
     function handleError() 
    {

	    return true;
    }
    window.onerror = handleError;


 