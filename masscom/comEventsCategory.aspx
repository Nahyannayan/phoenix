﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="comEventsCategory.aspx.vb" Inherits="masscom_comEventsCategory" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .text-bold {
            font-weight: 600 !important;
        }

        table td input[type=image] {
            border-radius: 4px !important;
        }

        .grid-thumbnail {
            height: 50px;
            width: 50px;
            border: 2px solid #8dc24c !important;
            border-radius: 4px !important;
            max-height: 50px !important;
            overflow: hidden !important;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Events Category
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label></td>
                    </tr>
                </table>
                <table width="100%">


                    <tr>
                        <td align="left" width="20%"><span class="field-label">Events Description</span><asp:Label ID="lblasterick" runat="server" Text="*" CssClass="text-danger"></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDescrip" runat="server"></asp:TextBox><br />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDescrip" ErrorMessage="Please enter Description" ValidationGroup="preview" />
                        </td>



                        <td align="left" width="20%"><span class="field-label">Short Code</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtshortcode" runat="server"></asp:TextBox><br />
                            <span>&nbsp;</span>
                            <%--<asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" controltovalidate="txtshortcode" errormessage="Please enter Shortcode" validationgroup="preview"/>--%>
                        </td>


                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Color</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtcolor" runat="server" Visible="false"></asp:TextBox>
                            <telerik:RadColorPicker RenderMode="Lightweight" runat="server" ShowIcon="true" ID="Expectation_Color_picker" PaletteModes="HSV" Columns="5" CssClass="ColorPickerPreview"
                                KeepInScreenBounds="true">
                            </telerik:RadColorPicker>
                            <%--<asp:Label ID="Label2" runat="server" text="*" ForeColor="red" ></asp:Label>--%>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtcolor" ErrorMessage="Please choose a color" ValidationGroup="preview" />
                        </td>

                        <td align="left" width="20%"><span class="field-label">Thumbnail Image</span><asp:Label ID="Label3" runat="server" Text="*" CssClass="text-danger"></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:FileUpload ID="FileUploadTHUMB" runat="server" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="FileUploadTHUMB" ErrorMessage="Please Upload a file" ValidationGroup="preview" />
                                    <asp:Image ID="ImgThumbnail" runat="server" Height="50px" Width="50px" Visible="false" />
                                    </br> 
                                    <asp:Label ID="lblmsgformat" runat="server" Text="*File should be JPEG/PNG format  " CssClass="text-warning"></asp:Label>
                                    </br><asp:Label ID="lblmsgsize" runat="server" Text="*File size be less than 50KB" CssClass="text-warning"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                                    <asp:Button runat="server" ID="btnFileUploadIMG" Text="Upload" Visible="false" />
                                    <asp:LinkButton ID="LNKFileUploadTHUMB" runat="server" Visible="false">Upload</asp:LinkButton><br />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$"
                                        ControlToValidate="FileUploadTHUMB" runat="server" CssClass="text-danger" ErrorMessage="Please select a valid jpg or png  file." Display="Dynamic" />
                             
                        </td>
                        
                      
                    </tr>

                    <tr style="display: none;">
                        <td align="left" width="20%"><span class="field-label">Active</span></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkActive" runat="server" Visible="false" /></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="preview" />&nbsp;&nbsp;
                                         <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>

                    </tr>

                    <tr>

                        <td align="center" colspan="4" valign="top">
                            <asp:GridView ID="gvEventsCategory" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem" HorizontalAlign="Center" />
                                <Columns>

                                    <asp:TemplateField HeaderText="SNO" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblECMID" runat="server" Text='<%# Bind("ECM_ID")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblSNO" runat="server" Text='<%# Bind("SNo")%>' Visible="false">></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("ECM_DESCR")%>' align="left"></asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="text-bold" />
                                        <HeaderStyle HorizontalAlign="Left" Width="50%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Color">
                                        <ItemTemplate>
                                            <asp:Label ID="lblColor" runat="server" Text="NO COLOR" data-toggle="tooltip" title='<%# System.Drawing.ColorTranslator.FromHtml(Eval("ECM_COLOR_HEX_CODE"))%>' BackColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("ECM_COLOR_HEX_CODE"))%>'
                                                ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("ECM_COLOR_HEX_CODE"))%>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Image">
                                        <ItemTemplate>
                                            <asp:Label ID="lblimgpath" runat="server" Text='<%# Bind("ECM_THUMBNAIL_PATH")%>' Visible="false"></asp:Label>
                                            <asp:UpdatePanel ID="updpnl" runat="server">
                                                <ContentTemplate>
                                                    <asp:ImageButton ID="Imgbtn" runat="server" AlternateText="Download" OnClick="Imgbtn_Click" CssClass="grid-thumbnail" data-toggle="tooltip" title="Click To Download" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="Imgbtn" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("StatusACTIVE")%>' align="left"></asp:Label>
                                            <asp:Label ID="LblResourceEntryattachPath" runat="server" Text='<%# Eval("ECM_THUMBNAIL_PATH")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("ECM_CREATED_DT", "{0:dd/MMM/yyyy}")%>' align="left"></asp:Label>
                                            <%-- <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("EVT_EVT_ID")%>'  visible="false" align="left"></asp:Label>--%>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnEdit" Text="Edit" runat="server" align="center" OnClick="lnkBtnEdit_Click"></asp:LinkButton>

                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" align="center" OnClick="lnkbtnDelete_Click" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle></ItemStyle>
                                        <HeaderStyle Width="10%" />
                                    </asp:TemplateField>

                                    <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>

                        </td>

                    </tr>
                </table>
                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });
                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                </script>
            </div>
        </div>
        <%--<uc2:usrMessageBar runat="server" ID="usrMessageBar" />--%>
    </div>



</asp:Content>

