<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comAssignGroupsEdit.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="masscom_comAssignGroupsEdit" %>

<%@ Register Src="UserControls/comAssignGroupsEdit.ascx" TagName="comAssignGroupsEdit"
    TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">


    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function SetCloseNewGRPValuetoParent(msg) {
            //alert(msg)

            //var path = window.location.href
            //var Rpath = ''
            //if (path.indexOf('?') != '-1') {
            //    Rpath = path.substring(path.indexOf('?'), path.length)
            //}
            ////var page_url = "comCreateAssignGroups.aspx" + "?MainMnu_code=kuz/F%20JuQII=&datamode=bW5AEI9plJ4=";
            //var page_url = "comNewListGroups.aspx" + "?MainMnu_code=kuz/F%20JuQII=&datamode=bW5AEI9plJ4=";
            //return ShowWindowWithClose(page_url, 'search', '100%', '100%')
            //return false;

            $("#<%=hdn_msg.ClientID%>").val(msg);
              __doPostBack('<%=hdn_msg.ClientID%>', "");
              <%-- $("#<%=hdn_msg.ClientID%>").val(msg);--%>
              <%-- var k = <%# hdn_msg.ClientID%>
                 k.val = msg;--%>
            <%-- var qty_id = document.getElementById('<%=hdn_msg.ClientID%>').value
             alert(qty_id);
            --%>
          }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTopTitle" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <uc1:comAssignGroupsEdit ID="ComAssignGroupsEdit1" runat="server" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdn_msg" runat="server" />


    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>
