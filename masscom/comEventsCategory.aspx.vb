﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic
Imports System.Drawing

Partial Class masscom_comEventsCategory
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' Dim smscript As ScriptManager = Master.FindControl("ScriptManager1")
        ' smscript.RegisterPostBackControl(btnSave)
        If Page.IsPostBack = False Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            'btnSave.Attributes.Add("onclick", "return UnpostOthFeecharge(" & h_UnpostFChg.ClientID & ") ")


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ' gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                If Session("sUsr_name") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_DAYEND_PROCESS
                            lblHead.Text = "Day End Process"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    Page.Form.Attributes.Add("enctype", "multipart/form-data")

                    'Call bindCategory()
                    Call BinGrid()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try

        End If
    End Sub
    Private Sub BinGrid()


        Dim strConnString As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim ds As DataSet = New DataSet

        Try

            ds = SqlHelper.ExecuteDataset(strConnString, CommandType.StoredProcedure, "dbo.[GetEventsCategorydetails_new1]")

            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvEventsCategory.DataSource = ds.Tables(0)
                gvEventsCategory.DataBind()
            End If
          

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, "eventscate")

        Finally

            'con.Close()

            'con.Dispose()

        End Try


    End Sub
    Protected Sub ClearData()
        txtDescrip.Text = ""
        txtshortcode.Text = ""
        Dim colorCode1 As Color = ColorTranslator.FromHtml("#FFFFFF")
        Expectation_Color_picker.SelectedColor = colorCode1
        If Expectation_Color_picker.SelectedColor = ColorTranslator.FromHtml("#FFFFFF") Then
            RequiredFieldValidator3.Enabled = False
        End If
        chkActive.Checked = False
        ImgThumbnail.Visible = False
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("EventsCategory").ToString()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim strans As SqlTransaction = Nothing
        If btnSave.Text = "Update" Then
            If FileUploadTHUMB.HasFile Then
                Dim filesize As Integer = FileUploadTHUMB.PostedFile.ContentLength

                If filesize < 50000 Then '5242880 bytes= 5MB
                    If Not Directory.Exists(PhyPath) Then
                        ' Create the directory.
                        Directory.CreateDirectory(PhyPath)
                    End If
                    Dim filename As String = Path.GetFileName(FileUploadTHUMB.FileName)
                    Dim path1 As String = PhyPath

                    ViewState("filename") = filename
                    FileUploadTHUMB.SaveAs(PhyPath + filename)
                Else
                    lblError.Text = "File Size should be less than 5MB"
                    Exit Sub
                End If
            Else

            End If
            Dim ParamUP(7) As SqlParameter



            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction

            ParamUP(0) = Mainclass.CreateSqlParameter("@ECM_ID", Convert.ToString(ViewState("ECM_ID")), SqlDbType.Int)
            ParamUP(1) = Mainclass.CreateSqlParameter("@ECM_DESCR", txtDescrip.Text, SqlDbType.VarChar)
            If txtshortcode.Text <> "" Then
                ParamUP(2) = Mainclass.CreateSqlParameter("@ECM_SHORT_CODE", txtshortcode.Text, SqlDbType.VarChar)
            Else
                ParamUP(2) = Mainclass.CreateSqlParameter("@ECM_SHORT_CODE", DBNull.Value, SqlDbType.VarChar)
            End If

            ParamUP(3) = Mainclass.CreateSqlParameter("@ECM_COLOR_HEX_CODE ", System.Drawing.ColorTranslator.ToHtml(Expectation_Color_picker.SelectedColor), SqlDbType.VarChar)
            ParamUP(4) = Mainclass.CreateSqlParameter("@ECM_THUMBNAIL_PATH", "~/Events/Category/" + ViewState("filename"), SqlDbType.VarChar)
            ParamUP(5) = Mainclass.CreateSqlParameter("@ECM_IS_ACTIVE", DBNull.Value, SqlDbType.Bit)
            'ParamUP(6) = Mainclass.CreateSqlParameter("@ECM_CREATED_DT", DBNull.Value, SqlDbType.Date)
            ParamUP(6) = Mainclass.CreateSqlParameter("@ECM_LAST_MODIFIED_DT", Format(Date.Now, "dd/MMM/yyyy"), SqlDbType.Date)
            'ParamUP(8) = Mainclass.CreateSqlParameter("@ECM_CREATED_BY", DBNull.Value, SqlDbType.Date)
            ParamUP(7) = Mainclass.CreateSqlParameter("@ECM_LAST_MODIFIED_BY", Session("sUsr_name"), SqlDbType.VarChar)

            SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[EditEventsCategoryDetailsBYID_1]", ParamUP) 'SaveEventsategoryMaster
            strans.Commit()
            lblError.Text = "Updated Successfully"
            btnSave.Text = "Save"



        Else
            If FileUploadTHUMB.HasFile Then
                Dim filesize As Integer = FileUploadTHUMB.PostedFile.ContentLength

                If filesize < 5242880 Then '5242880 bytes= 5MB
                    If Not Directory.Exists(PhyPath) Then
                        ' Create the directory.
                        Directory.CreateDirectory(PhyPath)
                    End If
                    Dim filename As String = Path.GetFileName(FileUploadTHUMB.FileName)
                    Dim path1 As String = PhyPath

                    ViewState("filename") = filename
                    FileUploadTHUMB.SaveAs(PhyPath + filename)
                Else
                    lblError.Text = "File Size should be less than 5MB"
                    Exit Sub
                End If
            End If

            Dim Param(8) As SqlParameter



            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction

            Param(0) = Mainclass.CreateSqlParameter("@ECM_DESCR", txtDescrip.Text, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ECM_SHORT_CODE", txtshortcode.Text, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@ECM_COLOR_HEX_CODE", System.Drawing.ColorTranslator.ToHtml(Expectation_Color_picker.SelectedColor), SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@ECM_THUMBNAIL_PATH ", "~/Events/Category/" + ViewState("filename"), SqlDbType.VarChar)
            Param(4) = Mainclass.CreateSqlParameter("@ECM_IS_ACTIVE", DBNull.Value, SqlDbType.Bit)
            Param(5) = Mainclass.CreateSqlParameter("@ECM_CREATED_DT", Format(Date.Now, "dd/MMM/yyyy"), SqlDbType.Date)
            Param(6) = Mainclass.CreateSqlParameter("@ECM_LAST_MODIFIED_DT", DBNull.Value, SqlDbType.Date)
            Param(7) = Mainclass.CreateSqlParameter("@ECM_CREATED_BY", Session("sUsr_name"), SqlDbType.VarChar)
            Param(8) = Mainclass.CreateSqlParameter("@ECM_LAST_MODIFIED_BY", DBNull.Value, SqlDbType.Date)

            SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[SaveEventsategoryMaster_new1]", Param) 'SaveEventsategoryMaster
            strans.Commit()
            lblError.Text = "Saved Successfully"
        End If

        '  strans.Commit()

        Call BinGrid()
        Call ClearData()
    End Sub

    Protected Sub Imgbtn_Click(sender As Object, e As ImageClickEventArgs)
        Dim PhyPath1 As String = Web.Configuration.WebConfigurationManager.AppSettings("EventsCategory").ToString()
        Dim lblimgpath As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent.Parent.Parent,  _
                                        GridViewRow)

        Dim index As Integer = gvRow.RowIndex
        lblimgpath = TryCast(gvRow.Cells(0).FindControl("lblimgpath"), Label)

        Dim filename As String = Path.GetFileName(lblimgpath.Text)
        Dim Fpath1 As String = lblimgpath.Text.Remove(0, 2)
        Dim finalpath As String = Fpath1.Replace("/", "\")
        Dim BYTES() As Byte = File.ReadAllBytes(PhyPath1 + filename)
        HttpContext.Current.Response.Clear()
        Response.ContentType = ContentType
        Response.AddHeader("content-disposition", "attachment; filename=" & filename)

        Response.CacheControl = "No-cache"
        Response.BinaryWrite(BYTES)
        Response.Flush()
        Response.SuppressContent = True
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub

    Protected Sub lnkBtnEdit_Click(sender As Object, e As EventArgs)
        Dim lblECMID As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                        GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        lblECMID = TryCast(gvRow.Cells(0).FindControl("lblECMID"), Label)
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        ViewState("ECM_ID") = lblECMID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable
        Dim PhyPath1 As String = Web.Configuration.WebConfigurationManager.AppSettings("EventsCategory").ToString()
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@ECM_ID", Convert.ToInt32(lblECMID.Text), SqlDbType.BigInt)

        ds1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetEventsCategoryBYID", Param)
        dt1 = ds1.Tables(0)
        If dt1.Rows.Count > 0 Then
            txtDescrip.Text = dt1.Rows(0).Item("ECM_DESCR")
            txtshortcode.Text = dt1.Rows(0).Item("ECM_SHORT_CODE")
            chkActive.Checked = dt1.Rows(0).Item("ECM_IS_ACTIVE")
            Dim colorCode As Color = ColorTranslator.FromHtml(dt1.Rows(0).Item("ECM_COLOR_HEX_CODE"))
            Expectation_Color_picker.SelectedColor = colorCode
            ImgThumbnail.Visible = True
            Dim thumbnailpath As String = dt1.Rows(0).Item("ECM_THUMBNAIL_PATH")
            Dim filename As String = Path.GetFileName(thumbnailpath)
            Dim Fpath1 As String = thumbnailpath.Remove(0, 2)
            Dim finalpath As String = Fpath1.Replace("/", "\")
            Dim BYTES() As Byte = File.ReadAllBytes(PhyPath1 + filename)
            Dim base64String As String = Convert.ToBase64String(BYTES, 0, BYTES.Length)

            ImgThumbnail.ImageUrl = "data:image/jpeg;base64," + base64String
        End If
        btnSave.Text = "Update"
    End Sub

    Protected Sub lnkbtnDelete_Click(sender As Object, e As EventArgs)

        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                        GridViewRow)

        Dim index As Integer = gvRow.RowIndex
        Dim lblECMID As Label

        lblECMID = TryCast(gvRow.Cells(0).FindControl("lblECMID"), Label)
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        ViewState("ECM_ID") = lblECMID.Text
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConnc As New SqlConnection(str_conn)
        objConnc.Open()

        Dim ds1 As New DataSet
        Dim dt1 As New DataTable
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@ECM_ID", Convert.ToInt32(lblECMID.Text), SqlDbType.BigInt)
        SqlHelper.ExecuteNonQuery(objConnc, "[dbo].[DeleteEventsCategoryBYID]", Param)
        objConnc.Close()
        Call BinGrid()
    End Sub

    Protected Sub gvEventsCategory_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvEventsCategory.RowDataBound
        Dim PhyPath1 As String = Web.Configuration.WebConfigurationManager.AppSettings("EventsCategory").ToString()
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Imgbtn As ImageButton
            Imgbtn = TryCast(e.Row.FindControl("Imgbtn"), ImageButton)
            Dim lblResourceCategorypath As Label = TryCast(e.Row.FindControl("lblimgpath"), Label)
            'C:\Resources\Category\
            Dim filename As String = Path.GetFileName(lblResourceCategorypath.Text)
            Dim Fpath1 As String = lblResourceCategorypath.Text.Remove(0, 2)
            Dim finalpath As String = Fpath1.Replace("/", "\")
            Dim BYTES() As Byte = File.ReadAllBytes(PhyPath1 + filename)
            'Imgbtn.ImageUrl = PhyPath1 + filename



            Dim base64String As String = Convert.ToBase64String(BYTES, 0, BYTES.Length)

            Imgbtn.ImageUrl = "data:image/jpeg;base64," + base64String


        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Call ClearData()
        btnSave.Text = "Save"
    End Sub
End Class
