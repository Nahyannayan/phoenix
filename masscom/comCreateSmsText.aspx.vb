Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_comCreateSmsText
    Inherits System.Web.UI.Page
    Dim mainMenuCode As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Encr_decrData As New Encryption64
        If Not IsPostBack Then
            Me.hfMenuCode.Value = Encr_decrData.Decrypt(Request.QueryString("mnu").Replace(" ", "+"))
            If Me.hfMenuCode.Value = "M0000100" Then
                Me.trCharCount.Visible = False
                HiddenCount.Value = "999999999"
                txtfrom.Text = WebConfigurationManager.AppSettings("smsSenderId_Urgent").ToString()
                txtfrom.ReadOnly = True
            Else
                Me.trCharCount.Visible = True
                HiddenCount.Value = "1060"
                txtfrom.Text = ""
                txtfrom.ReadOnly = False
            End If
            Hiddenbsuid.Value = Session("sBsuid")
            txtcount.Text = HiddenCount.Value
            BindTemplate()
            HiddenRefresh.Value = 0
            GetTRAGuideline()
            '-----------Added by Jacob on 15/Sep/2020
            If CheckGuidelineExists() = False Then '----disabled for other countries, enabled only for UAE
                chkIsPromotional.Checked = False
                chkIsPromotional.Visible = False
            Else
                chkIsPromotional.Checked = True
                chkIsPromotional.Visible = True
                btnSave.Attributes.Add("OnClick", "return ConfirmPromotional();")
            End If
        End If
        from_id.Visible = False
        If HiddenRefresh.Value = 1 Then
            BindTemplate()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        txtcount.Text = HiddenCount.Value
        txttaken.Text = "0"
        'AssignRights()
    End Sub

    Public Sub GetTRAGuideline()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT ISNULL(MessageText, '') MessageText FROM SMS.VW_TRA_GUIDELINE WHERE COUNTRY_ID = '" & Session("BSU_COUNTRY_ID") & "';"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            lblGuideline.InnerHtml = ds.Tables(0).Rows(0)(0).ToString()
        End If

    End Sub
    Public Function CheckGuidelineExists() As Boolean
        CheckGuidelineExists = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT COUNT(COUNTRY_ID) FROM SMS.VW_TRA_GUIDELINE WHERE COUNTRY_ID = '" & Session("BSU_COUNTRY_ID") & "';"

        Dim rows As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If rows > 0 Then
            CheckGuidelineExists = True
        End If

    End Function

    Public Function BindTemplate() As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parameters As SqlParameter() = New SqlParameter(4) {}
        parameters(0) = New SqlParameter("@bsuId", Hiddenbsuid.Value)
        parameters(1) = New SqlParameter("@MERGETYPE", "SMS")
        ''  Dim str_query = "select MERGE_ID,MERGE_TITLE_DES from COM_MERGE_TABLES where MERGE_BSU='" & Hiddenbsuid.Value & "' AND MERGE_TYPE='SMS'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_SMS_MERGE_VALUES", parameters)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtemplate.DataSource = ds
            ddtemplate.DataTextField = "MERGE_TITLE_DES"
            ddtemplate.DataValueField = "MERGE_ID"
            ddtemplate.DataBind()

            Dim item As New ListItem
            item.Text = "Select a Template"
            item.Value = "-1"
            ddtemplate.Items.Insert(0, item)
            Fields()
        Else
            TRDynamic.Visible = False
        End If

        Return ""

    End Function
   
    Public Sub Fields()
        ''Clear the Previous message
        'txtsmstext.Text = ""
        'txttaken.Text = ""
        'txtcount.Text = ""

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parameters1 As SqlParameter() = New SqlParameter(4) {}
        parameters1(0) = New SqlParameter("@mergeId", ddtemplate.SelectedValue)
        ''  parameters1(1) = New SqlParameter("@MERGETYPE", "SMS")
        ''  Dim str_query = "select MERGE_FIELDS from COM_MERGE_TABLES where MERGE_ID='" & ddtemplate.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_SMS_MERGE_FIELDS", parameters1)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim fields As String = ds.Tables(0).Rows(0).Item("MERGE_FIELDS").ToString()

            Dim Afields() As String = fields.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("Data")
            dt.Columns.Add("Value")

            Dim i = 0
            For i = 0 To Afields.Length - 1
                Dim cval As String = Afields(i)


                Dim csplit() As String = cval.Split(">")

                Dim dr As DataRow = dt.NewRow()

                dr("Value") = "$" & csplit(0) & "$"
                dr("Data") = csplit(1).Replace("$", "").Replace("<", "")

                dt.Rows.Add(dr)
            Next

            ddfields.DataSource = dt
            ddfields.DataTextField = "Data"
            ddfields.DataValueField = "Value"
            ddfields.DataBind()
            ddfields.Visible = True
            btninsert.Visible = True
        Else
            ddfields.Visible = False
            btninsert.Visible = False
        End If



    End Sub


    'Public Sub AssignRights()
    '    Dim Encr_decrData As New Encryption64
    '    Dim CurBsUnit As String = Session("sBsuid")
    '    Dim USR_NAME As String = Session("sUsr_name")

    '    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
    '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
    '    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

    '    Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
    '    directory.Add("Save", btnSave)
    '    Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))



    'End Sub
    'Public Sub smschedistrials()
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '    Dim bsu_id = Session("sBsuid")
    '    Dim sql_query = "select  BSU_SMSUSERNAME, BSU_SMSPWD,BSU_SMSFrom from BUSINESSUNIT_M where BSU_ID='" & bsu_id & "'"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        txtusername.Text = ds.Tables(0).Rows(0).Item("BSU_SMSUSERNAME").ToString()
    '        txtpassword.Text = ds.Tables(0).Rows(0).Item("BSU_SMSPWD").ToString()
    '        txtfrom.Text = ds.Tables(0).Rows(0).Item("BSU_SMSFrom").ToString()

    '    End If
    'End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Hiddenbsuid.Value
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000040") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

         ''SMS configuration from db nahyan on 29 May 2018       
        Dim dtN As New DataTable
        dtN = SmsService.sms.GetSMSConfiguration(Hiddenbsuid.Value, "COM")
        ''ends here nahyan 

        'Dim pParms(9) As SqlClient.SqlParameter
        Dim pParms(11) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@CMS_SMS_TEXT", txtsmstext.Text.Trim())

        If Me.hfMenuCode.Value <> "M0000100" Then
            ''comemnted bynahyan on 29 may 2018 to fetch configuration dynamically
            'If Hiddenbsuid.Value = "315888" Then ''for GIP
            '    pParms(1) = New SqlClient.SqlParameter("@CMS_USER_NAME", WebConfigurationManager.AppSettings("GIPMalaysiaSMSUsername").ToString())
            '    pParms(2) = New SqlClient.SqlParameter("@CMS_PASSWORD", WebConfigurationManager.AppSettings("GIPMalaysiaSMSpwd").ToString())
            'Else
            '    pParms(1) = New SqlClient.SqlParameter("@CMS_USER_NAME", WebConfigurationManager.AppSettings("smsUsername").ToString())
            '    pParms(2) = New SqlClient.SqlParameter("@CMS_PASSWORD", WebConfigurationManager.AppSettings("smspwd").ToString())
            'End If

            pParms(1) = New SqlClient.SqlParameter("@CMS_USER_NAME", dtN.Rows(0)("BSC_SMS_USER").ToString())
            pParms(2) = New SqlClient.SqlParameter("@CMS_PASSWORD", dtN.Rows(0)("BSC_SMS_PASSWORD").ToString())
            pParms(3) = New SqlClient.SqlParameter("@CMS_FROM", dtN.Rows(0)("BSC_SMS_FROM").ToString())

        Else
            'If Hiddenbsuid.Value = "315888" Then ''for GIP
            '    pParms(1) = New SqlClient.SqlParameter("@CMS_USER_NAME", WebConfigurationManager.AppSettings("GIPMalaysiaSMSUsername").ToString())
            '    pParms(2) = New SqlClient.SqlParameter("@CMS_PASSWORD", WebConfigurationManager.AppSettings("GIPMalaysiaSMSpwd").ToString())
            'Else
            '    pParms(1) = New SqlClient.SqlParameter("@CMS_USER_NAME", WebConfigurationManager.AppSettings("smsUsername_Urgent").ToString())
            '    pParms(2) = New SqlClient.SqlParameter("@CMS_PASSWORD", WebConfigurationManager.AppSettings("smspwd_Urgent").ToString())
            'End If

            pParms(1) = New SqlClient.SqlParameter("@CMS_USER_NAME", dtN.Rows(0)("BSC_SMS_USER").ToString())
            pParms(2) = New SqlClient.SqlParameter("@CMS_PASSWORD", dtN.Rows(0)("BSC_SMS_PASSWORD").ToString())
            pParms(3) = New SqlClient.SqlParameter("@CMS_FROM", dtN.Rows(0)("BSC_SMS_FROM").ToString())
        End If


        ' pParms(3) = New SqlClient.SqlParameter("@CMS_FROM", txtfrom.Text.Trim())

        pParms(4) = New SqlClient.SqlParameter("@CMS_BSU_ID", Hiddenbsuid.Value)
        pParms(5) = New SqlClient.SqlParameter("@CMS_EMP_ID", Session("EmployeeId"))

        If ddtemplate.SelectedIndex > 0 And CheckArabic.Checked = False Then
            pParms(6) = New SqlClient.SqlParameter("@CMS_MERGE_ID", ddtemplate.SelectedValue)
        End If

        pParms(7) = New SqlClient.SqlParameter("@CMS_UTF_16", HiddenUTF16.Value.Replace(" ", ""))

        If CheckArabic.Checked Then
            pParms(8) = New SqlClient.SqlParameter("@CMS_ARABIC", True)
        End If

        If Me.hfMenuCode.Value <> "M0000100" Then
            pParms(9) = New SqlClient.SqlParameter("@CMS_URGENT", 0)
        Else
            pParms(9) = New SqlClient.SqlParameter("@CMS_URGENT", 10)
        End If
        pParms(10) = New SqlClient.SqlParameter("@OUT_VALUE", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.Output

        'pParms(11) = New SqlClient.SqlParameter("@SMC_CODE", hdn_SMC_CODE.Value.Trim())

        If chkIsPromotional.Checked = True Then
            pParms(11) = New SqlClient.SqlParameter("@SMC_CODE", "PRO")
        Else
            pParms(11) = New SqlClient.SqlParameter("@SMC_CODE", "TRA")
        End If


        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_INSERT_NEW", pParms)
        Dim outvalue As String = pParms(10).Value
        lblmessage.Text = "SMS Template Created with ID "

        Dim jsFunc As String = "SetCloseValuetoParent('" + lblmessage.Text.ToString + outvalue + "')"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", jsFunc, True)
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        txtsmstext.Text = ""
        'txtusername.Text = ""
        'txtpassword.Text = ""
        txtfrom.Text = ""
        txtcount.Text = HiddenCount.Value
        HiddenUTF16.Value = ""
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Write("<script language='javascript'>")
        Response.Write("window.returnValue = '" & 1 & "';")
        Response.Write("window.close();")
        Response.Write(" </script>")
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Calling javascript", "SetCloseFrame()", True)
    End Sub

    Protected Sub CheckArabic_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckArabic.CheckedChanged
        txtsmstext.Text = ""
        txttaken.Text = ""
        txtcount.Text = ""
        lblmessage.Text = ""
        If CheckArabic.Checked Then
            TRDynamic.Visible = False
            txtsmstext.Attributes.Add("style", "direction:rtl")
            TRDynamic.Visible = False

        Else
            TRDynamic.Visible = True
            txtsmstext.Attributes.Add("style", "direction:ltr")

            BindTemplate()
        End If

    End Sub

    Protected Sub ddtemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtemplate.SelectedIndexChanged
        Fields()
    End Sub

    
End Class
