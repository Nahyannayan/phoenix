﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Partial Class masscom_comEventsEntry
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim FeeCommon As New FeeCommon
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ' gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                If Session("sUsr_name") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_DAYEND_PROCESS
                            lblHead.Text = "Day End Process"
                    End Select
                   
                    ' ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), CStr(Session("sBsuid")))
                    Call BindAcademicyear_CLM()
                    Call BindEventsCategory()
                    'Call BindGrade()
                    Call BindGradecmb()
                    Call BindEventsStatus()
                    Call BindGrid()
                    ' btnSave.Attributes["onClick"]="return CheckValidDate(txtfromDate.Text, toDate.Text)""
                    ' btnSave.Attributes("onClick") = "return CheckValidDate(txtStartDate.Text,txtEnddate.Text)"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try

        End If
    End Sub

    Private Sub BindAcademicyear_CLM()
        'Dim dt As DataTable
        'dt = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
        'ddlAcademicYear.DataSource = dt
        'ddlAcademicYear.DataTextField = "ACY_DESCR"
        'ddlAcademicYear.DataValueField = "ACD_ID"
        'ddlAcademicYear.DataBind()
        'For Each rowACD As DataRow In dt.Rows
        '    If rowACD("ACD_CURRENT") Then
        '        ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
        '        Exit For
        '    End If
        'Next
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        Dim cmd As New SqlCommand("[EVT].[GET_CURRENT_AND_FUTURE_ACADEMIC_YEAR]", sqlCon) 'GetEventsCategory
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
        sqlCon.Open()
        Using dr As SqlDataReader = cmd.ExecuteReader()
            If dr.HasRows Then
                ddlAcademicYear.DataSource = dr
                ddlAcademicYear.DataTextField = "ACY_DESCR"
                ddlAcademicYear.DataValueField = "ACD_ID"
                ddlAcademicYear.DataBind()
                Dim li As New ListItem
                li.Text = "SELECT"
                li.Value = 0
                ddlAcademicYear.Items.Insert(0, li)
            End If
        End Using
        sqlCon.Close()
        'Dim li As New ListItem
        'li.Text = "SELECT"
        'li.Value = ""
        'ddlAcademicYear.Items.Insert(0, li)
    End Sub
    Private Sub BindGrid()

        Try

            Dim sqlCon_img As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim Param_img(0) As SqlParameter
            Param_img(0) = Mainclass.CreateSqlParameter("@bsuid", Session("sBsuid"), SqlDbType.VarChar)
            ds = SqlHelper.ExecuteDataset(sqlCon_img, "dbo.[GetEventsTranEntry_new1]", Param_img)

            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                dt = ds.Tables(0)
                '  If dt.Rows.Count > 0 Then
                gvEventsEnty.DataSource = dt

                gvEventsEnty.DataBind()
            End If
          

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, "EventsEntry")

        Finally


        End Try

    End Sub
    Private Sub BindEventsCategory()
        Try
            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
            Dim cmd As New SqlCommand("dbo.[GetEventsCategory_new]", sqlCon) 'GetEventsCategory
            cmd.CommandType = CommandType.StoredProcedure
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlCategory.DataSource = dr
                    ddlCategory.DataTextField = "ECM_DESCR"
                    ddlCategory.DataValueField = "ECM_ID"
                    ddlCategory.DataBind()
                    Dim li As New ListItem
                    li.Text = "SELECT"
                    li.Value = 0
                    ddlCategory.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub

    'Private Sub BindGrade()
    '    Try

    '        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)

    '        Dim cmd As New SqlCommand("[GET_ALL_GRADES_FROM_ACDID_NEW]", sqlCon)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
    '        cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = ddlAcademicYear.SelectedValue.ToString
    '        sqlCon.Open()
    '        Using dr As SqlDataReader = cmd.ExecuteReader()
    '            If dr.HasRows Then
    '                ddlgrade.DataSource = dr
    '                ddlgrade.DataTextField = "GRM_GRD_ID"
    '                ddlGrade.DataValueField = "GRM_ID"
    '                ddlgrade.DataBind()
    '                Dim li As New ListItem
    '                li.Text = "SELECT"
    '                li.Value = ""
    '                ddlgrade.Items.Insert(0, li)
    '            End If
    '        End Using
    '        sqlCon.Close()


    '    Catch ex As Exception
    '        Response.Write("Error:" + ex.Message.ToString())
    '    End Try
    'End Sub

    Private Sub BindGradecmb()
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)

            Dim cmd As New SqlCommand("[GET_ALL_GRADES_FROM_ACDID_NEW]", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
            cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = ddlAcademicYear.SelectedValue.ToString
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    cmbGrade.DataSource = dr
                    cmbGrade.DataTextField = "GRM_GRD_ID"
                    cmbGrade.DataValueField = "GRM_ID"
                    cmbGrade.DataBind()
                    Dim li As New ListItem
                    li.Text = "All Grade"
                    li.Value = ""
                    'cmbGrade.Items.Insert(0, li)
                    If ddlAcademicYear.SelectedValue.ToString <> 0 Then
                        For Each item As RadComboBoxItem In cmbGrade.Items

                            item.Checked = True

                        Next
                    End If
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub
    Private Sub BindGradeforedit(ByVal acdid As String)
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)

            Dim cmd As New SqlCommand("[GET_ALL_GRADES_FROM_ACDID_NEW]", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
            cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = acdid.ToString
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    cmbGrade.DataSource = dr
                    cmbGrade.DataTextField = "GRM_GRD_ID"
                    cmbGrade.DataValueField = "GRM_ID"
                    cmbGrade.DataBind()
                    Dim li As New ListItem
                    li.Text = "SELECT"
                    li.Value = ""
                    'ddlGrade.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub

    Private Sub BindSection()
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)

            Dim cmd As New SqlCommand("[GET_ALL_SECTION_FROM_GRADE_New]", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
            cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = ddlAcademicYear.SelectedValue.ToString
            cmd.Parameters.Add("@GRADE", SqlDbType.Int).Value = ddlGrade.SelectedItem.Value.ToString
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlSection.DataSource = dr
                    ddlSection.DataTextField = "SCT_DESCR"
                    ddlSection.DataValueField = "SCT_ID"
                    ddlSection.DataBind()
                    Dim li As New ListItem
                    li.Text = "SELECT"
                    li.Value = ""
                    ddlSection.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub

    Private Sub BindSectionforEdit(ByVal acdid As String, ByVal gradeid As String)
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)

            Dim cmd As New SqlCommand("[GET_ALL_SECTION_FROM_GRADE_New]", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
            cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = acdid.ToString
            cmd.Parameters.Add("@GRADE", SqlDbType.Int).Value = gradeid.ToString
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlSection.DataSource = dr
                    ddlSection.DataTextField = "SCT_DESCR"
                    ddlSection.DataValueField = "SCT_ID"
                    ddlSection.DataBind()
                    Dim li As New ListItem
                    li.Text = "SELECT"
                    li.Value = ""
                    ddlSection.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub
    Private Sub BindEventsStatus()
        Try
            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
            Dim cmd As New SqlCommand("dbo.[GetEventsStatus]", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlStatus.DataSource = dr
                    ddlStatus.DataTextField = "ECM_STATUS"
                    ddlStatus.DataValueField = "ECM_ST_ID"
                    ddlStatus.DataBind()
                    'Dim li As New ListItem
                    'li.Text = ""
                    'li.Value = ""
                    'ddlStatus.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub
    Protected Sub test()

    End Sub
    Private Sub cleardata()
        'ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), CStr(Session("sBsuid")))
        BindAcademicyear_CLM()
        ddlSection.Items.Clear()

        Dim Section As String = ""

        'BindGrade()
        cmbGrade.Items.Clear()
        Call BindGradecmb()
        txtDescrip.Text = ""
        txtRemarks.Text = ""
        txtStartDate.Text = ""
        txtEnddate.Text = ""

        BindEventsCategory()
        timepicker4.Value = ""
        timepicker5.Value = ""
        BindEventsStatus()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ' Call BindGrade()
        Call BindGradecmb()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call BindSection()
    End Sub





    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try



            Dim totaldays As Integer
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Dim starttime As DateTime
            Dim endtime As DateTime
            Dim strStarttime As String
            Dim strEndtime As String
            If timepicker4.Value <> "" Then
                starttime = DateTime.Parse(timepicker4.Value)
                strStarttime = starttime.ToString("HH:mm")
            Else

            End If
            If timepicker5.Value <> "" Then
                endtime = DateTime.Parse(timepicker5.Value)
                strEndtime = endtime.ToString("HH:mm")
            Else
                'endtime = ""
            End If




            'If starttime > endtime Then
            '    lbltimeerror.Text = "Please Select Start time Should be Less than End Time "
            '    ' Compare date is in the future!

            'End If


            Dim dtFromDate As DateTime = DateTime.MinValue, dtToDate As DateTime = DateTime.MinValue
            If DateTime.TryParse(txtStartDate.Text, dtFromDate) AndAlso DateTime.TryParse(txtEnddate.Text, dtToDate) Then
                If dtFromDate <= dtToDate Then

                    'If strStarttime < strEndtime Then


                    'valid dates






                    'Total Days Calculate

                    Dim startDT As DateTime = Convert.ToDateTime(txtStartDate.Text)

                    Dim endDT As DateTime = Convert.ToDateTime(txtEnddate.Text)

                    If startDT > endDT Then
                    Else

                    End If
                    Dim ts As TimeSpan = endDT.Subtract(startDT)
                    If Convert.ToInt32(ts.Days) >= 0 Then
                        txtTotaldays.Text = Convert.ToInt32(ts.Days) + 1
                        totaldays = Convert.ToInt32(ts.Days) + 1
                    Else
                    End If
                    Dim grade As String = ""
                    If ddlGrade.Items.Count > 0 Then
                        '    ddlgrade.SelectedItem.Text = ""
                        'Else
                        If ddlGrade.SelectedItem.ToString <> "SELECT" Then
                            grade = ddlGrade.SelectedItem.ToString
                        Else
                            grade = ""
                        End If
                    Else

                        grade = ""
                    End If
                    Dim SelectStgrade As String
                    Dim selectedgradeid As String
                    Dim totcount As Integer = cmbGrade.Items.Count
                    Dim gradecount As Integer = cmbGrade.CheckedItems.Count
                    ' Dim selectedcase As Integer = cmbGrade.Items
                    If (gradecount > 0) Then
                        For Each item As RadComboBoxItem In cmbGrade.Items
                            If (item.Checked = True) Then
                                If (SelectStgrade = "") Then
                                    SelectStgrade = item.Text
                                    selectedgradeid = item.Value
                                Else
                                    SelectStgrade = SelectStgrade + "," + item.Text
                                    selectedgradeid = selectedgradeid + "," + item.Value
                                End If
                            End If
                        Next
                    End If
                    Dim gradeArr() As String

                    If gradecount > 0 Then

                        gradeArr = selectedgradeid.Split(",")
                        If totcount = gradecount Then
                            gradecount = 1
                        Else

                        End If
                    Else
                        gradecount = 1
                    End If




                    Dim strArr() As String

                    Dim test As Integer
                    'strArr = SELStudid.Split(",")
                    'For grcount = 0 To gradeArr.Length - 1
                    '    test = gradeArr(grcount)
                    'Next

                    Dim Section As String = ""
                    If ddlSection.Items.Count > 0 Then
                        '    ddlgrade.SelectedItem.Text = ""
                        'Else
                        If ddlSection.SelectedItem.ToString <> "SELECT" Then
                            Section = ddlSection.SelectedItem.ToString
                        Else
                            Section = ""
                        End If
                    Else

                        Section = ""
                    End If


                    Dim time1 As String
                    Dim time2 As String
                    Dim dt1 As DateTime
                    If timepicker4.Value <> "" Then
                        Dim res As Boolean = DateTime.TryParse(timepicker4.Value, dt1)
                        If res Then
                            time1 = dt1.ToString("HH:mm:ss")
                            ' 13:45:00
                        End If
                    Else
                        time1 = ""
                    End If
                    Dim dt2 As DateTime
                    If timepicker5.Value <> "" Then
                        Dim res2 As Boolean = DateTime.TryParse(timepicker5.Value, dt2)
                        If res2 Then
                            time2 = dt2.ToString("HH:mm:ss")
                            ' 13:45:00
                        End If
                    Else
                        time2 = ""
                    End If

                    Dim resourcesdate As String = txtStartDate.Text

                    Dim hid As Integer = 0
                    If btnSave.Text = "Update" Then
                        Dim ParamUP(14) As SqlParameter

                        Dim strans As SqlTransaction = Nothing

                        Dim objConn As New SqlConnection(str_conn)
                        objConn.Open()
                        strans = objConn.BeginTransaction
                        For gcount As Integer = 0 To gradecount - 1


                            For count As Integer = 0 To totaldays - 1



                                ParamUP(0) = Mainclass.CreateSqlParameter("@EVT_EVT_ID", Convert.ToInt32(ViewState("EVT_EVT_ID")), SqlDbType.BigInt)
                                ParamUP(1) = Mainclass.CreateSqlParameter("@EVT_ECM_ID", Convert.ToInt32(ddlCategory.SelectedItem.Value), SqlDbType.BigInt)
                                ParamUP(2) = Mainclass.CreateSqlParameter("@EVT_ACD_ID", Convert.ToInt32(ddlAcademicYear.SelectedItem.Value), SqlDbType.Int)
                                If cmbGrade.CheckedItems.Count = 0 Or totcount = cmbGrade.CheckedItems.Count Then

                                    ParamUP(3) = Mainclass.CreateSqlParameter("@EVT_GRM_ID", DBNull.Value, SqlDbType.Int)
                                Else
                                    ParamUP(3) = Mainclass.CreateSqlParameter("@EVT_GRM_ID", gradeArr(gcount), SqlDbType.Int)
                                End If

                                If Section <> "" Then
                                    ParamUP(4) = Mainclass.CreateSqlParameter("@EVT_SCT_ID", Convert.ToInt32(ddlSection.SelectedItem.Value), SqlDbType.Int)
                                Else
                                    ParamUP(4) = Mainclass.CreateSqlParameter("@EVT_SCT_ID", DBNull.Value, SqlDbType.Int)
                                End If
                                ParamUP(5) = Mainclass.CreateSqlParameter("@EVT_DESCR", txtDescrip.Text, SqlDbType.VarChar)
                                ParamUP(6) = Mainclass.CreateSqlParameter("@EVT_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
                                ParamUP(7) = Mainclass.CreateSqlParameter("@EVT_STATUS", ddlStatus.SelectedItem.Text, SqlDbType.VarChar)


                                ParamUP(8) = Mainclass.CreateSqlParameter("@EVT_START_DT", startDT.AddDays(count), SqlDbType.Date)
                                ParamUP(9) = Mainclass.CreateSqlParameter("@EVT_END_DT", startDT.AddDays(count), SqlDbType.Date)
                                ParamUP(10) = Mainclass.CreateSqlParameter("@EVT_LAST_MODIFIED_DT", Format(Date.Now, "dd/MMM/yyyy"), SqlDbType.Date)
                                ParamUP(11) = Mainclass.CreateSqlParameter("@EVT_LAST_MODIFIED_BY", Session("sUsr_name"), SqlDbType.VarChar)
                                If time1.ToString <> "" Then
                                    ParamUP(12) = Mainclass.CreateSqlParameter("@EVT_STARTTIME", time1.ToString, SqlDbType.VarChar)
                                Else
                                    ParamUP(12) = Mainclass.CreateSqlParameter("@EVT_STARTTIME", DBNull.Value, SqlDbType.VarChar)
                                End If
                                If time2.ToString <> "" Then
                                    ParamUP(13) = Mainclass.CreateSqlParameter("@EVT_ENDTIME", time2.ToString, SqlDbType.VarChar)
                                Else
                                    ParamUP(13) = Mainclass.CreateSqlParameter("@EVT_ENDTIME", DBNull.Value, SqlDbType.VarChar)
                                End If


                                Dim mainevt_id As Integer = Convert.ToInt32(ViewState("EVT_EVT_ID"))
                                mainevt_id = mainevt_id + count
                                ParamUP(14) = Mainclass.CreateSqlParameter("@EVT_ID", mainevt_id, SqlDbType.BigInt)
                                SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[UPDATEEventsTranEntryDetails]", ParamUP)

                            Next

                        Next

                        strans.Commit()
                        lblError.Text = "Record Updated Successfully"

                    Else
                        'SAVE

                        Dim Param(17) As SqlParameter

                        Dim strans As SqlTransaction = Nothing

                        Dim objConn As New SqlConnection(str_conn)
                        objConn.Open()
                        strans = objConn.BeginTransaction
                        For gcount As Integer = 0 To gradecount - 1
                            For count As Integer = 0 To totaldays - 1



                                Param(0) = Mainclass.CreateSqlParameter("@EVT_ECM_ID", Convert.ToInt32(ddlCategory.SelectedItem.Value), SqlDbType.BigInt)
                                Param(1) = Mainclass.CreateSqlParameter("@EVT_BSU_ID", CStr(Session("sBsuid")), SqlDbType.VarChar)
                                Param(2) = Mainclass.CreateSqlParameter("@EVT_ACD_ID", Convert.ToInt32(ddlAcademicYear.SelectedItem.Value), SqlDbType.Int)
                                If cmbGrade.CheckedItems.Count = 0 Or totcount = cmbGrade.CheckedItems.Count Then

                                    Param(3) = Mainclass.CreateSqlParameter("@EVT_GRM_ID", DBNull.Value, SqlDbType.Int)
                                Else
                                    Param(3) = Mainclass.CreateSqlParameter("@EVT_GRM_ID", gradeArr(gcount), SqlDbType.Int)
                                End If

                                If Section <> "" Then
                                    Param(4) = Mainclass.CreateSqlParameter("@EVT_SCT_ID", Convert.ToInt32(ddlSection.SelectedItem.Value), SqlDbType.Int)
                                Else
                                    Param(4) = Mainclass.CreateSqlParameter("@EVT_SCT_ID", DBNull.Value, SqlDbType.Int)
                                End If

                                Param(5) = Mainclass.CreateSqlParameter("@EVT_DESCR", txtDescrip.Text, SqlDbType.VarChar)
                                Param(6) = Mainclass.CreateSqlParameter("@EVT_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
                                Param(7) = Mainclass.CreateSqlParameter("@EVT_STATUS", ddlStatus.SelectedItem.Text, SqlDbType.VarChar)
                                Param(8) = Mainclass.CreateSqlParameter("@EVT_START_DT", startDT.AddDays(count), SqlDbType.Date)
                                Param(9) = Mainclass.CreateSqlParameter("@EVT_END_DT", startDT.AddDays(count), SqlDbType.Date)
                                Param(10) = Mainclass.CreateSqlParameter("@EVT_CREATED_DT", Format(Date.Now, "dd/MMM/yyyy"), SqlDbType.Date)
                                Param(11) = Mainclass.CreateSqlParameter("@EVT_LAST_MODIFIED_DT", DBNull.Value, SqlDbType.Date)
                                Param(12) = Mainclass.CreateSqlParameter("@EVT_CREATED_BY", Session("sUsr_name"), SqlDbType.VarChar)
                                Param(13) = Mainclass.CreateSqlParameter("@EVT_LAST_MODIFIED_BY", DBNull.Value, SqlDbType.Date)
                                If time1.ToString <> "" Then
                                    Param(14) = Mainclass.CreateSqlParameter("@EVT_STARTTIME", time1.ToString, SqlDbType.VarChar)
                                Else
                                    Param(14) = Mainclass.CreateSqlParameter("@EVT_STARTTIME", DBNull.Value, SqlDbType.VarChar)
                                End If
                                If time2.ToString <> "" Then
                                    Param(15) = Mainclass.CreateSqlParameter("@EVT_ENDTIME", time2.ToString, SqlDbType.VarChar)
                                Else
                                    Param(15) = Mainclass.CreateSqlParameter("@EVT_ENDTIME", DBNull.Value, SqlDbType.VarChar)
                                End If

                                Param(16) = Mainclass.CreateSqlParameter("@hid", hid, SqlDbType.Int)

                                'SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[SaveEventsTranDetails_new]", Param) 'SaveEventsTranDetails
                                SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[SaveEventsTranDetails]", Param)
                                Param(17) = Mainclass.CreateSqlParameter("@EVT_EVT_ID", ParameterDirection.Output, SqlDbType.BigInt)
                                hid = Param(17).Value
                            Next

                        Next

                        strans.Commit()
                        lblError.Text = "Record Saved Successfully"
                    End If
                    Call cleardata()

                    Call BindGrid()
                    'Else
                    '    lbltimeerror.Text = "Start time Should be Less than End Time "
                    '    Call BindGrid()
                    'End If

                Else
                    'invalid dates
                    lblError.Text = "Start Date Should be less than End Date "

                    Call BindGrid()
                End If
            Else
                'invalid text 
            End If


            'Call cleardata()

            Call BindGrid()

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try


    End Sub

    Protected Sub gvEventsEnty_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvEventsEnty.PageIndexChanging
        Me.gvEventsEnty.PageIndex = e.NewPageIndex
        Call BindGrid()
    End Sub

    Protected Sub lnkBtnEdit_Click(sender As Object, e As EventArgs)
        Dim lblEVT_EVT_ID As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                        GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        lblEVT_EVT_ID = TryCast(gvRow.Cells(0).FindControl("lblEVT_EVT_ID"), Label)
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        ViewState("EVT_EVT_ID") = lblEVT_EVT_ID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable

        Dim Param(1) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@BSUID", CStr(Session("sBsuid")), SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@EVT_EVT_ID", lblEVT_EVT_ID.Text, SqlDbType.BigInt)

        ds1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].[GetEventsTranEntryDetails]", Param)
        dt1 = ds1.Tables(0)
        If dt1.Rows.Count > 0 Then
            'For i As Integer = 1 To dt1.Rows.Count - 1



            txtDescrip.Text = dt1.Rows(0).Item("ECM_DESCR")
            txtRemarks.Text = dt1.Rows(0).Item("EVT_REMARKS")
            txtStartDate.Text = dt1.Rows(0).Item("EVT_START_DT")
            txtEnddate.Text = dt1.Rows(0).Item("EVT_END_DT")
            ddlStatus.SelectedItem.Text = dt1.Rows(0).Item("ECM_IS_ACTIVE")
            ddlCategory.SelectedItem.Value = dt1.Rows(0).Item("EVT_ECM_ID")
            ddlAcademicYear.SelectedItem.Value = dt1.Rows(0).Item("EVT_ACD_ID")
            cmbGrade.SelectedItem.Value = dt1.Rows(0).Item("EVT_GRM_ID")

            ''ddlSection.SelectedItem.Value = dt1.Rows(0).Item("EVT_SCT_ID")
            ' Next
        End If
        btnSave.Text = "Update"
    End Sub

    Protected Sub lnkBtnEdit_Click1(sender As Object, e As EventArgs)
        Dim lblEVT_EVT_ID As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent.Parent.Parent,  _
                                        GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        lblEVT_EVT_ID = TryCast(gvRow.Cells(0).FindControl("lblEVT_EVT_ID"), Label)
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        ViewState("EVT_EVT_ID") = lblEVT_EVT_ID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable

        Dim Param(1) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@BSUID", CStr(Session("sBsuid")), SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@EVT_EVT_ID", Convert.ToInt32(lblEVT_EVT_ID.Text), SqlDbType.BigInt)
        ds1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].[GetEventsTranEntryDetails]", Param)
        dt1 = ds1.Tables(0)
        'Dim row As DataRow
        ''Dim str2 As String




        'For Each row In dt1.Rows
        '    'dt_img.Rows(0).Item("ROL_ID")
        '    Dim str1 As String = row("EVT_GRM_ID")
        '    str1 = str1


        '    Dim row2 As DataRow
        '    For Each row2 In ds2.Tables(0).Rows
        '        Dim str2 As String = row2("EVT_GRM_ID")
        '        If str1 = str2 Then
        '            chkUserRoles.Items.FindByValue(str2).Selected = True


        '        End If
        '    Next

        'Next


        If dt1.Rows.Count > 0 Then

            txtDescrip.Text = dt1.Rows(0).Item("EVT_DESCR")
            txtRemarks.Text = dt1.Rows(0).Item("EVT_REMARKS")
            txtStartDate.Text = dt1.Rows(0).Item("EVT_START_DT")
            txtStartDate.Text = Format(dt1.Rows(0).Item("EVT_START_DT"), "dd/MMM/yyyy")
            txtEnddate.Text = dt1.Rows(0).Item("EVT_END_DT")
            txtEnddate.Text = Format(dt1.Rows(0).Item("EVT_END_DT"), "dd/MMM/yyyy")
            '  ddlStatus.SelectedItem.Text = dt1.Rows(0).Item("EVT_STATUS")
            BindEventsCategory()
            ddlCategory.Items.FindByValue(dt1.Rows(0).Item("EVT_ECM_ID")).Selected = True
            BindAcademicyear_CLM()
            ddlAcademicYear.Items.FindByValue(dt1.Rows(0).Item("EVT_ACD_ID")).Selected = True
            Dim acadid As String = dt1.Rows(0).Item("EVT_ACD_ID")


            Dim grdid As String = ""

            '  Dim gradecount As Integer = cmbGrade.CheckedItems.Count
            ' Dim selectedcase As Integer = cmbGrade.Items
            'If (gradecount > 0) Then


            '        If (item.Checked = True) Then
            '            If (SelectStgrade = "") Then
            '                SelectStgrade = item.Text
            '                selectedgradeid = item.Value
            '            Else
            '                SelectStgrade = SelectStgrade + "," + item.Text
            '                selectedgradeid = selectedgradeid + "," + item.Value
            '            End If
            '        End If
            'End If

            'For Each s As String In myDepts
            '    For x As Integer = 0 To dtDeptList.Rows.Count - 2
            '        If dtDeptList.Rows(x)(0).ToString() = s Then
            '            cmbDept.SelectedItems.Add(cmbDept.Items(x))
            '        End If
            '    Next
            'Next
            BindGradecmb()
            Dim selectedgradeid1 As String = ""

            For i As Integer = 0 To dt1.Rows.Count - 1
                ' Dim yourStringList As New List(Of [String])()=
                If IsDBNull(dt1.Rows(i).Item("EVT_GRM_ID")) Then
                Else

                    For Each item As RadComboBoxItem In cmbGrade.Items
                        If dt1.Rows(i).Item("EVT_GRM_ID") = item.Value Then
                            item.Checked = True
                        End If
                    Next
                End If
            Next
            'If IsDBNull(dt1.Rows(i).Item("EVT_GRM_ID")) Then
            '    BindGradeforedit(acadid)
            '    ddlSection.Items.Clear()
            'Else
            '    BindGradeforedit(acadid)
            '    cmbGrade.SelectedItem.Value = dt1.Rows(i).Item("EVT_GRM_ID")

            '    cmbGrade.Items.FindItemByValue(dt1.Rows(i).Item("EVT_GRM_ID")).Checked = True

            'End If


            BindEventsStatus()
            ddlStatus.Items.FindByText(dt1.Rows(0).Item("EVT_STATUS")).Selected = True
            Dim dtime As DateTime
            Dim dtime2 As DateTime
            Dim time1 As String
            Dim res As Boolean
            Dim res2 As Boolean
            Dim time2 As String
            Dim test1 As String = dt1.Rows(0).Item("EVT_STARTTIME").ToString
            If dt1.Rows(0).Item("EVT_STARTTIME").ToString = "" Then
                timepicker4.Value = ""
            Else

                res = DateTime.TryParse(dt1.Rows(0).Item("EVT_STARTTIME"), dtime)
                time1 = dtime.ToShortTimeString

                timepicker4.Value = time1.ToString
            End If
            If dt1.Rows(0).Item("EVT_ENDTIME").ToString = "" Then
                timepicker5.Value = ""
            Else
                res2 = DateTime.TryParse(dt1.Rows(0).Item("EVT_ENDTIME"), dtime2)
                time2 = dtime2.ToShortTimeString
                timepicker5.Value = time2.ToString

            End If


            'If IsDBNull(dt1.Rows(0).Item("SCT_DESCR")) Then
            'Else
            '    BindSectionforEdit(acadid, grdid)
            '    ddlSection.Items.FindByText(dt1.Rows(0).Item("SCT_DESCR")).Selected = True
            'End If


        End If
        btnSave.Text = "Update"
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim SelectStgrade As String = ""
        Dim selectedgradeid As String = ""
        Dim totcount As Integer = cmbGrade.Items.Count
        Dim gradecount As Integer = cmbGrade.CheckedItems.Count
        Dim selectedcase As Integer = cmbGrade.FindItemIndexByText("All Grade")
        If (gradecount > 0) Then
            For Each item As RadComboBoxItem In cmbGrade.Items
                If (item.Checked = True) Then
                    If (SelectStgrade = "") Then
                        SelectStgrade = item.Text
                        selectedgradeid = item.Value
                    Else
                        SelectStgrade = SelectStgrade + "," + item.Text
                        selectedgradeid = selectedgradeid + "," + item.Value
                    End If
                End If
            Next
        End If
        Dim gradeArr() As String
        gradeArr = selectedgradeid.Split(",")
        'Dim strArr() As String
        Dim totalC = gradeArr.Length
        Dim test As Integer
        'strArr = SELStudid.Split(",")
        For grcount = 0 To gradeArr.Length - 1
            test = gradeArr(grcount)
        Next

    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Call cleardata()
        btnSave.Text = "Save"
    End Sub
End Class
