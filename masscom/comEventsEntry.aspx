﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="comEventsEntry.aspx.vb" Inherits="masscom_comEventsEntry" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="../cssfiles/timepicki.css" rel="stylesheet" />

   <link href="../FolderGallery/css/main-teacher.css" rel="stylesheet" />
    <%-- <link href="../FolderGallery/css/all.css" rel="stylesheet" />
    <link href="../cssfiles/EnquiryStyle.css" rel="stylesheet" />
    <link href="../FolderGallery/css/component.css" rel="stylesheet" />--%>
    <style type="text/css">
        .myclass input[type='text'] {
            color: #3a3a3a;
        }
   table .table-row tr:first-child, .gridheader_pop {
    background: rgba(0,0,0,0.04) !important;
    height: 40px !important;
    min-height: 100% !important;
    }
         .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 100%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbReadOnly .rcbArrowCellRight {
            /* background-position: 3px -10px; */
            border: solid black;
            border-width: 0 1px 1px 0;
            display: inline-block;
            padding: 0px;
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            width: 7px;
            height: 7px;
            overflow: hidden;
            margin-top: 15px;
            margin-left: -15px;
        }
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 22px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}
        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }
        .text-bold {
    font-weight: 600 !important;
}
        table td input[type=image] {
            border-radius:4px !important;
        }
        .grid-thumbnail {
    height: 50px;
    width: 50px;
    border: 2px solid #8dc24c !important;
    border-radius: 4px !important;
    max-height: 50px !important;
    overflow: hidden !important;
}
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Academic / Event Calendar
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">

            <div class="container-fluid p-0 mt-2 mb-4">

                <div class="container-fluid p-0 mt-2 mb-4 clearfix position-relative teachers-lockers">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div class="card min-height">
                                <div class="card-body">


                                    <!-- library content start -->
                                    <div class="library-data column-padding pt-3 pb-3">
                                        <div id="library" class="row view-group">
                                            <div class="col-lg-12 col-md-12 col-12">

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 text-left">
                                                        <table width="100%">
                                                            <tr valign="bottom">
                                                                <td align="left" colspan="3" valign="bottom">
                                                                    <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Basic Selection</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12 mb-3" style="padding: 10px; border: 1px solid #e5ebdd;">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" width="20%"><span class="field-label">Category</span><asp:Label ID="lblasterick1" runat="server" Text="*" ForeColor="red"></asp:Label></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true">
                                                                            </asp:DropDownList><br />
                                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator44" ControlToValidate="ddlCategory" ErrorMessage="Please select a category" ValidationGroup="preview" InitialValue="0" />
                                                                        </td>
                                                                        <td align="left" width="20%"><span class="field-label">Academic year</span><asp:Label ID="Label5" runat="server" Text="*" ForeColor="red"></asp:Label></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                                                            </asp:DropDownList><br />
                                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlAcademicYear" ErrorMessage="Please select Academic year" ValidationGroup="preview" InitialValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" width="20%"><span class="field-label">Grade </span></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" Visible="false">
                                                                            </asp:DropDownList>
                                                                            <telerik:RadComboBox ID="cmbGrade" runat="server" Width="80%" RenderMode="Lightweight" Localization-CheckAllString="All Grade" CheckBoxes="true" EnableCheckAllItemsCheckBox="true"></telerik:RadComboBox>
                                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlGrade" ErrorMessage="Please select grade" ValidationGroup="preview" Enabled="false" />
                                                                        </td>
                                                                        <td align="left" width="20%"><span class="field-label">Event Name</span><asp:Label ID="Label1" runat="server" Text="*" ForeColor="red"></asp:Label></td>
                                                                        <td align="left" width="30%">

                                                                            <asp:TextBox ID="txtDescrip" runat="server"></asp:TextBox><br />
                                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtDescrip" ErrorMessage="Please Description" ValidationGroup="preview" />
                                                                        </td>

                                                                    </tr>

                                                                    <tr style="display: none;">
                                                                        <td align="left" width="20%"><span class="field-label">Section</span></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true" Visible="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>

                                                                        </td>

                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Select Date & Time</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12 mb-3" style="padding: 10px; border: 1px solid #e5ebdd;">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" width="20%"><span class="field-label">Start Date</span><asp:Label ID="Label2" runat="server" Text="*" ForeColor="red"></asp:Label></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox><br />
                                                                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                                                PopupButtonID="imgEventDate" TargetControlID="txtStartDate">
                                                                            </ajaxToolkit:CalendarExtender>
                                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtStartDate" ErrorMessage="Please select a start date" ValidationGroup="preview" />
                                                                        </td>
                                                                        <td align="left" width="20%"><span class="field-label">End Date</span><asp:Label ID="Label3" runat="server" Text="*" ForeColor="red"></asp:Label></td>
                                                                        <td align="left" width="30%">

                                                                            <asp:TextBox ID="txtEnddate" runat="server"></asp:TextBox><br />

                                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                                                PopupButtonID="imgEventDate" TargetControlID="txtEnddate">
                                                                            </ajaxToolkit:CalendarExtender>


                                                                            <%--<asp:CompareValidator ID="CompareValidator1" ValidationGroup="preview" ForeColor="Red"
                                runat="server" ControlToValidate="txtStartDate" ControlToCompare="txtEnddate"
                                Operator="LessThan" Type="Date" ErrorMessage="Start date must be less than End date."></asp:CompareValidator>--%>
                                                                            <%--<asp:CompareValidator id="cvtxtStartDate" runat="server"  ControlToCompare="txtStartDate" cultureinvariantvalues="true" 
                                display="Dynamic" enableclientscript="true"  
       ControlToValidate="txtEnddate" 
     ErrorMessage="Start date must be earlier than finish date"
     type="Date" setfocusonerror="true" Operator="LessThanEqual" 
     text="Start date must be earlier than End date"></asp:CompareValidator>--%>
                                                                            <%--<asp:CompareValidator ID="cmpVal1" ControlToCompare="txtStartDate" 
         ControlToValidate="txtEnddate" Type="Date" Operator="GreaterThan"   
         ErrorMessage="*Invalid Data" runat="server"></asp:CompareValidator>--%>
                                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtEnddate" ErrorMessage="Please select a end date" ValidationGroup="preview" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" width="20%"><span class="field-label">Start Time</span></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:UpdatePanel ID="updtime1" runat="server">
                                                                                <ContentTemplate>
                                                                                    <input id="timepicker4" type="text" name="timepicker4" runat="server" />
                                                                                    <%-- <telerik:RadTimePicker ID="RadTimePicker2" runat="server"></telerik:RadTimePicker>--%>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="timepicker4" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>

                                                                        </td>
                                                                        <td align="left" width="20%"><span class="field-label">End Time</span><asp:Label ID="lbltimeerror" runat="server" EnableViewState="False"></asp:Label></td>
                                                                        <td align="left" width="30%">
                                                                            <input id="timepicker5" type="text" runat="server" onchange="test();" />
                                                                            <br />
                                                                            <%-- <asp:UpdatePanel ID="updtime2" runat="server">
                                <ContentTemplate>
                                   
                                </ContentTemplate>
                               <%-- <Triggers>
                                    <asp:PostBackTrigger ControlID="timepicker5" />
                                </Triggers
                            </asp:UpdatePanel>--%>
                                                                            <%-- <telerik:RadTimePicker ID="RadTimePicker1" runat="server"></telerik:RadTimePicker>--%>

                           
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Select Status</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12 mb-3" style="padding: 10px; border: 1px solid #e5ebdd;">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" width="20%"><span class="field-label">Status</span><asp:Label ID="Label4" runat="server" Text="*" ForeColor="red"></asp:Label></td>
                                                                        <td align="left" width="30%">
                                                                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true">
                                                                            </asp:DropDownList><br />
                                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="ddlStatus" ErrorMessage="Please select status" ValidationGroup="preview" />
                                                                        </td>


                                                                        <td width="20%"></td>
                                                                        <td width="30%"></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center" colspan="5">
                                                                    <asp:TextBox ID="txtTotaldays" runat="server" Visible="false"></asp:TextBox>
                                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="preview" />&nbsp;&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                                                    <asp:Button ID="Button1" runat="server" CssClass="button" Text="Tst" Visible="false" />&nbsp;&nbsp;           
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <asp:GridView ID="gvEventsEnty" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords." AllowPaging="true"
                                                            PageSize="20">
                                                            <RowStyle CssClass="griditem" HorizontalAlign="Center" />
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="SNO" Visible="false">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label ID="lblEVT_ID" runat="server" Text='<%# Bind("EVT_ID")%>' visible="false"></asp:Label>
                                                        <asp:Label ID="lblSNO" runat="server" Text='<%# Bind("SNo")%>' ></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Category">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("ECM_DESCR")%>' align="left"></asp:Label>

                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="text-bold" />
                                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("EVT_DESCR")%>' align="left"></asp:Label>

                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("EVT_STATUS")%>' align="left"></asp:Label>

                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Start Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("EVT_START_DT", "{0:dd/MMM/yyyy}")%>' align="left"></asp:Label>

                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="End Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("EVT_END_DT", "{0:dd/MMM/yyyy}")%>' align="left"></asp:Label>
                                                                        <asp:Label ID="lblEVT_EVT_ID" runat="server" Text='<%# Bind("EVT_EVT_ID")%>' Visible="false" align="left"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Details">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel ID="updpnl" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton ID="lnkBtnEdit" Text="Edit" runat="server" align="center" OnClick="lnkBtnEdit_Click1"></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lnkBtnEdit" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                    <ItemStyle></ItemStyle>
                                                                </asp:TemplateField>

                                                                <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>
                                                            </Columns>
                                                            <HeaderStyle cssclass="gridheader_pop"/>
                                                            <AlternatingRowStyle />
                                                        </asp:GridView>
                                                    </div>
                                                </div>




                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <%--<uc2:usrMessageBar runat="server" ID="usrMessageBar" />--%>
    </div>
    <script type="text/javascript" language="javascript">

        function OnClientItemChecked(sender, eventArgs) {
            //var item = eventArgs.get_item();
            //sender.set_text("You checked " + item.get_text());
            alert(1);
            var index, count;
            count = sender.get_items().get_count();
            alert(count);
            if (eventArgs._item._text == "All Grade") {
                alert(2);
                for (index = 1; index < count; index++) {
                    sender.get_items()._array[index].check();
                }
                eventArgs.set_cancel(true);
                sender.set_text("All Grade");
            }






        }
    </script>
    <script>
        // function pageLoad(sender, args) {
        $(document).ready(function () {
            $('#<%=txtStartDate.ClientID %>').change(function () {
                       //alert(1);
                       $('#<%=txtEnddate.ClientID %>').val($('#<%=txtStartDate.ClientID %>').val());
            });

        });
          // }
    </script>
    <script>
        function pageLoad(sender, args) {
            $(document).ready(function () {

                $('#<%=timepicker4.ClientID%>').timepicki({ custom_classes: "myclass" });
                $('#<%=timepicker5.ClientID%>').timepicki({ custom_classes: "myclass" });
            });
        }
	</script>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/timepicki.js"></script>
    <script type="text/javascript" lang="javascript">

        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>

