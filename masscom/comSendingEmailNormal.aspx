<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSendingEmailNormal.aspx.vb" Inherits="masscom_comSendingEmailNormal" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Email</title>
    <base target="_self" />
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script src="Javascript/SendingEmailEXE.js" type="text/javascript" ></script>
</head> 
<body onload="onloadFunction();">
    <form id="form1" runat="server">
      <div align="center">
          <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
          </ajaxToolkit:ToolkitScriptManager>
         <br />
         <br />
         <br />
          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
              <ContentTemplate>
            <table id="TReport" runat="server" border="1" bordercolor="#1b80b6" cellpadding="5" width="310" cellspacing="0">
                <tr>
                    <td align="center" style="width: 308px">
                        &nbsp;<asp:Image ID="Image1" ImageUrl="~/Images/Progress.gif" Visible="false"   runat="server" />&nbsp;<br />
                        &nbsp;<asp:Button ID="BtnSend" runat="server" CssClass="button" OnClientClick="javascript:window.setTimeout('BindData()',2000);" Width="60px"  Text="Send" /><asp:Button ID="BtnStopClose" runat="server" CssClass="button" OnClientClick="javascript:alert('Please see the reports for details.');window.close();" Width="60px" Text="Close" /></td>
                </tr>
                <tr>
                    <td align="center" style="width: 308px">
                        <asp:Panel ID="Panel1" runat="server"   Height="21px" Width="140px">
                              <center><asp:TextBox ID="TxtProgressBar" runat="server" Height="20px" ReadOnly="True" ></asp:TextBox></center>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="subheader_img" style="width: 308px">
                        Report</td>
                </tr>
                <tr>
                    <td align="left" style="width: 308px">
                        <table>
                            <tr>
                                <td>
                                    Current Count</td>
                                <td>
                                    <div id="Div2">
                                        0</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Total Emails
                                </td>
                                <td>
                                    <div id="Div4" runat="server">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Successfully Sent</td>
                                <td>
                                <div id="Div3">0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sending Failed</td>
                                <td>
                                <div id="Div6">0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Start Time</td>
                                <td>
                                    <div id="Div5">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    End Time</td>
                                <td>
                                 <div id="Div7">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:LinkButton ID="Linkrefresh" runat="server" OnClientClick="javascript:BindData();return false;">Refresh</asp:LinkButton></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
                        <div id="Div1" style="visibility: hidden"></div> <asp:HiddenField ID="HiddenCSE_ID"  runat="server" />
          <asp:HiddenField ID="HiddenEML_ID"  runat="server" />
          <asp:HiddenField ID="HiddenPath" Value="0" runat="server" /><asp:HiddenField ID="HiddenGroupid" Value="0" runat="server" />

          <asp:HiddenField ID="HiddenSendingId" Value="0" runat="server" />
          <asp:HiddenField ID="HiddenEmpID" Value="0" runat="server" />
          <asp:HiddenField ID="HiddenBsuid" Value="0" runat="server" />
              </ContentTemplate>
          </asp:UpdatePanel>


        </div>
    </form>
</body>
</html>
