Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_comManageSmsReportsView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Try
                CheckMenuRights()
                Session("gm") = Nothing
                Dim EncDec As New Encryption64
                Hiddengroupid.Value = EncDec.Decrypt(Request.QueryString("Groupid").Replace(" ", "+"))
                Hiddensmsid.Value = EncDec.Decrypt(Request.QueryString("Messageid").Replace(" ", "+"))
                BindGrid()
            Catch ex As Exception


            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000040") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select  student_m.stu_no, student_m.stu_id,student_m.STU_FIRSTNAME,student_m.STU_MIDNAME,student_m.STU_LASTNAME  ,student_m.STU_PRIMARYCONTACT " & _
                        ",(case student_m.STU_PRIMARYCONTACT when 'F' then sd.STS_FEMAIL  when 'M' then sd.STS_MEMAIL  when 'G' then sd.STS_GEMAIL  end) as ParentEmail  " & _
                        ",(case student_m.STU_PRIMARYCONTACT when 'F' then sd.STS_FMOBILE  when 'M' then sd.STS_MMOBILE  when 'G' then sd.STS_GMOBILE  end) as ParentMobile  " & _
                        ",(case student_m.STU_PRIMARYCONTACT when 'F' then sd.STS_FFIRSTNAME + ' ' + sd.STS_FLASTNAME when 'M' then sd.STS_MFIRSTNAME + ' ' + sd.STS_MLASTNAME when 'G' then sd.STS_GFIRSTNAME + ' ' + sd.STS_GLASTNAME end) as parent_name " & _
                        " ,cm.comp_name  as comp_name ,lo.log_status   from student_m " & _
                        " inner join student_d sd on student_m.stu_id =sd.sts_stu_id " & _
                        " left join comp_listed_m cm on cm.comp_id in (select (case student_m.STU_PRIMARYCONTACT when 'F' then sd.sts_f_comp_id when 'M' then sd.sts_m_comp_id when 'G' then sd.sts_g_comp_id end ) as id ) " & _
                        " inner join com_log_table lo on student_m.stu_id=lo.log_stu_id and lo.log_cgr_id='" & Hiddengroupid.Value & "' and log_cms_id='" & Hiddensmsid.Value & "' order by lo.log_entry_date "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Grdstatus.DataSource = ds
        Grdstatus.DataBind()
        Dim companydrop As DropDownList = DirectCast(Grdstatus.HeaderRow.FindControl("DropSearch1"), DropDownList)
        str_query = "select comp_id,comp_name from comp_listed_m"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        companydrop.DataSource = ds
        companydrop.DataTextField = "COMP_NAME"
        companydrop.DataValueField = "COMP_ID"
        companydrop.DataBind()
        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "-1"
        companydrop.Items.Insert(0, list)

    End Sub

    Protected Sub Grdstatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grdstatus.PageIndexChanging
        Grdstatus.PageIndex = e.NewPageIndex
        If Session("gm") Is Nothing Then
            BindGrid()
        Else
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sql_query As String = ""
            Dim ds As DataSet
            ds = Session("gm")
            Grdstatus.DataSource = ds
            Grdstatus.DataBind()
            Dim companydrop As DropDownList = DirectCast(Grdstatus.HeaderRow.FindControl("DropSearch1"), DropDownList)
            sql_query = "select comp_id,comp_name from comp_listed_m"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            companydrop.DataSource = ds
            companydrop.DataTextField = "COMP_NAME"
            companydrop.DataValueField = "COMP_ID"
            companydrop.DataBind()
            Dim list As New ListItem
            list.Text = "ALL"
            list.Value = "-1"
            companydrop.Items.Insert(0, list)
        End If
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If Grdstatus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = Grdstatus.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If Grdstatus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = Grdstatus.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If Grdstatus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = Grdstatus.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If Grdstatus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = Grdstatus.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If Grdstatus.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = Grdstatus.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function
    Public Function GetSearchString1(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = "  " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "   " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = "  " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = "  " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = "  " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = "  " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function
    Public Sub Search()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select  student_m.stu_no, student_m.stu_id,student_m.STU_FIRSTNAME,student_m.STU_MIDNAME,student_m.STU_LASTNAME  ,student_m.STU_PRIMARYCONTACT " & _
                        ",(case student_m.STU_PRIMARYCONTACT when 'F' then sd.STS_FEMAIL  when 'M' then sd.STS_MEMAIL  when 'G' then sd.STS_GEMAIL  end) as ParentEmail  " & _
                        ",(case student_m.STU_PRIMARYCONTACT when 'F' then sd.STS_FMOBILE  when 'M' then sd.STS_MMOBILE  when 'G' then sd.STS_GMOBILE  end) as ParentMobile  " & _
                        ",(case student_m.STU_PRIMARYCONTACT when 'F' then sd.STS_FFIRSTNAME + ' ' + sd.STS_FLASTNAME when 'M' then sd.STS_MFIRSTNAME + ' ' + sd.STS_MLASTNAME when 'G' then sd.STS_GFIRSTNAME + ' ' + sd.STS_GLASTNAME end) as parent_name " & _
                        " ,cm.comp_name  as comp_name ,lo.log_status   from student_m " & _
                        " inner join student_d sd on student_m.stu_id =sd.sts_stu_id " & _
                        " left join comp_listed_m cm on cm.comp_id in (select (case student_m.STU_PRIMARYCONTACT when 'F' then sd.sts_f_comp_id when 'M' then sd.sts_m_comp_id when 'G' then sd.sts_g_comp_id end ) as id ) " & _
                        " inner join com_log_table lo on student_m.stu_id=lo.log_stu_id and lo.log_cgr_id='" & Hiddengroupid.Value & "' and log_cms_id='" & Hiddensmsid.Value & "' "

        Dim strSidsearch As String()
        Dim SearchFilter As String = ""

        Dim Studentid As String = DirectCast(Grdstatus.HeaderRow.FindControl("txtSearch1"), TextBox).Text.Trim()
        Dim studentname As String = DirectCast(Grdstatus.HeaderRow.FindControl("txtSearch2"), TextBox).Text.Trim()
        Dim Parentname As String = DirectCast(Grdstatus.HeaderRow.FindControl("txtSearch3"), TextBox).Text.Trim()
        ''Dim email As String = DirectCast(Grdstatus.HeaderRow.FindControl("txtSearch4"), TextBox).Text.Trim()
        Dim mobile As String = DirectCast(Grdstatus.HeaderRow.FindControl("txtSearch5"), TextBox).Text.Trim()
        Dim company As DropDownList = DirectCast(Grdstatus.HeaderRow.FindControl("DropSearch1"), DropDownList)

        If Studentid <> "" Then
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("student_m.stu_no", Studentid.Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("student_m.stu_no", Studentid.Trim(), strSidsearch(0))
            End If

        End If

        If studentname <> "" Then
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("student_m.stu_firstname + student_m.stu_midname + student_m.stu_lastname", studentname.Replace(" ", "").Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("student_m.stu_firstname + student_m.stu_midname + student_m.stu_lastname", studentname.Replace(" ", "").Trim(), strSidsearch(0))
            End If

        End If

        If Parentname <> "" Then
            strSidsearch = h_Selected_menu_3.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("sd.sts_ffirstname + sd.sts_fmidname + sd.sts_flastname+sd.sts_mfirstname + sd.sts_mmidname + sd.sts_mlastname + sd.sts_gfirstname + sd.sts_gmidname + sd.sts_glastname", Parentname.Replace(" ", "").Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("sd.sts_ffirstname + sd.sts_fmidname + sd.sts_flastname+sd.sts_mfirstname + sd.sts_mmidname + sd.sts_mlastname + sd.sts_gfirstname + sd.sts_gmidname + sd.sts_glastname", Parentname.Replace(" ", "").Trim(), strSidsearch(0))
            End If

        End If

        'If email <> "" Then
        '    strSidsearch = h_Selected_menu_4.Value.Split("__")
        '    If SearchFilter = "" Then
        '        SearchFilter = SearchFilter & GetSearchString1("sd.sts_femail +sd.sts_memail + sd.sts_gemail", email.Replace(" ", "").Trim(), strSidsearch(0))
        '    Else
        '        SearchFilter = SearchFilter & GetSearchString("sd.sts_femail +sd.sts_memail + sd.sts_gemail", email.Replace(" ", "").Trim(), strSidsearch(0))
        '    End If

        'End If
        If mobile <> "" Then
            strSidsearch = h_Selected_menu_5.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("sd.sts_fmobile +sd.sts_mmobile+sd.sts_gmobile", mobile.Replace(" ", "").Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("sd.sts_fmobile +sd.sts_mmobile+sd.sts_gmobile", mobile.Replace(" ", "").Trim(), strSidsearch(0))
            End If

        End If

        If company.SelectedIndex > 0 Then
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & " (sd.sts_f_comp_id like coalesce('" & company.SelectedValue & "',sd.sts_f_comp_id) or sd.sts_m_comp_id like coalesce('" & company.SelectedValue & "',sd.sts_m_comp_id) or sd.sts_g_comp_id like coalesce('" & company.SelectedValue & "',sd.sts_g_comp_id))"
            Else
                SearchFilter = SearchFilter & " and (sd.sts_f_comp_id like coalesce('" & company.SelectedValue & "',sd.sts_f_comp_id) or sd.sts_m_comp_id like coalesce('" & company.SelectedValue & "',sd.sts_m_comp_id) or sd.sts_g_comp_id like coalesce('" & company.SelectedValue & "',sd.sts_g_comp_id))"
            End If

        End If

        If SearchFilter <> "" Then
            str_query = str_query & " and " & SearchFilter
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Grdstatus.DataSource = ds
        Grdstatus.DataBind()
        Session("gm") = ds
        If ds.Tables(0).Rows.Count > 0 Then
            Dim companydrop As DropDownList = DirectCast(Grdstatus.HeaderRow.FindControl("DropSearch1"), DropDownList)
            str_query = "select comp_id,comp_name from comp_listed_m"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            companydrop.DataSource = ds
            companydrop.DataTextField = "COMP_NAME"
            companydrop.DataValueField = "COMP_ID"
            companydrop.DataBind()
            Dim list As New ListItem
            list.Text = "ALL"
            list.Value = "-1"
            companydrop.Items.Insert(0, list)
        End If

    End Sub

    Public Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Search()
    End Sub
    Protected Sub ddlsearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Search()
    End Sub
End Class
