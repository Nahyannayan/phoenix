<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comListGroups.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="masscom_comListGroups" %>

<%@ Register Src="UserControls/comListGroups.ascx" TagName="comListGroups" TagPrefix="uc1" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTopTitle" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <uc1:comListGroups ID="ComListGroups1" runat="server"></uc1:comListGroups>

            </div>
        </div>
    </div>

</asp:Content>
