<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SendingReportFilter.ascx.vb"
    Inherits="masscom_Reports_UserControls_SendingReportFilter" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


<!-- Bootstrap header files ends here -->

<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Reports</td>
                </tr>
                <tr>
                    <td align="left" width="100%">
                        <table width="100%">
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                                <td align="left" colspan="3" width="80%">
                                    <asp:DropDownList ID="ddbsu" runat="server">
                                    </asp:DropDownList></td>

                            </tr>

                            <tr>
                                <td align="left" width="20%"><span class="field-label">Document Type</span></td>

                                <td align="left" width="30%">
                                    <asp:DropDownList ID="DropType" runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="SMS" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="News Letter Email" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Plain Text Email" Value="2"></asp:ListItem>
                                    </asp:DropDownList></td>
                                <td align="left" width="20%"><span class="field-label">Method</span></td>

                                <td align="left" width="30%">
                                    <asp:DropDownList ID="DropMethod" runat="server">
                                        <asp:ListItem Selected="True" Text="NORMAL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="SCHEDULE" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="BOTH" Value="2"></asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">From Date</span></td>

                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;<asp:Image ID="Image1"
                                        runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                                <td align="left" width="20%"><span class="field-label">To Date</span></td>

                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;<asp:Image ID="Image2"
                                        runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                            </tr>

                            <tr id="Tr1" runat="server">
                                <td align="left" width="20%"><span class="field-label">Cost per SMS</span></td>

                                <td align="left" width="30%">
                                    <asp:TextBox ID="TxtAmount" Text=".08" Width="35px" MaxLength="5" runat="server"></asp:TextBox></td>
                                <td align="left" width="20%"></td>
                                <td align="left" width="30%"></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btngeneratereport" runat="server" CssClass="button" Text="Generate Report" OnClick="btngeneratereport_Click" />
                                    &nbsp;
                                    <asp:Button ID="btnDownloadExcel" runat="server" CssClass="button" Text="Download Excel" />
                                </td>
                            </tr>
                        </table>
                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                            TargetControlID="txtFromDate">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
                            TargetControlID="txtToDate">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:FilteredTextBoxExtender ID="F1" runat="server" FilterType="Custom,Numbers"
                            TargetControlID="TxtAmount" ValidChars=".">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>
