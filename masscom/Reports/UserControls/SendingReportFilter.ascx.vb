Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class masscom_Reports_UserControls_SendingReportFilter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadExcel)
    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")

        'if user is super admin give access to all the Business Unit
        If tblbUSuper = True Then
            Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                'create a list item to bind records from reader to dropdownlist ddlBunit
                Dim di As ListItem
                ddbsu.Items.Clear()
                'check if it return rows or not
                If BUnitreaderSuper.HasRows = True Then
                    While BUnitreaderSuper.Read
                        di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                        'adding listitems into the dropdownlist
                        ddbsu.Items.Add(di)

                        For ItemTypeCounter = 0 To ddbsu.Items.Count - 1
                            'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                            If ddbsu.Items(ItemTypeCounter).Value = buser_id Then
                                ddbsu.SelectedIndex = ItemTypeCounter
                            End If
                        Next
                    End While
                End If
            End Using

        Else
            'if user is not super admin get access to the selected  Business Unit
            Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                Dim di As ListItem
                ddbsu.Items.Clear()
                If BUnitreader.HasRows = True Then
                    While BUnitreader.Read
                        di = New ListItem(BUnitreader(2), BUnitreader(0))
                        ddbsu.Items.Add(di)
                        For ItemTypeCounter = 0 To ddbsu.Items.Count - 1
                            'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                            If ddbsu.Items(ItemTypeCounter).Value = buser_id Then
                                ddbsu.SelectedIndex = ItemTypeCounter
                            End If
                        Next
                    End While
                End If
            End Using
        End If


    End Sub

    Protected Sub DropType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropType.SelectedIndexChanged

        If DropType.SelectedValue = 0 Then
            Tr1.Visible = True
        Else
            Tr1.Visible = False
        End If


    End Sub

    Protected Sub btngeneratereport_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim param As New Hashtable
        ''Default Parameters 
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))


        If txtFromDate.Text.Trim() <> "" Then
            param.Add("@FROM_DATE", txtFromDate.Text.Trim())
        Else
            param.Add("@FROM_DATE", DBNull.Value)
        End If

        If txtToDate.Text.Trim() <> "" Then
            param.Add("@TODATE", txtToDate.Text.Trim())
        Else
            param.Add("@TODATE", DBNull.Value)
        End If

        param.Add("@BSU_ID_FILTER", ddbsu.SelectedValue)

        If DropType.SelectedValue = 0 Then 'SMS'
            param.Add("@ReportHeader", "From : " & txtFromDate.Text & " :: To : " & txtToDate.Text & " :: Cost/SMS : " & TxtAmount.Text)
            If DropType.SelectedValue = 0 Then ''SMS
                If TxtAmount.Text <> "" Then
                    param.Add("@COST_PER_SMS", Convert.ToDouble(TxtAmount.Text))
                Else
                    param.Add("@COST_PER_SMS", DBNull.Value)
                End If
            End If
        ElseIf DropType.SelectedValue = 1 Then ''News Letter
            param.Add("@ReportHeader", "From : " & txtFromDate.Text & " :: To : " & txtToDate.Text)


        ElseIf DropType.SelectedValue = 2 Then ''Plain Text
            param.Add("@ReportHeader", "From : " & txtFromDate.Text & " :: To : " & txtToDate.Text)

        End If





        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If DropType.SelectedValue = 0 Then ''SMS

                If DropMethod.SelectedValue = 0 Then ''Normal
                    .reportPath = Server.MapPath("~/masscom/Reports/CrystalReports/comSmsReportsNormal.rpt")
                Else ''Schedule
                    .reportPath = Server.MapPath("~/masscom/Reports/CrystalReports/comSmsReportsSchedule.rpt")
                End If

            ElseIf DropType.SelectedValue = 1 Then ''News Letter

                If DropMethod.SelectedValue = 0 Then ''Normal
                    .reportPath = Server.MapPath("~/masscom/Reports/CrystalReports/comNewsletterReportsNormal.rpt")
                Else ''Schedule
                    .reportPath = Server.MapPath("~/masscom/Reports/CrystalReports/comNewsletterReportsSchedule.rpt")
                End If

            ElseIf DropType.SelectedValue = 2 Then ''Plain Text

                If DropMethod.SelectedValue = 0 Then ''Normal
                    .reportPath = Server.MapPath("~/masscom/Reports/CrystalReports/comPlainTextReportsNormal.rpt")
                Else ''Schedule
                    .reportPath = Server.MapPath("~/masscom/Reports/CrystalReports/comPlainTextReportsSchedule.rpt")
                End If

            End If

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub btnDownloadExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadExcel.Click
        DownloadExcelReport()
    End Sub

    Private Sub DownloadExcelReport()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim params(5) As SqlParameter

            Dim ds As New DataSet


            If txtFromDate.Text.Trim() <> "" Then

                params(0) = New SqlClient.SqlParameter("@FROM_DATE", txtFromDate.Text.Trim())
            Else
                params(0) = New SqlClient.SqlParameter("@FROM_DATE", DBNull.Value)
            End If

            If txtToDate.Text.Trim() <> "" Then
                params(1) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text.Trim())
            Else
                params(1) = New SqlClient.SqlParameter("@TODATE", DBNull.Value)
            End If

            params(2) = New SqlClient.SqlParameter("@BSU_ID_FILTER", ddbsu.SelectedValue)

            If DropType.SelectedValue = 0 Then ''SMS

                If DropMethod.SelectedValue = 0 Then ''Normal
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "SMS_NORMAL")
                ElseIf DropMethod.SelectedValue = 1 Then ''Schedule
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "SMS_SCHEDULE")
                Else ''Both
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "SMS_BOTH")
                End If

            ElseIf DropType.SelectedValue = 1 Then ''News Letter

                If DropMethod.SelectedValue = 0 Then ''Normal
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "NEWSLETTER_NORMAL")
                ElseIf DropMethod.SelectedValue = 1 Then ''Schedule
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "NEWSLETTER_SCHEDULE")
                Else ''Both
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "NEWSLETTER_BOTH")
                End If

            ElseIf DropType.SelectedValue = 2 Then ''Plain Text

                If DropMethod.SelectedValue = 0 Then ''Normal
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "PLAINTEXT_NORMAL")
                ElseIf DropMethod.SelectedValue = 1 Then ''Schedule
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "PLAINTEXT_SCHEDULE")
                Else ''Both
                    params(3) = New SqlClient.SqlParameter("@EXCEL_TYPE", "PLAINTEXT_BOTH")
                End If

            End If
            If DropType.SelectedValue = 0 Then ''SMS
                If TxtAmount.Text <> "" Then
                    params(4) = New SqlClient.SqlParameter("@COST_PER_SMS", Convert.ToDouble(TxtAmount.Text))
                Else
                    params(4) = New SqlClient.SqlParameter("@COST_PER_SMS", DBNull.Value)
                End If
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COM_SENDING_REPORT_EXCEL_DOWNLOAD", params)

            SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            Dim ef As ExcelFile = New ExcelFile

            Dim dtEXCEL As New DataTable
            If ds.Tables IsNot Nothing Then
                dtEXCEL = ds.Tables(0)


                If dtEXCEL.Rows.Count > 0 Then

                    Dim ws As ExcelWorksheet = ef.Worksheets.Add("DATA_EXPORT")

                    ws.InsertDataTable(dtEXCEL, "A1", True)
                    For i As Integer = 0 To dtEXCEL.Columns.Count - 1
                        ws.Cells(0, i).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, i).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Cells(0, i).Style.Font.Weight = ExcelFont.MaxWeight
                    Next
                    Dim stuFilename As String = "Send_Report_" & Today.Now().ToString().Replace("/", "-").Replace(":", "-") & ".xls"

                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                    Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                    Dim pathSave As String
                    pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xls"
                    ef.SaveXls(cvVirtualPath & "StaffExport\" + pathSave)
                    Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                End If
            End If
        Catch ex As Exception
            Dim err As String = ex.Message
        End Try
    End Sub
End Class
