<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSendingReports.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="masscom_comSendingReports" %>

<%@ Register Src="UserControls/comSendingReports.ascx" TagName="comSendingReports"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-email mr-3"></i>Sending Reports
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <div>
                    <uc1:comSendingReports ID="ComSendingReports1" runat="server" />
                </div>

            </div>
        </div>
    </div>

</asp:Content>
