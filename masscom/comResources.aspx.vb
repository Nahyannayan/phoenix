﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic

Partial Class masscom_comResources
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            'btnSave.Attributes.Add("onclick", "return UnpostOthFeecharge(" & h_UnpostFChg.ClientID & ") ")


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ' gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                If Session("sUsr_name") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_DAYEND_PROCESS
                            lblHead.Text = "Day End Process"
                    End Select
                    

                    Page.Form.Attributes.Add("enctype", "multipart/form-data")

                    Call BINDGRID()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try

        End If
    End Sub

    Protected Sub BINDGRID()
        Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("ResourcesCategory").ToString()


        Dim sqlCon_img As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim Param_img(0) As SqlParameter
        Param_img(0) = Mainclass.CreateSqlParameter("@bsuid", Session("sBsuid"), SqlDbType.VarChar)
        ds = SqlHelper.ExecuteDataset(sqlCon_img, "dbo.[GetResourcesCategoryDetails]", Param_img)
        dt = ds.Tables(0)
        gvResourcesCategory.DataSource = dt

        gvResourcesCategory.DataBind()

    End Sub
    Protected Sub ClearData()
        txtCategory.Text = ""
        chkActive.Checked = False
        ImgThumbnail.Visible = False
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            If btnSave.Text = "Update" Then
                If FileUploadControl.HasFile Then
                    Dim filesize As Integer = FileUploadControl.PostedFile.ContentLength
                    Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("ResourcesCategory").ToString()
                    If filesize < 50000 Then '5242880 bytes= 5MB
                        If Not Directory.Exists(PhyPath) Then
                            ' Create the directory.
                            Directory.CreateDirectory(PhyPath)
                        End If
                        Dim filename As String = Path.GetFileName(FileUploadControl.FileName)
                        Dim path1 As String = PhyPath

                        ViewState("filename") = filename

                        FileUploadControl.SaveAs(PhyPath + filename)

                    Else

                        lblError.Text = "File Size should be less than 50KB"
                        Exit Sub
                    End If



                End If


                Dim Param(5) As SqlParameter

                Dim strans As SqlTransaction = Nothing

                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction

                'Convert.ToInt32(ViewState("RCM_ID"))
                Param(0) = Mainclass.CreateSqlParameter("@RCM_ID", Convert.ToInt32(ViewState("RCM_ID")), SqlDbType.Int)
                Param(1) = Mainclass.CreateSqlParameter("@RCM_DESCR", txtCategory.Text, SqlDbType.VarChar)
                Param(2) = Mainclass.CreateSqlParameter("@RCM_IS_ACTIVE", DBNull.Value, SqlDbType.Bit)
                Param(3) = Mainclass.CreateSqlParameter("@RCM_LAST_MODIFIED_BY", Session("sUsr_name"), SqlDbType.VarChar)
                Param(4) = Mainclass.CreateSqlParameter("@RCM_LAST_MODIFIED_DT", Format(Date.Now, "dd/MMM/yyyy"), SqlDbType.Date)
                'Param(5) = Mainclass.CreateSqlParameter("@RCM_THUMBNAIL_PATH", "~/Resources/Category/" + ViewState("filename"), SqlDbType.VarChar)
                If FileUploadControl.HasFile Then
                    Param(5) = Mainclass.CreateSqlParameter("@RCM_THUMBNAIL_PATH", "~/Resources/Category/" + ViewState("filename"), SqlDbType.VarChar)
                Else
                    Param(5) = Mainclass.CreateSqlParameter("@RCM_THUMBNAIL_PATH", DBNull.Value, SqlDbType.VarChar)
                End If

                SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[EditResourcesCategoryByID_New1]", Param)
                strans.Commit()
                lblError.Text = "Updated Successfully"
                btnSave.Text = "Save"
                Call BINDGRID()
                Call ClearData()

            Else
                'save
                If FileUploadControl.HasFile Then
                    Dim filesize As Integer = FileUploadControl.PostedFile.ContentLength
                    Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("ResourcesCategory").ToString()
                    If filesize < 5242880 Then '5242880 bytes= 5MB
                        If Not Directory.Exists(PhyPath) Then
                            ' Create the directory.
                            Directory.CreateDirectory(PhyPath)
                        End If
                        Dim filename As String = Path.GetFileName(FileUploadControl.FileName)
                        Dim path1 As String = PhyPath

                        ViewState("filename") = filename
                        FileUploadControl.SaveAs(PhyPath + filename)
                    Else

                        lblError.Text = "File Size should be less than 5MB"
                        Exit Sub
                    End If

                End If



                Dim Param(8) As SqlParameter

                Dim strans As SqlTransaction = Nothing

                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction

                Param(0) = Mainclass.CreateSqlParameter("@RCM_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                Param(1) = Mainclass.CreateSqlParameter("@RCM_DESCR", txtCategory.Text, SqlDbType.VarChar)
                Param(2) = Mainclass.CreateSqlParameter("@RCM_SHORT_CODE", DBNull.Value, SqlDbType.VarChar)
                Param(3) = Mainclass.CreateSqlParameter("@RCM_IS_ACTIVE", DBNull.Value, SqlDbType.Bit)
                Param(4) = Mainclass.CreateSqlParameter("@RCM_CREATED_BY", Session("sUsr_name"), SqlDbType.VarChar)
                Param(5) = Mainclass.CreateSqlParameter("@RCM_LAST_MODIFIED_BY", "", SqlDbType.VarChar)
                Param(6) = Mainclass.CreateSqlParameter("@RCM_CREATED_DT", Format(Date.Now, "dd/MMM/yyyy"), SqlDbType.Date)
                Param(7) = Mainclass.CreateSqlParameter("@RCM_LAST_MODIFIED_DT", DBNull.Value, SqlDbType.Date)
                If FileUploadControl.HasFile Then
                    Param(8) = Mainclass.CreateSqlParameter("@RCM_THUMBNAIL_PATH", "~/Resources/Category/" + ViewState("filename"), SqlDbType.VarChar)
                Else
                    Param(8) = Mainclass.CreateSqlParameter("@RCM_THUMBNAIL_PATH", DBNull.Value, SqlDbType.VarChar)
                End If

                SqlHelper.ExecuteNonQuery(str_conn, "[dbo].[Saveresourcecategorydetails_New]", Param)


                strans.Commit()
                lblError.Text = "Saved Successfully"
                Call BINDGRID()
                Call ClearData()
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub






    Protected Sub gvResourcesCategory_DataBound(sender As Object, e As EventArgs) Handles gvResourcesCategory.DataBound

    End Sub
    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim myHyperLink As HyperLink = TryCast(e.Row.FindControl("myHyperLinkID"), HyperLink)
        End If
    End Sub

    Protected Sub gvResourcesCategory_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvResourcesCategory.RowDataBound
        Dim PhyPath1 As String = Web.Configuration.WebConfigurationManager.AppSettings("ResourcesCategory").ToString()
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Imgbtn As ImageButton
            Imgbtn = TryCast(e.Row.FindControl("Imgbtn"), ImageButton)
            Dim lblResourceCategorypath As Label = TryCast(e.Row.FindControl("lblResourceCategorypath"), Label)
            'C:\Resources\Category\

            If lblResourceCategorypath.Text = "" Then
                lblnotavli.Visible = True
                Imgbtn.Visible = False
            Else
                Imgbtn.Visible = True
                Dim filename As String = Path.GetFileName(lblResourceCategorypath.Text)
                Dim Fpath1 As String = lblResourceCategorypath.Text.Remove(0, 2)
                Dim finalpath As String = Fpath1.Replace("/", "\")
                Dim BYTES() As Byte = File.ReadAllBytes(PhyPath1 + filename)
                'Imgbtn.ImageUrl = PhyPath1 + filename



                Dim base64String As String = Convert.ToBase64String(BYTES, 0, BYTES.Length)

                Imgbtn.ImageUrl = "data:image/jpeg;base64," + base64String

            End If


        End If

    End Sub



    Protected Sub lnkBtnEdit_Click(sender As Object, e As EventArgs)
        Dim lblRcatID As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                        GridViewRow)
        Dim index As Integer = gvRow.RowIndex
        lblRcatID = TryCast(gvRow.Cells(0).FindControl("lblRcatID"), Label)
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString)
        ViewState("RCM_ID") = lblRcatID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable
        Dim PhyPath1 As String = Web.Configuration.WebConfigurationManager.AppSettings("ResourcesCategory").ToString()
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@RCM_ID", Convert.ToInt32(lblRcatID.Text), SqlDbType.BigInt)
        'ds1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetResourcescategoryBYID", Param)
        ds1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].GetResourcescategoryBYID_New", Param)
        dt1 = ds1.Tables(0)
        If dt1.Rows.Count > 0 Then
            txtCategory.Text = dt1.Rows(0).Item("RCM_DESCR")
            chkActive.Checked = dt1.Rows(0).Item("RCM_IS_ACTIVE")
            If IsDBNull(dt1.Rows(0).Item("RCM_THUMBNAIL_PATH")) Then

            Else
                ImgThumbnail.Visible = True
                Dim thumbnailpath As String = dt1.Rows(0).Item("RCM_THUMBNAIL_PATH")
                Dim filename As String = Path.GetFileName(thumbnailpath)
                Dim Fpath1 As String = thumbnailpath.Remove(0, 2)
                Dim finalpath As String = Fpath1.Replace("/", "\")
                Dim BYTES() As Byte = File.ReadAllBytes(PhyPath1 + filename)
                Dim base64String As String = Convert.ToBase64String(BYTES, 0, BYTES.Length)

                ImgThumbnail.ImageUrl = "data:image/jpeg;base64," + base64String

            End If


        End If
        btnSave.Text = "Update"
    End Sub

    Protected Sub lnkbtnDelete_Click(sender As Object, e As EventArgs)
        Dim lblRcatID As Label
        'Dim linkdelete As LinkButton
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                        GridViewRow)

        Dim index As Integer = gvRow.RowIndex
        lblRcatID = TryCast(gvRow.Cells(0).FindControl("lblRcatID"), Label)
        ' linkdelete = TryCast(gvRow.Cells(0).FindControl("lnkbtnDelete"), LinkButton)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConnc As New SqlConnection(str_conn)
        objConnc.Open()
        ViewState("RCM_ID") = lblRcatID.Text
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable

        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@RCM_ID", Convert.ToInt32(lblRcatID.Text), SqlDbType.BigInt)



        SqlHelper.ExecuteNonQuery(objConnc, "[dbo].[DeleteRESOURCES_CATEGORY_MBYID]", Param)
        objConnc.Close()
        '  lblError.Visible = False
        'linkdelete.Text = "Active"
        Call BINDGRID()
    End Sub

    Protected Sub btndownload_Click(sender As Object, e As EventArgs) Handles btndownload.Click 'FOR TEST ONLY
        Dim PhyPath1 As String = Web.Configuration.WebConfigurationManager.AppSettings("ResourcesCategory").ToString()
        'C:\Resources\Category\

        ' Dim fpath As String = "..\..\Resources\9\9_Certificate (1).pdf"
        Dim fpath As String = "Resources/Category/111.jpg"
        Dim fpathNew As String = fpath.Replace("/", "\")
        Dim fname As String = "111.jpg"

        Dim BYTES() As Byte = File.ReadAllBytes(PhyPath1 + fname)
        HttpContext.Current.Response.Clear()
        Response.ContentType = ContentType
        Response.AddHeader("content-disposition", "attachment; filename=" & fname)

        Response.CacheControl = "No-cache"
        Response.BinaryWrite(BYTES)
        Response.Flush()
        Response.SuppressContent = True
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub


    Protected Sub Imgbtn_Click(sender As Object, e As ImageClickEventArgs)
        Dim PhyPath1 As String = Web.Configuration.WebConfigurationManager.AppSettings("ResourcesCategory").ToString()
        Dim lblResourceCategorypath As Label
        Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent.Parent.Parent,  _
                                        GridViewRow)

        Dim index As Integer = gvRow.RowIndex
        lblResourceCategorypath = TryCast(gvRow.Cells(0).FindControl("lblResourceCategorypath"), Label)

        Dim filename As String = Path.GetFileName(lblResourceCategorypath.Text)
        Dim Fpath1 As String = lblResourceCategorypath.Text.Remove(0, 2)
        Dim finalpath As String = Fpath1.Replace("/", "\")
        Dim BYTES() As Byte = File.ReadAllBytes(PhyPath1 + filename)
        HttpContext.Current.Response.Clear()
        Response.ContentType = ContentType
        Response.AddHeader("content-disposition", "attachment; filename=" & filename)

        Response.CacheControl = "No-cache"
        Response.BinaryWrite(BYTES)
        Response.Flush()
        Response.SuppressContent = True
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub

    
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Call ClearData()
        btnSave.Text = "Save"
    End Sub
End Class
