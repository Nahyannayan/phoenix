Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports SurveyChart
Imports System.Drawing
Imports InfoSoftGlobal

Partial Class Tellal_comPrintSurveyResults_PD_online
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Private intHeadColCnt As Integer = 0
    Private nmvAnswers As New NameValueCollection
    Private nmvAnswerDesc As New NameValueCollection
    Private nmvSurvResults As New NameValueCollection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            SetFormatTable()
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            Dim str_Conn = ConnectionManger.GetTellalPDConnectionString
            If Not IsPostBack Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Try
                    ViewState("SurveyId") = "185"
                    ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("PD_CM_VAL").Replace(" ", "+")).Replace("|", "")
                    'ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("PD_CM_VAL").Replace(" ", "+"))
                    CurBsUnit = "999998"
                    Session("Bsuid") = CurBsUnit
                Catch ex As Exception

                End Try
                Session("gm") = Nothing



                'Session("SurveyId") = cmbSurvey.SelectedValue
                SetFormatTable()
                BindResult()

                Dim Param(10) As SqlClient.SqlParameter
                Param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))

                Using SENDDETAILS As SqlDataReader = SqlHelper.ExecuteReader(str_Conn, CommandType.StoredProcedure, "dbo.SURVEYSENDDETAILS_PD", Param)
                    If SENDDETAILS.HasRows Then
                        While SENDDETAILS.Read
                            TSent.Text = Convert.ToString(SENDDETAILS("SENDCOUNT")) 'Sent

                            TResponse.Text = Convert.ToString(SENDDETAILS("RESPONSECOUNT")) 'Response
                        End While
                    End If
                End Using
                lblPDTITLE.Text = GetPD_TITLE(ViewState("CM_ID"))


            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetFormatTable()
        With TabResults
            .Width = "827"
            .Height = "347"
            .ForeColor = Drawing.Color.Black
            .BackColor = Drawing.Color.LightBlue
            .Style.Value = ""
            .BorderColor = Drawing.Color.Black
            .BorderWidth = "1"
            .Font.Size = "10"
        End With
    End Sub

    Private Function GetCourseType() As String

        Dim connection As String = ConnectionManger.GetTellalPDConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT DISTINCT CM_COURSE_TYPE_ID FROM dbo.COURSE_M where CM_ID in (" & ViewState("CM_ID") & ")"
        Dim courseType As String
        courseType = SqlHelper.ExecuteScalar(connection, CommandType.Text, strSQL)
        Return courseType

    End Function


    Private Function GetPD_TITLE(ByVal CM_ID As String) As String

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT CM_TITLE FROM dbo.COURSE_M WHERE CM_ID='" & CM_ID & "'"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return ""
        End If
    End Function


    Private Sub BindResult()

        Dim Param(10) As SqlClient.SqlParameter
        Param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))
        Param(1) = New SqlClient.SqlParameter("@CourseType", "Online")
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim strSQL As String = ""

        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Graph_result", Param)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            'Dim returnValue = ExportDataTable(dsResponse.Tables(0))
            For Each tr As TableRow In TabResults.Rows

                For Each tc As TableCell In tr.Cells
                    If tc.ID IsNot Nothing Then

                        If tc.ID.Contains("S") And tc.ID.Contains("Q") Then



                            Dim strXML As String
                            Dim ANSWER As String = ""
                            Dim ANS_TOOLTIP As String

                            Dim TOT_ANSWER As Decimal
                            Dim arr() As String
                            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}
                            'Dim i As Integer = 0
                            strXML = ""
                            strXML = strXML & "<graph caption='EF Analysis' xAxisName='Answers' yAxisName='Percentage' decimalPrecision='2'  yAxisMaxValue='100'  numberSuffix='%25' formatNumberScale='1' rotateNames='1' labelPadding='100' >"


                            Dim cellID = tc.ID
                            For i As Integer = 0 To 4

                                If i = 0 Then
                                    ANSWER = "Strongly agree"
                                ElseIf i = 1 Then
                                    ANSWER = "Agree"
                                ElseIf i = 2 Then
                                    ANSWER = "Disagree"
                                ElseIf i = 3 Then
                                    ANSWER = "Strongly disagree"
                                ElseIf i = 4 Then
                                    ANSWER = "Not Applicable"
                                End If

                                ANS_TOOLTIP = ANSWER
                                '			
                                Dim dt = dsResponse.Tables(0).AsEnumerable().Where(Function(r) r.Field(Of String)(cellID) = ANSWER)
                                Dim table As DataTable = dsResponse.Tables(0).Clone()

                                If dt IsNot Nothing AndAlso DirectCast((dt.AsDataView), System.Data.DataView).Count > 0 Then
                                    TOT_ANSWER = Convert.ToDecimal(DirectCast((dt.AsDataView), System.Data.DataView).Count)
                                Else
                                    TOT_ANSWER = 0
                                End If

                                Dim TOT_TIMES_QT_ASKED As Decimal = 0
                                If TOT_TIMES_QT_ASKED = 0 Then
                                    TOT_TIMES_QT_ASKED = 1
                                End If
                                TOT_ANSWER = TOT_ANSWER * (100 / TOT_TIMES_QT_ASKED)
                                strXML = strXML & "<set name=" + "'" & ANSWER & "' value=" + "'" & TOT_ANSWER & "' color=" + "'" & arr(i) & "' hoverText='" & ANS_TOOLTIP & "'/>"
                                ''Qid,SurvId,Result
                                'i = i + 1
                            Next
                            strXML = strXML & "</graph>"
                            tc.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "500", "250", False)
                        End If

                    End If
                Next


            Next





        End If

    End Sub

    Public Function ExportDataTable(table As DataTable) As List(Of List(Of Object))
        Dim result As New List(Of List(Of Object))
        For Each row As DataRow In table.Rows
            Dim values As New List(Of Object)
            For Each column As DataColumn In table.Columns
                If row.IsNull(column) Then
                    values.Add(Nothing)
                Else
                    values.Add(row.Item(column))
                End If
            Next
            result.Add(values)
        Next
        Return result
    End Function
End Class
