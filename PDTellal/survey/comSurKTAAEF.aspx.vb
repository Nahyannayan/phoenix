Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class masscom_comSurKTAAEF
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Private strHeaderImagePath As String
    Private strFooterImagePath As String
    Private intHeadCols As Integer
    Private nmvAnswers As New NameValueCollection
    Private nmvSurvRadioGrps As New NameValueCollection
    Private nmvSurvChkGrps As New NameValueCollection
    Private intPrintRows As Integer = 0
    Private intHeadColCnt As Integer = 0
    Private intHeaderRows As Integer = 0
    Dim intRow As Integer = 0
    Dim intAnswer As Integer = 0
    Dim intGrpRow As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Not IsPostBack Then
                ViewState("errMessage") = ""
                

                If Request.QueryString("CRID") IsNot Nothing And Request.QueryString("CRID") <> "" Then
                    'Dim id As String = Encr_decrData.Decrypt(Request.QueryString("CRID")) 'Convert.ToInt32(Request.QueryString("CRID").ToString())
                    Dim id As Integer = Convert.ToInt32(Encr_decrData.Decrypt(Request.QueryString("CRID").Replace(" ", "+")))
                    ViewState("CR_ID") = id
                End If
                If Request.QueryString("KTA_ID") IsNot Nothing And Request.QueryString("KTA_ID") <> "" Then
                    Dim id As Integer = Convert.ToInt32(Request.QueryString("KTA_ID").ToString())
                    ViewState("KTA_ID") = id
                Else
                    ViewState("KTA_ID") = 0
                End If

                BindCourseDetails(ViewState("CR_ID"))
                BindKTA_Main(ViewState("CR_ID"))
                Bind_KTAA_ACTION_DETAILS(ViewState("CR_ID"))
            End If

        Catch ex As Exception

        End Try

    End Sub
    
    Sub BindCourseDetails(ByVal CR_ID As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", CR_ID)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Course_Req_By_Id", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    lblName.Text = Dt.Rows(0)("UserName").ToString()
                    lblActivity.Text = Dt.Rows(0)("CM_TITLE").ToString()
                    lblCourseDate.Text = Dt.Rows(0)("CM_EVENT_DT").ToString()
                    lblTrainername.Text = Dt.Rows(0)("Trainers").ToString()
                    lblTraininglocation.Text = Dt.Rows(0)("Location").ToString() & "," & Dt.Rows(0)("region").ToString()
                    ' lblPosition.Text = Dt.Rows(0)("position").ToString()
                    lblBsu.Text = Dt.Rows(0)("SchoolName").ToString()


                End If
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Sub BindKTA_Main(ByVal CR_ID As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", CR_ID) 
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_KTA_F2F_BY_CR_ID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                   
                    ViewState("KTA_ID") = Convert.ToString(Dt.Rows(0)("KTA_ID"))
                    ViewState("is_draft") = Convert.ToString(Dt.Rows(0)("is_draft"))

                    If Convert.ToBoolean(ViewState("is_draft")) = False Then
                        btnDraftEval.Visible = False
                        btnSaveEval.Visible = False
                    Else
                        btnDraftEval.Visible = True
                        btnSaveEval.Visible = True
                    End If
                    KTA_SCHOOL_LEVEL_EC.Checked = Convert.ToBoolean(Dt.Rows(0)("KTA_SCHOOL_LEVEL_EC"))
                    KTA_SCHOOL_LEVEL_P.Checked = Convert.ToBoolean(Dt.Rows(0)("KTA_SCHOOL_LEVEL_P"))
                    KTA_SCHOOL_LEVEL_S.Checked = Convert.ToBoolean(Dt.Rows(0)("KTA_SCHOOL_LEVEL_S"))
                    KTA_SCHOOL_LEVEL_ALL.Checked = Convert.ToBoolean(Dt.Rows(0)("KTA_SCHOOL_LEVEL_ALL"))

                    '--SECTION 1
                    S1Q1.SelectedValue = Convert.ToString(Dt.Rows(0)("S1Q1"))
                    S1Q2.SelectedValue = Convert.ToString(Dt.Rows(0)("S1Q2"))
                    S1Q3.SelectedValue = Convert.ToString(Dt.Rows(0)("S1Q3"))
                    S1Q4.SelectedValue = Convert.ToString(Dt.Rows(0)("S1Q4"))

                    '--SECTION 2
                    S2Q1.SelectedValue = Convert.ToString(Dt.Rows(0)("S2Q1"))
                    S2Q2.SelectedValue = Convert.ToString(Dt.Rows(0)("S2Q2"))
                    S2Q3.SelectedValue = Convert.ToString(Dt.Rows(0)("S2Q3"))

                    '--SECTION 3
                    S3Q1.SelectedValue = Convert.ToString(Dt.Rows(0)("S3Q1"))
                    S3Q2.SelectedValue = Convert.ToString(Dt.Rows(0)("S3Q2"))
                    S3Q3.SelectedValue = Convert.ToString(Dt.Rows(0)("S3Q3"))
                    S3Q4.SelectedValue = Convert.ToString(Dt.Rows(0)("S3Q4"))
                    S3Q5.SelectedValue = Convert.ToString(Dt.Rows(0)("S3Q5"))
                    S3Q6.SelectedValue = Convert.ToString(Dt.Rows(0)("S3Q6"))
                    S3Q7.SelectedValue = Convert.ToString(Dt.Rows(0)("S3Q7"))

                    '--SECTION 4                 
                    S4Q1.SelectedValue = Convert.ToString(Dt.Rows(0)("S4Q1"))
                    S4Q2.SelectedValue = Convert.ToString(Dt.Rows(0)("S4Q2"))
                    S4Q3.SelectedValue = Convert.ToString(Dt.Rows(0)("S4Q3"))

                    '--SECTION 5

                    S5Q1.SelectedValue = Convert.ToString(Dt.Rows(0)("S5Q1"))
                    S5Q2.SelectedValue = Convert.ToString(Dt.Rows(0)("S5Q2"))
                    S5Q3.SelectedValue = Convert.ToString(Dt.Rows(0)("S5Q3"))
                    '--SECTION 6
                    S6Q1.SelectedValue = Convert.ToString(Dt.Rows(0)("S6Q1"))
                    S6Q2.SelectedValue = Convert.ToString(Dt.Rows(0)("S6Q2"))
                    S6Q3.SelectedValue = Convert.ToString(Dt.Rows(0)("S6Q3"))
                    S6Q4.SelectedValue = Convert.ToString(Dt.Rows(0)("S6Q4"))
                    S6Q5.SelectedValue = Convert.ToString(Dt.Rows(0)("S6Q5"))

                    '--SECTION 7
                    S7Q1.Checked = Convert.ToBoolean(Dt.Rows(0)("S7Q1"))
                    S7Q2.Checked = Convert.ToBoolean(Dt.Rows(0)("S7Q2"))
                    S7Q3.Checked = Convert.ToBoolean(Dt.Rows(0)("S7Q3"))
                    S7Q4.Checked = Convert.ToBoolean(Dt.Rows(0)("S7Q4"))
                    S7Q5.Checked = Convert.ToBoolean(Dt.Rows(0)("S7Q5"))
                    S7Q6.Checked = Convert.ToBoolean(Dt.Rows(0)("S7Q6"))

                    '--Comment section
                    '--SECTION 7 

                    COMMENT_1.Value = Convert.ToString(Dt.Rows(0)("COMMENT_1"))
                    COMMENT_2.Value = Convert.ToString(Dt.Rows(0)("COMMENT_2"))
                    COMMENT_3.Value = Convert.ToString(Dt.Rows(0)("COMMENT_3"))
                    COMMENT_4.Text = Convert.ToString(Dt.Rows(0)("COMMENT_4"))
                    COMMENT_5.Text = Convert.ToString(Dt.Rows(0)("COMMENT_5"))
                    COMMENT_6.Text = Convert.ToString(Dt.Rows(0)("COMMENT_6"))
                End If
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub
    Sub Bind_KTAA_ACTION_DETAILS(ByVal CR_ID As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", CR_ID)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_KAT_ACTION_BY_KTAID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)
                BindKTAAGrid(ds)
                'gvCourseActions.DataSource = ds
                'gvCourseActions.DataBind()

                'Session("COURSEACTION") = Dt
            End If

        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub
    Private Sub BindKTAAGrid(ByVal ds As DataSet)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim tmpRow As DataRow
            'ds.Tables(0).Columns.Add("SR_NO")
            For i As Integer = 1 To 5
                tmpRow = ds.Tables(0).NewRow()
                tmpRow.Item("SR_NO") = i
                tmpRow.Item("TEXTBOX1") = ""
                tmpRow.Item("TEXTBOX2") = ""
                tmpRow.Item("TEXTBOX3") = ""
                tmpRow.Item("TEXTBOX4") = ""
                'tmpRow.Item("PD_KTA_RELEVANCE") = ""
                'tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                'tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                'tmpRow.Item("UniqueId") = i

                ds.Tables(0).Rows.Add(tmpRow)
            Next
            ds.AcceptChanges()
            Me.gvActions.DataSource = ds.Tables(0)
            Me.gvActions.DataBind()
        Else
            'Dim dv As New DataView(ds.Tables(0), Nothing, "UniqueId Desc", DataViewRowState.CurrentRows)
            'ds.Tables.RemoveAt(0)
            'ds.Tables.Add(dv.ToTable)
            'ds.AcceptChanges()
            ''ds.Tables(0).Columns.Add("SNo")
            'If ds.Tables(0).Rows.Count < 5 Then
            '    Dim tmpRow As DataRow
            '    For i As Integer = ds.Tables(0).Rows.Count To 4
            '        tmpRow = ds.Tables(0).NewRow()
            '        tmpRow.Item("TEXTBOX1") = ""
            '        tmpRow.Item("TEXTBOX2") = ""
            '        tmpRow.Item("TEXTBOX3") = ""
            '        tmpRow.Item("TEXTBOX4") = "" 
            '        ds.Tables(0).Rows.Add(tmpRow)
            '    Next
            '    ds.AcceptChanges()
            'End If
            'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            '    ds.Tables(0).Rows(i).Item("SR_NO") = i + 1
            'Next
            'ds.AcceptChanges()

            Me.gvActions.DataSource = ds.Tables(0)
            Me.gvActions.DataBind()
        End If



    End Sub




    Protected Sub btnDraftEval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDraftEval.Click


        ViewState("SEND_PDC") = 0
        Dim errormsg As String = String.Empty

        If callTransactionNew(True, errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
            ' Bind_KTAA_MASTER(ViewState("CR_ID"))

        End If
    End Sub

    Protected Sub btnSaveEval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveEval.Click
        ViewState("SEND_PDC") = 0
        Dim errormsg As String = String.Empty
        If callTransactionNew(False, errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
            Exit Sub
        Else
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Thank you, your evaluation form has been submitted !!!</div>"
            ' Bind_KTAA_MASTER(ViewState("CR_ID"))
            btnDraftEval.Visible = False
            btnSaveEval.Visible = False
        End If
    End Sub

    Private Function callTransactionNew(ByVal IS_DRAFT As Boolean, ByRef errormsg As String) As Integer

        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CR_ID As String = ViewState("CR_ID")
        Dim iReturnvalue As Integer
        Using CONN As SqlConnection = ConnectionManger.Get_TellalPD_Connection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                'Dim dtCOURSEACTIONDetails As DataTable
                'If Session("COURSEACTION") IsNot Nothing Then
                '    dtCOURSEACTIONDetails = Session("COURSEACTION")
                '    dtCOURSEACTIONDetails.Columns.Remove("UniqueID")

                'End If

                Dim status As String = String.Empty


                Dim param(50) As SqlParameter

                param(0) = New SqlParameter("@KTA_ID", ViewState("KTA_ID"))
                param(1) = New SqlParameter("@CR_ID", ViewState("CR_ID"))
                param(2) = New SqlParameter("@KTA_SCHOOL_LEVEL_EC", KTA_SCHOOL_LEVEL_EC.Checked)
                param(3) = New SqlParameter("@KTA_SCHOOL_LEVEL_P", KTA_SCHOOL_LEVEL_P.Checked)
                param(4) = New SqlParameter("@KTA_SCHOOL_LEVEL_S", KTA_SCHOOL_LEVEL_S.Checked)
                param(5) = New SqlParameter("@KTA_SCHOOL_LEVEL_ALL", KTA_SCHOOL_LEVEL_ALL.Checked)

                '--SECTION 1
                param(6) = New SqlParameter("@S1Q1", S1Q1.SelectedValue)
                param(7) = New SqlParameter("@S1Q2", S1Q2.SelectedValue)
                param(8) = New SqlParameter("@S1Q3", S1Q3.SelectedValue)
                param(9) = New SqlParameter("@S1Q4", S1Q4.SelectedValue)
                '--SECTION 2

                param(10) = New SqlParameter("@S2Q1", S2Q1.SelectedValue)
                param(11) = New SqlParameter("@S2Q2", S2Q2.SelectedValue)
                param(13) = New SqlParameter("@S2Q3", S2Q3.SelectedValue)
                '--SECTION 3
                param(14) = New SqlParameter("@S3Q1", S3Q1.SelectedValue)
                param(15) = New SqlParameter("@S3Q2", S3Q2.SelectedValue)
                param(16) = New SqlParameter("@S3Q3", S3Q3.SelectedValue)
                param(17) = New SqlParameter("@S3Q4", S3Q4.SelectedValue)
                param(18) = New SqlParameter("@S3Q5", S3Q5.SelectedValue)
                param(19) = New SqlParameter("@S3Q6", S3Q6.SelectedValue)
                param(20) = New SqlParameter("@S3Q7", S3Q7.SelectedValue)
                '--SECTION 4
                param(21) = New SqlParameter("@S4Q1", S4Q1.SelectedValue)
                param(22) = New SqlParameter("@S4Q2", S4Q2.SelectedValue)
                param(23) = New SqlParameter("@S4Q3", S4Q3.SelectedValue)
                '--SECTION 5
                param(24) = New SqlParameter("@S5Q1", S5Q1.SelectedValue)
                param(25) = New SqlParameter("@S5Q2", S5Q2.SelectedValue)
                param(26) = New SqlParameter("@S5Q3", S5Q3.SelectedValue)
                '--SECTION 6
                param(27) = New SqlParameter("@S6Q1", S6Q1.SelectedValue)
                param(28) = New SqlParameter("@S6Q2", S6Q2.SelectedValue)
                param(29) = New SqlParameter("@S6Q3", S6Q3.SelectedValue)
                param(30) = New SqlParameter("@S6Q4", S6Q4.SelectedValue)
                param(31) = New SqlParameter("@S6Q5", S6Q5.SelectedValue)
                '--SECTION 7
                param(47) = New SqlParameter("@S7Q1", S7Q1.Checked)
                param(32) = New SqlParameter("@S7Q2", S7Q2.Checked)
                param(33) = New SqlParameter("@S7Q3", S7Q3.Checked)
                param(34) = New SqlParameter("@S7Q4", S7Q4.Checked)
                param(35) = New SqlParameter("@S7Q5", S7Q5.Checked)
                param(36) = New SqlParameter("@S7Q6", S7Q6.Checked)

                '--Comment section
                '--SECTION 7
                param(37) = New SqlParameter("@COMMENT_1 ", COMMENT_1.Value)
                param(38) = New SqlParameter("@COMMENT_2", COMMENT_2.Value)
                param(39) = New SqlParameter("@COMMENT_3", COMMENT_3.Value)
                param(40) = New SqlParameter("@COMMENT_4", COMMENT_4.Text)
                param(41) = New SqlParameter("@COMMENT_5", COMMENT_5.Text)
                param(42) = New SqlParameter("@COMMENT_6", COMMENT_6.Text)

                param(43) = New SqlParameter("@IS_DRAFT", IS_DRAFT)


                param(44) = New SqlParameter("@KTA_ID_NEW", SqlDbType.Int)
                param(44).Direction = ParameterDirection.Output
                'param(5) = New SqlParameter("@MyKTAATableType", dtCOURSEACTIONDetails)
                param(45) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(45).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.SAVE_KTA_FACE_TO_FACE", param)
                Dim ReturnFlag As Integer = param(45).Value
                Dim id As Integer = param(44).Value
                ViewState("KTA_ID") = id
                 
                Dim paramA(10) As SqlParameter

                Dim iIndex As Integer
                If id > 0 Then
                    
                    For iIndex = 0 To gvActions.Rows.Count - 1
                        Dim RelvIds As String = ""
                        Dim row As GridViewRow = gvActions.Rows(iIndex)
                        Dim cbl As CheckBoxList = CType(row.FindControl("cblRelevance"), CheckBoxList)
                        Dim txtRelevanceOther As TextBox = CType(row.FindControl("txtRelevanceOther"), TextBox)
                        For i As Integer = 0 To cbl.Items.Count - 1
                            If cbl.Items(i).Selected = True Then
                                RelvIds &= cbl.Items(i).Value & "|"
                            End If
                        Next
                        If RelvIds.EndsWith("|") Then RelvIds = RelvIds.TrimEnd("|")

                        'If CType(row.FindControl("txtAction"), TextBox).Text <> Nothing And CType(row.FindControl("txtSuccess"), TextBox).Text <> Nothing Then
                        paramA(0) = New SqlParameter("@SRNo", CType(row.FindControl("lblSNo"), Label).Text)
                        paramA(1) = New SqlParameter("@TEXTBOX1", CType(row.FindControl("TEXTBOX1"), TextBox).Text)
                        paramA(2) = New SqlParameter("@TEXTBOX2", CType(row.FindControl("TEXTBOX2"), TextBox).Text)
                        paramA(3) = New SqlParameter("@TEXTBOX3", CType(row.FindControl("TEXTBOX3"), TextBox).Text)
                        paramA(4) = New SqlParameter("@TEXTBOX4", CType(row.FindControl("TEXTBOX4"), TextBox).Text)

                        paramA(5) = New SqlParameter("@CHK_OTHER", txtRelevanceOther.Text)
                        paramA(6) = New SqlParameter("@KTA_ID", ViewState("KTA_ID"))
                        paramA(7) = New SqlParameter("@RelvnceIDs", RelvIds)

                        paramA(8) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        paramA(8).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.KTA_SAVE_ACTIONS_EVAL", paramA)
                        iReturnvalue = paramA(8).Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                        'End If
                    Next
                End If
                ''end add actions 

                If ReturnFlag = -1 Then
                    callTransactionNew = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransactionNew = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransactionNew = "0"

                End If
            Catch ex As Exception
                callTransactionNew = "1"
                errormsg = ex.Message
            Finally
                If callTransactionNew = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransactionNew <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function
  

    Protected Sub gvActions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvActions.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)
            'Dim lblId As System.Web.UI.WebControls.Label = CType(e.Row.FindControl("lblRelevanceId"), System.Web.UI.WebControls.Label)

            Dim cbl As CheckBoxList = CType(e.Row.FindControl("cblRelevance"), CheckBoxList)
            Dim lbl As Label = CType(e.Row.FindControl("lblRelevanceId"), Label)
            Dim lblSNo As Label = CType(e.Row.FindControl("lblSNo"), Label)
            Dim lblMandatory As Label = CType(e.Row.FindControl("lblMandatory"), Label)
            Dim txtRelevanceOther As TextBox = CType(e.Row.FindControl("txtRelevanceOther"), TextBox)

            If Not lblSNo Is Nothing AndAlso CInt(lblSNo.Text) = 1 Then
                lblMandatory.Visible = True
            End If
            If Not cbl Is Nothing AndAlso Not lbl Is Nothing Then
                Dim tmpStr() As String = IIf(lbl.Text = Nothing, "", lbl.Text).ToString.Split("#")
                For i As Integer = 0 To tmpStr.Length - 1
                    For j As Integer = 0 To cbl.Items.Count - 1
                        If tmpStr(i).Split("|")(0) = cbl.Items(j).Value Then
                            cbl.Items(j).Selected = True
                            If tmpStr(i).Split("|")(0) = 4 Then
                                txtRelevanceOther.Text = tmpStr(i).Split("|")(1)
                            End If
                        End If
                    Next
                Next
            End If


        End If
    End Sub
End Class
