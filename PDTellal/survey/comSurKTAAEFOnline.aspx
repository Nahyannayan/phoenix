<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSurKTAAEFOnline.aspx.vb" Inherits="comSurKTAAEFOnline" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Survey</title>
     <%-- <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />--%>
    <script src="../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.3.min.js"></script>
<script language="javascript" type="text/javascript">
    
    
    function SubmitEvalClick() {

        var result = "0";
        var i=1;
        for (i = 1; i < 6; i++)
        {
            var j = 1;
            if (i == 2)
            {
                j = 2;
            } else if (i == 1 || i == 3 || i == 4) {
                j = 3;
            } else if (i == 5) {
                j = 4;
            }  
            
            var k = 1;
            for (k = 1; k <= j; k++) {
                var id='S'+i+'Q'+k;
               // alert($('#' + id + ' input:checked').length);
                if ($('#' + id + ' input:checked').length == "0")
                {
                    result = "1"
                    break;
                }
            }
            if (result == "1")
            {
                break;
            }
            //$('#S1Q1 input:checked').length

            
        }

        if ($("#COMMENT_5").val() == "" || $("#gvActions_ctl02_TEXTBOX1").val() == "" || $("#gvActions_ctl02_TEXTBOX2").val() == "" || $("#gvActions_ctl02_TEXTBOX3").val() == "" || $("#gvActions_ctl02_TEXTBOX4").val() == "")
        {
            result = "1"
        }

        if (result == "1")
        {
            alert("Please complete all mandatory* information required.");
            return false;
        }
         
        return true;
    }
 
    
</script> 

    <style type="text/css">
        .EVALBUTTON {
            width: 149px;
            height: 39px;
            border: 1px solid #ccc;
            font-weight: bold;
            font-size: 11px;
            color: #000;
        }
        .RBL label
        {
        display: block;
        }

        .RBL td
        {
        text-align: center;
        /*width: 20px;*/
        }
         .header { 
        background: url(../../Images/TELLAL_Logo.png) no-repeat 0 0!important;   
       }
    </style>
    <link href="../../cssfiles/parentSurvey.css" rel="stylesheet" type="text/css" />
    <%-- <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
</head>
<body class="Surveybody">
    <div class="wrapper">
        <div class="header">
            <img src="../../Images/TELLAL_Logo.png" alt="" />
        </div>
        <div class="clear"></div>
        <center>


            <form id="form1" runat="server">
                <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                </ajaxToolkit:ToolkitScriptManager>

                <asp:Literal ID="ltHea" runat="server"></asp:Literal>
                <table width="100%" id="tbltitleInfo" runat="server">
                    <tr class="trSub_Header">
                        <td align="left" class="" colspan="5">
                            <font><span style="font-family: Verdana; color: white;">Please complete this Evaluation Form so that we can strengthen, promote and improve future professional development and determine whether professional development had the desired effects and to ensure programme accountability.</span></font>
                        </td>
                    </tr>
                </table>
                 <span style="color:green;"><b> Note:Complete the Evaluation form in the one sitting and please note that the system times out after 15 minutes</b>
                </span> 
                <table id="tblEmpInfo" runat="server" width="100%" class="BlueTable_simple">
                    <tr class="trStyleOdd">
                        <td align="left" class="style1">
                            <strong>Name</strong>
                        </td>
                        <td class="matters" style="width: 4px">:
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblName" runat="server"></asp:Label>
                        </td>
                        <td class="matters">
                            <strong>School</strong>
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblBsu" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="trStyleEven">
                        <td align="left" class="style1">
                            <%--<strong>Position</strong>--%>
                        </td>
                        <td class="matters" style="width: 4px"><%--:--%>
                        </td>
                        <td align="left" class="matters">
                          <%--  <asp:Label ID="lblPosition" runat="server"></asp:Label>--%>
                        </td>
                        <td class="matters">
                            <strong>School Level</strong>
                        </td>
                        <td align="left" class="matters">
                            <table width="100%">
                                <tr>
                                    <td>EC<br />
                                        <asp:CheckBox ID="KTA_SCHOOL_LEVEL_EC" runat="server" Text="EC / EY / KG / FS " />
                                    </td>
                                    <td>P<br />
                                        <asp:CheckBox ID="KTA_SCHOOL_LEVEL_P" runat="server" Text="Primary / Elementary" />
                                    </td>
                                    <td>S<br />
                                        <asp:CheckBox ID="KTA_SCHOOL_LEVEL_S" runat="server" Text="Secondary / Middle school / High School" />
                                    </td>
                                    <td>ALL<br />
                                        <asp:CheckBox ID="KTA_SCHOOL_LEVEL_ALL" runat="server" Text="ALL" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="trStyleOdd">
                        <td align="left" class="style1">
                            <strong>Title of Training</strong>
                        </td>
                        <td class="matters" style="width: 4px">:
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblActivity" runat="server"></asp:Label>
                        </td>
                        <td class="matters">
                            <strong>Date of Training</strong>
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblCourseDate" runat="server"></asp:Label>
                        </td>
                    </tr>

                    <tr class="trStyleEven">
                        <td align="left" class="style1">
                            <strong>Name of Trainer</strong>
                        </td>
                        <td class="matters" style="width: 4px">:
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblTrainername" runat="server"></asp:Label>
                        </td>
                        <td class="matters">
                            <strong>Location of Training</strong>
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblTraininglocation" runat="server"></asp:Label>
                        </td>
                    </tr>

                </table>

                <table width="100%" id="tblmessage" runat="server">
                    <tr id="trLabelError">
                        <td align="left" class="matters" valign="bottom" colspan="5">
                            <div id="lblError" runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr class="trSub_Header">
                        <td align="left" class="" colspan="5">
                            <font><span style="font-family: Verdana; color: white;">For each statement, please tick if you agree or disagree using a rating scale from 1 to 4 as below. </span></font>
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>

                            <table width="80%" border="1" style="border-color: white;">
                                <tr>
                                    <td style="background-color: #3FD117;"><b>4</b></td>
                                    <td style="background-color: #F7F55C;"><b>3</b></td>
                                    <td style="background-color: #f4bc42;"><b>2</b></td>
                                    <td style="background-color: #F73802;"><b>1</b></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #3FD117;"><b>Strongly Agree</b></td>
                                    <td style="background-color: #F7F55C;"><b>Agree</b></td>
                                    <td style="background-color: #f4bc42;"><b>Disagree</b></td>
                                    <td style="background-color: #F73802;"><b>Strongly Disagree</b></td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>

               <table class="BlueTable_simple" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; width: 950px;">
                   <tbody>
                       <tr>
                           <td width="5%"></td>
                           <td width="35%"></td>
                           <td width="12%"></td>
                           <td width="12%"></td>
                           <td width="12%"></td>
                           <td width="12%"></td>
                           <td width="12%"></td>
                       </tr>


                       <tr style="font-weight: bold; font-size: 8pt; text-align: center; height: 20px;">
                           <td class="mainheading" colspan="7" style="color: #FFFFFF; border-width: 0px; text-align: left; letter-spacing: 1px; height: 18px; font-size: 10pt;">* indicates mandatory section / fields</td>
                       </tr>
                       <tr style="font-weight: bold; font-size: 8pt; text-align: center; height: 20px;">
                           <td class="titleText" colspan="7" style="border-width: 0px; text-align: left; letter-spacing: 1px; height: 18px;"></td>
                       </tr>
                       <tr class="trSub_Header" style="text-align: left;">
                           <td colspan="7" style="color: #FFFFFF;"></td>
                       </tr>

                       <tr>
                           <td class="headerMainCell">No.</td>
                           <td class="headerMainCell">Preparation and Planning *</td>
                           <td class="headerMainCell" style="color: #000000; background-color: #3FD117;">Strongly agree</td>
                           <td class="headerMainCell" style="color: #000000; background-color: #F7F55C;">Agree</td>
                           <td class="headerMainCell" style="color: #000000; background-color: #f4bc42;">Disagree</td>
                           <td class="headerMainCell" style="color: #000000; background-color: #F73802;">Strongly disagree</td>
                            <td class="headerMainCell" style="color: #000000;">Not Applicable</td>
                       </tr>
                       
                       <tr id="677">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">1</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">Learning objectives were clear and appropriate	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S1Q1" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                                  
                </tr>
                <tr id="678">
                    <td class="trStyleEven" style="white-space: nowrap; text-align: center !important;">2</td>
                    <td class="trStyleEven" style="white-space: nowrap; padding-left: 7px;text-align: left;">Screen instructions were clear	 </td>
                        <td class="trStyleEven" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S1Q2" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                  <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> 
                                <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr id="679">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">3</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">The schedule provided sufficient time to cover all of the proposed activities	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S1Q3" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                 
                <tr class="trSub_Header" style="text-align: left;">
                    <td colspan="7" style="color: #FFFFFF;"></td>
                </tr>
                <tr>
                    <td class="headerMainCell">No.</td>
                    <td class="headerMainCell">Use of resources and facilities *</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #3FD117;">Strongly agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F7F55C;">Agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #f4bc42;">Disagree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F73802;">Strongly disagree</td>
                    <td class="headerMainCell" style="color: #000000;">Not Applicable</td>
                </tr>
                <tr id="681">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">1</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">Resources and digital tools were used appropriately and allowed objectives to be met</td>
                    <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                        <asp:RadioButtonList ID="S2Q1" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                            <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                            <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                            <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                             <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                        </asp:RadioButtonList>
                    </td>
                </tr>					
 
                <tr id="682">
                    <td class="trStyleEven" style="white-space: nowrap; text-align: center !important;">2</td>
                    <td class="trStyleEven" style="white-space: nowrap; padding-left: 7px;text-align: left;">The online facility was easy to navigate</td>
                        <td class="trStyleEven" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S2Q2" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                 
                <tr class="trSub_Header" style="text-align: left;">
                    <td colspan="7" style="color: #FFFFFF;"></td>
                </tr>
                <tr>
                    <td class="headerMainCell">No.</td>
                    <td class="headerMainCell">Content *	</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #3FD117;">Strongly agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F7F55C;">Agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #f4bc42;">Disagree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F73802;">Strongly disagree</td>
                    <td class="headerMainCell" style="color: #000000;">Not Applicable</td>
                </tr>
                <tr id="684">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">1</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">The content covered was relevant and suitable to participants	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S3Q1" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr id="685">
                    <td class="trStyleEven" style="white-space: nowrap; text-align: center !important;">2</td>
                    <td class="trStyleEven" style="white-space: nowrap; padding-left: 7px;text-align: left;">The content was reflective of objectives	</td>
                        <td class="trStyleEven" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S3Q2" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr id="686">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">3</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">There was adequate opportunity to explore theory and research	</td>
                    <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S3Q3" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                 
                <tr class="trSub_Header" style="text-align: left;">
                    <td colspan="7" style="color: #FFFFFF;"></td>
                </tr>
                <tr>
                    <td class="headerMainCell">No.</td>
                    <td class="headerMainCell">Activities *	</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #3FD117;">Strongly agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F7F55C;">Agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #f4bc42;">Disagree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F73802;">Strongly disagree</td>
                    <td class="headerMainCell" style="color: #000000;">Not Applicable</td>
                </tr>
                <tr id="691">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">1</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">The online facility effectively used various techniques and activities to engage the audience	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S4Q1" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr id="692">
                    <td class="trStyleEven" style="white-space: nowrap; text-align: center !important;">2</td>
                    <td class="trStyleEven" style="white-space: nowrap; padding-left: 7px;text-align: left;">The activities were appropriate and relevant to the topic	</td>
                        <td class="trStyleEven" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S4Q2" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr id="693">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">3</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">The online facility supported collaboration and sharing (e.g. discussion forum)	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S4Q3" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr class="trSub_Header" style="text-align: left;">
                    <td colspan="7" style="color: #FFFFFF;"></td>
                </tr>
                <tr>
                    <td class="headerMainCell">No.</td>
                    <td class="headerMainCell">Participant learning *</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #3FD117;">Strongly agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F7F55C;">Agree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #f4bc42;">Disagree</td>
                    <td class="headerMainCell" style="color: #000000; background-color: #F73802;">Strongly disagree</td>
                    <td class="headerMainCell" style="color: #000000;">Not Applicable</td>
                </tr>
                <tr id="694">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">1</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">The workshop level was appropriate for you	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S5Q1" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr id="695">
                    <td class="trStyleEven" style="white-space: nowrap; text-align: center !important;">2</td>
                    <td class="trStyleEven" style="white-space: nowrap; padding-left: 7px;text-align: left;">The workshop has enhanced your knowledge and understanding	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S5Q2" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>
                <tr id="696">
                    <td class="trStyleOdd" style="white-space: nowrap; text-align: center !important;">3</td>
                    <td class="trStyleOdd" style="white-space: nowrap; padding-left: 7px;text-align: left;">The workshop will be useful toward your professional practice	</td>
                        <td class="trStyleOdd" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S5Q3" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>

                 <tr id="6960">
                    <td class="trStyleEven" style="white-space: nowrap; text-align: center !important;">4</td>
                    <td class="trStyleEven" style="white-space: nowrap; padding-left: 7px;text-align: left;">You engaged well in group discussion and activity	</td>
                        <td class="trStyleEven" style="white-space: nowrap;" colspan="5">
                            <asp:RadioButtonList ID="S5Q4" runat="server" RepeatLayout="Table" CssClass="RBL" RepeatDirection="Horizontal" TextAlign="Left" Width="100%">
                                <asp:ListItem Value="Strongly agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Agree" Text=""></asp:ListItem>
                                <asp:ListItem Value="Disagree" Text=""></asp:ListItem>
                                 <asp:ListItem Value="Strongly disagree" Text=""></asp:ListItem> <asp:ListItem Value="Not Applicable" Text=""></asp:ListItem>  
                            </asp:RadioButtonList>
                        </td>
                </tr>

                  
                       <tr>
                           <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                               <br />
                               <div style="font-family: verdana,Arial, Helvetica, sans-serif; font-size: 13px; color: #fff;">
                                 WWW � What Worked Well
                            <br />
                                   What elements of the online PD Workshop worked well, i.e. what you enjoyed / found most useful?
                               </div>
                           </td>
                       </tr>
                       
                <tr>
                    <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                        <div>
                            <textarea id="COMMENT_1" runat="server" style="overflow: auto; width: 100%; height: 50px !important; color: #666; border: 1px solid #ccc; font-family: Verdana; font-size: 11px;" rows="5" cols="100">     </textarea>
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                        <br />
                        <div style="align: left; font-family: verdana,Arial, Helvetica, sans-serif; font-size: 13px; color: #fff;">                           
                            EBI � Even Better If 
                                <br />
                            What elements of the online PD Workshop do you think need improvement?
                        </div>
                    </td>
                </tr>
                <tr>
                    <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                        <div>
                            <textarea id="COMMENT_2" runat="server" style="overflow: auto; width: 100%; height: 50px !important; color: #666; border: 1px solid #ccc; font-family: Verdana; font-size: 11px;" rows="5" cols="100">     </textarea>
                                <br/>
                            <br/>
                            <br/>
                        </div>
                    </td>
                </tr>
                       <tr>
                           <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                               <br />
                               <div style="align: left; font-family: verdana,Arial, Helvetica, sans-serif; font-size: 13px; color: #fff;">
                                   Discussion Forum 
                                <br />
                                   Please comment on your experience with the discussion forums (e.g. time frames; posting; commenting; collaboration)
                               </div>
                           </td>
                       </tr>
                       <tr>
                           <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                               <div>
                                   <textarea id="COMMENT_3" runat="server" style="overflow: auto; width: 100%; height: 50px !important; color: #666; border: 1px solid #ccc; font-family: Verdana; font-size: 11px;" rows="5" cols="100">     </textarea>
                                   <br />
                                   <br />
                                   <br />
                               </div>
                           </td>
                       </tr>

                       <tr>
                           <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                               <br />
                               <div style="align: left; font-family: verdana,Arial, Helvetica, sans-serif; font-size: 13px; color: #fff;">
                                   Any other comments? 
                               </div>
                           </td>
                       </tr>
                       <tr>
                           <td  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: left; font-weight: bold;">
                               <div>
                                   <textarea id="COMMENT_4" runat="server" style="overflow: auto; width: 100%; height: 50px !important; color: #666; border: 1px solid #ccc; font-family: Verdana; font-size: 11px;" rows="5" cols="100">     </textarea>
                                   <br />
                                   <br />
                                   <br />
                               </div>
                           </td>
                       </tr> 


                 
                <tr>
                    <td id="A13" colspan="5" style="border-width: 0px; white-space: nowrap; text-align: center; font-weight: bold; width: 100%;"></td>
                </tr>
                <tr>
                    <td id="A6"  colspan="7" style="border-width: 0px; white-space: nowrap; text-align: right; width: 100%;"></td>
                </tr>
                   </tbody>
               </table>
                 

                
                <%--<table align="left" cellpadding="5" cellspacing="0" style="width: 950px" id="tblKTAA" runat="server" class="BlueTable_simple">--%>
                <table class="BlueTable_simple" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; width: 950px;">
                    <tr class="mainheading">
                        <td colspan="5">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Key Take-Aways and Actions</span></font>
                        </td>
                    </tr>


                    <tr class="trSub_Header">
                        <td align="left" class="" colspan="5">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana"><span style="font-family: Verdana">What are your key take-away learnings from the professional development?</span></font>
                        </td>
                    </tr>
                    <tr valign="middle" style="background-color: white;">
                        <td colspan="5">
                            <table width="100%" cellpadding="4" cellspacing="4" style="background-color: white;">
                                <tr class="trStyleOdd">
                                    <td align="center" style="font-weight: bold; color: #FF0000" valign="top">*&nbsp;<asp:TextBox ID="COMMENT_5" runat="server" TextMode="MultiLine" Width="97.5%" Height="50px" SkinID="MultiText"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="trStyleEven">
                                    <td align="center" style="color: #000000">

                                        <asp:TextBox ID="COMMENT_6" runat="server" TextMode="MultiLine" Width="97.5%" Height="50px" SkinID="MultiText"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="trStyleOdd">
                                    <td align="center" style="color: #000000">

                                        <asp:TextBox ID="COMMENT_7" runat="server" TextMode="MultiLine" Width="97.5%" Height="50px" SkinID="MultiText"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="trSub_Header">
                        <td align="left" class="" colspan="5">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">What actions will you now take to implement the new learning? Consider both self
                        (own practice) and others (departmental / school level impact) when writing action
                        steps.</span></font>
                        </td>
                    </tr>
                    <tr id="tableActions" runat="server">
                        <td class="style1" colspan="5" style="width: 100%">
                            <table width="100%" cellpadding="4" cellspacing="4">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvActions" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                                            Width="100%" EnableModelValidation="True" Style="overflow: scroll;" OnRowDataBound="gvActions_RowDataBound">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="S. No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMandatory" runat="server" Text='*' Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
                                                        &nbsp;<asp:Label ID="lblSNo" runat="server" Text='<%# bind("SR_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action Step / Objectives ">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("TEXTBOX1") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TEXTBOX1" runat="server" TextMode="MultiLine" Text='<%# Bind("TEXTBOX1")%>' Style="width: 100%; height: 100px; margin: 0; padding: 0; border-width: 0" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Success Criteria /Expected Outcome">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TEXTBOX2" runat="server" Text='<%# Bind("TEXTBOX2") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TEXTBOX2" runat="server" SkinID="MultiText" TextMode="MultiLine" Style="width: 100%; height: 100px; margin: 0; padding: 0; border-width: 0" Text='<%# Bind("TEXTBOX2")%>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="18%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Timeline">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TEXTBOX3" runat="server" Text='<%# Bind("TEXTBOX3")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TEXTBOX3" runat="server" SkinID="MultiText" TextMode="MultiLine" Style="width: 100%; height: 100px; margin: 0; padding: 0; border-width: 0" Text='<%# Bind("TEXTBOX3")%>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText=" Resources / Preparation / Support needed">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TEXTBOX4" runat="server" Text='<%# Bind("TEXTBOX4")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TEXTBOX4" runat="server" SkinID="MultiText" TextMode="MultiLine" Style="width: 100%; height: 100px; margin: 0; padding: 0; border-width: 0" Text='<%# Bind("TEXTBOX4")%>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="18%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText=" Relevance to role">
                                                    <EditItemTemplate>
                                                        <%--<asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("PD_KTA_RelevanceText") %>'></asp:TextBox>--%>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRelevanceId" runat="server" Text='<%# bind("PD_KTA_RELEVANCE") %>' Visible="false" ></asp:Label>

                                                        <asp:CheckBoxList ID="cblRelevance" runat="server" BorderColor="MenuHighlight" BorderStyle="Solid" BorderWidth="1px" CellPadding="2" CellSpacing="2" CssClass="matters" Height="10px" RepeatColumns="1" Width="100%">
                                                         <asp:ListItem Value="1">Personal Performance Management</asp:ListItem>
                                                            <asp:ListItem Value="2">Designated Responsibilities</asp:ListItem>
                                                            <asp:ListItem Value="3">School Development Plan</asp:ListItem>
                                                            <asp:ListItem Value="4">Other</asp:ListItem>
                                                        </asp:CheckBoxList>
                                                        <asp:TextBox ID="txtRelevanceOther" runat="server" Width="80%" ></asp:TextBox><%--Text='<%# Bind("OTHERTEXT")%>'--%>
                                                        <ajaxToolkit:TextBoxWatermarkExtender ID="txtRelevanceOtherw" runat="server" WatermarkCssClass="watermarked" WatermarkText="please state(other)" TargetControlID="txtRelevanceOther"></ajaxToolkit:TextBoxWatermarkExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="trStyleEven" Height="25px" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="titleText" Height="25px" BackColor="#003366" />
                                            <AlternatingRowStyle CssClass="trStyleOdd" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="5" style="text-align: center">
                            
                    <asp:Button ID="btnDraftEval" runat="server" CssClass="EVALBUTTON" Text="SAVE AS DRAFT" />&nbsp;
                    <asp:Button ID="btnSaveEval" runat="server" Text="SAVE AND SUBMIT" CssClass="EVALBUTTON" OnClientClick="return SubmitEvalClick();"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div class="footer">
                                <div class="in">Copyright � 2016 GEMS Education. All Rights Reserved.</div>
                            </div>
                        </td>
                    </tr>
                </table>

            </form>
        </center>



    </div>
</body>
</html>



