Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports SurveyChart
Imports System.Drawing
Imports InfoSoftGlobal

Partial Class Tellal_comPrintSurveyResults_PDConsol
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Private intHeadColCnt As Integer = 0
    Private nmvAnswers As New NameValueCollection
    Private nmvAnswerDesc As New NameValueCollection
    Private nmvSurvResults As New NameValueCollection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            SetFormatTable()
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            Dim str_Conn = ConnectionManger.GetTellalPDConnectionString
            If Not IsPostBack Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Try
                    ViewState("SurveyId") = "185"
                    ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("PD_CM_VAL").Replace(" ", "+"))
                    'ViewState("Emp_ID") = Encr_decrData.Decrypt(Request.QueryString("Emp_Id").Replace(" ", "+"))
                    CurBsUnit = "999998"
                    Session("Bsuid") = CurBsUnit
                Catch ex As Exception

                End Try
                Session("gm") = Nothing

                'Session("SurveyId") = cmbSurvey.SelectedValue
                SetFormatTable()
                BuildQuestions()


                str_Conn = ConnectionManger.GetTellalPDConnectionString
                Dim Param_Participants(3) As SqlClient.SqlParameter
                Param_Participants(0) = New SqlClient.SqlParameter("@CM_IDs", ViewState("CM_ID"))
                Param_Participants(1) = New SqlClient.SqlParameter("@FILTER_COND", "")
                Param_Participants(2) = New SqlClient.SqlParameter("@Attendance", "P")
                Dim dsParticipants As DataSet
                dsParticipants = SqlHelper.ExecuteDataset(str_Conn, CommandType.StoredProcedure, "dbo.Get_Participants_List_For_Course", Param_Participants)
                If Not dsParticipants Is Nothing Then
                    'TSent.Text = dsParticipants.Tables(0).Rows.Count
                    TSent.Text = dsParticipants.Tables(0).Select("Attendance = 'PRESENT'").Length

                    dsParticipants.Tables.Clear()
                    dsParticipants.Dispose()
                End If

                'Get Response Count
                'str_Conn = ConnectionManger.GetTellalPDConnectionString
                

                'Dim Param(2) As SqlClient.SqlParameter
                'Param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))

                'Using SENDDETAILS As SqlDataReader = SqlHelper.ExecuteReader(str_Conn, CommandType.StoredProcedure, "dbo.SURVEYRESPONSECOUNT_PD", Param)
                '    If SENDDETAILS.HasRows Then
                '        While SENDDETAILS.Read
                '            ' TSent.Text = Convert.ToString(SENDDETAILS("SENDCOUNT")) 'Sent

                '            TResponse.Text = Convert.ToString(SENDDETAILS("RESPONSECOUNT")) 'Response
                '        End While
                '    End If
                'End Using
                'lblPDTITLE.Text = GetPD_TITLE(ViewState("CM_ID"))
                GetPD_TITLE(ViewState("CM_ID"))

            End If

        Catch ex As Exception
            Dim ss As String = ex.Message
        End Try
    End Sub


    Private Function GetCourseType() As String

        Dim connection As String = ConnectionManger.GetTellalPDConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT DISTINCT CM_COURSE_TYPE_ID FROM dbo.COURSE_M where CM_ID in (" & ViewState("CM_ID") & ")"
        Dim courseType As String
        courseType = SqlHelper.ExecuteScalar(connection, CommandType.Text, strSQL)
        Return courseType

    End Function
    Private Sub SetFormatTable()
        With TabResults
            .Width = "827"
            .Height = "347"
            .ForeColor = Drawing.Color.Black
            .BackColor = Drawing.Color.White
            .Style.Value = ""
            .BorderColor = Drawing.Color.Black
            .BorderWidth = "1"
            .Font.Size = "10"
        End With
    End Sub


    Private Sub BuildQuestions()

        Dim strXML As String
        Dim strXML_Online As String
        Dim ANSWER As String = String.Empty
        Dim TOT_ANSWER As Decimal
        Dim arr() As String
        arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))
        param(1) = New SqlClient.SqlParameter("@ReportType", 2)

        Dim HasRowInFaceToFace As Boolean = False
        Dim check1 As Boolean = False
        Dim HasRowInOnline As Boolean = False
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "dbo.Get_ConsolidatedReport", param)

            strXML = ""
            strXML_Online = ""
            strXML = strXML & "<graph caption='EF Analysis' xAxisName='' yAxisName='Average' decimalPrecision='2'  yAxisMaxValue='4'  numberSuffix='' formatNumberScale='1' rotateNames='1' labelPadding='200' >"
            strXML_Online = strXML_Online & "<graph caption='EF Analysis' xAxisName='' yAxisName='Average' decimalPrecision='2'  yAxisMaxValue='4'  numberSuffix='' formatNumberScale='1' rotateNames='1' labelPadding='200' >"
            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read
                    If (readerStudent_Detail("coursetype") = "1") Then
                        If Convert.ToInt32(readerStudent_Detail("response")) > 0 Then
                            HasRowInFaceToFace = True
                        End If


                        strXML = strXML & "<set name=" + "'" & readerStudent_Detail("GroupName") & "' value=" + "'" & readerStudent_Detail("Aveg") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        'If check1 = False Then
                        '    strXML = strXML & "<set name=" + "'Total' value=" + "'" & readerStudent_Detail("Total") & "' color=" + "'" & arr(0) & "' hoverText=''  />"

                        '    check1 = True
                        'End If
                    Else
                        If Convert.ToInt32(readerStudent_Detail("response")) > 0 Then
                            HasRowInOnline = True
                        End If

                        strXML_Online = strXML_Online & "<set name=" + "'" & readerStudent_Detail("GroupName") & "' value=" + "'" & readerStudent_Detail("Aveg") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                    End If
                End While
            End If
            strXML = strXML & "</graph>"
            strXML_Online = strXML_Online & "</graph>"



            If HasRowInOnline = False Then
                trOnline.Visible = False
            Else
                cell_online.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML_Online, "myNext1", "800", "450", False)
            End If

            If HasRowInFaceToFace = False Then
                trFacetoFAce.Visible = False
            Else
                cell_face.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "800", "450", False)
            End If
        End Using


    End Sub
     
    Private Function GetPD_TITLE(ByVal CM_ID As String) As String
     
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString

        Dim strSQL As String = ""
        strSQL = "SELECT CM_TITLE FROM dbo.COURSE_M WHERE CM_ID In (Select ID FROM dbo.fnSplitMe('" & CM_ID & "', '|'))"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then

            Me.lstCourses.DataSource = dsResponse.Tables(0)
            Me.lstCourses.DataBind()

            Dim tmpStr As String = String.Empty
            For Each row As DataRow In dsResponse.Tables(0).Rows
                tmpStr &= row.Item("CM_Title") & ", "
            Next
            tmpStr = tmpStr.TrimEnd(" ")
            tmpStr = tmpStr.TrimEnd(",")
            'Me.DataList1.DataSource = dsResponse.Tables(0)
            'Me.DataList1.DataKeyField = "cm_title"
            'Me.DataList1.DataBind()
            Return tmpStr
        Else
            Return ""
        End If

    End Function
    



End Class
