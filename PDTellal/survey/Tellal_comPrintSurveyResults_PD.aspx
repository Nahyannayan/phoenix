<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Tellal_comPrintSurveyResults_PD.aspx.vb" Inherits="Tellal_comPrintSurveyResults_PD" title="Untitled Page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Survey Results Analysis</title>
     <script type="text/javascript">
    //window.print();
    </script>
    <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../cssfiles/tabber.js"></script> 
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
 function GetResult(Qid,SurvId,Result) 
 {
 //alert(Qid);
 window.showModalDialog("comSurvPopup.aspx?Qid="+Qid+"&SurvId="+SurvId+"&Result='"+Result+"'","null","height=200,width=550,status=yes,toolbar=no,menubar=no,location=no");
 }
 
</script>
    <style type="text/css">
        .tableTR
        {
            background-color: azure;
        }

    </style>
 </head>
<body>
<form id="form1" runat="server">
    <div>
   
    <table border="0"  cellpadding="5" cellspacing="0" style="width: 845px">
       
        <tr>
            <td align="center" class="subheader_img" colspan="2" style="height: 5px; text-align: left;background-image: inherit!important;background-color:forestgreen!important;">
                <asp:Label id="lblPDTITLE" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
            <table border="0">
                <tr>
                <td style="width: 273px; color: white;background-color:forestgreen!important;background-image: inherit!important;" class="subheader_img" >Total Participants : </td>
                <td style="width: 257px; font-size: 12px; font-family: Verdana;background-color:forestgreen!important;background-image: inherit!important;" class="subheader_img"><asp:Label id="TSent" runat="server" Width="41px" Font-Bold="True" ForeColor="white"></asp:Label></td>
                <td style="width: 231px; color: white;background-color:forestgreen!important;background-image: inherit!important;" class="subheader_img">Total Response :</td>
                <td style="width: 203px;background-color:forestgreen!important;background-image: inherit!important;" class="subheader_img"><asp:Label id="TResponse" runat="server" Width="41px" ForeColor="white"></asp:Label></td>
                </tr>
            </table>    
           </td>
        </tr>
               
        <tr>
            <td align="left" colspan="2" style="height: 347px" valign="top">
                <asp:Table id="TabResults" runat="server" Width="827px">
                    <asp:TableRow BorderStyle="Solid" BorderWidth="5px">
                        <asp:TableHeaderCell>Sr.No	
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>  Preparation and Planning	
                        </asp:TableHeaderCell>
                          <asp:TableHeaderCell> #
                        </asp:TableHeaderCell>
                    </asp:TableRow>
                    <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            1
                        </asp:TableCell>
                        <asp:TableCell>
                            The trainer / s were well prepared for the session	 
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S1Q1">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            2
                        </asp:TableCell>
                        <asp:TableCell>
                           Learning objectives were clear and appropriate	 
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S1Q2">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            3
                        </asp:TableCell>
                        <asp:TableCell>
                           Learning objectives were shared effectively 
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S1Q3">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                          4
                        </asp:TableCell>
                        <asp:TableCell>
                           The schedule provided sufficient time to cover all of the proposed activities
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S1Q4">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                     <asp:TableRow>
                        <asp:TableHeaderCell>Sr.No	
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell> Use of resources and facilities
                        </asp:TableHeaderCell>
                          <asp:TableHeaderCell> #
                        </asp:TableHeaderCell>
                    </asp:TableRow>
                     <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            1
                        </asp:TableCell>
                        <asp:TableCell>
                            Resources and materials were used appropriately	 
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S2Q1">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            2
                        </asp:TableCell>
                        <asp:TableCell>
                           Facilities were utilised effectively
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S2Q2">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            3
                        </asp:TableCell>
                        <asp:TableCell>
                           Use of resources enabled objectives to be met
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S2Q3">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                     
                     <asp:TableRow>
                        <asp:TableHeaderCell>Sr.No	
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell> Delivery skills 
                        </asp:TableHeaderCell>
                          <asp:TableHeaderCell> #
                        </asp:TableHeaderCell>
                    </asp:TableRow>
                     <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            1
                        </asp:TableCell>
                        <asp:TableCell>
                           Ways of Working were honoured and maintained
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S3Q1">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            2
                        </asp:TableCell>
                        <asp:TableCell>
                           The trainer / s were appropriately knowledgeable
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S3Q2">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            3
                        </asp:TableCell>
                        <asp:TableCell>
                          The trainer / s were receptive to questioning and feedback
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S3Q3">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                     <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                           4
                        </asp:TableCell>
                        <asp:TableCell>
                           The trainer / s adopted appropriate voice projection and body language
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S3Q4">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                           5
                        </asp:TableCell>
                        <asp:TableCell>
                           The trainer / s established a good rapport with the audience
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S3Q5">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                           6
                        </asp:TableCell>
                        <asp:TableCell>
                          The trainer / s were enthusiastic and passionate
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S3Q6">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                        <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                           7
                        </asp:TableCell>
                        <asp:TableCell>
                          The pace of the training was appropriate
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S3Q7">
                            
                        </asp:TableCell>
                    </asp:TableRow>


                      <asp:TableRow>
                        <asp:TableHeaderCell>Sr.No	
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell> Content 
                        </asp:TableHeaderCell>
                          <asp:TableHeaderCell> #
                        </asp:TableHeaderCell>
                    </asp:TableRow>
                     <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            1
                        </asp:TableCell>
                        <asp:TableCell>
                           The content covered was relevant and suitable to participants
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S4Q1">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            2
                        </asp:TableCell>
                        <asp:TableCell>
                           The content was reflective of objectives
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S4Q2">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            3
                        </asp:TableCell>
                        <asp:TableCell>
                          There was adequate opportunity to explore theory and research
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S4Q3">
                            
                        </asp:TableCell>
                    </asp:TableRow>


                     <asp:TableRow>
                        <asp:TableHeaderCell>Sr.No	
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell> Activities 
                        </asp:TableHeaderCell>
                          <asp:TableHeaderCell> #
                        </asp:TableHeaderCell>
                    </asp:TableRow>
                     <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            1
                        </asp:TableCell>
                        <asp:TableCell>
                           The trainer/s effectively used various techniques and activities to engage the audience
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S5Q1">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            2
                        </asp:TableCell>
                        <asp:TableCell>
                           The activities were appropriate and relevant to the topic
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S5Q2">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            3
                        </asp:TableCell>
                        <asp:TableCell>
                          The trainer/s effectively used grouping techniques to encourage collaboration and sharing
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S5Q3">
                            
                        </asp:TableCell>
                    </asp:TableRow>



                     <asp:TableRow>
                        <asp:TableHeaderCell>Sr.No	
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell> Participant learning  
                        </asp:TableHeaderCell>
                          <asp:TableHeaderCell> #
                        </asp:TableHeaderCell>
                    </asp:TableRow>
                     <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            1
                        </asp:TableCell>
                        <asp:TableCell>
                           The workshop level was appropriate for you
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S6Q1">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            2
                        </asp:TableCell>
                        <asp:TableCell>
                          The workshop has enhanced your knowledge and understanding
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S6Q2">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                            3
                        </asp:TableCell>
                        <asp:TableCell>
                          The workshop will be useful toward your professional practice
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S6Q3">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                           4
                        </asp:TableCell>
                        <asp:TableCell>
                           You engaged well in group discussion and activity
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S6Q4">
                            
                        </asp:TableCell>
                    </asp:TableRow>

                      <asp:TableRow CssClass="tableTR">
                        <asp:TableCell>
                           5
                        </asp:TableCell>
                        <asp:TableCell>
                          You maintained and honoured the Ways of Working
                        </asp:TableCell>
                        <asp:TableCell runat="server" ID="S6Q5">
                            
                        </asp:TableCell>
                    </asp:TableRow>
                 </asp:Table><table style="width: 827px">
                   
                </table>
                <%--<asp:Button id="Button1" runat="server" onclick="Button1_Click" Text="Show Result" />--%></td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>

