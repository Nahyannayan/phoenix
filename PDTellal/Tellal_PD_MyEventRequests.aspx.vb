Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Partial Class Tellal_PD_MyEventRequests
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim ddlGroup As Object

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

     
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                Me.Bind_Region()
                Me.Bind_Location(Me.ddlRegion.SelectedValue)

                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                'show all requests by default
                Me.optAll.Checked = True
                Me.SearchData("O")
                Me.gvRequest.Columns(6).Visible = True

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub gvRequest_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
           
            Dim lnkCertificatePdf As LinkButton = DirectCast(e.Row.FindControl("lnkCertificatePdf"), LinkButton)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkCertificatePdf)

        End If
    End Sub

    Protected Sub lnkCertificatePdf_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim CR_ID As String = sender.CommandArgument.ToString

            ''added by nahyan to find event date is after sep 2015 to display new certificate (5-oct-2015)

            Dim eventdate As String = GetEventDate(CR_ID)
            Dim reportpath As String = String.Empty

            If Convert.ToDateTime(eventdate) > "01/Sep/2019" Then
                reportpath = "Reports/PD_Certificate_TELLAL_2019.rpt"
            ElseIf Convert.ToDateTime(eventdate) < "01/Sep/2015" Then
                reportpath = "Reports/PD_CertificateLS_Before1Sep2015.rpt"
            ElseIf Convert.ToDateTime(eventdate) < "30/Aug/2016" Then

                reportpath = "Reports/PD_CertificateLS.rpt"
            ElseIf Convert.ToDateTime(eventdate) < "30/Sep/2018" Then
                reportpath = "Reports/PD_Certificate_TELLAL_2017.rpt"
            ElseIf Convert.ToDateTime(eventdate) < "30/Sep/2018" Then
                reportpath = "Reports/PD_Certificate_TELLAL_2017.rpt"
            Else
                reportpath = "Reports/PD_Certificate_TELLAL.rpt"
            End If
            ''reportpath = "/PHOENIXBETA/pdtellal/Reports/PD_Certificate_TELLAL.rpt"


            ''ends here 
            Dim param As New Hashtable
            param.Add("@CR_ID", CR_ID)
            param.Add("@EMP_ID", 0)
            param.Add("@User_ID", 0)

            Dim rptClass As New rptClassTellal
            With rptClass
                .crDatabase = "Tellal_PD"
                .reportParameters = param
                .reportPath = Server.MapPath(reportpath)
            End With

            Dim rptDownload As New ReportDownloadTellal
            rptDownload.LoadReports_Tellal(rptClass, rs)
            rptDownload = Nothing




            'Dim filepath As String = String.Empty
            'Dim EnqCode As String = String.Empty
            'Dim html As String = ScreenScrapeHtml(Server.MapPath("~\PD\PD_Certificate.htm"))
            'BindCourseDetails(Convert.ToInt32(CR_ID), html)
            'HTMLToPdf(html.ToString, "PD", filepath, True)
        Catch ex As Exception

        End Try
    End Sub

   
    Function GetEventDate(ByVal crId As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim str_Sql As String
            Dim eventDate As String = String.Empty

            str_Sql = " SELECT CM_EVENT_DT FROM dbo.COURSE_M WHERE CM_ID IN (SELECT CR_CM_ID FROM dbo.COURSE_REQ WHERE CR_ID=" & crId & ")"
            eventDate = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
            Return eventDate
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Function
      
    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtFromDate.Text <> "" Then
                Dim strfDate As String = txtFromDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"
            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If

            Return ErrorStatus
        Catch ex As Exception
            'UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function

        
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try


        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True

        'smScriptManager.RegisterPostBackControl(gvInfo)
        'Catch ex As Exception

        'End Try
    End Sub
    Protected Sub gvInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) 'Handles gvInfo.RowDataBound
        Try


        Catch ex As Exception

        End Try


    End Sub

    Protected Sub gvRequest_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequest.RowCommand
        
    End Sub


    Protected Sub gvStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRequest.RowDataBound
        
    End Sub

    Private Sub SearchData(ByVal Status As String)

        Dim Sgm_Id, Stu_Id, Apd_Id, From_Date, To_Date, Remarks, Ssa_Id ', status As String
        Dim ddlStatus As DropDownList

        Me.txtFromDate.Text = "01/Jan/" & Now.Year.ToString
        Me.txtToDate.Text = "31/Dec/" & Now.Year.ToString

        If txtFromDate.Text.Contains("/") Then
            From_Date = txtFromDate.Text.Split("/")(2) & "-" & txtFromDate.Text.Split("/")(1) & "-" & txtFromDate.Text.Split("/")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("/")(2) & "-" & txtToDate.Text.Split("/")(1) & "-" & txtToDate.Text.Split("/")(0) & " 23:59:59"
        ElseIf txtFromDate.Text.Contains("-") Then
            From_Date = txtFromDate.Text.Split("-")(2) & "-" & txtFromDate.Text.Split("-")(1) & "-" & txtFromDate.Text.Split("-")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("-")(2) & "-" & txtToDate.Text.Split("-")(1) & "-" & txtToDate.Text.Split("-")(0) & " 23:59:59"
        Else
            From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text) & " 00:00:01"
            To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text) & " 23:59:59"
        End If

        'From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)
        'To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text)

        Dim transaction As SqlTransaction

        Dim Conn As SqlConnection = ConnectionManger.Get_TellalPD_Connection
        Dim cmd As SqlCommand

        Try

            Dim params(8) As SqlParameter
            'params(0) = New SqlParameter("@bsu_id", Me.hfBsuId.Value)
            params(0) = New SqlParameter("@bsu_id", Session("sBsuid"))
            params(1) = New SqlParameter("@region", Me.ddlRegion.SelectedValue)
            params(2) = New SqlParameter("@location", Me.ddlLocation.SelectedValue)
            params(3) = New SqlParameter("@from_date", Me.txtFromDate.Text)
            params(4) = New SqlParameter("@to_date", Me.txtToDate.Text)
            params(5) = New SqlParameter("@title", Me.txtTitle.Text)
            If Status = "P" Then
                params(6) = New SqlParameter("@status", "P")
            ElseIf Status = "A" Then
                params(6) = New SqlParameter("@status", "A")
            ElseIf Status = "R" Then
                params(6) = New SqlParameter("@status", "R")
            ElseIf Status = "O" Then
                params(6) = New SqlParameter("@status", "O")
            End If
            params(7) = New SqlParameter("@usr_id", Session("sUsr_Id"))


            Dim ds As DataSet = SqlHelper.ExecuteDataset(Conn, CommandType.StoredProcedure, "GET_MY_COURSE_REQUESTS_GEMS_STAFF", params)

            If Not ds Is Nothing Then
                ds.Tables(0).Columns.Add("COMBINED_DATE", GetType(String))
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    row.Item("COMBINED_DATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("EVENT_DATE")) & " to " & String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("EVENT_END_DATE"))
                    row.Item("Sr_No") = i + 1
                    i += 1
                Next
                ds.AcceptChanges()
                Me.gvRequest.DataSource = ds.Tables(0)
                Me.gvRequest.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error Occured While loading the data"
        Finally

        End Try

    End Sub

    

    Private Sub Bind_Region()
        Try
            Dim CONN As String = ConnectionManger.GetTellalPDConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[dbo].[GetRegion]")
             
            If Not ds Is Nothing Then
                ddlRegion.DataSource = ds.Tables(0)
                ddlRegion.DataValueField = "R_ID"
                ddlRegion.DataTextField = "R_REGION_NAME"
                ddlRegion.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading region"
        End Try
    End Sub

    Private Sub Bind_Location(Optional ByVal RegionId As Integer = 0)
        Try
            Dim CONN As String = ConnectionManger.GetTellalPDConnectionString

            Dim query As String

            If RegionId = 0 Then
                query = "SELECT 0 AS id, '[SELECT]' AS NAME"
            Else
                query = "SELECT * FROM (SELECT 0 AS id, '[SELECT]' AS NAME UNION  SELECT l.L_ID AS id, l.L_DESCR AS name FROM dbo.LOCATION L Where L.L_R_Id = " & RegionId & ") t ORDER BY NAME"
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlLocation.DataSource = ds.Tables(0)
                ddlLocation.DataValueField = "Id"
                ddlLocation.DataTextField = "Name"
                ddlLocation.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading location"
        End Try
    End Sub

    Protected Sub optPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPending.CheckedChanged
        Me.SearchData("P")
        'Me.gvRequest.Columns(6).Visible = False
        'Me.gvRequest.Columns(7).Visible = False
        'Me.gvRequest.Columns(9).Visible = True
    End Sub

    Protected Sub optApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optApproved.CheckedChanged
        Me.SearchData("A")
        'Me.gvRequest.Columns(6).Visible = False
        'Me.gvRequest.Columns(7).Visible = False
        'Me.gvRequest.Columns(9).Visible = False
    End Sub

    Protected Sub optRejected_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optRejected.CheckedChanged
        Me.SearchData("R")
        'Me.gvRequest.Columns(6).Visible = False
        'Me.gvRequest.Columns(7).Visible = False
        'Me.gvRequest.Columns(9).Visible = False
    End Sub

    Protected Sub optAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optAll.CheckedChanged
        Me.SearchData("O")
        'Me.gvRequest.Columns(6).Visible = True
        'Me.gvRequest.Columns(7).Visible = False
        'Me.gvRequest.Columns(9).Visible = True
    End Sub

    Protected Sub gvRequest_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvRequest.RowDeleting
         

    End Sub

    Protected Sub ddlRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion.SelectedIndexChanged
        Bind_Location(Me.ddlRegion.SelectedValue)
    End Sub


    Protected Function GetNavigateUrl(ByVal CM_ID As String, ByVal CRID As String, ByVal CM_EF_ENABLED As String) As String

        If CM_EF_ENABLED = "FALSE" Then
            Return ""
        End If

        Dim strUrl As String
        Dim PD_CM_ID As String = Encr_decrData.Encrypt(CM_ID)
        Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
        Dim EMP_ID As String = Encr_decrData.Encrypt(Session("EmployeeId"))
        CRID = Encr_decrData.Encrypt(CRID)
        Dim strCourseType As String = ""
        Dim strQuery As String
        Dim strRedirect As String = ""
        Dim strReturnValue As String = ""
        Dim CONN As String = ConnectionManger.GetTellalPDConnectionString
        strQuery = "SELECT CM_COURSE_TYPE_ID FROM dbo.COURSE_M WHERE CM_ID=" & Encr_decrData.Decrypt(PD_CM_ID)
        strCourseType = SqlHelper.ExecuteScalar(CONN, CommandType.Text, strQuery)
        If strCourseType = "1" Then
            strRedirect = "Survey/comSurKTAAEF.aspx"
        ElseIf strCourseType = "2" Then
            strRedirect = "Survey/comSurKTAAEFOnline.aspx"
        End If
        If strCourseType <> "" And strRedirect <> "" Then
            strUrl = String.Format("{0}?PD_CM_VAL={1}&BSU_ID={2}&EMP_ID={3}&CRID={4}", strRedirect, PD_CM_ID, BSU_ID, EMP_ID, CRID)
            strReturnValue = "javascript:var popup = window.open('" + strUrl + "', '',''); return false;"
        Else
            strReturnValue = "#"
        End If
        Return strReturnValue
    End Function

     
    Protected Sub lnkBackToCalendar_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/PDTellal/Tellal_PD_Calendar.aspx?MainMnu_code=" + Encr_decrData.Encrypt("U000045"), False)
        Exit Sub
    End Sub
End Class
