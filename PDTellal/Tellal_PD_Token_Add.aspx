﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Tellal_PD_Token_Add.aspx.vb" Inherits="Tellal_PD_Token_Add" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server"> 
     <title></title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
    </style>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
      <script>

         function addrowInTbl() {

             if ($("#txtTokenNum").val().length <= 0) {
                 alert("Please Enter the Token Numbers");
                 return false;
             }
             $("#tblToken").find("tr:gt(0)").remove();

             var counter = $("#txtTokenNum").val();

             var seq = $("#hdnSeq").val();
              

             for (cnt = 0; cnt < counter; cnt++) {
                 var newRow = $("<tr>");
                 var cols = "";
                 cols += '<td>' + (cnt + 1) + '</td>';
                 cols += '<td><span class="tokenClass">' + GenerateSeq(String(seq)) + '</span></td>';
                     //<input type="label" class="form-control tokenClass" name="txtToken' + counter + '"/></td>';
                // cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger" onclick="removeRow(this)" value="Delete"></td>';
                 newRow.append(cols);
                 $("#tblToken").append(newRow);
                 seq = parseInt(seq) + 1;
             }

             return false;
         }


         function GenerateSeq(sq)
         {
            // alert(sq.length);
             var tokenNum = "TKN";
             var zr = "";
             for (var cnt = 0; cnt < (7 - (sq.length)) ; cnt++)
             {
                 zr = zr + "0";
             }
             tokenNum = tokenNum + zr + sq;
             return tokenNum;
         }
         function removeRow(obj) {
             $(obj).closest('tr').remove();
         }

         function ValidateForm() {
             $("#hdnTokens").val("");
             var str = "";
             var chk = "0";

             $('.tokenClass').each(function (index, obj) {
                 //str = str + "," + obj.value;
                 str = str + "," + obj.innerHTML;
             });

             $("#hdnTokens").val(str);
             return true;
         }
         function fancyClose() {

             parent.$.fancybox.close();
             parent.location.reload();
         }

    </script>
    
</head>
<body>
    <form id="form1" runat="server">
         <asp:HiddenField ID="hdnSeq" runat="server" />
        <table id="tblCategory" width="100%">
            <tr>
                <td class="title-bg" colspan="4">Add Tellal PD Token's</td>
            </tr>
            
            
            <tr id="trGridv" runat="server">
                <td align="center">
                    <table width="100%"> 
                        <tr>
                            <td>
                               <telerik:RadScriptManager runat="server" ID="RadScriptManager1" /> 
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                <table width="80%">
                                    <tr id="trLabelError">
                                        <td align="left" class="matters" valign="bottom">
                                            <div id="lblError" runat="server">
                                            </div>
                                        </td>
                                    </tr> 
                                </table>
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                   <telerik:RadComboBox RenderMode="Lightweight" ID="ddlBSU" runat="server" Width="100%"
                                        AutoPostBack="false" EmptyMessage="- Select BSU -" AllowCustomText="true">
                                    </telerik:RadComboBox>
                                 </div>
                                <div class="col-xs-12 col-sm-4 col-md-4"> 
                                     <telerik:RadComboBox RenderMode="Lightweight" ID="ddlCourse" runat="server" Width="100%"
                                        AutoPostBack="false" EmptyMessage="- Select Course -" AllowCustomText="true" >
                                    </telerik:RadComboBox>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                </div>
                            </div>
                            
                                 
                             <div class="row mt-2"> 
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                     Number of Token's
                                </div>
                                  <div class="col-xs-12 col-sm-4 col-md-4">
                                      <div class="form-group">
                                        <asp:TextBox ID="txtTokenNum" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="col-xs-12 col-sm-4 col-md-4">
                                     <div>
                                         <asp:Button ID="btnGenerateToken" runat="server" class="button" Text="Generate Token" OnClientClick="return addrowInTbl();" />
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                      <table width="80%" class="table table-striped" id="tblToken" runat="server">
                                          <tr>
                                              <td>#</td> <td>Token Number</td>
                                          </tr>
                                         
                                          </table>
                                    <br />

                                    <br />
                                    
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-6 col-md-6" align="right">
                                    <asp:HiddenField ID="hdnTokens" runat="server" />
                                    <asp:Button ID="btnSubmit" runat="server" class="button" Text="Submit" OnClientClick="ValidateForm()" />
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                     <%--<asp:Button ID="btnBack" runat="server" class="btn btn-primary btn-block" Text="Go Back"/>--%>
                                    <input type="button" class="button" id="btnCancel1" title="CLOSE" value="CLOSE" onclick="fancyClose()" />
                                  </div>
                            </div>
                              </telerik:RadAjaxPanel>
                             
                            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                               <%-- <asp:Button ID="btnAddPDC" Text="ADD" runat="server" CssClass="button" ValidationGroup="rfAdd" />
                                <input type="button" class="button" id="btnCancel1" title="CLOSE" value="CLOSE" onclick="fancyClose()" />--%>

                            </td>
                        </tr>


                    </table>
                </td>
            </tr>


            </table>

    </form>
     
    
</body>
</html>
