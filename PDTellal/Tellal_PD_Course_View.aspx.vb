﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Partial Class Tellal_PD_Course_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                ViewState("CM_ID") = 0
                'bindBusinessUnits()
                BindPD_PD_Co_Course()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try


        End If

    End Sub


    Protected Sub lbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PDTellal\Tellal_Course_Add_Update.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String

            Dim lblCM_ID As New Label
            lblCM_ID = TryCast(sender.FindControl("lblID"), Label)
            ViewState("CM_ID") = lblCM_ID.Text
            lblError.InnerHtml = ""

            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "edit"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim CMId As String = Encr_decrData.Encrypt(lblCM_ID.Text)
            url = String.Format("~\PDTellal\Tellal_Course_Add_Update.aspx?MainMnu_code={0}&datamode={1}&cmId={2}", ViewState("MainMnu_code"), ViewState("datamode"), CMId)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvPDTrainsers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPDTrainsers.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            'Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)

            'Dim lblId As System.Web.UI.WebControls.Label = CType(e.Row.FindControl("lblID"), System.Web.UI.WebControls.Label)
            'Dim lnkEdit As System.Web.UI.WebControls.HyperLink = CType(e.Row.FindControl("lnkEdit"), System.Web.UI.WebControls.HyperLink)
            'Dim url As String
            'url = String.Format("~\PDTellal\Tellal_PD_Trainer_Add.aspx?Id=" + lblId.Text)
            'lnkEdit.NavigateUrl = url


        End If
    End Sub

    Protected Sub gvPDTrainsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDTrainsers.PageIndex = e.NewPageIndex
        BindPD_PD_Co_Course()
    End Sub

    Protected Sub btnSearchCM_TITLE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Course()
    End Sub

    Protected Sub btnSearchCM_EVENT_DT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Course()
    End Sub

    Protected Sub btnSearchCM_START_TIME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Course()
    End Sub
    Protected Sub btnSearchCM_MAX_CAPACITY_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Course()
    End Sub

    Protected Sub btnSearchCM_END_TIME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Course()
    End Sub


    Sub BindPD_PD_Co_Course()
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString

        Dim param(5) As SqlClient.SqlParameter

        Dim str_CM_title As String = String.Empty
        Dim str_EMP_Name As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim CM_title As String = String.Empty
        Dim EMP_Name As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim Dt As New DataTable
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty
        Try

            If gvPDTrainsers.Rows.Count > 0 Then

                txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtCM_TITLE")

                If txtSearch.Text.Trim <> "" Then
                    CM_title = " AND replace(CM_Title,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_CM_title = txtSearch.Text.Trim
                End If
                 

            End If
            FILTER_COND = CM_title

            param(0) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[Get_Course_View]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                gvPDTrainsers.DataSource = ds.Tables(0)
                gvPDTrainsers.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvPDTrainsers.DataSource = ds.Tables(0)
                Try
                    gvPDTrainsers.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvPDTrainsers.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPDTrainsers.Rows(0).Cells.Clear()
                gvPDTrainsers.Rows(0).Cells.Add(New TableCell)
                gvPDTrainsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDTrainsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDTrainsers.Rows(0).Cells(0).Text = "No records available !!!"
            End If

            txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtCM_TITLE")
            txtSearch.Text = str_CM_title

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try

    End Sub

    Protected Sub lbtnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim lblCM_ID As New Label()
        lblCM_ID = TryCast(sender.FindControl("lblID"), Label)
        ViewState("CM_ID") = lblCM_ID.Text

        Dim dt As DataSet = Bind_PD_Participants()

        'Create a dummy GridView
        Dim GridView1 As New GridView()


        If dt.Tables(0).Rows.Count > 0 Then
            GridView1.AllowPaging = False
            GridView1.DataSource = dt
            GridView1.DataBind()
        Else

            dt.Tables(0).Rows.Add(dt.Tables(0).NewRow())

            GridView1.DataSource = dt.Tables(0)
            Try
                GridView1.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = GridView1.Rows(0).Cells.Count
            ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell)
            GridView1.Rows(0).Cells(0).ColumnSpan = columnCount
            GridView1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GridView1.Rows(0).Cells(0).Text = "No participants exists !!!"
        End If


        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", _
             "attachment;filename=ParticipantsList.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        For i As Integer = 0 To GridView1.Rows.Count - 1
            'Apply text style to each Row
            GridView1.Rows(i).Attributes.Add("class", "textmode")
        Next
        GridView1.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()


    End Sub

    Function Bind_PD_Participants() As System.Data.DataSet
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim param(1) As SqlClient.SqlParameter

        Dim Dt As New System.Data.DataTable

        param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))
        '  param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Participants_List", param)

        If ds.Tables(0).Rows.Count > 0 Then



        End If
        Return ds
    End Function


     
End Class
