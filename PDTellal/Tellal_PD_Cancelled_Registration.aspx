﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_PD_Cancelled_Registration.aspx.vb" Inherits="Tellal_PD_Cancelled_Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Cancelled Registrations
        </div>
        <div class="card-body">
            <div class="table-responsive">
     <table id="Table1" width="100%" border="0">
        <tbody>
           

             <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom" colspan="2">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        </tbody>
    </table>
     <table id="tbl_AddGroup" runat="server"
        cellspacing="0" width="100%" >

                <tr>
            <td  colspan="6">
                <table width="100%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <span style="font-size: 10pt; color: #ffffff">
                                <asp:Literal ID="ltLabel" runat="server" Text="Search Criteria"></asp:Literal></span>
                        </td>
                    </tr>
                    </table>
                </td>
                    </tr>
        <tr>
            <td > <span class="field-label" >Academic Year</span></td>
           
            <td >

                <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged"></asp:DropDownList>
            </td>

       
                        <td >
                         <span class="field-label" >  Course </span>
                        </td>
                      
                        <td align="left" >
                            <asp:DropDownList ID="ddlCourse" runat="server"  Width="400px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvCourse" runat="server" ValidationGroup="AttGroup" ErrorMessage="Please select" ControlToValidate="ddlCourse" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
           <tr id="tr1" runat="server" >
                        <td >
                         <span class="field-label" >  Staff Name </span>
                        </td>
                       
                        <td align="left" colspan="4">
                            <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                            </td>
               </tr>
           <tr>
            <td colspan="6">

                <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="button" ValidationGroup="AttGroup"  />
            </td>

        </tr>
         </table>
    <table id="tblGridView" runat="server" width="100%">
         <tr>

             <td colspan="6">
                 <asp:GridView ID="gvParticipantList" runat="server"  AutoGenerateColumns="False" CssClass="table table-bordered table-row" OnPageIndexChanging="gvParticipantList_PageIndexChanging"
                    Height="100%" Width="100%" EnableModelValidation="True" SkinID="GridViewView" AllowPaging="true" PageSize="25"  HeaderStyle-Height="30"  >
                   
                     <Columns>
                           <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                           <asp:TemplateField HeaderText="Title">
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" __designer:wfdid="w18" Text='<%# Bind("CM_TITLE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDate" runat="server" __designer:wfdid="w18" Text='<%# Bind("COMBINED_DATE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Participant">
                            <ItemTemplate>
                                <asp:Label ID="lblParticipant" runat="server" __designer:wfdid="w18" Text='<%# Bind("EMPLOYEE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                         

                           <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                  
                                    <asp:LinkButton ID="lnkRestoreReg" runat="server"
                                    OnClick="lnkRestoreReg_Click" OnClientClick="return confirm('Are you sure you want to restore the cancelled PD request?');"   CommandArgument='<%# Bind("CR_ID")%>'>RESTORE</asp:LinkButton>&nbsp;
                                    
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                     </Columns>
                        <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop" Height="15px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Font-Size="11px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Height="25px" />
                 </asp:GridView>
             </td>
         </tr>
         </table>
    <table id="tblNoAccess" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" visible="false">
        <tr>
            <td align="center">
               
                   
                        <asp:Label id="lblNoAccess" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label>
                  
                        
              
            </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
                </div></div></div>
</asp:Content>

