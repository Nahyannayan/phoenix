﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Tellal_PD_Trainer_Add.aspx.vb" Inherits="Tellal_PD_Trainer_Add" %>


<!DOCTYPE html>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server"> 
     <title></title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }

        .pageDDL{
            border-color: #dee2da!important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1)!important;
            border-radius: 6px!important;
            border: 11px!important;
        }
    </style>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
      <script>

            
        function fancyClose() {

            parent.$.fancybox.close();
            parent.location.reload();
        }
    </script>
    <style>
        .emailClass {
            border-color: #dee2da!important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1)!important;
            padding: 10px!important;
            min-width: 80% !important;
            width: 75%!important;
            font-size: 16px !important;
            color: #333!important;
        }
    </style>
</head>
<body>
     <form id="form1" runat="server">
         <asp:HiddenField ID="hdnSeq" runat="server" />
         <table id="tblCategory" width="98%">
             <tr>
                 <td class="title-bg" colspan="4">Add/Update trainer</td>
             </tr>
             <tr id="trGridv" runat="server">
                 <td align="center">
                     <table width="100%">
                         <tr>
                             <td><telerik:RadScriptManager runat="server" ID="RadScriptManager1" /> 
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                
                            <asp:Label ID="lblResult" runat="server" EnableViewState="False" CssClass="error" ForeColor="Red" Style="color:red!important" />

                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <asp:RadioButton ID="rdIsGemsStaff" GroupName="IsGemsStaff" Text="Is GEMS Staff" runat="server"  AutoPostBack="true" OnCheckedChanged="Group1_CheckedChanged" />
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6"> 
                                    <asp:RadioButton ID="rdIsContractEmp" GroupName="IsGemsStaff" Text="Other" runat="server" AutoPostBack="true" OnCheckedChanged="Group1_CheckedChanged"  />
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                </div>
                            </div>
                            
                                
                            <div class="row mt-2" id="divBSU" runat="server" visible="false">

                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <telerik:RadComboBox RenderMode="Lightweight" ID="ddlBSU" runat="server" Width="80%" 
                                        AutoPostBack="true" EmptyMessage="- Select BSU -" AllowCustomText="true"
                                        OnSelectedIndexChanged="ddlBSU_SelectedIndexChanged"  >
                                    </telerik:RadComboBox>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <telerik:RadComboBox RenderMode="Lightweight" ID="ddlEmp" runat="server"  Width="80%"
                                        AutoPostBack="true" EmptyMessage="- Select Employee -" OnSelectedIndexChanged="ddlEmp_SelectedIndexChanged">
                                    </telerik:RadComboBox>  
                                </div>
                            </div>
                         
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtFName" runat="server" placeholder="Enter First Name" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtMName" runat="server" placeholder="Enter Middle Name" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div> 
                             
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                       <asp:TextBox ID="txtLastName" runat="server" placeholder="Enter Last Name" class="form-control" ></asp:TextBox> 
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                          <asp:TextBox ID="txtEmail" TextMode="Email" runat="server" placeholder="Enter Email" class="form-control emailClass" aria-describedby="emailHelp"></asp:TextBox> 
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtMobile" runat="server" placeholder="Enter Mobile" class="form-control" ></asp:TextBox> 
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlGender" runat="server">
                                       <asp:ListItem Value="-1"> --Select Gender--</asp:ListItem>
                                          <asp:ListItem Value="1">Male</asp:ListItem>
                                          <asp:ListItem Value="0"> Female</asp:ListItem>
                                    </asp:DropDownList>
                                    </div>
                                </div>
                            </div> 
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                   <%-- <asp:TextBox ID="txtCountry" runat="server" placeholder="Enter Country" class="form-control" aria-describedby="emailHelp"></asp:TextBox>
                                      --%>  <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                            </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                         <asp:DropDownList ID="ddlCity" runat="server">
                                            </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                             <div class="row mt-2"> 
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                     
                                </div>
                                  <div class="col-xs-12 col-sm-6 col-md-6">
                                      
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-1 col-md-1">
                                    <asp:Button ID="btnSubmit" runat="server" class="button" Text="Submit" />
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-2">
                                      <input type="button" class="button" id="btnCancel1" title="Close" value="Close" onclick="fancyClose()" />
                                  </div>
                                <div class="col-xs-12 col-sm-8 col-md-8">
                                    </div>
                            </div>
                              </telerik:RadAjaxPanel>
                            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
                                <AjaxSettings>
                                    <telerik:AjaxSetting AjaxControlID="RadComboBox1">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="RadComboBox2" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>

                                </AjaxSettings>
                            </telerik:RadAjaxManager>
                            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" /></td>
                         </tr>


                     </table>
                 </td>
             </tr>
         </table>

    </form>
      
</body>
</html>
