﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Tellal_Course_Add_Update.aspx.vb" Inherits="Tellal_Course_Add_Update" ValidateRequest="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link rel="stylesheet" type="text/css" href="../Scripts/jquery.ui.timepicker.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/jquery-ui-1.9.2.custom.css" />
    <link href="../Scripts/jquery.cleditor.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.8.3.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />

    <script src="../Scripts/jquery.cleditor.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.timepicker.js" type="text/javascript"></script>

    <script type="text/javascript">
       



        function openTrainers() {
            
            var cUrl = ""
          

            cUrl = "Tellal_Add_Course_Trainers.aspx?ID=CST";

            $.fancybox({
                'type': 'iframe',
                'width': '100%',
                'height': '100%',
                'hideOnOverlayClick': false,
                'hideOnContentClick': false,
                'overlayOpacity': 0.7,
                'enableEscapeButton': false,
                'href': cUrl,
                  
              });

           
        }


        function CallFromTrainerSelection(obj) {
            //alert(obj);
            NameandCode = obj.split('||');
            var trainers =document.getElementById('<%=h_TrainerId.ClientID%>').value +','+ NameandCode[0]
            document.getElementById('<%=txtTrainerIDs.ClientID%>').value = NameandCode[0];
            document.getElementById('<%=h_TrainerId.ClientID%>').value = trainers;

            //alert(trainers);
             __doPostBack('<%= txtTrainerIDs.ClientID%>', 'TextChanged');
         }


         

        $(document).ready(function () {

            $('#<%= txtTime.ClientID %>').timepicker(

    {
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });

    $('#<%= txtTimeTo.ClientID %>').timepicker(

{
    showPeriod: true,
    onHourShow: OnHourShowCallback,
    onMinuteShow: OnMinuteShowCallback
});
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }



});

function FnADD() {

     

        }

        function FnREMOVE(trId) {

             

}

function CancelCourse() {
    var cStatus = $("#<%=ddlStatus1.ClientID%>").val();

    if (cStatus == "C") {
        return confirm('Do you really want to change the status of this course to cancelled? Please confirm.');
    }

}
    </script>
    <style type="text/css">
        .scrollingCheckBoxList {
            border: 1px #808080 solid;
            margin: 10px 10px 10px 10px;
            height: 20px;
        }

        .scrollingControlContainer {
            overflow-x: hidden;
            overflow-y: scroll;
        }

        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
            width: 2em;
        }

        #ui-timepicker-div {
            background-color: #ffffff;
        }
    </style>
   <%-- <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_Trainers" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager--%>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Add PD Course
           
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:HiddenField ID="hdnSelected" runat="server" />
                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <div id="lblError" runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table id="tblCategory" width="100%">
                                <tr align="left">
                                    <td width="25%">
                                        <span class="field-label">Code<font color="maroon">*</font></span>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Code required" ForeColor=""
                                            SetFocusOnError="True" ValidationGroup="category"></asp:RequiredFieldValidator>


                                    </td>
                                   <td width="25%">
                                        <span class="field-label">Course Category<font color="maroon">*</font></span>
                                    </td>
                                    <td width="25%">
                                      <%--  <asp:DropDownList ID="ddlCategory" runat="server">
                                        </asp:DropDownList>--%>
                                        <div class="combobox-wraper">
                                            <telerik:RadComboBox RenderMode="Lightweight" ID="ddlCategory" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true"
                                                Width="100%"> 
                                            </telerik:RadComboBox>
                                         </div>
            


                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Title<font color="maroon">*</font></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="200" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="rftitle" runat="server" ControlToValidate="txtTitle"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Title required" ForeColor=""
                                            SetFocusOnError="True" ValidationGroup="category"></asp:RequiredFieldValidator>


                                    </td>

                                    <td width="25%">
                                        <span class="field-label">Type<font color="maroon">*</font></span>
                                    </td>
                                    <td width="25%">
                                        <asp:DropDownList ID="ddlCourseType" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Start Date<font color="maroon">*</font></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFrom" runat="server" autocomplete="off"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCal1" TargetControlID="txtFrom" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>

                                    <td>
                                        <span class="field-label">End Date<font color="maroon">*</font></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTodate" runat="server" autocomplete="off"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton1" TargetControlID="txtTodate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvEnd" runat="server" ControlToValidate="txtTodate"
                                            Display="Dynamic" ErrorMessage="To Date required" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Time From</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTime" runat="server" autocomplete="off"></asp:TextBox></td>
                                    <td>
                                        <span class="field-label">To </span></td>
                                    <td>
                                        <asp:TextBox ID="txtTimeTo" runat="server" autocomplete="off"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Max Capacity</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMAxCapacity" runat="server"></asp:TextBox>
                                    </td>
                                     <td>
                                        <span class="field-label">Department Dimension<font color="maroon">*</font></span>
                                         
                                    </td>
                                    <td>
                                         <asp:DropDownList ID="DDLDepartment_Dimension" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvddldepatDimen" runat="server" ControlToValidate="DDLDepartment_Dimension" ErrorMessage="Please choose Dept.Dimension" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="" InitialValue="0">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Online Apply Date</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOnlineDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton2" TargetControlID="txtOnlineDate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvOnline" runat="server" ControlToValidate="txtOnlineDate"
                                            Display="Dynamic" ErrorMessage="Apply Date required" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>

                                    <td>
                                        <span class="field-label">Close Date</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCloseDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton3" TargetControlID="txtCloseDate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvClose" runat="server" ControlToValidate="txtCloseDate"
                                            Display="Dynamic" ErrorMessage="Close required" ValidationGroup="AttGroup" CssClass="error"
                                            ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Value</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseValue" runat="server"></asp:TextBox>
                                    </td>

                                    <td>
                                        <span class="field-label">Value Comments</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtValueComments" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Details</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDetails" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td width="25%">
                                        <span class="field-label">Course External Category </span>
                                    </td>
                                    <td width="25%"> 
                                        <telerik:RadComboBox RenderMode="Lightweight" ID="ddlCourseExtCategory" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true"
                                                Width="100%"> 
                                            </telerik:RadComboBox> 
                                        <%--<div class="combobox-wraper">
                                            
                                         </div>--%>
                                       <%-- <asp:CheckBoxList ID="ddlCourseExtCategory" runat="server"> 
                                        </asp:CheckBoxList>--%>

                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Description</span>
                                    </td>
                                    <td colspan="3">



                                        <telerik:RadEditor ID="txtDesc" runat="server" EditModes="Design" ToolsFile="~/HelpDesk/xml/FullSetOfTools.xml">
                                        </telerik:RadEditor>

                                    </td>
                                </tr>


                                <tr align="left">
                                    <td>
                                        <span class="field-label">Trainers</span></td>
                                    <td>
                                        <div>
                                            <asp:CheckBoxList ID="chkTrainers" runat="server" EnableTheming="True"
                                                Font-Bold="False"
                                                Visible="False">
                                            </asp:CheckBoxList>
                                            <asp:TextBox ID="txtTrainerIDs" runat="server" OnTextChanged="txtTrainerIDs_TextChanged"  ></asp:TextBox>
                                            <asp:ImageButton ID="imgParticipants2" runat="server" ImageUrl="~/Images/cal.gif"
                                                 OnClientClick="openTrainers();return false;"></asp:ImageButton><%-- --%>
                                            <asp:GridView ID="gvTrainer" runat="server" AutoGenerateColumns="False"
                                                Width="100%" PageSize="5"
                                                EnableModelValidation="True" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>' Font-Bold="False"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Trainer Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTrainerName" runat="server" Text='<%# bind("FullName") %>'></asp:Label>
                                                        </ItemTemplate>                                                        
                                                        <ItemStyle Width="80%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Remove</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:CommandField DeleteText="Remove" ShowDeleteButton="True">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_new" />
                                            </asp:GridView>



                                            <asp:HiddenField ID="h_TrainerId" runat="server" />



                                            &nbsp;
                                        </div>

                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="4">
                                        <table id="tblRegion" width="100%">
                                            <tr id="trRegion1">
                                                <td>
                                                    <span class="field-label">Region</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlRegion1" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="field-label">Location</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLocation1" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus1" runat="server">
                                                        <asp:ListItem Text="ACTIVE" Value="A"></asp:ListItem>
                                                        <%--<asp:ListItem Text="SCHEDULE" Value="S"></asp:ListItem>
                                            <asp:ListItem Text="RE-SCHEDULED" Value="R"></asp:ListItem>
                                            <asp:ListItem Text="COMPLETED" Value="F"></asp:ListItem>--%>
                                                        <asp:ListItem Text="CANCELLED" Value="C"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <input type="button" id="btnAddMore" class="button" value="ADD MORE" onclick="FnADD()" style="display: none" />
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAddCourse" runat="server" Text="Add/Save" CssClass="button" ValidationGroup="category"
                                            Width="130px" />
                                        <asp:Button ID="btnUpdateCourse" runat="server" Text="Update/Save" CssClass="button"
                                            Width="130px" OnClientClick="CancelCourse();" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="False"
                                            Width="70px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
