﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_PD_Trainer_View.aspx.vb" Inherits="Tellal_PD_Trainer_View" %>

<%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameAddPDC").fancybox({
                type: 'iframe',
                fitToView: false,
                width: '85%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Trainers
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" width="100%">
                    <tr id="trLabelError">
                        <td align="left" class="matters" valign="bottom">
                            <div id="lblError" runat="server" class="error">
                            </div>
                        </td>
                    </tr>
                    <tr id="trAdd">
                        <td align="left">

                            <%--<asp:LinkButton ID="lbtnAdd" runat="server" CausesValidation="false" Text="Add New"
                                OnClick="lbtnAdd_Click" Visible="false"></asp:LinkButton>--%>
                            <a id="frameAdd" class="frameAddPDC" href="Tellal_PD_Trainer_Add.aspx?Id=0">ADD NEW</a>
                            

                        </td>
                    </tr>
                    <tr id="trGridv" runat="server">
                        <td align="center">

                            <asp:GridView ID="gvPDTrainsers" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="10" Width="100%" OnPageIndexChanging="gvPDTrainsers_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Full Name">
                                        <HeaderTemplate>
                                            <span class="field-label">Full Name
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtFullName" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchFullName" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchFullName_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFullName" runat="server" Text='<%# bind("FullName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="School Name">
                                        <HeaderTemplate>
                                            <span class="field-label">School Name
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtBSU_Name" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchBSU_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchBSU_Name_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSU_Name" runat="server" Text='<%# Bind("BSU_Name")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <HeaderTemplate>
                                            <span class="field-label">Email
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtEmail" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchEmail" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchEmail_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile">
                                        <HeaderTemplate>
                                            <span class="field-label">Mobile
                                            </span>
                                            <%--<br />
                                            <asp:TextBox ID="txtMobile" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchMobile" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchMobile_Click"></asp:ImageButton>--%>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMobile" runat="server" Text='<%# Bind("Mobile")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="lbtnEdit" CssClass="frameAddPDC" runat="server" CausesValidation="false" Text="Edit"
                                                OnClick="lbtnEdit_Click"></asp:LinkButton>--%>
                                            <asp:HyperLink ID="lnkEdit" class="frameAddPDC" runat="server" NavigateUrl="~/PDTellal/Tellal_PD_Trainer_Add.aspx">Edit</asp:HyperLink>
                                             <%--<a id="frameEdit" class="frameAddPDC" r>Edit</a>--%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

