﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_PDExportOnlineCourse.aspx.vb" Inherits="Tellal_PDExportOnlineCourse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
      <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" language="javascript">

        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)


            if (result == '' || result == undefined) {
                //            document.getElementById("txtDate").value=''; 
                return false;
            }
            if (mode == 1)
                $("#<%=txtFrom.ClientID%>").val(result);


        }

    </script>
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Export Online Course
        </div>
        <div class="card-body">
            <div class="table-responsive">
        <table border="0"  width="100%" style="border-style: none;
        border-width: 0px;" id="tblData" runat="server">
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom" colspan="2">
                <div id="lblNoAccessa" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table  id="tblCategory"  width="100%" runat="server">
                  
                      <tr align="left">
                        <td class="matters">
                              <span class="field-label" >  Course Date<font color="maroon">*</font></span>
                        </td>
                          <td>
                             <asp:TextBox ID="txtFrom" runat="server" Width="20%" autocomplete="off"></asp:TextBox>
                               <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtFrom" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(1);return false;" Visible="false" />
                            <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                Display="Dynamic" ErrorMessage=" Date required" ValidationGroup="AttGroup" CssClass="error"
                                ForeColor="">*</asp:RequiredFieldValidator>
                           
                          </td></tr>
                     <tr align="left">
                        <td class="matters" colspan="4">
                            <asp:Button ID="btnDownloadUserInfo" runat="server" Text="Download User Info" CssClass="button" />
                            <asp:Button ID="btnDownloadCourseInfo" runat="server" Text="DOWNLOAD Course Info" CssClass="button" />
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>
            </table>

    <table id="tblNoAccess" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" visible="false">
        <tr>
            <td align="center">
               
                   
                        <asp:Label id="lblNoAccess" runat="server"  EnableViewState="False"
                            Font-Size="10px"></asp:Label>
                  
                        
              
            </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
                </div></div></div>
</asp:Content>

