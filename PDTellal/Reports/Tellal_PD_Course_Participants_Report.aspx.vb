﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Tellal_PD_Course_Participants_Report
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.QueryString("msg") Is Nothing Then
                    lblError.InnerHtml = "<div class='alert alert-success'>Record updated successfully !!!</div>"
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("CM_ID") = 0

                    bindCourses()
                    '' bindCourseList()

                    tblGridData.Visible = False

                    'Dim IsTRainer As Boolean = Check_PD_Trainer()

                    'If Not IsTRainer Then
                    '    tblNoAccess.Visible = True
                    '    tblData.Visible = False
                    '    Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                    '    lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & Errormsg & "</div>"
                    'End If
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try
        Else
            'bindSelectedCourseList(ddlCourse.SelectedValue)
            'bindSelectedCourseList(hdnSelectedCourse.Value)
        End If


    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        bindParticipantsGridByEmpId(hdnSelected.Value)

    End Sub
    Protected Sub gvCourseList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCourseList.PageIndex = e.NewPageIndex
        bindSelectedCourseList(hdnSelectedCourse.Value)
    End Sub
    Protected Sub txtCourses_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtCourses.Text.Trim.Length >= 1 Then

                bindSelectedCourseList(hdnSelectedCourse.Value)
                txtCourses.Text = "You have selected courses"
                'txtCourses.Visible = False
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub txtParticipants_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtParticipants.Text.Trim.Length >= 1 Then

                bindParticipantsGridByEmpId(txtParticipants.Text)
                txtParticipants.Text = "You have selected participants"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgParticipants_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        
        For Each li As ListItem In chkCourses.Items
            li.Attributes.Add("someValue", li.Value)
        Next
    End Sub


    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim CourseId As String = String.Empty
         
        CourseId = hdnSelectedCourse.Value
        lnkDelete.OnClientClick = "javascript:ParticipantsList('" + CourseId + "');return false;"
    End Sub

    Private Sub bindParticipantsGridByEmpId(ByVal EmpIDs As String)

        Try


            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim FILTER_COND As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim CR_IDs As String = String.Empty
            Dim param(5) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable


            'If EmpIDs <> "" Then
            '    FILTER_COND = " AND CR.CR_ID IN (" & EmpIDs.Replace("||", "','") & ")"
            'End If

            CR_IDs = EmpIDs.Replace("||", "|")
            If CR_IDs.EndsWith("|") Then
                CR_IDs = CR_IDs.TrimEnd("|")
            End If

            Dim CourseIds As String = String.Empty
             
            CourseIds = Replace(hdnSelectedCourse.Value, "||", "|")
            If CourseIds.EndsWith("|") Then
                CourseIds = CourseIds.TrimEnd("|")
            End If




            If gvParticipants.Rows.Count > 0 Then

               
                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMPLOYEE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' OR replace(EMPLOYEE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                     
                    str_EMP_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BusinessUnit,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "

                    str_BSU_Name = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = EMP_Name + BSU_Name



            param(0) = New SqlClient.SqlParameter("@CM_IDs", CourseIds)

            param(1) = New SqlClient.SqlParameter("@CR_IDs", CR_IDs)
            param(2) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Participants_List_For_Course", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
                gvParticipants.Rows(0).Cells.Clear()
                gvParticipants.Rows(0).Cells.Add(New TableCell)
                gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
                tblGridData.Visible = False
            Else
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                tblGridData.Visible = True
                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")
                txtSearch.Text = str_EMP_Name
                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")
                txtSearch.Text = str_BSU_Name
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    gvParticipants.PageIndex = e.NewPageIndex
    '    bindParticipantsGrid()
    '    For Each li As ListItem In chkCourses.Items
    '        li.Attributes.Add("someValue", li.Value)
    '    Next
    'End Sub

    Protected Sub lbtnRemoveCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCMID As New Label
        lblCMID = TryCast(sender.FindControl("lblCM_ID"), Label)
        Dim strCMId As String = hdnSelectedCourse.Value
        If lblCMID.Text <> "" Then
            Dim output As String = strCMId.Replace(lblCMID.Text, String.Empty)
            hdnSelectedCourse.Value = output.ToString
            bindSelectedCourseList(output.ToString())
        End If


        If hdnSelectedCourse.Value = "" Then
            txtCourses.Text = ""
        End If
    End Sub

    Sub bindSelectedCourseList(ByVal CM_IDs As String)


        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim CM_IDfilter As String = String.Empty
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        Try

            param(0) = New SqlClient.SqlParameter("@CM_ID", CM_IDs)


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Course_Info_By_Ids", param)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
                Dim columnCount As Integer = gvCourseList.Rows(0).Cells.Count
                gvCourseList.Rows(0).Cells.Clear()
                gvCourseList.Rows(0).Cells.Add(New TableCell)
                gvCourseList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCourseList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCourseList.Rows(0).Cells(0).Text = "Currently there is no courses Exists"
            Else
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
            End If


        Catch ex As Exception

        End Try

    End Sub
    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click

        ParticipantsExcelDownload()

    End Sub
      

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGridByEmpId(hdnSelected.Value)
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGridByEmpId(hdnSelected.Value)
        For Each li As ListItem In chkCourses.Items
            li.Attributes.Add("someValue", li.Value)
        Next
    End Sub

        
    Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipants.PageIndex = e.NewPageIndex
        bindParticipantsGridByEmpId(hdnSelected.Value)
       
    End Sub

    Sub bindCourses()
        ddlCourse.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        Try
            param(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
          
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Calendar_Events_CAL_VIEW", param)

            If ds IsNot Nothing Then
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddlCourse.DataSource = ds
                        ddlCourse.DataTextField = "CM_TITLE"
                        ddlCourse.DataValueField = "CM_ID"
                        ddlCourse.DataBind()
                    End If

                End If
            End If
            ddlCourse.Items.Insert(-1, "")

        Catch ex As Exception

        End Try

    End Sub


     

    Private Sub ParticipantsExcelDownload()

        Try


            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim CR_IDs As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(5) As SqlClient.SqlParameter
            Dim ds As DataSet

            CR_IDs = hdnSelected.Value.Replace("||", "|")
            If CR_IDs.EndsWith("|") Then
                CR_IDs = CR_IDs.TrimEnd("|")
            End If

            Dim CourseIds As String = String.Empty

            CourseIds = Replace(hdnSelectedCourse.Value, "||", "|")
            If CourseIds.EndsWith("|") Then
                CourseIds = CourseIds.TrimEnd("|")
            End If




            If gvParticipants.Rows.Count > 0 Then


                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMPLOYEE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' OR replace(EMPLOYEE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"

                    str_EMP_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BusinessUnit,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "

                    str_BSU_Name = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = EMP_Name + BSU_Name



            param(0) = New SqlClient.SqlParameter("@CM_IDs", CourseIds)

            param(1) = New SqlClient.SqlParameter("@CR_IDs", CR_IDs)
            param(2) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Participants_List_For_Course", param)



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else
                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)
                dtEXCEL.Columns.Remove("SR_NO")
                dtEXCEL.Columns.Remove("CR_CM_ID")
                dtEXCEL.Columns.Remove("CR_ID")
                dtEXCEL.Columns.Remove("SELECTED")
                dtEXCEL.Columns.Remove("STATUS")
                'dtEXCEL.Columns("CM_TITLE").ColumnName = "TITLE"
                'dtEXCEL.Columns("EVENT_DATE").ColumnName = "EVENT DATE"

                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")

                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")

                Dim strFilename As String = dtEXCEL.Rows(0)("CM_TITLE").ToString()
                strFilename = strFilename.Replace("/", "").Replace("\", "").Replace(":", "").Replace(";", "").Replace(",", "").Replace("?", "")
                ''  strFilename = RemoveSpecialCharacters(strFilename)
                Dim stuFilename As String = "Participants_For_" & strFilename & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                
                'ws.Columns.Remove(2)
                'ws.Columns.Remove(3)
                'ws.Columns.Remove(4)
                'TITLE	EVENT DATE	LOCATION_REGION	BusinessUnit	EMPLOYEE	EMAIL	REQUEST_DATE	STATUS	SELECTED	Course_Category	PHONE	Attendance	EVAL SUBMITTED

                ws.Cells(0, 0).Value = "Title"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("A").Width = 10000

                ws.Cells(0, 1).Value = "EVENT DATE"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("B").Width = 4000
                ws.Columns("B").Style.NumberFormat = "dd-MMM-yyyy"


                ws.Cells(0, 2).Value = "LOCATION/REGION"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("C").Width = 5000

                ws.Cells(0, 3).Value = "BUSINESS UNIT"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("D").Width = 7000

                ws.Cells(0, 4).Value = "EMPLOYEE NAME"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("E").Width = 7000

                ws.Cells(0, 5).Value = "EMAIL"
                ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 6).Value = "REGISTRATION DATE"
                ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("G").Width = 7000
                ws.Columns("G").Style.NumberFormat = "dd-MMM-yyyy"

                ws.Cells(0, 7).Value = "COURSE CATEGORY"
                ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("H").Width = 7000

                ws.Cells(0, 8).Value = "PHONE"
                ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("I").Width = 7000

                ws.Cells(0, 9).Value = "ATTENDANCE"
                ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("J").Width = 7000

                ws.Cells(0, 10).Value = "EVAL SUBMITTED"
                ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("K").Width = 7000

                ''included by nahyan on 30sep2019
                ws.Cells(0, 11).Value = "Course Value"
                ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("L").Width = 7000
                ws.Columns("L").Style.NumberFormat = "@"
                ''included by nahyan on 31Oct2018
                ws.Cells(0, 12).Value = "COURSE DATE"
                ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("M").Width = 7000

                ''included by nahyan on 31Oct2018
                ws.Cells(0, 13).Value = "COURSE ENDDATE"
                ws.Cells(0, 13).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 13).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Columns("M").Width = 7000

                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End() 
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lbtn_Excel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
         
            ParticipantsExcelDownload()

    End Sub

    Public Shared Function RemoveSpecialCharacters(str As String) As String
        Dim sb As New StringBuilder()
        For Each c As Char In str
            If (c >= "0"c AndAlso c <= "9"c) OrElse (c >= "A"c AndAlso c <= "Z"c) OrElse (c >= "a"c AndAlso c <= "z"c) Then
                sb.Append(c)
            End If
        Next
        Return sb.ToString()
    End Function
    
    
End Class
