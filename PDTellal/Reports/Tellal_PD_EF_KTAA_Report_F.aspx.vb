﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_EF_KTAA_Report_F
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("cm_Id") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("cm_Id").ToString
                ViewState("MainID") = MainID
                bindParticipantsGrid()
            End If
        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipants.PageIndex = e.NewPageIndex
        bindParticipantsGrid()
    End Sub

    Private Sub bindParticipantsGrid()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim param(2) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        param(0) = New SqlClient.SqlParameter("@CM_IDs", ViewState("MainID"))
        'param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))

        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Participants_List_By_Course", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
                gvParticipants.Rows(0).Cells.Clear()
                gvParticipants.Rows(0).Cells.Add(New TableCell)
                gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
            Else
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Emp_IDs As String = String.Empty

        For Each gvrow As GridViewRow In gvParticipants.Rows
            Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
            If chk IsNot Nothing And chk.Checked Then
                Emp_IDs += gvParticipants.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

            End If
        Next

        ''checking if seat is available

        Emp_IDs = Emp_IDs.Trim(",".ToCharArray())
        hdnSelected.Value = Emp_IDs
        Session("liEmpList") = Emp_IDs

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('hdnSelected').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("parent.$.fancybox.close();")
        Response.Write("} </script>")
     
   
    End Sub
End Class
