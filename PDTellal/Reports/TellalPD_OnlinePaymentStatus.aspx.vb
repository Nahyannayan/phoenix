﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
'Imports NPOI.Util

Partial Class TellalPD_OnlinePaymentStatus
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.QueryString("msg") Is Nothing Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        'Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("CM_ID") = 0

                    Dim IsCorp_User As Boolean = Check_Corp_User()
                    Dim IsPDC = Check_PD_Coordinator()
                    Dim IsTRainer As Boolean = Check_PD_Trainer()


                    'If Not IsTRainer AndAlso Not IsPDC AndAlso Not IsCorp_User Then
                    '    tblNoAccess.Visible = True
                    '    tblData.Visible = False
                    '    Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                    '    lblNoAccess.Text = Errormsg
                    'End If
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try
        Else
            ' bindParticipantsGrid()
        End If

    End Sub
     
    Function Check_Corp_User() As Boolean
        Dim IsCorp_User As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_CORP_ACCESS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsCorp_User = True
            End If

        End If
        Return IsCorp_User
    End Function
    Protected Sub gvParticipants_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Dim lnkKT As HyperLink = DirectCast(e.Row.FindControl("lnkKT"), HyperLink)

            Dim lblID As Label = DirectCast(e.Row.FindControl("lblCRid"), Label)
            Dim CR_ID As String = lblID.Text
           
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        bindParticipantsGrid()

    End Sub

    Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipants.PageIndex = e.NewPageIndex
        bindParticipantsGrid()

    End Sub
     

    Protected Sub btnSearchTitle_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
        'bindParticipantsGrid()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
    End Sub
     

    Private Sub bindParticipantsGrid()

        Try


            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim str_Title As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim CR_IDs As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim TITLE As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable




            If gvParticipants.Rows.Count > 0 Then
                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BusinessUnit,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_BSU_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtTitle")

                If txtSearch.Text.Trim <> "" Then

                    TITLE = " AND replace(CM_Title,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_Title = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMPLOYEE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_EMP_Name = txtSearch.Text.Trim
                End If
            End If
            FILTER_COND = TITLE + EMP_Name + BSU_Name

            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)
            param(0) = New SqlClient.SqlParameter("@FROMDATE", txtFrom.Text)
            param(2) = New SqlClient.SqlParameter("@TODATE", txtTo.Text)
            param(3) = New SqlClient.SqlParameter("@PStatus", radStatus.SelectedValue)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_PaymentStatus", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
                gvParticipants.Rows(0).Cells.Clear()
                gvParticipants.Rows(0).Cells.Add(New TableCell)
                gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"

            Else
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()

                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")
                txtSearch.Text = str_EMP_Name
                txtSearch = gvParticipants.HeaderRow.FindControl("txtTitle")
                txtSearch.Text = str_Title
                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")
                txtSearch.Text = str_BSU_Name
            End If
        Catch ex As Exception

        End Try
    End Sub

    Function Check_PD_Coordinator() As Boolean
        Dim IsPDC As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@User_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PDC_By_UserID", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsPDC = True
            End If

        End If
        Return IsPDC
    End Function
    Protected Sub btnDownloadExcel_Click(sender As Object, e As EventArgs)
        DownloadCourseInfoExcel()
    End Sub
    Private Sub DownloadCourseInfoExcel()

        Try


            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
             
            Dim str_Title As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim CR_IDs As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim TITLE As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet


            If gvParticipants.Rows.Count > 0 Then
                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BusinessUnit,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_BSU_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtTitle")

                If txtSearch.Text.Trim <> "" Then

                    TITLE = " AND replace(CM_Title,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_Title = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMPLOYEE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_EMP_Name = txtSearch.Text.Trim
                End If
            End If
            FILTER_COND = TITLE + EMP_Name + BSU_Name

            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)
            param(0) = New SqlClient.SqlParameter("@FROMDATE", txtFrom.Text)
            param(2) = New SqlClient.SqlParameter("@TODATE", txtTo.Text)
            param(3) = New SqlClient.SqlParameter("@PStatus", radStatus.SelectedValue)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_PaymentStatus", param)



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else
                ''UtilityObj.Errorlog(ds.Tables(0).Rows.Count, System.Reflection.MethodBase.GetCurrentMethod().Name)

                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)
                dtEXCEL = ds.Tables(0)
                dtEXCEL.Columns.Remove("CR_ID")
                dtEXCEL.Columns.Remove("CM_ID")

                dtEXCEL.Columns.Remove("CM_EVENT_DT")

                dtEXCEL.Columns("cm_title").ColumnName = "Course Title"
                dtEXCEL.Columns("BusinessUnit").ColumnName = "School Name"
                dtEXCEL.Columns("PaymentStatus").ColumnName = "Payment Status"
                dtEXCEL.Columns("CR_APPR_STATUS").ColumnName = "Course Status"
                dtEXCEL.Columns("Departmental_Dimensions").ColumnName = "Departmental Dimensions"
                dtEXCEL.Columns("PaymentMode").ColumnName = "Payment Mode"
                dtEXCEL.Columns("CM_EVENT_DT1").ColumnName = "Course Date"
                '	EMPLOYEE	Email	PaymentStatus	CR_APPR_STATUS	Departmental_Dimensions	PaymentMode	CM_EVENT_DT1



                Dim currDate As DateTime = Date.Now
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                Dim excelfiledate1 As String = currDate.ToShortDateString().ToString().Replace("/", "_").Replace(" ", "").Replace(":", "")
                 
                Dim strFilename As String = "Payment_Status_Report"

                Dim stuFilename As String = strFilename & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                 

                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub
    Function Check_PD_Trainer() As Boolean
        Dim IsTrainer As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_TRAINER_BY_BSU_EMP", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsTrainer = True
            End If

        End If
        Return IsTrainer
    End Function
End Class
