﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" EnableEventValidation="false"   CodeFile="TellalPD_OnlinePaymentStatus.aspx.vb" Inherits="TellalPD_OnlinePaymentStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .GridPager a, .GridPager span
        {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }
        .GridPager a
        {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }
        .GridPager span
        {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
        .scroll_checkboxes
        {
            height: 120px;
            width: 350px;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }
    </style>
    <script src="../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../Scripts/jquery-1.4.3.min.js"></script>
   
     <script type="text/javascript" src="../../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script type="text/javascript">
 
        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)
            if (mode == 2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTo.ClientID %>').value, "", sFeatures)

            if (result == '' || result == undefined) {
                //            document.getElementById("txtDate").value=''; 
                return false;
            }
            if (mode == 1)

                $("#<%=txtFrom.ClientID%>").val(result);
            if (mode == 2)

                $("#<%=txtTo.ClientID%>").val(result);

        }
    </script>
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Payment Status Report
        </div>
        <div class="card-body">
            <div class="table-responsive">
     
    <table border="0"  width="100%" style="border-style: none;
        border-width: 0px;" id="tblData" runat="server">
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom" colspan="2">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table  id="tblCategory"  width="100%">
                  
                      
                      <tr align="left">
                        <td class="matters">
                             <span class="field-label" > Course From<font color="maroon">*</font></span>
                        </td>
                          <td>
                             <asp:TextBox ID="txtFrom" runat="server" Width="110px" autocomplete="off"></asp:TextBox>
                                 <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtFrom" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(1);return false;" Visible="false" />
                            <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                Display="Dynamic" ErrorMessage=" Date required" ValidationGroup="AttGroup" CssClass="error"
                                ForeColor="">*</asp:RequiredFieldValidator></td><td>
                               <span class="field-label" >  <span class="matters"> To</span> </span>
                                <asp:TextBox ID="txtTo" runat="server" Width="110px" autocomplete="off"></asp:TextBox>
                                 <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtTo" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgCalendar2" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(2);return false;" Visible="false" />
                            <asp:RequiredFieldValidator ID="rfvCalendar2" runat="server" ControlToValidate="txtTo"
                                Display="Dynamic" ErrorMessage="To Date required" ValidationGroup="AttGroup" CssClass="error"
                                ForeColor="">*</asp:RequiredFieldValidator>
                          </td></tr>
                    <tr align="left">
                        <td class="matters">
                           <span class="field-label" >   Status<font color="maroon">*</font></span>
                        </td>
                        <td colspan="3" align="left">                           
                            <asp:RadioButtonList ID="radStatus" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Successfull" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Unsuccessfull" Value="2"></asp:ListItem>
                                <asp:ListItem Text="All" Value="3" Selected="True" ></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                  
                    <tr align="left">
                        <td class="matters" colspan="4">                          
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" /> 
                            <asp:Button ID="btnDownloadExcel" runat="server" Text="Download Excel" CssClass="button" OnClick="btnDownloadExcel_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <table width="100%" >
                    <tr>
                        <td class="matters">
                            <asp:GridView ID="gvParticipants" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="100%"
                                DataKeyNames="CR_ID">
                              
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CR_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCRid" runat="server" Text='<%# Bind("CR_ID") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">
                                                            Business Unit
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtBSU_Name" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchBSU_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchBSU_NAME_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("BusinessUnit") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Title">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">
                                                            Course Title
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtTitle" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchTitle" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchTitle_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCName" runat="server" Text='<%# Bind("CM_TITLE") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="800px"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Event Date">
                                        <ItemTemplate>
                                           <asp:Label ID="lblCM_EVENT_DT" runat="server" Text='<%# Bind("CM_EVENT_DT1")%>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">
                                                            Name
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtEMP_Name" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchEMP_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchEMP_Name_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMPLOYEE") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Course Value">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCV" runat="server" Text='<%# Bind("[Caourse Value]")%>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Departmental Dimensions ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Departmental_Dimensions") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Payment Mode">
                                        <ItemTemplate>
                                           <asp:Label ID="lblPaymentMode" runat="server" Text='<%# Bind("PaymentMode")%>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Payment Status">
                                        <ItemTemplate>
                                           <asp:Label ID="lblPaymentStatus" runat="server" Text='<%# Bind("PaymentStatus")%>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Payment Date">
                                        <ItemTemplate>
                                           <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Bind("PAYMENTDATE")%>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Card Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblct" runat="server" Text='<%# Bind("[Card Type]")%>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

      <table id="tblNoAccess" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" visible="false">
        <tr>
            <td align="center">
               
                   
                        <asp:Label id="lblNoAccess" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label>
                  
                        
              
            </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table> </div></div></div>
</asp:Content>
