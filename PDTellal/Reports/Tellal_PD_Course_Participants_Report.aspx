﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Tellal_PD_Course_Participants_Report.aspx.vb" Inherits="Tellal_PD_Course_Participants_Report" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }

        .scroll_checkboxes {
            height: 120px;
            width: 350px;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }
    </style>
    <script type="text/javascript">
         function ChangeList(refValue) {
            $("#<%= chkCourses.ClientID%> input[type='checkbox']").each(function () {
                $(this).attr('checked', refValue.checked);
            });
        }
        function openCourseFancyBox() { 
             var fromdate = $("#<%=txtFrom.ClientID%>").val();
            var Todate = $("#<%=txtTo.ClientID%>").val();

            $.fancybox({
                type: 'iframe',
                maxWidth: 700,
                href: "Tellal_PD_Course_Selection.aspx?empId=0&fromdate=" + fromdate + "&ToDate=" + Todate,
                maxHeight: 600,
                fitToView: false,
                width: '100%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                topRatio: 0,
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '20px', 'bottom': 'auto' });
                }
            });
            return false;
        }


        function CallFromCourseSelection(obj) { 
             
            NameandCode = obj.split('||'); 
                document.getElementById('<%=txtCourses.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=hdnSelectedCourse.ClientID%>').value = NameandCode[0];
                __doPostBack('<%= txtCourses.ClientID%>', 'TextChanged'); 
        }

         function CallFromParticipentSelection(obj) {              
                NameandCode = obj.split('||');
             
                document.getElementById('<%=txtParticipants.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=hdnSelected.ClientID%>').value = NameandCode[0];
                __doPostBack('<%= txtCourses.ClientID%>', 'TextChanged');
        }


        function openParticipants() {
            var url = "";
              var cm_Id = $("#<%=ddlCourse.ClientID%>").val();
            var selCMId = $("#<%=hdnSelectedCourse.ClientID %>").val();
            selCMId = selCMId.replace(/,/g, '|');
             
            //url = "PD_Course_Participant_Report_F.aspx?cm_Id=" + selCMId
            //var oWnd = radopen(url, "pop_participant");



            $.fancybox({
                type: 'iframe',
                maxWidth: 700,
                href: "Tellal_PD_Course_Participant_Report_F.aspx?cm_Id="+selCMId,
                maxHeight: 600,
                fitToView: false,
                width: '100%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                topRatio: 0,
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '20px', 'bottom': 'auto' });
                }
            });
            return false;

        }
     </script>
      <%--  <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_course" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
             <telerik:RadWindow ID="pop_participant" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>--%>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Participants Report
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table border="0" cellpadding="2" cellspacing="2" width="100%" style="border-style: none; border-width: 0px;"
                    id="tblData" runat="server">
                    <tr id="trLabelError">
                        <td align="left" class="matters" valign="bottom" colspan="2">
                            <div id="lblError" runat="server" class="error">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table id="tblCategory" width="100%">

                                <tr align="left">
                                    <td>
                                        <span class="field-label">Course From<font color="maroon">*</font></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFrom" runat="server" AutoComplete="off"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal1" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCal1" TargetControlID="txtFrom" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage=" Date required" ValidationGroup="AttGroup" CssClass="error"
                                            ForeColor="">*</asp:RequiredFieldValidator></td>
                                    <td>
                                        <span class="field-label">To</span>
                                        <asp:TextBox ID="txtTo" runat="server" AutoComplete="off"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton1" TargetControlID="txtTo" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvCalendar2" runat="server" ControlToValidate="txtTo"
                                            Display="Dynamic" ErrorMessage="To Date required" ValidationGroup="AttGroup" CssClass="error"
                                            ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                                <tr align="left">
                                    <td class="matters">
                                        <span class="field-label">Course<font color="maroon">*</font></span>
                                    </td>
                                    <td colspan="3" align="left">
                                        <asp:DropDownList ID="ddlCourse" runat="server" AutoPostBack="true" Visible="False">
                                        </asp:DropDownList>
                                        <div class="scroll_checkboxes" style="display: none;">
                                            <asp:CheckBox ID="chkAll" runat="server" Text="ALL" onclick="ChangeList(this)" />
                                            <asp:CheckBoxList ID="chkCourses" runat="server" EnableTheming="True" Font-Bold="False"
                                                Font-Names="Times New Roman" Font-Size="10pt">
                                            </asp:CheckBoxList>
                                        </div>
                                        <asp:TextBox ID="txtCourses" runat="server" Width="330px" AutoPostBack="True"
                                            OnTextChanged="txtCourses_TextChanged"></asp:TextBox>&nbsp;
                                            <asp:ImageButton ID="imgCourses" runat="server" ImageUrl="~/Images/cal.gif"
                                                OnClientClick="openCourseFancyBox();return false;"></asp:ImageButton>
                                     <%--   <asp:ImageButton ID="imgCourseList" runat="server" ImageUrl="~/Images/cal.gif"
                                            CssClass="frameAddCourse" Visible="false"></asp:ImageButton>--%>


                                        <br />
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvCourseList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        HeaderStyle-Height="30" PageSize="20" Width="100%" DataKeyNames="CM_ID" EnableModelValidation="True" OnPageIndexChanging="gvCourseList_PageIndexChanging">

                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select">
                                                                <HeaderTemplate>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCM_ID" runat="server" Text='<%# Bind("CM_ID") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Title">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("CM_TITLE") %>' __designer:wfdid="w40"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEvntDate" runat="server" Text='<%# Bind("CM_EVENT_DT") %>' __designer:wfdid="w40"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemTemplate> 
                                                                    <asp:LinkButton ID="lbtnRemoveCourse" runat="server" Text="Remove"  OnClick="lbtnRemoveCourse_Click"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="matters">
                                        <span class="field-label">Participants</span>
                                    </td>
                                    <td colspan="3" align="left">
                                       <asp:TextBox ID="txtParticipants" runat="server" Width="330px" AutoPostBack="True"
                                            OnTextChanged="txtParticipants_TextChanged"></asp:TextBox>&nbsp;
                                       <asp:ImageButton ID="imgParticipants" runat="server" ImageUrl="~/Images/cal.gif"
                                        OnClick="imgParticipants_Click" OnClientClick="getParticipants()" Visible="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgParticipants2" runat="server" ImageUrl="~/Images/cal.gif"
                                           OnClientClick="openParticipants();return false;"></asp:ImageButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Visible="false">
                                            <img src="~/Images/cal.gif" alt="Participants" /></asp:LinkButton>
                                        <asp:HiddenField ID="hdnSelected" runat="server" />
                                        <asp:HiddenField ID="hfTerms" runat="server" Value="0" />
                                        <asp:HiddenField ID="hdnSelectedCourse" runat="server" />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="matters" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="button" Width="130px" />
                                        <asp:Button ID="btnExcel" runat="server" Text="DOWNLOAD EXCEL" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr id="trGridv" runat="server">
                        <td align="center" colspan="4">
                            <table id="tblGridData" runat="server" width="100%">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lbtn_Excel" runat="server" Text="Download Excel" Visible="false" OnClick="lbtn_Excel_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters">
                                        <asp:GridView ID="gvParticipants" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging="gvParticipants_PageIndexChanging"
                                            DataKeyNames="CR_ID">

                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="CR_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCRid" runat="server" Text='<%# Bind("CR_ID") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Business Unit">
                                                    <HeaderTemplate>
                                                        <table style="width: 100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="2" class="gridheader_text" align="center">Business Unit
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="gridheader_text" align="left">
                                                                        <asp:TextBox ID="txtBSU_Name" runat="server" Width="160px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 5px" valign="middle">
                                                                        <asp:ImageButton ID="btnSearchBSU_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                            ImageAlign="Top" OnClick="btnSearchBSU_NAME_Click"></asp:ImageButton>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("BusinessUnit") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Title">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCName" runat="server" Text='<%# Bind("CM_TITLE") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="800px"></ItemStyle>
                                                </asp:TemplateField>
                                                 
                                                <asp:TemplateField HeaderText="Name">
                                                    <HeaderTemplate>
                                                        <table style="width: 100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="2" class="gridheader_text" align="center">Name
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="gridheader_text" align="left">
                                                                        <asp:TextBox ID="txtEMP_Name" runat="server" Width="160px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 5px" valign="middle">
                                                                        <asp:ImageButton ID="btnSearchEMP_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                            ImageAlign="Top" OnClick="btnSearchEMP_Name_Click"></asp:ImageButton>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMPLOYEE") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Phone">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("PHONE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Attendance">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAttendance" runat="server" Text='<%# Bind("Attendance") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="400px"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Evaluation Submitted">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAttendance" runat="server" Text='<%# Bind("[EVAL SUBMITTED]") %>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="400px"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>

                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

      <script>
          $(document).ready(function () {

              //setTimeout(function () { $('[data-toggle="popover"]').popover(); }, 4000);


               

          });
    </script>

    
</asp:Content>
