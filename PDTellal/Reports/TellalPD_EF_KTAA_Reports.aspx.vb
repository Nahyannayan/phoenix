﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
'Imports NPOI.Util

Partial Class TellalPD_EF_KTAA_Reports
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            'Me.btnGraphs.Attributes.Add("onclick", "return confirm('If multiple course(s) are selected, graph for each selected course will open in a new window. Are you sure you want to continue?');")
            'Me.btnGraphs.Attributes.Add("onclick", "return confirm('Graph for each selected course will open in a new window. Are you sure you want to continue?');")

            Try

                If hdnSelectedCourse.Value <> "" Then
                    imgParticipants2.PostBackUrl = "PD_EF_KTAA_Report_F.aspx?cm_Id=2"
                End If
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.QueryString("msg") Is Nothing Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Record saved successfully !!!</div>"
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("CM_ID") = 0



                    tblGridData.Visible = False
                    Dim IsCorp_User As Boolean = Check_Corp_User()
                    Dim IsPDC = Check_PD_Coordinator()
                    Dim IsTRainer As Boolean = Check_PD_Trainer()


                    If Not IsTRainer AndAlso Not IsPDC AndAlso Not IsCorp_User Then
                        tblNoAccess.Visible = True
                        tblData.Visible = False
                        Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                        lblNoAccess.Text = Errormsg
                    Else
                        If Not IsCorp_User Then
                            btnGraphs.Visible = False
                        End If
                    End If
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try
        Else
            bindSelectedCourseList(hdnSelectedCourse.Value)
        End If

    End Sub

    Protected Sub gvCourseList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCourseList.PageIndex = e.NewPageIndex
        bindSelectedCourseList(hdnSelectedCourse.Value)
    End Sub
    Function Check_Corp_User() As Boolean
        Dim IsCorp_User As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_CORP_ACCESS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsCorp_User = True
            End If

        End If
        Return IsCorp_User
    End Function
    Protected Sub gvParticipants_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Dim lnkKT As HyperLink = DirectCast(e.Row.FindControl("lnkKT"), HyperLink)

            Dim lblID As Label = DirectCast(e.Row.FindControl("lblCRid"), Label)
            Dim CR_ID As String = lblID.Text
            'lnkKT.NavigateUrl = "~/PD/PD_KTAA_VIEW.aspx?ID=" & CR_ID

            If ddlType.SelectedValue = "1" Then
                'e.Row.Cells(8).Visible = False
                'gvParticipants.HeaderRow.Cells(8).Visible = False
                'e.Row.Cells(7).Visible = False
                'gvParticipants.HeaderRow.Cells(7).Visible = False
            End If

            'If ddlType.SelectedValue = "2" Then
            '    e.Row.Cells(7).Visible = False
            '    gvParticipants.HeaderRow.Cells(7).Visible = False

            'End If

        End If
    End Sub

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

    '    bindParticipantsGrid()

    'End Sub

    Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipants.PageIndex = e.NewPageIndex
        bindParticipantsGridByEmpId(hdnSelected.Value)

    End Sub
    Protected Sub txtCourses_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtCourses.Text.Trim.Length >= 1 Then

                bindSelectedCourseList(hdnSelectedCourse.Value)
                txtCourses.Text = "You have selected courses"
                'txtCourses.Visible = False
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lbtnRemoveCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCMID As New Label
        lblCMID = TryCast(sender.FindControl("lblCM_ID"), Label)
        Dim strCMId As String = hdnSelectedCourse.Value

        If lblCMID.Text <> "" Then
            Dim output As String = strCMId.Replace(lblCMID.Text, String.Empty)

            hdnSelectedCourse.Value = output.ToString
            bindSelectedCourseList(output.ToString())
        End If


        If hdnSelectedCourse.Value = "" Then
            txtCourses.Text = ""
        End If
    End Sub
    Protected Sub imgParticipants_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub



    Protected Sub btnSearchTitle_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGridByEmpId(hdnSelected.Value)
        'bindParticipantsGrid()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGridByEmpId(hdnSelected.Value)
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGridByEmpId(hdnSelected.Value)
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "1" Then
            btnExcel.Visible = True
            'btnSearch.Visible = False
        Else
            btnExcel.Visible = False
            'btnSearch.Visible = True

        End If
    End Sub

    Protected Sub txtParticipants_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtParticipants.Text.Trim.Length >= 1 Then
                Dim ss As String = hdnSelectedCourse.Value
                bindParticipantsGridByEmpId(txtParticipants.Text)
                txtParticipants.Text = "You have selected participants"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim dsExcel As DataSet
            

            Dim PARAM(3) As SqlParameter
            Dim selCourse As String = String.Empty
            selCourse = hdnSelectedCourse.Value.Replace("||", "|").TrimEnd("|").TrimStart("|")
             
            ViewState("CM_ID") = selCourse

            PARAM(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))
            PARAM(1) = New SqlParameter("@EMP_ID", Session("EmployeeId"))

            'Hashmi temp code only for debugging
            ''PARAM(1).Value = 9235
            'End Hashmi temp code only for debugging

            dsExcel = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.EF_PD_STAT", PARAM)
             
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile
            'TypicalTableSample(ef.Worksheets.Add("EF"), dsExcel)
            Dim courseType As String = GetCourseTypeID()
            If courseType = "1" Then
                CreateEFExcel(ef.Worksheets.Add("EF"), dsExcel)
            Else
                CreateEFExcelOnlineCourse(ef.Worksheets.Add("EF"), dsExcel)

            End If

          

            Dim fileName As String = "PD_EvaluationForm.xlsx"
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()

            Dim pathSave As String = Session("sUsr_id") & "\" & fileName
            If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                ' Create the directory.
                Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
            End If
            ef.Worksheets(0).HeadersFooters.AlignWithMargins = True
            ef.Save(cvVirtualPath & pathSave)
            Dim path = cvVirtualPath & pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnExcel_Click")
        End Try


    End Sub
    Private Function GetCourseTypeID() As String
        'ByVal SurvId As String
        'SURVEYID= " & SurvId & " AND 
        Dim connection As String = ConnectionManger.GetTellalPDConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@CM_IDs", ViewState("CM_ID"))
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "dbo.GET_COURSE_BY_IDs", param)
        If dsSurveyInfo.Tables(0).Rows.Count >= 1 Then
            Return dsSurveyInfo.Tables(0).Rows(0)("cm_course_type_id").ToString()
        Else
            Return ""
        End If

    End Function
    Protected Sub btnExcel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel2.Click
        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim dsExcel As DataSet

            ViewState("CM_ID") = hdnSelectedCourse.Value
             

            Dim selCourse As String = String.Empty
            selCourse = hdnSelectedCourse.Value.Replace("||", "|").TrimEnd("|").TrimStart("|")
            Me.hdnSelectedCourse.Value = selCourse
            ViewState("CM_ID") = selCourse

            bindSelectedCourseList(hdnSelectedCourse.Value)


            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))
            PARAM(1) = New SqlParameter("@ReportType", 1)
            dsExcel = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_ConsolidatedReport", PARAM)

            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            TypicalTableSample2(ef.Worksheets.Add("EF"), dsExcel, Me.GetPD_TITLE(ViewState("CM_ID")))
            Dim fileName As String = "PD_EvaluationFormConsol.xlsx"
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()


            Dim pathSave As String = Session("sUsr_id") & "\" & fileName
            If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                ' Create the directory.
                Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
            End If

            ef.Save(cvVirtualPath & pathSave)
            Dim path = cvVirtualPath & pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()

        Catch ex As Exception
            Dim exs As String = ex.Message
        End Try

    End Sub

    Private Sub CreateEFExcel(ByVal ws As ExcelWorksheet, ByVal dsExcel As DataSet)

        Dim dtEXCEL As New DataTable()

        Dim headerStyle As CellStyle = New CellStyle
        Dim colHeader As CellStyle = New CellStyle
        Dim CellStyle As CellStyle = New CellStyle

        Try
            Dim colTot As Integer = dsExcel.Tables(0).Columns.Count

            colHeader.HorizontalAlignment = HorizontalAlignmentStyle.Center
            colHeader.VerticalAlignment = VerticalAlignmentStyle.Center
            colHeader.Font.Weight = ExcelFont.BoldWeight

            headerStyle.Font.Size = 20 * 20
            headerStyle.Font.Name = "Verdana"
            headerStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
            headerStyle.VerticalAlignment = VerticalAlignmentStyle.Center

            '====== Row 0 - Set the column headers
            ws.Cells(0, 0).Value = "CR_ID"
            ws.Cells(0, 1).Value = "CM_ID"
            ws.Columns("A").Hidden = True
            ws.Columns("B").Hidden = True

            ws.Cells(0, 2).Value = "COURSE TITLE"
            ws.Cells(0, 3).Value = "Course Type"
            ws.Cells(0, 4).Value = "Name"
            ws.Cells(0, 5).Value = "EMAIL"

            ws.Cells(0, 6).Value = "EC / EY / KG / FS"
            ws.Cells(0, 7).Value = "Primary / Elementary"
            ws.Cells(0, 8).Value = "Secondary / Middle school / High School"
            ws.Cells(0, 9).Value = "ALL"

            ws.Cells(0, 10).Value = " Preparation and Planning"
            ws.Cells.GetSubrangeAbsolute(0, 10, 0, 13).Merged = True
            'Preperation and Planning columns heading
            ws.Cells(1, 10).Value = " Q1 "
            ws.Cells(1, 11).Value = " Q2 "
            ws.Cells(1, 12).Value = " Q3 "
            ws.Cells(1, 13).Value = " Q4 "

            ws.Cells(0, 14).Value = " Use of resources and facilities"
            ws.Cells.GetSubrangeAbsolute(0, 14, 0, 16).Merged = True
            'Preperation and Planning columns heading
            ws.Cells(1, 14).Value = " Q1 "
            ws.Cells(1, 15).Value = " Q2 "
            ws.Cells(1, 16).Value = " Q3 "
            'ws.Cells(1, 17).Value = " Q4 "




            ws.Cells(0, 17).Value = " Delivery skills"
            ws.Cells.GetSubrangeAbsolute(0, 17, 0, 23).Merged = True
            ws.Cells(1, 17).Value = " Q1 "
            ws.Cells(1, 18).Value = " Q2 "
            ws.Cells(1, 19).Value = " Q3 "
            ws.Cells(1, 20).Value = " Q4 "
            ws.Cells(1, 21).Value = " Q5 "
            ws.Cells(1, 22).Value = " Q6 "
            ws.Cells(1, 23).Value = " Q7 "

            ws.Cells(0, 24).Value = "Content"
            ws.Cells.GetSubrangeAbsolute(0, 24, 0, 26).Merged = True
            ws.Cells(1, 24).Value = " Q1 "
            ws.Cells(1, 25).Value = " Q2 "
            ws.Cells(1, 26).Value = " Q3 "

            ws.Cells(0, 27).Value = " Activities"
            ws.Cells.GetSubrangeAbsolute(0, 27, 0, 29).Merged = True
            ws.Cells(1, 27).Value = " Q1 "
            ws.Cells(1, 28).Value = " Q2 "
            ws.Cells(1, 29).Value = " Q3 "

            ws.Cells(0, 30).Value = "Participant learning "
            ws.Cells.GetSubrangeAbsolute(0, 30, 0, 34).Merged = True
            ws.Cells(1, 30).Value = " Q1 "
            ws.Cells(1, 31).Value = " Q2 "
            ws.Cells(1, 32).Value = " Q3 "
            ws.Cells(1, 33).Value = " Q4 "
            ws.Cells(1, 34).Value = " Q5 "


            ws.Cells(0, 35).Value = " How will the workshop influence your professional practice?"
            ws.Cells.GetSubrangeAbsolute(0, 35, 0, 40).Merged = True
            ws.Cells(1, 35).Value = " CHK1 "
            ws.Cells(1, 36).Value = " CHK2 "
            ws.Cells(1, 37).Value = " CHK3 "
            ws.Cells(1, 38).Value = " CHK4 "
            ws.Cells(1, 39).Value = " CHK5 "
            ws.Cells(1, 40).Value = " CHK6 "


            ws.Cells(0, 41).Value = " What elements of the PD Workshop that you thought went well, i.e. what you enjoyed / found most useful?"
            ws.Cells(0, 42).Value = " Is there something you would have liked to have seen or have been told about?"
            ws.Cells(0, 43).Value = " Any other comments?"

            ws.Cells(0, 44).Value = " What are your key take-away learnings from the professional development?"
            ws.Cells.GetSubrangeAbsolute(0, 44, 0, 46).Merged = True
            ws.Cells(1, 44).Value = " Text1 "
            ws.Cells(1, 45).Value = " Text2 "
            ws.Cells(1, 46).Value = " Text3 "



            CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
            CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
            CellStyle.Font.Name = "Verdana"
            CellStyle.Font.Weight = ExcelFont.BoldWeight
            CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#f2f2f2")
            CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#455798"))
            CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
            'For rowLoop As Integer = 0 To 1
            '    'For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 4
            '    For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 3
            '        ws.Cells(rowLoop, colLoop).Style = CellStyle
            '    Next
            'Next

            CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
            CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
            CellStyle.Font.Name = "Verdana"
            CellStyle.Font.Weight = ExcelFont.NormalWeight
            CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#000000")
            CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#ffffff"))
            CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)


            For index As Integer = 0 To 46
                ws.Cells(0, index).Style.Font.Color = Drawing.Color.White
                ' ws.Cells(0, index).Style.Borders = Drawing.Color.White
                ws.Cells(0, index).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
                ws.Cells(1, index).Style.Font.Color = Drawing.Color.Black
                ws.Cells(1, index).Style.FillPattern.SetSolid(Drawing.Color.LightGray)
                ws.Cells(0, index).Style.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
                ws.Cells(1, index).Style.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)

            Next

            For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
                Dim cnt As Integer = rowLoop + 2
                For colLoop As Integer = 0 To 46
                    ws.Cells(cnt, colLoop).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                    ws.Cells(cnt, colLoop).Style = CellStyle
                Next
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "CreateEFExcelTELLAL")
        End Try
        
        'For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
        '    For colLoop As Integer = 4 To 6
        '        ws.Cells(2 + rowLoop, colLoop + 31).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
        '        ws.Cells(2 + rowLoop, colLoop + 31).Style = CellStyle
        '    Next
        'Next

    End Sub

    Private Sub CreateEFExcelOnlineCourse(ByVal ws As ExcelWorksheet, ByVal dsExcel As DataSet)

       Dim dtEXCEL As New DataTable()

        Dim headerStyle As CellStyle = New CellStyle
        Dim colHeader As CellStyle = New CellStyle
        Dim CellStyle As CellStyle = New CellStyle
        Dim colTot As Integer = dsExcel.Tables(0).Columns.Count

        dsExcel.Tables(0).Columns.Remove("S1Q4")
        dsExcel.Tables(0).Columns.Remove("S2Q3")

        dsExcel.Tables(0).Columns.Remove("S3Q4")
        dsExcel.Tables(0).Columns.Remove("S3Q5")

        dsExcel.Tables(0).Columns.Remove("S3Q6")
        dsExcel.Tables(0).Columns.Remove("S3Q7")

        dsExcel.Tables(0).Columns.Remove("S6Q2")
        dsExcel.Tables(0).Columns.Remove("S6Q3")

        dsExcel.Tables(0).Columns.Remove("S6Q4")
        dsExcel.Tables(0).Columns.Remove("S6Q5")

        dsExcel.Tables(0).Columns.Remove("S7Q1")
        dsExcel.Tables(0).Columns.Remove("S7Q2")
        dsExcel.Tables(0).Columns.Remove("S7Q3")
        dsExcel.Tables(0).Columns.Remove("S7Q4")
        dsExcel.Tables(0).Columns.Remove("S7Q5")
        dsExcel.Tables(0).Columns.Remove("S7Q6")

        dsExcel.Tables(0).Columns.Remove("IS_DRAFT")
        dsExcel.Tables(0).Columns.Remove("ADDED_ON")
        dsExcel.Tables(0).Columns.Remove("MODIFIED_ON") 

        colHeader.HorizontalAlignment = HorizontalAlignmentStyle.Center
        colHeader.VerticalAlignment = VerticalAlignmentStyle.Center
        colHeader.Font.Weight = ExcelFont.BoldWeight

        headerStyle.Font.Size = 20 * 20
        headerStyle.Font.Name = "Verdana"
        headerStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        headerStyle.VerticalAlignment = VerticalAlignmentStyle.Center

        '====== Row 0 - Set the column headers
        ws.Cells(0, 0).Value = "CR_ID"
        ws.Cells(0, 1).Value = "CM_ID"
        ws.Columns("A").Hidden = True
        ws.Columns("B").Hidden = True

        ws.Cells(0, 2).Value = "COURSE TITLE"
        ws.Cells(0, 3).Value = "Course Type"
        ws.Cells(0, 4).Value = "Name"
        ws.Cells(0, 5).Value = "EMAIL"

        ws.Cells(0, 6).Value = "EC / EY / KG / FS"
        ws.Cells(0, 7).Value = "Primary / Elementary"
        ws.Cells(0, 8).Value = "Secondary / Middle school / High School"
        ws.Cells(0, 9).Value = "ALL"

        ws.Cells(0, 10).Value = " Preparation and Planning "
        ws.Cells.GetSubrangeAbsolute(0, 10, 0, 12).Merged = True
        'Preperation and Planning columns heading
        ws.Cells(1, 10).Value = " Q1 "
        ws.Cells(1, 11).Value = " Q2 "
        ws.Cells(1, 12).Value = " Q3 "


        ws.Cells(0, 13).Value = " Use of resources and facilities *"
        ws.Cells.GetSubrangeAbsolute(0, 13, 0, 14).Merged = True 
        ws.Cells(1, 13).Value = " Q1 "
        ws.Cells(1, 14).Value = " Q2 "
        



        ws.Cells(0, 15).Value = "Content"
        ws.Cells.GetSubrangeAbsolute(0, 15, 0, 17).Merged = True
        ws.Cells(1, 15).Value = " Q1 "
        ws.Cells(1, 16).Value = " Q2 "
        ws.Cells(1, 17).Value = " Q3 "
        
        ws.Cells(0, 18).Value = "Activities "
        ws.Cells.GetSubrangeAbsolute(0, 18, 0, 20).Merged = True
        ws.Cells(1, 18).Value = " Q1 "
        ws.Cells(1, 19).Value = " Q2 "
        ws.Cells(1, 20).Value = " Q3 "

        ws.Cells(0, 21).Value = " Participant learning *"
        ws.Cells.GetSubrangeAbsolute(0, 21, 0, 24).Merged = True
        ws.Cells(1, 21).Value = " Q1 "
        ws.Cells(1, 22).Value = " Q2 "
        ws.Cells(1, 23).Value = " Q3 "
        ws.Cells(1, 24).Value = " Q4 "
         

        ws.Cells(0, 25).Value = "WWW – What Worked Well  " + Environment.NewLine + " What elements of the online PD Workshop worked well, i.e. what you enjoyed / found most useful? "
        ws.Cells(0, 26).Value = "EBI – Even Better If " + Environment.NewLine + " What elements of the online PD Workshop do you think need improvement? "
        ws.Cells(0, 27).Value = "Discussion Forum " + Environment.NewLine + " Please comment on your experience with the discussion forums (e.g. time frames; posting; commenting; collaboration)"
        ws.Cells(0, 28).Value = "Any other comments? "

        ws.Cells(0, 29).Value = "  Key Take - Aways And Actions " + Environment.NewLine + " What are your key take-away learnings from the professional development? "
        ws.Cells.GetSubrangeAbsolute(0, 29, 0, 31).Merged = True
        ws.Cells(1, 29).Value = " Comment 1 "
        ws.Cells(1, 30).Value = "  Comment 1 "
        ws.Cells(1, 31).Value = "  Comment 1 "

        ws.Cells(0, 32).Value = "What actions will you now take to implement the new learning?" + Environment.NewLine + "  Consider both self (own practice) and others (departmental / school level impact) when writing action steps."
        ws.Cells.GetSubrangeAbsolute(0, 32, 0, 35).Merged = True
        ws.Cells(1, 32).Value = " Text1 "
        ws.Cells(1, 33).Value = " Text2 "
        ws.Cells(1, 34).Value = " Text3"
        ws.Cells(1, 35).Value = " Text4 "

        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.BoldWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#f2f2f2")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#455798"))
        CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
        'For rowLoop As Integer = 0 To 1
        '    'For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 4
        '    For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 3
        '        ws.Cells(rowLoop, colLoop).Style = CellStyle
        '    Next
        'Next

        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.NormalWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#000000")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#ffffff"))
        CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)


        For index As Integer = 0 To 46
            ws.Cells(0, index).Style.Font.Color = Drawing.Color.White
            ' ws.Cells(0, index).Style.Borders = Drawing.Color.White
            ws.Cells(0, index).Style.FillPattern.SetSolid(Drawing.Color.ForestGreen)
            ws.Cells(1, index).Style.Font.Color = Drawing.Color.Black
            ws.Cells(1, index).Style.FillPattern.SetSolid(Drawing.Color.LightGray)
            ws.Cells(0, index).Style.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
            ws.Cells(1, index).Style.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)

        Next

        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            Dim cnt As Integer = rowLoop + 2
            For colLoop As Integer = 0 To 46
                ws.Cells(cnt, colLoop).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                ws.Cells(cnt, colLoop).Style = CellStyle
            Next
        Next

    End Sub

    Sub TypicalTableSample(ByVal ws As ExcelWorksheet, ByVal dsExcel As DataSet)

        Dim dtEXCEL As New DataTable()

        Dim headerStyle As CellStyle = New CellStyle
        Dim colHeader As CellStyle = New CellStyle
        Dim CellStyle As CellStyle = New CellStyle
        Dim colTot As Integer = dsExcel.Tables(0).Columns.Count

        colHeader.HorizontalAlignment = HorizontalAlignmentStyle.Center
        colHeader.VerticalAlignment = VerticalAlignmentStyle.Center
        colHeader.Font.Weight = ExcelFont.BoldWeight

        headerStyle.Font.Size = 20 * 20
        headerStyle.Font.Name = "Verdana"
        headerStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        headerStyle.VerticalAlignment = VerticalAlignmentStyle.Center

        '====== Row 0 - Set the column headers
        ws.Cells(0, 0).Value = "PD TITLE"
        ws.Cells(0, 1).Value = "EMP ID"
        ws.Cells(0, 2).Value = "EMP NAME"
        ws.Cells(0, 3).Value = "EMP EMAIL"

        ws.Cells(0, 34).Value = "Any other comments"
        ws.Cells(0, 35).Value = "WWW – What Went Well "
        ws.Cells(0, 36).Value = "EBI – Even Better If"


        ws.Columns(0).Width = "5000"
        ws.Columns(1).Width = "3000"
        ws.Columns(2).Width = "10000"
        ws.Columns(3).Width = "10000"

        ws.Columns(34).Width = "10000"
        ws.Columns(35).Width = "10000"
        ws.Columns(36).Width = "10000"


        '====== Row 0 - The below will be merged columns based on questions
        ws.Cells.GetSubrangeAbsolute(0, 0, 1, 0).Merged = True
        ws.Cells.GetSubrangeAbsolute(0, 1, 1, 1).Merged = True
        ws.Cells.GetSubrangeAbsolute(0, 2, 1, 2).Merged = True
        ws.Cells.GetSubrangeAbsolute(0, 3, 1, 3).Merged = True

        ws.Cells.GetSubrangeAbsolute(0, 34, 1, 34).Merged = True
        ws.Cells.GetSubrangeAbsolute(0, 35, 1, 35).Merged = True
        ws.Cells.GetSubrangeAbsolute(0, 36, 1, 36).Merged = True


        ws.Cells(0, 4).Value = " Preparation and Planning"
        ws.Cells.GetSubrangeAbsolute(0, 4, 0, 7).Merged = True
        ws.Cells(0, 8).Value = " Use of resources and facilities"
        ws.Cells.GetSubrangeAbsolute(0, 8, 0, 10).Merged = True
        ws.Cells(0, 11).Value = " Delivery skills"
        ws.Cells.GetSubrangeAbsolute(0, 11, 0, 17).Merged = True
        ws.Cells(0, 18).Value = " Content"
        ws.Cells.GetSubrangeAbsolute(0, 18, 0, 19).Merged = True
        ws.Cells(0, 20).Value = " Activities"
        ws.Cells.GetSubrangeAbsolute(0, 20, 0, 22).Merged = True
        ws.Cells(0, 23).Value = " Participant learning"
        ws.Cells.GetSubrangeAbsolute(0, 23, 0, 27).Merged = True
        ws.Cells(0, 28).Value = " How will the workshop influence your professional practice?"
        ws.Cells.GetSubrangeAbsolute(0, 28, 0, 33).Merged = True


        '====== Row 1 - The below will be merged columns based on questions
        ws.Cells(1, 4).Value = " Q1 "
        ws.Cells(1, 5).Value = " Q2 "
        ws.Cells(1, 6).Value = " Q3 "
        ws.Cells(1, 7).Value = " Q4 "

        ws.Cells(1, 8).Value = " Q1 "
        ws.Cells(1, 9).Value = " Q2 "
        ws.Cells(1, 10).Value = " Q3 "

        ws.Cells(1, 11).Value = " Q1 "
        ws.Cells(1, 12).Value = " Q2 "
        ws.Cells(1, 13).Value = " Q3 "
        ws.Cells(1, 14).Value = " Q4 "
        ws.Cells(1, 15).Value = " Q5 "
        ws.Cells(1, 16).Value = " Q6 "
        ws.Cells(1, 17).Value = " Q7 "

        ws.Cells(1, 18).Value = " Q1 "
        ws.Cells(1, 19).Value = " Q2 "


        ws.Cells(1, 20).Value = " Q1 "
        ws.Cells(1, 21).Value = " Q2 "
        ws.Cells(1, 22).Value = " Q3 "

        ws.Cells(1, 23).Value = " Q1 "
        ws.Cells(1, 24).Value = " Q2 "
        ws.Cells(1, 25).Value = " Q3 "
        ws.Cells(1, 26).Value = " Q4 "
        ws.Cells(1, 27).Value = " Q5 "


        ws.Cells(1, 28).Value = " Q1 "
        ws.Cells(1, 29).Value = " Q2 "
        ws.Cells(1, 30).Value = " Q3 "
        ws.Cells(1, 31).Value = " Q4 "
        ws.Cells(1, 32).Value = " Q5 "
        ws.Cells(1, 33).Value = " Q6 "

        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.BoldWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#f2f2f2")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#455798"))
        CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
        For rowLoop As Integer = 0 To 1
            For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 4
                ws.Cells(rowLoop, colLoop).Style = CellStyle
            Next
        Next


        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.NormalWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#000000")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#ffffff"))

        '=== Headers Done - now insert data . 16 + is given so that we fill the radios first from SP. the 6 columns before that are checkboxes
        '-12 is given to handle the output from SP.
        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            For colLoop As Integer = 0 To 3
                ws.Cells(2 + rowLoop, colLoop).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                ws.Cells(2 + rowLoop, colLoop).Style = CellStyle
            Next
        Next


        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            For colLoop As Integer = 16 To dsExcel.Tables(0).Columns.Count - 1
                ws.Cells(2 + rowLoop, colLoop - 12).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                ws.Cells(2 + rowLoop, colLoop - 12).Style = CellStyle
            Next
        Next

        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            For colLoop As Integer = 9 To 14
                ws.Cells(2 + rowLoop, colLoop + 19).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                ws.Cells(2 + rowLoop, colLoop + 19).Style = CellStyle
            Next
        Next


        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Left
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.WrapText = True
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.NormalWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#000000")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#ffffff"))

        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            For colLoop As Integer = 4 To 6
                ws.Cells(2 + rowLoop, colLoop + 30).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                ws.Cells(2 + rowLoop, colLoop + 30).Style = CellStyle
            Next
        Next

    End Sub

    Private Function GetPD_Type(ByVal CM_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT CM_COURSE_TYPE_ID FROM dbo.COURSE_M WHERE CM_ID In (Select ID FROM dbo.fnSplitMe('" & CM_ID & "', '|'))"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            Dim tmpStr As String = String.Empty
            For Each row As DataRow In dsResponse.Tables(0).Rows
                tmpStr &= row.Item("CM_COURSE_TYPE_ID") & ", "
            Next
            tmpStr = tmpStr.TrimEnd(" ")
            tmpStr = tmpStr.TrimEnd(",")
            Return tmpStr
        Else
            Return ""
        End If
    End Function

    Private Function GetPD_TITLE(ByVal CM_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT CM_TITLE FROM dbo.COURSE_M WHERE CM_ID In (Select ID FROM dbo.fnSplitMe('" & CM_ID & "', '|'))"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            Dim tmpStr As String = String.Empty
            For Each row As DataRow In dsResponse.Tables(0).Rows
                tmpStr &= row.Item("CM_Title") & ", "
            Next
            tmpStr = tmpStr.TrimEnd(" ")
            tmpStr = tmpStr.TrimEnd(",")
            Return tmpStr
        Else
            Return ""
        End If
    End Function

    Sub TypicalTableSample2(ByVal ws As ExcelWorksheet, ByVal dsExcel As DataSet, ByVal ExcelCourseNames As String)

        Dim dtEXCEL As New DataTable()

        Dim headerStyle As CellStyle = New CellStyle
        Dim colHeader As CellStyle = New CellStyle
        Dim CellStyle As CellStyle = New CellStyle
        Dim CellStyle_LEFT As CellStyle = New CellStyle

        Dim colTot As Integer = dsExcel.Tables(0).Columns.Count

        colHeader.HorizontalAlignment = HorizontalAlignmentStyle.Center
        colHeader.VerticalAlignment = VerticalAlignmentStyle.Center
        colHeader.Font.Weight = ExcelFont.BoldWeight

        headerStyle.Font.Size = 20 * 20
        headerStyle.Font.Name = "Verdana"
        headerStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        headerStyle.VerticalAlignment = VerticalAlignmentStyle.Center


        'ws.Cells(0, 0).Value = "PD Evaluatio FS1 Curriculum PD COURSE"
        Dim CourseNameStyle As New CellStyle
        CourseNameStyle.WrapText = True
        ws.Cells(0, 0).Style = CourseNameStyle
        ws.Cells(0, 0).Value = ExcelCourseNames
        ws.Cells.GetSubrangeAbsolute(0, 0, 0, 13).Merged = True

        '====== Row 0 - Set the column headers
        ws.Cells(1, 0).Value = "Eval Group"
        ws.Cells(1, 1).Value = "Eval Question"
        ws.Cells(1, 2).Value = "SA"
        ws.Cells(1, 3).Value = "A"
        ws.Cells(1, 4).Value = "D"
        ws.Cells(1, 5).Value = "SD"
        ''   ws.Cells(1, 6).Value = "NA"
        ws.Cells(1, 6).Value = "Response"
        ws.Cells(1, 7).Value = "SA Value"
        ws.Cells(1, 8).Value = "A Value"
        ws.Cells(1, 9).Value = "D Value"
        ws.Cells(1, 10).Value = "SD Value"
        ws.Cells(1, 11).Value = "Total"
        ws.Cells(1, 12).Value = "Total /Response"




        ws.Columns(0).Width = "10000"
        ws.Columns(1).Width = "20000"
        ws.Columns(13).Width = "5000"





        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Left
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.BoldWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#f2f2f2")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#455798"))
        CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
        For rowLoop As Integer = 0 To 1
            For colLoop As Integer = 0 To 1
                ws.Cells(rowLoop, colLoop).Style = CellStyle
            Next
        Next

        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.BoldWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#f2f2f2")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#455798"))
        CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
        For rowLoop As Integer = 1 To 1
            For colLoop As Integer = 2 To dsExcel.Tables(0).Columns.Count - 1
                ws.Cells(rowLoop, colLoop).Style = CellStyle
            Next
        Next


        CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle.Font.Name = "Verdana"
        CellStyle.Font.Weight = ExcelFont.NormalWeight
        CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#000000")
        CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#ffffff"))
        CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)

        CellStyle_LEFT.HorizontalAlignment = HorizontalAlignmentStyle.Left
        CellStyle_LEFT.VerticalAlignment = VerticalAlignmentStyle.Center
        CellStyle_LEFT.Font.Name = "Verdana"
        CellStyle_LEFT.Font.Weight = ExcelFont.NormalWeight
        CellStyle_LEFT.Font.Color = System.Drawing.ColorTranslator.FromHtml("#000000")
        CellStyle_LEFT.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#ffffff"))
        CellStyle_LEFT.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)
        '=== Headers Done - now insert data . 16 + is given so that we fill the radios first from SP. the 6 columns before that are checkboxes
        '-12 is given to handle the output from SP.
        For rowLoop As Integer = 0 To dsExcel.Tables(0).Rows.Count - 1
            For colLoop As Integer = 0 To dsExcel.Tables(0).Columns.Count - 1
                ws.Cells(2 + rowLoop, colLoop).Value = dsExcel.Tables(0).Rows(rowLoop)(colLoop)
                If colLoop >= 2 Then
                    ws.Cells(2 + rowLoop, colLoop).Style = CellStyle
                Else
                    ws.Cells(2 + rowLoop, colLoop).Style = CellStyle_LEFT
                End If

            Next
        Next

    End Sub

    Private Sub bindParticipantsGridByEmpId(ByVal EmpIDs As String)

        Try


            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim str_Title As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim CR_IDs As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim TITLE As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable




            If gvParticipants.Rows.Count > 0 Then
                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BusinessUnit,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_BSU_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtTitle")

                If txtSearch.Text.Trim <> "" Then

                    TITLE = " AND replace(CM_Title,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_Title = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMPLOYEE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_EMP_Name = txtSearch.Text.Trim
                End If
            End If
            FILTER_COND = TITLE + EMP_Name + BSU_Name


            If EmpIDs <> "" Then
                CR_IDs = EmpIDs.Replace("||", "','")
            End If

            param(0) = New SqlClient.SqlParameter("@CM_IDs", hdnSelectedCourse.Value)

            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)

            param(2) = New SqlClient.SqlParameter("@CR_IDs", CR_IDs)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Participants_List_For_Course", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
                gvParticipants.Rows(0).Cells.Clear()
                gvParticipants.Rows(0).Cells.Add(New TableCell)
                gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
                tblGridData.Visible = False
            Else
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                tblGridData.Visible = True
                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")
                txtSearch.Text = str_EMP_Name
                txtSearch = gvParticipants.HeaderRow.FindControl("txtTitle")
                txtSearch.Text = str_Title
                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")
                txtSearch.Text = str_BSU_Name
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnGraphs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGraphs.Click
        Dim url As String
        Dim selCourse As String = String.Empty
        selCourse = hdnSelectedCourse.Value
        selCourse = selCourse.Trim("|").TrimStart("|")
        If selCourse.IndexOf("|") <> "-1" Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>Please select single course from the list</div>"
            hdnSelectedCourse.Value = ""
            txtCourses.Text = ""
            bindSelectedCourseList(hdnSelectedCourse.Value)
        Else
            Dim CourseType = GetPD_Type(hdnSelectedCourse.Value)
            Dim strRedirect As String = ""
            If CourseType = "1" Then
                strRedirect = "../Survey/Tellal_comPrintSurveyResults_PD.aspx"
            Else
                strRedirect = "../Survey/Tellal_comPrintSurveyResults_PD_online.aspx"
            End If

            Dim PD_CM_ID As String = Encr_decrData.Encrypt(hdnSelectedCourse.Value)
            Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
            Dim EMP_ID As String = Encr_decrData.Encrypt(Session("EmployeeId"))

            url = String.Format("{0}?PD_CM_VAL={1}&BSU_ID={2}&EMP_ID={3}", strRedirect, PD_CM_ID, BSU_ID, EMP_ID)
            ResponseHelper.Redirect(url, "_blank", "")
        End If

        'Dim PD_CM_ID As String
        'Dim strRedirect As String = "../Survey/comPrintSurveyResults_PD.aspx"
        ''Dim PD_CM_ID As String = Encr_decrData.Encrypt(hdnSelectedCourse.Value)
        'Dim PD_CM_IDs() As String
        'hdnSelectedCourse.Value = hdnSelectedCourse.Value.Replace("||", "|")
        'If hdnSelectedCourse.Value.EndsWith("|") Then
        '    hdnSelectedCourse.Value = hdnSelectedCourse.Value.TrimEnd("|")
        'End If
        'PD_CM_IDs = hdnSelectedCourse.Value.Split("|")
        'Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
        'Dim EMP_ID As String = Encr_decrData.Encrypt(Session("EmployeeId"))

        ''if multiple courses are selected, then first get a prompt confirmation
        ''If PD_CM_IDs.Length > 1 Then
        ''    Me.btnGraphs.Attributes.Add("onclick", "return confirm('If multiple course(s) are selected, each selected course graph will open in a new window. Are you sure you want to continue?');")
        ''End If

        'Dim i As Integer = 0
        'For Each Str As String In PD_CM_IDs
        '    If Not Str = "" Then
        '        PD_CM_ID = Encr_decrData.Encrypt(Str)
        '        url = String.Format("{0}?PD_CM_VAL={1}&BSU_ID={2}&EMP_ID={3}", strRedirect, PD_CM_ID, BSU_ID, EMP_ID)
        '        Dim script As String
        '        script = "window.open(" & Chr(34) & url & Chr(34) & "," & Chr(34) & "_blank" & Chr(34) & ");"
        '        ScriptManager.RegisterStartupScript(Page, GetType(Page), "Redirect" & i.ToString, script, True)
        '    End If
        '    i += 1
        'Next

    End Sub

    Protected Sub btnGraphs2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGraphs2.Click
        Dim url As String
        'Dim selCourse As String = String.Empty
        'selCourse = hdnSelectedCourse.Value
        'selCourse = selCourse.Trim("|")
        'If selCourse.IndexOf("|") <> "-1" Then
        '    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>Please select single course from the list</div>"
        '    hdnSelectedCourse.Value = ""
        '    txtCourses.Text = ""
        '    bindSelectedCourseList(hdnSelectedCourse.Value)
        'Else
        '    Dim strRedirect As String = "../Survey/comPrintSurveyResults_PDCONSOL.aspx"
        '    Dim PD_CM_ID As String = Encr_decrData.Encrypt(hdnSelectedCourse.Value)
        '    Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
        '    Dim EMP_ID As String = Encr_decrData.Encrypt(Session("EmployeeId"))

        '    url = String.Format("{0}?PD_CM_VAL={1}&BSU_ID={2}&EMP_ID={3}", strRedirect, PD_CM_ID, BSU_ID, EMP_ID)
        '    ResponseHelper.Redirect(url, "_blank", "")
        'End If

        Dim selCourse As String = String.Empty
        selCourse = hdnSelectedCourse.Value.Replace("||", "|").TrimEnd("|").TrimStart("|")
        bindSelectedCourseList(hdnSelectedCourse.Value)

        'hashmi temp code used only while debugging
        ''selCourse = "327|444"
        'end hashmi temp code used only while debugging

        Dim strRedirect As String = "../Survey/Tellal_comPrintSurveyResults_PDCONSOL.aspx"
        'Dim PD_CM_ID As String = Encr_decrData.Encrypt(hdnSelectedCourse.Value)
        Dim PD_CM_ID As String = Encr_decrData.Encrypt(selCourse)
        Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
        Dim EMP_ID As String = Encr_decrData.Encrypt(Session("EmployeeId"))
        url = String.Format("{0}?PD_CM_VAL={1}&BSU_ID={2}&EMP_ID={3}", strRedirect, PD_CM_ID, BSU_ID, EMP_ID)
        ResponseHelper.Redirect(url, "_blank", "")
    End Sub
    Sub bindSelectedCourseList(ByVal CM_IDs As String)

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim CM_IDfilter As String = String.Empty
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        Try

            param(0) = New SqlClient.SqlParameter("@CM_ID", CM_IDs)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Course_Info_By_Ids", param)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
                Dim columnCount As Integer = gvCourseList.Rows(0).Cells.Count
                gvCourseList.Rows(0).Cells.Clear()
                gvCourseList.Rows(0).Cells.Add(New TableCell)
                gvCourseList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCourseList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCourseList.Rows(0).Cells(0).Text = "Please select course from the list."
            Else
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
            End If

        Catch ex As Exception

        End Try

    End Sub

  

    Function Check_PD_Coordinator() As Boolean
        Dim IsPDC As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@User_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PDC_By_UserID", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsPDC = True
            End If

        End If
        Return IsPDC
    End Function

    Function Check_PD_Trainer() As Boolean
        Dim IsTrainer As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_TRAINER_BY_BSU_EMP", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsTrainer = True
            End If

        End If
        Return IsTrainer
    End Function
End Class
