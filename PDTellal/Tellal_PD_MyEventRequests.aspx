<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="Tellal_PD_MyEventRequests.aspx.vb" Inherits="Tellal_PD_MyEventRequests" Title="::::Tellal PD::::" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script type="text/javascript">
        function timedRefresh(timeoutPeriod) {
        
            setTimeout("location.reload(true);", timeoutPeriod);
        }
        $(document).ready(function() {

            $(".frameKTAA").fancybox({
                type: 'iframe',
               
                fitToView: false,
                width: '75%',
                height: '75%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });

         

            $(".frameCERTIFICATE").fancybox({
                type: 'iframe',
                maxWidth: 500,
                maxHeight: 800,
                fitToView: false,
                width: '70%',
                height: '110%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });



        function checkDate(sender, args) {

            var currenttime = document.getElementById("<%=hfDate.ClientID %>").value;

            if (sender._selectedDate > new Date(currenttime)) {
                alert("You cannot select a day greater than today!");
                sender._selectedDate = new Date(currenttime);
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
      
    </script>
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>My PD Requests
        </div>
        <div class="card-body">
            <div class="table-responsive">
   
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label><span style="color: #c00000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary>
                        <span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        <tr>
            <td >
                <table width="100%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <span style="font-size: 10pt; color: #ffffff">
                                <asp:Literal ID="ltLabel" runat="server" Text="Search Criteria"></asp:Literal></span>
                        </td>
                    </tr>
                    
                    
                    <tr id="trAcademicYear" runat="server">
                        <td align="left" >
                          <span class="field-label" >      Region</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="True" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                           <span class="field-label" >     Location</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" Width="200px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trSubject" runat="server">
                        <td align="left" >
                          <span class="field-label" >      From Date</span>
                        </td>
                      
                        <td align="left" >
                            <asp:TextBox ID="txtFromDate" runat="server" Width="90px" autocomplete="off"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtFromDate" CssClass="MyCalendar" >
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                Visible="False"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server"
                                    ControlToValidate="txtFromDate" CssClass="error" Display="Dynamic" ErrorMessage="From Date required"
                                    ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" >
                         <span class="field-label" >       To Date</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtToDate" runat="server" Width="90px" autocomplete="off"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtToDate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;<asp:ImageButton ID="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"
                                Visible="False"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="rfvDate0" runat="server" ControlToValidate="txtToDate"
                                CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="trSubject1" runat="server">
                        <td align="left" >
                           <span class="field-label" >     Event Title</span>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtTitle" runat="server" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="matters"  valign="bottom">
            </td>
        </tr>
        <tr>
            <td class="matters"  valign="bottom">
                <asp:RadioButton ID="optPending" runat="server" AutoPostBack="True" GroupName="Selection"
                    Text="Pending" />
                &nbsp;&nbsp;
                <asp:RadioButton ID="optApproved" runat="server" AutoPostBack="True" GroupName="Selection"
                    Text="Approved" />
                &nbsp;&nbsp;
                <asp:RadioButton ID="optRejected" runat="server" AutoPostBack="True" GroupName="Selection"
                    Text="Rejected" />
                &nbsp;&nbsp;
                <asp:RadioButton ID="optAll" runat="server" AutoPostBack="True" GroupName="Selection"
                    Text="All" />
                <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" Visible="False" />
            </td>
        </tr>
        <tr>
            <td class="matters"  valign="bottom">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td >
                <asp:GridView ID="gvRequest" runat="server" AutoGenerateColumns="False"
                    Height="100%" Width="100%" EnableModelValidation="True"  CssClass="table table-bordered table-row" OnRowDataBound="gvRequest_RowDataBound">
                   
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" __designer:wfdid="w18" Text='<%# bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SR. NO">
                            <ItemTemplate>
                                <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("SR_NO") %>' ></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Title">
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" Text='<%# bind("Title") %>' ></asp:Label>
                                <asp:Literal ID="ltStar0" runat="server" ></asp:Literal>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px" Wrap="True"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" Width="30%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Date">
                            <ItemTemplate>
                                <asp:Label ID="lblEventDate" runat="server" Text='<%# Bind("COMBINED_DATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Location &amp; Region">
                            <ItemTemplate>
                                <asp:Label ID="lblLocationRegion" runat="server"  Text='<%# bind("Location_Region") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Request Date">
                            <ItemTemplate>
                                <asp:Label ID="lblRequestDate" runat="server" __designer:wfdid="w18" Text='<%# Eval("REQUEST_DATE", "{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" __designer:wfdid="w18" Text='<%# bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CS_STATUS" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCSStatus" runat="server" __designer:wfdid="w18" Text='<%# bind("CS_STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField SelectText="Cancel" ShowDeleteButton="True" Visible="false" />
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnCancel" runat="server" CommandName="Delete" ImageUrl="~/Images/DELETE.png"
                                    OnClientClick="return confirm('Are you sure you want to cancel the selected PD request?');"
                                    EnableTheming="True" Height="14px" Width="14px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Evaluation">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtnEF" runat="server" 
                                OnClientClick='<%# GetNavigateUrl(Eval("CM_ID").ToString(), Eval("ID").ToString(), Eval("CM_EF_ENABLED").ToString())%>'
                                     CommandArgument='<%# Bind("CM_ID") %>' Enabled='<%# Bind("CM_EF_ENABLED") %>'>Evaluation & Action Plan Form</asp:LinkButton>&nbsp;<%--OnClick="lnkPreview_Click"--%>
                                
                            </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                        </asp:TemplateField>
                        

                           <asp:TemplateField HeaderText="Certificate (PDF)">
                            <ItemTemplate>
                                  
                                    <asp:LinkButton ID="lnkCertificatePdf" runat="server"
                                    OnClick="lnkCertificatePdf_Click" Enabled='<%# Bind("CM_CERTIFICATE_ENABLED") %>'  CommandArgument='<%# Bind("ID") %>'  >Download & Save/Print</asp:LinkButton>&nbsp;
                                    
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop" Height="15px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Font-Size="11px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Height="25px" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 22px" valign="bottom">
                &nbsp;
            </td>
        </tr>
        <tr>
                        <td  align="right">
                              <asp:Button ID="lnkBackToCalendar" runat="server" Text="Back" OnClick="lnkBackToCalendar_Click" CssClass="button"></asp:Button>
                        </td>
                    </tr>
    </table>
      </div></div></div>
    <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMode" runat="server"></asp:HiddenField>
     <cr:crystalreportsource id="rs" runat="server" cacheduration="1">
    </cr:crystalreportsource>
</asp:Content>
