﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Partial Class Tellal_PD_Trainer_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                ViewState("PDC_ID") = 0
                'bindBusinessUnits()
                BindPD_PD_Co_Trainers()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try


        End If

    End Sub


    Protected Sub lbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_Trainers_ADD.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblPDT_ID As New Label
            Dim errormsg As String = String.Empty

            lblPDT_ID = TryCast(sender.FindControl("lblID"), Label)
            ViewState("ID") = lblPDT_ID.Text

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PDTellal\Tellal_PD_Trainer_Add.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)

        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvPDTrainsers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPDTrainsers.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)

            Dim lblId As System.Web.UI.WebControls.Label = CType(e.Row.FindControl("lblID"), System.Web.UI.WebControls.Label)
            Dim lnkEdit As System.Web.UI.WebControls.HyperLink = CType(e.Row.FindControl("lnkEdit"), System.Web.UI.WebControls.HyperLink)
            Dim url As String 
            url = String.Format("~\PDTellal\Tellal_PD_Trainer_Add.aspx?Id=" + lblId.Text)
            lnkEdit.NavigateUrl = url
            'imageFile.ImageUrl = strImageUrl

        End If
    End Sub

    Protected Sub gvPDTrainsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDTrainsers.PageIndex = e.NewPageIndex
        BindPD_PD_Co_Trainers()
    End Sub

    Protected Sub btnSearchFullName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Trainers()
    End Sub

    Protected Sub btnSearchEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Trainers()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Trainers()
    End Sub
    Protected Sub btnSearchMobile_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Trainers()
    End Sub



    Sub BindPD_PD_Co_Trainers()
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString

        Dim param(5) As SqlClient.SqlParameter

        Dim str_FullName As String = String.Empty
        Dim str_Email As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim fullName As String = String.Empty
        Dim email As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim Dt As New DataTable
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty
        Try

            If gvPDTrainsers.Rows.Count > 0 Then

                txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtFullName")

                If txtSearch.Text.Trim <> "" Then
                    fullName = " AND FullName like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_FullName = txtSearch.Text.Trim
                End If

                txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND BSU_NAME like '%" & txtSearch.Text.Trim() & "%' "
                    str_BSU_Name = txtSearch.Text.Trim()
                End If

                txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtEmail")

                If txtSearch.Text.Trim <> "" Then

                    email = " AND Email like '%" & txtSearch.Text.Trim() & "%' "
                    str_Email = txtSearch.Text.Trim()
                End If


            End If
            FILTER_COND = fullName + BSU_Name + email

            param(0) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetTrainers_All]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                gvPDTrainsers.DataSource = ds.Tables(0)
                gvPDTrainsers.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvPDTrainsers.DataSource = ds.Tables(0)
                Try
                    gvPDTrainsers.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvPDTrainsers.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPDTrainsers.Rows(0).Cells.Clear()
                gvPDTrainsers.Rows(0).Cells.Add(New TableCell)
                gvPDTrainsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDTrainsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDTrainsers.Rows(0).Cells(0).Text = "No records available !!!"
            End If

            txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtFullName")
            txtSearch.Text = str_FullName

            txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtBSU_Name")
            txtSearch.Text = str_BSU_Name

            txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtEmail")
            txtSearch.Text = str_Email

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BindPD_PD_Co_Trainers")
        End Try

    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim PDT_ID As String = ViewState("PDT_ID")

        Using CONN As SqlConnection = ConnectionManger.Get_TellalPD_Connection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(3) As SqlParameter
                param(0) = New SqlParameter("@PDT_ID", PDT_ID)

                param(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(1).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.PDT_REMOVE_COORDINATOR", param)
                Dim ReturnFlag As Integer = param(1).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    ' resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function
End Class
