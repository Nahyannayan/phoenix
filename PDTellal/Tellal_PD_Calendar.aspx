﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_PD_Calendar.aspx.vb" Inherits="Tellal_PD_Calendar" %>
 <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<style  media="all">
        #RadMonthYearPicker1 {   
            display:none!important;
        } 
       
        .newList {
          background-color: white !important;
          padding-top: 10px;
          border-bottom: 1px solid #cecece;
        }
        .list-view-img {
    padding: 0px !important;
}
        .list-view-title {
    font-size: 18px;
    font-weight: bold;
    color: rgb(102,102,102);
    font-variant: all-small-caps;
}
        .list-view-trainer {
    color: rgb(0,179,168);
    font-size: 14px;
    font-weight600;
}
        .list-event {
    color: rgb(102,102,102);
    font-size: 14px;
    font-weight: bold;
}


        .cal-title {
    font-size: 17px;
    font-weight: bold;
    color: rgb(102,102,102);
    font-variant: all-small-caps;
    line-height: 17px;
    margin-bottom: 6px;
}
        .cal-trainer {
    color: rgb(0,179,168);
    font-size: 12px;
    font-weight:600;
        line-height: 12px;
    margin-bottom: 6px;
}
        .cal-event {
    color: rgb(102,102,102);
    font-size: 12px;
    font-weight: bold;
}
.cal-desc {
    color: rgb(102,102,102);
    font-size: 12px; 
        font-weight: normal;
}

        .cal-day, .calendarSDays, .cal-top, .DayDefaultOtherMonth, .calendarDays {
        vertical-align:top !important;
        font-size:14px !important;
        }

        .switch label input[type=checkbox]:checked+.lever:after {
            background-color: #85BD00 !important;
        }
        .switch label input[type=checkbox]:checked+.lever {
    background-color: #cfdcb0;
}

        .RadCalendarMonthView_Default {
            background-color:#ffffff !important;
        }
    </style>

    <style>
        .switch label input[type=checkbox]:checked+.lever {
    background-color: #dccfe2;
}
 
.switch label input[type=checkbox]:checked+.lever {
    background-color: #cfdcb0;
}

.switch label input[type=checkbox] {
    opacity: 0;
     width: 0; 
     height: 0; 
}
        .switch label {
    cursor: pointer;
}
        .switch label .lever {
    content: "";
    display: inline-block;
    position: relative;
    background-color: #818181;
    -webkit-border-radius: .9375rem;
    border-radius: .9375rem;
    margin-right: .625rem;
    vertical-align: middle;
    margin: 0 1rem;
    width: 2.5rem;
    height: .9375rem;
    -webkit-transition: background .3s ease;
    -o-transition: background .3s ease;
    transition: background .3s ease;
}
.switch label .lever:after {
    content: "";
    position: absolute;
    display: inline-block;
    background-color: #f1f1f1;
    -webkit-border-radius: 1.3125rem;
    border-radius: 1.3125rem;
    left: -.3125rem;
    top: -.1875rem;
    -webkit-box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0,0,0,.4);
    box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0,0,0,.4);
    width: 1.3125rem;
    height: 1.3125rem;
    -webkit-transition: left .3s ease,background .3s ease,-webkit-box-shadow 1s ease;
    transition: left .3s ease,background .3s ease,-webkit-box-shadow 1s ease;
    -o-transition: left .3s ease,background .3s ease,box-shadow 1s ease;
    transition: left .3s ease,background .3s ease,box-shadow 1s ease;
    transition: left .3s ease,background .3s ease,box-shadow 1s ease,-webkit-box-shadow 1s ease;
}

        .card {
    -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    border: 0;
    font-weight: 400;
}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
    </style>


<link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
          
      <link href="../vendor/bootstrap/css/compiled-4.6.1.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="../vendor/bootstrap/css/bootstrap.css" type="text/css"  rel="stylesheet" media="all"/>


    <script type="text/javascript" src="../Scripts/jquery-1.9.1.js" ></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
      
    <script type="text/javascript">



        function GoToPage(Id) {
           
            $.fancybox({
                type: 'iframe',
                maxWidth: 300,
                href: 'Tellal_PD_Calendar_Apply.aspx?val=' + Id,
                maxHeight: 600,
                fitToView: false,
                width: '100%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                topRatio: 0,
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '20px', 'bottom': 'auto' });
                }
             });
         
        }

        function GoToPages(eventId) {
        window.location.href = "Tellal_PD_Calendar_Apply.aspx?val=" + eventId;
    }
    </script>
     

     <div class="card mb-3">
        <div class="card-header letter-space" style="background-color:rgba(0, 0, 0, 0.1)!important;">
            <i class="fa fa-book mr-3"></i>TELLAL PD Calendar
        </div>
         <div class="card-body">
             <div id="lblError" runat="server">
              </div>
      
         <asp:HiddenField ID="hdnCM_ID" runat="server" />
            <div class="row pt-4 pb-4">

                <div class="col-xs-12 col-lg-4 col-sm-4 col-md-4 m-auto">
                    <!-- Material checked -->
                    <div class="switch">
                        <label>
                            Calendar View
                            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="Group1_CheckedChanged" Checked="false" />
                         <%-- <input type="checkbox" id="chkCalendar" onchange="GetListView();" checked/>--%>
                            <span class="lever"></span>List View
                        </label>
                    </div>


                    <%--<asp:RadioButton ID="rdCalender" GroupName="CalenderView" CssClass="field-label" Text="Calender View" Checked="true" runat="server" AutoPostBack="true" OnCheckedChanged="Group1_CheckedChanged" />--%>
                </div>
                <div class="col-xs-12 col-lg-3 col-sm-3 col-md-3 m-auto">
                   <%-- <asp:RadioButton ID="rdListView" GroupName="CalenderView" CssClass="field-label" Text="List View" runat="server" AutoPostBack="true" OnCheckedChanged="Group1_CheckedChanged" />--%>

                    <telerik:RadMonthYearPicker RenderMode="Lightweight" ID="RadMonthYearPicker1" runat="server" Width="238px" AutoPostBack="true">
                    </telerik:RadMonthYearPicker>
                </div>
               <div class="col-xs-12 col-lg-3 col-sm-3 col-md-3 m-auto">
                     
                        <asp:TextBox ID="txtSearch" CssClass="form-control" runat="server" Visible="false"></asp:TextBox>
                    
                </div>
                <div class="col-xs-12 col-lg-2 col-sm-2 col-md-2 m-auto">
                    
                        <asp:ImageButton ID="imgSearch" runat="server" ImageUrl="~/Images/bullet_04.gif" Visible="false"/>
                  
                </div>
                
            </div>
            

            <div id="divCalenderView" runat="server" visible="true" class="row">
               <asp:Calendar ID="calendarPD" runat="server" BorderStyle="Solid" BorderColor="LightGray"
                    CellPadding="5" CellSpacing="5" FirstDayOfWeek="Sunday" Height="300px" OnDayRender="calendarPD_DayRender"
                    OnVisibleMonthChanged="calendarPD_VisibleMonthChanged"
                    ShowGridLines="True" Width="100%"
                    ForeColor="WhiteSmoke" SelectionMode="Day" DayNameFormat="Full" Font-Names="Book Antiqua"
                    Font-Size="Medium" OnPreRender="calendarPD_PreRender"
                    NextPrevFormat="FullMonth" NextPrevStyle-ForeColor="Gray">
                    <DayHeaderStyle CssClass="" BackColor="#00b3a8" BorderColor="white" />
                    <DayStyle CssClass="cal-day" BackColor="#ffffff" BorderColor="LightGray" BorderWidth="1" Font-Bold="True"
                        Font-Italic="False" Width="100" Font-Size="Smaller"
                        Font-Overline="False" Font-Underline="False" ForeColor="#85bd00" Height="100px"
                        HorizontalAlign="left" VerticalAlign="Top" BorderStyle="Solid"  />
                    <NextPrevStyle />
                    <OtherMonthDayStyle CssClass="DayDefaultOtherMonth" BorderColor="LightGray" BorderStyle="Solid" />
                    <SelectedDayStyle CssClass="calendarSDays" ForeColor="Black" />
                    <TitleStyle CssClass="cal-top" BackColor="white" ForeColor="Gray" Font-Size="X-Large" Font-Bold="true" VerticalAlign="Top" BorderColor="#2d9b23" BorderWidth="0" />
                </asp:Calendar>

            </div>

            <div id="divListView" runat="server" visible="false" class="row">

                  <asp:Repeater ID="RepterDetails" runat="server" OnItemDataBound="RepterDetails_ItemDataBound">
                    <HeaderTemplate>                        
                           <%-- <div class="row">
                                <div class="col-12 col-lg-12 col-md-12">                                 
                                </div>
                            </div>--%>
                            
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="container-fluid">
                        <div class="row newList">
                         <div class="col-3 col-lg-3 col-md-3">
                            <%-- <img src="../Images/5.png" id="img" runat="server"  />--%>
                             <asp:Image ID="imgCM" runat="server" cssClass="img-fluid" alt="Responsive image"  />
                         </div>
                        <div class="col-9 col-lg-9 col-md-9">
                            <div>
                                 
                                <div class="list-view-title"> 
                                    <%--<asp:HyperLink ID="hyplink" runat="server" onclick="GoToPage('<%#Eval("CM_ID") %>')" style="color:rgb(102,102,102)"><u>  <%#Eval("CM_TITLE")%></u></asp:HyperLink>--%>

                                    <a href="#" style="color:rgb(102,102,102)" onclick="GoToPage(<%#Eval("CM_ID") %>)" ><u>  <%#Eval("CM_TITLE")%></u></a>
                                  </div>
                                  <asp:HiddenField ID="hdnCM_ID" runat="server" Value='<%#Eval("CM_ID") %>' />
                                  <div class="list-view-trainer"> <%#Eval("Trainers")%></div>
                                
                                <div class="list-event">
                                    <%#Eval("CM_EVENT_DT", "{0:MMMM dd yyyy}")%> | <%#Eval("CM_START_TIME") %> - <%#Eval("CM_END_TIME") %>                                     
                                </div>
                                <div class="list-desc"><p>
                                    <asp:Label ID="lblDesc" runat="server" width="100%" Text='<%#Eval("CM_DESCR")%>' ></asp:Label></p></div>
                                   
                               <%--<div class="list-link"><a target="_blank" href="PD_Calendar_Apply_New.aspx?val=<%#Eval("CM_ID") %>">Apply</a></div>--%>
                            </div>
                        </div>
                        </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                      
                    </FooterTemplate>
                </asp:Repeater>


               
            </div>

             <asp:Label ID="lblMsg" runat="server" CssClass="errMsg" Text="Sorry, no course is there to show." Visible="false"/>
                 <asp:Button ID="btnHiddn" runat="server" style="display:none" Text="hdnbtn" />
                  <asp:HiddenField ID="hdnFCO_FCO_ID1" runat="server" />
         
             </div>
  </div>

    
</asp:Content>

