﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Tellal_Add_Course_Trainers
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("ID") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("ID").ToString
                ViewState("MainID") = MainID
                bindTrainersGrid()
            End If
        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        bindTrainersGrid()
    End Sub

    Private Sub bindTrainersGrid()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim param(2) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        Dim str_query As String = ""



        Dim str_FullName As String = String.Empty
        Dim str_BSU_Name As String = String.Empty 
        Dim FullName As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty

        Try
            If gvPDC.Rows.Count > 0 Then

                txtSearch = gvPDC.HeaderRow.FindControl("txtFullName")

                If txtSearch.Text.Trim <> "" Then
                    FullName = " AND replace(FullName,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_FullName = txtSearch.Text.Trim
                End If
                 

                txtSearch = gvPDC.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BSU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "


                    str_BSU_Name = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = FullName + BSU_Name

            param(0) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetTrainers_All]", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPDC.DataSource = ds
                gvPDC.DataBind()
                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "Currently there is no Trainers Exists"
            Else
                gvPDC.DataSource = ds
                gvPDC.DataBind()
            End If
        Catch ex As Exception

        End Try

    End Sub

    
    Protected Sub btnSearchFullName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindTrainersGrid()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindTrainersGrid()
    End Sub

    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click
        Dim Ids As String = String.Empty

        For Each gvrow As GridViewRow In gvPDC.Rows
            Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
            If chk IsNot Nothing And chk.Checked Then
                Ids += gvPDC.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

            End If
        Next

        ''checking if seat is available

        Ids = Ids.Trim(",".ToCharArray())
        hdnSelected.Value = Ids
        Session("liEmpList") = Ids
        hdnSelectedTrainersSelection.Value = Ids
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Script11weasd", "setTrainerToParent();", True)

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('hdnSelected').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write(" var oArg = new Object();")
        'Response.Write("oArg.NameCode = '" & hdnSelected.Value & "||" & hdnSelected.Value & "';")
        'Response.Write("var oWnd = GetRadWindow('" & hdnSelected.Value & "||" & hdnSelected.Value & "');")
        'Response.Write("oWnd.close(oArg);")
        'Response.Write("} </script>")

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("parent.$.fancybox.close();")
        'Response.Write("} </script>")
        'test

    End Sub
End Class
