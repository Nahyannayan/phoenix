﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Tellal_PD_Calendar_Apply.aspx.vb"
    Inherits="Tellal_PD_Calendar_Apply" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript">
        function fancyClose() {

            parent.$.fancybox.close();
        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
   
        <div id="lblError" runat="server" class="alert-warning">
        </div>
      
        <table width="100%" 
            id="tblCategory" >
            <tr>
                <td class="title-bg" colspan="4">Apply For Session</td>
            </tr>
            <tr align="left" id="trBSU" runat="server" >
                <td >
                   <span class="field-label">  Title</span>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblCSTitle" runat="server" CssClass="field-value"></asp:Label>
                </td>
               
            </tr>

             <tr align="left">
                <td width="30%">
                 <span class="field-label">    Course Details</span>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblDetails" runat="server" CssClass="field-value"></asp:Label>
                    
                </td>
                <td></td>
            </tr>
            <tr align="left">
                <td >
                  <span class="field-label">    Date</span>
                </td>
                <td  >
                    <asp:Label ID="lblDate" runat="server" CssClass="field-value"></asp:Label>
                  
                </td>
              
                <td >
                   <span class="field-label">   Time</span>
                </td>
                <td  >
                    <asp:Label ID="lblTimeSlot" runat="server" CssClass="field-value"></asp:Label>
              
                </td>
                
            </tr>
                <tr align="left">
                <td width="30%">
                   <span class="field-label"> Trainers</span>
                </td>
                <td>
                    <asp:Label ID="lblTrainer" runat="server" CssClass="field-value"></asp:Label>                   
                </td>
                   <td>
                   <span class="field-label">  Max Seat</span>
                </td>
                <td  colspan="2">
                    <asp:Label ID="lblMax" runat="server" CssClass="field-value"></asp:Label>
                   
                </td>
            </tr>
           
            <tr align="left">
                <td width="30%">
                    <span class="field-label"> Registration Available From</span>
                </td>
                <td>
                    <asp:Label ID="lblOnlineDate" runat="server" CssClass="field-value"></asp:Label>
                    <br />
                </td>
                <td>
                   <span class="field-label"> Course Type</span>  
                </td>
                <td><asp:Label ID="lblCourseType" runat="server" CssClass="field-value"></asp:Label></td>
               
            </tr>
            
             <tr align="left">
                  <td>
                    <span class="field-label">   Region</span>
                </td>
                <td>
                    <%--<asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="true">
                    </asp:DropDownList>--%>
                    <asp:Label ID="lblRegion" runat="server" CssClass="field-value"></asp:Label>
                </td>
                <td>
                     <span class="field-label">   Location</span>
                </td>
                <td>
                    <%--<asp:DropDownList ID="ddlLocation" runat="server" >
                    </asp:DropDownList>--%>
                    <asp:Label ID="lblLocation" runat="server" CssClass="field-value"></asp:Label>
                </td> 
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="btnRegsiter" runat="server" Text="REGISTER" CssClass="button" />
                    <input type="button" class="button" id="btnCancel1" title="CLOSE" value="CLOSE" onclick="fancyClose()" />
                </td>
            </tr>
        </table>
    
    </form>
</body>
</html>
