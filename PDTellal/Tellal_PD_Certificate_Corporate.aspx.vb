﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Partial Class Tellal_PD_Certificate_Corporate
    Inherits BasePage
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Dim isPD_SuperUser As Boolean = Check_PD_SuperUser()

                    'If Not isPD_SuperUser Then
                    '    tblNoAccess.Visible = True
                    '    tbl_AddGroup.Visible = False
                    '    tblGridView.Visible = False
                    '    Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                    '    ' lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & Errormsg & "</div>"
                    '    lblNoAccess.Text = Errormsg
                    'End If

                    txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                    bindAcademicYearFilter()

                End If

                'show all requests by default


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub gvParticipantList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lnkCertificatePdf As LinkButton = DirectCast(e.Row.FindControl("lnkCertificatePdf"), LinkButton)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkCertificatePdf)
        End If
    End Sub
     
    Protected Sub lnkCertificatePdf_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim CR_ID As String = sender.CommandArgument.ToString
            'Dim isInsert As Integer = InsertDownloadAudit(CR_ID)

            ''added by nahyan to find event date is after sep 2015 to display new certificate (5-oct-2015)

            Dim eventdate As String = GetEventDate(CR_ID)

            Dim reportpath As String = String.Empty
            'If Convert.ToDateTime(eventdate) < "01/Sep/2015" Then
            '    reportpath = "Report/PD_CertificateLS_Before1Sep2015.rpt"
            'ElseIf Convert.ToDateTime(eventdate) < "30/Aug/2016" Then

            '    reportpath = "Report/PD_CertificateLS.rpt"
            'ElseIf Convert.ToDateTime(eventdate) <= "30/Sep/2018" Then
            '    reportpath = "Report/PD_Certificate_TELLAL_2017.rpt"
            'Else

            'End If
            ''ends here 

            reportpath = "Reports/PD_Certificate_TELLAL.rpt"

            Dim param As New Hashtable
            param.Add("@CR_ID", CR_ID) 
            param.Add("@EMP_ID", 0)
            param.Add("@User_ID", 0)


            Dim rptClass As New rptClassTellal
            With rptClass
                .crDatabase = "TELLAL_PD"
                .reportParameters = param
                .reportPath = Server.MapPath(reportpath)
            End With

            Dim rptDownload As New ReportDownloadTellal
            rptDownload.LoadReports_Tellal(rptClass, rs)
            rptDownload = Nothing


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim AcyID As String
            AcyID = ddlAcdYear.SelectedValue
            bindCourseList(AcyID)

        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim ds As DataSet
            Dim DT As DataTable

            If ddlCourse.SelectedItem.Value <> "0" Then
                bindParticipantsGrid(Convert.ToInt32(ddlCourse.SelectedItem.Value))
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipantList.DataSource = ds
                gvParticipantList.DataBind()
                Dim columnCount As Integer = gvParticipantList.Rows(0).Cells.Count
                gvParticipantList.Rows(0).Cells.Clear()
                gvParticipantList.Rows(0).Cells.Add(New TableCell)
                gvParticipantList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipantList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipantList.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvParticipantList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipantList.PageIndex = e.NewPageIndex
        If ddlCourse.SelectedItem.Value <> "0" Then
            bindParticipantsGrid(Convert.ToInt32(ddlCourse.SelectedItem.Value))
        End If
    End Sub
    Function GetEventDate(ByVal crId As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim str_Sql As String
            Dim eventDate As String = String.Empty

            str_Sql = " SELECT CM_EVENT_DT FROM dbo.COURSE_M WHERE CM_ID IN (SELECT CR_CM_ID FROM COURSE_REQ WHERE CR_ID=" & crId & ")"
            eventDate = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
            Return eventDate
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Function
    Private Sub bindParticipantsGrid(ByVal CM_ID As Integer)

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim param(2) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        param(0) = New SqlClient.SqlParameter("@CM_ID", CM_ID)

        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_PARTICIPANTS_FOR_CERTIFICATE", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.AcceptChanges()
                ds.Tables(0).Columns.Add("COMBINED_DATE", GetType(String))
                gvParticipantList.DataSource = ds
                gvParticipantList.DataBind()
                Dim columnCount As Integer = gvParticipantList.Rows(0).Cells.Count
                gvParticipantList.Rows(0).Cells.Clear()
                gvParticipantList.Rows(0).Cells.Add(New TableCell)
                gvParticipantList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipantList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipantList.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
            Else
                ds.Tables(0).Columns.Add("COMBINED_DATE", GetType(String))
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows


                    row.Item("COMBINED_DATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("CM_EVENT_DT")) & " to " & String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("CM_EVENT_END_DT"))


                    'row.Item("Sr_No") = i + 1
                    i += 1
                Next
                ds.AcceptChanges()

                gvParticipantList.DataSource = ds
                gvParticipantList.DataBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try



    End Sub
     
    Sub bindAcademicYearFilter()
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_ACADEMIC_YEARS")
            If ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ddlAcdYear.DataSource = ds
                ddlAcdYear.DataTextField = "ACY_DESCR"
                ddlAcdYear.DataValueField = "ACY_ID"
                ddlAcdYear.DataBind()
                ''  ddlAcdYear.Items.Insert(0, New ListItem("-All-", "0"))
                ddlAcdYear_SelectedIndexChanged(0, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub bindCourseList(ByVal AcyID As String)
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim txtSearch As New TextBox

        Dim param(5) As SqlClient.SqlParameter
        Try


            param(0) = New SqlClient.SqlParameter("@ACY_ID", AcyID)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_FINISHED_COURSE_BY_ACADEMIC_YEAR", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCourse.DataSource = ds
                ddlCourse.DataTextField = "CM_TITLE"
                ddlCourse.DataValueField = "CM_ID"
                ddlCourse.DataBind()
                ddlCourse.Items.Insert(0, New ListItem("-Please Select-", "0"))

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Function Check_PD_SuperUser() As Boolean
        Dim IsAdmin As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EmpId", Session("EmployeeId"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_Super_User_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAdmin = True
            End If

        End If
        Return IsAdmin
    End Function
End Class
