﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_PD_Token_View.aspx.vb" Inherits="Tellal_PD_Token_View" %>


<%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameAddPDC").fancybox({
                type: 'iframe',
                fitToView: false,
                width: '65%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    parent.location.reload(true);
                }
            });
        });
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Token Generation
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" width="100%">
                    <tr id="trLabelError">
                        <td align="left" class="matters" valign="bottom">
                            <div id="lblError" runat="server" >
                            </div>
                        </td>
                    </tr>
                    <tr id="trAdd">
                        <td align="left"> 
                            <a id="frameAdd" class="frameAddPDC" href="Tellal_PD_Token_Add.aspx?BSU_ID=0&&CM_ID=0">ADD NEW</a> 
                        </td>
                    </tr>
                    <tr id="trGridv" runat="server">
                        <td align="center">

                            <asp:GridView ID="gvPDTrainsers" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="10" Width="100%" OnPageIndexChanging="gvPDTrainsers_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("RowNum") %>'></asp:Label>
                                            <asp:Label ID="hdnBSU_ID" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                                            <asp:Label ID="hdnCM_ID" runat="server" Text='<%# Bind("CM_ID") %>'></asp:Label>
                                           <%-- <asp:HiddenField ID="hdnBSU_ID" runat="server" Value ='<%# Bind("BSU_ID") %>'/>
                                            <asp:HiddenField ID="hdnCM_ID" runat="server"  Value ='<%# Bind("CM_ID") %>'/>--%>
                                        </ItemTemplate> 
                                        <ItemStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU Name">
                                        <HeaderTemplate>
                                            <span class="field-label">BSU NAME
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtBSU_Name" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchBSU_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchBSU_Name_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFullName" runat="server" Text='<%# bind("BSU_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText=" COURSE TITLE">
                                        <HeaderTemplate>
                                            <span class="field-label">COURSE TITLE
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtCM_TITLE" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchCM_TITLE" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchCM_TITLE_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCM_TITLE" runat="server" Text='<%# Bind("CM_TITLE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No_Of_Tokens">
                                        <HeaderTemplate>
                                            <span class="field-label">NO OF TOKENS
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtNo_Of_Tokens" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchNo_Of_Tokens" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchNo_Of_Tokens_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNo_Of_Tokens" runat="server" Text='<%# Bind("No_Of_Tokens")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     

                                    <asp:TemplateField HeaderText="#" ShowHeader="False">
                                        <ItemTemplate> 
                                           <%-- <asp:HyperLink ID="lnkEdit" class="frameAddPDC" runat="server" NavigateUrl="~/PDTellal/Tellal_PD_Trainer_Add.aspx">Edit</asp:HyperLink>--%>
                                             <asp:LinkButton ID="lbtnDownload" runat="server" CausesValidation="false" Text="Download"
                                                OnClick="lbtnDownload_Click"></asp:LinkButton>
                                             </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


