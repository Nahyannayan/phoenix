﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Tellal_PD_Calendar
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Shared dtPD_Calendar As DataTable
    Public Shared cnt As Integer
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ''ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                

                   
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ''load cal year

                Dim calyear As Integer = System.DateTime.Now.Year

                'ddlyear.ClearSelection()
                'For i As Integer = 0 To 100
                '    ddlyear.Items.Add((calyear - i).ToString())
                'Next
                RadMonthYearPicker1.SelectedDate = Date.Now

                Dim calmonth As Integer = System.DateTime.Now.Month
                'ddlmonth.Items.FindByValue(calmonth).Selected = True
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page
                calendarPD.VisibleDate = DateTime.Today
                calendarPD.SelectedDate = DateTime.Today
                dtPD_Calendar = BindPD_Calendar_Events()


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ' Dim dtDate As New DateTime(calendarPD.VisibleDate)
        If CheckBox1.Checked = False Then
            RadMonthYearPicker1.SelectedDate = calendarPD.VisibleDate
        End If

    End Sub
    Function BindPD_Calendar_Events() As DataTable
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim param(1) As SqlClient.SqlParameter

        Dim Dt As New DataTable

        param(0) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[Get_Calendar_Events_CAL_VIEW]", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dt = ds.Tables(0)
        Else
            Dt = Nothing
        End If
        Return Dt
    End Function
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    'Protected Sub calendarPD_DayRender(ByVal sender As Object, ByVal e As DayRenderEventArgs)

    '    Dim dtDate As New DateTime(e.Day.[Date].Year, e.Day.[Date].Month, e.Day.[Date].Day)

    '    Dim expression As String = "CM_EVENT_DT >= '" & dtDate & "' AND CM_EVENT_DT <= '" & dtDate & "'"

    '    If dtPD_Calendar IsNot Nothing AndAlso dtPD_Calendar.Rows.Count > 0 Then
    '        For Each item As DataRow In dtPD_Calendar.Select(expression)
    '            Dim content As String = item("CM_START_TIME").ToString() & " - " & item("CM_END_TIME").ToString() & ":- " & item("CM_TITLE").ToString()


    '            'If item("CR_EMP_ID").ToString() <> 0 AndAlso item("CR_APPR_STATUS").ToString() <> "C" Then
    '            '    e.Cell.Controls.Add(New LiteralControl("<br /><a href=""#"" alt=""already registered"" style=""text-decoration:none;color:white;min-width:100px;font-family:century gothic;"" onclick=""GoToPage('" & item("CM_ID") & "')""> <b>" & content & "</b></a>"))
    '            '    e.Cell.CssClass = "calendarDays"

    '            'Else
    '            e.Cell.Controls.Add(New LiteralControl("<br /><a href=""#"" style=""text-decoration:none;color:white;min-width:100px;font-family:century gothic;"" onclick=""GoToPage('" & item("CM_ID") & "')""> <b>" & content & "</b></a><br/>"))

    '            e.Cell.CssClass = "calendarDays"
    '            'End If


    '        Next item
    '    End If


    'End Sub


    Protected Sub calendarPD_DayRender(ByVal sender As Object, ByVal e As DayRenderEventArgs)

        Dim dtDate As New DateTime(e.Day.[Date].Year, e.Day.[Date].Month, e.Day.[Date].Day)

        Dim expression As String = "CM_EVENT_DT >= '" & dtDate & "' AND CM_EVENT_DT <= '" & dtDate & "'"

        If dtPD_Calendar IsNot Nothing AndAlso dtPD_Calendar.Rows.Count > 0 Then
            For Each item As DataRow In dtPD_Calendar.Select(expression)
                ' Dim content As String = item("CM_START_TIME").ToString() & " - " & item("CM_END_TIME").ToString() & ":- " & item("CM_TITLE").ToString()

                Dim strBuilder As StringBuilder = New StringBuilder()

                Dim CM_TITLE As String = item("CM_TITLE").ToString()
                strBuilder.Append(" <div class=""cal-title""> " + CM_TITLE + "</div>")
                strBuilder.Append(Environment.NewLine)
                Dim trners As String = item("Trainers").ToString()

                strBuilder.Append(" <div class=""cal-trainer""> " + trners + "</div>")

                Dim eventTime As String = item("CM_START_TIME").ToString() & " - " & item("CM_END_TIME").ToString()


                strBuilder.Append(Environment.NewLine)
                ' strBuilder.Append()
                'strBuilder.Append("<br/>")
                strBuilder.Append("<div class=""cal-event"">" + eventTime + "</div>")
                ' strBuilder.Append("<br/>")
                strBuilder.Append(Environment.NewLine)
                Dim desc As String = Convert.ToString(item("CM_DESCR"))
                If desc.Length > 80 Then
                    desc = desc.Substring(0, 79) + "...."
                End If
                'strBuilder.Append(desc)
                ''commented by nahyan 23jun2019
                ''    strBuilder.Append("<div class=""cal-desc""><p>" + desc + "</p></div>")


                'If item("CR_EMP_ID").ToString() <> 0 AndAlso item("CR_APPR_STATUS").ToString() <> "C" Then
                '    e.Cell.Controls.Add(New LiteralControl("<br /><a href=""#"" alt=""already registered"" style=""text-decoration:none;color:white;min-width:100px;font-family:century gothic;"" onclick=""GoToPage('" & item("CM_ID") & "')""> <b>" & content & "</b></a>"))
                '    e.Cell.CssClass = "calendarDays"

                'Else


                Dim cm_id As String = item("CM_ID")
                e.Cell.Controls.Add(New LiteralControl("<br /><a href=""#"" style=""text-decoration:none;color:gray;min-width:100px;font-family:calibri;"" onclick=""GoToPage('" & cm_id & "')""> <b>" & Server.HtmlDecode(strBuilder.ToString()) & "</b></a><br/>"))
                e.Cell.CssClass = "calendarDays"
                'End If


            Next item
        End If


    End Sub
    Protected Sub calendarPD_VisibleMonthChanged(ByVal sender As Object, ByVal e As MonthChangedEventArgs)
        Dim str As String = e.ToString
    End Sub
    Protected Sub calendarPD_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        Dim str As String = e.ToString
    End Sub
    'Protected Sub lnkBackToESS_Click(sender As Object, e As EventArgs)
    '    Response.Redirect("~/ESSDashboard.aspx", False)
    '    Exit Sub
    'End Sub
    'Protected Sub ddlmonth_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    calendarPD.VisibleDate = New DateTime(Convert.ToInt32(ddlyear.SelectedValue), Convert.ToInt32(ddlmonth.SelectedValue), 1)
    '    BindListView()
    'End Sub
    'Protected Sub ddlyear_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    calendarPD.VisibleDate = New DateTime(Convert.ToInt32(ddlyear.SelectedValue), Convert.ToInt32(ddlmonth.SelectedValue), 1)
    '    BindListView()
    'End Sub


    Protected Sub Group1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        ShowDetails()
    End Sub

    Protected Sub RadMonthYearPicker1_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles RadMonthYearPicker1.SelectedDateChanged
        ShowDetails()
    End Sub

    Private Sub ShowDetails()
        If CheckBox1.Checked Then
            divCalenderView.Visible = False
            divListView.Visible = True
            txtSearch.Visible = True
            imgSearch.Visible = True
            BindListView()

        Else
            divCalenderView.Visible = True
            divListView.Visible = False
            If RadMonthYearPicker1.SelectedDate IsNot Nothing Then
                calendarPD.VisibleDate = RadMonthYearPicker1.SelectedDate
            End If
            txtSearch.Visible = False
            imgSearch.Visible = False
        End If

    End Sub

    Private Sub BindListView()

        'Dim now As New DateTime(ddlyear.SelectedItem.Value, ddlmonth.SelectedItem.Value, 1)
        Dim now As New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        If RadMonthYearPicker1.SelectedDate IsNot Nothing Then
            now = RadMonthYearPicker1.SelectedDate.Value
        Else
            RadMonthYearPicker1.SelectedDate = now
        End If



        If dtPD_Calendar IsNot Nothing Then

            Dim thisMonthRows = dtPD_Calendar.AsEnumerable().Where(Function(r) r.Field(Of DateTime)("CM_EVENT_DT").Year = now.Year AndAlso r.Field(Of DateTime)("CM_EVENT_DT").Month = now.Month AndAlso (r.Field(Of String)("CM_Title").Contains(txtSearch.Text.Trim) Or txtSearch.Text.Trim.Length = 0))
            Dim table As DataTable = dtPD_Calendar.Clone()
            If thisMonthRows IsNot Nothing AndAlso DirectCast((thisMonthRows.AsDataView), System.Data.DataView).Count > 0 Then
                table = thisMonthRows.CopyToDataTable()
                lblMsg.Visible = False
            Else
                table = Nothing
                'table.Rows.Add(table.NewRow())
                lblMsg.Visible = True
            End If
            RepterDetails.DataSource = table
            RepterDetails.DataBind()
        End If


    End Sub

    Protected Sub RepterDetails_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Dim imgCM As Image = e.Item.FindControl("imgCM")

        If cnt > 9 Then
            cnt = 1
        End If
        If imgCM IsNot Nothing Then
            imgCM.ImageUrl = "~/Images/CM/" + cnt.ToString() + ".png"
            cnt = cnt + 1
        End If

        Dim lblTitleRp As Label = e.Item.FindControl("lblDesc")
        If lblTitleRp IsNot Nothing Then
            lblTitleRp.ToolTip = lblTitleRp.Text
            Dim cDescr = lblTitleRp.Text
            cDescr = System.Web.HttpUtility.HtmlEncode(cDescr)
            If cDescr.Length > 80 Then
                ''commented by nahyan 
                '  lblTitleRp.Text = cDescr.Substring(0, 79) + "...."
                ' lblTitleRp.Text = lblTitleRp.Text.Substring(0, 79) + "...."
            End If
        End If

        Dim hyplink As HyperLink = e.Item.FindControl("hyplink")
        'If hyplink IsNot Nothing Then
        '    Dim hdnCM_ID As HiddenField = e.Item.FindControl("hdnCM_ID")
        '    Dim encrypt As New Encryption64

        '    Dim hlnkAddUserurl As String = String.Format("PD_Calendar_Apply_New.aspx?val={0}", encrypt.Encrypt(hdnCM_ID.Value))
        '    hyplink.NavigateUrl = hlnkAddUserurl
        'End If
    End Sub

    Protected Sub imgSearch_Click(sender As Object, e As ImageClickEventArgs) Handles imgSearch.Click
        ShowDetails()
        ' BindListView()
    End Sub
End Class
