﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Web.UI.WebControls

Partial Class Tellal_Course_Add_Update
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")

        'smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' pop_Trainers.OpenerElementID = imgParticipants2.ClientID
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If ViewState("datamode") = "add" Then
                    btnUpdateCourse.Visible = False
                    ViewState("ID") = 0
                ElseIf ViewState("datamode") = "edit" Then
                    btnUpdateCourse.Visible = True
                    btnAddCourse.Visible = False

                    ViewState("ID") = Encr_decrData.Decrypt(Request.QueryString("cmId").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                Me.gvTrainer.DataSource = Nothing
                Me.gvTrainer.DataBind()

                Me.txtMAxCapacity.Text = 30
                Me.txtTime.Text = "9:00 AM"
                Me.txtTimeTo.Text = "4:00 PM"

                BindDepartmentDimension()
                BindCoursetype()
                BindCourseCategory()
                BindCourseEXTCategory()
                BindTrainers()
                ' ShowCheckedItems(cmbTrainer, itemsClientSide)
                BindRegion()
                BindLocation()
                If ViewState("ID") > 0 Then
                    GetCourseDetails()
                End If


                'calling pageright class to get the access rights

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub btnAddCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCourse.Click
        ViewState("ID") = 0

        btnUpdateCourse.Visible = False
        Dim errormsg As String = String.Empty

        Dim stDate As Date, EnDate As Date
        If txtFrom.Text <> "" AndAlso txtTodate.Text <> "" Then
            If IsDate(txtFrom.Text) AndAlso IsDate(txtTodate.Text) Then
                stDate = txtFrom.Text
                EnDate = txtTodate.Text

                If stDate > EnDate Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>Start Date should not be greater than End Date.</div>"
                    Exit Sub
                ElseIf EnDate < stDate Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>End Date must be greater than start date</div>"
                    Exit Sub
                End If
            End If
        End If

        If Is_code_Exists() Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>Course Code already exists.</div>"
        Else
            If callTransaction(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"

            Else

                'lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Record saved successfully !!!</div>"

                Dim url As String
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                'define the datamode to view if view is clicked
                ViewState("datamode") = "view"
                'Encrypt the data that needs to be send through Query String
                Dim msg As String
                msg = "1"
                msg = Encr_decrData.Encrypt(msg)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                url = String.Format("~\PDTellal\Tellal_PD_Course_View.aspx?MainMnu_code={0}&datamode={1}&msg={2}", ViewState("MainMnu_code"), ViewState("datamode"), msg)
                Response.Redirect(url)


            End If
        End If

    End Sub

    Protected Sub btnUpdateCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCourse.Click


        btnAddCourse.Visible = False
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"

        Else

            'lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Record saved successfully !!!</div>"

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            Dim msg As String
            msg = "1"
            msg = Encr_decrData.Encrypt(msg)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PDTellal\Tellal_PD_Course_View.aspx?MainMnu_code={0}&datamode={1}&msg={2}", ViewState("MainMnu_code"), ViewState("datamode"), msg)
            Response.Redirect(url)


        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PDTellal\Tellal_PD_Course_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlRegion1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion1.SelectedIndexChanged
        BindLocation()
    End Sub


    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("ID")

        Using CONN As SqlConnection = ConnectionManger.Get_TellalPD_Connection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(30) As SqlParameter
                Dim RegionLoc As String = ""
                Dim LocStatus = ""
                param(0) = New SqlParameter("@CM_ID", CM_ID)
                param(1) = New SqlParameter("@title", txtTitle.Text)
                param(2) = New SqlParameter("@FromDate", Convert.ToDateTime(txtFrom.Text.Trim))
                param(3) = New SqlParameter("@StartTime", txtTime.Text.Trim)
                param(4) = New SqlParameter("@EndTime", txtTimeTo.Text.Trim)
                param(5) = New SqlParameter("@MaxCapacity", txtMAxCapacity.Text.Trim)
                param(6) = New SqlParameter("@OpenDate", Convert.ToDateTime(txtOnlineDate.Text.Trim))
                param(7) = New SqlParameter("@CloseDate", Convert.ToDateTime(txtCloseDate.Text.Trim))
                param(8) = New SqlParameter("@Description", Server.HtmlEncode(txtDesc.Content))

                param(9) = New SqlParameter("@Details", txtDetails.Text.Trim)
                param(10) = New SqlParameter("@EndDate", txtTodate.Text.Trim)
                param(11) = New SqlParameter("@Code", txtCode.Text.Trim)
                If ddlRegion1.SelectedValue <> 0 Then
                    RegionLoc = ddlRegion1.SelectedValue

                    If ddlLocation1.SelectedValue <> 0 Then
                        RegionLoc = RegionLoc & "|" & ddlLocation1.SelectedValue
                        LocStatus = ddlStatus1.SelectedValue & "|" & ddlLocation1.SelectedValue
                    End If
                End If

                param(12) = New SqlParameter("@RegionLocation", RegionLoc)
                param(13) = New SqlParameter("@LocationStatus", LocStatus)
                param(14) = New SqlParameter("@CreatedBy", Session("sUsr_id"))

                param(15) = New SqlParameter("@CourseValue", txtCourseValue.Text)
                param(16) = New SqlParameter("@CourseComments", txtValueComments.Text)
                param(19) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(19).Direction = ParameterDirection.ReturnValue

                Dim tmpstr As String = ""
                'For i As Integer = 0 To Me.chkTrainers.Items.Count - 1
                '    If chkTrainers.Items(i).Selected = True Then
                '        tmpstr &= chkTrainers.Items(i).Value & "|"
                '    End If
                'Next
                For i As Integer = 0 To Me.gvTrainer.Rows.Count - 1
                    tmpstr &= CType(Me.gvTrainer.Rows(i).FindControl("lblID"), Label).Text & "|"
                Next

                param(17) = New SqlParameter("@TRAINER_IDS", tmpstr)
                param(18) = New SqlParameter("@CourseType", Convert.ToInt32(ddlCourseType.SelectedValue))
                'param(20) = New SqlParameter("@CM_Course_Category_ID", Convert.ToInt16(ddlCategory.SelectedValue))


                param(20) = New SqlParameter("@IsForGEMSOnly", CheckCategorySelect(1))
                param(21) = New SqlParameter("@IsForNonGEMSIndivisuals", CheckCategorySelect(2))
                param(22) = New SqlParameter("@IsForNonGEMSScholl", CheckCategorySelect(3))
                param(23) = New SqlParameter("@CM_Department_Dimension_ID", DDLDepartment_Dimension.SelectedValue)
                Dim tmpstrEXT As String = ""
                For i As Integer = 0 To Me.ddlCourseExtCategory.Items.Count - 1
                    If ddlCourseExtCategory.Items(i).Checked = True Then
                        tmpstrEXT &= ddlCourseExtCategory.Items(i).Value & "|"
                    End If
                Next
                param(24) = New SqlParameter("@EXT_Course_IDs", tmpstrEXT)
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_T_SAVE_COURSE_M", param)
                Dim ReturnFlag As Integer = param(19).Value


                If CM_ID > 0 Then
                    If ddlStatus1.SelectedValue = "C" Then

                        If ReturnFlag = 0 Then
                            Dim params1(1) As SqlParameter
                            params1(0) = New SqlParameter("@CM_ID", CM_ID)

                            '  SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_T.Send_Course_Cancel_Email", params1)
                        End If

                    End If
                End If





                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"

                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function CheckCategorySelect(ByVal val As String) As Boolean
        Dim result As Boolean = False
        Dim collection As IList(Of RadComboBoxItem) = ddlCategory.CheckedItems
        For Each item As RadComboBoxItem In collection
            If item.Value = val Then
                result = True
                Exit For
            End If
        Next
        Return result
    End Function
    Sub BindTrainers()


        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetTrainers]")
        If Not ds Is Nothing Then
            Me.chkTrainers.DataValueField = "Id"
            Me.chkTrainers.DataTextField = "FullName"
            Me.chkTrainers.DataSource = ds
            Me.chkTrainers.DataBind()
        End If

    End Sub
   
    Private Function FillTrainerNames(ByVal TrainerIDs As String) As Boolean

        Dim IDs As String() = TrainerIDs.Split(",")
        Dim condition As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()
        Dim str_Sql As String
        Dim ds As DataSet, dt As DataTable
        Dim param(1) As SqlClient.SqlParameter
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next

        If TrainerIDs <> "" Then
            param(0) = New SqlParameter("@IDs", TrainerIDs)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetTrainers_New]", param)


            gvTrainer.DataSource = ds
            Session("PD_Add_Course_Trainers") = ds
            gvTrainer.DataBind()

            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
        Else
            Return False
        End If



        Return True
    End Function
    Protected Sub txtTrainerIDs_TextChanged(sender As Object, e As EventArgs) Handles txtTrainerIDs.TextChanged
        txtTrainerIDs.Text = ""
        If h_TrainerId.Value <> "" Then
            gvTrainer.Visible = True
            FillTrainerNames(h_TrainerId.Value)
        Else
            gvTrainer.Visible = False
        End If
    End Sub
    Protected Sub imgGetTrainers_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        ' Me.FillTrainerNames(Me.h_TrainerId.Value)
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        Dim dt As DataTable, rowtoremove As DataRow
        lblEMPID = TryCast(sender.FindControl("lblID"), Label)
        If Not lblEMPID Is Nothing Then
            h_TrainerId.Value = h_TrainerId.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            '    gvEMPName.PageIndex = gvEMPName.PageIndex
            '    FillEmpNames(h_EMPID.Value)

            If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
                dt = Session("dtTaskEscalationLevelsMasterEmployee")
                If dt.Select("ID='" & lblEMPID.Text & "'").Length > 0 Then
                    rowtoremove = dt.Select("ID='" & lblEMPID.Text & "'")(0)
                    dt.Rows.Remove(rowtoremove)
                    dt.AcceptChanges()
                    Session("dtTaskEscalationLevelsMasterEmployee") = dt
                    h_TrainerId.Value = ""
                    For Each row As DataRow In dt.Rows
                        h_TrainerId.Value &= row.Item("ID") & "|"
                    Next
                    If h_TrainerId.Value.EndsWith("|") Then
                        h_TrainerId.Value.TrimEnd("|")
                    End If
                    gvTrainer.DataSource = dt
                    gvTrainer.DataBind()
                End If
            End If
        End If
    End Sub

    Protected Sub gvEMPName_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTrainer.RowDeleting
        Try
            Dim id As Integer = CType(Me.gvTrainer.Rows(e.RowIndex).FindControl("lblId"), Label).Text
            Dim ds As DataSet = Session("PD_Add_Course_Trainers")
            If Not ds Is Nothing Then
                ds.Tables(0).Rows.Remove(ds.Tables(0).Select("Id = " & id)(0))
                ds.AcceptChanges()
                Me.gvTrainer.DataSource = ds
                Session("PD_Add_Course_Trainers") = ds
                Dim trainerID As String = h_TrainerId.Value
                Dim newsd As String = String.Empty
                Dim newsd1 As String = String.Empty
                If trainerID <> "" Then
                    newsd = trainerID.Replace(id, "")
                    newsd1 = newsd.Replace(", ,", ",")
                    newsd1.TrimEnd(", ")
                    h_TrainerId.Value = newsd1.TrimEnd()
                    h_TrainerId.Value = newsd1.TrimStart()

                    If h_TrainerId.Value.EndsWith(",") Then
                        Dim subE As String = h_TrainerId.Value.Substring(0, h_TrainerId.Value.Length - 1)

                        h_TrainerId.Value = subE

                    End If
                    If h_TrainerId.Value.StartsWith(",") Then
                        Dim SubS = h_TrainerId.Value.Substring(1, h_TrainerId.Value.Length - 1)
                        h_TrainerId.Value = SubS
                    End If

                End If
                Me.gvTrainer.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvEMPName_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles gvTrainer.SelectedIndexChanged

    End Sub

    Function Is_code_Exists() As Boolean
        Dim IsCodeEx As Boolean = False
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@Code", txtCode.Text)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[Get_Course_Details_By_Code]", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)
            IsCodeEx = True
        End If
        Return IsCodeEx
    End Function

    Sub BindDepartmentDimension()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetDepartment_Dimension]")
        If Not ds Is Nothing Then
            DDLDepartment_Dimension.Items.Clear()
            DDLDepartment_Dimension.DataSource = ds
            DDLDepartment_Dimension.DataTextField = "Desc"
            DDLDepartment_Dimension.DataValueField = "id"
            DDLDepartment_Dimension.DataBind()
        End If
    End Sub


    Sub BindCoursetype()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetCourseType]")
        If Not ds Is Nothing Then
            ddlCourseType.Items.Clear()
            ddlCourseType.DataSource = ds
            ddlCourseType.DataTextField = "COURSE_TYPE_DESC"
            ddlCourseType.DataValueField = "COURSE_TYPE_ID"
            ddlCourseType.DataBind()
        End If
    End Sub
    Sub BindCourseCategory()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetCourseCategory]")
        If Not ds Is Nothing Then
            ddlCategory.Items.Clear()
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "Category_Desc"
            ddlCategory.DataValueField = "Id"
            ddlCategory.DataBind()
        End If
    End Sub

    Sub BindCourseEXTCategory()
        Try

            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetCourseExtCategory]")

            If Not ds Is Nothing Then
                Dim dt As DataTable = ds.Tables(0)
                ddlCourseExtCategory.Items.Clear()
                ddlCourseExtCategory.DataSource = dt
                ddlCourseExtCategory.DataTextField = "EXT_CAT_DESC"
                ddlCourseExtCategory.DataValueField = "Id"
                ddlCourseExtCategory.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Sub BindRegion()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetRegion]")
        If Not ds Is Nothing Then
            ddlRegion1.Items.Clear()
            ddlRegion1.DataSource = ds
            ddlRegion1.DataTextField = "R_REGION_NAME"
            ddlRegion1.DataValueField = "R_ID"
            ddlRegion1.DataBind()
        End If
    End Sub
    Sub BindLocation()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetLocation]")
        If Not ds Is Nothing Then
            ddlLocation1.Items.Clear()
            ddlLocation1.DataSource = ds
            ddlLocation1.DataTextField = "L_DESCR"
            ddlLocation1.DataValueField = "L_ID"
            ddlLocation1.DataBind()
        End If
    End Sub

    Sub BindTrainer_old()

        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetTrainers]")
        If Not ds Is Nothing Then
            'cmbTrainer.Items.Clear()
            'cmbTrainer.DataSource = ds
            'cmbTrainer.DataTextField = "FullName"
            'cmbTrainer.DataValueField = "ID"
            'cmbTrainer.DataBind()
        End If
    End Sub



    Private Sub GetCourseDetails()
        Try
            If Convert.ToInt64(ViewState("ID")) > 0 Then

                Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
                Dim param(2) As SqlClient.SqlParameter
                Dim ds As DataSet
                Dim DT As DataTable
                param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("ID"))
                param(1) = New SqlClient.SqlParameter("@EMP_ID", 0)

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Course_Info_By_Id", param)

                If ds.Tables(0).Rows.Count > 0 Then
                    DT = ds.Tables(0)
                    txtCode.Text = Convert.ToString(DT.Rows(0)("CM_Code"))
                    txtTitle.Text = Convert.ToString(DT.Rows(0)("CM_TITLE"))
                    ' ddlCourseType.SelectedValue = Convert.ToString(DT.Rows(0)("CM_COURSE_TYPE_ID"))

                    txtFrom.Text = Convert.ToString(DT.Rows(0)("CM_EVENT_DT"))
                    txtTodate.Text = Convert.ToString(DT.Rows(0)("CM_EVENT_END_DT"))
                    txtTime.Text = Convert.ToString(DT.Rows(0)("CM_START_TIME"))

                    txtTimeTo.Text = Convert.ToString(DT.Rows(0)("CM_END_TIME"))
                    txtMAxCapacity.Text = Convert.ToString(DT.Rows(0)("CM_MAX_CAPACITY"))
                    txtOnlineDate.Text = Convert.ToString(DT.Rows(0)("CM_OPEN_ONLINE_DT"))


                    txtCloseDate.Text = Convert.ToString(DT.Rows(0)("CM_CLOSE_ONLINE_DT"))
                    txtCourseValue.Text = Convert.ToString(DT.Rows(0)("CM_Course_value"))
                    txtValueComments.Text = Convert.ToString(DT.Rows(0)("CM_COURSE_COMEMNTS"))

                    txtDetails.Text = Convert.ToString(DT.Rows(0)("CM_Details"))
                    txtDesc.Content = Server.HtmlDecode(Convert.ToString(DT.Rows(0)("CM_DESCR")))
                    ddlRegion1.SelectedValue = Convert.ToString(DT.Rows(0)("CS_R_ID"))
                    ddlLocation1.SelectedValue = Convert.ToString(DT.Rows(0)("CS_L_ID"))
                    ddlStatus1.SelectedValue = Convert.ToString(DT.Rows(0)("CS_STATUS"))

                    If Convert.ToInt32(DT.Rows(0)("CM_Department_Dimension_ID")) > 0 Then
                        DDLDepartment_Dimension.SelectedValue = Convert.ToString(DT.Rows(0)("CM_Department_Dimension_ID"))
                    End If

                    If Convert.ToInt32(DT.Rows(0)("CM_COURSE_TYPE_ID")) > 0 Then
                        ddlCourseType.SelectedValue = Convert.ToString(DT.Rows(0)("CM_COURSE_TYPE_ID"))
                    End If


                    If Convert.ToString(DT.Rows(0)("IsForGEMSOnly")) = "True" Then
                        Dim item As RadComboBoxItem = ddlCategory.FindItemByValue("1")
                        If Not item Is Nothing Then
                            item.Checked = True
                        End If
                    End If

                    If Convert.ToString(DT.Rows(0)("IsForNonGEMSIndivisuals")) = "True" Then
                        Dim item As RadComboBoxItem = ddlCategory.FindItemByValue("2")
                        If Not item Is Nothing Then
                            item.Checked = True
                        End If
                    End If

                    If Convert.ToString(DT.Rows(0)("IsForNonGEMSScholl")) = "True" Then
                        Dim item As RadComboBoxItem = ddlCategory.FindItemByValue("3")
                        If Not item Is Nothing Then
                            item.Checked = True
                        End If
                    End If


                    If Convert.ToString(DT.Rows(0)("Course_Category")).Length > 0 Then

                        Dim str As String() = DT.Rows(0)("Course_Category").ToString.Split(",")
                        For Each id As String In str
                            Dim item As RadComboBoxItem = ddlCourseExtCategory.FindItemByValue(id.Trim)
                            If Not item Is Nothing Then
                                item.Checked = True
                            End If
                            ' If ddlCategory.Items.c Then
                        Next

                    End If
                End If

            End If

            Dim str_conn1 As String = ConnectionManger.GetTellalPDConnectionString
            Dim ds1 As DataSet
            Dim DT1 As DataTable
            Dim param1(2) As SqlClient.SqlParameter
            param1(0) = New SqlClient.SqlParameter("@cm_id", ViewState("ID"))
            ds1 = SqlHelper.ExecuteDataset(str_conn1, CommandType.StoredProcedure, "GetTrainers_by_CourseID", param1)

            If ds1.Tables(0).Rows.Count > 0 Then
                DT1 = ds1.Tables(0)

                Dim tmpstr As String = ""
                For Each row As DataRow In DT1.Rows
                    'If Not Me.chkTrainers.Items.FindByValue(row.Item("Id")) Is Nothing Then
                    '    Me.chkTrainers.Items.FindByValue(row.Item("Id")).Selected = True
                    'End If
                    tmpstr &= row.Item("Trainer_ID") & ","
                Next
                If tmpstr.EndsWith(",") Then
                    tmpstr = tmpstr.TrimEnd(",")
                End If

                h_TrainerId.Value = tmpstr
                Me.FillTrainerNames(tmpstr)

            End If



        Catch ex As Exception
            lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub
End Class
