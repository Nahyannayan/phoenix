﻿
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports Telerik.Web.UI

Public Class Tellal_PD_Trainer_Add
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ViewState("ID") = 0
            Dim ID As String = Request.QueryString("Id")
            ViewState("ID") = ID
            ' Fill the continents combo.
            GetCountry_info()
            LoadBSU()
            GetTrainerDetails()
        End If
    End Sub
    

    Protected Sub ddlBSU_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        ddlEmp.Text = ""
        LoadEmployees(e.Value)
        ShowBSU()
    End Sub

    Protected Sub ddlEmp_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

        Dim adapter As New SqlDataAdapter("select * from employees where emp_id=@emp_id", connection)
        adapter.SelectCommand.Parameters.AddWithValue("@emp_id", ddlEmp.SelectedItem.Value)

        Dim dt As New DataTable()
        adapter.Fill(dt)


        If dt.Rows.Count > 0 Then
            txtFName.Text = dt.Rows(0)("EMP_FNAME").ToString()
            txtMName.Text = dt.Rows(0)("EMP_MNAME").ToString()
            txtLastName.Text = dt.Rows(0)("EMP_LNAME").ToString()
        End If

        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "Emp_id"
        'ddlEmp.DataSource = dt
        'ddlEmp.DataBind()
    End Sub

    Protected Sub Group1_CheckedChanged(sender As Object, e As EventArgs) Handles rdIsGemsStaff.CheckedChanged
        ShowBSU()
    End Sub
    Private Sub ShowBSU()
        If rdIsGemsStaff.Checked Then
            divBSU.Visible = True
        Else
            divBSU.Visible = False
        End If
    End Sub
    Protected Sub LoadBSU()
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

        Dim adapter As New SqlDataAdapter("Select BSU_ID,BSU_Name from [dbo].[vw_All_Valid_BSU ] order by bsu_name", connection)
        Dim dt As New DataTable()
        adapter.Fill(dt)

        ddlBSU.DataTextField = "BSU_Name"
        ddlBSU.DataValueField = "BSU_ID"
        ddlBSU.DataSource = dt
        ddlBSU.DataBind()
        ' Insert the first item.
        ddlBSU.Items.Insert(0, New RadComboBoxItem("- Select a BSU -"))
        ddlBSU.Filter = DirectCast(Convert.ToInt32(1), RadComboBoxFilter)
        ddlEmp.Filter = DirectCast(Convert.ToInt32(1), RadComboBoxFilter)
    End Sub

    Protected Sub LoadEmployees(ByVal BSU_ID As String)
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

        Dim adapter As New SqlDataAdapter("select Emp_id,EMP_FNAME+' '+ EMP_LNAME as Name from employees where emp_bsu_id=@emp_bsu_id", connection)
        adapter.SelectCommand.Parameters.AddWithValue("@emp_bsu_id", BSU_ID)

        Dim dt As New DataTable()
        adapter.Fill(dt)

        ddlEmp.DataTextField = "Name"
        ddlEmp.DataValueField = "Emp_id"
        ddlEmp.DataSource = dt
        ddlEmp.DataBind()
    End Sub

    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If validateForm() Then


            Dim errormsg As String = String.Empty
            If callTransaction(errormsg) <> 0 Then
                lblResult.Text = "<div class='error'>" & errormsg & "</div>"
            Else  
                lblResult.Text = "<div class='alert alert-success'>Record saved successfully !!!</div>" 
            End If

        End If
    End Sub


    Private Function callTransaction(ByRef errormsg As String) As Integer
        Dim tran As SqlTransaction

        Using CONN As SqlConnection = ConnectionManger.Get_TellalPD_Connection

            tran = CONN.BeginTransaction("SampleTransaction")
            Dim EMP_IDs As String = String.Empty
            Dim IsNewGroup As Boolean = False

            Dim ReturnFlag As Integer
            Dim pParms(16) As SqlClient.SqlParameter
            Try
                 
                pParms(0) = New SqlClient.SqlParameter("@ID", ViewState("ID"))
                'pParms(0).Direction = ParameterDirection.Output

                pParms(1) = New SqlClient.SqlParameter("@FName", txtFName.Text.Trim())
                pParms(2) = New SqlClient.SqlParameter("@MName", txtMName.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@LName", txtLastName.Text.Trim())

                pParms(4) = New SqlClient.SqlParameter("@IsGemsStaff", (If(rdIsGemsStaff.Checked, 1, 0)))
                pParms(5) = New SqlClient.SqlParameter("@BSU_ID", (If(rdIsGemsStaff.Checked, ddlBSU.SelectedItem.Value, 0)))
                pParms(6) = New SqlClient.SqlParameter("@Emp_id", (If(rdIsGemsStaff.Checked, ddlEmp.SelectedItem.Value, 0)))

                pParms(7) = New SqlClient.SqlParameter("@IsMale", ddlGender.SelectedItem.Value)
                pParms(8) = New SqlClient.SqlParameter("@Email", txtEmail.Text.Trim())
                pParms(9) = New SqlClient.SqlParameter("@Mobile", txtMobile.Text.Trim())

                pParms(10) = New SqlClient.SqlParameter("@City", ddlCity.SelectedValue)
                pParms(11) = New SqlClient.SqlParameter("@State", String.Empty)
                pParms(12) = New SqlClient.SqlParameter("@Country", ddlCountry.SelectedValue)
                

                pParms(13) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                pParms(13).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[dbo].[Insert_Trainer_M]", pParms)
                'SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_PD_TRAINERS_BULK", param)

                ReturnFlag = pParms(13).Value
                callTransaction = ReturnFlag
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message

            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()


                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()

                Else
                    errormsg = ""
                    tran.Commit()
                End If

            End Try
        End Using
    End Function
    'Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
    '    If validateForm() Then

    '        Dim str_conn2 As String = ConnectionManger.GetTellalPDConnectionString
    '        Dim pParms(16) As SqlClient.SqlParameter
    '        pParms(0) = New SqlClient.SqlParameter("@ID", ViewState("ID"))
    '        'pParms(0).Direction = ParameterDirection.Output

    '        pParms(1) = New SqlClient.SqlParameter("@FName", txtFName.Text.Trim())
    '        pParms(2) = New SqlClient.SqlParameter("@MName", txtMName.Text.Trim())
    '        pParms(3) = New SqlClient.SqlParameter("@LName", txtLastName.Text.Trim())

    '        pParms(4) = New SqlClient.SqlParameter("@IsGemsStaff", (If(rdIsGemsStaff.Checked, 1, 0)))
    '        pParms(5) = New SqlClient.SqlParameter("@BSU_ID", (If(rdIsGemsStaff.Checked, ddlBSU.SelectedItem.Value, 0)))
    '        pParms(6) = New SqlClient.SqlParameter("@Emp_id", (If(rdIsGemsStaff.Checked, ddlEmp.SelectedItem.Value, 0)))

    '        pParms(7) = New SqlClient.SqlParameter("@IsMale", ddlGender.SelectedItem.Value)
    '        pParms(8) = New SqlClient.SqlParameter("@Email", txtEmail.Text.Trim())
    '        pParms(9) = New SqlClient.SqlParameter("@Mobile", txtMobile.Text.Trim())

    '        pParms(10) = New SqlClient.SqlParameter("@City", txtCity.Text.Trim())
    '        pParms(11) = New SqlClient.SqlParameter("@State", txtState.Text.Trim())
    '        pParms(12) = New SqlClient.SqlParameter("@Country", txtCountry.Text.Trim())

    '        Dim id = SqlHelper.ExecuteNonQuery(str_conn2, CommandType.StoredProcedure, "[dbo].[Insert_Trainer_M]", pParms)
    '        lblResult.Text = "Trainer has been added successfully..."
    '        'Dim url As String
    '        'url = String.Format("~\PDTellal\Tellal_PD_Trainer_View.aspx")
    '        'Response.Redirect(url)
    '    End If
    '    'Response.Redirect("/PD/PD_Course_Registration?CM_ID=" + Convert.ToString(ViewState("CM_ID")))


    'End Sub

    Function validateForm() As Boolean
        Dim result As Boolean = False

        If rdIsGemsStaff.Checked = False And rdIsContractEmp.Checked = False Then
            lblResult.Text = "Please select the Is GEMS staff or other option"
            result = False
            Return result
            Exit Function
        End If

        If rdIsGemsStaff.Checked = True Then
            If ddlBSU.Items.Count >= 0 And ddlEmp.Items.Count >= 0 Then
                If ddlBSU.SelectedValue = "" Or ddlEmp.SelectedValue = "" Then
                    result = False
                    lblResult.Text = "Please Select The School Name or employee"
                    Return result
                    Exit Function
                End If
            ElseIf ddlBSU.Items.Count <= 0 And ddlBSU.Items.Count <= 0 Then
                result = False
                lblResult.Text = "Please Select The School Name or employee"
                Return result
                Exit Function
            End If
        End If

        If txtFName.Text.Trim().Length <= 0 Then
            lblResult.Text = "Please Enter the First Name"
            result = False
            Return result
            Exit Function
        End If

        If txtLastName.Text.Trim().Length <= 0 Then
            lblResult.Text = "Please Enter the Last Name"
            txtLastName.Focus()
            result = False
            Return result
            Exit Function
        End If

        If txtEmail.Text.Trim().Length <= 0 Then
            lblResult.Text = "Please Enter the Email"
            result = False
            Return result
            Exit Function
        End If

        If txtMobile.Text.Trim().Length <= 0 Then
            lblResult.Text = "Please Enter the Mobile"
            result = False
            Return result
            Exit Function
        End If



        result = True
        Return result
    End Function

    Private Sub GetTrainerDetails()
        Try
            If Convert.ToInt64(ViewState("ID")) > 0 Then

                Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
                Dim param(2) As SqlClient.SqlParameter
                Dim ds As DataSet
                Dim DT As DataTable
                param(0) = New SqlClient.SqlParameter("@id", ViewState("ID"))


                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetTrainer_ByID", param)

                If ds.Tables(0).Rows.Count > 0 Then
                    DT = ds.Tables(0)
                    If Convert.ToInt16(DT.Rows(0)("IsGemsStaff")) = 1 Then
                        rdIsGemsStaff.Checked = True
                        ddlBSU.SelectedValue = Convert.ToString(DT.Rows(0)("BSU_ID"))
                        LoadEmployees(ddlBSU.SelectedValue)
                        ddlEmp.SelectedValue = Convert.ToString(DT.Rows(0)("Emp_id"))
                        divBSU.Visible = True
                    Else
                        rdIsContractEmp.Checked = True
                        divBSU.Visible = False
                    End If
                    txtFName.Text = Convert.ToString(DT.Rows(0)("FName"))
                    txtMName.Text = Convert.ToString(DT.Rows(0)("MName"))
                    txtLastName.Text = Convert.ToString(DT.Rows(0)("LName"))

                    txtEmail.Text = Convert.ToString(DT.Rows(0)("Email"))
                    txtMobile.Text = Convert.ToString(DT.Rows(0)("Mobile"))

                    If ddlCity.Items.Contains(New ListItem(Convert.ToString(DT.Rows(0)("City")))) Then
                        ddlCity.SelectedValue = Convert.ToString(DT.Rows(0)("City"))
                    End If
                    If ddlCountry.Items.Contains(New ListItem(Convert.ToString(DT.Rows(0)("Country")))) Then
                        ddlCountry.SelectedValue = Convert.ToString(DT.Rows(0)("Country"))
                    End If


                    ' txtState.Text = Convert.ToString(DT.Rows(0)("State"))
                    'ddlCountry.SelectedValue = Convert.ToString(DT.Rows(0)("Country"))

                    ddlGender.SelectedValue = Convert.ToInt16(DT.Rows(0)("IsMale"))
                End If

            End If
        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Protected Sub GetCountry_info()
        Try
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

            Dim adapter As New SqlDataAdapter("Select CTY_ID,case CTY_ID when '5' then 'Not Available' else CTY_DESCR end CTY_DESCR  from Country_m order by CTY_DESCR", connection)
            Dim dt As New DataTable()
            adapter.Fill(dt)

            ddlCountry.DataTextField = "CTY_DESCR"
            ddlCountry.DataValueField = "CTY_ID"
            ddlCountry.DataSource = dt
            ddlCountry.DataBind()
            ' Insert the first item.
            ddlCountry.Items.Insert(0, New ListItem("- Select Country -"))
            ddlCity.Items.Insert(0, New ListItem("-Select city-"))
            ddlCountry.SelectedValue = "172"
            LoadCity()
            'ddlCountry.Filter = DirectCast(Convert.ToInt32(1), RadComboBoxFilter)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        LoadCity()
    End Sub
    Private Sub LoadCity()

        Dim sql_conn As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")
        ddlCity.Items.Clear()
        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)

                ddlCity.Items.Add(New ListItem("-Select city-", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                Else
                    ddlCity.Items.Add(New ListItem("Other", "0"))
                End If

            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FState")
        End Try
    End Sub

End Class