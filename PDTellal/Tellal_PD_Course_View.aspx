﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_PD_Course_View.aspx.vb" Inherits="Tellal_PD_Course_View" %>

<%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameAddPDC").fancybox({
                type: 'iframe',
                fitToView: false,
                width: '85%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Course
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" width="100%">
                    <tr id="trLabelError">
                        <td align="left" class="matters" valign="bottom">
                            <div id="lblError" runat="server" class="error">
                            </div>
                        </td>
                    </tr>
                    <tr id="trAdd">
                        <td align="left">                             
                          <%--  <a id="frameAdd" class="frameAddPDC" href="Tellal_PD_Trainer_Add.aspx?Id=0">ADD NEW</a>--%>                            
                             <asp:LinkButton ID="lbtnAdd" runat="server" CausesValidation="false" Text="Add New"
                        OnClick="lbtnAdd_Click"></asp:LinkButton>
                
                        </td>
                    </tr>
                    <tr id="trGridv" runat="server">
                        <td align="center">

                            <asp:GridView ID="gvPDTrainsers" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="10" Width="100%" OnPageIndexChanging="gvPDTrainsers_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("CM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CM_TITLE">
                                        <HeaderTemplate>
                                            <span class="field-label">TITLE
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtCM_TITLE" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchCM_TITLE" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchCM_TITLE_Click"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCM_TITLE" runat="server" Text='<%# Bind("CM_TITLE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EVENT DATE">
                                        <HeaderTemplate>
                                            <span class="field-label">EVENT DATE
                                            </span>
                                            <%--<br />
                                            <asp:TextBox ID="txtCM_EVENT_DT" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchCM_EVENT_DT" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchCM_EVENT_DT_Click"></asp:ImageButton>--%>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCM_EVENT_DT" runat="server" Text='<%# Bind("CM_DATE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="START TIME">
                                        <HeaderTemplate>
                                          <span class="field-label">START TIME
                                            </span>
                                             <%-- <br />
                                            <asp:TextBox ID="txtCM_START_TIME" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchCM_START_TIME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchCM_START_TIME_Click"></asp:ImageButton>--%>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCM_START_TIME" runat="server" Text='<%# Bind("CM_START_TIME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CM_END_TIME">
                                        <HeaderTemplate>
                                          <span class="field-label">END TIME
                                            </span>
                                            <%--  <br />
                                            <asp:TextBox ID="txtCM_END_TIME" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchCM_END_TIME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchCM_END_TIME_Click"></asp:ImageButton>--%>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCM_END_TIME" runat="server" Text='<%# Bind("CM_END_TIME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CM_MAX_CAPACITY">
                                        <HeaderTemplate>
                                           <span class="field-label">MAX CAPACITY
                                            </span>
                                             <%--<br />
                                            <asp:TextBox ID="txtCM_MAX_CAPACITY" runat="server" Width="160px"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchCM_MAX_CAPACITY" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchCM_MAX_CAPACITY_Click"></asp:ImageButton>--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCM_MAX_CAPACITY" runat="server" Text='<%# Bind("CM_MAX_CAPACITY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Text="Edit"
                                                OnClick="lbtnEdit_Click"></asp:LinkButton>
                                           <%-- <asp:HyperLink ID="lnkEdit" class="frameAddPDC" runat="server" NavigateUrl="~/PDTellal/Tellal_PD_Trainer_Add.aspx">Edit</asp:HyperLink>
                                           --%>  <%--<a id="frameEdit" class="frameAddPDC" r>Edit</a>--%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Participants" ShowHeader="False">
                            <ItemTemplate>
                              <%--  <a id="framePartcpnt" class="frameParticipant" href="PD_Participants.aspx?ID=<%#Eval("CM_ID")%>">
                                    View</a> |--%>
                                <asp:LinkButton ID="lbtnExcel" runat="server" CausesValidation="false" Text="Download"
                                    OnClick="lbtnExcel_Click"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                        </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

