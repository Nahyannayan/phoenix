<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="Tellal_PD_EventRequest_17sep.aspx.vb" Inherits="Tellal_PD_EventRequest" Title="::::Tellal PD Event Request::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function checkDate(sender, args) {

            var currenttime = document.getElementById("<%=hfDate.ClientID %>").value;

            if (sender._selectedDate > new Date(currenttime)) {
                alert("You cannot select a day greater than today!");
                sender._selectedDate = new Date(currenttime);
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }





    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Course Requests
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label><span style="color: #c00000">&nbsp;</span>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                    <span style="color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%"> 
                                <tr id="trBusinessUnit" runat="server">
                                    <td align="left">
                                        <span class="field-label">Staff Type</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlEmpType" runat="server" AutoPostBack="True" Width="200px"> 
                                            <asp:ListItem Value="1">Gems</asp:ListItem>
                                            <asp:ListItem Value="2">Non-Gems</asp:ListItem> 
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Business Unit</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBusinessUnit" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                     
                                </tr>
                                <tr id="trAcademicYear" runat="server">
                                    <td align="left">
                                        <span class="field-label">Region</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Location</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlLocation" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trSubject" runat="server">
                                    <td align="left" style="height: 21px">
                                        <span class="field-label">Reg. From Date</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalendar" TargetControlID="txtFromDate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        &nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Visible="False"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server"
                                                ControlToValidate="txtFromDate" CssClass="error" Display="Dynamic" ErrorMessage="From Date required"
                                                ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">To Date</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalendar" TargetControlID="txtToDate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        &nbsp;<asp:ImageButton ID="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Visible="False"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvDate0" runat="server" ControlToValidate="txtToDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                                <tr id="tr1" runat="server">
                                    <td align="left">
                                        <span class="field-label">Course From Date</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCFromDate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="clCfrom" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalendar" TargetControlID="txtCFromDate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        &nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Visible="False"></asp:ImageButton><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="txtCFromDate" CssClass="error" Display="Dynamic" ErrorMessage="From Date required"
                                                ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">To Date</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCtodate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="clTo" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalendar" TargetControlID="txtCtodate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        &nbsp;<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Visible="False"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCtodate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="trSubject1" runat="server">
                                    <td align="left">
                                        <span class="field-label">Event Title</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                    </td>
                                    <td colspan="3">



                                        <asp:RadioButton ID="optUpcoming" runat="server" AutoPostBack="True" Checked="True"
                                            GroupName="Status" Text="Upcoming" Visible="false" />
                                        <asp:RadioButton ID="optOnGoing" runat="server" AutoPostBack="True" GroupName="Status"
                                            Text="Ongoing" Visible="false" />
                                        <asp:RadioButton ID="optExpired" runat="server" AutoPostBack="True" GroupName="Status"
                                            Text="Expired" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" style="height: 7px" valign="bottom" align="center">
                            <asp:GridView ID="gvRequest" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Height="100%" Width="100%" EnableModelValidation="True" AllowPaging="True">

                                <Columns>
                                    <asp:TemplateField HeaderText="SR. NO">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSrNo" runat="server" __designer:wfdid="w18" Text='<%# bind("SR_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Event Title">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLocationRegion" runat="server" Text='<%# bind("Title") %>' __designer:wfdid="w6"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="25%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Event Location &amp; Region">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTitle" runat="server" Text='<%# bind("Location_Region") %>' __designer:wfdid="w6"></asp:Label>
                                            <asp:Literal ID="ltStar0" runat="server" __designer:wfdid="w7"></asp:Literal>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Height="25px" Wrap="True"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="25%" VerticalAlign="Middle" Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Event Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# bind("CM_EVENT_DT") %>' __designer:wfdid="w6"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="25%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Max Capacity">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMaxCapacity" runat="server" __designer:wfdid="w18" Text='<%# bind("Max_Capacity") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                   <%-- <asp:TemplateField HeaderText="Course Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCourseCategory" runat="server" __designer:wfdid="w18" Text='<%# bind("COURSE_CATEGORY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Available">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalAvailable" runat="server" __designer:wfdid="w18" Text='<%# bind("TOTAL_AVAILABLE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Requested">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalRequested" runat="server" __designer:wfdid="w18" Text='<%# bind("Total_Requested") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Total Approved">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalApproved" runat="server" __designer:wfdid="w18" Text='<%# bind("Total_Approved") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Total Pending">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalPending" runat="server" __designer:wfdid="w18" Text='<%# bind("Total_Pending") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Cancelled">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalCancelled" runat="server" __designer:wfdid="w18" Text='<%# bind("Total_Cancelled") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:CommandField SelectText="View All Requests" ShowSelectButton="True">
                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="CM ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbLCMID" runat="server" __designer:wfdid="w18" Text='<%# bind("CM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" Height="15px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Height="25px" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" style="height: 22px" valign="bottom">&nbsp;
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfMode" runat="server"></asp:HiddenField>
                <table id="tblNoAccess" runat="server" align="center" width="100%" visible="false">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblNoAccess" runat="server"  EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
