﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_Newsletter_Track.aspx.vb" Inherits="PDTellal_Tellal_Newsletter_Track" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>News Letter Track
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="tbl_AddGroup" runat="server" width="100%">
                <tr>
                    <td align="center" valign="bottom">
                        <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div class="col-md-12">
                <table style="width: 100% !important;">
                    <tr>
                        <td align="left" width="10%"><span class="field-label">Select Subject :</span></td>
                        <td align="left" width="20%">
                            <asp:DropDownList ID="DDLSubject" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                        <td width="70%"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table width="100%">
                <tr>
                    <td align="center">
                        <asp:GridView ID="gv_News_Track" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                            OnPageIndexChanging="gv_News_Track_PageIndexChanging"
                            PageSize="10">
                            <RowStyle CssClass="griditem" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <Columns>
                                 <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                <%--<asp:BoundField DataField="Track_ID" HeaderText="Track ID" HeaderStyle-CssClass="field-label" ItemStyle-Width="10%" />--%>
                                <asp:BoundField DataField="Track_Email_ID" HeaderText="Email ID" HeaderStyle-CssClass="field-label" ItemStyle-Width="35%" />
                                <asp:BoundField DataField="Track_Email_subject" HeaderText="Subject" HeaderStyle-CssClass="field-label" ItemStyle-Width="35%" />
                                <asp:BoundField DataField="Track_Date" HeaderText="Track Date" HeaderStyle-CssClass="field-label" ItemStyle-Width="20%" />
                            </Columns>
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>

        </div>
    </div>
</asp:Content>

