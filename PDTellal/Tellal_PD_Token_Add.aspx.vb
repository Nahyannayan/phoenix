﻿
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports Telerik.Web.UI

Partial Class Tellal_PD_Token_Add
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ' Fill the continents combo.
            Dim BSU_ID As String = Request.QueryString("BSU_ID")
            Dim CM_ID As String = Request.QueryString("CM_ID")
            ViewState("BSU_ID") = BSU_ID
            ViewState("CM_ID") = CM_ID
            ViewState("id") = "0"
            hdnSeq.Value = "0"
            GetLastSq()
            LoadBSU()
            LoadCourse()
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If ValidateForm() Then

            Dim str As String = hdnTokens.Value
            If str.Length > 0 Then
                str = str.Substring(1, str.Length - 1) 

                Dim finalStatus As Boolean = False
                ' For Each token In tokens
                Dim errormsg As String = String.Empty
                If callTransaction(str, errormsg) <> 0 Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"
                    ' finalStatus = False
                Else
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Record saved successfully !!!</div>"
                    'finalStatus = True
                End If

                ' Next
                'If finalStatus Then
                '    Dim errormsg As String = String.Empty
                '    If UpdateSeq(errormsg) <> 0 Then
                '        lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"

                '    Else
                '        lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Record saved successfully !!!</div>"

                '    End If
                'End If
            End If

        End If
        BindTokens()
    End Sub
    Private Function GenerateToken(ByVal sq As String) As String
        Dim tokenNum As String = "TKN"
        Dim zr As String = ""
        For cnt As Integer = 0 To (7 - (sq.Length)) - 1
            zr = zr + "0"
        Next
        tokenNum = tokenNum + zr + sq
        Return tokenNum
    End Function

    Private Sub BindTokens()
        Try

            Dim rwCnt As Integer = tblToken.Rows.Count
            Dim seq As Integer = hdnSeq.Value
            Dim num As Integer = 0
            If txtTokenNum.Text.Trim.Length > 0 Then
                num = Convert.ToInt16(txtTokenNum.Text.Trim)
            End If


            For cnt As Integer = 0 To num - 1

                Dim tr As HtmlTableRow = New HtmlTableRow()

                Dim tc1 As HtmlTableCell = New HtmlTableCell()
                tc1.InnerHtml = cnt + 1
                tr.Cells.Add(tc1)
                Dim tc2 As HtmlTableCell = New HtmlTableCell()
                tc2.InnerHtml = "<span class='tokenClass'>" + GenerateToken(seq.ToString()) + "</span>"
                tr.Cells.Add(tc2)
                ' tr.Cells.Add(New HtmlTableCell("<span class='tokenClass'>" + GenerateToken(seq.ToString()) + "</span>"))
                tblToken.Rows.Add(tr)
                seq = seq + 1
            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub



    Protected Sub LoadBSU()
        Dim connection As New SqlConnection(ConnectionManger.GetTellalPDConnectionString)

        Dim str As StringBuilder = New StringBuilder()
        str.Append("Select '1' as rownum, ID,SchoolName from [dbo].[Contract_Company_M] cm  ")

        Dim adapter As New SqlDataAdapter(str.ToString, connection)
        Dim dt As New DataTable()
        adapter.Fill(dt)

        ddlBSU.DataTextField = "SchoolName"
        ddlBSU.DataValueField = "ID"
        ddlBSU.DataSource = dt
        ddlBSU.DataBind()
        ' Insert the first item.
        ddlBSU.Items.Insert(0, New RadComboBoxItem("- Select a BSU -"))
        ddlBSU.Filter = DirectCast(Convert.ToInt32(1), RadComboBoxFilter)
    End Sub

    Protected Sub GetLastSq()
        Dim connection As New SqlConnection(ConnectionManger.GetTellalPDConnectionString)

        Dim adapter As New SqlDataAdapter("select max(LastTokenSeq)+1 as Seq from [dbo].[TokenSeq_M]", connection)
        Dim dt As New DataTable()
        adapter.Fill(dt)
        If dt.Rows.Count > 0 Then
            hdnSeq.Value = Convert.ToString(dt.Rows(0)("Seq"))
        End If

    End Sub

    Private Sub LoadCourse()
        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EMP_ID", 0)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Calendar_Events_CAL_VIEW", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                ddlCourse.DataTextField = "CM_TITLE"
                ddlCourse.DataValueField = "CM_ID"
                ddlCourse.DataSource = DT
                ddlCourse.DataBind()
                ' Insert the first item.
                ddlCourse.Items.Insert(0, New RadComboBoxItem("- Select a Course -"))
                ddlCourse.Filter = DirectCast(Convert.ToInt32(1), RadComboBoxFilter)

            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Function ValidateForm() As Boolean
        Dim str As String = hdnTokens.Value
        Dim errMsg As String = String.Empty

        If ddlBSU.Items.Count >= 0 Then
            If ddlBSU.SelectedValue = "" Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>Please select the BSU</div>"
                Return False
            End If

        End If

        If ddlCourse.Items.Count >= 0 Then
            If ddlCourse.SelectedValue = "" Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>Please select the Course</div>"
                Return False
            End If
        End If


        If str.Length > 0 Then
            str = str.Substring(1, str.Length - 1)
            Dim tokens As String() = str.Split(New Char() {","c})

            Dim token As String
            For Each token In tokens
                If token.Length <= 0 Then
                    errMsg = "Please Enter the all the tokens"
                End If

            Next
        Else
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>Please Generate the Token</div>"
            Return False
        End If

        If errMsg.Length > 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errMsg & "</div>"
            Return False
        End If


        Return True
    End Function

    Private Function callTransaction(ByVal token As String, ByRef errormsg As String) As Integer

        Dim tran As SqlTransaction
        Dim ID As String = ViewState("id")

        Using CONN As SqlConnection = ConnectionManger.Get_TellalPD_Connection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim num As Integer = Convert.ToInt64(Convert.ToInt64(hdnSeq.Value) - 1)
                num = num + Convert.ToInt16(txtTokenNum.Text.Trim())
                Dim param(5) As SqlParameter
                param(0) = New SqlParameter("@ID", ID)
                param(1) = New SqlParameter("@TokenNo", token)
                param(2) = New SqlParameter("@Course_id", ddlCourse.SelectedItem.Value)
                param(3) = New SqlParameter("@Cotract_Company_ID", ddlBSU.SelectedItem.Value)
                param(4) = New SqlParameter("@Seq", num)

                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.Insert_Token_M", param)
                Dim ReturnFlag As Integer = param(5).Value

               

                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SVC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    'resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using 

    End Function

    
End Class
