﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class PDTellal_Tellal_Newsletter_Track
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'If Not Request.UrlReferrer Is Nothing Then
                '    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                'End If

                ''get the data mode from the query string to check if in add or edit mode 
                'ViewState("datamode") = "add"
                ''get the menucode to confirm the user is accessing the valid page
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ''check for the usr_name and the menucode are valid otherwise redirect to login page
                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F351011") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else
                '        Response.Redirect("~\noAccess.aspx")
                '    End If

                'Else
                BindSubject()
                GridBind(DDLSubject.SelectedValue.ToString())
                'End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Information)
            End Try
        End If
    End Sub
    Private Sub GridBind(ByVal FilterSubject As String)
        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()
            Dim ds As New DataSet

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@StrMode", "GET_DATA")
            pParms(1) = New SqlClient.SqlParameter("@Track_Email_subject", FilterSubject)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "NEWSLETTER_TRACK_CRUD", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                gv_News_Track.DataSource = ds.Tables(0)
                gv_News_Track.DataBind()
            Else
                gv_News_Track.DataSource = Nothing
                gv_News_Track.DataBind()

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gv_News_Track.DataSource = ds.Tables(0)
                Try
                    gv_News_Track.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gv_News_Track.Rows(0).Cells.Count
                gv_News_Track.Rows(0).Cells.Clear()
                gv_News_Track.Rows(0).Cells.Add(New TableCell)
                gv_News_Track.Rows(0).Cells(0).ColumnSpan = columnCount
                gv_News_Track.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gv_News_Track.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindSubject()
        Try
            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString()
            Dim ds As New DataSet

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@StrMode", "GET_SUBJECT")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "NEWSLETTER_TRACK_CRUD", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                DDLSubject.DataSource = ds.Tables(0)
                DDLSubject.DataTextField = "Track_Email_subject"
                DDLSubject.DataValueField = "Track_Email_subject"
                DDLSubject.DataBind()
                DDLSubject.ClearSelection()
            Else
                DDLSubject.DataSource = Nothing
                DDLSubject.DataBind()
            End If

            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = ""
            DDLSubject.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gv_News_Track_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        Me.gv_News_Track.PageIndex = e.NewPageIndex
        GridBind(DDLSubject.SelectedValue.ToString())
    End Sub
    Protected Sub DDLSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLSubject.SelectedIndexChanged
        GridBind(DDLSubject.SelectedValue.ToString())
    End Sub
End Class
