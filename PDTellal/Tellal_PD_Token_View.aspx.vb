﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Partial Class Tellal_PD_Token_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                ViewState("PDC_ID") = 0
                'bindBusinessUnits()
                BindPD_Token()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try


        End If

    End Sub


    Protected Sub lbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_Trainers_ADD.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbtnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim errormsg As String = String.Empty

            Dim hdnCM_ID As New Label
            hdnCM_ID = TryCast(sender.FindControl("hdnCM_ID"), Label)

            Dim hdnBSU_ID As New Label
            hdnBSU_ID = TryCast(sender.FindControl("hdnBSU_ID"), Label)

            DownloadToken(Convert.ToInt64(hdnCM_ID.Text), Convert.ToInt64(hdnBSU_ID.Text))

            'hdnBSU_ID hdnCM_ID

            'Dim url As String
            'ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ''define the datamode to view if view is clicked
            'ViewState("datamode") = "add"
            ''Encrypt the data that needs to be send through Query String

            'ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            'url = String.Format("~\PDTellal\Tellal_PD_Trainer_Add.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            'Response.Redirect(url)

        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvPDTrainsers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPDTrainsers.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            'Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)

            'Dim lblId As System.Web.UI.WebControls.Label = CType(e.Row.FindControl("lblID"), System.Web.UI.WebControls.Label)
            'Dim lnkEdit As System.Web.UI.WebControls.HyperLink = CType(e.Row.FindControl("lnkEdit"), System.Web.UI.WebControls.HyperLink)
            'Dim url As String
            'url = String.Format("~\PDTellal\Tellal_PD_Trainer_Add.aspx?Id=" + lblId.Text)
            'lnkEdit.NavigateUrl = url
            'imageFile.ImageUrl = strImageUrl

        End If
    End Sub

    Protected Sub gvPDTrainsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDTrainsers.PageIndex = e.NewPageIndex
        BindPD_Token()
    End Sub

   

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_Token()
    End Sub

    Protected Sub btnSearchCM_TITLE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_Token()
    End Sub
    Protected Sub btnSearchNo_Of_Tokens_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_Token()
    End Sub

    Sub DownloadToken(ByVal CM_ID As Integer, ByVal BSU_ID As Integer)
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString

        Dim param(5) As SqlClient.SqlParameter

        Try

            param(0) = New SqlClient.SqlParameter("@CM_ID", CM_ID.ToString())
            param(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID.ToString())

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetTokens_byCourseID_BSUID]", param)
            Dim Dt As DataTable = New DataTable()
            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)

                Dim sb As New StringBuilder()
                Dim sw As New StringWriter(sb)
                Dim htw As New HtmlTextWriter(sw)
                Dim pag As New Page()
                Dim form As New HtmlForm()
                Dim gridV As New GridView()
                gridV.EnableViewState = False
                gridV.DataSource = Dt
                gridV.DataBind()
                pag.EnableEventValidation = False
                pag.DesignerInitialize()
                pag.Controls.Add(form)
                form.Controls.Add(gridV)
                pag.RenderControl(htw)
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=Tokens.xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default
                Response.Write(sb.ToString())
                Response.End()


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try

    End Sub


    Sub BindPD_Token()
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString

        Dim param(5) As SqlClient.SqlParameter

        Dim str_CourseTitle As String = String.Empty
        Dim str_NoOfTokens As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim CourseTitle As String = String.Empty
        Dim NoOfTokens As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim Dt As New DataTable
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty
        Try

            If gvPDTrainsers.Rows.Count > 0 Then

                txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtCM_TITLE")

                If txtSearch.Text.Trim <> "" Then
                    CourseTitle = " AND CM_TITLE like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_CourseTitle = txtSearch.Text.Trim
                End If

                txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND BSU_Name like '%" & txtSearch.Text.Trim() & "%' "
                    str_BSU_Name = txtSearch.Text.Trim()
                End If


                txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtNo_Of_Tokens")

                If txtSearch.Text.Trim <> "" Then

                    NoOfTokens = " AND No_Of_Tokens=" & txtSearch.Text.Trim()
                    str_NoOfTokens = txtSearch.Text.Trim()
                End If
            End If
            FILTER_COND = CourseTitle + BSU_Name + NoOfTokens

            param(0) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[Get_BSU_Token]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                gvPDTrainsers.DataSource = ds.Tables(0)
                gvPDTrainsers.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvPDTrainsers.DataSource = ds.Tables(0)
                Try
                    gvPDTrainsers.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvPDTrainsers.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPDTrainsers.Rows(0).Cells.Clear()
                gvPDTrainsers.Rows(0).Cells.Add(New TableCell)
                gvPDTrainsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDTrainsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDTrainsers.Rows(0).Cells(0).Text = "No records available !!!"
            End If

            txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtCM_TITLE")
            txtSearch.Text = str_CourseTitle

            txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtBSU_Name")
            txtSearch.Text = str_BSU_Name


            txtSearch = gvPDTrainsers.HeaderRow.FindControl("txtNo_Of_Tokens")
            txtSearch.Text = str_NoOfTokens

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try

    End Sub
     
End Class
