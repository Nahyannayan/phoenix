﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Tellal_PD_Certificate.aspx.vb" Inherits="Tellal_PD_Certificate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="Javascript1.2">
  <!--
  function printpage() {
  window.print();
  }
  //-->
</script>
<title>PD certificate 2013-2014</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">td img {display: block;}</style>
<!--Fireworks CS5 Dreamweaver CS5 target.  Created Thu Sep 05 14:16:06 GMT+0400 (Arabian Standard Time) 2013-->
</head>

<body bgcolor="#ffffff" style="text-align:center" onload="printpage()">
<div style="zindex:100">
<span style="position:absolute;top:180px;Left:430px">
<p style="font-weight:bold;color:#000000;font-size:55px;"><asp:Label ID="lblPArticipant" runat="server"></asp:Label></P></span>

<span style="position:absolute;top:390px;Left:430px">
<p style="font-weight:bold;color:#000000;font-size:30px;font-family:Arial Baltic"><asp:Label ID="lblCourse" runat="server"></asp:Label> </P></span>


<span style="position:absolute;top:468px;Left:318px">
<p style="font-weight:bold;color:#000000;font-size:25px;"><asp:Label ID="lblDate" runat="server"> </asp:Label></P></span>

<span style="position:absolute;top:948px;right:58px">
<p style="font-weight:Normal;color:#000000;font-size:20px;text-align:left;font-family:Arial;"><asp:Label ID="lblTrainer" runat="server"></asp:Label> </P></span>

</div>
<div style="z-index:0">

<table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="1600px">
<!-- fwtable fwsrc="PD certificate template 2013-2014.png" fwpage="Page 1" fwbase="PD certificate template 2013-2014.jpg" fwstyle="Dreamweaver" fwdocid = "2061693461" fwnested="0" -->
  <tr>
   <td><img src="images/spacer.gif" width="800" height="1" border="0" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="1" border="0" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="1" border="0" alt="" /></td>
  </tr>

  <tr>
   <td colspan="2"><img name="PDcertificatetemplate20132014_r1_c1" src="images/PD%20certificate%20template%202013-2014_r1_c1.jpg" width="1600px" height="284" border="0" id="PDcertificatetemplate20132014_r1_c1" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="284" border="0" alt="" /></td>
  </tr>
  <tr>
   <td><img name="PDcertificatetemplate20132014_r2_c1" src="images/PD%20certificate%20template%202013-2014_r2_c1.jpg" width="1600px" height="231" border="0" id="PDcertificatetemplate20132014_r2_c1" alt="" /></td>
   <td valign="top"><p style="margin:0px"></p></td>
   <td><img src="images/spacer.gif" width="1" height="231" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="2"><img name="PDcertificatetemplate20132014_r3_c1" src="images/PD%20certificate%20template%202013-2014_r3_c1.jpg" width="1600px" height="429" border="0" id="PDcertificatetemplate20132014_r3_c1" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="429" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="2"><img name="PDcertificatetemplate20132014_r4_c1" src="images/PD%20certificate%20template%202013-2014_r4_c1.jpg" width="1600px" height="237" border="0" id="PDcertificatetemplate20132014_r4_c1" usemap="#m_PD20certificate20template2020132014_r4_c1" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="237" border="0" alt="" /></td>
  </tr>
</table></div>
<map name="m_PD20certificate20template2020132014_r4_c1" id="m_PD20certificate20template2020132014_r4_c1">
<area shape="rect" coords="1408,28,1588,167" href="http://www.gemseducation.com" target="_blank" title="GEMS Education" alt="GEMS Education" />
<area shape="rect" coords="1128,28,1392,167" href="http://www.gemseducation.com/philanthropy" target="_blank" alt="" />
</map>
</body>
</html>--%>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <script language="Javascript1.2">
  <!--
  function printpage() {
  window.print();
  }
  //-->
</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>GEMS Professional Development Passport</title>
<style type="text/css">
.auto-style1 {
	background-image: url('images/bg_certificate_v2.jpg');
}
.auto-style2 {
	text-align: center;
}
.auto-style4 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
	color: #858484;
	text-align: left;
}
.auto-style5 {
	text-align: center;
	color: #17468F;
	font-size: 42pt;
}
.auto-style6 {
	text-align: left;
	color: #FF00FF;
}
.auto-style7 {
	text-align: left;
}
.auto-style8 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style9 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22pt;
	color: #808080;
}
.auto-style10 {
	font-family: Arial, Helvetica, sans-serif;
	color: #808080;
	font-size: medium;
}
.auto-style11 {
	font-size: medium;
}
.auto-style12 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32pt;
	color: #C82709;
}
</style>
</head>

<body onload="printpage()">

<table cellpadding="0" cellspacing="0" style="width: 1625px; height: 1125px;" class="auto-style1">
	<tr>
		<td class="auto-style2"><br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<div id="name_and_course" class="auto-style4" style="border-style: none; border-color: inherit; border-width: 0px; position: absolute; width: 436px; height: 27px; z-index: 4; left: 1099px; top: 861px; visibility: visible;">
			<div id="CourseTitle" class="auto-style5" style="position: absolute; width: 1384px; height: 59px; z-index: 2; left: -972px; top: -196px">
				<strong><asp:Label ID="lblCourse" runat="server"></asp:Label> </strong></div>
			<div id="nameParticipant0" class="auto-style5" style="position: absolute; width: 1384px; height: 59px; z-index: 3; left: -972px; top: -383px">
				<strong><asp:Label ID="lblPArticipant" runat="server"></asp:Label></strong></div>
			<strong>Name of Trainer<br />
			</strong><span class="auto-style11"><asp:Label ID="lblTrainer" runat="server"></asp:Label></span></div>
		<div id="nameTrainer" class="auto-style4" style="border-style: none; border-color: inherit; border-width: 0px; position: absolute; width: 436px; height: 27px; z-index: 6; left: 1099px; top: 861px;display:none;">
			<strong>Name of Trainer<br />
			</strong><span class="auto-style11">Trainer</span></div>
		<div id="signature_Trainer" class="auto-style6" style="position: absolute; width: 296px; height: 45px; z-index: 8; left: 1103px; top: 805px; right: 678px;display:none;">
			image signature here</div>
		<div id="signature_LindaRush" style="position: absolute; width: 174px; height: 41px; z-index: 9; left: 377px; top: 815px">
			<img alt="" height="35" src="images/signature_lindarush.png" width="132" /></div>
		<div id="LindaRush" class="auto-style7" style="position: absolute; width: 457px; height: 100px; z-index: 10; left: 386px; top: 859px">
			<span class="auto-style9"><strong>Linda Rush</strong></span><br class="auto-style8" />
			<span class="auto-style10">Vice President</span><br class="auto-style10" />
			<span class="auto-style10">GEMS Institute of Teacher Education, 
			Leadership &amp; Learning</span></div>
		<div id="CourseDate" class="auto-style12" style="position: absolute;  width: 349px; height: 58px; z-index: 11; left: 648px; top: 1020px">
           <font size="4"><asp:Label ID="lblDate" runat="server"> </asp:Label></font> </div>
		</td>
	</tr>
	</table>

</body>

</html>
