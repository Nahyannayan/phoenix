﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tellal_PD_Add_Location.aspx.vb" Inherits="Tellal_PD_Add_Location" %>

<%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .k-chart, .k-gauge, .k-sparkline, .k-stockchart, .k-chart text {
            font-family: Raleway, sans-serif !important;
        }
    </style>

    <section class="col-md-12 col-lg-12 col-sm-12 colContainerBox">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group">
                <div class="row">
                     <div class="table-responsive">

                    <table width="100%" id="tblCategory" cellspacing="2" cellpadding="2">
                        <tr id="trLabelError">
                            <td align="left" valign="bottom" colspan="2">
                                <div id="lblError" runat="server">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" align="center">
                                <table width="100%" id="tblSubAct" cellspacing="4" cellpadding="4">
                                    <tr align="left">
                                        <td>
                                            <span class="field-label">Region </span>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblREGION" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td>
                                            <span class="field-label">Location<font color="maroon">*</font></span>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="txtLocation" runat="server"></asp:TextBox>
                                        </td>

                                        <td>
                                            <span class="field-label">Short Code<font color="maroon">*</font></span>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="txtShort" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnAddLOCATION" runat="server" Text="Add/Save" CssClass="button"
                                                ValidationGroup="rfvSub" Width="130px" />
                                            <asp:Button ID="btnUpdateLOCATION" runat="server" Text="Update/Save" CssClass="button"
                                                Width="130px" ValidationGroup="rfvSub" />
                                            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button"
                                                Width="130px"/>
                                            <input type="button" class="button" id="btnCancel1" title="Close" value="Close" onclick="fancyClose()" />
                                            <asp:HiddenField ID="hdnID" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="trGridV">
                            <td colspan="2">
                                <table id="Table2" runat="server" align="center"
                                    cellpadding="5" cellspacing="0" style="width: 100%">

                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="gvLocations" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging="gvLocations_PageIndexChanging">
                                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                                <EmptyDataRowStyle Wrap="False" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="L_ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblL_ID" runat="server" Text='<%# Bind("L_ID") %>' __designer:wfdid="w40"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Region">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblREGIONG" runat="server" Text='<%# Bind("R_REGION_NAME") %>' __designer:wfdid="w40"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblL_DESCR" runat="server" Text='<%# Bind("L_DESCR") %>' __designer:wfdid="w40"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Short">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblL_SHORT" runat="server" Text='<%# Bind("L_SHORT") %>' __designer:wfdid="w40"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Text="Edit"
                                                                OnClick="lbtnEdit_Click"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            </td>
                        </tr>
                    </table>
                </div>



                </div>
            </div>
        </div>
    </section>
</asp:Content>


