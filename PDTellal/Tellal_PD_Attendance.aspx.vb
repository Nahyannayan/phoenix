﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Tellal_PD_Attendance
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.QueryString("msg") Is Nothing Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Attendance marked successfully!!!</div>"
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

               ViewState("CM_ID") = 0
                'bindBusinessUnits()
                bindCourses()
                Dim IsTRainer As Boolean = Check_PD_Trainer()

                Dim isPDSuperuser As Boolean = Check_PD_SuperUser()

                If Not IsTRainer AndAlso Not isPDSuperuser Then
                    tblNoAccess.Visible = True
                    tblData.Visible = False
                    Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                    lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:black;padding:5pt;background-color:white;'>" & Errormsg & "</div>"
                End If
                'calling pageright class to get the access rights

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub ddlCourse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCourse.SelectedIndexChanged
        Try '
            'ddlBusinessunit_SelectedIndexChanged()

            ViewState("CM_ID") = ddlCourse.SelectedValue
            If ddlCourse.SelectedValue <> "0" Then
                BindCourseInfoById(Convert.ToInt32(ddlCourse.SelectedValue))
            End If
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    'Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlbusinessunit.SelectedIndexChanged
    '    Try

    '        ViewState("BSU_ID") = ddlbusinessunit.SelectedItem.Value
    '        bindCourses()
    '    Catch ex As Exception
    '        '' lblError.InnerHtml = "Request could not be processed"
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '    End Try
    'End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        bindParticipantsGrid()

    End Sub

    Protected Sub btnPresent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPresent.Click
        ViewState("STATUS") = "A"
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"

        Else 
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Attendance marked successfully !!!</div>"
            bindParticipantsGrid()
        End If

    End Sub

    Protected Sub btnSearchEMP_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
    End Sub

    Protected Sub btnAbsent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAbsent.Click
        ViewState("STATUS") = "R"
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"

        Else
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Attendance marked successfully !!!</div>"
            bindParticipantsGrid()
        End If

    End Sub
    Protected Sub btnApprovedLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovedLeave.Click
        ViewState("STATUS") = "AL"
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"

        Else
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:white;'>Attendance marked successfully!!!</div>"
            bindParticipantsGrid()
        End If

    End Sub

    Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipants.PageIndex = e.NewPageIndex
        bindParticipantsGrid()
    End Sub

    'Sub bindBusinessUnits()
    '    ddlBusinessunit.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = ""
    '    Dim ds As DataSet
    '    Dim param(2) As SqlClient.SqlParameter

    '    param(0) = New SqlClient.SqlParameter("@Emp_id", Session("EmployeeId"))

    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_Trainer_BusinessUnit", param)



    '    ddlBusinessunit.DataSource = ds
    '    ddlBusinessunit.DataTextField = "bsu_name"
    '    ddlBusinessunit.DataValueField = "bsu_id"
    '    ddlBusinessunit.DataBind()
    '    ddlbusinessunit.Items.Insert(0, New ListItem("-All-", "0"))
    '    ddlBusinessunit_SelectedIndexChanged(ddlbusinessunit, Nothing)

    'End Sub

    Sub BindCourseInfoById(ByVal CM_ID As Integer)
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@CM_ID", CM_ID)
        param(1) = New SqlClient.SqlParameter("@EMP_ID", CM_ID)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Course_Info_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)
            txtFrom.Text = Convert.ToString(Dt.Rows(0)("CM_EVENT_DT"))

        End If
    End Sub

    Sub bindCourses()
        ddlCourse.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        Try



            param(0) = New SqlClient.SqlParameter("@EMP_ID", "0")
            ' param(1) = New SqlClient.SqlParameter("@BSU_ID", ViewState("BSU_ID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Calendar_Events_CAL_VIEW", param)



            If ds IsNot Nothing Then
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddlCourse.DataSource = ds
                        ddlCourse.DataTextField = "CM_TITLE"
                        ddlCourse.DataValueField = "CM_ID"
                        ddlCourse.DataBind()
                        ddlCourse_SelectedIndexChanged(ddlCourse, Nothing)
                    End If

                End If
            End If


        Catch ex As Exception

        End Try

    End Sub

    Private Sub bindParticipantsGrid()

        Try


            Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable


            If gvParticipants.Rows.Count > 0 Then

                
                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then
                    EMP_Name = " AND name like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_EMP_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND BusinessUnit like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_BSU_Name = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = EMP_NO + EMP_Name + BSU_Name
            param(0) = New SqlClient.SqlParameter("@CM_ID", ddlCourse.SelectedValue) 
            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Participants_List", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
                gvParticipants.Rows(0).Cells.Clear()
                gvParticipants.Rows(0).Cells.Add(New TableCell)
                gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
            Else
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                btnPresent.Visible = True
                btnAbsent.Visible = True
                btnApprovedLeave.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer


        Dim tran As SqlTransaction
         
        Using CONN As SqlConnection = ConnectionManger.Get_TellalPD_Connection

            tran = CONN.BeginTransaction("SampleTransaction")
            Dim CR_IDs As String = String.Empty
            Dim IsNewGroup As Boolean = False

            Dim ReturnFlag As Integer
            Dim param(4) As SqlParameter
            Try



                For Each gvrow As GridViewRow In gvParticipants.Rows
                    Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
                    If chk IsNot Nothing And chk.Checked Then
                        CR_IDs += gvParticipants.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

                    End If
                Next

                ''checking if seat is available

                CR_IDs = CR_IDs.Trim(",".ToCharArray())
                param(0) = New SqlParameter("@CR_IDs", CR_IDs)
                param(1) = New SqlParameter("@EMP_ID", Session("EmployeeId"))
                param(2) = New SqlParameter("@STATUS", ViewState("STATUS"))

                param(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.MARK_ATTENDANCE", param)
                ReturnFlag = param(3).Value
                callTransaction = ReturnFlag
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message

            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()


                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()

                Else
                    errormsg = ""
                    tran.Commit()
                End If

            End Try
        End Using
    End Function
    Function Check_PD_SuperUser() As Boolean
        Dim IsAdmin As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EmpId", Session("EmployeeId"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_Super_User_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAdmin = True
            End If

        End If
        Return IsAdmin
    End Function
    Function Check_PD_Trainer() As Boolean
        Dim IsTrainer As Boolean = False
        Dim str_conn As String = ConnectionManger.GetTellalPDConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_TRAINER_BY_BSU_EMP", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsTrainer = True
            End If

        End If
        Return IsTrainer
    End Function
    
End Class
