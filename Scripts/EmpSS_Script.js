﻿/// <reference path="../EmpSelfServices/empLoadfile.aspx" />
/// <reference path="../EmpSelfServices/empLoadfile.aspx" />
/// <reference path="../EmpSelfServices/empLoadfile.aspx" />
var IsFirstLoad = true;

$(document).ready(function () {
    Sys.Application.add_load(function () {


        $("#divPayDrag").draggable({
            axis: "x",
            start: function (event, ui) {
                ui.helper.bind("click.prevent",
                  function (event) { event.preventDefault(); });

            },
            stop: function (event, ui) {
                setTimeout(function () { ui.helper.unbind("click.prevent"); }, 300);
                var totalWidth = 0;
                var MaxLeft = 0;
                var singleDiv = 0;
                var selectNew = $(this).find('.divPaySlipNew');
                selectNew.each(function (index) {
                    totalWidth = +(totalWidth) + (+$(this).outerWidth(true));
                    singleDiv = $(this).outerWidth(true);
                });
                var selectOld = $(this).find('.divPaySlipOld');
                selectOld.each(function (index) {
                    totalWidth = +(totalWidth) + (+$(this).outerWidth(true));
                    singleDiv = $(this).outerWidth(true);
                });
                //alert(+(totalWidth)); //  total 
                //if MaxLeft is less than 0 then only singleDiv size
                MaxLeft = totalWidth - singleDiv;
                if (MaxLeft < singleDiv) {
                    MaxLeft = -singleDiv;
                }
                else {
                    //if MaxLeft is greater than 0 then only singleDiv size
                    MaxLeft = -(totalWidth - singleDiv) + 20;
                }

                if (ui.position.left > 0) {
                    $("#divPayDrag").animate({ "left": "0px" }, 600);
                }
                else if (ui.position.left < MaxLeft) {
                    $("#divPayDrag").animate({ "left": MaxLeft + "px" }, 600);
                }
            }
        });




        $("#PanelPrinter").click(function () {
            var newWin = window.open('', '', 'left=0,top=0,width=0,height=0,title=no,toolbar=no,location=no,status=no;');
            //newWin.document.open();
            newWin.document.write('<html><head> <title>::::GEMS OASIS:::: Online Student Administration System::::</title><link href="../css/SelfServiceStyle.css" rel="stylesheet" /> <body   onload="window.print()">' + $('#PrintContentholder').html() + '</body></html>');
            newWin.document.close();
            setTimeout(function () { newWin.close(); }, 500);


        });

        $("#PanelClose").click(function () {
            
            $("#divboxPrintPanel").css("display", "none");
            $("#divbox-panel").css("width", "648px");
        });

        $('.dbLinkbtn').bind('click', function (e) {
        
            //if (IsFirstLoad) {
            //    e.preventDefault();
            //}
            var element = $(this);
           //var data = element.data('key');
           //alert(data.param1 + ' ' + data.param2);

$('#iFrame1').attr('src', element.data('url'));
            $('.divDBMainHolder').animate({
                'width': '0px',
                'marginLeft': '0px',
                'opacity':'0'
            }, 800, 'swing', function () {
               
                $('.tranPanel').css({ 'display': 'none' });
                
                $('#Framecontainer').fadeIn(800, 'swing');//.css({ 'display': 'block', 'opacity': '0' }).animate({ 'width': '100%', 'opacity': '1' }, 300, 'easeOutBounce')
                   
                 
              


                // $('#' + element.id + '').click();
               // __doPostBack(element.id, "OnClick")
                //IsFirstLoad = false;
              //  alert(IsFirstLoad);
                // __doPostBack(element.id, "OnClick")
                //return true
            });
        });
    });
});
 
function ShowLeaveDetails() {
    $('.divDBMainHolder').animate({
        'width': '0px',
        'marginLeft': '0px',
        'opacity': '0'
    }, 800, 'swing', function () {

        $('.tranPanel').css({ 'display': 'none' });

        $('#Framecontainer').fadeIn(800, 'swing');

    });
}

function ResetDashBoard() {
    $('.divDBMainHolder').animate({
        'width': '1335px',
        'marginLeft': '12px',
        'opacity': '1'
    }, 800, 'swing', function () {
        $('#Framecontainer').css({ 'display': 'none' });
        $('.tranPanel').fadeIn(800, 'swing');
    });
}

    function PrintCont(Cont) {
 
        var PrintHolder = $('#PrintContentholder');
        var intWidth = $(Cont).width();
        var incWidth
        $('#divboxPrintPanel').css("display", "block");
        if (intWidth != 648) {
            incWidth = 22;
            $('#divbox-panel').animate({ width: '+=' + incWidth }, 500, 'easeOutBounce');
        }
        else {
            incWidth =10;

            $('#divbox-panel').animate({ width: '-=' + incWidth }, { "queue": false, "duration": 500 },500, 'easeOutBounce')
       .animate({ width: '+=' + incWidth }, 500, 'easeOutBounce');

        }

        PrintHolder.html($(Cont).html()).find('.spanRight_TitleLink').css("display", "none");
        PrintHolder.find("a").each(function(i)
        {
            $(this).css("cursor", "default");

            $(this).click(function (e) {
                e.preventDefault();
            });
        })
        
    }

  