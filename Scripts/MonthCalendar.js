function onCalendarShown(sender, args) {
    try {
        //Setting the default mode to month
        sender._switchMode("months", true);
        //Iterate every month Item and attach click event to it
        if (sender._monthsBody) {
            for (var i = 0; i < sender._monthsBody.rows.length; i++) {
                var row = sender._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    var callback1 = Function.createCallback(call, sender);
                    Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", callback1);
                }
            }
        }
    }
    catch (err) {
        //Handle errors here
    }
}
function onCalendarHidden(sender, args) {
    try {
        //Iterate every month Item and remove click event from it
        if (sender._monthsBody) {
            for (var i = 0; i < sender._monthsBody.rows.length; i++) {
                var row = sender._monthsBody.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    var callback1 = Function.createCallback(call, sender);
                    Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", callback1);
                }
            }
        }
    }
    catch (err) {
        //Handle errors here
    }
}
function call(eventElement, sender) {
    try {
        var target = eventElement.target;
        switch (target.mode) {
            case "month":
                sender._visibleDate = target.date;
                sender.set_selectedDate(target.date);
                sender._switchMonth(target.date);
                sender._blur.post(true);
                sender.raiseDateSelectionChanged();
                break;
        }
    }
    catch (err) {
        //Handle errors here
    }

} 