﻿$(document).ready(function () {


    Sys.Application.add_load(function () {

        $('ul.art-vmenu li').click(function (e) {
           
            //First page
            // if ($(this).hasClass("subMenuCssProf"))
            if ($(this).attr('id') == "Page1") {
                $('#mybook').booklet("gotopage", "1");
                return false;
            }
                //Second page
            else if ($(this).attr('id') == "Page2") {
                $('#mybook').booklet("gotopage", "1");
                return false;
            }
                //Third page
            else if ($(this).attr('id') == "Page3") {
                $('#mybook').booklet("gotopage", "3");
                return false;
            }
                //Fourth page
            else if ($(this).attr('id') == "Page4") {
                $('#mybook').booklet("gotopage", "3");
                return false;
            }
                //Fifth page
            else if ($(this).attr('id') == "Page5") {
                $('#mybook').booklet("gotopage", "5");
                return false;
            }
                //Sixth page
            else if ($(this).attr('id') == "Page6") {
                $('#mybook').booklet("gotopage", "5");
                return false;
            }
                //Seventh page
            else if ($(this).attr('id') == "Page7") {
                $('#mybook').booklet("gotopage", "7");
                return false;
            }
                //Eighth page
            else if ($(this).attr('id') == "Page8") {
                $('#mybook').booklet("gotopage", "7");
                return false;
            }
        });

        $("#PanelPrinter").click(function () {
            var newWin = window.open('', '', 'left=0,top=0,width=0,height=0,title=no,toolbar=no,location=no,status=no;');
           newWin.document.open();

            //alert('<html><head> <title>::::GEMS OASIS:::: Online Student Administration System::::</title><link href="../css/ProfileStyle.css" rel="stylesheet" type="text/css"  media="print" /> <body>' + $('#PrintContentholder').html() + '</body></html>')
           newWin.document.write('<html><head> <title>::::GEMS OASIS:::: Online Student Administration System::::</title><link href="../cssfiles/ProfileStyle.css" rel="stylesheet" type="text/css"  media="print" /> <body   onload="window.print()">' + $('#PrintContentholder').html() + '</body></html>');
            newWin.document.close();

            setTimeout(function () { newWin.close(); }, 500);


            //var DocumentContainer = document.getElementById(div_id);
           // var strHtml = "<html>\n<head>\n <link rel=\"stylesheet\" type=\"text/css\"  href=\"http://localhost:28576/css/ProfileStyle.css\">\n</head><body><div>\n"
           //+ $('#PrintContentholder').html() + "\n</div>\n</body>\n</html>";

           // var WindowObject = window.open("", "PrintWindow",
           // "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
           // WindowObject.document.writeln(strHtml);
           // WindowObject.document.close();
           // WindowObject.focus();
           // WindowObject.print();
           // WindowObject.close();
           // setTimeout(function () { WindowObject.close(); }, 500);


        });

        $("#PanelClose").click(function () {

            $("#divboxPrintPanel").css("display", "none");
            $("#divbox-panel").css("width", "570px");
                });
        Load_booklet();

        $("#Next").click(function () {
            $("#mybook").turn("next");
            return false;
        });

        $("#Prev").click(function () {
            $("#mybook").turn("previous");
            return false;
        });


      


    });
});


function PrintCont(Cont) {


    var PrintHolder = $('#PrintContentholder');
    var intWidth = $(Cont).width();
    var intHeight = $(Cont).find('.profContentBox').outerHeight(true);
    
    var incWidth
    $('#divboxPrintPanel').css("display", "block");
    if (intWidth != 570) {
        incWidth = 22;
        $('#divbox-panel').animate({ width: '+=' + incWidth }, 500, 'easeOutBounce');
    }
    else {
        incWidth = 10;

        $('#divbox-panel').animate({ width: '-=' + incWidth }, { "queue": false, "duration": 500 }, 500, 'easeOutBounce')
   .animate({ width: '+=' + incWidth }, 500, 'easeOutBounce');

    }


    PrintHolder.html($(Cont).html()).find('.ProfRight_TitleLink').css("display", "none"); 

    PrintHolder.find('#divFilterLeave').css('display', 'none');
    PrintHolder.find('.divbuttonbox').css('display', 'none');
    PrintHolder.find('.ProfBoxScroll').css('height', intHeight + 20 + 'px');//'750px'
    PrintHolder.find('.ProfBoxScrollInside').css('height', intHeight + 40 + 'px');
    PrintHolder.find('.watermark').css('display', 'inline');
    PrintHolder.find("a").each(function (i) {
        $(this).css("cursor", "default");

        $(this).click(function (e) {
            e.preventDefault();
            Load_booklet();
        });
    })

}
$("#Next").click(function () {
    $("#mybook").turn("next");
    return false;
});

$("#Prev").click(function () {
    $("#mybook").turn("previous");
    return false;
});

function Load_booklet() {
    $("#mybook").booklet({
        width: "1220px",
        height: "500px",
        pageNumbers: false,
        keyboard: true,
        next: "#custom-next",
        prev: "#custom-prev",
        //closed: true,
        pageSelector: true,
        pageBorder: 1,
        arrows: true,
        pagePadding: 1,
        autoCenter: true,
        hash: true,
        shadows: true,
        overlays: false

    });

}


