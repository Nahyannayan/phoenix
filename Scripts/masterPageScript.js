﻿/// <reference path="../WebServices/MasterPageServices.asmx" />
/// <reference path="../WebServices/MasterPageServices.asmx" />


$(document).ready(function () {
    Sys.Application.add_load(function () {

        $('.hrefmsgClose').on('click', function () {
           
            $('#divMmsgBox').addClass('divHide')
            $('#divMmsgBoxInfo').val('');

        });

        $("#divMmsgBox").draggable({
            axis: "y",
            containment: 'parent'
        });

          
        $('#target').on("keydown", function (e) {
            if (e.which === 27)
            {
               
                $('#divboxBg').hide();
                $('#divBoxCRM').hide();
            }

            if (e.altKey && (e.which === 83))//sign out-ALT+s
            {
                $('#divboxBg').show();
                $('#divPassChangeMsg').addClass('divHide');
                $('#tblpassTextBox').addClass('divHide');
                $('#divFeedback').addClass('divHide');
                $('#divFeedBkMsg').addClass('divHide');
                $('#divLogoutConf').removeClass('divHide');
                $('.darkPanelMTop').css({ 'margin-top': '140px', 'width': '500px', 'height': '303px' });
                $('.holderInner').css({ 'width': '485px', 'height': '260px' });
                $('#lblTitlePl').text('Confirm Sign Out');

            }

            else if (e.altKey && (e.which === 80))//password change --ALT+P
            {
                $('#divboxBg').show();
                $('#divPassChangeMsg').removeClass('divHide');
                $('#tblpassTextBox').removeClass('divHide');
                $('#divFeedback').addClass('divHide');
                $('#divFeedBkMsg').addClass('divHide');
                $('#divLogoutConf').addClass('divHide');
                $('.darkPanelMTop').css({ 'margin-top': '110px', 'width': '550px', 'height': '443px' });
                $('.holderInner').css({ 'width': '535px', 'height': '400px' });
                $('#lblTitlePl').text('Change Password');
            }
            else if (e.altKey && (e.which === 70))//feedback  ALT+F
            {
                $('#divboxBg').show();
                $('#divPassChangeMsg').addClass('divHide');
                $('#tblpassTextBox').addClass('divHide');
                $('#divFeedback').removeClass('divHide');
                $('#divFeedBkMsg').removeClass('divHide');
                $('#divLogoutConf').addClass('divHide');
                $('.darkPanelMTop').css({ 'margin-top': '110px', 'width': '545px', 'height': '443px' });
                $('.holderInner').css({ 'width': '530px', 'height': '400px' });
                $('#lblTitlePl').text('Feedback');
            }
            else if (e.altKey && (e.which === 67))//Calculator
            {
                alert('C');
            }
            else if (e.altKey && (e.which === 82))//Report issue
            {
                MasterPageServices.CaptureScreen(SetTabSessionValueSucceed, SetTabSessionValueFailed);


               //$.ajax({
               //     type: "POST",
               //     url: "../WebServices/MasterPageServices.asmx/CaptureScreen",
               //     data: "{}",
               //     contentType: "application/json; charset=utf-8",
               //     dataType: "json",
               //     async: true,
               //     success: SetTabSessionValueSucceed,
               //     error: SetTabSessionValueFailed
               // });


               //// $('#divBoxCRM').show();
               // $('.darkPanelMTop').css({ 'margin-top': '60px', 'width': '1200px', 'height': '573px' });
               // $('.holderInner').css({ 'width': '1185px', 'height': '530px' });
               // $('#spCRMTitlePl').text('Report Issue');

                // var ajaxManager = $('[id*=rdImageEdit_CRM]')
              
             
            }

        });


        function SetTabSessionValueSucceed(result) {
           
            $('#hfCRM_ImgpathMaster').val( result);
        
            $("#btnrptIssueHidden").click();
        }

        function SetTabSessionValueFailed() {

            alert('call failed');
            $('#hfCRM_ImgpathMaster').val('');

        }

        
  

        $('.calCss a').each(function () {

            $(this).replaceWith($(this).text());

        });

        $("#divBsuColDrop").hover(
function () {
  
    $(this).addClass("BsuColDropFixed")
},
function () {
  
    $(this).removeClass("BsuColDropFixed").addClass("BsuColDrop");
}
);
     
        
        $("#divCurrDayColDrop").hover(
function () {
  
    $("#divCurrDayColDrop").addClass("CurrDayColDropFixed")
  
},
function () {
  
    $("#divCurrDayColDrop").removeClass("CurrDayColDropFixed").addClass("CurrDayColDrop");
}
);
        //$("#rddlMth_master,#rddlYear_master").hover(function () {

        //    $("#divCurrDayColDrop").addClass("CurrDayColDropFixed")
        //});

     


 

        $("#txtBsuDropSearch").on("keydown", function (e) {
            if (e.which == 13) {
               
                setTimeout(function () {
                    $("#btnSearchBsuDrop").trigger("click");
                }, 100);

                           }
        });

      
       
        $("table.gridViewClassBlue").delegate('td', 'mouseover mouseleave', function (e) {
           
            if (e.type == 'mouseover') {
                            $(this).parent().addClass("highlight");
                // var ind = $(this).index();
                //var iCol = $(this).parent().children().index(this) + 1;
                //$("table.gridViewClass td:nth-child(" + (iCol) + ")").each(function () {
                //    $(this).addClass("highlight");
                //});


                //$("colgroup").eq($(this).index()).addClass("highlight");
            }
            else {
                $(this).parent().removeClass("highlight");
                //var iCol = $(this).parent().children().index(this) + 1;
                //$("table.gridViewClass td:nth-child(" + (iCol) + ")").each(function () {
                //    $(this).removeClass("highlight");
                //});


            }
        });

        $("table.gridViewClassGray").delegate('td', 'mouseover mouseleave', function (e) {

            if (e.type == 'mouseover') {
                $(this).parent().addClass("highlight");
                // var ind = $(this).index();
                //var iCol = $(this).parent().children().index(this) + 1;
                //$("table.gridViewClass td:nth-child(" + (iCol) + ")").each(function () {
                //    $(this).addClass("highlight");
                //});


                //$("colgroup").eq($(this).index()).addClass("highlight");
            }
            else {
                $(this).parent().removeClass("highlight");
                //var iCol = $(this).parent().children().index(this) + 1;
                //$("table.gridViewClass td:nth-child(" + (iCol) + ")").each(function () {
                //    $(this).removeClass("highlight");
                //});


            }
        });
//        $("#imgfavicon").on("click", function () {
//            alert($('#myHiddenVar').val());
//            //alert('<%=Session("sUsr_id") %>');
//    // $("#divFooterMaster").removeClass("footerMasterFixedCss").addClass("footerMasterCss");
//    var position = divfooter.css('bottom');
    
//    if (parseInt(position) == 0) {
       
//        divfooter.stop().animate({ bottom: '-28px' }, 750, 'easeOutBounce');
//        $('#hffooterVal').val(0);
       
//    }
//    else {
//        divfooter.stop().animate({ bottom: 0 }, 750, 'easeInExpo');
//        $('#hffooterVal').val(1);
//    }
    

//});


    });


});

//if ($('#hffooterVal').val() == 0) {
//    divfooter.css("bottom", '-28px');
//}
//else {
//    divfooter.css("bottom", '0px');
//}


//$(document).ready(function () {
//    // bind your jQuery events here initially
//});

//var prm = Sys.WebForms.PageRequestManager.getInstance();

//prm.add_endRequest(function () {
//    // re-bind your jQuery events here
//});

function ShowFooter_win(event,flag) {

    if (flag=='S')//sign out-ALT+s
    {
        $('#divboxBg').show();
        $('#divPassChangeMsg').addClass('divHide');
        $('#tblpassTextBox').addClass('divHide');
        $('#divFeedback').addClass('divHide');
        $('#divFeedBkMsg').addClass('divHide');
        $('#divLogoutConf').removeClass('divHide');
        $('.darkPanelMTop').css({ 'margin-top': '140px', 'width': '500px', 'height': '303px' });
        $('.holderInner').css({ 'width': '485px', 'height': '260px' });
        $('#lblTitlePl').text('Confirm Sign Out');
        event.preventDefault();
        return false;
    }

    else if (flag == 'P')//password change --ALT+P
    {
        $('#divboxBg').show();
        $('#divPassChangeMsg').removeClass('divHide');
        $('#tblpassTextBox').removeClass('divHide');
        $('#divFeedback').addClass('divHide');
        $('#divLogoutConf').addClass('divHide');
        $('#divFeedBkMsg').addClass('divHide');
        $('.darkPanelMTop').css({ 'margin-top': '90px', 'width': '550px', 'height': '443px' });
        $('.holderInner').css({ 'width': '535px', 'height': '400px' });
        $('#lblTitlePl').text('Change Password');
        event.preventDefault();
        return false;
    }
    else if (flag == 'F')//feedback  ALT+F
    {
        $('#divboxBg').show();
        $('#divPassChangeMsg').addClass('divHide');
        $('#tblpassTextBox').addClass('divHide');
        $('#divFeedback').removeClass('divHide');
        $('#divFeedBkMsg').removeClass('divHide');
        $('#divLogoutConf').addClass('divHide');
        $('.darkPanelMTop').css({ 'margin-top': '90px', 'width': '545px', 'height': '443px' });
        $('.holderInner').css({ 'width': '530px', 'height': '400px' });
        $('#lblTitlePl').text('Feedback');
        event.preventDefault();
        return false;
    }
    else if (flag == 'C')//Calculator
    {
        $('#divboxBg').show();
        event.preventDefault();
        return false;
    }
    else if (flag == 'R')//Report issue
    {

        MasterPageServices.CaptureScreen(SetSucceed, SetFailed);

        //$.ajax({
        //     type: "POST",
        //     url: "../WebServices/MasterPageServices.asmx/CaptureScreen",
        //     data: "{}",
        //     contentType: "application/json; charset=utf-8",
        //     dataType: "json",
        //     async: true,
        //     success: SetSucceed,
        //     error: SetFailed
        // });


        //$('#divBoxCRM').show();
        //$('.darkPanelMTop').css({ 'margin-top': '60px', 'width': '1200px', 'height': '573px' });
        //$('.holderInner').css({ 'width': '1185px', 'height': '530px' });
        //$('#spCRMTitlePl').text('Report Issue');

        event.preventDefault();
    
        
        //return false;
    
    }
}

function SetSucceed(result) {

    $('#hfCRM_ImgpathMaster').val(result);
    $("#btnrptIssueHidden").click();

}

function SetFailed() {
   
    $('#hfCRM_ImgpathMaster').val();
   
}

function HideFooter_win(FooterBox) {
    $(FooterBox).hide();
}

function ShowCRM_window()
{
    $('#divBoxCRM').show();
    $('.darkPanelMTop').css({ 'margin-top': '10px', 'width': '1200px', 'height': '563px' });
    $('.holderInner').css({ 'width': '1185px', 'height': '520px' });
    $('#spCRMTitlePl').text('Report Issue');
}