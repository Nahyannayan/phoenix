﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class OasisYearly_Reports_ASPX_rptProgramDetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Bind_Program()
            Bind_PIstatus()
            Bind_partner()
            Bind_Curiculum()
            OasisYearly.Oasis_Yearly.BindCountries(ddlCountry, True, "")
            BINDTYPE()
        End If
    End Sub
    Public Sub Bind_Program()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
        Dim Sql_Query = "SELECT PROGRAM_ID,PROGRAM_NAME FROM dbo.YEARLY_PROGRAM"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        chkl_program.DataSource = ds
        chkl_program.DataTextField = "PROGRAM_NAME"
        chkl_program.DataValueField = "PROGRAM_ID"
        chkl_program.DataBind()
        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStates(this," & ds.Tables(0).Rows.Count & " );")
        chkl_program.Items.Insert(0, list)
    End Sub
    Public Sub Bind_partner()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
        Dim Sql_Query = "SELECT PARNER_ID,PARTNER_NAME FROM PARTNER_MASTER ORDER BY PARTNER_NAME "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        chkl_partner.DataSource = ds
        chkl_partner.DataTextField = "PARTNER_NAME"
        chkl_partner.DataValueField = "PARNER_ID"
        chkl_partner.DataBind()
        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStates(this," & ds.Tables(0).Rows.Count & " );")
        chkl_partner.Items.Insert(0, list)
    End Sub
    Sub Bind_PIstatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
        Dim Sql_Query = "select * from dbo.PI_STATUS_MASTER order by PI_STATUS_ID "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        chkl_PIStatus.DataSource = ds
        chkl_PIStatus.DataTextField = "PI_STATUS_DESC"
        chkl_PIStatus.DataValueField = "PI_STATUS_ID"
        chkl_PIStatus.DataBind()

        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStates(this," & ds.Tables(0).Rows.Count & " );")

        chkl_PIStatus.Items.Insert(0, list)
    End Sub
    Public Sub Bind_Curiculum()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
        Dim Sql_Query = "SELECT DISTINCT CURRICULUM,CURRICULUM FROM PI_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        chkl_curriculam.DataSource = ds
        chkl_curriculam.DataTextField = "CURRICULUM"
        chkl_curriculam.DataValueField = "CURRICULUM"
        chkl_curriculam.DataBind()
        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStates(this," & ds.Tables(0).Rows.Count & " );")

        chkl_curriculam.Items.Insert(0, list)
    End Sub
    Sub BINDTYPE()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
        Dim Sql_Query = "SELECT TYPE_ID,TYPE_DESC FROM PI_TYPE_MASTER ORDER BY TYPE_DESC "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        chkl_type.DataSource = ds
        chkl_type.DataTextField = "TYPE_DESC"
        chkl_type.DataValueField = "TYPE_ID"
        chkl_type.DataBind()
        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStates(this," & ds.Tables(0).Rows.Count & " );")

        chkl_type.Items.Insert(0, list)
    End Sub
    Sub BIND_DETAILS(ByVal opt As String)
        Try

            Dim param As New Hashtable
            Dim strBprogram As New StringBuilder
            For Each lst As ListItem In chkl_program.Items
                If lst.Value <> "-1" Then
                    If lst.Selected = True Then
                        strBprogram.Append(lst.Value)
                        strBprogram.Append("|")
                    End If
                End If
            Next

            Dim strBpartner As New StringBuilder
            For Each lst As ListItem In chkl_partner.Items
                If lst.Value <> "-1" Then
                    If lst.Selected = True Then
                        strBpartner.Append(lst.Value)
                        strBpartner.Append("|")
                    End If
                End If
            Next
            Dim strPIstatus As New StringBuilder
            For Each lst As ListItem In chkl_PIStatus.Items
                If lst.Value <> "-1" Then
                    If lst.Selected = True Then
                        strPIstatus.Append(lst.Value)
                        strPIstatus.Append("|")
                    End If
                End If
            Next
            Dim strcurriculam As New StringBuilder
            For Each lst As ListItem In chkl_curriculam.Items
                If lst.Value <> "-1" Then
                    If lst.Selected = True Then
                        strcurriculam.Append(lst.Value)
                        strcurriculam.Append("|")
                    End If
                End If
            Next
            Dim strtype As New StringBuilder
            For Each lst As ListItem In chkl_type.Items
                If lst.Value <> "-1" Then
                    If lst.Selected = True Then
                        strtype.Append(lst.Value)
                        strtype.Append("|")
                    End If
                End If
            Next
            Dim strICM As New StringBuilder
            For Each lst As ListItem In chkl_ICM.Items
                If lst.Value <> "-1" Then
                    If lst.Selected = True Then
                        strICM.Append(lst.Value)
                        strICM.Append("|")
                    End If
                End If
            Next
            If opt = "R" Then
                param.Add("@PROGRAM_ID", IIf(strBprogram.ToString = "", DBNull.Value, strBprogram.ToString.Trim))
                param.Add("@PI_STATUS", IIf(strPIstatus.ToString = "", DBNull.Value, strPIstatus.ToString.Trim))
                param.Add("@TYPE", IIf(strtype.ToString = "", DBNull.Value, strtype.ToString.Trim))
                param.Add("@PARTNER", IIf(strBpartner.ToString.Trim = "", DBNull.Value, strBpartner.ToString.Trim))
                param.Add("@CURRICULUM", IIf(strcurriculam.ToString = "", DBNull.Value, strcurriculam.ToString.Trim))
                param.Add("@ICM_STATUS", IIf(strICM.ToString = "", DBNull.Value, strICM.ToString.Trim))
                param.Add("@CTY_ID", IIf(ddlCountry.SelectedValue = "-1", DBNull.Value, ddlCountry.SelectedValue))
                param.Add("@Comments", IIf(chkb_comments.Checked = True, "1", "0"))
                param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                param.Add("UserName", Session("sUsr_name"))
                'param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                'param.Add("UserName", Session("sUsr_name"))
                Dim rptClass As New rptClass
                With rptClass
                    .reportPath = Server.MapPath("../RPT/rptProgramDetails.rpt")
                    .crDatabase = "OASIS_YEARLY"
                    .reportParameters = param
                End With
                Session("rptClass") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ElseIf opt = "E" Then
                ''DataOption M-Mobile , E-Email 
                Dim parm(10) As SqlClient.SqlParameter

                Dim ds As DataSet
                Dim dt As DataTable
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
                parm(0) = New SqlClient.SqlParameter("@PI_STATUS", IIf(strPIstatus.ToString = "", DBNull.Value, strPIstatus.ToString.Trim))
                parm(1) = New SqlClient.SqlParameter("@TYPE", IIf(strtype.ToString = "", DBNull.Value, strtype.ToString.Trim))
                parm(2) = New SqlClient.SqlParameter("@PARTNER", IIf(strBpartner.ToString.Trim = "", DBNull.Value, strBpartner.ToString.Trim))
                parm(3) = New SqlClient.SqlParameter("@CURRICULUM", IIf(strcurriculam.ToString = "", DBNull.Value, strcurriculam.ToString.Trim))
                parm(4) = New SqlClient.SqlParameter("@ICM_STATUS", IIf(strICM.ToString = "", DBNull.Value, strICM.ToString.Trim))
                parm(5) = New SqlClient.SqlParameter("@CTY_ID", IIf(ddlCountry.SelectedValue = "-1", DBNull.Value, ddlCountry.SelectedValue))
                parm(6) = New SqlClient.SqlParameter("@PROGRAM_ID", IIf(strBprogram.ToString = "", DBNull.Value, strBprogram.ToString.Trim))
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EXPORTEXCEL_PROGRAM_DETAILS", parm)
                dt = ds.Tables(0)

                ' Create excel file.
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile

                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Pipeline")
                ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                '  ws.HeadersFooters.AlignWithMargins = True
                'Response.Clear()
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment;filename=" + "BDPipeline.xlsx")
                'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)
                'Response.End()

                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                Dim pathSave As String
                pathSave = "Data.xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btn_report_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_report.Click
        BIND_DETAILS("R")

    End Sub

    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        BIND_DETAILS("E")

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
