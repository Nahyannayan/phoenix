﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptProgramDetails.aspx.vb" Inherits="OasisYearly_Reports_ASPX_rptProgramDetails" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language ="javascript" type ="text/javascript" >




 function ChangeAllCheckBoxStates(checkedBox,Cnt)
        {
            
          var ChkState = false;
          if (checkedBox.checked)
          {
          ChkState = true
          }
                      
              for (var i = 0; i < Cnt; i++)
              {              
              var ChkId = checkedBox.id 
                                          
              ChkId = ChkId.substring(ChkId,ChkId.length - 1)
              ChkId = ChkId + (i + 1)           
              document.getElementById(ChkId).checked = ChkState
              
              }
          //checkedBox.checked = false;
         }

</script>
    <table class="BlueTable" width="70%">
        <tr class ="matters">
            <td class="subheader_img" colspan="6">
                Project Initiation Report</td>
        </tr>     
        <tr class ="matters" align="left">
            <td>
                Programs</td>
            <td>
                :</td>
            <td>
                <asp:Panel ID="Panel5" runat="server" Height="100px" ScrollBars="Auto">
                    <asp:CheckBoxList ID="chkl_program" runat="server">
                    </asp:CheckBoxList>
                </asp:Panel>
            </td>
            <td>
               Partner</td>
            <td width="1px">
                :</td>
            <td>
       
                <asp:Panel ID="Panel1" runat="server" Height="100px" ScrollBars="Auto">
                    <asp:CheckBoxList ID="chkl_partner" runat="server">
                    </asp:CheckBoxList>
                </asp:Panel>
       
            </td>
        </tr>     
        <tr class ="matters" align="left">
            <td>
                Curriculum</td>
            <td>
                :</td>
            <td>
                <asp:Panel ID="Panel3" runat="server" Height="100px" ScrollBars="Auto">
                    <asp:CheckBoxList ID="chkl_curriculam" runat="server">
                    </asp:CheckBoxList>
                </asp:Panel>
            </td>
            <td>
                Type</td>
            <td width="1px">
                :</td>
            <td>
                <asp:Panel ID="Panel4" runat="server" Height="100px" ScrollBars="Auto">
                    <asp:CheckBoxList ID="chkl_type" runat="server">
                    </asp:CheckBoxList>
                </asp:Panel>
            </td>
        </tr>     
        <tr class ="matters" align="left">
            <td>
                PI Status</td>
            <td>
                :</td>
            <td>
                         <asp:Panel ID="Panel2" runat="server" Height="100px" ScrollBars="Auto">
                    <asp:CheckBoxList ID="chkl_PIStatus" runat="server">
                    </asp:CheckBoxList>
                </asp:Panel>
            </td>
            <td>
                ICM Approval Status</td>
            <td width="1px">
                :</td>
            <td>
                <asp:CheckBoxList ID="chkl_ICM" runat="server">
                    <asp:ListItem Selected="True" Value="Yes">Yes</asp:ListItem>
                    <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                </asp:CheckBoxList>
            </td>
        </tr>     
        <tr class ="matters" align="left">
            <td>
                See&nbsp; Comments</td>
            <td>
                :</td>
            <td>
                <asp:CheckBox ID="chkb_comments" runat="server" Text="Yes" />
            </td>
            <td>
                 Country</td>
            <td width="1px">
                :</td>
            <td>
                <asp:DropDownList ID="ddlCountry" runat="server">
                </asp:DropDownList>
                </td>
        </tr>     
        <tr class ="matters">
            <td align="center" colspan="6">
                <asp:Button ID="btn_report" runat="server" CssClass="button" 
                    Text="Generate Report" />
            &nbsp;<asp:Button ID="btn_export" runat="server" CssClass="button" 
                    Text="Export To Excel" />
            </td>
        </tr>     
    </table>
    <script language ="javascript" type ="text/javascript" >
    function check()
    {
        for(i=0; i<document.forms[0].elements.length; i++)
        {
            var currentid =document.forms[0].elements[i].id; 
            if(document.forms[0].elements[i].type=="checkbox" )
            {
                document.forms[0].elements[i].checked=true;
            }
        }
    }
    check()
//    && currentid.indexOf(value)==-1
    </script>
</asp:Content>

