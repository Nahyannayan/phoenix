<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InsertImages.aspx.vb" Inherits="InsertImages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 465px">
        <tr>
            <td  style="height: 79px">
                &nbsp;
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
         <td align="center"  valign="bottom" style="height: 20px">
             <asp:Label ID="lblImg" runat="server" Width="176px"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="height: 93px; font-weight: bold;" valign="top">
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    style="width: 348px">
                    <tr class="subheader_img">
                        <td align="left" colspan="9" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                              Subject Master</span></font></td>
                    </tr>
                    
                         <tr>
                        <td align="left" class="matters" style="width: 244px">
                            Image</td>
                        <td class="matters" style="width: 46px">
                            :</td>
                        <td align="left" class="matters" style="width: 347px" colspan="4">
                            &nbsp;<asp:FileUpload ID="flImage" runat="server" Width="341px" /></td>          
                    </tr>
                    
                    <tr>
                     <td style="width: 219px; height: 24px" class="matters">
                    <asp:Label ID="lblShort" runat="server" Text="BusinessUnit" Width="122px" style="text-align: left"></asp:Label></td>
                    <td class="matters" style="width: 46px">
                            :</td>
            <td align="left" style="width: 50px; height: 24px;" >
                <asp:DropDownList ID="ddlBsu" runat="server" Width="399px">
                </asp:DropDownList></td>
          
          
                        <td align="left" class="matters" style="width: 244px">
                            Type</td>
                        <td class="matters" style="width: 46px">
                            :</td>
                        <td align="left" class="matters" style="width: 347px">
          <asp:TextBox ID="TextBox1" runat="server" CausesValidation="True" ValidationGroup="groupM1" TabIndex="2" Width="94px" MaxLength="3">LOGO</asp:TextBox>&nbsp;</td>          
                    </tr>
                                       
                     </table>
               </td>
        </tr>
       
        <tr>
         <td align="center" style="height: 24px">  <asp:Button  ID="btnSave" runat="server" CssClass="button" Text="Save"  TabIndex="7" Width="86px" />
       </td></tr>
        
    </table>
    </div>
    </form>
</body>
</html>
