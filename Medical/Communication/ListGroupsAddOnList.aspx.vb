﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_SMS_gpsListGroupsAddOnList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            BindGrid(Request.QueryString("gpid").ToString())

        End If

        If GridInfo.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.FooterRow.FindControl("btnexport"), Button))

        End If

    End Sub


    Public Sub BindGrid(ByVal Groupid As String, Optional ByVal export As Boolean = False)

        Dim str_conn = ""

        str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim query = " select * from COM_V2_GROUP_MASTER where GROUP_ID='" & Groupid & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        query = ds.Tables(0).Rows(0).Item("GROUP_QUERY").ToString()
        Dim aonid = ds.Tables(0).Rows(0).Item("GROUP_ADD_ON_IDS").ToString()
        Dim gtype = ds.Tables(0).Rows(0).Item("GROUP_TYPE").ToString()


        If Request.QueryString("mid") = "MEDICAL" Then
            str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString
        Else
            str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        End If
 
        If ds.Tables(0).Rows.Count > 0 Then

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

            If gtype = "AD" Then

                Dim filter = "USR_ID in (" & aonid & ")"
                Dim dvwView As New DataView
                dvwView = ds.Tables(0).DefaultView
                dvwView.RowFilter = filter

                GridInfo.DataSource = dvwView
                GridInfo.DataBind()
                lbltotal.Text = " Total Count :" & dvwView.ToTable.Rows.Count

                If export Then
                    Dim dt As DataTable = dvwView.ToTable
                    ExportExcel(dt)
                End If

            Else

                GridInfo.DataSource = ds
                GridInfo.DataBind()
                lbltotal.Text = " Total Count :" & ds.Tables(0).Rows.Count

                If export Then
                    Dim dt As DataTable = ds.Tables(0)
                    ExportExcel(dt)
                End If

            End If

        End If

    End Sub


    Public Sub exporting(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid(Request.QueryString("gpid").ToString(), True)
    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub


    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid(Request.QueryString("gpid").ToString())
    End Sub

 
End Class
