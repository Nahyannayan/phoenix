﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ListGroupsAddOnList.aspx.vb" Inherits="Transport_GPS_Tracking_SMS_gpsListGroupsAddOnList" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
 <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
 <asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server" >
 
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" 
    width="800">
    <tr>
        <td class="subheader_img">
         Group List &nbsp;
            <asp:Label ID="lbltotal" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
           <asp:GridView ID="GridInfo" runat="server"  ShowFooter="true" 
                AutoGenerateColumns="false"   
                Width="100%" AllowPaging="True" PageSize="20">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                     User ID
                              
                              
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("USR_ID")%>
      <asp:HiddenField ID="HiddenUserid" Value='<%# Eval("USR_ID")%>' runat="server" />
                                </center>
                        </ItemTemplate>
                        <FooterTemplate>
                        <center>
                        <asp:Button ID="btnexport" runat="server" CssClass="button" OnClick="exporting" Text="Export" />
                        </center>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                       User Name
                                         

                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                           
                               <%# Eval("NAME")%> 
                              
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Details
                                         

                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                           <%# Eval("DETAILS")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Contact Name
                                            

                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                           
                           <%# Eval("CONTACT_NAME")%>
                             
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Contact No
                                             

                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%# Eval("MOBILE")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Email Id
                                           

                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            
                            <%# Eval("EMAIL")%>
                               
                        </ItemTemplate>
                    </asp:TemplateField>
                 
                </Columns>
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView></td>
    </tr>
</table>
 
 </asp:Content> 

    
           
    

