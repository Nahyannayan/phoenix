﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSendLog.aspx.vb" Inherits="Transport_GPS_Tracking_Communication_xml_comSendLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


        function Opnewindow(logid) {

            window.showModalDialog("comPreviewSentData.aspx?logid=" + logid, "", "dialogWidth:800px; dialogHeight:600px; center:yes");


        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
            <tr>
                <td class="subheader_img">
                    Send Report &nbsp;
                    <asp:LinkButton ID="Linkexport" runat="server">Export</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                        PageSize="20" EmptyDataText="Information not available." Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                ID
                                                <br />
                                                <asp:TextBox ID="Txt1" Width="80px" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                    ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search"
                                                    ImageUrl="~/Images/forum_search.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("SEND_UNIQUE_ID")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Name
                                                <br />
                                                <asp:TextBox ID="Txt2" Width="80px" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                    ID="ImageSearch2" CausesValidation="false" runat="server" CommandName="search"
                                                    ImageUrl="~/Images/forum_search.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("NAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                To
                                                <br />
                                                <asp:TextBox ID="Txt3" Width="80px" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                    ID="ImageSearch3" CausesValidation="false" runat="server" CommandName="search"
                                                    ImageUrl="~/Images/forum_search.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("SEND_MESSAGE_TO")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Response
                                                <br />
                                                <asp:DropDownList ID="Dropresponce" AutoPostBack="true" OnSelectedIndexChanged="BindGridsearch"  runat="server">
                                                    <asp:ListItem Value="-1" Text="ALL"></asp:ListItem>
                                                    <asp:ListItem Value="ERROR" Text="ERROR"></asp:ListItem>
                                                    <asp:ListItem Value="SUCCESS" Text="SUCCESS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("SHOW_STATUS")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Date Time
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("SEND_DATE")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Message
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="LinkView" ToolTip='<%#Eval("SEND_RESPONCE")%>' OnClientClick='<%#Eval("Opnewindow")%>' runat="server">View</asp:LinkButton>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
