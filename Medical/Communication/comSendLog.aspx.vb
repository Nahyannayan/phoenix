﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Transport_GPS_Tracking_Communication_xml_comSendLog
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindGrid(False)
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindGrid(ByVal export As Boolean)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim sql_query = ""
        Dim ds As DataSet

        Dim useid = ""
        Dim name = ""
        Dim status = ""
        Dim sendto = ""

        If GridInfo.Rows.Count > 0 Then
            useid = DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            name = DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            sendto = DirectCast(GridInfo.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()
            status = DirectCast(GridInfo.HeaderRow.FindControl("Dropresponce"), DropDownList).SelectedValue
        End If

        Dim usertype = Request.QueryString("usertype")

        If usertype = "STUDENT" Then
            sql_query = " SELECT SEND_UNIQUE_ID,ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') NAME,SEND_MESSAGE_TO,SEND_RESPONCE,CONVERT(VARCHAR(20),SEND_DATE, 100)SEND_DATE,CASE CHARINDEX('Error',SEND_RESPONCE) WHEN  1  THEN 'ERROR' ELSE 'SUCCESS' END SHOW_STATUS,'javascript:Opnewindow('''+ convert(varchar,log_id) +''');return false;' Opnewindow FROM dbo.VV_STUDENT_M A " & _
                    " INNER JOIN  dbo.COM_V2_MESSAGE_SEND_LOG B ON A.STU_NO= B.SEND_UNIQUE_ID WHERE SEND_ID='" & Request.QueryString("send_id") & "'  "


        ElseIf usertype = "STAFF" Then
            sql_query = " SELECT SEND_UNIQUE_ID,ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') NAME,SEND_MESSAGE_TO,SEND_RESPONCE,CONVERT(VARCHAR(20),SEND_DATE, 100)SEND_DATE,CASE CHARINDEX('Error',SEND_RESPONCE) WHEN  1  THEN 'ERROR' ELSE 'SUCCESS' END SHOW_STATUS,'javascript:Opnewindow('''+ convert(varchar,log_id) +''');return false;' Opnewindow FROM dbo.VW_EMPLOYEE_M A " & _
                    " INNER JOIN  dbo.COM_V2_MESSAGE_SEND_LOG B ON A.EMPNO= B.SEND_UNIQUE_ID WHERE SEND_ID='" & Request.QueryString("send_id") & "'  "

        Else
            sql_query = " SELECT SEND_UNIQUE_ID,'' AS NAME,SEND_MESSAGE_TO,SEND_RESPONCE,CONVERT(VARCHAR(20),SEND_DATE, 100)SEND_DATE,CASE CHARINDEX('Error',SEND_RESPONCE) WHEN  1  THEN 'ERROR' ELSE 'SUCCESS' END SHOW_STATUS,'javascript:Opnewindow('''+ convert(varchar,log_id) +''')' Opnewindow FROM dbo.COM_V2_MESSAGE_SEND_LOG B  WHERE SEND_ID='" & Request.QueryString("send_id") & "'  "

        End If

        sql_query = "select * from (" & sql_query & ") AAA WHERE 1=1"

        If useid <> "" Then
            sql_query &= " AND SEND_UNIQUE_ID LIKE '%" & useid & "%'"
        End If
        If name <> "" Then
            sql_query &= " AND NAME LIKE '%" & name & "%'"
        End If
        If sendto <> "" Then
            sql_query &= " AND SEND_MESSAGE_TO LIKE '%" & sendto & "%'"
        End If

        If status <> "-1" And status <> "" Then
            sql_query &= " AND SHOW_STATUS='" & status & "'"
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        GridInfo.DataSource = ds
        GridInfo.DataBind()

        If GridInfo.Rows.Count > 0 Then

            DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text = useid
            DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text = name
            DirectCast(GridInfo.HeaderRow.FindControl("Txt3"), TextBox).Text = sendto
            DirectCast(GridInfo.HeaderRow.FindControl("Dropresponce"), DropDownList).SelectedValue = status

        End If

        If export Then
            ExportExcel(ds.Tables(0))
        End If

    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '  ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging

        GridInfo.PageIndex = e.NewPageIndex
        BindGrid(False)

    End Sub

    Public Sub BindGridsearch(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid(False)
    End Sub

    Protected Sub Linkexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkexport.Click
        BindGrid(True)
    End Sub

    Public Sub BindGridsearch()
        BindGrid(False)
    End Sub
    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "search" Then
            BindGrid(False)
        End If

    End Sub
End Class
