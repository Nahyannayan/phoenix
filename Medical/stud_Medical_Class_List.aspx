﻿<%@ Page Title="Medical Class List" Language="VB" MasterPageFile="~/Medical/MedicalMasterPage.master" AutoEventWireup="false" CodeFile="stud_Medical_Class_List.aspx.vb" Inherits="Medical_stud_Medical_Class_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        $(document).ready(function () {
            ShowLoader();
            Get_Grade();

            $("#ddlGrade").change(function () {
                Get_Section($(this));
            });
            $("#ddlSection").change(function () {
                GetStudents();
            });


            $.extend($.expr[":"], {
                "containsIN": function (elem, i, match, array) {
                    return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });
            $('.search-field').keyup(function () {

                var filter = $(this).val().trim();
                if ($.isNumeric(filter) == false) {
                    $('.student-profile-section').find('[itemref]:not(:containsIN("' + filter + '"))').hide();
                    $('.student-profile-section').find('[itemref]:containsIN("' + filter + '")').show();
                }

                else {
                    $('.student-profile-section').find('[itemid]:not(:containsIN("' + filter + '"))').hide();
                    $('.student-profile-section').find('[itemid]:containsIN("' + filter + '")').show();
                }
             
                if ($('.student-profile-section').find('[itemid]:visible').length == 0) {
                    $('.no-found').show();
                }
                else {
                    $('.no-found').hide();
                }
            });
        });

        function OpenDetail(obj) {
            var url = '../Medical/stud_Medical_Detail.aspx?view_id=' + $(obj).attr('link').trim();
            window.open(url, '_blank');
        }
        function Get_Grade() {
            $('.no-found').hide();
            $.ajax({
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/Get_Grade")%>',
                dataType: "json",
                type: "POST",
                data: {},
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {

                    $.each(response.d, function (key, value) {
                        //$('#' + ObjID).append($("<option></option>").attr("value", value.Value).text(value.Text));
                        $('#ddlGrade').append('<option value = ' + value.Value + '>' + value.Text + '</option>');
                    });

                    $('#ddlGrade').selectpicker('refresh');

                },

                error: function (jqXHR, textStatus, errorThrown) {

                },

                complete: function (jqXHR, textStatus, errorThrown) {
                    Get_Section($('#ddlGrade'));
                }
            });
        }
        function Get_Section(grade) {
            $('.no-found').hide();

            var postdata = { GRD_ID: $(grade).val() };
            $.ajax({
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/Get_Section")%>',
                dataType: "json",
                type: "POST",
                data: JSON.stringify(postdata),
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {

                    $('#ddlSection').find('option').remove();
                    //$('#ddlSection').append('<option value = "">ALL</option>');
                    $('#ddlSection').selectpicker('refresh');
                  
                        $.each(response.d, function (key, value) {
                            $('#ddlSection').append('<option value = ' + value.Value + '>' + value.Text + '</option>');
                        });
                        $('#ddlSection').selectpicker('refresh');
                    
                   

                },
                complete: function (jqXHR, textStatus, errorThrown) {
                    GetStudents();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                      HideLoader();
                },
            });
        }
        function GetStudents() {
            ShowLoader();
            $('#search').val('');
            $('.no-found').hide();
            var PostData = {
                Grade: $('#ddlGrade').val(),
                Section: $('#ddlSection').val()
            }
            $.ajax({
                type: "POST",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/Get_Students")%>',
                data: JSON.stringify(PostData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response, item) {
                    debugger
                    $('#student-details').empty();
                    $.each(JSON.parse(response.d), function (index, value) {
                        if (value.STU_PHOTOPATH != '' && value.STU_PHOTOPATH != null) {
                            if ((value.STU_PHOTOPATH.trim().toLowerCase().indexOf(".jpg") >= 0) == false) {
                                value.STU_PHOTOPATH = '<%= ResolveUrl("~/Medical/assets/img/index.png") %>'
                            }
                        }

                        var HTML = '<div aria-controls="true" class="col-xl-2 col-lg-2 col-md-2 col-12 px-0 text-center border mb-2 bg-white" itemid="';
                        HTML += value.STU_NO + '"';
                        HTML += ' itemref="' + value.STU_NAME + '"';
                        HTML += ' >  <div class="medical-tag">';
                        if (value.MedicalCondition != null) {
                            HTML += value.MedicalCondition;
                        }
                        if (value.IncidentReport != null) {
                            HTML += value.IncidentReport;
                        }
                        HTML += ' </div>';
                        HTML += ' <div class="student-img img-radius border mt-3 mx-auto ">';
                        HTML += ' <a href="#">';
                        HTML += ' <img link=" ' + value.Encr_STU_ID + '"   onclick="OpenDetail(this)" src=" ' + value.STU_PHOTOPATH + ' " alt="' + value.STU_NAME + '"';
                        HTML += ' class="img-fluid"></a>';
                        HTML += ' </div>';
                        HTML += ' <div class="student-meta mt-3">';
                        HTML += ' <h3 class="mb-0"><a class="stu_name"  data-toggle="tooltip" title="' + value.STU_NAME + '" link="' + value.Encr_STU_ID + ' "  onclick="OpenDetail(this)" href="#"> ' + value.STU_NAME + ' </a></h3>';
                        HTML += ' <p class="mb-1">ID :';
                        HTML += value.STU_NO
                        HTML += ' </p>';
                        HTML += ' <h4 class="mb-0"> ' + value.GRD_DISPLAY + '</h4>';
                        HTML += ' </div>';
                        HTML += ' <div class="student-class-info d-flex justify-content-around border-top">';
                        HTML += ' <h3 class="m-0 w-100 pt-2 pb-2 border-right cursor-pointer" data-toggle="tooltip" title="Medical Profile"><a link="' + value.Encr_STU_ID + '  " onclick="OpenDetail(this)"';
                        HTML += ' href="#"><i class="fa fa-notes-medical  text-primary"></i></a></h3>';
                        HTML += ' <h3 class="m-0 w-100 pt-2 pb-2 border-right cursor-pointer d-none" data-toggle="tooltip" title="Medication History"><i class="fa fa-heartbeat text-primary"></i></h3>';
                        HTML += ' <h3 class="m-0 w-100 pt-2 pb-2 border-right cursor-pointer d-none" data-toggle="tooltip" title="Medical Incident"><i class="fa fa-ambulance text-primary"></i></h3>';
                        HTML += ' </div>';
                        HTML += ' </div>';

                        $('#student-details').append(HTML);
                    });
                    HideLoader();
                },
                failure: function (r) {
                    HideLoader();
                },
                error: function (response) {
                    HideLoader();
                },
                complete: function (response) {

                    $('[data-toggle="tooltip"]').tooltip({
                        //trigger: 'focus',
                        html: true,
                        animated: 'fade',
                    });
                    HideLoader();
                }
            });
        }
    </script>

    <style>
        .student-profile-section .student-meta {
            padding: 0 0.25rem;
            min-height: 130px;
        }

        .card .card-body h3 {
            font-weight: 500;
            line-height: 1.5rem;
        }

        .btn-default {
            background-color: white !important;
        }
    </style>


    <!-- student search filter starts here -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div class="card shadow-none">
                <div class="card-body column-padding">
                    <div class="row task-search p-2 border-0">
                        <div class="col-lg-4 col-12 m-auto">
                            <div class="GradeSection">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <div class="md-form md-outline mb-0">
                                            <select class="selectpicker" id="ddlGrade">
                                            </select>
                                            <label class="active select-label">Grade </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-12">
                                        <div class="md-form md-outline mb-0">
                                            <select class="selectpicker" id="ddlSection">
                                            </select>
                                            <label class="active select-label">Section </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 mt-5"></div>
                        <div class="col-lg-4 col-6 pull-right mt-1">
                            <div class="search-wrapper">
                                <i class="fas fa-search active"></i>
                                <input type="text" id="search" class="search-field" placeholder="Search Students">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- student search filter ends here -->
    <!-- student profile section start -->
    <div class="row student-profile-section">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="card bg-transparent border-0">
                <div class="card-body">
                    <div class="row ml-0 mr-0" id="student-details">
                        <!-- student details start -->
                        <!-- student details end -->
                    </div>
                    <div class="no-found" style="display: none">
                        <div class="student-meta mt-3">
                            <h3 class="mb-0" style="background-color: white; text-align: center;">No Record Found </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- student profile section end -->
</asp:Content>

