﻿<%@ Page Title="Student Medical Student" Language="VB" MasterPageFile="~/Medical/MedicalMasterPage.master" AutoEventWireup="false" CodeFile="stud_Medical_Detail.aspx.vb" Inherits="Medical_stud_Medical_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .health-label {
            font-size: 14px;
            color: #000;
            font-weight: bold;
        }
    </style>
    <script src='<%= ResolveUrl("~/Medical/assets/js/2.11.2moment.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Medical/assets/js/datetime-moment.js")%>'></script>



    <script type="text/javascript">


        $(document).ready(function () {

            ShowLoader();
            $('#pills-home').mCustomScrollbar({
                setHeight: "250",
                autoExpandScrollbar: true,
                scrollbarPosition: "inside",
                autoHideScrollbar: true,
            });
            $('#pills-contact').mCustomScrollbar({
                setHeight: "250",
                autoExpandScrollbar: true,
                scrollbarPosition: "inside",
                autoHideScrollbar: true,

            });
            $('#pills-profile').mCustomScrollbar({
                setHeight: "250",
                autoExpandScrollbar: true,
                scrollbarPosition: "inside",
                autoHideScrollbar: true,

            });
            $('#emergency-contact .card-body .content').mCustomScrollbar({
                setHeight: "200",
                autoExpandScrollbar: true,
                scrollbarPosition: "inside",
                autoHideScrollbar: true,

            });
            $('#contact-info .card-body .content').mCustomScrollbar({
                setHeight: "200",
                autoExpandScrollbar: true,
                scrollbarPosition: "inside",
                autoHideScrollbar: true,

            });

            GET_STU_DETAIL();
        });


        function GET_STU_DETAIL() {
            var STU_ID = '<%= Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+"))%>';

            var PostData = { STU_ID: STU_ID };
            $.ajax({
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GetStudent_Detail")%>',
                dataType: "json",
                type: "POST",
                data: JSON.stringify(PostData),
                contentType: "application/json; charset=utf-8",
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {

                    if (response.d != null) {

                        var ParseJSON = JSON.parse(response.d);
                        if (ParseJSON.STU_PHOTO != '' && ParseJSON.STU_PHOTO != null) {
                            if ((ParseJSON.STU_PHOTO.trim().toLowerCase().indexOf(".jpg") >= 0) == false) {
                                ParseJSON.STU_PHOTO = '<%= ResolveUrl("~/Medical/assets/img/index.png") %>'
                            }
                        }
                        $('#stu_img').attr('src', ParseJSON.STU_PHOTO);
                        $('#stu_name').text(ParseJSON.STU_NAME);
                        $('#stu_no').text(ParseJSON.STU_NO);
                        $('#stu_grd').text(ParseJSON.STU_GRD);

                        $('#stu_age').text(ParseJSON.STU_AGE);
                        $('#stu_dob').text(ParseJSON.STU_DOB);
                        $('#stu_gender').text(ParseJSON.STU_GENDER);


                        $('#pname').text(ParseJSON.STU_RNAME);
                        $('#prelation').text(ParseJSON.STU_RELATION);
                        $('#pnationality').text(ParseJSON.STU_RNationality);


                        //$('#paddress').text(ParseJSON.STU_RAddress);
                        //$('#pcity').text(ParseJSON.STU_RCity);
                        //$('#pcountry').text(ParseJSON.STU_RCountry);


                        $('#pemail').text(ParseJSON.STU_REmail);
                        $('#pcontactnumber').text(ParseJSON.STU_RContactNumber);
                        $('#poccupation').text(ParseJSON.STU_ROccupation);
                        $('#pcompany').text(ParseJSON.STU_RCompany);


                        $('#ecname').text(ParseJSON.STU_RNAME);
                        $('#ecnationality').text(ParseJSON.STU_RNationality);


                        //$('#ecaddress').text(ParseJSON.STU_RAddress);
                        //$('#eccity').text(ParseJSON.STU_RCity);
                        //$('#eccountry').text(ParseJSON.STU_RCountry);


                        $('#ecname').text(ParseJSON.STU_ECContactName);
                        $('#eccontactnumber').text(ParseJSON.STU_ECContactNumber);
                        //$('#ecoccupation').text(ParseJSON.STU_ROccupation);
                        //$('#eccompany').text(ParseJSON.STU_RCompany);



                        //health info
                        $('#lblIsAllergy').text(ParseJSON.STU_bALLERGIES);
                        $('#lblAllergyDetails').text(ParseJSON.STU_ALLERGIES);

                        $('#lblIsDisability').text(ParseJSON.STU_bDisabled);
                        $('#lblDisabilitydetails').text(ParseJSON.STU_Disabled);

                        $('#lblIsSpclMed').text(ParseJSON.STU_bRCVSPMEDICATION);
                        $('#lblMedNotes').text(ParseJSON.STU_SPMEDICATION);

                        $('#lblPedRestriction').text(ParseJSON.STU_bPHYSICAL);
                        $('#lblPedRestrictionNotes').text(ParseJSON.STU_PHYSICAL);

                        $('#lblspecialeducation').text(ParseJSON.STU_bSEN);
                        $('#lblspecialeducationNotes').text(ParseJSON.STU_SEN_REMARK);

                        if (ParseJSON.StudIncidentDetail != null)
                            $('.incident-count').text(ParseJSON.StudIncidentDetail.length);
                        else
                            $('.incident-count').text('0');
                        $('#incident').DataTable({
                            "rowId": "INCIDENT_ID",
                            "ordering": true,
                            "pageLength": 5,
                            "order": [[1, "desc"]],
                            "searching": true,
                            "drawCallback": function () {
                                var $api = this.api();
                                var pages = $api.page.info().pages;
                                if (pages == 0) {
                                    $('#incident_paginate').hide();
                                }
                                else {
                                    $('#incident_paginate').show();
                                }


                            },
                            "lengthChange": false,
                            "paging": true,
                            "data": ParseJSON.StudIncidentDetail,
                            "columns": [
                                { "data": "INCIDENT_ID" },
                                { "data": "INCIDENT_DATE" },
                                { "data": "LOC_DESCR" },
                                { "data": "INJURED_AREA_DESCR" },
                                { "data": "SYMPTOMS_DESCR" }
                            ],
                            // "info": false
                            columnDefs: [{
                                orderable: true
                            }, {
                                "targets": [0],
                                className: "d-none"
                            }, {
                                "targets": [1],
                                className: "td-incident-date"
                            },
                            {
                                "targets": [2],
                                className: "td-location"
                            },
                            {
                                "targets": [3],
                                className: "td-injured-area"
                            },
                            {
                                "targets": [4],
                                className: "td-symptoms"
                            }

                            ]
                        });
                        $('#incident tbody').on('click', 'tr', function () {

                            OpenModal(this);
                        });
                        if (ParseJSON.StudMedicationDetail != null)
                            $('.condition-count').text(ParseJSON.StudMedicationDetail.length);
                        else
                            $('.condition-count').text('0');
                        $('#medicalCondition').DataTable({
                            "rowId": "MEDICATION_ID",
                            "ordering": true,
                            "pageLength": 5,
                            "searching": true,
                            "drawCallback": function () {
                                var $api = this.api();
                                var pages = $api.page.info().pages;
                                //var rows = $api.data().length;

                                if (pages == 0) {

                                    $('#medicalCondition_paginate').hide();
                                }
                                else {
                                    $('#medicalCondition_paginate').show();
                                }

                            },
                            "lengthChange": false,
                            "paging": true,
                            "data": ParseJSON.StudMedicationDetail,
                            "columns": [
                                { "data": "MEDICATION_ID" },
                                { "data": "MEDICATION" },
                                { "data": "SIDEEFFECTS" },
                                { "data": "CONDITIONDESCR" },

                            ],
                            // "info": false
                            columnDefs: [{
                                orderable: true
                            }, {
                                "targets": [0],
                                className: "d-none"
                            }, {
                                "targets": [1],
                                className: "td-medical-condition"
                            },
                            {
                                "targets": [2],
                                className: "td-sideeffects"
                            },
                            {
                                "targets": [3],
                                className: "td-condition"
                            },]
                        });
                        $('#medicalCondition tbody').on('click', 'tr', function () {

                            OpenMedicalModal(this);
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                },
                complete: function () {
                    HideLoader();
                },
            });
        }

        function OpenModal(tr) {
            Incident_ID = $(tr).find('.d-none').text();
            if (Incident_ID != "" && Incident_ID != "0") {
                $('#medicalIncidentModal').attr('incident_id', Incident_ID);
                $('#medicalIncidentModal').modal('show');
                Get_Incident_Data(Incident_ID);
            }
        }

        function OpenMedicalModal(tr) {
            Medication_ID = $(tr).find('.d-none').text();
            if (Medication_ID != "" && Medication_ID != "0") {
                $('#medicalConditionModal').attr('Medication_ID', Medication_ID);
                $('#medicalConditionModal').modal('show');
                $('.multiple-row').not(':last').remove();
                Get_Medical_Data(Medication_ID);
            }
        }

    </script>
    <div class="row d-none">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div class="card shadow-none p-2">
                <div class="card-body column-padding">
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-12 d-none">
                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit"><i class="fa fa-pencil-alt pr-2 text-white"></i>Student Details</button>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-12 d-none">
                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit"><i class="fa fa-ambulance pr-2 text-white"></i>Ambulance</button>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-12">
                            Incident_History
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- student profile section start -->
    <div class="row student-profile-section">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row ml-0">
                        <!-- student details start -->
                        <div class="col-xl-3 col-lg-4 col-md-4 col-12 px-0 text-center border-right">
                            <div class="student-img rounded-circle border mt-3 mx-auto">
                                <img id="stu_img" class="img-fluid" />
                            </div>
                            <div class="student-meta mt-3">
                                <h3><span id="stu_name"></span></h3>
                                <p class="mb-1"><strong>ID</strong> : <span id="stu_no"></span></p>
                                <p><strong>Class</strong> : <span id="stu_grd"></span></p>
                            </div>
                            <div class="student-class-info d-flex justify-content-around border-top">
                                <h3 class="m-0 w-100 pt-2 pb-2 border-right"><span class="d-block mb-1">Date Of Birth</span> <span id="stu_dob" class="h3_value"></span></h3>
                                <h3 class="m-0 w-100 pt-2 pb-2 border-right"><span class="d-block mb-1">Gender</span><span id="stu_gender" class="h3_value">   </span></h3>
                                <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Age</span><span id="stu_age" class="h3_value">   </span></h3>
                            </div>
                        </div>
                        <!-- student details end -->
                        <!-- student other details start -->
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12 px-0 text-center border-right">
                            <div class="student-parent-info">
                                <div class="accordion text-left" id="studentParentDetails">
                                    <div class="card z-depth-0 m-0 border-bottom border-top-0 border-left-0 border-right-0">
                                        <div class="card-header" id="parentInfo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link d-block text-left m-0 p-0 collapse" type="button" data-toggle="collapse" data-target="#parent-info" aria-expanded="true" aria-controls="parent-info">
                                                    Parent information <i class="fas fa-angle-down float-right m-0"></i>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="parent-info" class="collapse show border-top" aria-labelledby="parentInfo" data-parent="#studentParentDetails">
                                            <div class="card-body column-padding pt-2 pb-2">
                                                <div class="content">
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Name</span><span id="pname" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Relation</span><span id="prelation" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Nationality</span><span id="pnationality" class="h3_value"> </span></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card z-depth-0 m-0 border-bottom border-top-0 border-left-0 border-right-0">
                                        <div class="card-header" id="contactInfo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link d-block text-left m-0 p-0 collapsed" type="button" data-toggle="collapse" data-target="#contact-info" aria-expanded="false" aria-controls="contact-info">
                                                    Contact information <i class="fas fa-angle-down float-right m-0"></i>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="contact-info" class="collapse border-top" aria-labelledby="contactInfo" data-parent="#studentParentDetails">
                                            <div class="card-body column-padding pt-2 pb-2">
                                                <div class="content">
                                                    <%--  <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Address</span><span id="paddress" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">City</span><span id="pcity" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Country</span><span id="pcountry" class="h3_value"> </span></h3>--%>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Email</span><a href="#"><span id="pemail" class="h3_value"> </span></a></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Contact number</span><span id="pcontactnumber" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Occupation</span><span id="poccupation" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Company</span><span id="pcompany" class="h3_value"> </span></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="d-none card z-depth-0 m-0">
                                        <div class="card-header" id="emergencyContact">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link d-block text-left m-0 p-0 collapsed" type="button" data-toggle="collapse" data-target="#emergency-contact" aria-expanded="false" aria-controls="sibling-info">
                                                    Emergency Contact <i class="fas fa-angle-down float-right m-0"></i>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="emergency-contact" class="collapse border-top" aria-labelledby="emergencyContact" data-parent="#studentParentDetails">
                                            <div class="card-body column-padding pt-2 pb-2">
                                                <div class="content">
                                                    <%--      <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Address</span><span id="ecaddress" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">City</span><span id="eccity" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Country</span><span id="eccountry" class="h3_value"> </span></h3>--%>
                                                    <h3 class="m-0 w-100 pt-2 pb-2 d-none"><span class="d-block mb-1">Contact Name</span><a href="#"><span id="ecname" class="h3_value"> </span></a></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Contact Number</span><span id="eccontactnumber" class="h3_value"> </span></h3>
                                                    <%--   <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Occupation</span><span id="ecoccupation" class="h3_value"> </span></h3>
                                                    <h3 class="m-0 w-100 pt-2 pb-2"><span class="d-block mb-1">Company</span><span id="eccompany" class="h3_value"> </span></h3>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- student other details end -->
                        <!-- student siblind details start -->
                        <div class="col-xl-5 col-lg-4 col-md-4 col-12 pl-0">
                            <div class="card border-0 attendance-widget">
                                <div class="card-body">
                                    <h3 class="card-title">Medical Record</h3>
                                    <!-- tab panels start -->
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="card">
                                                <div class="card-body p-2">
                                                    <div class="tabPanels">
                                                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                                                                    aria-controls="pills-contact" aria-selected="false">Incidents</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                                                                    aria-controls="pills-home" aria-selected="true">Medical Conditions</a>
                                                            </li>
                                                            <%-- <li class="nav-item">
                                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                                                    aria-controls="pills-profile" aria-selected="false">Medication Use</a>
                                                </li>--%>
                                                        </ul>
                                                        <div class="tab-content column-padding pt-3 pb-3" id="pills-tabContent">
                                                            <div class="tab-pane fade  show active table-responsive" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                                                <table id="incident" class="table table-bordered table-hover datatable bg-white align-items-center assesment-table" style="width: 100%;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="d-none">Incident Id</th>
                                                                            <th class="td-incident-date" style="width: 25%;"><small><strong>INCIDENT DATE & TIME</strong></small></th>
                                                                            <th class="td-location" style="width: 25%;"><small><strong>LOCATION</strong></small> </th>
                                                                            <th class="td-injured-area" style="width: 25%;"><small><strong>INJURED AREA</strong></small></th>
                                                                            <th class="td-symptoms" style="width: 25%;"><small><strong>SYMPTOMS</strong></small></th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade table-responsive" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                                                <table id="medicalCondition" class="table table-bordered table-hover datatable bg-white align-items-center assesment-table" style="width: 100%;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="d-none">MEDICAL ID</th>
                                                                            <th class="text-left" style="width: 20%;"><small><strong>MEDICAL CONDITION</strong></small></th>
                                                                            <th class="text-left" style="width: 40%;"><small><strong>SIDE EFFECTS OF DOSAGE</strong></small></th>
                                                                            <th class="text-left" style="width: 40%;"><small><strong>CONDITION</strong></small></th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- tab panels end -->
                                </div>
                            </div>
                        </div>
                        <!-- student sibling details end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- student profile section end -->
    <!-- statistics section start -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div class="card bg-transparent shadow-none">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-9 col-lg-9 col-md-8 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h3 class="card-title"><i class="fas fa-briefcase-medical pr-2"></i>Health Info</h3>
                                    <div class="pt-3 column-padding pb-2">
                                        <!-- Projects table -->
                                        <table class="table table-bordered table-hover align-items-center assesment-table footer">
                                            <thead>
                                                <th style="width: 310px;"><small><strong>Health Record</strong> </small></th>
                                                <th class="text-center" style="width: 150px;"><small><strong>Yes / No</strong> </small></th>
                                                <th class="text-center"><small><strong>Details</strong> </small></th>
                                            </thead>
                                            <tbody>
                                                <tr class="border-bottom">
                                                    <td><span class="health-label">Allergies</span></td>
                                                    <td class="text-center">
                                                        <label id="lblIsAllergy"></label>
                                                    </td>
                                                    <td><span class="text-dark">
                                                        <label id="lblAllergyDetails"></label>
                                                    </span>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td><span class="health-label">Disabilities</span></td>
                                                    <td class="text-center">
                                                        <label id="lblIsDisability"></label>
                                                    </td>
                                                    <td><span class=" text-dark">
                                                        <label id="lblDisabilitydetails"></label>
                                                    </span>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td><span class="health-label">Special Medication </span></td>
                                                    <td class="text-center">
                                                        <label id="lblIsSpclMed"></label>
                                                    </td>
                                                    <td><span class=" text-dark">
                                                        <label id="lblMedNotes"></label>
                                                    </span>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td><span class="health-label">Physical Education Restrictions  </span></td>
                                                    <td class="text-center">
                                                        <label id="lblPedRestriction"></label>
                                                    </td>
                                                    <td><span class=" text-dark">
                                                        <label id="lblPedRestrictionNotes"></label>
                                                    </span>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td><span class="health-label">Child have any special education needs</span></td>
                                                    <td class="text-center">
                                                        <label id="lblspecialeducation"></label>
                                                    </td>
                                                    <td><span class=" text-dark">
                                                        <label id="lblspecialeducationNotes"></label>
                                                    </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-12">
                            <div class="login-data medical-incident overflow-hidden position-relative bg-white p-3 mb-2">
                                <h2 class="incident-count"></h2>
                                <h2><span class="d-block">Medical Incident</span></h2>
                            </div>
                            <div class="login-data medical-condition overflow-hidden position-relative bg-white p-3 mb-2">
                                <h2 class="condition-count"></h2>
                                <h2><span class="d-block">Medical Condition</span></h2>
                            </div>
                            <div class="accordion text-left" id="sudentSiblingInfo">
                                <div class="card z-depth-0 m-0 border-top-0 border-left-0 border-right-0">
                                    <div class="card-header" id="siblingInfo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link d-block text-left m-0 p-0 collapse" type="button" data-toggle="collapse" data-target="#sibling-details" aria-expanded="true" aria-controls="sibling-details" style="pointer-events: none;">
                                                Other Medical Informations
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="sibling-details" class="collapse show border-top" aria-labelledby="siblingInfo" data-parent="#sudentSiblingInfo">
                                        <div class="card-body column-padding">
                                            <div class="content">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- statistics section end -->
    <div class="row d-none">
        <!-- Medical Information start -->
        <div class="col-lg-4 col-md-4 col-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Medical Information</h3>
                    <div class="column-padding pt-3 pb-3">
                    </div>
                </div>
            </div>
        </div>
        <!-- Medical Information end -->
        <!-- Activities section start -->
        <div class="col-lg-4 col-md-4 col-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Activities</h3>
                    <div class="column-padding pt-3 pb-3">
                    </div>
                </div>
            </div>
        </div>
        <!-- Activities section end -->
        <!-- Behavioral Details section end -->
        <div class="col-lg-4 col-md-4 col-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Behavioral Details</h3>
                    <div class="column-padding pt-3 pb-3">
                    </div>
                </div>
            </div>
        </div>
        <!-- Behavioral Details section end-->
    </div>
</asp:Content>
