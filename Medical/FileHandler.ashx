﻿<%@ WebHandler Language="VB" Class="FileHandler" %>


Imports System
Imports System.Web
Imports System.IO
Imports UtilityObj
Imports System.Web.Configuration
Imports System.Web.HttpContext
Imports System.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet

Public Class FileHandler : Implements IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState
    Dim Encr_decrData As New Encryption64


    Public ReadOnly Property IsReusable As Boolean Implements IHttpHandler.IsReusable
        Get
            Throw New NotImplementedException()
        End Get
    End Property


    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim path As String
        Try
            If context.Request.Files.Count > 0 Then
                Dim files As HttpFileCollection = context.Request.Files
                Dim Stu_NO = context.Request.Headers("Stu_NO")
                Dim UploadPath = WebConfigurationManager.AppSettings.Item("MedicationCondition")
                path = UploadPath & Stu_NO & "/"
                Dim File As HttpPostedFile = files(0)
                If Directory.Exists(path) = False Then
                    Directory.CreateDirectory(path)

                End If
                Dim NewFileName As String = Guid.NewGuid().ToString() + "-" + System.IO.Path.GetFileName(File.FileName)
                path = path & NewFileName
                File.SaveAs(path)
                Dim customHeader As New NameValueCollection
                customHeader.Add("originalfilename", System.IO.Path.GetFileName(File.FileName))
                customHeader.Add("filedir", Encr_decrData.Encrypt(Stu_NO & "/" & NewFileName))
                customHeader.Add("contenttype", GetContentType(System.IO.Path.GetExtension(path)))
                context.Response.Headers.Add(customHeader)

                context.Response.ContentType = "text/plain"
                context.Response.Write("File Uploaded Successfully!")
            End If
        Catch ex As Exception
            HttpContext.Current.Response.Headers.Add("Error", "Error")
            UtilityObj.Errorlog(ex.Message, "uploadfile")
        End Try
    End Sub






    Private Function GetContentType(ByVal FileExtension As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Dim ContentType As String = ""
        Select Case FileExtension
            Case ".doc"
                ContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                ContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                ContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                ContentType = "image/jpg"
                Exit Select
            Case ".png"
                ContentType = "image/png"
                Exit Select
            Case ".gif"
                ContentType = "image/gif"
                Exit Select
            Case ".pdf"
                ContentType = "application/pdf"
                Exit Select
        End Select
        Return ContentType
    End Function



End Class