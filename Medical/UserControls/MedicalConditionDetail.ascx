﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MedicalConditionDetail.ascx.vb" Inherits="Medical_UserControls_MedicalConditionDetail" %>
<link href="../Medical/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../Medical/assets/css/addons/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="../Medical/assets/css/addons/jquery.minicolors.css" rel="stylesheet" type="text/css">
<link href="../Medical/assets/css/addons/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
<!-- dataTable css -->
<link href="../Medical/assets/css/addons/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../Medical/assets/css/addons/datatables-select.min.css" rel="stylesheet" type="text/css">
<!-- Material Design Bootstrap -->
<link href="../Medical/assets/css/main-medical.css" rel="stylesheet" type="text/css">


<style type="text/css">
    .ui-front {
        z-index: 10000;
    }

    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        cursor: default;
    }

    .ui-widget-content {
        border: 1px solid #dddddd;
        background: #eeeeee url(images/ui-bg_highlight-soft_100_eeeeee_1x100.png) 50% top repeat-x;
        color: #333333;
    }
</style>
<script type="text/javascript" src="../Medical/assets/js/addons/bootstrap-select.js"></script>
<script type="text/javascript" src="../Medical/assets/js/popper.min.js"></script>
<script type="text/javascript" src="../Medical/assets/js/bootstrap.min.js"></script>
<!-- chart js -->
<!-- <script type="text/javascript" src="js/charts.js"></script> -->
<!-- custom select js -->
<script type="text/javascript" src="../Medical/assets/js/addons/moment-with-locales.js"></script>
<script type="text/javascript" src="../Medical/assets/js/addons/particles.min.js"></script>
<!-- date-time And color picker js -->
<script type="text/javascript" src="../Medical/assets/js/addons/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="../Medical/assets/js/addons/jquery.minicolors.min.js"></script>
<script type="text/javascript" src="../Medical/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- dataTable js -->
<script src="../Medical/assets/js/datatables.min.js" type="stylesheet"></script>
<script type="text/javascript">

    var Medication_ID = '0';

    function ViewAttachments(ID) {
        var postData = { MEDICATION_ID: ID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GET_ATTACHMENTS_BY_ID")%>',
            data: JSON.stringify(postData),
            dataType: "json",
            beforeSend: CheckSession(),
            success: function (data) {
                $('.view-image-table tbody').empty();
                if (data.d != "") {
                    JSON.parse(data.d).forEach(function (value, item) {
                        $('.view-image-table tbody').append('<tr><th scope="row" class="text-center">' + value.ATTACHFILENAME + '</th> <th class="text-center">  <i contenttype ="' + value.CONTENTTYPE + '" filename="' + value.ATTACHFILENAME + '"  file="' + value.FILEPATH + '"   href="#"  onclick="DownloadAttachment(this)" class="fa fa-file pr-2 text-primary"></i> </th></tr>');
                    });
                }
                else {
                    $('.view-image-table tbody').append('<tr><th scope="row" colspan="2" class="text-center"> No Attachment  </th></tr>');
                }
                $('#ModalAttachment').modal('show');
            },
            error: function (result) {
                BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Error occurred');
            },
        });
    }

    function ValidateMedical() {
        var returnval = true;
        var todayDate = new Date();
        if ($('#txtsearchforMedical').val().trim() == '') {
            $('#txtsearchforMedical').addClass('border-danger');
            BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            returnval = false;
        }
        else {
            var original_stu_name = $('#txtsearchforMedical').attr('item-data');
            var entered_stu_name = $('#txtsearchforMedical').val();
            if (original_stu_name != undefined && original_stu_name.trim() != entered_stu_name.trim()) {
                $('#txtsearchforMedical').addClass('border-danger');
                $('#txtsearchforMedical-Invalid-togglePopup').show();
                returnval = false;
            }
        }
        if ($('#txtsearchforMedical').attr('item-value') == '' || $('#txtsearchforMedical').attr('item-value') == undefined) {
            $('#txtsearchforMedical').addClass('border-danger');
            BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            returnval = false;
        }


        if ($('#medicalConditionName').val().trim() == '') {
            $('#medicalConditionName').addClass('border-danger');
            BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            returnval = false;
        }
        if ($('#txtsideeffects').val().trim() == '') {
            $('#txtsideeffects').addClass('border-danger');
            BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            returnval = false;
        }

        $('.multiple-row').each(function (key, value) {


            if ($(value).find('[itemid="txtnameofmed-"]:not(label)').val().trim() == '') {
                $(value).find('[itemid="txtnameofmed-"]:not(label)').addClass('border-danger'); returnval = false;
                BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            }
            if ($(value).find('[itemid="txtDosage-"]:not(label)').val().trim() == '') {
                $(value).find('[itemid="txtDosage-"]:not(label)').addClass('border-danger'); returnval = false;
                BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            }
            if ($(value).find('[itemid="txtmedicationgivenon-"]:not(label)').val().trim() == '') {
                $(value).find('[itemid="txtmedicationgivenon-"]:not(label)').addClass('border-danger'); returnval = false;
                BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            }
            else {

                if (fn_DateCompare($(value).find('[itemid="txtmedicationgivenon-"]:not(label)').val(), todayDate) == 1) {
                    $(value).find('[itemid="txtmedicationgivenon-"]:not(label)').addClass('border-danger');
                    $(value).find('[data-d=medicationgivenon-togglePopup-]').show();
                    returnval = false;
                }
            }

            if ($(value).find('[itemid="ddladminMedical-"]:not(label)').val() == '0') {
                $(value).find('[data-id]').addClass('border-danger'); returnval = false;
                BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            }

        });


        if ($('#ddlCondition').val() == '0') {

            $('[data-id="ddlCondition"]').addClass('border-danger');
            BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            returnval = false;
        }

        if ($('#lastDosageGivenOn').val().trim() != "") {

            if (fn_DateCompare($('#lastDosageGivenOn').val(), todayDate) == 1) {
                $('#lastDosageGivenOn').addClass('border-danger');
                $('#lastDosageGivenOn-togglePopup').show();
                returnval = false;
            }
        }
        return returnval;
    }

    function ClearMedicalFields() {

        $('#txtsearchforMedical').val('');
        $('#txtsearchforMedical').removeAttr('item-value');
        $('#medicalConditionName').val('');
        $('#txtsideeffects').val('');
        $('#nextDosageDateMedicial').val('');
        $('#nextDosageTimeMedicial').val('');
        $('#lastDosageGivenOn').val('');
        $('#specialPrecautionMed').val('');
        $('#ddlCondition').val('0');
        $('#ddlCondition').selectpicker('refresh');
        $('.multiple-row').not(':first-child').remove();
        $('[itemid="txtnameofmed-"]:not(label)').val('');
        $('[itemid="txtDosage-"]:not(label)').val('');
        $('[itemid="ddladminMedical-"]:not(label)').val('0');
        $('[itemid="ddladminMedical-"]:not(label)').selectpicker('refresh');
        $('[itemid="txtmedicationgivenon-"]:not(label)').val('');
        $($('.multiple-row:first').find('i:last-child')).attr('class', 'far fa-plus-square');
        $($('.multiple-row:first').find('i:last-child')).attr('onclick', 'addnewrow(this)');
        $('.image-table tbody').empty();
        $('.file-upload-content').parents('.row').addClass('d-none');
        $('.error-toggle-msg').hide();
        $('.border-danger').each(function () {
            $(this).removeClass('border-danger');
        });
        $('#lastDosageGivenOn-togglePopup').hide();
        $('#nextdosagetime-togglePopup').hide();
        $('#nextdosage-togglePopup').hide();
        $('#txtsearchforMedical-Invalid-togglePopup').hide();
        $('#txtsearchforMedical-togglePopup').hide();
        $('[data-d="medicationgivenon-togglePopup-"]').hide();
    }

    function SaveMedicalConditionData() {

        refreshFields();
        if (ValidateMedical() == true) {

            var meditions = [];
            var files = [];
            $('.multiple-row').each(function (key, value) {
                var details = {
                    NAMEOFMEDICATION: $(value).find('[itemid="txtnameofmed-"]:not(label)').val(),
                    DOSAGE: $(value).find('[itemid="txtDosage-"]:not(label)').val(),
                    ADMIN: $(value).find('[itemid="ddladminMedical-"]:not(label)').val(),
                    MEDICATIONGIENON: $(value).find('[itemid="txtmedicationgivenon-"]:not(label)').val()
                }
                meditions[key] = details;
            });



            $('.image-table .image i.fa-file').each(function (key, value) {

                var attachment = {
                    ATTACHFILENAME: $(value).attr('filename'),
                    FILEPATH: $(value).attr('file'),
                    MEDICATION_ID: Medication_ID,
                    STU_NO: $('#txtsearchforMedical').attr('item-value').trim(),
                    ACTION: $(value).attr('action')
                }

                files[key] = attachment;
            });

            var postData = {
                MEDICATION_ID: Medication_ID,
                ACTION: Medication_ID == '0' ? 'INSERT' : 'UPDATE',
                STU_ID: $('#txtsearchforMedical').attr('item-value').trim(),
                MEDICATION: $('#medicalConditionName').val().trim(),
                SIDEEFFECTS: $('#txtsideeffects').val(),
                DOSAGE_TIME: $('#nextDosageDateMedicial').val().trim() + " " + $('#nextDosageTimeMedicial').val().trim(),
                LAST_DOSAGE: $('#lastDosageGivenOn').val().trim(),
                SPECIALPRECAUTION: $('#specialPrecautionMed').val().trim(),
                CONDITION: $('#ddlCondition').val(),
                MEDICATIONCONDITION: JSON.stringify(meditions),
                ATTACHMENTS: JSON.stringify(files)
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/SAVE_STUDENT_MEDICATION")%>',
                data: JSON.stringify(postData),
                dataType: "json",
                beforeSend: CheckSession(),
                success: function (data) {
                    if (data.d.IsSuccess) {
                        BootstrapMessage('messageMed', 'success', 'check', data.d.Message);
                        if (Medication_ID != '0')
                            $('#btnmedicalsave').prop('disabled', true);
                        if (Medication_ID != '0' && Medication_ID != undefined) {
                            var rowdate = $('#medicalCondition').find('tr[id=' + Medication_ID + ']');
                            rowdate.find('.td-medical-condition').text($('#medicalConditionName').val());
                            rowdate.find('.td-sideeffects').text($('#txtsideeffects').val());
                            rowdate.find('.td-condition').text($('#ddlCondition option:selected').text());

                        }
                        else {
                            GetMedicalRecords(false);
                        }
                        ClearMedicalFields();
                    }
                    else {
                        BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', data.d.Message);
                    }
                },
                error: function (result) {
                    BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Error occurred');
                },
            });

        }
    }
    function refreshFields() {

        $('.error-toggle-msg').hide();
        $('#messageMed').hide();
        $('.border-danger').each(function (key, value) {
            $(this).removeClass('border-danger');
        });

    }

    function Get_Medical_Data(Medication_ID) {

        $('#btnmedicalsave').prop('disabled', false);
        ClearMedicalFields();
        refreshFields();
        $('#txtsearchforMedical').attr("disabled", true);

        $('#medicalConditionModal').find('label').addClass('active');
        var PostData = { MEDICATION_ID: Medication_ID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GetMedicalConditionByID")%>',
            data: JSON.stringify(PostData),
            dataType: "json",
            beforeSend: CheckSession(),
            success: function (data) {

                var Medication = JSON.parse(data.d);
                Medication = Medication[0];
                $('#txtsearchforMedical').val(Medication.STU_NAME);
                $('#txtsearchforMedical').attr('item-data', Medication.STU_NAME.trim());
                $('#txtsearchforMedical').attr('item-value', Medication.STU_NO);
                $('#medicalConditionName').val(Medication.MEDICATION);
                $('#txtsideeffects').val(Medication.SIDEEFFECTS);
                if (Medication.NEXTDOSAGEIFANY_DATE == '')
                    $('#nextDosageTimeMedicial').val('');
                else
                    $('#nextDosageTimeMedicial').val(Medication.NEXTDOSAGEIFANY_TIME);

                $('#nextDosageDateMedicial').val(Medication.NEXTDOSAGEIFANY_DATE);


                $('#lastDosageGivenOn').val(Medication.LASTDOSAGE_DATE);

                $('#specialPrecautionMed').val(Medication.SPECIALPRECAUTIONS);

                $('#ddlCondition').val(Medication.CONDITION);
                $('#ddlCondition').selectpicker('refresh');

                if (Medication.MEDICALCONDITION != null) {
                    JSON.parse(Medication.MEDICALCONDITION).forEach(function (value, item) {

                        if (item == 0) {
                            $('[itemid="txtnameofmed-"]:not(label)').val(value.NAMEOFMEDICATION);
                            $('[itemid="txtDosage-"]:not(label)').val(value.DOSAGE);
                            $('[itemid="ddladminMedical-"]:not(label)').val(value.ADMIN);
                            $('[itemid="ddladminMedical-"]:not(label)').selectpicker('refresh');
                            $('[itemid="txtmedicationgivenon-"]:not(label)').val(value.MEDICATIONGIENON)
                        }
                        else {
                            var ConditionDiv = addnewrow($('.fa-plus-square')); // removerow($('.fa-minus-square'))
                            $(ConditionDiv).find('[itemid="txtnameofmed-"]:not(label)').val(value.NAMEOFMEDICATION);
                            $(ConditionDiv).find('[itemid="txtDosage-"]:not(label)').val(value.DOSAGE);
                            $(ConditionDiv).find('[itemid="ddladminMedical-"]:not(label)').val(value.ADMIN);
                            $(ConditionDiv).find('[itemid="ddladminMedical-"]:not(label)').selectpicker('refresh');
                            $(ConditionDiv).find('[itemid="txtmedicationgivenon-"]:not(label)').val(value.MEDICATIONGIENON);
                        }


                    });
                }

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GET_ATTACHMENTS_BY_ID")%>',
                    data: JSON.stringify(PostData),
                    dataType: "json",
                    success: function (data) {
                        $('.image-table tbody').empty();
                        if (data.d != "") {
                            $('.file-upload-content').parents('.row').removeClass('d-none');
                            JSON.parse(data.d).forEach(function (value, item) {

                                $('.image-table tbody').append('<tr  class="image"><th scope="row" class="text-center">' + value.ATTACHFILENAME + '</th> <th class="text-center">  <i contenttype ="' + value.CONTENTTYPE + '"  file="' + value.FILEPATH + '"  filename="' + value.ATTACHFILENAME + '"   href="#"  onclick="DownloadAttachment(this)" class="fa fa-file pr-2 text-primary"></i> </th><th  class="text-center"><i href="#" onclick="deleteFile(this)"  class="far fa-trash-alt"></i></th></tr>');
                            });
                        }
                    },
                    error: function (result) {
                        BootstrapMessage('messageMed', 'danger', 'exclamation-triangle', 'Error occurred');

                    },
                    compelete: function () {
                    }
                });
            },
            error: function (result) {
                //alert(result);
            },
            compelete: function () {
            }
        });
    }





    var image_row = '';
    function deleteFile(obj) {
        image_row = obj;
        $("#deletedialog").modal('show');

    }

    function removeImage() {
        $(image_row).parents(".image").remove();
        if ($('.image-table tr').length == 1) {
            $('.file-upload-content').parents('.row').addClass('d-none');
        }
        $("#deletedialog").modal('hide');
    }


    function OpenFile() {
        $('#fileupload').click();
    }

    var counter;
    var filename = new Array();
    function UploadFile() {
        var files = $('#fileupload').get(0).files;
        debugger;
        counter = 0;
        filename = [];
        // Loop through files
        var Stu_NO = $('#txtsearchforMedical').attr('item-value');
        if (Stu_NO == '' || Stu_NO == undefined) {
            $('#txtsearchforMedical').addClass('border-danger');
            $('#txtsearchforMedical-togglePopup').show();
            return;
        }
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            var file = files[i];

            if (file.size > 2097152) {
                $('#fileLimitmsg').show();
                $('#exceededfilename').append('<span class="text-default font-small d-block">' + file.name + '</span>');
                setTimeout(function () {
                    $('#fileLimitmsg').hide();
                    $('#exceededfilename').empty();
                }, 5000);
            }
            else {

                var extension = file.name.substr((file.name.lastIndexOf('.') + 1));
                if (extension.toLowerCase() != 'png' && extension.toLowerCase() != 'pdf' && extension.toLowerCase() != 'jpg' && extension.toLowerCase() != 'jpeg') {
                    return;
                }
                data.append(files[i].name, files[i]);
                $.ajax({
                    url: '<% = ResolveUrl("~/Medical/FileHandler.ashx")%>',
                     type: "POST",
                     data: data,
                     contentType: false,
                     processData: false,
                     beforeSend: function (xhr) {
                         xhr.setRequestHeader('Stu_NO', Stu_NO);
                     },

                     success: function (result) { },
                     error: function (err) {
                         //  alert(err.statusText)
                     }
                 }).done(function (event, xhr, settings) { //use this
                     $('.file-upload-content').parents('.row').removeClass('d-none');
                     $('.file-upload-content tbody').append('<tr class="image"><th scope="row" class="text-center">' + settings.getResponseHeader('originalfilename') + '</th> <th class="text-center">  <i action="INSERT" filename="' + settings.getResponseHeader('originalfilename') + '" contenttype="' + settings.getResponseHeader('contenttype') + '" file="' + settings.getResponseHeader('filedir') + '"   href="#"   onclick="DownloadAttachment(this)" class="fa fa-file pr-2 text-primary"></i> </th><th  class="text-center"><i href="#"   onclick="deleteFile(this)" class="far fa-trash-alt"></i></th></tr>');
                 });

            }
        }
    }
    function progressHandler(event) {

        $("#loaded_n_total").html("Uploaded " + event.loaded + " bytes of " + event.total);
        var percent = (event.loaded / event.total) * 100;


    }
    function completeHandler(event) {

        $('.file-upload-content').parents('.row').removeClass('d-none');
        $('.file-upload-content tbody').append('<tr class="image"><th scope="row" class="text-center">' + event.target.getResponseHeader('originalfilename') + '</th> <th class="text-center">  <i action="INSERT" filename="' + event.target.getResponseHeader('originalfilename') + '" contenttype="' + event.target.getResponseHeader('contenttype') + '" file="' + event.target.getResponseHeader('filedir') + '"   href="#"   onclick="DownloadAttachment(this)" class="fa fa-file pr-2 text-primary"></i> </th><th  class="text-center"><i href="#"   onclick="deleteFile(this)" class="far fa-trash-alt"></i></th></tr>');

        counter++

    }
    function errorHandler(event) {
        // $("#status").html("Upload Failed");
    }
    function abortHandler(event) {
        // $("#status").html("Upload Aborted");
    }
    $(document).ready(function () {

        AutoCompleteCall('txtsearchforMedical', 'SearchStudent');


        FillDropDown('ddlCondition', 'MED_CONDITION');

        $('#lastDosageGivenOn').click(function () {
            $('#lastDosageGivenOn-togglePopup').hide();
        });
        $('[itemid=txtmedicationgivenon-]').click(function () {

            $(this).parent().find('.error-toggle-msg').hide();
        });

        $('#txtsearchforMedical').click(function () {
            $('#txtsearchforMedical').removeClass('border-danger');
            $('#txtsearchforMedical-togglePopup').hide();
            $('#txtsearchforMedical-Invalid-togglePopup').hide();
        });

        $('#fileLimitmsg').hide();
        $('#exceededfilename').empty();
    });

    function removerow(obj) {
        $(obj).closest('.multiple-row').remove();
    }

    function addnewrow(obj) {

        var $orginal = $('.multiple-row').last();
        var clone = $orginal.clone();

        var newid = '';
        $(clone).find('.wraper').each(function (index, item) {

            var itemCount = $('.multiple-row').length;
            if ($(item).find('input').length > 0) {
                $(item).find('input').attr('id', $(item).find('input').attr('itemid') + itemCount);
                $(item).find('input').val('');
            }
            if ($(item).find('label').length > 0) {
                $(item).find('label').attr('for', $(item).find('label').attr('itemid') + itemCount);
            }

            if ($(item).find('select').length > 0) {

                var id = $(item).find('select').attr('itemid');
                newid = id + $('.multiple-row').length;
                $(item).find('select').attr('id', newid);
                $(item).find('.btn.dropdown-toggle').attr('data-id', newid);
                newid = '#' + newid;


                $(item).find(newid).selectpicker('refresh');

                $(item).find('[data-toggle="dropdown"]:last').remove();

            }
            $(item).find('.datetime-picker').datetimepicker({
                format: 'DD/MMM/YYYY',
                icons: {
                    up: "fa fa-chevron-circle-up",
                    down: "fa fa-chevron-circle-down",
                    next: "fa fa-chevron-circle-right",
                    previous: "fa fa-chevron-circle-left"
                }
            });
            $(item).find('.datetime-picker').val('');
        });


        $(obj).attr('class', 'far fa-minus-square');
        $(obj).attr('onclick', 'removerow(this)');
        clone.find('[data-d]').hide();
        $(clone).appendTo('.wrap');
        $('[itemid=txtmedicationgivenon-]').click(function () {
            $(this).parent().find('.error-toggle-msg').hide();
        });
        return $('.multiple-row').last();
    }



</script>

<div class="modal fade overflow-auto" id="medicalConditionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Medical Condition</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="column-padding pt-3 pb-3">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-0">
                                <div class="error-toggle-msg" id="txtsearchforMedical-Invalid-togglePopup" style="display: none;">Student Name is invalid</div>
                                <div class="error-toggle-msg" id="txtsearchforMedical-togglePopup" style="display: none;">Select student before upload files</div>
                                <input type="text" maxlength="20" class="form-control" id="txtsearchforMedical" required />
                                <label class="active select-label">Student Name<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <input type="text" id="medicalConditionName" maxlength="100" class="form-control">
                                <label for="medicalConditionName" class="active select-label">Medication Condition<span class="text-danger">*</span></label>
                            </div>
                        </div>

                    </div>
                    <hr />
                    <div class="wrap">
                        <div class="row multiple-row">

                            <div class=" col-lg-3 col-md-3 col-12">
                                <div class="md-form md-outline mb-0 mt-0 wraper">
                                    <input type="text" maxlength="100" itemid="txtnameofmed-" id="txtnameofmed" class="form-control">
                                    <label for="txtnameofmed" itemid="txtnameofmed-" class="active select-label">Name of medication<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="md-form md-outline mb-0 mt-0 wraper">
                                    <input type="text" itemid="txtDosage-" maxlength="100" id="txtDosage" class="form-control">
                                    <label for="txtDosage" itemid="txtDosage-" class="active select-label">Dosage of medication<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="md-form md-outline mb-0 wraper">
                                    <select id="ddladminMedical" itemid="ddladminMedical-" class="selectpicker">
                                        <option value="0" selected>Select</option>
                                        <option value="1">Self</option>
                                        <option value="2">School Clinic</option>
                                    </select>
                                    <label for="ddladminMedical" itemid="ddladminMedical-" class="active select-label">Administration<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0 wraper">
                                    <i class="far fa-calendar-alt"></i>
                                    <div class="error-toggle-msg" data-d="medicationgivenon-togglePopup-" style="display: none;">Give On Date should be less than current date</div>
                                    <input type="text" id="txtmedicationgivenon" itemid="txtmedicationgivenon-" class="form-control datetime-picker">
                                    <label for="txtmedicationgivenon" itemid="txtmedicationgivenon-" class="active select-label">Medication given on<span class="text-danger">*</span></label>
                                    <i onclick="addnewrow(this)" style="right: -22px;"
                                        class="far fa-plus-square"></i>
                                </div>

                            </div>

                        </div>
                    </div>
                    <hr />
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <input type="text" id="txtsideeffects" maxlength="100" class="form-control">
                                <label for="txtsideeffects" class="active select-label">Side effects of Medication<span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="error-toggle-msg" id="nextdosage-togglePopup" style="display: none;">Next Dosage date should be greater than current date</div>
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                <i class="far fa-calendar-alt"></i>
                                <input type="text" id="nextDosageDateMedicial" class="form-control datetime-picker">
                                <label for="nextDosageDateMedicial" class="active select-label">Next Dosage on if applicable</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="error-toggle-msg" id="nextdosagetime-togglePopup" style="display: none;">Next Dosage time should be greater than current time</div>
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                <i class="far fa-clock"></i>
                                <input type="text" id="nextDosageTimeMedicial" class="form-control time-picker">
                                <label for="nextDosageTimeMedicial" class="active select-label">Next Dosage time</label>
                            </div>
                        </div>

                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                        <i class="far fa-calendar-alt"></i>
                                        <div class="error-toggle-msg" id="lastDosageGivenOn-togglePopup" style="display: none;">Last Dosage Date should be less than current date</div>
                                        <input type="text" id="lastDosageGivenOn" class="form-control datetime-picker">
                                        <label for="lastDosageGivenOn" class="active select-label">Last Dosage given on</label>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="md-form md-outline mb-0">
                                        <select id="ddlCondition" class="selectpicker">
                                        </select>
                                        <label class="active select-label">Condition<span class="text-danger">*</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-3">
                                <textarea id="specialPrecautionMed" maxlength="500" class="form-control"></textarea>
                                <label for="specialPrecautionMed" class="active select-label">Special precautions if any</label>
                            </div>
                        </div>

                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <div class="file-upload-wrapper pt-3 pb-3 text-center">
                                    <div class="wrapper">
                                        <input type="file" id="fileupload" multiple="multiple" class="d-none" onchange="UploadFile()" />
                                        <input type="button" class="btn btn-primary waves-effect waves-light" id="btnfileupload" value="Click to Upload File" onclick="OpenFile()" />

                                        <div class="font-weight-light small text-default text-right">(Supported file formats are .pdf, .jpg, .jpeg, .png) </div>
                                    </div>

                                </div>
                                <label for="UploadMedicalDoc" class="bg-white pl-0 ml-2">Upload Related Document</label>
                            </div>
                        </div>

                    </div>
                    <div class="row mb-3 d-none">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <div class="file-upload-content ">
                                    <table class="image-table table table-bordered table-hover bg-white align-items-center assesment-table  ">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="text-left">File Name
                                                </th>
                                                <th scope="col" class="text-center">Download </th>
                                                <th scope="col" class="text-center">Delete
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3" style="display: none;" id="fileLimitmsg">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="text-danger text-left font-small">File size Exceeds 2MB limit</div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-12" id="exceededfilename">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-4 col-md-4 col-12"></div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div id="messageMed" class="text-center"></div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12"></div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnmedicalsave" onclick="return SaveMedicalConditionData()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalAttachment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Medication Condition Attachments</h4>

            </div>
            <div class="modal-body">
                <table class="view-image-table table table-bordered table-hover bg-white align-items-center assesment-table  ">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center"><small><strong>FILE NAME</strong> </small>
                            </th>
                            <th scope="col" class="text-center"><small><strong>DOWNLOAD</strong> </small></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>

<div class="modal fade" id="deletedialog" style="z-index: 100000" tabindex="-1" role="dialog" aria-labelledby="ModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="left: -9px;">
            <div class="modal-body">
                Are you sure you want to Delete?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">Cancel</button>
                <a onclick="removeImage()" class="btn btn-primary btn-ok waves-effect waves-light">Delete</a>
            </div>
        </div>
    </div>
</div>
