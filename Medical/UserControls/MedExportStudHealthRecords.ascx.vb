﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection

Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports ResponseHelper
Partial Class Medical_UserControls_MedExportStudHealthRecords
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindVaccinatin()
            txtason.Text = Today.ToString("dd/MMM/yyyy")

            bindAcademic_Year()

        End If
    End Sub


    Public Sub BindVaccinatin()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()

        Dim Sql_Query = "SELECT [VCN_ID],[VCN_NAME] FROM [OASIS_MED].[dbo].[VACCINE_MASTER] WHERE VCN_ID=VCN_REF_VCN ORDER BY VCN_NAME"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        CheckVac.DataSource = ds
        CheckVac.DataTextField = "VCN_NAME"
        CheckVac.DataValueField = "VCN_ID"
        CheckVac.DataBind()


    End Sub
   




    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "'"
            ddlAcdID.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlAcdID.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACY_ID")))
            End While
            reader.Close()
            ddlAcdID.ClearSelection()
            ddlAcdID.Items.FindByValue(Session("Current_ACY_ID")).Selected = True
            ddlAcdID_SelectedIndexChanged(ddlAcdID, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdID.SelectedIndexChanged
        bindCLM()
    End Sub
    
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub
    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        BindGrade()
    End Sub

    

    Sub bindCLM()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim acd_id As String = ddlAcdID.SelectedValue

        str_query = " select clm_descr,clm_id from dbo.CURRICULUM_M where clm_id in " & _
" (select distinct acd_clm_id from academicyear_d where acd_acy_id='" & acd_id & "' and acd_bsu_id='" & Session("sBsuid") & "')"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCurri.DataSource = ds
        ddlCurri.DataTextField = "clm_descr"
        ddlCurri.DataValueField = "clm_id"
        ddlCurri.DataBind()

        If ddlCurri.Items.Count > 1 Then
            ddlCurri.Items.Add(New ListItem("ALL", "0"))
            ddlCurri.ClearSelection()
            ddlCurri.Items.FindByText("ALL").Selected = True
            trCLM.Visible = True
        Else
            trCLM.Visible = False
        End If
        ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
    End Sub



    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim clm As String = ddlCurri.SelectedValue
        Dim acy_id As String = ddlAcdID.SelectedValue

        Dim ds As DataSet

        If clm <> "0" Then
            str_query = " SELECT     distinct GRADE_BSU_M.GRM_GRD_ID, GRADE_BSU_M.GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER " & _
           " FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
           " WHERE(GRADE_BSU_M.GRM_BSU_ID = '" & Session("sBsuid") & "') AND (GRADE_BSU_M.GRM_ACY_ID = '" & acy_id & "') AND (GRADE_BSU_M.GRM_ACD_ID = " & _
           " (SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID ='" & clm & "') AND " & _
           "  (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "'))) order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "grm_grd_id"
            ddlGrade.DataBind()
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = True
        Else
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = False
        End If


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub

    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim clm As String = ddlCurri.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim acy_id As String = ddlAcdID.SelectedValue
        If clm <> "0" Then
            Dim str_query As String = " SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE  SCT_GRD_ID= '" & GRD_ID & "'" & _
           " AND SCT_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & clm & "') AND " & _
            " (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "')) ORDER BY SCT_DESCR"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = True
        Else
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = False
        End If

    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        CallReport()
    End Sub


    Sub CallReport()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
        Dim str_query As String
        Dim lstrExportType As Integer

        Dim lstrConsent As String
        If RadioConsent.SelectedItem.Value = "YES" Then
            lstrConsent = "YES"
        ElseIf RadioConsent.SelectedItem.Value = "NO" Then
            lstrConsent = "NO"
        ElseIf RadioConsent.SelectedItem.Value = "NR" Then
            lstrConsent = "NR"
        Else
            lstrConsent = "ALL"
        End If

        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUId"))
        pParms(1) = New SqlClient.SqlParameter("@ACY_ID", ddlAcdID.SelectedItem.Value)
        pParms(2) = New SqlClient.SqlParameter("@GRADE", ddlGrade.SelectedItem.Value)
        pParms(3) = New SqlClient.SqlParameter("@SECTION", ddlSection.SelectedItem.Value)
        pParms(4) = New SqlClient.SqlParameter("@CONSENT", lstrConsent)

        Dim item As ListItem

        Dim val = ""
        For Each item In CheckVac.Items
            If item.Selected Then
                If val = "" Then
                    val &= item.Value
                Else
                    val &= "," & item.Value
                End If

            End If
        Next

        If val = "" Then
            For Each item In CheckVac.Items

                If val = "" Then
                    val &= item.Value
                Else
                    val &= "," & item.Value
                End If

            Next

        End If


        pParms(5) = New SqlClient.SqlParameter("@VACCINATION", val)


        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "QRY_MED_RECORDS", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)
        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT_MEDICAL")
            ws.InsertDataTable(dtEXCEL)
            ws.HeadersFooters.AlignWithMargins = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xls")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "" + pathSave)
            Dim path = cvVirtualPath & "" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else

        End If

    End Sub

   

    

    Public Function getbsucityid() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim query = "SELECT BSU_COUNTRY_ID FROM BUSINESSUNIT_M WHERE [BSU_ID]='" & Session("sBsuid") & "'"

        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)


    End Function

    Private Function txtvacdate() As Object
        Throw New NotImplementedException
    End Function

   


  


End Class
