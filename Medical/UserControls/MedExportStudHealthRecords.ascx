﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MedExportStudHealthRecords.ascx.vb" Inherits="Medical_UserControls_MedExportStudHealthRecords" %>
<style type="text/css">

table.BlueTableView 
{
	border: 1pt solid #1b80b6;
        padding: 8px;
        border-collapse:collapse;	
	    -moz-border-radius:0;
        margin-left: 0px;
    } 
.matters { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #1B80B6}
.button 
{ 
	border: 1pt solid White;
    background-color:#1B80B6; 
	background-image:url('../../images/bg-menu-main.png');
	font-weight: bold; color:#FFFFFF;
	FONT-FAMILY: tahoma,sans-serif; 
	FONT-SIZE: 11px; TEXT-DECORATION: none; 
}
    </style>
                

                <script type="text/javascript">
                    function change_chk_stateg(chkThis) {
                        var chk_state = !chkThis.checked;
                        for (i = 0; i < document.forms[0].elements.length; i++) {
                            var currentid = document.forms[0].elements[i].id;
                            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("Checkcheck") != -1) {

                                document.forms[0].elements[i].checked = chk_state;
                                document.forms[0].elements[i].click();
                            }
                        }
                    }


                    function opnenstu(a) {

                        //window.showModalDialog("ImmunizationParentEntry.aspx?stu_id=" + a , "", "dialogWidth:1000px; dialogHeight:400px; center:yes");
                        window.open("ImmunizationParentEntry.aspx?stu_id=" + a, '', 'Height=400px,Width=1000px,scrollbars=yes,resizable=no,directories=yes');

                        //window.open("ImmunizationParentEntry.aspx?stu_id=" + a);

                    }
                
                </script>
                <table id="tblStud" runat="server" align="center" border="1" class="BlueTableView" bordercolor="#1b80b6" cellpadding="3" cellspacing="0" >
                    <tr>
                       <td align="left" colspan="6" style="font-weight: bold; font-size: 8pt; background-image: url('../../Images/bgblue.gif');
                color: white; font-family: Verdana; ">
                <span style="font-size: 10pt; font-family: Arial">Search Option</span></td>
                    </tr>
                    <tr >
                        <td align="left" class="matters" >
                Academic Year</td>
                        <td align="center" class="matters" >
                            :</td>
                        <td align="left" class="matters" colspan="4" >
                <asp:DropDownList id="ddlAcdID" CssClass="ToArabicNumber ArabicNumbertxt" runat="server" Width="124px" 
                    OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trCLM">
                        <td align="left" class="matters" >
                            Curriculum<span style="font-size: 7pt; color: #ff0000">*</span></td>
                        <td align="center" class="matters" >
                            :</td>
                        <td align="left" class="matters" colspan="4" >
                            <asp:DropDownList id="ddlCurri" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurri_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" >
                            Select Grade<span style="color: #ff0000">*</span></td>
                        <td align="center" class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                            <asp:DropDownList ID="ddlGrade" SKINID="smallcmb" runat="server" AutoPostBack="True" Width="100px" >
                            </asp:DropDownList></td>
                        <td align="left" class="matters" ><span style="color: #1b80b6">
                            Select Section</span></td>
                        <td align="center" class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                            <asp:DropDownList ID="ddlSection" SKINID="smallcmb" runat="server" Width="100px"  >
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="tr_Age" runat="server" visible="false">
                        <td align="left" class="matters" >
                            Age</td>
                        <td align="center" class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                            <asp:TextBox id="txtage" runat="server" Width="120px">
                            </asp:TextBox></td>
                        <td align="left" class="matters" >
                            As on</td>
                        <td align="center" class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                            <asp:TextBox id="txtason" runat="server" Width="120px">
                            </asp:TextBox></td>
                    </tr>
                     <tr id="tr_DOJ" runat="server" visible="false">
                        <td align="left" class="matters">
                            Date of Join between</td>
                        <td align="center" class="matters">
                            :</td>
                        <td align="left" class="matters">
                            <asp:TextBox id="txtdojfrom" runat="server" Width="120px">
                            </asp:TextBox></td>
                        <td align="left" class="matters">
                            <asp:TextBox id="txtdojto" runat="server" Width="120px">
                            </asp:TextBox></td>
                        <td align="center" class="matters">
                            &nbsp;</td>
                        <td align="left" class="matters">
                            &nbsp;</td>
                    </tr>
                  

                     <tr>
                        <td align="left" class="matters">
                            Consent given ? </td>
                        <td align="center" class="matters">
                            :</td>
                        <td align="left" class="matters" colspan="2">
                            <asp:RadioButtonList ID="RadioConsent" runat="server" 
                                RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Text="Yes" Value="YES" Selected="True" ></asp:ListItem> 
                            <asp:ListItem Text="No" Value="NO" ></asp:ListItem> 
                            <asp:ListItem Text="Not Responded" Value="NR" ></asp:ListItem> 
                             <asp:ListItem Text="All" Value="ALL" ></asp:ListItem> 
                            </asp:RadioButtonList>
                        </td>
                        <td align="center" class="matters">
                            &nbsp;</td>
                        <td align="left" class="matters">
                            &nbsp;</td>
                    </tr>



                     <tr>
                        <td align="left" class="matters">
                            Vaccination</td>
                        <td align="center" class="matters">
                            :</td>
                        <td align="left" class="matters" colspan="2">
                            <asp:CheckBoxList ID="CheckVac" runat="server">
                            </asp:CheckBoxList>
                         </td>
                        <td align="center" class="matters">
                            &nbsp;</td>
                        <td align="left" class="matters">
                            &nbsp;</td>
                    </tr>



                    <tr>
                        <td align="center" class="matters" colspan="6">
                            &nbsp;<asp:Button ID="btnSearch" runat="server" Text="Export Report" CssClass="button" TabIndex="4" Height="27px" Width="158px" CausesValidation="False" />&nbsp;
                        </td>
                    </tr>
                    </table>
            
<ajaxToolkit:CalendarExtender id="CalendarExtender3" TargetControlID="txtason" PopupButtonId="txtason"  Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender id="CalendarExtender1" TargetControlID="txtdojfrom" PopupButtonId="txtdojfrom"  Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender id="CalendarExtender2" TargetControlID="txtdojto" PopupButtonId="txtdojto"  Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
 

 <br />

 <div class="matters">
 


 </div>
<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
<br />


