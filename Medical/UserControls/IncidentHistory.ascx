﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IncidentHistory.ascx.vb" Inherits="Medical_UserControls_IncidentHistory" %>

<!-- Bootstrap core CSS -->
<link href="../Medical/assets/css/fonts.css" rel="stylesheet" />
<link href="../Medical/assets/css/jquery-ui.structure.min.css" rel="stylesheet" />
<link href="../Medical/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../Medical/assets/css/addons/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="../Medical/assets/css/addons/jquery.minicolors.css" rel="stylesheet" type="text/css">
<link href="../Medical/assets/css/addons/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
<!-- dataTable css -->
<link href="../Medical/assets/css/addons/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../Medical/assets/css/addons/datatables-select.min.css" rel="stylesheet" type="text/css">
<!-- Material Design Bootstrap -->
<link href="../Medical/assets/css/main-medical.css" rel="stylesheet" type="text/css">
<style>
    .bootstrap-datetimepicker-widget {
        display: block;
    }


    .student-profile-section .student-meta {
        padding: 0 0.25rem;
        min-height: 130px;
    }

    .ui-front {
        z-index: 10000;
    }

    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        cursor: default;
    }

    .ui-widget-content {
        border: 1px solid #dddddd;
        background: #eeeeee url(images/ui-bg_highlight-soft_100_eeeeee_1x100.png) 50% top repeat-x;
        color: #333333;
    }

    .select2-container--default .select2-selection--multiple {
        background-color: white;
        border: 1px solid #e6e6e6 !important;
        border-radius: 4px;
        cursor: text
    }

    .select2-container--default .select2-search--inline .select2-search__field, .select2-container .select2-search--inline {
        width: 100% !important;
    }

    .select2-container {
        width: 100% !important;
    }
</style>
<script type="text/javascript" src="../Medical/assets/js/addons/bootstrap-select.js"></script>
<script type="text/javascript" src="../Medical/assets/js/popper.min.js"></script>
<script type="text/javascript" src="../Medical/assets/js/bootstrap.min.js"></script>
<!-- chart js -->
<!-- <script type="text/javascript" src="js/charts.js"></script> -->
<!-- custom select js -->
<script type="text/javascript" src="../Medical/assets/js/addons/moment-with-locales.js"></script>
<script type="text/javascript" src="../Medical/assets/js/addons/particles.min.js"></script>
<!-- date-time and color picker js -->
<script type="text/javascript" src="../Medical/assets/js/addons/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="../Medical/assets/js/addons/jquery.minicolors.min.js"></script>
<script type="text/javascript" src="../Medical/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- dataTable js -->
<script src="../Medical/assets/js/datatables.min.js" type="stylesheet"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $('#incidentDate').click(function () {
            $('#incidentdate-togglePopup').hide();
        });
        $('#incidentTime').click(function () {
            $('#incidenttime-togglePopup').hide();
        });


        $('#medicationGivenOn').click(function () {
            $('#medicationGivenOn-togglePopup').hide();
        });

        $('#txtsearch').click(function () {
            $('#txtsearch').removeClass('border-danger');
            $('#txtsearch-Invalid-togglePopup').hide();
        });

        $('#OthersAider').hide();
        $('#checkOthers').click(function () {
            if (this.checked) {
                $('#OthersAider').show(1000);
                $('#firstAiderName').val('');
                $('#OthersAidertxt').val('');
                $('#firstAiderName').attr('disabled', 'disabled');
            }
            else {
                $('#OthersAider').hide(1000);
                $('#firstAiderName').removeAttr('disabled');
            }
        });
        $('#txtsearch').removeAttr("disabled");

        GetStaff();

        $("#ddlstaff").select2({
            placeholder: "Select",
            allowClear: false
        });



        $('#FollowTime').hide();
        $('#checkFollowup').click(function () {

            $('#folowUpDate').val('');
            $('#folowUptime').val('');
            if (this.checked) {
                $('#FollowDate').show(1000);
                $('#FollowTime').show(1000);
            }
            else {
                $('#FollowDate').hide(1000);
                $('#FollowTime').hide(1000);
            }

        });

        $('#otherStaffSelection').hide();
        $('#checkOtherStaff').click(function () {
            if (this.checked) {
                $('#otherStaffSelection').show(1000);
            }
            else {
                $('#otherStaffSelection').hide(1000);
            }
            $('#ddlstaff').val(null).trigger('change');
        });

        FillDropDown('ddllocation', 'LOCATION');
        FillDropDown('ddlinjuredarea', 'INJURED');
        FillDropDown('ddlinjuredsymtoms', 'SYMPTOMS');
        FillDropDown('ddlhowithappen', 'HOWITHAPPENED');
        FillDropDown('ddlwhathappennext', 'WHATHAPPENDNEXT');


        AutoCompleteCall('txtsearch', 'SearchStudent');
        AutoCompleteCall('firstAiderName', 'GetAiderNames');

        $('.time-picker').datetimepicker({
            format: 'LT',

        });
        $('.datetime-picker').datetimepicker({
            format: 'DD/MMM/YYYY',
            icons: {
                up: "fa fa-chevron-circle-up",
                down: "fa fa-chevron-circle-down",
                next: "fa fa-chevron-circle-right",
                previous: "fa fa-chevron-circle-left"
            }
        });

        $('.time-picker').keypress(function (event) {
            return false;
        });

        $('.datetime-picker').keypress(function (event) {
            return false;
        });

    });




    function GetStaff() {

        $('#ddlstaff').find('option').not(':first').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GetStaff")%>',
            dataType: "json",
            type: "POST",
            data: {},
            beforeSend: CheckSession(),
            success: function (response, status, xhr) {

                $.each(response.d, function (key, value) {
                    var newOption = new Option(value.Text, value.Value, false, false);
                    $('#ddlstaff').append(newOption).trigger('change');
                });
            },
            complete: function () {

            },
            error: function (jqXHR, textStatus, errorThrown) {
            },
        });
    }

    function Get_Incident_Data(Incident_ID) {

        $('#btnsaveincident').prop('disabled', false);

        ClearIncidentFields();
        $('#txtsearch').attr("disabled", true);
        refreshIncidentFields();
        $('#medicalIncidentModal').find('label').addClass('active');
        var PostData = { Incident_ID: Incident_ID };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GetIncidentHistoryByID")%>',
            data: JSON.stringify(PostData),
            dataType: "json",
            beforeSend: CheckSession(),
            success: function (data) {

                var Incident = JSON.parse(data.d);
                Incident = Incident[0];

                $('#txtsearch').val(Incident.STU_NAME);
                $('#txtsearch').attr('item-data', Incident.STU_NAME.trim());
                $('#txtsearch').attr('item-value', Incident.STU_NO);
                if (Incident.OTHERAIDER == true) {
                    $('#firstAiderName').val('');
                    $('#firstAiderName').attr('disabled', 'disabled');
                    $('#OthersAidertxt').val(Incident.AIDER_NAME);
                    $('#checkOthers').prop('checked', true);
                    $('#OthersAider').show();
                }
                else {
                    $('#firstAiderName').val(Incident.AIDER_NAME);
                    $('#checkOthers').prop('checked', false);
                    $('#OthersAider').hide();
                }
                $('#incidentDate').val(Incident.INCIDENT_DATE);
                $('#incidentTime').val(Incident.INCIDENT_TIME);

                $('#ddllocation').val(Incident.LOC_ID);
                $('#ddllocation').selectpicker('refresh');
                $('#ddlinjuredarea').val(Incident.INJURED_AREA_ID);
                $('#ddlinjuredarea').selectpicker('refresh');
                $('#ddlinjuredsymtoms').val(Incident.SYMPTOMS_ID);
                $('#ddlinjuredsymtoms').selectpicker('refresh');
                $('#injuryDesc').val(Incident.INJURY_DESC);
                $('#ddlhowithappen').val(Incident.HOW_IT_HAPPENED);
                $('#ddlhowithappen').selectpicker('refresh');
                $('#txttreatadmin').val(Incident.TREATED_ADMIN);

                $('#ddlwhathappennext').val(Incident.WHAT_HAPPENED_NEXT);
                $('#ddlwhathappennext').selectpicker('refresh');

                $('#nameMedication').val(Incident.NAME_OF_MEDICATION);
                $('#dosageMedication').val(Incident.DOSAGE_OF_MEDICATION);
                $('#sideEffects').val(Incident.SIDE_EFFECTS);


                $('#ddladmin').val(Incident.ADMINISTRATION);
                $('#ddladmin').selectpicker('refresh');

                $('#medicationGivenOn').val(Incident.MEDICATION_GIVE_ON_DATE);
                $('#nextDosageDate').val(Incident.NEXTDOSAGEDATE);
                $('#nextDosageTime').val(Incident.NEXTDOSAGETIME);
                $('#specialPrecaution').val(Incident.SPECIAL_PRECUATIONS);

                $('#folowUpDate').val(Incident.FOLLOWUPDATE);
                $('#folowUptime').val(Incident.FOLLOWUPTIME);




                if (Incident.FOLLOWUPDATE != '' && Incident.FOLLOWUPTIME != '') {
                    $('#FollowDate').show();
                    $('#FollowTime').show();
                    $('#checkFollowup').prop('checked', true);

                }
                else {
                    $('#FollowDate').hide();
                    $('#FollowTime').hide();
                    $('#checkFollowup').prop('checked', false);
                }

                if (Incident.STAFF == '' || Incident.STAFF == null) {
                    $('#checkOtherStaff').prop('checked', false);
                    $('#otherStaffSelection').hide();
                }
                else {
                    $('#checkOtherStaff').prop('checked', true);
                    $('#otherStaffSelection').show();

                    var attributeIds = Incident.STAFF.split(',');
                    var len = attributeIds.length;

                    while (len--) {
                        attributeIds[len] = +attributeIds[len];
                    }

                    $('#ddlstaff').val(attributeIds);
                    $('#ddlstaff').select2().trigger('change');
                }
            },
            error: function (result) {

            },
            complete: function (result) {

            },
        });
    }


    function ConvertTime(time) {

        if (time == '' || time == null)
            return "";
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }


    function refreshIncidentFields() {

        $('.error-toggle-msg').hide();
        $('#message').hide();
        $('.border-danger').each(function (key, value) {
            $(this).removeClass('border-danger');
        });

    }

    function ClearIncidentFields() {

        $('#txtsearch').val('');
        $('#txtsearch').removeAttr('item-value');
        $('#incidentDate').val('');
        $('#incidentTime').val('');
        $('#firstAiderName').val('');
        $('#firstAiderName').removeAttr('disabled');
        $('#OthersAidertxt').val('');
        $('#checkOthers').attr('checked', false);
        $('#OthersAider').hide();
        $('#ddllocation').val(0);
        $('#ddllocation').selectpicker('refresh');
        $('#ddlinjuredarea').val(0);
        $('#ddlinjuredarea').selectpicker('refresh');
        $('#ddlinjuredsymtoms').val(0);
        $('#ddlinjuredsymtoms').selectpicker('refresh');
        $('#injuryDesc').val('');
        $('#txttreatadmin').val('');

        $('#ddlhowithappen').val(0);
        $('#ddlhowithappen').selectpicker('refresh');

        $('#ddlwhathappennext').val(0);
        $('#ddlwhathappennext').selectpicker('refresh');

        $('#sideEffects').val('');
        $('#nameMedication').val('');
        $('#dosageMedication').val('');
        $('#dosageMedication').val('');

        $('#ddladmin').val(0);
        $('#ddladmin').selectpicker('refresh');

        $('#medicationGivenOn').val('');
        $('#nextDosageDate').val('');
        $('#nextDosageTime').val('');
        $('#specialPrecaution').val('');

        $('#checkFollowup').prop('checked', false);

        $('#folowUpDate').val('');
        $('#folowUptime').val('');

        $('#FollowDate').hide();
        $('#FollowTime').hide();
        $('#checkOtherStaff').prop('checked', false);
        $('#checkOthers').prop('checked', false);

        $('#otherStaffSelection').hide();
        $('#ddlstaff').val(null).trigger('change');
        $('.border-danger').each(function (key, value) {
            $(this).removeClass('border-danger');
        });
        $('#txtsearch-Invalid-togglePopup').hide();
        $('#incidentdate-togglePopup').hide();
        $('#incidenttime-togglePopup').hide();
        $('#medicationGivenOn-togglePopup').hide();
    }

    function SaveData() {

        refreshIncidentFields();
        if (VALIDATEDATA() == true) {

            var postData = {
                ID: Incident_ID,
                ACTION: Incident_ID == '0' ? 'INSERT' : 'UPDATE',
                INCIDENT_DATE: $('#incidentDate').val() + " " + $('#incidentTime').val(),
                LOC_ID: $('#ddllocation').val(),
                SYMPTOMS_ID: $('#ddlinjuredsymtoms').val(),
                INJURED_AREA_ID: $('#ddlinjuredarea').val(),
                WHAT_HAPPENED_NEXT: $('#ddlwhathappennext').val(),
                NAME_OF_MEDICATION: $('#nameMedication').val(),
                DOSAGE_OF_MEDICATION: $('#dosageMedication').val(),
                ADMINISTRATION: $('#ddladmin').val() == '' ? '0' : $('#ddladmin').val(),
                MEDICATION_GIVE_ON: $('#medicationGivenOn').val(),
                SPECIAL_PRECUATIONS: $('#specialPrecaution').val(),
                INJURY_DESC: $('#injuryDesc').val(),
                HOW_IT_HAPPENED: $('#ddlhowithappen').val(),
                TREATED_ADMIN: $('#txttreatadmin').val(),
                AIDER_NAME: $('#checkOthers').is(':checked') == false ? $('#firstAiderName').val() : $('#OthersAidertxt').val(),
                SIDE_EFFECTS: $('#sideEffects').val(),
                NEXTDOSAGEDATE: $('#nextDosageDate').val() + " " + $('#nextDosageTime').val(),
                FOLLOWUPDATE: $('#checkFollowup').is(':checked') ? $('#folowUpDate').val() + " " + $('#folowUptime').val() : '',
                NOTIFYCLASSTUTOR: true,
                NOTIFYPARENT: true,
                NOTIFYOTHERSTAFF: $('#checkOtherStaff').is(':checked'),
                STAFF: $('#ddlstaff').val().length == 0 ? '' : $('#ddlstaff').val().join(","),
                STU_ID: $('#txtsearch').attr('item-value').trim(),
                OTHERAIDER: $('#checkOthers').is(':checked')

            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/SAVE_STUDENT_HISTORY")%>',
                data: JSON.stringify(postData),
                dataType: "json",
                beforeSend: CheckSession(),
                success: function (data) {
                    if (data.d.IsSuccess) {
                        if (Incident_ID != '0')
                            $('#btnsaveincident').prop('disabled', true);
                        BootstrapMessage('message', 'success', 'check', data.d.Message);
                        if (Incident_ID != '0' && Incident_ID != undefined) {
                            var rowdate = $('#incident').find('tr[id=' + Incident_ID + ']');
                            rowdate.find('.td-incident-date').text($('#incidentDate').val() + ' ' + $('#incidentTime').val());
                            rowdate.find('.td-location').text($('#ddllocation option:selected').text());
                            rowdate.find('.td-injured-area').text($('#ddlinjuredarea option:selected').text());
                            rowdate.find('.td-symptoms').text($('#ddlinjuredsymtoms option:selected').text());
                        }
                        else {
                            GetIncidentRecords(false);
                        }
                        ClearIncidentFields();
                    }
                    else {
                        BootstrapMessage('message', 'danger', 'exclamation-triangle', data.d.Message);
                    }
                },
                error: function (result) {
                    BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Error occurred');
                },

            });

        }

        return false;
    }

    function gettime(obj) {
        var time = $('#' + obj).val();
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;


        var timeNow = new Date();

        if (sHours < timeNow.getHours() && sMinutes < timeNow.getMinutes()) {
            return false;
        }
        else
            return true;

    }


    function fn_DateCompare(DateA, DateB) {

        var a = new Date(DateA);
        var b = new Date(DateB);

        var msDateA = Date.UTC(a.getFullYear(), a.getMonth() + 1, a.getDate());
        var msDateB = Date.UTC(b.getFullYear(), b.getMonth() + 1, b.getDate());

        if (parseFloat(msDateA) < parseFloat(msDateB))
            return -1;  // lt
        else if (parseFloat(msDateA) == parseFloat(msDateB))
            return 0;  // eq
        else if (parseFloat(msDateA) > parseFloat(msDateB))
            return 1;  // gt
        else
            return null;  // error
    }


    function VALIDATEDATA() {
        var todayDate = new Date();
        var returnval = true;
        if ($('#txtsearch').val().trim() == '') {
            $('#txtsearch').addClass('border-danger');
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            returnval = false;
        }
        else {
            var original_stu_name = $('#txtsearch').attr('item-data');
            var entered_stu_name = $('#txtsearch').val();
            if (original_stu_name.trim() != entered_stu_name.trim()) {
                $('#txtsearch').addClass('border-danger');
                $('#txtsearch-Invalid-togglePopup').show();
                returnval = false;
            }
        }
        if ($('#txtsearch').attr('item-value') == '' || $('#txtsearch').attr('item-value') == undefined) {
            $('#txtsearch').addClass('border-danger');
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            returnval = false;
        }
        if ($('#incidentDate').val().trim() == '') {
            $('#incidentDate').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }
        else {
            if (fn_DateCompare($('#incidentDate').val(), todayDate) == 1) {
                $('#incidentDate').addClass('border-danger');

                $('#incidentdate-togglePopup').show();
                returnval = false;
            }
        }

        if ($('#incidentTime').val().trim() == '') {
            $('#incidentTime').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');

        }
        else {

            if (fn_DateCompare($('#incidentDate').val(), todayDate) == 0) {
                var currentTime = moment();
                if (currentTime.isBefore(moment(ConvertTime($('#incidentTime').val()), 'hh:mm')) == true) {
                    $('#incidentTime').addClass('border-danger');

                    $('#incidenttime-togglePopup').show();
                    returnval = false;
                }
            }
        }

        if ($('#ddllocation').val() == "0") {
            $('[data-id="ddllocation"]').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }

        if ($('#ddlinjuredarea').val() == "0") {
            $('[data-id="ddlinjuredarea"]').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }

        if ($('#ddlinjuredsymtoms').val() == "0") {
            $('[data-id="ddlinjuredsymtoms"]').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }

        if ($('#txttreatadmin').val().trim() == '') {
            $('#txttreatadmin').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }
        if ($('#injuryDesc').val().trim() == '') {
            $('#injuryDesc').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }
        

        if ($('#ddlhowithappen').val() == "0") {
            $('[data-id="ddlhowithappen"]').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }

        if ($('#ddlwhathappennext').val() == "0") {
            $('[data-id="ddlwhathappennext"]').addClass('border-danger'); returnval = false;
            BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
        }

        if ($('#medicationGivenOn').val().trim() != "") {

            if (fn_DateCompare($('#medicationGivenOn').val(), todayDate) == 1) {
                $('#medicationGivenOn').addClass('border-danger');
                //animateScrolltocontrol('medicalIncidentModal', 'medicationGivenOn');
                $('#medicationGivenOn-togglePopup').show();
                returnval = false;
            }

        }




        if ($('#checkFollowup').is(':checked')) {

            if ($('#folowUpDate').val().trim() == '') {
                $('#folowUpDate').addClass('border-danger'); returnval = false;
                BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            }

            if ($('#folowUptime').val().trim() == '') {
                $('#folowUptime').addClass('border-danger'); returnval = false;
                BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
            }

        }
        if ($('#checkOtherStaff').is(':checked')) {
            if ($('#ddlstaff').val().length == 0) {

                BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Fields Marked with ( * ) are mandatory');
                $('#otherStaffSelection').find('li input').addClass('border-danger');
                returnval = false;
            }
        }

        return returnval;
    }

</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
<%--<link href="../assets/css/addons/select2.min.css" rel="stylesheet" />--%>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="modal fade overflow-auto" id="medicalIncidentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Incident Enquiry</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="column-padding pt-3 pb-3">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0">
                                <div class="error-toggle-msg" id="txtsearch-Invalid-togglePopup" style="display: none;">Student Name is invalid</div>
                                <input type="text" autocomplete="off" maxlength="20" class="form-control" id="txtsearch" required />
                                <label class="active select-label">Student Name<span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="error-toggle-msg" id="incidentdate-togglePopup" style="display: none;">Incident date should be less than current date</div>
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                <i class="far fa-calendar-alt"></i>
                                <input type="text" id="incidentDate" class="form-control datetime-picker" required>
                                <label for="incidentDate" class="active select-label">Incident Date <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="error-toggle-msg" id="incidenttime-togglePopup" style="display: none;">Incident time should be less than current time</div>
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                <i class="far fa-clock"></i>
                                <input type="text" id="incidentTime" class="form-control time-picker">
                                <label for="incidentDate" class="active select-label">Incident Time<span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <input type="text" id="firstAiderName" maxlength="100" class="form-control">
                                <label for="firstAiderName" class="active select-label">Name of first aider</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="custom-control custom-checkbox custom-control-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="checkOthers">
                                <label class="custom-control-label active" for="checkOthers">Others</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0 mt-0" id="OthersAider">
                                <input type="text" placeholder="Other Aider" maxlength="100" id="OthersAidertxt" class="form-control">
                                <label for="OthersAidertxt" class="active select-label">Other Aider</label>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row mb-3">
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0">
                                <select class="selectpicker" id="ddllocation">
                                </select>
                                <label class="active select-label">Location of incident ?<span class="text-danger"> *</span></label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0">
                                <select class="selectpicker" id="ddlinjuredarea">
                                </select>
                                <label class="active select-label">Injured area<span class="text-danger"> *</span></label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0">
                                <select class="selectpicker" id="ddlinjuredsymtoms">
                                    <%-- data-live-search="true" --%>
                                </select>
                                <label class="active select-label">Injury / Symptoms<span class="text-danger"> *</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-3 h-100">
                                <textarea id="injuryDesc" maxlength="250" class="form-control h-100"></textarea>
                                <label for="injuryDesc" class="active select-label">Injury Description <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-3 h-100">
                                <textarea id="txttreatadmin" maxlength="250" class="form-control   h-100"></textarea>
                                <label for="txttreatadmin" class="active select-label">Treatment Administered <span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-3">
                                <select class="selectpicker" id="ddlhowithappen">
                                </select>
                                <label class="active select-label active select-label">How it happened?<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="md-form md-outline mb-3">
                                <select class="selectpicker" id="ddlwhathappennext">
                                </select>
                                <label class="active select-label active select-label">What happened next ?<span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <input type="text" id="nameMedication" maxlength="100" class="form-control">
                                <label for="nameMedication" class="active select-label">Name of Medication</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <input type="text" id="dosageMedication" maxlength="100" class="form-control">
                                <label for="dosageMedication" class="active select-label">Dosage of Medication</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <input type="text" id="sideEffects" maxlength="300" class="form-control">
                                <label for="sideEffects" class="active select-label">Side effects of Medication</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="md-form md-outline mb-0 mt-0">
                                <select class="selectpicker" id="ddladmin">
                                    <option value="0" selected>Select</option>
                                    <option value="1">Self</option>
                                    <option value="2">School Clinic</option>
                                </select>
                                <label class="active select-label">Administration By </label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                <i class="far fa-calendar-alt"></i>
                                <div class="error-toggle-msg" id="medicationGivenOn-togglePopup" style="display: none;">Given On date should be less than current date</div>
                                <input type="text" id="medicationGivenOn" class="form-control datetime-picker">
                                <label for="medicationGivenOn" class="active select-label">Medication given on</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">

                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                <i class="far fa-calendar-alt"></i>
                                <input type="text" id="nextDosageDate" class="form-control datetime-picker">
                                <label for="nextDosageDate" class="active select-label">Next Dosage on if applicable</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0">
                                <i class="far fa-clock"></i>
                                <input type="text" id="nextDosageTime" class="form-control time-picker">
                                <label for="nextDosageTime" class="active select-label">Next Dosage time</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="md-form md-outline mb-3">
                                <textarea id="specialPrecaution" maxlength="500" type="text" class="form-control"></textarea>
                                <label for="specialPrecaution" class="active select-label">Special precautions if any</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-12">
                            <h3 class="sub-heading text-dark border-bottom text-bold pb-2">Follow Up's</h3>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="custom-control custom-checkbox custom-control-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="checkFollowup">
                                <label class="custom-control-label active select-label" for="checkFollowup">Follow Up Required</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0" id="FollowDate">
                                <i class="far fa-calendar-alt"></i>
                                <input type="text" placeholder="Incident Date" id="folowUpDate" class="form-control datetime-picker">
                                <label for="folowUpDate" class="active select-label">Follow Up Date<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="md-form md-outline date-time-picker field-with-icon mb-0 mt-0" id="FollowTime">
                                <i class="far fa-clock"></i>
                                <input type="text" placeholder="Incident Date" id="folowUptime" class="form-control time-picker">
                                <label for="folowUptime" class="active select-label">Follow Up Time<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-12">
                            <h3 class="sub-heading text-dark border-bottom text-bold pb-2">Notify Staff</h3>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="custom-control custom-checkbox custom-control-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="checkClassteacher" checked disabled>
                                <label class="custom-control-label " for="checkClassteacher">Class Teacher / Tutor</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="custom-control custom-checkbox custom-control-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="checkParent" checked disabled>
                                <label class="custom-control-label" for="checkParent">Parent / Guardian</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="custom-control custom-checkbox custom-control-inline mr-3">
                                <input type="checkbox" class="custom-control-input" id="checkOtherStaff">
                                <label class="custom-control-label" for="checkOtherStaff">Other Staffs</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12"></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="md-form md-outline mb-3" id="otherStaffSelection">
                                <select class="form-control js-example-placeholder-single" id="ddlstaff" multiple="multiple">
                                </select>
                                <label class="active select-label">Staff Name<span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-4 col-md-4 col-12"></div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div id="message" class="text-center"></div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" id="btnsaveincident" onclick="return SaveData()" class="btn btn-primary">Save</button>
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
