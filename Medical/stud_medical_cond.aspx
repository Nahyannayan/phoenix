﻿<%@ Page Title="Student Medical Condition" Language="VB" MasterPageFile="~/Medical/MedicalMasterPage.master" AutoEventWireup="false" CodeFile="stud_medical_cond.aspx.vb" Inherits="Medical_stud_medical_cond" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        

        td.details-control:before {
            content: "\f0fe";
            cursor: pointer;
            font-weight: 900;
            font-family: "Font Awesome 5 Free";
            color: #6a923a;
        }

        tr.shown td.details-control:before {
            content: "\f146";
            cursor: pointer;
            font-weight: 900;
            font-family: "Font Awesome 5 Free";
            color: #6a923a;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
              ShowLoader();
            //$("#medicalConditionModal .modal-body").mCustomScrollbar({
            //     setHeight: "550",
            //     autoExpandScrollbar: true,
            //     scrollbarPosition: "inside",
            //     autoHideScrollbar: false,
            // });

            $('#ModalAttachment .modal-body').mCustomScrollbar({
                setHeight: "300",
                autoExpandScrollbar: true,
                scrollbarPosition: "inside",
                autoHideScrollbar: false,
            });

            Get_Grade();

            $("#ddlgrade").change(function () {
                Get_Section($(this));
            });
            $("#ddlsection").change(function () {

                GetMedicalRecords();
            });
        });


        function Get_Grade() {
            $.ajax({
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/Get_Grade")%>',
                dataType: "json",
                type: "POST",
                data: {},
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {
                    $('#ddlgrade').append('<option value ="0">ALL</option>');
                    $.each(response.d, function (key, value) {
                        //$('#' + ObjID).append($("<option></option>").attr("value", value.Value).text(value.Text));
                        $('#ddlgrade').append('<option value = ' + value.Value + '>' + value.Text + '</option>');
                    });

                    $('#ddlgrade').selectpicker('refresh');

                },

                error: function (jqXHR, textStatus, errorThrown) {

                },

                complete: function (jqXHR, textStatus, errorThrown) {
                    Get_Section($('#ddlgrade'));
                }
            });
        }

        function Get_Section(grade) {


            var postdata = { GRD_ID: $(grade).val() };
            $.ajax({
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/Get_Section")%>',
                dataType: "json",
                type: "POST",
                data: JSON.stringify(postdata),
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {

                    $('#ddlsection').find('option').remove();
                    $('#ddlsection').append('<option value ="0">ALL</option>');
                    $('#ddlsection').selectpicker('refresh');
                    $.each(response.d, function (key, value) {
                        $('#ddlsection').append('<option value = ' + value.Value + '>' + value.Text + '</option>');
                    });

                    $('#ddlsection').selectpicker('refresh');

                },
                complete: function (jqXHR, textStatus, errorThrown) {
                    GetMedicalRecords();
                },
                error: function (jqXHR, textStatus, errorThrown) {

                },
            });
        }
        function GetMedicalRecords(loader) {
            if (loader == undefined)
                ShowLoader();
            var postdata = {
                Grade: $('#ddlgrade').val(),
                Section: $('#ddlsection').val()
            }

            $.ajax({
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GetMedicalConditionByBSUID")%>',
                dataType: "json",
                type: "POST",
                data: JSON.stringify(postdata),
                contentType: "application/json; charset=utf-8",
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {

                    if (response.d != null) {

                        var table = $('#tblMedicialCondition').DataTable();
                        table.destroy()
                        table = $('#tblMedicialCondition').DataTable({
                            "stateSave": false,
                            "bDestroy": true,
                            "rowId": "MEDICATION_ID",
                            "ordering": true,
                            "pageLength": 10,
                            "searching": true,
                            "lengthChange": false,
                            "paging": true,
                            "data": response.d,
                            "columns": [
                                {
                                    "className": 'details-control',
                                    "orderable": false,
                                    "data": null,
                                    "defaultContent": ''
                                },
                                { "data": "STU_NAME" },
                                { "data": "MEDICATION" },
                                { "data": "SIDEEFFECTS" },
                                {
                                    "className": 'text-center',
                                    "data": "CONDITIONDESCR"
                                },
                                { "data": "SPECIALPRECAUTIONS" },
                                {
                                    "className": 'text-center',
                                    "data": "ATTACHMENT"
                                },
                                { "data": "MEDICALCONDITION" }
                            ], columnDefs: [{
                                orderable: true
                            }, {
                                "targets": [7],

                                className: "d-none medical-condition",

                            }]
                        });
                        $('#tblMedicialCondition tbody').on('click', 'td.details-control', function () {

                            var tr = $(this).closest('tr');
                            var row = table.row(tr);

                            if (row.child.isShown()) {
                                row.child.hide(1000);
                                tr.removeClass('shown');
                            }
                            else {

                                row.child(format($(tr).find("td.medical-condition").text())).show(1000);
                                tr.addClass('shown');
                            }

                        });
                        if (response.d.length == 0) {
                            $('#tblMedicialCondition_paginate').hide();
                        }
                        else {
                            $('#tblMedicialCondition_paginate').show();
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoader();
                },
                complete: function () {
                    if (loader == undefined)
                        HideLoader();
                    $('#tblMedicialCondition_filter input').on('keyup', function () {

                        if ($('#tblMedicialCondition').find('tbody').find('.dataTables_empty').length == 1)
                            $('#tblMedicialCondition_paginate').hide();
                        else
                            $('#tblMedicialCondition_paginate').show();
                    });
                }
            });
        }
        function format(condition) {
            var table = '<table cellpadding="5" cellspacing="0" border="0"  class="table table-bordered table-hover datatable bg-white align-items-center assesment-table footer no-footer dataTable">';
            table += '<thead></th><th  class="text-center"><small><strong>NAME OF MEDICATION</strong> </small></th><th  class="text-center"><small><strong>DOSAGE OF MEDICATION</strong> </small></th ><th  class="text-center"><small><strong>ADMINISTRATION</strong> </small></th><th  class="text-center"><small><strong>MEDICATION GIVEN ON</strong> </small></th></tr></thead>';
            JSON.parse(condition).forEach(function (value, item) {
                table += '<tr>' +
                    '<td class="text-center">' + value.NAMEOFMEDICATION + '</td>' +
                    '<td class="text-center">' + value.DOSAGE + '</td>' +
                    '<td class="text-center">' + (value.ADMIN == '1' ? 'Self' : 'School Clinic') + '</td>' +
                    '<td class="text-center">' + value.MEDICATIONGIENON + '</td>' +
                    '</tr>';
            });
            table += '</table>';
            return table;
        }
    </script>



    <link href="assets/css/addons/fileinput/fileinput.css" rel="stylesheet" type="text/css">
    <link href="assets/css/addons/fileinput/explorer-fas-theme.css" rel="stylesheet" type="text/css">
    <link href="assets/css/addons/fileinput/fileinput-rtl.css" rel="stylesheet" type="text/css">
    <style>
        .wrapper {
            margin: 0 auto;
            background-color: transparent;
            padding: 10px;
            border: solid 1px #e6e6e6;
            border-radius: 6px;
        }

        .student-profile-section .student-meta {
            padding: 0 0.25rem;
            min-height: 130px;
        }



        /* CSS for animated bouncing loader. */
        .loader {
            display: flex;
            justify-content: center;
            align-items: center;
            position: absolute;
            top: 50%;
            left: 50%;
        }

            /* Loader circles */
            .loader > span {
                background: /*#6a923a*/ #ffffff;
                border-radius: 50%;
                margin: 5rem 0.5rem;
                animation: bouncingLoader 0.6s infinite alternate;
            }

                .loader > span:nth-child(2) {
                    animation-delay: 0.2s;
                }

                .loader > span:nth-child(3) {
                    animation-delay: 0.4s;
                }

        /* Define the animation called bouncingLoader. */
        @keyframes bouncingLoader {
            from {
                width: 0.1rem;
                height: 0.1rem;
                opacity: 1;
                transform: translate3d(0);
            }

            to {
                width: 1rem;
                height: 1rem;
                opacity: 0.1;
                transform: translate3d(0, -1rem, 0);
            }
        }
    </style>
    <!-- Uplpoad files-->
    <script src="assets/js/addons/fileinput/fileinput.js" type="text/javascript"></script>
    <script src="assets/js/addons/fileinput/piexif.min.js" type="text/javascript"></script>
    <script src="assets/js/addons/fileinput/sortable.min.js" type="text/javascript"></script>
    <script src="assets/js/addons/fileinput/ar.js" type="text/javascript"></script>
    <script src="assets/js/addons/fileinput/fas-theme.js" type="text/javascript"></script>
    <script src="assets/js/addons/fileinput/explorer-fas-theme.js" type="text/javascript"></script>
    <script src="assets/js/file-uploader-config.js" type="text/javascript"></script>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div class="card shadow-none p-2">
                <div class="card-body column-padding">
                    <div class="row task-search border-0">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <div>
                                <button class="btn btn-primary" onclick="ClearMedicalFields()" type="button" data-toggle="modal" data-target="#medicalConditionModal" id="medicalCondition">
                                    Add New Medical Condition
                                </button>

                            </div>
                        </div>

                        <div class="col-lg-8 col-12 mt-5"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>






    <!-- statistics section start -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div class="card shadow-none">
                <div class="card-body">

                    <h3 class="card-title"><i class="fas fa-briefcase-medical pr-2"></i>Medical Condition</h3>
                    <div class="row ml-3">
                        <div class="col-lg-4 col-md-12 col-12 mt-3">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="md-form md-outline mb-0">
                                        <select class="selectpicker" id="ddlgrade">
                                        </select>
                                        <label class="active select-label">Grade </label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="md-form md-outline mb-0">
                                        <select class="selectpicker" id="ddlsection">
                                        </select>
                                        <label class="active select-label">Section </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-12">
                        </div>
                        <div class="col-lg-4 col-md-12 col-12">
                        </div>
                    </div>


                    <div class="column-padding">
                        <!-- Projects table -->
                        <table id="tblMedicialCondition" style="width:100%" class="table table-bordered  datatable bg-white align-items-center assesment-table footer">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-left" style="width: 10%"><small><strong>STUDENT NAME</strong> </small></th>
                                    <th class="text-left" style="width: 25%"><small><strong>MEDICAL CONDITION</strong> </small></th>
                                    <th class="text-left" style="width: 25%"><small><strong>SIDE EFFECTS OF DOSAGE</strong> </small></th>
                                    <th class="text-left" style="width: 10%"><small><strong>CONDITION</strong> </small></th>
                                    <th class="text-left" style="width: 25%"><small><strong>SPECIAL PRECAUTIONS</strong> </small></th>
                                    <th class="text-left" style="width: 5%"><small><strong>ATTACHMENTS</strong> </small></th>
                                    <th class="d-none ">MEDICALCONDITION</th>

                                </tr>
                            </thead>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>





</asp:Content>

