﻿<%@ Page Title="Student Incident Log" Language="VB" MasterPageFile="~/Medical/MedicalMasterPage.master" AutoEventWireup="false" CodeFile="stud_log_incident.aspx.vb" Inherits="Medical_stud_log_incident" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src='<%= ResolveUrl("~/Medical/assets/js/2.11.2moment.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Medical/assets/js/jquery.dataTables.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Medical/assets/js/datetime-moment.js")%>'></script>
    <script type="text/javascript">

        var Incident_ID = '0';


        $(document).ready(function () {
            ShowLoader();
            Get_Grade();

            $("#ddlgrade").change(function () {
                Get_Section($(this));
            });
            $("#ddlsection").change(function () {
                GetIncidentRecords();
            });

        });


        function OpenNotifyDetails(IncidentID) {
            var postData = {
                Incident_ID: IncidentID
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '<%= ResolveUrl("~/Medical/Student_Medical_Service.asmx/GET_INCIDENT_NOTIFICATION_BY_ID")%>',
                data: JSON.stringify(postData),
                dataType: "json",
                beforeSend: CheckSession(),
                success: function (data) {

                    $('#NotificationModal').modal('show');
                    if (data != null && data.d.length > 0) {
                        var array = JSON.parse(data.d);

                        var parent = $.grep(array, function (check) {
                            return (check.EMAILTO == 'PARENT');
                        });
                        var tutor = $.grep(array, function (check) {
                            return (check.EMAILTO == 'TUTOR');
                        });
                        var staff = $.grep(array, function (check) {
                            return (check.EMAILTO == 'STAFF');
                        });

                        BindNotificaction('Parenttbl', parent);
                        BindNotificaction('Tutortbl', tutor);
                        BindNotificaction('Stafftbl', staff);


                    }
                    else {
                        BootstrapMessage('message', 'danger', 'exclamation-triangle', data.d.Message);
                    }
                },
                error: function (result) {
                    BootstrapMessage('message', 'danger', 'exclamation-triangle', 'Error occurred');
                },

            });
        }
        function BindNotificaction(table, data) {
            var PendingIcon = '<i class="fas fa-hourglass  text-primary"></i>';
            var CompleteIcon = '<i class="fas fa-check-square text-primary"></i>';
            $('#' + table + ' tbody').empty();
            if (data.length == 0) {
                $('#' + table + ' tbody').append('<tr> <td  style="width: 40%;"> Not Found </td><td></td><td></td></tr>');
            }
            else {
                $.each(data, function (key, value) {
                    $('#' + table + ' tbody').append('<tr><td style="width: 40%;">' + value.NAME + '</td><td class="text-center">' + ((value.LOG_STATUS == 'PENDING') ? 'PENDING' : 'SENT') + '</td><td class="text-left">' + ((value.LOG_SEND_DATE == null) ? '' : value.LOG_SEND_DATE) + ' </td></tr>')
                });
            }
        }

        function GetIncidentRecords(loader) {
            if (loader == undefined)
                ShowLoader();
            var postdata = {
                Grade: $('#ddlgrade').val(),
                Section: $('#ddlsection').val()
            }
            $.fn.dataTable.moment('DD-MMM-Y HH:mm:ss');
            $.ajax({
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/GetIncidentHistoryByBSUID")%>',
                dataType: "json",
                type: "POST",
                data: JSON.stringify(postdata),
                contentType: "application/json; charset=utf-8",
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {
                    if (response.d != null) {
                        var table = $('#incident').DataTable();
                        table.destroy()
                        $('#incident').DataTable({
                            "stateSave": false,
                            "serverSide": false,
                            "bDestroy": true,
                            "rowId": "INCIDENT_ID",
                            "ordering": true,
                            "pageLength": 10,
                            "searching": true,
                            "lengthChange": false,
                            "paging": true,
                            "data": response.d,
                            "order": [[0, "desc"]],
                            "columns": [
                                { "data": "INCIDENT_DATE" },
                                { "data": "STUDENT_NAME" },
                                { "data": "LOC_DESCR" },
                                { "data": "INJURED_AREA_DESCR" },
                                { "data": "SYMPTOMS_DESCR" },
                                { "data": "WHAHAPPENDNEXT" },
                                { "data": "FIRSTAIDER" },
                                {
                                    "className": 'text-center',
                                    "data": "NOTIFY"
                                },

                            ], "columnDefs": [
                                { "width": "10%", "targets": 6 }
                            ]
                        });
                        if (response.d.length == 0) {
                            $('#incident_paginate').hide();
                        }
                        else {
                            $('#incident_paginate').show();
                        }

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                },
                complete: function () {
                    if (loader == undefined)
                        HideLoader();
                    $('#incident_filter input').on('keyup', function () {
                        if ($('#incident').find('tbody').find('.dataTables_empty').length == 1)
                            $('#incident_paginate').hide();
                        else
                            $('#incident_paginate').show();
                    });
                }
            });
        }
        function Get_Grade() {
            $.ajax({
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/Get_Grade")%>',
                dataType: "json",
                type: "POST",
                data: {},
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {
                    $('#ddlgrade').append('<option value ="0">ALL</option>');
                    $.each(response.d, function (key, value) {
                        //$('#' + ObjID).append($("<option></option>").attr("value", value.Value).text(value.Text));
                        $('#ddlgrade').append('<option value = ' + value.Value + '>' + value.Text + '</option>');
                    });

                    $('#ddlgrade').selectpicker('refresh');

                },

                error: function (jqXHR, textStatus, errorThrown) {

                },

                complete: function (jqXHR, textStatus, errorThrown) {
                    Get_Section($('#ddlgrade'));
                }
            });
        }

        function Get_Section(grade) {

            var postdata = { GRD_ID: $(grade).val() };
            $.ajax({
                contentType: "application/json; charset=utf-8",
                url: '<% = ResolveUrl("~/Medical/Student_Medical_Service.asmx/Get_Section")%>',
                dataType: "json",
                type: "POST",
                data: JSON.stringify(postdata),
                beforeSend: CheckSession(),
                success: function (response, status, xhr) {

                    $('#ddlsection').find('option').remove();
                    $('#ddlsection').append('<option value ="0">ALL</option>');
                    $('#ddlsection').selectpicker('refresh');
                    $.each(response.d, function (key, value) {
                        $('#ddlsection').append('<option value = ' + value.Value + '>' + value.Text + '</option>');
                    });
                    $('#ddlsection').selectpicker('refresh');

                },
                complete: function (jqXHR, textStatus, errorThrown) {
                    GetIncidentRecords();
                },
                error: function (jqXHR, textStatus, errorThrown) {

                },
            });
        }
    </script>

    <style>
        fieldset.scheduler-border {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow: 0px 0px 0px 0px #000;
            box-shadow: 0px 0px 0px 0px #000;
        }

        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width: auto;
            padding: 0 10px;
            border-bottom: none;
        }
    </style>



    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card shadow-none p-2">
            <div class="card-body column-padding">
                <div class="row task-search border-0">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-12">
                        <div>
                            <button class="btn btn-primary" type="button" data-toggle="modal" onclick="ClearIncidentFields()" data-target="#medicalIncidentModal" id="dropdownMenuButton">
                                Add New Incident
                            </button>
                        </div>
                    </div>

                    <div class="col-lg-10 col-md-10 col-12 mt-5"></div>

                </div>
            </div>
        </div>
    </div>


    <!-- statistics section start -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div class="card shadow-none">
                <div class="card-body">

                    <h3 class="card-title"><i class="fas fa-briefcase-medical pr-2"></i>Medical Incidents</h3>
                    <div class="row ml-3">
                        <div class="col-lg-4 col-md-12 col-12 mt-3">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="md-form md-outline mb-0">
                                        <select class="selectpicker" id="ddlgrade">
                                        </select>
                                        <label class="active select-label">Grade </label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="md-form md-outline mb-0">
                                        <select class="selectpicker" id="ddlsection">
                                        </select>
                                        <label class="active select-label">Section </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-12">
                        </div>
                        <div class="col-lg-4 col-md-12 col-12">
                        </div>
                    </div>
                    <div class="pt-3 column-padding pb-2">
                        <!-- Projects table -->
                        <table id="incident" style="width: 100%" class="table table-bordered table-hover datatable bg-white align-items-center assesment-table footer">
                            <thead>
                                <tr>
                                    <th><small><strong>Incident Date & Time</strong> </small></th>
                                    <th><small><strong>Student Name</strong> </small></th>
                                    <th><small><strong>Location</strong> </small></th>
                                    <th><small><strong>Injured Area</strong> </small></th>
                                    <th><small><strong>Enquiry / Symptom</strong> </small></th>
                                    <th><small><strong>What happened next?</strong> </small></th>
                                    <th style="width: 80px !important"><small><strong>First Aider</strong> </small></th>
                                    <th class="text-center"><small><strong>Notified</strong> </small></th>
                                    <th class="text-center d-none"><small><strong>Attachments</strong> </small></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- statistics section end -->


    <div class="modal fade overflow-auto" id="NotificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>Parent :</div>
                    <table class="table table-sm table-striped" id="Parenttbl">
                        <thead>
                            <tr>
                                <th scope="col"><small><strong>NAME</strong> </small></th>
                                <th scope="col" class="text-center"><small><strong>STATUS</strong> </small></th>
                                <th scope="col" class="text-left"><small><strong>DATE & TIME</strong> </small></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <br />
                    <div>Tutor :</div>
                    <table class="table table-sm table-striped" id="Tutortbl">
                        <thead>
                            <tr>
                                <th scope="col"><small><strong>NAME</strong> </small></th>
                                <th scope="col" class="text-center"><small><strong>STATUS</strong> </small></th>
                                <th scope="col" class="text-left"><small><strong>DATE & TIME</strong> </small></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <br />
                    <div>Staff :</div>
                    <table class="table table-sm table-striped" id="Stafftbl">
                        <thead>
                            <tr>
                                <th scope="col"><small><strong>NAME</strong> </small></th>
                                <th scope="col" class="text-center"><small><strong>STATUS</strong> </small></th>
                                <th scope="col" class="text-left"><small><strong>DATE & TIME</strong> </small></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

