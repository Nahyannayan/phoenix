Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.IO

Partial Class homePageSS
    Inherits System.Web.UI.Page
    Dim userSuper As Boolean
    Dim Encr_decrData As New Encryption64
    Dim sqlReader As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ltMsg.Text = "<div style='font-weight: bold;size=9pt: color:Red; font-size: 8pt; color: red; font-family: VERDANA;border:1px; background-color:#ffffcc; '>Congratulations to our dear Chairman for being awarded the Padma Shree, one of India's highest civilian award this year</div>"
        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        If Page.IsPostBack = False Then
            Dim username As String = Session("sUsr_name") + ""
            BindGrids()
            Dim iPasswordExpireDays As Integer = isPasswordExpire(username)
            If iPasswordExpireDays < 5 Then
                If iPasswordExpireDays > 0 Then
                    lbExpiry.Text = String.Format("Your Password will expire in {0} Days", iPasswordExpireDays.ToString)
                    trPassword.Visible = True
                Else
                    lbExpiry.Text = "Your Password has Expired!!!"
                    trPassword.Visible = True
                End If
            Else
                trPassword.Visible = False
            End If
        End If
    End Sub
    Private Sub BindGrids()
        Try
            Dim rol_id As String = Session("sroleid")
            'Session("ListDays") = 1
            Session("EntryDate") = DateTime.Now
            Dim mnu_module As String = Session("sModule")
            '  Dim userSuper As Boolean
            Dim bsu_id As String = Session("sBsuid")
            Dim usr_id As String = Session("sUsr_id")
            Dim userSuper As Boolean
            userSuper = Session("sBusper")
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim userMod As String = Session("sModule")
            Dim bunit As String = Session("sBsuid")
            Dim userRol As String = Session("sroleid")
            Dim mnu_DT As New DataTable
            Dim sqlString, sqlSelect, sqlOrderBy, sqlWhere As String
            Dim TransMaxNoSelect, ApprMaxNoSelect, PersMaxNoSelect, RepMaxNoSelect, HelpMaxNoSelect As String

            If userSuper = False Then
                sqlSelect = "  select  MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                              " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,'Images\ButtonImages\' + MNU_CODE +'.png' MNU_IMAGE,MNU_ID,MNU_DESCR from MENUS_M" & _
                              " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                               " where (mnu_parentid is not null) and mnu_code in(select mnr_mnu_id from menurights_s where mnu_module='" & userMod & "' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0) and mnu_module='" & userMod & "'  "
            Else
                sqlSelect = "select   MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                              " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,'Images\ButtonImages\' + MNU_CODE +'.png' MNU_IMAGE,MNU_ID,MNU_DESCR from MENUS_M" & _
                              " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                               " where (mnu_parentid is not null) and mnu_module='" & userMod & "'"
            End If
            sqlOrderBy = " order by MNU_PARENTID,MNU_ID,MNU_TEXT "

            If ViewState("TransViewAll") = "True" Then
                TransMaxNoSelect = "select top 100 PERCENT * from "
            Else
                TransMaxNoSelect = "select top 5 * from "
            End If
            sqlWhere = " AND MNU_PARENTID='U000020'"
            sqlString = TransMaxNoSelect & "( " & sqlSelect & sqlWhere & " ) A " & sqlOrderBy
            mnu_DT = Mainclass.getDataTable(sqlString, connStr)
            '  lnkTransactionMore.Visible = (mnu_DT.Rows.Count > 5)
            dlTransaction.DataSource = mnu_DT
            dlTransaction.DataBind()
            trTransaction.Visible = mnu_DT.Rows.Count > 0
            If ViewState("ApprViewAll") = "True" Then
                ApprMaxNoSelect = "select top 100 PERCENT * from "
            Else
                ApprMaxNoSelect = "select top 5 * from "
            End If
            sqlWhere = " AND MNU_PARENTID='U000040'"
            sqlString = ApprMaxNoSelect & "( " & sqlSelect & sqlWhere & " ) A " & sqlOrderBy
            mnu_DT = Mainclass.getDataTable(sqlString, connStr)
            'lnkApproveMore.Visible = (mnu_DT.Rows.Count > 5)
            dlApproval.DataSource = mnu_DT
            dlApproval.DataBind()
            trApproval.Visible = mnu_DT.Rows.Count > 0
            If ViewState("PersViewAll") = "True" Then
                PersMaxNoSelect = "select top 100 PERCENT * from "
            Else
                PersMaxNoSelect = "select top 5 * from "
            End If
            sqlWhere = " AND MNU_PARENTID='U000060'"
            sqlString = PersMaxNoSelect & "( " & sqlSelect & sqlWhere & " ) A " & sqlOrderBy
            mnu_DT = Mainclass.getDataTable(sqlString, connStr)
            'lnkPersonalMore.Visible = (mnu_DT.Rows.Count > 5)
            dlPersonal.DataSource = mnu_DT
            dlPersonal.DataBind()
            trPersonal.Visible = mnu_DT.Rows.Count > 0
            If ViewState("RepViewAll") = "True" Then
                RepMaxNoSelect = "select top 100 PERCENT * from "
            Else
                RepMaxNoSelect = "select top 5 * from "
            End If
            sqlWhere = " AND MNU_PARENTID='U000080'"
            sqlString = RepMaxNoSelect & "( " & sqlSelect & sqlWhere & " ) A " & sqlOrderBy
            mnu_DT = Mainclass.getDataTable(sqlString, connStr)
            'lnkReportsMore.Visible = (mnu_DT.Rows.Count > 5)
            dlReports.DataSource = mnu_DT
            dlReports.DataBind()
            trReports.Visible = mnu_DT.Rows.Count > 0


            If ViewState("HelpViewAll") = "True" Then
                HelpMaxNoSelect = "select top 100 PERCENT * from "
            Else
                HelpMaxNoSelect = "select top 5 * from "
            End If
            sqlWhere = " AND MNU_PARENTID='U000100'"
            sqlString = HelpMaxNoSelect & "( " & sqlSelect & sqlWhere & " ) A " & sqlOrderBy
            mnu_DT = Mainclass.getDataTable(sqlString, connStr)
            'lnkReportsMore.Visible = (mnu_DT.Rows.Count > 5)
            dlHelp.DataSource = mnu_DT
            dlHelp.DataBind()
            trHelp.Visible = mnu_DT.Rows.Count > 0

        Catch ex As Exception

        End Try
    End Sub
    Private Function isPasswordExpire(ByVal username As String) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select  datediff(day, getdate(),isnull(USR_EXPDATE,getdate())) as Days from USERS_M where USR_NAME='" & username & "'"


        Dim Days As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not Days Is DBNull.Value Then
            Return Days
        Else
            Return 0
        End If

    End Function
    Private Function GetDataForApproval() As Integer
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "exec GetDataForApproval '" & Session("sUsr_name") & "', 'leave'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Return ds.Tables(0).Rows.Count
        Catch ex As Exception

        End Try
    End Function
    'Private Sub gridbind(ByVal Mnu_module As String, ByVal rol_id As String, ByVal bsu_id As String, ByVal usr_id As String)
    '    Try
    '        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

    '        Using conn As SqlConnection = New SqlConnection(str_conn)

    '            Dim str_Sql As String
    '            str_Sql = "SELECT  MENUS_M.MNU_CODE as code,MENUACCESS_S.MNA_ROL_ID as ROL_ID, " & _
    '" MENUACCESS_S.MNA_BSU_ID as BSU_ID,MENUS_M.MNU_TEXT as MNU_TEXT, " & _
    '" MENUS_M.MNU_NAME as MNU_Name FROM  MENUACCESS_S INNER JOIN MENUS_M " & _
    ' " ON MENUACCESS_S.MNA_MNU_ID = MENUS_M.MNU_CODE where MENUACCESS_S.MNA_USR_ID='" & usr_id & "' " & _
    '" and MENUACCESS_S.MNA_BSU_ID='" & bsu_id & "' and MENUACCESS_S.MNA_ROL_ID='" & rol_id & "' " & _
    ' " and MENUACCESS_S.MNA_MODULE = '" & Mnu_module & "'"
    '            Dim ds As New DataSet
    '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '            gvMenu.DataSource = ds.Tables(0)
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                gvMenu.DataSource = ds
    '                gvMenu.DataBind()
    '            Else
    '                gvMenu.DataBind()
    '            End If

    '        End Using
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub

    Protected Sub lbText_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblmenu As New Label
            Dim lblcode As New Label
            Dim lblMenuText As New LinkButton

            Dim url As String = String.Empty
            lblmenu = TryCast(sender.FindControl("lblmenu"), Label)
            lblMenuText = TryCast(sender.FindControl("lbText"), LinkButton)
            lblcode = TryCast(sender.FindControl("lblcode"), Label)
            Dim mCode As String = encrData.Encrypt(lblcode.Text)
            Dim datamode As String = encrData.Encrypt("none")
            Session("temp") = lblMenuText.Text
            url = String.Format("{0}?MainMnu_code={1}&datamode={2}", lblmenu.Text, mCode, datamode)
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub

    Function HasMenuAccess(ByVal MNU_CODE As String) As Boolean
        Dim sqlString As String = String.Empty
        If Session("sBusper") Then
            Return True
        Else
            sqlString = "SELECT count(*) FROM   MENUS_M  JOIN MENURIGHTS_S " & _
            " ON MENUS_M.MNU_CODE = MENURIGHTS_S.MNR_MNU_ID " & _
            " and (MENURIGHTS_S.MNR_BSU_ID = '" & Session("sBsuid") & _
            "' and MENURIGHTS_S.MNU_module='" & Session("sModule") & _
            "') AND (MENURIGHTS_S.MNR_ROL_ID ='" & Session("sroleid") & "' and " & _
            " MENUS_M.MNU_CODE = MENURIGHTS_S.MNR_MNU_ID)or MENURIGHTS_S.MNR_MNU_ID is  null " & _
            " where MNU_CODE='" & MNU_CODE & "'"
            'sqlString = "SELECT  MENUS_M.MNU_CODE as code,MENUACCESS_S.MNA_ROL_ID as ROL_ID,MENUACCESS_S.MNA_BSU_ID as BSU_ID," & _
            '" MENUS_M.MNU_TEXT as MNU_TEXT,MENUACCESS_S.MNA_USR_ID as usr_id, MENUACCESS_S.MNA_MODULE as M_Module, " & _
            ' " MENUS_M.MNU_NAME as MNU_Name FROM  MENUACCESS_S INNER JOIN MENUS_M " & _
            ' " ON MENUACCESS_S.MNA_MNU_ID = MENUS_M.MNU_CODE where MENUACCESS_S.MNA_USR_ID='" & Session("sUsr_id") & "'" & _
            ' " and MENUACCESS_S.MNA_BSU_ID='" & Session("sBsuid") & "' and MENUACCESS_S.MNA_ROL_ID='" & Session("sroleid") & _
            ' "' and MENUACCESS_S.MNA_MODULE = '" & Session("sModule") & "' and  MENUS_M.MNU_CODE ='" & MNU_CODE & "'"
            Dim Docemp_id As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
            If IsNumeric(Docemp_id) Then
                If CInt(Docemp_id) > 0 Then
                    Return True
                End If
            Else
                Return False
            End If
        End If
    End Function

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
      Public Shared Function GetAttQuick(ByVal contextKey As String) As DataSet
        Dim ACD_ID As String = HttpContext.Current.Session("Current_ACD_ID")
        Dim BSU_ID As String = HttpContext.Current.Session("sBsuid")
        Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
        Dim STEMP As String = String.Empty
        Dim ds As New DataSet
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@TODT", TODT)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[ATT].[ATT_PERC_QUICKVIEW]", pParms)
            'HttpContext.Current.Session("dt_ATT_quick") = ds.Tables(0)

        Catch ex As Exception

        End Try

        Return ds
    End Function

    Private Function getUrlOfMenu(ByVal Menu_Id As String) As String
        Try
            Dim MainMnu_code As String = Menu_Id
            Dim strRedirect As String = "#"
            Dim menu_rights As String = ""
            Dim MainMmnr_bsu_id As String = Session("sBsuid")
            Dim MainMmnr_rol_id As String = Session("sroleid")
            Dim usr_id As String = Session("sUsr_id")
            Dim url As String = ""
            userSuper = Session("sBusper")
            If usr_id = "" Or MainMmnr_rol_id = "" Or MainMmnr_bsu_id = "" Then
                Response.Redirect("~/login.aspx")
            Else
                Using readerUserDetail As SqlDataReader = AccessRoleUser.GetRedirectPage(MainMmnr_bsu_id, MainMnu_code, MainMmnr_rol_id, userSuper)
                    If userSuper = False Then
                        While readerUserDetail.Read()
                            menu_rights = Convert.ToString(readerUserDetail(0))
                            ' Menu_text = Convert.ToString(readerUserDetail(1))
                            strRedirect = Convert.ToString(readerUserDetail(2))
                        End While
                    Else
                        While readerUserDetail.Read()
                            ' Menu_text = Convert.ToString(readerUserDetail(0))
                            strRedirect = Convert.ToString(readerUserDetail(1))
                        End While
                        menu_rights = "6"
                    End If
                End Using
                Dim datamode As String = "none"
                Dim encrData As New Encryption64
                ' Session("Menu_text") = UtilityObj.GetFilepath(e.Item.ValuePath.Split("/"), e.Item.Depth)
                MainMnu_code = encrData.Encrypt(MainMnu_code)
                datamode = encrData.Encrypt(datamode)
                url = String.Format("{0}?MainMnu_code={1}&datamode={2}", strRedirect, MainMnu_code, datamode)
                If Trim(strRedirect) = "#" Then
                Else
                    Return url
                End If
            End If
        Catch ex As Exception
            'lblMainError.Text = "File can not be located"
        End Try
    End Function
    Protected Sub lbQuickView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Call AttQuick()
        'If MPEATT IsNot Nothing Then
        '    MPEATT.Show()
        'End If
    End Sub

    'Protected Sub U000022_Click(ByVal sender As Object, ByVal e As EventArgs) Handles U000022.Click, lbl_U000022.Click
    '    Menu_Click(Replace(sender.ID, "lbl_", ""))
    'End Sub

    'Protected Sub U000042_Click(ByVal sender As Object, ByVal e As EventArgs) Handles U000042.Click, lbl_U000042.Click
    '    Menu_Click(Replace(sender.ID, "lbl_", ""))
    'End Sub

    'Protected Sub U000024_Click(ByVal sender As Object, ByVal e As EventArgs) Handles U000024.Click, lbl_U000024.Click
    '    Menu_Click(Replace(sender.ID, "lbl_", ""))
    'End Sub

    'Protected Sub U000044_Click(ByVal sender As Object, ByVal e As EventArgs) Handles U000044.Click, lbl_U000044.Click
    '    Menu_Click(Replace(sender.ID, "lbl_", ""))
    'End Sub

    'Protected Sub U000046_Click(ByVal sender As Object, ByVal e As EventArgs) Handles U000046.Click, lbl_U000046.Click
    '    Menu_Click(Replace(sender.ID, "lbl_", ""))
    'End Sub


    
    Protected Sub dlTransaction_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlTransaction.ItemDataBound, dlApproval.ItemDataBound, dlPersonal.ItemDataBound, dlReports.ItemDataBound, dlHelp.ItemDataBound
        Try
            Dim hdnMenuCode As New HiddenField
            hdnMenuCode = e.Item.FindControl("hdnMenuCode")
            If Not hdnMenuCode Is Nothing Then
                Dim imgMenu As New ImageButton
                imgMenu = e.Item.FindControl("imgMenuImage")
                Dim lnkMenu As New LinkButton
                lnkMenu = e.Item.FindControl("lblMenuText")
                Dim tdItem As New Table
                tdItem = e.Item.FindControl("tblItem")
                Dim hdnMenuDescr As HiddenField
                hdnMenuDescr = e.Item.FindControl("hdnMenuDescr")
                If Not tdItem Is Nothing Then
                    Dim strToolTip As String
                    strToolTip = IIf(hdnMenuDescr.Value <> "", hdnMenuDescr.Value, lnkMenu.Text)
                    strToolTip = Replace(strToolTip, "<br>", Environment.NewLine)
                    tdItem.ToolTip = strToolTip
                End If
                Dim url As String
                url = getUrlOfMenu(hdnMenuCode.Value)
                If rbListView.Checked Then
                    imgMenu.Visible = False
                End If
               
                If hdnMenuCode.Value = "U000068" Then ''if it is view profile 
                    Dim USRID As String = Encr_decrData.Encrypt(Session("sUsr_id"))
                    Dim username As String = Encr_decrData.Encrypt(Session("sUsr_name"))
                    Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
                    Dim empid As String = Encr_decrData.Encrypt(Session("EmployeeId"))
                    Dim sUsrRoleID As String = Encr_decrData.Encrypt(Session("sroleid"))
                    ''Dim bsuname As String = Encr_decrData.Encrypt(Session("BSU_Name"))
                    ''Dim designation As String = Encr_decrData.Encrypt(Session("sUsr_Desig_master"))
                    Dim url2 As String
                    url2 = String.Format("https://school.gemsoasis.com/OASISNEWTHEME/EmpSelfServices/Profile2.aspx" & "?USR_ID=" + USRID + "&username=" + username + "&BSU_ID=" + BSU_ID + "&empid=" + empid + "&roleId=" + sUsrRoleID + "")
                    '' url = String.Format("http://localhost:59128/EmpSelfServices/Profile2.aspx" & "?USR_ID=" + USRID + "&username=" + username + "&BSU_ID=" + BSU_ID + "&empid=" + empid + "&roleId=" + sUsrRoleID + "")

                    Dim str As String = "window.open('" & url2 + "', 'popup_window', 'width=1400,height=900,left=10,top=10,resizable=yes');"

                    ''imgMenu.PostBackUrl = url2
                    '' lnkMenu.PostBackUrl = url2
                    lnkMenu.OnClientClick = "window.open('" & url2 + "', 'popup_window', 'width=1400,height=900,left=10,top=10,resizable=yes');"
                Else
                    imgMenu.PostBackUrl = url
                    lnkMenu.PostBackUrl = url
                End If
                'If Not tdItem Is Nothing Then
                '    tdItem.Attributes.Add("onClick", "window.location='" & Replace(url, "~/", "") & "'")
                '    tdItem.ToolTip = lnkMenu.Text
                'End If
            End If
        Catch ex As Exception

        End Try
    End Sub
   

    Protected Sub lnkTransactionMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTransactionMore.Click
        If ViewState("TransViewAll") = "True" Then
            lnkTransactionMore.Text = "more.."
            ViewState("TransViewAll") = "False"
            BindGrids()
        Else
            lnkTransactionMore.Text = "less.."
            ViewState("TransViewAll") = "True"
            BindGrids()
        End If
    End Sub

    Protected Sub lnkApproveMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveMore.Click
        If ViewState("ApprViewAll") = "True" Then
            lnkApproveMore.Text = "more.."
            ViewState("ApprViewAll") = "False"
            BindGrids()
        Else
            lnkApproveMore.Text = "less.."
            ViewState("ApprViewAll") = "True"
            BindGrids()
        End If
    End Sub

    Protected Sub lnkPersonalMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPersonalMore.Click
        If ViewState("PersViewAll") = "True" Then
            lnkPersonalMore.Text = "more.."
            ViewState("PersViewAll") = "False"
            BindGrids()
        Else
            lnkPersonalMore.Text = "less.."
            ViewState("PersViewAll") = "True"
            BindGrids()
        End If
    End Sub

    Protected Sub lnkReportsMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReportsMore.Click
        If ViewState("RepViewAll") = "True" Then
            lnkReportsMore.Text = "more.."
            ViewState("RepViewAll") = "False"
            BindGrids()
        Else
            lnkReportsMore.Text = "less.."
            ViewState("RepViewAll") = "True"
            BindGrids()
        End If
    End Sub

    Protected Sub lnkHelpMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHelpMore.Click
        If ViewState("HelpViewAll") = "True" Then
            lnkReportsMore.Text = "more.."
            ViewState("HelpViewAll") = "False"
            BindGrids()
        Else
            lnkReportsMore.Text = "less.."
            ViewState("HelpViewAll") = "True"
            BindGrids()
        End If
    End Sub

    Protected Sub rbListView_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbListView.CheckedChanged
        dlTransaction.RepeatColumns = 1
        dlApproval.RepeatColumns = 1
        dlReports.RepeatColumns = 1
        dlPersonal.RepeatColumns = 1
        dlHelp.RepeatColumns = 1
        BindGrids()
    End Sub

    Protected Sub rbGridView_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbGridView.CheckedChanged
        dlTransaction.RepeatColumns = 5
        dlApproval.RepeatColumns = 5
        dlReports.RepeatColumns = 5
        dlPersonal.RepeatColumns = 5
        dlHelp.RepeatColumns = 5
        BindGrids()
    End Sub

   
End Class
