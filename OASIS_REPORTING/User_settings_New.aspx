<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="User_settings_New.aspx.vb" Inherits="User_Settings_New" Theme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);


            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../Images/VerArrow.png";
                }
                else {
                    img.src = "../Images/VerArrow.png";
                }
                img.alt = "Close to view other details";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/HorArrow.png";
                }
                else {
                    img.src = "../Images/HorArrow.png";
                }
                img.alt = "Expand to show details";
            }



        }
    </script>

    &nbsp;<table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" width="50%">
                <asp:Literal ID="ltHeader" runat="server" Text="Setting User�s Alert"></asp:Literal>
            </td>
        </tr>
    </table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
        <tr valign="top">
            <td align="left">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
    </table>
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" width="98%">
        <tr class="subheader_img" valign="top">
            <td align="left" valign="middle">
                <table width="100%">
                    <tr>
                        <td class="subheader_img">
                            Business Unit
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlBSU_Add" runat="server" Style="margin-left: 0px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="subheader_img">
                            Employee Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtEName" runat="server" Width="173px"></asp:TextBox>
                            <asp:ImageButton ID="btnSearchEname" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                Style="width: 16px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="left">
                <table cellpadding="5" cellspacing="0" width="70%">
                    <tr>
                        <td class="matters">
                            Employee name
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlEname" runat="server" Style="margin-left: 0px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqename" Text="Please select the employee name"
                                InitialValue="0" ControlToValidate="ddlEname" runat="server" CssClass="error"
                                Display="Dynamic" ForeColor="" ValidationGroup="usr" />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">
                            Email address
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" runat="server" Width="250px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                                CssClass="error" Display="Dynamic" ErrorMessage="Email address required" ForeColor=""
                                SetFocusOnError="True" ValidationGroup="usr">Email address required</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">
                            Mobile no
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtMobNo" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMobNo"
                                CssClass="error" Display="Dynamic" ErrorMessage="Mobile no required" ForeColor=""
                                SetFocusOnError="True" ValidationGroup="usr">Mobile no required</asp:RequiredFieldValidator>
                            <br />
                            <span style="font-size: 7pt">Country-Area-Number</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <asp:Button ID="btnAddUser" runat="server" Text="Add" CssClass="button" ValidationGroup="usr" /><asp:Button
                                ID="btnUpdateUsr" runat="server" Text="Update" CssClass="button" ValidationGroup="usr" /><asp:Button
                                    ID="btnCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="2" valign="top">
                <asp:GridView ID="gvManageUser" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    PageSize="20" Width="100%" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px"
                    DataKeyNames="AUM_ID" EnableModelValidation="True">
                    <RowStyle CssClass="griditem" Height="20px" />
                    <Columns>
                        <asp:TemplateField HeaderText="AUM_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAUM_ID" runat="server" Text='<%# Bind("AUM_ID") %>'></asp:Label>
                                <asp:Label ID="lblAUM_EMP_ID" runat="server" Text='<%# Bind("AUM_EMP_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Details">
                            <ItemTemplate>
                                &nbsp; <a href="javascript:expandcollapse('div<%# Eval("AUM_ID") %>', 'one');">
                                    <img id='imgdiv<%# Eval("AUM_ID") %>' alt="Click to show/hide info" border="0" height="19px"
                                        width="32px" src="../Images/HorArrow.png" />
                                </a>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee name">
                            <ItemTemplate>
                                <asp:Label ID="lblENAMEH" runat="server" Text='<%# Bind("ENAME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email address">
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAUM_EMAIL" runat="server" Text='<%# Bind("AUM_EMAIL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mobile No">
                            <ItemTemplate>
                                <asp:Label ID="lblAUM_MOBILE" runat="server" Text='<%# bind("AUM_MOBILE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Allowed Categories">
                            <ItemTemplate>
                                <asp:Label ID="lblCatView" runat="server"></asp:Label>
                                <asp:LinkButton ID="lbtnCategories" runat="server">Edit</asp:LinkButton>
                                <div id="plR_Categories" runat="server" style="text-align: left; background-color: #ffffcc;
                                    border: solid 1px #1B80B6; color: #000000; width: 370px; height: 170px; overflow: auto;
                                    display: none;">
                                    <span style="font-size: 7pt">
                                        <asp:CheckBoxList ID="lstCategories" runat="server" BorderWidth="0px" CellPadding="2"
                                            CellSpacing="2" Font-Names="Verdana" Font-Size="10px" RepeatColumns="1" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                    </span>
                                    <div style="float: right;">
                                        <asp:LinkButton ID="btnup" runat="server" Text="Update" OnClick="BTN_CLICK" CausesValidation="false" /></div>
                                </div>
                                <ajaxToolkit:PopupControlExtender ID="pcet_Categories" runat="server" OffsetX="-10"
                                    OffsetY="-160" PopupControlID="plR_Categories" Position="top" TargetControlID="lbtnCategories">
                                </ajaxToolkit:PopupControlExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Show in all units">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnAUM_bALL_UNIT" runat="server" CausesValidation="false" ImageUrl='<%# Bind("ALL_FLAG") %>'
                                    CommandArgument='<%# Bind("ARG") %>' CommandName="ALLUNIT" OnClientClick="return confirm('Are you sure you want to update the status?'); " />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbEdit" runat="server" OnClick="lbEdit_Click" CausesValidation="false">Edit</asp:LinkButton>
                                <asp:Label ID="lblEditable" runat="server" Text='<%# Bind("Editable") %>' Visible="False"></asp:Label>
                                <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>' Visible="False"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDel" runat="server" OnClick="lbDel_Click" >Delete</asp:LinkButton>
                                
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <tr align="center" style="margin: 0px; padding: 0px; border-collapse: collapse; border-style: none;">
                                    <td colspan="100%" align="center" style="margin: 0px; padding: 0px; border-collapse: collapse;
                                        border-style: none;">
                                        <div id="div<%# Eval("AUM_ID") %>" style="display: block; position: relative; left: 5px;
                                            margin: 0px; overflow: auto; width: 100%">
                                            <table width="100%" style="margin: 0px; padding: 0px; border-collapse: collapse;
                                                border-style: none;">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvdetail" runat="server" AutoGenerateColumns="False" OnRowCommand="gvdetail_RowCommand"
                                                            DataKeyNames="R1" EmptyDataText="No record available !!!" EnableModelValidation="True"
                                                            CssClass="gridstyle" Width="98%" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px">
                                                            <RowStyle CssClass="griditem" Height="19px" />
                                                            <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAUS_ID" runat="server" Text='<%# Bind("AUS_ID") %>'></asp:Label>
                                                                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type")%>' Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sr.No">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblR1" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Incident level">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblLEVEL_DESCRP" runat="server" Text='<%# BIND("LEVEL_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Can be modified">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnAUM_bEDITED" runat="server" CausesValidation="false" ImageUrl='<%# Bind("EDIT_FLAG") %>'
                                                                            CommandArgument='<%# Bind("ARG") %>' CommandName="ISEDIT" OnClientClick="return confirm('Are you sure you want to update the status?'); " />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email alert">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnAUS_bEMAIL" runat="server" CausesValidation="false" ImageUrl='<%# Bind("EMAIL_FLAG") %>'
                                                                            CommandArgument='<%# Bind("ARG") %>' CommandName="ISEMAIL" OnClientClick="return confirm('Are you sure you want to update the status?'); " />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SMS alert">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnAUS_bSMS" runat="server" CausesValidation="false" ImageUrl='<%# Bind("SMS_FLAG") %>'
                                                                            CommandArgument='<%# Bind("ARG") %>' CommandName="ISSMS" OnClientClick="return confirm('Are you sure you want to update the status?'); " />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle CssClass="PagerStyle" />
                                                            <SelectedRowStyle BackColor="Wheat" />
                                                            <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="Wheat" />
                    <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
