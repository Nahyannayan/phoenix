Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class User_Settings_New
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> "IR05005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If

                btnCancel.Visible = False
                btnUpdateUsr.Visible = False

                BindBusinessUnits()
                gridbind_Parent()
                BindIncidentCategories()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:red;'>Request could not be processed</div>"
            End Try
        End If
    End Sub
    Private Sub BindBusinessUnits()
        Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
        Dim param(3) As SqlParameter
        Dim ds As New DataSet
        param(0) = New SqlParameter("@USR_ID", Session("sUsr_id"))
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[INC_T].[GETALERTUSER_BSU]", param)
        ddlBSU_Add.DataSource = ds.Tables(0)
        ddlBSU_Add.DataTextField = "BSU_NAME"
        ddlBSU_Add.DataValueField = "BSU_ID"
        ddlBSU_Add.DataBind()

        If Not ddlBSU_Add.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_Add.ClearSelection()
            ddlBSU_Add.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
        ddlBSU_Add_SelectedIndexChanged(ddlBSU_Add, Nothing)
    End Sub

    Private Sub bindemp()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", ddlBSU_Add.SelectedValue)
            ddlEname.DataSource = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "INC_T.GETEMPLOYEE_LIST", param)
            ddlEname.DataTextField = "ENAME"
            ddlEname.DataValueField = "EMP_ID"
            ddlEname.DataBind()
            ddlEname_SelectedIndexChanged(ddlEname, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub
    Private Sub bindINFO()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@EMP_ID", ddlEname.SelectedValue)
            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "INC_T.GETEMPLOYEE_INFO", param)
                If DATAREADER.HasRows = True Then

                    While DATAREADER.Read
                        txtEmail.Text = ""
                        txtMobNo.Text = ""
                        txtEmail.Text = Convert.ToString(DATAREADER("EMD_EMAIL"))
                        txtMobNo.Text = Convert.ToString(DATAREADER("EMD_CUR_MOBILE"))
                    End While
                Else
                    txtEmail.Text = ""
                    txtMobNo.Text = ""
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub
    Protected Sub ddlEname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEname.SelectedIndexChanged
        bindINFO()
    End Sub
    Private Sub gridbind_Parent()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
            Dim str_Sql As String = String.Empty
            Dim STR_FILTER_CATEGORY_DES As String = String.Empty
            Dim str_filter_BName As String = String.Empty
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", ddlBSU_Add.SelectedValue)
            If txtEName.Text.Trim = "" Then
                param(1) = New SqlParameter("@ENAME", System.DBNull.Value)
            Else
                param(1) = New SqlParameter("@ENAME", txtEName.Text.Trim)
            End If

            Dim ds As New DataSet

            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INC_T.GETALERTUSER_LIST", param)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INC_T.GETALERTUSER_LIST_All", param)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'ds.Tables(0).Rows(0)(8) = True
                ds.Tables(0).Rows(0)("AUM_bALL_UNIT") = True
                ds.Tables(0).Rows(0)("ALL_FLAG") = True
                gvManageUser.DataSource = ds.Tables(0)
                gvManageUser.DataBind()
                Dim columnCount As Integer = gvManageUser.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvManageUser.Rows(0).Cells.Clear()
                gvManageUser.Rows(0).Cells.Add(New TableCell)
                gvManageUser.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUser.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUser.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUser.DataSource = ds.Tables(0)
                gvManageUser.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvManageUser_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvManageUser.PageIndexChanging
        gvManageUser.PageIndex = e.NewPageIndex
        gridbind_Parent()
    End Sub

    Protected Sub ddlBSU_Add_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSU_Add.SelectedIndexChanged
        bindemp()
        gridbind_Parent()
    End Sub

    Sub changeStatus(ByVal ARG As String, ByVal FLAG As String)
        Try
            Dim ARR As String()
            ARR = ARG.Split("|")

            If ARR.Length = 3 AndAlso ARR(2) <> "1" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), _
         "script", "<script language='javascript'>alert('Changing settings for this user is not allowed!!!');  </script>", False)
                Exit Sub
            End If

            Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ROW", ARR(0))
            pParms(1) = New SqlClient.SqlParameter("@AUM_ID", ARR(1))
            pParms(2) = New SqlClient.SqlParameter("@FLAG", FLAG)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INC_T.SaveALERTUSER_STATUS", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value

            If ReturnFlag <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Error Occured While Saving.</div>"

            Else
                gridbind_Parent()
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated Successfully !!!</div>"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "changeStatus")
        End Try

    End Sub

    Protected Sub gvdetail_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvManageUser.RowCommand

        Dim ARG As String = e.CommandArgument

        If e.CommandName = "ALLUNIT" Then
            changeStatus(ARG, "ALLUNIT")

        ElseIf e.CommandName = "ISEDIT" Then
            changeStatus(ARG, "ISEDIT")
        ElseIf e.CommandName = "ISEMAIL" Then
            changeStatus(ARG, "ISEMAIL")
        ElseIf e.CommandName = "ISSMS" Then
            changeStatus(ARG, "ISSMS")
        End If

    End Sub

    Protected Sub gvManageUser_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvManageUser.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                ViewState("Indx") = e.Row.RowIndex
                Dim lblAUM_ID As Label = DirectCast(e.Row.FindControl("lblAUM_ID"), Label)
                Dim lblAUM_EMP_ID As Label = DirectCast(e.Row.FindControl("lblAUM_EMP_ID"), Label)
                Dim lblEditable As Label = DirectCast(e.Row.FindControl("lblEditable"), Label)
                Dim lblType As Label = DirectCast(e.Row.FindControl("lblType"), Label)
                Dim lbEdit As LinkButton = DirectCast(e.Row.FindControl("lbEdit"), LinkButton)
                Dim lbDel As LinkButton = DirectCast(e.Row.FindControl("lbDel"), LinkButton)
                Dim lstCategories As CheckBoxList = DirectCast(e.Row.FindControl("lstCategories"), CheckBoxList)
                Dim lblCatView As Label = DirectCast(e.Row.FindControl("lblCatView"), Label)
                Dim bt As LinkButton = DirectCast(e.Row.FindControl("btnup"), LinkButton)
                Dim lbtnCategories As LinkButton = DirectCast(e.Row.FindControl("lbtnCategories"), LinkButton)

                ViewState("cat_emp") = lblAUM_EMP_ID.Text

                If Not lstCategories Is Nothing Then
                    If Not Session("IncidentCategories") Is Nothing Then
                        Dim dsCat As DataSet = CType(Session("IncidentCategories"), DataSet)
                        lstCategories.DataValueField = "Cat_Id"
                        lstCategories.DataTextField = "Cat_Descr"
                        lstCategories.DataSource = dsCat
                        lstCategories.DataBind()
                        Me.GetEmployeeIncidentCategories(lblAUM_EMP_ID.Text, lstCategories, lblCatView)
                    End If
                End If

                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(bt)
                ScriptManager1.RegisterPostBackControl(lbDel)

                If Not lblEditable Is Nothing Then
                    If lblEditable.Text = "0" Then
                        lbEdit.Visible = False
                        lbDel.Visible = False
                        lbtnCategories.Visible = False
                        If Not lblType Is Nothing Then
                            If lblType.Text = "2" Then
                                e.Row.BackColor = Drawing.Color.Orange
                            ElseIf lblType.Text = "3" Then
                                e.Row.BackColor = Drawing.Color.LightGreen
                            End If
                        End If
                    Else
                        lbEdit.Visible = True
                        lbDel.Visible = True
                        lbtnCategories.Visible = True
                    End If
                End If

                Dim gvdetail As GridView = DirectCast(e.Row.FindControl("gvdetail"), GridView)

                Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
                Dim ds As New DataSet
                Dim sql_string As String = String.Empty
                Dim PARAM(2) As SqlParameter
                PARAM(0) = New SqlParameter("@AUM_ID", lblAUM_ID.Text)
                PARAM(1) = New SqlParameter("@TYPE", lblType.Text)

                'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INC_T.GETALERTUSER_LIST_INFO", PARAM)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INC_T.GETALERTUSER_LIST_INFO_ALL", PARAM)
                If ds.Tables(0).Rows.Count = 0 Then
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                    'ds.Tables(0).Rows(0)(8) = True
                    ds.Tables(0).Rows(0)("EDIT_FLAG") = True
                    ds.Tables(0).Rows(0)("EMAIL_FLAG") = True
                    ds.Tables(0).Rows(0)("SMS_FLAG") = True

                    gvdetail.DataSource = ds.Tables(0)
                    gvdetail.DataBind()
                    Dim columnCount As Integer = gvManageUser.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvdetail.Rows(0).Cells.Clear()
                    gvdetail.Rows(0).Cells.Add(New TableCell)
                    gvdetail.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvdetail.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvdetail.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                Else
                    gvdetail.DataSource = ds.Tables(0)
                    gvdetail.DataBind()
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub

    Protected Sub btnSearchEname_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchEname.Click
        gridbind_Parent()
    End Sub


    Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("AUM_ID") = 0
            Dim lblAUM_ID As New Label
            Dim lblAUM_EMP_ID As New Label
            Dim lblAUM_EMAIL As New Label
            Dim lblAUM_MOBILE As New Label
            'Dim lbEdit As New LinkButton

            lblAUM_ID = TryCast(sender.FindControl("lblAUM_ID"), Label)
            lblAUM_EMP_ID = TryCast(sender.FindControl("lblAUM_EMP_ID"), Label)
            lblAUM_EMAIL = TryCast(sender.FindControl("lblAUM_EMAIL"), Label)
            lblAUM_MOBILE = TryCast(sender.FindControl("lblAUM_MOBILE"), Label)
            '  lbEdit = TryCast(sender.FindControl("lbEdit"), LinkButton)

            ViewState("AUM_ID") = lblAUM_ID.Text
            If Not ddlEname.Items.FindByValue(lblAUM_EMP_ID.Text) Is Nothing Then
                ddlEname.ClearSelection()
                ddlEname.Items.FindByValue(lblAUM_EMP_ID.Text).Selected = True
                ddlEname.Enabled = False
            End If
            txtEmail.Text = lblAUM_EMAIL.Text
            txtMobNo.Text = lblAUM_MOBILE.Text

            ddlBSU_Add.Enabled = False
            btnAddUser.Visible = False
            btnUpdateUsr.Visible = True
            btnCancel.Visible = True
            'lbEdit.Enabled = False
            gvManageUser.Columns(6).Visible = False

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Error while selecting record for editing !!!</div>"
        End Try
    End Sub
    Protected Sub lbDel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("AUM_ID") = 0
            Dim lblAUM_ID As New Label
            Dim lblAUM_EMP_ID As New Label
            Dim lblAUM_EMAIL As New Label
            Dim lblAUM_MOBILE As New Label
            'Dim lbEdit As New LinkButton

            lblAUM_ID = TryCast(sender.FindControl("lblAUM_ID"), Label)
            lblAUM_EMP_ID = TryCast(sender.FindControl("lblAUM_EMP_ID"), Label)
            lblAUM_EMAIL = TryCast(sender.FindControl("lblAUM_EMAIL"), Label)
            lblAUM_MOBILE = TryCast(sender.FindControl("lblAUM_MOBILE"), Label)
            '  lbEdit = TryCast(sender.FindControl("lbEdit"), LinkButton)

            ViewState("AUM_ID") = lblAUM_ID.Text

            Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
            Dim ds As New DataSet
            Dim sql_string As String = String.Empty
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@AUM_ID", lblAUM_ID.Text)
            PARAM(1) = New SqlParameter("@USR_ID", Session("EmployeeId"))


            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INC_T.saveALERTUSERS_AUDIT", PARAM)



            gridbind_Parent()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            'lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Error while selecting record for editing !!!</div>"
        End Try
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ddlBSU_Add.Enabled = True
        btnAddUser.Visible = True
        btnUpdateUsr.Visible = False
        btnCancel.Visible = False
        txtEmail.Text = ""
        txtMobNo.Text = ""
        If Not ddlEname.Items.FindByValue("0") Is Nothing Then
            ddlEname.ClearSelection()
            ddlEname.Items.FindByValue("0").Selected = True
            ddlEname.Enabled = True
        End If
        gvManageUser.Columns(6).Visible = True
    End Sub

    Protected Sub btnUpdateUsr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateUsr.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(True, errorMessage)
        If str_err = "0" Then
            btnAddUser.Visible = True
            btnUpdateUsr.Visible = False
            btnCancel.Visible = False
            ddlEname.Enabled = True
            ddlBSU_Add.Enabled = True
            If Not ddlEname.Items.FindByValue("0") Is Nothing Then
                ddlEname.ClearSelection()
                ddlEname.Items.FindByValue("0").Selected = True
                ddlEname.Enabled = True
            End If
            txtEmail.Text = ""
            txtMobNo.Text = ""
            gridbind_Parent()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record Saved Successfully.</div>"

        Else
            lblError.InnerHtml = errorMessage
        End If
    End Sub
    Protected Sub btnAddUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUser.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ViewState("AUM_ID") = "0"
        str_err = calltransaction(False, errorMessage)
        If str_err = "0" Then
            btnAddUser.Visible = True
            btnUpdateUsr.Visible = False
            btnCancel.Visible = False
            txtEmail.Text = ""
            txtMobNo.Text = ""
            If Not ddlEname.Items.FindByValue("0") Is Nothing Then
                ddlEname.ClearSelection()
                ddlEname.Items.FindByValue("0").Selected = True
                ddlEname.Enabled = True
            End If
            gridbind_Parent()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record Saved Successfully.</div>"
        Else
            lblError.InnerHtml = errorMessage
        End If
    End Sub
    Function calltransaction(ByVal bEdit As Boolean, ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty
        Dim AUM_ID As String = String.Empty
        Dim AUM_EMP_ID As String = String.Empty
        Dim AUM_EMAIL As String = String.Empty
        Dim AUM_MOBILE As String = String.Empty

        Using conn As SqlConnection = ConnectionManger.GetOASIS_REPORTINGConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim PARAM(7) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@AUM_ID", ViewState("AUM_ID"))
                PARAM(1) = New SqlClient.SqlParameter("@AUM_BSU_ID", ddlBSU_Add.SelectedValue)
                PARAM(2) = New SqlClient.SqlParameter("@AUM_EMP_ID", ddlEname.SelectedValue)
                PARAM(3) = New SqlClient.SqlParameter("@AUM_EMAIL", txtEmail.Text)
                PARAM(4) = New SqlClient.SqlParameter("@AUM_MOBILE", txtMobNo.Text)
                PARAM(5) = New SqlClient.SqlParameter("@bEdit", bEdit)
                PARAM(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(6).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "INC_T.SAVEALERTUSER_INFO", PARAM)
                ReturnFlag = PARAM(6).Value

                If ReturnFlag = 11 Then
                    calltransaction = 1
                ElseIf ReturnFlag <> 0 Then
                    calltransaction = 1
                    errorMessage = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;color:red;padding:5pt;background-color:#edf3fa;'>Error occured while saving !!!</div>"
                End If
            Catch ex As Exception
                sysErr = ex.Message
                calltransaction = 1
                errorMessage = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Error occured while saving !!!</div>"
            Finally
                If calltransaction <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using
    End Function

    Private Sub BindIncidentCategories()
        Try
            Dim dsCategories As DataSet
            dsCategories = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_REPORTINGConnectionString, CommandType.Text, "SELECT CAT_ID, CAT_DESCR FROM INC_M.CATEGORY_M WITH (NOLOCK) ORDER BY CAT_DISPLAYORDER")
            If Not dsCategories Is Nothing Then
                Session.Add("IncidentCategories", dsCategories)
            End If
        Catch ex As Exception
            lblError.InnerText = "Error loading incident category list"
        End Try
    End Sub

    Private Sub GetEmployeeIncidentCategories(ByVal Emp_Id As String, ByRef ChkList As CheckBoxList, ByRef lbl As Label)
        Try
            Dim dsCategories As DataSet
            dsCategories = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_REPORTINGConnectionString, CommandType.Text, "SELECT CBU_CAT_ID FROM INC_M.CATEGORY_BASED_USERS WHERE CBU_EMP_ID = " & Emp_Id & " AND CBU_bACTIVE = 'TRUE'")
            If Not dsCategories Is Nothing AndAlso dsCategories.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ChkList.Items.Count - 1
                    If dsCategories.Tables(0).Select("cbu_cat_id = " & ChkList.Items(i).Value).Length > 0 Then
                        ChkList.Items(i).Selected = True

                    End If
                Next
            Else
                For i As Integer = 0 To ChkList.Items.Count - 1
                    ChkList.Items(i).Selected = True
                Next
            End If
            For i As Integer = 0 To ChkList.Items.Count - 1
                If ChkList.Items(i).Selected = True Then
                    lbl.Text = lbl.Text + " <br>" + ChkList.Items(i).Text + " <br>"
                End If
            Next

        Catch ex As Exception
            lblError.InnerText = "Error getting incident category list assigned to the user(s)"
        End Try
    End Sub
    Sub BTN_CLICK(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim bt As LinkButton = DirectCast(sender, LinkButton)
        Dim gr As GridViewRow = DirectCast(bt.Parent.Parent.Parent, GridViewRow)

        Dim lblAUM_EMP_ID As Label = DirectCast(gr.FindControl("lblAUM_EMP_ID"), Label)
        Dim ChkList As CheckBoxList = gr.FindControl("lstCategories")
        Dim str As String = ""


        For i As Integer = 0 To ChkList.Items.Count - 1
            If ChkList.Items(i).Selected = True Then
                str = str + ChkList.Items(i).Value + "|"
            End If
        Next
        Dim str_conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CBU_EMP_ID", lblAUM_EMP_ID.Text)
        pParms(1) = New SqlClient.SqlParameter("@CBU_CAT_ID", str)
        pParms(2) = New SqlClient.SqlParameter("@USR_ID", Session("EmployeeId"))

        pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INC_M.SAVE_CATEGORY_BASED_USERS", pParms)
        Dim ReturnFlag As Integer = pParms(3).Value
        If ReturnFlag = 0 Then
            gridbind_Parent()
        End If

    End Sub
End Class
