Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj
 
Partial Class UserControls_usrBSUnitsIncident
    Inherits System.Web.UI.UserControl

    Public Property MenuCode() As String
        Get
            Return h_MenuCode.Value
        End Get
        Set(ByVal value As String)
            h_MenuCode.Value = value
        End Set
    End Property
    Public Property ExpandFirstNodeOnStart() As String
        Get
            Return IIf(ViewState("ExpandFirstNodeOnStart"), True, False)
        End Get
        Set(ByVal value As String)
            ViewState("ExpandFirstNodeOnStart") = value
        End Set
    End Property

    Public Function GetSelectedNode(Optional ByVal seperator As String = "||") As String
        Dim strBSUnits As New StringBuilder
        For Each node As TreeNode In tvBusinessunit.CheckedNodes
            If node.Value.Length > 2 Then
                strBSUnits.Append(node.Value)
                strBSUnits.Append(seperator)
            End If
        Next
        Return strBSUnits.ToString()
    End Function

    Public Sub ExpandOne(ByVal Level As Integer)
        tvBusinessunit.ExpandDepth = Level
        tvBusinessunit.DataBind()
    End Sub

    Protected Sub tvBusinessunit_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBusinessunit.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
      ByVal parentNode As TreeNode)
        PopulateNodes(Me.GetBsuTree_Incident_WithRights(Session("sBusper"), Session("sUsr_id"), parentid, h_MenuCode.Value), parentNode.ChildNodes)
    End Sub

    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        tvBusinessunit.Nodes.Clear()
        str_Sql = "SELECT 0 AS BSU_ID,'All' AS BSU_NAME, COUNT (*) AS childnodecount FROM BUSSEGMENT_M BSG"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvBusinessunit.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            tn.Checked = True
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        tvBusinessunit.Nodes.Clear()
        PopulateRootLevel()
        tvBusinessunit.DataBind()
        tvBusinessunit.CollapseAll()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            PopulateTree()
            'Me.tvBusinessunit.Nodes(0).Checked = True
            If ExpandFirstNodeOnStart Then tvBusinessunit.Nodes(0).Expand()
        End If
    End Sub

    Public Function SetSelectedNodes(ByVal strBSUid As String) As Boolean
        For i As Integer = 0 To tvBusinessunit.CheckedNodes.Count - 1
            tvBusinessunit.CheckedNodes(i).Checked = False
        Next
        Dim IDs As String() = strBSUid.Split("||")
        Dim condition As String = String.Empty
        For i As Integer = 0 To IDs.Length - 1
            CheckSelectedNode(IDs(i), tvBusinessunit.Nodes)
        Next
        Return True
    End Function

    Private Sub CheckSelectedNode(ByVal vBSU_ID As String, ByVal vNodes As TreeNodeCollection)
        Dim ienum As IEnumerator = vNodes.GetEnumerator()
        Dim trNode As TreeNode
        While (ienum.MoveNext())
            trNode = ienum.Current
            If trNode.ChildNodes.Count > 0 Then
                CheckSelectedNode(vBSU_ID, trNode.ChildNodes)
            ElseIf trNode.Value = vBSU_ID Then
                trNode.Checked = True
            End If
        End While
    End Sub

    Private Function GetBsuTree_Incident_WithRights(ByVal bSuper As Boolean, ByVal USER As String, _
        ByVal ParentId As String, ByVal MenuID As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@bSuper", SqlDbType.Bit)
        pParms(0).Value = bSuper
        pParms(1) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(1).Value = USER
        pParms(2) = New SqlClient.SqlParameter("@ParentId", SqlDbType.VarChar)
        pParms(2).Value = ParentId
        pParms(3) = New SqlClient.SqlParameter("@MenuID", SqlDbType.VarChar)
        pParms(3).Value = MenuID
        'pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        'pParms(3).Direction = ParameterDirection.ReturnValue
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "GetBsuTree_Incident_WithRights", pParms)
        Return dsData.Tables(0)
    End Function

End Class

