﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Imports System.Data.SqlTypes

Imports GemBox.Spreadsheet
Imports ResponseHelper
Partial Class GenUpdates_EmployeeComplianceGlobal
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'collect the url of the file to be redirected in view state
                If (ViewState("MainMnu_code") <> "H000280") Then


                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                Else

                    rbAll.Checked = True
                    btnAppr.Enabled = False
                    btnOverRide.Enabled = False



                    bindBusinessunit()
                    bindCategories()

                    If ddlbusinessunit.SelectedItem.Value <> "-1" Then
                        getreenroll_count()

                    End If
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub

    Protected Sub gvReEnrol_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkSelect As CheckBox = DirectCast(e.Row.FindControl("chkList"), CheckBox)
            Dim lblDispStatus As Label = DirectCast(e.Row.FindControl("lbldisstatus"), Label)

            If lblDispStatus IsNot Nothing Then

                If lblDispStatus.Text = "ACCEPTED" Then
                    chkSelect.Enabled = False
                Else
                    chkSelect.Enabled = True
                End If
            End If
        End If


    End Sub

    Private Sub bindBusinessunit()
        Dim str_conn As String = ConnectionManger.GetOASIS_COMTRACK_ConnectionString
        Dim di As ListItem

        Try


            Dim param(10) As SqlParameter
            param(0) = New SqlParameter("@pBSU_ID", System.DBNull.Value)
            param(1) = New SqlParameter("@pNAME", System.DBNull.Value)
            param(2) = New SqlParameter("@pTMPL", 0)
            param(3) = New SqlParameter("@pLOG_USR", Session("sUsr_id"))
            param(4) = New SqlParameter("@pOPTION", 9)
            param(5) = New SqlParameter("@pEMP_ID", System.DBNull.Value)


            'ddlAcademicYear.Items.Clear()

            '   ddlbusinessunit.Items.Clear()
            di = New ListItem("ALL", "0")
            ddlbusinessunit.Items.Add(di)


            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "COMTRACK_EMPLOYEE_COMPLIANCE_GLOBAL", param)
                If reader.HasRows Then
                    ddlbusinessunit.DataSource = reader
                    ddlbusinessunit.DataTextField = "BSU_NAME"
                    ddlbusinessunit.DataValueField = "BSU_ID"
                    ddlbusinessunit.DataBind()



                End If
            End Using


            For ItemTypeCounter As Integer = 0 To ddlbusinessunit.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex

                If ddlbusinessunit.Items(ItemTypeCounter).Value = "-1" Then
                    ddlbusinessunit.SelectedIndex = ItemTypeCounter
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Error in COMP")
        End Try
    End Sub

    Private Sub bindCategories()
        Dim str_conn As String = ConnectionManger.GetOASIS_COMTRACK_ConnectionString
        Dim di As ListItem


        Dim param(10) As SqlParameter
        param(0) = New SqlParameter("@pBSU_ID", System.DBNull.Value)
        param(1) = New SqlParameter("@pNAME", System.DBNull.Value)
        param(2) = New SqlParameter("@pTMPL", 0)
        param(3) = New SqlParameter("@pLOG_USR", Session("sUsr_id"))
        param(4) = New SqlParameter("@pOPTION", 53)
        param(5) = New SqlParameter("@pEMP_ID", System.DBNull.Value)


        'ddlAcademicYear.Items.Clear()

        ddlCategory.Items.Clear()
        di = New ListItem("ALL", "0")
        ddlCategory.Items.Add(di)


        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "COMTRACK_EMPLOYEE_COMPLIANCE_GLOBAL", param)
            If reader.HasRows Then
                ddlCategory.DataSource = reader
                ddlCategory.DataTextField = "TMPL_DESCR"
                ddlCategory.DataValueField = "TMPL_ID"
                ddlCategory.DataBind()
            End If
        End Using


        For ItemTypeCounter As Integer = 0 To ddlCategory.Items.Count - 1
            'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex

            If ddlCategory.Items(ItemTypeCounter).Value = "-1" Then
                ddlCategory.SelectedIndex = ItemTypeCounter
            End If

        Next

    End Sub

    Protected Sub ddlbusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlbusinessunit.SelectedIndexChanged
        Session("hashCheck") = Nothing
        If ddlbusinessunit.SelectedItem.Value <> "-1" Then
            getreenroll_count()
        End If

    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Session("hashCheck") = Nothing
        If ddlbusinessunit.SelectedItem.Value <> "-1" Then
            getreenroll_count()
        End If

    End Sub

    Private Sub getEmailAlreadySent()
        Dim str_conn As String = ConnectionManger.GetOASIS_COMTRACK_ConnectionString
        Dim di As ListItem


        Dim param(10) As SqlParameter
        Dim Status As String = String.Empty
        param(0) = New SqlParameter("@pBSU_ID", ddlbusinessunit.SelectedItem.Value)
        param(1) = New SqlParameter("@pNAME", System.DBNull.Value)
        param(2) = New SqlParameter("@pTMPL", ddlCategory.SelectedItem.Value)
        param(3) = New SqlParameter("@pLOG_USR", Session("sUsr_id"))
        param(4) = New SqlParameter("@pOPTION", 52)
        param(5) = New SqlParameter("@pEMP_ID", System.DBNull.Value)


        Dim emailCount As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COMTRACK_EMPLOYEE_COMPLIANCE_GLOBAL", param)


        If emailCount > 0 Then
            btnAppr.Visible = True
            btnOverRide.Visible = True
            btnResendAll.Visible = False


        Else
            btnAppr.Visible = True
            btnOverRide.Visible = False
            btnResendAll.Visible = True

        End If


    End Sub


    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetEmlCountDetails(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        Dim VContKeys As String = contextKey
        Dim splitITem As String() = VContKeys.Split("$")

        Dim drReader As SqlDataReader
        Try


            Dim str_Sql As String = "SELECT EML_TYPE,EML_RESPONSE,EML_PROFILE_ID,EML_TOEMAIL,EML_SUBJECT,EML_DATE FROM dbo.[COMPLIANCE_EMAIL_SCHEDULE] WHERE EML_TYPE= " & splitITem(1) & " and EML_PROFILE_ID =" & splitITem(0) & " order by EML_DATE "
            Dim str_conn As String = ConnectionManger.GetOASIS_COMTRACK_ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class='popdetails'>")
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=4><b>Details</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            sTemp.Append("<td><b>Subject</b></td>")
            sTemp.Append("<td><b>To Email</b></td>")
            sTemp.Append("<td><b>Date</b></td>")
            sTemp.Append("<td><b>Status</b></td>")
            sTemp.Append("</tr>")
            While (drReader.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drReader("EML_SUBJECT").ToString & "</td>")
                sTemp.Append("<td>" & drReader("EML_TOEMAIL").ToString & "</td>")
                sTemp.Append("<td>" & drReader("EML_DATE").ToString & "</td>")
                sTemp.Append("<td>" & drReader("EML_RESPONSE").ToString & "</td>")
                sTemp.Append("</tr>")
            End While

        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function


    Private Sub getreenroll_count()
        Dim str_conn As String = ConnectionManger.GetOASIS_COMTRACK_ConnectionString
        Dim di As ListItem


        Dim param(10) As SqlParameter
        param(0) = New SqlParameter("@pBSU_ID", ddlbusinessunit.SelectedItem.Value)
        param(1) = New SqlParameter("@pNAME", System.DBNull.Value)
        param(2) = New SqlParameter("@pTMPL", ddlCategory.SelectedItem.Value)
        param(3) = New SqlParameter("@pLOG_USR", Session("sUsr_id"))
        param(4) = New SqlParameter("@pOPTION", 10)
        param(5) = New SqlParameter("@pEMP_ID", System.DBNull.Value)

        Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "COMTRACK_EMPLOYEE_COMPLIANCE_GLOBAL", param)
            If dr.HasRows = True Then
                While dr.Read
                    lblReenroll_Count.Text = "<div style='padding-top:10px'>Total Staff  <font color='red'>" & Convert.ToString(dr("READS")) & " </font>&nbsp;&nbsp;&nbsp;Accepted <font color='red'>" & Convert.ToString(dr("TOTAL_PARTICIPATED")) & _
                    "</font>&nbsp;&nbsp;&nbsp;   Not Accepted  <font color='red'>" & _
                    Convert.ToString(dr("NOTS")) & "</font>&nbsp;&nbsp;&nbsp;   </div>"


                End While
            Else
            End If

        End Using
    End Sub

    Public Sub gridbind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_COMTRACK_ConnectionString
            Dim ds As New DataSet
            Dim txtSearch As New TextBox
            Dim str_query As String = String.Empty
            Dim STU_NO As String = String.Empty
            Dim str_STU_NO As String = String.Empty

            Dim SNAME As String = String.Empty
            Dim str_SNAME As String = String.Empty

            Dim CONTACTNAME As String = String.Empty
            Dim str_CONTACTNAME As String = String.Empty


            Dim CONTACTMOBILE As String = String.Empty
            Dim str_CONTACTMOBILE As String = String.Empty

            Dim CONTACTMAIL As String = String.Empty
            Dim str_CONTACTMAIL As String = String.Empty

            Dim FLAG_STATUS As String = String.Empty
            Dim str_FLAG_STATUS As String = String.Empty
            Dim GRD_SCT As String = String.Empty
            Dim str_GRD_SCT As String = String.Empty
            Dim FILTER_COND As String = String.Empty
            Dim FILTER_ENROLL As String = String.Empty
            Dim lintOPtion
            btnAppr.Enabled = True
            btnOverRide.Enabled = True

            If rbNotRead.Checked = True Then
                lintOPtion = 11
            ElseIf rbNotUpdated.Checked = True Then
                lintOPtion = 2
            ElseIf rbUpdated.Checked = True Then
                lintOPtion = 3
                btnAppr.Enabled = False
                btnOverRide.Enabled = False
            Else
                lintOPtion = 17
                btnAppr.Enabled = True
                btnOverRide.Enabled = False
            End If
            Dim param(10) As SqlParameter
            param(0) = New SqlParameter("@pBSU_ID", ddlbusinessunit.SelectedItem.Value)
            param(1) = New SqlParameter("@pNAME", txtEMPNAME.Text)
            param(2) = New SqlParameter("@pTMPL", ddlCategory.SelectedItem.Value)
            param(3) = New SqlParameter("@pLOG_USR", Session("sUsr_id"))
            param(4) = New SqlParameter("@pOPTION", lintOPtion)
            param(5) = New SqlParameter("@pEMP_ID", Session("EmployeeId"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COMTRACK_EMPLOYEE_COMPLIANCE_GLOBAL", param)

            If ds.Tables(0).Rows.Count > 0 Then
                gvReEnrol.DataSource = ds.Tables(0)
                gvReEnrol.DataBind()

                getEmailAlreadySent()
                If ddlbusinessunit.SelectedItem.Value <> "-1" Then
                    getreenroll_count()
                End If

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(6) = True
                'ds.Tables(0).Rows(0)("bTC") = False
                gvReEnrol.DataSource = ds.Tables(0)
                Try
                    gvReEnrol.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvReEnrol.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvReEnrol.Rows(0).Cells.Clear()
                gvReEnrol.Rows(0).Cells.Add(New TableCell)
                gvReEnrol.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReEnrol.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReEnrol.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvRef_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReEnrol.PageIndexChanged
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If

        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
            ' txtRemark = DirectCast((rowItem.Cells(0).FindControl("txtRemark")), TextBox)
            STU_ID = gvReEnrol.DataKeys(rowItem.RowIndex)("EMP_ID").ToString()
            If hash.Contains(STU_ID) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next
    End Sub

    Protected Sub gvRef_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReEnrol.PageIndexChanging
        gvReEnrol.PageIndex = e.NewPageIndex
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If

        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)


            STU_ID = gvReEnrol.DataKeys(rowItem.RowIndex)("EMP_ID").ToString()
            If chk.Checked = True Then
                If hash.Contains(STU_ID) = False Then
                    hash.Add(STU_ID, "")
                End If

            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If
        Next
        Session("hashCheck") = hash
        gridbind()
    End Sub

    Protected Sub btnSearchFLAG_STATUS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchCONTACTMOBILE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing

        gridbind()
    End Sub

    Protected Sub btnSearchCONTACTMAIL_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchCONTACTNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchGRD_SCT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnAppr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppr.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If


        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        str_err = calltransaction(errorMessage, "SELECTED")





        If str_err = "0" Then
            lblError.Text = "Mail Sent Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub
    Protected Sub btnOverRide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOverRide.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean



        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        str_err = calltransaction(errorMessage, "ALL")
        If str_err = "0" Then
            lblError.Text = "Mail Sent Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub

    Protected Sub btnResendAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResendAll.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean



        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        str_err = calltransaction(errorMessage, "NEWEMAIL")
        If str_err = "0" Then
            lblError.Text = "Mail Sent Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub

    Function calltransaction(ByRef errorMessage As String, ByVal FLAG As String, Optional ByVal ACTION As String = "NOTHING") As Integer
        Dim ACD_ID As String = ddlbusinessunit.SelectedValue
        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty
        Dim STU_IDS As New StringBuilder
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If




        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            STU_ID = DirectCast(rowItem.FindControl("lblSTU_ID"), Label).Text

            If chk.Checked = True Then
                'If hash.Contains(RF_ID) = False Then
                '    hash.Add(RF_ID, DirectCast(rowItem.FindControl("lblRF_ID"), Label).Text)
                'End If

                hash(STU_ID) = ""
            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If

        Next
        Session("hashCheck") = hash
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry
            For Each hashloop In hash
                STU_IDS.Append(hashloop.Key.ToString)
                STU_IDS.Append("|")

            Next
        End If

        Dim lintOPtion
        Dim lstrEMPID
        If FLAG = "ALL" Then
            lintOPtion = 6
            lstrEMPID = ""
        ElseIf FLAG = "NEWEMAIL" Then
            lintOPtion = 8
            lstrEMPID = ""
        Else
            lintOPtion = 7
            lstrEMPID = STU_IDS.ToString
        End If

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_COMTRACKConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer
                Dim param(10) As SqlParameter
                param(0) = New SqlParameter("@pBSU_ID", ddlbusinessunit.SelectedItem.Value)
                param(1) = New SqlParameter("@pNAME", txtEMPNAME.Text)
                param(2) = New SqlParameter("@pTMPL", ddlCategory.SelectedItem.Value)
                param(3) = New SqlParameter("@pLOG_USR", Session("sUsr_id"))
                param(4) = New SqlParameter("@pOPTION", lintOPtion)
                param(5) = New SqlParameter("@pEMP_ID", lstrEMPID)
                param(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                param(6).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "COMTRACK_EMPLOYEE_COMPLIANCE_GLOBAL", param)
                status = param(6).Value

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If

                Session("hashCheck") = Nothing
                calltransaction = "0"

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                    gridbind()
                    If ddlbusinessunit.SelectedItem.Value <> "-1" Then
                        getreenroll_count()
                    End If
                End If
            End Try

        End Using
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn As String = ConnectionManger.GetOASIS_COMTRACK_ConnectionString
        Dim ds As New DataSet
        Dim str_query As String
        Dim lstrExportType As Integer

        Dim lintOPtion
        btnAppr.Enabled = True
        btnOverRide.Enabled = True

        If rbNotRead.Checked = True Then
            lintOPtion = 11
        ElseIf rbNotUpdated.Checked = True Then
            lintOPtion = 2
        ElseIf rbUpdated.Checked = True Then
            lintOPtion = 3
            btnAppr.Enabled = False
            btnOverRide.Enabled = False
        Else
            lintOPtion = 1
            btnAppr.Enabled = True
            btnOverRide.Enabled = False
        End If


        Dim param(10) As SqlParameter
        param(0) = New SqlParameter("@pBSU_ID", ddlbusinessunit.SelectedItem.Value)
        param(1) = New SqlParameter("@pNAME", txtEMPNAME.Text)
        param(2) = New SqlParameter("@pTMPL", ddlCategory.SelectedItem.Value)
        param(3) = New SqlParameter("@pLOG_USR", Session("sUsr_id"))
        param(4) = New SqlParameter("@pOPTION", lintOPtion)
        param(5) = New SqlParameter("@pEMP_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COMTRACK_TRANS_M_EXCEL", param)


        Dim dtEXCEL As New DataTable
        dtEXCEL = ds.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT_PWD_POLICY")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            ' ws.HeadersFooters.AlignWithMargins = True


            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
