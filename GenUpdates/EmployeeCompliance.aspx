﻿<%@ Page  Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EmployeeCompliance.aspx.vb" Inherits="GenUpdates_EmployeeCompliance" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">



        function confirm_Reset() {

            if (confirm("You are about to resend the email invitation to the selected employee(s).Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function confirm_OverRide() {
          

            if (confirm("You are about to resend the email invitation to all employee(s) who havent participated.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function confirm_Register() {

            if (confirm("You are about to Register & Re-Enroll the selected student(s).Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function confirm_sendAll() {

            if (confirm("You are about to send the email invitation to all employee(s) who havent participated.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkList") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }
          </script>
    <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%; height: 27px;">
              Staff Compliance</td>
        </tr>
    </table>
    
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
        cellspacing="0">
        <tr>
            <td align="center">
                
                    <div align="center">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="12px"></asp:Label></div>
                  
            </td>
        </tr>
        <tr>
          <td align="center">
             <table align="center" border="1" bordercolor="#1b80b6" cellpadding="2" cellspacing="0"
                    style="width: 70%; border-collapse:collapse;" >
                    <tr class="subheader_img">
                        <td align="center" colspan="6" style="height: 1px" valign="middle">
                            <div align="left">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                   Search Filter</font></div>
                        </td>
                    </tr>
                   
                    <tr>
                        <td align="left" class="matters" width="140px">
                            Business Unit</td>
                        <td align="center" class="matters">
                            :</td>
                        <td align="left" class="matters" style="text-align: left" colspan="4">
                            <asp:DropDownList id="ddlbusinessunit" runat="server" AutoPostBack="True">
                            </asp:DropDownList>

                     
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="140px">
                            Category</td>
                        <td align="center" class="matters">
                            :</td>
                        <td align="left" class="matters" style="text-align: left" colspan="4">
                            <asp:DropDownList id="ddlCategory" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    
                    
                       <tr>
                        <td align="left" class="matters" width="140px">
                           Employee Name</td>
                        <td align="center" class="matters">
                            :</td>
                        <td align="left" class="matters" style="text-align: left" colspan="4">
                           
                           <asp:TextBox ID="txtEMPNAME" runat="server" MaxLength="20" Width="485" ></asp:TextBox>
                    <ajaxToolkit:TextBoxWatermarkExtender ID="txtBoxWExt" runat="server"
            TargetControlID="txtEMPNAME" WatermarkText="Employee Name"  WatermarkCssClass="watermarkedtextIR">
           </ajaxToolkit:TextBoxWatermarkExtender>
                           
                           </td>
                    </tr>
                   
                  
                    <tr>
                        <td align="left" class="matters">
                            Update Status</td>
                        <td align="center" class="matters">
                            :</td>
                        <td align="left" class="matters" style="text-align: left" colspan="4">
                             <asp:RadioButton ID="rbNotRead" runat="server" Text="Not Read"  GroupName="reenroll" Visible="false" />
                            <asp:RadioButton ID="rbNotUpdated" runat="server" Text="Not Updated"   GroupName="reenroll"/>
                            <asp:RadioButton ID="rbUpdated" runat="server" Text="Updated"  GroupName="reenroll"/>
                             <asp:RadioButton ID="rbAll" runat="server" Text="All"  GroupName="reenroll" />
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search"                   Width="70px" />
                           <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export"                   Width="70px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbReenroll" runat="server" Text="Reenrolled"  AutoPostBack="true" GroupName="reenroll" Visible="false"/>
                            <br />
                            <asp:RadioButton ID="rbFullPaidNotReenrolled" runat="server" Text="Full Fee Paid (Not Reenrolled)"
                              AutoPostBack="true" GroupName="reenroll" Visible="false"/>
                             <asp:RadioButton ID="rbTCStudent" runat="server" Text="TC Students" AutoPostBack="true" GroupName="reenroll" Visible="false"/>
                            <asp:RadioButton ID="rbRefund" runat="server" Text="Refund"  AutoPostBack="true" GroupName="reenroll" Visible="false"/>  
                            
                            
                              </td>
                              
                              
                    </tr>
                  
                </table>
        
        </td>
        </tr>
         <tr><td align="center" class="matters"><asp:Label ID="lblReenroll_Count" runat="server" ></asp:Label></td></tr>
       <tr><td>&nbsp;</td></tr>
        <tr>
            <td class="matters" align="center">
        
                <asp:GridView id="gvReEnrol" runat="server" AutoGenerateColumns="False"
                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    PageSize="20"  DataKeyNames="EMP_ID" AllowPaging="True">
                    <rowstyle cssclass="griditem" height="20px" />
                    <columns>
<asp:TemplateField HeaderText="Select"><HeaderTemplate>
<asp:CheckBox id="chkAll" onclick="javascript:change_chk_state(this);" 
runat="server"></asp:CheckBox>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="chkList" runat="server"></asp:CheckBox> 
   <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("EMP_ID") %>' Visible="false"></asp:Label>
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>


<asp:TemplateField HeaderText="BSU">
                        
<ItemTemplate><asp:label ID="lblBSU_NAME" runat="server"        Text='<%# Bind("BSU_NAME") %>'></asp:label>
        
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        
                        
                        

                        <asp:TemplateField HeaderText="Emp No">
                       
<ItemTemplate><asp:label ID="lblSTU_NO" runat="server"        Text='<%# Bind("EMPNO") %>'></asp:label>
        
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                        </asp:TemplateField>

<asp:TemplateField HeaderText="Emp Name">

<ItemTemplate>   
       <asp:label ID="lblSName" runat="server"        Text='<%# Bind("EMP_NAME") %>'></asp:label> 
        
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>
 
<asp:TemplateField HeaderText="Designation">
   
    <ItemTemplate>
        <asp:Label ID="lblDesig" runat="server" Text='<%# bind("DES_DESCR") %>'></asp:Label>
    </ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Sent Count">

    <ItemTemplate>
       
        <asp:Label ID="lblSENT_COUNT" runat="server" Text='<%# BIND("SENTCOUNT") %>' Visible="false"></asp:Label>
    <asp:LinkButton ID="lnksndCount" runat="server"  Text='<%#Bind("SENTCOUNT")%>' CausesValidation="False"
                                    OnClientClick="return false;"  ></asp:LinkButton>
         <ajaxToolkit:PopupControlExtender
                                        ID="PopupControlExtender1" runat="server" DynamicContextKey='<%# Eval("EMP_ID") & "$" & Eval("TMPL_ID")%>'
                                        DynamicControlID="Panel1" DynamicServiceMethod="GetEmlCountDetails" PopupControlID="Panel1"
                                        Position="Bottom" TargetControlID="lnksndCount">
                                    </ajaxToolkit:PopupControlExtender>

    </ItemTemplate>
    <HeaderStyle Width="80px" />
    <ItemStyle Width="80px" HorizontalAlign="CENTER" />
</asp:TemplateField>





<asp:TemplateField HeaderText="Update Status">

    <ItemTemplate>
     <asp:Image id="imgDoc2" runat="server" ImageUrl='<%# BIND("READ_IMAGE") %>' HorizontalAlign="Center"></asp:Image> 
          <asp:Label ID="Label1" runat="server" Text='<%# Bind("DISP_STATUS")%>'></asp:Label>
        
        <asp:Label ID="lblFLAG_STATUS1" runat="server" Text='<%# BIND("DISP_READ_DATE") %>'></asp:Label>
     
    </ItemTemplate>
    <HeaderStyle Width="80px" />
    <ItemStyle Width="80px" HorizontalAlign="CENTER" />
</asp:TemplateField>
                       
</columns>
                    <selectedrowstyle backcolor="Wheat" />
                    <headerstyle cssclass="gridheader_pop" height="25px" horizontalalign="Center" verticalalign="Middle" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView>
              
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 28px" valign="bottom">
        
                </td>
        </tr>
        <tr>
            <td class="matters" style="height: 28px" valign="middle" align="center">
        
                <asp:Button ID="btnAppr" runat="server" CssClass="button" Text="Resend To Checked" OnClientClick="return confirm_Reset();" Width="150px" /> 
                    <asp:Button ID="btnOverRide" runat="server" CssClass="button" Text="Resend To All (Not Participated)"  
                    OnClientClick="return confirm_OverRide();"
                    Width="250px" /> <asp:Button ID="btnRegReEnroll" runat="server" CssClass="button" Text="Register & Re-Enroll" 
                     OnClientClick="return confirm_Register();"
                    Width="150px"  Visible="False" /><asp:Button ID="btnTCNotEnroll_Rfd" runat="server" CssClass="button" Text="TC Not Re-Enrolling" OnClientClick="return confirm(1);"
                    Width="160px"  Visible="false"/> <asp:Button ID="btnStaffChild_Rfd" runat="server" CssClass="button" Text="Staff Child Re-Enrolled"  
                    OnClientClick="return confirm(2);" Visible="false"
                    Width="160px" /> <asp:Button ID="btnReenrolled_Rfd" runat="server" CssClass="button" Text="Re-Enrolled" 
                     OnClientClick="return confirm(3);" Visible="false"
                    Width="160px"   /><asp:Button ID="btnPending_Rfd" runat="server" CssClass="button" Text="Pending" 
                     OnClientClick="return confirm(4);" Visible="false" 
                    Width="160px"   />
                
                    <asp:Button ID="btnResendAll" runat="server" Text="Send to All" CssClass="button" OnClientClick="return confirm_sendAll();" Visible="false"/>
                    </td>
        </tr>
        <tr>
            <td class="matters" valign="bottom" style="height: 12px">
           
                &nbsp;</td>
        </tr>
      
    </table>
    <asp:Panel ID="panel1" runat="server" Width="300px" HorizontalAlign="Left" style="position:fixed;left:100px;top:0;margin-left:-210px;float:left;"  ></asp:Panel>
</asp:Content>

