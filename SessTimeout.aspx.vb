﻿Imports Telerik.Web.UI

Partial Class SessTimeout
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs)
        If Not IsPostBack Then
            'set the expire timeout for the session 
            Session.Timeout = 2
            'configure the notification to automatically show 1 min before session expiration
            RadNotification1.ShowInterval = (Session.Timeout - 1) * 60 * 1000
            'set the redirect url as a value for an easier and faster extraction in on the client
            RadNotification1.Value = Page.ResolveClientUrl("login.aspx")
        End If
    End Sub

    Protected Sub OnCallbackUpdate(sender As Object, e As RadNotificationEventArgs)

    End Sub
End Class
