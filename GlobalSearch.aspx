﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="GlobalSearch.aspx.vb" Inherits="ESSDashboard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <%--<script src="vendor/bootstrap/js/bootstrap.min.js"></script>--%>
    <%-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--%>
    <script src="/js/sb-admin.js"></script>

    <%--<script src="Scripts/jquery-1.12.4.min.js"></script>--%>
    <!------ Include the above in your HEAD tag ---------->

    <style>
        /*.tooltip {
            position: relative;
            float: right;   
        }

            .tooltip > .tooltip-inner {
                background-color: #eebf3f;
                padding: 5px 15px;
                color: rgb(23,44,66);
                font-weight: bold;
                font-size: 13px;
            }

        .popOver + .tooltip > .tooltip-arrow {
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 5px solid #eebf3f;
        }*/

        /*section {
           margin:140px auto; 
  height:1000px;
        }

        .progress {
            border-radius: 0;
            overflow: visible;
        }

        .progress-bar {
            background: rgb(23,44,60);
            -webkit-transition: width 1.5s ease-in-out;
            transition: width 1.5s ease-in-out;
        }*/

        .progress {
            height: 20px;
            margin-bottom: 5px;
            overflow: hidden;
            background-color: #f5f5f5;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
        }

        .progress-bar {
            float: left;
            width: 0;
            height: 100%;
            font-size: 12px;
            line-height: 20px;
            color: #fff;
            text-align: center;
            background-color: #428bca;
            -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
            box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
            -webkit-transition: width .6s ease;
            -o-transition: width .6s ease;
            transition: width .6s ease;
        }

        .progress-col1 {
            background-color: #ff7600;
        }

        .progress-col2 {
            background-color: rgb(151,215,0);
        }

        .progress-col3 {
            background-color: rgb(236,179,203);
        }

        .progress-col4 {
            background-color: rgb(218,24,132);
        }

        .progress-striped .progress-bar, .progress-bar-striped {
            background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            -webkit-background-size: 40px 40px;
            background-size: 40px 40px;
        }

        .progress.active .progress-bar, .progress-bar.active {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            -o-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite;
        }

        .progress-bar-success {
            background-color: #5cb85c;
        }

        .progress-striped .progress-bar-success {
            background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        }

        .box-shadow {
            box-shadow: 5px 4px 10px #ccc;
            border-bottom-right-radius: 10px;
            border-bottom-left-radius: 10px;
            height:100% !important;
        }
    </style>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({ trigger: 'manual' }).tooltip('hide');
        });

        $(document).ready(function () {
            // if($( window ).scrollTop() > 10){   scroll down abit and get the action   
            $(".progress-bar").each(function () {
                each_bar_width = $(this).attr('aria-valuenow');
                // alert(each_bar_width)
                $(this).width(each_bar_width + '%');
            });

            //  }  
        });

        function GetStudetails() {


            var dataToSend = { ID: '<%=ViewState("STUID")%>', MethodName: 'LoadStudentDetails' };

            var options =
            {
                url: '<%=ResolveUrl("~/StudentSearchProfile.aspx")%>?x=' + new Date().getTime(),
                data: dataToSend,
                dataType: 'JSON',
                type: 'POST',
                async: true,
                success: function (response) {
                    window.location.href = '<%=ResolveUrl("~/StudentSearchProfile.aspx")%>/';
                    //after success will redirect to new page
                }
            }
                $.ajax(options);


            }

            function ViewStudentProfDetails(pMode) {
                var NameandCode;
                var Id;
                /*pMode = "STUDETAILSVIEW"*/
                Id = document.getElementById("<%=hdnStudId.ClientID%>").value
                url = "/Students/StudentProfileView.aspx?mode=" + pMode + "&Id=" + Id;

                // var oWnd = radopen(url, "pop_stuProfile");
                Popup(url);

            }


            function OnClientClose(oWnd, args) {
                //get the transferred arguments


                var arg = args.get_argument();

                if (arg) {

                    NameandCode = arg.Employee.split('|');



                    //document.getElementById(DFUDESCR).value = NameandCode[1];
                    //document.getElementById(DFUDOMID).value = NameandCode[0];

                    //document.getElementById(DFUDESCR).disabled = true;
                    //   window.location.href = "http://localhost:1202/Oasis_Redirect.aspx?Type=" + NameandCode[1] + "&STU=" + NameandCode[0] + "&MNU=" + NameandCode[2] + "&SEC=" + NameandCode[3] + "&USR=" + NameandCode[4];
                    //  window.open('http://localhost:1202/Oasis_Redirect.aspx?Type=' + NameandCode[1] + '&STU=' + NameandCode[0] + '&MNU=' + NameandCode[2] + '&SEC=' + NameandCode[3] + '&USR=' + NameandCode[4] , '_blank');
                    //  window.open('http://localhost:1202/Oasis_Redirect.aspx?Type=1', '_blank');
                    //   window.open('https://support.wwf.org.uk/earth_hour/index.php?type=individual','_blank');
                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;

                var height = body.scrollHeight;
                var width = body.scrollWidth;

                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;

                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }
            function setCustomPosition(sender, args) {

                sender.moveTo(sender.get_left(), sender.get_top());

            }
    </script>
    <script language="javascript">

        function ShowRPTSETUP_S(id, status) {

            var sFeatures;
            sFeatures = "dialogWidth: 65%; ";
            sFeatures += "dialogHeight: 90%; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '/StudProfileGeneral.aspx?id=' + id + '&status=' + status + '';
            //alert(status) 
            //return ShowWindowWithClose(url, 'search', '65%', '90%')
            var win = window.open(url, '_blank');
            win.focus();
            return false;
            //result = window.showModalDialog(url, "", sFeatures);

            //if (result == '' || result == undefined) {
            //    return false;
            //}

        }


    </script>
    <telerik:RadWindowManager ID="radEmployee" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_stuProfile" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="900px" Height="700px" OnClientShow="setCustomPosition">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="container-fluid">
        <div class="row">
            <div class="card-header-profile col-md-2 text-center mb-3">
                <%--<img class="img-thumbnail border-radius mb-2" src="https://school.gemsoasis.com/OASISPHOTOSNEW/OASIS_HR/ApplicantPhoto//900501/empphoto/27932/EMPPHOTO.JPG" alt="Card image cap" >--%>
                <asp:Image ID="imgStuImage" runat="server" class="img-thumbnail border-radius mb-2 img-profile" ImageUrl="~/Images/Photos/no_image.gif" />

                <div class="nav flex-column nav-pills text-left" id="v_pills_tab" runat="server" role="tablist" aria-orientation="vertical">
                    <%--<a class="nav-link border-bottom active" id="v-pills-main-tab" data-toggle="pill" href="#v-pills-main" role="tab" aria-controls="v-pills-main" aria-selected="true">Main</a>
                    <a class="nav-link border-bottom" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false" >Fee Details</a>
                    <a class="nav-link border-bottom" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Curriculum</a>
                    <a class="nav-link border-bottom" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false" >Attendance</a>
                    --%>      <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('STUDENTPROFILE');return false;">Main</a>
                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('PARENTPROFILE');return false;">Parent</a>
                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('STUDENTATTENDANCE');return false;">Attendance</a>

                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('STUDENTFEES');return false;">Fees</a>
                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('CURRICULUM');return false;">Curriculum</a>
                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('BEHAVIOUR');return false;">Achievements</a>
                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('LIBRARY');return false;">Library</a>
                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('COMMUNICATION');return false;">Communication</a>
                </div>
                <%--FEES SIDE BAR--%>
                <div class="nav flex-column nav-pills text-left" id="v_pills_tab_Fees" runat="server" role="tablist" aria-orientation="vertical">
                  <a class="nav-link border-bottom" href="#Basic"  >Basic Details</a>
                    <a class="nav-link border-bottom" href="#Basic"  >Quick Links</a>
                    <a class="nav-link border-bottom" href="#FeeSummary"  >Fee Summary(Charts)</a>

                    <a class="nav-link border-bottom" href="#Ledger" >Ledger Summary</a>
                    <a class="nav-link border-bottom" href="#Ledger" >Student Ageing</a>
                    <a class="nav-link border-bottom" href="#Activity" >List Of Activities</a>
                    <a class="nav-link border-bottom" href="#feecollection" >Last 5 Fee Collections</a>
                    <a class="nav-link border-bottom" href="#feecollection"  >Last 5 Fee Concessions</a>
                    <a class="nav-link border-bottom" href="#feeReminder">Reminders Sent</a>
                    <a class="nav-link border-bottom" href="#feeReminder">Due Followup</a>
                    <a class="nav-link border-bottom" href="#feeProforma">Proforma Invoice</a>
                    <a class="nav-link border-bottom" href="#feeProforma">Tax Invoice</a>
                    <a class="nav-link border-bottom" href="#feeCR_DR">Credit/Debit Note</a>
                </div>
                   <%--TRANSPORT SIDE BAR--%>
                <div class="nav flex-column nav-pills text-left" id="v_pills_tab_Transport" runat="server" role="tablist" aria-orientation="vertical">
                  <a class="nav-link border-bottom" href="#TransBasic"  >Basic Details</a>
                    <a class="nav-link border-bottom" href="#TransBasic"  >Quick Links</a>
                    <a class="nav-link border-bottom" href="#ServiceInfo">Service Info</a>
                    <a class="nav-link border-bottom" href="#FeeSummary" >Fee Summary</a>
                    <a class="nav-link border-bottom" href="#ServiceHistory" >Transport Service History</a>
                    <a class="nav-link border-bottom" href="#ServiceAudit" >Transport Service Audit</a>
                </div>
                
               

                <div class="nav flex-column nav-pills text-left" id="vtab2" role="tablist" aria-orientation="vertical" runat="server" visible="false">
                    <asp:LinkButton ID="lnkAttendance" runat="server" Text="Attendance" CssClass="nav-link border-bottom"></asp:LinkButton>
                    <asp:LinkButton ID="lnkCurriculum" runat="server" Text="curriculum" CssClass="nav-link border-bottom"></asp:LinkButton>

                </div>
            </div>
            <div class="col-md-10" id="div_General" runat="server">
                <!-- Outer container for the tab content starts here -->
                <div class="tab-content" id="v-pills-tabContent">
                    <!-- First Tab content starts here -->
                    <div class="tab-pane fade show active" id="v-pills-main" role="tabpanel" aria-labelledby="v-pills-main-tab">

                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Basic Details</h6>
                                    <div class="card-body">
                                        <table class="table">
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Name</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="ltStudName" runat="server"></asp:Label></span></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Student ID</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="ltStudId" runat="server"></asp:Label></span></td>
                                            </tr>

                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Grade & Section</span></td>

                                                <td>
                                                    <asp:Label ID="ltGrd" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Current Status</span></td>

                                                <td>
                                                    <asp:Label ID="ltStatus" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Username</span></td>

                                                <td>
                                                    <asp:Label ID="ltStudUserName" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">House</span></td>
                                                <td>
                                                    <asp:Label ID="ltHouseStu" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="border-bottom" runat="server" id="rowLastAtt" visible="false">
                                                <td>
                                                    <span class="profile-label">Last Attendance Date</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="ltLastAttDate" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr runat="server" class="border-bottom" id="rowLeaveDate" visible="false">
                                                <td>
                                                    <span class="profile-label">Leave Date</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="ltLeaveDate" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <h5 class="card-title"></h5>
                                        <p class="card-text"></p>
                                        
                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('STUDENTPROFILE');return false;">View More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Parent Details / Contact Info</h6>
                                    <div class="card-body">
                                        <table class="table">
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Primary Contact</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblPrimarycontact" runat="server"></asp:Label></span></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Name</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblPrimaryname" runat="server"></asp:Label></span></td>
                                            </tr>

                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Mobile</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblPrimaryMobile" runat="server"></asp:Label></span></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Email</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblPrimaryEmail" runat="server"></asp:Label></span></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Username</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblParentUsername" runat="server"></asp:Label></span></td>
                                            </tr>
                                        </table>
                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('PARENTPROFILE');return false;">View More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--- Sibling-->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Sibling Details</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <asp:GridView ID="gvStudChange" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                                    PageSize="20" Width="100%" AllowSorting="True">
                                                    <RowStyle CssClass="griditem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStu_id" runat="server" Text='<%# Bind("Stu_id") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" />
                                                            <HeaderStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Stud. No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" />
                                                            <HeaderStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSNAME" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Grade &amp; Section">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrd_Sct" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                            </ItemTemplate>

                                                            <ItemStyle HorizontalAlign="center" />
                                                            <HeaderStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Current Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# bind("Stu_currstatus") %>'></asp:Label>
                                                            </ItemTemplate>

                                                            <ItemStyle HorizontalAlign="center" />
                                                            <HeaderStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="View">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lblView" runat="server" OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;STU_ID&quot;) & &quot;','&quot; & Container.DataItem(&quot;Stu_currstatus&quot;) & &quot;');return false;&quot; %>">View</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" />
                                                            <HeaderStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!--- fees-->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Fee Summary</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <telerik:RadHtmlChart runat="server" ID="radFeeTotalPaidChart" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="TMONTH" DataFieldY="Y">
                                                                <TooltipsAppearance DataFormatString="{0:n}" />
                                                                <LabelsAppearance Visible="true" />
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="X">
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance DataFormatString="{0:n}" />
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false" />
                                                    </Legend>
                                                    <ChartTitle Text="FEE PAID BY MONTH">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>
                                            <div class="col-sm-6">
                                                <telerik:RadHtmlChart runat="server" ID="radFeePiechart" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                                <LabelsAppearance DataFormatString="{0:n}">
                                                                </LabelsAppearance>
                                                                <TooltipsAppearance>
                                                                    <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                                </TooltipsAppearance>

                                                            </telerik:DonutSeries>

                                                        </Series>
                                                        <YAxis>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>

                                                        <Appearance Position="Right" Visible="true" />
                                                    </Legend>
                                                    <ChartTitle Text="OUTSTANDING AS ON TODAY">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>


                                        </div>
                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('STUDENTFEES');return false;">View More</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!--- attendance-->

                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Attendance Summary</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">

                                                <asp:Repeater ID="rptAttendance" runat="server">

                                                    <ItemTemplate>
                                                        <h3 class="card-title mb-0">Attendance for the year</h3>
                                                        <span class="stud-cont-gray"><%# Eval("acd_year")%></span>
                                                        <div class="row">

                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("tot_marked")%></span></div>
                                                            <div class="col-md-8 col-lg-8">




                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>Present</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("tot_att")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("tot_att")%>'><%# Eval("tot_att")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>

                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>Authorized Absent </b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("tot_leave")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("tot_leave")%>'><%# Eval("tot_leave")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>

                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>Unauthorized absent</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col4" role="progressbar" aria-valuenow='<%# Eval("tot_abs")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("tot_abs")%>'><%# Eval("tot_abs")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>


                                                            </div>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <%--   <asp:Literal ID="FCLiteral" runat="server" Visible="false"></asp:Literal>--%>
                                            </div>
                                            <div class="col-sm-6">

                                                <telerik:RadHtmlChart runat="server" ID="radAttendanceChart" Height="250">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="TMONTH" DataFieldY="TOT_ATT">
                                                                <TooltipsAppearance />
                                                                <LabelsAppearance Visible="true" />
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="TMONTH">
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance />
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false" />
                                                    </Legend>
                                                    <ChartTitle Text="ATTENDANCE BY MONTH">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('STUDENTATTENDANCE');return false;">View More</a>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!--Merit -->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Achievements - Behavioural Merits/De Merits</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <asp:Repeater ID="rptAchievements" runat="server">

                                                    <ItemTemplate>
                                                        <h3 class="card-title mb-0">Achievements</h3>
                                                        <span class="stud-cont-gray"><%# Eval("ACDYEAR")%></span>
                                                        <div class="row">

                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("STUD_TOTALPERC")%></span></div>
                                                            <div class="col-md-8 col-lg-8">




                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>School</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("TOTALSCHOOLRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSCHOOLRANK")%>'><%# Eval("TOTALSCHOOLRANK")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>

                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>Student</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("TOTALSTUDENTRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSTUDENTRANK")%>'><%# Eval("TOTALSTUDENTRANK")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>




                                                            </div>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <div class="col-sm-4">


                                                <asp:Repeater ID="rptBehaviouralMerits" runat="server">

                                                    <ItemTemplate>
                                                        <h3 class="card-title mb-0">Behavioural Merits</h3>
                                                        <span class="stud-cont-gray"><%# Eval("ACDYEAR")%></span>
                                                        <div class="row">

                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("STUD_TOTALPERC")%></span></div>
                                                            <div class="col-md-8 col-lg-8">




                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>School</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("TOTALSCHOOLRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSCHOOLRANK")%>'><%# Eval("TOTALSCHOOLRANK")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>

                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>Student</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("TOTALSTUDENTRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSTUDENTRANK")%>'><%# Eval("TOTALSTUDENTRANK")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>




                                                            </div>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:Repeater>



                                            </div>
                                            <div class="col-sm-4">

                                                <asp:Repeater ID="rptDemerit" runat="server">

                                                    <ItemTemplate>
                                                        <h3 class="card-title mb-0">Behavioural De Merits</h3>
                                                        <span class="stud-cont-gray"><%# Eval("ACDYEAR")%></span>
                                                        <div class="row">

                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("STUD_TOTALPERC")%></span></div>
                                                            <div class="col-md-8 col-lg-8">




                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>School</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("TOTALSCHOOLRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSCHOOLRANK")%>'><%# Eval("TOTALSCHOOLRANK")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>

                                                                <div class="barWrapper">
                                                                    <span class="progressText"><b>Student</b></span>
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("TOTALSTUDENTRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSTUDENTRANK")%>'><%# Eval("TOTALSTUDENTRANK")%></span>
                                                                        </div>
                                                                    </div>



                                                                </div>




                                                            </div>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('BEHAVIOUR');return false;">View More</a>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <!--- curriculum-->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Curriculum</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12 mb-5">

                                                <telerik:RadHtmlChart runat="server" ID="RadCurriculumPie" Height="250px">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="Highest" DataFieldY="HIGHEST">

                                                                <LabelsAppearance Visible="True">
                                                                </LabelsAppearance>
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="Lowest" DataFieldY="LOWEST">

                                                                <LabelsAppearance Visible="True">
                                                                </LabelsAppearance>
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="Average" DataFieldY="GRADE_AVG">

                                                                <LabelsAppearance Visible="True">
                                                                </LabelsAppearance>
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="Performance" DataFieldY="RST_MARK">

                                                                <LabelsAppearance Visible="True">
                                                                </LabelsAppearance>
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="SBG_SHORTCODE">
                                                            <%-- <Items>
                                                        <telerik:AxisItem LabelText="ARB" />
                                                        <telerik:AxisItem LabelText="ENG" />
                                                        <telerik:AxisItem LabelText="EVS" />
                                                        <telerik:AxisItem LabelText="FRE" />
                                                        <telerik:AxisItem LabelText="MAT" />
                                                        <telerik:AxisItem LabelText="MS" />
                                                        <telerik:AxisItem LabelText="USS" />
                                                       
                                                    </Items>--%>
                                                        </XAxis>
                                                    </PlotArea>
                                                    <ChartTitle Text="Student Performance">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5>CAT4</h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="">Verbal SAS</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblCatVerbalSAS" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Quantitative SAS</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblQuantitativeSAS" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">NonVerbal SAS</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblNonVerbalSAS" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Spatial SAS</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblSpatialSAS" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Overall SAS</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblOverallSAS" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-sm-2">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5>Asset</h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="">English Percentile</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblEngPercentile" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">English Stanine</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblEngStanine" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Percentile</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblMathsPercentile" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Stanine</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblMathsStenine" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Percentile</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblSciencePercentile" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Stanine</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblScienceStenine" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-sm-2">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5>AnnualExam</h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="">English Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblEngMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">English Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblEngGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblMathsMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblMathsGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblScienceMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblScienceGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-sm-2">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5>Term1</h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="">English Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm1EngMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">English Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm1EngGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm1MathsMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm1MathsGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm1ScienceMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm1ScienceGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="col-sm-2">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5>Term2</h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="">English Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm2EngMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">English Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm2EngGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm2MathsMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm2MathsGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm2ScienceMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm2ScienceGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="col-sm-2">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5> <asp:Label ID="lblTerm3" runat="server"></asp:Label></h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="">English Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm3EngMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">English Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm3EngGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm3MathsMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Maths Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm3MathsGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Mark</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm3ScienceMark" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="">Science Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTerm3ScienceGrade" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <%--    <div class="col-sm-2">
                                                        <h5>TERM2</h5>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <h5>TERM3</h5>
                                                    </div>--%>
                                        </div>
                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('CURRICULUM');return false;">View More</a>

                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- transport-->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Transport</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <table class="table">
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Pickup Point</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblPickupPoint" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Dropoff Point</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblDropoffPoint" runat="server"></asp:Label></span></td>
                                                    </tr>

                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Pickup Bus No</span></td>

                                                        <td>
                                                            <asp:Label ID="lblPickupBusNo" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Dropoff Bus No</span></td>

                                                        <td>
                                                            <asp:Label ID="lblDropoffBusNo" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-sm-6">

                                                <telerik:RadHtmlChart runat="server" ID="radTransportFeePie" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                                <LabelsAppearance DataFormatString="{0:n}">
                                                                </LabelsAppearance>
                                                                <TooltipsAppearance>
                                                                    <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                                </TooltipsAppearance>

                                                            </telerik:DonutSeries>

                                                        </Series>
                                                        <YAxis>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>

                                                        <Appearance Position="Right" Visible="true" />
                                                    </Legend>
                                                    <ChartTitle Text="OUTSTANDING AS ON TODAY">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- health-->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Health Records</h6>
                                    <div class="card-body">
                                        <asp:GridView ID="gvActivity" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Activity Enrolled"
                                            HeaderStyle-Height="30" PageSize="20" Width="100%" Visible="false">
                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Activity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActivity" runat="server" Text='<%# Bind("ALD_EVENT_NAME")%>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Activity Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActivityType" runat="server" Text='<%# Bind("Payment")%>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>

                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>


                                        <div class="row">
                                            <div class="col-sm-4">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5>Health</h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Allergies</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsAllergy" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Allergy Details</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblAllergyDetails" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Disabilities</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsDisability" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Disability Details</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblDisabilitydetails" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Special Medication</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsSpclMed" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Medication Notes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblMedNotes" runat="server"></asp:Label></span></td>
                                                    </tr>

                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">PEd Restriction Notes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblPedRestriction" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Any other health Info</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblAnyhealthInfo" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>

                                            </div>
                                            <div class="col-sm-4">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td>
                                                            <h5>Learning</h5>
                                                        </td>
                                                        <td></td>
                                                    </tr>


                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Learning support/Therapy</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblLIsTherapy" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">LearningTherapyNotes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblTherapyNotes" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">SpclEduNeeds</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsSpclEduNeeds" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">SpclEduNeedsNotes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblSpclEduNeeds" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">EngSupport</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsEngSupport" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Eng Notes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblEngNotes" runat="server"></asp:Label></span></td>
                                                    </tr>

                                                    <%--<tr class="border-bottom">
                                                        <td><span class="profile-label">Ever Repeated Failed</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsEverRepeated" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                              <tr class="border-bottom">
                                                        <td><span class="profile-label">Repeated Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblRepeatedGrade" runat="server"></asp:Label></span></td>
                                                    </tr>--%>

                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Behaviour concern</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblbehaviourconcern" runat="server"></asp:Label></span></td>
                                                    </tr>


                                                </table>

                                            </div>
                                            <div class="col-sm-4">
                                                <table class="table">
                                                    <tr class="title-bg">
                                                        <td colspan="2">
                                                            <h5>Behavioural & Activities</h5>
                                                        </td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Behaviour notes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblbehaviournotes" runat="server"></asp:Label></span></td>
                                                    </tr>

                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">AnySpecificEnrichemnts</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsAnyspecificEnrichment" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">SpecificEnrichemntsNotes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblEnrichmentNotes" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Musically proficient</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsMusicallyProf" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Musically proficientNotes</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblMusicNotes" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Extra CurriculurActivities</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblExtraCurriculurActivities" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                    <tr class="border-bottom">
                                                        <td><span class="profile-label">Interesting sports</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblInterestingSports" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('MEDICAL');return false;">View More</a>

                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                    <!-- First Tab content ends here -->
                    <!-- Second Tab content starts here -->
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">

                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Basic Details</h6>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Parent Details / Contact Info</h6>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Fee Summarys</h6>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Student Details</h6>
                                    <div class="card-body">
                                        <table class="table">
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Name</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblSName" runat="server"></asp:Label></span></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Student ID</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblSID" runat="server"></asp:Label></span></td>
                                            </tr>

                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Grade & Section</span></td>

                                                <td>
                                                    <asp:Label ID="lblSgrade" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Current Status</span></td>

                                                <td>
                                                    <asp:Label ID="lblScurrStatus" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Shift</span></td>

                                                <td>
                                                    <asp:Label ID="lblSShift" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Stream</span></td>

                                                <td>
                                                    <asp:Label ID="lblsStream" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">House</span></td>

                                                <td>
                                                    <asp:Label ID="lblShouse" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Parent Details</h6>
                                    <div class="card-body">
                                        <table class="table">
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Primary Contact</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblPprimary" runat="server"></asp:Label></span></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Parent Username</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblPusername" runat="server"></asp:Label></span></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Father Name</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblPFname" runat="server"></asp:Label></span></td>
                                            </tr>

                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Father Mobile</span></td>

                                                <td>
                                                    <asp:Label ID="lblFmobile" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Father Email</span></td>

                                                <td>
                                                    <asp:Label ID="lblFemail" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Mother Name</span></td>

                                                <td><span class="text-lg-left text-dark">
                                                    <asp:Label ID="lblMotherName" runat="server"></asp:Label></span></td>
                                            </tr>

                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Mother Mobile</span></td>

                                                <td>
                                                    <asp:Label ID="lblMMobile" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><span class="profile-label">Mother Email</span></td>

                                                <td>
                                                    <asp:Label ID="lblMEmail" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Second Tab content ends here -->
                    <!-- Third Tab content starts here- curriculum starts here -->
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                        <div>
                            <ajaxToolkit:ModalPopupExtender ID="mpe" OnOkScript="onPrvOk();" CancelControlID="ImageButton1" DynamicServicePath="" runat="server"
                                Enabled="True" TargetControlID="btnTarget" PopupControlID="plPrev" BackgroundCssClass="modalBackground" />
                            <asp:Button ID="btnTarget" runat="server" Text="Button" Style="display: none" />
                            <div id="plPrev" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px; width: 80%; vertical-align: top; margin-top: 0px;">
                                <div class="msg_header" style="width: 100%; margin-top: 0px; vertical-align: top">
                                    <div class="msg" style="text-align: right; margin-top: 0px; background-color: #ffffff; vertical-align: top;">

                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/close.png" />
                                    </div>
                                </div>

                                <asp:Panel ID="plPrev1" runat="server" BackColor="White" Height="530px" ScrollBars="vertical" Width="100%">
                                    <iframe id="ifSibDetail" runat="server" style="background-image: url(../Images/Misc/loading.gif); background-position: center center; background-attachment: fixed; background-repeat: no-repeat;" height="100%" scrolling="yes" marginwidth="0" frameborder="0" width="100%"></iframe>
                                </asp:Panel>
                            </div>
                        </div>


                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Reportcard Details</h6>
                                    <div class="card-body">
                                        <!--curriculum reportdetails--->
                                        <div width="100%" align="left">
                                            <table class="table table-striped table-condensed table-hover table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">
                                                <tr>
                                                    <td style="vertical-align: middle">Select Academic Year :
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlAcademicYear" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                    </td>
                                                    <td style="vertical-align: middle">
                                                        <asp:RadioButton runat="server" ID="rdView" Text="View" Checked="True" GroupName="g1"></asp:RadioButton>
                                                        <asp:RadioButton runat="server" ID="rdDownload" Text="Download" GroupName="g1"></asp:RadioButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <asp:DataList ID="dlReports" runat="server" RepeatColumns="1" CssClass="divcurrNoborder">
                                            <ItemStyle Wrap="false" />
                                            <ItemTemplate>
                                                <div class="divCurrBox">
                                                    <asp:Image ID="imgNew" ImageUrl="~/Images/FlashingNEW.gif"
                                                        Style="display: inline; float: left; width: 30px; height: 20px; vertical-align: top;" runat="server" />
                                                    <asp:Image ID="Image1" runat="server"
                                                        ImageUrl="~/Images/arrow.png" Style="display: inline; float: left; margin-top: 5px; margin-right: 4px" />
                                                    <asp:LinkButton ID="lnkReport" runat="server" Text='<%# Eval("RPF_DESCR") + " (Released on " + Eval("RPP_RELEASEDATE", "{0:dd/MMM/yyyy}") + ")"%>' OnClick="lnkReport_Click"></asp:LinkButton>
                                                    <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("RPF_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRsm" runat="server" Text='<%# Bind("RPF_RSM_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("RPF_DATE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblReport" runat="server" Text='<%# Bind("RPF") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblAcdId" runat="server" Text='<%# Bind("RSM_ACD_ID") %>' Visible="false"></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        <iframe runat="server" id="ifReport" style="display: none;"></iframe>
                                        <asp:HiddenField ID="hfType" runat="server" />
                                        <!---->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Third Tab content ends here -->
                    <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">

                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Attendance Details</h6>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Attendance Info</h6>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Attendance Graph</h6>
                                    <div class="card-body">
                                        <h6 class="card-title">Special title treatment</h6>
                                        <p class="card-text">
                                            <asp:Label ID="lblOneName" runat="server" Text="ggg"></asp:Label>
                                            <asp:Button ID="btnReg" runat="server" Text="Ghl" OnClick="btnReg_Click" />
                                        </p>
                                        <a href="#" class="btn btn-primary float-right">View More</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Outer container for the tab content ends here -->
            </div>

            <%-------------------------------DIV FOR FEES MODULE START-----------------------------------------------%>
            <div class="col-md-10" id="div_FEES" runat="server">
                <!-- Outer container for the tab content starts here -->
                <div class="tab-content" id="v-pills1-tabContent">
                    <!-- First Tab content starts here -->
                    <div class="tab-pane fade show active" id="v-pills-main" role="tabpanel" aria-labelledby="v-pills-main-tab">
                         <a name="Basic"></a>
 
                        <div class="row mb-3">
                            <asp:Repeater ID="rptBasic" runat="server">
                                <HeaderTemplate>
                                    <div class="col-sm-9">
                                        <div class="card box-shadow">
                                            <h6 class="card-header p-2 pl-3">Basic Details</h6>
                                            <div class="card-body">
                                                <table class="table">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="border-bottom">
                                        <td><span class="profile-label">
                                            <asp:Label ID="lbl_F_Basic_LABEL" runat="server" Text='<%# Bind("X") %>'></asp:Label></span></td>

                                        <td><span class="text-lg-left text-dark">
                                            <asp:Label ID="lbl_F_Basic_VALUE" runat="server" Text='<%# Bind("Y") %>'></asp:Label></span></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                    </div>
                                </div>
                            </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <!--- Quick Links-->
                            <asp:Repeater ID="rptQuick" runat="server" OnItemDataBound="rptQuick_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="col-sm-3">
                                        <div class="card box-shadow">
                                            <h6 class="card-header p-2 pl-3">Quick Links</h6>
                                            <div class="card-body">
                                                <table class="table">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="border-bottom">
                                        <td><span class="profile-label">
                                            <asp:LinkButton ID="lnk_F_QuickLink" runat="server" Text='<%# Bind("MNU_NAME") %>'></asp:LinkButton></span>
                                            <asp:HiddenField ID="hf_QuickLink" runat="server" Value='<%# Bind("MNU_URL") %>' />
                                        </td>

                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                    </div>
                                </div>
                            </div>
                                </FooterTemplate>
                            </asp:Repeater>
                          <a name="FeeSummary"></a>
                        </div>
                        <!--- fees -->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Fee Summary</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <telerik:RadHtmlChart runat="server" ID="chart_by_month" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="TMONTH" DataFieldY="Y">
                                                                <TooltipsAppearance DataFormatString="{0:n}" />
                                                                <LabelsAppearance Visible="true" />
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="X">
                                                            <LabelsAppearance DataFormatString="MMM-yyyy" />
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance DataFormatString="{0:n}" />
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false" />
                                                    </Legend>
                                                    <ChartTitle Text="FEE PAID BY MONTH">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>
                                            <div class="col-sm-6">
                                                <telerik:RadHtmlChart runat="server" ID="chart_aging" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                                <LabelsAppearance DataFormatString="{0:n}">
                                                                </LabelsAppearance>
                                                                <TooltipsAppearance>
                                                                    <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                                </TooltipsAppearance>

                                                            </telerik:DonutSeries>

                                                        </Series>
                                                        <YAxis>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>

                                                        <Appearance Position="Right" Visible="true" />
                                                    </Legend>
                                                    <ChartTitle Text="FEE AGING CHART">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a name="Ledger"></a>
                        </div>

                        <%--Fee Summary--%>

                        <div class="row mb-3">
                            <asp:Repeater ID="rptLedgerSummary" runat="server">
                                <HeaderTemplate>
                                    <div class="col-sm-4">
                                        <div class="card box-shadow">
                                            <h6 class="card-header p-2 pl-3">Ledger Summary</h6>
                                            <div class="card-body">
                                                <table class="table">
                                                    <tr>
                                                        <td>
                                                            <h5><strong>Fee Description</strong></h5>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <h5><strong>Amount(<asp:Label ID="lblAmount" runat="server" Text='<%#Session("BSU_CURRENCY")%>'></asp:Label>)</strong></h5>
                                                        </td>
                                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="border-bottom">
                                        <td><span>
                                            <asp:Label ID="lbl_F_LedgerSummary_LABEL" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label></span></td>

                                        <td style="text-align: right"><span class="text-lg-right text-dark">
                                            <asp:Label ID="lbl_F_LedgerSummary_Value" runat="server" Text='<%# Bind("NetAmount") %>'></asp:Label></span></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td colspan="2">
                                    <a onclick="ViewStudentProfDetails('STUDENTFEES');" class="btn btn-primary float-right">View Ledger</a>
                                            </td>
                                        </tr>
                                    </table>
                                    </div>
                                </div>
                            </div>
                                </FooterTemplate>
                            </asp:Repeater>

                            <!--- Student Aging Summary-->
                            <div class="col-sm-8">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Student Ageing Summary</h6>
                                    <div class="card-body">
                                        <table width="100%"  >
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_F_Student_aging" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                             <a name="Activity"></a>
                
                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">List Of Activities</h6>
                                    <div class="card-body">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_F_Activity" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <a name="feecollection"></a>
                        </div>

                        <%--Last 5 Fee Collections--%>

                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Last 5 Fee Collections</h6>
                                    <div class="card-body">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_Collection" AutoGenerateColumns="false" runat="server" Width="100%"
                                                        CssClass="table table-row table-bordered" OnRowDataBound="gv_Collection_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Receipt Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_receipt_date" runat="server" Text='<%# Bind("ReceiptDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="User">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_user" runat="server" Text='<%# Bind("FCL_EMP_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Narration">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_narration" runat="server" Text='<%# Bind("Narration") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_amount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Receipt No">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbl_f_receipt_no" runat="server" Text='<%# Bind("ReceiptNo") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="hidden" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_IsTaxable" runat="server" Text='<%# Bind("BUS_bFEE_TAXABLE") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_f_IsEnquiry" runat="server" Text='<%# Bind("IsEnquiry") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--- Last 10 Fee Concessions-->
                             <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Last 5 Fee Concessions</h6>
                                    <div class="card-body">
                                        <table width="100%">
                                            <tr>
                                                <td>


                                                    <asp:GridView ID="gv_Concession" AutoGenerateColumns="false" runat="server" Width="100%"
                                                        CssClass="table table-row table-bordered" OnRowDataBound="gv_Concession_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_date" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Document No">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbl_f_Doc_no" runat="server" Text='<%# Bind("DocumentNo") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Narration">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_narration" runat="server" Text='<%# Bind("Narration") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_amount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Receipt No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_Status" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="hidden" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_f_FCH_ID" runat="server" Text='<%# Bind("FCH_ID") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_f_FCH_DRCR" runat="server" Text='<%# Bind("FCH_DRCR")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <a name="feeReminder"></a>
                        </div>
                        <!--- Reminders Sent-->
                         <div class="row mb-3">
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Reminders Sent</h6>
                                    <div class="card-body">
                                        <table width="100%" >
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_F_Reminders" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!--- Due Followup-->
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Due Followup</h6>
                                    <div class="card-body">
                                        <table width="100%" class="table-responsive">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_F_Followups" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                              <a name="feeProforma"></a>
                        </div>


                         <!--- Proforma Invoice-->
                         <div class="row mb-3">
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Proforma Invoice</h6>
                                    <div class="card-body">
                                        <table width="100%" >
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_F_Proforma" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!--- Tax Invoice-->
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Tax Invoice</h6>
                                    <div class="card-body">
                                        <table width="100%" >
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_F_Tax_invoice" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            
                 <a name="feeCR_DR"></a>
                        </div>


                         <!--- Credit/Debit Note-->
                         <div class="row mb-3">
                            <div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Credit/Debit Note</h6>
                                    <div class="card-body">
                                        <table width="100%" >
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_F_CR_DR" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!--- Tax Invoice-->
                            <%--<div class="col-sm-6">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">tax invoice</h6>
                                    <div class="card-body">
                                        <table width="100%" class="table-responsive">
                                            <tr>
                                                <td>
                                                    <asp:gridview id="gridview4" width="100%" cssclass="table table-row table-bordered" runat="server" autogeneratecolumns="true"></asp:gridview>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>--%>
                        </div>


                    </div>
                    <!-- First Tab content ends here -->
                </div>
                <!-- Outer container for the tab content ends here -->
            </div>
            <%-------------------------------DIV FOR FEES MODULE END-----------------------------------------------%>


            <%-------------------------------DIV FOR TRANSPORT MODULE START-----------------------------------------------%>
            <div class="col-md-10" id="div_Transport" runat="server">
                <!-- Outer container for the tab content starts here -->
                <div class="tab-content" id="v-pills2-tabContent">
                    <!-- First Tab content starts here -->
                    <div class="tab-pane fade show active" id="v-pills-main" role="tabpanel" aria-labelledby="v-pills-main-tab">
                         <a name="TransBasic"></a>
                         <!--- Basic Info-->
                        <div class="row mb-3">
                            <asp:Repeater ID="rptTransBasic" runat="server">
                                <HeaderTemplate>
                                    <div class="col-sm-4">
                                        <div class="card box-shadow">
                                            <h6 class="card-header p-2 pl-3">Basic Details</h6>
                                            <div class="card-body">
                                                <table class="table">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="border-bottom">
                                        <td><span class="profile-label">
                                            <asp:Label ID="lbl_Trans_Basic_LABEL" runat="server" Text='<%# Bind("X") %>'></asp:Label></span></td>

                                        <td><span class="text-lg-left text-dark">
                                            <asp:Label ID="lbl_Trans_Basic_VALUE" runat="server" Text='<%# Bind("Y") %>'></asp:Label></span></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                    </div>
                                </div>
                            </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="rptTransService" runat="server">
                                <HeaderTemplate>
                                    <div class="col-sm-5">
                                        <div class="card box-shadow">
                                            <h6 class="card-header p-2 pl-3">Service Info</h6>
                                            <div class="card-body">
                                                <table class="table">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="border-bottom">
                                        <td><span class="profile-label">
                                            <asp:Label ID="lbl_Trans_Service_LABEL" runat="server" Text='<%# Bind("X") %>'></asp:Label></span></td>

                                        <td><span class="text-lg-left text-dark">
                                            <asp:Label ID="lbl_Trans_Service_VALUE" runat="server" Text='<%# Bind("Y") %>'></asp:Label></span></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                    </div>
                                </div>
                            </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <!--- Quick Links-->
                            <asp:Repeater ID="rptTransQuick" runat="server" OnItemDataBound="rptTransQuick_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="col-sm-3">
                                        <div class="card box-shadow">
                                            <h6 class="card-header p-2 pl-3">Quick Links</h6>
                                            <div class="card-body">
                                                <table class="table">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="border-bottom">
                                        <td><span class="profile-label">
                                            <asp:LinkButton ID="lnk_Trans_QuickLink" runat="server" Text='<%# Bind("MNU_NAME") %>'></asp:LinkButton></span>
                                            <asp:HiddenField ID="hf_Trans_QuickLink" runat="server" Value='<%# Bind("MNU_URL") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                    </div>
                                </div>
                            </div>
                                </FooterTemplate>
                            </asp:Repeater>
                          <a name="ServiceInfo"></a>
                        </div>
                        <%--Service Info--%>

                        
                        <!--- FeeSummary -->
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Fee Summary</h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <telerik:RadHtmlChart runat="server" ID="Chart_by_month_Transport" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="TMONTH" DataFieldY="Y">
                                                                <TooltipsAppearance DataFormatString="{0:n}" />
                                                                <LabelsAppearance Visible="true" />
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="X">
                                                            <LabelsAppearance DataFormatString="MMM-yyyy" />
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance DataFormatString="{0:n}" />
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false" />
                                                    </Legend>
                                                    <ChartTitle Text="FEE PAID BY MONTH">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>
                                            <div class="col-sm-6">
                                                <telerik:RadHtmlChart runat="server" ID="Chart_fee_summary_Transport" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                                <LabelsAppearance DataFormatString="{0:n}">
                                                                </LabelsAppearance>
                                                                <TooltipsAppearance>
                                                                    <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                                </TooltipsAppearance>
                                                            </telerik:DonutSeries>

                                                        </Series>
                                                        <YAxis>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>

                                                        <Appearance Position="Right" Visible="true" />
                                                    </Legend>
                                                    <ChartTitle Text="FEE SUMMARY CHART">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a name="ServiceHistory"></a>
                        </div>

                         <%--Transport Service History--%>
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Transport Service History</h6>
                                    <div class="card-body">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_Trans_Service_History" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <a name="ServiceAudit"></a>
                        </div>

                             <%--Transport Service Audit--%>
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="card box-shadow">
                                    <h6 class="card-header p-2 pl-3">Transport Service Audit</h6>
                                    <div class="card-body">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gv_Trans_Service_Audit" Width="100%" CssClass="table table-row table-bordered" runat="server" AutoGenerateColumns="true"></asp:GridView>

                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <a name="feecollection"></a>
                        </div>
                   


                    </div>
                    <!-- First Tab content ends here -->
                </div>
                <!-- Outer container for the tab content ends here -->
            </div>
            <%-------------------------------DIV FOR TRANSPORT MODULE END-----------------------------------------------%>





        </div>
    </div>

    <asp:HiddenField ID="hdnStudId" runat="server" />

</asp:Content>

