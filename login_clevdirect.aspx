<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login_clevdirect.aspx.vb" Inherits="login_clevdirect"  Title="::::GEMS OASIS:::: Online Student Administration System::::"%>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    
    <title>Login Page</title>
     
      <link rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon"> 
      <link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin-top:-1px">
    <script Language="javascript" type="text/javascript">
        self.moveTo(0, 0); self.resizeTo(screen.availWidth, screen.availHeight);

        function CloseWindow() {
            window.open('', '_self', '');
            window.close();
        }
        function show() {
            if (document.getElementById('<%=txtUsername.ClientID %>').value != '' && document.getElementById('<%=txtPassword.ClientID %>').value != '')
            setTimeout('showGif()', 500);
    }
    function showGif() {
        setTimeout('showGif()', 500);
        document.getElementById('yourImage').style.display = 'block';
    }
    function CheckOnPostback() {
        if (document.getElementById('<%=hfExpired.ClientID %>').value == 'expired')
            ChangePassword();
    }

    function ChangePassword() {
        var sFeatures;
        sFeatures = "dialogWidth: 450px; ";
        sFeatures += "dialogHeight: 250px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        result = window.showModalDialog("ChangePassword.aspx", "", sFeatures)
    }

     </script>
    <form id="form1" defaultfocus="imgBtnLogin" runat="server"> 
        <table style="border-collapse:collapse;" align="center" >
            <tr>
                <td style="width: 763px">
                    <img src="images/loginImage/head.jpg" /></td>
            </tr>
            <tr>
                <td style="width: 763px; height: 23px;">
                </td>
            </tr>
            <tr>
                <td style="width: 763px; background-image: url(images/loginImage/background1.jpg); background-repeat: repeat; height: 20%;">
                    <table style="border-collapse:collapse;width: 100%; height: 157px" cellpadding="0" cellspacing="0">
                        <tr> 
                            <td colspan="2" style="background-color: #06c; color: white; font-weight: bold; font-size: 16px; font-family: Arial,Helvetica, sans-serif; background-image: url(images/loginImage/cellone_top.gif); background-repeat: no-repeat; height: 46px;" align="center" valign="middle">
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;Login to GEMS OASIS</td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td colspan="2" style="background-image: url(Images/loginImage/background-MAIN.jpg); background-repeat: no-repeat; height: 204px; background-position: center center;" align="center">
                                           <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="Server">
                                            </ajaxToolkit:ToolkitScriptManager>
                                <table cellpadding="4" cellspacing="2" align="center" border="0">
                                    <tr>
                                        <td align="left" colspan="2" valign="top">
                                            <asp:Label ID="lblResult" runat="server" EnableViewState="False" CssClass="error" ForeColor="Red" /></td>
                                    </tr>
                                    <tr>
                                        <td class="matters" style="width: 75px; text-align: left">
                                            Username: 
                                        <td style="width: 100px;">
                                            <asp:TextBox ID="txtUsername" runat="server" Height="18px" Width="150px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="matters"  style="width: 75px; text-align: left">
                                            Password: </td>
                                        <td style="width: 100px;">
                                            <asp:TextBox ID="txtPassword" runat="server" Height="18px" TextMode="Password" Width="150px" EnableViewState="False"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 75px">
                                            &nbsp; &nbsp; &nbsp;&nbsp;
                                        </td>
                                        <td align="center" >
                                            <asp:ImageButton ID="imgBtnLogin" runat="server" BorderColor="Purple" BorderStyle="Solid"
                                                BorderWidth="2px" ImageUrl="~/images/loginImage/login.jpg" Style="background-repeat: no-repeat; text-align: right;" OnClientClick="show();" />
                                            <asp:ImageButton ID="imgBtnExit" runat="server" BorderColor="Purple" BorderStyle="Solid"
                                                BorderWidth="2px" ImageUrl="~/Images/loginImage/exit.jpg" OnClientClick="CloseWindow();"
                                                Style="background-repeat: no-repeat; text-align: right" />&nbsp;</td>
                                    </tr>
                                   <tr id="yourImage" valign="top"> 
                                    <td class="matters" colspan="3" valign="top" align="center">
                                     <img id="img" src="Images/loading1.gif" />&nbsp;<br />
                                        Please Wait....</td> 
                                     </tr>
                                </table>
                                <asp:RequiredFieldValidator ID="NReq" runat="server" ControlToValidate="txtUsername"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />Username is required." ></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="PNReq" runat="server" ControlToValidate="txtPassword"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />Password is required."></asp:RequiredFieldValidator>&nbsp;
                                                <ajaxToolkit:ValidatorCalloutExtender ID="PNReqE" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="PNReq" Width="230px">
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="NReqE" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="NReq" Width="230px">
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:HiddenField ID="hfExpired" runat="server" />
                                &nbsp;
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td style="width: 763px; background-image: url(images/loginImage/background1.jpg); background-repeat: repeat; height: 126px; text-align: center;">
                    <img src="images/loginImage/allschools4.jpg" width="757" align="middle"/>
                    </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td align="center"  style="width: 763px; height: 15px; background-image: url(images/loginImage/background1.jpg); background-repeat: repeat;">
                    <span style="LETTER-SPACING: 3px; font-family: Arial Rounded MT Bold; font-size: 14px; color: #0066cc;">GEMS - 
            IT</span></td>
            </tr>
            <tr>
                <td style="background-image: url(images/loginImage/footerSep.gif); width: 763px; height: 10px">
                </td>
            </tr>
        </table>
        <script> document.getElementById('yourImage').style.display ='none';</script> 
    </form>
</body>
</html>
