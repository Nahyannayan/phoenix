﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports Telerik
''modified by jacob on 27th jan for adding new session variables for tax
''modified by nahyan on 1oct2019- to add pulse url
''modifed by nahyan on 1oct2019 to rediect user if there is no menu access
Partial Class mainMasterPage
    Inherits System.Web.UI.MasterPage
    Dim userSuper As Boolean
    Dim Encr_decrData As New Encryption64
    Dim SessionFlag As Integer

    Public Property STU_BSU_ID() As String
        Get
            Return h_STU_BSU_ID.Value
        End Get
        Set(ByVal value As String)
            h_STU_BSU_ID.Value = value
        End Set
    End Property
    Public Property usr() As String
        Get
            Return h_user.Value
        End Get
        Set(ByVal value As String)
            h_user.Value = value
        End Set
    End Property
    Public Property modulename() As String
        Get
            Return h_module.Value
        End Get
        Set(ByVal value As String)
            h_module.Value = value
        End Set
    End Property

    Public ReadOnly Property MenuName() As String
        Get
            Return Session("temp")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        If Not Request.QueryString("MainMnu_code") Is Nothing Then
            If UtilityObj.CheckUSerAccessRights(Session("sBsuid"), Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")), _
                   Session("sModule"), Session("sUsr_name"), Session("sBusper")) <> 0 Then
                'Response.Redirect("~/BusinessUnit.aspx")

                Response.Redirect("~/Homepage.aspx")
                Exit Sub
            End If
        End If
        ' CheckAlert()
        Page.Title = OASISConstants.Gemstitle
        If Request.QueryString("MainMnu_code") <> "" Then
            Session("lastMnuCode") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        End If
        ' CheckForSQLInjection(Me.Page)
        ' Session("ReportSel") = "PDF"
        If Page.IsPostBack = False Then
            Try
                lblLogtime.Text = "Last Logged in Date & Time : " + Session("lastlogtime")
                If Session("Modulename") <> "" Then
                    lblModule.Text = "Logged in Module : " + Session("Modulename")
                End If
                If Session("ReportSel") = "" Or Session("ReportSel") Is Nothing Then
                    'Session("ReportSel") = "PDF"
                    Session("ReportSel") = "NT"
                End If

                Dim mnuCode As String = Request.QueryString("MainMnu_code")
                If mnuCode Is Nothing Then
                    Session.Remove("dsGradeSubjects")
                ElseIf Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")) <> "C970020" Then
                    Session.Remove("dsGradeSubjects")
                End If
                STU_BSU_ID = Encr_decrData.Encrypt(Session("sBsuid"))
                usr = Encr_decrData.Encrypt(Session("sUsr_name"))
                modulename = Encr_decrData.Encrypt(Session("sModule"))
            Catch ex As Exception
            End Try
            ' lblSwitch.Visible = False
            ' lblModules.Visible = False
            If Session("sUsr_name") & "" <> "" Then
                '   lbFinancial.Text = "Financial Year : " + Session("F_Descr")
                lblFilePath.Text = Session("Menu_text")
                If Session("Menu_text") <> "" And Session("Menu_text") <> String.Empty Then
                    Dim menu_arr() As String = Session("Menu_text").Split("|")
                    Session("temp") = menu_arr(menu_arr.Length - 1)
                End If
                Select Case Session("sModule")
                    Case "A0"
                        '  hlHome.Text = "Accounts"
                    Case "P0"
                        '   hlHome.Text = "Payroll"
                    Case Else
                        '   hlHome.Text = "Home"
                End Select
                set_bsu(Session("sBsuid"))
                check_ZAP(Session("sUsr_name"))
                check_PhoenixSMS(Session("sUsr_name"))
                hdnPageInfo.Value = Session("menu_descr")
                ' Session("Current_ACY_ID") = "28"
                If Session("sModule") = "S0" Or Session("sModule") = "C0" Or Session("sModule") = "E0" Then
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim str_query As String = "EXEC checkAcademicYear_Descr '" + Session("Current_ACD_ID") + "', '" + Session("sBsuid") + "' , '" + Session("CLM") + "'"
                    Session("AcadYear_Descr") = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                    '
                    ' lblModules.Visible = True
                    If Session("tot_bsu_count") = "1" Then
                        ' lblSwitch.Visible = True
                    End If
                    If Session("sModule") = "E0" Then
                        '  hlHome.NavigateUrl = "~\EduShield\eduShield_Home.aspx"
                    End If
                    '  lbFinancial.Text = "Academic Year : " + Session("AcadYear_Descr")
                End If
                Try

                    Dim ds As New DataSet
                    Dim moduleDecr As New Encryption64
                    Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim bunit As String = Session("sBsuid")
                    Dim userRol As String = Session("sroleid")
                    Dim userMod As String = Session("sModule")
                    Dim user As String = Session("sUsr_name")

                    gridbind(userMod, userRol, bunit, user)

                    Dim bunitName As String = Session("BSU_Name")

                    spBsuName.InnerText = bunitName
                    ' userMod = "S0"
                    Dim sqlString As String = ""
                    Dim username As String = Session("sUsr_Display_Name") 'Session("sUsr_name")
                    '  lblUserLog.Text = username.ToUpper
                    userSuper = Session("sBusper")
                    Using Imgreader As SqlDataReader = AccessRoleUser.GetBUnitImage(bunit)
                        While Imgreader.Read
                            '  imgSchool.ImageUrl = Imgreader.GetString(0)
                        End While
                    End Using
                    ' userRol = 54
                    ' userSuper = False

                    If Session("modPage") = "1" Then
                        divMenu.Style.Add("display", "block")
                    Else
                        divMenu.Style.Add("display", "none")
                    End If


                    Using conn As SqlConnection = New SqlConnection(connStr)
                        If userSuper = False Then
                            sqlString = "select MNU_CODE, MNU_TEXT," & _
                                          " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
                                          " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                                           " where (mnu_parentid is not null) and mnu_code in(select mnr_mnu_id from menurights_s where  mnu_text not in ('Home','Help','Log Off') and mnu_module='" & userMod & "' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0) and mnu_module='" & userMod & "' order by MNU_PARENTID,MNU_ID "
                        Else
                            sqlString = "select MNU_CODE,  MNU_TEXT," & _
                                          " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
                                          " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                                           " where mnu_text not in ('Home','Help','Log Off') and (mnu_parentid is not null) and mnu_module='" & userMod & "' order by MNU_PARENTID,MNU_ID "
                        End If
                        Dim da As SqlDataAdapter = New SqlDataAdapter(sqlString, conn)
                        da.Fill(ds)
                        da.Dispose()
                        SqlConnection.ClearPool(conn)
                    End Using
                    ds.DataSetName = "MENURIGHTS"
                    ds.Tables(0).TableName = "MENURIGHT"
                    Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("MENURIGHT").Columns("MNU_CODE"), ds.Tables("MENURIGHT").Columns("MNU_PARENTID"), True)
                    relation.Nested = True
                    ds.Relations.Add(relation)
                    If ds.Tables(0).Rows.Count > 0 Then

                        xmlDataSource.Data = ds.GetXml()
                        xmlDataSource.DataBind()
                    Else

                        ' Response.Redirect("~/Modulelogin.aspx")
                    End If

                    BIND_BSU(bunit, user)
                    ''added by nahyan on 4nov2018 
                    bind_Financial_year()

                    'If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then

                    '    ddlFinYear.SelectedValue = Request.Cookies("OASIS_SETTINGS")("F_YEAR")
                    'End If
                    'ddlFinYear_SelectedIndexChanged(ddlFinYear, Nothing)
                    'Dim con As SqlConnection = New SqlConnection(connection)
                    'con.Open()

                    'Dim sqltran As SqlTransaction
                    'sqltran = con.BeginTransaction("trans")
                    'Try

                    '    Dim param(5) As SqlParameter

                    '    param(0) = New SqlParameter("@BSU_ID", bunit)
                    '    param(1) = New SqlParameter("@USR_ROL_ID", userRol)
                    '    param(2) = New SqlParameter("@MODULE_ID", userMod)

                    '    Dim dsData As DataSet = SqlHelper.ExecuteDataset(sqltran, CommandType.StoredProcedure, "SP_GET_MENU_ACCESS_FOR_USER_V2", param)

                    '    sqltran.Commit()

                    '    If dsData.Tables(0).Rows.Count >= 1 Then
                    '        '  divMenu.InnerHtml = dsData.Tables(0).Rows(0).Item("MNU_HTML")
                    '    End If
                    '    BIND_BSU(bunit, user)


                    'Catch ex As Exception

                    '    sqltran.Rollback()

                    'Finally
                    '    If con.State = ConnectionState.Open Then
                    '        con.Close()
                    '    End If
                    'End Try

                    ''to display marquee for offline
                    Dim strMArquee As String = String.Empty
                    strMArquee = GetMArqueeText()
                    If strMArquee.ToString <> "" Then
                        lblMArquee.Visible = True
                        lblMArquee.Text = strMArquee
                    Else
                        lblMArquee.Visible = True
                    End If
                Catch ex As Exception
                    '    lblMainError.Text = "Your page could not processed"
                End Try
            End If
        End If
        ' ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Hiding_the_error_class", "HideError()", True)
    End Sub

    Private Sub BIND_BSU(ByVal bsu As String, ByVal user As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@BSU", bsu)
        param(1) = New SqlParameter("@USR_NAME", user)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "Get_Businessunit", param)
            rptHeader.DataSource = datareader
            rptHeader.DataBind()
        End Using
    End Sub
    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadMenuEventArgs)
        Try
            Dim MainMnu_code As String = e.Item.Value
            Dim strRedirect As String = "#"
            Dim menu_rights As String = ""
            Dim menu_descr As String = ""
            Dim MainMmnr_bsu_id As String = Session("sBsuid")
            Dim MainMmnr_rol_id As String = Session("sroleid")
            Dim usr_id As String = Session("sUsr_id")
            Dim url As String = ""
            userSuper = Session("sBusper")
            If usr_id = "" Or MainMmnr_rol_id = "" Or MainMmnr_bsu_id = "" Then
                Response.Redirect("~/login.aspx")
            Else
                Using readerUserDetail As SqlDataReader = AccessRoleUser.GetRedirectPage(MainMmnr_bsu_id, MainMnu_code, MainMmnr_rol_id, userSuper)
                    If userSuper = False Then
                        While readerUserDetail.Read()
                            menu_rights = Convert.ToString(readerUserDetail(0))
                            menu_descr = Convert.ToString(readerUserDetail(3))
                            strRedirect = Convert.ToString(readerUserDetail(2))
                        End While
                    Else
                        While readerUserDetail.Read()
                            ' Menu_text = Convert.ToString(readerUserDetail(0))
                            strRedirect = Convert.ToString(readerUserDetail(1))
                            menu_descr = Convert.ToString(readerUserDetail(2))
                        End While
                        menu_rights = "6"
                    End If
                End Using
                Dim datamode As String = "none"
                Dim encrData As New Encryption64
                Session("Menu_text") = UtilityObj.GetFilepath(e.Item.Value.Split("/"), e.Item.Level)
                Session("MNU_CODE") = MainMnu_code
                Session("menu_descr") = menu_descr
                MainMnu_code = encrData.Encrypt(MainMnu_code)
                datamode = encrData.Encrypt(datamode)
                url = String.Format("{0}?MainMnu_code={1}&datamode={2}", strRedirect, MainMnu_code, datamode)
                If Trim(strRedirect) = "#" Then
                Else
                    Response.Redirect(url)
                End If
            End If
        Catch ex As Exception
            ' lblMainError.Text = "File can not be located"
        End Try
    End Sub

    Public Sub DisableScriptManager()
        ScriptManager1.EnablePartialRendering = False
    End Sub
    Public Sub DissableAsynPostback()
        ScriptManager1.AllowCustomErrorsRedirect = "false"
        ScriptManager1.AsyncPostBackErrorMessage = ""
    End Sub
    Private Function CheckForSQLInjection(ByVal myParentObj As Object) As Boolean
        Try
            Dim mObj As Object
            For Each mObj In myParentObj.Controls
                Try
                    If mObj.HasControls Then
                        CheckForSQLInjection(mObj)
                    End If
                Catch ex As Exception

                End Try
                If TypeOf (mObj) Is TextBox Then
                    If TypeOf (mObj.Parent) Is GridView Or TypeOf (mObj.Parent) Is DataControlFieldHeaderCell Then
                        mObj.Text = mainclass.cleanString(mObj.Text)
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
    End Function

    Public Function IsSessionMatchesForSave() As Boolean
        'lblMainError.Text = "Sessions not matching for Save...."
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim SqlCmd As New SqlCommand("[FEES].[CHECK_SESSIONMATCHES]", objConn)
        Try
            SqlCmd.Parameters.AddWithValue("@SESSIONID", Session.SessionID)
            SqlCmd.Parameters.AddWithValue("@USR_ID", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.ExecuteNonQuery()
            Dim lintRetVal As Integer = CInt(SqlCmd.Parameters("@ReturnValue").Value)
            If (lintRetVal = 0) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        Finally
            objConn.Close() 'Finally, close the connection
        End Try
        Return False
    End Function
    Public Function StringVerify(ByVal pVal As String) As Boolean
        Dim lstrLen As String
        Dim lintCounter As Integer
        lstrLen = pVal.Length
        For lintCounter = 1 To lstrLen
            Select Case Asc(Mid(pVal, lintCounter, 1))
                Case 65 To 90 'A-Z 
                    ' --- Ok
                Case 97 To 122 'a-z 
                    ' --- OK
                Case 48 To 57 ' 0123456789
                    '   --- OK
                Case Else
                    Return False
            End Select
        Next
        Return True
    End Function

    Public Function StringWithSpace(ByVal pVal As String) As Boolean
        Dim lstrLen As String
        Dim lintCounter As Integer
        lstrLen = pVal.Length
        For lintCounter = 1 To lstrLen
            If (Trim(Mid(pVal, lintCounter, 1)) <> "") Then
                Select Case Asc(Mid(pVal, lintCounter, 1))
                    Case 65 To 90 'A-Z 
                        ' --- Ok
                    Case 97 To 122 'a-z 
                        ' --- OK
                    Case 48 To 57 ' 0123456789
                        '   --- OK
                    Case Else
                        Return False
                End Select
            End If
        Next
        Return True
    End Function

    Public Function NumberVerify(ByVal pVal As String) As Boolean
        Dim lstrLen As String
        Dim lintCounter As Integer
        lstrLen = pVal.Length
        For lintCounter = 1 To lstrLen
            Select Case Asc(Mid(pVal, lintCounter, 1))
                Case 48 To 57 ' 0123456789
                    '   --- OK
                Case Else
                    Return False
            End Select
        Next
        Return True
    End Function
    Public Function GetNextDocNo(ByVal pDocType As String, ByVal pMonth As String, ByVal pYear As String) As String
        Return AccountFunctions.GetNextDocId(pDocType, Session("sBSUId"), pMonth, pYear)
    End Function

    Public Function GetDescr(ByVal pTableName As String, ByVal pFieldName As String, _
    ByVal pWhere As String, ByVal pVal As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim str_Sql As String = "SELECT  [" & pFieldName & "] From " & pTableName & " WHERE " & pWhere & "='" & pVal & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0) & ""
        Else
            Return "--"
        End If
    End Function

    Public Function CheckPosted(ByVal pDocType As String, ByVal pDocNo As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim str_Sql As String = "select GUID From Journal_H WHERE JHD_BSU_ID='" & Session("sBsuId") & "' AND JHD_DOCTYPE='" & pDocType & "' AND JHD_DOCNO='" & pDocNo & "' and JHD_bdeleted=0 "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Protected Sub rptHeader_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptHeader.ItemDataBound
        ' Dim ddlBusinessunit As New DropDownList
        Dim ds As DataSet
        Dim divSearch As New HtmlGenericControl
        Dim alertsDropdown As New HtmlAnchor
        Dim rpNotification As New Repeater
        If e.Item.DataItem Is Nothing Then
            Return



        Else

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                divSearch = e.Item.FindControl("divSearch")
                SetStudentProfileAccess()
                If Session("modPage") = "1" Then
                    If Session("ShowProfile") = "1" Then
                        divSearch.Style.Add("display", "flex")
                    Else
                        divSearch.Style.Add("display", "none")
                    End If

                Else
                    divSearch.Style.Add("display", "none")
                End If
            End If


            If Session("modPage") = "1" Then
                ds = bind_Notification(1)
            ElseIf Session("sModule") = "SS" Then
                ds = bind_Notification(1)
            Else
                ds = bind_Notification(0)
            End If
            rpNotification = DirectCast(e.Item.FindControl("rpNotification"), Repeater)
            alertsDropdown = e.Item.FindControl("alertsDropdown")
            If ds.Tables(0).Rows.Count > 0 Then
                alertsDropdown.Attributes.Add("data-badge", ds.Tables(0).Rows(0).Item("Total"))
                alertsDropdown.Attributes.Add("data-badge", ds.Tables(0).Rows(0).Item("Total"))
                rpNotification.DataSource = ds.Tables(1)
                rpNotification.DataBind()
            Else
                alertsDropdown.Attributes.Remove("data-badge")
            End If

            'ddlBusinessunit = DirectCast(e.Item.FindControl("ddlBusinessunit"), DropDownList)
            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            'CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
            '& " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")


            'If ds.Tables(0).Rows.Count > 0 Then

            '    ddlBusinessunit.DataTextField = "BSU_NAME"
            '    ddlBusinessunit.DataValueField = "BSU_ID"
            '    ddlBusinessunit.DataSource = ds.Tables(0)
            '    ddlBusinessunit.DataBind()

            'End If




        End If
    End Sub
    Function bind_Notification(ByVal type As String) As DataSet
        Dim ds As New DataSet

        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString


        Dim sqlString As String = ""
        Dim PARAM(3) As SqlParameter

        PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
        PARAM(1) = New SqlParameter("@BSUID", Session("sBsuid"))
        If type = 0 Then
            PARAM(2) = New SqlParameter("@MODULE", "")
        Else
            PARAM(2) = New SqlParameter("@MODULE", Session("sModule"))
        End If

        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "GET_DOC_PENDING_APPROVALS_LIST", PARAM)

        Return ds
    End Function
    Private Sub SetStudentProfileAccess()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(3) As SqlClient.SqlParameter

        Try

            param(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar)
            param(0).Value = Session("sUsr_name")
            param(1) = New SqlClient.SqlParameter("@USR_BSU_ID", SqlDbType.VarChar)
            param(1).Value = Session("sBsuid")
            param(2) = New SqlClient.SqlParameter("@MOD_ID", SqlDbType.VarChar)
            param(2).Value = Session("sModule")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GetStudentProfileAccess", param)

            If Not ds Is Nothing AndAlso Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim Dt As DataTable = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    Session("ShowProfile") = Convert.ToString(Dt.Rows(0)("Show").ToString())

                End If

            End If
        Catch ex As Exception
            ' Errorlog(ex.Message)
        End Try



    End Sub
    Sub set_bsu(ByVal bsuid As String)
        Try
            'Session("BSU_Name") = ddlBUnit.SelectedItem.Text
            'Session("sBsuid") = ddlBUnit.SelectedValue
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then

                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(bsuid)
                    While BSUInformation.Read
                        Session("CLM") = IIf(Convert.ToString(BSUInformation("BSU_CLM_ID")) <> Session("CLM"), Session("CLM"), Convert.ToString(BSUInformation("BSU_CLM_ID")))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_COUNTRY_ID") = Trim(Convert.ToString(BSUInformation("BSU_COUNTRY_ID")))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                        Session("BSU_IsOnDAX") = Convert.ToInt32(BSUInformation("BSU_IsOnDAX"))
                        Session("BSU_IsHROnDAX") = Convert.ToInt32(BSUInformation("BSU_IsHROnDAX"))

                        Session("BSU_bVATEnabled") = Convert.ToBoolean(BSUInformation("BSU_bVATEnabled"))
                        Session("BSU_bGSTEnabled") = Convert.ToBoolean(BSUInformation("BSU_bGSTEnabled"))
                        Dim strformat As String = "0."
                        For i As Integer = 1 To CInt(Session("BSU_ROUNDOFF"))
                            strformat = strformat & "0"
                        Next
                        Session("BSU_DataFormatString") = strformat
                    End While
                End Using


            End If



            Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Business Unit", "Business Unit", "Login", Page.User.Identity.Name.ToString)
            If auditFlag <> 0 Then
                Throw New ArgumentException("Unable to track your request")
            End If

            If ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                Dim url As String
                url = String.Format("~\eduShield\eduShield_Home.aspx")
                Session("sModule") = "E0"
                Response.Redirect(url)
            Else
                ' Response.Redirect("Modulelogin.aspx")
            End If
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, "Business Unit Selection")
        End Try
    End Sub
    'Protected Sub ddlBusinessunit_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim ddl As DropDownList = sender
    '    Session("sBsuid") = ddl.SelectedValue
    '    Session("BSU_Name") = ddl.SelectedItem.ToString()
    '    set_bsu(Session("sBsuid"))
    '    Response.Redirect("/PHOENIXBETA/modulelogin.aspx")
    'End Sub

    'Protected Sub lnklogoff_Click(sender As Object, e As EventArgs) Handles lnklogoff.Click
    '    Dim url As String
    '    ' url = String.Format("/PHOENIXBETA/login.aspx?logoff=1")
    '    url = String.Format("/PHOENIXBETA/login.aspx?logoff=1")
    '    Session.Clear()
    '    Response.Redirect(url)
    'End Sub

    'Updated by : Mahesh Chikhale
    'Updated Date : 8/4/2019
    Protected Sub lnklogoff_Click(sender As Object, e As EventArgs) Handles lnklogoff.Click
        Dim url As String
        ' url = String.Format("/PHOENIXBETA/login.aspx?logoff=1")
        Dim isChilLoggedIn As Boolean
        Dim ParentuserName As String
        ParentuserName = Convert.ToString(HttpContext.Current.Session("sUsr_nameActual"))
        Dim currentUserLoginStatus As String = Convert.ToString(HttpContext.Current.Session("isLoggedinChild"))
        isChilLoggedIn = If(String.IsNullOrEmpty(currentUserLoginStatus), False, Convert.ToBoolean(currentUserLoginStatus))
        If (isChilLoggedIn) Then
            Session("sUsr_name") = ParentuserName
            Session("isLoggedinChild") = False
            getActualUserdetails(ParentuserName)
            'url = String.Format("~/Modulelogin.aspx")

        Else
            url = String.Format("/PHOENIXBETA/login.aspx?logoff=1")
            Session.Clear()
            Response.Redirect(url)
        End If


    End Sub
    Private Sub gridbind(ByVal Mnu_module As String, ByVal rol_id As String, ByVal bsu_id As String, ByVal usr_id As String)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Using conn As SqlConnection = New SqlConnection(str_conn)
                Dim str_Sql As String
                str_Sql = "SELECT  MENUS_M.MNU_CODE as code,MENUACCESS_S.MNA_ROL_ID as ROL_ID, " & _
                " MENUACCESS_S.MNA_BSU_ID as BSU_ID,MENUS_M.MNU_TEXT as MNU_TEXT, " & _
                " MENUS_M.MNU_NAME as MNU_Name,MNA_USR_ID,MNA_MNU_ID,MNA_MODULE FROM  MENUACCESS_S WITH(NOLOCK) INNER JOIN MENUS_M WITH(NOLOCK) " & _
                " ON MENUACCESS_S.MNA_MNU_ID = MENUS_M.MNU_CODE WHERE MENUACCESS_S.MNA_USR_ID='" & usr_id & "' " & _
                " and MENUACCESS_S.MNA_BSU_ID='" & bsu_id & "' and MENUACCESS_S.MNA_ROL_ID='" & rol_id & "' " & _
                " and MENUACCESS_S.MNA_MODULE = '" & Mnu_module & "'"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                gvMenu.DataSource = ds.Tables(0)
                If ds.Tables(0).Rows.Count > 0 Then
                    gvMenu.DataSource = ds
                    gvMenu.DataBind()
                Else
                    gvMenu.DataBind()
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lbText_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblmenu As New Label
            Dim lblcode As New Label
            Dim lblMenuText As New LinkButton

            Dim url As String = String.Empty
            lblmenu = TryCast(sender.FindControl("lblmenu"), Label)
            lblMenuText = TryCast(sender.FindControl("lbText"), LinkButton)
            lblcode = TryCast(sender.FindControl("lblcode"), Label)
            Dim mCode As String = encrData.Encrypt(lblcode.Text)
            Dim datamode As String = encrData.Encrypt("none")
            Session("temp") = lblMenuText.Text
            url = String.Format("{0}?MainMnu_code={1}&datamode={2}", lblmenu.Text, mCode, datamode)

            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub
    
   
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Check if the InfoGridView is empty or not to do the transaction

            Dim str_err As String
            Dim MNA_USR_ID As String = Session("sUsr_id")
            Dim MNA_MODULE As String = Session("sModule")
            Dim MNA_rol As String = Session("sroleid")
            Dim MNA_BSU_ID As String = Session("sBsuid")
            Dim mna_code As String = Session("MNU_CODE")
            'new added code
            'Dim dupflag As Integer = AccessRoleUser.checkduplicateMenu_Access(MNA_USR_ID, MNA_MODULE, MNA_BSU_ID)
            ''code needs to be modified
            'If dupflag = 0 Then
            '    Dim sqlstring As String
            '    Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            '        sqlstring = "delete from MENUACCESS_S where  MNA_USR_ID='" & MNA_USR_ID & "' and MNA_MODULE='" & MNA_MODULE & "' and MNA_BSU_ID='" & MNA_BSU_ID & "'"
            '        Dim command As SqlCommand = New SqlCommand(sqlstring, connection)
            '        command.CommandType = Data.CommandType.Text
            '        command.ExecuteNonQuery()
            '    End Using
            'End If
            str_err = CallTransactions(MNA_rol, MNA_BSU_ID, mna_code, MNA_USR_ID, MNA_MODULE)
            If str_err = "0" Then

                gridbind(MNA_MODULE, MNA_rol, MNA_BSU_ID, MNA_USR_ID)




            End If
        Catch ex As Exception
            ' lblErrorMessage.Text = "Process could not be completed"
        End Try
    End Sub
    Private Function CallTransactions(ByVal RoleId As String, BusUnit As String, ByVal menucode As String, ByVal Usr_ID As String, ByVal modules As String) As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim StatusFlag As Integer

                Dim arrModule As New ArrayList


                StatusFlag = AccessRoleUser.InsertQuickMenu(RoleId, BusUnit, menucode, Usr_ID, modules, transaction)
                ' StatusFlag = AccessRoleUser.InsertMenuRights(RoleId, BusUnit, menucode, Usr_ID, modules, transaction)

                If StatusFlag <> 0 Then
                    Throw New ArgumentException("Error while inserting Menu")
                End If

                transaction.Commit()
                Return "0"
                'Adding transaction info
            Catch myex As ArgumentException
                transaction.Rollback()
                Return "1"
            Catch ex As Exception
                transaction.Rollback()
                Return "1"
            End Try
        End Using
    End Function
    Protected Sub btnRpt1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("ReportSel") = "NT"
    End Sub
    Protected Sub btnRpt2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("ReportSel") = "POP"
    End Sub
    Protected Sub btnRpt3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("ReportSel") = "PDF"
    End Sub
    Protected Sub h_STU_ID_ValueChanged(sender As Object, e As EventArgs) Handles h_STU_ID.ValueChanged
        Dim STUID As String = DirectCast(sender, HiddenField).Value
        Response.Redirect("/PHOENIXBETA/GlobalSearch.aspx?ID=" & Encr_decrData.Encrypt(STUID))
    End Sub

    
    Protected Sub ddlFinYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        Session("F_YEAR") = ddlFinYear.SelectedItem.Value
        Session("F_Descr") = ddlFinYear.SelectedItem.Text
        Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(ddlFinYear.SelectedItem.Value)

    End Sub

    Sub bind_Financial_year()
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT FYR_ID, FYR_DESCR, bDefault FROM FINANCIALYEAR_S WITH ( NOLOCK ) ORDER BY bDefault DESC ,FYR_ID DESC"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        ddlFinYear.DataTextField = "FYR_DESCR"
        ddlFinYear.DataValueField = "FYR_ID"
        ddlFinYear.DataSource = ds.Tables(0)
        ddlFinYear.DataBind()

        If Not Session("F_YEAR") Is Nothing Then
            ddlFinYear.ClearSelection()
            '' ddlFinYear.SelectedItem.Value = Session("F_YEAR")
            ddlFinYear.Items.FindByValue(Session("F_YEAR")).Selected = True
        Else
            Session("F_YEAR") = ddlFinYear.SelectedItem.Value
            ''  End If
            Session("F_Descr") = ddlFinYear.SelectedItem.Text
            Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(ddlFinYear.SelectedItem.Value)
        End If
        'If Not Session("F_YEAR") Is Nothing Then
        '    ddlFinYear.SelectedItem.Value = Session("F_YEAR")
        'End If
    End Sub

    Protected Sub hfDeleteQAccess_ValueChanged(sender As Object, e As EventArgs) Handles hfDeleteQAccess.ValueChanged
        Dim MNUID As String = DirectCast(sender, HiddenField).Value
        If MNUID <> "" Then
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim SqlCmd As New SqlCommand("dbo.DELETE_QUICK_ACCESS_MENU", objConn)
            Try
                Dim MNA_USR_ID As String = Session("sUsr_id")
                Dim MNA_MODULE As String = Session("sModule")
                Dim MNA_rol As String = Session("sroleid")
                Dim MNA_BSU_ID As String = Session("sBsuid")

                SqlCmd.Parameters.AddWithValue("@BSU_ID", MNA_BSU_ID)
                SqlCmd.Parameters.AddWithValue("@USR_ID", MNA_USR_ID)
                SqlCmd.Parameters.AddWithValue("@MODULE_ID", MNA_MODULE)
                SqlCmd.Parameters.AddWithValue("@MNU_ID", MNUID)
                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.CommandType = CommandType.StoredProcedure
                SqlCmd.ExecuteNonQuery()
                Dim lintRetVal As Integer = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                If (lintRetVal = 0) Then
                    gridbind(MNA_MODULE, MNA_rol, MNA_BSU_ID, MNA_USR_ID)
                End If
            Catch ex As Exception
            Finally
                objConn.Close()
                SqlCmd.Dispose()
            End Try
        End If
    End Sub
    Sub check_ZAP(ByVal USERNAME As String)
        Dim ds As New DataSet

        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString


        Dim sqlString As String = ""
        Dim PARAM(3) As SqlParameter

        PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))


        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "GET_ZAP_USERS_ACCESS", PARAM)
        If Not ds Is Nothing AndAlso Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                Session("ZAPAccess") = Convert.ToString(Dt.Rows(0)("Access").ToString())

            End If

        End If
        If Session("ZAPAccess") = "1" Then
            ZAPInfo.Visible = True
        Else
            ZAPInfo.Visible = False
        End If
    End Sub


    Sub check_PhoenixSMS(ByVal USERNAME As String)
        Dim ds As New DataSet

        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString


        Dim sqlString As String = ""
        Dim PARAM(3) As SqlParameter

        PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))


        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "dbo.GET_PHOENIXSMS_USERS_ACCESS", PARAM)
        If Not ds Is Nothing AndAlso Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                Session("PSMSAccess") = Convert.ToString(Dt.Rows(0)("Access").ToString())

            End If

        End If
        If Session("PSMSAccess") = "1" Then
            liPhoenixSMS.Visible = True
        Else
            liPhoenixSMS.Visible = False
        End If
    End Sub

    Protected Sub btnAltWDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltWDetails.Click
        Dim str_data As String = UtilityObj.GetDataFromSQL("SELECT     SYS_EMAIL_HOST + '|' + LTRIM(SYS_EMAIL_PORT) + '|' + SYS_ADMIN_EMAIL FROM SYSINFO_S", _
                                    ConnectionManger.GetOASISFINConnectionString)
        Dim str_to_mail As String = str_data.Split("|")(2)
        Try
            str_to_mail = str_to_mail.Split(",")(0) & "," & str_to_mail.Split(",")(1)
            UtilityObj.SendEmail(str_to_mail, "Server name: " & Server.MachineName & _
                                   ", User : " & Session("sUsr_name"), _
             str_data.Split("|")(0), str_data.Split("|")(1), Session("sUsr_name"), _
             "<html><Body>Hi,<br /> " & "Server : " & Server.MachineName & " <br />  This is an Automated mail from GEMS PHOENIX for troubleshooting.<br/>  " _
             & "User Name : " & Session("sUsr_name") & "<br />" _
             & "IP Address : " & Request.UserHostAddress.ToString() & "<br />" _
             & "<br /><br /><br /><br />Regards,<br />GEMS PHOENIX TEAM  </Body></html>", "system@gemseducation.com")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "ALTWEMAIL")

        End Try

    End Sub
    Public Sub getActualUserdetails(Username As String)
        Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Username)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "Get_USR_NAME_ACTIVE", pParms)
            While reader.Read
                Session("sUsr_name") = Username
                Dim lstrActive = Convert.ToString(reader("CURRSTATUS"))
                If lstrActive = "EXPIRED" Then
                    ' Show messge if expired
                    Exit Sub
                End If
            End While
        End Using


        Dim tblusername As String = ""
        Dim tblpassword As String = ""
        Dim tblroleid As Integer = 0
        Dim tblbsuper As Boolean = False
        Dim tblbITSupport As Boolean = False
        Dim tblbsuid As String = ""
        Dim tblbUsr_id As String = ""
        Dim tblDisplay_usr As String = ""
        Dim bCount As Integer = 0
        Dim var_F_Year As String = ""
        Dim var_F_Year_ID As String = ""
        Dim var_F_Date As String = ""
        Dim bUSR_MIS As Integer = 0
        Dim USR_bShowMISonLogin As Integer = 0
        Dim tblModuleAccess As String = ""
        Dim USR_FCM_ID As String = ""
        Dim tblReqRole As String = ""
        Dim tblDays As Integer = 0

        Dim tblBSU_Name As String = ""
        Dim tblEMPID As String = ""
        Dim tblCOSTEMP As String = ""
        Dim tblCOSTSTU As String = ""
        Dim tblCOSTOTH As String = ""
        Dim tbldef_module As String = ""
        Dim tblUSR_ForcePwdChange As Integer = 0

        Try
            Dim dt As New DataTable

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As String = "1000"
                retval = getActualUserdetails(Username, dt, stTrans)
                stTrans.Commit()

                If retval = "0" Then
                    If dt.Rows.Count > 0 Then
                        tblusername = Convert.ToString(dt.Rows(0)("usr_name"))
                        tblDisplay_usr = Convert.ToString(dt.Rows(0)("Display_Name"))
                        'tblpassword = Convert.ToString(dt.Rows(0)("USR_Hashed_PASSWORD")).Split(New Char() {"."c})(1)

                        If Convert.ToString(dt.Rows(0)("USR_Hashed_PASSWORD")) <> "" Then
                            tblpassword = Convert.ToString(dt.Rows(0)("USR_Hashed_PASSWORD")).Split(New Char() {"."c})(1)
                        Else
                            tblpassword = Encr_decrData.Decrypt(Convert.ToString(dt.Rows(0)("usr_password")).Replace(" ", "+"))
                        End If

                        tblroleid = Convert.ToInt32(dt.Rows(0)("usr_rol_id"))
                        tblbsuper = IIf(IsDBNull(dt.Rows(0)("usr_bsuper")), False, dt.Rows(0)("usr_bsuper"))
                        tblbITSupport = IIf(IsDBNull(dt.Rows(0)("USR_bITSupport")), False, dt.Rows(0)("USR_bITSupport"))
                        tblbsuid = Convert.ToString(dt.Rows(0)("usr_bsu_id"))
                        tblbUsr_id = Convert.ToString(dt.Rows(0)("usr_id"))
                        tblModuleAccess = Convert.ToString(dt.Rows(0)("USR_MODULEACCESS"))
                        tblReqRole = Convert.ToString(dt.Rows(0)("USR_REQROLE"))
                        tblEMPID = Convert.ToString(dt.Rows(0)("USR_EMP_ID"))
                        USR_FCM_ID = Convert.ToString(dt.Rows(0)("USR_FCM_ID"))
                        tblBSU_Name = Convert.ToString(dt.Rows(0)("BSU_NAME"))
                        tblCOSTEMP = dt.Rows(0)("COSTcenterEMP").ToString()
                        tblCOSTSTU = dt.Rows(0)("COSTcenterSTU").ToString()
                        tblCOSTOTH = dt.Rows(0)("COSTcenterOTH").ToString()
                        tbldef_module = dt.Rows(0)("USR_MODULEACCESS").ToString()
                        tblUSR_ForcePwdChange = dt.Rows(0)("USR_ForcePwdChange")
                        If IsDBNull(dt.Rows(0)("USR_MIS")) Then
                            bUSR_MIS = 0
                        Else
                            bUSR_MIS = Convert.ToInt32(dt.Rows(0)("USR_MIS"))
                        End If
                        If IsDBNull(dt.Rows(0)("USR_bShowMISonLogin")) Then
                            USR_bShowMISonLogin = 0
                        Else
                            USR_bShowMISonLogin = Convert.ToInt32(dt.Rows(0)("USR_bShowMISonLogin"))
                        End If

                        tblDays = CInt(dt.Rows(0)("Days"))
                    End If
                ElseIf retval = "560" Then

                    Session("sUsr_name") = Username

                    'DisableUserLogin()
                    Exit Sub
                ElseIf retval = "2600" Then
                    Dim RedirectUsername As String = Username

                    Dim LogTime As String = Now.ToString("dd-MMM-yyyyHH")
                    Dim RedirectQueryStr As String
                    'RedirectQueryStr = RedirectUsername & "|" & RedirectPassword & "|" & LogTime
                    'RedirectQueryStr = passwordEncr.Encrypt(RedirectQueryStr)
                    'Dim URL As String
                    'URL = WebConfigurationManager.AppSettings("OldOasisURL") & "/loginRedirect.aspx?OASISLogin=" & RedirectQueryStr
                    'Response.Redirect(URL, False)



                End If
            Catch ex As Exception
                stTrans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try


        Catch ex As Exception

        End Try

        Session("sUsr_id") = tblbUsr_id
        'to used for all the page
        Session("sUsr_name") = tblusername
        Session("sUsr_Display_Name") = tblDisplay_usr
        Session("sroleid") = tblroleid
        Session("sBusper") = tblbsuper
        Session("sBsuid") = tblbsuid
        Session("sBITSupport") = tblbITSupport
        Session("BSU_Name") = tblBSU_Name
        Session("sModuleAccess") = tblModuleAccess
        Session("sReqRole") = tblReqRole
        Session("EmployeeId") = tblEMPID
        Session("counterId") = USR_FCM_ID

        Session("CostEMP") = tblCOSTEMP
        Session("CostSTU") = tblCOSTSTU
        Session("CostOTH") = tblCOSTOTH
        Session("sModule") = tbldef_module
        'insert the session id of the cuurently logged in user
        Session("Sub_ID") = "007"

        Using RoleReader As SqlDataReader = AccessRoleUser.GetRoles_modules(tblroleid)
            While RoleReader.Read
                Session("ROL_MODULE_ACCESS") = Convert.ToString(RoleReader("ROL_MODULE_ACCESS"))
            End While
        End Using

        Using collectReader As SqlDataReader = AccessRoleUser.getCollectBank(tblbsuid)
            While collectReader.Read
                Session("CollectBank") = Convert.ToString(collectReader("CollectBank"))
                Session("Collect_name") = Convert.ToString(collectReader("Collect_name"))
            End While
        End Using
        Using CurriculumReader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M_ID(tblbsuid)
            While CurriculumReader.Read
                Session("CLM") = Convert.ToString(CurriculumReader("BSU_CLM_ID"))
            End While
        End Using
        SessionFlag = AccessRoleUser.UpdateSessionID(tblusername, Session.SessionID)
        If SessionFlag <> 0 Then
            Throw New ArgumentException("Unable to track your request")
        End If

        Using UserreaderFINANCIALYEAR As SqlDataReader = AccessRoleUser.GetFINANCIALYEAR()
            While UserreaderFINANCIALYEAR.Read
                'handle the null value returned from the reader incase  convert.tostring
                var_F_Year_ID = Convert.ToString(UserreaderFINANCIALYEAR("FYR_ID"))
                var_F_Year = Convert.ToString(UserreaderFINANCIALYEAR("FYR_Descr"))
                var_F_Date = Convert.ToString(UserreaderFINANCIALYEAR("FYR_TODT"))
            End While
            'clear of the resource end using
        End Using

        Session("F_YEAR") = var_F_Year_ID
        Session("F_Descr") = var_F_Year
        Session("F_TODT") = Format(var_F_Date, OASISConstants.DateFormat)

        Dim OasisSettingsCookie As HttpCookie
        If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
        Else
            OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
        End If
        OasisSettingsCookie.Values("UsrName") = Username

        OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
        Response.Cookies.Add(OasisSettingsCookie)
        If tblDays <= 0 Then
            'hfExpired.Value = "expired"
            Exit Sub
        Else
            'hfExpired.Value = ""
        End If

        Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Login", "login", "Login", Page.User.Identity.Name.ToString)
        If auditFlag <> 0 Then
            Throw New ArgumentException("Unable to track your request")
        End If
        'if super admin allow all businessunit access
        Session("USR_MIS") = bUSR_MIS
        Session("USR_bShowMISonLogin") = USR_bShowMISonLogin

        If USR_bShowMISonLogin = 1 Then
            Response.Redirect("~/Management/empManagementMain.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()

            Exit Sub
        ElseIf USR_bShowMISonLogin = 2 Then
            Response.Redirect("~/Management/empManagementPrincipal.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()

            Exit Sub
        End If
        If tblbsuper = True Then

            ' Response.Redirect("BusinessUnit.aspx")
            Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                While BSUInformation.Read
                    Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                    Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                    Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                    Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                    Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                    Session("BSU_bVATEnabled") = Convert.ToBoolean(BSUInformation("BSU_bVATEnabled"))
                End While
            End Using
            Session("tot_bsu_count") = "1"
            getActualUsersSettings(Session("sBsuid"))
            Response.Redirect("modulelogin.aspx", False)
            'HttpContext.Current.ApplicationInstance.CompleteRequest()
            'm_bIsTerminating = True
            Exit Sub

        Else
            'if user with single business unit direct the user to module login page else
            'allow the user to select the business unit
            Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                While BUnitreader.Read
                    bCount += 1
                    If bCount = 1 Then
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BUnitreader("Roundoff"))
                        Session("BSU_CURRENCY") = Convert.ToString(BUnitreader("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Session("F_YEAR"), BUnitreader("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Month(Date.Today), BUnitreader("BSU_PAYMONTH")))
                        Session("BSU_bVATEnabled") = Convert.ToBoolean(BUnitreader("BSU_bVATEnabled"))
                        Session("BSU_IsOnDAX") = Convert.ToInt32(BUnitreader("BSU_IsOnDAX"))
                        Session("BSU_IsHROnDAX") = Convert.ToInt32(BUnitreader("BSU_IsHROnDAX"))
                    End If
                End While
            End Using

            Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                While BSUInformation.Read
                    Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                    Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                    Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                    Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                    Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                End While
            End Using
        End If

        getActualUsersSettings(Session("sBsuid"))
        Response.Redirect("~/Modulelogin.aspx", False)
        HttpContext.Current.ApplicationInstance.CompleteRequest()

        Exit Sub

    End Sub

    Public Shared Function GetActualUserDetails(ByVal p_USR_NAME As String, ByRef dtUserData As DataTable, ByVal stTrans As SqlTransaction) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Try

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
            pParms(0).Value = p_USR_NAME
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "GetUserDetails_byUserName", pParms)

            If pParms(1).Value = "0" Then
                If ds.Tables.Count > 0 Then
                    dtUserData = ds.Tables(0)

                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        Return GetActualUserDetails = 1
    End Function


    Private Sub getActualUsersSettings(ByVal BusUnit As String)
        Try
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(BusUnit)
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_COUNTRY_ID") = Trim(Convert.ToString(BSUInformation("BSU_COUNTRY_ID")))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                        Session("BSU_IsOnDAX") = Convert.ToInt32(BSUInformation("BSU_IsOnDAX"))
                        Session("BSU_IsHROnDAX") = Convert.ToInt32(BSUInformation("BSU_IsHROnDAX"))
                        Dim strformat As String = "0."
                        For i As Integer = 1 To CInt(Session("BSU_ROUNDOFF"))
                            strformat = strformat & "0"
                        Next
                        Session("BSU_DataFormatString") = strformat
                    End While
                End Using
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim SQLStr As String
                SQLStr = "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1"
                Dim dtFinYear As DataTable
                dtFinYear = Mainclass.getDataTable(SQLStr, str_conn)
                If dtFinYear.Rows.Count > 0 Then
                    Session("F_YEAR") = dtFinYear.Rows(0).Item("FYR_ID")
                    Session("F_Descr") = dtFinYear.Rows(0).Item("FYR_DESCR")
                    Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(dtFinYear.Rows(0).Item("FYR_ID"))
                End If
            End If

            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Business Unit Selection")
        End Try
    End Sub

    Protected Sub ahelpdesk_ServerClick(sender As Object, e As EventArgs)
        Dim url As String
        url = String.Format(Page.ResolveUrl("~/homepage.aspx"))
        Session("sModule") = "HD"
        Session("Modulename") = "Help Desk"
        StoreSettings()

        ' Dim HdeskAccess = GetAccesstoPHOENIXModule()

        'If HdeskAccess > 0 Then
        Response.Redirect(url)

        '' redirectToOasis()
        'Response.Redirect(url)
    End Sub

    Sub StoreSettings()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC update_module '" + Session("sUsr_name") + "','" + Session("sModule") + "'"
        SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Session("modPage") = "1"
    End Sub

    'Protected Sub imgPulse_Click(sender As Object, e As ImageClickEventArgs)
    '    Dim strUSername As String = String.Empty
    '    strUSername = Encr_decrData.Encrypt(Session("sUsr_name"))
    '    '' Response.Redirect("https://myphoenix.gemseducation.com/PHOENIXPULSE/AX_Integration_PULSE.aspx?U=" + strUSername + "&C=7MZKxsKZgG0=", True)

    '    Dim strurl As String = "https://myphoenix.gemseducation.com/PHOENIXPULSE/AX_Integration_PULSE.aspx?U=" + strUSername + "&C=7MZKxsKZgG0="

    '    Response.Write("<script>window.open ('" & strurl & "','_blank');</script>")
    'End Sub

    Protected Sub PulseLink_Click(sender As Object, e As EventArgs)
        Dim strUSername As String = String.Empty
        strUSername = Encr_decrData.Encrypt(Session("sUsr_name"))
        '' Response.Redirect("https://myphoenix.gemseducation.com/PHOENIXPULSE/AX_Integration_PULSE.aspx?U=" + strUSername + "&C=7MZKxsKZgG0=", True)

        '' Dim strurl As String = "https://myphoenix.gemseducation.com/PHOENIXPULSE/AX_Integration_PULSE.aspx?U=" + strUSername + "&C=7MZKxsKZgG0="

        Dim strurl As String = "https://pulse.gemseducation.com/AX_Integration_PULSE.aspx?U=" + strUSername + "&C=7MZKxsKZgG0="

        Response.Write("<script>window.open ('" & strurl & "','_blank');</script>")
    End Sub

    Function GetMArqueeText() As String
        Dim marqueeText As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@PORTAL", "PHOENIX")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GetMarqueeForOfflineMessage", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                marqueeText = Dt.Rows("0")("MARQUEETEXT").ToString()
            End If

        End If
        Return marqueeText
    End Function

    Function GetSIMSREDIRECTUSER() As String
        Dim redirctactive As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_USR_REDIRECT_SIMS ", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                redirctactive = Dt.Rows("0")("active").ToString()
            End If

        End If
        Return redirctactive
    End Function

    Protected Sub lnkPhoenixSMS_Click(sender As Object, e As EventArgs)
        Dim strUSername As String = String.Empty

        Dim strreplaceusr = String.Empty

        Dim redirectactive = String.Empty
        redirectactive = GetSIMSREDIRECTUSER()

        If redirectactive = "1" Then
            strreplaceusr = "f.sarwar_gfm"
        Else
            strreplaceusr = Session("sUsr_name")
        End If

        ''added for charan
        'If Session("sUsr_name") = "test.user" Or Session("sUsr_name") = "charan" Then
        '    strreplaceusr = "f.sarwar_gfm"
        'Else
        '    strreplaceusr = Session("sUsr_name")
        'End If
        strUSername = "&Username=" + Encr_decrData.Encrypt(strreplaceusr)
        Dim strurl As String = WebConfigurationManager.AppSettings("phoenixSMSUrl")

        strurl = strurl + strUSername

        Response.Write("<script>window.open ('" & strurl & "','_blank');</script>")
    End Sub
End Class

