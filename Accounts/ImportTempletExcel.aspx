﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ImportTempletExcel.aspx.vb" Inherits="Accounts_ImportTempletExcel" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
    <style>
        .btn-info {

    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
    margin-left: 15px;

}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <link href="../vendor/custom-file/component.css" rel="stylesheet" />
    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <i class="fa fa-book mr-3"></i>
                    Import Excel Template 
      
                </div>


            </div>
        </div>

        <div class="card-body">
            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>

            <div class="row">
                <div class="col-md-12">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>

                </div>
                <div class="col-md-12">
                    <asp:CheckBox Text="Create New Template and table" ID="chb_NewTable" AutoPostBack="true" runat="server" OnCheckedChanged="chb_NewTable_CheckedChanged" />
                </div>
                <div class="col-md-12" runat="server" id="DIV_Create_Table_holder" visible="false">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="Ftitle">Template Name </label>
                                <asp:TextBox ID="txt_Template_name" CssClass="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ErrorMessage="Template Name" ControlToValidate="txt_Template_name" runat="server" ID="rfv_txt_Template_name" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="Ftitle">Table Name </label>
                                <asp:TextBox ID="txt_table_Name" CssClass="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ErrorMessage="Table Name" ControlToValidate="txt_table_Name" runat="server" ID="rfv_txt_table_Name" />
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="Ftitle">Select School   </label>

                                <asp:DropDownList ID="ddlBsu" runat="server" CssClass="form-control" ValidationGroup="Selecte_Temp"></asp:DropDownList>

                                <asp:RequiredFieldValidator ErrorMessage="Select School" ControlToValidate="ddlBsu" ValidationGroup="Selecte_Temp" runat="server" ID="rfv_ddlBsu" InitialValue="0" />

                            </div>
                        </div>
                        <div class="col-md-4" id="DIV_Import_Files_holder" runat="server">
                            <div class="form-group">
                                <label class="Ftitle">Select Template </label>
                                <asp:DropDownList ID="ddlTemplate" CssClass="form-control" ValidationGroup="Selecte_Temp" Style="text-transform: capitalize" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ErrorMessage="Select Template" ValidationGroup="Selecte_Temp" ControlToValidate="ddlTemplate" runat="server" ID="RequiredFieldValidator1" InitialValue="0" />
                            </div>
                        </div>

                        <div class="col-md-4 ">

                            <div class="md-form md-outline p-4">
                                <div class="box">
                                    <%--<input type="file" name="file_7[]" id="file_7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple style="display:none" />--%>
                                    <asp:FileUpload runat="server" ID="FileUpload_File" CssClass="inputfile inputfile-6" Style="display: none" data-multiple-caption="{count} files selected" />
                                    <label for="ctl00_cphMasterpage_FileUpload_File">
                                        <span></span><strong>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" />
                                            </svg>
                                            Choose a file&hellip;</strong></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <asp:Button Text="Create" runat="server" ID="btn_Create_table" class="btn btn-primary" OnClick="btn_Create_table_Click" />

                            <asp:Button Text="Save" runat="server" ID="btn_save" class="btn btn-primary" ValidationGroup="Selecte_Temp" OnClick="btn_save_Click" />
                            <asp:Button Text="Get Upload History" runat="server" ID="btn_get_Upload_history" class="btn btn-info" ValidationGroup="Selecte_Temp" OnClick="btn_get_Upload_history_Click" />
                        </div>
                    </div>
                </div>
                  <div class="col-md-12" runat="server" id="DIV_File_View" visible="false">
                 <div class="table-responsive mt-2">
                        <div class="row">
                <div class="col-md-12">
    <asp:GridView ID="gvExcelImport" runat="server" CssClass="table table-bordered table-row  table-striped"
        EmptyDataText="No Data" AutoGenerateColumns="true"
        PageSize="20" AllowPaging="True" Width="100%"
        ShowFooter="True" OnPageIndexChanging="gvExcelImport_PageIndexChanging">
    </asp:GridView>
                    </div>
                            </div>
</div>
                      </div>
      <div class="col-md-12 mt-2" runat="server" id="DIV_History_View" visible="false">
             <div class="row">
                <div class="col-md-12">
    <asp:GridView ID="GV_Upload_history" runat="server" CssClass="table table-bordered table-row table-striped"
        EmptyDataText="No Data" AutoGenerateColumns="false"
        PageSize="20" AllowPaging="True" Width="100%"
      OnPageIndexChanging="GV_Upload_history_PageIndexChanging">
        <RowStyle CssClass="griditem" />
        <Columns>
            <asp:TemplateField HeaderText="Available" >
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <HeaderTemplate>

                                <asp:CheckBox ID="chkAll1" onclick="javascript:change_chk_state1(this);" runat="server"
                                    ToolTip="Click here to select/deselect all rows" Text=""></asp:CheckBox>


                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:CheckBox ID="chkSelect1" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>

                            </ItemTemplate>
                            <HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
            <asp:TemplateField HeaderText="FTF_ID" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblFTF_ID" runat="server" Text='<%# Bind("FTF_ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Table Name" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblTable_Name" runat="server" Text='<%# Bind("FTM_Table_Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Schoole">
                <ItemTemplate>
                    <asp:Label ID="lblBSU_NAME" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Template">
                <ItemTemplate>
                    <asp:Label ID="lblFTM_DESCR" runat="server" Text='<%# Bind("FTM_DESCR") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="CreateBy">
                <ItemTemplate>
                    <asp:Label ID="lblFTF_CreateBy" runat="server"  Text='<%# Bind("FTF_CreateBy") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CreateOn">
                <ItemTemplate>
                    <asp:Label ID="lblFTF_CreateOn" runat="server"  Text='<%# Bind("FTF_CreateOn", "{0:dd/MMM/yyyy-hh:mm tt}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
         
        </Columns>
    </asp:GridView>
                    </div>
                 <div class="col-md-12 text-center">
                      <asp:LinkButton Text="Delete" class="btn btn-primary" runat="server" ID="lbtn_Delete_History" OnClick="lbtn_Delete_History_Click" OnClientClick="if ( ! UserDeleteConfirmation()) return false;" />
                     </div>
                 </div>

    </div>
            </div>
        </div>
    </div>

   


    <script src="../vendor/custom-file/custom-file-input.js"></script>
    <script>
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=GV_Upload_history.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }

        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


    function change_chk_state1(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect1") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click(); //fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }

        function UserDeleteConfirmation() {
    return confirm("Are you sure you want to delete this row(s)?");
}
    </script>
</asp:Content>

