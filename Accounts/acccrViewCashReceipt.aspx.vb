Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Accounts_acccrViewCashReceipt
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "acccrCashReceipt.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

            h_Grid.Value = "top"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A150005") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            gvChild.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
            If Request.QueryString("deleted") <> "" Then
                lblError.Text = getErrorMessage("520")
            End If
        End If
    End Sub


    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function returnpath(ByVal p_posted As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_posted)
            If p_posted Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try
    End Function


    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If

    End Function


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblGUID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = "acccrCashReceipt.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                hlCEdit.Enabled = True
            End If
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String

        Dim lstrRefNo As String = String.Empty
        Dim lstrDocNo As String = String.Empty
        Dim lstrDocDate As String = String.Empty
        Dim lstrRecieptNo As String = String.Empty
        Dim lstrNarration As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrCollection As String = String.Empty
        Dim lstrOpr As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltrefNo As String = String.Empty
        Dim lstrFiltRecieptNo As String = String.Empty
        Dim lstrFiltDocDate As String = String.Empty
        Dim lstrFiltNarration As String = String.Empty
        Dim lstrFiltAmount As String = String.Empty
        Dim lstrFiltColln As String = String.Empty
        Dim larrSearchOpr() As String

        Dim txtSearch As New TextBox
        
        If gvJournal.Rows.Count > 0 Then
            ' --- Initialize The Variables


            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   refno
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtrefNo")
            lstrRefNo = Trim(txtSearch.Text)
            If (lstrRefNo <> "") Then lstrFiltrefNo = SetCondn(lstrOpr, "VHH_REFNO", lstrRefNo)

            '   -- 1  docno
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtdocNo")
            lstrDocNo = Trim(txtSearch.Text)
            If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "VHH_DOCNO", lstrDocNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtDocDATE")
            lstrDocDate = txtSearch.Text
            If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "VHH_DOCDT", lstrDocDate)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
            lstrNarration = txtSearch.Text
            If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "VHH_Narration", lstrNarration)

            '-- 6 Amount
            larrSearchOpr = h_Selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            lstrAmount = txtSearch.Text
            If (lstrAmount <> "") Then lstrFiltAmount = SetCondn(lstrOpr, "VHH_AMOUNT", lstrAmount)

            '   --   COLLUN
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvJournal.HeaderRow.FindControl("txtCollection")
            lstrCollection = txtSearch.Text
            If (lstrCollection <> "") Then lstrFiltColln = SetCondn(lstrOpr, "COL_DESCR", lstrCollection)

        End If

        Dim str_ListDoc As String = String.Empty
        If Session("ListDays") IsNot Nothing Then
            'If String.Compare(Session("ListDays"), "all", True) <> 0 Then
            str_ListDoc = " AND VOUCHER_H.VHH_DOCDT BETWEEN '" & Format(Date.Now.AddDays(Session("ListDays") * -1), OASISConstants.DataBaseDateFormat) & "' AND '" & Format(Date.Now, OASISConstants.DataBaseDateFormat) & "' "
        End If

        '''''''''''
        Dim str_Filter As String = ""
        If rbPosted.Checked = True Then
            str_Filter = "AND VHH_bPOSTED=1"
            str_Filter += str_ListDoc
        End If
        If rbUnposted.Checked = True Then
            str_Filter = "AND VHH_bPOSTED=0"
        End If

        If rbAll.Checked = True Then
            str_Filter = str_ListDoc
        End If
        Dim str_Topfilter As String = ""
        If UsrTopFilter1.FilterCondition <> "All" Then
            str_Topfilter = " top " & UsrTopFilter1.FilterCondition
        End If
        str_Sql = "SELECT  " & str_Topfilter & " VHH.VHH_REFNO,VHH.GUID, VHH.VHH_SUB_ID, VHH.VHH_BSU_ID," _
        & " VHH.VHH_FYEAR, VHH.VHH_DOCTYPE, VHH.VHH_DOCNO, VHH.VHH_TYPE," _
        & " VHH.VHH_DOCDT, VHH.VHH_CHQDT, VHH.VHH_CUR_ID, VHH.VHH_EXGRATE1," _
        & " VHH.VHH_EXGRATE2, VHH.VHH_NARRATION, VHH.VHH_bPOSTED, VHH.VHH_Count,VHH_AMOUNT," _
        & " VHH.VHH_bDELETED, VHH.VHH_AMOUNT, VHH.VHH_COL_ID, " _
        & " COL.COL_DESCR FROM VOUCHER_H AS VHH INNER JOIN COLLECTION_M AS COL " _
        & " ON VHH.VHH_COL_ID = COL.COL_ID" & _
        " WHERE  (VHH.VHH_SUB_ID = '" & Session("Sub_ID") & "') " _
        & " AND (VHH.VHH_BSU_ID = '" & Session("sBsuid") & "') AND (VHH.VHH_bDELETED = 0) " _
        & " AND (VHH.VHH_DOCTYPE = 'CR') AND VHH.VHH_FYEAR = " & Session("F_YEAR") & str_Filter _
        & lstrFiltrefNo & lstrFiltDocNo & lstrFiltDocDate & lstrFiltNarration & lstrFiltAmount & _
        lstrFiltColln & lstrFiltRecieptNo & _
        " ORDER BY VHH.VHH_DOCDT DESC, VHH.VHH_DOCNO DESC"

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvJournal.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvJournal.DataBind()
            Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

            gvJournal.Rows(0).Cells.Clear()
            gvJournal.Rows(0).Cells.Add(New TableCell)
            gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
            gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvJournal.DataBind()
        End If


        txtSearch = gvJournal.HeaderRow.FindControl("txtrefNo")
        txtSearch.Text = lstrRefNo

        txtSearch = gvJournal.HeaderRow.FindControl("txtDocno")
        txtSearch.Text = lstrDocNo

        txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
        txtSearch.Text = lstrNarration

        txtSearch = gvJournal.HeaderRow.FindControl("txtDocdate")
        txtSearch.Text = lstrDocDate

        txtSearch = gvJournal.HeaderRow.FindControl("txtCollection")
        txtSearch.Text = lstrCollection

        txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
        txtSearch.Text = lstrAmount
        'gvJournal.DataBind()
        gvJournal.SelectedIndex = p_selected_id
    End Sub


    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvDetails.Visible = True
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim i As Integer = sender.parent.parent.RowIndex
                gridbind(i)
                gvDetails.SelectedIndex = -1
                '        Dim lblGrpCode As New Label
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)

                str_Sql = "select * FROM VOUCHER_H where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                h_Grid.Value = "detail"

                If ds.Tables(0).Rows.Count > 0 Then
                    'str_Sql = "SELECT * FROM VOUCHER_D WHERE VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'" _
                    '& " AND VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "' AND VHD_BSU_ID='" _
                    '& ds.Tables(0).Rows(0)("VHH_BSU_ID") & "' AND VHD_DOCTYPE='CR'"


                    str_Sql = "SELECT     VOUCHER_D.GUID, VOUCHER_D.VHD_SUB_ID, " _
                    & " VOUCHER_D.VHD_BSU_ID, VOUCHER_D.VHD_FYEAR," _
                    & " VOUCHER_D.VHD_DOCTYPE, VOUCHER_D.VHD_DOCNO," _
                    & " VOUCHER_D.VHD_LINEID, VOUCHER_D.VHD_ACT_ID," _
                    & " VOUCHER_D.VHD_AMOUNT, VOUCHER_D.VHD_NARRATION," _
                    & " (VOUCHER_D.VHD_ACT_ID+'-'+ACCOUNTS_M.ACT_NAME) AS ACCOUNT" _
                    & " FROM VOUCHER_D INNER JOIN ACCOUNTS_M" _
                    & " ON VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID" _
                    & " AND VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID" _
                    & " WHERE (VOUCHER_D.VHD_DOCNO = '" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "')" _
                    & " AND (VOUCHER_D.VHD_SUB_ID = '" & Session("Sub_ID") & "')" _
                    & " AND (VOUCHER_D.VHD_BSU_ID = '" & Session("sBsuid") & "') " _
                    & " AND  (VOUCHER_D.VHD_DOCTYPE = 'CR')"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
   Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        Dim vDOC_NO As String = contextKey
        Dim drReader As SqlDataReader
        Try
            Dim str_sql As String = "SELECT VHD_bBANKRECNO, " & _
            " SUM(VOUCHER_D.VHD_AMOUNT) as VHD_AMOUNT FROM VOUCHER_D INNER JOIN ACCOUNTS_M ON  " & _
            " VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID " & _
            " WHERE  (VOUCHER_D.VHD_DOCNO = '" & vDOC_NO & "') " & _
            " AND (VOUCHER_D.VHD_BSU_ID = '" & HttpContext.Current.Session("sBSUID") & "')  AND (VOUCHER_D.VHD_DOCTYPE = 'CR') " & _
            "GROUP BY VHD_bBANKRECNO"
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class ='popdetails'>")
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=3><b>Related Vouchers</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            'sTemp.Append("<td><b>ACOUNT NAME</b></td>")
            sTemp.Append("<td><b>REC. No</b></td>")
            sTemp.Append("<td><b>AMOUNT</b></td>")
            sTemp.Append("</tr>")
            While (drReader.Read())
                sTemp.Append("<tr>")
                If drReader("VHD_bBANKRECNO").ToString() = "" Then
                    Return ""
                End If
                'sTemp.Append("<td>super" & drReader("ACT_CODE").ToString & "</td>")
                sTemp.Append("<td>" & drReader("VHD_bBANKRECNO").ToString & "</td>")
                sTemp.Append("<td>" & drReader("VHD_AMOUNT").ToString & "</td>")
                sTemp.Append("</tr>")
            End While
            '    If HttpContext.Current.Session("STUD_DET") Is Nothing Then
            '        sTemp.Append("</table>")
            '        Return sTemp.ToString()
            '    End If
            '    Dim httab As Hashtable = HttpContext.Current.Session("STUD_DET")
            '    Dim vFEE_PERF As FEEPERFORMAINVOICE = httab(STUD_ID)
            '    If Not vFEE_PERF Is Nothing Then
            '        Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
            '        Dim ienum As IEnumerator = arrList.GetEnumerator()
            '        While (ienum.MoveNext())
            '            Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
            '            sTemp.Append("<tr>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_ID.ToString & "</td>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_DESCR & "</td>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_AMOUNT.ToString & "</td>")
            '            sTemp.Append("</tr>")
            '        End While
            '    End If
        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function


    Protected Sub gvJournal_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvJournal.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Programmatically reference the PopupControlExtender 
            Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("PopupControlExtender1"), AjaxControlToolkit.PopupControlExtender)

            ' Set the BehaviorID 
            Dim behaviorID As String = String.Concat("pce", e.Row.RowIndex)
            pce.BehaviorID = behaviorID

            ' Programmatically reference the Image control 
            Dim i As LinkButton = DirectCast(e.Row.Cells(1).FindControl("lblAmount"), LinkButton)

            ' Add the clie nt-side attributes (onmouseover & onmouseout) 
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)

            'i.Attributes.Add("onmouseover", OnMouseOverScript)
            i.Attributes.Add("onclick", OnMouseOverScript)
            i.Attributes.Add("onmouseout", OnMouseOutScript)
        End If
    End Sub


    Protected Sub gvChild_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvChild.Sorting
        ''gridbind_child()
    End Sub


    Protected Sub gvDetails_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvDetails.SelectedIndexChanging
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True
            gvDetails.SelectedIndex = e.NewSelectedIndex
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label

                '        Dim lblGrpCode As New Label
                lblGUID = TryCast(gvDetails.SelectedRow.FindControl("lblGUID"), Label)
                lblSlno = TryCast(gvDetails.SelectedRow.FindControl("lblSlno"), Label)

                str_Sql = "select * FROM VOUCHER_D where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    'str_Sql = "SELECT * FROM VOUCHER_D_S WHERE VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHD_DOCNO") & "'" _
                    '& " AND VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHD_SUB_ID") & "' AND VDS_BSU_ID='" _
                    '& ds.Tables(0).Rows(0)("VHD_BSU_ID") & "' AND VDS_DOCTYPE='" & ds.Tables(0).Rows(0)("VHD_DOCTYPE") & "'" _
                    '& "AND VDS_SLNO='" & lblSlno.Text & "'"

                    str_Sql = "SELECT    VOUCHER_D_S.vds_doctype,VOUCHER_D_S.vds_docno," _
                    & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME,  " _
                    & " VOUCHER_D_S.VDS_DESCR, " _
                    & " case isnull(VOUCHER_D_S.VDS_CODE,'') " _
                    & " when '' then 'GENERAL' " _
                    & " else  COSTCENTER_S.CCS_DESCR end as GRPFIELD, " _
                    & " COSTCENTER_S.CCS_DESCR , VOUCHER_D_S.VDS_CODE, " _
                    & " VOUCHER_D_S.VDS_AMOUNT, COSTCENTER_S.CCS_QUERY " _
                    & " FROM VOUCHER_D INNER JOIN ACCOUNTS_M ON " _
                    & " ACCOUNTS_M.ACT_ID = VOUCHER_D.VHD_ACT_ID " _
                    & " LEFT OUTER JOIN VOUCHER_D_S ON " _
                    & " VOUCHER_D.VHD_SUB_ID=VOUCHER_D_S.VDS_SUB_ID AND " _
                    & " VOUCHER_D.VHD_BSU_ID=   VOUCHER_D_S.VDS_BSU_ID  AND " _
                    & " VOUCHER_D.VHD_FYEAR =VOUCHER_D_S.VDS_FYEAR AND " _
                    & " VOUCHER_D.VHD_DOCTYPE=VOUCHER_D_S.VDS_DOCTYPE AND " _
                    & " VOUCHER_D.VHD_DOCNO=VOUCHER_D_S.VDS_DOCNO  AND " _
                    & " VOUCHER_D.VHD_LINEID = VOUCHER_D_S.VDS_SLNO " _
                    & " LEFT OUTER JOIN  COSTCENTER_S " _
                    & " ON  VOUCHER_D_S.VDS_CCS_ID=COSTCENTER_S.CCS_ID " _
                    & " WHERE  VOUCHER_D_S.VDS_DOCTYPE='CR' AND " _
                    & " VOUCHER_D_S.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHD_DOCNO") & "' AND" _
                    & " VOUCHER_D_S.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHD_SUB_ID") & "' AND" _
                    & " VOUCHER_D_S.VDS_BSU_ID='" & Session("sBsuid") & "' AND" _
                    & " VOUCHER_D_S.VDS_FYEAR = " & Session("F_YEAR") _
                    & "AND VDS_SLNO='" & lblSlno.Text & "'" _
                    & " ORDER BY GRPFIELD"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    'gvChild.DataBind()
                    If dsc.Tables(0).Rows.Count > 0 Then
                        h_Grid.Value = "child"
                    Else
                        h_Grid.Value = "detail"
                    End If
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    'helper.RegisterGroup("5", True, True)

                    helper.RegisterSummary("VDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    gvChild.DataBind()
                    helper.ApplyGroupSort()

                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                Throw        'Bubble up the exception
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        ViewState("STATE") = "ALL"
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub


    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPosted.CheckedChanged
        ViewState("STATE") = "POSTED"
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub


    Protected Sub rbUnposted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnposted.CheckedChanged
        ViewState("STATE") = "UNPOSTED"
        gridbind()
        gvChild.Visible = False
        gvDetails.Visible = False
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Public Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim grdRow As GridViewRow
            If sender.Parent IsNot Nothing Then
                grdRow = sender.Parent.Parent
                If grdRow IsNot Nothing Then
                    Dim lblDocNo As Label
                    lblDocNo = grdRow.FindControl("lblDocNo")
                    If lblDocNo Is Nothing Then Exit Sub
                    Dim repSource As New MyReportClass
                    repSource = VoucherReports.CashReceiptVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "CR", lblDocNo.Text, Session("HideCC"))
                    Session("ReportSource") = repSource
                    'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
                    ReportLoadSelection()
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
