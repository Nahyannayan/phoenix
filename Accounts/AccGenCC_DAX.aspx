<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccGenCC_DAX.aspx.vb" Inherits="AccGenCC_DAX" Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value);
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(2);
            UpdateSum();
            return true;
        }
        function UpdateSum() {
            var txtBankCom, txtNetAmount, txtdepAmount, txtBankCom_Client, txtTax_coll;
            txtBankCom = parseFloat(document.getElementById('<%=txtBankCom.ClientID %>').value);
            if (isNaN(txtBankCom))
                txtBankCom = 0;
            txtdepAmount = parseFloat(document.getElementById('<%=txtdepAmount.ClientID %>').value);
            if (isNaN(txtdepAmount))
                txtdepAmount = 0;
            txtBankCom_Client = parseFloat(document.getElementById('<%=txtBankCom_Client.ClientID %>').value);
            if (isNaN(txtBankCom_Client))
                txtBankCom_Client = 0;

            txtTax_coll = parseFloat(document.getElementById('<%=txtTax_coll.ClientID %>').value);
            if (isNaN(txtTax_coll))
                txtTax_coll = 0;


            //        txtNetAmount=txtdepAmount-txtBankCom-txtBankCom_Client-txtTax_coll;             
            txtNetAmount = txtdepAmount - txtBankCom - txtBankCom_Client;
            document.getElementById('<%=txtNetAmount.ClientID %>').value = txtNetAmount.toFixed(2);
        }

        function PopUpCall(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

            var sFeatures;
            var lstrVal;
            var lintScrVal;

            sFeatures = "dialogWidth: " + pWidth + "px; ";
            sFeatures += "dialogHeight: " + pHeight + "px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            var result = radopen("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById('<%=txtBankCode.ClientId %>').value, "pop_up2")
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl).value = lstrVal[0];
            //document.getElementById(ctrl1).value = lstrVal[1];
        }


        function PopUpCall2(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

            var sFeatures;
            var lstrVal;
            var lintScrVal;

            sFeatures = "dialogWidth: " + pWidth + "px; ";
            sFeatures += "dialogHeight: " + pHeight + "px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            // result = window.showModalDialog("acccpShowCashflow.aspx" + QRYSTR, "", sFeatures);
            var result = radopen("acccpShowCashflow.aspx?rss=1", "pop_up3")
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('__');
            //document.getElementById(ctrl).value = lstrVal[0];
        }

        function PopUpCall3(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

            var sFeatures;
            var lstrVal;
            var lintScrVal;

            sFeatures = "dialogWidth: " + pWidth + "px; ";
            sFeatures += "dialogHeight: " + pHeight + "px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            //result = window.showModalDialog("accgenbrcc.aspx?ShowType=" + pMode + "", "", sFeatures);
            var result = radopen("accgenbrcc.aspx?ShowType=EDItdEP", "pop_up3")
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl1).value = lstrVal[0];
            //document.getElementById(ctrl2).value = lstrVal[1];
        }


        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtCashFlow.ClientID%>').value = NameandCode[0];
              <%--  document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1];--%>
            }
        }
        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtdepAmount.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtBankCom.ClientId %>').value = NameandCode[1];
            }
        }


        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientId %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1];
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Credit Card Clearance
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table cellspacing="0" cellpadding="5" width="100%" align="center">

                    <tr>
                        <td align="left" width="100%">
                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                <tr valign="top">
                                    <td align="left" colspan="4" width="100%">
                                        <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" EmptyDataText="No transaction details added yet." Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Doc Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltranDate" runat="server" Text='<%# Bind("DocDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="VHH_DOCNO" HeaderText="Doc No" ReadOnly="True" />

                                                <asp:TemplateField HeaderText="CRR_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCRRID" runat="server" Text='<%# Bind("CRR_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CCC_DESCR" HeaderText="Card Type" ReadOnly="True">
                                                    <ItemStyle />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Total transaction">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("tranAmount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Bank Charge(Client)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBankCharge_Client" runat="server" Text='<%# Bind("BANKCHARGE_CLIENT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tax">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTax" runat="server" Text='<%# Bind("TAX_AMOUNT_VAL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edit"
                                                            OnClick="LinkButton1_Click" Text="Deposit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Deposit
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table align="center" cellspacing="0" cellpadding="5" width="100%">
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Doc No</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtdocNo" runat="server"></asp:TextBox></td>
                                    <td width="20%" align="left"><span class="field-label">Doc Date</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgDocDate" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Bank Account</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtBankCode" runat="server"></asp:TextBox>
                                        <a href="#" onclick="PopUpCall('460','400','BANK','<%=txtBankCode.ClientId %>','<%=txtBankDescr.ClientId %>')">
                                            <img border="0" src="../Images/cal.gif" id="IMG3" /></a></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <asp:HiddenField ID="hCardAccount" runat="server" />
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Cash Flow</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtCashFlow" runat="server">
                                        </asp:TextBox>
                                        <a href="#" onclick="PopUpCall2('460','400','CASHFLOW_BR','<%=txtCashFlow.ClientId %>')">
                                            <img border="0" src="../Images/cal.gif" id="IMG1" /></a></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Narration</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtNarration" runat="server"
                                            TabIndex="16" TextMode="MultiLine">CREDIT CARD COLLECTION DEPOSIT</asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Card Type</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtCardType" runat="server"></asp:TextBox></td>
                                    <td width="20%" align="left"><span class="field-label">Deposit Amount</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtdepAmount" runat="server"></asp:TextBox>
                                        <a href="#" onclick="PopUpCall3('600','400','EDItdEP','1','<%=txtdepAmount.ClientId %>','<%=txtBankCom.ClientId %>')">
                                            <img border="0" src="../Images/cal.gif" id="IMG8" language="javascript" /></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Commission</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtComsn" runat="server"></asp:TextBox></td>
                                    <td width="20%" align="left"><span class="field-label">Bank Charges</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtBankCom" runat="server" onBlur="CheckAmount(this);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label"></span></td>
                                    <td width="30%" align="left"></td>
                                    <td width="20%" align="left"><span class="field-label">Bank Charges(Client)</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtBankCom_Client" runat="server" onBlur="CheckAmount(this);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label"></span></td>

                                    <td width="30%" align="left"></td>
                                    <td width="20%" align="left"><span class="field-label">Tax</span></td>

                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtTax_coll" runat="server" onblur="CheckAmount(this);"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                    <td width="20%" align="left"><span class="field-label">Net</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtNetAmount" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            TabIndex="30" Text="Cancel" /></td>
                        </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDocDate" TargetControlID="txtdocDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hColln" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
