﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class Accounts_DashboardSetting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MainMnu_code As String

        Try
            If Not IsPostBack Then
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    ' Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    filldashboardtypes()
                    FILLDASHBOARDREPORT()
                    POPULATEROLE()                    
                    idvalue.Value = 0
                    DisplayActions()
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Load: " + ex.Message, "DASHBOARD SETTING SERVICES")
        End Try

    End Sub
   

    Public Function GetDoubleVal(ByVal Value As Object) As Double
        'TO CONVERT VALUE TO DECIMAL VALUE
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

    Private Sub filldashboardtypes()        
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim dsActivityTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[GET_DASHBOARD_TYPES]")
            ddltype.DataSource = dsActivityTypes.Tables(0)
            ddltype.DataBind()
            ddltype.DataTextField = "TYPE"
            ddltype.DataValueField = "ID"
            ddltype.DataBind()
            ddltype.Items.Insert(0, New ListItem("SELECT", 0))
            ddltype.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From filldashboardtypes: " + ex.Message, "DASHBOARD SETTING SERVICES")
        End Try
    End Sub

    Protected Sub chkdrldwn_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If (chkdrldwn.Checked) Then
                txtdrill.Visible = True
            Else
                txtdrill.Visible = False
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From chkdrldwn_CheckedChanged: " + ex.Message, "DASHBOARD SETTING SERVICES")
        End Try
        
    End Sub
    Sub clearall()
        txtdrill.Text = ""
        chkdrldwn.Checked = False
        chkpage.Checked = False
        chkCorporate.Checked = False
        txtdshbrdnm.Text = ""
        ddltype.ClearSelection()
        lstModule.ClearSelection()
        listCategory.ClearSelection()
    End Sub

    Private Sub FILLDASHBOARDREPORT()
        'FILL GRADES IN GRIDVIEW - IN VIEW AND FINANCE PAGE
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[GET_DASHBOARD_DETAILS]")
            Dim dt As DataTable = DS.Tables(0)
            ViewState("tblDashboard") = dt
            gvDashboardRslt.DataSource = dt
            gvDashboardRslt.DataBind()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From FILLDASHBOARDREPORT: " + ex.Message, "DASHBOARD SETTING SERVICES")
        End Try
    End Sub
   
    Protected Sub gvDashboardRslt_RowCommand(sender As Object, e As GridViewCommandEventArgs)

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim selectedRow As GridViewRow = DirectCast(gvDashboardRslt.Rows(index), GridViewRow)
        Dim lblid As Label = selectedRow.FindControl("lblid")
        Dim lbDrill As Label = selectedRow.FindControl("lbDrill")
        idvalue.Value = Convert.ToInt64(lblid.Text)
        If (e.CommandName = "updating") Then
            btnSave.Text = "Update Dashboard"
            Dim dt As DataTable = ViewState("tblDashboard")
            Dim foundRows() As Data.DataRow
            foundRows = dt.Select("ID = " + lblid.Text)
            'assigning values
            txtdshbrdnm.Text = foundRows(0)("NAME")
            If (foundRows(0)("ISDRILL") = "Yes") Then
                chkdrldwn.Checked = True
                txtdrill.Text = foundRows(0)("DRILLPAGE")
            End If
            If (foundRows(0)("ISPAGE") = "Yes") Then
                chkpage.Checked = True
                txtdrill.Text = foundRows(0)("DRILLPAGE")
            End If
            If (foundRows(0)("ISCORP") = "Yes") Then
                chkCorporate.Checked = True

            End If
            ddltype.ClearSelection()
            ddltype.Items.FindByText(foundRows(0)("TYPE")).Selected = True
            Dim MODULES As String = foundRows(0)("MODULE")
            Dim Category As String = foundRows(0)("CATEGORY")

            Dim modulearray As String() = MODULES.Split("|")
            For Each mdl In modulearray
                lstModule.ClearSelection()
                lstModule.Items.FindByValue(mdl).Selected = True
            Next
            Dim categoryarray As String() = Category.Split(",")
            For Each ctr In categoryarray
                listCategory.ClearSelection()
                listCategory.Items.FindByText(ctr).Selected = True
            Next
        ElseIf (e.CommandName = "deleting") Then
            Dim strans As SqlTransaction = Nothing
            Try
                ShowMessage("", True)
                Dim qry As String = "[DBO].[UPDATE_DASHBOARD_DETAILS]"
                Dim pParams(9) As SqlParameter
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim objConn As New SqlConnection(str_conn)
                Dim Categorys As String = ""
                Dim modules As String = ""
                ' Dim strans As SqlTransaction = Nothing
                objConn.Open()
                strans = objConn.BeginTransaction
                pParams(0) = Mainclass.CreateSqlParameter("@DBRD_NAME", "", SqlDbType.VarChar)
                pParams(1) = Mainclass.CreateSqlParameter("@DBRD_CATEGRY", Categorys, SqlDbType.VarChar)
                pParams(2) = Mainclass.CreateSqlParameter("@DBRD_TYPE", "", SqlDbType.VarChar)
                pParams(3) = Mainclass.CreateSqlParameter("@DBRD_MODULE", modules, SqlDbType.VarChar)
                pParams(4) = Mainclass.CreateSqlParameter("@DBRD_DRILLPAGE", "", SqlDbType.VarChar)
                pParams(5) = Mainclass.CreateSqlParameter("@DBRD_ISDRILL", chkdrldwn.Checked, SqlDbType.Bit)
                pParams(6) = Mainclass.CreateSqlParameter("@OPTION", "D", SqlDbType.Char)
                pParams(7) = Mainclass.CreateSqlParameter("@DBRD_ID", GetDoubleVal(lblid.Text), SqlDbType.BigInt)
                pParams(8) = Mainclass.CreateSqlParameter("@DBRD_ISCORP", chkCorporate.Checked, SqlDbType.Bit)
                pParams(9) = Mainclass.CreateSqlParameter("@DBRD_ISREDIRECT", chkpage.Checked, SqlDbType.Bit)
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                strans.Commit()
                FILLDASHBOARDREPORT()
                ShowMessage("Dashboard Deleted Successfully", False)
            Catch ex As Exception
                strans.Rollback()
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From gvDashboardRslt_RowCommand(Delete): " + ex.Message, "DASHBOARD SETTING SERVICES")
            End Try
        ElseIf (e.CommandName = "drill") Then
            If lbDrill.Text = "Yes" Then
                divDrill.Visible = True
                Session("DBM_ID") = idvalue.Value
                cleardrill()
                FILLDRILLDOWN(idvalue.Value)
            Else
                divDrill.Visible = False
            End If


        Else
        End If
    End Sub

    Protected Sub gvDashboardRslt_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvDashboardRslt.PageIndex = e.NewPageIndex
        gvDashboardRslt.DataSource = ViewState("tblDashboard")
        gvDashboardRslt.DataBind()
    End Sub
    Protected Sub UpdateDetails(ByVal idval As Int64)
        Dim strans As SqlTransaction = Nothing
        Try
            If (txtdshbrdnm.Text = "") Then
                ShowMessage("Please enter dashboard name", True)
            ElseIf (listCategory.SelectedIndex = -1) Then
                ShowMessage("Please select category", True)
            ElseIf (lstModule.SelectedIndex = -1) Then
                ShowMessage("Please select atleast one module", True)
            ElseIf (ddltype.SelectedItem.Text = "SELECT") Then
                ShowMessage("Please select type", True)
            ElseIf chkdrldwn.Checked And txtdrill.Text = "" Then
                ShowMessage("Please enter drill down page name", True)
            Else
                ShowMessage("", True)
                Dim qry As String = "[DBO].[UPDATE_DASHBOARD_DETAILS]"
                Dim pParams(9) As SqlParameter
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim objConn As New SqlConnection(str_conn)
                Dim Categorys As String = ""
                For Each li As ListItem In listCategory.Items
                    If (li.Selected) Then
                        If Categorys = "" Then
                            Categorys = li.Text
                        Else
                            Categorys = Categorys + "," + li.Text
                        End If
                    End If
                Next
                Dim modules As String = ""
                For Each li As ListItem In lstModule.Items
                    If (li.Selected) Then
                        If modules = "" Then
                            modules = li.Value
                        Else
                            modules = modules + "|" + li.Value
                        End If
                    End If
                Next
                objConn.Open()
                strans = objConn.BeginTransaction
                pParams(0) = Mainclass.CreateSqlParameter("@DBRD_NAME", txtdshbrdnm.Text, SqlDbType.VarChar)
                pParams(1) = Mainclass.CreateSqlParameter("@DBRD_CATEGRY", Categorys, SqlDbType.VarChar)
                pParams(2) = Mainclass.CreateSqlParameter("@DBRD_TYPE", ddltype.SelectedItem.Text.ToString, SqlDbType.VarChar)
                pParams(3) = Mainclass.CreateSqlParameter("@DBRD_MODULE", modules, SqlDbType.VarChar)
                pParams(4) = Mainclass.CreateSqlParameter("@DBRD_DRILLPAGE", txtdrill.Text, SqlDbType.VarChar)
                pParams(5) = Mainclass.CreateSqlParameter("@DBRD_ISDRILL", chkdrldwn.Checked, SqlDbType.Bit)
                pParams(6) = Mainclass.CreateSqlParameter("@OPTION", "U", SqlDbType.Char)
                pParams(7) = Mainclass.CreateSqlParameter("@DBRD_ID", idval, SqlDbType.BigInt)
                pParams(8) = Mainclass.CreateSqlParameter("@DBRD_ISCORP", chkCorporate.Checked, SqlDbType.Bit)
                pParams(9) = Mainclass.CreateSqlParameter("@DBRD_ISREDIRECT", chkpage.Checked, SqlDbType.Bit)
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                strans.Commit()
                FILLDASHBOARDREPORT()
                clearall()
                btnSave.Text = "Add Dashboard"
                ShowMessage("Dashboard Details Updated Successfully", False)
            End If
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From gvDashboardRslt_RowCommand(Update): " + ex.Message, "DASHBOARD SETTING SERVICES")
        End Try
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If (btnSave.Text = "Add Dashboard") Then
            Dim strans As SqlTransaction = Nothing
            Try
                If (txtdshbrdnm.Text = "") Then
                    ShowMessage("Please enter dashboard name", True)
                ElseIf (listCategory.SelectedIndex = -1) Then
                    ShowMessage("Please select category", True)
                ElseIf (lstModule.SelectedIndex = -1) Then
                    ShowMessage("Please select atleast one module", True)
                ElseIf (ddltype.Items.FindByText("SELECT").Selected = True) Then
                    ShowMessage("Please select type", True)
                ElseIf chkdrldwn.Checked And txtdrill.Text = "" Then
                    ShowMessage("Please enter drill down page name", True)
                Else
                    ShowMessage("", True)
                    Dim qry As String = "[DBO].[UPDATE_DASHBOARD_DETAILS]"
                    Dim pParams(9) As SqlParameter
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim objConn As New SqlConnection(str_conn)
                    Dim Categorys As String = ""
                    For Each li As ListItem In listCategory.Items
                        If (li.Selected) Then
                            If Categorys = "" Then
                                Categorys = li.Text
                            Else
                                Categorys = Categorys + "," + li.Text
                            End If
                        End If
                    Next
                    Dim modules As String = ""
                    For Each li As ListItem In lstModule.Items
                        If (li.Selected) Then
                            If modules = "" Then
                                modules = li.Value
                            Else
                                modules = modules + "|" + li.Value
                            End If
                        End If
                    Next
                    

                    objConn.Open()
                    strans = objConn.BeginTransaction
                    pParams(0) = Mainclass.CreateSqlParameter("@DBRD_NAME", txtdshbrdnm.Text, SqlDbType.VarChar)
                    pParams(1) = Mainclass.CreateSqlParameter("@DBRD_CATEGRY", Categorys, SqlDbType.VarChar)
                    pParams(2) = Mainclass.CreateSqlParameter("@DBRD_TYPE", ddltype.SelectedItem.Text.ToString, SqlDbType.VarChar)
                    pParams(3) = Mainclass.CreateSqlParameter("@DBRD_MODULE", modules, SqlDbType.VarChar)
                    pParams(4) = Mainclass.CreateSqlParameter("@DBRD_DRILLPAGE", txtdrill.Text, SqlDbType.VarChar)
                    pParams(5) = Mainclass.CreateSqlParameter("@DBRD_ISDRILL", chkdrldwn.Checked, SqlDbType.Bit)
                    pParams(6) = Mainclass.CreateSqlParameter("@OPTION", "I", SqlDbType.Char)
                    pParams(7) = Mainclass.CreateSqlParameter("@DBRD_ID", 0, SqlDbType.BigInt)
                    pParams(8) = Mainclass.CreateSqlParameter("@DBRD_ISCORP", chkCorporate.Checked, SqlDbType.Bit)
                    pParams(9) = Mainclass.CreateSqlParameter("@DBRD_ISREDIRECT", chkpage.Checked, SqlDbType.Bit)
                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                    strans.Commit()
                    FILLDASHBOARDREPORT()
                    clearall()
                    ShowMessage("New Dashboard Details Inserted Successfully", False)
                End If
            Catch ex As Exception
                strans.Rollback()
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "DASHBOARD SETTING SERVICES")
            End Try
        ElseIf (btnSave.Text = "Update Dashboard") Then
            UpdateDetails(idvalue.Value)
        Else

        End If        
    End Sub

    Protected Sub POPULATEROLE()        
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim DSROLE As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[GET_ROLE_ID]")
        ddlusrrole.DataSource = DSROLE.Tables(0)
        ddlusrrole.DataTextField = "Rol_Descr"
        ddlusrrole.DataValueField = "rol_id"
        ddlusrrole.DataBind()
    End Sub
    Function loaddropdownlist() As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedValue, SqlDbType.BigInt)
        param(1) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedValue, SqlDbType.VarChar)
        param(2) = Mainclass.CreateSqlParameter("@CATGRY", rdCatgry.SelectedValue, SqlDbType.VarChar)
        Dim dsActivityTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[GET_NEWTIER_LOAD]", param)
        Return dsActivityTypes
    End Function

    Protected Sub rdCatgry_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (rdCatgry.SelectedItem.Value = "M") Then
            Dim DS1 As DataSet = loaddropdownlist()
            If (DS1.Tables(0).Rows.Count >= 3) Then
                rowbtn.Visible = True
                rowM.Visible = True
                ddl13.Visible = True
                ddl12.Visible = True
                '1st
                ddl11.DataSource = DS1.Tables(0)
                ddl11.DataTextField = "DBM_NAME"
                ddl11.DataValueField = "DBM_ID"
                ddl11.DataBind()
                '2ND
                ddl12.DataSource = DS1.Tables(0)
                ddl12.DataTextField = "DBM_NAME"
                ddl12.DataValueField = "DBM_ID"
                ddl12.DataBind()
                '3RD
                ddl13.DataSource = DS1.Tables(0)
                ddl13.DataTextField = "DBM_NAME"
                ddl13.DataValueField = "DBM_ID"
                ddl13.DataBind()

            Else
                ShowMessage("Required number of M dashboards are not available.", True)
            End If

        ElseIf (rdCatgry.SelectedItem.Value = "L") Then
            Dim DS1 As DataSet = loaddropdownlist()
            If (DS1.Tables(0).Rows.Count >= 2) Then
                rowbtn.Visible = True
                rowM.Visible = True
                ddl12.Visible = True
                '1st
                ddl11.DataSource = DS1.Tables(0)
                ddl11.DataTextField = "DBM_NAME"
                ddl11.DataValueField = "DBM_ID"
                ddl11.DataBind()
                '2ND
                ddl12.DataSource = DS1.Tables(0)
                ddl12.DataTextField = "DBM_NAME"
                ddl12.DataValueField = "DBM_ID"
                ddl12.DataBind()
                '3rd
                ddl13.Visible = False

            Else
                ShowMessage("Required number of L dashboards are not available.", True)
            End If

        ElseIf (rdCatgry.SelectedItem.Value = "XL") Then
            Dim DS1 As DataSet = loaddropdownlist()
            If (DS1.Tables(0).Rows.Count >= 1) Then
                rowbtn.Visible = True
                rowM.Visible = True
                '1st
                ddl11.DataSource = DS1.Tables(0)
                ddl11.DataTextField = "DBM_NAME"
                ddl11.DataValueField = "DBM_ID"
                ddl11.DataBind()
                '3rd
                ddl13.Visible = False
                '2nd
                ddl12.Visible = False

            Else
                ShowMessage("Required number of XL dashboards are not available.", True)
            End If
        Else
            'do nothing
        End If
    End Sub
    Sub DisplayActions()
        clearmanage()
        bindrepeater_tier()       
        POPULATEddlRemoveTier()
        PopulateddlSlctTiernm()
    End Sub
    Sub clearmanage()
        ShowMessage("", True)
        rdCatgry.ClearSelection()
        rowbtn.Visible = False
        rowM.Visible = False
    End Sub

    Protected Sub ddlmdl_SelectedIndexChanged(sender As Object, e As EventArgs)
        DisplayActions()
    End Sub

    Protected Sub ddlusrrole_SelectedIndexChanged(sender As Object, e As EventArgs)
        DisplayActions()
    End Sub
    Sub bindrepeater_tier()        
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
        param(1) = Mainclass.CreateSqlParameter("@MODULE", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
        Dim dstierdetails As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[GET_DASHBOARD_TIER_DETAILS]", param)
        ViewState("TierDetails") = dstierdetails.Tables(0)
        rptrtier.DataSource = dstierdetails.Tables(0)
        rptrtier.DataBind()
    End Sub
    Protected Sub rptrtier_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Dim lblName As Label = TryCast(e.Item.FindControl("lblName"), Label)
        If lblName.Text <> "" Then                        
            Dim lblName1 As Label = TryCast(e.Item.FindControl("lblName1"), Label)
            Dim lblName2 As Label = TryCast(e.Item.FindControl("lblName2"), Label)
            Dim lblName3 As Label = TryCast(e.Item.FindControl("lblName3"), Label)
            Dim lblarry As String() = lblName.Text.Split(",")
            Dim counts As Integer = lblarry.Length
            If counts = 3 Then
                lblName1.Visible = True
                lblName1.Text = lblarry(0)
                lblName2.Visible = True
                lblName2.Text = lblarry(1)
                lblName3.Visible = True
                lblName3.Text = lblarry(2)
            ElseIf counts = 2 Then
                lblName1.Visible = True
                lblName1.Text = lblarry(0)
                lblName1.Width = 614
                lblName2.Visible = True
                lblName2.Text = lblarry(1)
                lblName2.Width = 614
                lblName3.Visible = False
            ElseIf counts = 1 Then
                lblName1.Visible = True
                lblName1.Text = lblarry(0)
                lblName1.Width = 1230
                lblName2.Visible = False
                lblName3.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnAdd_Click1(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            If (rdCatgry.SelectedItem.Value = "M") And (ddl11.SelectedValue = ddl12.SelectedValue Or ddl11.SelectedValue = ddl13.SelectedValue Or ddl12.SelectedValue = ddl13.SelectedValue) Then
                ShowMessage("Please select different DASHBOARD values", True)
                Exit Sub
            ElseIf (rdCatgry.SelectedItem.Value = "L") And (ddl11.SelectedValue = ddl12.SelectedValue) Then
                ShowMessage("Please select different DASHBOARD values", True)
                Exit Sub
            Else
                Dim param(1) As SqlParameter
                param(0) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
                param(1) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
                Dim dstierdetails As Integer = SqlHelper.ExecuteScalar(str_conn, "[DBO].[GET_MAX_TIER]", param)


                Dim dbmidarray() As Integer = {0}
                If (rdCatgry.SelectedItem.Value = "M") Then
                    dbmidarray = {(ddl11.SelectedValue), (ddl12.SelectedValue), (ddl13.SelectedValue)}
                ElseIf (rdCatgry.SelectedItem.Value = "L") Then
                    dbmidarray = {(ddl11.SelectedValue), (ddl12.SelectedValue)}
                ElseIf (rdCatgry.SelectedItem.Value = "XL") Then
                    dbmidarray = {(ddl11.SelectedValue)}
                End If
                ' Dim Categorys As String = ""
                '  Dim modules As String = ""
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                If (dbmidarray.Length > 0) Then
                    For i = 1 To dbmidarray.Length
                        Dim Params(7) As SqlParameter
                        Params(0) = Mainclass.CreateSqlParameter("@DBMID", dbmidarray(i - 1), SqlDbType.BigInt)
                        Params(1) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
                        Params(2) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
                        Params(3) = Mainclass.CreateSqlParameter("@CAT", rdCatgry.SelectedValue, SqlDbType.VarChar)
                        Params(4) = Mainclass.CreateSqlParameter("@TIER", dstierdetails + 1, SqlDbType.BigInt)
                        Params(5) = Mainclass.CreateSqlParameter("@ORDER", i, SqlDbType.BigInt)
                        Params(6) = Mainclass.CreateSqlParameter("@OPTION", "I", SqlDbType.Char)
                        Params(7) = Mainclass.CreateSqlParameter("@DBSID", 0, SqlDbType.BigInt)
                        SqlHelper.ExecuteNonQuery(strans, "[DBO].[INSERTUPDATE_MAX_TIER]", Params)
                    Next
                    strans.Commit()
                End If
            End If
            DisplayActions()
            ShowMessage("Tier Added Successfully", True)
        Catch ex As Exception
            strans.Rollback()
        End Try
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim param(3) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
            param(2) = Mainclass.CreateSqlParameter("@OPTION", "D", SqlDbType.Char)
            param(3) = Mainclass.CreateSqlParameter("@TIER", ddlRemoveTier.SelectedValue, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, "[DBO].[EDIT_MODIFY_TIER]", param)
            strans.Commit()
            DisplayActions()
            ShowMessage("Tier got deleted", True)
        Catch ex As Exception
            strans.Rollback()
        End Try

    End Sub

    Protected Sub btnDisplay_Click(sender As Object, e As EventArgs)
        Try
            rowDshbrd.Visible = True
            rowupdbtn.Visible = True
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(3) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
            param(2) = Mainclass.CreateSqlParameter("@OPTION", "E", SqlDbType.Char)
            param(3) = Mainclass.CreateSqlParameter("@TIER", ddlSlctTiernm.SelectedValue, SqlDbType.BigInt)
            Dim dsTier As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[EDIT_MODIFY_TIER]", param)
            Dim dtTier1 As DataTable = dsTier.Tables(0)
            Dim dtTier2 As DataTable = dsTier.Tables(1)
            ViewState("DBSTable") = dtTier2
            Dim dtTier3 As DataTable = dsTier.Tables(2)
            Dim Category As String = dsTier.Tables(0).Rows(0)("CTGRY")
            ViewState("Category") = Category
            If (Category = "M") Then
                ddl22.Visible = True
                ddl21.Visible = True
                ddl21.DataSource = dtTier3
                ddl21.DataTextField = "DBM_NAME"
                ddl21.DataValueField = "DBM_ID"
                ddl21.DataBind()
                ddl21.Items.FindByText(dtTier2.Rows(0)("DBM_NAME")).Selected = True
                ddl22.DataSource = dtTier3
                ddl22.DataTextField = "DBM_NAME"
                ddl22.DataValueField = "DBM_ID"
                ddl22.DataBind()
                ddl22.Items.FindByText(dtTier2.Rows(1)("DBM_NAME")).Selected = True
                ddl23.DataSource = dtTier3
                ddl23.DataTextField = "DBM_NAME"
                ddl23.DataValueField = "DBM_ID"
                ddl23.DataBind()
                ddl23.Items.FindByText(dtTier2.Rows(2)("DBM_NAME")).Selected = True
            ElseIf (Category = "L") Then
                ddl23.Visible = False
                ddl22.Visible = True
                ddl21.DataSource = dtTier3
                ddl21.DataTextField = "DBM_NAME"
                ddl21.DataValueField = "DBM_ID"
                ddl21.DataBind()
                ddl21.Items.FindByText(dtTier2.Rows(0)("DBM_NAME")).Selected = True
                ddl22.DataSource = dtTier3
                ddl22.DataTextField = "DBM_NAME"
                ddl22.DataValueField = "DBM_ID"
                ddl22.DataBind()
                ddl22.Items.FindByText(dtTier2.Rows(1)("DBM_NAME")).Selected = True
            ElseIf (Category = "XL") Then
                ddl23.Visible = False
                ddl22.Visible = False
                ddl21.DataSource = dtTier3
                ddl21.DataTextField = "DBM_NAME"
                ddl21.DataValueField = "DBM_ID"
                ddl21.DataBind()
                ddl21.Items.FindByText(dtTier2.Rows(0)("DBM_NAME")).Selected = True
            End If
        Catch ex As Exception

        End Try        
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim Dts As DataTable = ViewState("DBSTable")
            Dim Category As String = ViewState("Category")

            If (Category = "M") And (ddl21.SelectedValue = ddl22.SelectedValue Or ddl21.SelectedValue = ddl23.SelectedValue Or ddl22.SelectedValue = ddl23.SelectedValue) Then
                ShowMessage("Please select different DASHBOARD values", True)
                Exit Sub
            ElseIf (Category = "L") And (ddl21.SelectedValue = ddl22.SelectedValue) Then
                ShowMessage("Please select different DASHBOARD values", True)
                Exit Sub
            Else
                Dim dbmidarray() As Integer = {0}
                If (Category = "M") Then
                    dbmidarray = {(ddl21.SelectedValue), (ddl22.SelectedValue), (ddl23.SelectedValue)}
                ElseIf (Category = "L") Then
                    dbmidarray = {(ddl21.SelectedValue), (ddl22.SelectedValue)}
                ElseIf (Category = "XL") Then
                    dbmidarray = {(ddl21.SelectedValue)}
                End If

                Dim dbsarray() As Integer = {0}
                If (Category = "M") Then
                    dbsarray = {Dts.Rows(0)("DBS_ID"), Dts.Rows(1)("DBS_ID"), Dts.Rows(2)("DBS_ID")}
                ElseIf (Category = "L") Then
                    dbsarray = {Dts.Rows(0)("DBS_ID"), Dts.Rows(1)("DBS_ID")}
                ElseIf (Category = "XL") Then
                    dbsarray = {Dts.Rows(0)("DBS_ID")}
                End If
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                If (dbmidarray.Length > 0) Then
                    For i = 1 To dbmidarray.Length
                        Dim Params(7) As SqlParameter
                        Params(0) = Mainclass.CreateSqlParameter("@DBMID", dbmidarray(i - 1), SqlDbType.BigInt)
                        Params(1) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
                        Params(2) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
                        Params(3) = Mainclass.CreateSqlParameter("@CAT", Category, SqlDbType.VarChar)
                        Params(4) = Mainclass.CreateSqlParameter("@TIER", ddlSlctTiernm.SelectedValue, SqlDbType.BigInt)
                        Params(5) = Mainclass.CreateSqlParameter("@ORDER", i, SqlDbType.BigInt)
                        Params(6) = Mainclass.CreateSqlParameter("@OPTION", "U", SqlDbType.Char)
                        Params(7) = Mainclass.CreateSqlParameter("@DBSID", dbsarray(i - 1), SqlDbType.BigInt)
                        SqlHelper.ExecuteNonQuery(strans, "[DBO].[INSERTUPDATE_MAX_TIER]", Params)
                    Next
                    strans.Commit()
                    DisplayActions()
                    ShowMessage("Tier GOT UPDATED", False)
                End If
            End If
        Catch ex As Exception
            strans.Rollback()
        End Try

    End Sub

    Protected Sub POPULATEddlRemoveTier()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(3) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
            param(2) = Mainclass.CreateSqlParameter("@OPTION", "S", SqlDbType.Char)
            param(3) = Mainclass.CreateSqlParameter("@TIER", 0, SqlDbType.BigInt)
            Dim dsTier As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[EDIT_MODIFY_TIER]", param)
            ddlRemoveTier.DataSource = dsTier.Tables(0)
            ddlRemoveTier.DataTextField = "DBS_TIER"
            ddlRemoveTier.DataValueField = "DBS_TIER"
            ddlRemoveTier.DataBind()
        Catch ex As Exception

        End Try        
    End Sub
    Protected Sub PopulateddlSlctTiernm()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(3) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@ROLID", ddlusrrole.SelectedItem.Value, SqlDbType.BigInt)
            param(1) = Mainclass.CreateSqlParameter("@MDL", ddlmdl.SelectedItem.Value, SqlDbType.VarChar)
            param(2) = Mainclass.CreateSqlParameter("@OPTION", "S", SqlDbType.Char)
            param(3) = Mainclass.CreateSqlParameter("@TIER", 0, SqlDbType.BigInt)
            Dim dsTier As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[EDIT_MODIFY_TIER]", param)
            ddlSlctTiernm.DataSource = dsTier.Tables(0)
            ddlSlctTiernm.DataTextField = "DBS_TIER"
            ddlSlctTiernm.DataValueField = "DBS_TIER"
            ddlSlctTiernm.DataBind()
        Catch ex As Exception

        End Try        
    End Sub

    Protected Sub btClose_Click(sender As Object, e As EventArgs) Handles btClose.Click
        divDrill.Visible = False
    End Sub

    Protected Sub btnUClose_Click(sender As Object, e As EventArgs) Handles btnUClose.Click
        divDrill.Visible = False
    End Sub

    Protected Sub ddlDrillType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDrillType.SelectedIndexChanged
        If ddlDrillType.SelectedItem.Text = "Page" Then
            trURL.Visible = True
        Else
            trURL.Visible = False
        End If
    End Sub

    Private Sub FILLDRILLDOWN(ByVal id As String)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(0) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@DBM_ID", id, SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[DBO].[get_drilldownpage_dets]", param)
            Dim dt As DataTable = DS.Tables(0)
            grdDrillPage.DataSource = dt
            grdDrillPage.DataBind()
            ViewState("tblDashboarddrill") = dt
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From FILLDASHBOARDREPORT: " + ex.Message, "DASHBOARD SETTING SERVICES")
        End Try
    End Sub

    Protected Sub btnDrill_Click(sender As Object, e As EventArgs) Handles btnDrill.Click
        If (btnDrill.Text = "Add") Then
            Dim strans As SqlTransaction = Nothing
            Try
                If (txtDrillName.Text = "") Then
                    ShowMessage("Please enter dashboard name", True)
                ElseIf (ddlParams.SelectedIndex = -1) Then
                    ShowMessage("Please select Params", True)
                ElseIf (ddlDrillType.Items.FindByText("--").Selected = True) Then
                    ShowMessage("Please select type", True)
                Else
                    ShowMessage("", True)
                    Dim qry As String = "[DBO].[UPDATE_DASHBOARD_DRILL_DETAILS]"
                    Dim pParams(8) As SqlParameter
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim objConn As New SqlConnection(str_conn)
                    Dim Categorys As String = ""
                    For Each li As ListItem In ddlParams.Items
                        If (li.Selected) Then
                            If Categorys = "" Then
                                Categorys = li.Text
                            Else
                                Categorys = Categorys + "," + li.Text
                            End If
                        End If
                    Next



                    objConn.Open()
                    strans = objConn.BeginTransaction
                    pParams(0) = Mainclass.CreateSqlParameter("@DBRD_NAME", txtDrillName.Text, SqlDbType.VarChar)
                    pParams(1) = Mainclass.CreateSqlParameter("@DBRD_PARAMS", Categorys, SqlDbType.VarChar)
                    pParams(2) = Mainclass.CreateSqlParameter("@DBRD_TYPE", ddlDrillType.SelectedItem.Text.ToString, SqlDbType.VarChar)
                    pParams(3) = Mainclass.CreateSqlParameter("@DBRD_NEXT", ddlNextparam.SelectedItem.Text.ToString, SqlDbType.VarChar)
                    pParams(4) = Mainclass.CreateSqlParameter("@DBRD_URL", txtdrill.Text, SqlDbType.VarChar)
                    pParams(5) = Mainclass.CreateSqlParameter("@DBRD_DBM_ID", Session("DBM_ID"), SqlDbType.BigInt)
                    pParams(6) = Mainclass.CreateSqlParameter("@DBRD_ORDER", txtOrder.Text, SqlDbType.VarChar)
                    pParams(7) = Mainclass.CreateSqlParameter("@OPTION", "I", SqlDbType.Char)
                    pParams(8) = Mainclass.CreateSqlParameter("@DBRD_ID", 0, SqlDbType.BigInt)
                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                    strans.Commit()
                    FILLDRILLDOWN(Session("DBM_ID"))
                    cleardrill()
                    ShowMessage("New Dashboard Details Inserted Successfully", False)
                End If
            Catch ex As Exception
                strans.Rollback()
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "DASHBOARD SETTING SERVICES")
            End Try
        ElseIf (btnSave.Text = "Update") Then
            updatedrill(idvalue.Value)
        Else

        End If
    End Sub
    Sub updatedrill(ByVal id As String)

        Dim strans As SqlTransaction = Nothing
        Try
            If (txtDrillName.Text = "") Then
                ShowMessage("Please enter dashboard name", True)
            ElseIf (ddlParams.SelectedIndex = -1) Then
                ShowMessage("Please select Params", True)
            ElseIf (ddlDrillType.Items.FindByText("--").Selected = True) Then
                ShowMessage("Please select type", True)
            Else
                ShowMessage("", True)
                Dim qry As String = "[DBO].[UPDATE_DASHBOARD_DRILL_DETAILS]"
                Dim pParams(8) As SqlParameter
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim objConn As New SqlConnection(str_conn)
                Dim Categorys As String = ""
                For Each li As ListItem In ddlParams.Items
                    If (li.Selected) Then
                        If Categorys = "" Then
                            Categorys = li.Text
                        Else
                            Categorys = Categorys + "," + li.Text
                        End If
                    End If
                Next



                objConn.Open()
                strans = objConn.BeginTransaction
                pParams(0) = Mainclass.CreateSqlParameter("@DBRD_NAME", txtDrillName.Text, SqlDbType.VarChar)
                pParams(1) = Mainclass.CreateSqlParameter("@DBRD_PARAMS", Categorys, SqlDbType.VarChar)
                pParams(2) = Mainclass.CreateSqlParameter("@DBRD_TYPE", ddlDrillType.SelectedItem.Text.ToString, SqlDbType.VarChar)
                pParams(3) = Mainclass.CreateSqlParameter("@DBRD_NEXT", ddlNextparam.SelectedItem.Text.ToString, SqlDbType.VarChar)
                pParams(4) = Mainclass.CreateSqlParameter("@DBRD_URL", txtdrill.Text, SqlDbType.VarChar)
                pParams(5) = Mainclass.CreateSqlParameter("@DBRD_DBM_ID", Session("DBM_ID"), SqlDbType.BigInt)
                pParams(6) = Mainclass.CreateSqlParameter("@DBRD_ORDER", txtOrder.Text, SqlDbType.VarChar)
                pParams(7) = Mainclass.CreateSqlParameter("@OPTION", "U", SqlDbType.Char)
                pParams(8) = Mainclass.CreateSqlParameter("@DBRD_ID", id, SqlDbType.BigInt)
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                strans.Commit()
                FILLDRILLDOWN(Session("DBM_ID"))
                cleardrill()
                ShowMessage("New Dashboard Details Inserted Successfully", False)
            End If
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "DASHBOARD SETTING SERVICES")
        End Try
        


    End Sub
    Sub cleardrill()
        txtOrder.Text = ""
        txtDrillName.Text = ""
        txtURL.Text = ""
        ddlDrillType.ClearSelection()
        ddlParams.ClearSelection()
        ddlNextparam.ClearSelection()
    End Sub

    Protected Sub grdDrillPage_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrillPage.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim selectedRow As GridViewRow = DirectCast(grdDrillPage.Rows(index), GridViewRow)
        Dim lblid As Label = selectedRow.FindControl("lblid")

        idvalue.Value = Convert.ToInt64(lblid.Text)
        If (e.CommandName = "updating") Then
            btnDrill.Text = "Update"
            Dim dt As DataTable = ViewState("tblDashboarddrill")
            Dim foundRows() As Data.DataRow
            foundRows = dt.Select("DBD_ID = " + lblid.Text)
            'assigning values
            txtDrillName.Text = foundRows(0)("DBD_NAME")
           
            txtOrder.Text = foundRows(0)("DBD_ORDER")
            txtURL.Text = foundRows(0)("DBD_URL")

            ddlDrillType.ClearSelection()
            ddlDrillType.Items.FindByText(foundRows(0)("DBD_TYPE")).Selected = True

            ddlNextparam.ClearSelection()
            ddlNextparam.Items.FindByText(foundRows(0)("DBD_NEXT_PARAM")).Selected = True

            Dim Category As String = foundRows(0)("DBD_PARAMS")

            
            Dim categoryarray As String() = Category.Split(",")
            For Each ctr In categoryarray
                ddlParams.ClearSelection()
                ddlParams.Items.FindByText(ctr).Selected = True
            Next
        ElseIf (e.CommandName = "deleting") Then
            Dim strans As SqlTransaction = Nothing
            Try
                ShowMessage("", True)
                Dim qry As String = "[DBO].[UPDATE_DASHBOARD_DRILL_DETAILS]"
                Dim pParams(8) As SqlParameter
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim objConn As New SqlConnection(str_conn)
                Dim Categorys As String = ""
                Dim modules As String = ""
                ' Dim strans As SqlTransaction = Nothing
                objConn.Open()
                strans = objConn.BeginTransaction
                pParams(0) = Mainclass.CreateSqlParameter("@DBRD_NAME", txtDrillName.Text, SqlDbType.VarChar)
                pParams(1) = Mainclass.CreateSqlParameter("@DBRD_PARAMS", Categorys, SqlDbType.VarChar)
                pParams(2) = Mainclass.CreateSqlParameter("@DBRD_TYPE", ddlDrillType.SelectedItem.Text.ToString, SqlDbType.VarChar)
                pParams(3) = Mainclass.CreateSqlParameter("@DBRD_NEXT", ddlNextparam.SelectedItem.Text.ToString, SqlDbType.VarChar)
                pParams(4) = Mainclass.CreateSqlParameter("@DBRD_URL", txtdrill.Text, SqlDbType.VarChar)
                pParams(5) = Mainclass.CreateSqlParameter("@DBRD_DBM_ID", Session("DBM_ID"), SqlDbType.BigInt)
                pParams(6) = Mainclass.CreateSqlParameter("@DBRD_ORDER", txtOrder.Text, SqlDbType.VarChar)
                pParams(7) = Mainclass.CreateSqlParameter("@OPTION", "D", SqlDbType.Char)
                pParams(8) = Mainclass.CreateSqlParameter("@DBRD_ID", idvalue.Value, SqlDbType.BigInt)
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                strans.Commit()
                FILLDRILLDOWN(Session("DBM_ID"))
                ShowMessage("Dashboard Deleted Successfully", False)
            Catch ex As Exception
                strans.Rollback()
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From gvDashboardRslt_RowCommand(Delete): " + ex.Message, "DASHBOARD SETTING SERVICES")
            End Try

        Else
        End If
    End Sub
End Class
