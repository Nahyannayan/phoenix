<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accccSetLastYearData.aspx.vb" Inherits="Accounts_SetLastYearData" Title="Untitled Page" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="SetLastYearData"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddBusinessunit" runat="server"
                                TabIndex="5" AutoPostBack="True" OnSelectedIndexChanged="ddBusinessunit_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Financial Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReports" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReports_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvAmountDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="RSS_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# BIND("RSS_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="RSS_TYP" Visible="False"></asp:BoundField>
                                    <asp:BoundField DataField="RSS_DESCR" HeaderText="Description" ItemStyle-Width="75%"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount" ItemStyle-Width="25%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" MaxLength="10"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button" />
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" CssClass="button" /></td>
                    </tr>

                </table>


                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_ID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                &nbsp;&nbsp;

            </div>
        </div>
    </div>
</asp:Content>




