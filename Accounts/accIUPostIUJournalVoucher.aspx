<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accIUPostIUJournalVoucher.aspx.vb" Inherits="iuPostJournalVoucher" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
        function fnVoucherMSg() {
            if (document.getElementById('<%=chkPrint.ClientID %>').checked == true) {
                var curr_elem;
                var countChecked;
                countChecked = 0;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];
                    if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0) && (curr_elem.name != '')) {
                        if (curr_elem.checked)
                            countChecked = countChecked + 1;
                    }
                }
                if (countChecked > 1)
                    return confirm('Only first voucher will be printed(Multiple vouchers are selected)');
                else
                    return true;
            }
        }
        Sys.Application.add_load(
               function CheckForPrint() {
                   if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                       document.getElementById('<%= h_print.ClientID %>').value = '';
                       radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
                   }
               }
                    );

               function autoSizeWithCalendar(oWindow) {
                   var iframe = oWindow.get_contentFrame();
                   var body = iframe.contentWindow.document.body;
                   var height = body.scrollHeight;
                   var width = body.scrollWidth;
                   var iframeBounds = $telerik.getBounds(iframe);
                   var heightDelta = height - iframeBounds.height;
                   var widthDelta = width - iframeBounds.width;
                   if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                   if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                   oWindow.center();
               }



    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Post Interunit Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <table width="100%" id="tbl" border="0">
                    <tr>
                        <td></td>
                        <td align="center" valign="top">
                            <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="False" Width="100%" DataKeyNames="GUID" AllowPaging="True" EmptyDataText="No Data Found">
                                <Columns>
                                    <asp:TemplateField SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No<br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("IJH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />

                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Doc Date
                                            <br />
                                            <asp:TextBox ID="txtDocDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp; &nbsp;<asp:Label ID="lblDate" runat="server" Text='<%# Bind("IJH_DOCDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Unit (DR)
                                            <br />
                                            <asp:TextBox ID="txtBankAC" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAC" runat="server" Text='<%# Bind("DR_BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Narration
                                            <br />
                                            <asp:TextBox ID="txtNarrn" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNarrn" runat="server" Text='<%# Bind("IJH_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Unit (CR)
                                            <br />
                                            <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodewww" runat="server" Text='<%# Bind("CR_BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IJH_bMIRROR" HeaderText="Mirror Entry" SortExpression="IJH_bMIRROR">
                                        <HeaderStyle Font-Size="8pt" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Differ Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("IJH_DIFFAMT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DEBIT">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("DEBITTOTAL")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CREDIT">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("CREDITTOTAL")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" type="checkbox" value='<%# Bind("GUID") %>' />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Post<input id="Checkbox2" onclick="fnSelectAll(this)" type="checkbox" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" CausesValidation="false" CommandName="View"
                                                OnClick="lbView_Click" Text="Summary"></asp:LinkButton>
                                            <asp:HyperLink ID="hlView"
                                                runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print Voucher" />
                            &nbsp;<asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" OnClientClick="return fnVoucherMSg();" /><br />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="center" valign="top">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="IJL_ID" EmptyDataText="NO TRANSACTION DETAILS ADDED YET" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="IJL_DOCNO" HeaderText="Document No" SortExpression="IJL_DOCNO" />
                                    <asp:BoundField DataField="IJL_NARRATION" HeaderText="Narration" SortExpression="IJL_NARRATION" />
                                    <asp:BoundField DataField="IJL_ACT_ID" HeaderText="Account No" SortExpression="IJL_ACT_ID" />
                                    <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" SortExpression="ACT_NAME" />
                                    <asp:TemplateField HeaderText="Debit">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("IJL_DEBIT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Credit">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("IJL_CREDIT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbViewChild" runat="server" CausesValidation="false" CommandName=""
                                                OnClick="lbViewChild_Click" Text="View"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="slno" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSlno" runat="server" Text='<%# Bind("IJL_SLNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">
                            <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="THERE IS NO ALLOCATION FOR CURRENTLY SELECTED ACCOUNT" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="VDS_ACT_ID" HeaderText="Account No" SortExpression="VDS_ACT_ID" />
                                    <asp:BoundField DataField="VDS_SLNO" HeaderText="Slno" SortExpression="VDS_SLNO"
                                        Visible="False" />
                                    <asp:BoundField DataField="CCS_DESCR" HeaderText="Cost Center" SortExpression="CCS_DESCR" />
                                    <asp:BoundField DataField="VDS_CODE" HeaderText="CODE" SortExpression="VDS_CODE" />
                                    <asp:BoundField DataField="VDS_DESCR" HeaderText="Name" SortExpression="VDS_DESCR" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VDS_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Debit/Credit" SortExpression="VDS_DRCR">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCrDb" runat="server" Text='<%# returnCrDb(Container.DataItem("VDS_DRCR")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" OnValueChanged="h_print_ValueChanged" />
                <input id="h_FirstVoucher" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>

