Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_acccrPostCashReceipt
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            h_Grid.Value = "top"
            Page.Title = OASISConstants.Gemstitle
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If 
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A150005" And MainMnu_code <> "A200005") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            '''''check menu rights
            gvChild.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
    End Sub
 

    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If
    End Function


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
     

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If chkPost IsNot Nothing Then
                If chkPost.Checked = True Then
                    chkPost.Disabled = True
                Else
                    If lblGUID IsNot Nothing Then
                        ' Dim i As New Encryption64
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                        'hlCEdit.NavigateUrl = "acccpCashPayments.aspx?editid=" & lblGUID.Text
                        hlview.NavigateUrl = "acccrCashReceipt.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                        'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                        hlCEdit.Enabled = True
                    End If
                End If
            End If

        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub Find_Checked(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    post_voucher(chk.Value.ToString)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
        Next
    End Sub


    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        gvChild.Visible = False
        gvDetails.Visible = False
        h_FirstVoucher.Value = ""
        Find_Checked(Me.Page)
        If h_FirstVoucher.Value <> "" And chkPrint.Checked Then
            PrintVoucher(h_FirstVoucher.Value)
        End If
        gridbind()
    End Sub


    Function post_voucher(ByVal p_guid As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim str_Sql As String
                str_Sql = "select * FROM VOUCHER_H where GUID='" & p_guid & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    Dim cmd As New SqlCommand("POSTVOUCHER", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("VHH_SUB_ID") & ""
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = "CR"
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    'Adding header info
                    cmd.Parameters.Clear()
                    'Adding transaction info
                    ' Dim str_err As String
                    Dim vSUB_ID As String = ds.Tables(0).Rows(0)("VHH_SUB_ID")
                    Dim vF_YEAR As Integer = ds.Tables(0).Rows(0)("VHH_FYEAR")
                    Dim vDOC_NO As String = ds.Tables(0).Rows(0)("VHH_DOCNO")
                    Dim vDOC_DATE As String = ds.Tables(0).Rows(0)("VHH_DOCDT")
                    Dim vVHH_CUR_ID As String = ds.Tables(0).Rows(0)("VHH_CUR_ID")
                    If iReturnvalue = 0 Then AccountsDetails.SaveMonthlyData_D("CR", "E", Session("sBSUID"), vDOC_NO, vSUB_ID, vVHH_CUR_ID, vF_YEAR, vDOC_DATE, objConn, stTrans)

                    If (iReturnvalue = 0) Then
                        stTrans.Commit()
                        If h_FirstVoucher.Value = "" Then
                            h_FirstVoucher.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                        End If
                    Else
                        stTrans.Rollback()
                    End If
                    If (iReturnvalue = 0) Then
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("VHH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = "Successfully Posted"
                    Else
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If ' ds.Tables(0).Rows(0)("ERR_MSG")
                Else

                End If

            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function


    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT VHH.VHH_REFNO, VHH.GUID, VHH.VHH_SUB_ID, VHH.VHH_BSU_ID," _
                & " VHH.VHH_FYEAR, VHH.VHH_DOCTYPE, VHH.VHH_DOCNO, VHH.VHH_TYPE," _
                & " VHH.VHH_DOCDT, VHH.VHH_CHQDT, VHH.VHH_CUR_ID, VHH.VHH_EXGRATE1," _
                & " VHH.VHH_EXGRATE2, VHH.VHH_NARRATION, VHH.VHH_bPOSTED, VHH.VHH_Count," _
                & " (SELECT     SUM(VHD_AMOUNT) AS TOTAL FROM VOUCHER_D WHERE  (VHD_SUB_ID = '" & Session("Sub_ID") & "')" _
                & " AND (VHD_BSU_ID = '" & Session("sBsuid") & "') AND (VHD_DOCTYPE = 'CR') " _
                & " AND (VHD_DOCNO = VHH.VHH_DOCNO)) AS TOTAL, VHH.VHH_bDELETED," _
                & " VHH.VHH_AMOUNT, VHH.VHH_COL_ID, COLLECTION_M.COL_DESCR" _
                & " FROM VOUCHER_H AS VHH INNER JOIN COLLECTION_M ON VHH.VHH_COL_ID = COLLECTION_M.COL_ID" _
                & " WHERE  (VHH.VHH_SUB_ID = '" & Session("Sub_ID") & "') " _
                & " AND (VHH.VHH_BSU_ID = '" & Session("sBsuid") & "') AND VHH.VHH_FYEAR = " & Session("F_YEAR") _
                & " AND (VHH.VHH_bDELETED = 0)  AND (VHH.VHH_DOCTYPE = 'CR')" _
                & " AND VHH.VHH_bPOSTED=0 ORDER BY VHH.VHH_DOCDT DESC, VHH.VHH_DOCNO DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count > 0 Then
                btnPost.Visible = True
            Else
                btnPost.Visible = False
            End If
            gvJournal.DataBind()
            gvJournal.SelectedIndex = p_selected_id
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvDetails.Visible = True
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim i As Integer = sender.parent.parent.RowIndex
                gridbind(i)
                gvDetails.SelectedIndex = -1
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                str_Sql = "select * FROM VOUCHER_H where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                h_Grid.Value = "detail"
                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT     VOUCHER_D.GUID, VOUCHER_D.VHD_SUB_ID, " _
                      & " VOUCHER_D.VHD_BSU_ID, VOUCHER_D.VHD_FYEAR," _
                      & " VOUCHER_D.VHD_DOCTYPE, VOUCHER_D.VHD_DOCNO," _
                      & " VOUCHER_D.VHD_LINEID, VOUCHER_D.VHD_ACT_ID," _
                      & " VOUCHER_D.VHD_AMOUNT, VOUCHER_D.VHD_NARRATION," _
                      & " (VOUCHER_D.VHD_ACT_ID+'-'+ACCOUNTS_M.ACT_NAME) AS ACCOUNT" _
                      & " FROM VOUCHER_D INNER JOIN ACCOUNTS_M" _
                      & " ON VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID" _
                      & " AND VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID" _
                      & " WHERE (VOUCHER_D.VHD_DOCNO = '" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "')" _
                      & " AND (VOUCHER_D.VHD_SUB_ID = '" & Session("Sub_ID") & "')" _
                      & " AND (VOUCHER_D.VHD_BSU_ID = '" & Session("sBsuid") & "') " _
                      & " AND  (VOUCHER_D.VHD_DOCTYPE = 'CR')"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
     

    Protected Sub gvDetails_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvDetails.SelectedIndexChanging
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True
            gvDetails.SelectedIndex = e.NewSelectedIndex
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label
                lblGUID = TryCast(gvDetails.SelectedRow.FindControl("lblGUID"), Label)
                lblSlno = TryCast(gvDetails.SelectedRow.FindControl("lblSlno"), Label)
                str_Sql = "select * FROM VOUCHER_D where GUID='" & lblGUID.Text & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT    VOUCHER_D_S.vds_doctype,VOUCHER_D_S.vds_docno," _
                    & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME,  " _
                    & " VOUCHER_D_S.VDS_DESCR, " _
                    & " case isnull(VOUCHER_D_S.VDS_CODE,'') " _
                    & " when '' then 'GENERAL' " _
                    & " else  COSTCENTER_S.CCS_DESCR end as GRPFIELD, " _
                    & " COSTCENTER_S.CCS_DESCR , VOUCHER_D_S.VDS_CODE, " _
                    & " VOUCHER_D_S.VDS_AMOUNT, COSTCENTER_S.CCS_QUERY " _
                    & " FROM VOUCHER_D INNER JOIN ACCOUNTS_M ON " _
                    & " ACCOUNTS_M.ACT_ID = VOUCHER_D.VHD_ACT_ID " _
                    & " LEFT OUTER JOIN VOUCHER_D_S ON " _
                    & " VOUCHER_D.VHD_SUB_ID=VOUCHER_D_S.VDS_SUB_ID AND " _
                    & " VOUCHER_D.VHD_BSU_ID=   VOUCHER_D_S.VDS_BSU_ID  AND " _
                    & " VOUCHER_D.VHD_FYEAR =VOUCHER_D_S.VDS_FYEAR AND " _
                    & " VOUCHER_D.VHD_DOCTYPE=VOUCHER_D_S.VDS_DOCTYPE AND " _
                    & " VOUCHER_D.VHD_DOCNO=VOUCHER_D_S.VDS_DOCNO  AND " _
                    & " VOUCHER_D.VHD_LINEID = VOUCHER_D_S.VDS_SLNO " _
                    & " LEFT OUTER JOIN  COSTCENTER_S " _
                    & " ON  VOUCHER_D_S.VDS_CCS_ID=COSTCENTER_S.CCS_ID " _
                    & " WHERE  VOUCHER_D_S.VDS_DOCTYPE='CR' AND " _
                    & " VOUCHER_D_S.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHD_DOCNO") & "' AND" _
                    & " VOUCHER_D_S.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHD_SUB_ID") & "' AND" _
                    & " VOUCHER_D_S.VDS_BSU_ID='" & Session("sBsuid") & "' AND" _
                    & " VOUCHER_D_S.VDS_FYEAR = " & Session("F_YEAR") _
                    & "AND VDS_SLNO='" & lblSlno.Text & "'" _
                    & " ORDER BY GRPFIELD"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    'helper.RegisterGroup("5", True, True)
                    If dsc.Tables(0).Rows.Count > 0 Then
                        h_Grid.Value = "child"
                    Else
                        h_Grid.Value = "detail"
                    End If
                    helper.RegisterSummary("VDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    gvChild.DataBind()
                    helper.ApplyGroupSort()
                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                Throw        'Bubble up the exception
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub PrintVoucher(ByVal p_Docno As String)

        'TAX CODE
        Dim BSU_IsTAXEnabled As Boolean = False
        Dim ds7 As New DataSet
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms0(1).Value = Session("sBsuid")
        ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
        BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))

        Dim str_Sql, strFilter As String
        strFilter = " VHH_FYEAR = " & Session("F_YEAR") & " and VHH_DOCTYPE = 'CR' and VHH_DOCNO = '" & p_Docno _
        & "' and VHH_FYEAR = '" & Session("F_YEAR") & "' and VHH_BSU_ID in('" & Session("sBsuid") & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            Dim formulas As New Hashtable
            formulas("HideCC") = Session("HideCC")
            repSource.Formulas = formulas

            params("UserName") = Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(Session("SUB_ID"), "CR", p_Docno)
            params("voucherName") = "CASH RECEIPT VOUCHER"
            params("Summary") = False
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.VoucherName = "CASH RECEIPT VOUCHER"
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(p_Docno, "CR", Session("sBSUID"))
            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            ReDim Preserve repSourceSubRep(3)
            Dim subReportLength As Integer = 0
            subReportLength = repSourceSubRep.Length

            repSourceSubRep(subReportLength - 1) = New MyReportClass
            repSourceSubRep(subReportLength - 1) = getCostCenterTransDetail(Session("sBSUID"), Session("SUB_ID"), Session("F_YEAR"), "CR", p_Docno)
            repSource.SubReport = repSourceSubRep
            repSource.Command = cmd


            repSource.ResourceName = "../RPT_Files/CashPaymentReport.rpt"
           
            'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
            If BSU_IsTAXEnabled = True Then
                repSource.ResourceName = "../RPT_Files/CashPaymentTaxVoucher.rpt"
            End If
            Session("ReportSource") = repSource
            h_print.Value = "print"

        End If
    End Sub
    Public Shared Function getCostCenterTransDetail(ByVal Bsuid As String, ByVal SubId As String, ByVal FinYear As String, ByVal DocType As String, ByVal DocNo As String) As MyReportClass
        Try
            Dim SubRepCostCenter As New MyReportClass
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim cmdCostCenter As New SqlCommand
            Dim Connection As New SqlConnection(str_conn)
            Dim param(3) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@VHD_BSU_ID", Bsuid, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(0))
            param(1) = Mainclass.CreateSqlParameter("@VHD_SUB_ID", SubId, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(1))
            param(2) = Mainclass.CreateSqlParameter("@VHD_DOCTYPE", DocType, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(2))
            param(3) = Mainclass.CreateSqlParameter("@VHD_DOCNO", DocNo, SqlDbType.VarChar)
            cmdCostCenter.Parameters.Add(param(3))
            cmdCostCenter.CommandText = "GetCostCenterTransDetails"
            cmdCostCenter.CommandType = CommandType.StoredProcedure
            cmdCostCenter.Connection = Connection
            SubRepCostCenter.Command = cmdCostCenter
            getCostCenterTransDetail = SubRepCostCenter
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Protected Sub gvChild_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

    End Sub
End Class
