Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Accounts_accChqCancellation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtBankDescr.Attributes.Add("ReadOnly", "ReadOnly")
                txtChqBook.Attributes.Add("ReadOnly", "ReadOnly")
                txtChqNo.Attributes.Add("ReadOnly", "ReadOnly")
                gvGroup1.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub gridbind()
        Try
            Dim str_txtCode, str_txtName, str_txtControl As String
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = "" 
            Dim str_Sql As String = " SELECT CHD_ID, CHB_ID,CHD_No, ACT_NAME," _
            & " CHB_LOTNO,CHB_PREFIX ,CHB_FROM ,CHB_TO,CHB_NEXTNo,AvlNos" _
            & " FROM vw_OSA_CHQBOOK_M AM WHERE CHD_ALLOTED = 0 AND CHB_BSU_ID ='" _
            & Session("sBSUID") & "' AND CHB_ACT_ID= '" & txtBankCode.Text & _
            "' AND CHB_LOTNO='" & txtChqBook.Text & "' order by CHB_LOTNO,CHD_No"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            gvGroup1.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup1.DataBind()
                Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup1.Rows(0).Cells.Clear()
                gvGroup1.Rows(0).Cells.Add(New TableCell)
                gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup1.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim arrList As New ArrayList
        Find_Checked(Me.Page, arrList)
        If SaveCancelCheques(arrList) Then
            lblError.Text = "Data Updated sucessfully..."
            txtNarration.Text = ""
            txtBankCode.Text = ""
            txtBankDescr.Text = ""
            txtChqBook.Text = ""
            txtChqNo.Text = ""
            gridbind()
        End If
    End Sub

    Private Sub Find_Checked(ByVal Page As Control, ByVal arrList As ArrayList)
        If arrList Is Nothing Then
            arrList = New ArrayList
        End If
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    'chk.Visible = False
                    arrList.Add(chk.Value.ToString)
                    'GetRowDetails(str, CHD_ID)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl, arrList)
                End If
            End If
        Next
    End Sub

    Private Sub GetRowDetails(ByVal CHD_ID As Integer, ByRef CHQ_NO As Integer, ByRef CHB_ID As Integer)
        Dim lblCHD_ID As Label
        Dim lblCHBId As Label
        Dim lblChdNo As Label
        For Each row As GridViewRow In gvGroup1.Rows
            lblCHD_ID = row.FindControl("lblCHD_ID")
            If Not lblCHD_ID Is Nothing Then
                If lblCHD_ID.Text = CHD_ID Then
                    lblCHBId = row.FindControl("lblCHBLotNo")
                    lblChdNo = row.FindControl("lblChdNo")
                    If Not lblCHBId Is Nothing AndAlso Not lblChdNo Is Nothing Then
                        CHB_ID = lblCHBId.Text
                        CHQ_NO = lblChdNo.Text
                        Exit Sub
                    End If
                End If
            End If
        Next
    End Sub

    Private Function SaveCancelCheques(ByVal arrList As ArrayList) As Boolean
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim stTrans As SqlTransaction
        Dim iReturnvalue As Integer
        Dim vCHQ_NO, vCHB_ID As Integer

        objConn.Open()
        stTrans = objConn.BeginTransaction
        Try
            Dim ienum As IEnumerator = arrList.GetEnumerator
            While ienum.MoveNext()

                GetRowDetails(ienum.Current, vCHQ_NO, vCHB_ID)

                Dim cmd As New SqlCommand("[CANCELCHEQUE]", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpCHD_NO As New SqlParameter("@CHD_NO", SqlDbType.Int)
                sqlpCHD_NO.Value = vCHQ_NO
                cmd.Parameters.Add(sqlpCHD_NO)

                Dim sqlpCHB_ID As New SqlParameter("@CHB_ID", SqlDbType.Int)
                sqlpCHB_ID.Value = vCHB_ID
                cmd.Parameters.Add(sqlpCHB_ID)

                Dim sqlpNarration As New SqlParameter("@Narration", SqlDbType.VarChar, 200)
                sqlpNarration.Value = txtNarration.Text
                cmd.Parameters.Add(sqlpNarration)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue = 0 Then iReturnvalue = UtilityObj.operOnAudiTable(Master.MenuName, "CHQ NO:" & vCHQ_NO & " LOT NO:" & vCHB_ID, txtBankCode.Text & " - " & txtBankDescr.Text, Page.User.Identity.Name.ToString, Me.Page, "CHEQUE CANCELLED")
                If iReturnvalue <> 0 Then
                    Exit While
                End If
            End While

            If (iReturnvalue = 0) Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
            If (iReturnvalue = 0) Then
                Return True
            Else
                lblError.Text = getErrorMessage(iReturnvalue)
                Return False
            End If
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
            Return False
        Finally
            objConn.Close()
        End Try
    End Function
 
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub imgCHQAllote_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCHQAllote.Click
        gridbind()
    End Sub

End Class
