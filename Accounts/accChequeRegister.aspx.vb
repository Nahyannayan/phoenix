Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Accounts_accChequeRegister
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            gvJournal.Columns(12).Visible = False
            Dim str_Sql As String
            If MainMnu_code = "A150115" Then
                lblrptCaption.Text = "Receive Cheque(s)"
                str_Sql = "SELECT CST_ID, CST_DESCR FROM CHEQUESTATUS WHERE CST_ID in(1,2)"
                tr_Narration.Visible = False
            ElseIf MainMnu_code = "A150120" Then
                lblrptCaption.Text = "Verify Cheque(s)"
                gvJournal.Columns(12).Visible = True
                str_Sql = "SELECT CST_ID, CST_DESCR FROM CHEQUESTATUS WHERE CST_ID in(2,3,6)"
                tr_Narration.Visible = True
            End If 
          
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            ddStatusChange.DataSource = ds
            ddStatusChange.DataTextField = "CST_DESCR"
            ddStatusChange.DataValueField = "CST_ID"
            ddStatusChange.DataBind()

            If USR_NAME = "" Or CurBsUnit = "" Or ((MainMnu_code <> "A150115") And (MainMnu_code <> "A150120")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            'hlAddNew.NavigateUrl = "accChequeRegister.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            ddlBusinessunit.DataBind()
            If Not ddlBusinessunit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                ddlBusinessunit.ClearSelection()
                ddlBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            End If
            gridbind()
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim VDC_BSU_ID As String = e.Row.DataItem("VDC_BSU_ID").ToString
                Dim VHH_DOCNO As String = e.Row.DataItem("VHH_DOCNO").ToString
                Dim lblVDC_CST_ID As New Label

                Dim lbView As LinkButton = TryCast(e.Row.FindControl("lbView"), LinkButton)
                lbView.OnClientClick = "javascript:ShowDetails('" & VDC_BSU_ID & "','" & VHH_DOCNO & "');return false;"
                lblVDC_CST_ID = TryCast(e.Row.FindControl("lblVDC_CST_ID"), Label)
                Dim cmdCol As Integer = gvJournal.Columns.Count - 1
                Dim ddUpdateStatus As New DropDownList
                ddUpdateStatus = TryCast(e.Row.FindControl("ddUpdateStatus"), DropDownList)
                If lblVDC_CST_ID IsNot Nothing Then
                    If CInt(lblVDC_CST_ID.Text) >= 5 Then
                        ddUpdateStatus.Visible = False
                    End If
                    Dim str_Sql As String = ""

                    If MainMnu_code = "A150115" Then
                        str_Sql = "SELECT CST_ID, CST_DESCR FROM CHEQUESTATUS WHERE CST_ID in(1,2)"
                    ElseIf MainMnu_code = "A150120" Then
                        str_Sql = "SELECT CST_ID, CST_DESCR FROM CHEQUESTATUS WHERE CST_ID in(2,3,6)"
                    End If
                    Dim ds As New DataSet
                    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
                    ddUpdateStatus.DataSource = ds
                    ddUpdateStatus.DataTextField = "CST_DESCR"
                    ddUpdateStatus.DataValueField = "CST_ID"
                    ddUpdateStatus.DataBind()
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim iReturnValue As Integer
        For Each gvr As GridViewRow In gvJournal.Rows
            If gvr.RowType = DataControlRowType.DataRow Then
                'Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                Dim chkControl As HtmlInputCheckBox = gvr.FindControl("chkControl")
                If Not chkControl.Checked Then Continue For
                Dim lblVDC_ID As Label = gvr.FindControl("lblVDC_ID")
                Dim lblVDC_CST_ID As Label = TryCast(gvr.FindControl("lblVDC_CST_ID"), Label)
                Dim ddUpdateStatus As New DropDownList
                ddUpdateStatus = TryCast(gvr.FindControl("ddUpdateStatus"), DropDownList)
                If Not lblVDC_ID Is Nothing And Not lblVDC_CST_ID Is Nothing _
                And Not ddUpdateStatus Is Nothing Then
                    If lblVDC_CST_ID.Text <> ddUpdateStatus.SelectedItem.Value And CInt(lblVDC_CST_ID.Text) < CInt(ddUpdateStatus.SelectedItem.Value) Then
                        iReturnValue = UpdateVOUCHER_D_CHEQUES(lblVDC_ID.Text, ddUpdateStatus.SelectedItem.Value)
                        If iReturnValue <> 0 Then
                            gvr.BackColor = Drawing.Color.Red
                        End If
                    End If
                End If
            End If
        Next
        txtNarrReject.Text = ""
        gridbind()
    End Sub

    Private Sub gridbind()
        Try
            Dim str_filter As String = String.Empty
            Dim lstrDocNo As String = String.Empty
            Dim lstrDocDate As String = String.Empty
            Dim lstrChqDate As String = String.Empty
            Dim lstrPayee As String = String.Empty

            Dim lstrNarration As String = String.Empty
            Dim lstrOpr As String = String.Empty

            Dim lstrChqNo As String = String.Empty
            Dim lstrAmount As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables 
                larrSearchOpr = h_Selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   DocNo
                larrSearchOpr = h_Selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                lstrDocNo = Trim(txtSearch.Text)
                If (lstrDocNo <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VHH_DOCNO", lstrDocNo)

                '   -- 2  DocDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
                lstrChqNo = txtSearch.Text
                If (lstrChqNo <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VDC_VHD_CHQNO", lstrChqNo)

                '   -- 3  DocDate
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
                lstrDocDate = txtSearch.Text
                If (lstrDocDate <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VDC_APPROVEDATE", lstrDocDate)

                '   -- 4  Narration
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
                lstrNarration = txtSearch.Text
                If (lstrNarration <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VDC_REMARKS", lstrNarration)

                '   -- 5  Amount
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrAmount = txtSearch.Text
                If (lstrAmount <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VDC_BundleNo", lstrAmount)

                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtPayee")
                lstrPayee = txtSearch.Text
                If (lstrPayee <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "Payee", lstrPayee)

                larrSearchOpr = h_Selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtChqDate")
                lstrChqDate = txtSearch.Text
                If (lstrChqDate <> "") Then str_filter = str_filter & SetCondn(lstrOpr, "VHD_CHQDT", lstrChqDate)

            End If

            Dim str_Sql As String = String.Empty
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
               
            If MainMnu_code = "A150115" Then
                str_filter = str_filter & " AND VDC_CST_ID=1"
            ElseIf MainMnu_code = "A150120" Then
                str_filter = str_filter & " AND VDC_CST_ID in (1,2) "
            End If

            If ddlBusinessunit.SelectedItem.Value <> "ALL" Then
                str_filter = str_filter & " AND VDC_BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "'  "
            End If
            str_Sql = "SELECT * FROM VW_OSA_VOUCHER_D_CHEQUES " _
                & " WHERE 1=1 " & str_filter _
                & " ORDER BY VDC_BundleNo DESC,VDC_APPROVEDATE DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                tr_Post.Visible = False
            Else
                tr_Post.Visible = True
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = lstrDocNo

            txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
            txtSearch.Text = lstrDocDate

            txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
            txtSearch.Text = lstrChqNo

            txtSearch = gvJournal.HeaderRow.FindControl("txtNarration")
            txtSearch.Text = lstrNarration

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrAmount

            txtSearch = gvJournal.HeaderRow.FindControl("txtpayee")
            txtSearch.Text = lstrPayee

            txtSearch = gvJournal.HeaderRow.FindControl("txtChqDate")
            txtSearch.Text = lstrChqDate

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub rb_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Function UpdateVOUCHER_D_CHEQUES(ByVal VDC_ID As String, ByVal VDC_CST_ID As String) As Integer
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim stTrans As SqlTransaction
        Dim iReturnvalue As Integer
        objConn.Open()
        stTrans = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand("UpdateVOUCHER_D_CHEQUES", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpVDC_ID As New SqlParameter("@VDC_ID", SqlDbType.BigInt)
            sqlpVDC_ID.Value = VDC_ID
            cmd.Parameters.Add(sqlpVDC_ID)

            Dim sqlpVDC_SENDBY As New SqlParameter("@VDC_APPROVEDBY", SqlDbType.VarChar, 100)
            sqlpVDC_SENDBY.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpVDC_SENDBY)

            Dim sqlpVDC_SendDate As New SqlParameter("@VDC_APPROVEDATE", SqlDbType.DateTime)
            sqlpVDC_SendDate.Value = Date.Now.ToString("dd/MMM/yyyy")
            cmd.Parameters.Add(sqlpVDC_SendDate)

            Dim sqlpVDC_STATUS As New SqlParameter("@VDC_CST_ID", SqlDbType.VarChar, 20)
            sqlpVDC_STATUS.Value = VDC_CST_ID
            cmd.Parameters.Add(sqlpVDC_STATUS)

            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)

            Dim sqlpVDC_APPRREMARKS As New SqlParameter("@VDC_APPRREMARKS", SqlDbType.VarChar)
            sqlpVDC_APPRREMARKS.Value = txtNarrReject.Text
            cmd.Parameters.Add(sqlpVDC_APPRREMARKS)

            cmd.ExecuteNonQuery()
            iReturnvalue = retValParam.Value

            If iReturnvalue = 0 Then iReturnvalue = UtilityObj.operOnAudiTable(Master.MenuName, "VCD_ID:" & VDC_ID, "Approval", Page.User.Identity.Name.ToString, Me.Page, "CHEQUE REGISTER")

            If (iReturnvalue = 0) Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
            lblError.Text = getErrorMessage(iReturnvalue)
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            stTrans.Rollback()
            Errorlog(ex.Message)
            iReturnvalue = 1000
        Finally
            objConn.Close()
        End Try
        Return iReturnvalue
    End Function

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub ddStatusChange_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddStatusChange.SelectedIndexChanged
        ' Dim lblBundleNo As New Label
        ' Dim str_lblBundleNo As String = String.Empty
        'For Each row As GridViewRow In gvJournal.Rows
        '    If row.RowType = DataControlRowType.DataRow Then
        '        lblBundleNo = TryCast(row.FindControl("lblBundleNo"), Label)
        '        If lblBundleNo IsNot Nothing Then
        '            If str_lblBundleNo = String.Empty Then
        '                str_lblBundleNo = lblBundleNo.Text
        '            ElseIf str_lblBundleNo <> lblBundleNo.Text Then
        '                lblError.Text = "Cannot update status since multiple bundles are selected..."
        '                ddStatusChange.SelectedIndex = 0 
        '                Exit Sub
        '            End If
        '        End If
        '    End If
        'Next

        Dim lblVDC_CST_ID As New Label
        Dim ddUpdateStatus As New DropDownList
        For Each row As GridViewRow In gvJournal.Rows
            If row.RowType = DataControlRowType.DataRow Then
                lblVDC_CST_ID = TryCast(row.FindControl("lblVDC_CST_ID"), Label)
                ddUpdateStatus = TryCast(row.FindControl("ddUpdateStatus"), DropDownList)
                If lblVDC_CST_ID IsNot Nothing Then
                    If Not ddUpdateStatus.Items.FindByValue(ddStatusChange.SelectedItem.Value) Is Nothing Then
                        ddUpdateStatus.ClearSelection()
                        ddUpdateStatus.Items.FindByValue(ddStatusChange.SelectedItem.Value).Selected = True
                    End If
                End If
            End If
        Next
    End Sub

End Class

