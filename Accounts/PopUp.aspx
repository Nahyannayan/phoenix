<%@ Page Language="VB" AutoEventWireup="true" CodeFile="PopUp.aspx.vb" Inherits="PopUp" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Accounts</title>
  
     <base target="_self" />
        <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
<%--<style type="text/css" > 
 .odd{background-color: white;} 
 .even{background-color: gray;} 
</style>--%>
        <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    
   <script language="javascript" type="text/javascript">
        //not in use
     
    function Return(retval)
    {
    window.returnValue = retval;
    //alert(retval);
    window.close();    
    }
    function hide(id)
     { 
     var tdid='tbl_Message'+id;
    // alert(tdid);
     document.getElementById(tdid).style.display='none';
     }
       function help(id) {     
   
     //var groupcode=
     
            var sFeatures;
            sFeatures="dialogWidth: 460px; ";
            sFeatures+="dialogHeight: 150px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url='ShowError.aspx?id='+id; 
            result = window.showModalDialog(url,"", sFeatures)
              }  
    </script>
    
       <script language="javascript" type="text/javascript">
      
    function hide(id)
     { 
     var tdid='tbl_Message'+id;
    // alert(tdid);
     document.getElementById(tdid).style.display='none';
     }
    function help(id) {
        var sFeatures;
        sFeatures = "dialogWidth: 460px; ";
        sFeatures += "dialogHeight: 150px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = 'ShowError.aspx?id=' + id;
        result = window.showModalDialog(url, "", sFeatures)
    }
              
            
        function MoveCursorDown() 
        {//alert("ii");
         var c=16;
        c=document.getElementById("<%=h_SelectedId.ClientID %>").value-0;
        //alert(c)
        c++;
        if (c <= 0 || c==-1)
        {
        document.getElementById("<%=h_SelectedId.ClientID %>").value=='17';
        c=16;
        }
        //alert(document.getElementById("<%=h_SelectedId.ClientID %>").value);
       
        //c++;
        //document.getElementById("<%=h_SelectedId.ClientID %>").value=c;
            selectedRow = null; 
            var index=0;
            table = document.getElementById("gvGroup");
            if(table == null) return;
            rows = table.getElementsByTagName("TR");

            if (c<rows.length-1)
            { 
             rows[c].className = "gridheader";
             if (c%2==0)
             rows[c-1].className = "griditem_alternative";
             else
              rows[c-1].className = "griditem";
             document.getElementById("<%=h_SelectedId.ClientID %>").value=c;
             
            }
             // alert(rows.length-900);

        }
                
    </script>
</head>
<body  class="matter" onload="listen_window();" onkeydown="MoveCursorDown();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">

 <table width="100%" id="tbl" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3">                     
                        <input id="h_SelectedId" runat="server" type="hidden" value="-1" /> 
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    </td>
                </tr>
                
                <tr >
                    <td>
                    </td>
                    <td align="center" >
                        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"  Width="100%" DataKeyNames="Code" EmptyDataText="No Data" CssClass="table table-row table-bordered">
                            <Columns>
                                <asp:TemplateField HeaderText="Account Code" SortExpression="Code">
                                    <EditItemTemplate>
                                        &nbsp;
                                    </EditItemTemplate>
                                    <HeaderTemplate>
                                        <table style="width: 100%; height: 100%">
                                            <tr>
                                                <td align="center"   class="gridheader_text">
                                                    Account Code
                                                    <br />
                                                     <asp:TextBox ID="txtCode" runat="server" width="70%"></asp:TextBox>
                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />&nbsp;
                                                </td>
                                            </tr>                                           
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Account Name" SortExpression="Description">
                                    <EditItemTemplate>
                                        &nbsp;
                                    </EditItemTemplate>
                                    <HeaderTemplate>
                                        <table style="width: 100%; height: 100%">
                                            <tr>
                                                <td align="center"  style="width: 100%" class="gridheader_text">
                                                    Account Name
                                                    <br />
                                                    <asp:TextBox ID="txtName" runat="server" Width="70%"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />&nbsp;
                                                </td>
                                            </tr>                                           
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("Description") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                             <RowStyle CssClass="griditem" />
                            <HeaderStyle CssClass="gridheader_pop"  />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <SelectedRowStyle BackColor="Aqua" />
                        </asp:GridView>
                        &nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3"></td>                    
                </tr>
            </table>
 
</form>
</body>
</html>
