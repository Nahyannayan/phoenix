<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowPrepaid.aspx.vb" Inherits="Accounts_ShowPrepaid" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Accounts</title>
    <base target="_self" />
    

    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>

    <script language="javascript" type="text/javascript">

        function Return(retval) {
            window.returnValue = retval;
            //alert(retval);
            window.close();
        }




        function MoveCursorDown() {//alert("ii");
            var c = 16;
            c = document.getElementById("<%=h_SelectedId.ClientID %>").value - 0;
            //alert(c)
            c++;
            if (c <= 0 || c == -1) {
                document.getElementById("<%=h_SelectedId.ClientID %>").value == '17';
                c = 16;
            }
            //alert(document.getElementById("<%=h_SelectedId.ClientID %>").value);

            //c++;
            //document.getElementById("<%=h_SelectedId.ClientID %>").value=c;
            selectedRow = null;
            var index = 0;
            table = document.getElementById("gvGroup");
            if (table == null) return;
            rows = table.getElementsByTagName("TR");

            if (c < rows.length - 1) {
                rows[c].className = "gridheader";
                if (c % 2 == 0)
                    rows[c - 1].className = "griditem_alternative";
                else
                    rows[c - 1].className = "griditem";
                document.getElementById("<%=h_SelectedId.ClientID %>").value = c;

            }
            // alert(rows.length-900);

        }
    </script>
</head>
<body  onload="listen_window();">
    <form id="form1" runat="server">

        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <%--<tr valign="top">
                <td width="2%"></td>
                <td width="28%"></td>
                <td width="20%"></td>
                <td width="20%"></td>
                <td width="28%"></td>
                <td width="2%"></td>
            </tr>--%>
            <tr>
                <td align="center" >
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="ACT_ID" EmptyDataText="No Data" AllowPaging="True" PageSize="18"
                        CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Account Code" SortExpression="ACT_ID">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Account Code
                                    <br />
                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Name" SortExpression="ACT_NAME">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Account Name
                                    <br />
                                    <asp:TextBox ID="txtName" runat="server" Width="100%"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("ACT_NAME") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="retactcode" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblRET_CODE" runat="server" Text='<%# Bind("RET_CODE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="retactname" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblRET_NAME" runat="server" Text='<%# Bind("RET_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <HeaderStyle CssClass="gridheader_pop" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <SelectedRowStyle BackColor="Aqua" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center"  >
                    <span id="sp_message" class="error" runat="server">
                    </span>
                </td>               
            </tr>
            <tr>
                <td>
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
            </tr>
        </table>


    </form>
</body>
</html>
