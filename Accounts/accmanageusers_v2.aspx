<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accmanageusers_v2.aspx.vb" Inherits="manageusers_v2" Title="Manage Users" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>
            Manage Users_V2
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" >&nbsp;<asp:Label ID="lblErrorMessage" runat="server" CssClass="error"></asp:Label></td>                       
                    </tr>
                    <tr>
                        <td align="left"  valign="middle">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                        </td>                        
                    </tr>
                    <tr>
                        <td >
                            <table align="center" width="100%">
                                <tr>
                                    <td colspan="4" valign="top">
                                        <asp:GridView ID="gvManageUsers" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="USR_ID" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="User ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("USR_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserID" runat="server" Text='<%# Bind("USR_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User Name" SortExpression="USR_NAME">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("USR_NAME") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                         <asp:Label ID="lbluserNameH" runat="server" CssClass="gridheader_text" Text="User Name"
                                                                       ></asp:Label>
                                                                                <br />
                                                                                 <asp:TextBox ID="txtUserName" runat="server" ></asp:TextBox>
                                                                                 <asp:ImageButton ID="btnSearchUserName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchUserName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsername" runat="server" Text='<%# Bind("USR_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User Role">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                       <asp:Label ID="lblRoleH" runat="server" CssClass="gridheader_text" Text="User Role"
                                                                        ></asp:Label>
                                                                                <br />
                                                                                 <asp:TextBox ID="txtUserRole" runat="server"></asp:TextBox>
                                                                                 <asp:ImageButton ID="btnSearchUserRole" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchUserRole_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserRole" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Emp Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Full_Name") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEmpNameH" runat="server" CssClass="gridheader_text" Text="Employee Name"
                                                                       ></asp:Label>
                                                                                <br />
                                                                                   <asp:TextBox ID="txtEmpName" runat="server" ></asp:TextBox>
                                                                                 <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchEmpName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("Full_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText=" Business Unit">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblbsuNameH" runat="server" CssClass="gridheader_text" Text="Business Unit"
                                                                        ></asp:Label>
                                                                                <br />
                                                                                <asp:TextBox ID="txtBusName" runat="server" ></asp:TextBox>
                                                                                 <asp:ImageButton ID="btnSearchBusUnit" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchBusUnit_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBsuName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField HeaderText="View" Text="View" CommandName="Select">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle Width="10%" />
                                                </asp:ButtonField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="0" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="odsUpdateDelete" runat="server" SelectMethod="getUserInfo"
                    TypeName="AccessRoleUser" DeleteMethod="DeleteUser">
                    <DeleteParameters>
                        <asp:Parameter Name="usr_ID" Type="String" />
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>
</asp:Content>

