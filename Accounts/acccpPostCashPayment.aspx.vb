Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_acccpPostCashPayment
    Inherits System.Web.UI.Page 
    Dim Encr_decrData As New Encryption64
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            Page.Title = OASISConstants.Gemstitle
            h_Grid.Value = "top"
            '''''check menu rights
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A200010") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            '''''check menu rights
            gvChild.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            hlCEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If chkPost IsNot Nothing Then
                If chkPost.Checked = True Then
                    chkPost.Disabled = True
                Else
                    If lblGUID IsNot Nothing Then
                        ' Dim i As New Encryption64
                        viewstate("datamode") = Encr_decrData.Encrypt("view")
                        hlview.NavigateUrl = "acccpCashPayments.aspx?viewid=" & lblGUID.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode")
                    End If
                End If
            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub Find_Checked(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    post_voucher(chk.Value.ToString)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
        Next
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        gvChild.Visible = False
        gvDetails.Visible = False
        h_FirstVoucher.Value = ""
        Find_Checked(Me.Page)
        If h_FirstVoucher.Value <> "" And chkPrint.Checked Then
            PrintVoucher(h_FirstVoucher.Value)
        End If
        gridbind()
    End Sub

    Function post_voucher(ByVal p_guid As String) As Boolean
        Try
            Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here
                '@JHD_SUB_ID	varchar(10),
                '	@JHD_BSU_ID	varchar(10),
                '	@JHD_FYEAR	int,
                '	@JHD_DOCTYPE varchar(20),
                '	@JHD_DOCNO	varchar(20) 
                ''get header info
                Dim str_Sql As String
                str_Sql = "select * FROM VOUCHER_H where GUID='" & p_guid & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then

                    Dim cmd As New SqlCommand("POSTVOUCHER", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("VHH_SUB_ID") & ""
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = "CP"
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim STATUS As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    STATUS = retValParam.Value
                    'Adding header info
                    cmd.Parameters.Clear()
                    'Adding transaction info
                    ' Dim str_err As String
                    Dim vSUB_ID As String = ds.Tables(0).Rows(0)("VHH_SUB_ID")
                    Dim vF_YEAR As Integer = ds.Tables(0).Rows(0)("VHH_FYEAR")
                    Dim vDOC_NO As String = ds.Tables(0).Rows(0)("VHH_DOCNO")
                    Dim vDOC_DATE As String = ds.Tables(0).Rows(0)("VHH_DOCDT")
                    Dim vVHH_CUR_ID As String = ds.Tables(0).Rows(0)("VHH_CUR_ID")
                    If STATUS = 0 Then AccountsDetails.SaveMonthlyData_D("CP", "D", Session("sBSUID"), vDOC_NO, vSUB_ID, vVHH_CUR_ID, vF_YEAR, vDOC_DATE, objConn, stTrans)

                    Dim VHId As Integer

                    Dim dsDetail As New DataSet
                    Dim j As Integer
                    Dim PRP_ID As String = ds.Tables(0).Rows(0)("VHH_DOCNO")
                    Dim PRP_SUB_ID As String = ds.Tables(0).Rows(0)("VHH_SUB_ID")
                    Dim PRP_BSU_ID As String = ds.Tables(0).Rows(0)("VHH_BSU_ID")
                    Dim PRP_FYEAR As Integer = ds.Tables(0).Rows(0)("VHH_FYEAR")
                    'Dim PRP_AMOUNT As Decimal = ds.Tables(0).Rows(0)("VHH_AMOUNT")
                    Dim PRP_AMOUNT As Decimal
                    Dim PRP_DOCTYPE = ds.Tables(0).Rows(0)("VHH_DOCTYPE")
                    Dim PRP_PREPAYACC As String
                    Dim PRP_EXPENSEACC As String
                    Dim PRP_CRNARRATION As String = ""
                    Dim PRP_DRNARRATION As String = ""
                    Dim PRP_TAXCODE As String = ""
                    Dim PRP_DOCDT As DateTime = Format(Date.Parse(ds.Tables(0).Rows(0)("VHH_DOCDT")), "dd/MMM/yyyy")
                    Dim PRP_CUR_ID As String = ds.Tables(0).Rows(0)("VHH_CUR_ID")
                    Dim PRP_EXGRATE1 As Decimal = ds.Tables(0).Rows(0)("VHH_EXGRATE1")
                    Dim PRP_EXGRATE2 As Decimal = ds.Tables(0).Rows(0)("VHH_EXGRATE2")
                    'VHId = GetDocNo(ds.Tables(0).Rows(0)("VHH_BSU_ID"), ds.Tables(0).Rows(0)("VHH_FYEAR"), "CP", ds.Tables(0).Rows(0)("VHH_DOCNO"))
                    'strAccountID = GetAccountDetail(ds.Tables(0).Rows(0)("VHH_BSU_ID"), ds.Tables(0).Rows(0)("VHH_FYEAR"), "CP", ds.Tables(0).Rows(0)("VHH_DOCNO"))

                    str_Sql = "SELECT * FROM VOUCHER_D WHERE VHD_BSU_ID='" & PRP_BSU_ID & "' AND VHD_SUB_ID='" & PRP_SUB_ID & "' AND VHD_FYEAR=" & PRP_FYEAR & " AND VHD_DOCTYPE='" & PRP_DOCTYPE & "' AND VHD_DOCNO='" & PRP_ID & "'"
                    dsDetail = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
                    If dsDetail.Tables(0).Rows.Count > 0 Then
                        For j = 0 To dsDetail.Tables(0).Rows.Count - 1
                            VHId = dsDetail.Tables(0).Rows(j)("VHD_ID")
                            PRP_PREPAYACC = dsDetail.Tables(0).Rows(j)("VHD_ACT_ID")
                            PRP_CRNARRATION = dsDetail.Tables(0).Rows(j)("VHD_NARRATION")
                            PRP_DRNARRATION = dsDetail.Tables(0).Rows(j)("VHD_NARRATION")
                            PRP_TAXCODE = dsDetail.Tables(0).Rows(j)("VHD_TAX_CODE")
                            If VHId > 0 Then
                                Dim strQuery As String
                                Dim dsSub As DataSet
                                strQuery = "SELECT * FROM VOUCHER_D_SUB WHERE VSB_VHD_ID=" & VHId & ""
                                dsSub = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, strQuery)
                                If dsSub.Tables(0).Rows.Count > 0 Then
                                    Dim d1, d2 As Date
                                    Dim t As TimeSpan
                                    Dim PRP_NOOFINST As Integer

                                    Dim PRP_FTFROM As DateTime
                                    Dim PRP_DTTO As DateTime

                                    Dim PRP_CUT_ID As String
                                    Dim PRP_POSTED As Boolean
                                    Dim PRP_DELETED As Boolean  
                                    Dim bEdit As Boolean

                                    d1 = dsSub.Tables(0).Rows(0)("VSB_DTFROM")
                                    d2 = dsSub.Tables(0).Rows(0)("VSB_DTTO")
                                    PRP_FTFROM = Format(Date.Parse(d1), "dd/MMM/yyyy")
                                    PRP_DTTO = Format(Date.Parse(d2), "dd/MMM/yyyy")
                                    t = d2 - d1
                                    PRP_CUT_ID = dsSub.Tables(0).Rows(0)("VSB_CUT_ID")
                                    PRP_POSTED = True
                                    PRP_DELETED = False
                                    Dim totMonth As Integer = DateDiff("m", d1, d2) + 1
                                    Dim totdays As Integer = DateDiff("d", d1, d2) + 1
                                    Dim firstdayLastMonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d2), Date.MinValue)
                                    Dim LastDayofFirstMonth As Date = DateAdd(DateInterval.Second, -3, DateAdd(DateInterval.Month, DateDiff("m", Date.MinValue, d1) + 1, Date.MinValue)).Date
                                    Dim remfirstday As Integer = DateDiff("d", d1, LastDayofFirstMonth) + 1
                                    Dim remLastday As Integer = DateDiff("d", firstdayLastMonth, d2) + 1
                                    Dim totalAmt As Double
                                    Dim PRD_ID As Integer

                                    Dim PRP_NEWDOCNO As String = ""
                                    Dim PRD_BSU_ID As String = PRP_BSU_ID
                                    Dim PRD_DOCDT As String
                                    Dim PRP_ALLOCAMT As String
                                    Dim decFirstMonthAmount As Decimal = 0
                                    Dim Guid As Guid
                                    Dim PRP_JVNO As String = String.Empty
                                    Dim i As Integer

                                    totalAmt = dsDetail.Tables(0).Rows(j)("VHD_AMOUNT")
                                    PRP_AMOUNT = dsDetail.Tables(0).Rows(j)("VHD_AMOUNT")
                                    Dim copyTotalAmt As Double = totalAmt
                                    bEdit = 0

                                    If totMonth > 2 Then
                                        Dim perDayAmt As Double = totalAmt / totdays
                                        perDayAmt = perDayAmt
                                        Dim firstMonthAmt As Double = remfirstday * perDayAmt
                                        firstMonthAmt = Math.Round(firstMonthAmt, 0)

                                        Dim lastMonthAmt As Double = remLastday * perDayAmt
                                        lastMonthAmt = Math.Round(lastMonthAmt, 0)
                                        Dim total_remAmt4RemMonth As Double
                                        total_remAmt4RemMonth = 0
                                        PRP_NOOFINST = totMonth
                                        PRP_EXPENSEACC = GetAccountId(dsSub.Tables(0).Rows(0)("VSB_CUT_ID"), stTrans)
                                        STATUS = VoucherFunctions.SavePREPAYMENTS_H(PRP_ID, PRP_SUB_ID, PRP_BSU_ID, _
                                        PRP_FYEAR, "", PRP_PREPAYACC, PRP_EXPENSEACC, PRP_DOCDT, PRP_FTFROM, _
                                        PRP_DTTO, PRP_AMOUNT, "", PRP_NOOFINST, PRP_CUR_ID, PRP_EXGRATE1, _
                                        PRP_EXGRATE2, "", PRP_CUT_ID, PRP_CRNARRATION, PRP_DRNARRATION, "", _
                                        False, True, PRP_NEWDOCNO, vDOC_NO, "CP", "", PRP_TAXCODE, stTrans)
                                        STATUS = UtilityObj.operOnAudiTable(Master.MenuName, PRP_NEWDOCNO, _
                                        "Pre Payment Voucher from CP", Page.User.Identity.Name.ToString, Me.Page)
                                        If STATUS = 0 Then
                                            For ii As Integer = 0 To dsSub.Tables(0).Rows.Count - 1 'Only one will be there
                                                STATUS = VoucherFunctions.UpdateVOUCHER_D_SUB(VHId, PRP_NEWDOCNO, True, stTrans)
                                            Next
                                        End If
                                        Dim dtMonthend As Date
                                        dtMonthend = CDate(GetFREEZEDT(Session("sBsuid")))

                                        PRD_DOCDT = LastDayofFirstMonth
                                        PRP_ALLOCAMT = firstMonthAmt
                                        If LastDayofFirstMonth <= dtMonthend Then
                                            decFirstMonthAmount = decFirstMonthAmount + firstMonthAmt
                                        Else
                                            STATUS = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRP_NEWDOCNO, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, stTrans)
                                        End If
                                        For i = 1 To (totMonth - 2)
                                            Dim nextmonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d1.AddMonths(i)), Date.MinValue)
                                            Dim lastNextMonth As Date

                                            lastNextMonth = DateAdd("d", -1, DateAdd("m", 1, nextmonth))
                                            Dim remAmt4RemMonth As Double = perDayAmt * Day(lastNextMonth)
                                            remAmt4RemMonth = Math.Round(remAmt4RemMonth, 0)
                                            total_remAmt4RemMonth = total_remAmt4RemMonth + remAmt4RemMonth
                                            PRD_DOCDT = lastNextMonth

                                            If lastNextMonth <= dtMonthend Then
                                                decFirstMonthAmount = decFirstMonthAmount + remAmt4RemMonth
                                            Else
                                                PRP_ALLOCAMT = remAmt4RemMonth + decFirstMonthAmount
                                                decFirstMonthAmount = 0
                                                STATUS = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRP_NEWDOCNO, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, stTrans)
                                            End If
                                        Next
                                        PRD_DOCDT = DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth))
                                        Dim checkAmtFinal As Double = firstMonthAmt + lastMonthAmt + total_remAmt4RemMonth
                                        Dim AmtDiff As Double = copyTotalAmt - checkAmtFinal
                                        Dim addedToLastMonthAmt As Double = lastMonthAmt + AmtDiff
                                        PRP_ALLOCAMT = addedToLastMonthAmt
                                        STATUS = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRP_NEWDOCNO, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, stTrans)
                                       
                                    End If
                                    If STATUS = 0 Then
                                        Dim cmdPost As New SqlCommand("POSTPREPAYMENT", objConn, stTrans)
                                        cmdPost.CommandType = CommandType.StoredProcedure

                                        Dim sqlpJHDSUB_ID As New SqlParameter("@PRP_SUB_ID", SqlDbType.VarChar, 20)
                                        sqlpJHDSUB_ID.Value = PRP_SUB_ID & ""
                                        cmdPost.Parameters.Add(sqlpJHDSUB_ID)

                                        Dim sqlpJHDBSU_ID As New SqlParameter("@PRP_BSU_ID", SqlDbType.VarChar, 20)
                                        sqlpJHDBSU_ID.Value = PRP_BSU_ID & ""
                                        cmdPost.Parameters.Add(sqlpJHDBSU_ID)

                                        Dim sqlpJHDFYEAR As New SqlParameter("@PRP_FYEAR", SqlDbType.Int)
                                        sqlpJHDFYEAR.Value = PRP_FYEAR & ""
                                        cmdPost.Parameters.Add(sqlpJHDFYEAR)

                                        Dim sqlpJHDDOCNO As New SqlParameter("@PRP_DOCNO", SqlDbType.VarChar, 20)
                                        sqlpJHDDOCNO.Value = PRP_NEWDOCNO & ""
                                        cmdPost.Parameters.Add(sqlpJHDDOCNO)

                                        Dim retValue As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                                        retValue.Direction = ParameterDirection.ReturnValue
                                        cmdPost.Parameters.Add(retValue)

                                        cmdPost.ExecuteNonQuery()
                                        STATUS = retValue.Value
                                        cmdPost.Parameters.Clear()
                                    End If

                                End If

                            End If
                        Next
                    End If
                    If (STATUS = 0) Then
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("VHH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        stTrans.Commit()
                        If h_FirstVoucher.Value = "" Then
                            h_FirstVoucher.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                        End If
                        lblError.Text = "Successfully Posted"
                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(STATUS)
                    End If
                Else
                End If
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT VHH.GUID, VHH.VHH_REFNO, VHH.VHH_SUB_ID, VHH.VHH_BSU_ID," _
               & " VHH.VHH_FYEAR, VHH.VHH_DOCTYPE, VHH.VHH_DOCNO, VHH.VHH_TYPE," _
               & " VHH.VHH_DOCDT, VHH.VHH_CHQDT, VHH.VHH_ACT_ID, VHH.VHH_CUR_ID," _
               & " VHH.VHH_EXGRATE1, VHH.VHH_EXGRATE2, VHH.VHH_NARRATION, VHH.VHH_bDELETED," _
               & " VHH.VHH_bPOSTED, VHH.VHH_AMOUNT, (SELECT SUM(VHD_AMOUNT) AS TOTAL" _
               & " FROM VOUCHER_D WHERE  (VHD_SUB_ID = '" & Session("Sub_ID") & "') " _
               & " AND (VHD_BSU_ID = '" & Session("sBsuid") & "') AND (VHD_DOCTYPE = 'CP')" _
               & " AND (VHD_DOCNO = VHH.VHH_DOCNO)) AS Total, ( VHH.VHH_ACT_ID + '-'+ ACCOUNTS_M.ACT_NAME) as ACCOUNT" _
               & " FROM VOUCHER_H AS VHH INNER JOIN ACCOUNTS_M  ON VHH.VHH_ACT_ID = ACCOUNTS_M.ACT_ID" _
               & " WHERE (VHH.VHH_SUB_ID = '" & Session("Sub_ID") & "') " _
               & " AND (VHH.VHH_BSU_ID = '" & Session("sBsuid") & "') " _
               & " AND (VHH.VHH_bDELETED = 0) AND VHH.VHH_bPOSTED=0 AND VHH.VHH_FYEAR = " & Session("F_YEAR") _
               & " AND (VHH.VHH_DOCTYPE = 'CP')" _
               & " ORDER BY VHH.VHH_DOCDT DESC, VHH.VHH_DOCNO DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count > 0 Then
                btnPost.Visible = True
                chkPrint.Visible = True
            Else
                btnPost.Visible = False
                chkPrint.Visible = False
            End If
            gvJournal.DataBind()
            gvJournal.SelectedIndex = p_selected_id
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvDetails.Visible = True
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim i As Integer = sender.parent.parent.RowIndex
                gridbind(i)
                gvDetails.SelectedIndex = -1
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                str_Sql = "select * FROM VOUCHER_H where GUID='" & lblGUID.Text & "' "
                h_Grid.Value = "detail"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT     VOUCHER_D.GUID, VOUCHER_D.VHD_SUB_ID, " _
                     & " VOUCHER_D.VHD_BSU_ID, VOUCHER_D.VHD_FYEAR," _
                     & " VOUCHER_D.VHD_DOCTYPE, VOUCHER_D.VHD_DOCNO," _
                     & " VOUCHER_D.VHD_LINEID, VOUCHER_D.VHD_ACT_ID," _
                     & " VOUCHER_D.VHD_AMOUNT, VOUCHER_D.VHD_NARRATION," _
                     & " (VOUCHER_D.VHD_ACT_ID+'-'+ACCOUNTS_M.ACT_NAME) AS ACCOUNT" _
                     & " FROM VOUCHER_D INNER JOIN ACCOUNTS_M" _
                     & " ON VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID" _
                     & " AND VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID" _
                     & " WHERE (VOUCHER_D.VHD_DOCNO = '" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "')" _
                     & " AND (VOUCHER_D.VHD_SUB_ID = '" & Session("Sub_ID") & "')" _
                     & " AND (VOUCHER_D.VHD_BSU_ID = '" & Session("sBsuid") & "') " _
                     & " AND  (VOUCHER_D.VHD_DOCTYPE = 'CP')"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub gvDetails_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvDetails.SelectedIndexChanging
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True
            gvDetails.SelectedIndex = e.NewSelectedIndex
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label
                lblGUID = TryCast(gvDetails.SelectedRow.FindControl("lblGUID"), Label)
                lblSlno = TryCast(gvDetails.SelectedRow.FindControl("lblSlno"), Label)
                str_Sql = "select * FROM VOUCHER_D where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT    VOUCHER_D_S.vds_doctype,VOUCHER_D_S.vds_docno," _
                    & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME,  " _
                    & " VOUCHER_D_S.VDS_DESCR, " _
                    & " case isnull(VOUCHER_D_S.VDS_CODE,'') " _
                    & " when '' then 'GENERAL' " _
                    & " else  COSTCENTER_S.CCS_DESCR end as GRPFIELD, " _
                    & " COSTCENTER_S.CCS_DESCR , VOUCHER_D_S.VDS_CODE, " _
                    & " VOUCHER_D_S.VDS_AMOUNT, COSTCENTER_S.CCS_QUERY " _
                    & " FROM VOUCHER_D INNER JOIN ACCOUNTS_M ON " _
                    & " ACCOUNTS_M.ACT_ID = VOUCHER_D.VHD_ACT_ID " _
                    & " LEFT OUTER JOIN VOUCHER_D_S ON " _
                    & " VOUCHER_D.VHD_SUB_ID=VOUCHER_D_S.VDS_SUB_ID AND " _
                    & " VOUCHER_D.VHD_BSU_ID=   VOUCHER_D_S.VDS_BSU_ID  AND " _
                    & " VOUCHER_D.VHD_FYEAR =VOUCHER_D_S.VDS_FYEAR AND " _
                    & " VOUCHER_D.VHD_DOCTYPE=VOUCHER_D_S.VDS_DOCTYPE AND " _
                    & " VOUCHER_D.VHD_DOCNO=VOUCHER_D_S.VDS_DOCNO  AND " _
                    & " VOUCHER_D.VHD_LINEID = VOUCHER_D_S.VDS_SLNO " _
                    & " LEFT OUTER JOIN  COSTCENTER_S " _
                    & " ON  VOUCHER_D_S.VDS_CCS_ID=COSTCENTER_S.CCS_ID " _
                    & " WHERE  VOUCHER_D_S.VDS_DOCTYPE='CP' AND " _
                    & " VOUCHER_D_S.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHD_DOCNO") & "' AND" _
                    & " VOUCHER_D_S.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHD_SUB_ID") & "' AND" _
                    & " VOUCHER_D_S.VDS_BSU_ID='" & Session("sBsuid") & "' AND" _
                    & " VOUCHER_D_S.VDS_FYEAR = " & Session("F_YEAR") _
                    & "AND VDS_SLNO='" & lblSlno.Text & "'" _
                    & " ORDER BY GRPFIELD"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    If dsc.Tables(0).Rows.Count > 0 Then
                        h_Grid.Value = "child"
                    Else
                        h_Grid.Value = "detail"
                    End If
                    helper.RegisterSummary("VDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    gvChild.DataBind()
                    helper.ApplyGroupSort()
                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub PrintVoucher(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.CashPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "CP", p_Docno, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Private Function GetDocNo(ByVal BSUID As String, ByVal FYear As Integer, ByVal Doctype As String, ByVal docno As String)
        Dim strSql As String
        Dim strcon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As DataSet
        Dim VhdID As Integer
        strSql = "SELECT VHD_ID FROM VOUCHER_D WHERE VHD_BSU_ID='" & BSUID & "' AND VHD_FYEAR=" & FYear & " AND VHD_DOCTYPE='" & Doctype & "' AND VHD_DOCNO='" & docno & "'"
        ds = SqlHelper.ExecuteDataset(strcon, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            VhdID = ds.Tables(0).Rows(0)("VHD_ID")
        Else
            VhdID = 0
        End If
        Return VhdID
    End Function

    Private Function GetAccountId(ByVal CUTid As Integer, ByVal strTrans As SqlTransaction) As String
        Dim strSql As String
        Dim strconn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim strAccountId As String
        Dim ds As DataSet
        strSql = "SELECT CUT_EXP_ACT_ID FROM COSTUNIT_M WHERE CUT_ID=" & CUTid & ""
        ds = SqlHelper.ExecuteDataset(strconn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            strAccountId = ds.Tables(0).Rows(0)("CUT_EXP_ACT_ID")
        Else
            strAccountId = ""
        End If
        Return strAccountId
    End Function

    Private Function GetFREEZEDT(ByVal BSU_ID As String) As String
        Dim sqlString As String = "SELECT     BSU_FREEZEDT FROM BUSINESSUNIT_M where  BSU_ID='" & BSU_ID & "'"
        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)

        End Using
        Return CStr(result)
    End Function

    Private Function GetAccountDetail(ByVal BSUID As String, ByVal FYear As Integer, ByVal Doctype As String, ByVal docno As String) As String
        Dim strQuery As String
        Dim ds1 As DataSet
        Dim strAccount As String = String.Empty
        Dim strcon As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        strQuery = "SELECT VHD_ACT_ID FROM VOUCHER_D WHERE VHD_BSU_ID='" & BSUID & "' AND VHD_FYEAR=" & FYear & " AND VHD_DOCTYPE='" & Doctype & "' AND VHD_DOCNO='" & docno & "'"
        ds1 = SqlHelper.ExecuteDataset(strcon, CommandType.Text, strQuery)
        If ds1.Tables(0).Rows.Count > 0 Then
            strAccount = ds1.Tables(0).Rows(0)("VHD_ACT_ID")
        End If
        Return strAccount

    End Function

End Class
