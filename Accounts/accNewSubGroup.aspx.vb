Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class NewSubGroup
    Inherits System.Web.UI.Page


    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            bindlist_M()
            bindlist()
            lblError.Text = ""
            Page.Title = OASISConstants.Gemstitle
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150001"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                viewstate("datamode") = "none"
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A100005") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                viewstate("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
            End If
            If Request.QueryString("editid") = "" Then
                reset_view_data()

            Else
                set_view_data()
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                'tbl_AddSubgroup.Visible = False
                'tbl_ModifySub.Visible = True
                Dim encObj As New Encryption64

                setModifyvalues(encObj.Decrypt(Request.QueryString("editid").Replace(" ", "+")))
            End If

            UtilityObj.beforeLoopingControls(Me.Page)
        End If
    End Sub
    Sub set_view_data()
        txtSubGroupcode.Attributes.Add("readonly", "readonly")
        txtSubgroupname.Attributes.Add("readonly", "readonly")
    End Sub

    Sub reset_view_data()
        txtSubGroupcode.Attributes.Remove("readonly")
        txtSubgroupname.Attributes.Remove("readonly")
        
    End Sub
    Private Sub bindlist()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "select * FROM ACCGRP_M"
            'ds = 
            DropDownListParentGroup.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            DropDownListParentGroup.DataTextField = "GPM_DESCR"
            DropDownListParentGroup.DataValueField = "GPM_ID"

            DropDownListParentGroup.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindlist_M()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "select * FROM ACCGRP_M"
            'ds = 
            DropDownListParentGroup.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            DropDownListParentGroup.DataTextField = "GPM_DESCR"
            DropDownListParentGroup.DataValueField = "GPM_ID"

            DropDownListParentGroup.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub



    'Protected Sub btnAddgroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddgroup.Click
    '    lblError.Text = ""
    '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    '    Dim objConn As New SqlConnection(str_conn)
    '    Dim str_type As String = ""
    '    objConn.Open()
    '    '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
    '    Try
    '        If rbBank.Checked = True Then
    '            str_type = "B"
    '        ElseIf RbNormal.Checked = True Then
    '            str_type = "N"
    '        Else
    '            str_type = "C"
    '        End If

    '        Dim str_success As String = fnDoGroupOperations("Insert", txtSubGroupcode.Text & "", txtSubgroupname.Text & "", DropDownListParentGroup.SelectedItem.Value, str_type)
    '        If (str_success = "0") Then

    '            'lblError.Text = displayMessage("254", 20, "A")
    '            'txtSubGroupcode.Text = ""
    '            'txtSubgroupname.Text = ""
    '            RbNormal.Checked = True
    '            Dim encObj As New Encryption64

    '            Response.Redirect("accSubGroupmaster.aspx?modified=" & encObj.Encrypt("254") & "&newid=" & encObj.Encrypt(txtSubGroupcode.Text) & "")

    '        Else
    '            lblError.Text = getErrorMessage(str_success)
    '        End If

    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub





    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "select * FROM ACCSGRP_M where SGP_ID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtSubGroupcode.Text = p_Modifyid
                txtSubgroupname.Text = ds.Tables(0).Rows(0)("SGP_DESCR")
                'DropDownListMParentGroup.SelectedIndex = DropDownListMParentGroup.Items.IndexOf(ds.Tables(0).Rows(0)("SGP_GPM_ID"))
                Dim i As Integer
                For i = 0 To DropDownListParentGroup.Items.Count - 1
                    If DropDownListParentGroup.Items(i).Value = ds.Tables(0).Rows(0)("SGP_GPM_ID") Then
                        DropDownListParentGroup.SelectedIndex = i
                        Exit For
                    End If
                Next
                If ds.Tables(0).Rows(0)("SGP_TYPE") = "C" Then
                    rbCash.Checked = True
                ElseIf ds.Tables(0).Rows(0)("SGP_TYPE") = "B" Then
                    rbBank.Checked = True
                ElseIf ds.Tables(0).Rows(0)("SGP_TYPE") = "N" Then
                    RbNormal.Checked = True
                End If
            Else
                Response.Redirect("accGroupMaster.aspx")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    'Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
    '    Try
    '        lblMError.Text = ""
    '        Dim str_type As String = ""
    '        If rbMNormal.Checked = True Then
    '            str_type = "N"
    '        ElseIf rbMCash.Checked = True Then
    '            str_type = "C"
    '        ElseIf rbMBank.Checked = True Then
    '            str_type = "B"
    '        End If
    '        If txtSubMgroupname.Text <> "" Then
    '            Dim str_success As String = fnDoGroupOperations("Update", txtSubMGroupcode.Text & "", txtSubMgroupname.Text & "", DropDownListMParentGroup.SelectedItem.Value & "", str_type & "")
    '            If (str_success = "0") Then
    '                lblMError.Text = getErrorMessage("266")
    '                Dim encObj As New Encryption64

    '                Response.Redirect("accSubGroupMaster.aspx?modified=" & encObj.Encrypt("258"))
    '            Else
    '                lblMError.Text = getErrorMessage(str_success)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try

    'End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Call clear_all()
        reset_view_data()
        viewstate("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
    End Sub
    Sub clear_all()
        txtSubGroupcode.Text = ""
        txtSubgroupname.Text = ""
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        viewstate("datamode") = "edit"
        txtSubGroupcode.Attributes.Add("readonly", "readonly")
        txtSubgroupname.Attributes.Remove("readonly")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Then
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
        If ViewState("datamode") = "edit" Then

            Call clear_all()
            'clear the textbox and set the default settings
            setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")))
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If viewstate("datamode") = "edit" Then

            Try
                lblError.Text = ""
                Dim str_type As String = ""
                If RbNormal.Checked = True Then
                    str_type = "N"
                ElseIf rbCash.Checked = True Then
                    str_type = "C"
                ElseIf rbBank.Checked = True Then
                    str_type = "B"
                End If
                If txtSubgroupname.Text <> "" Then
                    Dim str_success As String = MasterFunctions.fnDoSubGroupOperations("Update", txtSubGroupcode.Text & "", txtSubgroupname.Text & "", DropDownListParentGroup.SelectedItem.Value & "", str_type & "")
                    If (str_success = "0") Then
                        lblError.Text = getErrorMessage("266")
                        Dim encObj As New Encryption64
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSubGroupcode.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        viewstate("datamode") = Encr_decrData.Encrypt(viewstate("datamode"))
                        Response.Redirect("accSubGroupMaster.aspx?modified=" & encObj.Encrypt("258") & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
                    Else
                        lblError.Text = getErrorMessage(str_success)
                    End If
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        Else
            lblError.Text = ""
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim str_type As String = ""
            objConn.Open()
            '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                If rbBank.Checked = True Then
                    str_type = "B"
                ElseIf RbNormal.Checked = True Then
                    str_type = "N"
                Else
                    str_type = "C"
                End If

                Dim str_success As String = MasterFunctions.fnDoSubGroupOperations("Insert", txtSubGroupcode.Text & "", txtSubgroupname.Text & "", DropDownListParentGroup.SelectedItem.Value, str_type)
                If (str_success = "0") Then

                    'lblError.Text = displayMessage("254", 20, "A")
                    'txtSubGroupcode.Text = ""
                    'txtSubgroupname.Text = ""
                    RbNormal.Checked = True
                    Dim encObj As New Encryption64
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSubGroupcode.Text, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If



                    Response.Redirect("accSubGroupmaster.aspx?modified=" & encObj.Encrypt("254") & "&newid=" & encObj.Encrypt(txtSubGroupcode.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code"))

                Else
                    lblError.Text = getErrorMessage(str_success)
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim str_type As String = ""
        objConn.Open()
        '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
           

            If Not Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) Is Nothing Then
                Dim str_success As String = MasterFunctions.fnDoSubGroupOperations("Delete", Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) & "", "", "", "")
                If (str_success = "0") Then
                    lblError.Text = getErrorMessage("265")
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSubGroupcode.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    clear_all()
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class

