<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="AccAddBankTran.aspx.vb" Inherits="AccAddBankTran" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sds" Namespace="Telerik.Web.SessionDS" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td input[type=text], table td select {
            min-width: 20% !important;
        }
    </style>
    <telerik:RadScriptBlock runat="server" ID="rcripts">

        <script language="javascript" type="text/javascript">
            function narration_check(ctrl) {
                if (document.getElementById(ctrl).value.length > 300) {
                    alert('the Length of narration exeeds 300 characters. the extra characters will be teruncated');
                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }


            function getUtilityPaymentDetails() {

                var NameandCode;
                var result;
                url = "Utilitydetails.aspx?Docno=" + document.getElementById('<%=txtDocno.ClientID %>').value + "&DocDate=" + document.getElementById('<%=txtdocDate.ClientID %>').value + "&Lineno=" + document.getElementById('<%=h_NextLine.ClientID %>').value;
                result = radopen(url, "pop_up");
                //if (result == '' || result == undefined) {
                //    return false;
                //}
                //return false;

            }

            function getAddTransferDetails() {

                var NameandCode;
                var result;
                url = "TransferDetails.aspx?Docno=" + document.getElementById('<%=txtDocno.ClientID %>').value + "&DocDate=" + document.getElementById('<%=txtdocDate.ClientID %>').value + "&Bank=" + document.getElementById('<%=txtBankCode.ClientID %>').value + "&Lineno=" + document.getElementById('<%=h_NextLine.ClientID %>').value;
                result = radopen(url, "pop_up2");
                //if (result == '' || result == undefined) {
                //    return false;
                //}
                //return false;

            }

            function getPettyCash() {

                var NameandCode;
                var result;
                result = radopen("AccPettycashShow.aspx?rss=0", "pop_up3")
                <%--if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById("<%=txtPettycashId.ClientID %>").value = NameandCode[0];
        document.getElementById('<%=txtLineAmount.ClientID %>').value = NameandCode[1];
         document.getElementById('<%=Detail_ACT_ID.ClientID %>').value = NameandCode[3];
         document.getElementById('<%=txtPettycashdocno.ClientID %>').value = NameandCode[4];
         var remarks = 'Being the Cheque paid to.. ' + NameandCode[4] + '..towards :' + NameandCode[2] + ' :..from request..:'
         document.getElementById('<%=txtNarrn.ClientID %>').value = remarks;
        document.getElementById('<%=txtLineNarrn.ClientID %>').value = remarks;
         return false;--%>
            }

            function OnClientClose3(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtPettycashId.ClientID %>").value = NameandCode[0];
                    document.getElementById('<%=txtLineAmount.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=Detail_ACT_ID.ClientID %>').value = NameandCode[3];
                    document.getElementById('<%=txtPettycashdocno.ClientID %>').value = NameandCode[4];
                    var remarks = 'Being the Cheque paid to.. ' + NameandCode[4] + '..towards :' + NameandCode[2] + ' :..from request..:'
                    document.getElementById('<%=txtNarrn.ClientID %>').value = remarks;
                    document.getElementById('<%=txtLineNarrn.ClientID %>').value = remarks;
                }
            }


            function CopyDetails() {
                document.getElementById('<%=txtLineNarrn.ClientID %>').value = document.getElementById('<%=txtNarrn.ClientID %>').value;
            }



            function getCheque() {
                if (document.getElementById('<%=rbOthers.ClientID %>').checked == false)
                    popUp('460', '400', 'CHQBOOK_PDC', '<%=txtChqBook.ClientId %>', '<%=hCheqBook.ClientId %>', '<%=txtChqNo.ClientId %>', '<%=txtBankCode.ClientId %>', '<%=txtdocNo.ClientId %>');
            }


            function getBank() {
                popUp('460', '400', 'BANK', '<%=txtBankCode.ClientId %>', '<%=txtBankDescr.ClientId %>')
                document.getElementById('<%=txtChqBook.ClientID %>').value = '';
                document.getElementById('<%=txtChqNo.ClientID %>').value = '';
            }

            function getParty() {
                popUp('960', '600', 'NOTCC', '<%=Detail_ACT_ID.ClientId %>', '<%=txtPartyDescr.ClientId %>');
            }

            function getProvision() {
                popUp('460', '400', 'PREPDAC', '<%=txtProvCode.ClientId %>', '<%=txtProvDescr.ClientId %>');
            }
            function getCostUnit() {
                alert('Cost');
                var NameandCode;
                var result;
                url = 'accCostUnitDetails.aspx';
                radopen(url, "pop_up4");
                return false;
            }


            function AddDetails(url) {

                var NameandCode;
                var result;
                var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';

                dates = document.getElementById('<%=txtdocDate.ClientID %>').value;
                dates = dates.replace(/[/]/g, '-')
                url_new = url_new + '&dt=' + dates;
                result = radopen("acccpAddDetails.aspx?" + url_new, "")
                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById('<%=Detail_ACT_ID.ClientID %>').focus();
                return false;
            }

            function Settle_Online() {

                var NameandCode;
                var result;
                var pId = 1;//h_NextLine alert()       
                if (pId == 1) {
                    url = "ShowOnlineSettlement.aspx?actid=" + document.getElementById('<%=Detail_ACT_ID.ClientID %>').value + "&lineid=" + document.getElementById('<%=h_NextLine.ClientID %>').value + "&docno=" + document.getElementById('<%=txtdocNo.ClientID %>').value + "&dt=" + document.getElementById('<%=txtdocDate.ClientID %>').value;
                }

                result = radopen(url, "pop_up5");
              <%--  if (result == '' || result == undefined) {
                    return false;
                }

                document.getElementById('<%=txtLineAmount.ClientID %>').value = result;
                return true;--%>
            }

            function OnClientClose5(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtLineAmount.ClientID %>').value = arg.NameandCode;
                }
            }

            function HideAll() {

                document.getElementById('<%= pnlCostUnit.ClientID %>').style.display = 'none';
                return false;
            }

            function getRoleID(mode) {

                var NameandCode;
                var result;
                var url;
                url = 'accShowEmpPP.aspx?id=' + mode;
                if (mode == 'CU') {
                    result = radopen(url, "pop_up6");

<%--                    if (result == '' || result == undefined)
                    { return false; }
                    NameandCode = result.split('___');
                    document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];
                    document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[4];
                    document.getElementById("<%=txtExpCode.ClientID %>").value = NameandCode[5];--%>
                }
            }

            function OnClientClose6(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];
                    document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[4];
                    document.getElementById("<%=txtExpCode.ClientID %>").value = NameandCode[5];
                }
            }


            function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

                var lstrVal;
                var lintScrVal;
                var NameandCode;
                var result;
                document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
                document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
                document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
                document.getElementById("<%=hd_ctrl2.ClientID %>").value = ctrl2;
                document.getElementById("<%=hd_ctrl3.ClientID%>").value = ctrl3;


                if (pMode == 'CHQBOOK_PDC') {
                    if (document.getElementById(ctrl3).value == "") {
                        alert("Please Select The Bank");
                        return false;
                    }

                    result = radopen("ShowChqs.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "pop_up7");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');

                    //document.getElementById(ctrl).value = lstrVal[1];
                    //document.getElementById(ctrl1).value = lstrVal[0];
                    //document.getElementById(ctrl2).value = lstrVal[2];
                    //document.getElementById(ctrl).focus();

                }
                else if (pMode == 'BANK') {
                    result = radopen("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up7");
                    //if (result == '' || result == undefined) {
                    //    return false;
                    //    lstrVal = result.split('||');
                    //    document.getElementById(ctrl).value = lstrVal[0];
                    //    document.getElementById(ctrl1).value = lstrVal[1];

                    //}
                }

                else if (pMode == 'NOTCC') {
                    if (ctrl2 == '' || ctrl2 == undefined) {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up7");
                    } else {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "pop_up7");
                    }
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];


                }

                else if (pMode == 'PREPDAC') {


                    result = radopen("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up7");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];
                    //if (ctrl2 != '')
                    //    if (document.getElementById(ctrl2).value == '') {
                    //        document.getElementById(ctrl2).value = lstrVal[2];
                    //        document.getElementById(ctrl3).value = lstrVal[3];
                    //    }
                }
                else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                    QRYSTR = '';
                    if (pMode == 'CASHFLOW_BP')
                        QRYSTR = "?rss=0";
                    if (pMode == 'CASHFLOW_BR')
                        QRYSTR = "?rss=1";

                    result = radopen("acccpShowCashflow.aspx" + QRYSTR, "pop_up7");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('__');
                    //document.getElementById(ctrl).value = lstrVal[0];
                }
            }

            function OnClientClose7(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                    var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                    var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                    var ctrl2 = document.getElementById("<%=hd_ctrl2.ClientID %>").value;
                    var ctrl3 = document.getElementById("<%=hd_ctrl3.ClientID%>").value;
                    if (pMode == 'CHQBOOK_PDC') {
                        document.getElementById(ctrl).value = NameandCode[1];
                        document.getElementById(ctrl1).value = NameandCode[0];
                        document.getElementById(ctrl2).value = NameandCode[2];
                        document.getElementById(ctrl).focus();
                    }
                    else if (pMode == 'BANK') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }
                    else if (pMode == 'NOTCC') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }
                    else if (pMode == 'PREPDAC') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }
                    else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                        document.getElementById(ctrl).value = NameandCode[0];
                    }
                }
            }


        </script>

    </telerik:RadScriptBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up6" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose6"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up7" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose7"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Bank Payment
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <asp:HiddenField ID="hd_ctrl2" runat="server" />
                <asp:HiddenField ID="hd_ctrl3" runat="server" />

                <table width="100%" align="center" border="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center" style="border-collapse: collapse">

                    <tr>
                        <td width="20%" align="left"><span class="field-label">Doc No [Old Ref No] <span class="text-danger">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocNo" runat="server" Width="45%"></asp:TextBox>[
                <asp:TextBox ID="txtOldDocNo" runat="server" Width="35%"></asp:TextBox>
                            ]
                        </td>
                        <td width="20%" align="left"><span class="field-label">Doc Date <span class="text-danger">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True" Width="80%"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Bank Account <span class="text-danger">*</span></span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtBankCode" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getBank();return false;" />
                            <asp:TextBox ID="txtBankDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Provision Account </span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtProvCode" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgProv" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getProvision();return false;" />
                            <asp:TextBox ID="txtProvDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Petty Cash Ref </span>
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtPettycashId" runat="server" Width="20%"></asp:TextBox>
                            <asp:ImageButton
                                ID="ImageButton2" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getPettyCash(); return false;"
                                TabIndex="14"></asp:ImageButton>
                            <asp:TextBox ID="txtPettycashdocno" runat="server"
                                Width="60%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Currency </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="True" Width="20%">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtExchRate" runat="server" Width="60%"></asp:TextBox>
                        </td>
                        <td width="20%" align="left"><span class="field-label">Group Rate </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLocalRate" runat="server" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Received By </span>
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtReceivedby" runat="server" Width="80%"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:CheckBox ID="ChkBearer" runat="server" Text="Bearer Cheque" CssClass="field-label" />

                            <asp:CheckBox ID="chkAdvance" runat="server" Text="Advance" AutoPostBack="True" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Narration <span class="text-danger">*</span></span>
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtNarrn" runat="server" Width="80%" TextMode="MultiLine"
                                MaxLength="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td colspan="4" align="left"><span class="field-label">Debit Details </span>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Paid To <span class="text-danger">*</span></span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="Detail_ACT_ID" runat="server" Width="20%" AutoCompleteType="Disabled"
                                AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgParty" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="getParty();" />
                            <asp:TextBox ID="txtPartyDescr" runat="server" Width="60%"></asp:TextBox><br />
                            <ajaxToolkit:PopupControlExtender ID="pceCostUnit" runat="server" PopupControlID="pnlCostUnit"
                                Position="Bottom" TargetControlID="lnkCostUnit">
                            </ajaxToolkit:PopupControlExtender>
                            <asp:LinkButton ID="lnkCostUnit" runat="server" OnClick="lnkCostUnit_Click">Specify CostUnit Details</asp:LinkButton>
                        </td>
                    </tr>
                    <tr runat="server" id="trPayTerm">
                        <td align="left" style="border: none;" colspan="4">
                            <asp:Label ID="lblPaymentTerm" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Narration </span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtLineNarrn" runat="server" TextMode="MultiLine" Width="80%"></asp:TextBox>
                            <asp:Button ID="btnTRNSDetails" Visible="false" runat="server" CssClass="button" Text="Add Transfer Details" OnClientClick="getAddTransferDetails(); return false;" CausesValidation="False" TabIndex="12" />
                            <asp:Button ID="btnUtilitPaymentDetails" Visible="false" runat="server" CssClass="button" Text="Utility Payments" OnClientClick="getUtilityPaymentDetails(); return false;" CausesValidation="False" TabIndex="12" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" contenteditable="true"><span class="field-label">Cheque/Refrence </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:RadioButton ID="rbCheque" runat="server" Checked="True" GroupName="chq" Text="Cheque"
                                CssClass="field-label" AutoPostBack="True" />

                            <asp:RadioButton ID="rbOthers" runat="server" GroupName="chq" Text="Other Instruments"
                                CssClass="field-label" AutoPostBack="True" />
                        </td>
                        <td width="20%" align="left"><span class="field-label">Ref. No. </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtrefChequeno" runat="server" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Cheque Book Lot <span class="text-danger">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtChqBook" runat="server" Width="54.5%"></asp:TextBox>
                            <a href="#" onclick="getCheque();return false;">
                                <img border="0" src="../Images/cal.gif" id="IMG2" language="javascript" runat="server" /></a>
                            <asp:TextBox ID="txtChqNo" runat="server" Width="20%"></asp:TextBox>
                        </td>
                        <td width="20%" align="left"><span class="field-label">Chq/Ref Date <span class="text-danger">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtChqDate" runat="server" Width="80%"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Cash Flow Line <span class="text-danger">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtCashFlow" runat="server" Width="80%"></asp:TextBox>
                            <asp:ImageButton ID="imgCashFlow" runat="server" ImageUrl="../Images/cal.gif" />
                        </td>
                        <td width="20%" align="left"><span class="field-label">Amount <span class="text-danger">*</span>
                        </span>
                            <asp:LinkButton ID="lbSettle" runat="server" OnClientClick="Settle_Online(); return false;">(Settle)</asp:LinkButton></td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLineAmount" runat="server" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trTaxType" visible="false">
                        <td width="20%" align="left"><span class="field-label">TAX Type</span></td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddlVATCode" runat="server"></asp:DropDownList></td>
                        <td width="20%" align="left"></td>
                        <td width="30%" align="left"></td>
                    </tr>

                    <tr runat="server" id="tr_UploadEmplyeeCostCenter" visible="false">
                        <td width="20%" align="left"><span class="field-label">Upload Employees </span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:FileUpload ID="fuEmployeeData" runat="server" />
                            <asp:LinkButton ID="lbUploadEmployee" runat="server">Upload</asp:LinkButton>
                        </td>
                    </tr>
                    <tr runat="server" id="tr_SubLedger" visible="false">
                        <td width="20%" align="left"><span class="field-label">Select Subledger </span>
                        </td>
                        <td align="left">
                            <telerik:RadComboBox ID="CostCenterIDRadComboBoxMain" runat="server" DataSourceID="sdsSubledgerDefaultmpty"
                                DataTextField="ASM_NAME" DataValueField="ASM_ID">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Cost
                Allocation </span>
                        </td>
                        <td align="left" colspan="3">
                            <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnFill" runat="server" Text="Add" CssClass="button" />
                            <asp:Button ID="btnFillCancel" runat="server" Text="Cancel" OnClick="btnFillCancel_Click" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide Orders for Customer <%# Eval("id") %>"
                                                    border="0" src="../Images/Misc/plus.gif" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemStyle Width="1%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField ReadOnly="True" DataField="Accountid" HeaderText="Acount Code">
                                        <ItemStyle Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Account Name">
                                        <ItemStyle Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Narration" HeaderText="Narration">
                                        <ItemStyle Width="20%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLine" HeaderText="CashFlow">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField SortExpression="Amount" HeaderText="Amount">
                                        <ItemStyle Width="5%" HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" SkinID="Grid" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="ChqBookId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqBookId" runat="server" Text='<%# Bind("ChqBookId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ChqBookLot" HeaderText="Cheque Book">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="ChqNo">
                                        <ItemStyle Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("ChqNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ChqDate">
                                        <ItemStyle Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqDate" runat="server" Text='<%# Bind("ChqDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TaxCode" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxCode" runat="server" Text='<%# Bind("TaxCode")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEdit" OnClick="lbEdit_Click" runat="server" Text="Edit" CausesValidation="false"
                                                CommandName="Edits"> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="DeleteBtn" CommandArgument='<%# Eval("id") %>' CommandName="Delete"
                                                runat="server">
         Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="100%" align="right">
                                                    <div id="div<%# Eval("id") %>" style="display: none; position: relative; left: 15px; overflow: auto; width: 97%">
                                                        <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated" CssClass="table table-bordered table-row"
                                                            OnRowDataBound="gvCostchild_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIdCostchild" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                                                <asp:BoundField DataField="CostCenterTypeName" HeaderText="Costcenter" />
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Allocated"
                                                                    HtmlEncode="False" InsertVisible="False" SortExpression="Amount" Visible="false">
                                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Earn Code" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:TemplateField HeaderText="Amount">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtAmt" runat="server" CssClass="inputbox" Visible="false" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                                            onblur="CheckAmount(this)"></asp:TextBox>
                                                                        <asp:Label ID="txtAmt0" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td colspan="100%" align="right">
                                                                                <asp:GridView ID="gvCostAllocation" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated" CssClass="table table-bordered table-row"
                                                                                    Width="100%" OnRowDataBound="gvCostchild_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblIdCostAllocation" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="ASM_ID" HeaderText="Cost Allocation" />
                                                                                        <asp:BoundField DataField="ASM_NAME" HeaderText="Name" />
                                                                                        <asp:TemplateField HeaderText="Amount">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="txtAmt1" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><span class="field-label">Total <span style="color: red">*</span> </span>
                            <asp:TextBox ID="txtAmount" runat="server" Width="20%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Signed By  </span></td>
                        <td align="left">
                            <asp:TextBox ID="txtSignedBy" runat="server" MaxLength="50" Width="80%">
                            </asp:TextBox>
                        </td>
                        <td align="left">
                            <span class="field-label">Designation  </span></td>
                        <td align="left">
                            <asp:TextBox ID="txtDesignation" runat="server" MaxLength="50" Width="80%">
                            </asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" />
                            <asp:Button ID="btnSettle" runat="server" CssClass="button" Text="Settle" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:CheckBox ID="chkPrintChq" runat="server" CssClass="radiobutton" Text="Print Cheque" />
                        </td>
                    </tr>
                </table>
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <input id="h_NextLine" runat="server" type="hidden" /><br />
                <asp:HiddenField ID="hCheqBook" runat="server" />
                <asp:HiddenField ID="hNum" runat="server" Value="1" />
                <asp:HiddenField ID="hfId1" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtdocDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="ChqDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton3" TargetControlID="txtChqDate">
                </ajaxToolkit:CalendarExtender>
                <br />
                <asp:Panel ID="pnlCostUnit" runat="server" Style="display: none" CssClass="panel-cover">
                    <table align="center" border="0" width="100%">
                        <tr>
                            <td align="left" colspan="4"><span class="field-label">CostUnitDetails  </span>
                                <span class="float-right">
                                    <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/Curriculum/btnCloseblack.png"
                                        Style="text-align: right" TabIndex="54" OnClientClick="return HideAll();"></asp:ImageButton></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1"><span class="field-label">Period From  </span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFrom" runat="server" TabIndex="15" AutoPostBack="True" Width="80%"
                                    OnTextChanged="txtFrom_TextChanged"></asp:TextBox><asp:ImageButton ID="imgFrom"
                                        runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20"></asp:ImageButton>
                            </td>
                            <td align="left"><span class="field-label">To  
                            </span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTo" runat="server" TabIndex="15" AutoPostBack="True" Width="80%"
                                    OnTextChanged="txtTo_TextChanged"></asp:TextBox><asp:ImageButton ID="imgTo"
                                        runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1"><span class="field-label">CostUnit </span>
                            </td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtCostUnit" runat="server" Width="80%"
                                    Enabled="False"></asp:TextBox>
                                <asp:ImageButton ID="btnPrePaid" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getRoleID('CU');return false;"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Expense Account  </span>
                            </td>
                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtExpCode" runat="server" widt="40%"
                                    Enabled="False"></asp:TextBox>
                                <asp:TextBox ID="txtPreAcc" runat="server" Width="40%" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender1" runat="server" BehaviorID="100-calendarButtonExtender1"
                                    CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender2" runat="server" BehaviorID="100-calendarButtonExtender2"
                                    CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:SqlDataSource ID="sdsSubledgerDefaultmpty" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="SELECT '' AS ASM_ID, '' AS ASM_NAME, 1 AS ASL_bDefault UNION SELECT ASM_ID, ASM_NAME,ASL_bDefault  FROM ACCOUNTS_SUB_ACC_M_list WHERE (ASL_ACT_ID = @ASL_ACT_ID) ORDER BY ASL_bDefault DESC, ASM_NAME">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="Detail_ACT_ID" Name="ASL_ACT_ID" PropertyName="Text" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </div>
        </div>
    </div>
</asp:Content>
