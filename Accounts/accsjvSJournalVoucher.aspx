<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accsjvSJournalVoucher.aspx.vb" Inherits="Accounts_accsjvSJournalVoucher" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sds" Namespace="Telerik.Web.SessionDS" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
            table td input[type=text],table td select {
                min-width:20% !important;
            }
        </style>

    <telerik:RadScriptBlock ID="rcripts" runat="server">
        <script language="javascript" type="text/javascript">
            function expandcollapse(obj, row) {
                var div = document.getElementById(obj);
                var img = document.getElementById('img' + obj);

                if (div.style.display == "none") {
                    div.style.display = "block";
                    if (row == 'alt') {
                        img.src = "../images/Misc/minus.gif";
                    }
                    else {
                        img.src = "../images/Misc/minus.gif";
                    }
                    img.alt = "Close to view other Customers";
                }
                else {
                    div.style.display = "none";
                    if (row == 'alt') {
                        img.src = "../images/Misc/plus.gif";
                    }
                    else {
                        img.src = "../images/Misc/plus.gif";
                    }
                    img.alt = "Expand to show Orders";
                }
            }

            function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {


                var lstrVal;
                var lintScrVal;


                var NameandCode;
                var result;
                if (pMode == 'NORMAL') {
                    document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
                    document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
                    document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");
                    //if (result=='' || result==undefined)
                    //{    return false;      } 
                    //lstrVal=result.split('||');     
                    //document.getElementById(ctrl).value=lstrVal[0];
                    //document.getElementById(ctrl1).value=lstrVal[1];
                    //document.getElementById(ctrl2).value=lstrVal[2];
                    //document.getElementById(ctrl3).value=lstrVal[3];

                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                    var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                    var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                    if (pMode == 'NORMAL') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1]
                    }

                }
            }

            function getAccount() {
                popUp('960', '600', 'NORMAL', '<%=txtDAccountCode.ClientId %>', '<%=txtDAccountName.ClientId %>');
                return false;
            }
            function CopyDetails() {
                try {
                    if (document.getElementById('<%=txtDNarration.ClientID %>').value == '')
                        document.getElementById('<%=txtDNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
                }
                catch (ex) { }
            }

            function AddDetails(url) {

                var NameandCode;
                var result;
                var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
                //alert(url_new);
                dates = document.getElementById('<%=txtHDocdate.ClientID %>').value;
                dates = dates.replace(/[/]/g, '-')
                url_new = url_new + '&dt=' + dates;
                result = radopen("acccpAddDetails.aspx?" + url_new, "pop_up2" )
               <%-- if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById('<%=txtDAccountCode.ClientID %>').focus();
                return false;--%>
            }

             function OnClientClose2(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtDAccountCode.ClientID %>').focus();
                }
            }


            function getOther() {
                document.getElementById('<%=h_mode.ClientID %>').value = 'others';

                var NameandCode;
                var result;
                sFeatures = "";
                var url = "jvPickDetails.aspx?ccsmode=others";
                // alert(url);
                result = radopen(url, "pop_up3")
                return false;
            }


        </script>
    </telerik:RadScriptBlock>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
         <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
             </telerik:RadWindow>
          </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
         </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Self Reversing Journal
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <table align="center" width="100%" border="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" class="BlueTable" width="100%">

                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Doc No [Old Ref No]</span></td>

                                    <td align="left"  width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" Width="40%" ReadOnly="True"></asp:TextBox>
                                        [
                                <asp:TextBox ID="txtHOldrefno" runat="server" Width="39%" TabIndex="2"></asp:TextBox>
                                        ]</td>
                                    <td align="left" width="20%"><span class="field-label">Doc Date</span></td>

                                    <td  width="30%" align="left">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True" Width="80%"
                                            TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="6" /></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Reverse On</span></td>

                                    <td align="left" width="30%" ><asp:TextBox ID="txtRevDate" runat="server" AutoPostBack="True" Width="80%"
                                        TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="6" /></td>

                                
                                    <td align="left" width="20%"><span class="field-label">Currency</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True"
                                            Width="80%" TabIndex="8">
                                        </asp:DropDownList></td>
                                    </tr>
                                <tr>
                                    <td align="left" width="20%">
                                      <span class="field-label">  Exchange Rate </span>
                                    </td>
                                    <td  width="30%" align="left">
                                        <asp:TextBox ID="txtHExchRate" runat="server"
                                            Width="80%"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Group Rate</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHLocalRate" runat="server"
                                            Width="80%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>

                                    <td  width="30%" align="left">
                                        <asp:TextBox ID="txtHNarration" runat="server" TextMode="MultiLine" TabIndex="10" Width="80%"> </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle">Details
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Account</span></td>

                                    <td colspan="2" align="left">
                                        <asp:TextBox ID="txtDAccountCode" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDAccountCode"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:ImageButton
                                            ID="btnHAccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;" TabIndex="12" />
                                        <asp:TextBox ID="txtDAccountName" runat="server" Width="60%"></asp:TextBox></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Amount</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" Width="80%" TabIndex="14" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDAmount"
                                            Display="Dynamic" ErrorMessage="Amount Should be Valid" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                            ValidationGroup="Details">*</asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                               
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>

                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtDNarration" runat="server" Width="80%"
                                            TextMode="MultiLine"  TabIndex="16"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDNarration"
                                            ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                    </td>
                                </tr>
                                <tr runat="server" id="tr_UploadEmplyeeCostCenter" visible="false">
                                    <td width="20%" align="left"><span class="field-label">Upload Employees </span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:FileUpload ID="fuEmployeeData" runat="server" />
                                    </td>
                                    <td width="20%" align="left"><span class="field-label">Select Subledger </span>
                                    </td>
                                    <td width="30%" align="left">
                                        <telerik:RadComboBox ID="CostCenterIDRadComboBoxMain" runat="server" DataSourceID="sdsSubledgerDefaultmpty"
                                            DataTextField="ASM_NAME" DataValueField="ASM_ID">
                                        </telerik:RadComboBox>
                                        <asp:LinkButton ID="lbUploadEmployee" runat="server">Upload</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Cost Allocation </span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <!-- content start -->
                                        <!-- content end -->
                                        <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                                        <asp:SqlDataSource ID="sdsSubledgerDefaultmpty" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                            SelectCommand="SELECT '' AS ASM_ID, '' AS ASM_NAME, 1 AS ASL_bDefault UNION SELECT ASM_ID, ASM_NAME,ASL_bDefault  FROM ACCOUNTS_SUB_ACC_M_list WHERE (ASL_ACT_ID = @ASL_ACT_ID) ORDER BY ASL_bDefault DESC, ASM_NAME">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="txtDAccountCode" Name="ASL_ACT_ID" PropertyName="Text" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdds" runat="server" CssClass="button" Text="Add" ValidationGroup="Details"
                                            TabIndex="18"  />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="20"
                                             />
                                        <asp:Button ID="btnEditCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="22"
                                             />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="center" colspan="4" >
                                        <br />
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Transaction details added yet." Width="100%">
                                            <Columns>
                                                <asp:TemplateField Visible="False" HeaderText="id">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>' __designer:wfdid="w1"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField ReadOnly="True" DataField="Accountid" HeaderText="Account Code"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Debit">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDebit" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("debit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Credit">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCredit" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("credit")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        Edit                                            
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" runat="server" Text="Edit" CausesValidation="false" __designer:wfdid="w2" CommandName="Edits"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ShowDeleteButton="True" HeaderText="Delete">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:CommandField>
                                                <asp:BoundField DataField="CostCenter" Visible="False" HeaderText="TEST"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Allocate" ShowHeader="False" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnAlloca" runat="server" Text="Allocate" CausesValidation="false" __designer:wfdid="w3" CommandName=""></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False" HeaderText="Cost Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRequired" runat="server" __designer:wfdid="w4" Visible="False"></asp:Label>
                                                        width="20%"
                                                <asp:LinkButton ID="lbAllocate" OnClick="lbAllocate_Click" runat="server" __designer:wfdid="w5" Visible="False">Allocate</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"><span class="field-label">Debit Total </span>
                                <asp:TextBox ID="txtTDotalDebit" runat="server" Width="20%" ReadOnly="True"></asp:TextBox>
                                       <span class="field-label"> Credit Total </span>
                                        <asp:TextBox ID="txtTotalCredit"
                                            runat="server" Width="20%" ReadOnly="True"></asp:TextBox>
                                       <span class="field-label"> Difference </span>
                                        <asp:TextBox ID="txtDifference" runat="server"
                                            Width="20%" ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" ValidationGroup="Details" TabIndex="22" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="24" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="26" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" TabIndex="28" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="30" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print"
                                            TabIndex="32" />
                                        <br />
                                        <input id="h_Memberids" runat="server" type="hidden" />
                                        <input id="h_mode" runat="server" type="hidden" />
                                        <input id="h_editorview" runat="server" type="hidden" value="" />
                                        <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" TargetControlID="txtRevDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

