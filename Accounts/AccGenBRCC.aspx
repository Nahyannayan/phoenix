<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccGenBRCC.aspx.vb" Inherits="AccGenBRCC" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <title>Accounts</title>
    <link rel="stylesheet" type="text/css" href="../cssfiles/sb-admin.css" />
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.css" />
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="../vendor/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript">
        function getDate(pId) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("calendar.aspx", "", sFeatures)
            if (result == '' || result == undefined) {
                //            document.getElementById("txtDate").value=''; 
                return false;
            }


            return true;
        }

    </script>
    <script language="javascript">



        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";
            }
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <!--oncontextmenu="return false"-->
</head>
<body style="margin-top: 0px; margin-left: 0px; margin-right: 0px;" onload="listen_window();">


    <form id="form1" runat="server">

        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
        <input id="h_SelectedId" runat="server" type="hidden" />
        <br />
        <table align="center" width="100%">
            <tr>
                <td align="center" width="100%">
                    <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row"
                        AutoGenerateColumns="False" DataKeyNames="VHH_DOCNO"
                        OnRowDataBound="gvJournal_RowDataBound"
                        Width="100%" AllowPaging="True" EmptyDataText="Please Select the Card">

                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <a href="javascript:switchViews('div<%# Eval("VHH_DOCNO") %>', 'one');">
                                        <img id="imgdiv<%# Eval("VHH_DOCNO") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                    </a>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <a href="javascript:switchViews('div<%# Eval("VHH_DOCNO") %>', 'alt');">
                                        <img id="imgdiv<%# Eval("VHH_DOCNO") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                    </a>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="VHH_DOCNO" HeaderText="Doc No" ReadOnly="True"
                                SortExpression="VHH_DOCNO" />
                            <asp:BoundField DataField="VHH_DOCDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc Date"
                                HtmlEncode="False" SortExpression="VHH_DOCDT">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField DataField="NARRATION" HeaderText="Narration" SortExpression="NARRATION" />


                            <asp:TemplateField HeaderText="Total Transaction">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("VHH_AMOUNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Right" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="DeleteBtn"
                                        CommandArgument='<%# Eval("VHH_DOCNO") %>'
                                        CommandName="Delete" runat="server">
         Delete</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    </td></tr>
                               <tr>
                                   <td colspan="100%">
                                       <div id="div<%# Eval("VHH_DOCNO") %>" style="display: none; position: relative; left: 20px;">
                                           <asp:GridView ID="GridView2" runat="server" Width="80%" CssClass="table table-bordered table-row"
                                               AutoGenerateColumns="false" DataKeyNames="GUID"
                                               EmptyDataText="No orders for this customer.">

                                               <Columns>
                                                   <asp:BoundField DataField="VHD_ACT_ID" HeaderText="Account Id" HtmlEncode="False">
                                                       <ItemStyle HorizontalAlign="Center" />
                                                   </asp:BoundField>
                                                   <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" HtmlEncode="False">
                                                       <ItemStyle HorizontalAlign="Left" />
                                                   </asp:BoundField>
                                                   <asp:BoundField DataField="VHD_AMOUNT" HeaderText="Amount">
                                                       <ItemStyle HorizontalAlign="right" />
                                                   </asp:BoundField>
                                               </Columns>
                                               <RowStyle CssClass="griditem" Height="20px" Font-Names="Verdana" />
                                               <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                           </asp:GridView>
                                       </div>
                                   </td>
                               </tr>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <HeaderStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <SelectedRowStyle />
                        <PagerStyle HorizontalAlign="Left" />
                        <EmptyDataRowStyle />

                    </asp:GridView>
                    <br />

                    <br />
                    <br />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:mainDB %>"
                        SelectCommand="SELECT * FROM [VOUCHER_H] WHERE VHH_DOCTYPE='CR' AND VHH_bPOsted=0 ORDER BY VHH_DOCDT "></asp:SqlDataSource>

                </td>
            </tr>
        </table>
        <table id="Table1" runat="server" align="center" width="100%">

            <tr>
                <td align="left" width="20%"><span class="field-label">Total </span></td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
                <td align="left" width="20%"><span class="field-label">Bank Charges</span> </td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtBankCharges" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Done" />
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
