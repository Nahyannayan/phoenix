Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class AccGenBRCC
    Inherits System.Web.UI.Page



    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then
            lblError.Text = ""
            '''''check menu rights
            Session("MainMnu_code") = "A200014"
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")



            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            '   ----  Initalize the Cost Center Grid
            If Session("gdtSub") Is Nothing Then
                Session("gdtSub") = CreateDataTable()
            End If
            '
            gridbind()
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cDocNo As New DataColumn("VHH_DOCNO", System.Type.GetType("System.String"))
            Dim cDocDt As New DataColumn("VHH_DOCDT", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("VHH_AMOUNT", System.Type.GetType("System.String"))
            Dim cNarrn As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cDocNo)
            dtDt.Columns.Add(cDocDt)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cNarrn)
            dtDt.Columns.Add(cStatus)

            'Session("gdtSub").Columns.Add(cDocNo)
            'Session("gdtSub").Columns.Add(cDocDt)
            'Session("gdtSub").Columns.Add(cAmount)

            'Session("gdtSub").Columns.Add(cNarrn)
            'Session("gdtSub").Columns.Add(cStatus)




            'Return Session("gdtSub")
            Return dtDt
        Catch ex As Exception
            'Return Session("gdtSub")
            Return dtDt
        End Try
    End Function
    Private Sub CalcuTot()
        Dim ldblTot As Decimal
        Dim ldblBankCharges As Decimal

        Dim i As Integer

        ldblTot = 0
        For i = 0 To Session("gdtSub").Rows.Count - 1
            If (Session("gdtSub").Rows(i)("Status") <> "DELETED") Then
                ldblTot = ldblTot + Session("gdtSub").Rows(i)("VHH_AMOUNT")
            End If
        Next
        ldblBankCharges = ldblTot * (Session("cCardComsn") / 100)
        txtAmount.Text = Convert.ToString(ldblTot)
        txtBankCharges.Text = Convert.ToString(Math.Round(ldblBankCharges, 2))
    End Sub

    Protected Sub gridbind()
        'Dim ds As New DataSet
        'Dim str_Sql As String
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString


        'str_Sql = "SELECT Distinct A.*,'' as Status FROM [VOUCHER_H] A INNER JOIN VOUCHER_D B ON A.VHH_BSU_ID=B.VHD_BSU_ID AND A.VHH_DOCNO=B.VHD_DOCNo WHERE  VHH_DOCTYPE='CC' AND VHH_DOCDT='" & Session("sTranDt") & "' AND VHH_bPOsted='True'  AND VHH_CRR_ID='" & Session("sCardType") & "' AND isNull(VHD_bBankReceipt,0)=0 "
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        'Session("gdtSub") = ds.Tables(0)
        'gvJournal.DataSource = ds
        'gvJournal.DataBind()

        Dim dv As New DataView(Session("gdtSub"))
        dv.RowFilter = "Status<>'DELETED' "

        gvJournal.DataSource = dv
        gvJournal.DataBind()

        CalcuTot()
    End Sub






    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gv As GridView = e.Row.FindControl("GridView2")
            Dim dbSrc As New SqlDataSource
            dbSrc.ConnectionString = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            dbSrc.SelectCommand = "SELECT A.*,C.ACT_NAME FROM VOUCHER_D A INNER JOIN ACCOUNTS_M C ON A.VHD_ACT_ID=C.ACT_ID WHERE VHD_DOCTYPE='CC' AND VHD_DOCNO = '" & e.Row.DataItem("VHH_DOCNO").ToString & "' AND VHD_BSU_ID='" & Session("sBsuid") & "'"

            gv.DataSource = dbSrc
            gv.DataBind()
        End If
    End Sub
    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        Dim iRemove As Integer = 0
        Dim categoryID As String = gvJournal.DataKeys(e.RowIndex).Value
        For iRemove = 0 To Session("gdtSub").Rows.Count - 1
            If (Session("gdtSub").Rows(iRemove)("VHH_DOCNo") = categoryID) Then
                Session("gdtSub").Rows(iRemove)("Status") = "DELETED"
                Exit For
            End If
        Next


        Dim dv As New DataView(Session("gdtSub"))
        dv.RowFilter = "Status<>'DELETED' "

        gvJournal.DataSource = dv
        gvJournal.DataBind()

        CalcuTot()

    End Sub
    Private Function CreateDatesTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cDocDT As New DataColumn("DocDate", System.Type.GetType("System.DateTime"))





            dtDt.Columns.Add(cDocDT)




            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function
    Private Function CreateRecTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))

            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))





            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)




            dtDt.Columns.Add(cAmount)





            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & txtAmount.Text & "||" & txtBankCharges.Text & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")
        'h_SelectedId.Value = "Close"

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & txtAmount.Text & "||" & txtBankCharges.Text.Replace("'", "\'") & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & txtAmount.Text & "||" & txtBankCharges.Text.Replace("'", "\'") & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
        h_SelectedId.Value = "Close"

    End Sub
End Class
