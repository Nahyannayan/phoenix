<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accChqCancellation.aspx.vb" Inherits="Accounts_accChqCancellation" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function CallPopup() {
            //PopUpCall('460', '400', 'BANK', '<%=txtBankCode.ClientId %>', '<%=txtBankDescr.ClientId %>');
            //result = window.showModalDialog("PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientId %>').value, "", sFeatures);           
            var result = radopen("PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientId %>').value, "pop_up2")

        }

        function getCheque() {
            var sFeatures;
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            if (document.getElementById('<%=txtBankCode.ClientId %>').value == "") {
               alert("Please Select The Bank");
               return false;
           }
           //result = window.showModalDialog("ShowChqsPDC.aspx?ShowType=PDCCHQBOOK&BankCode=" + document.getElementById('<%=txtBankCode.ClientId %>').value + "&MonthsReq=1&docno=1", "", sFeatures);
           result = radopen("ShowChqsPDC.aspx?ShowType=PDCCHQBOOK&BankCode=" + document.getElementById('<%=txtBankCode.ClientId %>').value + "&MonthsReq=1&docno=1", "pop_up3")
            <%--if (result == '' || result == undefined)
            { return false; }
            lstrVal = result.split('||');
            document.getElementById('<%=txtChqBook.ClientID %>').value = lstrVal[0];
            document.getElementById('<%=txtChqNo.ClientID %>').value = lstrVal[1];
            //       document.getElementById(ctrl).focus();--%>
       }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientId %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1];
                document.getElementById('<%=txtChqBook.ClientID %>').value = '';
                document.getElementById('<%=txtChqNo.ClientID %>').value = '';

            }
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtChqBook.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtChqNo.ClientID %>').value = NameandCode[1];
                //document.getElementById(ctrl).focus();
                <%-- document.getElementById('<%=h_ACTIDs.ClientID%>').value = arg.NameandCode;
                document.forms[0].submit();
                //__doPostBack('<%= h_ACTIDs.ClientID%>', 'ValueChanged');--%>
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cheque Cancellation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="90%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="CHQ_CANCEL" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Bank</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankCode" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="CallPopup(); return false;" /></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBankDescr"
                                ErrorMessage="Bank Required" ValidationGroup="CHQ_CANCEL">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Cheque Lot</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtChqBook" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCHQAllote" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCheque(); return false;" /></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtChqNo"
                                ErrorMessage="Cheque Lot Required" ValidationGroup="CHQ_CANCEL">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvGroup1" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                DataKeyNames="CHB_ID" EmptyDataText="No Data" PageSize="20" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" type="checkbox"
                                                value='<%# Bind("CHD_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bank Name">
                                        <HeaderTemplate>
                                            Bank
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankName" runat="server" Text='<%# bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCHD_ID" runat="server" Text='<%# Bind("CHD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="CHB_LOTNo">
                                        <HeaderTemplate>
                                            Cheque Lot
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="2%" />
                                        <ItemTemplate>
                                            &nbsp; &nbsp;<asp:Label ID="lblCHBLotNo" runat="server" Text='<%# Bind("CHB_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            &nbsp; &nbsp;<asp:Label ID="lblChdNo" runat="server" Text='<%# Bind("CHD_No") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Cheque No
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="1%" />
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("CHD_No") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtNarration" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNarration"
                                ErrorMessage="Narration Required" ValidationGroup="CHQ_CANCEL">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="CHQ_CANCEL" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hCheqBook" runat="server" />
            </div>
        </div>
    </div>



    <script language="javascript" type="text/javascript">
        function PopUpCall(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

            var sFeatures;
            var lstrVal;
            var lintScrVal;

            sFeatures = "dialogWidth: " + pWidth + "px; ";
            sFeatures += "dialogHeight: " + pHeight + "px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (pMode == 'SUBGRP') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                document.getElementById(ctrl2).value = lstrVal[0];
            }
            else if (pMode == 'RFS') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                QRYSTR = '';
                if (pMode == 'CASHFLOW_BP')
                    QRYSTR = "?rss=0";
                if (pMode == 'CASHFLOW_BR')
                    QRYSTR = "?rss=1";

                result = window.showModalDialog("acccpShowCashflow.aspx" + QRYSTR, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('__');
                document.getElementById(ctrl).value = lstrVal[0];


            }
            else if (pMode == 'DOCTYPE') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];


            }
            else if (pMode == 'EMPS') {

                result = window.showModalDialog("ShowEmp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }

            else if (pMode == 'DEPT') {

                result = window.showModalDialog("ShowDept.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }

            else if (pMode == 'BSU') {

                result = window.showModalDialog("ShowBSU.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }

            else if (pMode == 'COLLN') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                document.getElementById(ctrl2).value = lstrVal[0];
                document.getElementById(ctrl3).value = lstrVal[1];

            }


            else if (pMode == 'RSS') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];


            }
            else if (pMode == 'CCM') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'CCS') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'CTY') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }

            else if (pMode == 'CONTROLACC') {
                var ddsel = '';
                var selObj = document.getElementById(acctype);
                ddsel = selObj.options[selObj.selectedIndex].value;

                result = window.showModalDialog("ShowControl.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value + "&acctype=" + ddsel, "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                document.getElementById(ctrl4).value = lstrVal[2];
                if (document.getElementById(ctrl).value == '06101001')
                    //{ document.getElementById(ctrl4).value='063'; }
                    //document.getElementById(ctrl5).value=lstrVal[0].substring(3,lstrVal[0].length); 
                    document.getElementById(ctrl5).value = lstrVal[4];


                var L = selObj.options.length;
                for (var i = 0; i <= L - 1; i++) {
                    if (lstrVal[5] == selObj.options[i].value) { selObj.options.selectedIndex = i; }

                }
            }
            else if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {

                result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
            }
            else if (pMode == 'EDITDEP') {

                result = window.showModalDialog("accgenbrcc.aspx?ShowType=" + pMode + "", "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl1).value = lstrVal[0];
                document.getElementById(ctrl2).value = lstrVal[1];
            }
            else if (pMode == 'ALLOC') {

                result = window.showModalDialog("TestAlloc.aspx?ShowType=" + pMode + "", "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
            }
            else if (pMode == 'ALLOCATE') {

                result = window.showModalDialog("TestAlloc.aspx?Id=" + ctrl + "&pAmount=" + document.getElementById(ctrl1).value + "&pPly=" + document.getElementById(ctrl2).value + "", "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');

            }
            else if (pMode == 'SHOWRSS') {

                result = window.showModalDialog("ShowRSS.aspx?ShowType=" + pMode + "", "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }

                document.getElementById(ctrl).value = replaceSubstring(result, "|", " ");



            }
            else if (pMode == 'BANK') {
                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }

            else if (pMode == 'INTRAC') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }

            else if (pMode == 'ACRDAC') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }

            else if (pMode == 'PREPDAC') {


                result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                if (ctrl2 != '')
                    if (document.getElementById(ctrl2).value == '') {
                        document.getElementById(ctrl2).value = lstrVal[2];
                        document.getElementById(ctrl3).value = lstrVal[3];
                    }
            }

            else if (pMode == 'CHQISSAC') {

                result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'CHQISSAC_PDC') {

                result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                if (document.getElementById(ctrl2).value == '') {
                    document.getElementById(ctrl2).value = lstrVal[2];
                    document.getElementById(ctrl3).value = lstrVal[3];
                }
            }

            else if (pMode == 'CHQBOOK') {
                if (document.getElementById(ctrl3).value == "") {
                    alert("Please Select The Bank");
                    return false;
                }

                result = window.showModalDialog("ShowChqs.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl3).value + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');

                document.getElementById(ctrl).value = lstrVal[1];
                document.getElementById(ctrl1).value = lstrVal[0];
                document.getElementById(ctrl2).value = lstrVal[2];

                document.getElementById(ctrl).focus();

            }
            else if (pMode == 'CHQBOOK_PDC') {
                if (document.getElementById(ctrl3).value == "") {
                    alert("Please Select The Bank");
                    return false;
                }

                result = window.showModalDialog("ShowChqs.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');

                document.getElementById(ctrl).value = lstrVal[1];
                document.getElementById(ctrl1).value = lstrVal[0];
                document.getElementById(ctrl2).value = lstrVal[2];
                document.getElementById(ctrl).focus();

            }



            else if (pMode == 'PDCCHQBOOK') {
                if (document.getElementById(ctrl2).value == "") {
                    alert("Please Select The Bank");
                    return false;
                }
                else if (document.getElementById(ctrl3).value == "") {
                    alert("Please Enter The Installment Months");
                    return false;
                }
                result = window.showModalDialog("ShowChqsPDC.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl2).value + "&MonthsReq=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');

                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

                document.getElementById(ctrl).focus();


            }
            else if (pMode == 'PARTY') {

                result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];

                document.getElementById(ctrl2).value = lstrVal[2];
                document.getElementById(ctrl3).value = lstrVal[3];

            }
                //EDITED BY GURU - FILTER BY PARTY, SHOW DEBIT ACC//
            else if (pMode == 'DEBIT_D' || pMode == 'PARTY1' || pMode == 'DEBIT' || pMode == 'PARTY2') {

                result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);

                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                if (pMode == 'DEBIT' && (document.getElementById(ctrl2).value == '')) {
                    document.getElementById(ctrl2).value = lstrVal[0];
                    document.getElementById(ctrl3).value = lstrVal[1];

                    document.getElementById(ctrl4).value = lstrVal[2];
                    document.getElementById(ctrl5).value = lstrVal[3];
                }
                if (pMode == 'DEBIT_D') {
                    document.getElementById(ctrl2).value = lstrVal[2];
                    document.getElementById(ctrl3).value = lstrVal[3];
                }
            }
            else if (pMode == 'ALL') {

                result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                document.getElementById(ctrl2).value = lstrVal[2];
                document.getElementById(ctrl3).value = lstrVal[3];

            }
            else if (pMode == 'NORMAL') {

                result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                //document.getElementById(ctrl2).value=lstrVal[2];
                //document.getElementById(ctrl3).value=lstrVal[3];

            }
            else if (pMode == 'NOTCC') {
                if (ctrl2 == '' || ctrl2 == undefined) {
                    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                } else {
                    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "", sFeatures);
                }
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl1).value = lstrVal[1];
                //document.getElementById(ctrl2).value=lstrVal[2];
                // document.getElementById(ctrl3).value=lstrVal[3];

            }
            else if (pMode == 'MSTCHQBOOK') {
                result = window.showModalDialog("accShowChequebook.aspx?BankCode=" + document.getElementById(ctrl1).value + "", "", sFeatures);
                if (result == '' || result == undefined)
                { return false; }
                lstrVal = result.split('||');
                document.getElementById(ctrl).value = lstrVal[0];
                document.getElementById(ctrl2).value = lstrVal[1];
            }
            //EDITED BY GURU//         
        }
    </script>
</asp:Content>

