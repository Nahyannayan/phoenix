<%@ Page Language="VB" AutoEventWireup="false" CodeFile="acccpAddDetails.aspx.vb" Inherits="Accounts_acccpAddDetails" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Cost Object Allocation</title>
    <base target="_self" />
    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

    <style type="text/css">
        .fntLink {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            font-weight: bold;
            color: #800000;
            text-decoration: underline;
        }

            .fntLink a:hover {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 8px;
                font-weight: bold;
                color: yellow;
                text-decoration: underline;
            }
    </style>
    <script language="javascript" type="text/javascript">
        function UpdateSum() {
            var sum = 0.0;
            var dsum = 0.0;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/txtAmt/) > 0) {
                    CheckAmount(document.forms[0].elements[i])
                    dsum = parseFloat(document.forms[0].elements[i].value);
                    if (isNaN(dsum))
                        document.forms[0].elements[i].value = 0;
                    sum += parseFloat(document.forms[0].elements[i].value);
                }
            }
            document.getElementById('<%=txtAllocated.ClientID %>').value = sum.toFixed(2);
            CheckAmount(document.getElementById('<%=txtAllocate.ClientID %>'));
            var difAmount = (Number(document.getElementById('<%=txtAllocate.ClientID %>').value) - Number(document.getElementById('<%=txtAllocated.ClientID %>').value));
            document.getElementById('<%=txtDifference.ClientID %>').value = difAmount.toFixed(2);
        }

        function ResetValue() {

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/txtAmt/) > 0)
                    document.forms[0].elements[i].value = '0.00';
            }
            document.getElementById('<%=txtAllocated.ClientID %>').value = '0.00';
            var difAmount = Number(document.getElementById('<%=txtAllocate.ClientID %>').value);
            document.getElementById('<%=txtDifference.ClientID %>').value = difAmount.toFixed(2);
        }


        function autoFill() {
            var rowLength = 0

            var Amount = Number(document.getElementById('<%=txtAllocate.ClientID %>').value);
            document.getElementById('<%=txtDifference.ClientID %>').value = '0.00'
            document.getElementById('<%=txtAllocated.ClientID %>').value = Amount.toFixed(2);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/txtAmt/) > 0)
                    rowLength++;
            }

            var divAmount = (Number(Amount) / Number(rowLength))
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/txtAmt/) > 0)

                    document.forms[0].elements[i].value = divAmount.toFixed(2);
            }

        }

        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(2);
            return true;
        }
        function getCostcentre(SubMenmberId, SubName) {

            var pWidth;
            var pHeight;

            var sFeatures;
            var lstrVal;
            sFeatures = "dialogWidth: " + pWidth + "px; ";
            sFeatures += "dialogHeight: " + pHeight + "px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //
            //if (nameCost == 'OTH') 
            //result = window.showModalDialog("OthercostcenterAdd.aspx");
            var url = 'OthercostcenterAdd.aspx?ccsmode=' + '<%=ddGroupPolicy.Selecteditem.value %>' + '&vid=' + '<%=Request.QueryString("vid") %>';
            url = url + '&dt=' + '<%=Request.QueryString("dt") %>' + '&SubMemberId=' + SubMenmberId + '&SubName=' + SubName;
            result = window.showModalDialog(url, window, sFeatures)
            //else   
            //result = window.showModalDialog("PickDetails.aspx","MAINCC", sFeatures); 

            if (result == '' || result == undefined) {
                return false;
            }
            document.getElementById('<%= h_Memberids.ClientID %>').value = result;
      document.form1.submit();
  }

  function AddCostcentre() {

      document.getElementById('<%=h_mode.ClientID %>').value = '';
      var sFeatures;
      var OthSession = '<%=Session("CostOTH")%>'
      var pageName = "acccpPickCodes.aspx?ccsmode="
      if ('<%=ddGroupPolicy.Selecteditem.value %>' == OthSession) {
        sFeatures = "dialogWidth: 529px; ";
        sFeatures += "dialogHeight: 634px; ";
        document.getElementById('<%=h_mode.ClientID %>').value = OthSession;
            pageName = "OthercostcenterShow.aspx?ccsmode="
        }
        else {
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 634px; ";
        }


        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: yes; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;

        var url = pageName + '<%=ddGroupPolicy.Selecteditem.value %>' + '&vid=' + '<%=Request.QueryString("vid") %>';
        url = url + '&dt=' + '<%=Request.QueryString("dt") %>';


      //alert(url);

      result = window.showModalDialog(url, window, sFeatures)
      document.getElementById('<%=h_Memberids.ClientID %>').value = result;
      document.forms[0].submit();

  }

  //date auto complete



    </script>
</head>
<body topmargin="0" leftmargin="0" onload="UpdateSum()">
    <form id="form1" runat="server">

        <table align="center" width="100%">
            <tr>
                <td>

                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
            </tr>
            <tr>
                <td>

                    <table width="100%" align="center">
                        <tr valign="top" class="title-bg">
                            <td align="left" colspan="4">Cost Center Allocation
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Select Cost Center</span></td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddGroupPolicy" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="left" colspan="2">
                                <asp:LinkButton ID="btnInsert" runat="server" OnClientClick="AddCostcentre();">Add New</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                    Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                            <EditItemTemplate>
                                                &nbsp;
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                        <asp:BoundField DataField="Costcenter" HeaderText="Costcenter" />
                                        <asp:BoundField DataField="Memberid" HeaderText="Memberid" />
                                        <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />

                                        <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Allocated"
                                            HtmlEncode="False" InsertVisible="False" SortExpression="Amount">
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Earn Code">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SubMemberId" HeaderText="SubMemberId" />
                                        <asp:TemplateField HeaderText="Change">
                                            <EditItemTemplate>
                                                &nbsp;
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                &nbsp;
                                                <asp:TextBox ID="txtAmt" runat="server" OnPreRender="txtAmt_PreRender"
                                                    Text='<%# Bind("Amount", "{0:0.00}") %>' onblur="UpdateSum()"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <HeaderStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <EmptyDataRowStyle />
                                </asp:GridView>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnOk" runat="server" CssClass="button" Text="Save Amount" />
                                <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Done" />
                                <asp:Button ID="btnResetCC" runat="server" CssClass="button" Text="Auto Fill" OnClientClick="autoFill();return false; " />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="10%"><span class="field-label">Amount to Allocate</span> </td>
                                        <td align="left" width="25%">
                                            <asp:TextBox ID="txtAllocate" runat="server"></asp:TextBox></td>
                                        <td align="left" width="10%"><span class="field-label">Total Allocated</span></td>
                                        <td align="left" width="25%">
                                            <asp:TextBox ID="txtAllocated" runat="server"></asp:TextBox></td>
                                        <td align="left" width="10%"><span class="field-label">Difference</span></td>
                                        <td align="left" width="20%">
                                            <asp:TextBox ID="txtDifference" runat="server"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="h_Voucherid" runat="server" />
        <asp:HiddenField ID="h_mode" runat="server" />
        <asp:HiddenField ID="h_Memberids" runat="server" />
    </form>
</body>
</html>
