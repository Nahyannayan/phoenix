Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class AccBankR
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim lstrErrMsg As String
    Dim BSU_IsTAXEnabled As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbUploadEmployee)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(ddlCostCenter)
        Session("BANKTRAN") = "BR"
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        LockControls()

        If Page.IsPostBack = False Then
            Try

                'TAX CODE
                Dim ds7 As New DataSet
                Dim pParms0(2) As SqlClient.SqlParameter
                pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms0(1).Value = Session("sBSUID")
                ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
                BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))
                ViewState("BSU_IsTAXEnabled") = BSU_IsTAXEnabled
                If BSU_IsTAXEnabled = True Then
                    trTaxType.Visible = True
                    LOAD_TAX_CODES()
                End If
                'TAX CODE

                usrCostCenter1.TotalAmountControlName = "txtLineAmount"
                bind_Collection()
                Session("CHECKLAST") = 0
                lbSettle.Visible = False
                chkAdvance.Visible = True
                Session("dtSettle") = DataTables.CreateDataTable_Settle
                imgCashFlow.OnClientClick = "popUp('460','400','CASHFLOW_BR','" & txtCashFlow.ClientID & "'); return false;"
                Session("idCostChild") = 0
                'txtNarrn.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtNarrn.ClientID & "');")
                'txtLineNarrn.Attributes.Add("onBlur", "narration_check('" & txtLineNarrn.ClientID & "');")
                txtCashFlow.Text = "363"
                '   --- For Checking Rights And Initilize The Edit Variables --
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvDTL.Attributes.Add("bordercolor", "#1b80b6")
                If (ViewState("datamode") = "add") Then

                    Call Clear_Details()
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("gintGridLine") = 1

                    Session("gDtlDataMode") = "ADD"
                    Session("dtSettle").rows.clear()
                    h_NextLine.Value = Session("gintGridLine")
                    txtdocDate.Text = GetDiplayDate()
                    If ViewState("datamode") = "add" Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                    End If
                    hCheqBook.Value = 0
                    bind_Currency()
                    Session("dtDTL") = DataTables.CreateDataTable_BR()

                    Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
                    Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
                    Session("dtCostChild").rows.clear()
                    GridInitialize()
                End If

                txtBankCode.Text = Session("CollectBank")
                txtBankDescr.Text = Session("Collect_name")
                'set_collection()
                If ViewState("datamode") = "view" Then
                    Session("Eid") = Convert.ToString(Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+")))
                End If

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "A150015" And ViewState("MainMnu_code") <> "A200012") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Page.Title = OASISConstants.Gemstitle
                    If ViewState("datamode") = "view" Then
                        '   --- Fill Data ---
                        FillValues()
                        bind_Currency()

                    End If
                End If


                '   --- Checking End  ---
                SetCostcenterControls()
                gvDTL.Attributes.Add("bordercolor", "#1b80b6")
                Session("gDtlDataMode") = "ADD"
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        Else
            If (ViewState("datamode") = "add") Then

                'txtDocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtDocDate.Text)), Year(Convert.ToDateTime(txtDocDate.Text))).ToString
                Session("SessDocDate") = txtdocDate.Text
            End If
            BSU_IsTAXEnabled = ViewState("BSU_IsTAXEnabled")
        End If



        If ViewState("datamode") <> "add" Then
            btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
            'ImageButton1.Enabled = False
        Else
            imgCalendar.Enabled = True
        End If
    End Sub

    Sub LOAD_TAX_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_CODES]", pParms)

        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAXDESC"
        ddlVATCode.DataValueField = "TAXCODE"
        ddlVATCode.DataBind()

    End Sub
    Private Sub bind_Collection() ' bind combos
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT COL_ID, COL_DESCR FROM COLLECTION_M"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddCollection.Items.Clear()
            ddCollection.DataSource = ds.Tables(0)
            ddCollection.DataTextField = "COL_DESCR"
            ddCollection.DataValueField = "COL_ID"
            ddCollection.DataBind()
            ddCollection.SelectedIndex = 0

            ddDCollection.Items.Clear()
            ddDCollection.DataSource = ds.Tables(0)
            ddDCollection.DataTextField = "COL_DESCR"
            ddDCollection.DataValueField = "COL_ID"
            ddDCollection.DataBind()
            ddDCollection.SelectedIndex = 0
            'getnextdocid()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub LockControls()
        txtdocNo.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        txtPartyDescr.Attributes.Add("readonly", "readonly")
        txtCashFlow.Attributes.Add("readonly", "readonly")
        'txtGColln.Attributes.Add("readonly", "readonly")
        'txtGCollnDescr.Attributes.Add("readonly", "readonly")

        'txtDocDate.Attributes.Add("readonly", "readonly")
        'txtBankCode.Attributes.Add("readonly", "readonly")
        'Detail_ACT_ID.Attributes.Add("readonly", "readonly")
        'txtChqDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub bind_Currency()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            cmbCurrency.Items.Clear()
            cmbCurrency.DataSource = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))

            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If cmbCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    cmbCurrency.SelectedIndex = 0
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In cmbCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            'Else
            'End If
            Return False

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)

            Return False

        End Try

    End Function



    Private Sub GridInitialize()
        gvDTL.DataBind()
        SetGridColumnVisibility(False)
    End Sub

    Protected Function GetFirstFinancialYearDate() As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim SQLStr As String
        SQLStr = "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT, GETDATE() AS CURDATE from FINANCIALYEAR_S WHERE  bDefault=1"
        Dim dtFinYear As DataTable
        dtFinYear = Mainclass.getDataTable(SQLStr, str_conn)
        If dtFinYear.Rows.Count > 0 Then
            Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(dtFinYear.Rows(0).Item("FYR_ID"))
            Return dtFinYear.Rows(0).Item("FYR_FROMDT")
        Else
            Return dtFinYear.Rows(0).Item("CURDATE")
        End If

    End Function

    Protected Function GetChqNos(ByVal pId As Integer, ByVal pGetNext As Boolean) As String
        Dim lintChqs As Integer
        Dim lintMaxChqs As Integer
        Dim lstrSql As String
        Dim lds As New DataSet
        Dim pBank As String = String.Empty
        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSql = "select CHB_ACT_ID FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
        If lds.Tables(0).Rows.Count > 0 Then
            pBank = lds.Tables(0).Rows(0)(0)
        End If


        If pGetNext = True Then
            lstrSql = "select isNull(MIN(CHB_ID),0) FROM vw_OSA_CHQBOOK_M where CHB_ID<>'" & pId & "' AND AvlNos>0 AND CHB_ACT_ID='" & pBank & "' AND CHB_BSU_ID='" & Session("sBsuid") & "'"
            lds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSql)
            If lds.Tables(0).Rows.Count > 0 Then
                pId = lds.Tables(0).Rows(0)(0)
            End If
        End If

        Dim str_Sql As String = "select CHB_NEXTNO,AvlNos,CHB_Id,CHB_LOTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & pId & "' "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lintChqs = ds.Tables(0).Rows(0)(0)
            lintMaxChqs = ds.Tables(0).Rows(0)(1)
            lstrChqBookId = ds.Tables(0).Rows(0)(2)
            lstrChqBookLotNo = ds.Tables(0).Rows(0)(3)
            Return Convert.ToString(lintChqs) & "|" & Convert.ToString(lintMaxChqs) & "|" & lstrChqBookId & "|" & lstrChqBookLotNo
        Else
            Return ""
        End If
    End Function



    Public Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click


        Dim i, lintIndex As Integer

        Dim lstrChqBookId As String
        Dim lstrChqBookLotNo As String
        Dim ldrNew As DataRow
        If ViewState("datamode") = "view" Then
            lblError.Text = "Record not in edit mode !!!"
            Exit Sub
        End If
        lstrErrMsg = ""
        ' --- (1) VALIDATE THE TEXTBOXES
        If Trim(Detail_ACT_ID.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Party " & "<br>"
        End If

        'If Trim(txtChqBook.Text) = "" Then
        '    lstrErrMsg = lstrErrMsg & "Enter the cheque book no " & "<br>"
        'End If
        'If Trim(txtChqDate.Text) = "" Then
        '    lstrErrMsg = lstrErrMsg & "Enter the cheque date " & "<br>"
        'End If
        Dim str_err As String = ""
        Dim strfDate As String = txtChqDate.Text.Trim
        If strfDate <> "" Then
            str_err = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                lstrErrMsg = lstrErrMsg & str_err & "<br>"
            Else
                Dim pass_date As DateTime = DateTime.ParseExact(strfDate, "dd/MMM/yyyy", Nothing)
                Dim cur_date As DateTime = DateTime.ParseExact(txtdocDate.Text, "dd/MMM/yyyy", Nothing)

                If pass_date > cur_date Then
                    lstrErrMsg = lstrErrMsg & "Cheque date should not be greater than document date " & "<br>"
                End If

                txtChqDate.Text = strfDate
            End If
        End If

        If Trim(txtLineAmount.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Amount " & "<br>"
        End If

        If (IsNumeric(txtLineAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If

        If Trim(txtCashFlow.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Cash Flow " & "<br>"
        End If

        If txtNarrn.Text = "" Then
            txtNarrn.Text = txtLineNarrn.Text
        End If

        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True

            lblError.Text = "Please check the following errrors : " & "<br>" & lstrErrMsg
            Exit Sub
        End If

        ''''COST CENTER VERIFICATION

        '''''FIND ACCOUNT IS THERE

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & Detail_ACT_ID.Text & "'" _
           & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH<>'CC'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPartyDescr.Text = ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                lblError.Text = getErrorMessage("303") ' account already there
                Exit Sub
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Errorlog(ex.Message)
        End Try
        '''''FIND ACCOUNT IS THERE

        'Check CostCenter Summary
        RecreateSsssionDataSource()
        If Not CostCenterFunctions.VerifyCostCenterAmount(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), _
                                                             txtLineAmount.Text) Then
            lblError.Text = "Invalid Cost Center Allocation!!!"
            Exit Sub
        End If


        '   --- (1) END OF VALIDATION DURING ADD

        '   --- (2) PROCEED TO SAVE IN GRID IF NO ERRORS FOUND
        If (lstrErrMsg = "") Then
            'tr_errLNE.Visible = False
            Try
                If Session("gDtlDataMode") = "ADD" Then

                    ldrNew = Session("dtDTL").NewRow
                    ldrNew("Id") = Session("gintGridLine")
                    If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                        CostCenterFunctions.AddCostCenter(Session("gintGridLine"), Session("sBsuid"), _
                                          Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                         Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                         Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    End If
                    Session("gintGridLine") = Session("gintGridLine") + 1
                    h_NextLine.Value = Session("gintGridLine")
                    ldrNew("AccountId") = Trim(Detail_ACT_ID.Text)
                    ldrNew("AccountName") = Trim(txtPartyDescr.Text)
                    ldrNew("Narration") = Trim(txtLineNarrn.Text)
                    ldrNew("CLine") = Trim(txtCashFlow.Text)
                    ldrNew("Colln") = Trim(ddDCollection.SelectedItem.Value)
                    'lstrChqDet = GetChqNos(Convert.ToInt32(hCheqBook.Value), False)

                    ' lintChqs = Convert.ToInt32(lstrChqDet.Split("|")(0))
                    ' lintMaxChqs = Convert.ToInt32(lstrChqDet.Split("|")(1))
                    'lstrChqBookId = lstrChqDet.Split("|")(2)
                    'lstrChqBookLotNo = lstrChqDet.Split("|")(3)

                    lstrChqBookId = 0
                    lstrChqBookLotNo = txtChqBook.Text
                    If (txtChqBook.Text = "") Then
                        lstrChqBookLotNo = " "
                    End If

                    ldrNew("ChqBookId") = lstrChqBookId
                    hCheqBook.Value = Convert.ToInt32(lstrChqBookId)
                    ldrNew("ChqBookLot") = lstrChqBookLotNo
                    ldrNew("ChqNo") = lstrChqBookLotNo



                    If (txtChqDate.Text = "") Then
                        ldrNew("ChqDate") = txtdocDate.Text
                    Else
                        ldrNew("ChqDate") = Trim(txtChqDate.Text)
                    End If



                    ldrNew("Amount") = Convert.ToDecimal(Trim(txtLineAmount.Text))
                    ldrNew("Status") = ""
                    ldrNew("GUID") = System.DBNull.Value

                    ldrNew("Ply") = ddlCostCenter.SelectedItem.Value

                    ldrNew("CostReqd") = False

                    If BSU_IsTAXEnabled Then
                        ldrNew("TaxCode") = ddlVATCode.SelectedValue
                    Else
                        ldrNew("TaxCode") = ""
                    End If





                    '   --- Check if these details are already there before adding to the grid
                    For i = 0 To Session("dtDTL").Rows.Count - 1
                        If Session("dtDTL").Rows(i)("Accountid") = ldrNew("Accountid") And _
                            Session("dtDTL").Rows(i)("Accountname") = ldrNew("Accountname") And _
                             Session("dtDTL").Rows(i)("Narration") = ldrNew("Narration") And _
                             Session("dtDTL").Rows(i)("CLine") = ldrNew("CLine") And _
                             Session("dtDTL").Rows(i)("Amount") = ldrNew("Amount") Then

                            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                            GridBind()
                            Exit Sub
                        End If
                    Next
                    Session("dtDTL").Rows.Add(ldrNew)
                    chkAdvance.Enabled = False '---sadhu

                    GridBind()
                ElseIf (Session("gDtlDataMode") = "UPDATE") Then
                    For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                        If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                            If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                                CostCenterFunctions.AddCostCenter(Session("gintEditLine"), Session("sBsuid"), _
                                                  Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                                 Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                                 Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                            End If
                            Session("dtDTL").Rows(lintIndex)("AccountId") = Detail_ACT_ID.Text
                            Session("dtDTL").Rows(lintIndex)("AccountName") = txtPartyDescr.Text
                            Session("dtDTL").Rows(lintIndex)("Narration") = txtLineNarrn.Text
                            Session("dtDTL").Rows(lintIndex)("CLine") = txtCashFlow.Text
                            Session("dtDTL").Rows(lintIndex)("Colln") = ddDCollection.SelectedItem.Value
                            Session("dtDTL").Rows(lintIndex)("ChqBookId") = 0

                            If txtChqBook.Text = "" Then
                                Session("dtDTL").Rows(lintIndex)("ChqBookLot") = " "
                                Session("dtDTL").Rows(lintIndex)("ChqNo") = " "

                                Session("dtDTL").Rows(lintIndex)("ChqDate") = IIf(txtChqDate.Text = "", txtdocDate.Text, txtChqDate.Text) 'changed as per request
                            Else
                                Session("dtDTL").Rows(lintIndex)("ChqBookLot") = txtChqBook.Text
                                Session("dtDTL").Rows(lintIndex)("ChqNo") = txtChqBook.Text

                                Session("dtDTL").Rows(lintIndex)("ChqDate") = IIf(txtChqDate.Text = "", txtdocDate.Text, txtChqDate.Text) 'changed as per request
                            End If

                            Session("dtDTL").Rows(lintIndex)("Amount") = txtLineAmount.Text

                            Session("dtDTL").Rows(lintIndex)("Ply") = ddlCostCenter.SelectedItem.Value
                            Session("dtDTL").Rows(lintIndex)("CostReqd") = False
                            If BSU_IsTAXEnabled Then
                                Session("dtDTL").Rows(lintIndex)("TaxCode") = ddlVATCode.SelectedValue
                            Else
                                Session("dtDTL").Rows(lintIndex)("TaxCode") = ""
                            End If





                            SetGridColumnVisibility(True)
                            Session("gDtlDataMode") = "ADD"
                            btnFill.Text = "ADD"
                            h_NextLine.Value = Session("gintGridLine")
                            gvDTL.SelectedIndex = -1
                            Clear_Details()
                            GridBind()
                            Exit For
                        End If
                    Next
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
            CalcuTot()
        End If




        '   --- (2) END OF SAVE IN GRID IF NO ERRORS FOUND

    End Sub



    Private Sub CalcuTot()
        Dim ldblTot As Decimal
        Dim i As Integer

        ldblTot = 0
        For i = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                ldblTot = ldblTot + Session("dtDTL").Rows(i)("Amount")
            End If
        Next
        txtAmount.Text = AccountFunctions.Round(ldblTot)
    End Sub



    Private Sub GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = DataTables.CreateDataTable_BR()


        If Session("dtDTL").Rows.Count > 0 Then
            For i = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("dtDTL").Columns.Count - 1
                        ldrTempNew.Item(j) = Session("dtDTL").Rows(i)(j)
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next

        End If


        gvDTL.DataSource = dtTempDtl
        gvDTL.DataBind()
        SetGridColumnVisibility(True)

        If Session("dtDTL").Rows.Count <= 0 Then
            GridInitialize()
        End If

        Clear_Details()
    End Sub



    Private Sub Clear_Details()
        Detail_ACT_ID.Text = ""
        txtPartyDescr.Text = ""
        'txtLineNarrn.Text = ""
        txtLineAmount.Text = ""

        txtChqBook.Text = ""
        If txtChqDate.Text <> txtdocDate.Text Then
            txtChqDate.Text = ""
        End If
        ClearRadGridandCombo()
    End Sub



    Private Sub Clear_Header()

        txtdocNo.Text = ""
        txtOldDocNo.Text = ""
        txtdocDate.Text = ""
        'txtColln.Text = ""
        'txtCollnDescr.Text = ""
        txtNarrn.Text = ""
        'bind_Currency()

        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")
    End Sub



    Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        Dim lintIndex As Integer = 0

        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)

        For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                Detail_ACT_ID.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountId"))
                txtPartyDescr.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountName"))
                txtLineNarrn.Text = Trim(Session("dtDTL").Rows(lintIndex)("Narration"))
                txtCashFlow.Text = Trim(Session("dtDTL").Rows(lintIndex)("CLine"))
                h_NextLine.Value = Session("gintEditLine")
                ddDCollection.SelectedIndex = -1
                ddDCollection.Items.FindByValue(Trim(Session("dtDTL").Rows(lintIndex)("Colln"))).Selected = True
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

                txtChqBook.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqNo"))

                txtChqDate.Text = Trim(Session("dtDTL").Rows(lintIndex)("ChqDate"))
                txtLineAmount.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Amount"))

                hPLY.Value = Trim(Session("dtDTL").Rows(lintIndex)("Ply"))
                hCostreqd.Value = Trim(Session("dtDTL").Rows(lintIndex)("CostReqd"))
                If BSU_IsTAXEnabled Then
                    ddlVATCode.SelectedValue = Trim(Session("dtDTL").Rows(lintIndex)("TaxCode"))
                End If


                gvDTL.SelectedIndex = lintIndex
                gvDTL.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                gvDTL.SelectedRowStyle.ForeColor = Drawing.Color.Black
                Session("gDtlDataMode") = "UPDATE"
                btnFill.Text = "UPDATE"
                '''''''
                If Not ddlCostCenter.Items.FindByValue(Session("dtDTL").Rows(lintIndex)("Ply")) Is Nothing Then
                    ddlCostCenter.ClearSelection()
                    ddlCostCenter.Items.FindByValue(Session("dtDTL").Rows(lintIndex)("Ply")).Selected = True
                End If
                ShowUploadOption()
                RecreateSsssionDataSource()
                CostCenterFunctions.SetGridSessionDataForEdit(Session("gintEditLine"), Session("dtCostChild"), _
                Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                usrCostCenter1.BindCostCenter()
                '''''''
                SetGridColumnVisibility(False)
                Exit For
            End If
        Next
        Set_SettleControls()
        txtLineAmount.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Amount"))
    End Sub

    Protected Sub SetGridColumnVisibility(ByVal pSet As Boolean)
        gvDTL.Columns(17).Visible = pSet
        gvDTL.Columns(18).Visible = pSet

        If BSU_IsTAXEnabled = True Then
            gvDTL.Columns(16).Visible = True
        Else
            gvDTL.Columns(16).Visible = False
        End If
    End Sub

    Protected Sub btnFillCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFillCancel.Click
        h_NextLine.Value = Session("gintGridLine")
        SetGridColumnVisibility(True)
        Session("gDtlDataMode") = "ADD"
        btnFill.Text = "ADD"
        gvDTL.SelectedIndex = -1
        Clear_Details()
    End Sub



    Protected Sub DeleteRecordByID(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim j As Integer

        For iRemove = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iRemove)("id") = pId) Then
                Session("dtDTL").Rows(iRemove)("Status") = "DELETED"

                '   --- Remove The Corresponding Rows From The Detail Grid Also
                'For j As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                While j < Session("dtCostChild").Rows.Count
                    If (Session("dtCostChild").Rows(j)("VoucherId") = pId) Then
                        Session("dtCostChild").Rows(j)("Status") = "DELETED"
                    Else
                        j = j + 1
                    End If
                End While
            End If
        Next
        GridBind()
    End Sub



    Protected Sub gvDTL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDTL.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblReqd As New Label
            'Dim lbAllocate As New LinkButton
            Dim lblid As New Label
            Dim lblAmount As New Label

            lblReqd = e.Row.FindControl("lblCostReqd")
            lblid = e.Row.FindControl("lblId")
            lblAmount = e.Row.FindControl("lblAmount")
            'lbAllocate = e.Row.FindControl("lbAllocate")


            If lblReqd IsNot Nothing Then
                If lblReqd.Text = "True" Then
                    e.Row.BackColor = Drawing.Color.Pink

                End If
            End If
            Dim gvCostchild As New GridView
            gvCostchild = e.Row.FindControl("gvCostchild")

            gvCostchild.Attributes.Add("bordercolor", "#fc7f03")

            'ClientScript.RegisterStartupScript([GetType](), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" & lblid.Text & "','one');</script>")
            If Not Session("dtCostChild") Is Nothing Then
                Dim dv As New DataView(Session("dtCostChild"))
                dv.RowFilter = "VoucherId='" & lblid.Text & "' and costcenter='" & e.Row.DataItem("Ply") & "'"
                dv.Sort = "MemberId"
                gvCostchild.DataSource = dv.ToTable
                gvCostchild.DataBind()
            End If

            'If lbAllocate IsNot Nothing Then
            '    Dim dAmt As Double
            '    If lblAmount.Text.ToString() = "" Then
            '        lblAmount.Text = "0"
            '    End If
            '    If CDbl(lblAmount.Text) > 0 Then
            '        dAmt = CDbl(lblAmount.Text)
            '    End If

            '    lbAllocate.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & lblid.Text & "');return false;"
            'End If


            Dim l As LinkButton = DirectCast(e.Row.FindControl("DeleteBtn"), LinkButton)
            l.Attributes.Add("onclick", "javascript:return " + "confirm('Are you sure you want to delete this record " + DataBinder.Eval(e.Row.DataItem, "id") + "')")
        End If
    End Sub



    Protected Sub gvDTL_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDTL.RowDeleting
        Try
            Dim categoryID As Integer = CInt(gvDTL.DataKeys(e.RowIndex).Value)
            DeleteRecordByID(categoryID)
            Dim il As Integer = 0
            While il < Session("dtSettle").Rows.Count
                If Session("dtSettle").rows(il)("Id") = categoryID Then
                    Session("dtSettle").rows(il).delete()
                Else
                    il = il + 1
                End If
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub



    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " GUID='" & Session("Eid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")


                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("LockVOUCHER_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpJHD_DOCNO)



                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    Session("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = "Error"
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
                '''''''
            Else
            End If
            Return " | | "
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function



    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
           & " GUID='" & Session("Eid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")
                txtNarrn.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = Session("sBSUId")
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = Session("F_YEAR")
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpVHH_DOCNO)

                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopVHH_TIMESTAMP.Value = Session("str_timestamp")
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message)
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function




    Protected Function SaveValidate() As Boolean
        Dim lstrErrMsg As String
        Dim iIndex As Integer
        Dim cIndex As Integer
        Dim lblnFound As Boolean

        lstrErrMsg = ""
        If Trim(txtdocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If

        If Trim(txtdocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If

        If Trim(txtNarrn.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter Narration " & "<br>"
        End If

        If Trim(txtBankCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Bank " & "<br>"
        End If



        If (IsNumeric(txtAmount.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        End If


        If Session("dtDTL").Rows.Count = 0 Then
            lstrErrMsg = lstrErrMsg & " Enter the Credit Details " & "<br>"
        End If

        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iIndex)("CostReqd") = "True") Then
                lblnFound = False
                For cIndex = 0 To Session("dtCostChild").Rows.Count - 1
                    If Session("dtDTL").Rows(iIndex)("Id") = Session("dtCostChild").Rows(cIndex)("VoucherId") Then
                        lblnFound = True
                    End If
                Next
                If lblnFound = False Then
                    lstrErrMsg = lstrErrMsg & " Enter the mandatory cost center details - Line " & iIndex + 1 & "<br>"
                End If
            End If
        Next
        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True
            lstrErrMsg = "Please check the following errors" & "<br>" & lstrErrMsg
            lblError.Text = lstrErrMsg
            Return False
        Else
            'tr_errLNE.Visible = False
            Return True
        End If
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String = String.Empty
        Dim lstrChqBookId As String
        'Dim lintColId As Integer
        Dim lblnNoErr As Boolean
        '   ----------------- VALIDATIONS --------------------
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid Bank Selected"
            Exit Sub
        Else
            lblError.Text = ""
        End If
        lblnNoErr = SaveValidate()
        If (lblnNoErr = False) Then
            Exit Sub
        End If

        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()

        '   ----------------- END OG VALIDATE -----------------
        If (Session("BANKTRAN") = "BP") Then
            lstrChqBookId = hCheqBook.Value
            ' lintColId = 0
        Else
            lstrChqBookId = ""
            'lintColId = Convert.ToInt32(Trim(txtColln.Text))
        End If
        Try
            Session("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try

                Dim SqlCmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
                SqlCmd.CommandType = CommandType.StoredProcedure
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = System.DBNull.Value
                SqlCmd.Parameters.Add(sqlpGUID)
                SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
                SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
                SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
                SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", Session("BANKTRAN"))
                SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", Trim(txtdocNo.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_REFNO", Trim(txtOldDocNo.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "R")
                SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", lstrChqBookId)
                SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtdocDate.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtChqDate.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", hNum.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", hNum.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", Convert.ToDecimal(txtAmount.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
                SqlCmd.Parameters.AddWithValue("@VHH_bAuto", False)
                SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

                SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", System.DBNull.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

                SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", cmbCurrency.SelectedItem.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", txtExchRate.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", txtLocalRate.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarrn.Text)
                SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", ddCollection.SelectedItem.Value)
                SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", Convert.ToDecimal(txtAmount.Text))
                SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
                SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
                SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
                SqlCmd.Parameters.AddWithValue("@VHH_bAdvance", chkAdvance.Checked) ''sadhu

                Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If ViewState("datamode") <> "edit" Then
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpJHD_TIMESTAMP.Value = Session("str_timestamp")
                End If
                SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
                SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
                SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
                SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
                SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
                If (ViewState("datamode") = "edit") Then
                    SqlCmd.Parameters.AddWithValue("@bEdit", True)
                Else
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)
                End If
                SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                If lintRetVal = 0 Then
                    If (ViewState("datamode") = "edit") Then
                        lstrNewDocNo = txtdocNo.Text
                    Else
                        lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
                    End If
                End If


                'Adding header info
                SqlCmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String
                If (lintRetVal = 0) Then
                    ' stTrans.Commit()
                    If ViewState("datamode") = "add" Then
                        str_err = DoTransactions(objConn, stTrans, lstrNewDocNo)
                    Else
                        str_err = DeleteVOUCHER_D_S_ALL(objConn, stTrans, txtdocNo.Text)
                        If str_err = 0 Then
                            str_err = DoTransactions(objConn, stTrans, txtdocNo.Text)
                        End If
                    End If
                    If str_err = "0" Then
                        h_editorview.Value = ""
                        stTrans.Commit()
                        If ViewState("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                        End If

                        Call Clear_Header()
                        Call Clear_Details()
                        txtLineNarrn.Text = ""
                        ViewState("datamode") = "add"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Session("gintGridLine") = 1
                        h_NextLine.Value = Session("gintGridLine")
                        GridInitialize()
                        Session("gDtlDataMode") = "ADD"
                        Session("dtDTL") = DataTables.CreateDataTable_BR()
                        Session("dtCostChild").Rows.Clear()
                        txtdocDate.Text = GetDiplayDate()
                        If ViewState("datamode") = "add" Then
                            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                        End If
                        txtAmount.Text = ""
                        bind_Currency()


                        'tr_errLNE.Visible = True
                        lblError.Text = "Data Successfully Saved..."
                    Else
                        'tr_errLNE.Visible = True
                        lblError.Text = getErrorMessage(str_err)
                        stTrans.Rollback()
                    End If
                Else
                    'tr_errLNE.Visible = True
                    lblError.Text = getErrorMessage(lintRetVal & "")
                    stTrans.Rollback()
                End If



            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                stTrans.Rollback()
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        cmd.Dispose()
        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = Session("BANKTRAN")
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        Dim lstrChqBookId As String
        Dim lintColId As Integer

        If (Session("BANKTRAN") = "BP") Then
            lstrChqBookId = hCheqBook.Value
            lintColId = 0
        Else
            lstrChqBookId = ""
            lintColId = 1
        End If
        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim iIndex As Integer
            Dim str_err As String = ""
            Dim dTotal As Double = 0
            Dim lstrChqNo As String = String.Empty
            Dim lintChbId As Integer
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            For iIndex = 0 To Session("dtDTL").Rows.Count - 1
                If Session("dtDTL").Rows(iIndex)("Status") & "" <> "DELETED" Then
                    cmd.Dispose()
                    cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure
                    '' ''Handle sub table
                    Dim str_crdb As String = "CR"
                    If Session("BANKTRAN") = "BP" Then
                        dTotal = Session("dtDTL").Rows(iIndex)("Amount")
                        str_crdb = "DR"

                        '   --- Get The Cheque Nos
                        Dim str_Sql As String = "select CHB_NEXTNO FROM vw_OSA_CHQBOOK_M where CHB_ID='" & Session("dtDTL").Rows(iIndex)("ChqBookId") & "'"
                        Dim ds As New DataSet
                        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                        If ds.Tables(0).Rows.Count > 0 Then
                            lstrChqNo = ds.Tables(0).Rows(0)(0)
                        End If
                    Else
                        dTotal = Session("dtDTL").Rows(iIndex)("Amount")
                        str_crdb = "CR"
                        lintChbId = 0
                        lstrChqNo = Trim(Session("dtDTL").Rows(iIndex)("ChqNo"))
                    End If

                    str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtDTL").Rows(iIndex)("id"), _
                    str_crdb, iIndex + 1 - Session("iDeleteCount"), Session("dtDTL").Rows(iIndex)("Accountid"), dTotal)
                    If str_err <> "0" Then
                        Return str_err
                    End If
                    '' ''
                    cmd.Parameters.AddWithValue("@GUID", Session("dtDTL").Rows(iIndex)("GUID"))
                    cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
                    cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
                    cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
                    cmd.Parameters.AddWithValue("@VHD_DOCTYPE", Session("BANKTRAN"))
                    cmd.Parameters.AddWithValue("@VHD_DOCNO", p_docno)
                    cmd.Parameters.AddWithValue("@VHD_LINEID", iIndex + 1)
                    cmd.Parameters.AddWithValue("@VHD_ACT_ID", Session("dtDTL").Rows(iIndex)("AccountId"))
                    cmd.Parameters.AddWithValue("@VHD_AMOUNT", Session("dtDTL").Rows(iIndex)("Amount"))
                    cmd.Parameters.AddWithValue("@VHD_NARRATION", Session("dtDTL").Rows(iIndex)("Narration"))
                    cmd.Parameters.AddWithValue("@VHD_CHQID", Session("dtDTL").Rows(iIndex)("ChqBookId"))
                    cmd.Parameters.AddWithValue("@VHD_CHQNO", lstrChqNo)
                    cmd.Parameters.AddWithValue("@VHD_CHQDT", Session("dtDTL").Rows(iIndex)("ChqDate")) 'Convert.ToDateTime(Session("dtDTL").Rows(iIndex)("ChqDate"))
                    cmd.Parameters.AddWithValue("@VHD_RSS_ID", Session("dtDTL").Rows(iIndex)("CLine"))
                    cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
                    cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
                    cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
                    cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
                    cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)
                    cmd.Parameters.AddWithValue("@VHD_COL_ID", Session("dtDTL").Rows(iIndex)("colln"))
                    If BSU_IsTAXEnabled = True Then
                        cmd.Parameters.AddWithValue("@VHD_TAX_CODE", Session("dtDTL").Rows(iIndex)("TaxCode"))
                    Else
                        cmd.Parameters.AddWithValue("@VHD_TAX_CODE", "")
                    End If

                    If (ViewState("datamode") = "edit") Then
                        cmd.Parameters.AddWithValue("@bEdit", True)
                    Else
                        cmd.Parameters.AddWithValue("@bEdit", False)
                    End If
                    cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmd.ExecuteNonQuery()
                    iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

                    Dim success_msg As String = ""
                    If iReturnvalue <> 0 Then
                        Exit For
                    Else
                        AccountFunctions.SAVETRANHDRONLINE_D("", Session("SUB_ID"), _
                    Session("dtDTL").Rows(iIndex)("Id"), Session("BANKTRAN"), cmbCurrency.SelectedItem.Text, _
                    txtExchRate.Text, txtLocalRate.Text, Session("sBsuid"), Session("F_YEAR"), _
                    p_docno, Session("dtDTL").Rows(iIndex)("AccountId"), "CR", Session("dtDTL").Rows(iIndex)("Amount"), _
                    txtdocDate.Text, "", Session("dtDTL").Rows(iIndex)("ChqDate"), _
                    Session("dtDTL").Rows(iIndex)("ChqNo"), False, Session("dtDTL").Rows(iIndex)("Narration"), stTrans)
                        For il As Integer = 0 To Session("dtSettle").Rows.count - 1 ''sadhu
                            If Session("dtDTL").Rows(iIndex)("Id") = Session("dtSettle").rows(il)("Id") And _
                            Session("dtDTL").Rows(iIndex)("AccountId") = Session("dtSettle").rows(il)("Accountid") Then
                                Dim SOL_DOCDT As DateTime

                                If CDate(txtdocDate.Text) > CDate(Session("dtDTL").Rows(iIndex)("ChqDate")) Then
                                    SOL_DOCDT = txtdocDate.Text
                                Else
                                    SOL_DOCDT = Session("dtDTL").Rows(iIndex)("ChqDate")
                                End If


                                str_err = AccountFunctions.SAVESETTLEONLINE_D(Session("Sub_ID"), Session("dtDTL").Rows(iIndex)("Id"), _
                                      Session("BANKTRAN"), txtExchRate.Text, txtLocalRate.Text, Session("sBsuid"), _
                                      Session("F_YEAR"), p_docno, _
                                      Session("dtDTL").Rows(iIndex)("AccountId"), Session("dtSettle").rows(il)("Amount"), cmbCurrency.SelectedItem.Text, _
                                    SOL_DOCDT, Session("dtSettle").rows(il)("jnlid"), stTrans)

                            End If

                        Next
                    End If
                    cmd.Parameters.Clear()
                Else
                    If Not Session("dtDTL").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                        Session("iDeleteCount") = Session("iDeleteCount") + 1
                        cmd = New SqlCommand("DeleteVOUCHER_D", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                        sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                        cmd.Parameters.Add(sqlpGUID)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        Dim success_msg As String = ""

                        If iReturnvalue <> 0 Then
                            Response.Write("DSF")
                        End If
                        cmd.Parameters.Clear()
                    End If
                End If
            Next
            If iIndex <= Session("dtDTL").Rows.Count - 1 Then
                Return iReturnvalue
            Else
                Return iReturnvalue
            End If
            'Adding transaction info
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function



    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), Session("BANKTRAN"), _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                txtdocDate.Text, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), Session("BANKTRAN"), "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue
    End Function



    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then 'And Session("dtCostChild").Rows(iIndex)("costcenter") = p_costid Then
                dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
            End If
        Next
        If Math.Round(dTotal, 4) = Math.Round(p_total, 4) Then
            Return True
        Else
            Return False
        End If
    End Function



    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        '   --- Remove ReadOnly From The Form ' sadhu
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                'tr_errLNE.Visible = True
                lblError.Text = getErrorMessage(str_)
            Else
                'tr_errLNE.Visible = True
                lblError.Text = "Did not get lock"
            End If
        Else
            h_editorview.Value = "Edit"
            ViewState("datamode") = "edit"
            chkAdvance.Enabled = False
            Session("dtSettle") = DataTables.CreateDataTable_Settle
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'ImageButton1.Enabled = False
        End If
    End Sub



    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '   --- Remove ReadOnly From The Form 
        h_editorview.Value = ""
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")
        chkAdvance.Enabled = True
        'set_collection()
        Call Clear_Header()
        Call Clear_Details()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Session("gintGridLine") = 1
        Session("dtSettle").rows.clear()
        GridInitialize()
        Session("gDtlDataMode") = "ADD"
        Session("dtDTL") = DataTables.CreateDataTable_BR()

        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("dtSettle") = DataTables.CreateDataTable_Settle

        txtdocDate.Text = GetDiplayDate()
        If ViewState("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
        imgCalendar.Enabled = True
        chkAdvance.Enabled = True
        bind_Currency()
    End Sub



    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL, lstrSQL2 As String

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer

            lstrSQL = "SELECT A.*,REPLACE(CONVERT(VARCHAR(11), A.VHH_DOCDT, 106), ' ', '/') as DocDate ,isNULL(VHH_Amount,0) as Amount,B.COL_DESCR, isNULL(A.VHH_REFNO,'') as OldDocNo FROM VOUCHER_H A    INNER JOIN COLLECTION_M B ON A.VHH_COL_ID=B.COL_ID WHERE A.GUID='" & Session("Eid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                txtdocNo.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")
                txtOldDocNo.Text = ds.Tables(0).Rows(0)("OldDocNo")
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("VHH_DOCDT"), "dd/MMM/yyyy")
                Session("SessDocDate") = txtdocDate.Text
                txtBankCode.Text = ds.Tables(0).Rows(0)("VHH_ACT_ID")
                txtBankDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("VHH_ACT_ID"))
                txtAmount.Text = AccountFunctions.Round(ds.Tables(0).Rows(0)("Amount"))
                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Text = ds.Tables(0).Rows(0)("VHH_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtExchRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE1")
                txtLocalRate.Text = ds.Tables(0).Rows(0)("VHH_EXGRATE2")
                txtNarrn.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                'txtColln.Text = ds.Tables(0).Rows(0)("VHH_COL_ID")
                'txtCollnDescr.Text = ds.Tables(0).Rows(0)("COL_DESCR")
                Dim dtDTL As DataTable = CostCenterFunctions.ViewVoucherDetails(ds.Tables(0).Rows(0)("VHH_SUB_ID"), ds.Tables(0).Rows(0)("VHH_BSU_ID"), _
               Session("BANKTRAN"), ds.Tables(0).Rows(0)("VHH_DOCNO"))


                '   --- Initialize The Grid With The Data From The Detail Table
                'lstrSQL2 = "SELECT Convert(VarChar,A.VHD_LINEID) as Id,A.VHD_ACT_ID as AccountId,C.ACT_NAME as AccountName,A.VHD_NARRATION as Narration,A.VHD_RSS_ID as CLine,A.VHD_COL_ID as Colln,A.VHD_AMOUNT as Amount,A.VHD_CHQID as ChqBookId,A.VHD_CHQNO as ChqBookLot,A.VHD_CHQNO as ChqNo," _
                '           & " REPLACE(CONVERT(VARCHAR(11), A.VHD_CHQDT, 106), ' ', '/') as  ChqDate,'' as Status,A.GUID,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd FROM VOUCHER_D A INNER JOIN vw_OSA_ACCOUNTS_M C ON A.VHD_ACT_ID=C.ACT_ID" _
                '           & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'  "
                'ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                'Session("dtDTL") = DataTables.CreateDataTable_BR()
                Session("dtDTL") = dtDTL

                Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(ds.Tables(0).Rows(0)("VHH_SUB_ID"), ds.Tables(0).Rows(0)("VHH_BSU_ID"), _
    Session("BANKTRAN"), ds.Tables(0).Rows(0)("VHH_DOCNO"))

                Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(ds.Tables(0).Rows(0)("VHH_SUB_ID"), ds.Tables(0).Rows(0)("VHH_BSU_ID"), _
                Session("BANKTRAN"), ds.Tables(0).Rows(0)("VHH_DOCNO"))


                lstrSQL2 = "SELECT MAx(VHD_LINEID) as Id,Max(isNull(VHD_CHQID,'')) as CHQID FROM VOUCHER_D A " _
                           & " WHERE A.VHD_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VHD_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "'   AND A.VHD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VHD_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                Session("gintGridLine") = ds2.Tables(0).Rows(0)("Id") + 1
                hCheqBook.Value = ds2.Tables(0).Rows(0)("CHQID")
                h_NextLine.Value = Session("gintGridLine")
                '   ----  Initalize the Cost Center Grid
                'Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
                'lstrSQL2 = "SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,A.VDS_CCS_ID as Costcenter,A.VDS_CODE as Memberid,A.VDS_DESCr as Name,VDS_ERN_ID ERN_ID,A.VDS_AMOUNT as Amount," _
                '                           & " '' as Status,A.GUID, VDS_CSS_CSS_ID as SubMemberId  FROM VOUCHER_D_S A" _
                '                           & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("VHH_BSU_ID") & "' " _
                '                           & " AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "' AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("VHH_DOCNO") & "' AND VDS_CODE IS NOT NULL  AND VDS_Auto IS NULL "

                'ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                'Session("dtCostChild") = ds2.Tables(0)
                'chkAdvance.Enabled = False

                gvDTL.DataSource = dtDTL
                gvDTL.DataBind()

                If BSU_IsTAXEnabled = True Then
                    gvDTL.Columns(16).Visible = True
                Else
                    gvDTL.Columns(16).Visible = False
                End If
            Else
                lblError.Text = "Record Not Found !!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Record Not Found !!! "
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            unlock()
            h_editorview.Value = ""
            Call Clear_Details()
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim Status As Integer
        Try
            '   --- CALL A GENERAL FUNCTION TO DELETE BY PASSING THE GUID AND TRAN TYPE ---
            Status = VoucherFunctions.DeleteVOUCHER(Session("BANKTRAN"), Session("sBsuid"), Session("Sub_ID"), Session("sUsr_name"), Session("Eid"))
            If Status <> 0 Then
                lblError.Text = (UtilityObj.getErrorMessage(Status))
                Exit Sub
            Else
                Status = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "delete", Page.User.Identity.Name.ToString, Me.Page)
                If Status <> 0 Then
                    Throw New ArgumentException("Could not complete your request")
                End If
                Call Clear_Details()
                lblError.Text = "Record Deleted Successfully"
            End If
        Catch myex As ArgumentException
            lblError.Text = "Record could not be Deleted"
            UtilityObj.Errorlog(myex.Message, Page.Title)
        Catch ex As Exception
            lblError.Text = "Record could not be Deleted"
            UtilityObj.Errorlog(ex.Message)

        End Try
        'End Using
        ViewState("datamode") = "none"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'If datamode <> "add" Then
        btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
        'End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        repSource = VoucherReports.BankReceiptVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BR", txtdocNo.Text, Session("HideCC"))
        Session("ReportSource") = repSource
        ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub



    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
            If txtChqDate.Text.Trim = "" Then
                txtChqDate.Text = strfDate
            End If
        End If
        bind_Currency()
        If ViewState("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
    End Sub

    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocDate.Text = strfDate
            If txtChqDate.Text.Trim = "" Then
                txtChqDate.Text = strfDate
            End If
        End If
        bind_Currency()
        If ViewState("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If

    End Sub

    Protected Sub imgBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBank.Click
        chk_bankcode()
    End Sub



    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
    End Sub

    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid bank selected"
            txtBankCode.Focus()
        Else
            lblError.Text = ""
            cmbCurrency.Focus()
        End If
        'set_collection()
    End Sub

    Sub Set_SettleControls()
        chk_DetailsAccount()

        txtPartyDescr.Text = AccountFunctions.Validate_Account(Detail_ACT_ID.Text, Session("sbsuid"), "NOTCC")
        Dim acttype As String = AccountFunctions.check_accounttype(Detail_ACT_ID.Text, Session("sBsuid"))
        If acttype = "C" Then
            set_AdvanceControls()
        Else
            txtLineAmount.Attributes.Remove("readonly")
            lbSettle.Visible = False
        End If
        If txtPartyDescr.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            Detail_ACT_ID.Focus()
        Else
            lblError.Text = ""
            txtLineNarrn.Focus()
        End If
    End Sub


    'Protected Sub Detail_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail_ACT_ID.TextChanged
    '    Set_SettleControls()
    '    ClearRadGridandCombo()
    'End Sub


    Protected Sub ddDCollection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddDCollection.SelectedIndexChanged
        Dim str_collection As String = AccountFunctions.get_CollectionAccount(ddDCollection.SelectedItem.Value, Session("sBsuid"))
        Try
            Detail_ACT_ID.Text = str_collection.Split("|")(0)
            txtPartyDescr.Text = str_collection.Split("|")(1)
        Catch ex As Exception
        End Try
    End Sub
    Private Function get_totalforaline() As Decimal
        Try
            Dim dTotal As Decimal = 0
            For i As Integer = 0 To Session("dtSettle").rows.count - 1
                If Session("dtSettle").rows(i)("Accountid") = Detail_ACT_ID.Text And _
                 Session("dtSettle").rows(i)("Id") = h_NextLine.Value Then
                    dTotal = dTotal + Convert.ToDecimal(Session("dtSettle").rows(i)("Amount"))
                End If
            Next
            Return dTotal
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function
    Sub set_AdvanceControls()

        If Not Detail_ACT_ID.Text = "" Then
            If chkAdvance.Checked = True Then
                txtLineAmount.Attributes.Remove("readonly")
                lbSettle.Visible = False
                Session("dtSettle").Rows.Clear()
            Else
                txtLineAmount.Text = get_totalforaline()
                If txtLineAmount.Text = "0" Then
                    txtLineAmount.Text = ""
                End If
                txtLineAmount.Attributes.Add("readonly", "readonly")

                lbSettle.Visible = True
            End If
        End If
    End Sub
    Protected Sub chkAdvance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAdvance.CheckedChanged
        set_AdvanceControls()
    End Sub
    Sub chk_DetailsAccount()
        txtPartyDescr.Text = AccountFunctions.Validate_Account(Detail_ACT_ID.Text, Session("sbsuid"), "NOTCC")
        If txtPartyDescr.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            Detail_ACT_ID.Focus()
        Else
            lblError.Text = ""
            txtLineNarrn.Focus()
        End If
    End Sub

    Protected Sub imgRcvd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRcvd.Click
        chk_DetailsAccount()
        Dim acttype As String = AccountFunctions.check_accounttype(Detail_ACT_ID.Text, Session("sBsuid"))
        Dim strPayTerm As String = AccountFunctions.GetPaymentTerm(Detail_ACT_ID.Text)
        ' lblPaymentTerm.Text = ""
        If strPayTerm <> "" Then
            'lblPaymentTerm.Text = "Payment Term : " & strPayTerm
            ' trPayTerm.Visible = True
        Else
            'trPayTerm.Visible = False
        End If
        If acttype = "C" Then
            set_AdvanceControls()
        Else
            txtLineAmount.Attributes.Remove("readonly")
            lbSettle.Visible = False
        End If
    End Sub

    Sub SetCostcenterControls()
        ddlCostCenter.DataBind()
        If Not ddlCostCenter.Items.FindByText("DEPARTMENT") Is Nothing Then
            ddlCostCenter.ClearSelection()
            ddlCostCenter.Items.FindByText("DEPARTMENT").Selected = True
        End If
        ShowUploadOption()
    End Sub

    Private Const ItemsPerRequest As Integer = 10
    Protected Sub RadComboBox1_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
        Dim data As DataTable = CostCenterFunctions.GetCostCenterData(e.Text, ddlCostCenter.SelectedItem.Value, Session("sBsuid"), _
        txtdocDate.Text, Session("CostOTH"))

        Dim itemOffset As Integer = e.NumberOfItems
        Dim endOffset As Integer = Math.Min(itemOffset + ItemsPerRequest, data.Rows.Count)
        e.EndOfItems = endOffset = data.Rows.Count

        For i As Integer = itemOffset To endOffset
            If data.Rows.Count > i Then
                o.Items.Add(New RadComboBoxItem(data.Rows(i)("Name").ToString() & " - " & data.Rows(i)("Column1").ToString(), data.Rows(i)("ID").ToString()))
            End If
        Next

        e.Message = GetStatusMessage(endOffset, data.Rows.Count)
    End Sub

    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function

    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIdCostchild As New Label
            lblIdCostchild = e.Row.FindControl("lblIdCostchild")
            Dim gvCostAllocation As New GridView
            gvCostAllocation = e.Row.FindControl("gvCostAllocation")
            If Not Session("CostAllocation") Is Nothing AndAlso Not gvCostAllocation Is Nothing Then
                gvCostAllocation.Attributes.Add("bordercolor", "#fc7f03")
                Dim dv As New DataView(Session("CostAllocation"))
                If Session("CostAllocation").Rows.Count > 0 Then
                    dv.RowFilter = "CostCenterID='" & lblIdCostchild.Text & "' "
                End If
                dv.Sort = "CostCenterID"
                gvCostAllocation.DataSource = dv.ToTable
                gvCostAllocation.DataBind()
            End If
        End If
    End Sub

    Protected Sub ddlCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCenter.SelectedIndexChanged
        ClearRadGridandCombo()
        ShowUploadOption()
    End Sub

    Sub ShowUploadOption()
        If ddlCostCenter.SelectedItem.Text.ToUpper = "EMPLOYEES" Then
            tr_UploadEmplyeeCostCenter.Visible = True
        Else
            tr_UploadEmplyeeCostCenter.Visible = False
        End If
    End Sub

    Protected Sub lbUploadEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUploadEmployee.Click
        If Not fuEmployeeData.HasFile Then
            lblError.Text = "Please select the file...!"
            Exit Sub
        End If
        If Not (fuEmployeeData.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("csv", StringComparison.OrdinalIgnoreCase)) Then
            lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
            Exit Sub
        End If
    End Sub

    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub

    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()
    End Sub

End Class
