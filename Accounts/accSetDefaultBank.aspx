<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accSetDefaultBank.aspx.vb" Inherits="Accounts_accSetDefaultBank" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function getBankReceipt() {
            //popUp('460', '400', 'BANK', '<%=txtBankReceiptCode.ClientId %>', '<%=txtBankReceiptDescr.ClientId %>')
            var result = radopen("PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankReceiptCode.ClientID%>').value, "pop_up2")
        }

        function getBankPayment() {
            //popUp('460', '400', 'BANK', '<%=txtBankPaymentCode.ClientId %>', '<%=txtBankPaymentDescr.ClientId %>');
            var result = radopen("PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankPaymentCode.ClientID%>').value, "pop_up3")
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankReceiptCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtBankReceiptDescr.ClientID%>').value = NameandCode[1];
            }
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankPaymentCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtBankPaymentDescr.ClientID%>').value = NameandCode[1];
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Set Common Settings
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblErr" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Bank Receipt</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankReceiptCode" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getBankReceipt();return false;" />
                        </td>
                        <%--<td align="left" width="20%"></td>--%>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtBankReceiptDescr" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Bank Payment</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankPaymentCode" runat="server" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getBankPayment();return false;" />
                        </td>
                        <%--<td align="left" width="20%"></td>--%>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtBankPaymentDescr" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" TabIndex="6" Text="Cancel" /></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

