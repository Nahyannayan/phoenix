<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="True" CodeFile="DayendManual.aspx.vb" Inherits="DayendManual" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <style type="text/css">
        .RadGrid {
            border-radius: 0px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            /*background-color: #00a3e0;*/
        }

        .RadGrid_Office2010Blue th.rgSorted {
            /*background-color: #00a3e0;*/
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCellLeft {
            background-position: 0 0;
            padding: 10px !important;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default {
            width: 80% !important;
        }

        .rcbReadOnly td.rcbInputCell.rcbInputCellLeft {
            /* padding: 0px !important; */
            background-color: #ffffff !important;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function SelectAll(Id) {
            var myform = document.forms[0];
            var len = myform.elements.length;
            var check = document.getElementById(Id).checked == true ? false : true;
            document.getElementById(Id).checked == true ? document.getElementById(Id).checked = false : document.getElementById(Id).checked = true;
            for (var i = 0; i < len; i++) {
                if (myform.elements[i].type == 'checkbox') {
                    if (check) {
                        myform.elements[i].checked = false;
                    }
                    else {
                        myform.elements[i].checked = true;
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Open Day\Month End Date
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:Button ID="btnFee" runat="server" Text="Save" AccessKey="M" Width="1" Height="1" Visible="false" />
                <table align="center" width="100%">

                    <tr>
                        <td>
                            <ajaxToolkit:TabContainer ID="TCMainContainer" runat="server"
                                ActiveTabIndex="0">
                                <ajaxToolkit:TabPanel ID="TPDMendDate" runat="server" HeaderText="qwerty">
                                    <HeaderTemplate>
                                        Day\Month End
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table align="center" width="100%">
                                            <tr id="mainCostCenter" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">Business Unit</span></td>
                                                <td align="left" runat="server" colspan="3">
                                                    <telerik:RadComboBox ID="DDLBsu" runat="server" DataTextField="BSU_NAME" AutoPostBack="True"
                                                        DataValueField="BSU_ID" Filter="Contains"
                                                        ZIndex="2000">
                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                            <tr class="matters">
                                                <td align="left" width="20%"><span class="field-label">DayEnd Date</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="DTDayend" runat="server" AccessKey="k"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCalendarD" runat="server" CausesValidation="False"
                                                        ImageAlign="Middle" ImageUrl="~/Images/calendar.gif" />
                                                </td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>
                                            <tr id="trCode" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">MonthEnd Date</span></td>
                                                <td align="left" runat="server">
                                                    <asp:TextBox ID="DTMonthend" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCalendarM" runat="server" CausesValidation="False"
                                                        ImageAlign="Middle" ImageUrl="~/Images/calendar.gif" />
                                                </td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>
                                            <tr id="Tr1" runat="server">
                                                <td align="center" colspan="4" runat="server">
                                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" /></td>
                                            </tr>
                                            <tr id="Tr2" runat="server">
                                                <td id="Td1" align="center" colspan="4" runat="server">

                                                    <asp:GridView ID="gvAuditLog" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row"
                                                        AllowPaging="True" PageSize="5">
                                                        <RowStyle CssClass="griditem"></RowStyle>

                                                        <Columns>
                                                            <asp:BoundField DataField="DME_OLD_DayDT" HeaderText="Prev. Day End" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DME_OLD_MonthDT" HeaderText="Prev.Month End" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="DME_New_DayDT" HeaderText="New Day End" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="DME_New_MonthDT" HeaderText="New Month End" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="MUSER" HeaderText="Log User">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DME_MODIDate" DataFormatString="{0:dd/MMM/yyyy H:mm:ss}" HeaderText="Log Date">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle></HeaderStyle>
                                                        <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <ajaxToolkit:CalendarExtender ID="calDayDate" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgCalendarD" TargetControlID="DTDayend" Enabled="True">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <ajaxToolkit:CalendarExtender ID="calMDate" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgCalendarM" TargetControlID="DTMonthend" Enabled="True">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>

                            </ajaxToolkit:TabContainer>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

