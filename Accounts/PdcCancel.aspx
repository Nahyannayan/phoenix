<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PdcCancel.aspx.vb" Inherits="Accounts_PdcCancel" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state1 = document.getElementById("<%=CheckDeferral.ClientID%>").checked;
            var chk_state = document.getElementById("ChkSelAll").checked;

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }

            document.getElementById("<%=CheckDeferral.ClientID%>").checked = chk_state1;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label></td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td>
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" AllowPaging="True" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="ChkSelect" type="checkbox" runat="server" value='<%# Bind("VHD_ID") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No<br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" OnClick="btnSearchControl_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Doc Date<br />
                                            <asp:TextBox ID="txtDocDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="btnSearchControl_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("VHH_DOCDT") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Bank
                                            <br />
                                            <asp:TextBox ID="txtBankAC" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAC" runat="server" Text='<%# Bind("BANK") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Party Name
                                            <br />
                                            <asp:TextBox ID="txtParty" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnPartySearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblParty" runat="server" Text='<%# Bind("PARTY") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Narration<br />
                                            <asp:TextBox ID="txtNarrn" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNarrn" runat="server" Text='<%# Bind("VHD_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Amount<br />
                                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("VHD_AMOUNT") %>'></asp:Label><br />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Chque No<br />
                                            <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" OnClick="btnSearchControl_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("VHD_CHQNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Change Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtChargeDate" runat="server" Text='<%# Bind("VHD_CHQDT", "{0:dd/MMM/yyyy}") %>' OnPreRender="txtChargeDate_PreRender"></asp:TextBox>
                                            <asp:ImageButton ID="imgDate" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalChargeDate" runat="server" CssClass="MyCalendar" TargetControlID="txtChargeDate" PopupButtonID="imgDate" Format="dd/MMM/yyyy" PopupPosition="BottomLeft">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Chque Date<br />
                                            <asp:TextBox ID="txtChqDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnChqdateSearch" OnClick="btnSearchControl_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqDate" runat="server" Text='<%# Bind("VHD_CHQDT") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="TableSave" runat="server" align="center" width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:Button CssClass="button" ID="btnSave" runat="server" TabIndex="26" Text="Save" OnClick="btnSave_Click" />
                                        <asp:CheckBox ID="CheckDeferral" runat="server" Text="PDC  Deferral" AutoPostBack="false"></asp:CheckBox>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_10" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>

