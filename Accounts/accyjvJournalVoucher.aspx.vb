Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Accounts_accyjvJournalVoucher
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        If Page.IsPostBack = False Then
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If
            bind_groupPolicy()
            initialize_components()
            'txtHNarration.Attributes.Add("onblur", "javascript:CopyDetails()")
            txtHNarration.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtHNarration.ClientID & "');")
            txtDNarration.Attributes.Add("onBlur", "narration_check('" & txtDNarration.ClientID & "');")

            '''''check menu rights
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            If (Request.QueryString("edited") <> "") Then
                lblError.Text = getErrorMessage("521")
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or MainMnu_code <> "A753010" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
                ViewState("menu_rights"), ViewState("datamode"))
            End If
            '''''check menu rights
            If Request.QueryString("viewid") <> "" Then
                set_viewdata()
                btnAdd.Visible = True
                btnEdit.Visible = True
                setViewData()
                setModifyHeader(Request.QueryString("viewid"))
            Else
                ResetViewData()
            End If
            gridbind()
            bind_Currency()
            If Request.QueryString("invalidedit") = "1" Then
                lblError.Text = "Invalid Editid"
                Exit Sub
            End If
            If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
                getnextdocid()
            End If
            ''call the object during the page load to check the initial stage
            UtilityObj.beforeLoopingControls(Me.Page)
        End If

    End Sub


    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()
        ViewState("str_timestamp") = New Byte()
        txtHExchRate.Attributes.Add("readonly", "readonly")
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")

        txtDAccountName.Attributes.Add("readonly", "readonly")
        txtHLocalRate.Attributes.Add("readonly", "readonly")
        Session("dtJournal") = DataTables.CreateDataTable_JV()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        ViewState("idJournal") = 0
        Session("idCostChild") = 0
        'btnCancel.Visible = False
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
    End Sub


    Private Sub setViewData()
        tbl_Details.Visible = False

        gvJournal.Columns(6).Visible = False
        gvJournal.Columns(7).Visible = False
        btnHAccount.Enabled = False
        imgCalendar.Enabled = False
        DDCurrency.Enabled = False
        txtHDocdate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
        txtDAccountCode.Attributes.Add("readonly", "readonly")
        txtHOldrefno.Attributes.Add("readonly", "readonly")
    End Sub


    Private Sub ResetViewData()

        tbl_Details.Visible = True

        gvJournal.Columns(6).Visible = True
        gvJournal.Columns(7).Visible = True

        btnHAccount.Enabled = True
        imgCalendar.Enabled = True
        DDCurrency.Enabled = True
        txtHDocdate.Attributes.Remove("readonly")
        txtHNarration.Attributes.Remove("readonly")
        txtHOldrefno.Attributes.Remove("readonly")

    End Sub


    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM YJOURNAL_H where GUID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocno.Text = p_Modifyid
                txtHDocdate.Text = Format(CDate(ds.Tables(0).Rows(0)("YHD_DOCDT")), "dd/MMM/yyyy")
                txtHNarration.Text = ds.Tables(0).Rows(0)("YHD_NARRATION")
                txtHOldrefno.Text = ds.Tables(0).Rows(0)("YHD_REFNO") & ""
                txtHDocno.Text = ds.Tables(0).Rows(0)("YHD_DOCNO")
                bind_Currency()
                setModifyDetails(p_Modifyid)
            Else
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyDetails(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM YJOURNAL_D where YJL_DOCNO='" _
            & ViewState("str_editData").Split("|")(0) & "'and YJL_BDELETED='False' " _
            & " AND (YJL_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (YJL_SUB_ID = '" & Session("Sub_ID") & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'JNL_DEBIT, JNL_CREDIT, JNL_NARRATION, 
                    Dim rDt As DataRow
                    'Dim i As Integer
                    Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("YJL_ACT_ID"))
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("YJL_SLNO")

                    ViewState("idJournal") = ViewState("idJournal") + 1
                    rDt("Accountid") = ds.Tables(0).Rows(iIndex)("YJL_ACT_ID")
                    rDt("Accountname") = str_actname_cost_mand.Split("|")(0)
                    rDt("Narration") = ds.Tables(0).Rows(iIndex)("YJL_NARRATION")

                    rDt("Credit") = ds.Tables(0).Rows(iIndex)("YJL_CREDIT")
                    rDt("Debit") = ds.Tables(0).Rows(iIndex)("YJL_DEBIT")

                    rDt("CostCenter") = str_actname_cost_mand.Split("|")(1)
                    rDt("Required") = Convert.ToBoolean(str_actname_cost_mand.Split("|")(2))
                    rDt("Status") = "Normal"
                    Session("dtJournal").Rows.Add(rDt)
                Next
                setModifyCost(p_Modifyid)
            Else

            End If
            ViewState("idJournal") = ViewState("idJournal") + 1

            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyCost(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            ' ''str_Sql = "SELECT GUID, JDS_DOCTYPE ,JDS_DOCNO, JDS_DESCR," _
            ' ''& "JDS_ACT_ID ,JDS_CCS_ID ,JDS_CODE ,JDS_AMOUNT," _
            ' ''& "JDS_SLNO,JDS_CCS_ID FROM JOURNAL_D_S WHERE JDS_DOCNO='" _
            ' '' &  viewstate("str_editData").Split("|")(0) & "' and JDS_BDELETED='False' "

            str_Sql = "SELECT JOURNAL_D_S.GUID, JOURNAL_D_S.JDS_DOCTYPE," _
            & " JOURNAL_D_S.JDS_DOCNO," _
            & " case isnull(JOURNAL_D_S.JDS_CODE,'')" _
            & " when '' then  COSTCENTER_S.CCS_DESCR else" _
            & " JOURNAL_D_S.JDS_DESCR end JDS_DESCR ," _
            & " JOURNAL_D_S.JDS_ACT_ID, JOURNAL_D_S.JDS_CCS_ID," _
            & " JOURNAL_D_S.JDS_CODE, JOURNAL_D_S.JDS_AMOUNT," _
            & " JOURNAL_D_S.JDS_SLNO FROM JOURNAL_D_S INNER JOIN" _
            & " COSTCENTER_S ON JOURNAL_D_S.JDS_CCS_ID = COSTCENTER_S.CCS_ID" _
            & " WHERE (JOURNAL_D_S.JDS_DOCNO = '" & ViewState("str_editData").Split("|")(0) & "')" _
            & " AND (JOURNAL_D_S.JDS_bDELETED = 'False')" _
            & " and (JOURNAL_D_S.JDS_DOCTYPE='JV')" _
            & " AND (JOURNAL_D_S.JDS_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (JOURNAL_D_S.JDS_SUB_ID = '" & Session("Sub_ID") & "')"

            ' '' ''str_Sql = "SELECT JOURNAL_D_S.GUID, JOURNAL_D_S.JDS_DOCTYPE," _
            ' '' ''& " JOURNAL_D_S.JDS_DOCNO," _
            ' '' ''& " CASE isnull(JOURNAL_D_S.JDS_CODE, '')" _
            ' '' ''& " WHEN '' THEN COSTCENTER_S.CCS_DESCR" _
            ' '' ''& " ELSE JOURNAL_D_S.JDS_DESCR END AS JDS_DESCR," _
            ' '' ''& " JOURNAL_D_S.JDS_ACT_ID, JOURNAL_D_S.JDS_CCS_ID," _
            ' '' ''& " JOURNAL_D_S.JDS_CODE, JOURNAL_D_S.JDS_AMOUNT," _
            ' '' ''& " JOURNAL_D_S.JDS_SLNO, ACCOUNTS_M.ACT_NAME" _
            ' '' ''& " FROM JOURNAL_D_S INNER JOIN" _
            ' '' ''& " COSTCENTER_S ON" _
            ' '' ''& " JOURNAL_D_S.JDS_CCS_ID = COSTCENTER_S.CCS_ID" _
            ' '' ''& " INNER JOIN ACCOUNTS_M" _
            ' '' ''& " ON JOURNAL_D_S.JDS_ACT_ID = ACCOUNTS_M.ACT_ID" _
            ' '' ''& " WHERE     (JOURNAL_D_S.JDS_DOCNO = '0708JV-00044')" _
            ' '' ''& " AND (JOURNAL_D_S.JDS_bDELETED = 'False')" _
            ' '' ''& " AND (JOURNAL_D_S.JDS_DOCTYPE = 'JV')"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim rDt As DataRow
                    'Dim i As Integer
                    'Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("JNL_ACT_ID"))
                    rDt = Session("dtCostChild").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Name") = ds.Tables(0).Rows(iIndex)("JDS_DESCR") & ""
                    rDt("Memberid") = ds.Tables(0).Rows(iIndex)("JDS_CODE")
                    rDt("VoucherId") = ds.Tables(0).Rows(iIndex)("JDS_SLNO")
                    rDt("Costcenter") = ds.Tables(0).Rows(iIndex)("JDS_CCS_ID")
                    'rDt("Name") = ds.Tables(0).Rows(iIndex)("JDS_CCS_ID")
                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("JDS_AMOUNT")
                    rDt("Status") = "Normal"
                    'idCostChild = idCostChild + 1
                    Session("dtCostChild").Rows.Add(rDt)
                Next
            Else
            End If
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function getAccountname(ByVal p_accountid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M where ACT_ID='" & p_accountid & "' "

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & p_accountid & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_NAME") & "|" _
                & ds.Tables(0).Rows(0)("PLY_COSTCENTER") & "|" _
                & ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else

            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
    End Function


    Sub bind_groupPolicy()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
            & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


            'Dim properFAQs As DataView = ds.Tables(0).DefaultView
            'properFAQs.RowFilter = "FAQCategoryID=" & ""

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub getnextdocid()
        If ViewState("datamode") <> "edit" Then
            Try
                txtHDocno.Text = AccountFunctions.GetNextDocId("YJV", Session("sBsuid"), CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                If txtHDocno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If

    End Sub


    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            ' Dim str_Sql As String
            ' str_Sql = "SELECT A.EXG_ID,A.EXG_CUR_ID," _
            '& " A.EXG_RATE,B.EXG_RATE LOCAL_RATE," _
            '& " (ltrim(B.EXG_RATE)+'__'+" _
            '& " ltrim(a.EXG_RATE))+'__'+" _
            '& " ltrim(a.EXG_ID) as RATES " _
            '& " FROM EXGRATE_S A,EXGRATE_S B" _
            '& " Where A.EXG_BSU_ID='" & Session("sBsuid") & "'" _
            '& " AND '" & txtHDocdate.Text & "' BETWEEN A.EXG_FDATE AND ISNULL(A.EXG_TDATE , GETDATE())" _
            '& " AND " _
            '& " B.EXG_BSU_ID='" & Session("sBsuid") & "' AND B.EXG_TDATE IS NULL" _
            '& " AND " _
            '& " B.EXG_CUR_ID='" & Session("BSU_CURRENCY") & "'"
            ' '& " order by gm.GPM_DESCR "
            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            DDCurrency.Items.Clear()
            DDCurrency.DataSource = MasterFunctions.GetExchangeRates(txtHDocdate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If DDCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
                btnSave.Enabled = True
            Else
                txtHExchRate.Text = "0"
                txtHLocalRate.Text = "0"
                btnSave.Enabled = False
                lblError.Text = "Cannot Save Data. Currency/Exchange Rate Not Set"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub



    Private Function set_default_currency() As Boolean
        Try
            'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            'Dim str_Sql As String
            'str_Sql = "SELECT     BSU_ID," _
            '& " BSU_SUB_ID, BSU_CURRENCY" _
            '& " FROM vw_OSO_BUSINESSUNIT_M" _
            '& " WHERE (BSU_ID = '" & Session("sBsuid") & "')"

            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            'If ds.Tables(0).Rows.Count > 0 Then
            For Each item As ListItem In DDCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            'Else
            'End If
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function


    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        Dim strfDate As String = txtHDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        bind_Currency()
        getnextdocid()
        txtHNarration.Focus()
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub


    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdds.Click
        txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))
        Dim str_cost_center As String = ""
        '''''FIND ACCOUNT IS THERE
        Dim bool_cost_center_reqired As Boolean = False

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            'str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M" _
            '& " WHERE ACT_ID='" & p_accid & "'" _
            '& " AND ACT_Bctrlac='FALSE'"

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else
                txtDAccountName.Text = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            txtDAccountName.Text = ""
        End Try
        '''''FIND ACCOUNT IS THERE
        If txtDAccountName.Text = "" Then
            lblError.Text = getErrorMessage("303") ' account already there
            Exit Sub
        End If
        Try
            Dim rDt As DataRow

            Dim i As Integer
            Dim dCrorDb As Double
            dCrorDb = CDbl(txtDAmount.Text.Trim)
            If dCrorDb <> 0 Then
                rDt = Session("dtJournal").NewRow
                rDt("Id") = ViewState("idJournal")
                ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = txtDAccountCode.Text.Trim
                rDt("Accountname") = txtDAccountName.Text.Trim
                rDt("Narration") = txtDNarration.Text.Trim
                If dCrorDb > 0 Then
                    rDt("Credit") = "0"
                    rDt("Debit") = dCrorDb
                Else
                    rDt("Credit") = dCrorDb * -1
                    rDt("Debit") = "0"
                End If
                rDt("CostCenter") = str_cost_center
                rDt("Required") = bool_cost_center_reqired
                rDt("GUID") = System.DBNull.Value

                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Accountid") = rDt("Accountid") And _
                         Session("dtJournal").Rows(i)("Accountname") = rDt("Accountname") And _
                          Session("dtJournal").Rows(i)("Narration") = rDt("Narration") And _
                          Session("dtJournal").Rows(i)("Credit") = rDt("Credit") And _
                          Session("dtJournal").Rows(i)("Debit") = rDt("Debit") Then
                        lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        gridbind()
                        Exit Sub
                    End If
                Next
                Session("dtJournal").Rows.Add(rDt)
            End If
            gridbind()
            Clear_Details()
            If gvJournal.Rows.Count = 1 Then
                txtDAmount.Text = AccountFunctions.Round(dCrorDb) * -1
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = getErrorMessage("510")
        End Try

    End Sub


    Private Sub gridbind()
        Try


            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = DataTables.CreateDataTable_JV()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        If Session("dtJournal").Rows(i)("Credit") <> 0 Then
                            dCredit = dCredit + Session("dtJournal").Rows(i)("Credit")
                        Else
                            dDebit = dDebit + Session("dtJournal").Rows(i)("Debit")
                        End If
                        ' dTotAmount = dTotAmount + session("dtCostChild").Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtTDotalDebit.Text = AccountFunctions.Round(dDebit)
            txtTotalCredit.Text = AccountFunctions.Round(dCredit)
            txtDifference.Text = AccountFunctions.Round(dDebit - dCredit)
            If dDebit <> dCredit Or dCredit = 0 Then
                btnSave.Enabled = False
            Else
                btnSave.Enabled = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function get_mandatory_costcenter(ByVal p_id As String) As String
        If Session("dtJournal").Rows.Count > 0 Then
            For i As Integer = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(i)("id") & "" = p_id Then
                    Return Session("dtJournal").Rows(i)("Costcenter")
                End If
            Next
        End If
        Return ""
    End Function


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
            Dim lblReqd As New Label
            Dim lblid As New Label

            Dim lblDebit As New Label
            Dim lblCredit As New Label

            Dim btnAlloca As New LinkButton

            lblReqd = e.Row.FindControl("lblRequired")
            lblid = e.Row.FindControl("lblId")

            lblDebit = e.Row.FindControl("lblDebit")
            lblCredit = e.Row.FindControl("lblCredit")

            btnAlloca = e.Row.FindControl("btnAlloca")
            If btnAlloca IsNot Nothing Then
                Dim dAmt As Double

                If CDbl(lblDebit.Text) > 0 Then
                    dAmt = CDbl(lblDebit.Text)
                Else
                    dAmt = CDbl(lblCredit.Text)
                End If

                btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & get_mandatory_costcenter(lblid.Text) & "');return false;"

            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        Try
            If btnAdds.Visible = True Then
                Dim row As GridViewRow = gvJournal.Rows(e.RowIndex)
                Dim lblTid As New Label
                '        Dim lblGrpCode As New Label
                lblTid = TryCast(row.FindControl("lblId"), Label)

                Dim iRemove As Integer = 0
                Dim str_Index As String = ""
                str_Index = lblTid.Text
                For iRemove = 0 To Session("dtJournal").Rows.Count - 1
                    If str_Index = Session("dtJournal").Rows(iRemove)("id") Then
                        If ViewState("datamode") <> "edit" Then
                            Session("dtJournal").Rows(iRemove).Delete()
                        Else
                            Session("dtJournal").Rows(iRemove)("Status") = "Deleted"
                        End If
                        Exit For
                    End If
                Next
                For iRemove = 0 To Session("dtCostChild").Rows.Count - 1
                    If str_Index = Session("dtCostChild").Rows(iRemove)("Voucherid") Then
                        'session("dtCostChild").Rows(iRemove).Delete()
                        Session("dtCostChild").Rows(iRemove)("Status") = "Deleted"
                    End If
                Next
                'gvTransaction.EditIndex = -1
                gridbind()
            Else
                lblError.Text = "Cannot delete. Please cancel updation and delete"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDAccountCode.Text = Session("dtJournal").Rows(iIndex)("Accountid")
                txtDAccountName.Text = Session("dtJournal").Rows(iIndex)("Accountname")
                If Session("dtJournal").Rows(iIndex)("Credit") > 0 Then
                    txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Credit")) * -1
                Else
                    txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Debit"))
                End If
                txtDNarration.Text = Session("dtJournal").Rows(iIndex)("Narration")
                btnAdds.Visible = False
                btnUpdate.Visible = True
                btnEditCancel.Visible = True
                gvJournal.SelectedIndex = iIndex
                'gridbind()
                Exit For
            End If
        Next
    End Sub


    Protected Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
        gvJournal.SelectedIndex = -1
        Clear_Details()
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim iIndex As Integer = 0
            Dim str_Search As String = ""
            Dim dCrordb As Double = CDbl(txtDAmount.Text)
            str_Search = h_Editid.Value
            '''''
            txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))
            Dim str_cost_center As String = ""
            '''''FIND ACCOUNT IS THERE
            Dim bool_cost_center_reqired As Boolean = False

            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String
                'str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M" _
                '& " WHERE ACT_ID='" & p_accid & "'" _
                '& " AND ACT_Bctrlac='FALSE'"

                str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
                & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
                & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
                & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
                & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
                & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
                & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

                '& " order by gm.GPM_DESCR "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                    str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                    bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
                Else
                    txtDAccountName.Text = ""
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try

            '''''
            If dCrordb = 0 Then
                Exit Sub
            End If
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If (Session("dtJournal").Rows(iIndex)("Accountid") <> txtDAccountCode.Text.Trim) Then
                        ''updation handle here
                        Dim j As Integer = 0
                        If ViewState("datamode") <> "edit" Then
                            While j < Session("dtCostChild").Rows.Count
                                If Session("dtCostChild").Rows(j)("Voucherid") = str_Search Then
                                    Session("dtCostChild").Rows.Remove(Session("dtCostChild").Rows(j))
                                Else
                                    j = j + 1
                                End If
                            End While 
                        End If
                        ''updation handle here
                    End If
                    Session("dtJournal").Rows(iIndex)("Accountid") = txtDAccountCode.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Accountname") = txtDAccountName.Text.Trim
                    If dCrordb > 0 Then
                        Session("dtJournal").Rows(iIndex)("Credit") = "0"
                        Session("dtJournal").Rows(iIndex)("Debit") = dCrordb
                    Else
                        Session("dtJournal").Rows(iIndex)("Credit") = dCrordb * -1
                        Session("dtJournal").Rows(iIndex)("Debit") = "0"
                    End If
                    Session("dtJournal").Rows(iIndex)("Narration") = txtDNarration.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Required") = bool_cost_center_reqired
                    Session("dtJournal").Rows(iIndex)("CostCenter") = str_cost_center
                    btnAdds.Visible = True
                    btnUpdate.Visible = False
                    btnEditCancel.Visible = False
                    gvJournal.SelectedIndex = iIndex
                    gvJournal.SelectedIndex = -1
                    Clear_Details()
                    gridbind()
                    Exit For
                End If
            Next
        Catch ex As Exception
            Errorlog("UPDATE")
        End Try
    End Sub


    Private Sub clear_All()
        Session("dtJournal").Rows.Clear()
        Session("dtCostChild").Rows.Clear()
        gridbind()
        Clear_Details()
        txtHOldrefno.Text = ""
        getnextdocid()
        txtHNarration.Text = ""
        txtDNarration.Text = ""
    End Sub


    Private Sub Clear_Details()
        txtDAmount.Text = ""
        txtDNarration.Text = txtHNarration.Text & ""
        txtDAccountName.Text = ""
        txtDAccountCode.Text = ""
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strfDate As String = txtHDocdate.Text.Trim

            Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            Else
                txtHDocdate.Text = strfDate
            End If

            ViewState("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here

                'Adding header info
                Dim cmd As New SqlCommand("SaveYJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpYHD_SUB_ID As New SqlParameter("@YHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpYHD_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpYHD_SUB_ID)

                Dim sqlpsqlpYHD_BSU_ID As New SqlParameter("@YHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpYHD_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpYHD_BSU_ID)

                Dim sqlpYHD_FYEAR As New SqlParameter("@YHD_FYEAR", SqlDbType.Int)
                sqlpYHD_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpYHD_FYEAR)

                Dim sqlpYHD_DOCTYPE As New SqlParameter("@YHD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpYHD_DOCTYPE.Value = "YJV"
                cmd.Parameters.Add(sqlpYHD_DOCTYPE)

                Dim sqlpYHD_DOCNO As New SqlParameter("@YHD_DOCNO", SqlDbType.VarChar, 20)
                If ViewState("datamode") = "edit" Then
                    sqlpYHD_DOCNO.Value = ViewState("str_editData").Split("|")(0) & ""
                Else
                    sqlpYHD_DOCNO.Value = " "
                End If
                cmd.Parameters.Add(sqlpYHD_DOCNO)

                Dim sqlpYHD_DOCDT As New SqlParameter("@YHD_DOCDT", SqlDbType.DateTime, 30)
                sqlpYHD_DOCDT.Value = txtHDocdate.Text & ""
                cmd.Parameters.Add(sqlpYHD_DOCDT)

                Dim sqlpYHD_CUR_ID As New SqlParameter("@YHD_CUR_ID", SqlDbType.VarChar, 12)
                sqlpYHD_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                cmd.Parameters.Add(sqlpYHD_CUR_ID)

                Dim sqlpYHD_NARRATION As New SqlParameter("@YHD_NARRATION", SqlDbType.VarChar, 300)
                sqlpYHD_NARRATION.Value = txtHNarration.Text & ""
                cmd.Parameters.Add(sqlpYHD_NARRATION)

                Dim sqlpYHD_EXGRATE1 As New SqlParameter("@YHD_EXGRATE1", SqlDbType.Decimal, 8)
                sqlpYHD_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                cmd.Parameters.Add(sqlpYHD_EXGRATE1)

                Dim sqlpYHD_EXGRATE2 As New SqlParameter("@YHD_EXGRATE2", SqlDbType.Decimal, 8)
                sqlpYHD_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                cmd.Parameters.Add(sqlpYHD_EXGRATE2)

                Dim sqlpYHD_bPOSTED As New SqlParameter("@YHD_bPOSTED", SqlDbType.Bit)
                sqlpYHD_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpYHD_bPOSTED)

                Dim sqlpYHD_bDELETED As New SqlParameter("@YHD_bDELETED", SqlDbType.Bit)
                sqlpYHD_bDELETED.Value = False
                cmd.Parameters.Add(sqlpYHD_bDELETED)

                Dim sqlpbGenerateNewNo As New SqlParameter("@bGenerateNewNo", SqlDbType.Bit)
                sqlpbGenerateNewNo.Value = True
                cmd.Parameters.Add(sqlpbGenerateNewNo)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim sqlpYHD_SESSIONID As New SqlParameter("@YHD_SESSIONID", SqlDbType.VarChar, 50)
                sqlpYHD_SESSIONID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpYHD_SESSIONID)

                Dim sqlpYHD_LOCK As New SqlParameter("@YHD_LOCK", SqlDbType.VarChar, 50)
                sqlpYHD_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpYHD_LOCK)

                Dim sqlpYHD_TIMESTAMP As New SqlParameter("@YHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                If ViewState("datamode") <> "edit" Then
                    sqlpYHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpYHD_TIMESTAMP.Value = ViewState("str_timestamp")
                End If
                cmd.Parameters.Add(sqlpYHD_TIMESTAMP)


                Dim sqlpVHH_REFNO As New SqlParameter("@YHD_REFNO", SqlDbType.VarChar, 50)
                sqlpVHH_REFNO.Value = txtHOldrefno.Text
                cmd.Parameters.Add(sqlpVHH_REFNO) '@YHD_REVDATE

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlopYHD_NEWDOCNO As New SqlParameter("@YHD_NEWDOCNO", SqlDbType.VarChar, 20)
                cmd.Parameters.Add(sqlopYHD_NEWDOCNO)
                cmd.Parameters("@YHD_NEWDOCNO").Direction = ParameterDirection.Output
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Adding header info
                cmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String = ""
                If (iReturnvalue = 0) Then
                    ' stTrans.Commit()
                    If ViewState("datamode") <> "edit" Then
                        str_err = DoTransactions(objConn, stTrans, sqlopYHD_NEWDOCNO.Value)
                    Else
                        str_err = DoTransactions(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                    End If
                    If str_err = "0" Then
                        stTrans.Commit()
                        h_editorview.Value = ""

                        btnSave.Enabled = False
                        gvJournal.Enabled = True
                        ''lblError.Text = getErrorMessage("304")
                        txtDNarration.Text = ""
                        If ViewState("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, sqlopYHD_NEWDOCNO.Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            lblError.Text = getErrorMessage("304")
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            'datamode = Encr_decrData.Encrypt("view")
                            'Response.Redirect("acccrCashReceipt.aspx?viewid=" & Request.QueryString("viewid") & "&edited=yes" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamode)
                            lblError.Text = getErrorMessage("305")
                        End If
                        clear_All()
                    Else '.Split("__")(0).Trim & ""
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim)
                        stTrans.Rollback()
                    End If
                Else
                    lblError.Text = getErrorMessage(iReturnvalue & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub


    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        Try
            Dim iIndex As Integer = 0
            ' ''FIND OPPOSITE ACCOUNT
            Dim str_dr_opposite, str_cr_opposite As String
            str_dr_opposite = ""
            str_cr_opposite = ""
            Dim d_cr_largest As Double = 0
            Dim d_dr_largest As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                    If Session("dtJournal").Rows(iIndex)("Debit") > d_dr_largest Then
                        str_dr_opposite = Session("dtJournal").Rows(iIndex)("Accountid")
                        d_dr_largest = Session("dtJournal").Rows(iIndex)("Debit")
                    End If
                Else
                    If Session("dtJournal").Rows(iIndex)("Credit") > d_cr_largest Then
                        str_cr_opposite = Session("dtJournal").Rows(iIndex)("Accountid")
                        d_cr_largest = Session("dtJournal").Rows(iIndex)("Credit")
                    End If
                End If
            Next
            ''''
            'Adding transaction info
            Dim cmd As New SqlCommand

            Dim str_err As String = ""
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    cmd.Dispose()
                    cmd = New SqlCommand("SaveYJOURNAL_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure
                    '' ''Handle sub table
                    Dim str_crdb As String = "CR"
                    If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                        dTotal = Session("dtJournal").Rows(iIndex)("Debit")
                        str_crdb = "DR"
                    Else
                        dTotal = Session("dtJournal").Rows(iIndex)("Credit")
                    End If

                    ''''pending

                    str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtJournal").Rows(iIndex)("id"), _
                    str_crdb, iIndex + 1 - ViewState("iDeleteCount"), Session("dtJournal").Rows(iIndex)("Accountid"), dTotal)
                    If str_err <> "0" Then
                        Return str_err
                    End If




                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                    sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim sqlpYJL_SUB_ID As New SqlParameter("@YJL_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpYJL_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpYJL_SUB_ID)

                    Dim sqlpsqlpYJL_BSU_ID As New SqlParameter("@YJL_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpYJL_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpYJL_BSU_ID)

                    Dim sqlpYJL_FYEAR As New SqlParameter("@YJL_FYEAR", SqlDbType.Int)
                    sqlpYJL_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpYJL_FYEAR)

                    Dim sqlpYJL_DOCTYPE As New SqlParameter("@YJL_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpYJL_DOCTYPE.Value = "YJV"
                    cmd.Parameters.Add(sqlpYJL_DOCTYPE)

                    Dim sqlpYJL_DOCNO As New SqlParameter("@YJL_DOCNO", SqlDbType.VarChar, 20)
                    sqlpYJL_DOCNO.Value = p_docno & ""
                    cmd.Parameters.Add(sqlpYJL_DOCNO)

                    Dim sqlpYJL_DOCDT As New SqlParameter("@YJL_DOCDT", SqlDbType.DateTime, 30)
                    sqlpYJL_DOCDT.Value = txtHDocdate.Text & ""
                    cmd.Parameters.Add(sqlpYJL_DOCDT)

                    Dim sqlpYJL_NARRATION As New SqlParameter("@YJL_NARRATION", SqlDbType.VarChar, 300)
                    sqlpYJL_NARRATION.Value = Session("dtJournal").Rows(iIndex)("Narration") & ""
                    cmd.Parameters.Add(sqlpYJL_NARRATION)

                    Dim sqlpYJL_CUR_ID As New SqlParameter("@YJL_CUR_ID", SqlDbType.VarChar, 12)
                    sqlpYJL_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                    cmd.Parameters.Add(sqlpYJL_CUR_ID)

                    Dim sqlpYJL_EXGRATE1 As New SqlParameter("@YJL_EXGRATE1", SqlDbType.Decimal, 8)
                    sqlpYJL_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                    cmd.Parameters.Add(sqlpYJL_EXGRATE1)

                    Dim sqlpYJL_EXGRATE2 As New SqlParameter("@YJL_EXGRATE2", SqlDbType.Decimal, 8)
                    sqlpYJL_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                    cmd.Parameters.Add(sqlpYJL_EXGRATE2)

                    Dim sqlpYJL_ACT_ID As New SqlParameter("@YJL_ACT_ID", SqlDbType.VarChar, 20)
                    sqlpYJL_ACT_ID.Value = Session("dtJournal").Rows(iIndex)("Accountid") & ""
                    cmd.Parameters.Add(sqlpYJL_ACT_ID)

                    Dim sqlpYJL_CREDIT As New SqlParameter("@YJL_CREDIT", SqlDbType.Decimal)
                    sqlpYJL_CREDIT.Value = Session("dtJournal").Rows(iIndex)("Credit")
                    cmd.Parameters.Add(sqlpYJL_CREDIT)

                    Dim sqlpYJL_DEBIT As New SqlParameter("@YJL_DEBIT", SqlDbType.Decimal, 8)
                    sqlpYJL_DEBIT.Value = Session("dtJournal").Rows(iIndex)("Debit")
                    cmd.Parameters.Add(sqlpYJL_DEBIT)

                    Dim sqlpbYJL_BDELETED As New SqlParameter("@YJL_BDELETED", SqlDbType.Bit)
                    sqlpbYJL_BDELETED.Value = False
                    cmd.Parameters.Add(sqlpbYJL_BDELETED)

                    Dim sqlpbYJL_SLNO As New SqlParameter("@YJL_SLNO", SqlDbType.Int)
                    sqlpbYJL_SLNO.Value = iIndex + 1 - ViewState("iDeleteCount")
                    cmd.Parameters.Add(sqlpbYJL_SLNO)

                    Dim sqlpYJL_REFDOCTYPE As New SqlParameter("@YJL_REFDOCTYPE", SqlDbType.VarChar, 20)
                    sqlpYJL_REFDOCTYPE.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpYJL_REFDOCTYPE)

                    Dim sqlpbYJL_REFDOCNO As New SqlParameter("@YJL_REFDOCNO", SqlDbType.Bit)
                    sqlpbYJL_REFDOCNO.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpbYJL_REFDOCNO)

                    Dim sqlpYJL_OPP_ACT_ID As New SqlParameter("@YJL_OPP_ACT_ID", SqlDbType.VarChar, 10)
                    If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                        sqlpYJL_OPP_ACT_ID.Value = str_dr_opposite
                    Else
                        sqlpYJL_OPP_ACT_ID.Value = str_cr_opposite
                    End If
                    cmd.Parameters.Add(sqlpYJL_OPP_ACT_ID)

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    If ViewState("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                    Else
                        sqlpbEdit.Value = False
                    End If
                    cmd.Parameters.Add(sqlpbEdit)

                    Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                    If iIndex = Session("dtJournal").Rows.Count - 1 Then
                        sqlpbLastRec.Value = True
                    Else
                        sqlpbLastRec.Value = False
                    End If
                    cmd.Parameters.Add(sqlpbLastRec)

                    'Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    'sqlpbEdit.Value = False
                    'cmd.Parameters.Add(sqlpbEdit)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    'Dim sqlopJHD_NEWDOCNO As New SqlParameter("@JHD_NEWDOCNO", SqlDbType.VarChar, 20)
                    'cmd.Parameters.Add(sqlopJHD_NEWDOCNO)
                    'cmd.Parameters("@JHD_NEWDOCNO").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""
                    If iReturnvalue <> 0 Then

                        Exit For
                    Else

                    End If
                    cmd.Parameters.Clear()
                Else
                    If Not Session("dtJournal").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                        ViewState("iDeleteCount") = ViewState("iDeleteCount") + 1
                        cmd = New SqlCommand("DeleteYJOURNAL_D", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                        sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                        cmd.Parameters.Add(sqlpGUID)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        Dim success_msg As String = ""

                        If iReturnvalue <> 0 Then
                            lblError.Text = getErrorMessage(iReturnvalue)
                        End If
                        cmd.Parameters.Clear()
                    End If
                End If
            Next
            If iIndex <= Session("dtJournal").Rows.Count - 1 Then
                Return iReturnvalue
            Else
                Return iReturnvalue
            End If
            'Adding transaction info
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function


    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer

        Try
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim str_cur_cost_center As String = ""
            Dim str_prev_cost_center As String = ""
            'Dim dTotal As Double = 0
            Dim iIndex As Integer
            Dim iLineid As Integer

            Dim str_err As String = "0"
            Dim str_balanced As Boolean = True

            For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then

                    If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                        iLineid = -1
                        str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                    End If
                    If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                        iLineid = -1
                        str_balanced = check_others(p_voucherid, p_amount)
                    End If
                    iLineid = iLineid + 1
                    If str_balanced = False Then
                        lblError.Text = "Error not balanced"
                        iReturnvalue = 511
                        Exit For
                    End If

                    str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")

                    cmd.Dispose()

                    cmd = New SqlCommand("SaveVOUCHER_D_S", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure
                    '       @GUID = NULL,
                    '		@VDS_SUB_ID = N'007',
                    '		@VDS_BSU_ID = N'125016',
                    '		@VDS_FYEAR = 2007,
                    '		@VDS_DOCTYPE = N'CR',

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtCostChild").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim sqlpVDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
                    sqlpVDS_ID.Value = p_slno
                    cmd.Parameters.Add(sqlpVDS_ID)

                    Dim sqlpVDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVDS_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVDS_SUB_ID)

                    Dim sqlpVDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpVDS_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpVDS_BSU_ID)

                    Dim sqlpVDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
                    sqlpVDS_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpVDS_FYEAR)

                    Dim sqlpVDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
                    sqlpVDS_DOCTYPE.Value = "YJV"
                    cmd.Parameters.Add(sqlpVDS_DOCTYPE)

                    '		@VDS_DOCNO = N'CP000017',
                    '		@VDS_DOCDT = N'08/20/2007',
                    '		@VDS_ACT_ID = N'02101001',
                    '		@VDS_ID=N'111',
                    '		@VDS_SLNO = 1,
                    '		@VDS_AMOUNT = 299,
                    '		@VDS_CCS_ID = N'AST',
                    '		@VDS_CODE = N'TEST',
                    '		@VDS_DRCR = N'DR',
                    '		@VDS_bPOSTED = 0,
                    '		@VDS_bDELETED = 0,
                    '		@bEdit = 0

                    Dim sqlpVDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
                    sqlpVDS_DOCNO.Value = p_docno
                    cmd.Parameters.Add(sqlpVDS_DOCNO)

                    Dim sqlpVDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
                    sqlpVDS_DOCDT.Value = txtHDocdate.Text & ""
                    cmd.Parameters.Add(sqlpVDS_DOCDT)

                    Dim sqlpVDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
                    sqlpVDS_ACT_ID.Value = p_accountid
                    cmd.Parameters.Add(sqlpVDS_ACT_ID)

                    Dim sqlpbVDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
                    sqlpbVDS_SLNO.Value = p_slno
                    cmd.Parameters.Add(sqlpbVDS_SLNO)

                    Dim sqlpVDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
                    sqlpVDS_AMOUNT.Value = Session("dtCostChild").Rows(iIndex)("Amount")
                    cmd.Parameters.Add(sqlpVDS_AMOUNT)

                    Dim sqlpVDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
                    sqlpVDS_CCS_ID.Value = Session("dtCostChild").Rows(iIndex)("costcenter")
                    cmd.Parameters.Add(sqlpVDS_CCS_ID)

                    Dim sqlpVDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
                    sqlpVDS_CODE.Value = Session("dtCostChild").Rows(iIndex)("Memberid")
                    cmd.Parameters.Add(sqlpVDS_CODE)

                    Dim sqlpVDS_DESCR As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
                    If Session("dtCostChild").Rows(iIndex)("Memberid") Is System.DBNull.Value Then
                        sqlpVDS_DESCR.Value = Session("dtCostChild").Rows(iIndex)("Memberid")
                    Else
                        sqlpVDS_DESCR.Value = Session("dtCostChild").Rows(iIndex)("Name")
                    End If
                    cmd.Parameters.Add(sqlpVDS_DESCR)

                    Dim sqlpbVDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
                    sqlpbVDS_DRCR.Value = p_crdr
                    cmd.Parameters.Add(sqlpbVDS_DRCR)

                    Dim sqlpVDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
                    sqlpVDS_bPOSTED.Value = False
                    cmd.Parameters.Add(sqlpVDS_bPOSTED)

                    Dim sqlpbVDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
                    sqlpbVDS_BDELETED.Value = False
                    cmd.Parameters.Add(sqlpbVDS_BDELETED)

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    If ViewState("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                    Else
                        sqlpbEdit.Value = False
                    End If
                    cmd.Parameters.Add(sqlpbEdit)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""

                    If iReturnvalue <> 0 Then
                        Exit For
                    End If
                    cmd.Parameters.Clear()
                Else
                    If Not Session("dtCostChild").Rows(iIndex)("GUID") Is System.DBNull.Value And Session("dtCostChild").Rows(iIndex)("Status") & "" = "Deleted" Then
                        cmd = New SqlCommand("DeleteVOUCHER_D_S", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                        sqlpGUID.Value = Session("dtCostChild").Rows(iIndex)("GUID")
                        cmd.Parameters.Add(sqlpGUID)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        Dim success_msg As String = ""

                        If iReturnvalue <> 0 Then
                            lblError.Text = getErrorMessage(iReturnvalue)
                        End If
                        cmd.Parameters.Clear()
                    End If
                End If
            Next
            Return iReturnvalue
        Catch ex As Exception
            Errorlog(ex.Message, "child")
            Return "1000"
        End Try
    End Function


    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Try
            Dim dTotal As Double = 0
            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("costcenter") = p_costid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function


    Function check_others(ByVal p_voucherid As String, ByVal p_total As String) As Boolean

        Try
            Dim dTotal As Double = 0

            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try


    End Function


    Public Sub UserMsgBox1(ByVal sMsg As String)

        Dim sb As New StringBuilder()
        Dim oFormObject As System.Web.UI.Control

        sMsg = sMsg.Replace("'", "\'")
        sMsg = sMsg.Replace(Chr(34), "\" & Chr(34))
        sMsg = sMsg.Replace(vbCrLf, "\n")
        sMsg = "<script language=javascript>alert(""" & sMsg & """)</script>"

        sb = New StringBuilder()
        sb.Append(sMsg)

        For Each oFormObject In Me.Controls
            If TypeOf oFormObject Is HtmlForm Then
                Exit For
            End If
        Next

        ' Add the javascript after the form object so that the 
        ' message doesn't appear on a blank screen.
        'oFormObject.Controls.AddAt(oFormObject.Controls.Count, New LiteralControl(sb.ToString()))
    End Sub


    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM YJOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" _
            & " AND YHD_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then


                'txtHNarration.Text = ds.Tables(0).Rows(0)("YHD_NARRATION")
                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("YHD_DOCNO") & "|" _
                   & ds.Tables(0).Rows(0)("YHD_SUB_ID") & "|" _
                   & ds.Tables(0).Rows(0)("YHD_DOCDT") & "|" _
                   & ds.Tables(0).Rows(0)("YHD_FYEAR") & "|"
                    txtHDocno.Text = ds.Tables(0).Rows(0)("YHD_DOCNO") & ""
                    Dim cmd As New SqlCommand("LockYJOURNAL_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    'If ds.Tables(0).Rows(0)("bPosted") = True Then
                    '    Return 1000
                    'End If
                    Dim sqlpYHD_SUB_ID As New SqlParameter("@YHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpYHD_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpYHD_SUB_ID)

                    Dim sqlpsqlpYHD_BSU_ID As New SqlParameter("@YHD_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpYHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpYHD_BSU_ID)

                    Dim sqlpYHD_FYEAR As New SqlParameter("@YHD_FYEAR", SqlDbType.Int)
                    sqlpYHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpYHD_FYEAR)

                    Dim sqlpYHD_DOCTYPE As New SqlParameter("@YHD_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpYHD_DOCTYPE.Value = "YJV"
                    cmd.Parameters.Add(sqlpYHD_DOCTYPE)

                    Dim sqlpYHD_DOCNO As New SqlParameter("@YHD_DOCNO", SqlDbType.VarChar, 20)
                    sqlpYHD_DOCNO.Value = ViewState("str_editData").Split("|")(0)
                    cmd.Parameters.Add(sqlpYHD_DOCNO)

                    Dim sqlpYHD_DOCDT As New SqlParameter("@YHD_DOCDT", SqlDbType.DateTime, 30)
                    sqlpYHD_DOCDT.Value = ds.Tables(0).Rows(0)("YHD_DOCDT") & ""
                    cmd.Parameters.Add(sqlpYHD_DOCDT)

                    Dim sqlpYHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpYHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpYHD_CUR_ID)

                    Dim sqlpYHD_USER As New SqlParameter("@YHD_USER", SqlDbType.VarChar, 50)
                    sqlpYHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpYHD_USER)

                    Dim sqlopYHD_TIMESTAMP As New SqlParameter("@YHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopYHD_TIMESTAMP)
                    cmd.Parameters("@YHD_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                        Return iReturnvalue
                    End If
                    ViewState("str_timestamp") = sqlopYHD_TIMESTAMP.Value
                    Return iReturnvalue

                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
                ViewState("str_editData") = ""
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            'Return " | | "
        End Try
        Return True
    End Function


    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
            & " JHD_DOCNO='" & ViewState("str_editData").Split("|")(0) & "'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("JHD_DOCDT")
                txtHDocno.Text = ViewState("str_editData").Split("|")(0)
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = ds.Tables(0).Rows(0)("JHD_DOCTYPE") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopJHD_TIMESTAMP.Value = ViewState("str_timestamp")
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = (iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ' ''btnEditcancel.Visible = True


        ' ''tbl_Details.Visible = False
        ' ''Response.Redirect("accjvjournalvoucher.aspx?editid=" & Request.QueryString("viewid"))


        'btnCancel.Visible = True
        tbl_Details.Visible = False

        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
                'btnCancel.Visible = False
            End If

        Else
            'btnCancel.Visible = True
            h_editorview.Value = "Edit"
            ResetViewData()
            ViewState("datamode") = "edit"
            'setModifyHeader(Request.QueryString("editid"))
            'txtHDocno.Text =  viewstate("str_editData").Split("|")(0)
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            btnSave.Enabled = True
        End If

    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            h_editorview.Value = ""
            If ViewState("datamode") = "edit" Then
                unlock()
            End If
            setViewData()
            'clear_All()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

        Dim ds As New DataSet

        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        objConn.Open()
        str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
               & " GUID='" & Request.QueryString("viewid") & "'"
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then

                Dim cmd As New SqlCommand("DeleteJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("JHD_SUB_ID") & ""
                cmd.Parameters.Add(sqlpJHD_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@JHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "YJV"
                cmd.Parameters.Add(sqlpDOCTYPE)


                'Dim sqlpJHD_DOCDT As New SqlParameter("@JHD_DOCDT", SqlDbType.DateTime, 30)
                'sqlpJHD_DOCDT.Value = txtHDocdate.Text & ""
                'cmd.Parameters.Add(sqlpJHD_DOCDT)


                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value

                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                Else
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    stTrans.Commit()
                    lblError.Text = getErrorMessage("516")
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    clear_All()
                    Response.Redirect("accjvViewJournalVoucher.aspx?deleted=done" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
                End If
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try


    End Sub


    Private Function set_viewdata() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM YJOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" _
            & " AND YHD_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("YHD_DOCDT")
                txtHDocno.Text = ds.Tables(0).Rows(0)("YHD_DOCNO")
                txtHNarration.Text = ds.Tables(0).Rows(0)("YHD_NARRATION")

                ViewState("str_editData") = ds.Tables(0).Rows(0)("YHD_DOCNO") & "|" _
               & ds.Tables(0).Rows(0)("YHD_SUB_ID") & "|" _
               & ds.Tables(0).Rows(0)("YHD_DOCDT") & "|" _
               & ds.Tables(0).Rows(0)("YHD_FYEAR") & "|"
                Return ""
            Else
                ViewState("str_editData") = ""
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("accyjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("accyjvJournalvoucher.spx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
        End Try
        Return True
    End Function


    Protected Sub lbAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub


    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If ViewState("datamode") = "edit" Then
            unlock()
        End If
        ResetViewData()
        clear_All()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub



    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim strFilter As String = String.Empty
        'SELECT * FROM vw_OSA_JOURNAL where FYEAR, DOCTYPE, DOCNO, DOCDT
        strFilter = "BSU_ID = '" & Session("SBSUID") & "' and FYEAR = " & Session("F_YEAR") & " and DOCTYPE = 'YJV' and SHD_DOCNO = '" & txtHDocno.Text & "' and SHD_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(txtHDocdate.Text)) & "'"
        str_Sql = "SELECT * FROM vw_OSA_YJOURNAL where " + strFilter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.Text

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("Summary") = chkSummary.Checked
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(Session("SUB_ID"), "YJV", txtHDocno.Text)
            params("VoucherName") = "YEAR END ADJUSTMENT VOUCHER"
            params("reportHeading") = "YEAR END ADJUSTMENT VOUCHER"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.VoucherName = "Journal Voucher"
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../RPT_Files/rptYJVoucherReport.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True) 
        End If
    End Sub


    Protected Sub imgCaledar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub txtDAccountCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDAccountCode.TextChanged
        txtDAccountName.Text = AccountFunctions.Validate_Account(txtDAccountCode.Text, Session("sbsuid"), "NOTCC")
        If txtDAccountName.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            txtDAccountCode.Focus()
        Else
            lblError.Text = ""
            txtDAmount.Focus()
        End If
    End Sub
End Class
