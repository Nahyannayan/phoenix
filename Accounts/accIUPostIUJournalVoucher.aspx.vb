Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj

Partial Class iuPostJournalVoucher
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim menu_rights As Integer
    Dim datamode As String = "none"
    Dim Encr_decrData As New Encryption64
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If Page.IsPostBack = False Then
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            Else
                lblError.Text = ""
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                datamode = ""
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or MainMnu_code <> "A200014" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Response.CacheControl = "no-cache"
                'calling pageright class to get the access rights
                menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, datamode)
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                gvChild.Attributes.Add("bordercolor", "#1b80b6")
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
            End If
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
   
    Public Function returnCrDb(ByVal p_CrDb As String) As String
        If p_CrDb = "CR" Then
            Return "Credit"
        Else
            Return "Debit"
        End If
    End Function

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGUID As New Label
            lblGUID = TryCast(e.Row.FindControl("lblGUID"), Label)
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            Dim chkPost As New HtmlInputCheckBox
            chkPost = e.Row.FindControl("chkPosted")
            Dim hlCEdit As New HyperLink
            Dim hlview As New HyperLink
            datamode = "view"
            datamode = Encr_decrData.Encrypt(datamode)
            Dim MainMnu_codeEncr As String = Encr_decrData.Encrypt("A150025")
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If chkPost IsNot Nothing Then
                If chkPost.Checked = True Then
                    chkPost.Disabled = True
                Else
                    If lblGUID IsNot Nothing Then
                        ' Dim i As New Encryption64
                        hlview.NavigateUrl = String.Format("accIUJournalvoucher.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_codeEncr, datamode, lblGUID.Text)
                        'hlview.NavigateUrl = "journalvoucher.aspx?viewid=" & lblGUID.Text
                        hlCEdit.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub Find_Checked(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True And chk.Disabled = False Then
                    post_voucher(chk.Value.ToString)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    Find_Checked(ctrl)
                End If
            End If
        Next
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        h_FirstVoucher.Value = ""
        Find_Checked(Me.Page)
        If h_FirstVoucher.Value <> "" And chkPrint.Checked Then
            PrintVoucher(h_FirstVoucher.Value)
        End If
        gridbind()
    End Sub

    Function post_voucher(ByVal p_guid As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                ''get header info
                Dim str_Sql As String
                str_Sql = "select * FROM IJOURNAL_H where GUID='" & p_guid & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    Dim cmd As New SqlCommand("POSTIJOURNAL", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpIJH_BSU_ID As New SqlParameter("@IJH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpIJH_BSU_ID.Value = Session("sBSUId")
                    cmd.Parameters.Add(sqlpIJH_BSU_ID)

                    Dim sqlpIJH_FYEAR As New SqlParameter("@IJH_FYEAR", SqlDbType.Int)
                    sqlpIJH_FYEAR.Value = ds.Tables(0).Rows(0)("IJH_FYEAR") & ""
                    cmd.Parameters.Add(sqlpIJH_FYEAR)

                    Dim sqlpIJH_DOCTYPE As New SqlParameter("@IJH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpIJH_DOCTYPE.Value = ds.Tables(0).Rows(0)("IJH_DOCTYPE") & ""
                    cmd.Parameters.Add(sqlpIJH_DOCTYPE)

                    Dim sqlpIJH_DOCNO As New SqlParameter("@IJH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpIJH_DOCNO.Value = ds.Tables(0).Rows(0)("IJH_DOCNO") & ""
                    cmd.Parameters.Add(sqlpIJH_DOCNO)

                    Dim sqlpbCR As New SqlParameter("@bCR", SqlDbType.Bit, 2)
                    Dim strDRBSUID As String = ds.Tables(0).Rows(0)("IJH_DR_BSU_ID")
                    If String.Compare(strDRBSUID, Session("sBSUId"), True) = 0 Then
                        sqlpbCR.Value = False
                    Else
                        sqlpbCR.Value = True
                    End If
                    cmd.Parameters.Add(sqlpbCR)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    'Adding header info
                    cmd.Parameters.Clear()
                    'Adding transaction info
                    ' Dim str_err As String
                    If (iReturnvalue = 0) Then
                        If h_FirstVoucher.Value = "" Then
                            h_FirstVoucher.Value = ds.Tables(0).Rows(0)("IJH_DOCNO") & ""
                        End If
                        stTrans.Commit()
                        'stTrans.Rollback()
                        lblError.Text = "IJV Successfully Posted"
                    Else
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                    If (iReturnvalue = 0) Then
                        lblError.Text = "Successfully Posted"
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ds.Tables(0).Rows(0)("IJH_DOCNO"), "Posting", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                    Else
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                End If
            Catch ex As Exception
                lblError.Text = getErrorMessage("1000")
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Function

    Private Sub gridbind()
        Try
            Dim lstrDocNo, lstrDocDate, lstrPartyAC, lstrBankAC, lstrNarration, lstrCurrency, lstrAmount As String
            Dim lstrFiltDocNo, lstrFiltDocDate, lstrFiltBankAC, lstrFiltNarration, lstrFiltCurrency, lstrOpr As String
            Dim larrSearchOpr() As String

            Dim txtSearch As New TextBox
            lstrFiltDocNo = ""
            lstrFiltDocDate = ""
            lstrFiltBankAC = ""
            lstrFiltNarration = ""
            lstrFiltCurrency = ""
            lstrDocNo = ""
            lstrDocDate = ""
            lstrPartyAC = ""
            lstrBankAC = ""
            lstrNarration = ""
            lstrCurrency = ""

            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                lstrDocNo = ""
                lstrDocDate = ""
                lstrPartyAC = ""
                lstrBankAC = ""
                lstrNarration = ""
                lstrCurrency = ""
                lstrAmount = ""

                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   DocNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                If txtSearch IsNot Nothing Then
                    lstrDocNo = Trim(txtSearch.Text)
                    If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "IJH_DOCNO", lstrDocNo)
                End If
                '   -- 2  DocDate
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
                If txtSearch IsNot Nothing Then
                    lstrDocDate = txtSearch.Text
                    If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "IJH_DOCDT", lstrDocDate)
                End If
                '   -- 3  Bank AC
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtBankAC")
                If txtSearch IsNot Nothing Then
                    lstrBankAC = txtSearch.Text
                    If (lstrBankAC <> "") Then lstrFiltBankAC = SetCondn(lstrOpr, "BUM.BSU_SHORTNAME", lstrBankAC)
                End If

                '   -- 5  Narration
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtNarrn")
                If txtSearch IsNot Nothing Then
                    lstrNarration = txtSearch.Text
                    If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "IJH_NARRATION", lstrNarration)
                End If

                '   -- 7  Currency
                larrSearchOpr = h_Selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
                If txtSearch IsNot Nothing Then
                    lstrCurrency = txtSearch.Text
                    If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "BUC.BSU_SHORTNAME", lstrCurrency)
                End If
            End If
            Dim str_Filter As String = lstrFiltDocNo & lstrFiltDocDate & lstrFiltBankAC & lstrFiltNarration & lstrFiltCurrency

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT IJH.*, case when IJH.IJH_CR_BSU_ID = '" & Session("sBsuid") & "' " _
            & " and IJH.IJH_bPOSTEDCRBSU = 1 then 1  else" _
            & " case when  IJH.IJH_DR_BSU_ID = '" & Session("sBsuid") & "' " _
            & " and IJH.IJH_bPOSTEDDRBSU = 1  then 1 else 0 end   end POSTED ," _
            & " SUM(IJD.IJL_CREDIT) AS CREDITTOTAL, SUM(IJD.IJL_DEBIT) AS DEBITTOTAL," _
            & " BUM.BSU_SHORTNAME AS DR_BSU_NAME, BUC.BSU_SHORTNAME AS CR_BSU_NAME FROM IJOURNAL_H AS IJH INNER JOIN" _
            & " IJOURNAL_D AS IJD ON IJH.IJH_FYEAR = IJD.IJL_FYEAR " _
            & " AND IJH.IJH_DOCTYPE = IJD.IJL_DOCTYPE AND IJH.IJH_DOCNO = IJD.IJL_DOCNO " _
            & " LEFT OUTER JOIN vw_OSO_BUSINESSUNIT_M AS BUM " _
            & " ON IJH.IJH_DR_BSU_ID = BUM.BSU_ID LEFT OUTER JOIN vw_OSO_BUSINESSUNIT_M AS BUC ON IJH.IJH_CR_BSU_ID = BUC.BSU_ID " _
            & " WHERE IJH.IJH_FYEAR = " & Session("F_YEAR") & str_Filter _
            & " AND ((IJH.IJH_bDELETED = 0 AND IJH.IJH_CR_BSU_ID = '" & Session("sBsuid") & "' and" _
            & " IJH.IJH_bPOSTEDCRBSU = 0  ) OR (IJH.IJH_bDELETED = 0 AND IJH.IJH_DR_BSU_ID = '" _
            & Session("sBsuid") & "' AND IJH.IJH_FYEAR = " & Session("F_YEAR") _
            & " AND IJH.IJH_bPOSTEDDRBSU = 0 )) " _
            & " AND (IJH_ISS_BSU_ID='" & Session("sBsuid") & "' OR IJH.IJH_bPOSTEDDRBSU = 1 OR IJH.IJH_bPOSTEDCRBSU=1)" _
            & " GROUP BY IJH.GUID, IJH.IJH_CR_BSU_ID, " _
            & " IJH.IJH_DR_BSU_ID, IJH.IJH_FYEAR, IJH.IJH_DOCTYPE, IJH.IJH_DOCDT, " _
            & " IJH.IJH_CUR_ID, IJH.IJH_EXGRATE1, IJH.IJH_EXGRATE2, IJH.IJH_DREXGRATE, " _
            & " IJH.IJH_bPOSTEDCRBSU, IJH.IJH_bPOSTEDDRBSU, IJH.IJH_bDELETED, IJH.IJH_SESSIONID, " _
            & " IJH.IJH_bMIRROR, IJH.IJH_DIFFAMT, IJH.IJH_LOCK, IJH.IJH_TIMESTAMP, " _
            & " IJH.IJH_NARRATION, BUM.BSU_SHORTNAME, BUC.BSU_SHORTNAME, IJH.IJH_DOCNO, IJH.IJH_ISS_BSU_ID ORDER BY " _
            & " IJH.IJH_DOCDT DESC, IJH.IJH_DOCNO DESC"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            gvJournal.DataBind()
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add()
                'ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                If txtSearch IsNot Nothing Then
                    txtSearch.Text = lstrDocNo
                End If
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocDate")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrDocDate

                txtSearch = gvJournal.HeaderRow.FindControl("txtBankAC")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrBankAC


                txtSearch = gvJournal.HeaderRow.FindControl("txtNarrn")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrNarration

                txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrCurrency
                set_Menu_Img()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                '        Dim lblGrpCode As New Label
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                str_Sql = "select * FROM IJOURNAL_H where GUID='" & lblGUID.Text & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    'str_Sql = "SELECT * FROM IJOURNAL_D WHERE IJL_DOCNO='" & ds.Tables(0).Rows(0)("IJH_DOCNO") & "'" _
                    '& " AND IJL_DOCTYPE='" & ds.Tables(0).Rows(0)("IJH_DOCTYPE") & "' AND IJL_FYEAR = " & ds.Tables(0).Rows(0)("IJH_FYEAR")
                    str_Sql = "SELECT IJL.GUID,IJL.IJL_ID,IJL.IJL_DOCNO,IJL.IJL_NARRATION,IJL.IJL_ACT_ID,IJL.IJL_DEBIT,IJL.IJL_CREDIT,IJL.IJL_SLNO,ACT.ACT_NAME " _
                                & " FROM IJOURNAL_D IJL INNER JOIN ACCOUNTS_M ACT ON IJL.IJL_ACT_ID=ACT.ACT_ID " _
                                & " WHERE IJL.IJL_DOCNO='" & ds.Tables(0).Rows(0)("IJH_DOCNO") & "' AND IJL.IJL_DOCTYPE='" & ds.Tables(0).Rows(0)("IJH_DOCTYPE") & "' " _
                                & " AND IJL.IJL_FYEAR=" & ds.Tables(0).Rows(0)("IJH_FYEAR") & ""
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc
                    gvDetails.DataBind()
                Else
                End If
                gvChild.Visible = False
            Catch ex As Exception
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbViewChild_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True

            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label 
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)
                lblSlno = TryCast(sender.FindControl("lblSlno"), Label)

                str_Sql = "select * FROM IJOURNAL_D where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then 
                    str_Sql = "SELECT VOUCHER_D_S.VDS_DOCTYPE,VOUCHER_D_S.VDS_DOCNO," _
                    & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, A.NAME, " _
                    & " case isnull(VOUCHER_D_S.VDS_CODE,'') " _
                    & " when '' then 'GENERAL'" _
                    & " else  COSTCENTER_S.CCS_DESCR end as GRPFIELD," _
                    & " COSTCENTER_S.CCS_DESCR ,VOUCHER_D_S.VDS_SLNO, VOUCHER_D_S.VDS_CODE," _
                    & " VOUCHER_D_S.VDS_AMOUNT, COSTCENTER_S.CCS_QUERY" _
                    & " FROM IJOURNAL_D INNER JOIN ACCOUNTS_M ON " _
                    & " ACCOUNTS_M.ACT_ID = IJOURNAL_D.IJL_ACT_ID " _
                    & " LEFT OUTER JOIN VOUCHER_D_S ON" _
                    & " IJOURNAL_D.IJL_FYEAR =VOUCHER_D_S.VDS_FYEAR AND" _
                    & " IJOURNAL_D.IJL_DOCTYPE=VOUCHER_D_S.VDS_DOCTYPE AND " _
                    & " IJOURNAL_D.IJL_DOCNO=VOUCHER_D_S.VDS_DOCNO  AND " _
                    & " IJOURNAL_D.IJL_SLNO=VOUCHER_D_S.VDS_SLNO " _
                    & " LEFT OUTER JOIN  COSTCENTER_S " _
                    & " ON  VOUCHER_D_S.VDS_CCS_ID=COSTCENTER_S.CCS_ID " _
                    & "" _
                    & "LEFT JOIN fn_ReturnCostCenterData() A ON COSTCENTER_S.CCS_ID=A.CCS_ID" _
                    & " AND VDS_CODE=A.ID" _
                    & " WHERE  VOUCHER_D_S.VDS_DOCTYPE='IJV' AND " _
                    & " VOUCHER_D_S.VDS_DOCNO='" & ds.Tables(0).Rows(0)("IJL_DOCNO") & "' AND" _
                    & " VOUCHER_D_S.VDS_SUB_ID='" & Session("Sub_ID") & "' AND" _
                    & " VOUCHER_D_S.VDS_BSU_ID='" & Session("sBsuid") & "' AND" _
                    & " VOUCHER_D_S.VDS_FYEAR='" & Session("F_YEAR") & "'" _
                    & " AND VOUCHER_D_S.VDS_SLNO='" & lblSlno.Text & "'" _
                    & " ORDER BY GRPFIELD"

                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    helper.RegisterSummary("VDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    gvChild.DataBind()
                    'helper.ApplyGroupSort()
                Else
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Sub PrintVoucher(ByVal p_Docno As String)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.InterUnitVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "IJV", p_Docno, True, Session("HideCC"))
        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Protected Sub h_print_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
End Class
