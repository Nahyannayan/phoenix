<%@ Page Language="VB" MaintainScrollPositionOnPostback="True" AutoEventWireup="true"
    MasterPageFile="~/mainMasterPage.master" CodeFile="AccAddPDC.aspx.vb" Inherits="AccAddPDC"
    Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td input[type=text], table td select {
            min-width: 20% !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function getAccountList() {

            var NameandCode;
            var result;
            result = radopen("../common/PopupForm.aspx?id=PDC&multiselect=false", "pop_up");
            <%--  if (result == '' || result == undefined) {
                return false;
            }
            lstrVal = result.split('__');
            document.getElementById('<%=hfAccountList.ClientID %>').value = lstrVal[0];
            return true;--%>
        }


        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfAccountList.ClientID %>').value = NameandCode[0];
            }
        }


        function get_Paidto() {

            popUp('960', '600', 'NOTCC', '<%=txtPaidto.ClientId %>', '<%=txtPaidDescr.ClientId %>', '<%=hPly.ClientId %>', '<%=hCostReqd.ClientId %>');
            //debugger;


        }
        function get_Bank() {
            popUp('460', '400', 'BANK', '<%=Detail_ACT_ID.ClientId %>', '<%=txtBankDescr.ClientId %>')


            try {
                document.getElementById('<%=txtChqBook.ClientID %>').value = '';
            }
            catch (ex) {
                document.getElementById('<%=txtNewChqBook.ClientID %>').value = '';
                document.getElementById('<%=txtNewChqNo.ClientID %>').value = '';
            }
        }
        function get_Party() {
            popUp('960', '600', 'NOTCC', '<%=txtNewPartycode.ClientId %>', '<%=txtNewPartyname.ClientId %>');
            document.getElementById('<%=txtReceivedBy.ClientID %>').value = document.getElementById('<%=txtNewPartyname.ClientID %>').value;
        }
        function get_Interest() {
            popUp('460', '400', 'INTRAC', '<%=txtIntr.ClientId %>', '<%=txtIntrDescr.ClientId %>')
        }
        function get_AcrdInterest() {
            popUp('460', '400', 'ACRDAC', '<%=txtAcrd.ClientId %>', '<%=txtAcrdDescr.ClientId %>');
        }
        function get_Prepaid() {
            popUp('460', '400', 'PREPDAC', '<%=txtPrepd.ClientId %>', '<%=txtPrepdDescr.ClientId %>', '<%=txtChqiss.ClientId %>', '<%=txtChqissDescr.ClientId %>');
        }
        function get_Chqiss() {
            popUp('460', '400', 'CHQISSAC_PDC', '<%=txtChqiss.ClientId %>', '<%=txtChqissDescr.ClientId %>', '<%=txtPrepd.ClientId %>', '<%=txtPrepdDescr.ClientId %>')
        }

        function Settle_Online(id) {

            var NameandCode;
            var result;
            var pId = 1; //h_NextLine alert(sFeatures)       
            if (pId == 1) {
                url = "ShowOnlineSettlement.aspx?actid=" + document.getElementById('<%=txtPaidto.ClientID %>').value + "&lineid=" + id + "&docno=" + document.getElementById('<%=txtdocNo.ClientID %>').value + "&dt=" + document.getElementById('<%=txtdocDate.ClientID %>').value;
            }
            result = radopen(url, "pop_up1");
            //if (result == '' || result == undefined) {
            //    return false;
            //}

            //return false;
        }


        function getRoleID(mode) {

            var NameandCode;
            var result;
            var url;
            url = 'accShowEmpPP.aspx?id=' + mode;
            if (mode == 'CU') {
                result = radopen(url, "pop_up2");

               <%-- if (result == '' || result == undefined)
                { return false; }
                //            ClearDocno();
                NameandCode = result.split('___');
                document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[4];
                document.getElementById("<%=txtExpCode.ClientID %>").value = NameandCode[5];--%>

            }

        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[4];
                document.getElementById("<%=txtExpCode.ClientID %>").value = NameandCode[5];
            }
        }

        function HideAll() {

            document.getElementById('<%= pnlCostUnit.ClientID %>').style.display = 'none';
            return false;
        }


        function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {
            var lstrVal;
            var lintScrVal;
            var NameandCode;
            var result;
            //debugger;
            document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
            document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
            document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
            document.getElementById("<%=hd_ctrl2.ClientID %>").value = ctrl2;
            document.getElementById("<%=hd_ctrl3.ClientID%>").value = ctrl3;

            if (pMode == 'BANK') {
                result = radopen("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up3");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'NOTCC') {

                if (ctrl2 == '' || ctrl2 == undefined) {

                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up3");
                } else {

                    ////alert("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value);
                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "pop_up3");
                }
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];
                ////document.getElementById(ctrl2).value=lstrVal[2];
                //// document.getElementById(ctrl3).value=lstrVal[3];

            }
            else if (pMode == 'INTRAC') {

                result = radopen("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up3");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'ACRDAC') {

                result = radopen("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up3");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];

            }
            else if (pMode == 'PREPDAC') {


                result = radopen("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up3");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];
                //if (ctrl2 != '')
                //    if (document.getElementById(ctrl2).value == '') {
                //        document.getElementById(ctrl2).value = lstrVal[2];
                //        document.getElementById(ctrl3).value = lstrVal[3];
                //    }
            }
            else if (pMode == 'CHQISSAC_PDC') {

                result = radopen("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up3");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];
                //if (document.getElementById(ctrl2).value == '') {
                //    document.getElementById(ctrl2).value = lstrVal[2];
                //    document.getElementById(ctrl3).value = lstrVal[3];
                //}
            }
            else if (pMode == 'ALLOCATE') {

                result = radopen("TestAlloc.aspx?Id=" + ctrl + "&pAmount=" + document.getElementById(ctrl1).value + "&pPly=" + document.getElementById(ctrl2).value + "", "pop_up3");

                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');

            }
            else if (pMode == 'PDCCHQBOOK') {
                if (document.getElementById(ctrl2).value == "") {
                    alert("Please Select The Bank");
                    return false;
                }
                else if (document.getElementById(ctrl3).value == "") {
                    alert("Please Enter The Installment Months");
                    return false;
                }
                result = radopen("ShowChqsPDC.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl2).value + "&MonthsReq=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "pop_up3");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');

                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];

                //document.getElementById(ctrl).focus();


            }


        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                var ctrl2 = document.getElementById("<%=hd_ctrl2.ClientID %>").value;
                var ctrl3 = document.getElementById("<%=hd_ctrl3.ClientID%>").value;

                if (pMode == 'BANK') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                }
                else if (pMode == 'NOTCC') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];

                    //if (document.getElementById('<%=txtNewPartycode.ClientID %>').value == '') {
                    document.getElementById('<%=txtNewPartycode.ClientID %>').value = document.getElementById('<%=txtPaidto.ClientID %>').value;
                    document.getElementById('<%=txtNewPartyname.ClientID %>').value = document.getElementById('<%=txtPaidDescr.ClientID %>').value;
                    //}
                }
                else if (pMode == 'PREPDAC') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                    //if (document.getElementById(ctrl2).value == '') {
                    document.getElementById(ctrl2).value = NameandCode[2];
                    document.getElementById(ctrl3).value = NameandCode[3];
                    //}
                }

                else if (pMode == 'CHQISSAC_PDC') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                    //if (document.getElementById(ctrl2).value == '') {
                    document.getElementById(ctrl2).value = NameandCode[2];
                    document.getElementById(ctrl3).value = NameandCode[3];
                    //}
                }
                else if (pMode == 'ACRDAC') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                }
                else if (pMode == 'INTRAC') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                }
                else if (pMode == 'ALLOCATE') {

                }
                else if (pMode == 'PDCCHQBOOK') {
                    document.getElementById(ctrl).value = NameandCode[1];
                    document.getElementById(ctrl1).value = NameandCode[0];

                    document.getElementById(ctrl).focus();
                }



        }
    }

    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;
        var height = body.scrollHeight;
        var width = body.scrollWidth;
        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;
        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    function expandcollapse(obj, row) {
        var div = document.getElementById(obj);
        var img = document.getElementById('img' + obj);

        if (div.style.display == "none") {
            div.style.display = "block";
            if (row == 'alt') {
                img.src = "../images/Misc/minus.gif";
            }
            else {
                img.src = "../images/Misc/minus.gif";
            }
            img.alt = "Close to view other Customers";
        }
        else {
            div.style.display = "none";
            if (row == 'alt') {
                img.src = "../images/Misc/plus.gif";
            }
            else {
                img.src = "../images/Misc/plus.gif";
            }
            img.alt = "Expand to show Orders";
        }
    }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Bank Payment(PDC)
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <asp:HiddenField ID="hd_ctrl2" runat="server" />
                <asp:HiddenField ID="hd_ctrl3" runat="server" />

                <table width="100%" align="center">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>

                <table width="100%" align="center" border="0">
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Doc No [Old Ref No]</span>
                        </td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocNo" runat="server" Width="45%"></asp:TextBox>[
                <asp:TextBox ID="txtOldDocNo" runat="server" Width="35%"></asp:TextBox>
                            ]
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Doc Date </span>
                        </td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocDate" runat="server" Width="80%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton1" TargetControlID="txtdocDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Currency </span>
                        </td>

                        <td width="30%" align="left">
                            <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="True" Width="20%">
                            </asp:DropDownList>

                            <asp:TextBox ID="txtExchRate" runat="server" Width="61%"></asp:TextBox>
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Group Rate </span>
                        </td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLocalRate" runat="server" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Narration <span class="text-danger">*</span> </span>
                        </td>

                        <td align="left">
                            <asp:TextBox ID="txtNarrn" runat="server" Style="left: -6px; top: 0px;" Width="82%"
                                TextMode="MultiLine"></asp:TextBox><br />
                            <asp:CheckBox ID="chkAdvance" runat="server" AutoPostBack="True" Text="Advance" />
                            <asp:CheckBox ID="ChkBearer" runat="server" Text="Bearer Cheque"></asp:CheckBox>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr class="title-bg">
                        <td colspan="4" align="left">A/C Details
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="5" width="100%" align="center" border="0">
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Bank A/C <span class="text-danger">*</span></span>
                        </td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="Detail_ACT_ID" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank(); return false;" />
                            <asp:TextBox ID="txtBankDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <asp:HiddenField ID="hPLY" runat="server" />
                    <asp:HiddenField ID="hCostReqd" runat="server" />
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Paid To <span class="text-danger">*</span></span>
                        </td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtPaidto" runat="server" Width="20%"></asp:TextBox>
                            <asp:ImageButton ID="imgPaidto" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Paidto(); return false;" />
                            <asp:TextBox ID="txtPaidDescr" runat="server" Width="60%"></asp:TextBox>
                            <br />
                            <ajaxToolkit:PopupControlExtender ID="pceCostUnit" runat="server" PopupControlID="pnlCostUnit"
                                Position="Bottom" TargetControlID="lnkCostUnit">
                            </ajaxToolkit:PopupControlExtender>
                            <asp:LinkButton ID="lnkCostUnit" runat="server">Specify CostUnit Details</asp:LinkButton>
                        </td>
                    </tr>
                    <tr runat="server" id="trPayTerm" visible="false">
                        <td align="left" colspan="4">
                            <asp:Label ID="lblPaymentTerm" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Interest A/C </span>
                            <asp:LinkButton ID="lbFillAccounts" runat="server" OnClientClick="getAccountList();">(Fill Accounts)</asp:LinkButton>
                        </td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtIntr" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgInterest" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Interest(); return false;" />
                            <asp:TextBox ID="txtIntrDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Acrd Interest A/C </span>
                        </td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtAcrd" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgAcrdint" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_AcrdInterest(); return false;" />
                            <asp:TextBox ID="txtAcrdDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Prepaid Exp A/C <span class="text-danger">*</span></span>
                        </td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtPrepd" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgPrepaid" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Prepaid(); return false;" />
                            <asp:TextBox ID="txtPrepdDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Chq Issues A/C <span class="text-danger">*</span></span>
                        </td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtChqiss" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgChqiss" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Chqiss(); return false;" />
                            <asp:TextBox ID="txtChqissDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Party A/C <span class="text-danger">*</span></span>
                        </td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtNewPartycode" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgParty" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Party(); return false;" />
                            <asp:TextBox ID="txtNewPartyname" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Received By </span>
                        </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtReceivedBy" runat="server" Width="82%"></asp:TextBox>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr class="title-bg">
                        <td colspan="4" align="left">Installment Details
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center" border="0">
                    <tr>
                        <td width="10%" align="left">
                            <span class="field-label">Total Amount <span class="text-danger">*</span></span>
                        </td>

                        <td align="left" width="20%">
                            <asp:TextBox ID="txtAmount" runat="server" Width="82%"></asp:TextBox></td>
                        <td align="left" width="10%">
                            <span class="field-label">Interest Applicable  </span></td>
                        <td align="left" width="20%">
                            <asp:RadioButton ID="optControlAccYes" runat="server" GroupName="optControl" Text="Yes"
                                Checked="True" AutoPostBack="True" />
                            <asp:RadioButton ID="optControlAccNo" runat="server" GroupName="optControl" Text="No"
                                AutoPostBack="True" />
                        </td>
                        <td align="left" width="10%">
                            <span class="field-label">Interest Rate <span class="text-danger">*</span></span></td>
                        <td>
                            <asp:TextBox ID="txtIntRate" runat="server" Width="20%"></asp:TextBox>%
                    <asp:DropDownList ID="cmbCalcu" runat="server" AutoPostBack="True" Width="52%">
                        <asp:ListItem Value="F">Flat</asp:ListItem>
                        <asp:ListItem Value="R">Reducing</asp:ListItem>
                    </asp:DropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td width="10%" align="left">
                            <span class="field-label">No. of Installments <span class="text-danger">*</span></span>
                        </td>

                        <td align="left" width="20%">
                            <asp:TextBox ID="txtMnths" runat="server" Width="80%"></asp:TextBox></td>
                        <td align="left" width="10%">
                            <span class="field-label">Months b/n Cheques <span class="text-danger">*</span> </span></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtMIntr" runat="server" Width="80%"></asp:TextBox>
                        </td>
                        <td align="left" width="10%">
                            <span class="field-label">Interest Starts After  <span class="text-danger">*</span></span></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtMonthInterestStart" runat="server" Width="80%"></asp:TextBox>
                            Months
                        </td>
                    </tr>
                    <asp:HiddenField ID="hCheqBook" runat="server" />
                    <tr>
                        <td align="left" width="10%">
                            <span class="field-label">Upload Data </span>
                        </td>

                        <td align="left" colspan="2">
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                        </td>
                        <td align="left">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="Import Excel" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="left">
                            <asp:RadioButton ID="rbHigherRO" runat="server" Checked="True" GroupName="ro" Text="Round Cheque Installment to Higher Amount" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rbLowerRO" runat="server" GroupName="ro" CssClass="field-label"
                                Text="Round cheque Installment to Lower Amount" AutoPostBack="True" />
                            <asp:RadioButton ID="rbEqual" runat="server" GroupName="ro" Text="Equal Cheque Installments" CssClass="field-label"
                                AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr id="tr_Add" runat="server">
                        <td width="20%" align="left" valign="top">
                            <span class="field-label">Cheque Book Lot <span class="text-danger">*</span></span>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtChqBook" runat="server" Width="60%"></asp:TextBox>
                            <a href="#" onclick="popUp('460','400','PDCCHQBOOK','<%=txtChqBook.ClientId %>','<%=hCheqBook.ClientId %>','<%=Detail_ACT_ID.ClientId %>','<%=txtMnths.ClientId %>','<%=txtdocNo.ClientId %>'); return false;">
                                <img border="0" src="../Images/cal.gif" id="IMG1" language="javascript" /></a>
                            <asp:TextBox ID="txtChqNo" runat="server" Visible="False" Width="20%"></asp:TextBox>
                        </td>
                        <td align="left" valign="top" width="10%">
                            <span class="field-label">Cheque Date <span class="text-danger">*</span></span></td>
                        <td>
                            <asp:TextBox ID="txtChqDate" runat="server" Width="80%"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton2" TargetControlID="txtChqDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        <td>
                            <asp:Button ID="btnFill" runat="server" Text="ADD" CssClass="button" />
                            <asp:Button ID="btnFillCancel" runat="server" Text="Cancel" OnClick="btnFillCancel_Click"
                                CssClass="button" />
                        </td>
                    </tr>
                    <tr id="tr_Update" runat="server">
                        <td width="20%" align="left">
                            <span class="field-label">Cheque Book Lot </span>
                        </td>

                        <td align="left" width="20%">
                            <asp:TextBox ID="txtNewChqBook" runat="server" Width="40%"></asp:TextBox>
                            <a href="#" onclick="popUp('460','400','CHQBOOK_PDC','<%=txtNewChqBook.ClientId %>','<%=hCheqBook.ClientId %>','<%=txtNewChqNo.ClientId %>','<%=Detail_ACT_ID.ClientId %>','<%=txtdocNo.ClientId %>'); return false;">
                                <img border="0" src="../Images/cal.gif" id="IMG10" language="javascript" /></a>
                            <asp:TextBox ID="txtNewChqNo" runat="server" Width="20%"></asp:TextBox>
                        </td>
                        <td align="left" colspan="4">

                            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" />
                            <asp:Button ID="btnUpdateCancel" runat="server" Text="Cancel" OnClick="btnUpdateCancel_Click"
                                CssClass="button" />
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center" border="0">
                    <tr>
                        <td align="center">
                            <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                EmptyDataText="No Transaction details added yet." Width="100%" CssClass="table table-bordered table-row">
                                <EmptyDataRowStyle CssClass="gridheader" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide Orders for Customer <%# Eval("id") %>"
                                                    width="9px" border="0" src="../Images/Misc/plus.gif" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="id">
                                        <ItemStyle Width="5%" HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:0.000}" DataField="PayAmount"
                                        HeaderText="Payment Amount">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:0.000}" DataField="AmtWOInterest"
                                        HeaderText="Principal Amount">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:0.000}" DataField="Interest"
                                        HeaderText="Interest">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:0.000}" DataField="BalDue"
                                        HeaderText="Balance Due">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField Visible="False" HeaderText="ChqBookId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqBookId" runat="server" Text='<%# Bind("ChqBookId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ChqBookLot" HeaderText="Cheque Book">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ChqNo" HeaderText="Cheque No">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ChqDate" HeaderText="Cheque Date">
                                        <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField Visible="False" HeaderText="GUID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="DELETED">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDeleted" runat="server" Text='<%# Bind("DELETED") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Ply">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPLY" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="CostReqd">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostReqd" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                OnClick="lbEdit_Click" Text="Edit"> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Settle" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" OnClientClick='<%# "javascript:Settle_Online("&Container.DataItem("id")&"); return false;" %>'
                                                Text="Settle"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="100%" align="right">
                                                    <div id="div<%# Eval("id") %>" style="display: none; position: relative; left: 15px; overflow: auto; width: 100%">
                                                        <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                            CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIdCostchild" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                                                <asp:BoundField DataField="CostCenterTypeName" HeaderText="Costcenter" />
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                <asp:BoundField DataField="Amount" DataFormatString="{0:0.000}" HeaderText="Allocated"
                                                                    HtmlEncode="False" InsertVisible="False" SortExpression="Amount" Visible="false">
                                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Earn Code" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                <asp:TemplateField HeaderText="Amount">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtAmt" runat="server" Visible="false" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                                            onblur="CheckAmount(this)"></asp:TextBox>
                                                                        <asp:Label ID="txtAmt0" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td colspan="100%" align="right">
                                                                                <asp:GridView ID="gvCostAllocation" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                                    Width="100%" CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblIdCostAllocation" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="ASM_ID" HeaderText="Cost Allocation" />
                                                                                        <asp:BoundField DataField="ASM_NAME" HeaderText="Name" />
                                                                                        <asp:TemplateField HeaderText="Amount">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="txtAmt1" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>' Width="104px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table width="100%" align="center" border="0">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Monthly Installment </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtMInst" runat="server" ReadOnly="true"></asp:TextBox>
                                        <a href="#" onclick="popUp('800','600','ALLOCATE','1','<%=txtMInst.ClientId %>','<%=hPly.ClientId %>'); return false;">
                                            <img border="0" src="../Images/cal.gif" id="IMG8" language="javascript" /></a>
                                    </td>


                                    <td align="left"><span class="field-label">Total Interest(As Per Bank) </span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGridInterest" runat="server" Width="40%"></asp:TextBox>
                                        <span class="field-label">Adjustment </span>
                                        <asp:TextBox ID="txtAdj" runat="server" Width="30%"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Total Amount </span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBankTotal" runat="server"></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CausesValidation="False" CssClass="button" />
                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CausesValidation="False" CssClass="button" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" Text="Delete" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button" />
                            <asp:Button ID="btnSettle" runat="server" Text="Settle" CssClass="button" />
                            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button" />
                            <asp:CheckBox ID="chkPrintChq" runat="server" CssClass="field-label" Text="Print Cheque" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfAccountList" runat="server" />
                <asp:HiddenField ID="hfId1" runat="server"></asp:HiddenField>
                <asp:Panel ID="pnlCostUnit" runat="server" Style="display: none">
                    <table align="center" width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="4"><span class="field-label">CostUnitDetails </span>
                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideAll();" Style="text-align: right" TabIndex="54"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="30%" rowspan="1">
                                <span class="field-label"><span class="field-label">Period From </span></span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFrom" runat="server" TabIndex="15" AutoPostBack="True" Width="80%"></asp:TextBox>
                                <asp:ImageButton
                                    ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20"></asp:ImageButton>
                            </td>
                            <td align="left" width="30%">
                                <span class="field-label"><span class="field-label">To </span></span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtTo" runat="server" TabIndex="15" AutoPostBack="True" Width="80%"></asp:TextBox>
                                <asp:ImageButton
                                    ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%" rowspan="1">
                                <span class="field-label"><span class="field-label">CostUnit </span></span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtCostUnit" runat="server">
                                </asp:TextBox><asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/cal.gif"
                                    OnClientClick="getRoleID('CU');return false;"></asp:ImageButton>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label"><span class="field-label">Expense Account </span></span>
                            </td>
                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtExpCode" runat="server">
                                </asp:TextBox>
                                <asp:TextBox ID="txtPreAcc" runat="server">
                                </asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender1" runat="server" BehaviorID="100-calendarButtonExtender1"
                                    CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender2" runat="server" BehaviorID="100-calendarButtonExtender2"
                                    CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
