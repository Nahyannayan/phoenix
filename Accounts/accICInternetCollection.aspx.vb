Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accICInternetCollection
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            '''''check menu rights
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If
            Session("CHECKLAST") = 0
            'txtHNarration.Attributes.Add("onblur", "javascript:")
            txtHNarration.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtHNarration.ClientID & "');")
            txtDNarration.Attributes.Add("onBlur", "narration_check('" & txtDNarration.ClientID & "');")

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            If (Request.QueryString("edited") <> "") Then
                lblError.Text = getErrorMessage("521")
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "A150085" And ViewState("MainMnu_code") <> OASISConstants.AccPostInternetCollection) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            '''''check menu rights
            ' setting controls
            initialize_components()

            'hide/show controls if view mode
            If Request.QueryString("viewid") <> "" Then
                set_viewdata()
                setViewData()
                setModifyHeader(Request.QueryString("viewid"))
            Else
                ResetViewData()
            End If
            gridbind()
            bind_Currency()

            If Request.QueryString("invalidedit") = "1" Then
                lblError.Text = "Invalid Editid"
                Exit Sub
            End If
            If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
                getnextdocid()
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
    End Sub


    Private Sub bind_Collection() ' bind combos
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT COL_ID, COL_DESCR FROM COLLECTION_M"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddCollection.Items.Clear()
            ddCollection.DataSource = ds.Tables(0)
            ddCollection.DataTextField = "COL_DESCR"
            ddCollection.DataValueField = "COL_ID"
            ddCollection.DataBind()
            ddCollection.SelectedIndex = 0

            ddDCollection.Items.Clear()
            ddDCollection.DataSource = ds.Tables(0)
            ddDCollection.DataTextField = "COL_DESCR"
            ddDCollection.DataValueField = "COL_ID"
            ddDCollection.DataBind()
            ddDCollection.SelectedIndex = 0

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()

        txtDCashflowcode.Text = "364"
        txtDCashflowname.Text = "Internet/Credit Card"
        txtDCashflowname.Attributes.Add("readonly", "readonly")
         txtHExchRate.Attributes.Add("readonly", "readonly")
        txtHLocalRate.Attributes.Add("readonly", "readonly")
        txtDCashflowname.Attributes.Add("readonly", "readonly")

        txtDAccountName.Attributes.Add("readonly", "readonly")
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")
        bind_Collection()
        Session("dtJournal") = DataTables.CreateDataTable_IC()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("idCostChild") = 0
        viewstate("idJournal") = 0
        'btnCancel.Visible = False
        ViewState("str_timestamp") = New Byte()
        btnAdddetails.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
    End Sub


    Private Sub setViewData() 'setting controls on view/edit
        tbl_Details.Visible = False
        imgBank.Enabled = False
        gvJournal.Columns(7).Visible = False
        gvJournal.Columns(8).Visible = False
        imgCalendar.Enabled = False
        imgCashflow.Enabled = False
        DDCurrency.Enabled = False
        ddCollection.Enabled = False
        txtHDocdate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
        'txtDAccountCode.Attributes.Add("readonly", "readonly")
        txtHOldrefno.Attributes.Add("readonly", "readonly")
    End Sub


    Private Sub ResetViewData() 'resetting controls on view/edit
        imgBank.Enabled = True
        tbl_Details.Visible = True
        gvJournal.Columns(7).Visible = True
        gvJournal.Columns(8).Visible = True
         imgCalendar.Enabled = True
        imgCashflow.Enabled = True
        DDCurrency.Enabled = True
        ddCollection.Enabled = True
        txtHDocdate.Attributes.Remove("readonly")
        txtHNarration.Attributes.Remove("readonly")
        txtHOldrefno.Attributes.Remove("readonly")
    End Sub


    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
 
            str_Sql = "SELECT VHH.VHH_REFNO,  VHH.GUID, VHH.VHH_SUB_ID,  VHH.VHH_BSU_ID, VHH.VHH_FYEAR, " _
                & " VHH.VHH_DOCTYPE, VHH.VHH_DOCNO, VHH.VHH_TYPE, VHH.VHH_CHB_ID, VHH.VHH_DOCDT, VHH.VHH_CHQDT," _
                & " VHH.VHH_ACT_ID, VHH.VHH_NOOFINST, VHH.VHH_MONTHINTERVEL, VHH.VHH_PARTY_ACT_ID, " _
                & " VHH.VHH_INSTAMT, VHH.VHH_INTPERCT, VHH.VHH_bINTEREST, VHH.VHH_CALCTYP, " _
                & " VHH.VHH_INT_ACT_ID, VHH.VHH_ACRU_INT_ACT_ID, VHH.VHH_CHQ_pdc_ACT_ID, VHH.VHH_PROV_ACT_ID, " _
                & " VHH.VHH_COL_ACT_ID, VHH.VHH_CUR_ID, VHH.VHH_EXGRATE1, VHH.VHH_EXGRATE2, " _
                & " VHH.VHH_NARRATION, VHH.VHH_bDELETED, VHH.VHH_bPOSTED, VHH.VHH_SESSIONID," _
                & " VHH.VHH_LOCK, VHH.VHH_TIMESTAMP, VHH.VHH_bPDC, VHH.VHH_COL_ID, VHH.VHH_AMOUNT, VHH.VHH_Count," _
                & " VHH.VHH_CRR_ID, VHH.VHH_BANKCHARGE, ACCOUNTS_M.ACT_NAME" _
                & " FROM VOUCHER_H AS VHH INNER JOIN ACCOUNTS_M ON VHH.VHH_ACT_ID = ACCOUNTS_M.ACT_ID" _
                & " WHERE (VHH.GUID = '" & p_Modifyid & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocno.Text = p_Modifyid
                txtHDocdate.Text = Format(CDate(ds.Tables(0).Rows(0)("VHH_DOCDT")), "dd/MMM/yyyy")
                txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION") & ""
                txtHDocno.Text = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                 txtHOldrefno.Text = ds.Tables(0).Rows(0)("VHH_REFNO") & ""
                txtBankCode.Text = ds.Tables(0).Rows(0)("VHH_ACT_ID") & ""
                txtBankDescr.Text = ds.Tables(0).Rows(0)("ACT_NAME") & ""
                bind_Currency()
                setModifyDetails(p_Modifyid)
            Else
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyDetails(ByVal p_Modifyid As String) ' 'setting detail table  Session("dtJournal") (view/del)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String 
            str_Sql = "SELECT VOUCHER_D.GUID, VOUCHER_D.VHD_SUB_ID, VOUCHER_D.VHD_BSU_ID, VOUCHER_D.VHD_FYEAR," _
             & " VOUCHER_D.VHD_DOCTYPE, VOUCHER_D.VHD_DOCNO, VOUCHER_D.VHD_LINEID, VOUCHER_D.VHD_ACT_ID, VOUCHER_D.VHD_CHQNO, " _
             & " VOUCHER_D.VHD_AMOUNT, VOUCHER_D.VHD_NARRATION, VOUCHER_D.VHD_RSS_ID, VOUCHER_D.VHD_OPBAL," _
             & " ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, RPTSETUP_S.RSS_DESCR, POLICY_M.PLY_BMANDATORY, " _
             & " ISNULL(POLICY_M.PLY_COSTCENTER, 'AST') PLY_COSTCENTER, VOUCHER_D.VHD_COL_ID," _
             & " COLLECTION_M.COL_DESCR FROM VOUCHER_D INNER JOIN ACCOUNTS_M ON " _
             & " VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID AND VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID " _
             & " LEFT OUTER JOIN POLICY_M ON  ACCOUNTS_M.ACT_PLY_ID = POLICY_M.PLY_ID " _
             & " LEFT OUTER JOIN RPTSETUP_S ON VOUCHER_D.VHD_RSS_ID = RPTSETUP_S.RSS_ID " _
             & " LEFT OUTER JOIN COLLECTION_M ON VOUCHER_D.VHD_COL_ID = COLLECTION_M.COL_ID" _
             & " WHERE     (VOUCHER_D.VHD_SUB_ID = '" & Session("Sub_ID") & "')" _
             & " AND (VOUCHER_D.VHD_DOCNO = '" & ViewState("str_editData").Split("|")(0) & "') " _
             & " AND (VOUCHER_D.VHD_BSU_ID = '" & Session("sBsuid") & "') " _
             & " AND (VOUCHER_D.VHD_DOCTYPE = 'IC')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'VHD_DEBIT, VHD_CREDIT, VHD_NARRATION, 
                    Dim rDt As DataRow 
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("VHD_LINEID")

                    viewstate("idJournal") = viewstate("idJournal") + 1
                    rDt("Accountid") = ds.Tables(0).Rows(iIndex)("VHD_ACT_ID")
                    rDt("Accountname") = ds.Tables(0).Rows(iIndex)("ACT_NAME")
                    rDt("Narration") = ds.Tables(0).Rows(iIndex)("VHD_NARRATION")

                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("VHD_AMOUNT")
                    rDt("Cashflow") = ds.Tables(0).Rows(iIndex)("VHD_RSS_ID")
                    rDt("Cashflowname") = ds.Tables(0).Rows(iIndex)("RSS_DESCR")
                    rDt("Refno") = ds.Tables(0).Rows(iIndex)("VHD_CHQNO")

                    rDt("Collection") = ds.Tables(0).Rows(iIndex)("COL_DESCR")
                    rDt("CollectionCode") = ds.Tables(0).Rows(iIndex)("VHD_COL_ID")

                    rDt("CostCenter") = ds.Tables(0).Rows(iIndex)("PLY_COSTCENTER")
                    rDt("Required") = ds.Tables(0).Rows(iIndex)("PLY_BMANDATORY")
                    rDt("Status") = "Normal"
                    Session("dtJournal").Rows.Add(rDt)
                Next
                setModifyCost(p_Modifyid)
            Else 
            End If
            viewstate("idJournal") = viewstate("idJournal") + 1 
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyCost(ByVal p_Modifyid As String) ' 'setting cost center table  Session("dtJournal") (view/del)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT GUID, VDS_DOCTYPE ,VDS_DOCNO ," _
            & "VDS_ACT_ID ,VDS_CCS_ID ,VDS_CODE,VDS_DESCR ,VDS_AMOUNT," _
            & "VDS_SLNO,VDS_CCS_ID,VDS_CSS_CSS_ID,VDS_ERN_ID FROM VOUCHER_D_S WHERE VDS_DOCNO='" _
            & ViewState("str_editData").Split("|")(0) & "' and VDS_BDELETED='False' " _
            & " AND (VDS_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (ISNULL(VDS_Auto , 0 ) = 0) " _
            & " AND (VDS_SUB_ID = '" & Session("Sub_ID") & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim rDt As DataRow
                    'Dim i As Integer
                    'Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("JNL_ACT_ID"))
                    rDt = Session("dtCostChild").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Memberid") = ds.Tables(0).Rows(iIndex)("VDS_CODE")
                    rDt("ERN_ID") = ds.Tables(0).Rows(iIndex)("VDS_ERN_ID")
                    rDt("SubMemberid") = ds.Tables(0).Rows(iIndex)("VDS_CSS_CSS_ID")
                    rDt("VoucherId") = ds.Tables(0).Rows(iIndex)("VDS_SLNO")
                    rDt("Costcenter") = ds.Tables(0).Rows(iIndex)("VDS_CCS_ID")
                    rDt("Name") = ds.Tables(0).Rows(iIndex)("VDS_DESCR")
                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("VDS_AMOUNT") 
                    rDt("Status") = "Normal" 
                    Session("dtCostChild").Rows.Add(rDt)
                Next
            Else
            End If
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
 

    Private Sub getnextdocid() '  generate next document number
        If ViewState("datamode") = "add" Then
            Try
                txtHDocno.Text = AccountFunctions.GetNextDocId("IC", Session("sBsuid"), CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                If txtHDocno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub


    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            DDCurrency.Items.Clear()
            DDCurrency.DataSource = MasterFunctions.GetExchangeRates(txtHDocdate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If DDCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
                btnSave.Enabled = True
            Else
                txtHExchRate.Text = "0"
                txtHLocalRate.Text = "0"
                btnSave.Enabled = False
                lblError.Text = "Cannot Save Data. Currency/Exchange Rate Not Set"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function set_default_currency() As Boolean
        Try  
            For Each item As ListItem In DDCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next 
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function


    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        ValidateDateformat()
    End Sub


    Sub ValidateDateformat()
        Dim strfDate As String = txtHDocdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        bind_Currency()
        getnextdocid()
    End Sub


    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, _
    ByVal e As System.EventArgs) Handles btnAdddetails.Click
        'adding to detail table
        txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))
        Dim str_cost_center As String = ""
        '''''FIND ACCOUNT IS THERE
        Dim bool_cost_center_reqired As Boolean = False
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
          & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH<>'CC'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else
                txtDAccountName.Text = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            txtDAccountName.Text = ""
        End Try
        '''''FIND ACCOUNT IS THERE
        If txtDAccountName.Text = "" Then
            lblError.Text = getErrorMessage("303") ' account already there
            Exit Sub
        End If
        Try
            Dim rDt As DataRow

            Dim i As Integer
            Dim dCrorDb As Double
            dCrorDb = CDbl(txtDAmount.Text.Trim)
            If dCrorDb > 0 Then
                rDt = Session("dtJournal").NewRow
                rDt("Id") = ViewState("idJournal")
                ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = txtDAccountCode.Text.Trim
                rDt("Accountname") = txtDAccountName.Text.Trim
                rDt("Narration") = txtDNarration.Text.Trim
                rDt("Amount") = dCrorDb
                rDt("CostCenter") = str_cost_center
                rDt("Required") = bool_cost_center_reqired
                rDt("GUID") = System.DBNull.Value
                rDt("Cashflow") = txtDCashflowcode.Text.Trim
                rDt("Refno") = txtRefNo.Text.Trim
                'CollectionCode
                rDt("Collection") = ddDCollection.SelectedItem.Text
                rDt("CollectionCode") = ddDCollection.SelectedItem.Value
                rDt("Cashflowname") = txtDCashflowname.Text.Trim
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Accountid") = rDt("Accountid") And _
                         Session("dtJournal").Rows(i)("Accountname") = rDt("Accountname") And _
                          Session("dtJournal").Rows(i)("Narration") = rDt("Narration") And _
                          Session("dtJournal").Rows(i)("Amount") = rDt("Amount") Then
                        lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        gridbind()
                        Exit Sub
                    End If
                Next
                Session("dtJournal").Rows.Add(rDt)
            Else
                lblError.Text = "Enter valid number"
                Exit Sub
            End If
            gridbind()
            Clear_Details()
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = getErrorMessage("510")
        End Try

    End Sub


    Private Sub gridbind() ' bind the detail table grid
        Try
            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = DataTables.CreateDataTable_IC()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        dCredit = dCredit + Session("dtJournal").Rows(i)("Amount")
                        ' dTotAmount = dTotAmount + dtCostChild.Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtDTotalamount.Text = dCredit
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function get_mandatory_costcenter(ByVal p_id As String) As String
        If Session("dtJournal").Rows.Count > 0 Then
            For i As Integer = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(i)("id") & "" = p_id Then
                    Return Session("dtJournal").Rows(i)("Costcenter")
                End If
            Next
        End If
        Return ""
    End Function


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
            Dim lblReqd As New Label
            Dim lblid As New Label
            Dim lblAmount As New Label
            Dim btnAlloca As New LinkButton

            lblReqd = e.Row.FindControl("lblRequired")
            lblid = e.Row.FindControl("lblId")
            lblAmount = e.Row.FindControl("lblAmount")
            btnAlloca = e.Row.FindControl("btnAlloca")
            If btnAlloca IsNot Nothing Then
                Dim dAmt As Double
                If CDbl(lblAmount.Text) > 0 Then
                    dAmt = CDbl(lblAmount.Text)
                End If
                btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & get_mandatory_costcenter(lblid.Text) & "');return false;"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        Try
            If btnAdddetails.Visible = True Then
                Dim row As GridViewRow = gvJournal.Rows(e.RowIndex)
                Dim lblTid As New Label
                '        Dim lblGrpCode As New Label
                lblTid = TryCast(row.FindControl("lblId"), Label)

                Dim iRemove As Integer = 0
                Dim str_Index As String = ""
                str_Index = lblTid.Text
                For iRemove = 0 To Session("dtJournal").Rows.Count - 1
                    If str_Index = Session("dtJournal").Rows(iRemove)("id") Then
                        If viewstate("datamode") <> "edit" Then
                            Session("dtJournal").Rows(iRemove).Delete()
                        Else
                            Session("dtJournal").Rows(iRemove)("Status") = "Deleted"
                        End If
                        Exit For
                    End If
                Next
                For iRemove = 0 To Session("dtCostChild").Rows.Count - 1
                    If str_Index = Session("dtCostChild").Rows(iRemove)("Voucherid") Then
                        'session("dtCostChild").Rows(iRemove).Delete()
                        Session("dtCostChild").Rows(iRemove)("Status") = "Deleted"
                    End If
                Next
                gridbind()
            Else
                lblError.Text = "Cannot delete. Please cancel updation and delete"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, _
    ByVal e As System.EventArgs)
        'handles the edit of grid button
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDAccountCode.Text = Session("dtJournal").Rows(iIndex)("Accountid")
                txtDAccountName.Text = Session("dtJournal").Rows(iIndex)("Accountname")
                txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Amount"))
                txtDNarration.Text = Session("dtJournal").Rows(iIndex)("Narration")
                txtDCashflowcode.Text = Session("dtJournal").Rows(iIndex)("Cashflow")
                txtDCashflowname.Text = Session("dtJournal").Rows(iIndex)("Cashflowname")
                txtRefNo.Text = Session("dtJournal").Rows(iIndex)("Refno")
                btnAdddetails.Visible = False
                btnUpdate.Visible = True
                btnEditCancel.Visible = True
                Try
                    ddDCollection.SelectedIndex = -1
                    ddDCollection.Items.FindByValue(Session("dtJournal").Rows(iIndex)("Collectioncode")).Selected = True
                Catch ex As Exception
                End Try
                gvJournal.SelectedIndex = iIndex
                'gridbind()
                Exit For
            End If
        Next
    End Sub


    Protected Sub btnEditCancel_Click(ByVal sender As Object, _
    ByVal e As System.EventArgs) Handles btnEditCancel.Click
        'handles cancel of griddetail
        btnAdddetails.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
        gvJournal.SelectedIndex = -1
        Clear_Details()
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, _
    ByVal e As System.EventArgs) Handles btnUpdate.Click
        'handles the update of grid detail
        Try
            Dim iIndex As Integer = 0
            Dim str_Search As String = ""
            Dim dCrordb As Double = CDbl(txtDAmount.Text)
            str_Search = h_Editid.Value
            '''''
            txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))
            Dim str_cost_center As String = ""
            '''''FIND ACCOUNT IS THERE
            Dim bool_cost_center_reqired As Boolean = False
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String
                str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
                & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
                & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
                & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
                & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
                & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH<>'CC'" _
                & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                    str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                    bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
                Else
                    txtDAccountName.Text = ""
                    lblError.Text = getErrorMessage("303") ' account already there
                    Exit Sub
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try
            If dCrordb = 0 Then
                Exit Sub
            End If
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If (Session("dtJournal").Rows(iIndex)("Accountid") <> txtDAccountCode.Text.Trim) Then
                        ''updation handle here
                        Dim j As Integer = 0
                        If ViewState("datamode") <> "edit" Then
                            While j < Session("dtCostChild").Rows.Count
                                If Session("dtCostChild").Rows(j)("Voucherid") = str_Search Then
                                    Session("dtCostChild").Rows.Remove(Session("dtCostChild").Rows(j))
                                Else
                                    j = j + 1
                                End If
                            End While 
                        End If

                        ''updation handle here
                    End If
                    Session("dtJournal").Rows(iIndex)("Accountid") = txtDAccountCode.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Accountname") = AccountFunctions.check_accountid(txtDAccountCode.Text.Trim, Session("sBsuid"))
                    If dCrordb > 0 Then
                        Session("dtJournal").Rows(iIndex)("Amount") = dCrordb
                    End If
                    Session("dtJournal").Rows(iIndex)("Narration") = txtDNarration.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Cashflow") = txtDCashflowcode.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Cashflowname") = txtDCashflowname.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Collection") = ddDCollection.SelectedItem.Text
                    Session("dtJournal").Rows(iIndex)("CollectionCode") = ddDCollection.SelectedItem.Value
                    Session("dtJournal").Rows(iIndex)("Required") = bool_cost_center_reqired
                    Session("dtJournal").Rows(iIndex)("CostCenter") = str_cost_center
                    Session("dtJournal").Rows(iIndex)("Refno") = txtRefNo.Text.Trim
                    btnAdddetails.Visible = True
                    btnUpdate.Visible = False
                    btnEditCancel.Visible = False
                    gvJournal.SelectedIndex = iIndex
                    gvJournal.SelectedIndex = -1
                    Clear_Details()
                    gridbind()
                    Exit For
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message, "UPDATE")
        End Try
    End Sub


    Private Sub clear_All()
        Session("dtJournal").Rows.Clear()
        Session("dtCostChild").Rows.Clear()
        gridbind()
        Clear_Details()
        txtHOldrefno.Text = "" 
        getnextdocid()
        txtHNarration.Text = ""
    End Sub


    Private Sub Clear_Details()
        txtDAmount.Text = ""
        txtDNarration.Text = txtHNarration.Text & ""
        txtDAccountName.Text = ""
        txtDAccountCode.Text = ""
        txtRefNo.Text = ""
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, _
    ByVal e As System.EventArgs) Handles btnSave.Click
        'save header info 
        ValidateDateformat()
        Dim str_err As String = ""
        Try
            txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
            If txtBankDescr.Text = "" Then
                lblError.Text = "Invalid bank selected"
                txtBankCode.Focus()
                Exit Sub
            Else
                lblError.Text = ""
            End If

            gridbind()
            If Session("dtJournal").Rows.Count = 0 Then
                lblError.Text = getErrorMessage(523)
                Exit Sub
            End If
            Dim s As String = check_Errors()
            If s <> "" Then
                lblError.Text = s
                Exit Sub
            End If

            ViewState("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here
                '@GUID = NULL,
                '@VHH_SUB_ID = N'007',
                '@VHH_BSU_ID = N'125016',
                '@VHH_FYEAR = 2007,
                '@VHH_DOCTYPE = N'CR',
                '@VHH_DOCNO = N'1',
                '@VHH_TYPE = N'P',
                '@VHH_CHB_ID = NULL,                
                '@VHH_DOCDT = N'20 APR 2007',

                'Adding header info params
                Dim cmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVHH_SUB_ID)

                Dim sqlpsqlpVHH_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpVHH_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpVHH_BSU_ID)

                Dim VHH_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                VHH_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(VHH_FYEAR)

                Dim sqlpVHH_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpVHH_DOCTYPE.Value = "IC"
                cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                Dim sqlpVHH_CHB_ID As New SqlParameter("@VHH_CHB_ID", SqlDbType.Int)
                sqlpVHH_CHB_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CHB_ID)

                Dim sqlpVHH_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                If ViewState("datamode") = "edit" Then
                    sqlpVHH_DOCNO.Value = ViewState("str_editData").Split("|")(0) & ""
                Else
                    sqlpVHH_DOCNO.Value = "1"
                End If
                cmd.Parameters.Add(sqlpVHH_DOCNO)

                Dim sqlpVHH_TYPE As New SqlParameter("@VHH_TYPE", SqlDbType.VarChar, 20)
                sqlpVHH_TYPE.Value = "R"
                cmd.Parameters.Add(sqlpVHH_TYPE)


                Dim sqlpVHH_DOCDT As New SqlParameter("@VHH_DOCDT", SqlDbType.DateTime, 30)
                sqlpVHH_DOCDT.Value = txtHDocdate.Text & ""
                cmd.Parameters.Add(sqlpVHH_DOCDT)

                '@VHH_CHQDT = NULL,
                '@VHH_ACT_ID = N'24301002',
                '@VHH_NOOFINST = 0,
                '@VHH_MONTHINTERVEL = 0,
                '@VHH_PARTY_ACT_ID = NULL,
                '@VHH_INSTAMT = NULL,
                '@VHH_INTPERCT = NULL,

                Dim sqlpVHH_CHQDT As New SqlParameter("@VHH_CHQDT", SqlDbType.VarChar, 100)
                sqlpVHH_CHQDT.Value = txtHDocdate.Text & ""
                cmd.Parameters.Add(sqlpVHH_CHQDT)

                Dim sqlpVHH_ACT_ID As New SqlParameter("@VHH_ACT_ID", SqlDbType.VarChar, 20)
                sqlpVHH_ACT_ID.Value = txtBankCode.Text
                cmd.Parameters.Add(sqlpVHH_ACT_ID)

                Dim sqlpVHH_NOOFINST As New SqlParameter("@VHH_NOOFINST", SqlDbType.Int)
                sqlpVHH_NOOFINST.Value = 0
                cmd.Parameters.Add(sqlpVHH_NOOFINST)

                Dim sqlpVHH_MONTHINTERVEL As New SqlParameter("@VHH_MONTHINTERVEL", SqlDbType.Int, 8)
                sqlpVHH_MONTHINTERVEL.Value = 0
                cmd.Parameters.Add(sqlpVHH_MONTHINTERVEL)

                Dim sqlpVHH_PARTY_ACT_ID As New SqlParameter("@VHH_PARTY_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_PARTY_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_PARTY_ACT_ID)

                Dim sqlpVHH_INSTAMT As New SqlParameter("@VHH_INSTAMT", SqlDbType.Decimal, 20)
                sqlpVHH_INSTAMT.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_INSTAMT)

                Dim sqlpVHH_INTPERCT As New SqlParameter("@VHH_INTPERCT", SqlDbType.Decimal, 20)
                sqlpVHH_INTPERCT.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_INTPERCT)

                '@VHH_bINTEREST = NULL,
                '@VHH_CALCTYP = NULL,
                '@VHH_INT_ACT_ID = NULL,
                '@VHH_ACRU_INT_ACT_ID = NULL,
                '@VHH_CHQ_pdc_ACT_ID = NULL,
                '@VHH_PROV_ACT_ID = NULL,
                '@VHH_COL_ACT_ID = NULL,
                '@VHH_CUR_ID = N'DHS',
                '@VHH_EXGRATE1 = 10,
                '@VHH_EXGRATE2 = 20,
                Dim sqlpVHH_bINTEREST As New SqlParameter("@VHH_bINTEREST", SqlDbType.Bit)
                sqlpVHH_bINTEREST.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_bINTEREST)

                Dim sqlpVHH_CALCTYP As New SqlParameter("@VHH_CALCTYP", SqlDbType.VarChar)
                sqlpVHH_CALCTYP.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CALCTYP)

                Dim sqlpVHH_INT_ACT_ID As New SqlParameter("@VHH_INT_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_INT_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_INT_ACT_ID)

                Dim sqlpVHH_ACRU_INT_ACT_ID As New SqlParameter("@VHH_ACRU_INT_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_ACRU_INT_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_ACRU_INT_ACT_ID)

                Dim sqlpVHH_CHQ_pdc_ACT_ID As New SqlParameter("@VHH_CHQ_pdc_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_CHQ_pdc_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CHQ_pdc_ACT_ID)

                Dim sqlpVHH_PROV_ACT_ID As New SqlParameter("@VHH_PROV_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_PROV_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_PROV_ACT_ID)

                Dim sqlpVHH_COL_ACT_ID As New SqlParameter("@VHH_COL_ACT_ID", SqlDbType.VarChar)
                sqlpVHH_COL_ACT_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_COL_ACT_ID)

                Dim sqlpVHH_CUR_ID As New SqlParameter("@VHH_CUR_ID", SqlDbType.VarChar, 12)
                sqlpVHH_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                cmd.Parameters.Add(sqlpVHH_CUR_ID)

                Dim sqlpVHH_EXGRATE1 As New SqlParameter("@VHH_EXGRATE1", SqlDbType.Decimal, 8)
                sqlpVHH_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                cmd.Parameters.Add(sqlpVHH_EXGRATE1)

                Dim sqlpVHH_EXGRATE2 As New SqlParameter("@VHH_EXGRATE2", SqlDbType.Decimal, 8)
                sqlpVHH_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                cmd.Parameters.Add(sqlpVHH_EXGRATE2)

                '@VHH_NARRATION = N'CHUMMA',
                '@VHH_bDELETED = 0,
                '@VHH_bPOSTED = 0,
                '@bGenerateNewNo = 1,
                '@VHH_TIMESTAMP = NULL,
                ''''''
                Dim sqlpVHH_AMOUNT As New SqlParameter("@VHH_AMOUNT", SqlDbType.Decimal, 21)
                sqlpVHH_AMOUNT.Value = CDbl(txtDTotalamount.Text)
                cmd.Parameters.Add(sqlpVHH_AMOUNT)
                '''''''''''

                Dim sqlpVHH_NARRATION As New SqlParameter("@VHH_NARRATION", SqlDbType.VarChar, 300)
                sqlpVHH_NARRATION.Value = txtHNarration.Text & ""
                cmd.Parameters.Add(sqlpVHH_NARRATION)

                Dim sqlpVHH_bPOSTED As New SqlParameter("@VHH_bPOSTED", SqlDbType.Bit)
                sqlpVHH_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpVHH_bPOSTED)

                Dim sqlpVHH_bDELETED As New SqlParameter("@VHH_bDELETED", SqlDbType.Bit)
                sqlpVHH_bDELETED.Value = False
                cmd.Parameters.Add(sqlpVHH_bDELETED)

                Dim sqlpbGenerateNewNo As New SqlParameter("@bGenerateNewNo", SqlDbType.Bit)
                sqlpbGenerateNewNo.Value = True
                cmd.Parameters.Add(sqlpbGenerateNewNo)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim sqlpVHH_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If ViewState("datamode") <> "edit" Then
                    sqlpVHH_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpVHH_TIMESTAMP.Value = ViewState("str_timestamp")
                End If
                cmd.Parameters.Add(sqlpVHH_TIMESTAMP)

                '@VHH_SESSIONID =  N'123',
                '@VHH_LOCK =  N'master',
                '@bEdit = 0,
                '@VHH_bPDC = 0,
                '@VHH_COL_ID = NULL,
                '@VHH_NEWDOCNO = @VHH_NEWDOCNO OUTPUT
                '@VHH_REFNO

                Dim sqlpVHH_SESSIONID As New SqlParameter("@VHH_SESSIONID", SqlDbType.VarChar, 50)
                sqlpVHH_SESSIONID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpVHH_SESSIONID)

                Dim sqlpVHH_LOCK As New SqlParameter("@VHH_LOCK", SqlDbType.VarChar, 50)
                sqlpVHH_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpVHH_LOCK)

                Dim sqlpVHH_bPDC As New SqlParameter("@VHH_bPDC", SqlDbType.Bit)
                sqlpVHH_bPDC.Value = 0
                cmd.Parameters.Add(sqlpVHH_bPDC)

                Dim sqlpVHH_COL_ID As New SqlParameter("@VHH_COL_ID", SqlDbType.Int)
                sqlpVHH_COL_ID.Value = ddCollection.SelectedItem.Value
                cmd.Parameters.Add(sqlpVHH_COL_ID)

                Dim sqlpVHH_CRR_ID As New SqlParameter("@VHH_CRR_ID", SqlDbType.Int)
                sqlpVHH_CRR_ID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHH_CRR_ID)

                Dim sqlpVHH_REFNO As New SqlParameter("@VHH_REFNO", SqlDbType.VarChar, 50)
                sqlpVHH_REFNO.Value = txtHOldrefno.Text
                cmd.Parameters.Add(sqlpVHH_REFNO)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlopVHH_NEWDOCNO As New SqlParameter("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
                cmd.Parameters.Add(sqlopVHH_NEWDOCNO)
                cmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Adding header info
                cmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String = ""
                If (iReturnvalue = 0) Then
                    'if header is inserted save detail record
                    If ViewState("datamode") <> "edit" Then
                        str_err = DoTransactions(objConn, stTrans, sqlopVHH_NEWDOCNO.Value)
                    Else
                        iReturnvalue = DeleteVOUCHER_D_S_ALL(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        If iReturnvalue = 0 Then
                            str_err = DoTransactions(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        End If
                    End If
                    If str_err = "0" Then
                        stTrans.Commit()
                        h_editorview.Value = ""
                        txtDNarration.Text = ""


                        gvJournal.Enabled = True
                        If ViewState("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, sqlopVHH_NEWDOCNO.Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            lblError.Text = getErrorMessage(str_err)
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            'datamode = Encr_decrData.Encrypt("view")
                            'Response.Redirect("acccrCashReceipt.aspx?viewid=" & Request.QueryString("viewid") & "&edited=yes" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamode)
                            lblError.Text = getErrorMessage(str_err)
                        End If
                        clear_All()
                    Else '.Split("__")(0).Trim & ""
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim)
                        stTrans.Rollback()
                    End If
                Else
                    lblError.Text = getErrorMessage(iReturnvalue & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString


        cmd.Dispose()

        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = "R"
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        'save the detail record
        Dim iReturnvalue As Integer 
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                cmd.Dispose()
                cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '' ''Handle sub table - save cost center

                str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtJournal").Rows(iIndex)("id"), _
                "CR", iIndex + 1 - ViewState("iDeleteCount"), Session("dtJournal").Rows(iIndex)("Accountid"), Session("dtJournal").Rows(iIndex)("Amount"))
                If str_err <> "0" Then
                    Return str_err
                End If

                '@GUID = NULL,
                '@VHD_SUB_ID = N'007',
                '@VHD_BSU_ID = N'125016',
                '@VHD_FYEAR = 2007,
                '@VHD_DOCTYPE = N'CR',
                '@VHD_DOCNO = N'CP000007',
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpVHD_SUB_ID As New SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHD_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVHD_SUB_ID)

                Dim sqlpsqlpVHD_BSU_ID As New SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpVHD_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpVHD_BSU_ID)

                Dim sqlpVHD_FYEAR As New SqlParameter("@VHD_FYEAR", SqlDbType.Int)
                sqlpVHD_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpVHD_FYEAR)

                Dim sqlpVHD_DOCTYPE As New SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpVHD_DOCTYPE.Value = "IC"
                cmd.Parameters.Add(sqlpVHD_DOCTYPE)

                Dim sqlpVHD_DOCNO As New SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
                sqlpVHD_DOCNO.Value = p_docno & ""
                cmd.Parameters.Add(sqlpVHD_DOCNO)


                '@VHD_LINEID = 1,
                '@VHD_ACT_ID = N'02201003',
                '@VHD_AMOUNT = 100,
                '@VHD_NARRATION = N'NARRA1',
                '@VHD_CHQID = NULL,
                '@VHD_CHQNO = NULL,
                '@VHD_CHQDT = '',

                Dim sqlpVHD_LINEID As New SqlParameter("@VHD_LINEID", SqlDbType.Int)
                sqlpVHD_LINEID.Value = iIndex + 1 - ViewState("iDeleteCount")
                cmd.Parameters.Add(sqlpVHD_LINEID)

                Dim sqlpVHD_ACT_ID As New SqlParameter("@VHD_ACT_ID", SqlDbType.VarChar, 12)
                sqlpVHD_ACT_ID.Value = Session("dtJournal").Rows(iIndex)("Accountid") & ""
                cmd.Parameters.Add(sqlpVHD_ACT_ID)

                Dim sqlpVHD_AMOUNT As New SqlParameter("@VHD_AMOUNT", SqlDbType.Decimal, 21)
                sqlpVHD_AMOUNT.Value = Session("dtJournal").Rows(iIndex)("Amount") & ""
                cmd.Parameters.Add(sqlpVHD_AMOUNT)

                Dim sqlpVHD_NARRATION As New SqlParameter("@VHD_NARRATION", SqlDbType.VarChar, 300)
                sqlpVHD_NARRATION.Value = Session("dtJournal").Rows(iIndex)("Narration") & ""
                cmd.Parameters.Add(sqlpVHD_NARRATION)

                Dim sqlpVHD_CHQID As New SqlParameter("@VHD_CHQID", SqlDbType.VarChar, 8)
                sqlpVHD_CHQID.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHD_CHQID)

                Dim sqlpVHD_CHQNO As New SqlParameter("@VHD_CHQNO", SqlDbType.VarChar, 8)
                sqlpVHD_CHQNO.Value = Session("dtJournal").Rows(iIndex)("Refno") & ""
                cmd.Parameters.Add(sqlpVHD_CHQNO)

                Dim sqlpVHD_CHQDT As New SqlParameter("@VHD_CHQDT", SqlDbType.VarChar, 2)
                sqlpVHD_CHQDT.Value = " "
                cmd.Parameters.Add(sqlpVHD_CHQDT)

                '@VHD_RSS_ID = 68,
                '@VHD_OPBAL = 0,
                '@VHD_INTEREST = 0,
                '@VHD_bBOUNCED = 0,
                '@VHD_bCANCELLED = 0,
                '@VHD_bDISCONTED = 0,
                '@bEdit = 0  

                Dim sqlpVHD_RSS_ID As New SqlParameter("@VHD_RSS_ID", SqlDbType.Int)
                sqlpVHD_RSS_ID.Value = Session("dtJournal").Rows(iIndex)("Cashflow") & ""
                cmd.Parameters.Add(sqlpVHD_RSS_ID)

                Dim sqlpVHD_OPBAL As New SqlParameter("@VHD_OPBAL", SqlDbType.Decimal, 20)
                sqlpVHD_OPBAL.Value = 0
                cmd.Parameters.Add(sqlpVHD_OPBAL)

                Dim sqlpVHD_INTEREST As New SqlParameter("@VHD_INTEREST", SqlDbType.Decimal, 30)
                sqlpVHD_INTEREST.Value = System.DBNull.Value
                cmd.Parameters.Add(sqlpVHD_INTEREST)

                Dim sqlpVHD_bBOUNCED As New SqlParameter("@VHD_bBOUNCED", SqlDbType.Bit)
                sqlpVHD_bBOUNCED.Value = False
                cmd.Parameters.Add(sqlpVHD_bBOUNCED)

                Dim sqlpVHD_bCANCELLED As New SqlParameter("@VHD_bCANCELLED", SqlDbType.Bit)
                sqlpVHD_bCANCELLED.Value = False
                cmd.Parameters.Add(sqlpVHD_bCANCELLED)

                'Dim sqlpbVHD_BDELETED As New SqlParameter("@VHD_BDELETED", SqlDbType.Bit)
                'sqlpbVHD_BDELETED.Value = False
                'cmd.Parameters.Add(sqlpbVHD_BDELETED)

                Dim sqlpbVHD_bDISCONTED As New SqlParameter("@VHD_bDISCONTED", SqlDbType.Int)
                sqlpbVHD_bDISCONTED.Value = 0
                cmd.Parameters.Add(sqlpbVHD_bDISCONTED)


                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)
                '@VHD_COL_ID
                Dim sqlpVHD_COL_ID As New SqlParameter("@VHD_COL_ID", SqlDbType.Int)
                sqlpVHD_COL_ID.Value = Session("dtJournal").Rows(iIndex)("CollectionCODE")
                cmd.Parameters.Add(sqlpVHD_COL_ID)

                '' ''Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                '' ''If iIndex =  Session("dtJournal").Rows.Count - 1 Then
                '' ''    sqlpbLastRec.Value = True
                '' ''Else
                '' ''    sqlpbLastRec.Value = False
                '' ''End If
                '' ''cmd.Parameters.Add(sqlpbLastRec)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                Dim success_msg As String = ""
                If iReturnvalue <> 0 Then
                    Exit For
                Else

                End If
                cmd.Parameters.Clear()
            Else
                If Not Session("dtJournal").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                    ViewState("iDeleteCount") = ViewState("iDeleteCount") + 1
                    cmd = New SqlCommand("DeleteVOUCHER_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""

                    If iReturnvalue <> 0 Then
                        Return iReturnvalue
                    End If
                    cmd.Parameters.Clear()
                End If
            End If
        Next
        If iIndex <= Session("dtJournal").Rows.Count - 1 Then
            Return iReturnvalue
        Else
            Return iReturnvalue
        End If 
    End Function


    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer
        Dim x As Integer = 1

        Dim str_err As String = "0"
        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                    iLineid = -1
                    str_balanced = check_others(p_voucherid, p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    lblError.Text = "Error not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")

                cmd.Dispose()

                cmd = New SqlCommand("SaveVOUCHER_D_S", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '       @GUID = NULL,
                '		@VDS_SUB_ID = N'007',
                '		@VDS_BSU_ID = N'125016',
                '		@VDS_FYEAR = 2007,
                '		@VDS_DOCTYPE = N'CR',

                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                sqlpGUID.Value = Session("dtCostChild").Rows(iIndex)("GUID")
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpVDS_ID As New SqlParameter("@VDS_ID", SqlDbType.Int)
                sqlpVDS_ID.Value = p_slno
                cmd.Parameters.Add(sqlpVDS_ID)

                Dim sqlpVDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVDS_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVDS_SUB_ID)

                Dim sqlpVDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
                sqlpVDS_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpVDS_BSU_ID)

                Dim sqlpVDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
                sqlpVDS_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpVDS_FYEAR)

                Dim sqlpVDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
                sqlpVDS_DOCTYPE.Value = "IC"
                cmd.Parameters.Add(sqlpVDS_DOCTYPE)

                '		@VDS_DOCNO = N'CP000017',
                '		@VDS_DOCDT = N'08/20/2007',
                '		@VDS_ACT_ID = N'02101001',
                '		@VDS_ID=N'111',
                '		@VDS_SLNO = 1,
                '		@VDS_AMOUNT = 299,
                '		@VDS_CCS_ID = N'AST',
                '		@VDS_CODE = N'TEST',
                '		@VDS_DRCR = N'DR',
                '		@VDS_bPOSTED = 0,
                '		@VDS_bDELETED = 0,
                '		@bEdit = 0

                Dim sqlpVDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
                sqlpVDS_DOCNO.Value = p_docno
                cmd.Parameters.Add(sqlpVDS_DOCNO)

                Dim sqlpVDS_DOCDT As New SqlParameter("@VDS_DOCDT", SqlDbType.DateTime, 30)
                sqlpVDS_DOCDT.Value = txtHDocdate.Text & ""
                cmd.Parameters.Add(sqlpVDS_DOCDT)

                Dim sqlpVDS_ACT_ID As New SqlParameter("@VDS_ACT_ID", SqlDbType.VarChar, 20)
                sqlpVDS_ACT_ID.Value = p_accountid
                cmd.Parameters.Add(sqlpVDS_ACT_ID)

                Dim sqlpbVDS_SLNO As New SqlParameter("@VDS_SLNO", SqlDbType.Int)
                sqlpbVDS_SLNO.Value = p_slno
                cmd.Parameters.Add(sqlpbVDS_SLNO)

                Dim sqlpVDS_AMOUNT As New SqlParameter("@VDS_AMOUNT", SqlDbType.Decimal, 20)
                sqlpVDS_AMOUNT.Value = Session("dtCostChild").Rows(iIndex)("Amount")
                cmd.Parameters.Add(sqlpVDS_AMOUNT)

                Dim sqlpVDS_CCS_ID As New SqlParameter("@VDS_CCS_ID", SqlDbType.VarChar, 20)
                sqlpVDS_CCS_ID.Value = Session("dtCostChild").Rows(iIndex)("costcenter")
                cmd.Parameters.Add(sqlpVDS_CCS_ID)

                Dim sqlpVDS_CODE As New SqlParameter("@VDS_CODE", SqlDbType.VarChar, 20)
                sqlpVDS_CODE.Value = Session("dtCostChild").Rows(iIndex)("Memberid")
                cmd.Parameters.Add(sqlpVDS_CODE)

                Dim sqlpVDS_DESCR As New SqlParameter("@VDS_DESCR", SqlDbType.VarChar, 20)
                If Session("dtCostChild").Rows(iIndex)("Memberid") Is System.DBNull.Value Then
                    sqlpVDS_DESCR.Value = Session("dtCostChild").Rows(iIndex)("Memberid")
                Else
                    sqlpVDS_DESCR.Value = Session("dtCostChild").Rows(iIndex)("Name")
                End If
                cmd.Parameters.Add(sqlpVDS_DESCR)

                Dim sqlpbVDS_DRCR As New SqlParameter("@VDS_DRCR", SqlDbType.VarChar, 2)
                sqlpbVDS_DRCR.Value = "DR" 'p_crdr
                cmd.Parameters.Add(sqlpbVDS_DRCR)

                Dim sqlpVDS_bPOSTED As New SqlParameter("@VDS_bPOSTED", SqlDbType.Bit)
                sqlpVDS_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpVDS_bPOSTED)

                Dim sqlpbVDS_BDELETED As New SqlParameter("@VDS_BDELETED", SqlDbType.Bit)
                sqlpbVDS_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbVDS_BDELETED)


                '' sadhu
                Dim sqlpbVDS_CSS_CSS_ID As New SqlParameter("@VDS_CSS_CSS_ID", SqlDbType.VarChar)
                sqlpbVDS_CSS_CSS_ID.Value = Session("dtCostChild").Rows(iIndex)("SubMemberid")
                cmd.Parameters.Add(sqlpbVDS_CSS_CSS_ID)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                Dim success_msg As String = ""

                If iReturnvalue <> 0 Then
                    Exit For

                Else



                    '' -------------------------added  for insert DEPARTMENT...
                    If (Session("dtCostChild").Rows(iIndex)("costcenter") = Session("CostEMP")) Then

                        Dim vchId As Integer = Session("dtCostChild").Rows(iIndex)("VoucherId")
                        Dim filterExp As String = "costcenter='" & Session("CostEMP") & "' and VoucherId = " & vchId & ""
                        Dim _dtRow() As DataRow = Session("dtCostChild").Select(filterExp)



                        If iIndex = (_dtRow.Length + Convert.ToInt32(Session("CHECKLAST"))) - 1 Then
                            'If iIndex = _dtRow.Length - 1 Then


                            cmd.CommandText = "SaveVOUCHER_D_S_dpt"
                            Dim sqlVDSCCSID As New SqlParameter("@VDSCCSID", SqlDbType.VarChar)
                            sqlVDSCCSID.Value = "0006"
                            cmd.Parameters.Add(sqlVDSCCSID)



                            cmd.ExecuteNonQuery()
                            iReturnvalue = retValParam.Value
                            success_msg = ""
                            x = 1
                            Session("CHECKLAST") = iIndex + 1
                            If iReturnvalue <> 0 Then
                                Exit For
                            End If

                        End If
                    End If
                    '' -------------------------added Sadhu for insert OTHER COSTCENTER...
                    If (Session("dtCostChild").Rows(iIndex)("costcenter") = Session("CostOTH")) Then

                        Dim vchIdOth As Integer = Session("dtCostChild").Rows(iIndex)("VoucherId")
                        Dim filterExpOth As String = "costcenter='" & Session("CostOTH") & "' and VoucherId = " & vchIdOth & "  AND SubMemberId <> '' "
                        Dim _dtRowOth() As DataRow = Session("dtCostChild").Select(filterExpOth)


                        If iIndex = (_dtRowOth.Length + Convert.ToInt32(Session("CHECKLAST"))) - 1 Then

                            cmd.CommandText = "SaveVOUCHER_D_S_Oth"
                            cmd.ExecuteNonQuery()
                            iReturnvalue = retValParam.Value
                            success_msg = ""
                            x = 1
                            Session("CHECKLAST") = iIndex + 1
                            If iReturnvalue <> 0 Then
                                Exit For
                            End If

                        End If

                    End If
                    '--------------------------------------------------



                End If
                cmd.Parameters.Clear()

            End If
        Next
        Return iReturnvalue
    End Function


    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Try
            Dim dTotal As Double = 0
            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("costcenter") = p_costid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function


    Function check_others(ByVal p_voucherid As String, ByVal p_total As String) As Boolean
        Try
            Dim dTotal As Double = 0

            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function


    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" _
            & " AND VHH_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then

                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim stTrans As SqlTransaction = objConn.BeginTransaction
                    viewstate("str_editData") = ds.Tables(0).Rows(0)("VHH_DOCNO") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_DOCDT") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_FYEAR") & "|"

                    Dim cmd As New SqlCommand("LockVOUCHER_H", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpVHH_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpVHH_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpVHH_BSU_ID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = "IC"
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = viewstate("str_editData").Split("|")(0)
                    cmd.Parameters.Add(sqlpVHH_DOCNO)


                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        stTrans.Rollback()
                        lblError.Text = getErrorMessage(iReturnvalue)
                        Return iReturnvalue
                    End If
                    stTrans.Commit()
                    ViewState("str_timestamp") = sqlopVHH_TIMESTAMP.Value
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
                ViewState("str_editData") = ""
                lblError.Text = "Invalid Edit id"
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = "Invalid Edit id"
            Return " | | "
        End Try
        Return True
    End Function


    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " VHH_DOCNO='" & ViewState("str_editData").Split("|")(0) & "'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then

                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpVHH_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpVHH_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                    cmd.Parameters.Add(sqlpVHH_FYEAR)

                    Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCTYPE.Value = "IC"
                    cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                    Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpVHH_DOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                    cmd.Parameters.Add(sqlpVHH_DOCNO)

                    Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpVHH_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpVHH_CUR_ID)

                    Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpVHH_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpVHH_USER)

                    Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopVHH_TIMESTAMP.Value = ViewState("str_timestamp")
                    cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'btnCancel.Visible = True
        tbl_Details.Visible = False
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
            End If
        Else
            ResetViewData()
            h_editorview.Value = "Edit"
            viewstate("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
            btnSave.Enabled = True
        End If
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            h_editorview.Value = "Edit"
            If ViewState("datamode") = "edit" Then
                unlock()
            End If
            setViewData()
            'clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
               & " GUID='" & Request.QueryString("viewid") & "'"
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim cmd As New SqlCommand("DeleteVOUCHER", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)
                '@DOCNO	varchar(20),
                '@DOCTYPE	varchar(20) ,
                '@VHH_BSU_ID	varchar(10), 
                '@VHH_FYEAR	int,
                '@VHH_SUB_ID varchar(20),
                '@VHH_LOCK varc
                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "IC"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("VHH_BSU_ID") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVHH_SUB_ID)

                Dim sqlpVHH_LOCK As New SqlParameter("@VHH_LOCK", SqlDbType.VarChar, 30)
                sqlpVHH_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpVHH_LOCK)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    lblError.Text = getErrorMessage("519")
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    clear_All()
                    Response.Redirect("accccViewCreditcardReceipt.aspx?deleted=done" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub


    Protected Sub txtOAmt_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("onBlur", "find_Ototal();")
        txtAmount.Attributes.Add("onFocus", "this.select();")
    End Sub


    Private Function set_viewdata() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" _
            & " AND VHH_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("VHH_DOCDT")
                'txtHDocno.Text = Request.QueryString("editid")
                txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Try
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("VHH_DOCNO") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_DOCDT") & "|" _
                   & ds.Tables(0).Rows(0)("VHH_FYEAR") & "|"
                    Return ""
                Catch ex As Exception
                    Errorlog(ex.Message)
                End Try
            Else
                ViewState("str_editData") = ""
                viewstate("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("acccrCashReceipt.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            viewstate("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("acccrCashReceipt.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
        End Try
        Return True
    End Function


    Protected Sub lblAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub


    Private Function check_Errors() As String
        Dim str_Error As String = ""
        Dim str_Err As String = ""
        Try
            'Adding transaction info
            Dim iIndex As Integer
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                Dim str_manndatory_costcenter As String
                If Convert.ToBoolean(Session("dtJournal").Rows(iIndex)("required")) = True Then
                    str_manndatory_costcenter = Session("dtJournal").Rows(iIndex)("Costcenter")
                Else
                    str_manndatory_costcenter = ""
                End If
                If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    str_Err = check_Errors_sub(Session("dtJournal").Rows(iIndex)("id"), _
                            Session("dtJournal").Rows(iIndex)("Amount"), str_manndatory_costcenter)
                    If str_Err <> "" Then
                        str_Error = str_Error & "<br/>" & str_Err & " At Line - " & iIndex + 1
                    End If
                End If
            Next
            'Adding transaction info
            Return str_Error
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
        Return ""
    End Function


    Private Function check_Errors_sub(ByVal p_voucherid As String, ByVal p_amount As String, _
    Optional ByVal p_mandatory_costcenter As String = "") As String
        Try
            Dim str_cur_cost_center As String = ""
            Dim str_prev_cost_center As String = ""
            'Dim dTotal As Double = 0
            Dim iIndex As Integer
            Dim iLineid As Integer
            Dim bool_check_other, bool_mandatory_exists As Boolean

            Dim str_err As String = ""
            If p_mandatory_costcenter <> "" Then
                bool_mandatory_exists = False
            Else
                bool_mandatory_exists = True
            End If
            Dim str_balanced As Boolean = True
            bool_check_other = False
            For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If Session("dtCostChild").Rows(iIndex)("costcenter") = p_mandatory_costcenter Then
                        bool_mandatory_exists = True
                    End If
                    If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                        iLineid = -1
                        str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                        If str_balanced = False Then
                            str_err = str_err & " <BR> Invalid Allocation for cost center " & AccountFunctions.get_cost_center(Session("dtCostChild").Rows(iIndex)("costcenter"))
                        End If
                    End If
                    str_balanced = True
                    If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And bool_check_other = False Then
                        iLineid = -1
                        bool_check_other = True
                        str_balanced = check_others(p_voucherid, p_amount)
                        If str_balanced = False Then
                            If str_err = "" Then
                                str_err = "Invalid Allocation for other cost center"
                            Else
                                str_err = str_err & " <BR> Invalid Allocation for other cost centers "
                            End If

                        End If
                    End If
                    iLineid = iLineid + 1
                    'If str_balanced = False Then
                    '    Exit For
                    'End If
                    str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                End If
            Next
            If bool_mandatory_exists = False Then
                str_err = "<br>Mandatory Cost center - " & AccountFunctions.get_cost_center(p_mandatory_costcenter) & " not allocated"
            End If
            Return str_err
        Catch ex As Exception
            Errorlog(ex.Message, "child")
            Return ""
        End Try
    End Function


    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        h_editorview.Value = ""
        If viewstate("datamode") = "edit" Then
            unlock()
        End If
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
        ResetViewData()
    End Sub


    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim str_Sql, strFilter As String
        strFilter = " VHH_FYEAR = " & Session("F_YEAR") & " and VHH_DOCTYPE = 'IC' and VHH_DOCNO = '" & txtHDocno.Text _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(txtHDocdate.Text)) & "' and VHH_BSU_ID in('" & Session("sBsuid") & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(Session("SUB_ID"), "IC", txtHDocno.Text)
            params("voucherName") = "INTERNET COLLECTION"
            params("Summary") = False
            repSource.VoucherName = "INTERNET COLLECTION"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(txtHDocno.Text, "IC", Session("sBSUID"))
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/CreditCardReport.rpt"
            Session("ReportSource") = repSource
            'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub imgCalendar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        getnextdocid()
        bind_Currency()
    End Sub


    Protected Sub ddCollection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCollection.SelectedIndexChanged
        ddDCollection.SelectedIndex = -1
        ddDCollection.Items.FindByValue(ddCollection.SelectedItem.Value).Selected = True
    End Sub


    Protected Sub btnAccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAccount.Click
        chk_bankcode()
    End Sub


    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
    End Sub


    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            lblError.Text = "Invalid bank selected"
            txtBankCode.Focus()
        Else
            lblError.Text = ""
            txtHNarration.Focus()
        End If
        'set_collection()
    End Sub


    Protected Sub txtDAccountCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDAccountCode.TextChanged
        txtDAccountName.Text = AccountFunctions.Validate_Account(txtDAccountCode.Text, Session("sbsuid"), "NOTCC")
        If txtDAccountName.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            txtDAccountCode.Focus()
        Else
            lblError.Text = ""
            txtDNarration.Focus()
        End If
    End Sub


    Protected Sub ddDCollection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddDCollection.SelectedIndexChanged
        Dim str_collection As String = AccountFunctions.get_CollectionAccount(ddDCollection.SelectedItem.Value, Session("sBsuid"))
        Try
            txtDAccountCode.Text = str_collection.Split("|")(0)
            txtDAccountName.Text = str_collection.Split("|")(1)
        Catch ex As Exception
        End Try
    End Sub


End Class
