﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accInterunitMonthendSetOff.aspx.vb" Inherits="Accounts_accInterunitMonthendSetOff" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">   

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Adjustment Journal
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="CHQ_CANCEL" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" DataSourceID="sdsBusinessunit"
                                DataTextField="BSU_NAME" DataValueField="BSU_ID" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList><asp:SqlDataSource ID="sdsBusinessunit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="select BSU_ID , BSU_NAME from [fn_GetBusinessUnits]   (@Usr_name) WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME">
                                <SelectParameters>
                                    <asp:SessionParameter Name="Usr_name" SessionField="sUsr_name" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Doc Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtdocDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="DocDate0" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtdocDate" TargetControlID="txtdocDate">
                            </ajaxToolkit:CalendarExtender>

                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Details</span></td>
                        <td align="left" colspan="3">

                            <asp:GridView ID="gvIJVDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="ACT_ID" HeaderText="Code" ReadOnly="True" />
                                    <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" />
                                    <asp:BoundField DataField="Amt" HeaderText="Amount" ReadOnly="True" />
                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit" />
                                    <asp:BoundField DataField="OPPAMT" HeaderText="Amount" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Post">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkPost" runat="server" />
                                            <asp:Label ID="lblBSU_ID" runat="server" Text='<%# Bind("BSU_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="CHQ_CANCEL" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
