Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class ShowEXPP
    Inherits System.Web.UI.Page
    Shared SearchMode As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Try
                SearchMode = Request.QueryString("id")

                Page.DataBind()

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                gridbind()
            Catch ex As Exception

            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub



    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub



    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Private Sub gridbind()
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_code As String
            Dim str_filter_name As String

            'If SearchMode = "CU" Then
            '    str_Sql = "Select * from(SELECT Cut_ID as ID,Cut_Descr as E_Name,CUT_PPAID_ACT_ID as ID2,CUT_EXP_ACT_ID as ID3 FROM Costunit_m)a WHERE a.ID<>'' "
            'ElseIf SearchMode = "PP" Then

            '    str_Sql = "Select * from(SELECT ACT_ID as ID,ACT_NAME as E_Name,'' as ID2,'' as ID3,ACT_bPREPAYMENT,ACT_TYPE FROM Accounts_m)a WHERE (a.ACT_bPREPAYMENT=1 and a.ACT_TYPE='ASSET') and a.ID<>'' "
            'ElseIf SearchMode = "EA" Then
            '    str_Sql = "Select * from(SELECT ACT_ID as ID,ACT_NAME as E_Name,'' as ID2,'' as ID3,ACT_TYPE FROM Accounts_m)a WHERE  a.ACT_TYPE='EXPENSES'and a.ID<>'' "
            'ElseIf SearchMode = "PA" Then
            '    str_Sql = "Select * from(SELECT ACT_ID as ID,ACT_NAME as E_Name,'' as ID2,'' as ID3,ACT_FLAG FROM Accounts_m)a WHERE a.ACT_FLAG='S' and a.ID<>'' "
            'End If

            If SearchMode = "CU" Then
                str_Sql = "Select * from(SELECT Cut_ID as ID,Cut_Descr as E_Name,CUT_PPAID_ACT_ID as ID2,CUT_EXP_ACT_ID as ID3 FROM Costunit_m)a WHERE a.ID<>'' "
            ElseIf SearchMode = "PP" Then

                str_Sql = "Select * from(SELECT ACT_ID as ID,ACT_NAME as E_Name,'' as ID2,'' as ID3,ACT_bPREPAYMENT,ACT_TYPE,ACT_BACTIVE,ACT_BSU_ID FROM Accounts_m)a WHERE (a.ACT_bPREPAYMENT=1 and a.ACT_TYPE='ASSET' and a.ACT_BACTIVE=1 and a.ACT_BSU_ID like '%" & Session("sBsuid") & "%') and a.ID<>'' "
            ElseIf SearchMode = "EA" Then
                str_Sql = "Select * from(SELECT ACT_ID as ID,ACT_NAME as E_Name,'' as ID2,'' as ID3,ACT_TYPE,ACT_BACTIVE,ACT_BSU_ID FROM Accounts_m)a WHERE  a.ACT_TYPE='EXPENSES' and a.ACT_BACTIVE=1 and a.ACT_BSU_ID like '%" & Session("sBsuid") & "%' and a.ID<>'' "
            ElseIf SearchMode = "PA" Then
                str_Sql = "Select * from(SELECT ACT_ID as ID,ACT_NAME as E_Name,'' as ID2,'' as ID3,ACT_FLAG,ACT_BACTIVE,ACT_BSU_ID FROM Accounts_m)a WHERE a.ACT_FLAG='S' and a.ACT_BACTIVE=1 and a.ACT_BSU_ID like '%" & Session("sBsuid") & "%' and  a.ID<>'' "
            End If

            Dim ds As New DataSet

            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_txtCode, str_txtName As String
            Dim str_search As String
            str_txtCode = ""
            str_txtName = ""

            str_filter_code = ""
            str_filter_name = ""

            If gvEmpInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String



                'If SearchMode = "CU" Then

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                str_txtCode = txtSearch.Text
                ''code

                If str_search = "LI" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_name = " AND a.E_Name  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = "  AND  NOT a.E_Name  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND a.E_Name   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND a.E_Name   NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND a.E_Name  LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                End If

                'End If


            End If



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name)



            gvEmpInfo.DataSource = ds.Tables(0)
            ' gvEmpInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEmpInfo.DataBind()
                Dim columnCount As Integer = gvEmpInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvEmpInfo.Rows(0).Cells.Clear()
                gvEmpInfo.Rows(0).Cells.Add(New TableCell)
                gvEmpInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvEmpInfo.HeaderRow.Visible = True
            Else
                gvEmpInfo.DataBind()


            End If



            If SearchMode = "CU" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Cost Unit ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Cost Unit Name"
                Page.Title = "Cost Unit Info"

            ElseIf SearchMode = "PP" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Account ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Account Name"
                Page.Title = "Prepaid  Account Info"


            ElseIf SearchMode = "EA" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Account ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Account Name"
                Page.Title = "Expensive Account Info"


            ElseIf SearchMode = "PA" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Account ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Account Name"
                Page.Title = "Creditor Account Info"

            End If


            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
            txtSearch.Text = str_txtCode
            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            ' Errorlog(ex.Message)
        End Try

    End Sub



    Protected Sub btnSearchEmpId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub




    Protected Sub gvEmpInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpInfo.PageIndexChanging
        gvEmpInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lblID2 As New Label
        Dim lblID3 As New Label
        Dim lbClose As New LinkButton
        Dim id2Descr As String = ""
        Dim id3Descr As String = ""
        Dim l_Str_Msg As String

        lbClose = sender
        lblcode = sender.Parent.FindControl("label1")
        lblID2 = sender.Parent.FindControl("lblID2")
        lblID3 = sender.Parent.FindControl("lblID3")
        Dim lblid As New Label
        lblid = sender.Parent.FindControl("lblID")
        If (Not lblcode Is Nothing And Not lblID2 Is Nothing And Not lblID3 Is Nothing) Then
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & CleanupStringForJavascript(lblid.Text & "||") & "';") '& lblDESCR2.Text & "||" & lblDESCR1.Text & "||" & lbClose.Text
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"
            If SearchMode = "CU" Then
                'lblID2 = e.Row.FindControl("lblID2")
                Using Userreader As SqlDataReader = AccessRoleUser.GetEmpPP(lblID2.Text, 1)
                    While Userreader.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        id2Descr = Convert.ToString(Userreader(0))
                    End While
                    'clear of the resource end using
                End Using
                'lblID3 = e.Row.FindControl("lblID3")
                Using Userreader As SqlDataReader = AccessRoleUser.GetEmpPP(lblID3.Text, 2)
                    While Userreader.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        id3Descr = Convert.ToString(Userreader(0))
                    End While
                    'clear of the resource end using
                End Using
                l_Str_Msg = lbClose.Text & "||" & lblcode.Text & "||" & id2Descr & "||" & lblID2.Text & "||" & id3Descr & "||" & lblID3.Text
            Else
                l_Str_Msg = lbClose.Text & "||" & lblcode.Text
            End If
            ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
            l_Str_Msg = l_Str_Msg.Replace("'", "\'")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & l_Str_Msg & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & l_Str_Msg & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    'Protected Sub gvEmpInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpInfo.RowDataBound
    '    Dim lblcode As New Label
    '    Dim lblID2 As New Label
    '    Dim lblID3 As New Label
    '    Dim lbClose As New LinkButton
    '    Dim id2Descr As String = ""
    '    Dim id3Descr As String = ""
    '    Dim l_Str_Msg As String


    '    'handle error in findcontrol
    '    Dim btn2 As New LinkButton
    '    'btn1 = e.Row.FindControl("LinkButton1")
    '    btn2 = e.Row.FindControl("LinkButton2")
    '    lblcode = e.Row.FindControl("label1")
    '    If Not btn2 Is Nothing Then
    '        If SearchMode = "CU" Then
    '            lblID2 = e.Row.FindControl("lblID2")
    '            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpPP(lblID2.Text, 1)
    '                While Userreader.Read
    '                    'handle the null value returned from the reader incase  convert.tostring
    '                    id2Descr = Convert.ToString(Userreader(0))
    '                End While
    '                'clear of the resource end using
    '            End Using
    '            lblID3 = e.Row.FindControl("lblID3")
    '            Using Userreader As SqlDataReader = AccessRoleUser.GetEmpPP(lblID3.Text, 2)
    '                While Userreader.Read
    '                    'handle the null value returned from the reader incase  convert.tostring
    '                    id3Descr = Convert.ToString(Userreader(0))
    '                End While
    '                'clear of the resource end using
    '            End Using
    '            l_Str_Msg = lbClose.Text & "___" & lblcode.Text & "___" & id2Descr & "___" & lblID2.Text & "___" & id3Descr & "___" & lblID3.Text
    '        Else
    '            l_Str_Msg = btn2.Text & "___" & lblcode.Text
    '        End If
    '        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
    '        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
    '        'btn1.OnClientClick = "return_value('" & l_Str_Msg & "');"
    '        btn2.OnClientClick = "return_value('" & l_Str_Msg & "');"
    '    End If

    'End Sub




End Class
