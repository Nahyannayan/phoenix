Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Accounts_acccpAddDetails
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        h_Voucherid.Value = Request.QueryString("vid")
        txtAllocate.Text = Request.QueryString("amt")
        'txtOAllocate.Text = Request.QueryString("amt")
        'POPULATE THE SUB COST CENTER ALLOCATION FROM CHILD WINDOW
        If h_Memberids.Value <> "" Then
            '' ''Add_Members()
            h_Memberids.Value = ""
            gridbind_CostChild()
        End If
        lblError.Text = ""

        If Page.IsPostBack = False Then
            ' BIND EVERYTHING
            txtDifference.Attributes.Add("ReadOnly", "ReadOnly")
            bind_groupPolicy()
            'gridbind_Others()
            initialize_components()
            'HIDE DELETE BUTTON, ADD NEW BUTTON IF VIEW MODE
            'If Request.QueryString("editid") = "" And Request.QueryString("viewid") <> "" Then
            '    setViewData()
            'Else
            '    ResetViewData()
            'End If

        End If

        'btnInsert1.Visible = False
        If ddGroupPolicy.SelectedItem.Value = Session("CostEMP") Then
            gvCostchild.Columns(7).Visible = True
            Session("EMP_ERN_CODE") = BindEarnCode(h_Voucherid.Value)
        Else
            gvCostchild.Columns(7).Visible = False
        End If

    End Sub



    Sub initialize_components()
        gvCostchild.Attributes.Add("bordercolor", "#1b80b6")
        'gvOthers.Attributes.Add("bordercolor", "#1b80b6")         
    End Sub

    

    Private Sub setViewData()
        'tr_Othersbtn.Visible = False
        '
        'btnInsert1.Visible = False

        'tbl_AllocationDetails.Visible = False
        'tr_allocationbuttons.Visible = False
        btnOk.Visible = False
        btnResetCC.Visible = False

        gvCostchild.Columns(8).Visible = False
        gvCostchild.Columns(7).Visible = False

        'gvOthers.Columns(5).Visible = False
        'gvOthers.Columns(6).Visible = False
    End Sub


    Private Sub ResetViewData()
        'btnInsert1.Visible = True
        'tr_Othersbtn.Visible = True

        btnOk.Visible = True
        btnResetCC.Visible = True
        ' tbl_AllocationDetails.Visible = False
        'tr_allocationbuttons.Visible = True

        gvCostchild.Columns(8).Visible = True
        gvCostchild.Columns(7).Visible = True

        'gvOthers.Columns(5).Visible = True
        'gvOthers.Columns(6).Visible = True
    End Sub


    Private Function getAccountname(ByVal p_accountid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M where ACT_ID='" & p_accountid & "' "

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & p_accountid & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_NAME") & "|" _
                & ds.Tables(0).Rows(0)("PLY_COSTCENTER") & "|" _
                & ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else

            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
    End Function


    Sub bind_groupPolicy()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
            & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
           

            Dim isel As Integer = 0
            Dim li As New ListItem
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                li = New ListItem()
                li.Text = ds.Tables(0).Rows(i)("CCS_DESCR")
                li.Value = ds.Tables(0).Rows(i)("CCS_ID")
                ddGroupPolicy.Items.Add(li)
                If Request.QueryString("sid") = ds.Tables(0).Rows(i)("CCS_ID") Then
                    isel = i
                End If
            Next
            'li = New ListItem()
            'li.Text = "Other Cost Center"
            'li.Value = Session("CostOTH")
            'ddGroupPolicy.Items.Add(li)
            'ddGroupPolicy.DataBind()
            ddGroupPolicy.SelectedIndex = isel 'SET SELECTED ITEM OF GROUP POLICY
            gridbind_CostChild()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cERN_ID As New DataColumn("ERN_ID", System.Type.GetType("System.String"))
            Dim cCredit As New DataColumn("Credit", System.Type.GetType("System.Decimal"))
            Dim cDebit As New DataColumn("Debit", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
            Dim cCCRequired As New DataColumn("Required", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cERN_ID)
            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCredit)
            dtDt.Columns.Add(cDebit)

            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

   

    Private Sub Add_Members() ' ADD THE IDS FROM CHILD WINDOW TO DATATABLE
        Try
            Dim str_memberid() As String
            Dim str_name As String = ""
            Dim i As Integer
            Dim bool_Repeat As Boolean
            If h_Memberids.Value <> "" Then
                str_memberid = h_Memberids.Value.Split("||")
                For i = 0 To str_memberid.Length - 1
                    If (str_memberid(i) <> "") Then
                        '''''
                        Try
                            '' '
                            '' 'find  ALREADY

                            Dim icheck As Integer
                            ''''normal.............
                            If h_mode.Value = "" Then
                                bool_Repeat = False
                                For icheck = 0 To Session("dtCostChild").Rows.Count - 1
                                    If Session("dtCostChild").Rows(icheck)("Memberid") & "" = str_memberid(i) And Session("dtCostChild").Rows(icheck)("VoucherId") = h_Voucherid.Value And Session("dtCostChild").Rows(icheck)("CostCenter") = ddGroupPolicy.SelectedItem.Value Then
                                        bool_Repeat = True
                                        Exit For
                                    End If
                                Next
                                If bool_Repeat = False Then
                                    ''''
                                    Try
                                        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                                        Dim str_Sql As String
                                        'str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M" _
                                        '& " WHERE ACT_ID='" & p_accid & "'" _
                                        '& " AND ACT_Bctrlac='FALSE'"

                                        str_Sql = get_Query() & ""
                                        str_Sql = str_Sql.Replace("###", Session("sBsuid"))
                                        str_Sql = str_Sql & " AND ID='" & str_memberid(i) & "'"
                                        '& " order by gm.GPM_DESCR "
                                        Dim ds As New DataSet
                                        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                                        If ds.Tables(0).Rows.Count > 0 Then
                                            str_name = ds.Tables(0).Rows(0)("NAME") & ""
                                        Else
                                            str_name = ""
                                        End If
                                    Catch ex As Exception
                                        Errorlog(ex.Message)
                                    End Try
                                    '' ' find
                                    Dim rDt As DataRow
                                    If str_name <> "" Then
                                        rDt = Session("dtCostChild").NewRow
                                        rDt("Id") = Session("idCostChild")
                                        Session("idCostChild") = Session("idCostChild") + 1
                                        rDt("VoucherId") = h_Voucherid.Value & ""
                                        rDt("Costcenter") = ddGroupPolicy.SelectedItem.Value
                                        rDt("Memberid") = str_memberid(i)
                                        rDt("SubMemberId") = str_memberid(i)
                                        rDt("Name") = str_name
                                        rDt("Amount") = 0
                                        rDt("GUID") = System.DBNull.Value
                                        Session("dtCostChild").Rows.Add(rDt)
                                    End If
                                End If
                            End If

                            If h_mode.Value = Session("CostOTH") Then
                                bool_Repeat = False
                                For icheck = 0 To Session("dtCostChild").Rows.Count - 1
                                    If Session("dtCostChild").Rows(icheck)("Costcenter") = str_memberid(i) And Session("dtCostChild").Rows(icheck)("VoucherId") = h_Voucherid.Value Then
                                        bool_Repeat = True
                                        Exit For
                                    End If
                                Next
                                If bool_Repeat = False Then
                                    ''''
                                    Try
                                        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                                        Dim str_Sql As String
                                        str_Sql = "SELECT * FROM" _
                                          & " (SELECT CCS_ID as ID , CCS_DESCR as NAME," _
                                          & " ' ' as DES_ID, ' ' AS BSU_ID, ' ' AS COLUMN1 ," _
                                          & " ' ' AS COLUMN2,' ' AS COLUMN3" _
                                          & " FROM COSTCENTER_S  WHERE CCS_CCT_ID <> '9999') DB where name<>''"

                                        '' ''str_Sql = get_Query() & ""
                                        '' ''str_Sql = str_Sql.Replace("###", Session("sBsuid"))
                                        str_Sql = str_Sql & " AND ID='" & str_memberid(i) & "'"
                                        '& " order by gm.GPM_DESCR "
                                        Dim ds As New DataSet
                                        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                                        If ds.Tables(0).Rows.Count > 0 Then
                                            str_name = ds.Tables(0).Rows(0)("NAME") & ""
                                        Else
                                            str_name = ""
                                        End If
                                    Catch ex As Exception
                                        Errorlog(ex.Message)
                                    End Try
                                    '' ' find
                                    Dim rDt As DataRow
                                    If str_name <> "" Then
                                        rDt = Session("dtCostChild").NewRow
                                        rDt("Id") = Session("idCostChild")
                                        Session("idCostChild") = Session("idCostChild") + 1
                                        rDt("VoucherId") = h_Voucherid.Value & ""
                                        rDt("Costcenter") = str_memberid(i)
                                        rDt("Memberid") = System.DBNull.Value
                                        rDt("SubMemberId") = System.DBNull.Value
                                        rDt("Name") = str_name
                                        rDt("Amount") = 0
                                        rDt("GUID") = System.DBNull.Value
                                        Session("dtCostChild").Rows.Add(rDt)
                                    End If
                                End If
                                '' 'find  ALREADY
                            End If

                        Catch ex As Exception
                            Errorlog(ex.Message, "Enter valid number")
                        End Try
                        '''''
                    End If
                Next

            End If
            gridbind_CostChild()
            'gridbind_Others()
        Catch ex As Exception
        Finally
            h_Memberids.Value = ""
            gridbind_CostChild()
            'gridbind_Others()
        End Try
    End Sub



    Private Sub gridbind_CostChild()
        Try
            Dim dv As New DataView(Session("dtCostChild"))
            If Session("dtCostChild").Rows.Count > 0 Then
                dv.RowFilter = "VoucherId='" & h_Voucherid.Value & "' and costcenter='" & ddGroupPolicy.SelectedItem.Value & "'"
            End If
            dv.Sort = "MemberId"
            gvCostchild.DataSource = dv
            gvCostchild.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Private Function BindEarnCode(ByVal id As Integer) As DataSet
        Dim vACT_ID, str_sql As String
        If Not Session("dtJournal") Is Nothing Then
            For i As Integer = 0 To Session("dtJournal").Rows.count - 1
                If Session("dtJournal").Rows(i)("Id") = id Then
                    vACT_ID = Session("dtJournal").Rows(i)("Accountid")
                    Exit For
                End If
            Next
            str_sql = "select ERN_ID, ERN_DESCR from EMPSALCOMPO_M WHERE ERN_ADVANCE_ACT_ID ='" & vACT_ID & "'"
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        Else
            Return Nothing
        End If
    End Function

 
    Function get_Query() As String 'GET QUERY FOR SELECTED COMBO
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT CCS_ID ,CCS_DESCR ,CCS_QUERY" _
            & " FROM COSTCENTER_S" _
            & " WHERE  CCS_CCT_ID='9999'" _
            & " AND CCS_ID='" & ddGroupPolicy.SelectedItem.Value & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("CCS_QUERY")
            Else
                Return ("0")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ("0")
        End Try
    End Function


    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click

        If Not txtDifference.Text = 0 Then
            lblError.Text = "Invalid Allocate Amount..!"
            Exit Sub
        End If
        save_amount()

    End Sub


    Private Sub save_amount()
        Try
            For i As Integer = 0 To gvCostchild.Rows.Count - 1
                Dim lblid As New Label
                Dim txtBox As New TextBox
                Dim ddlERN_Type As DropDownList
                Dim dblAmount As Double
                lblid = TryCast(gvCostchild.Rows(i).FindControl("lblId"), Label)
                ddlERN_Type = TryCast(gvCostchild.Rows(i).FindControl("ddlERN_ID"), DropDownList)
                txtBox = TryCast(gvCostchild.Rows(i).FindControl("txtAmt"), TextBox)
                For j As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                    If Session("dtCostChild").Rows(j)("id") = lblid.Text Then
                        Try
                            dblAmount = CDbl(txtBox.Text)
                            If Session("dtCostChild").Rows(j)("MemberId") <> Session("dtCostChild").Rows(j)("SubMemberId") Then
                                If dblAmount <= 0 Then
                                    dblAmount = 0
                                    lblError.Text = "Some amounts entered are less than Zero/Zero"
                                End If
                            End If
                        Catch ex As Exception
            dblAmount = 0
        End Try
                        Session("dtCostChild").Rows(j)("Amount") = dblAmount
                        Session("dtCostChild").Rows(j)("ERN_ID") = ddlERN_Type.SelectedValue
                        Exit For
                    End If
                Next
            Next
            If Not txtDifference.Text = 0 Then
                lblError.Text = "Invalid Allocate Amount..!"
                Exit Sub
            End If

            gridbind_CostChild()
            'gridbind_Others()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnResetCC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetCC.Click
        Try

            'Session("dtCostChild").Columns("Amount").DefaultValue = "0"

            'For i As Integer = 0 To gvCostchild.Rows.Count - 1
            '    Dim lblid As New Label
            '    Dim txtBox As New TextBox
            '    lblid = TryCast(gvCostchild.Rows(i).FindControl("lblId"), Label)

            '    txtBox = TryCast(gvCostchild.Rows(i).FindControl("txtAmt"), TextBox)
            '    For j As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            '        If Session("dtCostChild").Rows(j)("id") = lblid.Text And Session("dtCostChild").Rows(j)("CostCenter") = ddGroupPolicy.SelectedItem.Value Then
            '            Session("dtCostChild").Rows(j)("Amount") = "0"
            '            Exit For
            '        End If
            '    Next
            'Next

            'Dim dv As New DataView(Session("dtCostChild"))
            'dv.RowFilter = filterExp & " and  VoucherId = '" & Session("dtCostChild").Rows(iRemove)("VoucherId") & "' "
            'Session("dtCostChild") = Nothing
            'Session("dtCostChild") = dv.ToTable

            'Dim dt As New DataTable

            'gridbind_CostChild()
            'gridbind_Others()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub ddGroupPolicy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddGroupPolicy.SelectedIndexChanged
        gridbind_CostChild()
    End Sub


    Protected Sub txtAmt_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        'txtAmount.Attributes.Add("onBlur", "find_total();")
        txtAmount.Attributes.Add("onFocus", "this.select();")
    End Sub


    Protected Sub gvCostchild_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvCostchild.RowDeleting
        Try
            Dim row As GridViewRow = gvCostchild.Rows(e.RowIndex)
            Dim lblTid As New Label
            '        Dim lblGrpCode As New Label
            lblTid = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim str_Index As String = ""
            str_Index = lblTid.Text
            Dim MemberId As String = ""
            Dim IdArr(Session("dtCostChild").Rows.Count - 1) As String
            Dim x As Integer = 0
            Dim filterExp As String = ""



            For iRemove = 0 To Session("dtCostChild").Rows.Count - 1
                'Costcenter


                If str_Index = Session("dtCostChild").Rows(iRemove)("id") Then
                    'Session("dtCostChild").Rows(iRemove).Delete()
                    'Session("dtCostChild").AcceptChanges()
                    If Session("dtCostChild").Rows(iRemove)("SubMemberid") = Session("dtCostChild").Rows(iRemove)("Memberid") Then
                        filterExp = ddGroupPolicy.SelectedValue.ToString() & " = '" & Session("dtCostChild").Rows(iRemove)("Costcenter") & "' and Memberid <> '" & Session("dtCostChild").Rows(iRemove)("Memberid") & "' "
                    Else
                        filterExp = ddGroupPolicy.SelectedValue.ToString() & " = '" & Session("dtCostChild").Rows(iRemove)("Costcenter") & "' and SubMemberid <> '" & Session("dtCostChild").Rows(iRemove)("SubMemberid") & "' "
                    End If
                    Dim dv As New DataView(Session("dtCostChild"))
                    dv.RowFilter = filterExp & " and  VoucherId = '" & Session("dtCostChild").Rows(iRemove)("VoucherId") & "' "
                    Session("dtCostChild") = Nothing
                    Session("dtCostChild") = dv.ToTable

                    Exit For
                End If

            Next

            gridbind_CostChild()



        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub txtOAmt_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        'txtAmount.Attributes.Add("onBlur", "find_Ototal();")
        txtAmount.Attributes.Add("onFocus", "this.select();")
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click

        If Not txtDifference.Text = 0 Then
            lblError.Text = "Invalid Allocate Amount..!"
            Exit Sub
        End If
        save_amount()
        Response.Write("<script>window.close();</script>")
    End Sub


    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCostchild.RowDataBound
        If ddGroupPolicy.SelectedValue = Session("CostOTH") Then
            Exit Sub
        End If
        If Not e.Row.RowIndex.Equals(-1) Then
            If (e.Row.Cells(3).Text = e.Row.Cells(8).Text) Then
                Dim txtAmt As TextBox
                txtAmt = e.Row.FindControl("txtAmt")
                e.Row.Cells(5).Attributes.Add("onclick", "return getCostcentre('" & e.Row.Cells(3).Text.ToString() & "','" & e.Row.Cells(5).Text.ToString() & "')")
                e.Row.Cells(5).CssClass = "fntLink"
                e.Row.Cells(5).ToolTip = "Select to Add child costcenter"
                e.Row.Cells(5).Style("cursor") = "hand"
            End If
        End If
        If ddGroupPolicy.SelectedItem.Value = Session("CostEMP") Then 'EMP
            If Not Session("EMP_ERN_CODE") Is Nothing Then
                Dim ddlERN_ID As DropDownList = TryCast(e.Row.FindControl("ddlERN_ID"), DropDownList)
                If Not ddlERN_ID Is Nothing Then
                    ddlERN_ID.DataSource = Session("EMP_ERN_CODE")
                    ddlERN_ID.DataTextField = "ERN_DESCR"
                    ddlERN_ID.DataValueField = "ERN_ID"
                    ddlERN_ID.DataBind()
                    If Not Session("dtCostChild") Is Nothing Then
                        Dim lblID As Label = TryCast(e.Row.FindControl("lblId"), Label)
                        For Each dr As DataRow In Session("dtCostChild").Rows
                            If dr("id") = lblID.Text Then
                                ddlERN_ID.SelectedValue = CheckNullValue(dr("ERN_ID"))
                            End If
                        Next
                    End If
                End If
            End If
        End If
        If e.Row.Cells.Count > 7 Then
            e.Row.Cells(8).Style("display") = "none"
        End If
    End Sub

    Private Function CheckNullValue(ByVal obj As Object) As Object
        If obj Is Nothing Or obj Is DBNull.Value Then
            Return 0
        Else
            Return obj
        End If

    End Function


End Class
