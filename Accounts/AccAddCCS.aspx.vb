Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class AccAddCCS
    Inherits System.Web.UI.Page
    Dim lstrErrMsg As String

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "A100060" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            txtCCMCode.Attributes.Add("readonly", "readonly")
            txtCCMDescr.Attributes.Add("readonly", "readonly")
            txtCCSCode.Attributes.Add("readonly", "readonly")
            txtCCSDescr.Attributes.Add("readonly", "readonly")

            If ViewState("datamode") = "view" Then
                Dim editID As String '= Convert.ToString(Encr_decrData.Decrypt(Request.QueryString("CCS_ID").Replace(" ", "+")))
                If Request.QueryString("viewid") Is Nothing Then
                    editID = Convert.ToString(Request.QueryString("CCS_ID").Replace(" ", "+"))
                Else
                    editID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                FillValues(editID)
                EnableDissableControls(False)
            End If
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            Exit Sub
        End If
        Dim str_conn As String = ConnectionManger.GetOASISFINConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim sqlTran As SqlTransaction

        Dim lintRetVal As Integer
        'ServerValidate()
        '   --- Proceed To Save
        If (lstrErrMsg = "") Then
            objConn.Open()
            sqlTran = objConn.BeginTransaction("SaveCOSTCENTER_S")
            Try
                Dim SqlCmd As New SqlCommand("SaveCOSTCENTER_S", objConn, sqlTran)
                SqlCmd.CommandType = CommandType.StoredProcedure
                SqlCmd.Parameters.AddWithValue("@CCS_ID", Trim(txtCode.Text))
                SqlCmd.Parameters.AddWithValue("@CCS_DESCR", Trim(txtDescr.Text))
                SqlCmd.Parameters.AddWithValue("@CCS_CCT_ID", Convert.ToInt32(Val(txtCCMCode.Text)))
                SqlCmd.Parameters.AddWithValue("@CCS_PARENT_CCS_ID", Trim(txtCCSCode.Text))
                SqlCmd.Parameters.AddWithValue("@CCS_QUERY", "NA")
                Dim bEdit As Boolean = False
                If ViewState("datamode") = "edit" Then
                    bEdit = True
                End If
                SqlCmd.Parameters.AddWithValue("@bEdit", bEdit)
                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                If (lintRetVal = 0) Then
                    sqlTran.Commit()
                    lblError.Text = "Data updated Successfully"
                    ClearDetails()
                    EnableDissableControls(False)
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Else
                    sqlTran.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(lintRetVal)
                End If
            Catch ex As Exception
                sqlTran.Rollback()
                lblError.Text = ex.Message
            End Try
        End If
    End Sub

    Private Sub FillValues(ByVal editID As String)
        Try
            Dim lstrConn As String = ConnectionManger.GetOASISFINConnectionString
            Dim lstrSQL As String
            Dim ds As New DataSet
            lstrSQL = "SELECT CCS_ID, CCS_DESCR, MainParent, Parent, CCS_CCT_ID, " & _
            " CCS_PARENT_CCS_ID FROM vw_OSA_COSTCENTER_S WHERE CCS_ID='" & editID & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                txtCCMCode.Text = ds.Tables(0).Rows(0)("CCS_CCT_ID")
                If txtCCMCode.Text = 0 Then
                    txtCCMCode.Text = ""
                End If
                txtCCMDescr.Text = ds.Tables(0).Rows(0)("MainParent")
                txtCCSCode.Text = ds.Tables(0).Rows(0)("CCS_PARENT_CCS_ID")
                txtCCSDescr.Text = ds.Tables(0).Rows(0)("PARENT")
                txtCode.Text = ds.Tables(0).Rows(0)("CCS_ID")
                txtDescr.Text = ds.Tables(0).Rows(0)("CCS_DESCR")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ClearDetails()
        txtCCMCode.Text = ""
        txtCCMDescr.Text = ""
        txtCCSCode.Text = ""
        txtCCSDescr.Text = ""
        txtCode.Text = ""
        txtDescr.Text = ""
    End Sub

    Private Sub EnableDissableControls(ByVal bEnable As Boolean)
        txtCCMCode.ReadOnly = Not bEnable
        txtCCMDescr.ReadOnly = Not bEnable
        txtCCSCode.ReadOnly = Not bEnable
        txtCCSDescr.ReadOnly = Not bEnable
        txtCode.ReadOnly = Not bEnable
        txtDescr.ReadOnly = Not bEnable
        ImageButton1.Enabled = bEnable
        ImageButton2.Enabled = bEnable
    End Sub

    Protected Sub ServerValidate()
        lstrErrMsg = ""

        If ((Trim(txtCode.Text) = "") Or (Master.StringVerify(txtCode.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "Cost Center Code Should Be A String Value" & "<br>"
        End If

        If ((Trim(txtDescr.Text) = "") Or (Master.StringWithSpace(txtDescr.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "Cost Center Description Should Be A String Value" & "<br>"
        End If




        lblError.Text = lstrErrMsg
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        ClearDetails()
        EnableDissableControls(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        EnableDissableControls(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If


    End Sub
End Class
