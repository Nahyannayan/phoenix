Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class AccEditChqBook
    Inherits System.Web.UI.Page
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim MainMnu_code As String = String.Empty
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (MainMnu_code <> "A100040") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling page right class to get the access rights
                    FillChqformat()
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtControlAcc.Attributes.Add("readonly", "readonly")
                    txtControlAccDescr.Attributes.Add("readonly", "readonly")
                    txtLotid.Attributes.Add("readonly", "readonly")
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("Viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        FillValues()

                    End If

                End If
                UtilityObj.beforeLoopingControls(Me.Page)

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL As String
            Dim ds As New DataSet
            '   Dim i As Integer


            lstrSQL = "SELECT * FROM vw_OSA_CHQBOOK_M WHERE CHB_ID='" & ViewState("Viewid") & "' "
           
            lstrSQL = "SELECT CM.CHB_ID, " _
                & " CM.CHB_ACT_ID, CM.CHB_BSU_ID," _
                & " CM.CHB_LOTNO, CM.CHB_PREFIX, " _
                & " CM.CHB_FROM, CM.CHB_TO, " _
                & " CM.CHB_NEXTNO, CM.CHB_PREV_CHB_ID, " _
                & " AM.ACT_NAME,ISNULL(CM.CHQ_ID,0)CHQ_ID, " _
                & " CM_LOT.CHB_LOTNO AS PREV_LOT_NO" _
                & " FROM CHQBOOK_M AS CM INNER JOIN" _
                & " ACCOUNTS_M AS AM" _
                & " ON CM.CHB_ACT_ID = AM.ACT_ID " _
                & " LEFT OUTER JOIN " _
                & " CHQBOOK_M AS CM_LOT " _
                & " ON CM.CHB_PREV_CHB_ID = CM_LOT.CHB_ID" _
                & " WHERE CM.CHB_ID='" & ViewState("Viewid") & "'"
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)


            If ds.Tables(0).Rows.Count > 0 Then
                txtControlAcc.Text = ds.Tables(0).Rows(0)("CHB_ACT_ID")
                txtControlAccDescr.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                txtLotNo.Text = ds.Tables(0).Rows(0)("CHB_LOTNO")
                txtPrefix.Text = ds.Tables(0).Rows(0)("CHB_PREFIX")
                txtFrom.Text = ds.Tables(0).Rows(0)("CHB_FROM")
                txtTo.Text = ds.Tables(0).Rows(0)("CHB_TO")
                txtNext.Text = ds.Tables(0).Rows(0)("CHB_NEXTNO")
                txtLotid.Text = ds.Tables(0).Rows(0)("PREV_LOT_NO") & ""
                h_Chbid.Value = ds.Tables(0).Rows(0)("CHB_PREV_CHB_ID") & ""
                'If Not ds.Tables(0).Rows(0)("CHQ_ID").Equals(0) Then
                drpChequeFormat.SelectedValue = ds.Tables(0).Rows(0)("CHQ_ID")
                'End If
            End If
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub FillChqformat()
        Dim str_sql As String = "SELECT CHQ_ID,CHQ_DESCR FROM CHQ_M ORDER BY CHQ_DESCR "
        Dim _dataSet As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
        drpChequeFormat.DataSource = _dataSet.Tables(0)
        drpChequeFormat.DataTextField = "CHQ_DESCR"
        drpChequeFormat.DataValueField = "CHQ_ID"
        drpChequeFormat.DataBind()
        drpChequeFormat.Items.Insert(0, " ")
        drpChequeFormat.Items(0).Value = "0"
        drpChequeFormat.SelectedValue = "0"
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim lintRetVal As Integer
        ServerValidate()

        '   --- Proceed To Save
        If (lstrErrMsg = "") Then
            If ViewState("datamode") = "edit" Or ViewState("datamode") = "view" Then
                Dim i_prev_chbid As Integer
                Try
                    Try
                        i_prev_chbid = CInt(h_Chbid.Value)
                    Catch ex As Exception
                        i_prev_chbid = 0
                    End Try
                    objConn.Open()

                    Dim SqlCmd As New SqlCommand("SaveCHQBOOK_M", objConn)
                    SqlCmd.CommandType = CommandType.StoredProcedure
                    SqlCmd.Parameters.AddWithValue("@CHB_ID", ViewState("Viewid"))
                    SqlCmd.Parameters.AddWithValue("@CHB_ACT_ID", txtControlAcc.Text)
                    SqlCmd.Parameters.AddWithValue("@CHB_BSU_ID", Session("sBsuId"))
                    SqlCmd.Parameters.AddWithValue("@CHB_LOTNO", Trim(txtLotNo.Text))
                    SqlCmd.Parameters.AddWithValue("@CHB_PREFIX", Trim(txtPrefix.Text))
                    SqlCmd.Parameters.AddWithValue("@CHB_FROM", Convert.ToInt32(Trim(txtFrom.Text)))
                    SqlCmd.Parameters.AddWithValue("@CHB_TO", Convert.ToInt32(Trim(txtTo.Text)))
                    SqlCmd.Parameters.AddWithValue("@CHB_NEXTNO", txtNext.Text)
                    SqlCmd.Parameters.AddWithValue("@bEdit", True)
                    If i_prev_chbid > 0 And txtLotid.Text <> "" Then
                        SqlCmd.Parameters.AddWithValue("@CHB_PREV_CHB_ID", i_prev_chbid)
                    End If
                    SqlCmd.Parameters.AddWithValue("@CHQ_ID", Int32.Parse(drpChequeFormat.SelectedValue))
                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                    '  Response.Redirect("AccMstChqBook.aspx")
                    If lintRetVal = 0 Then



                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("Viewid"), "Edit", Page.User.Identity.Name.ToString, Me.Page)

                        If flagAudit <> 0 Then

                            Throw New ArgumentException("Could not process your request")

                        End If
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Call clearall()
                        lblErr.Text = "Data Successfully Saved..."
                    Else
                        lblErr.Text = getErrorMessage(lintRetVal)
                    End If

                  

                Catch myex As ArgumentException

                    lblErr.Text = myex.Message
                Catch ex As Exception

                    lblErr.Text = UtilityObj.getErrorMessage("1000")
                    UtilityObj.Errorlog(ex.Message)
                End Try

            End If

        End If
    End Sub
    Protected Sub ServerValidate()
        lstrErrMsg = ""
        If (IsNumeric(Trim(txtLotNo.Text)) = False) Then
            lstrErrMsg = lstrErrMsg & "Lot No Should Be A Numeric Value" & "<br>"
        End If


        If ((Trim(txtPrefix.Text) = "") Or (Master.StringVerify(txtPrefix.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "Prefix Should Be A String Value" & "<br>"
        End If

        If ((Trim(txtFrom.Text) = "") Or (Master.StringVerify(txtFrom.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "From # Should Be A Numeric Value" & "<br>"
        End If

        If ((Trim(txtTo.Text) = "") Or (Master.StringVerify(txtTo.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "To # Should Be A Numeric Value" & "<br>"
        End If

        If ((Trim(drpChequeFormat.SelectedValue.ToString()) = "") Or Trim(drpChequeFormat.SelectedValue.ToString().Equals("0"))) Then
            lstrErrMsg = lstrErrMsg & "Invalid Cheque Format.." & "<br>"
        End If
        lblErr.Text = lstrErrMsg
    End Sub
    Public Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("SAVED SUCCESSFULLY...")
            End If
        Catch ex As Exception

            Return ("0")
        End Try
    End Function
    Sub clearall()
        txtControlAcc.Text = ""
        txtControlAccDescr.Text = ""
        txtFrom.Text = ""
        txtLotNo.Text = ""
        txtNext.Text = ""
        txtPrefix.Text = ""
        txtTo.Text = ""

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear the textbox and set the default settings
            Call clearall()
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If



    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        'set the rights on the button control based on the user
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        Dim Eid As String


        If txtControlAcc.Text.Trim = "" Or txtLotNo.Text.Trim = "" Then

            lblErr.Text = "Record do not exits"

        Else

            'Delete  the  user


            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Eid = ViewState("Viewid")
                    Status = AccessRoleUser.DeleteChkBook(Eid, transaction)


                    If Status = -1 Then
                        Throw New ArgumentException("Record do not exits")
                    ElseIf Status <> 0 Then
                        Throw New ArgumentException("Record can not be Deleted")
                    Else
                        Status = UtilityObj.operOnAudiTable(Master.MenuName, Eid, "delete", Page.User.Identity.Name.ToString, Me.Page)


                        If Status <> 0 Then


                            Throw New ArgumentException("Could not complete your request")

                        End If
                        ViewState("datamode") = "none"

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        transaction.Commit()
                        Call clearall()
                        lblErr.Text = "Record Deleted Successfully"

                    End If

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblErr.Text = myex.Message
                Catch ex As Exception
                    transaction.Rollback()
                    lblErr.Text = "Record could not be Deleted"
                End Try
            End Using


        End If




    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click


        Try
            Dim url As String
            ViewState("datamode") = "add"
            Dim MainMnu_code As String
            MainMnu_code = Request.QueryString("MainMnu_code")

            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))


            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~/accounts/AccAddChqBook.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))

            Response.Redirect(url)
        Catch ex As Exception
            '  lblErrorMessage.Text = "Request could not be processed "
        End Try


    End Sub
End Class
