<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accFreezePeriod.aspx.vb" Inherits="Accounts_accFreezePeriod" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrCheckList.ascx" TagName="usrCheckList" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
   
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHeader" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 100%;">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Business Unit</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Current Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server" Enabled="False"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtNewFreezeDate" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewFreezeDate"
                                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator
                                            ID="revFromdate" runat="server" ControlToValidate="txtNewFreezeDate" Display="Dynamic"
                                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr runat="server" id="trCheckList">
                                    <td colspan="4" style="text-align: left">
                                        <uc1:usrCheckList ID="UsrCheckList1" runat="server" ProcessType="MONTHEND" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtNewFreezeDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" TargetControlID="txtNewFreezeDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

