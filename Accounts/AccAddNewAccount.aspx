<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccAddNewAccount.aspx.vb" Inherits="AccAddNewAccount" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function ShowRPTSETUP_S(id, tid, hid, tid2) {

            document.getElementById('<%=hid_id.ClientID %>').value = id;
            document.getElementById('<%=hid_tid.ClientID %>').value = tid;
            document.getElementById('<%=hid_hid.ClientID %>').value = hid;
            document.getElementById('<%=hid_tid2.ClientID %>').value = tid2;
            var NameandCode;
            var result;
            result = radopen("ShowRPTSETUP_S.aspx?rss_id=" + id, "pop_up");
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                var id = document.getElementById('<%=hid_id.ClientID %>').value;
                var tid = document.getElementById('<%=hid_tid.ClientID %>').value;
                var hid = document.getElementById('<%=hid_hid.ClientID %>').value;
                var tid2 = document.getElementById('<%=hid_tid2.ClientID %>').value;

                document.getElementById(tid).value = NameandCode[1];
                document.getElementById(hid).value = NameandCode[0] + '|' + NameandCode[2];
                if (tid2 != '')
                    if (NameandCode[2] != '')//hfDescription1
                    {
                        document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.display = '';
                        document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.visibility = '';
                        document.getElementById(tid2).style.display = '';
                    }
                    else {
                        document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.display = 'none';
                        //document.getElementById(tid.replace(/txtCode/,'lbSet1') ).style.visibility='' ;txtDesc1
                        document.getElementById(tid2).style.display = 'none';
                    }
            }
        }


    </script>


    <style>
        table td input[type=text], table td select, table td input[type=text]:disabled, table td select:disabled {
            min-width: 20% !important;
        }

            table td input[type=text]:disabled {
                width: 75% !important;
            }

        .popupControl {
            background-color: #fff;
            max-height: 350px;
            overflow: hidden;
            overflow-x: scroll;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function ChangeAllCheckBoxStates() {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("Checkbox1").checked;
            ChangeCheckBoxState(lstrChk);

        }
        function ChangeCheckBoxState(checkState) {
            var tableBody = document.getElementById('<%=chkBusUnit.ClientID%>').rows;
            //alert(tableBody[0]);            
            for (var i = 0; i < tableBody.length; i++) {
                //alert(tableBody[i].cells[0].innerText);
                var checkBoxes = tableBody[i].getElementsByTagName("INPUT");
                //alert(checkBoxes);
                for (var k = 0; k < checkBoxes.length; k++) {
                    //if (checkBoxes[k].checked) {
                    //    var row = checkBoxes[k].parentNode.parentNode;
                    //    alert(row.cells[0].innerHTML);
                    //}
                    checkBoxes[k].checked = checkState;
                }

            }

        }
        <%--function ChangeCheckBoxState(checkState) {
            var tableBody = document.getElementById('<%=chkBusUnit.ClientID %>').childNodes[0];
            //alert('called me')
            for (var i = 0; i < tableBody.childNodes.length; i++) {
                var currentTd = tableBody.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = checkState;



                // if ( listControl.checked == true )
                //alert('#' + i + ': is checked');
            }
        }--%>

        function changeValue() {
            var DNR = document.getElementById('<%= optControlAccYes.ClientID %>').checked;
            if (DNR == false) {
                styleObj = document.getElementById(21).style;
                styleObj.display = 'none';

                styleObj = document.getElementById(22).style;
                styleObj.display = '';

                document.getElementById('<%= txtAccCode2.ClientID %>').readOnly = true;
                document.getElementById('<%= txtAccCode2.ClientID %>').value = "";
                document.getElementById('<%= txtSGroup.ClientID %>').value = "";
                document.getElementById('<%= txtSGroupDescr.ClientID %>').value = "";

                document.getElementById('<%= txtControlAcc.ClientID %>').value = "";
                document.getElementById('<%= txtControlAccDescr.ClientID %>').value = "";

                document.getElementById('<%= txtAccCode.ClientID %>').value = "";
                document.getElementById('<%= txtAccDescr.ClientID %>').value = "";
            }
            else {
                styleObj = document.getElementById(21).style;
                styleObj.display = '';

                styleObj = document.getElementById(22).style;
                styleObj.display = 'none';

                document.getElementById('<%= txtAccCode2.ClientID %>').value = "";
                document.getElementById('<%= txtAccCode2.ClientID %>').readOnly = false;
                document.getElementById('<%= txtSGroup.ClientID %>').value = "";
                document.getElementById('<%= txtSGroupDescr.ClientID %>').value = "";

                document.getElementById('<%= txtControlAcc.ClientID %>').value = "";
                document.getElementById('<%= txtControlAccDescr.ClientID %>').value = "";

                document.getElementById('<%= txtAccCode.ClientID %>').value = "";
                document.getElementById('<%= txtAccDescr.ClientID %>').value = "";

            }
        }
        function GetSubValues(id, tid, hid, tid2) {
            document.getElementById('<%=hid_id.ClientID %>').value = id;
            document.getElementById('<%=hid_tid.ClientID %>').value = tid;
            document.getElementById('<%=hid_hid.ClientID %>').value = hid;
            document.getElementById('<%=hid_tid2.ClientID %>').value = tid2;
            //alert(hid);
            var NameandCode;
            var result;
            var RptCode;
            var rsscode = document.getElementById(hid).value;
            //alert(rsscode);
            RptCode = id + '|' + rsscode;
            result = radopen("ShowRptSetUpSub.aspx?rss_code=" + RptCode, "pop_up2");

            //document.getElementById(tid).value = NameandCode[0];

        }
        function OnClientClose2(oWnd, args) {
            var id = document.getElementById('<%=hid_id.ClientID %>').value;
            var tid = document.getElementById('<%=hid_tid.ClientID %>').value;
            var hid = document.getElementById('<%=hid_hid.ClientID %>').value;
            var tid2 = document.getElementById('<%=hid_tid2.ClientID %>').value;
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(tid).value = NameandCode[0];

                <%--__doPostBack('<%= txtempname.ClientID %>', 'TextChanged');--%>
            }
        }
        // ============================== VALIDATE ===================================================
        function trim(sString) {
            while (sString.substring(0, 1) == ' ') {
                sString = sString.substring(1, sString.length);
            }
            while (sString.substring(sString.length - 1, sString.length) == ' ') {
                sString = sString.substring(0, sString.length - 1);
            }
            return sString;
        }


        function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

            var NameandCode;
            var url;
            document.getElementById('<%=hpMode.ClientID %>').value = pMode;
            document.getElementById('<%=hf_ctrl4.ClientID %>').value = ctrl4;
            document.getElementById('<%=hf_ctrl1.ClientID %>').value = ctrl1;
            document.getElementById('<%=hf_ctrl.ClientID %>').value = ctrl;
            document.getElementById('<%=hf_ctrl2.ClientID %>').value = ctrl2;
            document.getElementById('<%=hf_ctrl5.ClientID %>').value = ctrl5;
            document.getElementById('<%=hf_acctype.ClientID %>').value = acctype;
            if (pMode == 'SUBGRP') {

                result = radopen("PopUp.aspx?ShowType=" + pMode + "", "pop_up3");
                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];
                //document.getElementById(ctrl2).value = lstrVal[0];
            }

            else if (pMode == 'CONTROLACC') {
                var ddsel = '';
                var selObj = document.getElementById(acctype);
                ddsel = selObj.options[selObj.selectedIndex].value;
                result = radopen("ShowControl.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value + "&acctype=" + ddsel, "pop_up3");

                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];
                //document.getElementById(ctrl1).value = lstrVal[1];
                //document.getElementById(ctrl4).value = lstrVal[2];
                //if (document.getElementById(ctrl).value == '06101001')
                //    //{ document.getElementById(ctrl4).value='063'; }
                //    //document.getElementById(ctrl5).value=lstrVal[0].substring(3,lstrVal[0].length); 
                //    document.getElementById(ctrl5).value = lstrVal[4];

                //var L = selObj.options.length;
                //for (var i = 0; i <= L - 1; i++) {
                //    if (lstrVal[5] == selObj.options[i].value) { selObj.options.selectedIndex = i; }

                //}
            }
        }

        function OnClientClose3(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            var pMode = document.getElementById('<%=hpMode.ClientID%>').value;
            var ctrl4 = document.getElementById('<%=hf_ctrl4.ClientID%>').value;
            var ctrl1 = document.getElementById('<%=hf_ctrl1.ClientID%>').value;
            var ctrl = document.getElementById('<%=hf_ctrl.ClientID %>').value;
            var ctrl2 = document.getElementById('<%=hf_ctrl2.ClientID%>').value;
            var ctrl5 = document.getElementById('<%=hf_ctrl5.ClientID%>').value;
            var acctype = document.getElementById('<%=hf_acctype.ClientID%>').value;

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (pMode == 'SUBGRP') {
                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                    document.getElementById(ctrl2).value = NameandCode[0];
                }
                else if (pMode == 'CONTROLACC') {

                    document.getElementById(ctrl).value = NameandCode[0];
                    document.getElementById(ctrl1).value = NameandCode[1];
                    document.getElementById(ctrl4).value = NameandCode[2];
                    if (document.getElementById(ctrl).value == '06101001')
                        //{ document.getElementById(ctrl4).value='063'; }
                        //document.getElementById(ctrl5).value=lstrVal[0].substring(3,lstrVal[0].length); 
                        document.getElementById(ctrl5).value = NameandCode[4];
                    var selObj = document.getElementById(acctype);
                    var L = selObj.options.length;
                    for (var i = 0; i <= L - 1; i++) {
                        if (NameandCode[5] == selObj.options[i].value) { selObj.options.selectedIndex = i; }

                    }
                }

            }
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Accounts Main
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <input id="hf_ctrl4" type="hidden" runat="server" value="" />
                <input id="hf_ctrl1" type="hidden" runat="server" value="" />
                <input id="hf_ctrl" type="hidden" runat="server" value="" />
                <input id="hf_ctrl2" type="hidden" runat="server" value="" />
                <input id="hf_ctrl5" type="hidden" runat="server" value="" />
                <input id="hf_acctype" type="hidden" runat="server" value="" />
                <asp:HiddenField ID="hpMode" runat="server" />

                <table border="0" width="100%" align="center">
                    <tr>
                        <td>
                            <div align="left" class="tabber" id="tab2">
                                <table width="100%" align="center" border="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblErr" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                                            <asp:HiddenField ID="hid_id" runat="server" />
                                            <asp:HiddenField ID="hid_tid" runat="server" />
                                            <asp:HiddenField ID="hid_hid" runat="server" />
                                            <asp:HiddenField ID="hid_tid2" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellpadding="5" align="center">
                                    <tr class="title-bg">
                                        <td colspan="4">Accounts Main
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Control Account</span>
                                        </td>
                                        <td width="30%">
                                            <asp:RadioButton ID="optControlAccYes" runat="server" Style="left: -6px; position: relative; top: 0px"
                                                GroupName="optControl" Text="Yes" AutoPostBack="True" />
                                            <asp:RadioButton ID="optControlAccNo" runat="server" Style="left: 16px; position: relative; top: 0px"
                                                GroupName="optControl" Text="No" Checked="True" AutoPostBack="True" />
                                        </td>
                                        <td width="20%"><span class="field-label">Active</span>
                                        </td>
                                        <td width="30%">
                                            <asp:CheckBox ID="chkActive" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Customer/Supplier</span>
                                        </td>
                                        <td width="30%">
                                            <asp:RadioButton ID="optNormal" runat="server" Checked="True" GroupName="optCust"
                                                Text="Normal" AutoPostBack="True" />
                                            <asp:RadioButton ID="optCustomer" runat="server" GroupName="optCust" Text="Customer"
                                                AutoPostBack="True" />
                                            <asp:RadioButton ID="optSupplier" runat="server" GroupName="optCust" Text="Supplier"
                                                AutoPostBack="True" />
                                        </td>
                                        <td width="20%"><span class="field-label">Type <span class="text-danger"> * </span></span>
                                        </td>
                                        <td width="30%">
                                            <asp:DropDownList ID="cmbType" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="tr_SubGroup" runat="server">
                                        <td width="20%"><span class="field-label">Sub Group</span>
                                        </td>
                                        <td width="30%">
                                            <asp:TextBox ID="txtSGroup" runat="server" OnTextChanged="txtSGroup_TextChanged">
                                            </asp:TextBox>
                                            <a href="#" onclick="popUp('460','400','SUBGRP','<%=txtSGroup.ClientId %>','<%=txtSGroupDescr.ClientId %>','<%=txtAccCode.ClientId %>')">
                                                <img border="0" src="../Images/cal.gif" id="IMG1" language="javascript" /></a>
                                            <asp:TextBox ID="txtSGroupDescr" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr id="tr_ControlAccount" runat="server">
                                        <td width="20%"><span class="field-label">Control Account <span class="text-danger">* </span></span> 
                                        </td>
                                        <td colspan="3">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="40%">
                                                        <asp:TextBox ID="txtControlAcc" runat="server" OnTextChanged="txtControlAcc_TextChanged" Width="80%">
                                                        </asp:TextBox>
                                                        <a href="#" onclick="popUp('760','400','CONTROLACC','<%=txtControlAcc.ClientId %>','<%=txtControlAccDescr.ClientId %>','<%=txtSGroup.ClientId %>','<%=txtSGroupDescr.ClientId %>','<%=txtAccCode.ClientId %>','<%=txtAccCode2.ClientId %>','<%=cmbType.ClientId %>'); return false;">
                                                            <img border="0" src="../Images/cal.gif" id="IMG2" alt="Search" /></a>
                                                    </td>
                                                    <td align="left" width="60%">
                                                        <asp:TextBox ID="txtControlAccDescr" runat="server" OnTextChanged="txtControlAccDescr_TextChanged" Width="85%">
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Account Code &amp; Description <span class="text-danger">* </span></span>
                                        </td>
                                        <td colspan="3">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="100%">
                                                        <asp:TextBox ID="txtAccCode" runat="server" MaxLength="4" Width="10%">
                                                        </asp:TextBox>
                                                        <asp:TextBox ID="txtAccCode2" runat="server" MaxLength="4" Width="10%">
                                                        </asp:TextBox>
                                                        <asp:TextBox ID="txtAccDescr" runat="server" Width="51%" AutoPostBack="True">
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="100%">
                                                        <asp:LinkButton ID="lnkAccounts" runat="server" OnClientClick="return false;">View Similiar Accounts</asp:LinkButton>
                                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" CommitProperty="value"
                                                            CommitScript="" PopupControlID="Panel1" Position="Center" TargetControlID="lnkAccounts">
                                                        </ajaxToolkit:PopupControlExtender>
                                                        <asp:Panel ID="Panel1" runat="server" CssClass="popupControl" ScrollBars="Vertical">
                                                            <asp:UpdatePanel runat="server" ID="up1">
                                                                <ContentTemplate>
                                                                    <left></left>
                                                                    <asp:GridView ID="gvAccountList" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="table table-bordered table-row"
                                                                        GridLines="None">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="ACCOUNT" HeaderText="Acc # - Account Name" />
                                                                        </Columns>
                                                                        <FooterStyle Font-Bold="True" />
                                                                        <RowStyle />
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle Font-Bold="True" />
                                                                        <HeaderStyle Font-Bold="True" />
                                                                        <AlternatingRowStyle />
                                                                    </asp:GridView>
                                                                    <asp:ObjectDataSource ID="odsAccountNames" runat="server" SelectMethod="GetAccountNameList"
                                                                        TypeName="AccountFunctions">
                                                                        <SelectParameters>
                                                                            <asp:ControlParameter ControlID="txtAccDescr" Name="ACT_NAME" PropertyName="Text"
                                                                                Type="String" />
                                                                        </SelectParameters>
                                                                    </asp:ObjectDataSource>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20% "><span class="field-label">Currency</span>
                                        </td>
                                        <td width="30%">
                                            <asp:DropDownList ID="cmbCurrency" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td width="20%"><span class="field-label">Bank / Cash</span>
                                        </td>
                                        <td width="30%">
                                            <asp:DropDownList ID="cmbBankCash" runat="server" Style="left: -2px; position: relative; top: 0px"
                                                AutoPostBack="True">
                                                <asp:ListItem Value="N">NORMAL</asp:ListItem>
                                                <asp:ListItem Value="B">BANK</asp:ListItem>
                                                <asp:ListItem Value="C">CASH</asp:ListItem>
                                                <asp:ListItem Value="CC">COLLECTION ACCT</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Bank Account #</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtBankAccount" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            <asp:Label ID="Label2" runat="server" Text="Facility Ceiling" CssClass="field-label"></asp:Label>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtFaciCelng" runat="server">
                                            </asp:TextBox>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr id="tr_Sales" runat="server">
                                        <td width="20%">
                                            <asp:Label ID="lblSales" runat="server" Text="Sales Tax # #" CssClass="field-label"></asp:Label>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtSalesTax" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            <asp:Label ID="lblPayment" runat="server" Text="Payment Terms" CssClass="field-label"></asp:Label>
                                        </td>

                                        <td width="30%">
                                            <asp:DropDownList ID="cmbPymntTrm" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trCreditDays" runat="server">
                                        <td width="20%"><span class="field-label">Credit Days <span class="text-danger">*</span></span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtCreditDays" runat="server" Width="8%">
                                            </asp:TextBox>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Policy Group </span>

                                        </td>

                                        <td width="30%">
                                            <asp:DropDownList ID="cmbPolicyGrp" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Report Linking <span class="text-danger">* </span></span> 
                                        </td>

                                        <td>
                                            <asp:GridView ID="gvRPTSETUP_M" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField SortExpression="RSM_TYP" Visible="False" HeaderText="RSM_TYP">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRSM_TYP" runat="server" Text='<%# Bind("RSM_TYP") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="RSM_DESCR" SortExpression="RSM_DESCR" HeaderText="Description"></asp:BoundField>
                                                    <asp:BoundField DataField="TYP" Visible="False" SortExpression="TYP" HeaderText="TYP"></asp:BoundField>
                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:TextBox Style="display: none" ID="txtCode" runat="server" Width="153px"></asp:TextBox>
                                                            <asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" runat="server" Text="Set"
                                                                CausesValidation="False" OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;RSM_TYP&quot;) & &quot;');return false;&quot; %>"
                                                                OnPreRender="LinkButton1_PreRender"></asp:LinkButton>
                                                            <asp:HiddenField ID="hfDescription" runat="server"></asp:HiddenField>
                                                            <asp:TextBox Style="display: none" ID="txtCode1" runat="server" Width="156px"></asp:TextBox>
                                                            <asp:LinkButton ID="lbSet1" runat="server" Text="Set (Opposite)" CausesValidation="False"
                                                                OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;RSM_TYP&quot;) & &quot;');return false;&quot; %>"
                                                                OnPreRender="LinkButton1_PreRender"></asp:LinkButton>
                                                            <asp:HiddenField ID="hfDescription1" runat="server"></asp:HiddenField>
                                                            <asp:TextBox ID="txtCode2" runat="server" Width="131px"></asp:TextBox>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="<%# &quot;GetSubValues('&quot; & Container.DataItem(&quot;RSM_TYP&quot;) & &quot;');return false;&quot; %>"
                                                                OnPreRender="LinkButton2_PreRender">SubAccount</asp:LinkButton><br />
                                                            <asp:HiddenField ID="hfDescription2" runat="server"></asp:HiddenField>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="griditem" />
                                                <SelectedRowStyle CssClass="griditem_hilight" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>

                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Linked Report details</span>
                                        </td>

                                        <td align="left" valign="top">
                                            <asp:GridView ID="gvRptDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("RSM_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="False" HeaderText="CODE">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCode" runat="server" Text='<%# Bind("RSS_CODE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Account Details">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("RSS_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="griditem" />
                                                <SelectedRowStyle CssClass="griditem_hilight" />
                                                <HeaderStyle CssClass="gridheader_new" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tabbertab ">

                                <table width="100%" align="center">

                                    <tr class="title-bg">
                                        <td colspan="4">Communication Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Contact Person</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtContact" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td width="20%"><span class="field-label">Designation</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtDesig" runat="server">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Address1</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtAddress1" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td width="20%"><span class="field-label">Address2</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtAddress2" runat="server">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">City</span>
                                        </td>

                                        <td width="30%">
                                            <asp:DropDownList ID="cmbCity" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td width="20%"><span class="field-label">Country</span>
                                        </td>

                                        <td width="30%">
                                            <asp:DropDownList ID="cmbCountry" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Telephone</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtPhone" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td width="20%"><span class="field-label">Fax</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtFax" runat="server">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Mobile</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtMobile" runat="server">
                                            </asp:TextBox>
                                        </td>
                                        <td width="20%"><span class="field-label">Email</span>
                                        </td>

                                        <td width="30%">
                                            <asp:TextBox ID="txtEmail" runat="server">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tabbertab ">
                                <table width="100%" align="center">
                                    <tr>
                                        <td colspan="4" class="title-bg">Business Units
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="chkAL" value="Check All" onclick="ChangeAllCheckBoxStates();"
                                                id="Checkbox1" checked="checked" />
                                            Select All
                                                <div align="center">
                                                    <asp:Panel ID="plChkBUnit" runat="server"
                                                        HorizontalAlign="Left"
                                                        Width="100%">
                                                        <div class="checkbox-list">
                                                            <asp:CheckBoxList ID="chkBusUnit" runat="server">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <table width="100%" align="center">
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>

            </div>
        </div>

    </div>
</asp:Content>
