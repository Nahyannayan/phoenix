<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowOnlineSettlement.aspx.vb" Inherits="Accounts_ShowOnlineSettlement" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Accounts</title>


    <base target="_self" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <%--<style type="text/css" > 
 .odd{background-color: white;} 
 .even{background-color: gray;} 
</style>--%>
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>

</head>
<body onload="listen_window();" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }
        function Changet(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.value = '1';
        }
        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("Checkbox1").checked;
            if (CheckboxSelect != null) {
                for (var i = 0; i < CheckboxSelect.length; i++) {
                    ChangeCheckBoxState(CheckboxSelect[i], lstrChk);

                    Changet(TextBoxDeposit[i], lstrChk); Changet(TextBoxPayment[i], lstrChk);
                }
            }
        }

        function ChangeTotal(checkState) {

            var lstrChk = document.getElementById("Checkbox1").checked;
            if (CheckboxSelect != null) {
                for (var i = 0; i < CheckboxSelect.length; i++)
                    //TextBoxPayment
                    //TextBoxDeposit  txtSAlloted


                    ChangeCheckBoxState(CheckboxSelect[i], lstrChk);
            }
        }
        function OnChange() {
            try {
                if (TextBoxAllocated != null && TextBoxAllocated != undefined) {
                    var sum = 0.0;
                    for (var i = 0; i < TextBoxAllocated.length; i++) {
                        TB = document.getElementById(TextBoxAllocated[i]);
                        if (TB != null) { //alert('1');
                            if (TB.value != '') {
                                temp = 0.0;
                                temp = sum + parseFloat(document.getElementById(TextBoxAllocated[i]).value);
                                //alert(temp);
                                if (isNaN(temp))
                                    document.getElementById(TextBoxAllocated[i]).value = '0';
                                else
                                    sum = sum + parseFloat(document.getElementById(TextBoxAllocated[i]).value);
                            }
                            else {
                                document.getElementById(TextBoxAllocated[i]).value = '0';
                            }
                        } //if                 

                    }//for txtBalance
                    document.getElementById('<%=txtSAlloted.ClientID %>').value = sum;
                }
            }//try
            catch (ex)
            { }
        }
        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
            var amt;
            amt = parseFloat(roundOff)
            if (isNaN(amt))
                amt = 2;
            return amt;
        }
        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(getRoundOff());
            return true;
        }


        function SetAmount(sender, amount) {
            if (CheckAmount(sender))
                if (sender.value == 0) {
                    sender.value = amount;
                    sender.select();
                }
                else

                    sender.select();
        }

    </script>
    <form id="form1" runat="server">
        <table align="center" width="100%">
            <tr class="title-bg">
                <td align="left" colspan="4" valign="middle">Settlement of Creditors </td>
            </tr>
            <tr>
                <td align="left" colspan="4"><span class="field-label">Receipts/Invoices</span></td>
            </tr>
            <tr>
                <td align="center" colspan="4" width="100%">
                    <asp:GridView ID="gvSettle" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                        EmptyDataText="There is no transactions" Width="100%" DataKeyNames="TRN_BSU_ID,TRN_FYEAR,TRN_DOCTYPE,TRN_DOCNO,TRN_LINEID,TRN_ACT_ID">
                        <EmptyDataRowStyle Wrap="True" />
                        <Columns>
                            <asp:BoundField DataField="TRN_DOCNO" HeaderText="Docno" ReadOnly="True" SortExpression="TRN_DOCNO" />
                            <asp:BoundField DataField="TRN_DOCDT" HeaderText="Date" SortExpression="TRN_DOCDT" HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TRN_CUR_ID" HeaderText="Currency" SortExpression="TRN_CUR_ID">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TRN_AMOUNT" HeaderText="Amount" SortExpression="TRN_AMOUNT">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TRN_ALLOCAMT" HeaderText="Allocated" SortExpression="TRN_ALLOCAMT">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />

                            </asp:BoundField>
                            <asp:BoundField DataField="TRN_INVNO" HeaderText="Invoice No" SortExpression="TRN_INVNO" />
                            <asp:BoundField DataField="TRN_CHQNO" HeaderText="Chqno" SortExpression="TRN_CHQNO" Visible="False" />
                            <asp:BoundField DataField="BALANCE" HeaderText="Balance">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Allocate Amount">
                                <ItemTemplate>
                                    &nbsp;<asp:TextBox ID="txtSAllocated" runat="server" CssClass="inputbox" Width="80px" onfocus='<%# Eval("BALANCE", "javascript:SetAmount(this,{0});") %>' onBlur="OnChange();">0</asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TRN_VOUCHDT" HeaderText="VDate" SortExpression="TRN_VOUCHDT" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TRN_NARRATION" HeaderText="Narration" SortExpression="TRN_NARRATION" />
                            <asp:TemplateField HeaderText="GUID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblGuid" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <HeaderStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span class="field-label">Amount Alloted</span> </td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtSAlloted" runat="server" ReadOnly="True">0</asp:TextBox>
                </td>
                <td align="left" width="20%"></td>
                <td align="left" width="30%"></td>
            </tr>
            <tr id="tr_SaveButtons" runat="server">
                <td align="center" colspan="4">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Done" />
                    <asp:Button ID="btnPreview" runat="server" CssClass="button" Text="Preview" ValidationGroup="Details" CausesValidation="False" />
                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label><br />
                </td>
            </tr>
        </table>
    </form>
    <input id="h_Close" runat="server" type="hidden" value="" />
</body>
</html>
