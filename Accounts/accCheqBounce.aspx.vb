Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration

Partial Class Accounts_accCheqClearanceBR
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("../noAccess.aspx")
        End If
        Dim MainMnu_code As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Page.Title = OASISConstants.Gemstitle

        UtilityObj.beforeLoopingControls(Me.Page)

        If Not Page.IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            txtChqNo.Attributes.Add("ReadOnly", "ReadOnly")
            txtDocNo.Attributes.Add("ReadOnly", "ReadOnly")
            txtOldBank.Attributes.Add("ReadOnly", "ReadOnly")
            txtOldChqDate.Attributes.Add("ReadOnly", "ReadOnly")
            txtAmount.Attributes.Add("ReadOnly", "ReadOnly")
        Else
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = String.Empty
            If txtDocNo.Text <> "" And txtChqNo.Text <> "" Then
                str_Sql = "SELECT ACCOUNTS_M.ACT_NAME, VOUCHER_D.VHD_ID,VOUCHER_D.VHD_SUB_ID, " & _
                "VOUCHER_D.VHD_AMOUNT, VOUCHER_H.VHH_ACT_ID AS ACT_ID," & _
                " VOUCHER_D.VHD_CHQNO, VOUCHER_D.VHD_CHQDT FROM VOUCHER_H INNER JOIN VOUCHER_D " & _
                " ON VOUCHER_H.VHH_SUB_ID = VOUCHER_D.VHD_SUB_ID AND VOUCHER_H.VHH_BSU_ID = VOUCHER_D.VHD_BSU_ID " & _
                " AND VOUCHER_H.VHH_FYEAR = VOUCHER_D.VHD_FYEAR AND VOUCHER_H.VHH_DOCTYPE = VOUCHER_D.VHD_DOCTYPE " & _
                " AND VOUCHER_H.VHH_DOCNO = VOUCHER_D.VHD_DOCNO LEFT OUTER JOIN ACCOUNTS_M ON " & _
                "VOUCHER_H.VHH_ACT_ID = ACCOUNTS_M.ACT_ID WHERE " & _
                " VHD_CHQNO = '" & txtChqNo.Text & "' AND VHD_DOCNO = '" & txtDocNo.Text & "'"
                'str_Sql = "SELECT VOUCHER_D.VHD_ACT_ID as VHD_ACT_ID, " & _
                '"VOUCHER_D.VHD_CHQDT AS VHD_CHQDT, ACCOUNTS_M.ACT_NAME as ACT_NAME" & _
                '" FROM VOUCHER_D LEFT OUTER JOIN " & _
                '" ACCOUNTS_M ON VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID WHERE " & _
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                While (dr.Read())
                    h_VHD_ACT_ID.Value = dr("ACT_ID")
                    h_VHD_ID.Value = dr("VHD_ID")
                    h_VHD_SUB_ID.Value = dr("VHD_SUB_ID")
                    txtOldBank.Text = dr("ACT_ID") & "-" & dr("ACT_NAME")
                    txtOldChqDate.Text = Format(dr("VHD_CHQDT"), OASISConstants.DateFormat)
                    txtAmount.Text = dr("VHD_AMOUNT")
                    lblError.Text = ""
                End While
            End If
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Not ValidateTextFields() Then
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Dim cmd As SqlCommand
        cmd = New SqlCommand("UpdateChequeStatus", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpVHD_SUB_ID As New SqlParameter("@VHD_SUB_ID", SqlDbType.VarChar, 10)
        sqlpVHD_SUB_ID.Value = h_VHD_SUB_ID.Value
        cmd.Parameters.Add(sqlpVHD_SUB_ID)

        Dim sqlpVHD_BSU_ID As New SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar, 10)
        sqlpVHD_BSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpVHD_BSU_ID)

        Dim sqlpVHD_FYEAR As New SqlParameter("@VHD_FYEAR", SqlDbType.Int)
        sqlpVHD_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpVHD_FYEAR)

        Dim sqlpVHD_DOCTYPE As New SqlParameter("@VHD_DOCTYPE", SqlDbType.VarChar, 20)
        sqlpVHD_DOCTYPE.Value = "BR"
        cmd.Parameters.Add(sqlpVHD_DOCTYPE)

        Dim sqlpVHD_DOCNO As New SqlParameter("@VHD_DOCNO", SqlDbType.VarChar, 20)
        sqlpVHD_DOCNO.Value = txtDocNo.Text
        cmd.Parameters.Add(sqlpVHD_DOCNO)

        Dim sqlpVHD_ID As New SqlParameter("@VHD_ID", SqlDbType.Int)
        sqlpVHD_ID.Value = h_VHD_ID.Value
        cmd.Parameters.Add(sqlpVHD_ID)

        Dim sqlpVHD_ACT_ID As New SqlParameter("@VHD_ACT_ID", SqlDbType.VarChar, 20)
        sqlpVHD_ACT_ID.Value = h_VHD_ACT_ID.Value
        cmd.Parameters.Add(sqlpVHD_ACT_ID)

        Dim sqlpVHD_AMOUNT As New SqlParameter("@VHD_AMOUNT", SqlDbType.Decimal, 21)
        sqlpVHD_AMOUNT.Value = txtAmount.Text
        cmd.Parameters.Add(sqlpVHD_AMOUNT)

        Dim sqlpVHD_CHQNO As New SqlParameter("@VHD_CHQNO", SqlDbType.VarChar, 20)
        sqlpVHD_CHQNO.Value = txtChqNo.Text
        cmd.Parameters.Add(sqlpVHD_CHQNO)

        Dim sqlpVHD_CHQDT As New SqlParameter("@VHD_CHQDT", SqlDbType.DateTime)
        sqlpVHD_CHQDT.Value = CDate(txtOldChqDate.Text)
        cmd.Parameters.Add(sqlpVHD_CHQDT)

        Dim sqlpVHD_bBOUNCED As New SqlParameter("@VHD_bBOUNCED", SqlDbType.Bit)
        sqlpVHD_bBOUNCED.Value = True
        cmd.Parameters.Add(sqlpVHD_bBOUNCED)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        Dim iReturnvalue As Integer = retValParam.Value

        If iReturnvalue <> 0 Then
            'Response.Write(getErrorMessage(iReturnvalue))
            lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
            stTrans.Rollback()
        Else
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", txtDocNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
            'UtilityObj.operOnAudiTable(USR_NAME, CurBsUnit, aud_module, aud_form, aud_docno, aud_action, aud_remark)
            If flagAudit <> 0 Then
                'add to all files
                lblError.Text = UtilityObj.getErrorMessage(flagAudit)
                stTrans.Rollback()
                Throw New ArgumentException("Could not process your request")
            Else
                stTrans.Commit()
                ClearFields()
                lblError.Text = "Data Updated Successfully....."
            End If
        End If
        cmd.Parameters.Clear()
    End Sub

    Private Sub ClearFields()
        txtDocNo.Text = ""
        txtChqNo.Text = ""
        txtOldBank.Text = ""
        txtOldChqDate.Text = ""
        txtAmount.Text = ""
    End Sub
    Private Function ValidateTextFields() As Boolean
        If txtDocNo.Text <> "" And txtChqNo.Text <> "" And _
            txtOldBank.Text <> "" And txtOldChqDate.Text <> "" And txtAmount.Text <> "" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If Not Request.UrlReferrer Is Nothing Then
            Response.Redirect(Request.UrlReferrer.ToString())
        End If
    End Sub
End Class
