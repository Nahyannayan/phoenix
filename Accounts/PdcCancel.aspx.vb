Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Accounts_PdcCancel
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            lblHead.Text = "PDC Cancellation / Deferral " 'Master.MenuName
            Try
                gvGroup1.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                'h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_10.Value = "LI__../Images/operations/like.gif"
                '   --- Lijo's Code ---
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or Session("MainMnu_code") <> "A150099" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))
                    Page.Title = OASISConstants.Gemstitle
                End If
                gridbind()
                If Session("datamode") = "none" Then
                    Session("datamode") = "add"
                End If
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvGroup1.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        'str_Sid_img = h_Selected_menu_4.Value.Split("__")
        'getid("mnu_4_img", str_Sid_img(2))
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid("mnu_8_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_10.Value.Split("__")
        getid("mnu_10_img", str_Sid_img(2))

    End Sub

    Private Sub gridbind()
        Dim lstrDocNo As String = String.Empty
        Dim lstrOldDocNo As String = String.Empty
        Dim lstrDocDate As String = String.Empty
        Dim lstrPartyAC As String = String.Empty
        Dim lstrBankAC As String = String.Empty
        Dim lstrNarration As String = String.Empty
        Dim lstrCurrency As String = String.Empty
        Dim lstrAmount As String = String.Empty
        Dim lstrOpr As String = String.Empty
        Dim lstrFiltDocNo As String = String.Empty
        Dim lstrFiltOldDocNo As String = String.Empty
        Dim lstrFiltDocDate As String = String.Empty
        Dim lstrFiltBankAC As String = String.Empty
        Dim lstrFiltNarration As String = String.Empty
        Dim lstrFiltCurrency As String = String.Empty
        Dim lstrFiltAmount As String = String.Empty

        Dim lstrChqdate As String = String.Empty
        Dim lstrFillChqdate As String = String.Empty
        Dim lstrParty As String = String.Empty
        Dim lstrFillParty As String = String.Empty

        Dim larrSearchOpr() As String
        Dim txtSearch As New TextBox

        If gvGroup1.Rows.Count > 0 Then
            ' --- Initialize The Variables 
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
            lstrDocNo = Trim(txtSearch.Text)
            If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "VHH_DOCNO", lstrDocNo)

            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
            lstrDocDate = txtSearch.Text
            If (lstrDocDate <> "") Then lstrFiltDocDate = SetCondn(lstrOpr, "VHH_DOCDT", lstrDocDate)

            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtBankAC")
            lstrBankAC = txtSearch.Text
            If (lstrBankAC <> "") Then lstrFiltBankAC = SetCondn(lstrOpr, "BANK.ACT_NAME", lstrBankAC)

            ''   -- 4  Type
            'larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            'lstrOpr = larrSearchOpr(0)
            'txtSearch = gvGroup1.HeaderRow.FindControl("txtDocType")
            'lstrOldDocNo = txtSearch.Text
            'If (lstrOldDocNo <> "") Then lstrFiltOldDocNo = SetCondn(lstrOpr, "VHH_DOCTYPE", lstrOldDocNo)

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
            lstrNarration = txtSearch.Text
            If (lstrNarration <> "") Then lstrFiltNarration = SetCondn(lstrOpr, "VHD_NARRATION", lstrNarration)

            '   -- 6 Amount
            larrSearchOpr = h_Selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtAmount")
            lstrAmount = txtSearch.Text
            If (lstrAmount <> "") Then lstrFiltAmount = SetCondn(lstrOpr, "VHD_AMOUNT", lstrAmount)

            '   -- 8 Chqe Date
            larrSearchOpr = h_Selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtChqDate")
            lstrChqdate = txtSearch.Text
            If (lstrChqdate <> "") Then lstrFillChqdate = SetCondn(lstrOpr, "VHD.VHD_CHQDT", lstrChqdate)


            '   -- 10 Party
            larrSearchOpr = h_Selected_menu_10.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvGroup1.HeaderRow.FindControl("txtParty")
            lstrParty = txtSearch.Text
            If (lstrParty <> "") Then lstrFillParty = SetCondn(lstrOpr, "PARTY.ACT_NAME", lstrParty)

        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String

        Dim CurDate As String = DateTime.Now.ToString("dd/MMM/yyyy")
        str_Sql = " SELECT   VHD.VHD_ID,  REPLACE(CONVERT(VARCHAR,VHH.VHH_DOCDT,106),' ','/') AS VHH_DOCDT, " & _
        " VHD.VHD_CHQNO, REPLACE(CONVERT(VARCHAR,VHD.VHD_CHQDT,106),' ','/') AS VHD_CHQDT, " & _
        " BANK.ACT_NAME AS BANK, PARTY.ACT_NAME AS PARTY, VHD.VHD_AMOUNT, " & _
        " VHH.VHH_DOCNO, VHH.VHH_DOCTYPE, VHD.VHD_NARRATION FROM  VOUCHER_H AS VHH INNER JOIN   " & _
        " VOUCHER_D AS VHD ON VHH.VHH_SUB_ID = VHD.VHD_SUB_ID AND VHH.VHH_BSU_ID = VHD.VHD_BSU_ID AND  " & _
        " VHH.VHH_FYEAR = VHD.VHD_FYEAR AND VHH.VHH_DOCTYPE = VHD.VHD_DOCTYPE AND VHH.VHH_DOCNO = VHD.VHD_DOCNO INNER JOIN " & _
        " ACCOUNTS_M AS BANK ON VHH.VHH_ACT_ID = BANK.ACT_ID LEFT OUTER JOIN ACCOUNTS_M AS PARTY ON VHH.VHH_ISSUEDTO = PARTY.ACT_ID " & _
        " WHERE VHH.VHH_BSU_ID ='" & Session("sBsuid") & "'  " & _
        " AND VHH.VHH_DOCTYPE IN('BP','QR') AND VHD_CHQDT>= '" & CurDate & "' " & _
        " AND (isnull(VHH_bpDC,0)=1) AND (VHH.VHH_DOCDT <= '" & CurDate & "' )  " & _
        " AND isnull(vhh_bposted,0)=1 " 'and VHD.VHD_ID not in(SELECT VHR_VHD_ID FROM VOUCHER_D_REVISION where isnull(VHR_bDeleted,0)=0)"
        
        str_Sql = str_Sql & lstrFiltDocNo & lstrFiltOldDocNo & lstrFiltDocDate & lstrFiltBankAC & lstrFiltNarration & lstrFiltAmount & lstrFiltCurrency & lstrFillChqdate & lstrFillParty
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvGroup1.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            TableSave.visible = False
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGroup1.DataBind()
            Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count

            gvGroup1.Rows(0).Cells.Clear()
            gvGroup1.Rows(0).Cells.Add(New TableCell)
            gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            TableSave.visible = True
            gvGroup1.DataBind()
        End If

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocNo")
        txtSearch.Text = lstrDocNo

        txtSearch = gvGroup1.HeaderRow.FindControl("txtDocDate")
        txtSearch.Text = lstrDocDate

        txtSearch = gvGroup1.HeaderRow.FindControl("txtBankAC")
        txtSearch.Text = lstrBankAC

        'txtSearch = gvGroup1.HeaderRow.FindControl("txtDocType")
        'txtSearch.Text = lstrOldDocNo

        txtSearch = gvGroup1.HeaderRow.FindControl("txtNarrn")
        txtSearch.Text = lstrNarration

        txtSearch = gvGroup1.HeaderRow.FindControl("txtAmount")
        txtSearch.Text = lstrAmount

        txtSearch = gvGroup1.HeaderRow.FindControl("txtChqDate")
        txtSearch.Text = lstrChqdate

        txtSearch = gvGroup1.HeaderRow.FindControl("txtParty")
        txtSearch.Text = lstrParty


        set_Menu_Img()
    End Sub

    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            doInsert()
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
        End Try
    End Sub

    Private Sub doInsert()
        Dim FctId As String = ""
        Dim bCancel As Boolean = True
        If CheckDeferral.Checked Then
            bCancel = False
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim _parameter As String(,) = New String(6, 1) {}
        Try
            For Each grow As GridViewRow In gvGroup1.Rows

                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                If ChkSelect IsNot Nothing And ChkSelect.Checked Then
                    Dim txtChargeDate As TextBox = CType(grow.FindControl("txtChargeDate"), TextBox)
                    If bCancel = False Then
                        If CDate(txtChargeDate.Text) < Date.Now Then
                            'Throw New Exception("Invalid Cheque Date..")
                            lblError.Text = "Invalid Cheque Date.."
                            stTrans.Rollback()
                            objConn.Close()
                            'grow.BackColor = Drawing.Color.BurlyWood
                            Exit Sub
                        End If
                    End If

                    Dim pParms(6) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@VHD_ID", SqlDbType.Int)
                    pParms(0).Value = ChkSelect.Value
                    pParms(1) = New SqlClient.SqlParameter("@VHD_BSU_ID", SqlDbType.VarChar)
                    pParms(1).Value = Session("sBsuid")
                    pParms(2) = New SqlClient.SqlParameter("@VHR_USER", SqlDbType.VarChar)
                    pParms(2).Value = Session("sUsr_name")
                    pParms(3) = New SqlClient.SqlParameter("@VHD_CHQDT", SqlDbType.DateTime)
                    pParms(3).Value = CDate(txtChargeDate.Text)
                    pParms(4) = New SqlClient.SqlParameter("@VHR_bCANCELLED", SqlDbType.Bit)
                    pParms(4).Value = bCancel
                    pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParms(5).Direction = ParameterDirection.ReturnValue
                    Dim retval As Integer
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SaveVOUCHER_D_REVISION", pParms)
                    If pParms(5).Value = "0" Then
                        lblError.Text = getErrorMessage(pParms(5).Value)
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ChkSelect.Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                    Else
                        lblError.Text = getErrorMessage(pParms(5).Value)
                        stTrans.Rollback()
                        objConn.Close()
                        'grow.BackColor = Drawing.Color.BurlyWood
                        Exit Sub
                    End If
                End If

            Next
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            stTrans.Rollback()
            objConn.Close()
        End Try
        stTrans.Commit()
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
    End Sub

    Protected Sub txtChargeDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtChargeDate As New TextBox
        txtChargeDate = sender
        txtChargeDate.Attributes.Add("ReadOnly", "ReadOnly")
    End Sub

End Class
