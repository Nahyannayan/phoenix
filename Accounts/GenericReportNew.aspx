﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="GenericReportNew.aspx.vb" Inherits="Accounts_GenericReportNew" %>



<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>--%>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
        <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="../Scripts/jquery.mtz.monthpicker.js" type="text/javascript"></script>
        <link href="../cssfiles/StyleSheet.css" rel="stylesheet" />
    <%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>
    <style>

        
        
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: SkyBlue;
            border-width: 3px;
            border-style: solid;
            border-color: Black;
            padding: 3px;
            width: 100px;
            height: 100px;
        }
  



        /*bootstrap class overrides starts here*/
        .card-body {
            padding: 0.25rem !important;
        }

        table th, table td {
            padding: 0.1rem !important;
        }

        /*bootstrap class overrides ends here*/


        /***/
        html body .riSingle .riTextBox[type="text"] {
            padding: 10px;
        }

        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
        }

        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
            background-position: 0 !important;
        }

        .RadPicker { /*width:80% !important;*/
        }

        table.RadCalendar_Default, .RadCalendar .rcMainTable {
            background: #ffffff !important;
        }

        /***/


        .RadGrid {
            border-radius: 0px !important;
            overflow: hidden;
        }

        .RadGrid_Default .rgCommandTable td {
            border: 0;
        }




        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            /*width: 80%;*/
            background-image: none !important;
            background-color: #fff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


        .RadComboBox .rcbInputCell {
            width: 100%;
            height: 31px !important;
            background-color: transparent !important;
            border-radius: 6px !important;
            background-image: none !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
            padding: 0px !important;
        }

        .RadComboBox_Default {
            /* color: #333; */
            font: inherit;
            width: 80% !important;
            font-style: normal !important;
        }

            .RadComboBox_Default .rcbEmptyMessage {
                font-style: normal !important;
            }

            .RadComboBox_Default .rcbFocused .rcbInput {
                color: black;
                /*padding: 10px;*/
            }

        .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image: none !important;
        }

        .RadComboBox table td.rcbInputCell {
            padding: 0px !important;
            border-width: 0;
        }

        /* overwriting bootstrap css class for caption*/
        .caption {
            padding: 0px inherit !important;
        }

        .RadComboBox .rcbReadOnly .rcbInput {
            cursor: default;
            /*border: 1px solid rgba(0,0,0,0.12) !important;
    padding: 20px 10px !important;*/
        }

        table.RadCalendarMonthView_Default {
            background-color: #ffffff !important;
        }

        .RadGrid_Default {
            border: 0px !important;
        }

            .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
                border: 1px solid rgba(0,0,0,0.16);
            }

        .RadComboBoxDropDown .rcbList {
            line-height: 30px !important;
            font-size: 14px;
        }

        .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
            border: 1px solid rgba(0, 0, 0, 0.16) !important;
        }

        .RadGrid_Default .rgGroupPanel {
            background-image: none !important;
        }

            .RadGrid_Default .rgGroupPanel td {
                padding: 0px !important;
            }

        .RadGrid .rgNumPart a {
            line-height: 6px !important;
            padding: 0px 1px !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function GetUserIds() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            type = 'USERS';
            result = radopen("../Common/PopupForm.aspx?multiSelect=false&ID=" + type, "pop_user")
           <%-- if (result != "" && result != "undefined") {
                NameandCode = result.split('___');
                document.getElementById('<%=txtUserID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=hdnUserID.ClientID %>').value = NameandCode[1];
            }
            return false;--%>
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtUserID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=hdnUserID.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtUserID.ClientID %>', 'TextChanged');

            }
        }

        function getStudent() {

            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var sFeatures;
            sFeatures = "dialogWidth: 875px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var StuBsuId = document.getElementById('<%= h_Sel_Bsu_Id.ClientID %>').value;
            if (document.getElementById('<%= radStud.ClientID %>').checked == true)
                Stu_type = "S";
            else
                Stu_type = "E";

            pMode = "ALL_SOURCE_STUDENTS"
            url = "../Fees/ShowStudent.aspx?type=" + pMode + "&ProviderBSUID=" + StuBsuId + "&Stu_Bsu_Id=" + StuBsuId + "&STU_TYPE=" + Stu_type;
            result = radopen(url, "pop_student");
            
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtStudentname.ClientID %>', 'TextChanged');

            }
        }

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../Accounts/accShowEmpDetail.aspx?id=EN", "pop_up")
            
        }

        function OnClientClose(oWnd, args) {
            
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_ID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID %>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function ClearEmployee() {
            document.getElementById("<%=h_Emp_ID.ClientID%>").value = "";
            document.getElementById("<%=txtEmpNo.ClientID%>").value = "";
        }
        function PrintRadGrid() {

            var tabledata = '<%= Session("GridData2")%>';
            //alert(tabledata);

            var headertitle = document.getElementById("<%= hdn_HeaderTitle.ClientID%>").value;

            if (isIE()) {
                var elem1 = '00CENTER00';
                while (headertitle.indexOf(elem1) != -1) {
                    var headertitle2 = headertitle.replace("00CENTER00", "<div style='font-weight: bold; text-align: center;'>");
                    headertitle = headertitle2;

                }

                var elem2 = '00LEFT00';
                while (headertitle.indexOf(elem2) != -1) {
                    var headertitle2 = headertitle.replace("00LEFT00", "<div style='font-weight: bold; text-align: left;'>");
                    headertitle = headertitle2;

                }

                var elem3 = '00RIGHT00';
                while (headertitle.indexOf(elem3) != -1) {
                    var headertitle2 = headertitle.replace("00RIGHT00", "</div>");
                    headertitle = headertitle2;

                }
            } else {
                while (headertitle.includes("00CENTER00")) {
                    var headertitle2 = headertitle.replace("00CENTER00", "<div style='font-weight: bold; text-align: center;'>");
                    headertitle = headertitle2;
                }
                while (headertitle.includes("00LEFT00")) {
                    var headertitle2 = headertitle.replace("00LEFT00", "<div style='font-weight: normal; text-align: left;'>");
                    headertitle = headertitle2;
                }
                while (headertitle.includes("00RIGHT00")) {
                    var headertitle2 = headertitle.replace("00RIGHT00", "</div>");
                    headertitle = headertitle2;
                }
            }

            var previewWnd = window.open('about:blank', '', '', false);
            //var previewWnd = window.open();
            <%--var sh = '<%= ClientScript.GetWebResourceUrl(RadGridReport.GetType(), String.Format("Telerik.Web.UI.Skins.{0}.Grid.{0}.css", RadGridReport.Skin))%>';--%>
            <%--var sh = '<%= ClientScript.GetWebResourceUrl(RadGridReport.GetType(), String.Format("/../vendor/bootstrap/css/bootstrap.css"))%>';--%>
            var sh = '/../vendor/datatables/dataTables.bootstrap4.css'
            var styleStr = "<html><head><link href='/../vendor/bootstrap/css/bootstrap.css' rel='stylesheet'></link><link href = '" + sh + "' rel='stylesheet' type='text/css'></link></head>";

            <%--var datacontnt_temp = $find('<%= RadGridReport.ClientID%>').get_element().outerHTML
            var datacontnt = datacontnt_temp.replace("<td>Drag a column header and drop it here to group by that column</td>", "<td></td>");
            datacontnt_temp = datacontnt.replace("<tfoot>", "<tfoot style='visibility:hidden'>");
            datacontnt = datacontnt_temp--%>
            var htmlcontent = styleStr + "<body>" + headertitle + tabledata + "</body></html>";
            <%--console.log($find('<%= RadGridReport.ClientID%>').get_element());--%>
            //previewWnd.document.open();
            previewWnd.document.write(htmlcontent);
            if (isIE()) {
                previewWnd.document.close();
            } else {

            }
            previewWnd.print();
            previewWnd.close();

        }
        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_user" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />

                <table width="99%">
                    <tr id="trReportType" runat="server" class="input-group">
                        <td runat="server" width="20%" align="left"><span class="field-label">Select The Report Type</span>
                        </td>
                        <td runat="server" colspan="2" width="30%" align="left">
                            <telerik:RadComboBox Width="100%" ID="radReportType" runat="server" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False" AutoPostBack="True">
                            </telerik:RadComboBox>

                        </td>
                        <%--<td runat="server" width="20%"></td>--%>
                        <td runat="server" width="40%" align="left">
                            <asp:Label ID="lblReportDescription" runat="server" class="input-lblMessage"></asp:Label></td>
                    </tr>
                    <tr id="trBSUIDs" runat="server" class="input-group">
                        <td runat="server" width="20%" align="left"><span class="field-label">Business Unit(s)</span>
                        </td>
                        <td runat="server" colspan="2" align="left">
                            <div class="checkbox-list">
                                <telerik:RadTreeView ID="RadBSUTreeView" Enabled="true" runat="server" CausesValidation="false"
                                    CheckBoxes="True" MultipleSelect="True" CheckChildNodes="True">
                                </telerik:RadTreeView>
                            </div>
                        </td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trBSUID" runat="server" class="input-group">
                        <td id="Td11" width="20%" runat="server"><span class="field-label">Business Unit</span>
                        </td>
                        <td id="Td12" runat="server" colspan="2">
                            <telerik:RadComboBox ID="ddlBSU" Style="width: 100% !important;" runat="server" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False" AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trAcademicYear" runat="server" class="input-group">
                        <td id="Td1" width="20%" runat="server"><span class="field-label">Academic Year</span>
                        </td>
                        <td id="Td2" runat="server" width="30%">
                            <telerik:RadComboBox ID="ddlAcademicYear" Style="width: 100% !important;" runat="server" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False">
                            </telerik:RadComboBox>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trFeeType" runat="server" class="input-group">
                        <td id="Td13" runat="server" width="20%"><span class="field-label">Fee Type</span>
                        </td>
                        <td id="Td14" align="left" runat="server" width="30%">
                            <telerik:RadComboBox ID="ddlFeeType" runat="server" Style="width: 100% !important;" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False">
                            </telerik:RadComboBox>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trService" runat="server" class="input-group">
                        <td id="Td15" runat="server" width="20%"><span class="field-label">Service</span>
                        </td>
                        <td id="Td16" align="left" runat="server" width="30%">
                            <telerik:RadComboBox ID="ddlService" runat="server" Style="width: 100% !important;" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False">
                            </telerik:RadComboBox>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trUserID" runat="server" class="input-group">
                        <td id="Td5" runat="server" width="20%"><span class="field-label">User Name</span>
                        </td>
                        <td id="Td6" align="left" runat="server" width="30%">
                            <asp:TextBox ID="txtUserID" runat="server" CssClass="inputbox"></asp:TextBox><asp:HiddenField
                                ID="hdnUserID" runat="server" />
                            <asp:ImageButton ID="imgUserID" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return GetUserIds(); return false;"></asp:ImageButton>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trStudents" runat="server" class="input-group">
                        <td id="Td7" width="20%" runat="server"><span class="field-label">Student Detail</span>
                        </td>
                        <td id="Td8" runat="server" colspan="3" width="80%">
                            <div>
                                <asp:RadioButton ID="radStud" runat="server" CssClass="field-label" AutoPostBack="true" Checked="True" GroupName="STUD_TYPE"
                                    Text="Student"></asp:RadioButton>
                                <asp:RadioButton ID="radEnq" runat="server" CssClass="field-label" AutoPostBack="true" GroupName="STUD_TYPE"
                                    Text="Enquiry"></asp:RadioButton>

                                <telerik:RadTextBox ID="txtStdNo" runat="server" AutoPostBack="True">
                                </telerik:RadTextBox>
                                &nbsp;<asp:ImageButton ID="imgStudent" runat="server" OnClientClick="getStudent();return false;"
                                    ImageUrl="~/Images/cal.gif" />
                                <telerik:RadTextBox ID="txtStudentname" runat="server" AutoPostBack="True" Width="30%">
                                </telerik:RadTextBox>
                            </div>
                        </td>
                        <%-- <td runat="server" width="20%"></td>
                        <td runat="server" width="30%"></td>--%>
                    </tr>
                    <tr id="trLeaveTypes" runat="server" class="input-group">
                        <td id="Td17" width="20%" runat="server"><span class="field-label">Leave Type(s)</span>
                        </td>
                        <td id="Td18" runat="server" width="30%">
                            <div class="checkbox-list">
                                <telerik:RadTreeView ID="RadTreeLeaveTypes" Enabled="true" runat="server" CausesValidation="false"
                                    CheckBoxes="True" MultipleSelect="True" CheckChildNodes="True">
                                </telerik:RadTreeView>
                            </div>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trLeaveType" runat="server" class="input-group">
                        <td id="Td19" width="20%" runat="server"><span class="field-label">Leave Type</span>
                        </td>
                        <td id="Td20" runat="server" width="30%">
                            <telerik:RadComboBox ID="ddlLeaveType" runat="server" Style="width: 100% !important;" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False" AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trEmpCategory" runat="server" class="input-group">
                        <td id="Td21" width="20%" runat="server"><span class="field-label">Employee Category(s)</span>
                        </td>
                        <td id="Td22" runat="server" width="30%">
                            <telerik:RadTreeView ID="RadTreeEmpCategory" Enabled="true" runat="server" CausesValidation="false"
                                CheckBoxes="True" MultipleSelect="True" CheckChildNodes="True">
                            </telerik:RadTreeView>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trEMPABC" runat="server" class="input-group">
                        <td id="Td23" width="20%" runat="server"><span class="field-label">ABC Category(s)</span>
                        </td>
                        <td id="Td24" runat="server" width="30%">
                            <div class="checkbox-list">
                                <telerik:RadTreeView ID="RadTreeABC" Enabled="true" runat="server" CausesValidation="false"
                                    CheckBoxes="True" MultipleSelect="True" CheckChildNodes="True">
                                </telerik:RadTreeView>
                            </div>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trEmployee" runat="server" class="input-group">
                        <td id="Td25" width="20%" runat="server"><span class="field-label">Employee</span>
                        </td>
                        <td id="Td26" runat="server" width="30%">
                            <telerik:RadTextBox ID="txtEmpNo" runat="server" AutoPostBack="True">
                            </telerik:RadTextBox>
                            &nbsp;<asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetEMPName(); return false;" />
                            <asp:LinkButton ID="lnkClearEmployee" OnClientClick="ClearEmployee();return false;" runat="server"
                                CssClass="lnkerror" EnableViewState="False">Clear</asp:LinkButton>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trDateRange" runat="server" class="input-group">
                        <td id="Td3" align="left" width="20%" runat="server"><span class="field-label">Date Range</span>
                        </td>
                        <td id="Td4" align="left" runat="server" colspan="3" width="80%">
                            <table width="100%">
                                <tr>
                                    <td width="30%" align="left" class="p-0">
                                        <telerik:RadDatePicker ID="txtFromDate" runat="server" DateInput-EmptyMessage="MinDate" Width="80%">
                                            <DateInput ID="DateInput1" EmptyMessage="MinDate" DateFormat="dd/MMM/yyyy" runat="server">
                                            </DateInput>
                                            <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                                ViewSelectorText="x" runat="server">
                                            </Calendar>
                                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </td>
                                    <td width="10%" align="left" class="p-0">
                                        <span class="field-label">To </span>
                                    </td>
                                    <td width="40%" align="left" class="p-0">
                                        <telerik:RadDatePicker ID="txtToDate" runat="server" DateInput-EmptyMessage="MinDate" Width="56%">
                                            <DateInput ID="DateInput2" EmptyMessage="MinDate" DateFormat="dd/MMM/yyyy" runat="server">
                                            </DateInput>
                                            <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                                ViewSelectorText="x" runat="server">
                                            </Calendar>
                                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                            </table>
                        </td>


                    </tr>
                    <tr id="trAXSubType" runat="server" class="input-group">
                        <td id="Td27" align="right" width="20%" runat="server"><span class="field-label">Sub Type</span>
                        </td>
                        <td id="Td28" align="left" runat="server" width="30%">
                            <telerik:RadComboBox ID="ddlAXSubType" runat="server" RenderMode="Lightweight" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False" AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trAXSubTypes" runat="server" class="input-group">
                        <td align="right" width="20%" runat="server"><span class="field-label">Sub Type(s)</span>
                        </td>
                        <td align="left" runat="server" width="30%">
                            <div class="checkbox-list">
                                <telerik:RadTreeView ID="RadTreeAXSubTypes" Enabled="true" runat="server" CausesValidation="false"
                                    CheckBoxes="True" MultipleSelect="True" CheckChildNodes="True">
                                </telerik:RadTreeView>
                            </div>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>


                    <tr id="trAsOnDate" runat="server" class="input-group">
                        <td align="left" width="20%" runat="server"><span class="field-label">As On Date</span>
                        </td>
                        <td align="left" runat="server" width="30%">
                            <telerik:RadDatePicker ID="txtDate" runat="server" DateInput-EmptyMessage="MinDate" Width="80%">
                                <DateInput ID="DateInput3" EmptyMessage="MinDate" DateFormat="dd/MMM/yyyy" runat="server">
                                </DateInput>
                                <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                    ViewSelectorText="x" runat="server">
                                </Calendar>
                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDatePicker>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trRESULTTYPE" runat="server" class="input-group">
                        <td style="width: 20%;" align="left" runat="server">
                            <asp:Label ID="lblFilterByStr" runat="server" CssClass="field-label"></asp:Label>
                        </td>
                        <td align="left" runat="server" width="50%" colspan="2">
                            <asp:RadioButtonList ID="rblFilter" RepeatDirection="Horizontal" runat="server">
                                <asp:ListItem Value="0"><span class="field-label">Option 1</span></asp:ListItem>
                                <asp:ListItem Value="1"><span class="field-label">Option 2</span></asp:ListItem>
                                <asp:ListItem Value="2"><span class="field-label">Option 3</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>

                        <td runat="server" width="30%"></td>
                    </tr>
                    <tr id="trSearchStr" runat="server" class="input-group">
                        <td id="Td9" style="width: 20%;" align="left" runat="server">
                            <asp:Label ID="lblSearchStr" runat="server" CssClass="field-label"></asp:Label>
                        </td>
                        <td id="Td10" align="left" runat="server" width="30%">
                            <asp:TextBox ID="txtSearchStr" runat="server"></asp:TextBox>
                        </td>
                        <td runat="server" width="10%"></td>
                        <td runat="server" width="40%"></td>
                    </tr>
                    <tr id="trGenerateReport" runat="server">
                        <td colspan="4" width="100%">
                            <table width="100%">
                                <tr>
                                    <td colspan="3" width="50%" align="right" class="p-0">
                                        <asp:Button ID="btnView" runat="server" Text="Generate Report" CssClass="button" />
                                        <asp:Button ID="lnkExportToExcel" runat="server" Text="Export To Excel" CssClass="button" />
                                        <asp:Button ID="lnkExportToPDF" runat="server" Text="Export To PDF" CssClass="button" Visible="False" />
                                        <asp:CheckBox ID="chkShowFilters" CssClass="field-label " runat="server" AutoPostBack="True" Visible="true" Checked="false" Text="Show Filter" />
                                        <asp:Button ID="btnPost" runat="server" Text="Post to Tagetik" CssClass="button"  />
                                    </td>
                                    <td width="50%" align="left">
                                        <table width="100%">
                                            <tr>
                                                <td width="40%" class="p-0">
                                                    <asp:CheckBox ID="chkSubscribe" runat="server"  Visible="false"  CssClass="field-label" Text="Subscribe" OnCheckedChanged="chkSubscribe_CheckedChanged" AutoPostBack="true" />
                                                </td>
                                                <td id="tdSubscribe" runat="server" width="60%" visible="false" class="p-0">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" width="80%" class="p-0">
                                                                <asp:RadioButtonList ID="rblSubscribe" runat="server" RepeatDirection="Horizontal" CssClass="field-label">
                                                                    <asp:ListItem Text="Daily" Value="D"><span class="field-label">Daily</span></asp:ListItem>
                                                                    <asp:ListItem Text="Weekly" Value="W"><span class="field-label">Weekly</span></asp:ListItem>
                                                                    <asp:ListItem Text="Monthly" Value="M"><span class="field-label">Monthly</span></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td align="left" width="20%" class="p-0">
                                                                <asp:Button ID="Button1" runat="server" CssClass="button" Text="Subscribe" />
                                                            </td>
                                                        </tr>
                                                    </table>


                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>

            </div>

            
        </div>
        
    </div>
    

    <div class="table-responsive">
        <table width="100%">
            <tr>
                <td align="center" colspan="4" width="100%">

                    <telerik:RadGrid ID="RadGridReport" runat="server" AllowSorting="True" ShowStatusBar="True" CssClass="table table-bordered table-row"
                        ShowGroupPanel="false" Width="100%" GridLines="Vertical" AllowPaging="True" PageSize="30"
                        EnableTheming="False" AllowFilteringByColumn="False">

                        <GroupingSettings CaseSensitive="False" />
                        <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="True">
                        </ClientSettings>
                        <AlternatingItemStyle CssClass="input-RadGridAltRow" HorizontalAlign="Left" />
                        <MasterTableView GridLines="Both">
                            <EditFormSettings>
                            </EditFormSettings>
                            <ItemStyle CssClass="input-RadGridRow" HorizontalAlign="Left" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                        </MasterTableView>
                    </telerik:RadGrid>

                </td>
            </tr>
        </table>
        <uc1:usrmessagebar runat="server" ID="usrMessageBar" />
    </div>
    <asp:HiddenField ID="h_STUD_ID" runat="server" />
    <asp:HiddenField ID="h_Sel_Bsu_Id" runat="server" />
    <asp:HiddenField ID="h_Emp_ID" runat="server" />

    <asp:HiddenField ID="hdn_HeaderTitle" runat="server" />
    

    <ModalPopupExtender  ID="ModalPopupExtender1" runat="server" X="250" Y="200"
        CancelControlID="btnCancel" OkControlID="btnOkay"
        TargetControlID="Button1" PopupControlID="Panel1"
        PopupDragHandleControlID="PopupHeader" Drag="true">
    </ModalPopupExtender>
       
    <asp:Panel ID="Panel1" Style="display: none; width: 400px;" runat="server" CssClass="panel-cover">
        <div>

            <div class="title-bg" id="PopupHeader">
                Confirm Subscribe?
                            <asp:ImageButton ID="ImageButton1" CssClass="float-right align-middle" runat="server" ImageUrl="~/Images/Curriculum/btnCloseblack.png" />
            </div>
            <div class="PopupBody m-3">
                <p>Click on 'Ok' to subscribe this report for the selected criteria(s). </p>
            </div>
            <div align="center">
                <asp:Button ID="btnOkay" runat="server" CssClass="button" UseSubmitBehavior="false" Text="Ok" OnClick="btnOkay_Click" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
            </div>
        </div>
    </asp:Panel>

    
<asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="ModalPopupExtender2" BehaviorID="mpe" runat="server"
    PopupControlID="pnlPopup" TargetControlID="lnkDummy" BackgroundCssClass="modalBackground" CancelControlID = "btnNo">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" Style="display: none;width: 400px;" CssClass="panel-cover">
    <div class="header">
        Budget Posting
    </div>
    <div class="body" >
        Budget for the selected month already posted. Do you want to re post?
        <br />
        <asp:Button ID="btnYes" CssClass="button" runat="server" Text="Yes" />
        <asp:Button ID="btnNo" CssClass="button" runat="server" Text="No" />
    </div>
</asp:Panel>

</asp:Content>
