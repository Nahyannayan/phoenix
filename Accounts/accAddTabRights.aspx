<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accAddTabRights.aspx.vb" Inherits="Accounts_accAddTabRights" Title="::::GEMS OASIS:::: Online Student Administration System::::"  %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
      <div class="card mb-3">
          <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>  
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
         <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblErrorMessage" runat="server"   CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>

                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left"colspan="2" > <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                </asp:DropDownList></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Specific Role Rights</span></td>
                        <td align="left" width="30%"> <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="True" DataTextField="ROL_DESCR"
                                    DataValueField="ROL_ID">
                                </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Select Business Unit Rights From</span></td>
                        <td align="left" width="30%">  <asp:DropDownList ID="ddlBusUnit" runat="server">
                                    </asp:DropDownList> <asp:Button ID="btnAddOpr" runat="server" CssClass="button" Text="Update" /></td>

                    </tr>
                    
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Menu Access Right</span></td>
                        <td align="left" width="30%">
                            <div class="checkbox-list">  
                            <asp:TreeView ID="tvmenu" runat="server" DataSourceID="XmlDataSourceMenu" ExpandDepth="0">
                                        <SelectedNodeStyle BackColor="#FFC080" />
                                        <DataBindings>
                                            <asp:TreeNodeBinding DataMember="menuItem" TextField="Text" ToolTipField="modules"
                                                ValueField="ValueField" />
                                        </DataBindings>
                                    </asp:TreeView>
                                </div>
                        </td>
                    </tr>
                    <tr  id="maintr" runat="server">
                       <td align="left" width="20%"><span class="field-label">Select Role Rights</span></td>
                        <td align="center" width="30%"> <asp:DropDownList ID="ddlRoleCopy" runat="server" AutoPostBack="True" DataTextField="ROL_DESCR"
                                        DataValueField="ROL_ID">
                                    </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">OperationRights</span></td>
                        <td align="center" width="30%"> <asp:DropDownList ID="ddlOprRight" runat="server">
                                    <asp:ListItem Value="0">Access Denied</asp:ListItem>
                                    <asp:ListItem Value="1">View</asp:ListItem>
                                    <asp:ListItem Value="2">Modify</asp:ListItem>
                                </asp:DropDownList> <asp:Button ID="btnCopy" runat="server" CssClass="button" Text="Copy" /></td>

                    </tr>
                    <tr >
                        <td align="left" colspan="4">
                            <span class="field-label">Operations Rights Color Code : </span>
                            <asp:label id="lblview" backcolor="#000000" runat="server"/><span style="padding:0px 10px;background-color:#000000; border:1px solid #e3e3e3;"></span> &nbsp<span class="border-right mr-3 pr-3">View</span>
                            <asp:label id="lblModify" backcolor="green" runat="server"/><span style="padding:0px 10px;background-color:green; border:1px solid #e3e3e3;"></span>&nbsp<span class="border-right mr-3 pr-3">Modify</span>
                            <asp:label id="lblAccessDenied" backcolor="red" runat="server"/><span style="padding:0px 10px;background-color:red; border:1px solid #e3e3e3;"></span>&nbsp<span class="">Access Denied</span>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4"><asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add/Edit" />
                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4"  valign="top">
                            <asp:GridView ID="InfoGridView" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20">
                                 <RowStyle CssClass="griditem"  HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="idLabel" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="BusUnit" HeaderText="Business Unit">
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RoleID" HeaderText="RoleID">
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TABCode" HeaderText="Tab Code">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TAB_PARENTID" HeaderText="TAB PARENTID" />
                                            <asp:BoundField DataField="FormText" HeaderText="Form Name">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OperationID" HeaderText="OperationID">
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Description">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="modules" HeaderText="Module">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:BoundField>
                                            <asp:CommandField HeaderText="Delete" ShowCancelButton="False" ShowDeleteButton="True" />
                                        </Columns>
                                        <HeaderStyle CssClass="gridheader" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                            </td>
                    </tr>
                     </table>
                </div></div>
          </div>








    
    <asp:XmlDataSource ID="XmlDataSourceMenu" runat="server" EnableCaching="False" TransformFile="~/TransformTabXSLT.xsl"
        XPath="MenuItems/MenuItem"></asp:XmlDataSource>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</asp:Content>

