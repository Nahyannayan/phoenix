Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class AccAddChqBook
    Inherits System.Web.UI.Page
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Dim MainMnu_code As String = String.Empty
        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                txtControlAcc.Text = Session("CollectBank")
                txtControlAccDescr.Text = Session("Collect_name")
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "A100040" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtControlAcc.Attributes.Add("readonly", "readonly")
                    txtControlAccDescr.Attributes.Add("readonly", "readonly")
                    txtLotid.Attributes.Add("readonly", "readonly")
                    FillChqformat()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

   
    Sub FillChqformat()
        Dim str_sql As String = "SELECT CHQ_ID,CHQ_DESCR FROM CHQ_M ORDER BY CHQ_DESCR "
        Dim _dataSet As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
        drpChequeFormat.DataSource = _dataSet.Tables(0)
        drpChequeFormat.DataTextField = "CHQ_DESCR"
        drpChequeFormat.DataValueField = "CHQ_ID"
        drpChequeFormat.DataBind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim lintRetVal As Integer
        ServerValidate()
        '   --- Proceed To Save
        If (lstrErrMsg = "") Then
            If ViewState("datamode") = "add" Then
                Try
                    objConn.Open()
                    Dim i_prev_chbid As Integer
                    Try
                        i_prev_chbid = CInt(h_Chbid.Value)
                    Catch ex As Exception
                        i_prev_chbid = 0
                    End Try
                    Dim SqlCmd As New SqlCommand("SaveCHQBOOK_M", objConn)
                    SqlCmd.CommandType = CommandType.StoredProcedure
                    SqlCmd.Parameters.AddWithValue("@CHB_ID", 0)
                    SqlCmd.Parameters.AddWithValue("@CHB_ACT_ID", txtControlAcc.Text)
                    SqlCmd.Parameters.AddWithValue("@CHB_BSU_ID", Session("SBSUId"))
                    SqlCmd.Parameters.AddWithValue("@CHB_LOTNO", Trim(txtLotNo.Text))
                    SqlCmd.Parameters.AddWithValue("@CHB_PREFIX", Trim(txtPrefix.Text))
                    SqlCmd.Parameters.AddWithValue("@CHB_FROM", Convert.ToInt32(Trim(txtFrom.Text)))
                    SqlCmd.Parameters.AddWithValue("@CHB_TO", Convert.ToInt32(Trim(txtTo.Text)))
                    SqlCmd.Parameters.AddWithValue("@CHB_NEXTNO", 1) '@CHB_PREV_CHB_ID

                    If i_prev_chbid > 0 And txtLotid.Text <> "" Then
                        SqlCmd.Parameters.AddWithValue("@CHB_PREV_CHB_ID", i_prev_chbid)
                    End If
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)
                    SqlCmd.Parameters.AddWithValue("@CHQ_ID", Int32.Parse(drpChequeFormat.SelectedValue)) '@CHB_PREV_CHB_ID
                    'SqlCmd.Parameters.Add("@CHB_GenNo", SqlDbType.Int)
                    ' SqlCmd.Parameters("@CHB_GenNo").Direction = ParameterDirection.Output

                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                    ' Response.Redirect("AccMstChqBook.aspx")
                    Dim SqlID As New SqlCommand("Select max(CHB_ID) from CHQBOOK_M", objConn)
                    SqlID.CommandType = CommandType.Text
                    Dim temp_id As String = SqlID.ExecuteScalar()
                    If lintRetVal = 0 Then
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, temp_id, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        ViewState("datamode") = "add"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Call clearall()
                        lblErr.Text = "Data Successfully Saved..."
                    Else
                        lblErr.Text = getErrorMessage(lintRetVal)
                    End If

                Catch myex As ArgumentException
                    lblErr.Text = myex.Message
                Catch ex As Exception
                    lblErr.Text = UtilityObj.getErrorMessage("1000")
                    UtilityObj.Errorlog(ex.Message)
                End Try
            End If
        End If
    End Sub


    Protected Sub ServerValidate()
        lstrErrMsg = ""
        If (IsNumeric(Trim(txtLotNo.Text)) = False) Then
            lstrErrMsg = lstrErrMsg & "Lot No Should Be A Numeric Value" & "<br>"
        End If
        If ((Trim(txtPrefix.Text) = "") Or (Master.StringVerify(txtPrefix.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "Prefix Should Be A String Value" & "<br>"
        End If
        If ((Trim(txtFrom.Text) = "") Or (Master.StringVerify(txtFrom.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "From # Should Be A Numeric Value" & "<br>"
        End If
        If ((Trim(txtTo.Text) = "") Or (Master.StringVerify(txtTo.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "To # Should Be A Numeric Value" & "<br>"
        End If
        txtControlAccDescr.Text = AccountFunctions.Validate_Account(txtControlAcc.Text, Session("sbsuid"), "BANK")
        If txtControlAccDescr.Text = "" Then
            lstrErrMsg = lstrErrMsg & "Invalid bank selected<br>"
        End If

        If ((Trim(drpChequeFormat.SelectedValue.ToString()) = "") Or Trim(drpChequeFormat.SelectedValue.ToString().Equals("0"))) Then
            lstrErrMsg = lstrErrMsg & "Invalid Cheque Format.." & "<br>"
        End If

        lblErr.Text = lstrErrMsg
    End Sub


    Public Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("SAVED SUCCESSFULLY...")
            End If
        Catch ex As Exception

            Return ("0")
        End Try
    End Function


    Sub clearall()
        txtControlAcc.Text = ""
        txtControlAccDescr.Text = ""
        txtFrom.Text = ""
        txtLotNo.Text = ""
        txtLotid.Text = ""
        h_Chbid.Value = ""
        txtPrefix.Text = ""
        txtTo.Text = ""
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        Call clearall()
        'set the rights on the button control based on the user
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear the textbox and set the default settings
            Call clearall()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Dim url As String
            Dim mainMnu_code As String = String.Empty
            ViewState("datamode") = "edit"
            'set the rights on the button control based on the user
            mainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Accounts\AccEditChqBook.aspx?MainMnu_code={0}&datamode={1}", mainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Request could not be processed "
        End Try
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


End Class
