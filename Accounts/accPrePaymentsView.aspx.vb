Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports UtilityObj
Partial Class accPrePaymentsView
    Inherits System.Web.UI.Page
     
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged

  
        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If MainMnu_code = "A150300" Then
                    rbAll.Visible = False
                    rbunpost.Visible = False
                    rbpost.Checked = True
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                'hardcode the menu code
                If CurUsr_id = "" Or CurRole_id = "" Or CurBsUnit = "" Or MainMnu_code <> "A150030" And MainMnu_code <> "A150300" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    '  h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    ' h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_9.Value = "LI__../Images/operations/like.gif"
                    rbunpost.Checked = True
                    Call gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
            Dim url As String
            'Encrypt the data that needs to be send through Query String
            Dim datamodeAdd As String = Encr_decrData.Encrypt("add")

            url = String.Format("~\Accounts\accPrepaymentsAddEdit.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), datamodeAdd)
            hlAddNew.NavigateUrl = url

        End If
        set_Menu_Img()

    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_2.Value.Split("__")
        ' getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_7.Value.Split("__")
        'getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_9.Value.Split("__")
        getid9(str_Sid_img(2))

    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    'Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
    '    If gvPrePayment.Rows.Count > 0 Then
    '        Dim s As HtmlControls.HtmlImage
    '        Try

    '            s = gvPrePayment.HeaderRow.FindControl("mnu_2_img")
    '            If p_imgsrc <> "" Then
    '                s.Src = p_imgsrc
    '            End If
    '            Return s.ClientID
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End If
    '    Return ""
    'End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    'Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
    '    If gvPrePayment.Rows.Count > 0 Then
    '        Dim s As HtmlControls.HtmlImage
    '        Try

    '            s = gvPrePayment.HeaderRow.FindControl("mnu_7_img")
    '            If p_imgsrc <> "" Then
    '                s.Src = p_imgsrc
    '            End If
    '            Return s.ClientID
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End If
    '    Return ""
    'End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid9(Optional ByVal p_imgsrc As String = "") As String
        If gvPrePayment.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvPrePayment.HeaderRow.FindControl("mnu_9_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function returnpath(ByVal p_posted) As String
        Try
            If p_posted = True Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try



            Dim Bpost As String = String.Empty
            If rbpost.Checked = True Then
                Bpost = " And a.P_bPost=1 "
            ElseIf rbunpost.Checked = True Then
                Bpost = " And a.P_bPost=0 "
            End If

            Dim MaCode As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If MaCode = "A150300" Then
                Bpost = " And a.P_bPost=1 "
            End If

            Dim str_conn As String = ConnectionManger.GetOASISFinConnection.ConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_Docno As String = String.Empty
            Dim str_filter_Party As String = String.Empty
            Dim str_filter_year As String = String.Empty
            Dim str_filter_prepay As String = String.Empty
            Dim str_filter_Exp As String = String.Empty
            Dim str_filter_Date As String = String.Empty
            Dim str_filter_Currency As String = String.Empty
            Dim str_filter_Amt As String = String.Empty
            Dim str_filter_NoInst As String = String.Empty

            Dim ds As New DataSet

            Dim str_Topfilter As String = ""
            If UsrTopFilter1.FilterCondition <> "All" Then
                str_Topfilter = " top " & UsrTopFilter1.FilterCondition
            End If
            str_Sql = "SELECT  " & str_Topfilter & " * from(SELECT PREPAYMENTS_H.Guid as Guid,PREPAYMENTS_H.PRP_ID as DocNo,PRP_BSU_ID as BSU_ID,PREPAYMENTS_H.PRP_FYEAR as FYear," & _
                    " PREPAYMENTS_H.PRP_PARTY as P_Party, ACCOUNTS_M.ACT_NAME as PARTYNAME,PREPAYMENTS_H.PRP_PREPAYACC " & _
                    " as P_PreAcc, ACCOUNTS_M_1.ACT_NAME as PREPAYACCNAME, PREPAYMENTS_H.PRP_EXPENSEACC as EXPENSEACC," & _
                    " ACCOUNTS_M_2.ACT_NAME as EXPACCNAME, PREPAYMENTS_H.PRP_DOCDT as DOCDT, PREPAYMENTS_H.PRP_AMOUNT as " & _
                    " P_Amt,PREPAYMENTS_H.PRP_CUR_ID as Cur,PREPAYMENTS_H.Prp_NOOFINST as NOOFINST,PREPAYMENTS_H.PRP_bPosted " & _
                    " as P_bPost,PREPAYMENTS_H.PRP_bDeleted as PRP_bDeleted FROM PREPAYMENTS_H left JOIN ACCOUNTS_M ON PREPAYMENTS_H.PRP_PARTY = ACCOUNTS_M.ACT_ID " & _
                    " INNER JOIN ACCOUNTS_M AS ACCOUNTS_M_1 ON PREPAYMENTS_H.PRP_PREPAYACC = ACCOUNTS_M_1.ACT_ID INNER JOIN " & _
                    " ACCOUNTS_M AS ACCOUNTS_M_2 ON PREPAYMENTS_H.PRP_EXPENSEACC = ACCOUNTS_M_2.ACT_ID)a " & _
                    " where isnull(a.PRP_bDeleted,0)=0 and a.BSU_ID= '" & Session("sBsuid") & "' and a.DocNo<>'' AND A.FYEAR =" & Session("F_YEAR")


            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox

            Dim str_search As String


            Dim str_txtDocNo As String = String.Empty
            Dim str_txtYear As String = String.Empty
            Dim str_txtCreditor As String = String.Empty
            Dim str_txtprepaid As String = String.Empty
            Dim str_txtExp As String = String.Empty
            Dim str_txtDate As String = String.Empty
            Dim str_txtCurrency As String = String.Empty
            Dim str_txtAmount As String = String.Empty
            Dim str_txtNoInst As String = String.Empty



            If gvPrePayment.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvPrePayment.HeaderRow.FindControl("txtDocNo")
                str_txtDocNo = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_Docno = " AND a.docno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Docno = " AND a.docno NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Docno = " AND a.docno LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Docno = " AND a.docno NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Docno = " AND a.docno LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Docno = " AND a.docno NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtCreditor")
                str_txtCreditor = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Party = "  AND  NOT isnull(a.PARTYNAME,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Party = " AND isnull(a.PARTYNAME,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtprepaid")

                str_txtprepaid = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_prepay = "  AND  NOT a.PREPAYACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_prepay = " AND a.PREPAYACCNAME NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtExp")

                str_txtExp = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Exp = " AND a.EXPACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Exp = "  AND  NOT a.EXPACCNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Exp = " AND a.EXPACCNAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Exp = " AND a.EXPACCNAME  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Exp = " AND a.EXPACCNAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Exp = " AND a.EXPACCNAME NOT LIKE '%" & txtSearch.Text & "'"
                End If




                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtDate")

                str_txtDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Date = " AND a.DOCDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Date = "  AND  NOT a.DOCDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Date = " AND a.DOCDT  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Date = " AND a.DOCDT  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Date = " AND a.DOCDT LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Date = " AND a.DOCDT NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_8.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtAmount")

                str_txtAmount = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Amt = " AND a.P_Amt LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Amt = "  AND  NOT a.P_Amt LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Amt = " AND a.P_Amt  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Amt = " AND a.P_Amt  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Amt = " AND a.P_Amt LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Amt = " AND a.P_Amt NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_9.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvPrePayment.HeaderRow.FindControl("txtNoInst")

                str_txtNoInst = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_NoInst = " AND a.NOOFINST LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_NoInst = "  AND  NOT a.NOOFINST LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_NoInst = " AND a.NOOFINST  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_NoInst = " AND a.NOOFINST  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_NoInst = " AND a.NOOFINST LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_NoInst = " AND a.NOOFINST NOT LIKE '%" & txtSearch.Text & "'"
                End If


            End If



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_Docno & str_filter_Party & str_filter_prepay & str_filter_Exp & str_filter_Date & str_filter_Amt & str_filter_NoInst & Bpost & "  order by DOCDT desc, a.DocNO desc")
            gvPrePayment.DataSource = ds.Tables(0)
            Dim ArrNext As String() = New String(ds.Tables(0).Rows.Count()) {}


            Session("ArrNext") = ArrNext
            If ds.Tables(0).Rows.Count > 0 Then


                gvPrePayment.DataBind()
                gvPrePayment.SelectedIndex = p_sindex
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPrePayment.DataBind()
                Dim columnCount As Integer = gvPrePayment.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPrePayment.Rows(0).Cells.Clear()
                gvPrePayment.Rows(0).Cells.Add(New TableCell)
                gvPrePayment.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPrePayment.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPrePayment.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvPrePayment.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = str_txtDocNo
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtyear")
            'txtSearch.Text = str_txtYear
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtCreditor")
            txtSearch.Text = str_txtCreditor
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtprepaid")
            txtSearch.Text = str_txtprepaid

            txtSearch = gvPrePayment.HeaderRow.FindControl("txtExp")
            txtSearch.Text = str_txtExp
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtDate")
            txtSearch.Text = str_txtDate
            'txtSearch = gvPrePayment.HeaderRow.FindControl("txtCurrency")
            'txtSearch.Text = str_txtCurrency
            txtSearch = gvPrePayment.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = str_txtAmount

            txtSearch = gvPrePayment.HeaderRow.FindControl("txtNoInst")
            txtSearch.Text = str_txtNoInst

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try



            Dim lblGuid As New Label
            Dim url As String
            Dim viewid As String

            lblGuid = TryCast(sender.FindControl("lblGuid"), Label)
            viewid = lblGuid.Text

            For y As Integer = 0 To Session("ArrNext").GetLength(0) - 1
                If Session("ArrNext")(y) = viewid Then
                    Session("NoNext") = y
                    Exit For
                End If
            Next


            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Request.QueryString("MainMnu_code")
            Dim Mnucode As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Accounts\accPrepaymentsAddEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            If Mnucode = "A150300" Then
                url = String.Format("~\Accounts\accPrePaymentsCancel.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            End If


            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

     

    Protected Sub lbSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            gvChild.Visible = True

            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGUID As New Label
                Dim lblSlno As New Label

                '        Dim lblGrpCode As New Label
                lblGUID = TryCast(sender.FindControl("lblGUID"), Label)

                Dim i As Integer = sender.parent.parent.RowIndex
                gridbind(i)
                str_Sql = "select * FROM PREPAYMENTS_H where GUID='" & lblGUID.Text & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then

                    str_Sql = "SELECT     VOUCHER_D_S.VDS_DOCTYPE, VOUCHER_D_S.VDS_DOCNO, ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, " _
                    & " CASE isnull(VOUCHER_D_S.VDS_CODE, '') WHEN '' THEN 'GENERAL' ELSE COSTCENTER_S.CCS_DESCR END AS GRPFIELD, " _
                    & " COSTCENTER_S.CCS_DESCR, VOUCHER_D_S.VDS_CODE,VOUCHER_D_S.vds_descr, VOUCHER_D_S.VDS_AMOUNT, COSTCENTER_S.CCS_QUERY " _
                    & " FROM  PREPAYMENTS_H INNER JOIN " _
                    & " ACCOUNTS_M ON ACCOUNTS_M.ACT_ID = PREPAYMENTS_H.PRP_EXPENSEACC LEFT OUTER JOIN " _
                    & " VOUCHER_D_S ON PREPAYMENTS_H.PRP_SUB_ID = VOUCHER_D_S.VDS_SUB_ID AND " _
                    & " PREPAYMENTS_H.PRP_BSU_ID = VOUCHER_D_S.VDS_BSU_ID AND PREPAYMENTS_H.PRP_FYEAR = VOUCHER_D_S.VDS_FYEAR AND " _
                    & " PREPAYMENTS_H.PRP_ID = VOUCHER_D_S.VDS_DOCNO and (VOUCHER_D_S.VDS_DOCTYPE = 'PP')  AND (VOUCHER_D_S.VDS_DOCNO = '" & ds.Tables(0).Rows(0)("PRP_ID") & "') AND  (VOUCHER_D_S.VDS_SUB_ID = '" & ds.Tables(0).Rows(0)("PRP_SUB_ID") & "') AND " _
                    & " (VOUCHER_D_S.VDS_BSU_ID = '" & ds.Tables(0).Rows(0)("PRP_BSU_ID") & "') AND (VOUCHER_D_S.VDS_FYEAR = '" & ds.Tables(0).Rows(0)("PRP_FYEAR") & "') " _
                    & "  LEFT OUTER JOIN  COSTCENTER_S ON VOUCHER_D_S.VDS_CCS_ID = COSTCENTER_S.CCS_ID " _
                    & " WHERE ( PREPAYMENTS_H.PRP_ID= '" & ds.Tables(0).Rows(0)("PRP_ID") & "')  ORDER BY GRPFIELD"




                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvChild.DataSource = dsc
                    'gvChild.DataBind()

                    Dim helper As GridViewHelper
                    helper = New GridViewHelper(gvChild, True)
                    helper.RegisterGroup("GRPFIELD", True, True)
                    'helper.RegisterGroup("5", True, True)

                    helper.RegisterSummary("VDS_AMOUNT", SummaryOperation.Sum, "GRPFIELD")
                    AddHandler helper.GroupSummary, AddressOf helper_GroupSummary

                    gvChild.DataBind()
                    helper.ApplyGroupSort()

                Else



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = UtilityObj.getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = UtilityObj.getErrorMessage("1000")
        End Try

    End Sub
    Private Sub helper_GroupSummary(ByVal groupName As String, ByVal values As Object(), ByVal row As GridViewRow)
        Try

            row.Cells(0).HorizontalAlign = HorizontalAlign.Right
            row.Cells(0).Text = "Total Amount:-"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvPrePayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPrePayment.SelectedIndexChanged
        gvPrePayment.SelectedRow.BackColor = System.Drawing.Color.Blue
    End Sub
    Protected Sub gvPrePayment_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvPrePayment.SelectedIndexChanging
        gvPrePayment.SelectedRow.BackColor = System.Drawing.Color.Blue

    End Sub
    Protected Sub btnSearchDocNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchYear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchCreditor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchPrepaid_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchExp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchCurrency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchAmount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchNoInst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvPrePayment_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrePayment.PageIndexChanging
        gvPrePayment.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub rbpost_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbpost.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbunpost_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbunpost.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub
    Private x As Integer = 0
    Protected Sub gvPrePayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblGuid As New Label
            lblGuid = TryCast(e.Row.FindControl("lblGuid"), Label)
            Session("ArrNext")(x) = lblGuid.Text
            x = x + 1
        End If
    End Sub

    Public Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim grdRow As GridViewRow
            If sender.Parent IsNot Nothing Then
                grdRow = sender.Parent.Parent
                If grdRow IsNot Nothing Then
                    Dim lblGuid As Label
                    lblGuid = grdRow.FindControl("lblGuid")
                    Dim lblDocNo As Label
                    lblDocNo = grdRow.FindControl("lblDocNo")
                    If lblDocNo Is Nothing Then Exit Sub
                    Dim repSource As New MyReportClass
                    repSource = VoucherReports.PrePaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "PP", lblDocNo.Text, lblGuid.Text, Session("HideCC"))
                    Session("ReportSource") = repSource
                    ReportLoadSelection()
                    '   Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
