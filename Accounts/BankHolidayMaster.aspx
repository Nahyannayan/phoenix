﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BankHolidayMaster.aspx.vb" Inherits="Accounts_BankHolidayMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div>
        <asp:ValidationSummary ID="MainError" runat="server" ValidationGroup="dateval" CssClass="error" />
        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
        <div class="card mb-3">
            <div class="card-header letter-space">
                <i class="fa fa-book mr-3"></i>
                Bank Holidays
            </div>
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <table style="width: 100%;">
                        <%--<tr class="subheader_img" valign="top">
                <td valign="middle" style="height: 19px" colspan="4">
                </td>
            </tr>--%>
                        <tr>
                            <td style="text-align: left;"><span class="field-label">Country</span></td>
                            <td style="text-align: left;" >
                                <asp:DropDownList ID="ddlCountry" runat="server"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><span class="field-label">From</span></td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtFDT" runat="server" 
                                     CausesValidation="True" ValidationGroup="dateval" TabIndex="4"></asp:TextBox>&nbsp;
                            <asp:ImageButton ID="imgSelectFDT" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="6" />
                                <ajaxToolkit:CalendarExtender ID="ceimgfdt" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgSelectFDT" TargetControlID="txtFDT">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="cetxtfdt" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="txtFDT" TargetControlID="txtFDT">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RegularExpressionValidator ID="revTodate" runat="server" ControlToValidate="txtFDT"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/MMM/yyyy e.g.  13/SEP/2016"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="dateval" EnableTheming="True">*</asp:RegularExpressionValidator></td>
                            <td style="text-align: left;"><span class="field-label">To</span></td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtTDT" runat="server" 
                                   CausesValidation="True" ValidationGroup="dateval" TabIndex="4"></asp:TextBox>&nbsp;
                            <asp:ImageButton ID="imgTDT" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="6" />
                                <ajaxToolkit:CalendarExtender ID="ceimgtdt" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgTDT" TargetControlID="txtTDT">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="cetxttdt" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                    PopupButtonID="txtTDT" TargetControlID="txtTDT">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTDT"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/MMM/yyyy e.g.  13/SEP/2016"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="dateval" EnableTheming="True">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><span class="field-label">Event</span></td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtComments" MaxLength="300" TextMode="MultiLine" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtComments"
                                    ErrorMessage="Comments Cannot be Blank" ValidationGroup="dateval">*</asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center;">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="32"
                                    ValidationGroup="dateval" />
                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" TabIndex="32"
                                    CausesValidation="False" />
                                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Delete" ValidationGroup="dateval" TabIndex="34" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="38"
                                    UseSubmitBehavior="False" CausesValidation="False" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

