<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accPrePaymentsPost.aspx.vb" Inherits="accPrePaymentsPost" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <!--oncontextmenu="return false"-->
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
        function fnVoucherMSg() {
            if (document.getElementById('<%=chkPrint.ClientID %>').checked == true) {
                var curr_elem;
                var countChecked;
                countChecked = 0;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];
                    if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0) && (curr_elem.name != '')) {
                        if (curr_elem.checked)
                            countChecked = countChecked + 1;
                    }
                }
                if (countChecked > 1)
                    return confirm('Only first voucher will be printed(Multiple vouchers are selected)');
                else
                    return true;
            }
        }
        Sys.Application.add_load(
               function CheckForPrint() {
                   if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                       document.getElementById('<%= h_print.ClientID %>').value = '';
                       radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
                   }
               }
                    );





               function monthDetails(url) {

                   var NameandCode;
                   var result;
                   result = radopen("accprepaymentmonthwise.aspx?" + url, "pop_up2");
                   //if (result=='' || result==undefined)
                   //{
                   //return false;
                   //}

                   return false;
               }

               function autoSizeWithCalendar(oWindow) {
                   var iframe = oWindow.get_contentFrame();
                   var body = iframe.contentWindow.document.body;
                   var height = body.scrollHeight;
                   var width = body.scrollWidth;
                   var iframeBounds = $telerik.getBounds(iframe);
                   var heightDelta = height - iframeBounds.height;
                   var widthDelta = width - iframeBounds.width;
                   if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                   if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                   oWindow.center();
               }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Payment Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <table align="center" width="100%">
                                <tr valign="top">
                                    <td align="center" width="100%">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Vouchers for posting"
                                            Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Doc No">
                                                    <EditItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("DocNo") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblDocNoH" runat="server" Text="Doc No"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchDocNo" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchDocNo_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FYEAR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblYear" runat="server" Text='<%# Bind("FYEAR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Creditors Account">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Partyname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblCreditorH" runat="server" Text="Creditor Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCreditor" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchCreditor" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchCreditor_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreditor" runat="server" Text='<%# Bind("Partyname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PrePaid Account" SortExpression="prepayaccName">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("prepayaccName") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblprepaidAccount" runat="server" Text="Prepaid Account"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtPrepaid" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchPrepaid" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchPrepaid_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrepaid" runat="server" Text='<%# Bind("prepayaccName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expense Account" SortExpression="EXPACCNAME">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("EXPACCNAME") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblExpH" runat="server" Text="Expense Account"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtExp" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchExp" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchExp_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExp" runat="server" Text='<%# Bind("EXPACCNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Doc Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("DocDt") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblDateH" runat="server" Text="Doc Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchDate" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchDate_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("DocDt", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Currency">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Cur") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("Cur") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("P_Amt") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblAmountH" runat="server" Text="Amount"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchAmount" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchAmount_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("P_Amt", "{0:#,0.00}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No. of Inst">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("NOOFINST") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblNoInstH" runat="server" Text="Months"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtNoInst" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchNoInst" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchNoInst_Click" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNoInst" runat="server" Text='<%# Bind("NOOFINST") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Post" SortExpression="P_bPost">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("P_bPost") %>' />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <input id="chkPosted" runat="server" checked='<%# Bind("P_bPost") %>' type="checkbox"
                                                            value='<%# Bind("GUID") %>' />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Post<input id="Checkbox2" onclick="fnSelectAll(this)" type="checkbox" />
                                                        &nbsp;
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbSummary" runat="server" CausesValidation="False" OnClick="lbSummary_Click">Summary</asp:LinkButton>
                                                        <asp:LinkButton ID="lbview" runat="server" OnClick="lbview_Click">View</asp:LinkButton>
                                                        <asp:LinkButton ID="lbMonthWise" runat="server">MonthWise</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                        <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print Voucher" />
                                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" OnClientClick="return fnVoucherMSg();" />&nbsp;<br />
                                        <br />
                                        <br />
                                        <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False" EmptyDataText="THERE IS NO ALLOCATION FOR CURRENTLY SELECTED ACCOUNT"
                                            Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" SortExpression="ACT_NAME">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="VDS_SLNO" HeaderText="Slno" SortExpression="VDS_SLNO"
                                                    Visible="False" />
                                                <asp:BoundField DataField="CCS_DESCR" HeaderText="Cost Center" SortExpression="CCS_DESCR">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="vds_descr" HeaderText="Name" SortExpression="vds_descr">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="VDS_AMOUNT" HeaderText="Amount" NullDisplayText="0" SortExpression="VDS_AMOUNT" DataFormatString="{0:#,0.00}" HtmlEncode="False">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="GRPFIELD" HeaderText="Cost Center" SortExpression="GRPFIELD">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>



                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_9" runat="server" type="hidden" value="=" />






                <asp:HiddenField ID="h_print" runat="server" />
                <input id="h_FirstVoucher" runat="server" type="hidden" /><br />

            </div>
        </div>
    </div>
</asp:Content>

