<%@ Page Language="VB" AutoEventWireup="false" CodeFile="accShowEXPP.aspx.vb" Inherits="ShowEXPP" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <base target="_self" />
    <link rel="stylesheet" type="text/css" href="../cssfiles/sb-admin.css" />
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.css" />
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="../vendor/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
     <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> --%>

    <script language="javascript" type="text/javascript">
        function return_value(retval) {
            window.returnValue = retval;
            window.close();
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>

</head>
<body onload="listen_window();" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
    <form id="form1" runat="server">

        <div>
            <div>
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <input type="hidden" id="h_SelectedId" runat="server" value="0" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        </td>

                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="center">
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <table align="center" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvEmpInfo" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row" AllowPaging="True" DataKeyNames="ID">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Employee ID" SortExpression="EMP_ID">
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>' CssClass="matters"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblID" runat="server" EnableViewState="False" Text="Business ID"></asp:Label><br />
                                                                    <asp:TextBox ID="txtcode" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpId_Click" />
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblName" runat="server" Text="Business Unit Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpName_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" OnClick="linklblBSUName_Click" CommandName="Selected"
                                                                        Text='<%# Eval("E_Name") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ID2" Visible="False">
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID2" runat="server" Text='<%# Eval("ID2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ID3" Visible="False">
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID3" runat="server" Text='<%# Eval("ID3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle />
                                                        <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"></td>
                    </tr>
                </table>
            </div>
            <table width="100%">
            </table>
        </div>



    </form>

</body>
</html>
