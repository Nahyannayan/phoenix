<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accAdjJournalAdd.aspx.vb" Inherits="Accounts_accAdjJournalAdd" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function GetJouranlDetails() {
            var sFeatures;
            var sFeatures;
            sFeatures = 'dialogWidth: 900px; dialogHeight: 475px; help: no; ';
            sFeatures += ' resizable: no; scroll: yes; status: no; unadorned: no; ';
            var NameandCode;
            var result, url;
            url = '../Common/PopupFromIDHiddenSeven.aspx?id=JOURNALLIST&multiSelect=true&bsu=' + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
            result = radopen(url, "pop_up2")
            //result = window.showModalDialog(url, "", sFeatures)
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=hfJNL_ID.ClientID %>').value = result;

            }
            return true--%>
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfJNL_ID.ClientID%>').value = arg.NameandCode;
                //alert(arg.NameandCode);
                //document.forms[0].submit();
                //__doPostBack('<%= hfJNL_ID.ClientID%>', 'ValueChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Adjustment Journal
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="CHQ_CANCEL" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" DataSourceID="sdsBusinessunit"
                                DataTextField="BSU_NAME" DataValueField="BSU_ID" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList><asp:SqlDataSource ID="sdsBusinessunit" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="select BSU_ID , BSU_NAME from [fn_GetBusinessUnits]   (@Usr_name) WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME">
                                <SelectParameters>
                                    <asp:SessionParameter Name="Usr_name" SessionField="sUsr_name" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Voucher</span></td>
                        <td align="left" colspan="3">
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetJouranlDetails();return false;" OnClick="imgBankSel_Click" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Details</span></td>
                        <td align="left" colspan="3">
                            <asp:GridView ID="gvJouranls" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                <Columns>
                                    <%--                        <asp:BoundField DataField="Bank" HeaderText="Bank" ReadOnly="True" ></asp:BoundField>--%>
                                    <asp:BoundField DataField="JHD_DOCNO" HeaderText="Doc. No"></asp:BoundField>
                                    <asp:BoundField DataField="JHD_DOCDT" HeaderText="Date" />
                                    <asp:BoundField DataField="JNL_CUR_ID" HeaderText="Currency" />
                                    <asp:BoundField DataField="Account" HeaderText="Account"></asp:BoundField>
                                    <asp:BoundField DataField="JNL_NARRATION" HeaderText="Narration"
                                        ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="JNL_DEBIT" HeaderText="Debit"
                                        DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="JNL_CREDIT" HeaderText="Credit"
                                        DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Doc Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server"
                                ImageUrl="~/Images/calendar.gif" /></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblVoucherDate" runat="server"></asp:Label>
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtdocDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="DocDate0" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtdocDate" TargetControlID="txtdocDate">
                            </ajaxToolkit:CalendarExtender>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Report</span> </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportType" runat="server"
                                DataSourceID="sdsReportType" DataTextField="RSM_DESCR"
                                DataValueField="RSM_TYP">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsReportType" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                SelectCommand=" SELECT RSM_TYP, RSM_DESCR FROM RPTSETUP_M  "></asp:SqlDataSource>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">To Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBusinessunitOpp" runat="server" DataSourceID="sdsBusinessunitOpp"
                                DataTextField="BSU_NAME" DataValueField="BSU_ID"
                                AutoPostBack="False">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsBusinessunitOpp" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                SelectCommand="select BSU_ID , BSU_NAME from [fn_GetBusinessUnits]   (@Usr_name) WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME">
                                <SelectParameters>
                                    <asp:SessionParameter Name="Usr_name" SessionField="sUsr_name" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtNarration" runat="server" TextMode="MultiLine" TabIndex="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="CHQ_CANCEL" />&nbsp;<asp:Button
                                ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfJNL_ID" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>


