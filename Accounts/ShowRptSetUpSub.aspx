<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowRptSetUpSub.aspx.vb" Inherits="Accounts_ShowRptSetUpSub" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <base target="_self" />

    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body onload="listen_window()" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
        <table width="98%" id="tbl" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="RSB_ID" EmptyDataText="No Data" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Account Code" SortExpression="RSS_ID">
                                <HeaderTemplate>
                                    Code
                                    <br />
                                    <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("RSB_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Name" SortExpression="Description">
                                <HeaderTemplate>
                                    Description
                                    <br />
                                    <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("RSB_DESCRIPTION") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="RSS_RFS_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblRSS_RFS_ID" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    </form>
</body>
</html>
