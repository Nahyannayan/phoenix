<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AccPostBankTran.aspx.vb" Inherits="AccPostBankTran" Title="Post Bank" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0) && !(curr_elem.name.search(/ChkPrintNotice/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
        function fnVoucherMSg() {
            if (document.getElementById('<%=chkPrint.ClientID %>').checked == true) {
                var curr_elem;
                var countChecked;
                countChecked = 0;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];
                    if ((curr_elem.type == 'checkbox') && (curr_elem.name.search(/chkPosted/) > 0) && (curr_elem.name != '')) {
                        if (curr_elem.checked)
                            countChecked = countChecked + 1;
                    }
                }
                if (countChecked > 1)
                    return confirm('Only first voucher will be printed(Multiple vouchers are selected)');
                else
                    return true;
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        Sys.Application.add_load(
               function CheckForPrint() {
                   if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                   document.getElementById('<%= h_print.ClientID %>').value = '';
                   radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
                   if (document.getElementById('<%= h_SecondTime.ClientID %>').value == '') {
                               document.getElementById('<%= h_SecondTime.ClientID %>').value = 'True';
                               document.forms[0].submit();
                           }
                       }
           }
                    );
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <input id="h_FirstVoucher" runat="server" type="hidden" /></td>
                    </tr>
                </table>
                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                    <%--<tr class="subheader_BlueTableView">
            <th align="left" colspan="9" valign="middle" style="height: 19px">
                </th>
        </tr>--%>
                    <tr valign="top">
                        <td align="center">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Vouchers for posting" Width="100%">
                                <Columns>
                                    <asp:BoundField ReadOnly="True" DataField="VHH_DOCNO" SortExpression="VHH_DOCNO" HeaderText="Doc No">
                                        <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="VHH_DOCDT" SortExpression="VHH_DOCDT" HeaderText="Doc Date">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="VHH_NARRATION" SortExpression="VHH_NARRATION" HeaderText="Narration">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Account" HeaderText="Account">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="VHH_CUR_ID" HeaderText="Currency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHH_AMOUNT")) %>' __designer:wfdid="w4"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="VHH_bPOSTED" HeaderText="Post">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Post<input id="chkSelectall" type="checkbox" onclick="fnSelectAll(this)" />

                                        </HeaderTemplate>

                                        <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" checked='<%# Bind("VHH_bPOSTED") %>' type="checkbox"
                                                value='<%# Bind("GUID") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" CausesValidation="false" CommandName="View"
                                                OnClick="lbView_Click" Text="Summary" Visible="false"></asp:LinkButton>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="GUID" Visible="False" HeaderText="GUID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" __designer:wfdid="w2" Enabled="False">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:CheckBox ID="ChkPrintNotice" runat="server" Text="Print Cheque Forwarding Letter" OnCheckedChanged="ChkPrintNotice_CheckedChanged"></asp:CheckBox>
                            <asp:CheckBox ID="chkPrint" runat="server" Text="Print Voucher" Checked="True" />
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" OnClientClick="return fnVoucherMSg();" />
                            <br />
                            <br />
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="VHD_DOCNO" HeaderText="Document No" SortExpression="VHD_DOCNO" />
                                    <asp:BoundField DataField="VHD_NARRATION" HeaderText="Narration" SortExpression="VHD_NARRATION" />
                                    <asp:BoundField DataField="VHD_FYEAR" HeaderText="Year" SortExpression="VHD_FYEAR" />
                                    <asp:BoundField DataField="VHD_ACT_ID" HeaderText="Account No" SortExpression="VHD_ACT_ID" />
                                    <asp:TemplateField HeaderText="Debit">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHD_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbViewChild" runat="server" CausesValidation="false" CommandName=""
                                                OnClick="lbViewChild_Click" Text="View"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GUID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="slno" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSlno" runat="server" Text='<%# Bind("VHD_LINEID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" EmptyDataText="THERE IS NO ALLOCATION FOR CURRENTLY SELECTED ACCOUNT"
                                Width="100%" Visible="False">
                                <Columns>
                                    <asp:BoundField DataField="ACT_ID" HeaderText="Account No" SortExpression="ACT_ID" />
                                    <asp:BoundField DataField="VDS_SLNO" HeaderText="Slno" SortExpression="VDS_SLNO"
                                        Visible="False" />
                                    <asp:BoundField DataField="CCS_DESCR" HeaderText="Cost Center" SortExpression="CCS_DESCR" />
                                    <asp:BoundField DataField="VDS_DESCR" HeaderText="Sub Cost Center" />
                                    <asp:BoundField DataField="VDS_CODE" HeaderText="CODE" SortExpression="VDS_CODE"
                                        Visible="False" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VDS_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GRPFIELD" HeaderText="Cost Center" SortExpression="GRPFIELD" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_SecondTime" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
