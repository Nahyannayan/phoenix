<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accPDCSelectAccounts.aspx.vb" Inherits="Accounts_accPDCSelectAccounts" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function get_Interest() {
            //popUp('460', '400', 'INTRAC', '<%=txtIntr.ClientId %>', '<%=txtIntrDescr.ClientId %>')
            //result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            var result = radopen("PopUp.aspx?ShowType=INTRAC&codeorname=" + document.getElementById('<%=txtIntr.ClientID%>').value, "pop_up2")

        }
        function get_AcrdInterest() {
            //popUp('460', '400', 'ACRDAC', '<%=txtAcrd.ClientId %>', '<%=txtAcrdDescr.ClientId %>');
            //result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            var result = radopen("PopUp.aspx?ShowType=ACRDAC&codeorname=" + document.getElementById('<%=txtAcrd.ClientID%>').value, "pop_up3")

        }
        function get_Prepaid() {
            //popUp('460', '400', 'PREPDAC', '<%=txtPrepd.ClientId %>', '<%=txtPrepdDescr.ClientId %>', '<%=txtChqiss.ClientId %>', '<%=txtChqissDescr.ClientId %>');
            //result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            var result = radopen("PopUp.aspx?ShowType=PREPDAC&codeorname=" + document.getElementById('<%=txtPrepd.ClientID%>').value, "pop_up4")
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl).value = lstrVal[0];
            //document.getElementById(ctrl1).value = lstrVal[1];
            //if (ctrl2 != '')
            //    if (document.getElementById(ctrl2).value == '') {
            //        document.getElementById(ctrl2).value = lstrVal[2];
            //        document.getElementById(ctrl3).value = lstrVal[3];
            //    }
        }
        function get_Chqiss() {
            //popUp('460', '400', 'CHQISSAC_PDC', '<%=txtChqiss.ClientId %>', '<%=txtChqissDescr.ClientId %>', '<%=txtPrepd.ClientId %>', '<%=txtPrepdDescr.ClientId %>')
            //result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
            var result = radopen("PopUp.aspx?ShowType=CHQISSAC_PDC&codeorname=" + document.getElementById('<%=txtChqiss.ClientID%>').value, "pop_up5")
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl).value = lstrVal[0];
            //document.getElementById(ctrl1).value = lstrVal[1];
            //if (document.getElementById(ctrl2).value == '') {
            //    document.getElementById(ctrl2).value = lstrVal[2];
            //    document.getElementById(ctrl3).value = lstrVal[3];
            //}
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtIntr.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtIntrDescr.ClientID%>').value = NameandCode[1];
            }
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtAcrd.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtAcrdDescr.ClientID%>').value = NameandCode[1];
            }
        }
        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtPrepd.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtPrepdDescr.ClientID%>').value = NameandCode[1];
                if (document.getElementById('<%=txtChqiss.ClientID%>') != '')
                    if (document.getElementById('<%=txtChqiss.ClientID%>').value == '') {                       
                        document.getElementById('<%=txtChqiss.ClientID%>').value = NameandCode[2];
                        document.getElementById('<%=txtChqissDescr.ClientID%>').value = NameandCode[3];
                    }
            }
        }

        function OnClientClose5(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtChqiss.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtChqissDescr.ClientID%>').value = NameandCode[1];
                if (document.getElementById('<%=txtPrepd.ClientID%>') != '')
                    if (document.getElementById('<%=txtPrepd.ClientID%>').value == '') {
                        document.getElementById('<%=txtPrepd.ClientID%>').value = NameandCode[2];
                        document.getElementById('<%=txtPrepdDescr.ClientID%>').value = NameandCode[3];
                    }
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            PDC Payment Group
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td id="Td1" runat="server" colspan="4" align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" AutoCompleteType="Disabled"
                                            TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Interest A/C</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtIntr" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgInterest" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Interest(); return false;" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtIntrDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Acrd Interest A/C</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAcrd" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgAcrdint" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_AcrdInterest(); return false;" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAcrdDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Prepaid Exp A/C</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPrepd" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgPrepaid" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Prepaid(); return false;" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPrepdDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Chq Issues A/C</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtChqiss" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgChqiss" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Chqiss(); return false;" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtChqissDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

