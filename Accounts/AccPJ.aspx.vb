Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class AccPJ
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim lstrErrMsg As String
    Dim BSU_IsTAXEnabled As Boolean = False
    Dim BSU_IsCOSTALLOCATION_REQUIRE As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbUploadEmployee)
        Session("BANKTRAN") = "PJ"
        Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If Page.IsPostBack = False Then
            Try

                'TAX CODE
                Dim ds7 As New DataSet
                Dim pParms0(2) As SqlClient.SqlParameter
                pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms0(1).Value = Session("sBSUID")
                ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
                BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))
                ViewState("BSU_IsTAXEnabled") = BSU_IsTAXEnabled
                If BSU_IsTAXEnabled = True Then
                    trTaxType.Visible = True
                    trTdsType.Visible = False
                    LOAD_TAX_CODES()
                    LOAD_TDS_CODES()
                    LOAD_TAX_TDS_CODES()
                    lblNet.Visible = True
                    txtNetTotalAmt.Visible = True
                Else
                    lblNet.Visible = False
                    txtNetTotalAmt.Visible = False
                End If
                'TAX CODE

                'usrCostCenter1.TotalAmountControlName = "txtDAmount"
                '   --- For Checking Rights And Initilize The Edit Variables 
                Session("CHECKLAST") = 0
                txtNarrn.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtNarrn.ClientID & "');")
                txtLineNarrn.Attributes.Add("onBlur", "narration_check('" & txtLineNarrn.ClientID & "');")
                LockControls()
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Session("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                If (Session("datamode") = "add") Then
                    Call Clear_Details()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Session("gintGridLine") = 1
                    GridInitialize()
                    Session("gDtlDataMode") = "ADD"

                    txtdocDate.Text = GetDiplayDate()
                    If (Session("datamode") = "add") Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), CDate(txtdocDate.Text).Month, CDate(txtdocDate.Text).Year).ToString
                    End If
                    bind_Currency()
                    Session("dtDTL") = DataTables.CreateDataTable_PJ()
                    Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
                    Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
                    Session("idCostChild") = 0
                End If
                If Session("datamode") = "view" Then
                    Session("Eid") = Convert.ToString(Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+")))
                End If

                If USR_NAME = "" Or (Session("MainMnu_code") <> "A150010" And Session("MainMnu_code") <> "A150021" And Session("MainMnu_code") <> "A200017") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                    Page.Title = OASISConstants.Gemstitle
                    If Session("datamode") = "view" Then
                        '   --- Fill Data ---
                        bind_Currency()
                        FillValues()
                    End If
                End If
                '   --- Checking End  ---
                gvDTL.Attributes.Add("bordercolor", "#1b80b6")
                Session("gDtlDataMode") = "ADD"
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        Else
            If (Session("datamode") = "add") Then

                txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                Session("SessDocDate") = txtdocDate.Text
            End If
            BSU_IsTAXEnabled = ViewState("BSU_IsTAXEnabled")
        End If
        If Session("datamode") <> "add" Then
            btnEdit.Enabled = Master.CheckPosted(Session("BANKTRAN"), txtdocNo.Text)
            ImageButton1.Enabled = False
            txtdocDate.Attributes.Remove("readonly")
            txtdetInvoiceDate.Attributes.Remove("readonly")
        Else
            ControlReadOnly(False)
            ImageButton1.Enabled = True
        End If
    End Sub
    Protected Sub OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 2
        pParms(1) = New SqlClient.SqlParameter("@TAX_TDS_CODE", SqlDbType.VarChar, 20)
        pParms(1).Value = ddlTaxTds.SelectedValue
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_TDS_CODES]", pParms)

        If Not ds Is Nothing And ds.Tables(0).Rows.Count >= 1 Then
            ddlVATCode.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE")
            ddlTDS.SelectedValue = ds.Tables(0).Rows(0)("TDS_CODE")
        End If

    End Sub
    Sub LOAD_TAX_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_CODES]", pParms)

        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAXDESC"
        ddlVATCode.DataValueField = "TAXCODE"
        ddlVATCode.DataBind()

    End Sub
    Sub LOAD_TAX_TDS_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_TDS_CODES]", pParms)

        ddlTaxTds.DataSource = ds
        ddlTaxTds.DataTextField = "DESCR"
        ddlTaxTds.DataValueField = "CODE"
        ddlTaxTds.DataBind()

    End Sub
    Sub LOAD_TDS_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TDS_CODES]", pParms)

        ddlTDS.DataSource = ds
        ddlTDS.DataTextField = "TDS_DESCR"
        ddlTDS.DataValueField = "TDS_CODE"
        ddlTDS.DataBind()

    End Sub
    Private Sub LockControls()
        txtdocNo.Attributes.Add("readonly", "readonly")
        txtdrDescr.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        txtPartyDescr.Attributes.Add("readonly", "readonly")
        txtdebitdescr.Attributes.Add("readonly", "readonly")
        txtdocDate.Text = GetDiplayDate()
        txtInvoiceNo.Attributes.Add("onBlur", "javascript:CopyInvoiceNo()")
        txtLPO.Attributes.Add("onBlur", "javascript:CopyLPONo()")
        txtdetInvoiceDate.Text = String.Format("{0:dd/MMM/yyyy}", Session("EntryDate"))
    End Sub

    Private Sub bind_Currency()
        Try
            Dim dt As DataTable = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBSUID"), Session("BSU_CURRENCY"))
            cmbCurrency.DataSource = dt 'ds.Tables(0)
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()

            If dt.Rows.Count > 0 Then
                If set_default_currency() <> True Then
                    cmbCurrency.SelectedIndex = 0
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
            Else
            End If
            'getnextdocid()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In cmbCurrency.Items
                If String.Compare(item.Text, Session("BSU_CURRENCY"), True) = 0 Then
                    item.Selected = True
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Private Sub GridInitialize()
        If BSU_IsTAXEnabled Then
            gvDTL.Columns(16).Visible = True
        Else
            gvDTL.Columns(16).Visible = False
        End If

        gvDTL.DataBind()
    End Sub

    Public Sub btnFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFill.Click
        Dim i, lintIndex As Integer
        Dim ldrNew As DataRow

        If Session("datamode") = "view" Then
            lblError.Text = "Record not in edit mode !!!"
            Exit Sub
        End If

        lstrErrMsg = ""
        ' --- (1) VALIDATE THE TEXTBOXES
        ' --- (1) VALIDATE THE TEXTBOXES
        If Trim(Detail_ACT_ID.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Debit Account " & "<br>"
        End If

        If Trim(txtLineNarrn.Text) = "" Then
            lstrErrMsg = lstrErrMsg & "Enter the Narration " & "<br>"
        End If
        If (IsNumeric(txtQty.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Quantity" & "<br>"
        End If

        If (IsNumeric(txtrate.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Rate" & "<br>"
        End If
        If (IsNumeric(txtCreditDays.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Enter the Credit days" & "<br>"
        End If

        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = Truessssssss

            lblError.Text = "Please check the following errrors : " & "<br>" & lstrErrMsg
            Exit Sub
        End If

        ''''COST CENTER VERIFICATION


        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID AND AM.ACT_BANKCASH='N' " _
            & " AND AM.ACT_FLAG = 'N' AND ACT_ID='" & Detail_ACT_ID.Text & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdebitdescr.Text = ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                lblError.Text = "Invalid Detail Account"
                Exit Sub
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
        '''''FIND ACCOUNT IS THERE
        '   --- (1) END OF VALIDATION DURING ADD
        'Check CostCenter Summary
        RecreateSsssionDataSource()

        Dim ds7 As New DataSet
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms0(1).Value = Session("sBSUID")
        ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
        BSU_IsCOSTALLOCATION_REQUIRE = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsCOSTALLOCATION_REQUIRE"))
        If BSU_IsCOSTALLOCATION_REQUIRE = True Then
            If Not CostCenterFunctions.VerifyCostCenterAmount(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), _
                                                                 txtrate.Text * txtQty.Text) Then
                lblError.Text = "Invalid Cost Center Allocation!!!"
                Exit Sub
            End If
        End If
        '   --- (2) PROCEED TO SAVE IN GRID IF NO ERRORS FOUND
        If (lstrErrMsg = "") Then
            Try
                If Session("gDtlDataMode") = "ADD" Then

                    ldrNew = Session("dtDTL").NewRow
                    ldrNew("Id") = Session("gintGridLine")
                    If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                        CostCenterFunctions.AddCostCenter(Session("gintGridLine"), Session("sBsuid"), _
                                          Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                         Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                         Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    End If
                    Session("gintGridLine") = Session("gintGridLine") + 1
                    ldrNew("AccountId") = Trim(Detail_ACT_ID.Text)
                    ldrNew("AccountName") = Trim(txtdebitdescr.Text)
                    ldrNew("Item") = Trim(txtLineNarrn.Text)
                    ldrNew("Qty") = Convert.ToDecimal(Trim(txtQty.Text))
                    ldrNew("Rate") = Convert.ToDecimal(txtrate.Text.Trim)
                    ldrNew("Amount") = Convert.ToDecimal(Trim(txtrate.Text)) * Convert.ToDecimal(Trim(txtQty.Text))
                    ldrNew("Status") = ""
                    ldrNew("GUID") = System.DBNull.Value
                    ldrNew("InvNo") = Trim(txtdetInvoiceNo.Text)
                    ldrNew("InvDate") = Trim(txtdetInvoiceDate.Text)
                    ldrNew("LPONo") = Trim(txtdetLPONo.Text)
                    ldrNew("CreditDays") = Trim(txtCreditDays.Text)
                    ldrNew("CostReqd") = False
                    If BSU_IsTAXEnabled Then
                        ldrNew("TaxCode") = ddlVATCode.SelectedValue
                        ldrNew("TdsCode") = ddlTDS.SelectedValue
                        Dim ds09 As New DataSet
                        Dim pParms09(4) As SqlClient.SqlParameter
                        pParms09(0) = New SqlClient.SqlParameter("@PUD_TAX_CODE", SqlDbType.VarChar, 20)
                        pParms09(0).Value = ddlVATCode.SelectedValue.ToString
                        pParms09(1) = New SqlClient.SqlParameter("@PUD_QTY", SqlDbType.Decimal)
                        pParms09(1).Value = Convert.ToDecimal(Trim(txtQty.Text))
                        pParms09(2) = New SqlClient.SqlParameter("@PUD_RATE", SqlDbType.Decimal)
                        pParms09(2).Value = Convert.ToDecimal(txtrate.Text.Trim)
                        pParms09(3) = New SqlClient.SqlParameter("@PUD_TDS_CODE", SqlDbType.VarChar, 20)
                        pParms09(3).Value = ddlTDS.SelectedValue.ToString
                        ds09 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "[dbo].[GetTaxTdsVoucherDetails]", pParms09)
                        ldrNew("TaxAmount") = Convert.ToDecimal(ds09.Tables(0).Rows(0)("TAX_AMNT")) 'ddlVATCode.SelectedValue
                        ldrNew("TdsAmount") = Convert.ToDecimal(ds09.Tables(0).Rows(0)("TDS_AMT")) 'ddlTDS.SelectedValue
                    Else
                        ldrNew("TaxCode") = ""
                        ldrNew("TdsCode") = ""
                        ldrNew("TaxAmount") = 0 '""
                        ldrNew("TdsAmount") = 0 '""
                    End If

                    '   --- Check if these details are already there before adding to the grid
                    For i = 0 To Session("dtDTL").Rows.Count - 1
                        If Session("dtDTL").Rows(i)("Accountid") = ldrNew("Accountid") And _
                            Session("dtDTL").Rows(i)("Accountname") = ldrNew("Accountname") And _
                            Session("dtDTL").Rows(i)("InvNo") = ldrNew("InvNo") And _
                            Session("dtDTL").Rows(i)("LPONo") = ldrNew("LPONo") And _
                             Session("dtDTL").Rows(i)("Rate") = ldrNew("Rate") Then

                            lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                            'lblError.Visible = True
                            'GridBind()
                            Exit Sub
                        End If
                    Next
                    Session("dtDTL").Rows.Add(ldrNew)
                    Clear_Details_1()
                    GridBind()
                ElseIf (Session("gDtlDataMode") = "UPDATE") Then
                    For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                        If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then

                            If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                                CostCenterFunctions.AddCostCenter(Session("gintEditLine"), Session("sBsuid"), _
                                                  Session("CostOTH"), txtdocDate.Text, Session("idCostChild"), _
                                                 Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                                 Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                            End If

                            Session("dtDTL").Rows(lintIndex)("AccountId") = Trim(Detail_ACT_ID.Text)
                            Session("dtDTL").Rows(lintIndex)("AccountName") = Trim(txtdebitdescr.Text)
                            Session("dtDTL").Rows(lintIndex)("Item") = txtLineNarrn.Text
                            Session("dtDTL").Rows(lintIndex)("Qty") = txtQty.Text
                            Session("dtDTL").Rows(lintIndex)("Rate") = txtrate.Text
                            Session("dtDTL").Rows(lintIndex)("Amount") = Convert.ToDecimal(Trim(txtrate.Text)) * Convert.ToDecimal(Trim(txtQty.Text))
                            Session("dtDTL").Rows(lintIndex)("InvNo") = Trim(txtdetInvoiceNo.Text)
                            Session("dtDTL").Rows(lintIndex)("InvDate") = Trim(txtdetInvoiceDate.Text)
                            Session("dtDTL").Rows(lintIndex)("LPONo") = Trim(txtdetLPONo.Text)

                            Session("dtDTL").Rows(lintIndex)("CostReqd") = False
                            Session("dtDTL").Rows(lintIndex)("CreditDays") = Trim(txtCreditDays.Text)
                            If BSU_IsTAXEnabled Then
                                Session("dtDTL").Rows(lintIndex)("TaxCode") = ddlVATCode.SelectedValue
                                Session("dtDTL").Rows(lintIndex)("TdsCode") = ddlTDS.SelectedValue

                                Dim ds09 As New DataSet
                                Dim pParms09(4) As SqlClient.SqlParameter
                                pParms09(0) = New SqlClient.SqlParameter("@PUD_TAX_CODE", SqlDbType.VarChar, 20)
                                pParms09(0).Value = ddlVATCode.SelectedValue.ToString
                                pParms09(1) = New SqlClient.SqlParameter("@PUD_QTY", SqlDbType.Decimal)
                                pParms09(1).Value = Convert.ToDecimal(Trim(txtQty.Text))
                                pParms09(2) = New SqlClient.SqlParameter("@PUD_RATE", SqlDbType.Decimal)
                                pParms09(2).Value = Convert.ToDecimal(txtrate.Text.Trim)
                                pParms09(3) = New SqlClient.SqlParameter("@PUD_TDS_CODE", SqlDbType.VarChar, 20)
                                pParms09(3).Value = ddlTDS.SelectedValue.ToString
                                ds09 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "[dbo].[GetTaxTdsVoucherDetails]", pParms09)
                                Session("dtDTL").Rows(lintIndex)("TaxAmount") = Convert.ToDecimal(ds09.Tables(0).Rows(0)("TAX_AMNT")) 'ddlVATCode.SelectedValue
                                Session("dtDTL").Rows(lintIndex)("TdsAmount") = Convert.ToDecimal(ds09.Tables(0).Rows(0)("TDS_AMT")) 'ddlTDS.SelectedValue

                            Else
                                Session("dtDTL").Rows(lintIndex)("TaxCode") = ""
                                Session("dtDTL").Rows(lintIndex)("TdsCode") = ""

                                Session("dtDTL").Rows(lintIndex)("TaxAmount") = 0
                                Session("dtDTL").Rows(lintIndex)("TdsAmount") = 0
                            End If

                            'ToggleCols(True)
                            Session("gDtlDataMode") = "ADD"
                            btnFill.Text = "ADD"
                            gvDTL.SelectedIndex = -1
                            Clear_Details_1()
                            GridBind()
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception

            End Try
        End If
        '   --- (2) END OF SAVE IN GRID IF NO ERRORS FOUND
    End Sub

    Private Sub GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = DataTables.CreateDataTable_PJ()
        Dim total As Decimal
        Dim nettotal As Decimal

        If Session("dtDTL").Rows.Count > 0 Then
            Dim ldrTempNew As DataRow
            For i = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(i)("Status") <> "DELETED") Then
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("dtDTL").Columns.Count - 1
                        ldrTempNew.Item(Session("dtDTL").Columns(j).ToString()) = Session("dtDTL").Rows(i)(Session("dtDTL").Columns(j).ToString())
                    Next
                    total += Convert.ToDecimal(Session("dtDTL").Rows(i)("Amount"))
                    'nettotal += (Convert.ToDecimal(Session("dtDTL").Rows(i)("Amount")) + Convert.ToDecimal(Session("dtDTL").Rows(i)("TaxAmount")) + Convert.ToDecimal(Session("dtDTL").Rows(i)("TdsAmount")))
                    nettotal += (Convert.ToDecimal(Session("dtDTL").Rows(i)("Amount")) + Convert.ToDecimal(Session("dtDTL").Rows(i)("TaxAmount")) - Convert.ToDecimal(Session("dtDTL").Rows(i)("TdsAmount"))) ' without TDS amount 11AUG2020
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
            txtdetTotalAmt.Text = AccountFunctions.Round(total)
            txtNetTotalAmt.Text = AccountFunctions.Round(nettotal)
        End If
        gvDTL.DataSource = dtTempDtl
        gvDTL.DataBind()
        If Session("dtDTL").Rows.Count <= 0 Then
            GridInitialize()
        End If
        Clear_Details_1()
    End Sub

    Private Sub Clear_Details()
        Detail_ACT_ID.Text = ""
        txtdebitdescr.Text = ""
        txtLineNarrn.Text = ""
        txtQty.Text = ""
        txtrate.Text = ""
        txtdetInvoiceNo.Text = ""
        txtdetInvoiceDate.Text = txtdocDate.Text
        txtQty.Text = "1"
        txtdetLPONo.Text = ""
        txtdetTotalAmt.Text = ""
        txtNetTotalAmt.Text = ""
        ClearRadGridandCombo()
    End Sub

    Private Sub Clear_Details_1()
        txtQty.Text = "1"
        txtrate.Text = ""
        txtLineNarrn.Text = txtNarrn.Text
        txtdetInvoiceNo.Text = txtInvoiceNo.Text
        txtdetInvoiceDate.Text = txtdocDate.Text
        txtdetLPONo.Text = txtLPO.Text
        ClearRadGridandCombo()
    End Sub

    Private Sub Clear_Header()

        txtdocNo.Text = ""
        txtOldDocNo.Text = ""

        txtdocDate.Text = ""
        txtdrCode.Text = ""
        txtdrDescr.Text = ""
        txtInvoiceNo.Text = ""
        txtLPO.Text = ""
        txtNarrn.Text = ""
        'bind_Currency()
        txtPartyCode.Text = ""
        txtPartyDescr.Text = ""
    End Sub

    Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        Dim lintIndex As Integer = 0

        lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
        Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)

        For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                Detail_ACT_ID.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountId"))
                txtdebitdescr.Text = Trim(Session("dtDTL").Rows(lintIndex)("AccountName"))
                txtLineNarrn.Text = Trim(Session("dtDTL").Rows(lintIndex)("Item"))
                txtQty.Text = Trim(Session("dtDTL").Rows(lintIndex)("Qty"))
                txtrate.Text = AccountFunctions.Round(Session("dtDTL").Rows(lintIndex)("Rate"))
                txtdetInvoiceNo.Text = Trim(Session("dtDTL").Rows(lintIndex)("InvNo").ToString())
                If Session("dtDTL").Rows(lintIndex)("InvDate").ToString() <> "" Then
                    txtdetInvoiceDate.Text = Format(Session("dtDTL").Rows(lintIndex)("InvDate"), OASISConstants.DateFormat)
                Else
                    txtdetInvoiceDate.Text = ""
                End If
                If BSU_IsTAXEnabled Then
                    ddlVATCode.SelectedValue = Trim(Session("dtDTL").Rows(lintIndex)("TaxCode"))
                    ddlTDS.SelectedValue = Trim(Session("dtDTL").Rows(lintIndex)("TdsCode"))
                End If
                txtdetLPONo.Text = Trim(Session("dtDTL").Rows(lintIndex)("LPONo").ToString())
                txtCreditDays.Text = Trim(Session("dtDTL").Rows(lintIndex)("CreditDays").ToString())

                gvDTL.SelectedIndex = lintIndex
                gvDTL.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                gvDTL.SelectedRowStyle.ForeColor = Drawing.Color.Black
                Session("gDtlDataMode") = "UPDATE"
                btnFill.Text = "UPDATE"
                RecreateSsssionDataSource()
                CostCenterFunctions.SetGridSessionDataForEdit(Session("gintEditLine"), Session("dtCostChild"), _
                Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                usrCostCenter1.BindCostCenter()
                'ToggleCols(False)
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnFillCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFillCancel.Click
        'ToggleCols(True)
        Session("gDtlDataMode") = "ADD"
        btnFill.Text = "ADD"
        gvDTL.SelectedIndex = -1
        Clear_Details()
    End Sub

    Protected Sub DeleteRecordByID(ByVal pId As String)
        Dim iRemove As Integer = 0
        Dim j As Integer
        For iRemove = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iRemove)("id") = pId) Then
                Session("dtDTL").Rows(iRemove)("Status") = "DELETED"

                '   --- Remove The Corresponding Rows From The Detail Grid Also
                'For j As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                While j < Session("dtCostChild").Rows.Count
                    If (Session("dtCostChild").Rows(j)("VoucherId") = pId) Then
                        Session("dtCostChild").Rows(j)("Status") = "DELETED"
                        j = j + 1
                    Else
                        j = j + 1
                    End If
                End While
            End If
        Next
        GridBind()
    End Sub

    Protected Sub gvDTL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDTL.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Dim lblReqd As New Label
                'Dim lbAllocate As New LinkButton
                Dim lblid As New Label
                'Dim lblAmount As New Label

                'lblReqd = e.Row.FindControl("lblCostReqd")
                lblid = e.Row.FindControl("lblId")
                'lblAmount = e.Row.FindControl("lblAmount")
                'lbAllocate = e.Row.FindControl("lbAllocate")

                'If lblReqd IsNot Nothing Then
                '    If lblReqd.Text = "True" Then
                '        e.Row.BackColor = Drawing.Color.Pink
                '    End If
                'End If

                Dim gvCostchild As New GridView
                gvCostchild = e.Row.FindControl("gvCostchild")
                If gvCostchild IsNot Nothing Then
                    gvCostchild.Attributes.Add("bordercolor", "#fc7f03")
                    'ClientScript.RegisterStartupScript([GetType](), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" & lblid.Text & "','one');</script>")
                    If Not Session("dtCostChild") Is Nothing Then
                        Dim dv As New DataView(Session("dtCostChild"))
                        dv.RowFilter = "VoucherId='" & lblid.Text & "' and costcenter='" & e.Row.DataItem("Ply") & "'"
                        dv.Sort = "MemberId"
                        gvCostchild.DataSource = dv.ToTable
                        gvCostchild.DataBind()
                    End If
                End If


                Dim l As LinkButton = DirectCast(e.Row.FindControl("DeleteBtn"), LinkButton)
                If l IsNot Nothing Then
                    'Dim dAmt As Double
                    'If CDbl(lblAmount.Text) > 0 Then
                    '    dAmt = CDbl(lblAmount.Text)
                    'End If
                    'lbAllocate.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & lblid.Text & "');return false;"

                    l.Attributes.Add("onclick", "javascript:return " + "confirm('Are you sure you want to delete this record " + DataBinder.Eval(e.Row.DataItem, "id") + "')")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvDTL_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDTL.RowDeleting
        Dim categoryID As Integer = CInt(gvDTL.DataKeys(e.RowIndex).Value)
        DeleteRecordByID(categoryID)
    End Sub

    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM PURCHASE_H WHERE" _
            & " GUID='" & Session("Eid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("PUH_DOCDT"), "dd/MMM/yyyy")
                txtdocNo.Text = ds.Tables(0).Rows(0)("PUH_DOCNO")
                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("LockPurchase_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = Session("BANKTRAN")
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = txtdocNo.Text
                    cmd.Parameters.Add(sqlpJHD_DOCNO)



                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                    cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    Session("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = "Error"
                    End If
                    Return iReturnvalue
                Catch ex As Exception

                Finally
                    objConn.Close()
                End Try
                '''''''
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Return " | | "
        End Try
        Return True
    End Function

    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            If Session("Eid") <> "" Then
                str_Sql = "SELECT * FROM PURCHASE_H WHERE" _
               & " GUID='" & Session("Eid") & "'"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtdocDate.Text = Format(ds.Tables(0).Rows(0)("PUH_DOCDT"), "dd/MMM/yyyy")
                    txtdocNo.Text = ds.Tables(0).Rows(0)("PUH_DOCNO")
                    txtNarrn.Text = ds.Tables(0).Rows(0)("PUH_NARRATION")
                    Dim objConn As New SqlConnection(str_conn)

                    Try
                        objConn.Open()
                        Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                        sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                        cmd.Parameters.Add(sqlpVHH_SUB_ID)

                        Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                        sqlpsqlpBSUID.Value = Session("sBSUId")
                        cmd.Parameters.Add(sqlpsqlpBSUID)

                        Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                        sqlpVHH_FYEAR.Value = Session("F_YEAR")
                        cmd.Parameters.Add(sqlpVHH_FYEAR)

                        Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                        sqlpVHH_DOCTYPE.Value = Session("BANKTRAN")
                        cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                        Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                        sqlpVHH_DOCNO.Value = txtdocNo.Text
                        cmd.Parameters.Add(sqlpVHH_DOCNO)

                        Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                        sqlpVHH_CUR_ID.Value = Session.SessionID
                        cmd.Parameters.Add(sqlpVHH_CUR_ID)

                        Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                        sqlpVHH_USER.Value = Session("sUsr_name")
                        cmd.Parameters.Add(sqlpVHH_USER)

                        Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                        sqlopVHH_TIMESTAMP.Value = Session("str_timestamp")
                        cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                        Dim iReturnvalue As Integer
                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)
                        cmd.ExecuteNonQuery()

                        iReturnvalue = retValParam.Value
                        If iReturnvalue <> 0 Then
                            lblError.Text = getErrorMessage(iReturnvalue)
                        End If
                        Return iReturnvalue
                    Catch ex As Exception
                        Errorlog(ex.Message)
                    Finally
                        objConn.Close()
                    End Try
                Else
                End If
                Return " | | "
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function

    Protected Function SaveValidate() As Boolean
        Dim lstrErrMsg As String
        Dim iIndex As Integer
        Dim cIndex As Integer
        Dim lblnFound As Boolean

        lstrErrMsg = ""
        If Trim(txtdocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If

        If Trim(txtdocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If

        If Trim(txtNarrn.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter Narration " & "<br>"
        End If

        If Trim(txtPartyCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Bank " & "<br>"
        End If

        If Trim(txtdrCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Debit Account " & "<br>"
        End If

        'If Trim(txtLPO.Text = "") Then
        '    lstrErrMsg = lstrErrMsg & "Invalid LPO " & "<br>"
        'End If

        'If Trim(txtInvoiceNo.Text = "") Then
        '    lstrErrMsg = lstrErrMsg & "Invalid Invoice Number " & "<br>"
        'End If


        If Session("dtDTL").Rows.Count = 0 Then
            lstrErrMsg = lstrErrMsg & " Enter the Debit Details " & "<br>"
        End If

        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If (Session("dtDTL").Rows(iIndex)("CostReqd") = "True") Then
                lblnFound = False
                For cIndex = 0 To Session("dtCostChild").Rows.Count - 1
                    If Session("dtDTL").Rows(iIndex)("Id") = Session("dtCostChild").Rows(cIndex)("VoucherId") Then
                        lblnFound = True
                    End If
                Next
                If lblnFound = False Then
                    lstrErrMsg = lstrErrMsg & " Enter the mandatory cost center details " & "<br>"
                End If
            End If
        Next
        If (lstrErrMsg <> "") Then
            'tr_errLNE.Visible = True
            lstrErrMsg = "Please check the following errors" & "<br>" & lstrErrMsg
            lblError.Text = lstrErrMsg
            Return False
        Else
            'tr_errLNE.Visible = False
            Return True
        End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String

        Dim lblnNoErr As Boolean
        '   ----------------- VALIDATIONS --------------------

        txtPartyDescr.Text = AccountFunctions.Validate_Account(txtPartyCode.Text, Session("sbsuid"), "PARTY2")
        If txtPartyDescr.Text = "" Then
            lblError.Text = "Invalid Party Account Selected"
            Exit Sub
        Else
            lblError.Text = ""
        End If

        txtdebitdescr.Text = AccountFunctions.Validate_Account(Detail_ACT_ID.Text, Session("sbsuid"), "DEBIT")
        If txtdebitdescr.Text = "" Then
            lblError.Text = "Invalid Debit Account Selected"
            Exit Sub
        Else
            lblError.Text = ""
        End If

        lblnNoErr = SaveValidate()
        If (lblnNoErr = False) Then
            Exit Sub
        End If
        '   ----------------- END OG VALIDATE -----------------
        Try
            Session("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)


            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try

                Dim SqlCmd As New SqlCommand("SavePURCHASE_H", objConn, stTrans)
                SqlCmd.CommandType = CommandType.StoredProcedure
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = System.DBNull.Value
                SqlCmd.Parameters.Add(sqlpGUID)
                SqlCmd.Parameters.AddWithValue("@PUH_SUB_ID", Session("SUB_ID"))
                SqlCmd.Parameters.AddWithValue("@PUH_BSU_ID", Session("sBsuid"))
                SqlCmd.Parameters.AddWithValue("@PUH_FYEAR", Session("F_YEAR"))
                SqlCmd.Parameters.AddWithValue("@PUH_DOCTYPE", "PJ")
                SqlCmd.Parameters.AddWithValue("@PUH_DOCNO", Trim(txtdocNo.Text))
                SqlCmd.Parameters.AddWithValue("@PUH_REFNO", Trim(txtOldDocNo.Text))
                SqlCmd.Parameters.AddWithValue("@PUH_DOCDT", Trim(txtdocDate.Text))
                SqlCmd.Parameters.AddWithValue("@PUH_PARTY_ACT_ID", txtPartyCode.Text)
                SqlCmd.Parameters.AddWithValue("@PUH_DEBIT_ACT_ID", txtdrCode.Text.ToString)
                SqlCmd.Parameters.AddWithValue("@PUH_INVOICENO", Trim(txtInvoiceNo.Text))
                SqlCmd.Parameters.AddWithValue("@PUH_NARRATION", Trim(txtNarrn.Text))
                SqlCmd.Parameters.AddWithValue("@PUH_LPO", Trim(txtLPO.Text))
                SqlCmd.Parameters.AddWithValue("@PUH_CUR_ID", cmbCurrency.SelectedItem.Text)
                SqlCmd.Parameters.AddWithValue("@PUH_EXGRATE1", txtExchRate.Text)
                SqlCmd.Parameters.AddWithValue("@PUH_EXGRATE2", txtLocalRate.Text)
                SqlCmd.Parameters.AddWithValue("@PUH_bDELETED", False)
                SqlCmd.Parameters.AddWithValue("@PUR_TYPE", "1")
                SqlCmd.Parameters.AddWithValue("@PUH_bPOSTED", False)
                SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
                Dim sqlpJHD_TIMESTAMP As New SqlParameter("@PUH_TIMESTAMP", SqlDbType.Timestamp, 8)
                If Session("datamode") <> "edit" Then
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpJHD_TIMESTAMP.Value = Session("str_timestamp")
                End If
                SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
                SqlCmd.Parameters.AddWithValue("@PUH_SESSIONID", Session.SessionID)
                SqlCmd.Parameters.AddWithValue("@PUH_LOCK", Session("sUsr_name"))
                SqlCmd.Parameters.Add("@PUH_NEWDOCNO", SqlDbType.VarChar, 20)
                SqlCmd.Parameters("@PUH_NEWDOCNO").Direction = ParameterDirection.Output
                If (Session("datamode") = "edit") Then
                    SqlCmd.Parameters.AddWithValue("@bEdit", True)
                Else
                    SqlCmd.Parameters.AddWithValue("@bEdit", False)
                End If

                SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                SqlCmd.ExecuteNonQuery()
                lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)

                'Adding transaction info
                Dim str_err As String
                If (lintRetVal = 0) Then
                    If (Session("datamode") = "edit") Then
                        lstrNewDocNo = txtdocNo.Text
                    Else
                        lstrNewDocNo = CStr(SqlCmd.Parameters("@PUH_NEWDOCNO").Value)
                    End If
                    'Adding header info
                    SqlCmd.Parameters.Clear()
                    If Session("datamode") = "add" Then
                        str_err = DoTransactions(objConn, stTrans, lstrNewDocNo)
                    Else
                        str_err = DeleteVOUCHER_D_S_ALL(objConn, stTrans, txtdocNo.Text)
                        If str_err = 0 Then
                            str_err = DoTransactions(objConn, stTrans, txtdocNo.Text)
                        End If
                    End If
                    If str_err = "0" Then
                        h_editorview.Value = ""
                        stTrans.Commit()
                        Call Clear_Header()
                        Call Clear_Details()

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
                        Session("gintGridLine") = 1
                        GridInitialize()
                        Session("dtDTL") = DataTables.CreateDataTable_PJ()
                        Session("dtCostChild").Rows.Clear()
                        txtdocDate.Text = GetDiplayDate()
                        If (Session("datamode") = "add") Then
                            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                        End If
                        bind_Currency()
                        If Session("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                        End If

                        'tr_errLNE.Visible = True
                        lblError.Text = "Data Successfully Saved..."
                    Else
                        'tr_errLNE.Visible = True
                        lblError.Text = getErrorMessage(str_err & "")
                        stTrans.Rollback()
                    End If
                Else
                    'tr_errLNE.Visible = True
                    lblError.Text = getErrorMessage(lintRetVal & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = getErrorMessage("1000")
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()

        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = Session("BANKTRAN")
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim iIndex As Integer
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If Session("dtDTL").Rows(iIndex)("Status") & "" <> "DELETED" Then
                cmd.Dispose()
                cmd = New SqlCommand("SavePURCHASE_D", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '' ''Handle sub table
                Dim str_crdb As String = "CR"
                If Session("dtDTL").Rows(iIndex)("Amount") > 0 Then
                    dTotal = Session("dtDTL").Rows(iIndex)("Amount")
                    str_crdb = "DR"
                End If

                str_err = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtDTL").Rows(iIndex)("id"), _
                str_crdb, iIndex + 1 - Session("iDeleteCount"), Session("dtDTL").Rows(iIndex)("Accountid"), Session("dtDTL").Rows(iIndex)("invDate"), dTotal)
                If str_err <> "0" Then
                    Return str_err
                End If
                '' ''
                Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                cmd.Parameters.Add(sqlpGUID)

                Dim sqlpJNL_SUB_ID As New SqlParameter("@PUD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJNL_SUB_ID.Value = Session("SUB_ID")
                cmd.Parameters.Add(sqlpJNL_SUB_ID)

                Dim sqlpsqlpJNL_BSU_ID As New SqlParameter("@PUD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJNL_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpJNL_BSU_ID)

                Dim sqlpJNL_FYEAR As New SqlParameter("@PUD_FYEAR", SqlDbType.Int)
                sqlpJNL_FYEAR.Value = Session("F_YEAR")
                cmd.Parameters.Add(sqlpJNL_FYEAR)

                Dim sqlpJNL_DOCTYPE As New SqlParameter("@PUD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpJNL_DOCTYPE.Value = "PJ"
                cmd.Parameters.Add(sqlpJNL_DOCTYPE)

                Dim sqlpJNL_DOCNO As New SqlParameter("@PUD_DOCNO", SqlDbType.VarChar, 20)
                sqlpJNL_DOCNO.Value = p_docno & ""
                cmd.Parameters.Add(sqlpJNL_DOCNO)

                Dim sqlpPUD_INVNO As New SqlParameter("@PUD_INVNO", SqlDbType.VarChar, 50)
                sqlpPUD_INVNO.Value = Session("dtDTL").Rows(iIndex)("InvNo")
                cmd.Parameters.Add(sqlpPUD_INVNO)

                Dim sqlpPUD_LPO As New SqlParameter("@PUD_LPO", SqlDbType.VarChar, 50)
                sqlpPUD_LPO.Value = Session("dtDTL").Rows(iIndex)("LPONo")
                cmd.Parameters.Add(sqlpPUD_LPO)

                Dim sqlpPUD_INVDT As New SqlParameter("@PUD_INVDT", SqlDbType.DateTime)
                sqlpPUD_INVDT.Value = Session("dtDTL").Rows(iIndex)("InvDate")
                cmd.Parameters.Add(sqlpPUD_INVDT)


                Dim sqlpJNL_ACT_ID As New SqlParameter("@PUD_DEBIT_ACT_ID", SqlDbType.VarChar, 20)
                sqlpJNL_ACT_ID.Value = Session("dtDTL").Rows(iIndex)("Accountid") & ""
                cmd.Parameters.Add(sqlpJNL_ACT_ID)

                Dim sqlpbJNL_SLNO As New SqlParameter("@PUD_LINENO", SqlDbType.Int)
                sqlpbJNL_SLNO.Value = iIndex + 1 - Session("iDeleteCount")
                cmd.Parameters.Add(sqlpbJNL_SLNO)

                Dim sqlpbJNL_ITEM As New SqlParameter("@PUD_ITEM", SqlDbType.VarChar, 500)
                sqlpbJNL_ITEM.Value = Session("dtDTL").Rows(iIndex)("Item") & ""
                cmd.Parameters.Add(sqlpbJNL_ITEM)

                Dim sqlpbJNL_Qty As New SqlParameter("@PUD_QTY", SqlDbType.Decimal)
                sqlpbJNL_Qty.Value = Session("dtDTL").Rows(iIndex)("Qty") & ""
                cmd.Parameters.Add(sqlpbJNL_Qty)

                Dim sqlpbJNL_Rate As New SqlParameter("@PUD_RATE", SqlDbType.Decimal)
                sqlpbJNL_Rate.Value = Session("dtDTL").Rows(iIndex)("Rate") & ""
                cmd.Parameters.Add(sqlpbJNL_Rate)

                Dim sqlpCreditDays As New SqlParameter("@PUD_CreditDays", SqlDbType.Decimal)
                sqlpCreditDays.Value = Session("dtDTL").Rows(iIndex)("CreditDays")
                cmd.Parameters.Add(sqlpCreditDays)

                Dim sqlpbJNL_BDELETED As New SqlParameter("@PUD_BDELETED", SqlDbType.Bit)
                sqlpbJNL_BDELETED.Value = False
                cmd.Parameters.Add(sqlpbJNL_BDELETED)

                If BSU_IsTAXEnabled Then
                    cmd.Parameters.AddWithValue("@PUD_TAX_CODE", Session("dtDTL").Rows(iIndex)("TaxCode") & "")
                    cmd.Parameters.AddWithValue("@PUD_TDS_CODE", Session("dtDTL").Rows(iIndex)("TdsCode") & "")
                Else
                    cmd.Parameters.AddWithValue("@PUD_TAX_CODE", "")
                    cmd.Parameters.AddWithValue("@PUD_TDS_CODE", "")
                End If

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If Session("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                cmd.ExecuteNonQuery()
                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

                Dim success_msg As String = ""
                If iReturnvalue <> 0 Then
                    Exit For
                End If
                cmd.Parameters.Clear()
            Else
                If Not Session("dtDTL").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                    Session("iDeleteCount") = Session("iDeleteCount") + 1
                    cmd = New SqlCommand("DeletePURCHASE_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                    sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""

                    If iReturnvalue <> 0 Then
                        Response.Write("DSF")
                    End If
                    cmd.Parameters.Clear()
                End If
            End If
        Next
        If iIndex <= Session("dtDTL").Rows.Count - 1 Then
            Return iReturnvalue
        Else
            Return iReturnvalue
        End If
    End Function

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal invdate As String, ByVal p_amount As String) As String
        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), Session("BANKTRAN"), _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                txtdocDate.Text, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), Session("BANKTRAN"), "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue
    End Function

    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then
                dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
            End If
        Next
        If Math.Round(dTotal, 3) = Math.Round(p_total, 3) Then
            Return True
        Else
            Return False
        End If

    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        '   --- Remove ReadOnly From The Form
        h_editorview.Value = ""
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                'tr_errLNE.Visible = True
                lblError.Text = getErrorMessage(str_)
            Else
                'tr_errLNE.Visible = True
                lblError.Text = "Did not get lock"
            End If
        Else
            h_editorview.Value = "Edit"
            Session("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            ImageButton1.Enabled = False
            ControlReadOnly(False)
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '   --- Remove ReadOnly From The Form 
        Call Clear_Header()
        Call Clear_Details()
        ControlReadOnly(False)
        Session("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Session("gintGridLine") = 1
        GridInitialize()
        Session("dtDTL") = DataTables.CreateDataTable_PJ()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
        Session("dtCostChild").rows.clear()
        txtdocDate.Text = GetDiplayDate()
        If (Session("datamode") = "add") Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
        ImageButton1.Enabled = True
        bind_Currency()
    End Sub

    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL, lstrSQL2 As String

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer
            lstrSQL = "SELECT A.*,REPLACE(CONVERT(VARCHAR(11), A.PUH_DOCDT, 106), ' ', '/') as DocDate , isNULL(A.PUH_REFNO,'') as OldDocNo FROM PURCHASE_H A  " _
                       & " WHERE A.GUID='" & Session("Eid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                txtdocNo.Text = ds.Tables(0).Rows(0)("PUH_DOCNO") & ""
                txtOldDocNo.Text = ds.Tables(0).Rows(0)("OldDocNo") & ""
                txtdocDate.Text = Format(ds.Tables(0).Rows(0)("PUH_DOCDT"), "dd/MMM/yyyy")
                Session("SessDocDate") = txtdocDate.Text
                txtPartyCode.Text = ds.Tables(0).Rows(0)("PUH_PARTY_ACT_ID" & "")
                txtPartyDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("PUH_PARTY_ACT_ID") & "")
                txtdrCode.Text = ds.Tables(0).Rows(0)("PUH_DEBIT_ACT_ID" & "")
                txtdrDescr.Text = Master.GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("PUH_DEBIT_ACT_ID") & "")
                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Text = ds.Tables(0).Rows(0)("PUH_CUR_ID") & "" Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtExchRate.Text = ds.Tables(0).Rows(0)("PUH_EXGRATE1") & ""
                txtLocalRate.Text = ds.Tables(0).Rows(0)("PUH_EXGRATE2") & ""
                txtNarrn.Text = ds.Tables(0).Rows(0)("PUH_NARRATION") & ""
                txtLPO.Text = ds.Tables(0).Rows(0)("PUH_LPO") & ""
                txtInvoiceNo.Text = ds.Tables(0).Rows(0)("PUH_INVOICENO") & ""

                ''   --- Initialize The Grid With The Data From The Detail Table
                'lstrSQL2 = "SELECT Convert(VarChar,A.PUD_LINENO) as Id,PUD_INVNO as InvNo, PUD_LPO as LPONo, PUD_INVDT as InvDate, A.PUD_DEBIT_ACT_ID as AccountId,C.ACT_NAME as AccountName,A.PUD_ITEM as Item,A.PUD_QTY as Qty,A.PUD_Rate as Rate,(A.PUD_QTY*A.PUD_Rate) as Amount, " _
                '            & " '' as Status,A.GUID,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd FROM PURCHASE_D A  " _
                '            & " INNER JOIN vw_OSA_ACCOUNTS_M C ON A.PUD_DEBIT_ACT_ID=C.ACT_ID" _
                '            & " WHERE A.PUD_SUB_ID='" & ds.Tables(0).Rows(0)("PUH_SUB_ID") & "'  AND PUD_BSU_ID='" & ds.Tables(0).Rows(0)("PUH_BSU_ID") & "'   AND A.PUD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.PUD_DOCNO='" & ds.Tables(0).Rows(0)("PUH_DOCNO") & "'  "
                'ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                'Session("dtDTL") = DataTables.CreateDataTable_PJ()
                'Session("dtDTL") = ds2.Tables(0)
                'gvDTL.DataSource = ds2
                Dim dtDTL As DataTable = CostCenterFunctions.ViewVoucherDetails(ds.Tables(0).Rows(0)("PUH_SUB_ID"), ds.Tables(0).Rows(0)("PUH_BSU_ID"), _
               Session("BANKTRAN"), ds.Tables(0).Rows(0)("PUH_DOCNO"))
                Session("dtDTL") = dtDTL

                lstrSQL2 = "SELECT MAx(PUD_LINENO) as Id FROM PURCHASE_D A " _
                           & " WHERE A.PUD_SUB_ID='" & ds.Tables(0).Rows(0)("PUH_SUB_ID") & "'  AND PUD_BSU_ID='" & ds.Tables(0).Rows(0)("PUH_BSU_ID") & "'   AND A.PUD_DOCTYPE='" & Session("BANKTRAN") & "' AND A.PUD_DOCNO='" & ds.Tables(0).Rows(0)("PUH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                Session("gintGridLine") = Val(ds2.Tables(0).Rows(0)("Id").ToString) + 1


                Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(ds.Tables(0).Rows(0)("PUH_SUB_ID"), ds.Tables(0).Rows(0)("PUH_BSU_ID"), _
    Session("BANKTRAN"), ds.Tables(0).Rows(0)("PUH_DOCNO"))

                Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(ds.Tables(0).Rows(0)("PUH_SUB_ID"), ds.Tables(0).Rows(0)("PUH_BSU_ID"), _
                Session("BANKTRAN"), ds.Tables(0).Rows(0)("PUH_DOCNO"))

                gvDTL.DataSource = dtDTL
                gvDTL.DataBind()
                GridBind()
                '   ----  Initalize the Cost Center Grid
                'Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
                'Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()


                'lstrSQL2 = "SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,A.VDS_CCS_ID as Costcenter,A.VDS_CODE as Memberid,A.VDS_DESCr as Name,VDS_ERN_ID AS ERN_ID,A.VDS_AMOUNT as Amount," _
                '            & " '' as Status,A.GUID, VDS_CSS_CSS_ID as SubMemberId FROM VOUCHER_D_S A" _
                '            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("PUH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("PUH_BSU_ID") & "'   AND A.VDS_DOCTYPE='" & Session("BANKTRAN") & "' " _
                '            & " AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("PUH_DOCNO") & "' AND VDS_CODE IS NOT NULL  AND VDS_Auto IS NULL"
                'ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                'Session("dtCostChild") = ds2.Tables(0)
                ControlReadOnly(True)
            Else
                lblError.Text = "Record Not Found !!!"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = "Record Not Found !!! "
        End Try
    End Sub

    Private Sub ControlReadOnly(ByVal parReadOnly As Boolean)
        txtdocNo.ReadOnly = parReadOnly
        txtOldDocNo.ReadOnly = parReadOnly
        txtdocDate.ReadOnly = parReadOnly
        txtPartyCode.ReadOnly = parReadOnly
        txtPartyDescr.ReadOnly = parReadOnly
        txtdrCode.ReadOnly = parReadOnly
        txtdrDescr.ReadOnly = parReadOnly
        txtExchRate.ReadOnly = parReadOnly
        txtLocalRate.ReadOnly = parReadOnly
        txtNarrn.ReadOnly = parReadOnly
        txtLPO.ReadOnly = parReadOnly
        txtInvoiceNo.ReadOnly = parReadOnly
        cmbCurrency.Enabled = Not parReadOnly
        ImageButton1.Enabled = Not parReadOnly
        txtQty.ReadOnly = parReadOnly
        txtdetInvoiceDate.ReadOnly = parReadOnly
        txtdetInvoiceNo.ReadOnly = parReadOnly
        txtdetLPONo.ReadOnly = parReadOnly
        txtLineNarrn.ReadOnly = parReadOnly
        txtrate.ReadOnly = parReadOnly
        imgDDebit.Visible = Not parReadOnly
        imgParty.Visible = Not parReadOnly
        imgDebit.Visible = Not parReadOnly
        ImageButton1.Enabled = Not parReadOnly

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Session("datamode") = "add" Or Session("datamode") = "edit" Then
            h_editorview.Value = ""
            unlock()
            Call Clear_Details()
            Session("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        str_Sql = "SELECT * FROM PURCHASE_H WHERE" _
               & " GUID='" & Session("Eid") & "'"

        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim cmd As New SqlCommand("DeletePURCHASE", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("PUH_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)
                '@DOCNO	varchar(20),
                '@DOCTYPE	varchar(20) ,
                '@VHH_BSU_ID	varchar(10), 
                '@VHH_FYEAR	int,
                '@VHH_SUB_ID varchar(20),
                '@VHH_LOCK varc
                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "PJ"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("PUH_BSU_ID") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("PUH_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHH_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpVHH_SUB_ID)

                Dim sqlpVHH_LOCK As New SqlParameter("@VHH_LOCK", SqlDbType.VarChar, 30)
                sqlpVHH_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpVHH_LOCK)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value

                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                    lblError.Text = getErrorMessage("519")
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    Session("datamode") = Encr_decrData.Encrypt("add")
                    Clear_Header()
                    Clear_Details()
                    Response.Redirect("AccTranPJ.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Request.QueryString("datamode"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try

    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        repSource = VoucherReports.PurchaseJournalVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "PJ", txtdocNo.Text, Session("HideCC"))
        Session("ReportSource") = repSource
        '  Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        DateFunctions.CorrectDateFormat(sender, lblError, False)
        If txtdetInvoiceDate.Text.Trim = "" Then
            txtdetInvoiceDate.Text = txtdocDate.Text
        End If
        getnextdocid()
        bind_Currency()
    End Sub

    Private Sub getnextdocid()
        If ViewState("datamode") = "add" Then
            Try
                txtdocNo.Text = AccountFunctions.GetNextDocId("PJ", Session("sBsuid"), CType(txtdocDate.Text, Date).Month, CType(txtdocDate.Text, Date).Year)
                If txtdocNo.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        getnextdocid()
        bind_Currency()
    End Sub

    Protected Sub txtPartyCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPartyCode.TextChanged
        chk_PartyAccount()
    End Sub
    Protected Sub lblbtn_Click(sender As Object, e As EventArgs) Handles lblbtn.Click


    End Sub

    Sub chk_PartyAccount()
        txtPartyDescr.Text = AccountFunctions.Validate_Account(txtPartyCode.Text, Session("sbsuid"), "PARTY2")
        If txtPartyDescr.Text = "" Then
            txtCreditDays.Text = ""
            lblPaymentTerm.Text = ""
            trPayTerm.Visible = False
            lblError.Text = "Invalid Party Account Selected"
        Else
            txtdrCode.Focus()
            lblPaymentTerm.Text = ""
            Dim strPayTerm As String = AccountFunctions.GetPaymentTerm(txtPartyCode.Text)
            If strPayTerm <> "" Then
                lblPaymentTerm.Text = "Payment Term : " & strPayTerm
                trPayTerm.Visible = True
            Else
                trPayTerm.Visible = False
            End If
            txtCreditDays.Text = GetCreditDays(txtPartyCode.Text)
            'lblPaymentTerm.Text = "Payment Term : " & AccountFunctions.GetPaymentTerm(txtPartyCode.Text)
            lblError.Text = ""
        End If
    End Sub
    Public Shared Function GetCreditDays(ByVal vAccCode As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim strSql As String = "SELECT isnull(ACT_CREDITDAYS,0) FROM ACCOUNTS_M WHERE ACCOUNTS_M.ACT_ID = '" & vAccCode & "'"
        If vAccCode = "" Then
            Return ""
        End If
        Dim obj As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strSql)
        If obj Is DBNull.Value Then
            Return ""
        Else
            Return obj.ToString
        End If

    End Function
    Sub chk_DebitAccount()
        txtdrDescr.Text = AccountFunctions.Validate_Account(txtdrCode.Text, Session("sbsuid"), "DEBIT")
        If txtdrDescr.Text = "" Then
            lblError.Text = "Invalid Debit Account Selected"
        Else
            txtLPO.Focus()
            lblError.Text = ""
        End If
    End Sub

    Sub chk_DDebitAccount()
        txtdebitdescr.Text = AccountFunctions.Validate_Account(Detail_ACT_ID.Text, Session("sbsuid"), "DEBIT_D")
        If txtdebitdescr.Text = "" Then
            lblError.Text = "Invalid Detail Account Selected"
        Else
            txtLineNarrn.Focus()
            lblError.Text = ""
        End If
    End Sub

    Protected Sub imgParty_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgParty.Click
        chk_PartyAccount()
    End Sub

    Protected Sub txtDrCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdrCode.TextChanged
        chk_DebitAccount()
    End Sub

    Protected Sub imgDebit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDebit.Click
        chk_DebitAccount()
    End Sub

    'Protected Sub Detail_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail_ACT_ID.TextChanged
    '    chk_DDebitAccount()
    '    ClearRadGridandCombo()
    'End Sub

    Protected Sub imgDDebit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDDebit.Click
        chk_DDebitAccount()
    End Sub

    Protected Sub txtdetInvoiceDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DateFunctions.CorrectDateFormat(sender, lblError, True)
    End Sub

    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function

    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIdCostchild As New Label
            lblIdCostchild = e.Row.FindControl("lblIdCostchild")
            Dim gvCostAllocation As New GridView
            gvCostAllocation = e.Row.FindControl("gvCostAllocation")
            If Not Session("CostAllocation") Is Nothing AndAlso Not gvCostAllocation Is Nothing Then
                gvCostAllocation.Attributes.Add("bordercolor", "#fc7f03")
                Dim dv As New DataView(Session("CostAllocation"))
                If Session("CostAllocation").Rows.Count > 0 Then
                    dv.RowFilter = "CostCenterID='" & lblIdCostchild.Text & "' "
                End If
                dv.Sort = "CostCenterID"
                gvCostAllocation.DataSource = dv.ToTable
                gvCostAllocation.DataBind()
            End If
        End If
    End Sub

    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub

    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()
    End Sub


End Class
