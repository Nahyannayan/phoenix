Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI

Partial Class jvJournalvoucher
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    Dim BSU_IsTAXEnabled As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbUploadEmployee)
        If Page.IsPostBack = False Then

            'TAX CODE
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = Session("sBSUID")
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))
            ViewState("BSU_IsTAXEnabled") = BSU_IsTAXEnabled
            If BSU_IsTAXEnabled = True Then
                trTaxType.Visible = True
                LOAD_TAX_CODES()
            End If
            'TAX CODE

            Session("CHECKLAST") = 0
            usrCostCenter1.TotalAmountControlName = "txtDAmount"
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If
            bind_groupPolicy()
            initialize_components()
            'txtHNarration.Attributes.Add("onblur", "javascript:CopyDetails()")
            txtHNarration.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtHNarration.ClientID & "');")
            txtDNarration.Attributes.Add("onBlur", "narration_check('" & txtDNarration.ClientID & "');")

            '''''check menu rights
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            If (Request.QueryString("edited") <> "") Then
                lblError.Text = getErrorMessage("521")
            End If
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or (MainMnu_code <> "A150020" And MainMnu_code <> "A200015") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") <> "" Then
                set_viewdata()
                btnAdd.Visible = True
                btnEdit.Visible = True
                setViewData()
                setModifyHeader(Request.QueryString("viewid"))
            Else
                ResetViewData()
            End If
            gridbind()
            bind_Currency()
            If Request.QueryString("invalidedit") = "1" Then
                lblError.Text = "Invalid Editid"
                Exit Sub
            End If
            If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
                getnextdocid()
            End If
            ''call the object during the page load to check the initial stage
            UtilityObj.beforeLoopingControls(Me.Page)
        Else
            BSU_IsTAXEnabled = ViewState("BSU_IsTAXEnabled")
        End If

    End Sub
    Sub LOAD_TAX_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_CODES]", pParms)

        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAXDESC"
        ddlVATCode.DataValueField = "TAXCODE"
        ddlVATCode.DataBind()

    End Sub
    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()
        ViewState("str_timestamp") = New Byte()
        txtHExchRate.Attributes.Add("readonly", "readonly")
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")

        txtDAccountName.Attributes.Add("readonly", "readonly")
        txtHLocalRate.Attributes.Add("readonly", "readonly")
        Session("dtDTL") = DataTables.CreateDataTable_JV()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
        ViewState("idJournal") = 0
        Session("idCostChild") = 0
        'btnCancel.Visible = False
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
    End Sub

    Private Sub setViewData()
        'tbl_Details.Visible = False
        tbl_Details.Attributes.Add("style", "display:none")
        'SetGridColumnVisibility(False)
        btnHAccount.Enabled = False
        imgCalendar.Enabled = False
        DDCurrency.Enabled = False
        txtHDocdate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
        txtHOldrefno.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub ResetViewData()
        'tbl_Details.Visible = True
        tbl_Details.Attributes.Add("style", "display:table")
        ' SetGridColumnVisibility(True)
        btnHAccount.Enabled = True
        imgCalendar.Enabled = True
        DDCurrency.Enabled = True
        txtHDocdate.Attributes.Remove("readonly")
        txtHNarration.Attributes.Remove("readonly")
        txtHOldrefno.Attributes.Remove("readonly")
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM JOURNAL_H where GUID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocno.Text = p_Modifyid
                txtHDocdate.Text = Format(CDate(ds.Tables(0).Rows(0)("JHD_DOCDT")), "dd/MMM/yyyy")
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")
                txtHOldrefno.Text = ds.Tables(0).Rows(0)("JHD_REFNO") & ""
                txtHDocno.Text = ds.Tables(0).Rows(0)("JHD_DOCNO")
                bind_Currency()
                setModifyDetails(p_Modifyid)
            Else
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub setModifyDetails(ByVal p_Modifyid As String)
        Dim dtDTL As DataTable = CostCenterFunctions.ViewVoucherDetails(Session("Sub_ID"), Session("sBsuid"), _
        "JV", ViewState("str_editData").Split("|")(0))
        If dtDTL.Rows.Count > 0 Then
            For iIndex As Integer = 0 To dtDTL.Rows.Count - 1
                'JNL_DEBIT, JNL_CREDIT, JNL_NARRATION, 
                Dim rDt As DataRow
                'Dim i As Integer
                'Dim str_actname_cost_mand As String = AccountFunctions.getAccountname(dtDTL.Rows(iIndex)("JNL_ACT_ID"), Session("sBsuid"))
                rDt = Session("dtDTL").NewRow
                rDt("GUID") = dtDTL.Rows(iIndex)("GUID")
                rDt("Id") = dtDTL.Rows(iIndex)("JNL_SLNO")

                ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = dtDTL.Rows(iIndex)("JNL_ACT_ID")
                rDt("Accountname") = dtDTL.Rows(iIndex)("ACT_NAME")
                rDt("Narration") = dtDTL.Rows(iIndex)("JNL_NARRATION")

                rDt("Credit") = dtDTL.Rows(iIndex)("JNL_CREDIT")
                rDt("Debit") = dtDTL.Rows(iIndex)("JNL_DEBIT")
                If BSU_IsTAXEnabled Then
                    rDt("TaxCode") = dtDTL.Rows(iIndex)("TaxCode")
                Else
                    rDt("TaxCode") = ""
                End If
                rDt("Ply") = dtDTL.Rows(iIndex)("Ply")
                rDt("CostReqd") = False
                rDt("Status") = "Normal"
                Session("dtDTL").Rows.Add(rDt)
            Next
            Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(Session("Sub_ID"), Session("sBsuid"), _
           "JV", ViewState("str_editData").Split("|")(0))

            Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(Session("Sub_ID"), Session("sBsuid"), _
            "JV", ViewState("str_editData").Split("|")(0))
        End If
        ViewState("idJournal") = ViewState("idJournal") + 1
    End Sub

    'Private Sub setModifyCost(ByVal p_Modifyid As String)
    '    Try
    '        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    '        Dim str_Sql As String

    '        str_Sql = "SELECT JOURNAL_D_S.GUID, JOURNAL_D_S.JDS_DOCTYPE," _
    '        & " JOURNAL_D_S.JDS_DOCNO," _
    '        & " case isnull(JOURNAL_D_S.JDS_CODE,'')" _
    '        & " when '' then  COSTCENTER_S.CCS_DESCR else" _
    '        & " JOURNAL_D_S.JDS_DESCR end JDS_DESCR ," _
    '        & " JOURNAL_D_S.JDS_ACT_ID, JOURNAL_D_S.JDS_CCS_ID," _
    '        & " JOURNAL_D_S.JDS_CODE, JOURNAL_D_S.JDS_AMOUNT," _
    '        & " JOURNAL_D_S.JDS_SLNO,JOURNAL_D_S.JDS_CCS_CCS_ID FROM JOURNAL_D_S INNER JOIN" _
    '        & " COSTCENTER_S ON JOURNAL_D_S.JDS_CCS_ID = COSTCENTER_S.CCS_ID" _
    '        & " WHERE (JOURNAL_D_S.JDS_DOCNO = '" & ViewState("str_editData").Split("|")(0) & "')" _
    '        & " AND (JOURNAL_D_S.JDS_bDELETED = 'False')" _
    '        & " AND (JOURNAL_D_S.JDS_DOCTYPE='JV')" _
    '        & " AND (ISNULL(JOURNAL_D_S.JDS_Auto, 0)  = 0 )" _
    '        & " AND (JOURNAL_D_S.JDS_BSU_ID = '" & Session("sBsuid") & "') " _
    '        & " AND (JOURNAL_D_S.JDS_SUB_ID = '" & Session("Sub_ID") & "')"

    '        Dim ds As New DataSet
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        If dtDTL.Rows.Count > 0 Then
    '            For iIndex As Integer = 0 To dtDTL.Rows.Count - 1
    '                Dim rDt As DataRow
    '                'Dim i As Integer
    '                'Dim str_actname_cost_mand As String = getAccountname(dtDTL.Rows(iIndex)("JNL_ACT_ID"))
    '                rDt = Session("dtCostChild").NewRow
    '                rDt("GUID") = dtDTL.Rows(iIndex)("GUID")
    '                rDt("Id") = dtDTL.Rows(iIndex)("GUID")
    '                rDt("Name") = dtDTL.Rows(iIndex)("JDS_DESCR") & ""
    '                rDt("Memberid") = dtDTL.Rows(iIndex)("JDS_CODE")
    '                rDt("VoucherId") = dtDTL.Rows(iIndex)("JDS_SLNO")
    '                rDt("Ply") = dtDTL.Rows(iIndex)("JDS_CCS_ID")
    '                rDt("SubMemberid") = dtDTL.Rows(iIndex)("JDS_CCS_CCS_ID")
    '                rDt("Amount") = dtDTL.Rows(iIndex)("JDS_AMOUNT")
    '                rDt("Status") = "Normal"
    '                'idCostChild = idCostChild + 1
    '                Session("dtCostChild").Rows.Add(rDt)
    '            Next
    '        Else
    '        End If
    '        gridbind()
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub

    Sub bind_groupPolicy()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
            & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub getnextdocid()
        If ViewState("datamode") <> "edit" Then
            Try
                txtHDocno.Text = AccountFunctions.GetNextDocId("JV", Session("sBsuid"), CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                If txtHDocno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            DDCurrency.Items.Clear()
            DDCurrency.DataSource = MasterFunctions.GetExchangeRates(txtHDocdate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If DDCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
                btnSave.Enabled = True
            Else
                txtHExchRate.Text = "0"
                txtHLocalRate.Text = "0"
                btnSave.Enabled = False
                lblError.Text = "Cannot Save Data. Currency/Exchange Rate Not Set"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In DDCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            'Else
            'End If
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        Dim strfDate As String = txtHDocdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdds.Click
        txtDAccountName.Text = AccountFunctions.check_accountid(Detail_ACT_ID.Text & "", Session("sBsuid"))

        '''''FIND ACCOUNT IS THERE 

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & Detail_ACT_ID.Text & "'" _
           & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
            Else
                txtDAccountName.Text = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            txtDAccountName.Text = ""
        End Try
        '''''FIND ACCOUNT IS THERE
        If txtDAccountName.Text = "" Then
            lblError.Text = getErrorMessage("303") ' account already there
            Exit Sub
        End If
        'Check CostCenter Summary
        Dim DAmount As Decimal
        If Not IsNumeric(txtDAmount.Text) Then
            lblError.Text = "Invalid Amount!!!"
            Exit Sub
        Else
            DAmount = Math.Abs(Convert.ToDecimal(txtDAmount.Text))
        End If
        RecreateSsssionDataSource()
        If Not CostCenterFunctions.VerifyCostCenterAmount(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), _
                                                            DAmount) Then
            lblError.Text = "Invalid Cost Center Allocation!!!"
            Exit Sub
        End If
        Try
            Dim rDt As DataRow
            Dim tempTax As Double = 0
            Dim i As Integer
            Dim dCrorDb As Double
            dCrorDb = AccountFunctions.Round(txtDAmount.Text.Trim)
            If dCrorDb <> 0 Then
                If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                    CostCenterFunctions.AddCostCenter(ViewState("idJournal"), Session("sBsuid"), _
                                      Session("CostOTH"), txtHDocdate.Text, Session("idCostChild"), _
                                     Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                     Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                End If
                rDt = Session("dtDTL").NewRow
                rDt("Id") = ViewState("idJournal")
                ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = Detail_ACT_ID.Text.Trim
                rDt("Accountname") = txtDAccountName.Text.Trim
                rDt("Narration") = txtDNarration.Text.Trim

                If dCrorDb > 0 Then
                    tempTax = getTaxValue(ddlVATCode.SelectedValue, dCrorDb)
                    rDt("Credit") = "0"
                    rDt("Debit") = tempTax + dCrorDb 'dCrorDb
                Else
                    tempTax = getTaxValue(ddlVATCode.SelectedValue, dCrorDb)
                    rDt("Credit") = (tempTax + dCrorDb) * -1
                    rDt("Debit") = "0"
                End If
                If BSU_IsTAXEnabled Then
                    rDt("TaxCode") = ddlVATCode.SelectedValue
                Else
                    rDt("TaxCode") = ""
                End If
                rDt("CostReqd") = False
                rDt("GUID") = System.DBNull.Value

                For i = 0 To Session("dtDTL").Rows.Count - 1
                    If Session("dtDTL").Rows(i)("Accountid") = rDt("Accountid") And _
                         Session("dtDTL").Rows(i)("Accountname") = rDt("Accountname") And _
                          Session("dtDTL").Rows(i)("Narration") = rDt("Narration") And _
                          Session("dtDTL").Rows(i)("Credit") = rDt("Credit") And _
                          Session("dtDTL").Rows(i)("Debit") = rDt("Debit") Then
                        lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        gridbind()
                        Exit Sub
                    End If
                Next
                Session("dtDTL").Rows.Add(rDt)
            End If
            SetGridColumnVisibility(True)
            gridbind()
            Clear_Details()
            If gvJournal.Rows.Count = 1 Then
                txtDAmount.Text = AccountFunctions.Round(dCrorDb + tempTax) * -1
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = getErrorMessage("510")
        End Try

    End Sub

    Private Function ISCrossMatch() As Boolean
        Dim dDebit As Double = 0
        Dim dCredit As Double = 0


       
        If Session("dtDTL").Rows.Count > 0 Then
            For i = 0 To Session("dtDTL").Rows.Count - 1
                Dim tempDebit As Double = 0
                Dim tempCredit As Double = 0
                If Session("dtDTL").Rows(i)("Status") & "" <> "Deleted" Then
                   
                    If Session("dtDTL").Rows(i)("Credit") <> 0 Then
                        tempCredit = getTaxValue(Session("dtDTL").Rows(i)("TaxCode"), Session("dtDTL").Rows(i)("Credit"))
                        'dCredit = AccountFunctions.Round(dCredit + Session("dtDTL").Rows(i)("Credit"))
                        Session("dtDTL").Rows(i)("Credit") = Session("dtDTL").Rows(i)("Credit") + tempCredit
                        dCredit = AccountFunctions.Round(dCredit + Session("dtDTL").Rows(i)("Credit"))
                    Else
                        tempDebit = getTaxValue(Session("dtDTL").Rows(i)("TaxCode"), Session("dtDTL").Rows(i)("Debit"))
                        'dDebit = AccountFunctions.Round(dDebit + Session("dtDTL").Rows(i)("Debit"))
                        Session("dtDTL").Rows(i)("Debit") = Session("dtDTL").Rows(i)("Debit") + tempDebit
                        dDebit = AccountFunctions.Round(dDebit + Session("dtDTL").Rows(i)("Debit"))
                    End If
                    dDebit = AccountFunctions.Round2(dDebit, 2)
                    dCredit = AccountFunctions.Round2(dCredit, 2)
                
                Else
                End If
            Next
        End If

        If dDebit = dCredit Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub gridbind()
        Try
            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = DataTables.CreateDataTable_JV()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0

          
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtDTL").Rows.Count > 0 Then
                For i = 0 To Session("dtDTL").Rows.Count - 1
                    Dim tempDebit As Double = 0
                    Dim tempCredit As Double = 0
                    If Session("dtDTL").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtDTL").Columns.Count - 1
                            rDt.Item(j) = Session("dtDTL").Rows(i)(j)
                        Next
                        If Session("dtDTL").Rows(i)("Credit") <> 0 Then
                            'tempCredit = getTaxValue(Session("dtDTL").Rows(i)("TaxCode"), Session("dtDTL").Rows(i)("Credit"))
                            'dCredit = AccountFunctions.Round(dCredit + Session("dtDTL").Rows(i)("Credit"))
                            'Session("dtDTL").Rows(i)("Credit") = Session("dtDTL").Rows(i)("Credit") + tempCredit
                            dCredit = AccountFunctions.Round(dCredit + Session("dtDTL").Rows(i)("Credit"))
                        Else
                            'tempDebit = getTaxValue(Session("dtDTL").Rows(i)("TaxCode"), Session("dtDTL").Rows(i)("Debit"))
                            'dDebit = AccountFunctions.Round(dDebit + Session("dtDTL").Rows(i)("Debit"))
                            'Session("dtDTL").Rows(i)("Debit") = Session("dtDTL").Rows(i)("Debit") + tempDebit
                            dDebit = AccountFunctions.Round(dDebit + Session("dtDTL").Rows(i)("Debit"))
                        End If
                        dDebit = AccountFunctions.Round2(dDebit, 2)
                        dCredit = AccountFunctions.Round2(dCredit, 2)
                        ' dTotAmount = dTotAmount + session("dtCostChild").Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtTDotalDebit.Text = AccountFunctions.Round2(dDebit, 2)
            txtTotalCredit.Text = AccountFunctions.Round2(dCredit, 2)
            txtDifference.Text = AccountFunctions.Round(dDebit - dCredit)
            If dDebit <> dCredit Or dCredit = 0 Then
                btnSave.Enabled = False
            Else
                btnSave.Enabled = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function getTaxValue(ByVal p_code As String, ByVal p_amount As String) As Double

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim amount As Double

            str_Sql = "select dbo.[GET_TAX_VALUES_FN] ('" + p_code + "','" + p_amount + "')"


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                amount = ds.Tables(0).Rows(0)(0)
            Else
                amount = 0
            End If
            Return amount

        Catch ex As Exception
            Errorlog(ex.Message)
            Return 0
        End Try
    End Function

    Private Function getReverseTotalFromTaxValue(ByVal p_code As String, ByVal p_amount As String) As Double

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim amount As Double

            str_Sql = "select dbo.[GET_REVERSETOTAL_TAX_VALUES_FN] ('" + p_code + "','" + p_amount + "')"


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                amount = ds.Tables(0).Rows(0)(0)
            Else
                amount = 0
            End If
            Return amount

        Catch ex As Exception
            Errorlog(ex.Message)
            Return 0
        End Try
    End Function

    Private Function get_mandatory_costcenter(ByVal p_id As String) As String
        If Session("dtDTL").Rows.Count > 0 Then
            For i As Integer = 0 To Session("dtDTL").Rows.Count - 1
                If Session("dtDTL").Rows(i)("id") & "" = p_id Then
                    Return Session("dtDTL").Rows(i)("Ply")
                End If
            Next
        End If
        Return ""
    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cmdCol As Integer = gvJournal.Columns.Count - 1
                'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
                Dim lblReqd As New Label
                Dim lblid As New Label

                Dim lblDebit As New Label
                Dim lblCredit As New Label

                'Dim btnAlloca As New LinkButton

                lblReqd = e.Row.FindControl("lblRequired")
                lblid = e.Row.FindControl("lblId")

                lblDebit = e.Row.FindControl("lblDebit")
                lblCredit = e.Row.FindControl("lblCredit")
                Dim gvCostchild As New GridView
                gvCostchild = e.Row.FindControl("gvCostchild")

                gvCostchild.Attributes.Add("bordercolor", "#fc7f03")

                'ClientScript.RegisterStartupScript([GetType](), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" & lblid.Text & "','one');</script>")
                If Not Session("dtCostChild") Is Nothing Then
                    Dim dv As New DataView(Session("dtCostChild"))
                    dv.RowFilter = "VoucherId='" & lblid.Text & "' and costcenter='" & e.Row.DataItem("Ply") & "'"
                    dv.Sort = "MemberId"
                    gvCostchild.DataSource = dv.ToTable
                    gvCostchild.DataBind()
                End If

                'btnAlloca = e.Row.FindControl("btnAlloca")
                'If btnAlloca IsNot Nothing Then
                '    Dim dAmt As Double

                '    If CDbl(lblDebit.Text) > 0 Then
                '        dAmt = CDbl(lblDebit.Text)
                '    Else
                '        dAmt = CDbl(lblCredit.Text)
                '    End If 
                '    btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & get_mandatory_costcenter(lblid.text) & "');return false;"
                'End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        Try
            If btnAdds.Visible = True Then
                Dim row As GridViewRow = gvJournal.Rows(e.RowIndex)
                Dim lblTid As New Label
                lblTid = TryCast(row.FindControl("lblId"), Label)

                Dim iRemove As Integer = 0
                Dim str_Index As String = ""
                str_Index = lblTid.Text
                For iRemove = 0 To Session("dtDTL").Rows.Count - 1
                    If str_Index = Session("dtDTL").Rows(iRemove)("id") Then
                        If ViewState("datamode") <> "edit" Then
                            Session("dtDTL").Rows(iRemove).Delete()
                        Else
                            Session("dtDTL").Rows(iRemove)("Status") = "Deleted"
                        End If
                        Exit For
                    End If
                Next
                For iRemove = 0 To Session("dtCostChild").Rows.Count - 1
                    If str_Index = Session("dtCostChild").Rows(iRemove)("Voucherid") Then
                        Session("dtCostChild").Rows(iRemove)("Status") = "Deleted"
                    End If
                Next
                gridbind()
            Else
                lblError.Text = "Cannot delete. Please cancel updation and delete"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("dtDTL").Rows.Count - 1
            If str_Search = Session("dtDTL").Rows(iIndex)("id") And Session("dtDTL").Rows(iIndex)("Status") & "" <> "Deleted" Then
                Detail_ACT_ID.Text = Session("dtDTL").Rows(iIndex)("Accountid")
                txtDAccountName.Text = Session("dtDTL").Rows(iIndex)("Accountname")
                If BSU_IsTAXEnabled Then
                    ddlVATCode.SelectedValue = Trim(Session("dtDTL").Rows(iIndex)("TaxCode"))
                End If
                Dim tempReverseTotal As Double = 0
                If Session("dtDTL").Rows(iIndex)("Credit") > 0 Then
                    tempReverseTotal = getReverseTotalFromTaxValue(ddlVATCode.SelectedValue, Session("dtDTL").Rows(iIndex)("Credit"))
                    'txtDAmount.Text = AccountFunctions.Round((Session("dtDTL").Rows(iIndex)("Credit") - tempTax)) * -1
                    txtDAmount.Text = AccountFunctions.Round(tempReverseTotal) * -1
                Else
                    tempReverseTotal = getReverseTotalFromTaxValue(ddlVATCode.SelectedValue, Session("dtDTL").Rows(iIndex)("Debit"))
                    'txtDAmount.Text = AccountFunctions.Round((Session("dtDTL").Rows(iIndex)("Debit") - tempTax))
                    txtDAmount.Text = AccountFunctions.Round(tempReverseTotal)
                End If
                txtDNarration.Text = Session("dtDTL").Rows(iIndex)("Narration")
              
                btnAdds.Visible = False
                btnUpdate.Visible = True
                btnEditCancel.Visible = True
                gvJournal.SelectedIndex = iIndex
                ''''''
                RecreateSsssionDataSource()
                CostCenterFunctions.SetGridSessionDataForEdit(str_Search, Session("dtCostChild"), _
                Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                usrCostCenter1.BindCostCenter()
                'tbl_Details.Visible = True
                tbl_Details.Attributes.Add("style", "display:table")
                SetGridColumnVisibility(False)
                ''''''
                'gridbind()
                Exit For
            End If
        Next
    End Sub
    Protected Sub SetGridColumnVisibility(ByVal pSet As Boolean)
        gvJournal.Columns(8).Visible = pSet
        gvJournal.Columns(9).Visible = pSet

        If BSU_IsTAXEnabled = True Then
            gvJournal.Columns(7).Visible = True
        Else
            gvJournal.Columns(7).Visible = False
        End If
    End Sub
    Protected Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        btnAdds.Visible = True
        SetGridColumnVisibility(True)
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
        gvJournal.SelectedIndex = -1
        Clear_Details()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim iIndex As Integer = 0
            Dim str_Search As String = ""
            If ViewState("datamode") = "view" Then
                lblError.Text = "Record not in edit mode !!!"
                Exit Sub
            End If
            Dim dCrordb As Double = AccountFunctions.Round(txtDAmount.Text)
            str_Search = h_Editid.Value
            '''''
            txtDAccountName.Text = AccountFunctions.check_accountid(Detail_ACT_ID.Text & "", Session("sBsuid"))

            '''''FIND ACCOUNT IS THERE 
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String
                str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
                    & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
                    & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
                    & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
                    & " AND ACT_ID='" & Detail_ACT_ID.Text & "'" _
                    & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
                    & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

                '& " order by gm.GPM_DESCR "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                Else
                    txtDAccountName.Text = ""
                    lblError.Text = getErrorMessage("303")
                    Exit Sub
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try

            '''''
            If dCrordb = 0 Then
                Exit Sub
            End If
            For iIndex = 0 To Session("dtDTL").Rows.Count - 1
                If str_Search = Session("dtDTL").Rows(iIndex)("id") And Session("dtDTL").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If (Session("dtDTL").Rows(iIndex)("Accountid") <> Detail_ACT_ID.Text.Trim) Then
                        ''updation handle here
                        Dim j As Integer = 0
                        If ViewState("datamode") <> "edit" Then
                            While j < Session("dtCostChild").Rows.Count
                                If Session("dtCostChild").Rows(j)("Voucherid") = str_Search Then
                                    Session("dtCostChild").Rows.Remove(Session("dtCostChild").Rows(j))
                                Else
                                    j = j + 1
                                End If
                            End While
                        End If
                        ''updation handle here
                    End If
                    If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                        CostCenterFunctions.AddCostCenter(str_Search, Session("sBsuid"), _
                                          Session("CostOTH"), txtHDocdate.Text, Session("idCostChild"), _
                                         Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                         Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    End If
                    Session("dtDTL").Rows(iIndex)("Accountid") = Detail_ACT_ID.Text.Trim
                    Session("dtDTL").Rows(iIndex)("Accountname") = txtDAccountName.Text.Trim
                    If dCrordb > 0 Then
                        Dim tempDebit As Double = 0
                        tempDebit = getTaxValue(ddlVATCode.SelectedValue, dCrordb)
                        Session("dtDTL").Rows(iIndex)("Credit") = "0"
                        Session("dtDTL").Rows(iIndex)("Debit") = (tempDebit + dCrordb) 'dCrordb


                    Else
                        Dim tempCredit As Double = 0
                        tempCredit = getTaxValue(ddlVATCode.SelectedValue, dCrordb)
                        Session("dtDTL").Rows(iIndex)("Credit") = (tempCredit + dCrordb) * -1
                        Session("dtDTL").Rows(iIndex)("Debit") = "0"
                    End If
                    If BSU_IsTAXEnabled Then
                        Session("dtDTL").Rows(iIndex)("TaxCode") = ddlVATCode.SelectedValue
                    End If

                    Session("dtDTL").Rows(iIndex)("Narration") = txtDNarration.Text.Trim
                    Session("dtDTL").Rows(iIndex)("CostReqd") = False

                    btnAdds.Visible = True
                    btnUpdate.Visible = False
                    btnEditCancel.Visible = False
                    gvJournal.SelectedIndex = iIndex
                    gvJournal.SelectedIndex = -1
                    SetGridColumnVisibility(True)
                    Clear_Details()
                    gridbind()
                    Exit For
                End If
            Next
        Catch ex As Exception
            Errorlog("UPDATE")
        End Try
    End Sub

    Private Sub clear_All()
        Session("dtDTL").Rows.Clear()
        Session("dtCostChild").Rows.Clear()
        gridbind()
        Clear_Details()
        txtHOldrefno.Text = ""
        getnextdocid()
        txtHNarration.Text = ""
        txtDNarration.Text = ""
    End Sub

    Private Sub Clear_Details()
        txtDAmount.Text = ""
        txtDNarration.Text = txtHNarration.Text & ""
        txtDAccountName.Text = ""
        Detail_ACT_ID.Text = ""
        ClearRadGridandCombo()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strfDate As String = txtHDocdate.Text.Trim
            Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            Else
                txtHDocdate.Text = strfDate
            End If

            'If Not ISCrossMatch() Then
            '    lblError.Text = "Debit and Credit Not Tallying. Check TAX added to line details."
            '    Exit Sub
            'End If

            str_err = check_Errors()
            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            End If
            ViewState("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here
                'Adding header info
                Dim cmd As New SqlCommand("SaveJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJHD_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpJHD_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@JHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpJHD_DOCTYPE As New SqlParameter("@JHD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpJHD_DOCTYPE.Value = "JV"
                cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                Dim sqlpJHD_DOCNO As New SqlParameter("@JHD_DOCNO", SqlDbType.VarChar, 20)
                If ViewState("datamode") = "edit" Then
                    sqlpJHD_DOCNO.Value = ViewState("str_editData").Split("|")(0) & ""
                Else
                    sqlpJHD_DOCNO.Value = " "
                End If
                cmd.Parameters.Add(sqlpJHD_DOCNO)

                Dim sqlpJHD_DOCDT As New SqlParameter("@JHD_DOCDT", SqlDbType.DateTime, 30)
                sqlpJHD_DOCDT.Value = txtHDocdate.Text & ""
                cmd.Parameters.Add(sqlpJHD_DOCDT)

                Dim sqlpJHD_CUR_ID As New SqlParameter("@JHD_CUR_ID", SqlDbType.VarChar, 12)
                sqlpJHD_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                cmd.Parameters.Add(sqlpJHD_CUR_ID)

                Dim sqlpJHD_NARRATION As New SqlParameter("@JHD_NARRATION", SqlDbType.VarChar, 300)
                sqlpJHD_NARRATION.Value = txtHNarration.Text & ""
                cmd.Parameters.Add(sqlpJHD_NARRATION)

                Dim sqlpJHD_EXGRATE1 As New SqlParameter("@JHD_EXGRATE1", SqlDbType.Decimal, 8)
                sqlpJHD_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                cmd.Parameters.Add(sqlpJHD_EXGRATE1)

                Dim sqlpJHD_EXGRATE2 As New SqlParameter("@JHD_EXGRATE2", SqlDbType.Decimal, 8)
                sqlpJHD_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                cmd.Parameters.Add(sqlpJHD_EXGRATE2)

                Dim sqlpJHD_bPOSTED As New SqlParameter("@JHD_bPOSTED", SqlDbType.Bit)
                sqlpJHD_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpJHD_bPOSTED)

                Dim sqlpJHD_bDELETED As New SqlParameter("@JHD_bDELETED", SqlDbType.Bit)
                sqlpJHD_bDELETED.Value = False
                cmd.Parameters.Add(sqlpJHD_bDELETED)

                Dim sqlpbGenerateNewNo As New SqlParameter("@bGenerateNewNo", SqlDbType.Bit)
                sqlpbGenerateNewNo.Value = True
                cmd.Parameters.Add(sqlpbGenerateNewNo)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim sqlpJHD_SESSIONID As New SqlParameter("@JHD_SESSIONID", SqlDbType.VarChar, 50)
                sqlpJHD_SESSIONID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpJHD_SESSIONID)

                Dim sqlpJHD_LOCK As New SqlParameter("@JHD_LOCK", SqlDbType.VarChar, 50)
                sqlpJHD_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpJHD_LOCK)

                Dim sqlpJHD_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                If ViewState("datamode") <> "edit" Then
                    sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpJHD_TIMESTAMP.Value = ViewState("str_timestamp")
                End If
                cmd.Parameters.Add(sqlpJHD_TIMESTAMP)


                Dim sqlpVHH_REFNO As New SqlParameter("@JHD_REFNO", SqlDbType.VarChar, 50)
                sqlpVHH_REFNO.Value = txtHOldrefno.Text
                cmd.Parameters.Add(sqlpVHH_REFNO)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlopJHD_NEWDOCNO As New SqlParameter("@JHD_NEWDOCNO", SqlDbType.VarChar, 20)
                cmd.Parameters.Add(sqlopJHD_NEWDOCNO)
                cmd.Parameters("@JHD_NEWDOCNO").Direction = ParameterDirection.Output
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Adding header info
                cmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String = ""
                If (iReturnvalue = 0) Then
                    ' stTrans.Commit()
                    If ViewState("datamode") <> "edit" Then
                        str_err = DoTransactions(objConn, stTrans, sqlopJHD_NEWDOCNO.Value)
                    Else
                        str_err = DeleteJOURNAL_D_S_ALL(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        If str_err = 0 Then
                            str_err = DoTransactions(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        End If
                    End If
                    If str_err = "0" Then
                        stTrans.Commit()
                        h_editorview.Value = ""

                        btnSave.Enabled = False
                        gvJournal.Enabled = True
                        ''lblError.Text = getErrorMessage("304")
                        txtDNarration.Text = ""
                        If ViewState("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, sqlopJHD_NEWDOCNO.Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            lblError.Text = getErrorMessage("304")
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            'datamode = Encr_decrData.Encrypt("view")
                            'Response.Redirect("acccrCashReceipt.aspx?viewid=" & Request.QueryString("viewid") & "&edited=yes" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamode)
                            lblError.Text = getErrorMessage("305")
                        End If
                        clear_All()
                    Else '.Split("__")(0).Trim & ""
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim)
                        stTrans.Rollback()
                    End If
                Else
                    lblError.Text = getErrorMessage(iReturnvalue & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub

    Private Function DeleteJOURNAL_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()

        cmd = New SqlCommand("DeleteJOURNAL_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpJDS_SUB_ID As New SqlParameter("@JDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@JDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@JDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@JDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = "JV"
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@JDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        Try
            Dim iIndex As Integer = 0
            ' ''FIND OPPOSITE ACCOUNT
            Dim str_DR_LargestAccount, str_CR_LargestAccount As String
            str_DR_LargestAccount = ""
            str_CR_LargestAccount = ""
            Dim d_cr_largest As Double = 0
            Dim d_dr_largest As Double = 0
            For iIndex = 0 To Session("dtDTL").Rows.Count - 1
                If Session("dtDTL").Rows(iIndex)("Debit") > 0 Then
                    If Session("dtDTL").Rows(iIndex)("Debit") > d_dr_largest Then
                        str_DR_LargestAccount = Session("dtDTL").Rows(iIndex)("Accountid")
                        d_dr_largest = Session("dtDTL").Rows(iIndex)("Debit")
                    End If
                Else
                    If Session("dtDTL").Rows(iIndex)("Credit") > d_cr_largest Then
                        str_CR_LargestAccount = Session("dtDTL").Rows(iIndex)("Accountid")
                        d_cr_largest = Session("dtDTL").Rows(iIndex)("Credit")
                    End If
                End If
            Next
            'Adding transaction info
            Dim cmd As New SqlCommand
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtDTL").Rows.Count - 1
                If Session("dtDTL").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    cmd.Dispose()
                    cmd = New SqlCommand("SaveJOURNAL_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure
                    '' ''Handle sub table
                    Dim str_crdb As String = "CR"
                    If Session("dtDTL").Rows(iIndex)("Debit") > 0 Then
                        dTotal = Session("dtDTL").Rows(iIndex)("Debit")
                        str_crdb = "DR"
                    Else
                        dTotal = Session("dtDTL").Rows(iIndex)("Credit")
                    End If
                    iReturnvalue = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtDTL").Rows(iIndex)("id"), _
                    str_crdb, iIndex + 1 - ViewState("iDeleteCount"), Session("dtDTL").Rows(iIndex)("Accountid"), dTotal)
                    If iReturnvalue <> "0" Then
                        Return iReturnvalue
                    End If
                    '' ''
                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                    sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim sqlpJNL_SUB_ID As New SqlParameter("@JNL_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJNL_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpJNL_SUB_ID)

                    Dim sqlpsqlpJNL_BSU_ID As New SqlParameter("@JNL_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJNL_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJNL_BSU_ID)

                    Dim sqlpJNL_FYEAR As New SqlParameter("@JNL_FYEAR", SqlDbType.Int)
                    sqlpJNL_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJNL_FYEAR)

                    Dim sqlpJNL_DOCTYPE As New SqlParameter("@JNL_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJNL_DOCTYPE.Value = "JV"
                    cmd.Parameters.Add(sqlpJNL_DOCTYPE)

                    Dim sqlpJNL_DOCNO As New SqlParameter("@JNL_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJNL_DOCNO.Value = p_docno & ""
                    cmd.Parameters.Add(sqlpJNL_DOCNO)

                    Dim sqlpJNL_DOCDT As New SqlParameter("@JNL_DOCDT", SqlDbType.DateTime, 30)
                    sqlpJNL_DOCDT.Value = txtHDocdate.Text & ""
                    cmd.Parameters.Add(sqlpJNL_DOCDT)

                    Dim sqlpJNL_NARRATION As New SqlParameter("@JNL_NARRATION", SqlDbType.VarChar, 300)
                    sqlpJNL_NARRATION.Value = Session("dtDTL").Rows(iIndex)("Narration") & ""
                    cmd.Parameters.Add(sqlpJNL_NARRATION)

                    Dim sqlpJNL_CUR_ID As New SqlParameter("@JNL_CUR_ID", SqlDbType.VarChar, 12)
                    sqlpJNL_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                    cmd.Parameters.Add(sqlpJNL_CUR_ID)

                    Dim sqlpJNL_EXGRATE1 As New SqlParameter("@JNL_EXGRATE1", SqlDbType.Decimal, 8)
                    sqlpJNL_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                    cmd.Parameters.Add(sqlpJNL_EXGRATE1)

                    Dim sqlpJNL_EXGRATE2 As New SqlParameter("@JNL_EXGRATE2", SqlDbType.Decimal, 8)
                    sqlpJNL_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                    cmd.Parameters.Add(sqlpJNL_EXGRATE2)

                    Dim sqlpJNL_ACT_ID As New SqlParameter("@JNL_ACT_ID", SqlDbType.VarChar, 20)
                    sqlpJNL_ACT_ID.Value = Session("dtDTL").Rows(iIndex)("Accountid") & ""
                    cmd.Parameters.Add(sqlpJNL_ACT_ID)

                    Dim sqlpJNL_RECONDT As New SqlParameter("@JNL_RECONDT", SqlDbType.DateTime, 30)
                    sqlpJNL_RECONDT.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpJNL_RECONDT)

                    Dim sqlpJNL_CHQNO As New SqlParameter("@JNL_CHQNO", SqlDbType.VarChar, 20)
                    sqlpJNL_CHQNO.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpJNL_CHQNO)

                    Dim sqlpJNL_CHQDT As New SqlParameter("@JNL_CHQDT", SqlDbType.DateTime, 30)
                    sqlpJNL_CHQDT.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpJNL_CHQDT)

                    Dim sqlpJNL_CREDIT As New SqlParameter("@JNL_CREDIT", SqlDbType.Decimal)
                    sqlpJNL_CREDIT.Value = Session("dtDTL").Rows(iIndex)("Credit")
                    cmd.Parameters.Add(sqlpJNL_CREDIT)

                    Dim sqlpJNL_DEBIT As New SqlParameter("@JNL_DEBIT", SqlDbType.Decimal, 8)
                    sqlpJNL_DEBIT.Value = Session("dtDTL").Rows(iIndex)("Debit")
                    cmd.Parameters.Add(sqlpJNL_DEBIT)

                    Dim sqlpbJNL_BDELETED As New SqlParameter("@JNL_BDELETED", SqlDbType.Bit)
                    sqlpbJNL_BDELETED.Value = False
                    cmd.Parameters.Add(sqlpbJNL_BDELETED)

                    Dim sqlpbJNL_SLNO As New SqlParameter("@JNL_SLNO", SqlDbType.Int)
                    sqlpbJNL_SLNO.Value = iIndex + 1 - ViewState("iDeleteCount")
                    cmd.Parameters.Add(sqlpbJNL_SLNO)

                    Dim sqlpJNL_BDISCOUNTED As New SqlParameter("@JNL_BDISCOUNTED", SqlDbType.Bit)
                    sqlpJNL_BDISCOUNTED.Value = False
                    cmd.Parameters.Add(sqlpJNL_BDISCOUNTED)

                    Dim sqlpJNL_REFDOCTYPE As New SqlParameter("@JNL_REFDOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJNL_REFDOCTYPE.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpJNL_REFDOCTYPE)

                    Dim sqlpbJNL_REFDOCNO As New SqlParameter("@JNL_REFDOCNO", SqlDbType.Bit)
                    sqlpbJNL_REFDOCNO.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpbJNL_REFDOCNO)

                    Dim sqlpJNL_OPP_ACT_ID As New SqlParameter("@JNL_OPP_ACT_ID", SqlDbType.VarChar, 10)
                    If Session("dtDTL").Rows(iIndex)("Debit") > 0 Then
                        sqlpJNL_OPP_ACT_ID.Value = str_CR_LargestAccount
                    Else
                        sqlpJNL_OPP_ACT_ID.Value = str_DR_LargestAccount
                    End If
                    cmd.Parameters.Add(sqlpJNL_OPP_ACT_ID)

                    Dim sqlpJNL_ACTVOUCHERDT As New SqlParameter("@JNL_ACTVOUCHERDT", SqlDbType.VarChar, 10)
                    sqlpJNL_ACTVOUCHERDT.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpJNL_ACTVOUCHERDT)

                    If BSU_IsTAXEnabled = True Then
                        cmd.Parameters.AddWithValue("@JNL_TAX_CODE", Session("dtDTL").Rows(iIndex)("TaxCode") & "")
                    Else
                        cmd.Parameters.AddWithValue("@JNL_TAX_CODE", "")
                    End If

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    If ViewState("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                    Else
                        sqlpbEdit.Value = False
                    End If
                    cmd.Parameters.Add(sqlpbEdit)

                    Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                    If iIndex = Session("dtDTL").Rows.Count - 1 Then
                        sqlpbLastRec.Value = True
                    Else
                        sqlpbLastRec.Value = False
                    End If
                    cmd.Parameters.Add(sqlpbLastRec)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    Dim sqlpJNL_bVALIDATE_ACCOUNT As New SqlParameter("@JNL_bVALIDATE_ACCOUNT", SqlDbType.Bit)
                    sqlpJNL_bVALIDATE_ACCOUNT.Value = True
                    cmd.Parameters.Add(sqlpJNL_bVALIDATE_ACCOUNT)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""
                    If iReturnvalue <> 0 Then

                        Exit For
                    Else

                    End If
                    cmd.Parameters.Clear()
                Else
                    If Not Session("dtDTL").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                        ViewState("iDeleteCount") = ViewState("iDeleteCount") + 1
                        cmd = New SqlCommand("DeleteJOURNAL_D", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                        sqlpGUID.Value = Session("dtDTL").Rows(iIndex)("GUID")
                        cmd.Parameters.Add(sqlpGUID)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        Dim success_msg As String = ""

                        If iReturnvalue <> 0 Then
                            lblError.Text = getErrorMessage(iReturnvalue)
                        End If
                        cmd.Parameters.Clear()
                    End If
                End If
            Next
            If iIndex <= Session("dtDTL").Rows.Count - 1 Then
                Return iReturnvalue
            Else
                Return iReturnvalue
            End If
            'Adding transaction info
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function




    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String

        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), "JV", _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                txtHDocdate.Text, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), "JV", "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue

    End Function

    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then
                dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
            End If
        Next
        If Math.Round(dTotal, 4) = Math.Round(p_total, 4) Then
            Return True
        Else
            Return False
        End If
    End Function

    Function check_others(ByVal p_voucherid As String, ByVal p_total As String) As Boolean
        Try
            Dim dTotal As Double = 0
            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" _
            & " AND JHD_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then


                'txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")
                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("JHD_DOCNO") & "|" _
                   & ds.Tables(0).Rows(0)("JHD_SUB_ID") & "|" _
                   & ds.Tables(0).Rows(0)("JHD_DOCDT") & "|" _
                   & ds.Tables(0).Rows(0)("JHD_FYEAR") & "|"
                    txtHDocno.Text = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                    Dim cmd As New SqlCommand("LockJOURNAL_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    'If ds.Tables(0).Rows(0)("bPosted") = True Then
                    '    Return 1000
                    'End If
                    Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@JHD_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@JHD_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = "JV"
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@JHD_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ViewState("str_editData").Split("|")(0)
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_DOCDT As New SqlParameter("@JHD_DOCDT", SqlDbType.DateTime, 30)
                    sqlpJHD_DOCDT.Value = ds.Tables(0).Rows(0)("JHD_DOCDT") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCDT)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                    cmd.Parameters("@JHD_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                        Return iReturnvalue
                    End If
                    ViewState("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                    Return iReturnvalue

                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
                ViewState("str_editData") = ""
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            'Return " | | "
        End Try
        Return True
    End Function

    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
            & " JHD_DOCNO='" & ViewState("str_editData").Split("|")(0) & "'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("JHD_DOCDT")
                txtHDocno.Text = ViewState("str_editData").Split("|")(0)
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = ds.Tables(0).Rows(0)("JHD_DOCTYPE") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopJHD_TIMESTAMP.Value = ViewState("str_timestamp")
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = (iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'tbl_Details.Visible = False
        tbl_Details.Attributes.Add("style", "display:none")
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
            End If
        Else
            h_editorview.Value = "Edit"
            ResetViewData()
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            btnSave.Enabled = True
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" And ViewState("ReferrerUrl") Is Nothing Then
            Response.Redirect(Session("ReferrerUrl"))
        ElseIf ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            h_editorview.Value = ""
            If ViewState("datamode") = "edit" Then
                unlock()
            End If
            setViewData()
            'clear_All()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        objConn.Open()
        str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
               & " GUID='" & Request.QueryString("viewid") & "'"
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then

                Dim cmd As New SqlCommand("DeleteJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("JHD_SUB_ID") & ""
                cmd.Parameters.Add(sqlpJHD_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@JHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "JV"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value

                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                Else
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    stTrans.Commit()
                    lblError.Text = getErrorMessage("516")
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    clear_All()
                    Response.Redirect("accjvViewJournalVoucher.aspx?deleted=done" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
                End If
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Function set_viewdata() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" ' _
            If Request.QueryString("BSUID") Is Nothing Then
                str_Sql += " AND JHD_BSU_ID='" & Session("sBsuid") & "'"
            Else
                str_Sql += " AND JHD_BSU_ID='" & Request.QueryString("BSUID") & "'"
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("JHD_DOCDT")
                txtHDocno.Text = ds.Tables(0).Rows(0)("JHD_DOCNO")
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")

                ViewState("str_editData") = ds.Tables(0).Rows(0)("JHD_DOCNO") & "|" _
               & ds.Tables(0).Rows(0)("JHD_SUB_ID") & "|" _
               & ds.Tables(0).Rows(0)("JHD_DOCDT") & "|" _
               & ds.Tables(0).Rows(0)("JHD_FYEAR") & "|"
                Return ""
            Else
                ViewState("str_editData") = ""
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
        End Try
        Return True
    End Function

    Protected Sub lbAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If ViewState("datamode") = "edit" Then
            unlock()
        End If
        ResetViewData()
        clear_All()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Function check_Errors() As String
        Dim str_Error As String = ""
        Dim str_Err As String = ""
        Dim iIndex As Integer
        Dim dTotal As Double = 0
        'For iIndex = 0 To Session("dtDTL").Rows.Count - 1
        'Dim str_manndatory_costcenter As String
        'If Convert.ToBoolean(Session("dtDTL").Rows(iIndex)("CostReqd")) = True Then
        '    str_manndatory_costcenter = Session("dtDTL").Rows(iIndex)("Ply")
        'Else
        '    str_manndatory_costcenter = ""
        'End If
        'If Session("dtDTL").Rows(iIndex)("Status") & "" <> "Deleted" Then
        '    If Session("dtDTL").Rows(iIndex)("Credit") > 0 Then
        '        str_Err = check_Errors_sub(Session("dtDTL").Rows(iIndex)("id"), _
        '         Session("dtDTL").Rows(iIndex)("Credit"), str_manndatory_costcenter)
        '    Else
        '        str_Err = check_Errors_sub(Session("dtDTL").Rows(iIndex)("id"), _
        '         Session("dtDTL").Rows(iIndex)("Debit"), str_manndatory_costcenter)
        '    End If

        '    If str_Err <> "" Then
        '        str_Error = str_Error & "<br/>" & str_Err & " At Line - " & iIndex + 1
        '    End If
        'End If
        'Next
        If Session("dtDTL").Rows.Count = 0 Then
            str_Error = str_Error & "<br/>Please add transaction info"
        End If
        Return str_Error
    End Function

    'Private Function check_Errors_sub(ByVal p_voucherid As String, ByVal p_amount As String, _
    'Optional ByVal p_mandatory_costcenter As String = "") As String
    '    Try
    '        Dim str_cur_cost_center As String = ""
    '        Dim str_prev_cost_center As String = ""
    '        'Dim dTotal As Double = 0
    '        Dim iIndex As Integer
    '        Dim iLineid As Integer
    '        Dim bool_check_other, bool_mandatory_exists As Boolean

    '        Dim str_err As String = ""
    '        If p_mandatory_costcenter <> "" Then
    '            bool_mandatory_exists = False
    '        Else
    '            bool_mandatory_exists = True
    '        End If
    '        Dim str_balanced As Boolean = True
    '        bool_check_other = False
    '        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
    '            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
    '                If Session("dtCostChild").Rows(iIndex)("Ply") = p_mandatory_costcenter Then
    '                    bool_mandatory_exists = True
    '                End If
    '                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("Ply") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
    '                    iLineid = -1
    '                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("Ply"), p_amount)
    '                    If str_balanced = False Then
    '                        str_err = str_err & " <BR> Invalid Allocation for cost center " & AccountFunctions.get_cost_center(Session("dtCostChild").Rows(iIndex)("Ply"))
    '                    End If
    '                End If
    '                str_balanced = True
    '                If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And bool_check_other = False Then
    '                    iLineid = -1
    '                    bool_check_other = True
    '                    str_balanced = check_others(p_voucherid, p_amount)
    '                    If str_balanced = False Then
    '                        If str_err = "" Then
    '                            str_err = "Invalid Allocation for other cost center"
    '                        Else
    '                            str_err = str_err & " <BR> Invalid Allocation for other cost centers "
    '                        End If

    '                    End If
    '                End If
    '                iLineid = iLineid + 1
    '                'If str_balanced = False Then
    '                '    Exit For
    '                'End If
    '                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("Ply")
    '            End If
    '        Next
    '        If bool_mandatory_exists = False Then
    '            str_err = "<br>Mandatory Cost center - " & AccountFunctions.get_cost_center(p_mandatory_costcenter) & " not allocated"
    '        End If
    '        Return str_err
    '    Catch ex As Exception
    '        Errorlog(ex.Message, "child")
    '        Return ""
    '    End Try
    'End Function

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        'Session("ReportSource") = AccountsReports.JournalVouchers(txtHDocno.Text, txtHDocdate.Text, Session("sBSUID"), Session("F_YEAR"))
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        Dim repSource As New MyReportClass
        repSource = VoucherReports.JournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "JV", txtHDocno.Text, Session("HideCC"))
        Session("ReportSource") = repSource
        '  Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub imgCaledar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub Detail_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail_ACT_ID.TextChanged
        chk_DetailsAccount()
        ClearRadGridandCombo()
    End Sub

    Sub chk_DetailsAccount()
        txtDAccountName.Text = AccountFunctions.Validate_Account(Detail_ACT_ID.Text, Session("sbsuid"), "NORMAL")
        If txtDAccountName.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            Detail_ACT_ID.Focus()
        Else
            lblError.Text = ""
            txtDAmount.Focus()
        End If
    End Sub

    Protected Sub btnHAccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHAccount.Click
        chk_DetailsAccount()
    End Sub

    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function

    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIdCostchild As New Label
            lblIdCostchild = e.Row.FindControl("lblIdCostchild")
            Dim gvCostAllocation As New GridView
            gvCostAllocation = e.Row.FindControl("gvCostAllocation")
            If Not Session("CostAllocation") Is Nothing AndAlso Not gvCostAllocation Is Nothing Then
                gvCostAllocation.Attributes.Add("bordercolor", "#fc7f03")
                Dim dv As New DataView(Session("CostAllocation"))
                If Session("CostAllocation").Rows.Count > 0 Then
                    dv.RowFilter = "CostCenterID='" & lblIdCostchild.Text & "' "
                End If
                dv.Sort = "CostCenterID"
                gvCostAllocation.DataSource = dv.ToTable
                gvCostAllocation.DataBind()
            End If
        End If
    End Sub

    Protected Sub lbUploadEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUploadEmployee.Click
        If Not fuEmployeeData.HasFile Then
            lblError.Text = "Please select the file...!"
            Exit Sub
        End If
        If Not (fuEmployeeData.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("csv", StringComparison.OrdinalIgnoreCase)) Then
            lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
            Exit Sub
        End If
        UpLoadDBF()
    End Sub

    Private Sub UpLoadDBF()
        RecreateSsssionDataSource()
        CostCenterFunctions.UploadAndPopulateEmployeeDataFromExcel(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), Session("sBSuid"), _
                     CostCenterIDRadComboBoxMain.SelectedValue, CostCenterIDRadComboBoxMain.Text, txtHDocdate.Text, fuEmployeeData, Session("sUsr_name"))
        usrCostCenter1.BindCostCenter()
    End Sub

    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub

    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()

        CostCenterIDRadComboBoxMain.Text = ""
        CostCenterIDRadComboBoxMain.ClearSelection()
        CostCenterIDRadComboBoxMain.DataBind()
    End Sub

End Class
