<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accFeeBankDepositVoucher.aspx.vb" Inherits="Accounts_accFeeBankDepositVoucher" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function getBank() {
            popUp('460', '400', 'BANK', '<%=txtBankCode.ClientId %>', '<%=txtBankDescr.ClientId %>');
    }

    function getreceived() {
        popUp('960', '600', 'NOTCC', '<%=txtPartyCode.ClientId %>', '<%=txtPartyDescr.ClientId %>', '<%=ddDCollection.ClientId %>');
    }

    function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {
 
        var NameandCode;
        var url;
        //document.getElementById(<%=hf_PmMode.ClientID%>).value = "Johnny Bravo";
        document.getElementById('<%=hf_PmMode.ClientID%>').value = pMode;
        document.getElementById('<%=hf_ctrl.ClientID %>').value = ctrl;
        document.getElementById('<%=hf_ctrl2.ClientID%>').value = ctrl1;
        if (pMode == 'BANK') {
            url = "PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value;
            //alert(url)
            radopen(url, "pop_up");
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl).value = lstrVal[0];
            //document.getElementById(ctrl1).value = lstrVal[1];

        }
        else if (pMode == 'NOTCC') {
            if (ctrl2 == '' || ctrl2 == undefined) {
               radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");
            } else {
                radopen("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "pop_up");
            }
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('||');
            //document.getElementById(ctrl).value = lstrVal[0];
            //document.getElementById(ctrl1).value = lstrVal[1];

            //document.getElementById(ctrl2).value=lstrVal[2];
            // document.getElementById(ctrl3).value=lstrVal[3];

        }
        else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
            QRYSTR = '';
            if (pMode == 'CASHFLOW_BP')
                QRYSTR = "?rss=0";
            if (pMode == 'CASHFLOW_BR')
                QRYSTR = "?rss=1";

            radopen("acccpShowCashflow.aspx" + QRYSTR, "pop_up");
            //if (result == '' || result == undefined)
            //{ return false; }
            //lstrVal = result.split('__');
            //document.getElementById(ctrl).value = lstrVal[0];


        }

        //if (pMode == 'SUBGRP') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];
        //    document.getElementById(ctrl2).value = lstrVal[0];
        //}
        //else if (pMode == 'RFS') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];

        //}

        //else if (pMode == 'DOCTYPE') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];


        //}
        //else if (pMode == 'EMPS') {

        //    result = window.showModalDialog("ShowEmp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];

        //}

        //else if (pMode == 'DEPT') {

        //    result = window.showModalDialog("ShowDept.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];

        //}

        //else if (pMode == 'BSU') {

        //    result = window.showModalDialog("ShowBSU.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];

        //}

        //else if (pMode == 'COLLN') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];
        //    document.getElementById(ctrl2).value = lstrVal[0];
        //    document.getElementById(ctrl3).value = lstrVal[1];

        //}


        //else if (pMode == 'RSS') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];


        //}
        //else if (pMode == 'CCM') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];

        //}
        //else if (pMode == 'CCS') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];

        //}
        //else if (pMode == 'CTY') {

        //    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];

        //}

        //else if (pMode == 'CONTROLACC') {
        //    var ddsel = '';
        //    var selObj = document.getElementById(acctype);
        //    ddsel = selObj.options[selObj.selectedIndex].value;

        //    result = window.showModalDialog("ShowControl.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value + "&acctype=" + ddsel, "", sFeatures);

        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];
        //    document.getElementById(ctrl4).value = lstrVal[2];
        //    if (document.getElementById(ctrl).value == '06101001')
        //        //{ document.getElementById(ctrl4).value='063'; }
        //        //document.getElementById(ctrl5).value=lstrVal[0].substring(3,lstrVal[0].length); 
        //        document.getElementById(ctrl5).value = lstrVal[4];


        //    var L = selObj.options.length;
        //    for (var i = 0; i <= L - 1; i++) {
        //        if (lstrVal[5] == selObj.options[i].value) { selObj.options.selectedIndex = i; }

        //    }
        //}
        //else if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {

        //    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);

        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];
        //}
        //else if (pMode == 'EDITDEP') {

        //    result = window.showModalDialog("accgenbrcc.aspx?ShowType=" + pMode + "", "", sFeatures);

        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl1).value = lstrVal[0];
        //    document.getElementById(ctrl2).value = lstrVal[1];
        //}
        //else if (pMode == 'ALLOC') {

        //    result = window.showModalDialog("TestAlloc.aspx?ShowType=" + pMode + "", "", sFeatures);

        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');
        //    document.getElementById(ctrl).value = lstrVal[0];
        //    document.getElementById(ctrl1).value = lstrVal[1];
        //}
        //else if (pMode == 'ALLOCATE') {

        //    result = window.showModalDialog("TestAlloc.aspx?Id=" + ctrl + "&pAmount=" + document.getElementById(ctrl1).value + "&pPly=" + document.getElementById(ctrl2).value + "", "", sFeatures);

        //    if (result == '' || result == undefined)
        //    { return false; }
        //    lstrVal = result.split('||');

        //}
        //else if (pMode == 'SHOWRSS') {

        //    result = window.showModalDialog("ShowRSS.aspx?ShowType=" + pMode + "", "", sFeatures);

        //    if (result == '' || result == undefined)
        //    { return false; }

        //    document.getElementById(ctrl).value = replaceSubstring(result, "|", " ");



        //}
        //else if (pMode == 'INTRAC') {

        //        result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];

        //    }

        //else if (pMode == 'ACRDAC') {

        //        result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];

        //    }

        //else if (pMode == 'PREPDAC') {


        //        result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];
        //        if (ctrl2 != '')
        //            if (document.getElementById(ctrl2).value == '') {
        //                document.getElementById(ctrl2).value = lstrVal[2];
        //                document.getElementById(ctrl3).value = lstrVal[3];
        //            }
        //    }

        //else if (pMode == 'CHQISSAC') {

        //        result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];

        //    }
        //else if (pMode == 'CHQISSAC_PDC') {

        //        result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];
        //        if (document.getElementById(ctrl2).value == '') {
        //            document.getElementById(ctrl2).value = lstrVal[2];
        //            document.getElementById(ctrl3).value = lstrVal[3];
        //        }
        //    }

        //else if (pMode == 'CHQBOOK') {
        //        if (document.getElementById(ctrl3).value == "") {
        //            alert("Please Select The Bank");
        //            return false;
        //        }

        //        result = window.showModalDialog("ShowChqs.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl3).value + "", "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');

        //        document.getElementById(ctrl).value = lstrVal[1];
        //        document.getElementById(ctrl1).value = lstrVal[0];
        //        document.getElementById(ctrl2).value = lstrVal[2];

        //        document.getElementById(ctrl).focus();

        //    }
        //else if (pMode == 'CHQBOOK_PDC') {
        //        if (document.getElementById(ctrl3).value == "") {
        //            alert("Please Select The Bank");
        //            return false;
        //        }

        //        result = window.showModalDialog("ShowChqs.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');

        //        document.getElementById(ctrl).value = lstrVal[1];
        //        document.getElementById(ctrl1).value = lstrVal[0];
        //        document.getElementById(ctrl2).value = lstrVal[2];
        //        document.getElementById(ctrl).focus();

        //    }



        //else if (pMode == 'PDCCHQBOOK') {
        //        if (document.getElementById(ctrl2).value == "") {
        //            alert("Please Select The Bank");
        //            return false;
        //        }
        //        else if (document.getElementById(ctrl3).value == "") {
        //            alert("Please Enter The Installment Months");
        //            return false;
        //        }
        //        result = window.showModalDialog("ShowChqsPDC.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl2).value + "&MonthsReq=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');

        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];

        //        document.getElementById(ctrl).focus();


        //    }
        //else if (pMode == 'PARTY') {

        //        result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];

        //        document.getElementById(ctrl2).value = lstrVal[2];
        //        document.getElementById(ctrl3).value = lstrVal[3];

        //    }
        //    //EDITED BY GURU - FILTER BY PARTY, SHOW DEBIT ACC//
        //else if (pMode == 'DEBIT_D' || pMode == 'PARTY1' || pMode == 'DEBIT' || pMode == 'PARTY2') {

        //        result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);

        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];
        //        if (pMode == 'DEBIT' && (document.getElementById(ctrl2).value == '')) {
        //            document.getElementById(ctrl2).value = lstrVal[0];
        //            document.getElementById(ctrl3).value = lstrVal[1];

        //            document.getElementById(ctrl4).value = lstrVal[2];
        //            document.getElementById(ctrl5).value = lstrVal[3];
        //        }
        //        if (pMode == 'DEBIT_D') {
        //            document.getElementById(ctrl2).value = lstrVal[2];
        //            document.getElementById(ctrl3).value = lstrVal[3];
        //        }
        //    }
        //else if (pMode == 'ALL') {

        //        result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];
        //        document.getElementById(ctrl2).value = lstrVal[2];
        //        document.getElementById(ctrl3).value = lstrVal[3];

        //    }
        //else if (pMode == 'NORMAL') {

        //        result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl1).value = lstrVal[1];
        //    //document.getElementById(ctrl2).value=lstrVal[2];
        //    //document.getElementById(ctrl3).value=lstrVal[3];

        //    }

        //else if (pMode == 'MSTCHQBOOK') {
        //        result = window.showModalDialog("accShowChequebook.aspx?BankCode=" + document.getElementById(ctrl1).value + "", "", sFeatures);
        //        if (result == '' || result == undefined)
        //        { return false; }
        //        lstrVal = result.split('||');
        //        document.getElementById(ctrl).value = lstrVal[0];
        //        document.getElementById(ctrl2).value = lstrVal[1];
        //    }


    }


    function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var pMode = document.getElementById('<%=hf_PmMode.ClientID %>').value;
         var ctrl =  document.getElementById('<%=hf_ctrl.ClientID %>').value ;
        var ctrl1 = document.getElementById('<%=hf_ctrl2.ClientID%>').value ;
         var arg = args.get_argument();
         if (arg) {
             NameandCode = arg.NameandCode.split('||');

             if (pMode == 'BANK') {
                 document.getElementById(ctrl).value = NameandCode[0];
                 document.getElementById(ctrl1).value = NameandCode[1];
                 __doPostBack(ctrl, 'TextChanged');
             }
             else if (pMode == 'NOTCC') {
                 document.getElementById(ctrl).value = NameandCode[0];
                 document.getElementById(ctrl1).value = NameandCode[1];
                 __doPostBack(ctrl, 'TextChanged');
             }
             else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                 document.getElementById(ctrl).value = NameandCode[0];
                 __doPostBack(ctrl, 'TextChanged');
             }
                <%--document.getElementById('<%=txtempname.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=h_EMP_ID.ClientID %>').value = NameandCode[1];
                    __doPostBack('<%= txtempname.ClientID %>', 'TextChanged');--%>
            }
     }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Cheque Deposit Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" align="center">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                    </tr>
                </table>

                <table width="100%" align="center">
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Doc No [Old Ref No]</span></td>

                        <td width="30%" align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtdocNo" runat="server"
                                            Width="49%"></asp:TextBox></td>
                                    <td align="left" width="50%">[<asp:TextBox ID="txtOldDocNo" runat="server"
                                        Width="35%"></asp:TextBox>
                                        ]</td>
                                </tr>
                            </table>

                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Doc Date</span></td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocDate" runat="server"
                                Width="66%" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton1" TargetControlID="txtdocDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtdocDate" TargetControlID="txtdocDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Bank Account</span></td>

                        <td colspan="4" align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getBank();return false;" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Bank Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" width="20%">
                            <span class="field-label">Collection Type</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddCollection" runat="server" TabIndex="10" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                </table>

                <table width="100%" align="center">
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Currency</span></td>

                        <td width="30%" align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtExchRate" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Group Rate</span></td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLocalRate" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr class="gridheader_pop">
                        <td align="left" width="20%"><span class="field-label">Credit Details </span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtNarrn" runat="server" TextMode="MultiLine" CssClass="inputbox" Visible="False"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Collection Type</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddDCollection" runat="server" AutoPostBack="true"
                                TabIndex="20" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Credit Account</span></td>

                        <td colspan="3" align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPartyCode" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgRcvd" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getreceived();return false;" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Creditcard Holder</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPartyDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Narration</span></td>

                        <td colspan="3" align="left">
                            <asp:TextBox ID="txtLineNarrn" runat="server"
                                TextMode="MultiLine" CssClass="inputbox_multi" SkinID="MultiText"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Cheque No</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtChqBook" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <td align="left">
                            <span class="field-label">Cheque Date</span></td>

                        <td width="25%" align="left">
                            <asp:TextBox ID="txtChqDate" runat="server" Width="54%"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="ImageButton3" TargetControlID="txtChqDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtChqDate" TargetControlID="txtChqDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Amount</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtLineAmount" runat="server" Width="150px" AutoCompleteType="Disabled"></asp:TextBox>
                        </td>
                        <td align="left">
                            <span class="field-label">Cash Flow Line</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtCashFlow" runat="server"></asp:TextBox>
                            <a href="#" onclick="popUp('460','400','CASHFLOW_BR','<%=txtCashFlow.ClientId %>'); return false;">
                                <img border="0" src="../Images/cal.gif" id="IMG1" language="javascript" /></a>
                            &nbsp;
                        <asp:Button ID="btnFill" runat="server" CssClass="button" Text="Add" SkinID="SmallButton" />
                            <asp:Button ID="btnFillCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnFillCancel_Click" SkinID="SmallButton" />
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvDTL" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" SkinID="GridViewNormal" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="True">
                                        <ItemStyle Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Account Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="15%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Narration" HeaderText="Narration">
                                        <ItemStyle Width="15%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLine" HeaderText="CashFlow">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Colln" HeaderText="Colln">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" SkinID="Grid" runat="server" Text='<%#AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="5%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ChqBookId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqBookId" runat="server" Text='<%# Bind("ChqBookId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ChqBookLot" HeaderText="Cheque Book" Visible="False">
                                        <ItemStyle Width="5%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="ChqNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("ChqNo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="15%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ChqDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChqDate" runat="server" Text='<%# Bind("ChqDate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="15%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ply" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPLY" runat="server" Text='<%# Bind("Ply") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Costreqd" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostreqd" runat="server" Text='<%# Bind("Costreqd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            Edit                                
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit" OnClick="LinkButton1_Click" Enabled="False">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="DeleteBtn"
                                                CommandArgument='<%# Eval("id") %>'
                                                CommandName="Delete" runat="server" Enabled="False">
                                     Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"></td>
                        <td width="30%"></td>
                        <td align="right" width="20%"><span class="field-label">Total</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:CheckBox ID="chkVoucher" runat="server" CssClass="field-label" Text="Print Cheque Deposit Voucher" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hCheqBook" runat="server" />
                <asp:HiddenField ID="hf_PmMode" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hf_ctrl" runat="server"/>
                <asp:HiddenField ID="hf_ctrl2" runat="server" />
                <asp:HiddenField ID="hPLY" runat="server" />
                <asp:HiddenField ID="hNum" runat="server" Value="1" />
                <asp:HiddenField ID="hCostreqd" runat="server" Value="1" />

            </div>
        </div>
    </div>


</asp:Content>


