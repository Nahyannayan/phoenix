Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class AccEditPymnt
    Inherits System.Web.UI.Page
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim MainMnu_code As String = String.Empty
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (MainMnu_code <> "A100020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling page right class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights



                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))




                    If Request.QueryString("viewid") <> "" Then
                        ViewState("Viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        FillValues()

                    End If

                End If
                UtilityObj.beforeLoopingControls(Me.Page)

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    
    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim lstrSQL As String
            Dim ds As New DataSet



            lstrSQL = "SELECT * FROM PAYMENTTERM_M WHERE PTM_ID='" & ViewState("Viewid") & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                txtDescr.Text = ds.Tables(0).Rows(0)("PTM_DESCR")
                If (ds.Tables(0).Rows(0)("PTM_bPDC") = True) Then
                    optYes.Checked = True
                Else
                    optNo.Checked = True
                End If
                txtDays.Text = ds.Tables(0).Rows(0)("PTM_DAYS")
            End If
        Catch ex As Exception


        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim lintRetVal As Integer
        ServerValidate()

        '   --- Proceed To Save
        If (lstrErrMsg = "") Then
            If ViewState("datamode") = "edit" Or ViewState("datamode") = "view" Then


                objConn.Open()
                Try
                    Dim SqlCmd As New SqlCommand("SavePAYMENTTERM_M", objConn)
                    SqlCmd.CommandType = CommandType.StoredProcedure
                    SqlCmd.Parameters.AddWithValue("@PTM_ID", Convert.ToInt32(ViewState("Viewid")))
                    SqlCmd.Parameters.AddWithValue("@PTM_DESCR", Trim(txtDescr.Text))
                    SqlCmd.Parameters.AddWithValue("@PTM_bPDC", optYes.Checked)
                    SqlCmd.Parameters.AddWithValue("@PTM_Days", Convert.ToInt32(Val(Trim(txtDays.Text))))
                    SqlCmd.Parameters.AddWithValue("@bEdit", 1)
                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
                    If lintRetVal = 0 Then



                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("Viewid"), "Edit", Page.User.Identity.Name.ToString, Me.Page)

                        If flagAudit <> 0 Then

                            Throw New ArgumentException("Could not process your request")

                        End If
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Call clearall()
                        lblErr.Text = "Data Successfully Saved..."
                    Else
                        lblErr.Text = getErrorMessage(lintRetVal)
                    End If



                Catch myex As ArgumentException

                    lblErr.Text = myex.Message
                Catch ex As Exception

                    lblErr.Text = UtilityObj.getErrorMessage("1000")
                    UtilityObj.Errorlog(ex.Message)
                End Try

            End If

        End If
    End Sub
    Sub clearall()
        txtDescr.Text = ""
        txtDays.Text = ""
        optYes.Checked = True
    End Sub
    Protected Sub ServerValidate()
        lstrErrMsg = ""

        If ((Trim(txtDescr.Text) = "") Or (Master.StringWithSpace(txtDescr.Text) = False)) Then
            lstrErrMsg = lstrErrMsg & "From # Should Be A String Value" & "<br>"
        End If

        If IsNumeric(txtDays.Text) = False Then
            lstrErrMsg = lstrErrMsg & "Days Should Be Entered" & "<br>"
        End If



        lblErr.Text = lstrErrMsg
    End Sub
    Public Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("SAVED SUCCESSFULLY...")
            End If
        Catch ex As Exception

            Return ("0")
        End Try
    End Function


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear the textbox and set the default settings
            Call clearall()
            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        'set the rights on the button control based on the user
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim transaction As SqlTransaction
        Dim Status As Integer
        Dim Eid As String

        If txtDescr.Text.Trim = "" Then

            lblErr.Text = "Record do not exits"
        Else



            'Delete  the  user
            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                    Eid = ViewState("Viewid")
                    Status = AccessRoleUser.DeletePymnt(Eid, transaction)


                    If Status = -1 Then
                        Throw New ArgumentException("Record do not exits")
                    ElseIf Status <> 0 Then
                        Throw New ArgumentException("Record can not be Deleted")
                    Else
                        Status = UtilityObj.operOnAudiTable(Master.MenuName, Eid, "delete", Page.User.Identity.Name.ToString, Me.Page)


                        If Status <> 0 Then


                            Throw New ArgumentException("Could not complete your request")

                        End If
                        ViewState("datamode") = "none"

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        transaction.Commit()
                        Call clearall()
                        lblErr.Text = "Record Deleted Successfully"

                    End If

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblErr.Text = myex.Message
                Catch ex As Exception
                    transaction.Rollback()
                    lblErr.Text = "Record could not be Deleted"
                End Try
            End Using


        End If


    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim url As String
            ViewState("datamode") = "add"
            Dim MainMnu_code As String
            MainMnu_code = Request.QueryString("MainMnu_code")

            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))


            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~/accounts/AccAddPymnt.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))

            Response.Redirect(url)
        Catch ex As Exception
            '  lblErrorMessage.Text = "Request could not be processed "
        End Try
    End Sub
End Class
