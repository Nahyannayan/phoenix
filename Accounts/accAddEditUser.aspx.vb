Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic

Partial Class AddEditUser
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim temp_ID As String

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            txtRole.Attributes.Add("readonly", "readonly")
            txtEmpId.Attributes.Add("readonly", "readonly")
            txtDefBusUnit.Attributes.Add("readonly", "readonly")
            txtFeeCounter.Attributes.Remove("readonly")

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            Dim MainMnu_code As String = String.Empty
            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'if query string returns Eid  if datamode is view state
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or ((MainMnu_code <> "D050001") And (MainMnu_code <> "D050049")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling page right class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                'disable the control based on the rights

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If Not (Request.QueryString("type") Is Nothing) Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("type").Replace(" ", "+"))
                Else
                    ViewState("TYPE") = "0"
                End If

                If ViewState("datamode") = "view" Then
                    btnBusUnit.Visible = False
                    btnFeeCounter.Visible = False
                    btnEmp.Visible = False
                    btnRole.Visible = False
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                    '  txtUserId.Attributes.Add("Readonly", "Readonly")
                    txtUserName.Attributes.Add("readonly", "readonly")
                    txtDisplayName.Attributes.Add("readonly", "readonly")
                    txtPassword.Attributes.Add("Readonly", "Readonly")
                    txtConfpassword.Attributes.Add("Readonly", "Readonly")
                    Using readerUserDetail As SqlDataReader = AccessRoleUser.GetUserIDDetail(ViewState("Eid"))
                        While readerUserDetail.Read()
                            'txtUserId.Text = Convert.ToString(readerUserDetail("usr_id"))
                            txtUserName.Text = Convert.ToString(readerUserDetail("usr_name"))
                            If readerUserDetail.GetString(2) <> "" Then
                                txtPassword.Attributes.Add("value", Encr_decrData.Decrypt(Convert.ToString(readerUserDetail("usr_password"))))
                                txtConfpassword.Attributes.Add("value", Encr_decrData.Decrypt(Convert.ToString(readerUserDetail("usr_password"))))
                            End If
                            txtRole.Text = Convert.ToString(readerUserDetail("DESCR"))
                            txtEmpId.Text = Convert.ToString(readerUserDetail("Full_Name"))
                            txtDefBusUnit.Text = Convert.ToString(readerUserDetail("BSU_NAME"))
                            txtFeeCounter.Text = Convert.ToString(readerUserDetail("FCM_DESCR"))
                            hfFEECounter.Value = Convert.ToString(readerUserDetail("USR_FCM_ID"))
                            txtDisplayName.Text = Convert.ToString(readerUserDetail("Display_Name"))

                            hfEmpID.Value = Convert.ToString(readerUserDetail("Emp_id"))
                            hfDefBusUnit.Value = Convert.ToString(readerUserDetail("usr_bsu_id"))
                            hfRoleId.Value = Convert.ToInt64(readerUserDetail("rol_id"))
                            chkActive.Checked = Not Convert.ToBoolean(readerUserDetail("USR_Active"))
                        End While
                    End Using

                    Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim busString As String
                    If ViewState("TYPE") = "V2" Then
                        busString = "SELECT BSU_ID,BSU_NAME FROM dbo.MENURIGHTS_S INNER JOIN dbo.BUSINESSUNIT_M ON BSU_ID=MNR_BSU_ID WHERE " & _
                                 " MNR_ROL_ID=" & Session("sroleid") & " AND MNR_MNU_ID='D050049' ORDER BY bsu_name"
                    Else
                        busString = "select bsu_id,bsu_name from businessunit_m order by bsu_name"
                    End If


                    Dim userString As String = "select usa_bsu_id from Useraccess_s where usa_usr_id='" & ViewState("Eid") & "'"
                    Dim userMString As String = "select usr_bsu_id from users_m where usr_id='" & ViewState("Eid") & "'"
                    Dim ds1 As New DataSet
                    Dim ds2 As New DataSet
                    Dim ds3 As New DataSet

                    ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, busString)
                    ds2 = SqlHelper.ExecuteDataset(conn, CommandType.Text, userString)
                    ds3 = SqlHelper.ExecuteDataset(conn, CommandType.Text, userMString)

                    Dim row As DataRow
                    For Each row In ds1.Tables(0).Rows

                        Dim str1 As String = row("bsu_name")
                        str1 = str1
                        Dim str2 As String = row("bsu_id")
                        chkBusUnit.Items.Add(New ListItem(str1, str2))
                        Dim row2 As DataRow
                        For Each row2 In ds2.Tables(0).Rows
                            Dim str3 As String = row2("usa_bsu_id")
                            If str2 = str3 Then
                                chkBusUnit.Items.FindByText(str1).Selected = True
                            End If
                        Next
                        Dim row3 As DataRow
                        For Each row3 In ds3.Tables(0).Rows
                            Dim str3 As String = row3("usr_bsu_id")
                            If str2 = str3 Then
                                chkBusUnit.Items.FindByText(str1).Selected = True
                            End If
                        Next
                    Next
                Else
                    ' txtUserId.Attributes.Remove("readonly")
                    ' txtUserName.Attributes.Remove("readonly")
                    txtPassword.Attributes.Remove("readonly")
                    txtConfpassword.Attributes.Remove("readonly")
                    btnBusUnit.Visible = True
                    btnFeeCounter.Visible = True
                    btnEmp.Visible = True
                    btnRole.Visible = True
                    ' Page.Title = "Add User"
                    Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim busString As String
                    If ViewState("TYPE") = "V2" Then
                        busString = "SELECT BSU_ID,BSU_NAME FROM dbo.MENURIGHTS_S INNER JOIN dbo.BUSINESSUNIT_M ON BSU_ID=MNR_BSU_ID WHERE " & _
                                 " MNR_ROL_ID=" & Session("sroleid") & " AND MNR_MNU_ID='D050049' ORDER BY bsu_name"
                    Else
                        busString = "select bsu_id,bsu_name from businessunit_m order by bsu_name"
                    End If
                    Dim ds1 As New DataSet
                    ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, busString)
                    Dim row As DataRow
                    For Each row In ds1.Tables(0).Rows
                        Dim str1 As String = row("bsu_name")
                        'str1 = chkBusUnit
                        Dim str2 As String = row("bsu_id")
                        chkBusUnit.Items.Add(New ListItem(str1, str2))
                    Next
                End If
            End If
        End If
    End Sub

    Sub clearall()
        ' txtUserId.Text = ""
        txtUserName.Text = ""
        txtDisplayName.Text = ""
        txtRole.Text = ""
        txtDefBusUnit.Text = ""
        txtEmpId.Text = ""
        txtFeeCounter.Text = ""
        hfFEECounter.Value = ""
        txtPassword.Attributes.Add("value", "")
        txtConfpassword.Attributes.Add("value", "")
        ViewState("Eid") = ""
        For Each item As ListItem In chkBusUnit.Items
            item.Selected = False
        Next

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            'Encrypt password before inserting
            '= txtUserId.Text 'removed to make it auto generated 
            If ViewState("datamode") = "add" Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "CheckPasswordStrength", "CheckPasswordStrength('" + txtPassword.Text + "');", True)
                If hdn_chk.Value = "0" Then
                    Exit Sub
                End If


                If validate_pwd() > 0 Then
                    lblErrorNew.Text = "Passwords must not contain the user's consecutive 3 characters "

                    Exit Sub
                End If




                If pwd_HISTORY() = 0 Then
                    lblErrorNew.Text = "Password needs to be 8 characters or more and avoid last three passwords used "

                    Exit Sub
                End If
            End If

            If Not UtilityObj.PasswordVerify(txtConfpassword.Text, 0, 0, 0) Then
                lblError.Text = UtilityObj.getErrorMessage("781")
                Exit Sub
            End If
            Dim usr_name As String = txtUserName.Text
            Dim usr_DisplayName As String = txtDisplayName.Text
            Dim usr_emp_id As String = hfEmpID.Value
            Dim updateStatus As Integer


            Dim tblPassword As String = Encr_decrData.Encrypt(txtPassword.Text)
            If ViewState("datamode") = "add" Then
                Do While True
                    Dim guidResult As String = System.Guid.NewGuid().ToString()
                    'Remove the hyphens
                    guidResult = guidResult.Replace("-", String.Empty)
                    temp_ID = guidResult.Substring(0, 8)
                    Dim dupUserID As Integer = AccessRoleUser.checkduplicateUserID(temp_ID)
                    'no duplicate id exist
                    If dupUserID = 1 Then
                        Exit Do
                    End If
                Loop
                'Call the class to check duplicate usr_name or usr_id exits or not

                Dim transaction As SqlTransaction
                'insert the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        If updateStatus = 0 Then
                            updateStatus = AccessRoleUser.InsertUsers(temp_ID, usr_name, usr_DisplayName, tblPassword, _
                            CInt(hfRoleId.Value), hfEmpID.Value, hfFEECounter.Value, hfDefBusUnit.Value, Not chkActive.Checked, transaction)
                            If updateStatus = 0 Then
                                updateStatus = AccessRoleUser.DeleteAccess(temp_ID, transaction)
                                If updateStatus = 0 Then
                                    For Each item As ListItem In chkBusUnit.Items
                                        If (item.Selected) Then
                                            'loop through the each checkbox and insert it
                                            updateStatus = AccessRoleUser.InsertUserAccess(temp_ID, item.Value, transaction)
                                            If updateStatus <> 0 Then
                                                Exit For
                                            End If
                                        End If
                                    Next
                                End If
                            End If
                            If updateStatus = 0 Then
                                updateStatus = UtilityObj.operOnAudiTable(Master.MenuName, usr_name, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                            End If
                            If updateStatus = 0 Then
                                ViewState("datamode") = "none"
                                btnBusUnit.Visible = False
                                btnFeeCounter.Visible = False
                                btnEmp.Visible = False
                                btnRole.Visible = False
                                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                Call resetButtonstate()
                                transaction.Commit()
                                lblErrorNew.Text = UtilityObj.getErrorMessage(updateStatus)
                                Call clearall()
                            Else
                                lblErrorNew.Text = UtilityObj.getErrorMessage(updateStatus)
                                transaction.Rollback()
                            End If
                        End If
                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message, Page.Title)
                        lblErrorNew.Text = "Record can not be inserted"
                        transaction.Rollback()
                    End Try
                End Using
            ElseIf ViewState("datamode") = "edit" Then
                Dim transaction As SqlTransaction
                temp_ID = ViewState("Eid")
                'insert the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        'update the user with the current record
                        updateStatus = Update_Users(temp_ID, usr_name, tblPassword, usr_DisplayName, _
                        CInt(hfRoleId.Value), hfEmpID.Value, hfFEECounter.Value, hfDefBusUnit.Value, Not chkActive.Checked, transaction)

                        'delete the user id from userAccess_s
                        If updateStatus = 0 Then
                            updateStatus = AccessRoleUser.DeleteAccess(temp_ID, transaction)
                            If updateStatus = 0 Then
                                For Each item As ListItem In chkBusUnit.Items
                                    If (item.Selected) Then
                                        'loop through the each checkbox and insert it
                                        updateStatus = AccessRoleUser.InsertUserAccess(temp_ID, item.Value, transaction)
                                        If updateStatus <> 0 Then
                                            Exit For
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        If updateStatus = 0 Then
                            updateStatus = UtilityObj.operOnAudiTable(Master.MenuName, usr_name, "Edit", Page.User.Identity.Name.ToString, Me.Page)
                        End If
                        If updateStatus = 0 Then
                            ViewState("datamode") = "none"
                            btnBusUnit.Visible = False
                            btnFeeCounter.Visible = False
                            btnEmp.Visible = False
                            btnRole.Visible = False
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            Call resetButtonstate()
                            transaction.Commit()
                            lblError.Text = UtilityObj.getErrorMessage(updateStatus)
                            Call clearall()
                        Else
                            lblError.Text = UtilityObj.getErrorMessage(updateStatus)
                            transaction.Rollback()
                        End If
                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message, Page.Title)
                        lblErrorNew.Text = "Record can not be updated"
                        transaction.Rollback()
                    End Try
                End Using
            End If
        End If
    End Sub
    Public Shared Function Update_Users(ByVal usr_id As String, ByVal usr_name As String, _
   ByVal usr_password As String, ByVal usr_DisplayName As String, ByVal usr_rol_id As Integer, ByVal usr_emp_id As String, _
   ByVal vFCM_ID As String, ByVal usr_bsu_id As String, ByVal USR_bDisable As Boolean, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usr_id", usr_id)
            pParms(1) = New SqlClient.SqlParameter("@usr_name", usr_name)
            pParms(2) = New SqlClient.SqlParameter("@usr_password", usr_password)
            pParms(3) = New SqlClient.SqlParameter("@usr_rol_id", usr_rol_id)
            pParms(4) = New SqlClient.SqlParameter("@usr_emp_id", usr_emp_id)
            pParms(5) = New SqlClient.SqlParameter("@usr_bsu_id", usr_bsu_id)

            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            pParms(7) = New SqlClient.SqlParameter("@USR_bDisable", USR_bDisable)
            pParms(8) = New SqlClient.SqlParameter("@USR_DISPLAY_NAME", usr_DisplayName)

            Dim Val As Object = DBNull.Value
            If vFCM_ID <> "" AndAlso vFCM_ID <> "0" Then
                Val = CInt(vFCM_ID)
            End If
            pParms(9) = New SqlClient.SqlParameter("@usr_fcm_id", Val)
            'Insert User
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "proUpdateUsers_new", pParms)
            Dim ReturnFlag As Integer = pParms(6).Value
            Return ReturnFlag

            SqlConnection.ClearPool(connection)
        End Using
    End Function
    Public Function validate_pwd() As Integer
        Dim i As Integer
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@username", txtUserName.Text)
        pParms(1) = New SqlClient.SqlParameter("@pwd", txtPassword.Text)

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "get_password_strength", pParms)

            While DATAREADER.Read
                i = Convert.ToString(DATAREADER("STATUS_VALUE"))
            End While
        End Using
        Return i
    End Function
    Public Function pwd_HISTORY() As Integer
        Dim i As Integer
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", hfDefBusUnit.Value)
        pParms(1) = New SqlClient.SqlParameter("@USR_NAME", txtUserName.Text)
        pParms(2) = New SqlClient.SqlParameter("@PWD", txtPassword.Text)

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "SAVE_PASSWORD_LOG", pParms)

            While DATAREADER.Read
                i = Convert.ToString(DATAREADER("STATUS_VALUE"))
            End While
        End Using
        Return i
    End Function
    Sub resetButtonstate()
        '  txtUserId.Attributes.Add("Readonly", "Readonly")
        txtUserName.Attributes.Add("readonly", "readonly")
        txtPassword.Attributes.Add("Readonly", "Readonly")
        txtConfpassword.Attributes.Add("Readonly", "Readonly")
        clearall()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear the textbox and set the default settings
            Call clearall()
            ViewState("datamode") = "none"
            btnBusUnit.Visible = False
            btnFeeCounter.Visible = False
            btnEmp.Visible = False
            btnRole.Visible = False
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Call resetButtonstate()
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'make userid editable
        ' txtUserId.Attributes.Remove("readonly")
        txtUserName.Attributes.Remove("readonly")
        txtDisplayName.Attributes.Remove("readonly")
        txtPassword.Attributes.Remove("readonly")
        txtConfpassword.Attributes.Remove("readonly")       
        'find the master page 
        btnBusUnit.Visible = True
        btnFeeCounter.Visible = True

        btnEmp.Visible = True
        btnRole.Visible = True
        'change the datamode to add when user click on Add button
        ViewState("datamode") = "add"
        Call clearall()
        'set the rights on the button control based on the user
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'make userid uneditable
        UtilityObj.beforeLoopingControls(Me.Page)
        'txtUserId.Attributes.Add("readonly", "readonly")
        txtUserName.Attributes.Add("readonly", "readonly")
        txtDisplayName.Attributes.Remove("readonly")
        txtPassword.Attributes.Remove("readonly")
        txtConfpassword.Attributes.Remove("readonly")
        btnBusUnit.Visible = True
        btnFeeCounter.Visible = True
        btnEmp.Visible = True
        btnRole.Visible = True
        'find the master page 
        'change the datamode to edit when user click on edit button
        ViewState("datamode") = "edit"
        'set the rights on the button control based on the user
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Status = AccessRoleUser.DeleteUser(ViewState("Eid"), transaction)
                If Status = -1 Then
                    Throw New ArgumentException("Record do not exits")
                ElseIf Status <> 0 Then
                    Throw New ArgumentException("Record can not be Deleted")
                Else
                    Status = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("Eid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        Throw New ArgumentException("Could not complete your request")
                    End If
                    ViewState("datamode") = "none"
                    btnBusUnit.Visible = False
                    btnFeeCounter.Visible = False
                    btnEmp.Visible = False
                    btnRole.Visible = False
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    transaction.Commit()
                    Call clearall()
                    lblError.Text = "Record Deleted Successfully"
                End If
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Deleted"
            End Try
        End Using
    End Sub

End Class
