<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accNewCurrency.aspx.vb" Inherits="NewCurrency" Title="Currency" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        //not in use
        function validate_add() {

            if (document.getElementById("txtCurrencyCode").value == '') {
                alert("Kindly enter Currency Code");
                return false;
            }
            if (document.getElementById("txtCurrencyName").value == '') {
                alert("Kindly enter Currency Name");
                return false;
            }
            if (document.getElementById("txtSmallerDenom").value == '') {
                alert("Kindly enter Smaller Denom");
                return false;
            }
            if (document.getElementById("txtExchangeRate").value == '') {
                alert("Kindly enter Exchange Rate");
                return false;
            }
            return true;
        }
        function getDate() {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("calendar.aspx?dt=" + document.getElementById('<%=txtDate.ClientID %>').value, "", sFeatures)
            if (result == '' || result == undefined) {
                //            document.getElementById("txtDate").value=''; 
                return false;
            }
            //            var days;
            //            days=result.split('/');

            document.getElementById('<%=txtDate.ClientID %>').value = result;

            //           document.getElementById("h_SelectedGroup").value=NameandCode[1];
            //return result;
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Currency
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddCurrency" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">

                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Currency Code</span></td>

                                    <td align="left" width="30%" align="left">
                                        <asp:TextBox ID="txtCurrencyCode" runat="server"></asp:TextBox></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Currency Name</span></td>

                                    <td align="left" width="30%">
                                        <!--
								<input type="text" name="Section" size="3" tabindex="1" value="" maxlength="3" onChange="javascript:while(''+this.value.charAt(0)==' ')this.value=this.value.substring(1,this.value.length);">
								-->
                                        <asp:TextBox ID="txtCurrencyName" runat="server"></asp:TextBox></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Denomination</span></td>

                                    <td align="left" width="30%">
                                        <!--
								<input type="text" name="Section" size="3" tabindex="1" value="" maxlength="3" onChange="javascript:while(''+this.value.charAt(0)==' ')this.value=this.value.substring(1,this.value.length);">
								-->
                                        <asp:TextBox ID="txtSmallerDenom" runat="server"></asp:TextBox></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr style="display: none">
                                    <td width="20%" align="left"><span class="field-label">Date From</span></td>

                                    <td width="30%" align="left">
                                        <!--
								<input type="text" name="Section" size="3" tabindex="1" value="" maxlength="3" onChange="javascript:while(''+this.value.charAt(0)==' ')this.value=this.value.substring(1,this.value.length);">
								-->
                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                        <img src="../Images/calendar.gif" id="imgCalender" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalender" TargetControlID="txtDate">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>

                                    <td colspan="2"></td>
                                </tr>
                                <tr style="display: none">
                                    <td width="20%" align="left"><span class="field-label">Exchange Rate</span></td>

                                    <td width="30%" align="left">
                                        <!--
								<input type="text" name="Section" size="3" tabindex="1" value="" maxlength="3" onChange="javascript:while(''+this.value.charAt(0)==' ')this.value=this.value.substring(1,this.value.length);">
								-->
                                        <asp:TextBox ID="txtExchangeRate" runat="server">100</asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

