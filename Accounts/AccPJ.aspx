<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="AccPJ.aspx.vb" Inherits="AccPJ" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sds" Namespace="Telerik.Web.SessionDS" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <style>
            table td input[type=text], table td select {
                min-width: 20% !important;
            }
        </style>
        <script language="javascript" type="text/javascript">
            function expandcollapse(obj, row) {
                var div = document.getElementById(obj);
                var img = document.getElementById('img' + obj);

                if (div.style.display == "none") {
                    div.style.display = "block";
                    if (row == 'alt') {
                        img.src = "../images/Misc/minus.gif";
                    }
                    else {
                        img.src = "../images/Misc/minus.gif";
                    }
                    img.alt = "Close to view other Customers";
                }
                else {
                    div.style.display = "none";
                    if (row == 'alt') {
                        img.src = "../images/Misc/plus.gif";
                    }
                    else {
                        img.src = "../images/Misc/plus.gif";
                    }
                    img.alt = "Expand to show Orders";
                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

                var lstrVal;
                var lintScrVal;
                var NameandCode;
                var result;
                document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
                document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
                document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
                document.getElementById("<%=hd_ctrl2.ClientID %>").value = ctrl2;
                document.getElementById("<%=hd_ctrl3.ClientID%>").value = ctrl3;
                document.getElementById("<%=hd_ctrl4.ClientID%>").value = ctrl4;
                document.getElementById("<%=hd_ctrl5.ClientID%>").value = ctrl5;


                if (pMode == 'PARTY') {

                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];

                    //document.getElementById(ctrl2).value = lstrVal[2];
                    //document.getElementById(ctrl3).value = lstrVal[3];

                }


                else if (pMode == 'DEBIT_D' || pMode == 'PARTY1' || pMode == 'DEBIT' || pMode == 'PARTY2') {

                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");

                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];
                    //if (pMode == 'DEBIT' && (document.getElementById(ctrl2).value == '')) {
                    //    document.getElementById(ctrl2).value = lstrVal[0];
                    //    document.getElementById(ctrl3).value = lstrVal[1];

                    //    document.getElementById(ctrl4).value = lstrVal[2];
                    //    document.getElementById(ctrl5).value = lstrVal[3];
                    //}
                    //if (pMode == 'DEBIT_D') {
                    //    document.getElementById(ctrl2).value = lstrVal[2];
                    //    document.getElementById(ctrl3).value = lstrVal[3];
                    //}
                }
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();

                var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                var ctrl2 = document.getElementById("<%=hd_ctrl2.ClientID %>").value;
                var ctrl3 = document.getElementById("<%=hd_ctrl3.ClientID%>").value;
                var ctrl4 = document.getElementById("<%=hd_ctrl4.ClientID%>").value;
                var ctrl5 = document.getElementById("<%=hd_ctrl5.ClientID%>").value;


                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    if (pMode == 'PARTY') {

                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];

                        $("#<%=lblbtn.ClientID%>").click();
                        document.getElementById(ctrl2).value = NameandCode[2];
                        document.getElementById(ctrl3).value = NameandCode[3];

                        __doPostBack('<%=txtPartyCode.ClientID%>', "");
                    }
                    else if (pMode == 'DEBIT_D' || pMode == 'PARTY1' || pMode == 'DEBIT' || pMode == 'PARTY2') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                        if (pMode == 'DEBIT') {//&& (document.getElementById(ctrl2).value == '')
                            document.getElementById(ctrl2).value = NameandCode[0];
                            document.getElementById(ctrl3).value = NameandCode[1];

                            document.getElementById(ctrl4).value = NameandCode[2];
                            document.getElementById(ctrl5).value = NameandCode[3];
                        }
                        if (pMode == 'DEBIT_D') {
                            document.getElementById(ctrl2).value = NameandCode[2];
                            document.getElementById(ctrl3).value = NameandCode[3];

                            document.getElementById(ctrl4).value = NameandCode[0];
                            document.getElementById(ctrl5).value = NameandCode[1];
                        }
                    }
                }
            }

            function AddDetails(url) {

                var NameandCode;
                var result;
                var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';

                dates = document.getElementById('<%=txtdocDate.ClientID %>').value;
                dates = dates.replace(/[/]/g, '-')
                url_new = url_new + '&dt=' + dates;
                result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById('<%=txtPartyCode.ClientID %>').focus();
                return false;
            }

            function CopyDetails() {
                if (document.getElementById('<%=txtNarrn.ClientID %>').value != '') {
                    document.getElementById('<%=txtLineNarrn.ClientID %>').value = document.getElementById('<%=txtNarrn.ClientID %>').value;
                }
            }


            function getParty() {
                popUp('960', '600', 'PARTY', '<%=txtPartyCode.ClientId %>', '<%=txtPartyDescr.ClientId %>');
            }

            function getdebit() {
                popUp('960', '600', 'DEBIT', '<%=txtdrCode.ClientId %>', '<%=txtdrDescr.ClientId %>', '<%=Detail_ACT_ID.ClientId %>', '<%=txtdebitdescr.ClientId %>', '<%=hPly.ClientId %>', '<%=hCostreqd.ClientId %>');
                }

                function getdDebit() {
                    popUp('960', '600', 'DEBIT_D', '<%=Detail_ACT_ID.ClientId %>', '<%=txtdebitdescr.ClientId %>', '<%=hPly.ClientId %>', '<%=hCostreqd.ClientId %>', '<%=txtdrCode.ClientId %>', '<%=txtdrDescr.ClientId %>');
                }

                function CopyInvoiceNo() {
                    if (document.getElementById('<%=txtInvoiceNo.ClientID %>').value) {
                        document.getElementById('<%=txtdetInvoiceNo.ClientID %>').value = document.getElementById('<%=txtInvoiceNo.ClientID %>').value;
                    }
                }

                function CopyLPONo() {
                    if (document.getElementById('<%=txtLPO.ClientID %>').value) {
                        document.getElementById('<%=txtdetLPONo.ClientID %>').value = document.getElementById('<%=txtLPO.ClientID %>').value;
                    }
                }

        </script>

    </telerik:RadScriptBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Purchase Journals
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <asp:HiddenField ID="hd_ctrl2" runat="server" />
                <asp:HiddenField ID="hd_ctrl3" runat="server" />
                <asp:HiddenField ID="hd_ctrl4" runat="server" />
                <asp:HiddenField ID="hd_ctrl5" runat="server" />

                <table width="100%" align="center" border="0">
                    <tr runat="server">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label><asp:ValidationSummary
                                ID="dateval" runat="server" ValidationGroup="dateval" />
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center"
                    style="border-collapse: collapse">

                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Doc No [Old Ref No]</span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocNo" runat="server" Width="45%" TabIndex="2"></asp:TextBox>[
                <asp:TextBox ID="txtOldDocNo" runat="server" Width="35%" TabIndex="4"></asp:TextBox>
                            ]
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Doc Date<span style="color: red">*</span> </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocDate" runat="server" Width="80%" CausesValidation="true" ValidationGroup="dateval"
                                TabIndex="6" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RegularExpressionValidator ID="revTodate" runat="server" ControlToValidate="txtdocDate"
                                Display="Dynamic" EnableTheming="true" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="dateval">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Party Account <span style="color: red">*</span></span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtPartyCode" runat="server" Width="20%" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="imgParty" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getParty();return false;" TabIndex="16" />
                            <asp:TextBox ID="txtPartyDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trPayTerm" visible="false">
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblPaymentTerm" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Debit Account  <span style="color: red">*</span></span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtdrCode" runat="server" Width="20%" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="imgDebit" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getdebit();return false;" TabIndex="16" />
                            <asp:TextBox ID="txtdrDescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">LPO No </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLPO" runat="server" TabIndex="12" AutoCompleteType="Disabled" Width="80%"></asp:TextBox>
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Invoice No </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtInvoiceNo" runat="server" TabIndex="14" AutoCompleteType="Disabled" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Currency </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="true" TabIndex="16" Width="20%">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtExchRate" runat="server" Width="60%"></asp:TextBox>
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Group Rate </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLocalRate" runat="server" AutoCompleteType="Disabled" Width="80%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td width="18%" align="left">
                            <span class="field-label">Narration <span style="color: red">*</span></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNarrn" runat="server" TextMode="MultiLine" MaxLength="300" Width="80%"
                                TabIndex="18"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td colspan="4" align="left">Details 
                        </td>
                    </tr>
                    <asp:HiddenField ID="hPLY" runat="server" />
                    <asp:HiddenField ID="hCostreqd" runat="server" Value="1" />
                    <input id="h_editorview" runat="server" type="hidden" value="" />

                    <tr>
                        <td align="left">
                            <span class="field-label">Debit Account </span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="Detail_ACT_ID" runat="server" Width="20%" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="imgDDebit" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getdDebit();return false;" TabIndex="16" />
                            <asp:TextBox ID="txtdebitdescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%">
                            <span class="field-label">Narration </span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtLineNarrn" runat="server" Width="80%" TextMode="MultiLine"
                                MaxLength="300" TabIndex="22"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%">
                            <span class="field-label">Invoice No <span style="color: red">*</span></span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdetInvoiceNo" runat="server" Width="80%"
                                TabIndex="24" AutoCompleteType="Disabled"></asp:TextBox>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">Invoice Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdetInvoiceDate" runat="server" CausesValidation="true"
                                ValidationGroup="dateval" Width="80%" TabIndex="26" AutoPostBack="True"
                                OnTextChanged="txtdetInvoiceDate_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="28" /><asp:RegularExpressionValidator ID="invoiceDate" runat="server" ControlToValidate="txtdetInvoiceDate"
                                    Display="Dynamic" EnableTheming="true" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="detdateval">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="invDateField" runat="server" ControlToValidate="txtdetInvoiceDate"
                                ErrorMessage="InvoiceDate Required" ValidationGroup="detdateval">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%">
                            <span class="field-label">LPO No </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdetLPONo" runat="server" Width="80%"
                                TabIndex="30" AutoCompleteType="Disabled"></asp:TextBox>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">Credit Days </span>

                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCreditDays" runat="server" TabIndex="34" Width="80%"
                                AutoCompleteType="Disabled"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%">
                            <span class="field-label">Quantity </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtQty" runat="server" TabIndex="32" AutoCompleteType="Disabled" Width="20%"></asp:TextBox>
                        </td>
                        <td width="20%" align="left">
                            <span class="field-label">Rate <span style="color: red">*</span></span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtrate" runat="server" TabIndex="34" AutoCompleteType="Disabled" Width="20%"> </asp:TextBox><a
                                href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a>

                        </td>
                    </tr>
                    <tr runat="server" id="trTaxType" visible="false">
                        <td width="20%" align="left"><span class="field-label">TAX Type</span></td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddlVATCode" runat="server"></asp:DropDownList></td>
                        <td width="20%" align="left"><span class="field-label">TDS</span></td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddlTDS" runat="server"></asp:DropDownList></td>
                    </tr>

                    <tr runat="server" id="trTdsType" visible="false">
                        <td width="20%" align="left"><span class="field-label">TAX and TDS</span></td>
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddlTaxTds" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged"></asp:DropDownList></td>
                        <td width="20%" align="left"></td>
                        <td width="30%" align="left"></td>
                    </tr>

                    <tr runat="server" id="tr_UploadEmplyeeCostCenter" visible="false">
                        <td width="20%" align="left">
                            <span class="field-label">Upload Employees </span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:FileUpload ID="fuEmployeeData" runat="server" />
                            <asp:LinkButton ID="lbUploadEmployee" runat="server">Upload</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Allocation </span>
                        </td>
                        <td align="left" colspan="3">
                            <!-- content start -->
                            <!-- content end -->
                            <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnFill" runat="server" Text="Add" CssClass="button"
                                TabIndex="36" ValidationGroup="detdateval" />
                            <asp:Button ID="btnFillCancel" runat="server" Text="Cancel" OnClick="btnFillCancel_Click"
                                CssClass="button" TabIndex="38" UseSubmitBehavior="False" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                EmptyDataText="No transaction details added yet." Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide Orders for Customer <%# Eval("id") %>"
                                                    width="9px" border="0" src="../images/Misc/plus.gif" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="True">
                                        <ItemStyle Width="1%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Account Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="17%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LPO No">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("LPONo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Credit Days">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreditDays" runat="server" Text='<%# Bind("CreditDays") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice No">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("InvNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Inv. Date">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%#Bind("InvDate", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Item" HeaderText="Item">
                                        <ItemStyle Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Qty" DataFormatString="{0:0.00}" HeaderText="Qty" HtmlEncode="False"
                                        SortExpression="Qty">
                                        <ItemStyle HorizontalAlign="Right" Width="5%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Rate">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Rate")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" SkinID="Grid" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ply" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPLY" runat="server" Text='<%# Bind("Ply") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Costreqd" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostreqd" runat="server" Text='<%# Bind("Costreqd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TaxCode" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxCode" runat="server" Text='<%# Bind("TaxCode")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TaxAmount" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxAmount" runat="server" Text='<%# Bind("TaxAmount")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TdsCode" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTdsCode" runat="server" Text='<%# Bind("TdsCode")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TdsAmount" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTdsAmount" runat="server" Text='<%# Bind("TdsAmount")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit" OnClick="lbEdit_Click"> </asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="DeleteBtn" CommandArgument='<%# Eval("id") %>' CommandName="Delete"
                                                runat="server">
         Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <span class="field-label">Total Amount  </span>
                            <asp:TextBox ID="txtdetTotalAmt" runat="server" ReadOnly="true" Width="20%"></asp:TextBox>
                            <span class="field-label" id="lblNet" runat="server">Net Amount  </span>
                            <asp:TextBox ID="txtNetTotalAmt" runat="server" ReadOnly="true" Width="20%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="42" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit"
                                CausesValidation="False" TabIndex="44" />
                            <asp:Button ID="btnSave" runat="server"
                                CssClass="button" Text="Save" TabIndex="40" ValidationGroup="dateval" />
                            <asp:Button
                                ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete"
                                TabIndex="46" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                CssClass="button" Text="Cancel" TabIndex="50" />
                            <asp:Button ID="btnPrint" runat="server"
                                CssClass="button" Text="Print" TabIndex="48" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" TargetControlID="txtdocDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" BehaviorID="ctl00_CalendarExtender3"
                    CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2" TargetControlID="txtdetInvoiceDate">
                </ajaxToolkit:CalendarExtender>
                <asp:Button ID="lblbtn" runat="server" Style="display: none" />
            </div>
        </div>
    </div>
</asp:Content>
