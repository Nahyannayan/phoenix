<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="acccpViewCashPayments.aspx.vb" Inherits="Accounts_acccpViewCashPayments"
    Theme="General" %>

<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        function scroll_page() {
            document.location.hash = '<%=h_Grid.value %>';
        }

        window.onload = scroll_page;

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cash payments
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td valign="top" width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <a id='top'></a>
                        </td>
                        <td align="right">
                            <asp:RadioButton ID="rbUnposted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Open" Checked="True" />
                            <asp:RadioButton ID="rbPosted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Posted" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="All" />
                        </td>
                    </tr>
                </table>
                <table align="center" class="BlueTableView" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="middle" width="20%"><span class="field-label">Cash Payments </span>
                        </td>
                        <td algn="left" width="30%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr valign="top">
                        <td align="center" width="100%" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Cash Payment Vouchers found"
                                Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Doc No." SortExpression="VHH_DOCNO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Doc No.
                                <br />
                                            <asp:TextBox ID="txtDocno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ref No.">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("VHH_REFNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Ref No.
                                <br />
                                            <asp:TextBox ID="txtrefNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Narration">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("VHH_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Narration
                                <br />
                                            <asp:TextBox ID="txtNarration" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Date" SortExpression="VHH_DOCDT">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("VHH_DOCDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Date
                                <br />
                                            <asp:TextBox ID="txtDocdate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Account">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Account
                                <br />
                                            <asp:TextBox ID="txtcOLLECTION" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="VHH_CUR_ID" HeaderText="Currency" SortExpression="VHH_CUR_ID">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHH_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Amount
                                <br />
                                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Posted" SortExpression="VHH_bPOSTED">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Image ID="imgPosted" runat="server" ImageUrl='<%# returnpath(Container.DataItem("VHH_bPOSTED")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False" HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" runat="server" CausesValidation="false" CommandName="View"
                                                OnClick="lbView_Click" Text="Summary"></asp:LinkButton>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <a id='detail'></a>
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="VHD_DOCNO" HeaderText="Document No" SortExpression="VHD_DOCNO" />
                                    <asp:BoundField DataField="VHD_NARRATION" HeaderText="Narration" SortExpression="VHD_NARRATION" />
                                    <asp:BoundField DataField="ACCOUNT" HeaderText="Account" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHD_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GUID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="slno" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSlno" runat="server" Text='<%# Bind("VHD_LINEID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="View" SelectText="View" ShowSelectButton="True" />
                                </Columns>
                            </asp:GridView>
                            <br />
                            <a id='child'></a>
                            <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False" EmptyDataText="THERE IS NO ALLOCATION FOR CURRENTLY SELECTED ACCOUNT"
                                Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:BoundField DataField="ACT_ID" HeaderText="Account No" SortExpression="ACT_ID" />
                                    <asp:BoundField DataField="VDS_SLNO" HeaderText="Slno" SortExpression="VDS_SLNO"
                                        Visible="False" />
                                    <asp:BoundField DataField="CCS_DESCR" HeaderText="Cost Center" SortExpression="CCS_DESCR" />
                                    <asp:BoundField DataField="VDS_DESCR" HeaderText="Cost Object" />
                                    <asp:BoundField DataField="VDS_CODE" HeaderText="CODE" SortExpression="VDS_CODE"
                                        Visible="False" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VDS_AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GRPFIELD" HeaderText="Cost Center" SortExpression="GRPFIELD" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>



                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>
