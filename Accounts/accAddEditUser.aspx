<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accAddEditUser.aspx.vb" Inherits="AddEditUser" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ PreviousPageType VirtualPath="~/accounts/accmanageusers.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ChangeAllCheckBoxStates() {
            var lstrChk = document.getElementById('<%=chkAl.ClientID%>').checked;
            ChangeCheckBoxState(lstrChk);
        }
        function ChangeCheckBoxState(checkState) {
            var tableBody = document.getElementById('<%=chkBusUnit.ClientID %>');
            var options = tableBody.getElementsByTagName('input');           
            for (var i = 0; i < options.length; i++) {               
                var currentTd = options[i];                                
                currentTd.checked = checkState;
            }
        }
        function getRoleID(mode) {
            document.getElementById("<%=hf_mode.ClientID%>").value = mode;
            var NameandCode;
            var result;
            var url;
            url = 'accShowEmpDetail.aspx?id=' + mode;
            if (mode == 'R') {
                result = radopen("accShowRole.aspx", "pop_up");
                <%--  if (result=='' || result==undefined)
                return false;
            NameandCode = result.split('___');  
            document.getElementById("<%=txtRole.ClientID %>").value=NameandCode[0];
            document.getElementById("<%=hfRoleID.ClientID %>").value=NameandCode[1];--%>
            }
            else if (mode == 'E') {
                result = radopen(url, "pop_up");
                <%--if (result=='' || result==undefined)
                return false;        
            NameandCode = result.split('___');  
            document.getElementById("<%=txtEmpId.ClientID %>").value=NameandCode[0];
            document.getElementById("<%=hfEmpId.ClientID %>").value=NameandCode[1];--%>
            }
            else if (mode == 'B') {
                result = radopen(url, "pop_up");
                <%-- if (result=='' || result==undefined)
                return false;
            NameandCode = result.split('___');  
            document.getElementById("<%=txtDefBusUnit.ClientID %>").value=NameandCode[0];
            document.getElementById("<%=hfDefBusUnit.ClientID %>").value=NameandCode[1];--%>
            }
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            var mode = document.getElementById("<%=hf_mode.ClientID%>").value;
            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                if (mode == 'R') {
                    document.getElementById("<%=txtRole.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfRoleID.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'E') {
                    document.getElementById("<%=txtEmpId.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfEmpId.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'B') {
                    document.getElementById("<%=txtDefBusUnit.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfDefBusUnit.ClientID %>").value = NameandCode[1];
                }
    }
}


function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;
    var height = body.scrollHeight;
    var width = body.scrollWidth;
    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;
    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}




function GetCounter() {

    var NameandCode;
    var result;
    var type;
    var BSUID = document.getElementById("<%=hfDefBusUnit.ClientID %>").value;
    result = radopen("../Common/PopupForm.aspx?multiSelect=false&ID=COUNTER&BSUID=" + BSUID, "pop_up2")
            <%-- if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
           document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
        }
        return false;--%>
}

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');
            }
        }

        function CheckPasswordStrength(password) {



            //Regular Expressions.
            var regex = new Array();
            regex.push("[A-Z]"); //Uppercase Alphabet.
            regex.push("[a-z]"); //Lowercase Alphabet.
            regex.push("[0-9]"); //Digit.
            regex.push("[$@$!%*#?&]"); //Special Character.

            var passed = 0;

            //Validate for each Regular Expression.
            for (var i = 0; i < regex.length; i++) {
                if (new RegExp(regex[i]).test(password)) {
                    passed++;
                }
            }

            //Validate for length of Password.

            if (passed > 2 && password.length >= 8) {

                document.getElementById("<%=lblError.ClientID%>").innerHTML = "";
                document.getElementById("<%=hdn_chk.ClientID%>").value = "1";

            }

            else {

                document.getElementById("<%=lblError.ClientID%>").innerHTML = "Passwords must meet the following minimum requirements:<ul><li>Not contain the user's account name or parts of the user's full name that exceed two consecutive characters</li><li>Be at least eight characters in length</li></ul>Contain characters from three of the following four categories:<ul><li>English uppercase characters (A through Z)</li><li>English lowercase characters (a through z)</li><li>Base 10 digits (0 through 9)</li><li>Non-alphabetic characters (for example, !, $, #, %)</li></ul>";
                document.getElementById("<%=hdn_chk.ClientID%>").value = "0";

            }



        }

    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
           <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Add/Edit User
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                 
                <table align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblErrorNew" runat="server" CssClass="error" EnableViewState="False"  ></asp:Label>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"   Text="Passwords must meet the following minimum requirements:<ul><li>Not contain the user's account name or parts of the user's full name that exceed two consecutive characters</li><li>Be at least eight characters in length</li></ul>Contain characters from three of the following four categories:<ul><li>English uppercase characters (A through Z)</li><li>English lowercase characters (a through z)</li><li>Base 10 digits (0 through 9)</li><li>Non-alphabetic characters (for example, !, $, #, %)</li></ul>"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error"  EnableViewState="False" ValidationGroup="savegroup"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                                    <td align="left" colspan="2" style="height: 16px" valign="middle">Manage User
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">User Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtUserName" runat="server"  ValidationGroup="savegroup" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server"  ValidationGroup="savegroup"  ErrorMessage="Kindly enter user name" Display="Dynamic" ControlToValidate="txtUserName" SetFocusOnError="True" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Display Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDisplayName" runat="server"  MaxLength="50" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Password</span></td>
                                    <td align="left" width="30%">
                                           <asp:Label ID="TextBox1_HelpLabel" runat="server"  ValidationGroup="savegroup"></asp:Label>
                                        <asp:TextBox ID="txtPassword" runat="server"  MaxLength="100"
                                            TextMode="Password"  ValidationGroup="savegroup"></asp:TextBox>
                                        <br />  
                                        <asp:RequiredFieldValidator ID="rfvPassword"  ValidationGroup="savegroup"  runat="server" ErrorMessage="Password can not be left empty" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="True" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revPassword"  ValidationGroup="savegroup" runat="server" ErrorMessage="Password can contain alphabet, number, period(dot) & must be 6 character long" ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="True" ValidationExpression="[a-zA-Z0-9.~!@#$%^&*()_+;:<>,?/\|]{6,20}" CssClass="error" ForeColor="" Enabled="false">*</asp:RegularExpressionValidator>                                                                                                                                                       
                                        <ajaxToolkit:PasswordStrength ID="PasswordStrength1" runat="server"
                                HelpStatusLabelID="TextBox1_HelpLabel" PreferredPasswordLength="8"
                                PrefixText="&nbsp;Strength:" StrengthIndicatorType="Text" DisplayPosition="BelowLeft"
                                StrengthStyles="red;blue;grey;yellow;green" TargetControlID="txtpassword"
                                TextStrengthDescriptions="Very Poor&nbsp;;Weak&nbsp;;Average&nbsp;;Strong;Excellent">
                            </ajaxToolkit:PasswordStrength>
                                          </td>

                                    <td align="left" width="20%"><span class="field-label">Confirm Password</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtConfpassword" runat="server"  MaxLength="100"
                                            TextMode="Password"  ></asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1"  ValidationGroup="savegroup"
                                                runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfpassword"
                                                ErrorMessage="Password do not match" CssClass="error" Display="Dynamic" ForeColor="">*</asp:CompareValidator>
                                        <asp:RequiredFieldValidator  ValidationGroup="savegroup"
                                                    ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtConfpassword" 
                                                    ErrorMessage="Kindly enter confirm password" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>
                                
                                <tr>
                                    <td align="left"><span class="field-label">Role</span></td>
                                    <td align="left" class="matter">
                                        <asp:TextBox ID="txtRole" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="btnRole" runat="server" ImageUrl="../Images/forum_search.gif"  OnClientClick="getRoleID('R');return false;" CausesValidation="False"/>
                                        <asp:RequiredFieldValidator ID="rfvRole" runat="server"  ValidationGroup="savegroup" ErrorMessage="Select valid user role" ControlToValidate="txtRole" Display="Dynamic"  CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>

                                    <td align="left"><span class="field-label">Emp Name</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmpId" runat="server"  MaxLength="100"
                                            ></asp:TextBox>
                                        <asp:ImageButton ID="btnEmp" runat="server" ImageUrl="../Images/forum_search.gif" OnClientClick="getRoleID('E');return false;" CausesValidation="False" />
                                        <asp:RequiredFieldValidator ID="rfvEmpname" runat="server"  ValidationGroup="savegroup" ErrorMessage="Select valid employee name" ControlToValidate="txtEmpId" Display="Dynamic"  CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Default Business Unit</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDefBusUnit" runat="server"  Font-Bold="False"
                                            MaxLength="100"></asp:TextBox>
                                        <asp:ImageButton ID="btnBusUnit" runat="server" ImageUrl="../Images/forum_search.gif" CausesValidation="False" OnClientClick="getRoleID('B');return false;" />
                                        <asp:RequiredFieldValidator ID="rfvBusUnit" runat="server" ValidationGroup="savegroup" ErrorMessage="Select default Business unit" ControlToValidate="txtDefBusUnit" Display="Dynamic" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>

                                    <td align="left"><span class="field-label">Fee Counter</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFeeCounter" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="btnFeeCounter" runat="server" ImageUrl="../Images/forum_search.gif" CausesValidation="False" OnClientClick="GetCounter();return false;" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">User Status</span></td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkActive" runat="server" Text="Active" /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                               
                                    <td align="left"><span class="field-label">Business Unit </span></td>
                                    <td align="left" colspan="2">
                                       <%-- <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates();" type="checkbox"
                                            value="Check All" title="Select" />--%>
                                        <asp:CheckBox ID="chkAL" runat="server" onclick="ChangeAllCheckBoxStates();" Text="Select All" />
                                        
                                        <div class="checkbox-list-full-height">
                                          <asp:CheckBoxList ID="chkBusUnit" runat="server"                                                 
                                              RepeatColumns="1" RepeatDirection="Horizontal">
                                          </asp:CheckBoxList></div>
                                    </td>
                                    <td></td>
                                    <%--  <td align="left" colspan="2">
                                        <asp:Panel ID="plChkBUnit" runat="server" Height="200px" Width="100%" ScrollBars="Vertical" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px">
                                          
                                        </asp:Panel>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button"
                                            Text="Add" CausesValidation="False" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button"
                                            Text="Edit" CausesValidation="False" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button"
                                            Text="Save" ValidationGroup="savegroup" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button"
                                            Text="Delete" CausesValidation="False" OnClientClick="return confirm('Are you sure');" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfEmpID" runat="server" />
                <asp:HiddenField ID="hfRoleId" runat="server" />
                <asp:HiddenField ID="hfDefBusUnit" runat="server" />
                <asp:HiddenField ID="hfFEECounter" runat="server" />
                <asp:HiddenField ID="hdn_chk" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

