Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
'version                Date                    Author              Change
'1.1                    14-Feb-2011             Swapna              To add filtering queries
Partial Class Accounts_FiltervaluesFind
    Inherits BasePage

    Public ControlNa As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


        If IsPostBack = False Then
            Try
                hidFrom.Value = Request.QueryString("FROM")
                getList()
            Catch ex As Exception
                MsgLable.Text = ex.Message
            End Try
            
        End If

    End Sub
    Private Sub getList()

        Dim MainObj As Mainclass = New Mainclass()

        Try


            Dim Query As String = ""
            Dim likQuery1 As String = "" 'V1.1
            Dim likQuery2 As String = "" 'V1.1
            Dim likQuery3 As String = "" 'V1.1
            Dim likQuery4 As String = "" 'V1.1

            Dim ordQuery As String = ""  'V1.1
            Dim dbName As String = "MainDB"
            Dim FromReq As String = hidFrom.Value.ToString()
            Dim likString As String = txtFind.Text.ToString()
            Dim BsuUnit As String = ""

            If FromReq.Equals("BSUNIT") Then
                Query = " SELECT BSU_ID, BSU_NAME FROM vw_OSO_BUSINESSUNIT_M WHERE BSU_NAME LIKE '%" & likString & "%' ORDER BY BSU_NAME "
                labHead.Text = "Business Unit"
                
            ElseIf FromReq.Equals("EMPLOYEE") Then  'V1.1
                BsuUnit = Request.QueryString("BSUID")
                Dim DptId = Request.QueryString("DPTID")
                Query = " select EMPLOYEE_M.EMP_ID as EMP_ID, isnull(EMPLOYEE_M.EMP_FNAME,'')+' " _
                & " '+isnull(EMPLOYEE_M.EMP_MNAME,'')+'  '+isnull(EMPLOYEE_M.EMP_LNAME,'') as EMPNAME FROM EMPLOYEE_M  "  ' _  commented for V1.1
                'Splitting conditions to handle null conditions
                If BsuUnit <> "" Then
                    Query = Query + " WHERE EMP_BSU_ID = '" & BsuUnit & "'"
                End If
                If DptId <> "" Then
                    Query = Query + " AND EMP_DPT_ID  = " & DptId & "  AND EMP_FNAME LIKE '%" & likString & "%' "
                Else
                    MsgBox("Please select department", MsgBoxStyle.OkOnly, "Error")
                    Exit Sub

                End If
                Query = Query + " ORDER BY EMPNAME "
                labHead.Text = "Employee Details"
                dbName = "MainDBO"

            ElseIf FromReq.Equals("DOCTYPE") Then       'V1.1
                Query = "SELECT DOC_ID,DOC_NAME FROM DOCUMENT_M "
                If likString <> "" Then
                    Query = Query + " WHERE DOC_NAME LIKE '%" & likString & "%'"
                End If
                Query = Query + " GROUP BY DOC_ID,DOC_NAME ORDER BY DOC_NAME"
                labHead.Text = "Document Type"


            ElseIf FromReq.Equals("BSUEMP") Then   'V1.1
                Dim BsuId As String = Request.QueryString("BSUID")

                Query = " SELECT USR_NAME,USR_NAME FROM vw_OSO_USERS_M " _
                        & " WHERE USR_BSU_ID = '" & BsuId & "' " _
                        & " AND USR_NAME LIKE '%" & likString & "%' GROUP BY  USR_NAME "


                labHead.Text = "User Name"
                ElseIf FromReq.Equals("PETTYCASH") Then
                    Query = " SELECT  PETTYEXP_H.PCH_APV_AMOUNT AS PCH_APV_AMOUNT ," _
                    & " vw_OSO_EMPLOYEE_M.EMP_FNAME AS Employee  FROM  PETTYEXP_H INNER JOIN vw_OSO_EMPLOYEE_M " _
                    & " ON PETTYEXP_H.PCH_EMP_ID = vw_OSO_EMPLOYEE_M.EMP_ID " _
                    & " AND  PETTYEXP_H.PCH_BSU_ID = vw_OSO_EMPLOYEE_M.EMP_BSU_ID " _
                    & " WHERE PCH_Fapproved = 1 AND PCH_BDELETED = 0 AND ISNULL(PCH_VHH_DOCNO,'') = '' ORDER BY EMP_FNAME "
                    'PETTYEXP_H.PCH_ID AS PCH_ID,

            ElseIf FromReq.Equals("DPT") Then   'V1.1

                Query = " SELECT DPT_ID, DPT_DESCR FROM  OASIS.dbo.DEPARTMENT_M "
                If likString <> "" Then
                    Query = Query + " WHERE DPT_DESCR LIKE '%" & likString & "%'"
                End If
                Query = Query + " ORDER BY DPT_DESCR "
                labHead.Text = "Department"

            End If
                Session("QUERY") = Query

                Dim table As DataTable = MainObj.getRecords(Query, dbName)

                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                gvJournal.DataSource = table
                gvJournal.DataBind()

                If gvJournal.Rows.Count = 0 Then
                    Exit Sub
                End If

                Dim TxtName1 As New TextBox
                Dim TxtCode1 As New TextBox

            TxtCode1.Width = Unit.Pixel(100)

                TxtName1.Width = Unit.Parse("90%")
                TxtName1.CssClass = "inputbox"
                TxtName1.TabIndex = 1

                gvJournal.HeaderRow.Cells(0).Controls.Add(TxtCode1)
            gvJournal.HeaderRow.Cells(1).Controls.Add(TxtName1)

            gvJournal.HeaderRow.Cells(0).Style("display") = "none"
            gvJournal.HeaderRow.Cells(1).Style("display") = "none"

                'gvJournal.HeaderRow.Cells(1).Attributes.Add("onclick", "sortList( '" & gvJournal.Rows.Count() & "','" & txtColom.ClientID & "','" & txtHidden.ClientID & "')")

                TxtName1.Attributes.Add("onkeypress", "postItem('" & TxtCode1.ClientID & "','" & TxtName1.ClientID & "')")
                TxtCode1.Attributes.Add("onkeypress", "postItem('" & TxtCode1.ClientID & "','" & TxtName1.ClientID & "')")

                TxtName1.Attributes.Add("onkeyup", "return FilterName('" & TxtName1.ClientID & "','Name','" & gvJournal.Rows.Count() & "','" & txtColom.ClientID & "','" & txtHidden.ClientID & "')")
                TxtCode1.Attributes.Add("onkeyup", "return FilterName('" & TxtCode1.ClientID & "','Code','" & gvJournal.Rows.Count() & "','" & txtColom.ClientID & "','" & txtHidden.ClientID & "')")

                TxtCode1.Attributes.Add("onkeydown", "return selectName('" & gvJournal.Rows.Count() & "','" & TxtName1.ClientID & "','" & TxtCode1.ClientID & "','" & txtHidden.ClientID & "')")
                TxtName1.Attributes.Add("onkeydown", "return selectName('" & gvJournal.Rows.Count() & "','" & TxtName1.ClientID & "','" & TxtCode1.ClientID & "','" & txtHidden.ClientID & "')")




                txtColom.Text = table.Columns.Count.ToString()
                txtRow.Text = table.Rows.Count.ToString()
                Me.Page.Title = labHead.Text
                TxtName1.Focus()
        Catch ex As Exception

            MsgLable.Text = ex.Message
        End Try
    End Sub
    Public x As Integer
    Public y As Integer

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound


       
        If Not e.Row.RowIndex = -1 Then
           
            Dim ht As New HtmlInputHidden()
            ht.ID = ("Cell" & e.Row.RowIndex) + x.ToString()
            ht.Value = e.Row.Cells(0).Text.ToString()
            e.Row.ID = "TR" + x.ToString()
            e.Row.Cells(0).ID = "CODE"
            e.Row.Cells(1).ID = "NAME"
            e.Row.Cells(0).Width = Unit.Parse("25%")
            'e.Row.Attributes.Add("onClick", "getValueIn('" & e.Row.Cells(0).Text & "','" & e.Row.Cells(1).Text.ToString().Replace("'", "") & "','" & e.Row.Cells(0).ClientID & "')")
            e.Row.Attributes.Add("onClick", "listen_window('" & e.Row.Cells(0).Text & "','" & e.Row.Cells(1).Text.ToString().Replace("'", "") & "','" & e.Row.Cells(0).ClientID & "')")


            e.Row.Attributes.Add("onmouseover", "return  Mouse_Move('" + e.Row.ClientID + "')")
            e.Row.Attributes.Add("onmouseout", "return  Mouse_Out('" + e.Row.ClientID + "')")
            e.Row.ToolTip = "Click for selection..!"
            x = x + 1
            txtHidden.Text = x.ToString()
            e.Row.Cells(0).Style("display") = "none"
        End If
    End Sub

    

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        getList()

    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        getList()
    End Sub
End Class
