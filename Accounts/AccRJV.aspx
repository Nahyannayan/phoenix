<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="AccRJV.aspx.vb" Inherits="AccRJV" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td input[type=text], table td select {
            min-width: 20% !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function CopyDetails() {
            document.getElementById('<%=txtLineNarrn.ClientID %>').value = document.getElementById('<%=txtNarrn.ClientID %>').value;
        }

        function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {


            var lstrVal;
            var lintScrVal;

             document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
                    document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
                    document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
            var NameandCode;
            var result;
            if (pMode == 'NORMAL') {
               
                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up");
                    //if (result=='' || result==undefined)
                    //{    return false;      } 
                    //lstrVal=result.split('||');     
                    //document.getElementById(ctrl).value=lstrVal[0];
                    //document.getElementById(ctrl1).value=lstrVal[1];
                    //document.getElementById(ctrl2).value=lstrVal[2];
                    //document.getElementById(ctrl3).value=lstrVal[3];

                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                    var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                    var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                    if (pMode == 'NORMAL') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1]
                    }

                }
            }

        function getAccount() {
            popUp('960', '600', 'NORMAL', '<%=txtdebitCode.ClientId %>', '<%=txtdebitdescr.ClientId %>');
        }

        function AddDetails(url) {

            var NameandCode;
            var result;
            var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
        dates = document.getElementById('<%=txtdocDate.ClientID %>').value;
        dates = dates.replace(/[/]/g, '-')
        url_new = url_new + '&dt=' + dates;
        result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
        if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById('<%=txtdebitCode.ClientID %>').focus();
        return false;
    }
    </script>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Recurring JV
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <table cellspacing="0" cellpadding="5" width="90%" align="center" border="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">


                    <tr>
                        <td width="20%" align="left"><span class="field-label">Doc No [Old Ref No]</span></td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocNo" runat="server"
                                Width="45%"></asp:TextBox>[
                          <asp:TextBox ID="txtOldDocNo" runat="server"
                              Width="33.5%"></asp:TextBox>
                            ]
                        </td>
                        <td width="20%" align="left"><span class="field-label">Doc Date</span></td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtdocDate" runat="server" Width="80%" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                    </tr>

                    <tr>
                        <td width="20%" align="left"><span class="field-label">No. of Installments</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtMonths" runat="server" Width="80%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Currency</span></td>

                        <td width="30%" align="left">
                            <asp:DropDownList ID="cmbCurrency" runat="server" Width="20%" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtExchRate" runat="server" Width="59%"></asp:TextBox></td>
                        <td width="20%" align="left"><span class="field-label">Group Rate</span></td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtLocalRate" runat="server" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left"><span class="field-label">Narration</span></td>

                        <td width="30%" align="left">
                            <asp:TextBox ID="txtNarrn" runat="server" TextMode="MultiLine" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table width="100%" align="center">
                    <tr class="title-bg">
                        <td colspan="4" align="left">Details
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Account Code</span></td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtdebitCode" runat="server" Width="20%" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="btnHAccount" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getAccount();return false;" TabIndex="12" />
                            <a href="#" onclick="popUp('960','600','NORMAL','<%=txtdebitCode.ClientId %>','<%=txtdebitdescr.ClientId %>')"></a>
                            <asp:TextBox ID="txtdebitdescr" runat="server" Width="60%"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtLineNarrn" runat="server"
                                TextMode="MultiLine"></asp:TextBox>
                            <a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a></td>

                        <td align="left" width="20%"><span class="field-label">Amount</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtLineAmount" runat="server" Width="52%"></asp:TextBox>
                            <asp:Button ID="btnFill" runat="server" Text="Add" CssClass="button" />
                            <asp:Button ID="btnFillCancel" runat="server" Text="Cancel" OnClick="btnFillCancel_Click" CssClass="button" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                EmptyDataText="No transaction details added yet." Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="True">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Account Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="20%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Narration" HeaderText="Narration">
                                        <ItemStyle Width="30%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Debit" SortExpression="Debit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDebit" SkinID="Grid" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Debit")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Credit" SortExpression="Credit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCredit" SkinID="Grid" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Credit")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" SkinID="Grid" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GUID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ply" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPLY" runat="server" Text='<%# Bind("Ply") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Costreqd" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostreqd" runat="server" Text='<%# Bind("Costreqd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit" OnClick="LinkButton1_Click"> </asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="DeleteBtn"
                                                CommandArgument='<%# Eval("id") %>'
                                                CommandName="Delete" runat="server">
                                     Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cost Center">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="8%" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnAlloca" runat="server">Allocate</asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <span class="field-label">Debit Total</span>
                            <asp:TextBox ID="txtdebit" runat="server" Width="20%"></asp:TextBox>
                            <span class="field-label">Credit Total </span>
                            <asp:TextBox ID="txtCredit" runat="server" Width="20%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="hCostreqd" runat="server" Value="1" />
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <asp:HiddenField ID="hPLY" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtdocDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

