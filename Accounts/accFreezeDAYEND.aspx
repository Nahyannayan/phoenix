<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accFreezeDAYEND.aspx.vb" Inherits="Accounts_accFreezePeriod" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrCheckList.ascx" TagName="usrCheckList" TagPrefix="uc1" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Do Day End
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Business Unit</span> </td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Current Freeze Date</span> </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server" Enabled="False"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">New Freeze Date</span> </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtNewFreezeDate" runat="server" AutoPostBack="True" OnTextChanged="txtNewFreezeDate_TextChanged"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewFreezeDate" ErrorMessage="From Date required"
                                                ValidationGroup="dayBook">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                    ID="revFromdate" runat="server" ControlToValidate="txtNewFreezeDate" Display="Dynamic"
                                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <uc1:usrCheckList ID="UsrCheckList1" runat="server" ProcessType="DAYEND" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="title-bg" colspan="4">Pending Approvals</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="grdViewPendingpost" runat="server" AutoGenerateColumns="False"
                                            CellPadding="3" Width="100%" CssClass="table table-bordered table-row">
                                            <RowStyle CssClass="griditem" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Transactions">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# bind("mnu_text") %>' SkinID="lblhome"></asp:Label><br />
                                                        <asp:Label ID="lblmnuCode" runat="server" Text='<%# bind("mnu_code") %>' Visible="False"></asp:Label>

                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="90%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hplData" runat="server">HyperLink</asp:HyperLink>

                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader" Font-Size="9pt" Height="22px" HorizontalAlign="Left" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtNewFreezeDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" TargetControlID="txtNewFreezeDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

