<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowAccount.aspx.vb" Inherits="ShowAccount" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Accounts</title>
    <base target="_self" />

       
    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body class="matter" onload="listen_window();">
    <form id="form1" runat="server">

        <table width="100%">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="ACT_ID" EmptyDataText="No Data" AllowPaging="True" PageSize="15" CssClass="table table-row table-bordered">
                        <Columns>
                            <asp:TemplateField HeaderText="Account Code" SortExpression="ACT_ID">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Account Code<br />
                                    <asp:TextBox ID="txtCode" runat="server"  ></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Name" SortExpression="ACT_NAME">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Account Name<br />
                                    <asp:TextBox ID="txtName" runat="server"  ></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("ACT_NAME") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Type" SortExpression="ACT_TYPE">
                                <HeaderTemplate>
                                    Acc Type<br />
                                    <asp:DropDownList ID="DDAccountType" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="DDAccountType_SelectedIndexChanged" >
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem Value="ASSET">ASSET</asp:ListItem>
                                        <asp:ListItem Value="EXPENSES">EXPENSES</asp:ListItem>
                                        <asp:ListItem Value="INCOME">INCOME</asp:ListItem>
                                        <asp:ListItem>LIABILITY</asp:ListItem>
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ACT_TYPE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Control Account" SortExpression="ACT_CTRLACC">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CTRL_NAME") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    Control Account<br />
                                    <asp:TextBox ID="txtControl" runat="server"  ></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchControl" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("CTRL_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bank Or Cash" SortExpression="ACT_BANKCASH">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ACT_BANKCASH") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Bank/Cash<br />
                                    <asp:DropDownList ID="DDBankorCash" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="DDBankorCash_SelectedIndexChanged" >
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem Value="B">Bank</asp:ListItem>
                                        <asp:ListItem Value="C">Cash</asp:ListItem>
                                        <asp:ListItem Value="N">Normal</asp:ListItem>
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="ACT_FLAG">
                                <HeaderTemplate>
                                    Type<br />
                                    <asp:DropDownList ID="DDCutomerSupplier" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="DDCutomerSupplier_SelectedIndexChanged" >
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem Value="C">Customer</asp:ListItem>
                                        <asp:ListItem Value="S">Supplier</asp:ListItem>
                                        <asp:ListItem Value="N">Normal</asp:ListItem>
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("ACT_FLAG") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Ply" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblPLY" runat="server" Text='<%# Bind("Ply") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CostReqd" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCostReqd" runat="server" Text='<%# Bind("CostReqd") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ACT_COL_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblACT_COL_ID" runat="server" Text='<%# Bind("ACT_COL_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
    </form>
</body>
</html>
