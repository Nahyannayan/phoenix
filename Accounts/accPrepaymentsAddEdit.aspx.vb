Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class empPrepayments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim BSU_IsTAXEnabled As Boolean = False

    Public Property InitialLoad() As Boolean
        Get
            Return ViewState("InitialLoad")
        End Get
        Set(ByVal value As Boolean)
            ViewState("InitialLoad") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        Dim MainMnu_code As String = String.Empty
        Dim viewid As String = String.Empty
        If Page.IsPostBack = False Then

            'TAX CODE
            Dim ds7 As New DataSet
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms0(1).Value = Session("sBSUID")
            ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
            BSU_IsTAXEnabled = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsTAXEnabled"))
            ViewState("BSU_IsTAXEnabled") = BSU_IsTAXEnabled
            If BSU_IsTAXEnabled = True Then
                trTaxType.Visible = True
                LOAD_TAX_CODES()
            End If
            'TAX CODE

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Session("CHECKLAST") = 0
            Try
                'txtNarration1.Attributes.Add("onBlur", "narration_check('" & txtNarration1.ClientID & "');")
                'txtNarration2.Attributes.Add("onBlur", "narration_check('" & txtNarration2.ClientID & "');")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'set the following textbox in read only
                txtHDocno.Attributes.Add("Readonly", "Readonly")
                txtCostUnit.Attributes.Add("Readonly", "Readonly")
                txtRefdocno.Attributes.Add("Readonly", "Readonly")
                DETAIL_ACT_ID.Attributes.Add("Readonly", "Readonly")
                txtPreCode.Attributes.Add("Readonly", "Readonly")
                txtPartCode.Attributes.Add("Readonly", "Readonly")
                txtPartyAcc.Attributes.Add("Readonly", "Readonly")
                txtExpAcc.Attributes.Add("Readonly", "Readonly")
                txtPreAcc.Attributes.Add("Readonly", "Readonly")
                'get the datamode from querystring
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the MainMnu_code
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'create the data table during the page load for storing the costcenter
                'Session("dtCostChild") = CreateDataTableCostcenter()
                'make the id for cost center to 0
                Session("idCostChild") = 0
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                'check for vaild user
                If USR_NAME = "" Or MainMnu_code <> "A150300" And MainMnu_code <> "A150030" And MainMnu_code <> "A200030" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'if user is valid check if in view or in add mode
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_SubId.Value = Session("Sub_ID")
                    h_fyear.Value = Session("F_YEAR")
                    h_BSU_ID.Value = Session("sBsuid")
                    If ViewState("datamode") = "view" Then
                        viewid = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        InitialLoad = True
                        displayDetails(viewid)
                        'hfGuid.Value = viewid
                        ''ddDoctype.Enabled = False
                        ''btnDocument.Enabled = False
                        ''lnkClear.Enabled = False

                        'Using UserreaderPrepayment As SqlDataReader = VoucherFunctions.GetPrepaymentDetail_Guid(viewid)
                        '    While UserreaderPrepayment.Read
                        '        'handle the null value returned from the reader incase  convert.tostring
                        '        txtHDocno.Text = Convert.ToString(UserreaderPrepayment("DocNo"))
                        '        txtPartCode.Text = Convert.ToString(UserreaderPrepayment("PRP_PARTY"))
                        '        txtPartyAcc.Text = Convert.ToString(UserreaderPrepayment("PARTY_NAME"))
                        '        hfId1.Value = Convert.ToString(UserreaderPrepayment("CostID"))
                        '        txtCostUnit.Text = Convert.ToString(UserreaderPrepayment("CostDescr"))
                        '        txtPreCode.Text = Convert.ToString(UserreaderPrepayment("PreAcc"))
                        '        txtPreAcc.Text = Convert.ToString(UserreaderPrepayment("PreAccDescr"))
                        '        DETAIL_ACT_ID.Text = Convert.ToString(UserreaderPrepayment("ExpAcc"))
                        '        txtExpAcc.Text = Convert.ToString(UserreaderPrepayment("ExpDescr"))
                        '        txtRefdocno.Text = Convert.ToString(UserreaderPrepayment("PRP_REFDOCNO"))
                        '        Dim str_doctype As String = Convert.ToString(UserreaderPrepayment("PRP_REFTYPE"))
                        '        If str_doctype <> "" Then
                        '            For i As Integer = 0 To ddDoctype.Items.Count - 1
                        '                If ddDoctype.Items(i).Value = str_doctype Then
                        '                    ddDoctype.SelectedIndex = i
                        '                End If
                        '            Next
                        '        End If

                        '        'Dim d As Date
                        '        'd = CDate(Convert.ToString(UserreaderPrepayment(8)))
                        '        txtdocdate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("DOCDT")))
                        '        txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("FTFrom")))
                        '        txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("FTto")))
                        '        txtAmount.Text = Convert.ToDouble(UserreaderPrepayment("TAmt"))
                        '        txtInvoiceNo.Text = Convert.ToString(UserreaderPrepayment("InVoice"))
                        '        ViewState("PRP_JVNO") = Convert.ToString(UserreaderPrepayment("JvNo"))
                        '        txtNarration1.Text = Convert.ToString(UserreaderPrepayment("NR1"))
                        '        txtNarration2.Text = Convert.ToString(UserreaderPrepayment("NR2"))
                        '        txtLpo.Text = Convert.ToString(UserreaderPrepayment("LPO"))
                        '        h_SubId.Value = Convert.ToString(UserreaderPrepayment("SubId"))
                        '        h_fyear.Value = Convert.ToInt64(UserreaderPrepayment("fyear"))
                        '        h_BSU_ID.Value = Convert.ToString(UserreaderPrepayment("BSU_ID"))
                        '    End While
                        '    ' clear of the resource end using
                        'End Using
                        'Call gridbindmonthwise(viewid)
                        'Dim v_docno As String = txtHDocno.Text
                        'Dim v_sub_id As String = h_SubId.Value
                        'Dim V_doctype As String = "PP"
                        'Dim V_Fyear As String = h_fyear.Value
                        'setModifyCost(v_docno, v_sub_id, V_doctype, V_Fyear)
                        'bind_Currency()
                        'set_mandatory_hidden()
                    Else
                        'in the add mode
                        txtdocdate.Text = UtilityObj.GetDiplayDate()
                        Call getnextdocid()
                        bind_Currency()
                        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
                        Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
                    End If
                End If
                UtilityObj.beforeLoopingControls(Me.Page)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        Else
            BSU_IsTAXEnabled = ViewState("BSU_IsTAXEnabled")
        End If
    End Sub
    Sub LOAD_TAX_CODES()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "dbo.[GET_TAX_CODES]", pParms)

        ddlVATCode.DataSource = ds
        ddlVATCode.DataTextField = "TAXDESC"
        ddlVATCode.DataValueField = "TAXCODE"
        ddlVATCode.DataBind()

    End Sub
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If InitialLoad And ViewState("datamode") = "view" Then
            Dim v_docno As String = txtHDocno.Text
            Dim v_sub_id As String = h_SubId.Value
            Dim V_doctype As String = "PP"
            Dim V_Fyear As String = h_fyear.Value
            setModifyCost(h_BSU_ID.Value, v_docno, v_sub_id, V_doctype, V_Fyear)
            InitialLoad = False
        End If

    End Sub


    Private Sub gridbindmonthwise(ByVal vid As String)
        Try
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""
                Dim lblGrpCode As New Label
                str_Sql = "select PRP_ID,PRP_BSU_ID FROM PREPAYMENTS_H where GUID='" & vid & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT * FROM PREPAYMENTS_D WHERE PRD_PRP_ID='" & ds.Tables(0).Rows(0)("PRP_ID") & "'" _
                   & "  AND PRD_BSU_ID='" & ds.Tables(0).Rows(0)("PRP_BSU_ID") & "' and PRD_bDELETED='false'"

                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    dsc.Tables(0).Columns.Add("Id", System.Type.GetType("System.Int16"))
                    Session("dtDTL") = dsc.Tables(0)
                    gvDetails.DataSource = dsc
                    Dim helper As New GridViewHelper(Me.gvDetails)
                    helper.RegisterSummary("PRD_ALLOCAMT", SummaryOperation.Sum)
                    AddHandler helper.GeneralSummary, AddressOf helper_ManualSummary
                    gvDetails.DataBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub helper_ManualSummary(ByVal row As GridViewRow)
        Try
            row.Cells(0).HorizontalAlign = HorizontalAlign.Right
            row.Cells(0).Text = "Total Amount:-"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub getnextdocid()
        If ViewState("datamode") <> "edit" Then
            Try
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim ds As New DataSet

                'Dim str_Sql As String = "select dbo.fN_ReturnNextNo('PP','" & Session("sBsuid") & "','" & CType(txtdocdate.Text, Date).Month & "','" & CType(txtdocdate.Text, Date).Year & "')"
                'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                Dim str_NEWID As String = AccountFunctions.GetNextDocId("PP", Session("sBsuid"), CType(txtdocdate.Text, Date).Month, CType(txtdocdate.Text, Date).Year)
                If str_NEWID <> "" Then
                    txtHDocno.Text = str_NEWID & ""
                Else
                    lblError.Text = "CANNOT SAVE DATA. VOUCHER SERIES NOT SET "
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "AccPrepaymentEdit")
                'error message to be handled
            End Try
        End If
    End Sub

    Private Sub bind_Currency()
        Try
            Dim dt As DataTable
            dt = MasterFunctions.GetExchangeRates(txtdocdate.Text, Session("sBSUID"), Session("Bsu_Currency"))
            DDCurrency.DataSource = dt
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If dt.Rows.Count > 0 Then
                If set_default_currency() <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHGroupRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
                btnSave.Enabled = True
            Else
                txtHExchRate.Text = "0"
                txtHGroupRate.Text = "0"
                btnSave.Enabled = False
                lblError.Text = "Cannot Save Data. Currency/Exchange Rate Not Set"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In DDCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHGroupRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        bind_Currency()
        getnextdocid()
    End Sub

    Private Sub setModifyCost(ByVal BSU_ID As String, ByVal v_docno As String, ByVal v_sub_id As String, ByVal V_doctype As String, ByVal V_Fyear As Integer)
        Try
            Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
            Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
            RecreateSsssionDataSource()
            Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(v_sub_id, BSU_ID, "PP", v_docno)
            Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(v_sub_id, BSU_ID, "PP", v_docno)
            ClearRadGridandCombo()
            Dim mrow As DataRow
            Dim lstVoucherId As String = ""
            For Each mrow In Session("dtCostChild").select("1=1", "VoucherId")
                If lstVoucherId <> mrow("VoucherId").ToString Then
                    lstVoucherId = mrow("VoucherId")
                    CostCenterFunctions.SetGridSessionDataForEdit(mrow("VoucherId"), Session("dtCostChild"), _
                                                  Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                End If
            Next
            usrCostCenter1.BindCostCenter()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Shared Sub SetGridSessionDataForEdit(ByVal Voucherid As Integer, ByVal dtCostChild As DataTable, ByVal dtCostAllocation As DataTable, ByVal dtCostGrid As DataTable, ByVal dtCostAllocGrid As DataTable)
        dtCostChild.DefaultView.RowFilter = "VoucherId ='" & Voucherid & "'"
        For Each mRow As DataRow In dtCostChild.DefaultView.ToTable.Rows
            Dim IdMax As Int32 = CostCenterFunctions.GetMaxIDFromDataTable(dtCostGrid, "ID") + 1
            dtCostAllocation.DefaultView.RowFilter = "CostCenterID = '" & mRow("Id") & "'"
            For Each mRowAlloc As DataRow In dtCostAllocation.DefaultView.ToTable.Rows
                Dim rDtAlloc As DataRow
                rDtAlloc = dtCostAllocGrid.NewRow
                Dim IdMaxAlloc As Int32 = CostCenterFunctions.GetMaxIDFromDataTable(dtCostAllocGrid, "ID") + 1
                rDtAlloc("Id") = IdMaxAlloc
                rDtAlloc("CostCenterID") = IdMax
                rDtAlloc("ASM_ID") = mRowAlloc("ASM_ID")
                rDtAlloc("ASM_NAME") = mRowAlloc("ASM_NAME")
                rDtAlloc("Amount") = mRowAlloc("Amount")
                dtCostAllocGrid.Rows.Add(rDtAlloc)
            Next
            Dim rDt As DataRow
            rDt = dtCostGrid.NewRow
            rDt("Id") = IdMax
            rDt("CostCenterTypeID") = mRow("CostCenter")
            rDt("CostCenterTypeName") = mRow("CostCenterTypeName")
            rDt("Memberid") = mRow("Memberid")
            rDt("Name") = mRow("Name")
            rDt("Amount") = mRow("Amount")
            rDt("ERN_ID") = ""
            dtCostGrid.Rows.Add(rDt)
        Next
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'work to be completed is  acct_bprep should be true to display Prepaid Account use where clause--done
        'Party Account should be selected based on act_flag='s' --done
        Try
            If txtPartyAcc.Text.Trim <> "" Then
                If txtInvoiceNo.Text = "" Or txtLpo.Text = "" Then
                    Throw New ArgumentException("Invoice No or LPO can not be left empty")
                End If
            End If
            If Not UpdateCostCenter() Then
                Throw New ArgumentException("Invalid cost center allocation!!!!")
            End If
            If Page.IsValid = True Then
                Try
                    Dim PRP_JVNO As String = String.Empty
                    Dim str_err As String = ""
                    Dim Status As Integer
                    Dim d1, d2 As Date
                    Dim bEdit As Boolean
                    Dim t As TimeSpan
                    Dim PRP_ID As String = txtHDocno.Text
                    Dim PRP_sub_ID As String = h_SubId.Value

                    Dim PRP_BSU_ID As String = h_BSU_ID.Value
                    Dim i As Integer
                    Dim PRP_FYEAR As String = h_fyear.Value
                    Dim PRP_PARTY As String = txtPartCode.Text

                    Dim PRP_PREPAYACC As String = txtPreCode.Text

                    Dim PRP_EXPENSEACC As String = DETAIL_ACT_ID.Text
                    Dim PRP_DOCDT As String = Convert.ToDateTime(txtdocdate.Text).ToString("dd/MMM/yyyy")

                    Dim PRP_FTFROM As String = Convert.ToDateTime(txtFromDate.Text).ToString("dd/MMM/yyyy")
                    Dim PRP_DTTO As String = Convert.ToDateTime(txtToDate.Text).ToString("dd/MMM/yyyy")
                    Dim PRP_AMOUNT As Double = CDbl(txtAmount.Text)
                    Dim PRP_INVOICENO As String = txtInvoiceNo.Text
                    Dim PRP_NOOFINST As Integer
                    Dim PRP_CUR_ID As String = DDCurrency.SelectedItem.Text
                    Dim PRP_EXGRATE1 As Double = CDbl(txtHExchRate.Text)
                    Dim PRP_EXGRATE2 As Double = CDbl(txtHGroupRate.Text)
                    Dim PRP_CUT_ID As String = hfId1.Value
                    Dim PRP_CRNARRATION As String = txtNarration1.Text
                    Dim PRP_DRNARRATION As String = txtNarration2.Text
                    Dim PRP_LPO As String = txtLpo.Text

                    'make True for generating new Noif adding new record
                    Dim bGenerateNewNo As Boolean = True
                    Dim PRP_NEWDOCNO As String = ""

                    If Session("dtCostChild") IsNot Nothing Then
                        If Not CType(Session("dtCostChild"), DataTable).Columns.Contains("CostAmountRT") Then
                            CType(Session("dtCostChild"), DataTable).Columns.Add("CostAmountRT", System.Type.GetType("System.Double"))
                        End If
                    End If

                    'for other table
                    Dim Guid As Guid
                    Dim PRD_ID, ID As Integer
                    Dim PRD_PRP_ID As String
                    Dim PRD_BSU_ID As String = PRP_BSU_ID
                    Dim PRD_DOCDT As String
                    Dim PRP_ALLOCAMT As String

                    Dim prp_taxcode As String = ""
                    If BSU_IsTAXEnabled Then
                        prp_taxcode = ddlVATCode.SelectedValue
                    End If




                    'for Audit table data
                    Dim aud_docno As String = txtHDocno.Text
                    'set the transaction block state to rollback incase of error
                    Dim transaction As SqlTransaction
                    'Business logic for  finding the date difference

                    Dim dtMonthend As Date

                    dtMonthend = CDate(GetFREEZEDT(Session("sBsuid")))
                    Dim decFirstMonthAmount As Decimal = 0

                    d1 = Convert.ToDateTime(txtFromDate.Text)
                    d2 = Convert.ToDateTime(txtToDate.Text)
                    t = d2 - d1
                    Dim totMonth As Integer = DateDiff("m", d1, d2) + 1
                    'modified code on 16/jan/2008 
                    'Amount calculated based on total days
                    'totdays from the start date to the end date of the entry date
                    Dim totdays As Integer = DateDiff("d", d1, d2) + 1

                    Dim firstdayLastMonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d2), Date.MinValue)
                    Dim LastDayofFirstMonth As Date = (DateAdd(DateInterval.Second, -3, DateAdd(DateInterval.Month, DateDiff("m", Date.MinValue, d1) + 1, Date.MinValue))).Date
                    Dim remfirstday As Integer = DateDiff("d", d1, LastDayofFirstMonth) + 1
                    Dim remLastday As Integer = DateDiff("d", firstdayLastMonth, d2) + 1
                    Dim totalAmt As Double
                    totalAmt = CDbl(txtAmount.Text)
                    Dim copyTotalAmt As Double = totalAmt
                    Dim s As String = check_Errors()
                    If s <> "" Then
                        Throw New ArgumentException(s)
                    End If
                    If ViewState("datamode") = "add" Then
                        bEdit = 0
                        ''''''''''''
                        'for month difference more than 2
                        If totMonth > 2 Then
                            '  Dim perMonthamt As Double = totalAmt \ totMonth
                            'new cde modified on 05/02/2008
                            'per day amount from the start date to the end  date
                            Dim perDayAmt As Double = totalAmt / totdays
                            perDayAmt = perDayAmt
                            '-----------
                            ' Dim perDayAmtxxx As Double = perMonthamt \ 30
                            'total amt 4 the first month
                            Dim firstMonthAmt As Double = remfirstday * perDayAmt
                            firstMonthAmt = Math.Round(firstMonthAmt, 0)
                            'last month amount on per day bases 4 remaining days
                            Dim lastMonthAmt As Double = remLastday * perDayAmt
                            lastMonthAmt = Math.Round(lastMonthAmt, 0)
                            'calcualting 4 the remaining month per day base
                            Dim total_remAmt4RemMonth As Double
                            PRP_NOOFINST = totMonth
                            ''''''''''''''''''
                            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
                                transaction = conn.BeginTransaction("SampleTransaction")
                                Try
                                    'call the class to insert the record into PREPAYMENTS_H
                                    Status = VoucherFunctions.SavePREPAYMENTS_H(PRP_ID, PRP_sub_ID, PRP_BSU_ID, PRP_FYEAR, PRP_PARTY, _
                                    PRP_PREPAYACC, PRP_EXPENSEACC, PRP_DOCDT, PRP_FTFROM, PRP_DTTO, PRP_AMOUNT, PRP_INVOICENO, _
                                    PRP_NOOFINST, PRP_CUR_ID, PRP_EXGRATE1, PRP_EXGRATE2, PRP_JVNO, PRP_CUT_ID, PRP_CRNARRATION, _
                                    PRP_DRNARRATION, PRP_LPO, bEdit, bGenerateNewNo, PRP_NEWDOCNO, txtRefdocno.Text, _
                                    ddDoctype.SelectedItem.Value, hfRefid.Value, prp_taxcode, transaction)
                                    'throw error if insert to the PREPAYMENTS_H is not Successfully
                                    If Status <> 0 Then
                                        Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                    Else
                                        str_err = DeleteVOUCHER_D_S_ALL(conn, transaction, PRP_ID)
                                        If str_err <> "" Then
                                            If str_err <> "0" Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                            End If
                                        End If
                                        PRD_PRP_ID = PRP_NEWDOCNO
                                        'code modified on 23-mar-2008
                                        PRD_DOCDT = LastDayofFirstMonth 'DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth))
                                        'insert first month record
                                        PRP_ALLOCAMT = firstMonthAmt

                                        'insert record for the remaining month

                                        If LastDayofFirstMonth <= dtMonthend Then
                                            decFirstMonthAmount = decFirstMonthAmount + firstMonthAmt
                                        Else
                                            '  PRP_ALLOCAMT = decFirstMonthAmount
                                            Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction, PRP_EXPENSEACC)
                                            If Status <> 0 Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                            End If
                                            str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, 1, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, False)
                                            If str_err <> "0" Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                            End If
                                        End If
                                        For i = 1 To (totMonth - 2)
                                            Dim nextmonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d1.AddMonths(i)), Date.MinValue)
                                            Dim lastNextMonth As Date
                                            lastNextMonth = DateAdd("d", -1, DateAdd("m", 1, nextmonth))
                                            Dim remAmt4RemMonth As Double = perDayAmt * Day(lastNextMonth) ' DateDiff("d", nextmonth, lastNextMonth) + 1   ' Dim totalDay_InMonth As Integer = 0
                                            remAmt4RemMonth = Math.Round(remAmt4RemMonth, 0)

                                            total_remAmt4RemMonth = total_remAmt4RemMonth + remAmt4RemMonth

                                            PRD_DOCDT = lastNextMonth    'nextmonth 
                                            If lastNextMonth <= dtMonthend Then
                                                decFirstMonthAmount = decFirstMonthAmount + remAmt4RemMonth
                                            Else
                                                PRP_ALLOCAMT = remAmt4RemMonth + decFirstMonthAmount
                                                decFirstMonthAmount = 0
                                                Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                                If Status <> 0 Then
                                                    Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                                End If
                                                str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, i + 1, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, False)
                                                If str_err <> "0" Then
                                                    Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                                End If
                                            End If
                                        Next
                                        'insert record for the last month
                                        PRD_DOCDT = DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth)) 'firstdayLastMonth
                                        'calculating the left out amount after allocating to all the month accept the last month
                                        Dim checkAmtFinal As Double = firstMonthAmt + lastMonthAmt + total_remAmt4RemMonth
                                        Dim AmtDiff As Double = copyTotalAmt - checkAmtFinal
                                        Dim addedToLastMonthAmt As Double = lastMonthAmt + AmtDiff
                                        If DateAdd("d", -1, firstdayLastMonth) = dtMonthend Then
                                            addedToLastMonthAmt = addedToLastMonthAmt + decFirstMonthAmount
                                        End If
                                        PRP_ALLOCAMT = addedToLastMonthAmt
                                        Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If
                                        str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, totMonth, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, True)
                                        If str_err <> "0" Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                        End If

                                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, PRD_PRP_ID, "Insert", Page.User.Identity.Name.ToString)

                                        If flagAudit <> 0 Then
                                            Throw New ArgumentException("Could not process your request")
                                        End If
                                    End If
                                    transaction.Commit()
                                    lblError.Text = "Record Inserted Successfully"
                                    Call clearMe()
                                    ViewState("datamode") = "none"
                                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                Catch myex As ArgumentException
                                    transaction.Rollback()
                                    lblError.Text = myex.Message
                                Catch ex As Exception
                                    transaction.Rollback()
                                    lblError.Text = "Record could not be Inserted"
                                End Try
                            End Using
                        Else
                            'Dim perMonthamt As Double = totalAmt \ totMonth
                            ' Dim perDayAmt As Double = perMonthamt \ 30
                            Dim perDayAmt As Double = totalAmt / totdays
                            perDayAmt = perDayAmt
                            Dim firstMonthAmt As Double = remfirstday * perDayAmt
                            firstMonthAmt = Math.Round(firstMonthAmt, 0)
                            totalAmt = totalAmt - firstMonthAmt
                            ' Dim AmtDiff As Double = copyTotalAmt - totalAmt
                            Dim addedToLastMonthAmt As Double = totalAmt
                            PRP_NOOFINST = totMonth
                            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
                                transaction = conn.BeginTransaction("SampleTransaction")
                                Try
                                    'call the class to insert the record into PREPAYMENTS_H
                                    Status = VoucherFunctions.SavePREPAYMENTS_H(PRP_ID, PRP_sub_ID, PRP_BSU_ID, PRP_FYEAR, _
                                    PRP_PARTY, PRP_PREPAYACC, PRP_EXPENSEACC, PRP_DOCDT, PRP_FTFROM, PRP_DTTO, PRP_AMOUNT, _
                                    PRP_INVOICENO, PRP_NOOFINST, PRP_CUR_ID, PRP_EXGRATE1, PRP_EXGRATE2, PRP_JVNO, PRP_CUT_ID, _
                                    PRP_CRNARRATION, PRP_DRNARRATION, PRP_LPO, bEdit, bGenerateNewNo, PRP_NEWDOCNO, txtRefdocno.Text, _
                                    ddDoctype.SelectedItem.Value, hfRefid.Value, prp_taxcode, transaction)
                                    'throw error if insert to the PREPAYMENTS_H is not Successfully
                                    If Status <> 0 Then
                                        Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                    Else
                                        str_err = DeleteVOUCHER_D_S_ALL(conn, transaction, PRP_ID)
                                        If str_err <> "" Then
                                            If str_err <> "0" Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                            End If
                                        End If
                                        PRD_PRP_ID = PRP_NEWDOCNO
                                        'code modified on 23-mar-2008
                                        PRD_DOCDT = LastDayofFirstMonth ' DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth)) '  d1
                                        'insert first month record
                                        PRP_ALLOCAMT = firstMonthAmt
                                        Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If
                                        str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, 1, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, False)
                                        If str_err <> "0" Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                        End If
                                        'insert record for the last month
                                        PRP_ALLOCAMT = addedToLastMonthAmt
                                        Dim nextmonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d1.AddMonths(i)), Date.MinValue)
                                        Dim lastNextMonth As Date
                                        lastNextMonth = DateAdd("d", -1, DateAdd("m", 1, nextmonth))

                                        PRD_DOCDT = lastNextMonth
                                        'PRD_DOCDT = nextmonth
                                        Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If
                                        str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, 2, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, True)
                                        If str_err <> "0" Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                        End If

                                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, PRD_PRP_ID, "Insert", Page.User.Identity.Name.ToString)
                                        If flagAudit <> 0 Then
                                            Throw New ArgumentException("Could not process your request")
                                        End If
                                    End If
                                    transaction.Commit()
                                    lblError.Text = "Record Inserted Successfully"
                                    Call clearMe()
                                    ViewState("datamode") = "none"
                                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                Catch myex As ArgumentException
                                    transaction.Rollback()
                                    lblError.Text = myex.Message
                                Catch ex As Exception
                                    transaction.Rollback()
                                    lblError.Text = "Record could not be Inserted"
                                End Try
                            End Using
                        End If
                    ElseIf ViewState("datamode") = "edit" Then
                        bEdit = 1
                        bGenerateNewNo = False
                        'for month difference more than 2
                        If totMonth > 2 Then
                            'Dim perMonthamt As Double = totalAmt \ totMonth
                            'Dim perDayAmt As Double = perMonthamt \ 30
                            'new code modified on 05/02/2008
                            Dim perDayAmt As Double = totalAmt / totdays
                            perDayAmt = perDayAmt
                            '-----------
                            ' Dim perDayAmtxxx As Double = perMonthamt \ 30

                            'total amt 4 the first month
                            Dim firstMonthAmt As Double = remfirstday * perDayAmt
                            firstMonthAmt = Math.Round(firstMonthAmt, 0)
                            'last month amount on per day bases 4 remaining days
                            Dim lastMonthAmt As Double = remLastday * perDayAmt

                            lastMonthAmt = Math.Round(lastMonthAmt, 0)

                            'calcualting 4 the remaining month per day base
                            Dim total_remAmt4RemMonth As Double
                            PRP_NOOFINST = totMonth
                            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
                                transaction = conn.BeginTransaction("SampleTransaction")
                                Try
                                    'call the class to insert the record into PREPAYMENTS_H
                                    Status = VoucherFunctions.SavePREPAYMENTS_H(PRP_ID, PRP_sub_ID, PRP_BSU_ID, PRP_FYEAR, _
                                    PRP_PARTY, PRP_PREPAYACC, PRP_EXPENSEACC, PRP_DOCDT, PRP_FTFROM, PRP_DTTO, PRP_AMOUNT, _
                                    PRP_INVOICENO, PRP_NOOFINST, PRP_CUR_ID, PRP_EXGRATE1, PRP_EXGRATE2, PRP_JVNO, PRP_CUT_ID, _
                                    PRP_CRNARRATION, PRP_DRNARRATION, PRP_LPO, bEdit, bGenerateNewNo, PRP_NEWDOCNO, txtRefdocno.Text, _
                                    ddDoctype.SelectedItem.Value, hfRefid.Value, prp_taxcode, transaction)
                                    If Status <> 0 Then
                                        Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                    Else
                                        str_err = DeleteVOUCHER_D_S_ALL(conn, transaction, PRP_ID)
                                        If str_err <> "" Then
                                            If str_err <> "0" Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                            End If
                                        End If
                                        'insert first month record
                                        PRD_PRP_ID = PRP_ID
                                        'code modified on 23-mar-2008
                                        PRD_DOCDT = LastDayofFirstMonth 'DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth)) 'd1
                                        PRP_ALLOCAMT = firstMonthAmt
                                        'before inserting the updated record set the previous record flag to true to mark for deletation
                                        Status = VoucherFunctions.DeletePREPAYMENTS_D(PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If
                                        'inserting the new record with flag set to false means not deleted
                                        '-------------------
                                        If LastDayofFirstMonth <= dtMonthend Then
                                            decFirstMonthAmount = decFirstMonthAmount + firstMonthAmt
                                        Else
                                            Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                            If Status <> 0 Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                            End If
                                            str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, 1, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, False)
                                            If str_err <> "0" Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                            End If
                                        End If
                                        '-------------------
                                        'insert record for the remaining month
                                        For i = 1 To (totMonth - 2)
                                            Dim nextmonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d1.AddMonths(i)), Date.MinValue)
                                            Dim lastNextMonth As Date
                                            lastNextMonth = DateAdd("d", -1, DateAdd("m", 1, nextmonth))

                                            Dim remAmt4RemMonth As Double = perDayAmt * Day(lastNextMonth)  '(DateDiff(DateInterval.Day, nextmonth, lastNextMonth) + 1)   ' Dim totalDay_InMonth As Integer = 0
                                            remAmt4RemMonth = Math.Round(remAmt4RemMonth, 0)

                                            total_remAmt4RemMonth = total_remAmt4RemMonth + remAmt4RemMonth
                                            PRD_DOCDT = lastNextMonth    'nextmonth

                                            'PRP_ALLOCAMT = remAmt4RemMonth
                                            'Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                            'If Status <> 0 Then
                                            '    Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                            'End If

                                            '--------------
                                            If lastNextMonth <= dtMonthend Then
                                                '-------
                                                decFirstMonthAmount = decFirstMonthAmount + remAmt4RemMonth
                                            Else
                                                PRP_ALLOCAMT = remAmt4RemMonth + decFirstMonthAmount
                                                decFirstMonthAmount = 0
                                                Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                                If Status <> 0 Then
                                                    Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                                End If
                                            End If
                                            str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, i + 1, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, False)
                                            If str_err <> "0" Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                            End If

                                        Next
                                        'insert record for the last month
                                        PRD_DOCDT = DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth)) 'firstdayLastMonth
                                        'calculating the left out amount after allocating to all the month accept the last month
                                        Dim checkAmtFinal As Double = firstMonthAmt + lastMonthAmt + total_remAmt4RemMonth
                                        Dim AmtDiff As Double = copyTotalAmt - checkAmtFinal
                                        Dim addedToLastMonthAmt As Double = lastMonthAmt + AmtDiff

                                        If DateAdd("d", -1, firstdayLastMonth) = dtMonthend Then
                                            addedToLastMonthAmt = addedToLastMonthAmt + decFirstMonthAmount
                                        End If
                                        PRP_ALLOCAMT = addedToLastMonthAmt

                                        Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If

                                        str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, totMonth, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, True)
                                        If str_err <> "0" Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                        End If

                                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, aud_docno, "Edit", Page.User.Identity.Name.ToString, Me.Page)
                                        If flagAudit <> 0 Then
                                            Throw New ArgumentException("Could not complete your request")
                                        End If
                                    End If
                                    transaction.Commit()
                                    lblError.Text = "Record Updated Successfully"
                                    Call clearMe()

                                    ViewState("datamode") = "none"
                                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                Catch myex As ArgumentException
                                    transaction.Rollback()
                                    lblError.Text = myex.Message
                                    UtilityObj.Errorlog(myex.Message, Page.Title)
                                Catch ex As Exception
                                    transaction.Rollback()
                                    lblError.Text = "Record could not be Updated"
                                    UtilityObj.Errorlog(ex.Message, Page.Title)
                                End Try
                            End Using
                        Else
                            ' Dim perMonthamt As Double = totalAmt \ totMonth
                            ' Dim perDayAmt As Double = perMonthamt \ 30
                            Dim perDayAmt As Double = totalAmt / totdays
                            perDayAmt = perDayAmt

                            Dim firstMonthAmt As Double = remfirstday * perDayAmt
                            firstMonthAmt = Math.Round(firstMonthAmt, 0)
                            totalAmt = totalAmt - firstMonthAmt
                            ' Dim AmtDiff As Double = copyTotalAmt - totalAmt
                            Dim addedToLastMonthAmt As Double = totalAmt
                            PRP_NOOFINST = totMonth
                            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection
                                transaction = conn.BeginTransaction("SampleTransaction")
                                Try
                                    'call the class to insert the record into PREPAYMENTS_H
                                    Status = VoucherFunctions.SavePREPAYMENTS_H(PRP_ID, PRP_sub_ID, PRP_BSU_ID, PRP_FYEAR, _
                                    PRP_PARTY, PRP_PREPAYACC, PRP_EXPENSEACC, PRP_DOCDT, PRP_FTFROM, PRP_DTTO, PRP_AMOUNT, _
                                    PRP_INVOICENO, PRP_NOOFINST, PRP_CUR_ID, PRP_EXGRATE1, PRP_EXGRATE2, PRP_JVNO, PRP_CUT_ID, _
                                    PRP_CRNARRATION, PRP_DRNARRATION, PRP_LPO, bEdit, bGenerateNewNo, PRP_NEWDOCNO, txtRefdocno.Text, _
                                    ddDoctype.SelectedItem.Value, hfRefid.Value, prp_taxcode, transaction)
                                    If Status <> 0 Then
                                        Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                    Else
                                        str_err = DeleteVOUCHER_D_S_ALL(conn, transaction, PRP_ID)
                                        If str_err <> "" Then
                                            If str_err <> "0" Then
                                                Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                            End If
                                        End If
                                        'insert first month record
                                        PRD_PRP_ID = PRP_ID
                                        'code modified on 23-mar-2008
                                        PRD_DOCDT = LastDayofFirstMonth 'DateAdd("d", -1, DateAdd("m", 1, firstdayLastMonth))

                                        PRP_ALLOCAMT = firstMonthAmt
                                        'before inserting the updated record set the previous record flag to true to mark for deletation
                                        Status = VoucherFunctions.DeletePREPAYMENTS_D(PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If

                                        'inserting the new record with flag set to false means not deleted
                                        Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If
                                        str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, 1, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, False)
                                        If str_err <> "0" Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                        End If

                                        'insert record for the remaining month
                                        PRP_ALLOCAMT = addedToLastMonthAmt
                                        Dim nextmonth As Date = DateAdd(DateInterval.Month, DateDiff(DateInterval.Month, Date.MinValue, d1.AddMonths(1)), Date.MinValue)
                                        Dim lastNextMonth As Date
                                        lastNextMonth = DateAdd("d", -1, DateAdd("m", 1, nextmonth))
                                        PRD_DOCDT = lastNextMonth    'nextmonth
                                        Status = VoucherFunctions.SavePREPAYMENTS_D(Guid, PRD_ID, PRD_PRP_ID, PRD_BSU_ID, PRP_FYEAR, PRD_DOCDT, PRP_ALLOCAMT, PRP_JVNO, bEdit, transaction)
                                        If Status <> 0 Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(Status))
                                        End If

                                        str_err = DoTransactions_Sub_Table(conn, transaction, PRD_PRP_ID, 2, "DR", PRD_ID, DETAIL_ACT_ID.Text, PRP_ALLOCAMT, PRD_DOCDT, True)
                                        If str_err <> "0" Then
                                            Throw New ArgumentException(UtilityObj.getErrorMessage(str_err))
                                        End If

                                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, aud_docno, "Edit", Page.User.Identity.Name.ToString, Me.Page)
                                        If flagAudit <> 0 Then
                                            Throw New ArgumentException("Could not complete your request")
                                        End If
                                    End If

                                    transaction.Commit()
                                    lblError.Text = "Record Updated Successfully"
                                    Call clearMe()
                                    ViewState("datamode") = "none"
                                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                Catch myex As ArgumentException
                                    transaction.Rollback()
                                    lblError.Text = myex.Message

                                    UtilityObj.Errorlog(myex.Message, Page.Title)
                                Catch ex As Exception
                                    transaction.Rollback()
                                    lblError.Text = "Record could not be Updated"
                                    UtilityObj.Errorlog(ex.Message, Page.Title)
                                End Try
                            End Using
                        End If
                    End If
                Catch myex As ArgumentException
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message)
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message)
                End Try
            End If
        Catch myex As Exception
            lblError.Text = myex.Message
        End Try
    End Sub

    Private Function check_Errors() As String
        Dim str_Error As String = ""
        Dim str_Err As String = ""
        Dim mandatory As Boolean
        Try
            'Adding transaction info
            Dim dTotal As Double = 0
            Dim str_manndatory_costcenter As String = ""
            'run your sql for the costcenter
            Using Userreader As SqlDataReader = AccessRoleUser.GetMandCostCenter(DETAIL_ACT_ID.Text)
                While Userreader.Read
                    mandatory = Convert.ToBoolean(Userreader("BMandatory"))
                    str_manndatory_costcenter = Convert.ToString(Userreader("PLY_COSTCENTER"))
                End While
            End Using
            If mandatory = True Then
                str_manndatory_costcenter = str_manndatory_costcenter
            Else
                str_manndatory_costcenter = ""
            End If
            str_Err = check_Errors_sub("0", txtAmount.Text, str_manndatory_costcenter)
            If str_Err <> "" Then
                str_Error = str_Error & "<br/>" & str_Err
            End If
            'Adding transaction info
            Return str_Error
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return ""
        End Try
        Return ""
    End Function

    Private Function DeleteVOUCHER_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim iReturnvalue As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        cmd.Dispose()
        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID")
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = "PP"
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        Return iReturnvalue
    End Function
    'adding to cost center table acc voucher_D_S and 

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String, ByVal docdate As DateTime, ByVal IsLastRec As Boolean) As String

        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True
        Dim CostAmtRunningTotal As Double
        CostAmtRunningTotal = 0
        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            'If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then
            If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                iLineid = -1
                str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), txtAmount.Text)
            End If
            iLineid = iLineid + 1
            If str_balanced = False Then
                'tr_errLNE.Visible = True
                lblError.Text = " Cost center allocation not balanced"
                iReturnvalue = 511
                Exit For
            End If
            str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
            Dim bEdit As Boolean
            If Session("datamode") = "add" Then
                bEdit = False
            Else
                bEdit = True
            End If
            Dim VDS_ID_NEW As String = String.Empty
            Dim CostAmount, SubLedAmount As Double
            If IsDBNull(Session("dtCostChild").Rows(iIndex)("CostAmountRT")) Then
                CostAmtRunningTotal = 0
            Else
                CostAmtRunningTotal = Val(Session("dtCostChild").Rows(iIndex)("CostAmountRT"))
            End If
            If IsLastRec Then 'to add the left out amount to last one
                Dim BalAmountToAdd As Double
                CostAmount = Session("dtCostChild").Rows(iIndex)("Amount") - CostAmtRunningTotal
            Else
                CostAmount = Session("dtCostChild").Rows(iIndex)("Amount")
                CostAmount = Math.Round(p_amount / txtAmount.Text * CostAmount, 2)
                CostAmtRunningTotal = CostAmtRunningTotal + CostAmount
                Session("dtCostChild").Rows(iIndex)("CostAmountRT") = CostAmtRunningTotal
            End If
            iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
            p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), "PP", _
            Session("dtCostChild").Rows(iIndex)("ERN_ID"), CostAmount, _
            Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
            Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
            docdate, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
            If iReturnvalue <> 0 Then Return iReturnvalue
            If Session("CostAllocation").Rows.Count > 0 Then
                Dim subLedgerTotal As Decimal = 0
                Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                    'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                    SubLedAmount = Math.Round(p_amount / txtAmount.Text * Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), 2)
                    iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                          Session("sBsuid"), Session("F_YEAR"), "PP", "", _
                                          SubLedAmount, VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                    If iReturnvalue <> 0 Then
                        Session("CostAllocation").DefaultView.RowFilter = ""
                        Return iReturnvalue
                    End If
                    'End If
                    subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                Next
                If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                Session("CostAllocation").DefaultView.RowFilter = ""
            End If
            If iReturnvalue <> 0 Then Exit For
            ' End If
        Next
        Return iReturnvalue
    End Function

    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Try
            Dim dTotal As Double = 0
            dTotal = Session("dtCostChild").compute("sum(amount)", "")
            If Math.Round(dTotal, 4) = Math.Round(p_total, 4) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    Function check_others(ByVal p_voucherid As String, ByVal p_total As String) As Boolean

        Try
            Dim dTotal As Double = 0

            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Function

    Private Function check_Errors_sub(ByVal p_voucherid As String, ByVal p_amount As String, _
        Optional ByVal p_mandatory_costcenter As String = "") As String
        Try
            Dim str_cur_cost_center As String = ""
            Dim str_prev_cost_center As String = ""
            'Dim dTotal As Double = 0
            Dim iIndex As Integer
            Dim iLineid As Integer
            Dim bool_check_other, bool_mandatory_exists As Boolean

            Dim str_err As String = ""

            If p_mandatory_costcenter <> "" Then
                bool_mandatory_exists = False
            Else
                bool_mandatory_exists = True
            End If
            Dim str_balanced As Boolean = True
            bool_check_other = False


            For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If Session("dtCostChild").Rows(iIndex)("costcenter") = p_mandatory_costcenter Then
                        bool_mandatory_exists = True
                    End If
                    If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                        iLineid = -1
                        str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                        If str_balanced = False Then
                            str_err = str_err & " <BR> Invalid Allocation for cost center " & get_cost_center(Session("dtCostChild").Rows(iIndex)("costcenter"))
                        End If
                    End If
                    str_balanced = True
                    If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And bool_check_other = False Then
                        iLineid = -1
                        bool_check_other = True
                        str_balanced = check_others(p_voucherid, p_amount)
                        If str_balanced = False Then
                            If str_err = "" Then
                                str_err = "Invalid Allocation for other cost center"
                            Else
                                str_err = str_err & " <BR> Invalid Allocation for other cost centers "
                            End If

                        End If
                    End If
                    iLineid = iLineid + 1



                    str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                End If
            Next




            If bool_mandatory_exists = False Then
                str_err = "<br>Mandatory Cost center - " & get_cost_center(p_mandatory_costcenter) & " not allocated"
            End If


            Return str_err
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "child")
            Return ""
        End Try
    End Function

    Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            usrCostCenter1.Visible = True
            Dim lblRowId As New Label
            Dim lintIndex As Integer = 0
            ClearRadGridandCombo()
            lblRowId = TryCast(sender.parent.FindControl("lblId"), Label)
            Session("gintEditLine") = Convert.ToInt32(lblRowId.Text)
            For lintIndex = 0 To Session("dtDTL").Rows.Count - 1
                If (Session("dtDTL").Rows(lintIndex)("Id") = Session("gintEditLine")) Then
                    gvDetails.SelectedIndex = lintIndex
                    gvDetails.SelectedRowStyle.BackColor = Drawing.Color.LightCoral
                    gvDetails.SelectedRowStyle.ForeColor = Drawing.Color.Black
                    Session("gDtlDataMode") = "UPDATE"
                    RecreateSsssionDataSource()
                    CostCenterFunctions.SetGridSessionDataForEdit(Session("gintEditLine"), Session("dtCostChild"), _
                    Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    usrCostCenter1.BindCostCenter()
                    Exit For
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function UpdateCostCenter() As Boolean
        Try
            RecreateSsssionDataSource()
            If Not CostCenterFunctions.VerifyCostCenterAmount(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), _
                                                         txtAmount.Text) Then
                lblError.Text = "Invalid Cost Center Allocation!!!"
                UpdateCostCenter = False
                Exit Function
            End If
            Session("dtCostChild").Rows.Clear()
            Session("CostAllocation").Rows.Clear()
            If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                Dim mrow As DataRow
                CostCenterFunctions.AddCostCenter(0, Session("sBsuid"), _
                           Session("CostOTH"), txtdocdate.Text, Session("idCostChild"), _
                          Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                          Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))

                UpdateCostCenter = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()
    End Sub
    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub

    Private Function GetFREEZEDT(ByVal BSU_ID As String) As String
        Dim sqlString As String = "SELECT     BSU_FREEZEDT FROM BUSINESSUNIT_M where  BSU_ID='" & BSU_ID & "'"
        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CStr(result)
    End Function

    Function get_cost_center(ByVal p_ccsid As String) As String
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
               & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999' AND CCS_ID='" & p_ccsid & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("CCS_DESCR")
            End If
            Return p_ccsid
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return p_ccsid
        End Try

    End Function

    Sub clearMe()
        txtPartCode.Text = ""
        txtPartyAcc.Text = ""
        hfId1.Value = ""
        txtCostUnit.Text = ""
        txtPreCode.Text = ""
        txtPreAcc.Text = ""
        DETAIL_ACT_ID.Text = ""
        txtExpAcc.Text = ""
        'clear txtHDocno.text and reset to todays date
        txtHDocno.Text = ""
        txtdocdate.Text = UtilityObj.GetDiplayDate()
        bind_Currency()
        txtRefdocno.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtAmount.Text = ""
        txtInvoiceNo.Text = ""
        txtNarration1.Text = ""
        txtNarration2.Text = ""
        txtLpo.Text = ""
        gvDetails.Visible = False
        ClearRadGridandCombo()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        h_editorview.Value = ""
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearMe()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            'ddDoctype.Enabled = True
            'btnDocument.Enabled = True
            'lnkClear.Enabled = True
            btnNext.Visible = False
            btnPrevious.Visible = False

            h_editorview.Value = "add"

            ViewState("datamode") = "add"
            Call clearMe()
            Session("dtCostChild").Rows.Clear()
            ' Dim dtt As DataTable
            'dtt.Rows.Clear()
            Call getnextdocid()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog("Pre Payment", ex.Message)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        h_editorview.Value = "Edit"
        ViewState("datamode") = "edit"
        'gridbindmonthwise(Nothing)
        ' gvDetails.Visible = False
        btnNext.Visible = False
        btnPrevious.Visible = False
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If txtHDocno.Text.Trim <> "" Or txtHDocno.Text <> String.Empty Then
            Dim transaction As SqlTransaction
            Dim Status As Integer
            Dim PRP_ID As String = txtHDocno.Text
            Dim PRP_sub_ID As String = Session("Sub_ID")
            Dim PRP_BSU_ID As String = Session("sBsuid")
            Dim PRP_FYEAR As String = Session("F_YEAR")
            'for audit table
            Dim aud_docno As String = txtHDocno.Text
            'Delete  the  document
            Using conn As SqlConnection = ConnectionManger.GetOASISFinConnection

                transaction = conn.BeginTransaction("SampleTransaction")

                Try
                    Status = VoucherFunctions.DeletePREPAYMENTS_H(PRP_ID, PRP_sub_ID, PRP_BSU_ID, PRP_FYEAR, transaction)
                    If Status <> 0 Then

                        transaction.Rollback()
                        lblError.Text = "Record can not be Deleted"
                    Else
                        'if above delete process is done then track the delete operation
                        ' Dim aud_remark As String = UtilityObj.AudiChange()

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, aud_docno, "Delete", Page.User.Identity.Name.ToString)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        transaction.Commit()
                        Call clearMe()
                        lblError.Text = "Record Deleted Successfully"
                    End If
                Catch ex As Exception
                    lblError.Text = "Record could not be Deleted"
                End Try
            End Using
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            lblError.Text = "No records to delete"
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        Dim viewid As String = String.Empty
        viewid = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
        repSource = VoucherReports.PrePaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "PP", txtHDocno.Text, viewid, Session("HideCC"))
        Session("ReportSource") = repSource
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub cvDateFrom_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDateFrom.ServerValidate
        Dim sflag As Boolean = False
        Dim dateTime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(dateTime1) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try

    End Sub

    Protected Sub cvTodate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTodate.ServerValidate
        Dim sflag As Boolean = False

        Dim DateTime2 As Date
        Dim dateTime1 As Date
        Try
            DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date

            If IsDate(DateTime2) Then

                If DateTime2 < dateTime1.AddMonths(1) Then
                    sflag = False
                Else
                    sflag = True
                End If
            End If
            If sflag Then
                args.IsValid = True
            Else
                'CustomValidator1.Text = ""
                args.IsValid = False
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvDocDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDocDate.ServerValidate
        Dim sflag As Boolean = False
        Dim datetime2 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            datetime2 = Date.ParseExact(txtdocdate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(datetime2) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExpAcc.Click
        set_mandatory_hidden()
    End Sub

    Sub set_mandatory_hidden()
        If DETAIL_ACT_ID.Text.Trim <> "" Then
            Using UserreaderExpCode As SqlDataReader = AccessRoleUser.GetPly_CostCenter(DETAIL_ACT_ID.Text)
                If UserreaderExpCode.HasRows Then
                    While UserreaderExpCode.Read
                        hfId2.Value = Convert.ToString(UserreaderExpCode("P_Cost"))
                    End While
                End If
            End Using
        End If
    End Sub

    Protected Sub btnCostunit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCostunit.Click
        set_mandatory_hidden()
    End Sub

    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHGroupRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocdate.TextChanged

        Dim strfDate As String = txtdocdate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtdocdate.Text = strfDate
        End If
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub txtFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromDate.TextChanged
        Dim strfDate As String = txtFromDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFromDate.Text = strfDate
        End If
    End Sub

    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToDate.TextChanged
        Dim strfDate As String = txtToDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = "Invalid To Date"
            Exit Sub
        Else
            txtToDate.Text = strfDate
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Master.DisableScriptManager()
    End Sub

    Private Sub displayDetails(ByVal viewid As String)
        ViewState("datamode") = "view"
        'viewid = Encr_decrData.Decrypt(viewid.Replace(" ", "+"))
        hfGuid.Value = viewid
        'ddDoctype.Enabled = False
        'btnDocument.Enabled = False
        'lnkClear.Enabled = False

        Using UserreaderPrepayment As SqlDataReader = VoucherFunctions.GetPrepaymentDetail_Guid(viewid)
            While UserreaderPrepayment.Read
                'handle the null value returned from the reader incase  convert.tostring
                txtHDocno.Text = Convert.ToString(UserreaderPrepayment("DocNo"))
                txtPartCode.Text = Convert.ToString(UserreaderPrepayment("PRP_PARTY"))
                txtPartyAcc.Text = Convert.ToString(UserreaderPrepayment("PARTY_NAME"))
                hfId1.Value = Convert.ToString(UserreaderPrepayment("CostID"))
                txtCostUnit.Text = Convert.ToString(UserreaderPrepayment("CostDescr"))
                txtPreCode.Text = Convert.ToString(UserreaderPrepayment("PreAcc"))
                txtPreAcc.Text = Convert.ToString(UserreaderPrepayment("PreAccDescr"))
                DETAIL_ACT_ID.Text = Convert.ToString(UserreaderPrepayment("ExpAcc"))
                txtExpAcc.Text = Convert.ToString(UserreaderPrepayment("ExpDescr"))
                txtRefdocno.Text = Convert.ToString(UserreaderPrepayment("PRP_REFDOCNO"))
                If BSU_IsTAXEnabled Then
                    ddlVATCode.SelectedValue = Convert.ToString(UserreaderPrepayment("PRP_TAX_CODE"))
                End If

                Dim str_doctype As String = Convert.ToString(UserreaderPrepayment("PRP_REFTYPE"))
                If str_doctype <> "" Then
                    For i As Integer = 0 To ddDoctype.Items.Count - 1
                        If ddDoctype.Items(i).Value = str_doctype Then
                            ddDoctype.SelectedIndex = i
                        End If
                    Next
                End If

                'Dim d As Date
                'd = CDate(Convert.ToString(UserreaderPrepayment(8)))
                txtdocdate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("DOCDT")))
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("FTFrom")))
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("FTto")))
                txtAmount.Text = Convert.ToDouble(UserreaderPrepayment("TAmt"))
                txtInvoiceNo.Text = Convert.ToString(UserreaderPrepayment("InVoice"))
                ViewState("PRP_JVNO") = Convert.ToString(UserreaderPrepayment("JvNo"))
                txtNarration1.Text = Convert.ToString(UserreaderPrepayment("NR1"))
                txtNarration2.Text = Convert.ToString(UserreaderPrepayment("NR2"))
                txtLpo.Text = Convert.ToString(UserreaderPrepayment("LPO"))
                h_SubId.Value = Convert.ToString(UserreaderPrepayment("SubId"))
                h_fyear.Value = Convert.ToInt64(UserreaderPrepayment("fyear"))
                h_BSU_ID.Value = Convert.ToString(UserreaderPrepayment("BSU_ID"))
            End While
            ' clear of the resource end using
        End Using
        Call gridbindmonthwise(viewid)
        bind_Currency()
        set_mandatory_hidden()
        btnPrevious.Enabled = True
        btnNext.Enabled = True
        Dim ss As Integer = Session("ArrNext").GetLength(0)
        If Session("NoNext") = 0 Then
            btnPrevious.Enabled = False
        ElseIf Session("NoNext") = Session("ArrNext").GetLength(0) - 2 Then
            btnNext.Enabled = False
        End If
    End Sub

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("NoNext") = 0 Then
            Exit Sub
        End If
        Session("NoNext") = CInt(Session("NoNext")) - 1
        displayDetails(Session("ArrNext")(Session("NoNext")))
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("NoNext") = Session("ArrNext").GetLength(0) - 2 Then
            Exit Sub
        End If
        Session("NoNext") = CInt(Session("NoNext")) + 1
        displayDetails(Session("ArrNext")(Session("NoNext")))
    End Sub

    Protected Sub DETAIL_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DETAIL_ACT_ID.TextChanged
        ClearRadGridandCombo()
    End Sub
End Class

