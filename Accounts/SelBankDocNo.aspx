<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelBankDocNo.aspx.vb" Inherits="SelBankDocNo" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bank Selection</title>
    
    <base target="_self" />
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" />
    <%--<style type="text/css" > 
 .odd{background-color: white;} 
 .even{background-color: gray;} 
</style>--%>
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;


            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }


    </script>
</head>
<body onload="listen_window();">


    <form id="form1" runat="server">
        <table id="tbl" width="100%" align="center">
            <tr valign="top">
                <td>
                    <input id="h_SelectedId" runat="server" type="hidden" value="" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                </td>
            </tr>
            <tr valign="top">
                <td >
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                        EmptyDataText="No Data" Width="100%" AllowPaging="True">
                        <RowStyle CssClass="griditem"  />
                        <SelectedRowStyle  />
                        <HeaderStyle CssClass="gridheader_pop"  />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("VHH_DOCDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />

                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                <HeaderTemplate>
                                    <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtBSUName" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("VHH_DOCNO") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <!--
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                SelectCommand="SELECT * FROM [vw_OSO_EMPLOYEE_M]"></asp:SqlDataSource>-->
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
