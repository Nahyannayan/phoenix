Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Accounts_accsjvSJournalVoucher
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbUploadEmployee)
        lblError.Text = ""
        If Page.IsPostBack = False Then
            Session("CHECKLAST") = 0
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If
            bind_groupPolicy()
            initialize_components()
            usrCostCenter1.AccountControlName = "txtDAccountCode"
            'txtHNarration.Attributes.Add("onblur", "javascript:CopyDetails()")
            txtHNarration.Attributes.Add("onBlur", "CopyDetails();narration_check('" & txtHNarration.ClientID & "');")
            txtDNarration.Attributes.Add("onBlur", "narration_check('" & txtDNarration.ClientID & "');")

            '''''check menu rights
            If Request.QueryString("editerror") <> "" Then
                lblError.Text = "Record already posted/Locked"
            End If

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            If (Request.QueryString("edited") <> "") Then
                lblError.Text = getErrorMessage("521")
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A150080" And MainMnu_code <> "A200060") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), _
                ViewState("menu_rights"), ViewState("datamode"))
            End If
            '''''check menu rights
            If Request.QueryString("viewid") <> "" Then
                set_viewdata()
                btnAdd.Visible = True
                btnEdit.Visible = True
                setViewData()
                setModifyHeader(Request.QueryString("viewid"))
            Else
                ResetViewData()
            End If
            gridbind()
            bind_Currency()
            If Request.QueryString("invalidedit") = "1" Then
                lblError.Text = "Invalid Editid"
                Exit Sub
            End If
            If Request.QueryString("editid") = "" And Request.QueryString("viewid") = "" Then
                getnextdocid()
            End If
            ''call the object during the page load to check the initial stage
            UtilityObj.beforeLoopingControls(Me.Page)
        End If

    End Sub


    Sub initialize_components()
        txtHDocdate.Text = GetDiplayDate()
        txtRevDate.Text = Format(CDate(Session("EntryDate")).AddDays(1), "dd/MMM/yyyy")
        ViewState("str_timestamp") = New Byte()
        txtHExchRate.Attributes.Add("readonly", "readonly")
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")
        txtDAccountName.Attributes.Add("readonly", "readonly")
        txtHLocalRate.Attributes.Add("readonly", "readonly")
        Session("dtJournal") = DataTables.CreateDataTable_JV()
        Session("dtCostChild") = CostCenterFunctions.CreateDataTableCostCenter()
        Session("CostAllocation") = CostCenterFunctions.CreateDataTable_CostAllocation()
        viewstate("idJournal") = 0
        Session("idCostChild") = 0
        'btnCancel.Visible = False
        btnAdds.Visible = True
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
    End Sub


    Private Sub setViewData()
        tbl_Details.Visible = False
        gvJournal.Columns(6).Visible = False
        gvJournal.Columns(7).Visible = False
        btnHAccount.Enabled = False
        imgCalendar.Enabled = False
        DDCurrency.Enabled = False
        txtHDocdate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")
        txtDAccountCode.Attributes.Add("readonly", "readonly")
        txtHOldrefno.Attributes.Add("readonly", "readonly")
    End Sub


    Private Sub ResetViewData()
        tbl_Details.Visible = True
        gvJournal.Columns(6).Visible = True
        gvJournal.Columns(7).Visible = True
        btnHAccount.Enabled = True
        imgCalendar.Enabled = True
        DDCurrency.Enabled = True
        txtHDocdate.Attributes.Remove("readonly")
        txtHNarration.Attributes.Remove("readonly")
        txtHOldrefno.Attributes.Remove("readonly")
    End Sub


    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM SJOURNAL_H where GUID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocno.Text = p_Modifyid
                txtHDocdate.Text = Format(CDate(ds.Tables(0).Rows(0)("SHD_DOCDT")), "dd/MMM/yyyy")
                txtRevDate.Text = Format(CDate(ds.Tables(0).Rows(0)("SHD_REVDATE")), "dd/MMM/yyyy")
                txtHNarration.Text = ds.Tables(0).Rows(0)("SHD_NARRATION")
                txtHOldrefno.Text = ds.Tables(0).Rows(0)("SHD_REFNO") & ""
                txtHDocno.Text = ds.Tables(0).Rows(0)("SHD_DOCNO")
                bind_Currency()
                setModifyDetails(p_Modifyid)
            Else
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyDetails(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM SJOURNAL_D where SJL_DOCNO='" _
            & ViewState("str_editData").Split("|")(0) & "'and SJL_BDELETED='False' " _
            & " AND (SJL_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (SJL_SUB_ID = '" & Session("Sub_ID") & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'JNL_DEBIT, JNL_CREDIT, JNL_NARRATION, 
                    Dim rDt As DataRow
                    'Dim i As Integer
                    Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("SJL_ACT_ID"))
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("SJL_SLNO")

                    viewstate("idJournal") = viewstate("idJournal") + 1
                    rDt("Accountid") = ds.Tables(0).Rows(iIndex)("SJL_ACT_ID")
                    rDt("Accountname") = str_actname_cost_mand.Split("|")(0)
                    rDt("Narration") = ds.Tables(0).Rows(iIndex)("SJL_NARRATION")

                    rDt("Credit") = ds.Tables(0).Rows(iIndex)("SJL_CREDIT")
                    rDt("Debit") = ds.Tables(0).Rows(iIndex)("SJL_DEBIT")

                    'rDt("CostCenter") = str_actname_cost_mand.Split("|")(1)
                    'rDt("Required") = Convert.ToBoolean(str_actname_cost_mand.Split("|")(2))
                    rDt("Status") = "Normal"
                    Session("dtJournal").Rows.Add(rDt)
                Next
                Session("dtCostChild") = CostCenterFunctions.ViewCostCenterDetails(Session("Sub_ID"), Session("sBsuid"), _
                "SJV", ViewState("str_editData").Split("|")(0))

                Session("CostAllocation") = CostCenterFunctions.ViewCostCenterAllocDetails(Session("Sub_ID"), Session("sBsuid"), _
                "SJV", ViewState("str_editData").Split("|")(0))
                'setModifyCost(p_Modifyid)
            Else

            End If
            viewstate("idJournal") = viewstate("idJournal") + 1

            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyCost(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT GUID, VDS_DOCTYPE ,VDS_DOCNO ," _
            & "VDS_ACT_ID ,VDS_CCS_ID ,VDS_CODE,VDS_DESCR ,VDS_AMOUNT," _
            & "VDS_SLNO,VDS_CCS_ID, VDS_ERN_ID,VDS_CSS_CSS_ID FROM VOUCHER_D_S WHERE VDS_DOCNO='" _
             & ViewState("str_editData").Split("|")(0) & "' and VDS_BDELETED='False'  AND  isnull(VDS_Auto,0) = 0 " _
            & " AND (VDS_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (VDS_SUB_ID = '" & Session("Sub_ID") & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim rDt As DataRow
                    'Dim i As Integer
                    'Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("JNL_ACT_ID"))
                    rDt = Session("dtCostChild").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("SubMemberid") = ds.Tables(0).Rows(iIndex)("VDS_CSS_CSS_ID")
                    rDt("Memberid") = ds.Tables(0).Rows(iIndex)("VDS_CODE")
                    rDt("VoucherId") = ds.Tables(0).Rows(iIndex)("VDS_SLNO")
                    rDt("Costcenter") = ds.Tables(0).Rows(iIndex)("VDS_CCS_ID")
                    rDt("ERN_ID") = ds.Tables(0).Rows(iIndex)("VDS_ERN_ID")
                    rDt("Name") = ds.Tables(0).Rows(iIndex)("VDS_DESCR")
                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("VDS_AMOUNT")

                    'btnAdddetails.Visible = False

                    rDt("Status") = "Normal"
                    'idCostChild = idCostChild + 1
                    Session("dtCostChild").Rows.Add(rDt)
                Next
            Else
            End If
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function getAccountname(ByVal p_accountid As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M where ACT_ID='" & p_accountid & "' "

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & p_accountid & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_NAME") & "|" _
                & ds.Tables(0).Rows(0)("PLY_COSTCENTER") & "|" _
                & ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
    End Function


    Sub bind_groupPolicy()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
            & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub getnextdocid()
        If ViewState("datamode") <> "edit" Then
            Try
                txtHDocno.Text = AccountFunctions.GetNextDocId("SJV", Session("sBsuid"), CType(txtHDocdate.Text, Date).Month, CType(txtHDocdate.Text, Date).Year)
                If txtHDocno.Text = "" Then
                    lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                    btnSave.Enabled = False
                Else
                    btnSave.Enabled = True
                End If
            Catch ex As Exception
                lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                btnSave.Enabled = False
                Errorlog(ex.Message)
            End Try
        End If

    End Sub


    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            DDCurrency.Items.Clear()
            DDCurrency.DataSource = MasterFunctions.GetExchangeRates(txtHDocdate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If DDCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    DDCurrency.SelectedIndex = 0
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
                btnSave.Enabled = True
            Else
                txtHExchRate.Text = "0"
                txtHLocalRate.Text = "0"
                btnSave.Enabled = False
                lblError.Text = "Cannot Save Data. Currency/Exchange Rate Not Set"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In DDCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            Errorlog(ex.Message)
            Return False
        End Try
    End Function


    Protected Sub txtHDocdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHDocdate.TextChanged
        Dim strfDate As String = txtHDocdate.Text.Trim

        Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtHDocdate.Text = strfDate
        End If
        bind_Currency()
        getnextdocid()
        txtHNarration.Focus()
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub


    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdds.Click
        'If Not IsNumeric(txtDAmount.Text) Then
        '    lblError.Text = "Invalid Amount!!!"
        '    Exit Sub
        'End If
        txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))
        Dim str_cost_center As String = ""
        '''''FIND ACCOUNT IS THERE
        Dim bool_cost_center_reqired As Boolean = False

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else
                txtDAccountName.Text = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            txtDAccountName.Text = ""
        End Try
        '''''FIND ACCOUNT IS THERE
        If txtDAccountName.Text = "" Then
            lblError.Text = getErrorMessage("303") ' account already there
            Exit Sub
        End If
        'Check CostCenter Summary
        Dim DAmount As Decimal
        If Not IsNumeric(txtDAmount.Text) Then
            lblError.Text = "Invalid Amount!!!"
            Exit Sub
        Else
            DAmount = Math.Abs(Convert.ToDecimal(txtDAmount.Text))
        End If
        RecreateSsssionDataSource()
        If Not CostCenterFunctions.VerifyCostCenterAmount(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), _
                                                            DAmount) Then
            lblError.Text = "Invalid Cost Center Allocation!!!"
            Exit Sub
        End If
        Try
            Dim rDt As DataRow

            Dim i As Integer
            Dim dCrorDb As Double
            dCrorDb = CDbl(txtDAmount.Text.Trim)
            If dCrorDb <> 0 Then
                If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                    CostCenterFunctions.AddCostCenter(ViewState("idJournal"), Session("sBsuid"), _
                                      Session("CostOTH"), txtHDocdate.Text, Session("idCostChild"), _
                                     Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                     Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                End If
                rDt = Session("dtJournal").NewRow
                rDt("Id") = ViewState("idJournal")
                ViewState("idJournal") = ViewState("idJournal") + 1
                rDt("Accountid") = txtDAccountCode.Text.Trim
                rDt("Accountname") = txtDAccountName.Text.Trim
                rDt("Narration") = txtDNarration.Text.Trim
                If dCrorDb > 0 Then
                    rDt("Credit") = "0"
                    rDt("Debit") = dCrorDb
                Else
                    rDt("Credit") = dCrorDb * -1
                    rDt("Debit") = "0"
                End If
                '   rDt("CostCenter") = str_cost_center
                ' rDt("Required") = bool_cost_center_reqired
                ' rDt("GUID") = System.DBNull.Value

                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Accountid") = rDt("Accountid") And _
                         Session("dtJournal").Rows(i)("Accountname") = rDt("Accountname") And _
                          Session("dtJournal").Rows(i)("Narration") = rDt("Narration") And _
                          Session("dtJournal").Rows(i)("Credit") = rDt("Credit") And _
                          Session("dtJournal").Rows(i)("Debit") = rDt("Debit") Then
                        lblError.Text = "Cannot add transaction details.The entered transaction details are repeating."
                        gridbind()
                        Exit Sub
                    End If
                Next
                Session("dtJournal").Rows.Add(rDt)
            End If
            SetGridColumnVisibility(True)
            gridbind()
            Clear_Details()
            If gvJournal.Rows.Count = 1 Then
                txtDAmount.Text = AccountFunctions.Round(dCrorDb) * -1
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "Enter valid number")
            lblError.Text = getErrorMessage("510")
        End Try
    End Sub


    Private Sub gridbind()
        Try
            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = DataTables.CreateDataTable_JV()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        If Session("dtJournal").Rows(i)("Credit") <> 0 Then
                            dCredit = dCredit + AccountFunctions.Round2(Session("dtJournal").Rows(i)("Credit"), 2)
                        Else
                            dDebit = dDebit + AccountFunctions.Round2(Session("dtJournal").Rows(i)("Debit"), 2)
                        End If
                        dDebit = AccountFunctions.Round2(dDebit, 2)
                        dCredit = AccountFunctions.Round2(dCredit, 2)
                        ' dTotAmount = dTotAmount + session("dtCostChild").Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtTDotalDebit.Text = AccountFunctions.Round2(dDebit, 2)
            txtTotalCredit.Text = AccountFunctions.Round2(dCredit, 2)
            txtDifference.Text = AccountFunctions.Round(dDebit - dCredit)
            If dDebit <> dCredit Or dCredit = 0 Then
                btnSave.Enabled = False
            Else
                btnSave.Enabled = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function get_mandatory_costcenter(ByVal p_id As String) As String
        If Session("dtJournal").Rows.Count > 0 Then
            For i As Integer = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(i)("id") & "" = p_id Then
                    Return Session("dtJournal").Rows(i)("Costcenter")
                End If
            Next
        End If
        Return ""
    End Function


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim cmdCol As Integer = gvJournal.Columns.Count - 1
            'For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
            Dim lblReqd As New Label
            Dim lblid As New Label
            Dim lblDebit As New Label
            Dim lblCredit As New Label
            Dim btnAlloca As New LinkButton
            lblReqd = e.Row.FindControl("lblRequired")
            lblid = e.Row.FindControl("lblId")
            lblDebit = e.Row.FindControl("lblDebit")
            lblCredit = e.Row.FindControl("lblCredit")
            btnAlloca = e.Row.FindControl("btnAlloca")
            If btnAlloca IsNot Nothing Then
                Dim dAmt As Double
                If CDbl(lblDebit.Text) > 0 Then
                    dAmt = CDbl(lblDebit.Text)
                Else
                    dAmt = CDbl(lblCredit.Text)
                End If
                btnAlloca.OnClientClick = "javascript:AddDetails('vid=" & lblid.Text & "&amt=" & dAmt & "&sid=" & get_mandatory_costcenter(lblid.Text) & "');return false;"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvJournal.RowDeleting
        Try
            If btnAdds.Visible = True Then
                Dim row As GridViewRow = gvJournal.Rows(e.RowIndex)
                Dim lblTid As New Label
                '        Dim lblGrpCode As New Label
                lblTid = TryCast(row.FindControl("lblId"), Label)

                Dim iRemove As Integer = 0
                Dim str_Index As String = ""
                str_Index = lblTid.Text
                For iRemove = 0 To Session("dtJournal").Rows.Count - 1
                    If str_Index = Session("dtJournal").Rows(iRemove)("id") Then
                        If viewstate("datamode") <> "edit" Then
                            Session("dtJournal").Rows(iRemove).Delete()
                        Else
                            Session("dtJournal").Rows(iRemove)("Status") = "Deleted"
                        End If
                        Exit For
                    End If
                Next
                For iRemove = 0 To Session("dtCostChild").Rows.Count - 1
                    If str_Index = Session("dtCostChild").Rows(iRemove)("Voucherid") Then
                        'session("dtCostChild").Rows(iRemove).Delete()
                        Session("dtCostChild").Rows(iRemove)("Status") = "Deleted"
                    End If
                Next
                'gvTransaction.EditIndex = -1
                gridbind()
            Else
                lblError.Text = "Cannot delete. Please cancel updation and delete"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTid As New Label
        lblTid = TryCast(sender.parent.FindControl("lblId"), Label)
        h_Editid.Value = lblTid.Text
        Dim iIndex As Integer = 0
        Dim str_Search As String = ""
        str_Search = lblTid.Text
        For iIndex = 0 To Session("dtJournal").Rows.Count - 1
            If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                txtDAccountCode.Text = Session("dtJournal").Rows(iIndex)("Accountid")
                txtDAccountName.Text = Session("dtJournal").Rows(iIndex)("Accountname")
                If Session("dtJournal").Rows(iIndex)("Credit") > 0 Then
                    txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Credit")) * -1
                Else
                    txtDAmount.Text = AccountFunctions.Round(Session("dtJournal").Rows(iIndex)("Debit"))
                End If
                txtDNarration.Text = Session("dtJournal").Rows(iIndex)("Narration")
                btnAdds.Visible = False
                btnUpdate.Visible = True
                btnEditCancel.Visible = True
                gvJournal.SelectedIndex = iIndex
                ''''''
                RecreateSsssionDataSource()
                CostCenterFunctions.SetGridSessionDataForEdit(str_Search, Session("dtCostChild"), _
                Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                usrCostCenter1.BindCostCenter()
                tbl_Details.Visible = True
                SetGridColumnVisibility(False)
                ''''''
                'gridbind()

                'gridbind()
                Exit For
            End If
        Next
    End Sub

    Protected Sub SetGridColumnVisibility(ByVal pSet As Boolean)
        gvJournal.Columns(6).Visible = pSet
        gvJournal.Columns(7).Visible = pSet
    End Sub

    Protected Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        btnAdds.Visible = True
        SetGridColumnVisibility(True)
        btnUpdate.Visible = False
        btnEditCancel.Visible = False
        gvJournal.SelectedIndex = -1
        Clear_Details()
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim iIndex As Integer = 0
            Dim str_Search As String = ""
            Dim dCrordb As Double = CDbl(txtDAmount.Text)
            str_Search = h_Editid.Value
            '''''
            txtDAccountName.Text = AccountFunctions.check_accountid(txtDAccountCode.Text & "", Session("sBsuid"))
            Dim str_cost_center As String = ""
            '''''FIND ACCOUNT IS THERE
            Dim bool_cost_center_reqired As Boolean = False

            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim str_Sql As String

                str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
                & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
                & " FROM ACCOUNTS_M AM, POLICY_M PM  WHERE" _
                & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
                & " AND ACT_ID='" & txtDAccountCode.Text & "'" _
                & " AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH='N'" _
                & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtDAccountName.Text = ds.Tables(0).Rows(0)("ACT_NAME")
                    str_cost_center = ds.Tables(0).Rows(0)("PLY_COSTCENTER")
                    bool_cost_center_reqired = ds.Tables(0).Rows(0)("PLY_BMANDATORY")
                Else
                    txtDAccountName.Text = ""
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
                txtDAccountName.Text = ""
            End Try
            If dCrordb = 0 Then
                Exit Sub
            End If
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If str_Search = Session("dtJournal").Rows(iIndex)("id") And Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then

                    If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
                        CostCenterFunctions.AddCostCenter(Session("dtJournal").Rows(iIndex)("id"), Session("sBsuid"), _
                                          Session("CostOTH"), txtHDocdate.Text, Session("idCostChild"), _
                                         Session("dtCostChild"), Session(usrCostCenter1.SessionDataSource_1.SessionKey), _
                                         Session("idCostAlocation"), Session("CostAllocation"), Session(usrCostCenter1.SessionDataSource_2.SessionKey))
                    End If

                    If (Session("dtJournal").Rows(iIndex)("Accountid") <> txtDAccountCode.Text.Trim) Then
                        ''updation handle here
                        Dim j As Integer = 0
                        If ViewState("datamode") <> "edit" Then
                            While j < Session("dtCostChild").Rows.Count
                                If Session("dtCostChild").Rows(j)("Voucherid") = str_Search Then
                                    Session("dtCostChild").Rows.Remove(Session("dtCostChild").Rows(j))
                                Else
                                    j = j + 1
                                End If
                            End While
                        End If
                        ''updation handle here
                    End If
                    Session("dtJournal").Rows(iIndex)("Accountid") = txtDAccountCode.Text.Trim
                    Session("dtJournal").Rows(iIndex)("Accountname") = txtDAccountName.Text.Trim
                    If dCrordb > 0 Then
                        Session("dtJournal").Rows(iIndex)("Credit") = "0"
                        Session("dtJournal").Rows(iIndex)("Debit") = dCrordb
                    Else
                        Session("dtJournal").Rows(iIndex)("Credit") = dCrordb * -1
                        Session("dtJournal").Rows(iIndex)("Debit") = "0"
                    End If
                    Session("dtJournal").Rows(iIndex)("Narration") = txtDNarration.Text.Trim
                    'Session("dtJournal").Rows(iIndex)("Required") = bool_cost_center_reqired
                    'Session("dtJournal").Rows(iIndex)("CostCenter") = str_cost_center
                    btnAdds.Visible = True
                    btnUpdate.Visible = False
                    btnEditCancel.Visible = False
                    gvJournal.SelectedIndex = iIndex
                    gvJournal.SelectedIndex = -1
                    SetGridColumnVisibility(True)
                    Clear_Details()
                    gridbind()
                    Exit For
                End If
            Next
        Catch ex As Exception
            Errorlog("UPDATE")
        End Try
    End Sub


    Private Sub clear_All()
        Session("dtJournal").Rows.Clear()
        Session("dtCostChild").Rows.Clear()
        gridbind()
        Clear_Details()
        txtHOldrefno.Text = ""
        getnextdocid()
        txtHNarration.Text = ""
        txtDNarration.Text = ""
    End Sub


    Private Sub Clear_Details()
        txtDAmount.Text = ""
        txtDNarration.Text = txtHNarration.Text & ""
        txtDAccountName.Text = ""
        txtDAccountCode.Text = ""
        ClearRadGridandCombo()
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strfDate As String = txtHDocdate.Text.Trim

            Dim str_err As String = DateFunctions.checkdate_nofuture(strfDate)
            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            Else
                txtHDocdate.Text = strfDate
            End If
            strfDate = txtRevDate.Text.Trim
            str_err = DateFunctions.checkdate(strfDate)

            If str_err <> "" Then
                lblError.Text = str_err
                Exit Sub
            Else
                txtRevDate.Text = strfDate
            End If

            Dim s As String = check_Errors()
            If s <> "" Then
                lblError.Text = s
                Exit Sub
            End If

            viewstate("iDeleteCount") = 0
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'your transaction here

                'Adding header info
                Dim cmd As New SqlCommand("SaveSSJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpSHD_SUB_ID As New SqlParameter("@SHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpSHD_SUB_ID.Value = Session("Sub_ID")
                cmd.Parameters.Add(sqlpSHD_SUB_ID)

                Dim sqlpsqlpSHD_BSU_ID As New SqlParameter("@SHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpSHD_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpSHD_BSU_ID)

                Dim sqlpSHD_FYEAR As New SqlParameter("@SHD_FYEAR", SqlDbType.Int)
                sqlpSHD_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpSHD_FYEAR)

                Dim sqlpSHD_DOCTYPE As New SqlParameter("@SHD_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpSHD_DOCTYPE.Value = "SJV"
                cmd.Parameters.Add(sqlpSHD_DOCTYPE)

                Dim sqlpSHD_DOCNO As New SqlParameter("@SHD_DOCNO", SqlDbType.VarChar, 20)
                If ViewState("datamode") = "edit" Then
                    sqlpSHD_DOCNO.Value = ViewState("str_editData").Split("|")(0) & ""
                Else
                    sqlpSHD_DOCNO.Value = " "
                End If
                cmd.Parameters.Add(sqlpSHD_DOCNO)

                Dim sqlpSHD_DOCDT As New SqlParameter("@SHD_DOCDT", SqlDbType.DateTime, 30)
                sqlpSHD_DOCDT.Value = txtHDocdate.Text & ""
                cmd.Parameters.Add(sqlpSHD_DOCDT)

                Dim sqlpSHD_CUR_ID As New SqlParameter("@SHD_CUR_ID", SqlDbType.VarChar, 12)
                sqlpSHD_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                cmd.Parameters.Add(sqlpSHD_CUR_ID)

                Dim sqlpSHD_NARRATION As New SqlParameter("@SHD_NARRATION", SqlDbType.VarChar, 300)
                sqlpSHD_NARRATION.Value = txtHNarration.Text & ""
                cmd.Parameters.Add(sqlpSHD_NARRATION)

                Dim sqlpSHD_EXGRATE1 As New SqlParameter("@SHD_EXGRATE1", SqlDbType.Decimal, 8)
                sqlpSHD_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                cmd.Parameters.Add(sqlpSHD_EXGRATE1)

                Dim sqlpSHD_EXGRATE2 As New SqlParameter("@SHD_EXGRATE2", SqlDbType.Decimal, 8)
                sqlpSHD_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                cmd.Parameters.Add(sqlpSHD_EXGRATE2)

                Dim sqlpSHD_bPOSTED As New SqlParameter("@SHD_bPOSTED", SqlDbType.Bit)
                sqlpSHD_bPOSTED.Value = False
                cmd.Parameters.Add(sqlpSHD_bPOSTED)

                Dim sqlpSHD_bDELETED As New SqlParameter("@SHD_bDELETED", SqlDbType.Bit)
                sqlpSHD_bDELETED.Value = False
                cmd.Parameters.Add(sqlpSHD_bDELETED)

                Dim sqlpbGenerateNewNo As New SqlParameter("@bGenerateNewNo", SqlDbType.Bit)
                sqlpbGenerateNewNo.Value = True
                cmd.Parameters.Add(sqlpbGenerateNewNo)

                Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                If ViewState("datamode") = "edit" Then
                    sqlpbEdit.Value = True
                Else
                    sqlpbEdit.Value = False
                End If
                cmd.Parameters.Add(sqlpbEdit)

                Dim sqlpSHD_SESSIONID As New SqlParameter("@SHD_SESSIONID", SqlDbType.VarChar, 50)
                sqlpSHD_SESSIONID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpSHD_SESSIONID)

                Dim sqlpSHD_LOCK As New SqlParameter("@SHD_LOCK", SqlDbType.VarChar, 50)
                sqlpSHD_LOCK.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpSHD_LOCK)

                Dim sqlpSHD_TIMESTAMP As New SqlParameter("@SHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                If ViewState("datamode") <> "edit" Then
                    sqlpSHD_TIMESTAMP.Value = System.DBNull.Value
                Else
                    sqlpSHD_TIMESTAMP.Value = ViewState("str_timestamp")
                End If
                cmd.Parameters.Add(sqlpSHD_TIMESTAMP)


                Dim sqlpVHH_REFNO As New SqlParameter("@SHD_REFNO", SqlDbType.VarChar, 50)
                sqlpVHH_REFNO.Value = txtHOldrefno.Text
                cmd.Parameters.Add(sqlpVHH_REFNO) '@SHD_REVDATE

                Dim sqlpSHD_REVDATE As New SqlParameter("@SHD_REVDATE", SqlDbType.VarChar, 50)
                sqlpSHD_REVDATE.Value = txtRevDate.Text
                cmd.Parameters.Add(sqlpSHD_REVDATE)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)

                Dim sqlopSHD_NEWDOCNO As New SqlParameter("@SHD_NEWDOCNO", SqlDbType.VarChar, 20)
                cmd.Parameters.Add(sqlopSHD_NEWDOCNO)
                cmd.Parameters("@SHD_NEWDOCNO").Direction = ParameterDirection.Output
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                'Adding header info
                cmd.Parameters.Clear()
                'Adding transaction info
                'Dim str_err As String = ""
                If (iReturnvalue = 0) Then
                    ' stTrans.Commit()
                    If ViewState("datamode") <> "edit" Then
                        str_err = DoTransactions(objConn, stTrans, sqlopSHD_NEWDOCNO.Value)
                    Else

                        str_err = DeleteJOURNAL_D_S_ALL(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        If str_err = "0" Then
                            str_err = DoTransactions(objConn, stTrans, ViewState("str_editData").Split("|")(0))
                        End If
                    End If
                    If str_err = "0" Then
                        stTrans.Commit()
                        h_editorview.Value = ""

                        btnSave.Enabled = False
                        gvJournal.Enabled = True
                        ''lblError.Text = getErrorMessage("304")
                        txtDNarration.Text = ""
                        If ViewState("datamode") <> "edit" Then
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, sqlopSHD_NEWDOCNO.Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            lblError.Text = getErrorMessage("304")
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            'datamode = Encr_decrData.Encrypt("view")
                            'Response.Redirect("acccrCashReceipt.aspx?viewid=" & Request.QueryString("viewid") & "&edited=yes" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & datamode)
                            lblError.Text = getErrorMessage("305")
                        End If
                        clear_All()
                    Else '.Split("__")(0).Trim & ""
                        lblError.Text = getErrorMessage(str_err.Split("__")(0).Trim)
                        stTrans.Rollback()
                    End If
                Else
                    lblError.Text = getErrorMessage(iReturnvalue & "")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                lblError.Text = getErrorMessage("1000")
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        End Try
    End Sub
    Private Function DeleteJOURNAL_D_S_ALL(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim cmd As New SqlCommand
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString


        cmd.Dispose()

        cmd = New SqlCommand("DeleteVOUCHER_D_S_ALL", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpJDS_SUB_ID As New SqlParameter("@VDS_SUB_ID", SqlDbType.VarChar, 20)
        sqlpJDS_SUB_ID.Value = Session("SUB_ID") & ""
        cmd.Parameters.Add(sqlpJDS_SUB_ID)

        Dim sqlpJDS_BSU_ID As New SqlParameter("@VDS_BSU_ID", SqlDbType.VarChar, 20)
        sqlpJDS_BSU_ID.Value = Session("sBsuid") & ""
        cmd.Parameters.Add(sqlpJDS_BSU_ID)

        Dim sqlpJDS_FYEAR As New SqlParameter("@VDS_FYEAR", SqlDbType.Int)
        sqlpJDS_FYEAR.Value = Session("F_YEAR") & ""
        cmd.Parameters.Add(sqlpJDS_FYEAR)

        Dim sqlpJDS_DOCTYPE As New SqlParameter("@VDS_DOCTYPE", SqlDbType.VarChar, 10)
        sqlpJDS_DOCTYPE.Value = "SJV"
        cmd.Parameters.Add(sqlpJDS_DOCTYPE)

        Dim sqlpJDS_DOCNO As New SqlParameter("@VDS_DOCNO", SqlDbType.VarChar, 20)
        sqlpJDS_DOCNO.Value = p_docno
        cmd.Parameters.Add(sqlpJDS_DOCNO)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        Return retValParam.Value
    End Function



    Private Function DoTransactions(ByVal objConn As SqlConnection, _
    ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        Try
            Dim iIndex As Integer = 0
            ' ''FIND OPPOSITE ACCOUNT
            Dim str_dr_opposite, str_cr_opposite As String
            str_dr_opposite = ""
            str_cr_opposite = ""
            Dim d_cr_largest As Double = 0
            Dim d_dr_largest As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                    If Session("dtJournal").Rows(iIndex)("Debit") > d_dr_largest Then
                        str_dr_opposite = Session("dtJournal").Rows(iIndex)("Accountid")
                        d_dr_largest = Session("dtJournal").Rows(iIndex)("Debit")
                    End If
                Else
                    If Session("dtJournal").Rows(iIndex)("Credit") > d_cr_largest Then
                        str_cr_opposite = Session("dtJournal").Rows(iIndex)("Accountid")
                        d_cr_largest = Session("dtJournal").Rows(iIndex)("Credit")
                    End If
                End If
            Next
            ''''
            'Adding transaction info
            Dim cmd As New SqlCommand

            Dim str_err As String = ""
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    cmd.Dispose()
                    cmd = New SqlCommand("SaveSJOURNAL_D", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure
                    '' ''Handle sub table
                    Dim str_crdb As String = "CR"
                    If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                        dTotal = Session("dtJournal").Rows(iIndex)("Debit")
                        str_crdb = "DR"
                    Else
                        dTotal = Session("dtJournal").Rows(iIndex)("Credit")
                    End If
                    '' ''Handle sub table
                    iReturnvalue = DoTransactions_Sub_Table(objConn, stTrans, p_docno, Session("dtJournal").Rows(iIndex)("id"), _
                    str_crdb, iIndex + 1 - ViewState("iDeleteCount"), Session("dtJournal").Rows(iIndex)("Accountid"), dTotal)
                    If iReturnvalue <> "0" Then
                        Return iReturnvalue
                    End If
                    '' ''

                    Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
                    sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                    cmd.Parameters.Add(sqlpGUID)

                    Dim sqlpSJL_SUB_ID As New SqlParameter("@SJL_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpSJL_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpSJL_SUB_ID)

                    Dim sqlpsqlpSJL_BSU_ID As New SqlParameter("@SJL_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpSJL_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpSJL_BSU_ID)

                    Dim sqlpSJL_FYEAR As New SqlParameter("@SJL_FYEAR", SqlDbType.Int)
                    sqlpSJL_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpSJL_FYEAR)

                    Dim sqlpSJL_DOCTYPE As New SqlParameter("@SJL_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpSJL_DOCTYPE.Value = "SJV"
                    cmd.Parameters.Add(sqlpSJL_DOCTYPE)

                    Dim sqlpSJL_DOCNO As New SqlParameter("@SJL_DOCNO", SqlDbType.VarChar, 20)
                    sqlpSJL_DOCNO.Value = p_docno & ""
                    cmd.Parameters.Add(sqlpSJL_DOCNO)

                    Dim sqlpSJL_DOCDT As New SqlParameter("@SJL_DOCDT", SqlDbType.DateTime, 30)
                    sqlpSJL_DOCDT.Value = txtHDocdate.Text & ""
                    cmd.Parameters.Add(sqlpSJL_DOCDT)

                    Dim sqlpSJL_NARRATION As New SqlParameter("@SJL_NARRATION", SqlDbType.VarChar, 300)
                    sqlpSJL_NARRATION.Value = Session("dtJournal").Rows(iIndex)("Narration") & ""
                    cmd.Parameters.Add(sqlpSJL_NARRATION)

                    Dim sqlpSJL_CUR_ID As New SqlParameter("@SJL_CUR_ID", SqlDbType.VarChar, 12)
                    sqlpSJL_CUR_ID.Value = DDCurrency.SelectedItem.Text & ""
                    cmd.Parameters.Add(sqlpSJL_CUR_ID)

                    Dim sqlpSJL_EXGRATE1 As New SqlParameter("@SJL_EXGRATE1", SqlDbType.Decimal, 8)
                    sqlpSJL_EXGRATE1.Value = DDCurrency.SelectedItem.Value.Split("__")(0).Trim & ""
                    cmd.Parameters.Add(sqlpSJL_EXGRATE1)

                    Dim sqlpSJL_EXGRATE2 As New SqlParameter("@SJL_EXGRATE2", SqlDbType.Decimal, 8)
                    sqlpSJL_EXGRATE2.Value = DDCurrency.SelectedItem.Value.Split("__")(2).Trim & ""
                    cmd.Parameters.Add(sqlpSJL_EXGRATE2)

                    Dim sqlpSJL_ACT_ID As New SqlParameter("@SJL_ACT_ID", SqlDbType.VarChar, 20)
                    sqlpSJL_ACT_ID.Value = Session("dtJournal").Rows(iIndex)("Accountid") & ""
                    cmd.Parameters.Add(sqlpSJL_ACT_ID)

                    Dim sqlpSJL_RECONDT As New SqlParameter("@SJL_RECONDT", SqlDbType.DateTime, 30)
                    sqlpSJL_RECONDT.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpSJL_RECONDT)

                    Dim sqlpSJL_CHQNO As New SqlParameter("@SJL_CHQNO", SqlDbType.VarChar, 20)
                    sqlpSJL_CHQNO.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpSJL_CHQNO)

                    Dim sqlpSJL_CHQDT As New SqlParameter("@SJL_CHQDT", SqlDbType.DateTime, 30)
                    sqlpSJL_CHQDT.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpSJL_CHQDT)

                    Dim sqlpSJL_CREDIT As New SqlParameter("@SJL_CREDIT", SqlDbType.Decimal)
                    sqlpSJL_CREDIT.Value = Session("dtJournal").Rows(iIndex)("Credit")
                    cmd.Parameters.Add(sqlpSJL_CREDIT)

                    Dim sqlpSJL_DEBIT As New SqlParameter("@SJL_DEBIT", SqlDbType.Decimal, 8)
                    sqlpSJL_DEBIT.Value = Session("dtJournal").Rows(iIndex)("Debit")
                    cmd.Parameters.Add(sqlpSJL_DEBIT)

                    Dim sqlpbSJL_BDELETED As New SqlParameter("@SJL_BDELETED", SqlDbType.Bit)
                    sqlpbSJL_BDELETED.Value = False
                    cmd.Parameters.Add(sqlpbSJL_BDELETED)

                    Dim sqlpbSJL_SLNO As New SqlParameter("@SJL_SLNO", SqlDbType.Int)
                    sqlpbSJL_SLNO.Value = iIndex + 1 - ViewState("iDeleteCount")
                    cmd.Parameters.Add(sqlpbSJL_SLNO)

                    Dim sqlpSJL_BDISCOUNTED As New SqlParameter("@SJL_BDISCOUNTED", SqlDbType.Bit)
                    sqlpSJL_BDISCOUNTED.Value = False
                    cmd.Parameters.Add(sqlpSJL_BDISCOUNTED)

                    Dim sqlpSJL_REFDOCTYPE As New SqlParameter("@SJL_REFDOCTYPE", SqlDbType.VarChar, 20)
                    sqlpSJL_REFDOCTYPE.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpSJL_REFDOCTYPE)

                    Dim sqlpbSJL_REFDOCNO As New SqlParameter("@SJL_REFDOCNO", SqlDbType.Bit)
                    sqlpbSJL_REFDOCNO.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpbSJL_REFDOCNO)

                    Dim sqlpSJL_OPP_ACT_ID As New SqlParameter("@SJL_OPP_ACT_ID", SqlDbType.VarChar, 10)
                    If Session("dtJournal").Rows(iIndex)("Debit") > 0 Then
                        sqlpSJL_OPP_ACT_ID.Value = str_dr_opposite
                    Else
                        sqlpSJL_OPP_ACT_ID.Value = str_cr_opposite
                    End If

                    cmd.Parameters.Add(sqlpSJL_OPP_ACT_ID)

                    Dim sqlpSJL_ACTVOUCHERDT As New SqlParameter("@SJL_ACTVOUCHERDT", SqlDbType.VarChar, 10)
                    sqlpSJL_ACTVOUCHERDT.Value = System.DBNull.Value
                    cmd.Parameters.Add(sqlpSJL_ACTVOUCHERDT)

                    Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    If viewstate("datamode") = "edit" Then
                        sqlpbEdit.Value = True
                    Else
                        sqlpbEdit.Value = False
                    End If
                    cmd.Parameters.Add(sqlpbEdit)

                    Dim sqlpbLastRec As New SqlParameter("@bLastRec", SqlDbType.Bit)
                    If iIndex = Session("dtJournal").Rows.Count - 1 Then
                        sqlpbLastRec.Value = True
                    Else
                        sqlpbLastRec.Value = False
                    End If
                    cmd.Parameters.Add(sqlpbLastRec)

                    'Dim sqlpbEdit As New SqlParameter("@bEdit", SqlDbType.Bit)
                    'sqlpbEdit.Value = False
                    'cmd.Parameters.Add(sqlpbEdit)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    'Dim sqlopJHD_NEWDOCNO As New SqlParameter("@JHD_NEWDOCNO", SqlDbType.VarChar, 20)
                    'cmd.Parameters.Add(sqlopJHD_NEWDOCNO)
                    'cmd.Parameters("@JHD_NEWDOCNO").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    Dim success_msg As String = ""
                    If iReturnvalue <> 0 Then
                        Exit For
                    End If
                    cmd.Parameters.Clear()
                Else
                    If Not Session("dtJournal").Rows(iIndex)("GUID") Is System.DBNull.Value Then
                        viewstate("iDeleteCount") = viewstate("iDeleteCount") + 1
                        cmd = New SqlCommand("DeleteSJOURNAL_D", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier, 20)
                        sqlpGUID.Value = Session("dtJournal").Rows(iIndex)("GUID")
                        cmd.Parameters.Add(sqlpGUID)

                        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                        retValParam.Direction = ParameterDirection.ReturnValue
                        cmd.Parameters.Add(retValParam)

                        cmd.ExecuteNonQuery()
                        iReturnvalue = retValParam.Value
                        Dim success_msg As String = ""

                        If iReturnvalue <> 0 Then
                            lblError.Text = getErrorMessage(iReturnvalue)
                        End If
                        cmd.Parameters.Clear()
                    End If
                End If
            Next
            If iIndex <= Session("dtJournal").Rows.Count - 1 Then
                Return iReturnvalue
            Else
                Return iReturnvalue
            End If
            'Adding transaction info
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function

    Function DoTransactions_Sub_Table(ByVal objConn As SqlConnection, _
      ByVal stTrans As SqlTransaction, ByVal p_docno As String, _
      ByVal p_voucherid As String, ByVal p_crdr As String, _
      ByVal p_slno As Integer, ByVal p_accountid As String, ByVal p_amount As String) As String
        Dim iReturnvalue As Integer
        Dim str_cur_cost_center As String = ""
        Dim str_prev_cost_center As String = ""
        'Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim iLineid As Integer

        Dim str_balanced As Boolean = True

        For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid Then

                If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") Then
                    iLineid = -1
                    str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                End If
                iLineid = iLineid + 1
                If str_balanced = False Then
                    'tr_errLNE.Visible = True
                    lblError.Text = " Cost center allocation not balanced"
                    iReturnvalue = 511
                    Exit For
                End If
                str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                Dim bEdit As Boolean
                If Session("datamode") = "add" Then
                    bEdit = False
                Else
                    bEdit = True
                End If
                Dim VDS_ID_NEW As String = String.Empty
                iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_S_NEW(objConn, stTrans, p_docno, p_crdr, p_slno, _
                p_accountid, Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), "SJV", _
                Session("dtCostChild").Rows(iIndex)("ERN_ID"), Session("dtCostChild").Rows(iIndex)("Amount"), _
                Session("dtCostChild").Rows(iIndex)("costcenter"), Session("dtCostChild").Rows(iIndex)("Memberid"), _
                Session("dtCostChild").Rows(iIndex)("Name"), Session("dtCostChild").Rows(iIndex)("SubMemberid"), _
                txtHDocdate.Text, Session("dtCostChild").Rows(iIndex)("MemberCode"), bEdit, VDS_ID_NEW)
                If iReturnvalue <> 0 Then Return iReturnvalue
                If Session("CostAllocation").Rows.Count > 0 Then
                    Dim subLedgerTotal As Decimal = 0
                    Session("CostAllocation").DefaultView.RowFilter = " (CostCenterID = '" & Session("dtCostChild").Rows(iIndex)("id") & "' )"
                    For iLooVar As Integer = 0 To Session("CostAllocation").DefaultView.ToTable.Rows.Count - 1
                        'If Session("dtCostChild").Rows(iIndex)("id") = Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("CostCenterID") Then
                        iReturnvalue = CostCenterFunctions.SaveVOUCHER_D_SUB_ALLOC(objConn, stTrans, p_docno, iLooVar, p_accountid, 0, Session("SUB_ID"), _
                                              Session("sBsuid"), Session("F_YEAR"), "JV", "", _
                                              Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount"), VDS_ID_NEW, Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("ASM_ID"))
                        If iReturnvalue <> 0 Then
                            Session("CostAllocation").DefaultView.RowFilter = ""
                            Return iReturnvalue
                        End If
                        'End If
                        subLedgerTotal += Session("CostAllocation").DefaultView.ToTable.Rows(iLooVar)("Amount")
                    Next
                    If subLedgerTotal > 0 And Session("dtCostChild").Rows(iIndex)("Amount") <> subLedgerTotal Then Return 411
                    Session("CostAllocation").DefaultView.RowFilter = ""
                End If
                If iReturnvalue <> 0 Then Exit For
            End If
        Next
        Return iReturnvalue
    End Function

    Function check_cost_child(ByVal p_voucherid As String, ByVal p_costid As String, ByVal p_total As Double) As Boolean
        Dim dTotal As Double = 0
        For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
            If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid Then
                dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
            End If
        Next
        If Math.Round(dTotal, 3) = Math.Round(p_total, 3) Then
            Return True
        Else
            Return False
        End If

    End Function


    Function check_others(ByVal p_voucherid As String, ByVal p_total As String) As Boolean
        Try
            Dim dTotal As Double = 0
            For iIndex As Integer = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("voucherid") = p_voucherid And Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    dTotal = dTotal + Session("dtCostChild").Rows(iIndex)("amount")
                End If
            Next
            If dTotal = p_total Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function


    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM SJOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" _
            & " AND SHD_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then


                'txtHNarration.Text = ds.Tables(0).Rows(0)("SHD_NARRATION")
                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("SHD_DOCNO") & "|" _
                   & ds.Tables(0).Rows(0)("SHD_SUB_ID") & "|" _
                   & ds.Tables(0).Rows(0)("SHD_DOCDT") & "|" _
                   & ds.Tables(0).Rows(0)("SHD_FYEAR") & "|"
                    txtHDocno.Text = ds.Tables(0).Rows(0)("SHD_DOCNO") & ""
                    Dim cmd As New SqlCommand("LockSJOURNAL_H", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    'If ds.Tables(0).Rows(0)("bPosted") = True Then
                    '    Return 1000
                    'End If
                    Dim sqlpSHD_SUB_ID As New SqlParameter("@SHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpSHD_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpSHD_SUB_ID)

                    Dim sqlpsqlpSHD_BSU_ID As New SqlParameter("@SHD_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpSHD_BSU_ID.Value = Session("sBsuid") & ""
                    cmd.Parameters.Add(sqlpsqlpSHD_BSU_ID)

                    Dim sqlpSHD_FYEAR As New SqlParameter("@SHD_FYEAR", SqlDbType.Int)
                    sqlpSHD_FYEAR.Value = Session("F_YEAR") & ""
                    cmd.Parameters.Add(sqlpSHD_FYEAR)

                    Dim sqlpSHD_DOCTYPE As New SqlParameter("@SHD_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpSHD_DOCTYPE.Value = "SJV"
                    cmd.Parameters.Add(sqlpSHD_DOCTYPE)

                    Dim sqlpSHD_DOCNO As New SqlParameter("@SHD_DOCNO", SqlDbType.VarChar, 20)
                    sqlpSHD_DOCNO.Value = viewstate("str_editData").Split("|")(0)
                    cmd.Parameters.Add(sqlpSHD_DOCNO)

                    Dim sqlpSHD_DOCDT As New SqlParameter("@SHD_DOCDT", SqlDbType.DateTime, 30)
                    sqlpSHD_DOCDT.Value = ds.Tables(0).Rows(0)("SHD_DOCDT") & ""
                    cmd.Parameters.Add(sqlpSHD_DOCDT)

                    Dim sqlpSHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpSHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpSHD_CUR_ID)

                    Dim sqlpSHD_USER As New SqlParameter("@SHD_USER", SqlDbType.VarChar, 50)
                    sqlpSHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpSHD_USER)

                    Dim sqlopSHD_TIMESTAMP As New SqlParameter("@SHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    cmd.Parameters.Add(sqlopSHD_TIMESTAMP)
                    cmd.Parameters("@SHD_TIMESTAMP").Direction = ParameterDirection.Output

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = getErrorMessage(iReturnvalue)
                        Return iReturnvalue
                    End If
                    ViewState("str_timestamp") = sqlopSHD_TIMESTAMP.Value
                    Return iReturnvalue

                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
                viewstate("str_editData") = ""
                viewstate("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            viewstate("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
            'Return " | | "
        End Try
        Return True
    End Function


    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
            & " JHD_DOCNO='" & viewstate("str_editData").Split("|")(0) & "'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("JHD_DOCDT")
                txtHDocno.Text = viewstate("str_editData").Split("|")(0)
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")
                Dim objConn As New SqlConnection(str_conn)

                Try
                    objConn.Open()
                    Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("Sub_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                    sqlpsqlpBSUID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpBSUID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = ds.Tables(0).Rows(0)("JHD_DOCTYPE") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                    sqlpJHD_CUR_ID.Value = Session.SessionID
                    cmd.Parameters.Add(sqlpJHD_CUR_ID)

                    Dim sqlpJHD_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                    sqlpJHD_USER.Value = Session("sUsr_name")
                    cmd.Parameters.Add(sqlpJHD_USER)

                    Dim sqlopJHD_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                    sqlopJHD_TIMESTAMP.Value = viewstate("str_timestamp")
                    cmd.Parameters.Add(sqlopJHD_TIMESTAMP)

                    Dim iReturnvalue As Integer
                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()

                    iReturnvalue = retValParam.Value
                    If iReturnvalue <> 0 Then
                        lblError.Text = (iReturnvalue)
                    End If
                    Return iReturnvalue
                Catch ex As Exception
                    Errorlog(ex.Message)
                Finally
                    objConn.Close()
                End Try
            Else
            End If
            Return " | | "
        Catch ex As Exception
            Errorlog(ex.Message)
            Return " | | "
        End Try
        Return True
    End Function


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        tbl_Details.Visible = False
        Dim str_ As String = lock()
        If str_ <> "0" Then
            If str_.Length = 3 Then
                lblError.Text = getErrorMessage(str_)
            Else
                lblError.Text = "Did not get lock"
            End If
        Else
            h_editorview.Value = "Edit"
            ResetViewData()
            viewstate("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
            btnSave.Enabled = True
        End If
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If viewstate("datamode") = "add" Or viewstate("datamode") = "edit" Then
            h_editorview.Value = ""
            If ViewState("datamode") = "edit" Then
                unlock()
            End If
            setViewData()
            'clear_All()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
        Else
            Response.Redirect(viewstate("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        objConn.Open()
        'str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
        '       & " GUID='" & Request.QueryString("viewid") & "'"
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        'Try
        '    ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
        '    If ds.Tables(0).Rows.Count > 0 Then

        '        Dim cmd As New SqlCommand("DeleteJOURNAL_H", objConn, stTrans)
        '        cmd.CommandType = CommandType.StoredProcedure

        '        Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
        '        sqlpDOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
        '        cmd.Parameters.Add(sqlpDOCNO)

        '        Dim sqlpJHD_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
        '        sqlpJHD_SUB_ID.Value = ds.Tables(0).Rows(0)("JHD_SUB_ID") & ""
        '        cmd.Parameters.Add(sqlpJHD_SUB_ID)

        '        Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@JHD_BSU_ID", SqlDbType.VarChar, 20)
        '        sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
        '        cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

        '        Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        '        sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
        '        cmd.Parameters.Add(sqlpJHD_FYEAR)

        '        Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
        '        sqlpDOCTYPE.Value = "SJV"
        '        cmd.Parameters.Add(sqlpDOCTYPE)

        '        Dim iReturnvalue As Integer
        '        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
        '        retValParam.Direction = ParameterDirection.ReturnValue
        '        cmd.Parameters.Add(retValParam)
        '        cmd.ExecuteNonQuery()
        '        iReturnvalue = retValParam.Value

        '        If iReturnvalue <> 0 Then
        '            lblError.Text = getErrorMessage(iReturnvalue)
        '            stTrans.Rollback()
        '        Else
        '            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
        '            If flagAudit <> 0 Then
        '                Throw New ArgumentException("Could not process your request")
        '            End If
        '            stTrans.Commit()
        '            lblError.Text = getErrorMessage("516")
        '            viewstate("datamode") = Encr_decrData.Encrypt("view")
        '            clear_All()
        '            Response.Redirect("accjvViewJournalVoucher.aspx?deleted=done" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
        '        End If
        '    End If
        'Catch ex As Exception
        '    stTrans.Rollback()
        '    Errorlog(ex.Message)
        'Finally
        '    objConn.Close()
        'End Try
        str_Sql = "SELECT * FROM SJOURNAL_H WHERE" _
               & " GUID='" & Request.QueryString("viewid") & "'"
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim cmd As New SqlCommand("DeleteSJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("SHD_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = "SJV"
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpJHD_FYEAR As New SqlParameter("@SHD_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("SHD_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@SHD_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = ds.Tables(0).Rows(0)("SHD_BSU_ID") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value

                If iReturnvalue <> 0 Then
                    lblError.Text = getErrorMessage(iReturnvalue)
                    stTrans.Rollback()
                Else
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtHDocno.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    stTrans.Commit()
                    lblError.Text = getErrorMessage("906")
                    'lblError.Text = "Successfully deleted"
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    clear_All()

                End If

            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
        'Response.Redirect("accjvViewJournalVoucher.aspx?deleted=done" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
    End Sub


    Private Function set_viewdata() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM SJOURNAL_H WHERE" _
            & " GUID='" & Request.QueryString("viewid") & "'" ' _
            If Request.QueryString("BSUID") Is Nothing Then
                str_Sql += " AND SHD_BSU_ID='" & Session("sBsuid") & "'"
            Else
                str_Sql += " AND SHD_BSU_ID='" & Request.QueryString("BSUID") & "'"
            End If
            '& " AND SHD_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = ds.Tables(0).Rows(0)("SHD_DOCDT")
                txtHDocno.Text = ds.Tables(0).Rows(0)("SHD_DOCNO")
                txtHNarration.Text = ds.Tables(0).Rows(0)("SHD_NARRATION")

                ViewState("str_editData") = ds.Tables(0).Rows(0)("SHD_DOCNO") & "|" _
               & ds.Tables(0).Rows(0)("SHD_SUB_ID") & "|" _
               & ds.Tables(0).Rows(0)("SHD_DOCDT") & "|" _
               & ds.Tables(0).Rows(0)("SHD_FYEAR") & "|"
                Return ""
            Else
                viewstate("str_editData") = ""
                viewstate("datamode") = Encr_decrData.Encrypt("view")
                Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & viewstate("datamode"))
            End If
            Return ""
        Catch ex As Exception
            Errorlog(ex.Message)
            viewstate("datamode") = Encr_decrData.Encrypt("view")
            Response.Redirect("accjvJournalvoucher.aspx?invalidedit=1" & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
        End Try
        Return True
    End Function


    Protected Sub lbAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub


    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If viewstate("datamode") = "edit" Then
            unlock()
        End If
        ResetViewData()
        clear_All()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))
    End Sub


    Private Function check_Errors() As String
        Dim str_Error As String = ""
        Dim str_Err As String = ""
        Try
            'Adding transaction info
            Dim iIndex As Integer
            Dim dTotal As Double = 0
            For iIndex = 0 To Session("dtJournal").Rows.Count - 1
                Dim str_manndatory_costcenter As String
                If Convert.ToBoolean(Session("dtJournal").Rows(iIndex)("required")) = True Then
                    str_manndatory_costcenter = Session("dtJournal").Rows(iIndex)("Costcenter")
                Else
                    str_manndatory_costcenter = ""
                End If
                If Session("dtJournal").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If Session("dtJournal").Rows(iIndex)("Credit") > 0 Then
                        str_Err = check_Errors_sub(Session("dtJournal").Rows(iIndex)("id"), _
                         Session("dtJournal").Rows(iIndex)("Credit"), str_manndatory_costcenter)
                    Else
                        str_Err = check_Errors_sub(Session("dtJournal").Rows(iIndex)("id"), _
                         Session("dtJournal").Rows(iIndex)("Debit"), str_manndatory_costcenter)
                    End If

                    If str_Err <> "" Then
                        str_Error = str_Error & "<br/>" & str_Err & " At Line - " & iIndex + 1
                    End If
                End If
            Next
            'Adding transaction info
            Return str_Error
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
        Return ""
    End Function


    Private Function check_Errors_sub(ByVal p_voucherid As String, ByVal p_amount As String, _
    Optional ByVal p_mandatory_costcenter As String = "") As String
        Try
            Dim str_cur_cost_center As String = ""
            Dim str_prev_cost_center As String = ""
            'Dim dTotal As Double = 0
            Dim iIndex As Integer
            Dim iLineid As Integer
            Dim bool_check_other, bool_mandatory_exists As Boolean

            Dim str_err As String = ""
            If p_mandatory_costcenter <> "" Then
                bool_mandatory_exists = False
            Else
                bool_mandatory_exists = True
            End If
            Dim str_balanced As Boolean = True
            bool_check_other = False
            For iIndex = 0 To Session("dtCostChild").Rows.Count - 1
                If Session("dtCostChild").Rows(iIndex)("VoucherId") = p_voucherid And Session("dtCostChild").Rows(iIndex)("Status") & "" <> "Deleted" Then
                    If Session("dtCostChild").Rows(iIndex)("costcenter") = p_mandatory_costcenter Then
                        bool_mandatory_exists = True
                    End If
                    If str_prev_cost_center <> Session("dtCostChild").Rows(iIndex)("costcenter") And Not Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value Then
                        iLineid = -1
                        str_balanced = check_cost_child(p_voucherid, Session("dtCostChild").Rows(iIndex)("costcenter"), p_amount)
                        If str_balanced = False Then
                            str_err = str_err & " <BR> Invalid Allocation for cost center " & AccountFunctions.get_cost_center(Session("dtCostChild").Rows(iIndex)("costcenter"))
                        End If
                    End If
                    str_balanced = True
                    If Session("dtCostChild").Rows(iIndex)("memberid") Is System.DBNull.Value And bool_check_other = False Then
                        iLineid = -1
                        bool_check_other = True
                        str_balanced = check_others(p_voucherid, p_amount)
                        If str_balanced = False Then
                            If str_err = "" Then
                                str_err = "Invalid Allocation for other cost center"
                            Else
                                str_err = str_err & " <BR> Invalid Allocation for other cost centers "
                            End If

                        End If
                    End If
                    iLineid = iLineid + 1
                    'If str_balanced = False Then
                    '    Exit For
                    'End If
                    str_prev_cost_center = Session("dtCostChild").Rows(iIndex)("costcenter")
                End If
            Next
            If bool_mandatory_exists = False Then
                str_err = "<br>Mandatory Cost center - " & AccountFunctions.get_cost_center(p_mandatory_costcenter) & " not allocated"
            End If
            Return str_err
        Catch ex As Exception
            Errorlog(ex.Message, "child")
            Return ""
        End Try
    End Function


    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim strFilter As String = String.Empty
        'SELECT * FROM vw_OSA_JOURNAL where FYEAR, DOCTYPE, DOCNO, DOCDT
        strFilter = "BSU_ID = '" & Session("SBSUID") & "' and FYEAR = " & Session("F_YEAR") & " and DOCTYPE = 'SJV' and SHD_DOCNO = '" & txtHDocno.Text & "' and SHD_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(txtHDocdate.Text)) & "'"
        str_Sql = "SELECT * FROM vw_OSA_SJOURNAL where " + strFilter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.Text

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("Summary") = chkSummary.Checked
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(Session("SUB_ID"), "SJV", txtHDocno.Text)
            params("VoucherName") = "Self Reversing Journal Voucher"
            params("reportHeading") = "Self Reversing Journal Voucher"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.IncludeBSUImage = True

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 0
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(0)
            Else
                subReportLength = repSourceSubRep.Length
                ReDim Preserve repSourceSubRep(subReportLength)
            End If
            repSource.SubReport = repSourceSubRep

            repSourceSubRep(subReportLength) = New MyReportClass
            Dim cmdNarrationDetails As New SqlCommand
            cmdNarrationDetails.CommandText = "SELECT SJOURNAL_D.SJL_SUB_ID AS VHH_SUB_ID, SJL_BSU_ID AS VHH_BSU_ID, " & _
            " SJOURNAL_D.SJL_FYEAR AS VHH_FYEAR, SJOURNAL_D.SJL_DOCTYPE AS VHH_DOCTYPE, " & _
            " SJOURNAL_D.SJL_DOCNO AS VHH_DOCNO, '' AS VHD_LINEID, " & _
            " SJOURNAL_D.SJL_NARRATION AS VHD_NARRATION FROM SJOURNAL_D " & _
            " INNER JOIN SJOURNAL_H ON SJOURNAL_D.SJL_SUB_ID = SJOURNAL_H.SHD_SUB_ID " & _
            " AND SJOURNAL_D.SJL_BSU_ID = SJOURNAL_H.SHD_BSU_ID AND " & _
            " SJOURNAL_D.SJL_FYEAR = SJOURNAL_H.SHD_FYEAR AND " & _
            " SJOURNAL_D.SJL_DOCTYPE = SJOURNAL_H.SHD_DOCTYPE AND " & _
            " SJOURNAL_D.SJL_DOCNO = SJOURNAL_H.SHD_DOCNO WHERE " + strFilter.Replace("BSU_ID", "SHD_BSU_ID").Replace("FYEAR", "SJL_FYEAR").Replace("DOCTYPE", "SJL_DOCTYPE")
            cmdNarrationDetails.Connection = New SqlConnection(str_conn)
            cmdNarrationDetails.CommandType = CommandType.Text
            repSourceSubRep(subReportLength).Command = cmdNarrationDetails

            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/SJVoucherReport.rpt"
            Session("ReportSource") = repSource
            'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub imgCaledar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCalendar.Click
        bind_Currency()
        getnextdocid()
    End Sub

    Protected Sub gvCostchild_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIdCostchild As New Label
            lblIdCostchild = e.Row.FindControl("lblIdCostchild")
            Dim gvCostAllocation As New GridView
            gvCostAllocation = e.Row.FindControl("gvCostAllocation")
            If Not Session("CostAllocation") Is Nothing AndAlso Not gvCostAllocation Is Nothing Then
                gvCostAllocation.Attributes.Add("bordercolor", "#fc7f03")
                Dim dv As New DataView(Session("CostAllocation"))
                If Session("CostAllocation").Rows.Count > 0 Then
                    dv.RowFilter = "CostCenterID='" & lblIdCostchild.Text & "' "
                End If
                dv.Sort = "CostCenterID"
                gvCostAllocation.DataSource = dv.ToTable
                gvCostAllocation.DataBind()
            End If
        End If
    End Sub

    Protected Sub lbUploadEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUploadEmployee.Click
        If Not fuEmployeeData.HasFile Then
            lblError.Text = "Please select the file...!"
            Exit Sub
        End If
        If Not (fuEmployeeData.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase) Or fuEmployeeData.FileName.EndsWith("csv", StringComparison.OrdinalIgnoreCase)) Then
            lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
            Exit Sub
        End If
        UpLoadDBF()
    End Sub

    Private Sub UpLoadDBF()
        RecreateSsssionDataSource()
        CostCenterFunctions.UploadAndPopulateEmployeeDataFromExcel(Session(usrCostCenter1.SessionDataSource_1.SessionKey), Session(usrCostCenter1.SessionDataSource_2.SessionKey), Session("sBSuid"), _
                     CostCenterIDRadComboBoxMain.SelectedValue, CostCenterIDRadComboBoxMain.Text, txtHDocdate.Text, fuEmployeeData, Session("sUsr_name"))
        usrCostCenter1.BindCostCenter()
    End Sub

    Sub RecreateSsssionDataSource()
        If Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_VOUCHER_D_S WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_1.SessionKey) = ds.Tables(0)
        End If
        If Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT * FROM VW_OSA_ACCOUNTS_SUB_ACC_M WHERE 1=2")
            Session(usrCostCenter1.SessionDataSource_2.SessionKey) = ds1.Tables(0)
        End If
    End Sub

    Sub ClearRadGridandCombo()
        If Not Session(usrCostCenter1.SessionDataSource_1.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_1.SessionKey).AcceptChanges()
        End If
        If Not Session(usrCostCenter1.SessionDataSource_2.SessionKey) Is Nothing Then
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).Rows.Clear()
            Session(usrCostCenter1.SessionDataSource_2.SessionKey).AcceptChanges()
        End If
        usrCostCenter1.BindCostCenter()

        CostCenterIDRadComboBoxMain.Text = ""
        CostCenterIDRadComboBoxMain.ClearSelection()
        CostCenterIDRadComboBoxMain.DataBind()
    End Sub


    Protected Sub txtDAccountCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDAccountCode.TextChanged
        txtDAccountName.Text = AccountFunctions.Validate_Account(txtDAccountCode.Text, Session("sbsuid"), "NORMAL")
        If txtDAccountName.Text = "" Then
            lblError.Text = "Invalid Account Selected in Details"
            txtDAccountCode.Focus()
        Else
            lblError.Text = ""
            txtDAmount.Focus()
        End If
        ClearRadGridandCombo()
    End Sub
End Class
