Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class NewSubLedger
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblError.Text = ""
            Page.Title = OASISConstants.Gemstitle
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150001"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = "none"
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A100085") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
           
            If Request.QueryString("editid") = "" Then
                If ViewState("datamode") <> "add" Then
                    FillLinkAccountList()
                    LockYN(False)
                End If
            Else
                ' txtSubLedgerCode.Attributes.Add("readonly", "readonly")
                LockYN(False)
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim encObj As New Encryption64
                setModifyvalues(encObj.Decrypt(Request.QueryString("editid").Replace(" ", "+")))
            End If
            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
                'h_BSUID.Value = ""
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        
    End Sub
    Sub LockYN(ByVal lockYN As Boolean)
        txtSubLedgerCode.ReadOnly = Not lockYN Or ViewState("datamode") = "edit"
        txtSubLedgerName.ReadOnly = Not lockYN
        grdACTDetails.Enabled = lockYN
        btnSave.Enabled = lockYN
    End Sub

    Private Sub FillLinkAccountList(Optional ByVal ASM_ID As String = "")
        Dim ds1 As New DataSet
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim lstrSQL As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        lstrSQL = " select a.ACT_ID,a.ACT_NAME,sl.ASL_bDefault IsDefault "
        lstrSQL = lstrSQL & " from   ACCOUNTS_M a INNER JOIN ACCOUNTS_SUB_LINK sl ON a.ACT_ID=sl.ASL_ACT_ID "
        If ASM_ID <> "" Then lstrSQL = lstrSQL & " and sl.ASL_ASM_ID='" & ASM_ID & "'"
        'lstrSQL = lstrSQL & " where ACT_bactive = 1 "
        lstrSQL = lstrSQL & " order by  case when sl.ASL_ACT_ID is not null then 1 else 0 end desc"
        ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, lstrSQL)
        Dim row As DataRow
        h_ACTIDs.Value = ""
        For Each row In ds1.Tables(0).Rows
            h_ACTIDs.Value += "||" + row("ACT_ID")
        Next
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ACCOUNTS_SUB_ACC_M where ASM_ID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtSubLedgerCode.Text = p_Modifyid
                txtSubLedgerName.Text = ds.Tables(0).Rows(0)("ASM_NAME")
                Dim s1 As Boolean = ds.Tables(0).Rows(0)("ASM_Bactive")
                If s1 = True Then
                    chkActive.Checked = True
                Else
                    chkActive.Checked = False
                End If
                FillLinkAccountList(p_Modifyid)
            Else
                Response.Redirect("accSubLedgerMaster.aspx")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Call clear_all()
        LockYN(True)
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Sub clear_all()
        txtSubLedgerCode.Text = ""
        txtSubLedgerName.Text = ""
        chkActive.Checked = False
        grdACTDetails.DataSource = Nothing
        grdACTDetails.DataBind()
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        LockYN(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            If ViewState("datamode") = "add" Then Call clear_all()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub
    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
    End Sub
    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If
    End Sub

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        If LCase(ViewState("datamode")) = "view" Then Exit Sub
        h_ACTIDs.Value += "||" + txtAccNames.Text.Replace(",", "||")
        FillACTIDs(h_ACTIDs.Value)
    End Sub
    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID,cast(0 as bit) IsDefault FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"
            str_Sql = "select a.ACT_ID,a.ACT_NAME,isnull(sl.ASL_bDefault,0) IsDefault  "
            str_Sql = str_Sql & " from   ACCOUNTS_M a LEFT OUTER JOIN ACCOUNTS_SUB_LINK sl ON a.ACT_ID=sl.ASL_ACT_ID  "
            str_Sql = str_Sql & " and sl.ASL_ASM_ID='" & txtSubLedgerCode.Text & "' "
            str_Sql = str_Sql & "  WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ArrLinkedAcc(0, 1), tmpArrLinkedAcc(0) As String
        Dim mDataTable As New DataTable
        ReDim ArrLinkedAcc((grdACTDetails.Rows.Count * grdACTDetails.PageCount) - 1, 1)
        Dim grdViewRow As GridViewRow
        Dim i As Int16 = 0
        For Each grdViewRow In grdACTDetails.Rows
            If grdViewRow.RowState = DataControlRowState.Alternate Or grdViewRow.RowState = DataControlRowState.Normal Then
                ArrLinkedAcc(i, 0) = grdViewRow.Cells(0).Text
                ArrLinkedAcc(i, 1) = CType(grdViewRow.FindControl("chkACCTIsDefault"), CheckBox).Checked
                i += 1
            End If
        Next
        If ViewState("datamode") = "edit" Then
            Dim str_success As String = MasterFunctions.fnDoSubLedgerOperations("Update", txtSubLedgerCode.Text & "", txtSubLedgerName.Text & "", chkActive.Checked, ArrLinkedAcc)
            If (str_success = "0") Then
                'lblMError.Text = displayMessage("258", 20, "D")
                Dim encObj As New Encryption64
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSubLedgerCode.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                Response.Redirect("accSubLedgerMaster.aspx?modified=" & encObj.Encrypt("1022") & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))
            Else
                lblError.Text = getErrorMessage(str_success)
            End If
        Else
            lblError.Text = ""
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Try
                Dim str_success As String = MasterFunctions.fnDoSubLedgerOperations("Insert", txtSubLedgerCode.Text & "", txtSubLedgerName.Text & "", chkActive.Checked, ArrLinkedAcc)
                If (str_success = "0") Then
                    Dim encObj As New Encryption64
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSubLedgerCode.Text, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If

                    Response.Redirect("accSubLedgerMaster.aspx?modified=" & encObj.Encrypt("1023") & "&newid=" & txtSubLedgerCode.Text & "&MainMnu_code=" & Request.QueryString("MainMnu_code"))
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            lblError.Text = ""
            If Not Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) Is Nothing Then
                Dim str_success As String = MasterFunctions.fnDoSubLedgerOperations("Delete", Encr_decrData.Decrypt(Request.QueryString("editid").Replace(" ", "+")) & "", "", chkActive.Checked, Nothing)
                If (str_success = "0") Then
                    lblError.Text = getErrorMessage("1024")
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtSubLedgerCode.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    clear_all()
                Else
                    lblError.Text = getErrorMessage(str_success)
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub h_ACTIDs_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles h_ACTIDs.ValueChanged
        If LCase(ViewState("datamode")) = "view" Then Exit Sub
        FillACTIDs(h_ACTIDs.Value)
    End Sub
End Class
