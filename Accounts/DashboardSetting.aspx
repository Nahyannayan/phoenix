﻿<%@ Page Language="VB" AutoEventWireup="true" MasterPageFile="~/mainMasterPage.master" CodeFile="DashboardSetting.aspx.vb" Inherits="Accounts_DashboardSetting" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .ajax__tab_xp .ajax__tab_header {
            font-family: inherit !important;
            font-size: inherit !important;
        }

            .ajax__tab_xp .ajax__tab_header .ajax__tab_tab {
                height: auto !important;
                padding: inherit !important;
                background-image: none !important;
            }

        .ajax__tab_default .ajax__tab_tab {
            overflow: inherit !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_outer {
            padding-right: inherit !important;
            background-image: none !important;
            height: inherit !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_outer {
            background-image: none !important;
            margin-right: 2px;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_inner {
            background-image: none !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_inner {
            background-image: none !important;
        }

        .ajax__tab_inner {
            border: 1px solid #808080;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
            padding: 6px 4px !important;
            background-color: #efefef !important;
        }

            .ajax__tab_inner:hover {
                background-color: #cecece !important;
            }

        .ajax__tab_active {
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_tab {
            background-image: none !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_hover .ajax__tab_tab {
            background-image: none !important;
        }
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            DashBoard Settings
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div align="left">
                    <asp:Label ID="lblError" runat="server" CssClass="error"> </asp:Label>
                </div>
                <ajaxToolkit:TabContainer runat="server" ID="tabForm">
                    <ajaxToolkit:TabPanel runat="server" ID="panel1" HeaderText="Add Dashboard">
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Dashboard Name</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtdshbrdnm" runat="server" placeholder="Enter dashboard name.."></asp:TextBox>
                                    </td>
                                    <td width="20%" align="left">
                                        <span class="field-label">Category</span>
                                    </td>
                                    <td width="30%" align="left">

                                        <asp:CheckBoxList runat="server" ID="listCategory" CssClass="field-label" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="M" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="L" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="XL" Value="3"></asp:ListItem>
                                        </asp:CheckBoxList>

                                    </td>
                                </tr>

                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">Modules</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="lstModule" runat="server">
                                                <asp:ListItem Text="Student Management" Value="S0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Curriculum Management" Value="C0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Co-Curricular Activities" Value="CC">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Library Management" Value="LB">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="ACe" Value="SN">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Health Records" Value="MD">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Fees Management" Value="F0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Payroll" Value="P0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Transport Management" Value="T0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Financial Accounting" Value="A0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Purchase" Value="PI">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="System Administration" Value="D0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Messaging" Value="M0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="HR" Value="H0">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Professional Development" Value="CD">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Facility & Service Management" Value="FD">                                   
                                                </asp:ListItem>
                                                <asp:ListItem Text="Employee Self Service" Value="SS">                                   
                                                </asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td width="20%" align="left">
                                        <span class="field-label">Type</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList runat="server" ID="ddltype"></asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkdrldwn" Text="Do you need drill down" CssClass="field-label" AutoPostBack="true" />
                                    </td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkpage" Text="Do you need Redirection" CssClass="field-label"  AutoPostBack="true" />
                                    </td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkCorporate" Text="Is Corporate dashboard" CssClass="field-label"  AutoPostBack="true" />
                                    </td>
                                    
                                    
                                </tr>
                                
                                <tr id="trDrill" runat="server">
                                    
                                    <td>
                                        
                                        <asp:TextBox runat="server" ID="txtdrill" placeholder="Enter page name.." />
                                    
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Add Dashboard" CssClass="button" OnClick="btnSave_Click" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <asp:GridView runat="server" ID="gvDashboardRslt" AllowPaging="true" AutoGenerateColumns="false"
                                            CssClass="table table-bordered table-row" OnRowCommand="gvDashboardRslt_RowCommand"
                                            OnPageIndexChanging="gvDashboardRslt_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblid" Text='<%# Bind("ID")%>' runat="server" />
                                                        <asp:Label ID="lbDrill" Text='<%# Bind("ISDRILL")%>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Id" DataField="ID" />
                                                <asp:BoundField HeaderText="DashBoard Name" DataField="NAME" />
                                                <asp:BoundField HeaderText="DashBoard Category" DataField="CATEGORY" />
                                                <asp:BoundField HeaderText="DashBoard Type" DataField="TYPE" />
                                                <asp:BoundField HeaderText="Modules included" DataField="MODULE" />
                                                <asp:BoundField HeaderText="Drill down page" DataField="DRILLPAGE" />
                                                <asp:ButtonField HeaderText="Drill down" DataTextField="ISDRILL" CommandName="drill"   />
                                                <asp:BoundField HeaderText="Redirect page" DataField="ISPAGE" />
                                                <asp:BoundField HeaderText="Is Corporate" DataField="ISCORP" />
                                                <asp:ButtonField HeaderText="Update" Text="Update" CommandName="updating" />
                                                <asp:ButtonField HeaderText="Delete" Text="Delete" CommandName="deleting"  />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:HiddenField ID="idvalue" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="panel2" runat="server" HeaderText="Manage Dashboard">
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    
                                    <td width="20%" align="left">
                                        <span class="field-label">Select Module</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlmdl" runat="server" OnSelectedIndexChanged="ddlmdl_SelectedIndexChanged" AutoPostBack="true">
                                            
                                            <asp:ListItem Text="Student Management" Value="S0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Curriculum Management" Value="C0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Co-Curricular Activities" Value="CC">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Library Management" Value="LB">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="ACe" Value="SN">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Health Records" Value="MD">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Fees Management" Value="F0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Payroll" Value="P0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Transport Management" Value="T0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Financial Accounting" Value="A0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Purchase" Value="PI">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="System Administration" Value="D0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Messaging" Value="M0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="HR" Value="H0">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Professional Development" Value="CD">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Facility & Service Management" Value="FD">                                   
                                            </asp:ListItem>
                                            <asp:ListItem Text="Employee Self Service" Value="SS">                                   
                                            </asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%" align="left">
                                        <span class="field-label">Select User Role</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlusrrole" runat="server" OnSelectedIndexChanged="ddlusrrole_SelectedIndexChanged" AutoPostBack="true">
                                           
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>
                                <tr id="display_tier">
                                    <td colspan="4">
                                        <asp:Repeater ID="rptrtier" runat="server" OnItemDataBound="rptrtier_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblName" CssClass="field-value" Text='<%# Bind("DBM_NAME")%>' Visible="false"></asp:Label>
                                                <table width="100%">
                                                    <tr width="100%">
                                                        <td></td>
                                                        <td align="center">
                                                            <asp:Label runat="server" ID="lblName1" CssClass="field-value" Visible="false" Width="30%" Height="100%"></asp:Label>
                                                            <asp:Label runat="server" ID="lblName2" CssClass="field-value" Visible="false" Width="30%" Height="100%"></asp:Label>
                                                            <asp:Label runat="server" ID="lblName3" CssClass="field-value" Visible="false" Width="30%" Height="100%"></asp:Label>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" id="tblNew" runat="server">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                            Add New Tier
                                                        </button>
                                                    </h5>
                                                </div>

                                                <%--  first--%>
                                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="20%" align="left">
                                                                    <span class="field-label">Select Category</span>
                                                                </td>
                                                                <td width="30%" align="left">
                                                                    <asp:RadioButtonList ID="rdCatgry" runat="server" OnSelectedIndexChanged="rdCatgry_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="Medium" Value="M">
                                                                        </asp:ListItem>
                                                                        <asp:ListItem Text="Large" Value="L">
                                                                        </asp:ListItem>
                                                                        <asp:ListItem Text="X- Large" Value="XL">
                                                                        </asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td colspan="2"></td>
                                                            </tr>
                                                                                                                       
                                                            <tr id="rowM" runat="server" visible="false">
                                                                <td align="left" width="10%">
                                                                    <span class="field-label">Please select dashboards for tier</span>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddl11" runat="server" ></asp:DropDownList></td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddl12" runat="server" ></asp:DropDownList></td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddl13" runat="server" ></asp:DropDownList></td>

                                                            </tr>
                                                            <tr id="rowbtn" runat="server" visible="false">
                                                                <td colspan="4" align="center">
                                                                    <asp:Button ID="btnAdd" runat="server" Text="Add Tier" CssClass="button" OnClick="btnAdd_Click1" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="accordion" id="accordionExample2">
                                            <div class="card">
                                                <div class="card-header" id="headingTwo">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
                                                            Delete Tier
                                                        </button>
                                                    </h5>
                                                </div>

                                                <%--  first--%>
                                                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample2">
                                                    <div class="card-body">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="20%" align="left">
                                                                   <span class="field-label"> Please select Tier number </span> 
                                                                </td>
                                                                <td width="30%" align="left">
                                                                    <asp:DropDownList ID="ddlRemoveTier" runat="server"></asp:DropDownList>
                                                                </td>
                                                                <td width="50%" align="left">
                                                                    <asp:Button ID="btnDelete" runat="server" Text="Remove Tier" CssClass="button" OnClick="btnDelete_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="accordion" id="accordionExample3">
                                            <div class="card">
                                                <div class="card-header" id="headingThree">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseOne">
                                                            Modify Existing Tier
                                                </div>

                                               
                                                <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample3">
                                                    <div class="card-body">
                                                         <table width="100%">
                                                            <tr>
                                                                <td width="20%" align="left">
                                                                   <span class="field-label"> Please select Tier number </span> 
                                                                </td>
                                                                <td width="30%" align="left">
                                                                    <asp:DropDownList ID="ddlSlctTiernm" runat="server"></asp:DropDownList>
                                                                </td>
                                                                <td width="50%" align="left">
                                                                    <asp:Button ID="btnDisplay" runat="server" Text="Edit Tier" CssClass="button" OnClick="btnDisplay_Click" />
                                                                </td>
                                                                <td></td>
                                                             </tr>
                                                             <tr id="rowDshbrd" runat="server" visible="false">
                                                                <td align="left" width="10%">
                                                                    <span class="field-label">Please select dashboards for tier</span>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddl21" runat="server"></asp:DropDownList></td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddl22" runat="server" ></asp:DropDownList></td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddl23" runat="server" ></asp:DropDownList></td>
                                                             </tr>
                                                             <tr id="rowupdbtn" runat="server" visible="false">
                                                                 <td colspan="4" align="center">
                                                                     <asp:Button ID="btnUpdate" Text="Update Tier" runat="server" CssClass="button" OnClick="btnUpdate_Click" />
                                                                 </td>                                                                 
                                                             </tr>
                                                        </table>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:Panel ID="divDrill" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="CENTER">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>

                                <table align="center" width="100%" cellpadding="2" cellspacing="0" width="100%">
                                    <tr>

                                        <td align="left" width="20%"><span class="field-label">Name</span>
                                        </td>

                                        <td align="left">
                                            <asp:TextBox ID="txtDrillName" runat="server"></asp:TextBox>
                                        </td>


                                        <td align="left" width="20%"><span class="field-label">Type</span>
                                        </td>

                                        <td align="left">
                                           <asp:DropDownList ID="ddlDrillType" runat="server" AutoPostBack="true">
                                               <asp:ListItem Text="--"></asp:ListItem>
                                               <asp:ListItem Text="Pie"></asp:ListItem>
                                                <asp:ListItem Text="Bar"></asp:ListItem>
                                                <asp:ListItem Text="Donut"></asp:ListItem>
                                                <asp:ListItem Text="Stacked"></asp:ListItem>
                                                <asp:ListItem Text="Line"></asp:ListItem>
                                                <asp:ListItem Text="Series"></asp:ListItem>
                                                <asp:ListItem Text="Gauge"></asp:ListItem>
                                                <asp:ListItem Text="Repeater"></asp:ListItem>
                                                <asp:ListItem Text="Grid"></asp:ListItem>
                                               <asp:ListItem Text="Page"></asp:ListItem>
                                           </asp:DropDownList>
                                        </td>


                                    </tr>
                                    <tr>

                                        <td align="left" width="20%"><span class="field-label">Params</span>
                                        </td>

                                        <td align="left">
                                           <asp:CheckBoxList ID="ddlParams" runat="server" CssClass="field-label" RepeatDirection="Vertical" RepeatColumns="2">
                                               <asp:ListItem Text="@BSU_ID"></asp:ListItem>
                                               <asp:ListItem Text="@ACD_DESCR"></asp:ListItem>
                                               <asp:ListItem Text="@SEL_GRADE"></asp:ListItem>
                                               <asp:ListItem Text="@SEL_SECTION"></asp:ListItem>
                                               <asp:ListItem Text="@SEL_STREAM"></asp:ListItem>
                                               <asp:ListItem Text="@SEL_CLM"></asp:ListItem>
                                               <asp:ListItem Text="@SEL_FEETYPE"></asp:ListItem>
                                               <asp:ListItem Text="@SEL_STU"></asp:ListItem>
                                           </asp:CheckBoxList>
                                        </td>


                                        <td align="left" width="20%"><span class="field-label">Next Param</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlNextparam" runat="server">
                                                <asp:ListItem Text="" ></asp:ListItem>
                                                <asp:ListItem Text="BSU_ID" ></asp:ListItem>
                                                <asp:ListItem Text="ACY_ID" ></asp:ListItem>
                                                <asp:ListItem Text="CLM_ID" ></asp:ListItem>
                                                <asp:ListItem Text="GRD_ID" ></asp:ListItem>
                                                <asp:ListItem Text="SCT_ID" ></asp:ListItem>
                                                <asp:ListItem Text="STM_ID" ></asp:ListItem>
                                                <asp:ListItem Text="FTYPE" ></asp:ListItem>
                                                <asp:ListItem Text="STU" ></asp:ListItem>
                                               
                                                
                                            </asp:DropDownList>
                                        </td>

                                    </tr>
                                    <tr >
                                        <td align="left" width="20%"><span class="field-label">Order</span>
                                        </td>
                                        <td align="left">
                                         <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
                                        </td>
                                        
                                    </tr>
                                    <tr id="trURL" runat="server" visible="false">
                                        <td align="left" width="20%"><span class="field-label">URL</span>
                                        </td>
                                        <td align="left">
                                         <asp:TextBox ID="txtURL" runat="server"></asp:TextBox>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                    <td colspan="4">
                                        <asp:GridView runat="server" ID="grdDrillPage"  AutoGenerateColumns="false"
                                            CssClass="table table-bordered table-row" 
                                            >
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblid" Text='<%# Bind("DBD_ID")%>' runat="server" />
                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Id" DataField="DBD_ID" />
                                                <asp:BoundField HeaderText="DashBoard Name" DataField="DBD_NAME" />
                                                <asp:BoundField HeaderText="DashBoard Type" DataField="DBD_TYPE" />
                                                <asp:BoundField HeaderText="Params included" DataField="DBD_PARAMS" />
                                                <asp:BoundField HeaderText="Next Params" DataField="DBD_NEXT_PARAM" />
                                                <asp:BoundField HeaderText="Redirect page" DataField="DBD_URL" />
                                                <asp:BoundField HeaderText="Order" DataField="DBD_ORDER" />
                                                <asp:ButtonField HeaderText="Update" Text="Update" CommandName="updating"  />
                                                 <asp:ButtonField HeaderText="Delete" Text="Delete" CommandName="deleting"  />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnDrill" Text="Add" CssClass="button" runat="server" />
                                            <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                        </td>
                                    </tr>

                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>
