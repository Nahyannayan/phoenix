﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class Accounts_ParentPortalMenuSetting
    Inherits System.Web.UI.Page
    Dim message As String = ""
    Dim Encr_decrData As New Encryption64
    Dim Latest_refid As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        If Not IsPostBack Then
            Dim MainMnu_code As String = ""
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    DataBindBsuUnits()

                End If
            Catch ex As Exception
            End Try
        End If

    End Sub
   

    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "diverrorPopUp"
            Else
                lblError.CssClass = "divvalidPopUp"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
    Protected Sub DataBindBsuUnits()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dsbsu As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_BSU_UNITS]")
        lstBsuUnit.DataSource = dsbsu.Tables(0)
        lstBsuUnit.DataTextField = "bsu_name"
        lstBsuUnit.DataValueField = "bsu_id"
        lstBsuUnit.DataBind()            
        fillTranAcdY()
        getallgrades()

    End Sub

    Protected Sub databindradiobuttonlist()        
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dsbsu As DataSet = SqlHelper.ExecuteDataset(str_conn, "[dbo].[GET_MAINMENUNAMES_PARENT]")
        radioMenu.DataSource = dsbsu.Tables(0)
        radioMenu.DataTextField = "GROUPNAME"       
        radioMenu.DataBind()
    End Sub

    Protected Sub getallgrades()
        '        
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", lstBsuUnit.SelectedValue, SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.BigInt)
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GETALLGRADE_INSCHOOL]", Param)
        If Not dsData Is Nothing Then
            chkgradesnew.DataSource = dsData.Tables(0)
            chkgradesnew.DataTextField = "GRM_GRD_ID"
            chkgradesnew.DataBind()
        Else

        End If        
    End Sub
   
    Sub fillTranAcdY()
        Try
        
            Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(lstBsuUnit.SelectedValue)
            ddlACDYear.DataSource = dtACD
            ddlACDYear.DataTextField = "ACY_DESCR"
            ddlACDYear.DataValueField = "ACD_ID"
            ddlACDYear.DataBind()
            For Each rowACD As DataRow In dtACD.Rows
                If rowACD("ACD_CURRENT") Then
                    ddlACDYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                    Exit For
                End If
            Next
            databindradiobuttonlist()
            '  PopulateGrade()
            ' lnkBtnSelectGrade.Visible = True
        Catch ex As Exception
            message = getErrorMessage("4000")
            ShowMessage(message, True)
            UtilityObj.Errorlog("From fillTranAcdY :" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub   

    Protected Sub lstBsuUnit_SelectedIndexChanged(sender As Object, e As EventArgs)
        fillTranAcdY()
        gridMenu.Visible = False
        btnSaveAll.Visible = False
        lnkaddnew.Visible = False
        divaddnew.Visible = False
    End Sub

    Protected Sub radioMenu_SelectedIndexChanged(sender As Object, e As EventArgs)
        SubMenuGridBind()
        lnkaddnew.Visible = True
        btnSaveAll.Visible = True
        divaddnew.Visible = False
    End Sub

    Protected Sub SubMenuGridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim dsData As DataSet = Nothing
        Dim Param(2) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@MENU_NAME", radioMenu.SelectedItem.Text, SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@BSU_ID", lstBsuUnit.SelectedValue, SqlDbType.VarChar)
        Param(2) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.BigInt)
        Dim sql_query As String = "[dbo].[GET_SUB_MENUNAMES_PARENT]"
        dsData = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, sql_query, Param)
        ViewState("dsdata") = dsData
        gridMenu.DataSource = dsData
        gridMenu.DataBind()
        gridMenu.Visible = True
    End Sub

    Protected Sub gridMenu_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ds As DataSet = ViewState("dsdata")
            Dim result As String = ""
            Dim dt As DataTable = ds.Tables(0)
            Dim chkgrades As CheckBoxList = CType(e.Row.FindControl("chkgrades"), CheckBoxList)
            Dim chkshow As CheckBox = CType(e.Row.FindControl("chkshow"), CheckBox)
            Dim hidoplmid As HiddenField = CType(e.Row.FindControl("hidoplmid"), HiddenField)
            Dim SubMenuName As String = e.Row.Cells(0).Text
            Dim foundRows() As Data.DataRow
            foundRows = dt.Select("MENU = '" & SubMenuName & "'")
            If (foundRows(0)("EXISTING_MENU") = "") Then
                chkshow.Checked = False
            Else
                If foundRows(0)("SHOW") = True Then
                    chkshow.Checked = True
                Else
                    chkshow.Checked = False
                End If
            End If
            hidoplmid.Value = foundRows(0)("OPLM_ID")
            Dim grades As String = foundRows(0)("ALLGRADES")
            Dim string_grades() As String = grades.Split("|")
            Dim string_selected_grades() As String = foundRows(0)("GRADES").ToString.Split("|")
            For Each grade In string_grades
                Dim Listitem As New ListItem
                Listitem.Text = grade
                Listitem.Value = grade
                chkgrades.Items.Add(Listitem)
                result = Array.Find(string_selected_grades, Function(m) m = grade)
                If (result) Is Nothing Or result = "" Then
                    Listitem.Selected = False
                Else
                    Listitem.Selected = True
                End If
            Next
        End If
    End Sub
    Protected Sub gridMenu_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            If e.CommandName = "btnSaveCommand" Then
                Dim GET_SELECTED_SECTIONS As String = ""
                Dim strans As SqlTransaction = Nothing
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim qry2 = "[dbo].[INSERT_NEWMENU_PARENT-PLMS]"
                Dim Params(5) As SqlParameter
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim gvRow As GridViewRow = gridMenu.Rows(index)
                Dim MenuName As String = gvRow.Cells(0).Text
                Dim MainMenu As String = radioMenu.SelectedValue
                Dim BSUUnit As String = lstBsuUnit.SelectedValue
                Dim hidoplmid As HiddenField = CType(gvRow.FindControl("hidoplmid"), HiddenField)
                Dim oplm_id As Integer = Convert.ToInt32(hidoplmid.Value)
                Dim checkboxlist As CheckBoxList = CType(gvRow.FindControl("chkgrades"), CheckBoxList)
                Dim checkbox As CheckBox = CType(gvRow.FindControl("chkshow"), CheckBox)
                For Each lst As ListItem In checkboxlist.Items
                    If lst.Selected = True Then
                        If (GET_SELECTED_SECTIONS = "") Then
                            GET_SELECTED_SECTIONS = lst.Text
                        Else
                            GET_SELECTED_SECTIONS = GET_SELECTED_SECTIONS + "|" + lst.Text
                        End If
                    End If
                Next
                'update starting
                Try
                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    Params(0) = Mainclass.CreateSqlParameter("@OPLM_ID", oplm_id, SqlDbType.Int)
                    Params(1) = Mainclass.CreateSqlParameter("@PLMS_OPLM_DESCR", MenuName, SqlDbType.VarChar)
                    Params(2) = Mainclass.CreateSqlParameter("@PLMS_OPLM_GROUP_NAME", MainMenu, SqlDbType.VarChar)
                    Params(3) = Mainclass.CreateSqlParameter("@PLMS_BSU_ID", BSUUnit, SqlDbType.VarChar)
                    Params(4) = Mainclass.CreateSqlParameter("@PLMS_GRD_ID", GET_SELECTED_SECTIONS, SqlDbType.VarChar)
                    If checkbox.Checked = True Then
                        Params(5) = Mainclass.CreateSqlParameter("@bSHOW", 1, SqlDbType.Bit)
                    Else
                        Params(5) = Mainclass.CreateSqlParameter("@bSHOW", 0, SqlDbType.Bit)
                    End If
                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)
                    strans.Commit()
                    ShowMessage("Menu updated successfully", False)
                    gridMenu.Visible = False
                    SubMenuGridBind()
                    buttonshow()
                Catch ex As Exception
                    strans.Rollback()
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub buttonshow()
        divaddnew.Visible = False
        btnSaveAll.Visible = True
        gridMenu.Visible = True
        lnkaddnew.Visible = True
    End Sub
    Protected Sub btnsavenew_Click1(sender As Object, e As EventArgs)
        Dim strans As SqlTransaction = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParams(3) As SqlParameter
        Dim Params(6) As SqlParameter
        Dim qry1 = "[dbo].[INSERT_NEWMENU_PARENT-OPLM]"
        Dim qry2 = "[dbo].[INSERT_NEWMENU_PARENT-PLMS]"
        Dim GET_SELECTED_SECTIONS As String = ""
        Try
            If (chkgradesnew.SelectedIndex = -1) Then
                message = getErrorMessage("4015")
                ShowMessage(message, True)
            ElseIf radioMenu.SelectedItem Is Nothing Then
                message = "Please select parent menu"
                ShowMessage(message, True)
            ElseIf txtnewmenuname.Text Is Nothing Or txtnewmenuname.Text = "" Then
                message = "Please enter menu description"
                ShowMessage(message, True)
            ElseIf txturl.Text Is Nothing Or txturl.Text = "" Then
                message = "Please enter menu url"
                ShowMessage(message, True)
            Else
                Try
                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    pParams(0) = Mainclass.CreateSqlParameter("@OPLM_DESCR", txtnewmenuname.Text, SqlDbType.VarChar)
                    pParams(1) = Mainclass.CreateSqlParameter("@OPLM_URL", txturl.Text, SqlDbType.VarChar)
                    pParams(2) = Mainclass.CreateSqlParameter("@OPLM_GROUP_NAME", radioMenu.SelectedItem.Text, SqlDbType.VarChar)
                    Latest_refid = SqlHelper.ExecuteScalar(strans, CommandType.StoredProcedure, qry1, pParams)

                    For Each lstitem As ListItem In chkgradesnew.Items
                        If lstitem.Selected Then
                            If (GET_SELECTED_SECTIONS = "") Then
                                GET_SELECTED_SECTIONS = lstitem.Text
                            Else
                                GET_SELECTED_SECTIONS = GET_SELECTED_SECTIONS + "|" + lstitem.Text
                            End If
                        End If
                    Next
                    Params(0) = Mainclass.CreateSqlParameter("@OPLM_ID", Latest_refid, SqlDbType.Int)
                    Params(1) = Mainclass.CreateSqlParameter("@PLMS_OPLM_DESCR", txtnewmenuname.Text, SqlDbType.VarChar)
                    Params(2) = Mainclass.CreateSqlParameter("@PLMS_OPLM_GROUP_NAME", radioMenu.SelectedValue, SqlDbType.VarChar)
                    Params(3) = Mainclass.CreateSqlParameter("@PLMS_BSU_ID", lstBsuUnit.SelectedValue, SqlDbType.VarChar)
                    Params(4) = Mainclass.CreateSqlParameter("@PLMS_GRD_ID", GET_SELECTED_SECTIONS, SqlDbType.VarChar)
                    Params(5) = Mainclass.CreateSqlParameter("@bSHOW", 1, SqlDbType.Bit)

                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)

                    strans.Commit()
                    ShowMessage("New menu added successfully", False)
                    divaddnew.Visible = False
                    SubMenuGridBind()
                    buttonshow()
                Catch ex As Exception
                    strans.Rollback()
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkaddnew_Click(sender As Object, e As EventArgs)
        divaddnew.Visible = True
        btnSaveAll.Visible = False
        gridMenu.Visible = False
        lnkaddnew.Visible = False
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        For Each gvRow In gridMenu.Rows
            Dim GET_SELECTED_SECTIONS As String = ""
            Dim strans As SqlTransaction = Nothing
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim qry2 = "[dbo].[INSERT_NEWMENU_PARENT-PLMS]"
            Dim Params(5) As SqlParameter
            'Dim index As Integer = Convert.ToInt32()
            'Dim gvRow As GridViewRow = gridMenu.Rows(index)
            Dim MenuName As String = gvRow.Cells(0).Text
            Dim MainMenu As String = radioMenu.SelectedValue
            Dim BSUUnit As String = lstBsuUnit.SelectedValue
            Dim hidoplmid As HiddenField = CType(gvRow.FindControl("hidoplmid"), HiddenField)
            Dim oplm_id As Integer = Convert.ToInt32(hidoplmid.Value)
            Dim checkboxlist As CheckBoxList = CType(gvRow.FindControl("chkgrades"), CheckBoxList)
            Dim checkbox As CheckBox = CType(gvRow.FindControl("chkshow"), CheckBox)
            For Each lst As ListItem In checkboxlist.Items
                If lst.Selected = True Then
                    If (GET_SELECTED_SECTIONS = "") Then
                        GET_SELECTED_SECTIONS = lst.Text
                    Else
                        GET_SELECTED_SECTIONS = GET_SELECTED_SECTIONS + "|" + lst.Text
                    End If
                End If
            Next
            'update starting
            Try
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                Params(0) = Mainclass.CreateSqlParameter("@OPLM_ID", oplm_id, SqlDbType.Int)
                Params(1) = Mainclass.CreateSqlParameter("@PLMS_OPLM_DESCR", MenuName, SqlDbType.VarChar)
                Params(2) = Mainclass.CreateSqlParameter("@PLMS_OPLM_GROUP_NAME", MainMenu, SqlDbType.VarChar)
                Params(3) = Mainclass.CreateSqlParameter("@PLMS_BSU_ID", BSUUnit, SqlDbType.VarChar)
                Params(4) = Mainclass.CreateSqlParameter("@PLMS_GRD_ID", GET_SELECTED_SECTIONS, SqlDbType.VarChar)
                If checkbox.Checked = True Then
                    Params(5) = Mainclass.CreateSqlParameter("@bSHOW", 1, SqlDbType.Bit)
                Else
                    Params(5) = Mainclass.CreateSqlParameter("@bSHOW", 0, SqlDbType.Bit)
                End If
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)
                strans.Commit()
                ShowMessage("All menu changes updated successfully", False)
                gridMenu.Visible = False
                SubMenuGridBind()
                buttonshow()
            Catch ex As Exception
                strans.Rollback()
            End Try
        Next
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)
        buttonshow()
    End Sub

    Protected Sub lnkslctall_Click(sender As Object, e As EventArgs)
        Dim obj As Object = sender.parent.parent
        Dim checkboxlist As CheckBoxList = CType(obj.FindControl("chkgrades"), CheckBoxList)
        For Each lstitem As ListItem In checkboxlist.Items
            lstitem.Selected = True
        Next
    End Sub
End Class
