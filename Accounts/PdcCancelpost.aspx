<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PdcCancelpost.aspx.vb" Inherits="Accounts_PdcCancelpost" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("ChkSelAll").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" >
                    <tr>
                        <td width="100%" align="left" >
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label></td>
                       
                    </tr>
                </table>
                <table align="center" width="100%">
                
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" AllowPaging="True" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="ChkSelect" type="checkbox" runat="server" value='<%# Bind("VHR_ID") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="VHH_DOCNO" HeaderText="Do cNo"></asp:BoundField>
                                    <asp:BoundField DataField="VHH_DOCDT" HeaderText="Doc Date"></asp:BoundField>
                                    <asp:BoundField DataField="BANK" HeaderText="Bank"></asp:BoundField>
                                    <asp:BoundField DataField="PARTY" HeaderText="Party"></asp:BoundField>
                                    <asp:BoundField DataField="VHD_CHQNO" HeaderText="Chq No"></asp:BoundField>
                                    <asp:BoundField DataField="VHD_CHQDT" HeaderText="Chq Date"></asp:BoundField>
                                    <asp:BoundField DataField="VHR_CHQDT" HeaderText="Change Date"></asp:BoundField>
                                    <asp:BoundField DataField="VHD_NARRATION" HeaderText="Narration"></asp:BoundField>
                                    <asp:BoundField DataField="DOACTION" HeaderText="Action"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <table id="TablePost" runat="server" align="center" width="100%">
                                <tr>
                                    <td align="center" >
                                        <asp:Button CssClass="button" ID="btnSave" runat="server" TabIndex="26" Text="Post" OnClick="btnSave_Click" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

