<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accccCreditcardReceipt.aspx.vb" Inherits="Accounts_accccCreditcardReceipt" Title="Credit Card Receipt" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td input[type=text], table td select {
            min-width: 20% !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function getCashcode() {

            var NameandCode;
            var result;
            result = radopen("acccpShowCashflow.aspx?rss=1", "pop_up")
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=txtdCashflowcode.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtdCashflowname.ClientID %>').value = NameandCode[1];

            return false;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
}

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                 document.getElementById('<%=txtdCashflowcode.ClientID %>').value = NameandCode[0];
                 document.getElementById('<%=txtdCashflowname.ClientID %>').value = NameandCode[1];
            }
        }

        function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {


            var lstrVal;
            var lintScrVal;


            document.getElementById("<%=hd_Pmode.ClientID %>").value = pMode;
            document.getElementById("<%=hd_ctrl.ClientID%>").value = ctrl;
            document.getElementById("<%=hd_ctrl1.ClientID %>").value = ctrl1;
            var NameandCode;
            var result;
            if (pMode == 'NOTCC') {
                if (ctrl2 == '' || ctrl2 == undefined) {
                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up1");
                } else {
                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "pop_up1");
                }
                //if (result=='' || result==undefined)
                //{    return false;      } 
                //     lstrVal=result.split('||');     
                //    document.getElementById(ctrl).value=lstrVal[0];
                //    document.getElementById(ctrl1).value=lstrVal[1];
                //    //document.getElementById(ctrl2).value=lstrVal[2];
                //   // document.getElementById(ctrl3).value=lstrVal[3];

            }
        }

         
              function OnClientClose1(oWnd, args) {
                  //get the transferred arguments
                  var arg = args.get_argument();
                  if (arg) {
                      NameandCode = arg.NameandCode.split('||');
                      var pMode = document.getElementById("<%=hd_Pmode.ClientID %>").value;
                      var ctrl = document.getElementById("<%=hd_ctrl.ClientID%>").value;
                      var ctrl1 = document.getElementById("<%=hd_ctrl1.ClientID %>").value;
                      if (pMode == 'NOTCC') {
                          document.getElementById(ctrl).value=NameandCode[0];
                          document.getElementById(ctrl1).value=NameandCode[1];
                      }

                  }
              }


             function getAccount() {
                 popUp('960', '600', 'NOTCC', '<%=txtdAccountCode.ClientId %>', '<%=txtdAccountName.ClientId %>', '<%=ddDCollection.ClientId %>');
                 return false;
             }
        //ttxCCard
             function getCard() {

                 var NameandCode;
                 var result;
                 result =radopen("accShowCreditCards.aspx", "pop_up2");
                 <%--if (result == '' || result == undefined) {
                //            document.getElementById("txtdate").value=''; 
                return false;
            }
            document.getElementById('<%=txtCCard.ClientID %>').value = result.split('___')[0];
            document.getElementById('<%=h_Cardid.ClientID %>').value = result.split('___')[1]
            //alert( document.getElementById('<%=h_Cardid.ClientID %>').value);
            return false;--%>
             }

             function OnClientClose2(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtCCard.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=h_Cardid.ClientID %>').value = NameandCode[1]

                }
            }

             function CopyDetails() {
                 try {
                     if (document.getElementById('<%=txtdNarration.ClientID %>').value == '')
                         document.getElementById('<%=txtdNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
                 }
                 catch (ex) { }
             }

        function AddDetails(url) {

            var NameandCode;
            var result;
            var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';
            //alert(url_new);
            dates = document.getElementById('<%=txtHDocdate.ClientID %>').value;
            dates = dates.replace(/[/]/g, '-')
            url_new = url_new + '&dt=' + dates;
            result = window.showModalDialog("acccpAddDetails.aspx?" + url_new, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=txtdAccountCode.ClientID %>').focus();


            return false;
        }

        function checkOldref() {
            if (document.getElementById('<%=txtHOldrefno.ClientID %>').value == '') {
                return (confirm('The Old Ref No. is Empty. Do You Want to Continue?'));
            }
            else {
                return true;
            }
        }
    </script>
    
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Credit Card
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                   <asp:HiddenField ID="hd_Pmode" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <table align="center" width="100%" border="0" ">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table class="BlueTable" align="center" width="100%">

                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Doc No [Old Ref No]</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True"
                                            Width="42%"></asp:TextBox>[
                                <asp:TextBox ID="txtHOldrefno" runat="server" Width="42%" TabIndex="2"></asp:TextBox>
                                        ]</td>
                                    <td width="20%" align="left"><span class="field-label">Doc Date</span></td>

                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True"
                                            Width="80%" TabIndex="3"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Currency</span></td>

                                    <td width="30%" valign="middle" align="left">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" Width="20%"
                                            TabIndex="5">
                                        </asp:DropDownList><asp:TextBox ID="txtHExchRate" runat="server" ReadOnly="True" width="65.5%"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Group Rate</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHLocalRate" runat="server" Width="80%"
                                            ></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Collection Account</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddCollection" runat="server" AutoPostBack="True" TabIndex="6" Width="85%">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Card Type</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCCard" runat="server" AutoPostBack="True" width="80%"> </asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgCard" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCard(); return false;" TabIndex="8" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCCard"
                                            ErrorMessage="Kindley Select a Credit Card" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Narration</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtHNarration" runat="server" TextMode="MultiLine" MaxLength="300" TabIndex="10" Width="85%">

                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4">Details</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Collection Account</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddDCollection" runat="server" TabIndex="14" AutoPostBack="True"  Width="85%"> 
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Amount</span></td>

                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtdAmount" runat="server" TabIndex="12" AutoCompleteType="Disabled" Width="80%"> </asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtdAmount"
                                            ErrorMessage="Amount Should be Valid" Operator="DataTypeCheck" Type="Double"
                                            ValidationGroup="Details" Display="Dynamic">*</asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtdAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Credit Account</span></td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtdAccountCode" runat="server" Width="20%" AutoPostBack="True"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtdAccountCode"
                                            ErrorMessage="Account Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="btnAccount" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;" TabIndex="16" />
                                        <asp:TextBox ID="txtdAccountName" runat="server"
                                            Width="69%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Cash Flow</span></td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtdCashflowcode" runat="server" Width="20%"></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtdCashflowcode"
                                            ErrorMessage="Cash Flow Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="imgCashflow" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCashcode(); return false;" TabIndex="18" /><asp:TextBox ID="txtdCashflowname" runat="server"
                                                    Width="69%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Narration</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtdNarration" runat="server"  Width="85%"
                                             TextMode="MultiLine" MaxLength="300" TabIndex="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdNarration"
                                            ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:Button ID="btnAdddetails" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" TabIndex="22" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="24" />
                                        <asp:Button ID="btnEditCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="26" />
                                        <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td align="center" colspan="4" width="100%">
                                        <br />
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Transaction details added yet." Width="100%">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Accountid" HeaderText="Account Code" ReadOnly="True" />
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                                <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Cashflowname" HeaderText="Cash Flow" />
                                                <asp:BoundField DataField="Collection" HeaderText="Collection" />
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                            OnClick="LinkButton1_Click" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="CostCenter" HeaderText="TEST" Visible="False" />
                                                <asp:TemplateField HeaderText="Allocate" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnAlloca" runat="server" CausesValidation="false" CommandName=""
                                                            Text="Allocate"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cost Center" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRequired" runat="server" Text='<%# Bind("Required") %>' Visible="False"></asp:Label>

                                                        <asp:LinkButton ID="lbAllocate" runat="server" Visible="False">Allocate</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"><span class="field-label">Total Amount </span>
                                <asp:TextBox ID="txtdTotalamount" runat="server" ReadOnly="True" Width="20%"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" ValidationGroup="Details" Width="56px" TabIndex="28" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="48px" OnClientClick="return checkOldref();" TabIndex="30" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" ValidationGroup="Details" Width="48px" TabIndex="32" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" ValidationGroup="Details" OnClientClick="return confirm('Are you sure');" TabIndex="34" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="36" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" Width="47px" TabIndex="38" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <input id="h_Cardid" runat="server" type="hidden" />
                <input id="h_mode" runat="server" type="hidden" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
