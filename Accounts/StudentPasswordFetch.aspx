<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudentPasswordFetch.aspx.vb" Inherits="Accounts_StudentPasswordFetch" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvUNITS.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#e0ffba';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state1(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Student Password Fetch"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="tabmain" align="center" border="0" cellpadding="0"
                                cellspacing="0" width="100%" runat="server">

                                <tr id="row2">
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                                    <td align="left" colspan="2">
                                        <asp:DropDownList ID="ddlBUnit" runat="server" DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="center" width="30%"> <asp:Button ID="btnRefresh" runat="server" CssClass="button" Text="Refresh" Visible="true" /></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvUNITS" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="60" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Available" Visible="true">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkAll" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state1(this);"
                                                            ToolTip="Click here to select/deselect all rows" Visible="false" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" AutoPostBack="true" runat="server" onclick="javascript:highlight(this);" OnCheckedChanged="chkSelect_CheckedChanged" EnableViewState="true" />
                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# bind("STU_ID") %>'></asp:HiddenField>
                                                        <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# bind("STU_USR_NAME") %>'></asp:HiddenField>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fee Id" HeaderStyle-Width="10%">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblTCHeaderFeeId" runat="server" Text="Fee Id">
                                                        </asp:Label><br />
                                                        <asp:TextBox ID="txtFeeIdSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnFeeId_Search" runat="server" ImageAlign="Middle"
                                                            ImageUrl="~/Images/forum_search.gif" OnClick="btnFeeId_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_Fee_Id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblTCHeaderGrade" runat="server" Text="Grade">
                                                        </asp:Label><br />
                                                        <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblTCHeaderSection" runat="server" Text="Section">
                                                        </asp:Label><br />
                                                        <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>

                                                    </ItemTemplate>


                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblTCHeaderStudentName" runat="server" Text="Student Name">
                                                        </asp:Label><br />
                                                        <asp:TextBox ID="txtStudNameSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudNameSearch" runat="server" ImageAlign="Middle"
                                                            ImageUrl="~/Images/forum_search.gif" OnClick="btnStudNameSearch_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudentName" runat="server" Text='<%# Bind("StudName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Parent User Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblTCHeaderParentUserName" runat="server" Text="Student User Name">
                                                        </asp:Label><br />
                                                        <asp:TextBox ID="txtParentUserNameSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnParentUserName_Search" runat="server" ImageAlign="Middle"
                                                            ImageUrl="~/Images/forum_search.gif" OnClick="btnParentUserName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblParentUserName" runat="server" Text='<%# Bind("STU_USR_NAME") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:ButtonField CommandName="resett" HeaderText="Reset" Text="Reset">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>

                                                <asp:TemplateField HeaderText="Reset Password">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHdrResetPassword" runat="server" Text="Reset Password">
                                                        </asp:Label><br />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblResetPassword" runat="server" Text='<%# Bind("RESET_PASSWORD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--<asp:TemplateField HeaderText="Password"><HeaderTemplate>
            <table><tr><td align="center">
            Password</td></tr></table>
            
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="lblpasswrd" runat="server" Width="40px" __designer:wfdid="w3"></asp:Label> <asp:HiddenField id="OLU_PASSWORD" runat="server" Value='<%# BIND("OLU_PASSWORD") %>' __designer:wfdid="w4"></asp:HiddenField> 
                            
</ItemTemplate>

</asp:TemplateField>--%>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblErrors" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <%--<asp:Button id="btnActivate" runat="server" CssClass="button"  
                    tabIndex="7" Text="Activate" ValidationGroup="groupM1" Width="70px" />--%>
                            <asp:Button ID="btnReset" runat="server" CssClass="button" ValidationGroup="groupM1"
                                TabIndex="8" Text="Reset" Visible="true" />
                        </td>
                    </tr>

                </table>




                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>

</asp:Content>

