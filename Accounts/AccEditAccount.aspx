<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccEditAccount.aspx.vb" Inherits="AccEditAccount" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function ShowRPTSETUP_S(id, tid, hid, tid2) {

            var NameandCode;
            var result;
            document.getElementById('<%= hdid.ClientID %>').value = id;
            document.getElementById('<%= hdtid.ClientID %>').value = tid;
            document.getElementById('<%= hdhid.ClientID %>').value = hid;
            document.getElementById('<%= hdtid2.ClientID %>').value = tid2;
            result = radopen("ShowRPTSETUP_S.aspx?rss_id=" + id, "pop_up");


            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('||');
            //document.getElementById(tid).value = NameandCode[1];
            //document.getElementById(hid).value = NameandCode[0] + '|' + NameandCode[2];
            //if (tid2 != '')
            //    if (NameandCode[2] != '')//hfDescription1
            //    {
            //        document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.display = '';
            //        document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.visibility = '';
            //        document.getElementById(tid2).style.display = '';
            //    }
            //    else {
            //        document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.display = 'none';
            //        //document.getElementById(tid.replace(/txtCode/,'lbSet1') ).style.visibility='' ;txtDesc1
            //        document.getElementById(tid2).style.display = 'none';
            //    }

            //return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
               NameandCode = arg.NameandCode.split('||');
             var id = document.getElementById('<%= hdid.ClientID %>').value ;
             var  tid = document.getElementById('<%= hdtid.ClientID %>').value ;
             var hid = document.getElementById('<%= hdhid.ClientID %>').value ;
             var tid2=  document.getElementById('<%= hdtid2.ClientID %>').value ;

            document.getElementById(tid).value = NameandCode[1];
            document.getElementById(hid).value = NameandCode[0] + '|' + NameandCode[2];
            if (tid2 != '')
                if (NameandCode[2] != '')//hfDescription1
                {
                    document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.display = '';
                    document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.visibility = '';
                    document.getElementById(tid2).style.display = '';
                }
                else {
                    document.getElementById(tid.replace(/txtCode/, 'lbSet1')).style.display = 'none';
                    //document.getElementById(tid.replace(/txtCode/,'lbSet1') ).style.visibility='' ;txtDesc1
                    document.getElementById(tid2).style.display = 'none';
                }

            }
        }


    </script>


    <script language="javascript" type="text/javascript">
        function ChangeAllCheckBoxStates() {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("Checkbox1").checked;
            ChangeCheckBoxState(lstrChk);
        }


        function ChangeCheckBoxState(checkState) {
            var tableBody = document.getElementById('<%=chkBusUnit.ClientID%>').rows;
            //alert(tableBody[0]);            
            for (var i = 0; i < tableBody.length; i++) {
                //alert(tableBody[i].cells[0].innerText);
                var checkBoxes = tableBody[i].getElementsByTagName("INPUT");
                //alert(checkBoxes);
                for (var k = 0; k < checkBoxes.length; k++) {
                    //if (checkBoxes[k].checked) {
                    //    var row = checkBoxes[k].parentNode.parentNode;
                    //    alert(row.cells[0].innerHTML);
                    //}
                    checkBoxes[k].checked = checkState;
                }

            }

        }
        <%--function ChangeCheckBoxState(checkState) {
            var tableBody = document.getElementById('<%=chkBusUnit.ClientID %>').childNodes[0];
            //alert('called me')
            for (var i = 0; i < tableBody.childNodes.length; i++) {
                var currentTd = tableBody.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = checkState;



                // if ( listControl.checked == true )
                //alert('#' + i + ': is checked');
            }
        }--%>



        function changeValue() {
            var DNR = document.getElementById('<%= optControlAccYes.ClientID %>').checked;
            if (DNR == false) {
                styleObj = document.getElementById(21).style;
                styleObj.display = 'none';

                styleObj = document.getElementById(22).style;
                styleObj.display = '';

                document.getElementById('<%= txtAccCode2.ClientID %>').readOnly = true;
                document.getElementById('<%= txtAccCode2.ClientID %>').value = "";
                document.getElementById('<%= txtSGroup.ClientID %>').value = "";
                document.getElementById('<%= txtSGroupDescr.ClientID %>').value = "";

                document.getElementById('<%= txtControlAcc.ClientID %>').value = "";
                document.getElementById('<%= txtControlAccDescr.ClientID %>').value = "";

                document.getElementById('<%= txtAccCode.ClientID %>').value = "";
                document.getElementById('<%= txtAccDescr.ClientID %>').value = "";
            }
            else {
                styleObj = document.getElementById(21).style;
                styleObj.display = '';

                styleObj = document.getElementById(22).style;
                styleObj.display = 'none';

                document.getElementById('<%= txtAccCode2.ClientID %>').value = "";
                document.getElementById('<%= txtAccCode2.ClientID %>').readOnly = false;
                document.getElementById('<%= txtSGroup.ClientID %>').value = "";
                document.getElementById('<%= txtSGroupDescr.ClientID %>').value = "";

                document.getElementById('<%= txtControlAcc.ClientID %>').value = "";
                document.getElementById('<%= txtControlAccDescr.ClientID %>').value = "";

                document.getElementById('<%= txtAccCode.ClientID %>').value = "";
                document.getElementById('<%= txtAccDescr.ClientID %>').value = "";

            }
        }
        function GetSubValues(id, tid, hid, tid2) {
            
            var NameandCode;
            var result;
            var RptCode;

            var rsscode = document.getElementById(hid).value;

            document.getElementById('<%= hd_tid.ClientID %>').value = tid;
            RptCode = id + '|' + rsscode;
            result = radopen("ShowRptSetUpSub.aspx?rss_code=" + RptCode, "pop_up1");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById(tid).value = NameandCode[0];--%>
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                var tid = document.getElementById('<%= hd_tid.ClientID %>').value;
                ocument.getElementById(tid).value = NameandCode[0];              
            }
        }


        function trim(sString) {
            while (sString.substring(0, 1) == ' ') {
                sString = sString.substring(1, sString.length);
            }
            while (sString.substring(sString.length - 1, sString.length) == ' ') {
                sString = sString.substring(0, sString.length - 1);
            }
            return sString;
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Accounts Main
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:HiddenField ID="hd_tid" runat="server" />
                <asp:HiddenField ID="hdtid" runat="server" />
                <asp:HiddenField ID="hdtid2" runat="server" />
                <asp:HiddenField ID="hdid" runat="server" />
                <asp:HiddenField ID="hdhid" runat="server" />

                <table width="100%" align="center">
                    <tr>
                        <td align="left">
                            <%-- <div align="left" class="tabber" id="tab2">
                                <div class="tabbertab tabbertabdefault">--%>
                            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" TabIndex="7">
                                <asp:TabPanel ID="TabPanel1" HeaderText="Accounts Main" runat="server">
                                    <HeaderTemplate>Accounts Main</HeaderTemplate>
                                    <ContentTemplate>
                                        <%-- <h2>Accounts Main</h2>--%>
                                        <table width="100%" align="center">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblErr" runat="server" CssClass="error"></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" align="center">
                                            <tbody>
                                                <tr class="title-bg">
                                                    <td align="left" colspan="4">Accounts Main                                                       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Control Account</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:RadioButton ID="optControlAccYes" runat="server" GroupName="optControl" Text="Yes"
                                                            AutoPostBack="True" Enabled="False" />
                                                        <asp:RadioButton ID="optControlAccNo" runat="server" GroupName="optControl" Text="No"
                                                            Checked="True" AutoPostBack="True" Enabled="False" />
                                                    </td>
                                                    <td align="left" width="20%"><span class="field-label">Active</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:CheckBox ID="chkActive" runat="server" CssClass="field-label" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Customer/Supplier</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:RadioButton ID="optNormal" runat="server" Checked="True" GroupName="optCust"
                                                            Text="Normal" AutoPostBack="True" /><asp:RadioButton ID="optCustomer" runat="server"
                                                                GroupName="optCust" Text="Customer" AutoPostBack="True" />
                                                        <asp:RadioButton ID="optSupplier" runat="server" GroupName="optCust" Text="Supplier"
                                                            AutoPostBack="True" /></td>
                                                    <td align="left" width="20%"><span class="field-label">Type</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="cmbType" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="tr_SubGroup" runat="server">
                                                    <td align="left" width="20%"><span class="field-label">Sub Group</span></td>

                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtSGroup" runat="server">
                                                        </asp:TextBox></td>
                                                    <td align="left" colspan="2">
                                                        <a href="#" onclick="popUp('460','400','SUBGRP','<%=txtSGroup.ClientId %>','<%=txtSGroupDescr.ClientId %>','<%=txtAccCode.ClientId %>')"></a>

                                                        <asp:TextBox ID="txtSGroupDescr" runat="server">
                                                        </asp:TextBox></td>
                                                </tr>
                                                <tr id="tr_ControlAccount" runat="server">
                                                    <td align="left" width="20%"><span class="field-label">Control Account</span><span class="text-danger">* </span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtControlAcc" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" colspan="2">
                                                        <a href="#" onclick="popUp('460','400','CONTROLACC','<%=txtControlAcc.ClientId %>','<%=txtControlAccDescr.ClientId %>','<%=txtSGroup.ClientId %>','<%=txtSGroupDescr.ClientId %>','<%=txtAccCode.ClientId %>','<%=txtAccCode2.ClientId %>')"></a>
                                                        <asp:TextBox ID="txtControlAccDescr" runat="server">
                                                        </asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Account Code &amp; Description</span><span class="text-danger">* </span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtAccCode" runat="server" MaxLength="4">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td align="left" width="20%">
                                                        <asp:TextBox ID="txtAccCode2" runat="server" MaxLength="4">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtAccDescr" runat="server" AutoPostBack="True" OnTextChanged="txtAccDescr_TextChanged">
                                                        </asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Currency</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="cmbCurrency" runat="server">
                                                        </asp:DropDownList></td>
                                                    <td align="left" width="20%"><span class="field-label">Bank / Cash</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="cmbBankCash" runat="server" AutoPostBack="True">
                                                            <asp:ListItem Value="N">NORMAL</asp:ListItem>
                                                            <asp:ListItem Value="B">BANK</asp:ListItem>
                                                            <asp:ListItem Value="C">CASH</asp:ListItem>
                                                            <asp:ListItem Value="CC">COLLECTION ACCT</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Bank Account #</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtBankAccount" runat="server">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td align="left" width="20%">
                                                        <asp:Label ID="Label2" runat="server" Text="Facility Ceiling" CssClass="field-label"></asp:Label></td>
                                                    <%--<td >
                                                        <asp:Label ID="lblDot3" runat="server" Text=":"></asp:Label></td>--%>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtFaciCelng" runat="server">
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="tr_Sales" runat="server">
                                                    <td align="left" width="20%">
                                                        <asp:Label ID="lblSales" runat="server" Text="Sales Tax # #" CssClass="field-label"></asp:Label></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtSalesTax" runat="server">
                                                        </asp:TextBox></td>
                                                    <td align="left" width="20%">
                                                        <asp:Label ID="lblPayment" runat="server" Text="Payment Terms" CssClass="field-label"></asp:Label></td>
                                                    <%--<td >
                                                        <asp:Label ID="lblDot2" runat="server" Text=":"></asp:Label></td>--%>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="cmbPymntTrm" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trCreditDays" runat="server">
                                                    <td align="left" width="20%"><span class="field-label">Credit Days</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtCreditDays" runat="server">
                                                        </asp:TextBox></td>
                                                    <td align="left" width="20%"></td>
                                                    <td align="left" width="30%"></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Policy Group</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="cmbPolicyGrp" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" width="20%"></td>
                                                    <td align="left" width="30%"></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Link To Report</span><span class="text-danger">* </span></td>
                                                    <td align="left" colspan="3">
                                                        <asp:GridView ID="gvRPTSETUP_M" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                                            <Columns>
                                                                <asp:TemplateField SortExpression="RSM_TYP" Visible="False" HeaderText="RSM_TYP">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRSM_TYP" runat="server" Text='<%# Bind("RSM_TYP") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="RSM_DESCR" SortExpression="RSM_DESCR" HeaderText="Description"></asp:BoundField>
                                                                <asp:BoundField DataField="TYP" Visible="False" SortExpression="TYP" HeaderText="TYP"></asp:BoundField>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox Style="display: none" ID="txtCode" runat="server"></asp:TextBox>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Set" CausesValidation="False" OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;RSM_TYP&quot;) & &quot;');return false;&quot; %>" OnPreRender="LinkButton1_PreRender"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hfDescription" runat="server"></asp:HiddenField>
                                                                        <asp:TextBox Style="display: none" ID="txtCode1" runat="server"></asp:TextBox>
                                                                        <asp:LinkButton ID="lbSet1" runat="server" Text="Set (Opposite)" CausesValidation="False" OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;RSM_TYP&quot;) & &quot;');return false;&quot; %>" OnPreRender="LinkButton1_PreRender"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hfDescription1" runat="server"></asp:HiddenField>
                                                                        <asp:TextBox ID="txtCode2" runat="server"></asp:TextBox>
                                                                        <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="<%# &quot;GetSubValues('&quot; & Container.DataItem(&quot;RSM_TYP&quot;) & &quot;');return false;&quot; %>" OnPreRender="LinkButton2_PreRender">Sub Account</asp:LinkButton><br />
                                                                        <asp:HiddenField ID="hfDescription2" runat="server"></asp:HiddenField>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="griditem" />
                                                            <SelectedRowStyle />
                                                            <HeaderStyle />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Linked Reports</span></td>

                                                    <td align="left" colspan="3">
                                                        <asp:GridView ID="gvRptDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("RSM_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="False" HeaderText="CODE">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("RSS_CODE") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Account Details">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("RSS_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="griditem" />
                                                            <SelectedRowStyle />
                                                            <HeaderStyle />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Linked Sub Ledger</span></td>
                                                    <td align="left" colspan="3">
                                                        <asp:GridView ID="gvLinkedAcc" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                                            <Columns>
                                                                <asp:BoundField DataField="ASM_ID" HeaderText="Sub Ledger Code" />
                                                                <asp:BoundField DataField="ASM_NAME" HeaderText="Sub Ledger Name" />
                                                                <asp:BoundField DataField="IsDefault" HeaderText="Is Default" />
                                                                <asp:TemplateField HeaderText="View">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlEdit" runat="server"
                                                                            NavigateUrl='<%# Eval("ASM_ID", "accNewSubLedger.aspx?editid={0}") %>'
                                                                            Text="View"></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblASMID" runat="server"
                                                                            Text='<%# Bind("ASM_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="griditem" />
                                                            <SelectedRowStyle />
                                                            <HeaderStyle />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                        </table>
                                        <%-- </div>--%>
                                    </ContentTemplate>
                                </asp:TabPanel>

                                <asp:TabPanel ID="TabPanel2" HeaderText="Communication Details" runat="server">
                                    <HeaderTemplate>Communication Details</HeaderTemplate>
                                    <ContentTemplate>
                                        <%-- <div class="tabbertab ">
                                    <h2>Communication Details</h2>--%>
                                        <table width="100%" align="center">
                                            <tbody>
                                                <tr class="title-bg">
                                                    <td colspan="4">
                                                        <div align="left">
                                                            Contact Details
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Contact Person</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtContact" runat="server">
                                                        </asp:TextBox></td>
                                                    <td align="left" width="20%"><span class="field-label">Designation</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtDesig" runat="server">
                                                        </asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Address1</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtAddress1" runat="server">
                                                        </asp:TextBox></td>
                                                    <td align="left" width="20%"><span class="field-label">Address2</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtAddress2" runat="server">
                                                        </asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">City</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="cmbCity" runat="server">
                                                        </asp:DropDownList></td>
                                                    <td align="left" width="20%"><span class="field-label">Country</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="cmbCountry" runat="server">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Telephone</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtPhone" runat="server" Width="80%">
                                                        </asp:TextBox></td>
                                                    <td align="left" width="20%"><span class="field-label">Fax</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtFax" runat="server" Width="80%">
                                                        </asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Mobile</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtMobile" runat="server" Width="80%">
                                                        </asp:TextBox></td>
                                                    <td align="left" width="20%"><span class="field-label">Email</span></td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtEmail" runat="server" Width="80%">
                                                        </asp:TextBox></td>
                                                </tr>
                                        </table>
                                        <%--</div>--%>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <%-- <div class="tabbertab " style="font-weight: bold; font-size: 12px; color: #1b80b6; font-family: Verdana">
                                    <h2>Business Units</h2>
                                    &nbsp;--%>
                                <asp:TabPanel ID="TabPanel3" HeaderText="Business Units" runat="server">
                                    <HeaderTemplate>Business Units</HeaderTemplate>
                                    <ContentTemplate>
                                        <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates();" type="checkbox"
                                            value="Check All" />
                                        Select All
                                        <div align="center">
                                            <asp:Panel ID="plChkBUnit" runat="server" Height="200px" HorizontalAlign="Left" ScrollBars="Vertical"
                                                Width="100%">
                                                <asp:CheckBoxList ID="chkBusUnit" runat="server" BorderWidth="0px" CellPadding="2"
                                                    CellSpacing="2" CssClass="path1" RepeatColumns="1" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </asp:Panel>
                                        </div>
                                        <%--  &nbsp;
                        </div>--%>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <%-- </div>--%>
                            </asp:TabContainer>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" /></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
