<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="accGeneralSettings.aspx.vb" Inherits="Accounts_AccGeneralSettings"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            General Settings
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>

                <table width="100%" align="center">

                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="chkHideCC" runat="server" Text="Hide Cost Center In Vouchers " CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="30" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="30" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
