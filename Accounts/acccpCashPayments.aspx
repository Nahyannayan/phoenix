<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="acccpCashPayments.aspx.vb" Inherits="cpCashPayments" Title="Cash Payment" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="sds" Namespace="Telerik.Web.SessionDS" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td select, div select, table td input[type=text], table td input[type=date], table td input[type=password], table td textarea {
            min-width: 20% !important;
        }
    </style>

    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script language="javascript" type="text/javascript">

            function expandcollapse(obj, row) {
                var div = document.getElementById(obj);
                var img = document.getElementById('img' + obj);

                if (div.style.display == "none") {
                    div.style.display = "block";
                    if (row == 'alt') {
                        img.src = "../images/Misc/minus.gif";
                    }
                    else {
                        img.src = "../images/Misc/minus.gif";
                    }
                    img.alt = "Close to view other Customers";
                }
                else {
                    div.style.display = "none";
                    if (row == 'alt') {
                        img.src = "../images/Misc/plus.gif";
                    }
                    else {
                        img.src = "../images/Misc/plus.gif";
                    }
                    img.alt = "Expand to show Orders";
                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

                document.getElementById('<%=hd_ctrl.ClientID%>').value = ctrl;
                document.getElementById('<%=hd_ctrl1.ClientID%>').value = ctrl1;
                document.getElementById('<%=hd_pMode.ClientID()%>').value = pMode;

                var lstrVal;
                var lintScrVal;


                var NameandCode;
                var result;

                if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {

                    result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up5");

                    //if (result=='' || result==undefined)
                    //{    return false;      } 
                    //lstrVal=result.split('||');     
                    //document.getElementById(ctrl).value=lstrVal[0];
                    //document.getElementById(ctrl1).value=lstrVal[1];              
                }
                else if (pMode == 'NOTCC') {
                    if (ctrl2 == '' || ctrl2 == undefined) {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "pop_up5");
                    } else {
                        result = radopen("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "pop_up5");
                    }
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl).value = lstrVal[0];
                    //document.getElementById(ctrl1).value = lstrVal[1];
                    ////document.getElementById(ctrl2).value=lstrVal[2];
                    //// document.getElementById(ctrl3).value=lstrVal[3];

                }

            }

            function OnClientClose5(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    var ctrl = document.getElementById('<%=hd_ctrl.ClientID%>').value;
                    var ctrl1 = document.getElementById('<%=hd_ctrl1.ClientID%>').value;
                    var pMode = document.getElementById('<%=hd_pMode.ClientID()%>').value;
                    NameandCode = arg.NameandCode.split('||');
                    if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }
                    else if (pMode == 'NOTCC') {
                        document.getElementById(ctrl).value = NameandCode[0];
                        document.getElementById(ctrl1).value = NameandCode[1];
                    }

                }
            }

            function getAccountH() {
                popUp('960', '600', 'CASHONLY', '<%=txtHAccountcode.ClientId %>', '<%=txtHAccountname.ClientId %>');
                return false;
            }
            function CopyDetails() {
                try {
                    if (document.getElementById('<%=txtDNarration.ClientID %>').value == '')
                        document.getElementById('<%=txtDNarration.ClientID %>').value = document.getElementById('<%=txtHNarration.ClientID %>').value;
                }
                catch (ex) { }
            }
            function getPettyCash() {
                var frm = "PETTYCASH"
                var bsuId = "B";
                var NameandCode;
                var result;
                result = radopen("AccPettycashShow.aspx?rss=0", "pop_up");
                <%--if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtPettycashId.ClientID %>").value = NameandCode[0];
       document.getElementById('<%=txtDAmount.ClientID %>').value = NameandCode[1];
       document.getElementById('<%=Detail_ACT_ID.ClientID %>').value = NameandCode[3];
       document.getElementById('<%=txtPettycashdocno.ClientID %>').value = NameandCode[4];
       var remarks = 'Being the cash paid to.. ' + NameandCode[4] + '..towards :' + NameandCode[2] + ':..from request'
       document.getElementById('<%=txtHNarration.ClientID %>').value = remarks;
    document.getElementById('<%=txtDNarration.ClientID %>').value = remarks;
       return false;--%>
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtPettycashId.ClientID %>").value = NameandCode[0];
                    document.getElementById('<%=txtDAmount.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=Detail_ACT_ID.ClientID %>').value = NameandCode[3];
                    document.getElementById('<%=txtPettycashdocno.ClientID %>').value = NameandCode[4];
                    var remarks = 'Being the cash paid to.. ' + NameandCode[4] + '..towards :' + NameandCode[2] + ':..from request'
                    document.getElementById('<%=txtHNarration.ClientID %>').value = remarks;
                    document.getElementById('<%=txtDNarration.ClientID %>').value = remarks;
                }
            }


            function getCashcode() {
                var NameandCode;
                var result;
                result = radopen("acccpShowCashflow.aspx?rss=0", "pop_up1")
                            <%--if (result == '' || result == undefined) {
                       return false;
                   }
                   NameandCode = result.split('___');
                   document.getElementById('<%=txtDCashflowcode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtDCashflowname.ClientID %>').value = NameandCode[1];
                return false;--%>
            }

            function OnClientClose1(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtDCashflowcode.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtDCashflowname.ClientID %>').value = NameandCode[1];
                }
            }

            function getAccount() {
                popUp('960', '600', 'NOTCC', '<%=Detail_ACT_ID.ClientId %>', '<%=txtDAccountName.ClientId %>');
                return false;
            }

            function AddDetails(url) {

                var NameandCode;
                var result;
                var url_new = url + '&editid=' + '<%=h_editorview.Value %>' + '&viewid=' + '<%=Request.QueryString("viewid") %>';

                dates = document.getElementById('<%=txtHDocdate.ClientID %>').value;
                dates = dates.replace(/[/]/g, '-')
                url_new = url_new + '&dt=' + dates;
                result = radopen("acccpAddDetails.aspx?" + url_new, "pop_up2")
                <%--if (result == '' || result == undefined) {
                        return false;
                    }
                    NameandCode = result.split('___');
                    document.getElementById('<%=Detail_ACT_ID.ClientID %>').focus();
                 return false;--%>
            }

            function OnClientClose2(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=Detail_ACT_ID.ClientID %>').focus();

                }
            }




            function Settle_Online() {

                var NameandCode;
                var result;
                var pId = 1;
                if (pId == 1) {
                    url = "ShowOnlineSettlement.aspx?actid=" + document.getElementById('<%=Detail_ACT_ID.ClientID %>').value + "&lineid=" + document.getElementById('<%=h_NextLine.ClientID %>').value + "&docno=" + document.getElementById('<%=txtHDocno.ClientID %>').value + "&dt=" + document.getElementById('<%=txtHDocdate.ClientID %>').value;
                }
                result = radopen(url, "pop_up3");
                <%--if (result == '' || result == undefined) {
                    return false;
                }
                document.getElementById('<%=txtDAmount.ClientID %>').value = result;
                return true;--%>
            }

            function OnClientClose3(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=txtDAmount.ClientID %>').value = arg.NameandCode;

                }
            }

            function getRoleID(mode) {

                document.getElementById("<%=hd_mode.ClientID%>").value = mode;
                var NameandCode;
                var result;
                var url;
                url = 'accShowEmpPP.aspx?id=' + mode;
                if (mode == 'EA') {
                    result = radopen(url, "pop_up4");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //NameandCode = result.split('___');
                }
                if (mode == 'CU') {
                    result = radopen(url, "pop_up4");
                   <%-- if (result == '' || result == undefined)
                    { return false; }
                    NameandCode = result.split('___');
                    document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];
                    document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[4];
                    document.getElementById("<%=txtExpCode.ClientID %>").value = NameandCode[3];--%>
                }

            }

            function OnClientClose4(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    var mode = document.getElementById("<%=hd_mode.ClientID%>").value;
                    NameandCode = arg.NameandCode.split('||');
                    if (mode == 'EA') {

                    }
                    if (mode == 'CU') {

                        document.getElementById("<%=txtCostUnit.ClientID %>").value = NameandCode[0];
                        document.getElementById("<%=hfId1.ClientID %>").value = NameandCode[1];
                        document.getElementById("<%=txtPreAcc.ClientID %>").value = NameandCode[4];
                        document.getElementById("<%=txtExpCode.ClientID %>").value = NameandCode[3];
                    }
                }
            }

            function HideAll() {
                document.getElementById("<%=pnlCostUnit.ClientID %>").style.display = 'none';
                return false;
            }

        </script>
    </telerik:RadScriptBlock>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up_Cost2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_Cost2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cash Payment
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hd_mode" runat="server" />
                <asp:HiddenField ID="hd_pMode" runat="server" />
                <asp:HiddenField ID="hd_ctrl1" runat="server" />
                <asp:HiddenField ID="hd_ctrl" runat="server" />

                <table align="center" width="100%" border="0">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_NextLine" runat="server" type="hidden" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table align="center" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle"><span class="field-label">Cash Payment</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Doc No [Old Ref No]</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHDocno" runat="server" ReadOnly="True" Width="42%"></asp:TextBox>
                                        [
                            <asp:TextBox ID="txtHOldrefno" runat="server" Width="42%" TabIndex="2"></asp:TextBox>
                                        ]
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Doc Date</span>
                                    </td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True"
                                            TabIndex="4"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="5" />
                                    </td>
                                </tr>
                                <tr valign="middle">
                                    <td align="left"><span class="field-label">Currency </span>
                                    </td>
                                    <td valign="middle" align="left">
                                        <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" Width="20%"
                                            TabIndex="6">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtHExchRate" runat="server" Width="66%"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Group Rate </span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtHLocalRate" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr valign="middle">
                                    <td align="left"><span class="field-label">Cash Account <span style="color: red">*</span> </span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtHAccountcode" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                            runat="server" ControlToValidate="Detail_ACT_ID" ErrorMessage="Account Code Cannot Be Empty"
                                            ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:ImageButton ID="btnHaccount" ImageAlign="Middle"
                                            runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccountH();return false;"
                                            TabIndex="8" />
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtHAccountname" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr valign="middle">
                                    <td align="left"><span class="field-label">Petty Cash Ref </span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPettycashId" runat="server"></asp:TextBox>
                                        &nbsp;
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getPettyCash(); return false;" TabIndex="14" ImageAlign="Middle" />
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtPettycashdocno" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Narration <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtHNarration" runat="server" Width="87%"
                                            TextMode="MultiLine" MaxLength="300" TabIndex="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                            ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkAdvance" runat="server" AutoPostBack="True" Text="Advance" CssClass="field-label" />
                                    </td>
                                </tr>
                            </table>
                            <table id="tbl_Details" runat="server" align="center" class="BlueTable" cellpadding="3"
                                cellspacing="0" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="4" valign="middle">Details
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Account <span style="color: red">*</span></span>
                                    </td>
                                    <td valign="middle" align="left" colspan="2">
                                        <asp:TextBox ID="Detail_ACT_ID" runat="server" Width="20%"
                                            AutoPostBack="True"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            runat="server" ControlToValidate="Detail_ACT_ID" ErrorMessage="Account Code Cannot Be Empty"
                                            ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <asp:ImageButton ID="btnAccount"
                                            runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;"
                                            TabIndex="12" />
                                        <asp:TextBox ID="txtDAccountName" runat="server" Width="55%"></asp:TextBox><br />
                                        <ajaxToolkit:PopupControlExtender ID="pceCostUnit" runat="server" PopupControlID="pnlCostUnit"
                                            Position="Bottom" TargetControlID="lnkCostUnit">
                                        </ajaxToolkit:PopupControlExtender>

                                        <asp:LinkButton ID="lnkCostUnit" runat="server">Specify CostUnit Details</asp:LinkButton>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr runat="server" id="trPayTerm" visible="false">
                                    <td align="left" width="20%"></td>
                                    <td align="left" valign="middle" colspan="2">
                                        <asp:Label ID="lblPaymentTerm" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Cash Flow <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" colspan="2" width="100%">
                                        <asp:TextBox ID="txtDCashflowcode" runat="server" Width="20%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDCashflowcode"
                                            ErrorMessage="Cash Flow Code Cannot Be Empty" ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton
                                                ID="ImageButton3" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCashcode(); return false;"
                                                TabIndex="14" />
                                        <asp:TextBox ID="txtDCashflowname" runat="server"
                                            Width="55%"></asp:TextBox></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Amount <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDAmount" runat="server" TabIndex="16"
                                            AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Cannot be empty" ValidationGroup="Details" Display="Dynamic">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDAmount"
                                            ErrorMessage="Amount Should be Valid" Operator="DataTypeCheck" Type="Double"
                                            ValidationGroup="Details" Display="Dynamic">*</asp:CompareValidator>
                                        <asp:LinkButton ID="lbSettle" runat="server" OnClientClick="Settle_Online();">(Settle)</asp:LinkButton>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration <span style="color: red">*</span></span>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtDNarration" runat="server"
                                            TextMode="MultiLine" MaxLength="300" TabIndex="18"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDNarration"
                                            ErrorMessage="Narration Cannot be Blank" ValidationGroup="Details">*</asp:RequiredFieldValidator>
                                        <br />

                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Details" />

                                    </td>
                                    <td width="30%"></td>
                                </tr>
                                <tr runat="server" id="trTaxType" visible="false">
                                    <td width="20%" align="left"><span class="field-label">TAX Type</span></td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlVATCode" runat="server"></asp:DropDownList></td>
                                    <td width="20%" align="left"></td>
                                    <td width="30%" align="left"></td>
                                </tr>

                                <tr runat="server" id="tr_UploadEmplyeeCostCenter" visible="false">
                                    <td width="20%" align="left"><span class="field-label">Upload Employees </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="fuEmployeeData" runat="server" />
                                        <asp:LinkButton ID="lbUploadEmployee" runat="server">Upload</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Cost Allocation </span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <!-- content start -->
                                        <div class="panel-cover">
                                            <uc1:usrCostCenter ID="usrCostCenter1" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdddetails" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" TabIndex="20" />
                                        <asp:Button ID="btnEditcancel" runat="server" CssClass="button" Text="Cancel" TabIndex="22" />
                                    </td>
                                </tr>
                                <%--  <tr>
                        <td width="15%" align="right" style="height: 21px" colspan="2"></td>
                    </tr>--%>
                            </table>
                            <table id="Table1" align="center" width="100%">
                                <tr>
                                    <td align="center" colspan="4">
                                        <br />
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                            CssClass="table table-bordered table-row" Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <a href="javascript:expandcollapse('div<%# Eval("id") %>', 'one');">
                                                            <img id="imgdiv<%# Eval("id") %>" alt="Click to show/hide Orders for Customer <%# Eval("id") %>"
                                                                width="9px" border="0" src="../images/ Misc/plus.gif" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False" HeaderText="id">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField ReadOnly="True" DataField="Accountid" HeaderText="Account Code"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Account Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Narration" HeaderText="Narration"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Cashflow" Visible="False" HeaderText="Cashflow code"></asp:BoundField>
                                                <asp:BoundField DataField="Cashflowname" HeaderText="Cash Flow"></asp:BoundField>
                                                <asp:TemplateField HeaderText="TaxCode" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaxCode" runat="server" Text='<%# Bind("TaxCode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        Edit
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbEdit" OnClick="lbEdit_Click" runat="server" Text="Edit"
                                                            CausesValidation="false" CommandName="Edits"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ShowDeleteButton="True" HeaderText="Delete">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:CommandField>
                                                <asp:BoundField DataField="CostCenter" Visible="False" HeaderText="TEST"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Allocate" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnAlloca" runat="server" Text="Allocate" CausesValidation="false"
                                                            CommandName=""></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False" HeaderText="Cost Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRequired" runat="server" Text='<%# Bind("CostReqd") %>' Visible="False"></asp:Label>
                                                        <asp:LinkButton ID="lbAllocate" runat="server" Visible="False">Allocate</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td colspan="100%" align="right">
                                                                <div id="div<%# Eval("id") %>" style="display: none; position: relative; left: 15px; overflow: auto; width: 100%">
                                                                    <asp:GridView ID="gvCostchild" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                        CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblIdCostchild" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="VoucherId" HeaderText="VoucherId" Visible="False" />
                                                                            <asp:BoundField DataField="Costcenter" HeaderText="Costcenter" />
                                                                            <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                            <asp:BoundField DataField="Allocated" HeaderText="Allocated" Visible="False" />
                                                                            <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                            <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Allocated"
                                                                                HtmlEncode="False" InsertVisible="False" SortExpression="Amount" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Earn Code" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList ID="ddlERN_ID" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Memberid" HeaderText="Memberid" Visible="False" />
                                                                            <asp:TemplateField HeaderText="Amount">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtAmt" runat="server" Visible="false" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                                                        Width="104px" onblur="CheckAmount(this)"></asp:TextBox>
                                                                                    <asp:Label ID="txtAmt0" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>' Width="104px"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:TemplateField>
                                                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:CommandField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td colspan="100%" align="right">
                                                                                            <asp:GridView ID="gvCostAllocation" runat="server" AutoGenerateColumns="False" EmptyDataText="Cost Center Not Allocated"
                                                                                                Width="100%" CssClass="table table-bordered table-row" OnRowDataBound="gvCostchild_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblIdCostAllocation" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="ASM_ID" HeaderText="Cost Allocation" />
                                                                                                    <asp:BoundField DataField="ASM_NAME" HeaderText="Name" />
                                                                                                    <asp:TemplateField HeaderText="Amount">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="txtAmt1" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>' Width="104px"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    </asp:CommandField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Total Amount </span>
                                    </td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtDTotalamount" runat="server"></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="Details"
                                            CausesValidation="False" TabIndex="24" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="26" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" ValidationGroup="Details" TabIndex="26" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" ValidationGroup="Details" OnClientClick="return confirm('Are you sure');"
                                            TabIndex="28" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False"
                                            TabIndex="30" />
                                        <asp:Button ID="btnSettle" runat="server" CssClass="button" Text="Settle" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print"
                                            TabIndex="32" /><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_editorview" runat="server" type="hidden" value="" />
                <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                <asp:HiddenField ID="hfId1" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="calHdocdate" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                </ajaxToolkit:CalendarExtender>
                <asp:Panel ID="pnlCostUnit" runat="server" Style="display: none">
                    <table align="center" width="100%">
                        <tr class="gridheader_pop_orange">
                            <td align="left" colspan="4">CostUnitDetails
                   <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif"
                       OnClientClick="return HideAll();" Style="text-align: right" TabIndex="54"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1">
                                <span class="field-label">Period From </span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFrom" runat="server" TabIndex="15" AutoPostBack="True"
                                    OnTextChanged="txtFrom_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgFrom"
                                    runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20"></asp:ImageButton>
                            </td>
                            <td align="left"><span class="field-label">To </span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTo" runat="server" TabIndex="15" AutoPostBack="True"
                                    OnTextChanged="txtTo_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="imgTo"
                                    runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1"><span class="field-label">CostUnit </span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCostUnit" runat="server"
                                    Enabled="False"></asp:TextBox>
                                <asp:ImageButton ID="btnPrePaid" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getRoleID('CU');return true;"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Expense Account  </span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtExpCode" runat="server"
                                    Enabled="False"></asp:TextBox>
                                <asp:TextBox ID="txtPreAcc" runat="server" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender1" runat="server" BehaviorID="100-calendarButtonExtender1"
                                    CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender2" runat="server" BehaviorID="100-calendarButtonExtender2"
                                    CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
