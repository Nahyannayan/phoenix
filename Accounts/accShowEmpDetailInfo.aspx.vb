﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Accounts_accShowEmpDetailInfo
    Inherits System.Web.UI.Page
    Dim SearchMode As String
    Dim display As String
    'Version        Date        Author          Change
    '1.1            5-May-2011  Swapna          added condition 'EL' for leave applications
    '1.2            19-mar-2012 swapna          to get reporting to employees from different bsus- ERP
    '1.3            27-Feb-2013 swapna          to display bsu wise earn types

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
     
        If Page.IsPostBack = False Then
            If h_SelectedId.Value <> "Close" Then
                Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)
                Response.Write("} </script>" & vbCrLf)
            End If
            SearchMode = Request.QueryString("id")
            If Request.QueryString("EMPNO") <> Nothing Then
                ViewState("EMPNO") = Request.QueryString("EMPNO")
            End If
            If SearchMode = "E" Or SearchMode = "EN" Or SearchMode = "EL" Or SearchMode = "RS_EM" Or SearchMode = "ERP" Or SearchMode = "ENR" Then
                Page.Title = "Employee Info"

            ElseIf SearchMode = "B" Then
                Page.Title = "Business Unit Info"

            ElseIf SearchMode = "C" Then
                Page.Title = "Category Info"
            ElseIf SearchMode = "D" Then
                Page.Title = "Deduction Info"
            End If
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            gridbind()
        End If
       
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            SearchMode = Request.QueryString("id")
            If Request.QueryString("display") <> Nothing Then
                display = Request.QueryString("display")
            End If
            If Request.QueryString("Bsu_id") <> Nothing Then
                ViewState("bsu_id") = Request.QueryString("Bsu_id")
            End If
            Dim str_filter_code As String
            Dim str_filter_name As String
            'Get all the employees
            If SearchMode = "E" Then
                str_Sql = "Select ID,EMPNO No, E_Name from(Select emp_ID as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,EMP_bACTIVE as bActive,ISNULL(EMPNO,'') EMPNO from Employee_M)a where a.bActive=1 and  a.id<>'' "
                'Getting all the employee for that BSU
            ElseIf SearchMode = "EN" And ViewState("bsu_id") <> "" Then
                str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M )a where a.bActive=1 and a.BSU='" & ViewState("bsu_id") & "' and a.id<>'' "
            ElseIf SearchMode = "EN" And display = "STF_NO" Then
                str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMP_STAFFNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M)a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
            ElseIf SearchMode = "EN" Then
                str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M where emp_status in (1,2,4) )a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
                'Get reporting to person from diff. BSUs
            ElseIf SearchMode = "ENR" Then
                str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M where emp_status in (1,2) )a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
            ElseIf SearchMode = "ERP" Then
                str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M where emp_bsu_id in " _
            & " (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe ((select bsu_emp_rep_bsuids from businessunit_m where BSU_ID='" & Session("sBsuid") & "' ),',')))a where a.bActive=1   and a.id<>'' "

                'Get all the BSU
            ElseIf SearchMode = "B" Then
                str_Sql = "Select ID,ID No, E_Name from(Select BSU_ID as ID,BSU_NAME as E_Name from businessunit_m)a where a.ID<>'' "
                'Get Category
            ElseIf SearchMode = "C" Then
                str_Sql = "Select ID,ID No, E_Name from(Select ECT_ID as ID,ECT_DESCR as E_Name from EMPCATEGORY_M)a where a.ID<>''"
                'Get Deduction Type
                'V1.3 comment
                'ElseIf SearchMode = "DT" Then
                '    str_Sql = "Select ID,ID No, E_Name from(Select ERN_ID as ID,ERN_DESCR as E_Name,ERN_TYP from EMPSALCOMPO_M)a where  a.ERN_TYP=0 and a.ID<>''"
                '    'Get Earning Type
                'ElseIf SearchMode = "ET" Then
                '    str_Sql = "Select ID,ID No, E_Name from(Select ERN_ID as ID,ERN_DESCR as E_Name,ERN_TYP, ERN_bSTD_SALTYP from EMPSALCOMPO_M)a where  a.ERN_TYP=1 and a.ID<>'' and isnull(a.ERN_bSTD_SALTYP,0) = 0 "

                'V1.3 addition
            ElseIf SearchMode = "DT" Then
                str_Sql = "Select ID,ID No, E_Name from(Select ERN_ID as ID,ERN_DESCR as E_Name,ERN_TYP from EMPSALCOMPO_M where '" & Session("sBsuid") & "' in (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe (Isnull(ERN_BSU_IDs ,'" & Session("sBsuid") & "'),'|')))a where  a.ERN_TYP=0 and a.ID<>''"
                'Get Earning Type
            ElseIf SearchMode = "ET" Then
                str_Sql = "Select ID,ID No, E_Name from(Select ERN_ID as ID,ERN_DESCR as E_Name,ERN_TYP, ERN_bSTD_SALTYP from EMPSALCOMPO_M where '" & Session("sBsuid") & "' in (SELECT Replace(ID,'''','') FROM dbo.fnSplitMe (Isnull(ERN_BSU_IDs ,'" & Session("sBsuid") & "'),'|')))a where  a.ERN_TYP=1 and a.ID<>'' and isnull(a.ERN_bSTD_SALTYP,0) = 0 "
            ElseIf SearchMode = "RES_EM" Then 'RESIGNED WMPLOYEES FOR FINAL SETTLEMENT
                str_Sql = "SELECT ID, ID No, E_Name FROM (SELECT ISNULL(EMPNO,'') ID , isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  E_Name from EMPLOYEE_M "
                str_Sql = str_Sql & " WHERE EMP_ID IN (SELECT EREG_EMP_ID FROM EMPRESIGNATION_D WHERE EREG_APPRSTATUS_HR='A')  AND EMP_BSU_ID ='" & Session("sBsuid") & "') A "

            ElseIf SearchMode = "ER" Then
                str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M where emp_id in (select EMP_REPORTTO_EMP_ID from employee_m))a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
            ElseIf SearchMode = "EAR" Then
                str_Sql = "Select distinct ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M where emp_id in (select distinct EMP_ATT_Approv_EMP_ID from employee_m union all select distinct EMP_REPORTTO_EMP_ID from employee_m ))a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
            ElseIf (SearchMode = "EL" And Session("sBusper") = False) Then
                '--  swapna adding new option to apply on behalf
                'V1.1
                '    str_Sql = "IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130016' and " & _
                '    "[MNR_BSU_ID]='" & Session("sBsuid") & "' and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
                '     "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_bsu_id='" & Session("sBsuid") & "' )as a where 1=1 " & _
                '    " ELSE IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130017' and " & _
                '    "[MNR_BSU_ID]='" & Session("sBsuid") & "'and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
                '    "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and  emp_bsu_id='" & Session("sBsuid") & "' and  emp_dpt_id in (select emp_dpt_id from employee_m inner join users_m " & _
                '    " on employee_m.emp_id=users_m.usr_emp_id and usr_name='" & Session("sUsr_name") & "'))as a where a.ID<>'' " & _
                '    " ELSE Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_id in (select usr_emp_id from  users_m where usr_name='" & Session("sUsr_name") & "' ) and emp_bsu_id='" & Session("sBsuid") & "')as a where a.ID<>''"
                str_Sql = "IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130016' and " & _
               "[MNR_BSU_ID]='" & Session("sBsuid") & "' and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
                "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_bsu_id='" & Session("sBsuid") & "' )as a where 1=1 " & _
               " ELSE IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130017' and " & _
               "[MNR_BSU_ID]='" & Session("sBsuid") & "'and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
               "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and  emp_bsu_id='" & Session("sBsuid") & "' and  emp_dpt_id in (select emp_dpt_id from employee_m inner join users_m " & _
               " on employee_m.emp_id=users_m.usr_emp_id and usr_name='" & Session("sUsr_name") & "'))as a where a.ID<>'' " & _
               " ELSE IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130018' and " & _
               "[MNR_BSU_ID]='" & Session("sBsuid") & "'and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
               " Select ELVA_ELA_EMP_ID ID,empno No,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m inner join USR_SS.ELEAVE_ASSIGN on " & _
               " emp_id= ELVA_ELA_EMP_ID inner join users_m on usr_emp_id=ELVA_EMP_ID where emp_status in (1,2) and isnull(emp_bactive,0)=1 and emp_bsu_id='" & Session("sBsuid") & "' and isnull(ELVA_IsDeleted,0)=0 " & _
               " ELSE Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_id in (select usr_emp_id from  users_m where usr_name='" & Session("sUsr_name") & "' ) and emp_bsu_id='" & Session("sBsuid") & "')as a where a.ID<>''"

            ElseIf (SearchMode = "EL" And Session("sBusper") = True) Then
                str_Sql = "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_bsu_id='" & Session("sBsuid") & "' )as a where a.ID<>'' "

            ElseIf SearchMode = "ERS" Then  '-- employee resignation
                str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M where emp_status in (1,2,4,5) and isnull(emp_isfinalsettled,0)=0 )a where  a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
                'Get reporting to person from diff. BSUs


            End If
            Dim ds As New DataSet
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_txtCode, str_txtName As String
            Dim str_search As String
            str_txtCode = ""
            str_txtName = ""
            str_filter_code = ""
            str_filter_name = ""
            If gvEmpInfo.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                If SearchMode = "E" Or SearchMode = "EN" Or SearchMode = "EL" Or SearchMode = "ERP" Or SearchMode = "ENR" Or SearchMode = "ERS" Then  'v1.2
                    'str_img = h_selected_menu_1.Value()
                    str_Sid_search = h_selected_menu_1.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                    str_txtCode = txtSearch.Text
                    ''code
                    If txtSearch.Text <> "" Then
                        If str_search = "LI" Then
                            str_filter_code = " AND a.NO LIKE '%" & txtSearch.Text & "%'"
                        ElseIf str_search = "NLI" Then
                            str_filter_code = " AND a.NO NOT LIKE '%" & txtSearch.Text & "%'"
                        ElseIf str_search = "SW" Then
                            str_filter_code = " AND a.NO LIKE '" & txtSearch.Text & "%'"
                        ElseIf str_search = "NSW" Then
                            str_filter_code = " AND a.NO NOT LIKE '" & txtSearch.Text & "%'"
                        ElseIf str_search = "EW" Then
                            str_filter_code = " AND a.NO LIKE '%" & txtSearch.Text & "'"
                        ElseIf str_search = "NEW" Then
                            str_filter_code = " AND a.NO NOT LIKE '%" & txtSearch.Text & "'"
                        End If
                    End If

                    ''name

                    str_Sid_search = h_Selected_menu_2.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                    str_txtName = txtSearch.Text
                    If txtSearch.Text <> "" Then
                        If str_search = "LI" Then
                            str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                        ElseIf str_search = "NLI" Then
                            str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                        ElseIf str_search = "SW" Then
                            str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                        ElseIf str_search = "NSW" Then
                            str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                        ElseIf str_search = "EW" Then
                            str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                        ElseIf str_search = "NEW" Then
                            str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                        End If
                    End If

                ElseIf SearchMode = "B" Then
                    'str_img = h_selected_menu_1.Value()
                    str_Sid_search = h_selected_menu_1.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                    str_txtCode = txtSearch.Text
                    ''code
                    If str_search = "LI" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                    End If
                    ''name
                    str_Sid_search = h_Selected_menu_2.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                    str_txtName = txtSearch.Text
                    If str_search = "LI" Then
                        str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                    End If
                ElseIf SearchMode = "C" Or SearchMode = "DT" Or SearchMode = "ET" Then
                    'str_img = h_selected_menu_1.Value()
                    str_Sid_search = h_selected_menu_1.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                    str_txtCode = txtSearch.Text
                    ''code
                    If str_search = "LI" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                    End If
                    ''name
                    str_Sid_search = h_Selected_menu_2.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                    str_txtName = txtSearch.Text
                    If str_search = "LI" Then
                        str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                    End If
                End If
            End If
            If (SearchMode = "EL" And Session("sBusper") = False) Then
                'str_Sql = "IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130016' and " & _
                '                "[MNR_BSU_ID]='" & Session("sBsuid") & "' and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
                '                 "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_bsu_id='" & Session("sBsuid") & "' )as a where 1=1 " & str_filter_code & str_filter_name & _
                '                " ELSE IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130017' and " & _
                '                "[MNR_BSU_ID]='" & Session("sBsuid") & "'and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
                '                "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and  emp_bsu_id='" & Session("sBsuid") & "' and  emp_dpt_id in (select emp_dpt_id from employee_m inner join users_m " & _
                '                " on employee_m.emp_id=users_m.usr_emp_id and usr_name='" & Session("sUsr_name") & "'))as a where a.ID<>'' " & str_filter_code & str_filter_name & _
                '                " ELSE Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_id in (select usr_emp_id from  users_m where usr_name='" & Session("sUsr_name") & "' ) and emp_bsu_id='" & Session("sBsuid") & "')as a where a.ID<>''" & str_filter_code & str_filter_name
                str_Sql = "IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130016' and " & _
               "[MNR_BSU_ID]='" & Session("sBsuid") & "' and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
                "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_bsu_id='" & Session("sBsuid") & "' )as a where 1=1 " & str_filter_code & str_filter_name & " order by a.E_Name" & _
               " ELSE IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130017' and " & _
               "[MNR_BSU_ID]='" & Session("sBsuid") & "'and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
               "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and  emp_bsu_id='" & Session("sBsuid") & "' and  emp_dpt_id in (select emp_dpt_id from employee_m inner join users_m " & _
               " on employee_m.emp_id=users_m.usr_emp_id and usr_name='" & Session("sUsr_name") & "'))as a where a.ID<>'' " & str_filter_code & str_filter_name & " order by a.E_Name" & _
               " ELSE IF (select count(*)   FROM [OASIS].[dbo].[MENURIGHTS_S] where [MNR_RIGHT]>=5 and [MNR_MNU_ID]='P130018' and " & _
               "[MNR_BSU_ID]='" & Session("sBsuid") & "'and [MNR_ROL_ID] in (select USR_ROL_ID from USERS_M where USR_NAME='" & Session("sUsr_name") & "'))>0 " & _
               "  Select ID,No, E_Name from (Select ELVA_ELA_EMP_ID ID,empno No,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m inner join USR_SS.ELEAVE_ASSIGN on " & _
               " emp_id= ELVA_ELA_EMP_ID inner join users_m on usr_emp_id=ELVA_EMP_ID where emp_status in (1,2) and isnull(emp_bactive,0)=1 and emp_bsu_id='" & Session("sBsuid") & "' and isnull(ELVA_IsDeleted,0)=0 and usr_name='" & Session("sUsr_name") & "') as a where a.ID<>'' " & str_filter_code & str_filter_name & _
               " Union Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_id in (select usr_emp_id from  users_m where usr_name='" & Session("sUsr_name") & "' ) and emp_bsu_id='" & Session("sBsuid") & "')as a where a.ID<>''" & str_filter_code & str_filter_name & " order by a.E_Name" & _
               " ELSE Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_id in (select usr_emp_id from  users_m where usr_name='" & Session("sUsr_name") & "' ) and emp_bsu_id='" & Session("sBsuid") & "')as a where a.ID<>'' " & str_filter_code & str_filter_name & " order by a.E_Name"
                'str_Sql = str_Sql & str_filter_code & str_filter_name

                str_filter_code = ""
                str_filter_name = ""
            ElseIf (SearchMode = "EL" And Session("sBusper") = True) Then
                str_Sql = "Select ID,No, E_Name from( Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from employee_m where emp_bactive=1 and emp_bsu_id='" & Session("sBsuid") & "' )as a where a.ID<>'' " & str_filter_code & str_filter_name & " order by a.E_Name"
                str_filter_code = ""
                str_filter_name = ""

            End If
            If SearchMode = "EL" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name)
            Else
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name & " order by a.E_Name")
            End If

            gvEmpInfo.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEmpInfo.DataBind()
                Dim columnCount As Integer = gvEmpInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvEmpInfo.Rows(0).Cells.Clear()
                gvEmpInfo.Rows(0).Cells.Add(New TableCell)
                gvEmpInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvEmpInfo.DataBind()
            End If
            If SearchMode = "E" Or SearchMode = "EN" Or SearchMode = "EL" Or SearchMode = "RS_EM" Or SearchMode = "RERP" Or SearchMode = "EAR" Or SearchMode = "ER" Then 'V1.2
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Employee ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Employee Name"
                Page.Title = "Employee Info"
            ElseIf SearchMode = "B" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Business ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Business Unit Name"
                Page.Title = "Business Unit Info"
            ElseIf SearchMode = "C" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Category ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Category Name"
                Page.Title = "Category Info"
            ElseIf SearchMode = "DT" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Deduction ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Description"
                Page.Title = "Deduction Info"
            ElseIf SearchMode = "ET" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Earning ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Description"
                Page.Title = "Earning Info"
            End If
            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
            txtSearch.Text = str_txtCode
            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnSearchEmpId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        If Not ViewState("EMPNO") Is Nothing Then
            If ViewState("EMPNO") = "1" Then
                lbClose = sender.Parent.FindControl("LinkButton2")
            End If
        End If
        lblcode = sender.Parent.FindControl("lblID")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text

        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            h_SelectedId.Value = l_Str_Msg
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.Students = document.getElementById('h_SelectedId').value;")
            Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value);")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
        End If
    End Sub

    Protected Sub gvEmpInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpInfo.PageIndexChanging
        gvEmpInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        'lbClose = sender.Parent.FindControl("LinkButton2")
        lbClose = sender.Parent.FindControl("LinkButton1") 'Swapna added
        lblcode = sender.Parent.FindControl("lblID")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            h_SelectedId.Value = l_Str_Msg
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.Students = document.getElementById('h_SelectedId').value;")
            Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value);")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
        End If
    End Sub

End Class

