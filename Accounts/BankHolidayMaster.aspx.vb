﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Partial Class Accounts_BankHolidayMaster
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = ""
            End If
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or ViewState("MainMnu_code") <> "A100003" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                'disable the control based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                initialize()
                If Not Request.QueryString("viewid") Is Nothing AndAlso Request.QueryString("viewid") <> "" Then
                    ViewState("BHY_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    LoadData()
                End If
            End If
        End If
    End Sub
    Private Sub initialize()
        txtFDT.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtTDT.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtComments.Text = ""
        ViewState("BHY_ID") = "0"
        LoadCountry()
    End Sub
    Private Sub Clear()
        txtComments.Text = ""
        txtFDT.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtTDT.Text = Format(Date.Now, "dd/MMM/yyyy")
        ViewState("BHY_ID") = "0"
    End Sub
    Private Sub LoadCountry()
        Dim qry As String = "SELECT CTY_ID,ISNULL(CTY_DESCR,'')CTY_DESCR,ISNULL(CTY_SHORT,'')CTY_SHORT FROM dbo.COUNTRY_M WITH(NOLOCK) ORDER BY CTY_DESCR"
        Dim dsCTY As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Dim CTY_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_COUNTRY_ID FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("sBsuId") & "'")
        If Not dsCTY Is Nothing AndAlso dsCTY.Tables(0).Rows.Count > 0 Then
            ddlCountry.DataSource = dsCTY.Tables(0)
            ddlCountry.DataTextField = "CTY_DESCR"
            ddlCountry.DataValueField = "CTY_ID"
            ddlCountry.DataBind()
            If Not CTY_ID Is Nothing AndAlso CTY_ID <> "" Then
                ddlCountry.SelectedValue = CTY_ID.Trim
            End If
        End If
    End Sub

    Private Sub LoadData()
        Dim objcls As New BankHoliday
        objcls.BHY_ID = ViewState("BHY_ID")
        objcls.LOAD_BANK_HOLIDAYS()
        If objcls.BHY_CTY_ID <> "0" Then
            ddlCountry.SelectedValue = objcls.BHY_CTY_ID
            txtFDT.Text = Format(objcls.BHY_FDT, "dd/MMM/yyyy")
            txtTDT.Text = Format(objcls.BHY_TDT, "dd/MMM/yyyy")
            txtComments.Text = objcls.BHY_COMMENTS
        End If
    End Sub
    Private Function ValidateForm() As Boolean
        ValidateForm = True
        If Not IsDate(txtFDT.Text) Then
            lblError.Text = "Please enter a valid From Date (dd/MMM/yyyy)"
            ValidateForm = False
            Exit Function
        End If
        If Not IsDate(txtTDT.Text) Then
            lblError.Text = "Please enter a valid To Date (dd/MMM/yyyy)"
            ValidateForm = False
            Exit Function
        End If
        If CDate(txtFDT.Text) > CDate(txtTDT.Text) Then
            lblError.Text = "From Date cannot be greater than To Date"
            ValidateForm = False
            Exit Function
        End If
        If txtComments.Text.Trim = "" Then
            lblError.Text = "Comments cannot be empty"
            txtComments.Focus()
            ValidateForm = False
            Exit Function
        End If
    End Function
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If ValidateForm() Then
            Dim objcls As New BankHoliday
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim trans As SqlTransaction = conn.BeginTransaction("BANKHOLIDAY")
            Try
                objcls.BHY_ID = ViewState("BHY_ID")
                objcls.BHY_CTY_ID = ddlCountry.SelectedValue
                objcls.BHY_FDT = txtFDT.Text
                objcls.BHY_TDT = txtTDT.Text
                objcls.BHY_COMMENTS = txtComments.Text.Trim
                objcls.USER = Session("sUsr_name")
                
                Dim retVal As Integer = 1000
                retVal = objcls.SAVE_BANK_HOLIDAYS(conn, trans)
                If retVal = 0 Then
                    trans.Commit()
                    lblError.Text = UtilityObj.getErrorMessage("0")
                    ViewState("datamode") = "add"
                    Clear()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Else
                    trans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(retVal)
                End If
            Catch ex As Exception
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage("1")
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try

        End If
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Not ViewState("BHY_ID") Is Nothing AndAlso ViewState("BHY_ID") <> "0" Then
            Dim objcls As New BankHoliday
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim trans As SqlTransaction = conn.BeginTransaction("BANKHOLIDAY")
            Try
                objcls.BHY_ID = ViewState("BHY_ID")
                objcls.BHY_CTY_ID = "0"
                objcls.BHY_FDT = DateTime.Now.ToShortDateString
                objcls.BHY_TDT = DateTime.Now.ToShortDateString
                objcls.BHY_COMMENTS = ""
                objcls.USER = Session("sUsr_name")

                Dim retVal As Integer = 1000
                retVal = objcls.DELETE_BANK_HOLIDAYS(conn, trans)
                If retVal = 0 Then
                    trans.Commit()
                    lblError.Text = UtilityObj.getErrorMessage("964")
                    ViewState("datamode") = "add"
                    Clear()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Else
                    trans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(retVal)
                End If
            Catch ex As Exception
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage("1")
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call Clear()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
End Class
