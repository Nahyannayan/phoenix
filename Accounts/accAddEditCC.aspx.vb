Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Partial Class accAddEditCC
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Try


                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim MainMnu_code As String

                Dim str_sql As String = ""


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    ViewState("Etype") = Encr_decrData.Decrypt(Request.QueryString("Etype").Replace(" ", "+"))
                    txtID.Attributes.Add("Readonly", "Readonly")
                End If


                'check for the usr_name and the menucode are valid otherwise redirect to login page  Or MainMnu_code = "P050008"

                If USR_NAME = "" Or (MainMnu_code <> "A100030" And MainMnu_code <> "A100035" And MainMnu_code <> "P050008" And MainMnu_code <> "P050007") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    If MainMnu_code = "A100030" Or MainMnu_code = "P050008" Then
                        lblTitle.Text = "City"
                        lblID.Text = "City ID"
                        lblCond.Text = "Country"
                        Dim Temp As String = String.Empty
                        ViewState("Etype") = "C"
                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetCountry_City(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("CIT_ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("CIT_DESCR"))
                                    Temp = Convert.ToString(Userreader("CIT_CTY_ID"))
                                End While
                                'clear of the resource end using
                            End Using
                            Dim ItemTypeCounter As Integer
                            Using CountryReader As SqlDataReader = AccessRoleUser.GetCountry()
                                While CountryReader.Read

                                    ddlCountry.Items.Add(New ListItem(CountryReader("CTY_DESCR").ToString, CountryReader("CTY_ID").ToString))

                                    If ddlCountry.Items(ItemTypeCounter).Value = Temp Then
                                        ddlCountry.SelectedIndex = ItemTypeCounter
                                    End If
                                    ItemTypeCounter = ItemTypeCounter + 1

                                End While
                            End Using

                        Else

                            Using CountryReader As SqlDataReader = AccessRoleUser.GetCountry()
                                While CountryReader.Read

                                    ddlCountry.Items.Add(New ListItem(CountryReader("CTY_DESCR").ToString, CountryReader("CTY_ID").ToString))


                                End While
                            End Using

                        End If
                        ' str_sql = "Select * from (select ECT_ID as id,ECT_DESCR as descr from EMPCATEGORY_M)a where id<>'' "


                    ElseIf MainMnu_code = "A100035" Or MainMnu_code = "P050007" Then
                        lblTitle.Text = "Country"

                        ViewState("Etype") = "G"


                        lblCond.Visible = False
                        '  lbldot.Visible = False
                        ddlCountry.Visible = False

                        If ViewState("datamode") = "view" Then

                            Using Userreader As SqlDataReader = AccessRoleUser.GetCountry_City(ViewState("Eid"), ViewState("Etype"))
                                While Userreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Userreader("CTY_ID"))
                                    txtDesc.Text = Convert.ToString(Userreader("CTY_DESCR"))

                                End While
                                'clear of the resource end using
                            End Using

                        End If


                    End If
                End If
                If ViewState("Etype") = "C" And ViewState("datamode") = "view" Then
                    txtDesc.Attributes.Add("Readonly", "Readonly")
                    txtID.Attributes.Add("Readonly", "Readonly")
                    ddlCountry.Enabled = False
                ElseIf ViewState("Etype") = "G" And ViewState("datamode") = "view" Then
                    txtDesc.Attributes.Add("Readonly", "Readonly")
                    txtID.Attributes.Add("Readonly", "Readonly")
                    ddlCountry.Visible = False

                End If
                If ViewState("Etype") = "C" And ViewState("datamode") = "add" Then
                    txtDesc.Attributes.Remove("readonly")
                    txtID.Attributes.Remove("readonly")
                    ddlCountry.Enabled = True
                ElseIf ViewState("Etype") = "G" And ViewState("datamode") = "add" Then
                    txtDesc.Attributes.Remove("readonly")
                    txtID.Attributes.Remove("readonly")
                    ddlCountry.Visible = False

                End If

            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, "AccAddEditCC pageload")
            End Try

        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If ViewState("Etype") = "C" Then
            txtDesc.Attributes.Remove("readonly")
            txtID.Attributes.Remove("readonly")
            ddlCountry.Enabled = True
        Else
            txtDesc.Attributes.Remove("readonly")
            txtID.Attributes.Remove("readonly")
            ddlCountry.Visible = False

        End If


        Call clearMe()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        Try

       
            UtilityObj.beforeLoopingControls(Me.Page)
            If ViewState("Etype") = "C" Then
                txtDesc.Attributes.Remove("readonly")
                txtID.Attributes.Add("Readonly", "Readonly")
                ddlCountry.Enabled = True
            Else
                txtDesc.Attributes.Remove("readonly")
                txtID.Attributes.Add("Readonly", "Readonly")
                ddlCountry.Visible = False

            End If



            'txtID.Attributes.Remove("readonly")


            'when the edit button is clicked set the datamode to edit
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Page.IsValid = True Then
            Dim Status As Integer
            Dim Descr As String = String.Empty
            Dim Cty_ID As String = String.Empty
            Dim Eid As String = String.Empty
            Eid = txtID.Text


            Descr = txtDesc.Text
            'check the data mode to do the required operation

            If ViewState("datamode") = "edit" Then

                Dim transaction As SqlTransaction

                'update  the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection

                    transaction = conn.BeginTransaction("SampleTransaction")

                    Try

                        If ViewState("Etype") = "C" Then

                            Cty_ID = ddlCountry.SelectedValue
                            Status = AccessRoleUser.UpdateCountryCityID(Eid, Descr, ViewState("Etype"), transaction, Cty_ID)

                        ElseIf ViewState("Etype") = "G" Then
                            Status = AccessRoleUser.UpdateCountryCityID(Eid, Descr, ViewState("Etype"), transaction, Cty_ID)

                        End If
                        'If error occured during the process  throw exception and rollback the process

                        If Status = -1 Then

                            Throw New ArgumentException("Record does not exist!!!")
                        ElseIf Status <> 0 Then
                            Throw New ArgumentException("Record could not be updated")


                        Else
                            'Store the required information into the Audit Trial table when Edited

                            Status = UtilityObj.operOnAudiTable(Master.MenuName, Eid, "edit", Page.User.Identity.Name.ToString, Me.Page)


                            If Status <> 0 Then

                                Throw New ArgumentException("Could not complete your request")

                            End If

                            transaction.Commit()
                            Call clearMe()
                            lblError.Text = "Record Updated Successfully"

                        End If


                    Catch myex As ArgumentException

                        transaction.Rollback()
                        lblError.Text = myex.Message

                    Catch ex As Exception

                        transaction.Rollback()
                        lblError.Text = "Record could not be Updated"

                    End Try


                End Using

            ElseIf ViewState("datamode") = "add" Then

                Dim transaction As SqlTransaction

                'insert the new user
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection

                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try


                        If ViewState("Etype") = "C" Then
                            Cty_ID = ddlCountry.SelectedValue
                            Status = AccessRoleUser.InsertCountryCityID(Eid, Descr, ViewState("Etype"), transaction, Cty_ID)
                        ElseIf ViewState("Etype") = "G" Then
                            Status = AccessRoleUser.InsertCountryCityID(Eid, Descr, ViewState("Etype"), transaction, Cty_ID)

                        End If

                        If Status = -1 Then

                            Throw New ArgumentException("Record already exist!!!")



                        ElseIf Status <> 0 Then

                            Throw New ArgumentException("Record could not be Inserted")


                        Else
                            'Store the required information into the Audit Trial table when inserted

                            Status = UtilityObj.operOnAudiTable(Master.MenuName, Eid, "insert", Page.User.Identity.Name.ToString, Me.Page)


                            If Status <> 0 Then


                                Throw New ArgumentException("Could not complete your request")

                            End If


                            transaction.Commit()
                            Call clearMe()
                            lblError.Text = "Record Inserted Successfully"

                        End If




                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()

                        lblError.Text = "Record could not be Inserted"

                    End Try
                End Using
            End If






            ViewState("datamode") = "none"

            If ViewState("Etype") = "C" And ViewState("datamode") = "none" Then
                txtDesc.Attributes.Add("Readonly", "Readonly")
                txtID.Attributes.Add("Readonly", "Readonly")
                ddlCountry.Enabled = False
            ElseIf ViewState("Etype") = "G" And ViewState("datamode") = "none" Then
                txtDesc.Attributes.Add("Readonly", "Readonly")
                txtID.Attributes.Add("Readonly", "Readonly")
                ddlCountry.Visible = False

            End If



            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        End If






    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearMe()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"
            If ViewState("Etype") = "C" And ViewState("datamode") = "none" Then
                txtDesc.Attributes.Add("Readonly", "Readonly")
                txtID.Attributes.Add("Readonly", "Readonly")
                ddlCountry.Enabled = False
            ElseIf ViewState("Etype") = "G" And ViewState("datamode") = "none" Then
                txtDesc.Attributes.Add("Readonly", "Readonly")
                txtID.Attributes.Add("Readonly", "Readonly")
                ddlCountry.Visible = False

            End If

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        Dim Cid As String = String.Empty

        'Delete  the  user
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                If ViewState("Etype") = "C" Then
                    Status = AccessRoleUser.DeleteCountryCity(ViewState("Eid"), ViewState("Etype"), transaction)
                ElseIf ViewState("Etype") = "G" Then
                    Status = AccessRoleUser.DeleteCountryCity(ViewState("Eid"), ViewState("Etype"), transaction)

                End If

                If Status = -1 Then

                    Throw New ArgumentException("Record does not exist!!!")
                ElseIf Status <> 0 Then
                    Throw New ArgumentException("Record could not be Deleted")
                Else

                    Status = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("Eid"), "delete", Page.User.Identity.Name.ToString, Me.Page)


                    If Status <> 0 Then


                        Throw New ArgumentException("Could not complete your request")

                    End If

                    transaction.Commit()
                    Call clearMe()
                    lblError.Text = "Record Deleted Successfully"

                End If

            Catch myex As ArgumentException
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message)
                transaction.Rollback()

            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using



        ViewState("datamode") = "none"




        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Sub clearMe()
        Try


            txtID.Text = ""
            txtDesc.Text = ""
            If ViewState("Etype") = "C" Then
                ddlCountry.SelectedIndex = 0
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub
End Class
