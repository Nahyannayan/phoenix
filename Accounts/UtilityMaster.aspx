﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="UtilityMaster.aspx.vb" Inherits="Accounts_UtilityMaster" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
    function confirm_delete() {

        if (confirm("You are about to delete this record.Do you want to proceed?") == true)
            return true;
        else
            return false;
    }
function getAccountName() {
        var sFeatures;
        var lstrVal;
        var lintScrVal;
        var pMode;
        var NameandCode;
        sFeatures = "dialogWidth: 760px; ";
        sFeatures += "dialogHeight: 420px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        pMode = "ACCOUNTSLISTFORUTILITY"
        <%--url = "../common/PopupSelect.aspx?id=" + pMode;
        result = window.showModalDialog(url, "", sFeatures);
        if (result == '' || result == undefined) return false;
        NameandCode = result.split('___');
        document.getElementById("<%=TxtActName.ClientID %>").value = NameandCode[1];
        document.getElementById("<%=txtAct_Id.ClientID %>").value = NameandCode[0];--%>
        
    var url = "../common/PopupSelect.aspx?id=" + pMode;
    var oWnd = radopen(url, "pop_accname");
                        
} 

function OnClientClose1(oWnd, args) {
    //get the transferred arguments

    var arg = args.get_argument();

    if (arg) {
               
        NameandCode = arg.NameandCode.split('||');
        document.getElementById("<%=TxtActName.ClientID %>").value = NameandCode[1];
        document.getElementById("<%=txtAct_Id.ClientID %>").value = NameandCode[0];
                __doPostBack('<%=txtAct_Id.ClientID%>', 'TextChanged');
            }
        }
    

   <%-- function getBDate() {
        var sFeatures;
        sFeatures = "dialogWidth: 229px; ";
        sFeatures += "dialogHeight: 252px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        result = window.showModalDialog("calendar.aspx?nofuture=&dt=" + document.getElementById('<%=txtBillDate.ClientID %>').value, "", sFeatures)
        if (result == '' || result == undefined) {
                 return false;
        }
        document.getElementById('<%=txtBillDate.ClientID %>').value = result;
        return true;
    }
    
    function getPDate() {
        var sFeatures;
        sFeatures = "dialogWidth: 229px; ";
        sFeatures += "dialogHeight: 252px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        result = window.showModalDialog("calendar.aspx?nofuture=&dt=" + document.getElementById('<%=txtPayDate.ClientID %>').value, "", sFeatures)
        if (result == '' || result == undefined) {
            return false;
        }
        document.getElementById('<%=txtPayDate.ClientID %>').value = result;
        return true;
    }      --%>


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_accname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-tasks mr-3"></i> Utility Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">
        <tr>
            <td align="left" width="100%">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td></tr>
         <tr>
            <td align="left">
                <table align="center" width="100%">
                   
                   <tr>
                    <td align="left" width="20%"><span class="field-label">Business Unit</span><span class="text-danger">*</span></td>
                    <td align="left" width="30%">
                            <asp:TextBox ID="txtBUID" runat="server" Enabled="false"></asp:TextBox>
                            <asp:TextBox ID="TxtBUName" runat="server" Enabled=false ></asp:TextBox></td>
                       <td align="left" width="20%"><span class="field-label">Account ID</span><span class="text-danger">*</span></td>
                    <td align="left"  width="30%">
                            <asp:TextBox ID="txtAct_Id" runat="server"></asp:TextBox>
                            <asp:TextBox ID="TxtActName" runat="server" Enabled="false" ></asp:TextBox>
                            <asp:ImageButton ID="imgICMDescr" runat="server" OnClientClick="getAccountName();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" /></td>
                   </tr>
                      
                   <tr>    
                    <td align="left" width="20%"><span class="field-label">Bill Date</span><span class="text-danger">*</span></td>
                    <td align="left" width="30%">
                            <asp:TextBox ID="txtBillDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btnHdate" runat="server" ImageUrl="~/Images/calendar.gif"/>
                        <ajaxToolkit:CalendarExtender ID="BillDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="btnHdate" TargetControlID="txtBillDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="BillDate1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtBillDate" TargetControlID="txtBillDate">
                            </ajaxToolkit:CalendarExtender>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Payment Date</span><span class="text-danger">*</span></td>
                    <td align="left" width="30%" >
                            <asp:TextBox ID="txtPayDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btnPdate" runat="server" ImageUrl="~/Images/calendar.gif"/>
                            <ajaxToolkit:CalendarExtender ID="PayDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="btnPdate" TargetControlID="txtPayDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="PayDate1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtPayDate" TargetControlID="txtPayDate">
                            </ajaxToolkit:CalendarExtender>
                    </td>
                   </tr>
                   <tr>    
                    <td align="left" width="20%"><span class="field-label">Document Number</span><span class="text-danger">*</span></td>
                    <td align="left" width="30%">
                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox></td>
                    <td align="left" width="20%"><span class="field-label">Duration Type</span><span class="text-danger">*</span></td>
                    <td align="left" width="30%">
                             <asp:DropDownList ID="ddlDurType" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="DAYS"></asp:ListItem>
                                <asp:ListItem>MONTHS</asp:ListItem>
                                <asp:ListItem>YEARS</asp:ListItem></asp:DropDownList></td></tr>
                   <tr>    
                    <td align="left" width="20%"><span class="field-label">Default Amount</span></td>
                    <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
                    <td align="left" width="20%"><span class="field-label">Period</span><span class="text-danger">*</span></td>
                    <td align="left" width="30%">
                            <asp:TextBox ID="txtDuration" runat="server"></asp:TextBox></td></tr>
                </table>
            </td>
        </tr>
       
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                    <asp:HiddenField ID="h_UTI_ID" runat="server" /></td>
        </tr>
        
    </table>
    
            </div>
        </div>
    </div>

</asp:Content>

