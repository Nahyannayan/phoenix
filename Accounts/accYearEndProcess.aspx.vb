Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_accYearEndProcess
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                bindBusinessunit()
                btnPrint.Visible = False
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim MainMnu_code As String = String.Empty
                'collect the url of the file to be redirected in view state 
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                setCalendar()
                txtTo.Attributes.Add("readonly", "readonly")
                If USR_NAME = "" Or (MainMnu_code <> OASISConstants.AccountsYearEnd) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling page right class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub


    Sub bindBusinessunit()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            If Session("sBusper") = True Then
                str_Sql = "SELECT    BSU_NAME , BSU_ID as BSUID FROM BUSINESSUNIT_M order by  BSU_NAME "
            Else
                str_Sql = "select us.usa_bsu_id as BSUID, bu.bsu_name as BSU_NAME  from useraccess_s as us " & _
                " join businessunit_m as bu on us.usa_bsu_id=bu.bsu_id where usa_usr_id='" & Session("sUsr_id") & "'  union " & _
                " select um.usr_bsu_id as BSUID, bu.bsu_name as BSU_NAME from users_m as um join " & _
                " businessunit_m as bu on um.usr_bsu_id=bu.bsu_id where usr_id='" & Session("sUsr_id") & "' order by BSU_NAME"
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddBusinessunit.Items.Clear()
            ddBusinessunit.DataSource = ds.Tables(0)
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSUID"
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            For Each item As ListItem In ddBusinessunit.Items
                If item.Value.ToString.Contains(Session("sBSUID")) Then
                    item.Selected = True
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not IsDate(txtTo.Text) Then
            lblError.Text = "Invalid Date <br>"
            Exit Sub
        End If
        If Not IsNumeric(txtExgrate.Text) Then
            lblError.Text = "Invalid Exchange Rate <br>"
            Exit Sub
        End If
        If rbHardClose.Checked Then
            If Not (chkCondition1.Checked And chkCondition2.Checked And chkCondition3.Checked And chkCondition3.Checked) Then
                lblError.Text = "Please Do all the year end transactions and Check all the check boxes to continue"
                Exit Sub
            End If
        End If

        If Not UsrCheckList1.MandatoryFieldsChecked Then
            lblError.Text = "Please select All Mandatory Check List"
            Exit Sub
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Dim conn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction

        Try
            '            EXEC  @return_value = [dbo].[DoYearEndProcess]
            '            @BSUID = N'125003',
            '            @DOCDT = N'31/mar/2008',
            '            @JHD_NEWDOCNO = @JHD_NEWDOCNO OUTPUT,
            '            @JHD_EXGRATE2 = 1

            If Not UsrCheckList1.SaveCheckListDetails(conn, trans, txtTo.Text, ddBusinessunit.SelectedItem.Value.Split("||")(0)) Then
                lblError.Text = "Please select All Mandatory Check List"
                trans.Rollback()
                stTrans.Rollback()
                Exit Sub
            End If

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
            pParms(0).Value = ddBusinessunit.SelectedItem.Value
            pParms(1) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
            pParms(1).Value = txtTo.Text
            pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            pParms(3) = New SqlClient.SqlParameter("@JHD_NEWDOCNO", SqlDbType.VarChar, 20)
            pParms(3).Direction = ParameterDirection.Output
            pParms(4) = New SqlClient.SqlParameter("@JHD_EXGRATE2", SqlDbType.Decimal)
            pParms(4).Value = txtExgrate.Text
            pParms(5) = New SqlClient.SqlParameter("@Typ", SqlDbType.VarChar, 2)
            If rbSoftClose.Checked Then
                pParms(5).Value = "S"
            Else
                pParms(5).Value = "H"
            End If
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "DoYearEndProcess", pParms)
            If pParms(2).Value = "0" Then
                lblError.Text = "Document Generated : " & pParms(3).Value & "<br>" & getErrorMessage("0")
                ViewState("docno") = pParms(3).Value
                ViewState("docdate") = txtTo.Text
                btnPrint.Visible = True
                stTrans.Commit()
                trans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("docno"), "Freeze", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            Else
                lblError.Text = getErrorMessage(pParms(2).Value)
                stTrans.Rollback()
                trans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            trans.Rollback()
            Errorlog(ex.Message)
            lblError.Text = getErrorMessage("1000")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            conn.Close()
        End Try
    End Sub


    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim repSource As New MyReportClass
        repSource = VoucherReports.JournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "JV", ViewState("docno"), Session("HideCC"))
        Session("ReportSource") = repSource
        Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
    End Sub
    Protected Sub rbSoftClose_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSoftClose.CheckedChanged
        setCalendar()
    End Sub

    Sub setCalendar()
        If rbSoftClose.Checked Then
            imgFromDate.Visible = True
        Else
            imgFromDate.Visible = False
            Dim str_sql As String = "SELECT REPLACE(CONVERT(VARCHAR(11), FYR_TODT, 113), ' ', '/') FROM FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "'"
            txtTo.Text = UtilityObj.GetDataFromSQL(str_sql, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            txtTo.Text = String.Format(txtTo.Text, "dd/MMM/yyyy")
        End If
    End Sub

    Protected Sub rbHardClose_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbHardClose.CheckedChanged
        setCalendar()
    End Sub
End Class
