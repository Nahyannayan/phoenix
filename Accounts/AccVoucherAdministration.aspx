<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AccVoucherAdministration.aspx.vb" Inherits="Accounts_AccVoucherAdministration"
    Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="../UserControls/usrCostCenter.ascx" TagName="usrCostCenter" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function GetDocNo() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 350px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var ddlbsu = document.getElementById('<%= ddlBSU.ClientID %>');
            var bsu = ddlbsu.options[ddlbsu.selectedIndex].value;
            //            var bsu = document.getElementById('<%= h_BSU_ID.ClientID %>').value;
            var subid = document.getElementById('<%= h_SUB_ID.ClientID %>').value;
            var ddl = document.getElementById('<%= ddDocumentType.ClientID %>');
            var docType = ddl.options[ddl.selectedIndex].value;
            var FYEAR = document.getElementById('<%= h_FYEAR.ClientID %>').value;

            pMode = "VOUCHERADMINISTRATION"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu + "&subid=" + subid + "&doctype=" + docType + "&FYEAR=" + FYEAR;
            result = radopen(url, "pop_up2")
            <%--result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            //            alert(result);
            NameandCode = result.split('___');
            document.getElementById('<%= h_DocType.ClientID %>').value = NameandCode[2];
            document.getElementById('<%= h_DocNo.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtDocNo.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= h_DocDate.ClientID %>').value = NameandCode[1];--%>
        }

        function GetIJVDocNo() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 350px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var ddlbsu1 = document.getElementById('<%= ddlBSU1.ClientID %>');
            var bsu1 = ddlbsu1.options[ddlbsu1.selectedIndex].value;
            var subid1 = document.getElementById('<%= h_SUB_ID.ClientID %>').value;
            var FYEAR1 = document.getElementById('<%= h_FYEAR.ClientID %>').value;
            pMode = "VOUCHERADMINISTRATIONIJV"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu1 + "&subid=" + subid1 + "&FYEAR=" + FYEAR1;
            result = radopen(url, "pop_up3")
            <%--result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%= txtDocNo1.ClientID %>').value = NameandCode[0];--%>

        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                
                document.getElementById('<%= h_DocType.ClientID %>').value = NameandCode[2];
                document.getElementById('<%= h_DocNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtDocNo.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= h_DocDate.ClientID %>').value = NameandCode[1];
            }
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%= txtDocNo1.ClientID %>').value = NameandCode[0];
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="CCAlloc" />
                <br />
                <table id="tblMain" runat="server" align="center" width="100%">
                    <tr>
                        <td>
                            <act:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                <act:TabPanel ID="TabPanel1" HeaderText="Student Charges" runat="server">
                                    <HeaderTemplate>
                                        Edit Voucher Details
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table align="left" width="100%">

                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                                                <td align="left" colspan="3">
                                                    <asp:DropDownList ID="ddlBSU" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Document Type</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddDocumentType" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">Document No</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox><asp:ImageButton
                                                        ID="imgDocNo" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetDocNo(); return false;" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="20%" align="left"><span class="field-label">Received By</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtReceivedby" runat="server" MaxLength="300"></asp:TextBox>
                                                </td>
                                                <td align="left" width="20%">
                                                    <asp:CheckBox ID="ChkBearer" runat="server" Text="Bearer Cheque" />
                                                </td>
                                                <td align="left" width="30%"></td>
                                            </tr>




                                            <tr>
                                                <td align="left" colspan="4">
                                                    <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" Width="100%" Style="text-align: center"
                                                        DataKeyNames="VHD_ID">
                                                        <Columns>
                                                            <asp:BoundField DataField="ACT_ID" HeaderText="Account Code"></asp:BoundField>
                                                            <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" />
                                                            <asp:BoundField DataField="VHD_AMOUNT" HeaderText="Amount"
                                                                SortExpression="VHD_AMOUNT">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="VHD_CHQNO" HeaderText="ChequeNo"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="NewChq.No">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtChqNo" runat="server" Text='<%# Bind("VHD_CHQNO") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="VHD_CHQDT" HeaderText="ChequeDate" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <ItemStyle Width="15%" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="New Chq.Date">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtChqDt" runat="server" Width="80px" MaxLength="11" Text='<%# Bind("VHD_CHQDT", "{0:dd/MMM/yyyy}") %>'></asp:TextBox><asp:ImageButton
                                                                        ID="imgFDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton><act:CalendarExtender
                                                                            ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                                                            PopupButtonID="imgFDT" TargetControlID="txtChqDt">
                                                                        </act:CalendarExtender>
                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="VHD_NARRATION" HeaderText="Narration" />
                                                            <asp:TemplateField HeaderText="New Narration">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtNarration" runat="server"
                                                                        TextMode="MultiLine" MaxLength="300" Text='<%# Bind("VHD_NARRATION") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ReUse Old Cheque">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkUnAllocate" runat="server" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4">
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="chqBR" />
                                                    <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                                                    <asp:HiddenField ID="h_SUB_ID" runat="server" />
                                                    <asp:HiddenField ID="h_BSU_ID" runat="server" />
                                                    <asp:HiddenField ID="h_DocType" runat="server" />
                                                    <asp:HiddenField ID="h_DocNo" runat="server" />
                                                    <asp:HiddenField ID="h_NextLine" runat="server" />
                                                    <asp:HiddenField ID="h_EditID" runat="server" />
                                                    <br />
                                                    <asp:HiddenField ID="h_DocDate" runat="server" />
                                                    <asp:HiddenField ID="h_FYEAR" runat="server" />
                                                    <asp:TextBox ID="Detail_ACT_ID" runat="server" AutoCompleteType="Disabled"
                                                        Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </act:TabPanel>

                                <act:TabPanel ID="TabPanel2" HeaderText="Student Charges" runat="server">
                                    <HeaderTemplate>
                                        Unlock  IJV Voucher
                                    </HeaderTemplate>

                                    <ContentTemplate>
                                        <table align="left" width="100%">
                                            <tr class="title-bg">
                                                <td align="left" colspan="4">
                                                    <asp:Label ID="Label1" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span></td>

                                                <td align="left" colspan="3">
                                                    <asp:DropDownList ID="ddlBSU1" runat="server"></asp:DropDownList></td>
                                            </tr>
                                            <%--<tr><td align="left" class="matters" width="25%">Document Type </td><td class="matters" width="1%">: </td><td align="left" class="matters" width="75%"><asp:DropDownList ID="DropDownList2" runat="server" Width="148px"></asp:DropDownList></td></tr>--%>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Document No</span> </td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocNo1" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgDocNo1" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetIJVDocNo(); return false;" /></td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>
                                            <%--<tr><td align="left" class="matters" colspan="3"><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                            SkinID="GridViewNormal" Width="100%" style="text-align: center" 
                                            DataKeyNames="VHD_ID"><Columns><asp:BoundField DataField="ACT_ID" HeaderText="Account Code"></asp:BoundField><asp:BoundField DataField="ACT_NAME" HeaderText="Account Name" /><asp:BoundField DataField="VHD_AMOUNT" HeaderText="Amount" 
                                                    SortExpression="VHD_AMOUNT"><ItemStyle HorizontalAlign="Right" /></asp:BoundField><asp:BoundField DataField="VHD_CHQNO" HeaderText="ChequeNo"></asp:BoundField><asp:TemplateField HeaderText="NewChq.No"><ItemTemplate><asp:TextBox ID="txtChqNo" runat="server" Width="80px" Text='<%# Bind("VHD_CHQNO") %>'></asp:TextBox></ItemTemplate></asp:TemplateField><asp:BoundField DataField="VHD_CHQDT" HeaderText="ChequeDate" dataformatstring="{0:dd/MMM/yyyy}"><ItemStyle Width="15%" /></asp:BoundField><asp:TemplateField HeaderText="New Chq.Date"><ItemTemplate><asp:TextBox ID="txtChqDt" runat="server" Width="80px" MaxLength="11" Text='<%# Bind("VHD_CHQDT", "{0:dd/MMM/yyyy}") %>'></asp:TextBox><asp:ImageButton
                                                        ID="imgFDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton><act:CalendarExtender
                                                            ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                                            PopupButtonID="imgFDT" TargetControlID="txtChqDt"></act:CalendarExtender></ItemTemplate></asp:TemplateField><asp:BoundField DataField="VHD_NARRATION" HeaderText="Narration" /><asp:TemplateField HeaderText="New Narration"><ItemTemplate><asp:TextBox ID="txtNarration" runat="server" style="Height :100px;"
                                                            TextMode="MultiLine" MaxLength="300" Width="200px" Text='<%# Bind("VHD_NARRATION") %>'></asp:TextBox></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Deallocate Old Cheque"><ItemTemplate><asp:CheckBox ID="chkUnAllocate" runat="server" /></ItemTemplate><ItemStyle HorizontalAlign="Center" /></asp:TemplateField></Columns><HeaderStyle HorizontalAlign="Center" /></asp:GridView></td></tr>--%>
                                            <tr>
                                                <td align="center" colspan="4">
                                                    <asp:Button ID="btnUnlock" runat="server" CssClass="button" Text="Un Lock" />
                                                    <asp:Button ID="btnCancel1" runat="server" CssClass="button" Text="Cancel" UseSubmitBehavior="False" /></td>
                                            </tr>
                                        </table>


                                    </ContentTemplate>

                                </act:TabPanel>
                            </act:TabContainer>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
