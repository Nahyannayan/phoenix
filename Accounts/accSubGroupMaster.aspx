<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accSubGroupMaster.aspx.vb" Inherits="SubGroupMaster" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        //not in use
        function validate_add() {

            if (document.getElementById("txtGroupcode").value == '') {
                alert("Kindly enter Group Code");
                return false;
            }
            if (document.getElementById("txtGroupname").value == '') {
                alert("Kindly enter Group Name");
                return false;
            }
            return true;
        }

    </script>

    

        <div class="card mb-3">
            <div class="card-header letter-space">
                <i class="fa fa-calculator mr-3"></i>
                Sub Group Master
            </div>
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                                <table align="center" width="100%">
                                    <tr>
                                        <td colspan="4">
                                            <asp:GridView ID="GridViewSubGroupMaster" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                DataKeyNames="SGP_ID" HeaderStyle-Height="30" Width="100%" AllowPaging="True">
                                                <Columns>
                                                    <asp:BoundField DataField="GUID" HeaderText="GUID" SortExpression="GUID" Visible="False" />
                                                    <asp:TemplateField HeaderText="Sub Group Code">
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lblsubgroupcode" runat="server" Text='<%# Eval("SGP_ID") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsubgroupcode" runat="server" Text='<%# Bind("SGP_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                        <HeaderTemplate>
                                                            Sub Group Code
                                                <br />
                                                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                                        </HeaderTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sub Group Name" SortExpression="SGP_DESCR">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtSubName" runat="server" Text='<%# Bind("SGP_DESCR") %>' Width="98%"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("SGP_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                        <HeaderTemplate>
                                                            Sub Group Name
                                                <br />
                                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="btnSearchName_Click" />
                                                        </HeaderTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Parent Group" SortExpression="SGP_GPM_ID">
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="DropDownListParentedit" runat="server" CssClass="listbox" DataSourceID="SqlDataSourceDDParent"
                                                                DataTextField="GPM_DESCR" DataValueField="GPM_ID" SelectedValue='<%# Bind("GPM_ID") %>'>
                                                            </asp:DropDownList><asp:SqlDataSource ID="SqlDataSourceDDParent" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                                                SelectCommand="SELECT * FROM [ACCGRP_M]"></asp:SqlDataSource>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("GPM_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            Parent Group Name
                                                <br />
                                                            <asp:TextBox ID="txtParent" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchpar" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchpar_Click" />

                                                        </HeaderTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type" SortExpression="SGP_TYPE">
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="DropDownListType" runat="server" CssClass="listbox" SelectedValue='<%# Bind("SGP_TYPE_CODE") %>'>
                                                                <asp:ListItem Value="N">Normal</asp:ListItem>
                                                                <asp:ListItem Value="B">Bank</asp:ListItem>
                                                                <asp:ListItem Value="C">Cash</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("SGP_TYPE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <HeaderTemplate>
                                                            Bank Or Cash<br />
                                                            <asp:DropDownList ID="DropDownListType" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                OnSelectedIndexChanged="DropDownListType_SelectedIndexChanged">
                                                                <asp:ListItem Value="A">All</asp:ListItem>
                                                                <asp:ListItem Value="N">Normal</asp:ListItem>
                                                                <asp:ListItem Value="B">Bank</asp:ListItem>
                                                                <asp:ListItem Value="C">Cash</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%# Eval("SGP_ID", "NewSubGroup.aspx?editid={0}") %>'
                                                                Text="View"></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Edit" ShowEditButton="True" Visible="False">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:CommandField>
                                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False" Visible="False">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete"
                                                                OnClientClick='return confirm("Are you sure to delete this ?");' Text="Delete"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="griditem" />
                                                <HeaderStyle CssClass="gridheader_pop" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                <SelectedRowStyle CssClass="griditem_hilight" />

                                            </asp:GridView>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
</asp:Content>

