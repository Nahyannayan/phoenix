Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Accounts_acccuCostUnit
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String 
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            bind_costtype()
            lblError.Text = ""
            txtPAccountcode.Attributes.Add("readonly", "readonly")
            txtPAccountname.Attributes.Add("readonly", "readonly")
            txtEAccountcode.Attributes.Add("readonly", "readonly")
            txtEAccountname.Attributes.Add("readonly", "readonly")
            ''''' 
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A150005"
            If Not Request.UrlReferrer Is Nothing Then
                viewstate("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                viewstate("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (MainMnu_code <> "A100070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                viewstate("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))

            End If
            '''' 
            If Request.QueryString("viewid") <> "" Then

                setViewData()
                setModifyHeader(Request.QueryString("viewid"))
            Else
                ResetViewData()

            End If
            If Request.QueryString("deleted") <> "" Then
                lblError.Text = getErrorMessage("520")
            End If
        End If
    End Sub

    Sub bind_costtype()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT  COST_ID ,COST_DESCR   FROM  COSTTYPE_M"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            ddCosttype.DataSource = ds.Tables(0)
            ddCosttype.DataTextField = "COST_DESCR"
            ddCosttype.DataValueField = "COST_ID"
            ddCosttype.DataBind()
            ddCosttype.Items.FindByValue(ds.Tables(0).Rows(0)("CUT_TYPE")).Selected = True
            ddCosttype.SelectedIndex = 0
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub setViewData()
        txtCostdescr.Attributes.Add("readonly", "readonly")
        txtCostItem.Attributes.Add("readonly", "readonly")
        ImageButton1.Enabled = False
        btnHaccount.Enabled = False
        ddCosttype.Enabled = False
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT     COSTUNIT_M.GUID, COSTUNIT_M.CUT_ID, " _
  & " COSTUNIT_M.CUT_DESCR, COSTUNIT_M.CUT_TYPE, " _
  & " COSTUNIT_M.CUT_ITEM, COSTUNIT_M.CUT_PPAID_ACT_ID, " _
  & " COSTUNIT_M.CUT_EXP_ACT_ID, PPD.ACT_NAME AS PPDNAME, " _
  & " EXP.ACT_NAME AS EXPNAME" _
  & " FROM COSTUNIT_M INNER JOIN" _
  & " ACCOUNTS_M AS PPD ON " _
  & " COSTUNIT_M.CUT_PPAID_ACT_ID = PPD.ACT_ID " _
  & " INNER JOIN ACCOUNTS_M AS EXP " _
  & " ON COSTUNIT_M.CUT_EXP_ACT_ID = EXP.ACT_ID" _
  & "  WHERE COSTUNIT_M.GUID='" & p_Modifyid & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtCostdescr.Text = ds.Tables(0).Rows(0)("CUT_DESCR")
                txtCostItem.Text = ds.Tables(0).Rows(0)("CUT_ITEM")
                txtEAccountcode.Text = ds.Tables(0).Rows(0)("CUT_EXP_ACT_ID")
                txtEAccountname.Text = ds.Tables(0).Rows(0)("EXPNAME")
                txtPAccountcode.Text = ds.Tables(0).Rows(0)("CUT_PPAID_ACT_ID")
                txtPAccountname.Text = ds.Tables(0).Rows(0)("PPDNAME")
                h_editid.Value = ds.Tables(0).Rows(0)("CUT_ID")

                ddCosttype.SelectedIndex = -1
                ddCosttype.Items.FindByValue(ds.Tables(0).Rows(0)("CUT_TYPE")).Selected = True
                '
            Else
            End If
        Catch ex As Exception
            ddCosttype.SelectedIndex = 0
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ResetViewData()
        txtCostdescr.Attributes.Remove("readonly")
        txtCostItem.Attributes.Remove("readonly")
        ImageButton1.Enabled = True
        btnHaccount.Enabled = True
        ddCosttype.Enabled = True
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim bool_edit As Boolean = False

            If viewstate("datamode") = "edit" Then
                bool_edit = True
            End If

            Dim returnvalue As String = MasterFunctions.SAVECOSTUNIT_M(h_editid.Value, txtCostdescr.Text.Trim, ddCosttype.SelectedItem.Value, txtCostItem.Text.Trim, txtPAccountcode.Text.Trim, txtEAccountcode.Text.Trim, bool_edit, stTrans)
            If returnvalue <> "0" Then
                stTrans.Rollback()
                lblError.Text = getErrorMessage(returnvalue)
            Else
                lblError.Text = getErrorMessage(returnvalue)
                stTrans.Commit()
                clear_all()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "Duplicate Record"
            Errorlog(ex.Message, "Save costunit")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub


    Sub clear_all()
        txtCostdescr.Text = ""
        txtCostItem.Text = ""
        txtEAccountcode.Text = ""
        txtEAccountname.Text = ""
        txtPAccountcode.Text = ""
        txtPAccountname.Text = ""

    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(viewstate("ReferrerUrl"))
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ResetViewData()
        viewstate("datamode") = "edit"

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), viewstate("menu_rights"), viewstate("datamode"))

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim bool_edit As Boolean = False


            Dim returnvalue As String = MasterFunctions.DeleteCOSTUNIT_M(h_editid.Value, stTrans)
            If returnvalue <> "0" Then
                stTrans.Rollback()
                lblError.Text = getErrorMessage(returnvalue)
            Else
                lblError.Text = getErrorMessage(returnvalue)
                stTrans.Commit()
                clear_all()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "error"
            Errorlog(ex.Message, "Save costunit")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
