Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class AccMstAccounts
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            hlAddNew.NavigateUrl = String.Format("~/accounts/AccAddNewAccount.aspx?MainMnu_code={0}&datamode={1}", Request.QueryString("MainMnu_code"), Encr_decrData.Encrypt("add"))



            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim MainMnu_code As String = String.Empty
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (MainMnu_code <> "A100010" And MainMnu_code <> "A100002") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling page right class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                    'disable the control based on the rights



                    gvGroup1.Attributes.Add("bordercolor", "#1b80b6")

                    h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

                    rbActive.Checked = True
                    gridbind()
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If

            set_Menu_Img()

    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup1.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup1.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup1.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup1.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub gridbind()
        Try
            Dim Bactive As String = String.Empty
            If rbActive.Checked = True Then
                Bactive = " And AM.Act_BActive=1 "
            ElseIf rbInactive.Checked = True Then
                Bactive = " And AM.Act_BActive=0 "
            End If

            Dim str_filter_acctype, str_filter_bankcash, str_filter_custsupp, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String
            str_mode = Request.QueryString("mode")
            Response.Write(str_mode)
            Dim i_dd_bank As Integer = 0
            Dim i_dd_acctype As Integer = 0
            Dim i_dd_custsupp As Integer = 0

            str_filter_bankcash = ""
            str_filter_custsupp = ""
            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""

            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""

            Dim ddbank As New DropDownList
            Dim ddcust As New DropDownList
            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup1.Rows.Count > 0 Then
                Try
                    Dim s As HtmlControls.HtmlImage = gvGroup1.HeaderRow.FindControl("mnu_2_img")
                    ddacctype = gvGroup1.HeaderRow.FindControl("DDAccountType")
                    ddbank = gvGroup1.HeaderRow.FindControl("DDBankorCash")
                Catch ex As Exception
                End Try

                If ddacctype.SelectedItem.Value <> "All" Then
                    str_filter_acctype = " AND AM.ACT_TYPE='" & ddacctype.SelectedItem.Value & "'"
                End If
                i_dd_bank = ddbank.SelectedIndex
                i_dd_acctype = ddacctype.SelectedIndex
                i_dd_custsupp = ddcust.SelectedIndex
                If ddbank.SelectedItem.Value = "B" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='B'"
                ElseIf ddbank.SelectedItem.Value = "C" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='C'"
                ElseIf ddbank.SelectedItem.Value = "N" Then
                    str_filter_bankcash = " AND AM.ACT_BANKCASH='N'"
                End If

                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup1.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND AM.ACT_ID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND AM.ACT_ID NOT LIKE '%" & txtSearch.Text & "'"
                End If
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvGroup1.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND AM.ACT_NAME LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND AM.ACT_NAME NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = " SELECT" _
            & " AM.ACT_ID ,AM.ACT_NAME ,AM.ACT_SGP_ID ,ASG.SGP_Descr,AM.ACT_TYPE," _
            & " CASE AM.ACT_BANKCASH" _
            & " WHEN 'B' THEN 'Bank' " _
            & " WHEN 'C' THEN 'Cash'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Not Applicable' END " _
            & " ACT_BANKCASH" _
            & " ,AM.ACT_CTRLACC ,AM.ACT_Bctrlac ," _
            & " CASE AM.ACT_FLAG" _
            & " WHEN 'S' THEN 'Supliers'" _
            & " WHEN 'C' THEN 'Customers'" _
            & " WHEN 'N' THEN 'Normal'" _
            & " ELSE 'Others' END" _
            & " ACT_FLAG " _
            & " FROM ACCOUNTS_M AM,ACCSGRP_M ASG" _
            & " WHERE AM.ACT_SGP_ID=ASG.SGP_ID"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & Bactive & str_filter_bankcash & str_filter_acctype & str_filter_custsupp & str_filter_code & str_filter_name & str_filter_control)
            gvGroup1.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup1.DataBind()
                Dim columnCount As Integer = gvGroup1.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup1.Rows(0).Cells.Clear()
                gvGroup1.Rows(0).Cells.Add(New TableCell)
                gvGroup1.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup1.DataBind()
            End If
            '  gvGroup.Columns(0).Visible = False
            'ddcust = gvGroup1.HeaderRow.FindControl("DDCutomerSupplier")
            ddacctype = gvGroup1.HeaderRow.FindControl("DDAccountType")
            ddbank = gvGroup1.HeaderRow.FindControl("DDBankorCash")

            ddbank.SelectedIndex = i_dd_bank
            ddacctype.SelectedIndex = i_dd_acctype
            'ddcust.SelectedIndex = i_dd_custsupp

            txtSearch = gvGroup1.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup1.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            'txtSearch = gvGroup1.HeaderRow.FindControl("txtControl")
            'txtSearch.Text = str_txtControl

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    

    Protected Sub gvGroup1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup1.PageIndexChanging
        gvGroup1.PageIndex = e.NewPageIndex
        gridbind()
    End Sub




    'Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup1.RowDataBound
    '    Try
    '        Dim cmdCol As Integer = gvGroup1.Columns.Count - 1

    '        For Each ctrl As Control In e.Row.Cells(cmdCol).Controls
    '            ' ''Dim lblCode As Label

    '            ' ''lblCode = e.Row.FindControl("lblGroupcode")
    '            '' ''lblGroupName
    '            '' ''lblName = e.Row.FindControl("lblGroupName")
    '            '' ''lblParent = e.Row.FindControl("lblPG")
    '            ' ''Dim ib, lc As New LinkButton
    '            ' ''ib = e.Row.FindControl("lbAccountname")
    '            ' ''lc = e.Row.FindControl("lbAccountcode")
    '            ' ''Dim i1 As Integer = e.Row.Controls.Count

    '            '' ''Dim btn1 As IButtonControl = TryCast(ctrl, IButtonControl)
    '            ' ''Dim btn As LinkButton = TryCast(ctrl, LinkButton)
    '            '' ''Dim btnc As LinkButton = TryCast(ctrl, LinkButton)

    '            ' ''btn = ib
    '            ' ''If btn IsNot Nothing Then
    '            ' ''    If btn.CommandName = "Selects" Then
    '            ' ''        Dim l_Str_Msg As String = btn.Text & "___" & lblCode.Text
    '            ' ''        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
    '            ' ''        Dim str As String = "javascript:Return('" & l_Str_Msg & "');"
    '            ' ''        btn.OnClientClick = str
    '            ' ''    End If
    '            ' ''End If
    '            ' ''btn = lc
    '            ' ''If btn IsNot Nothing Then
    '            ' ''    If btn.CommandName = "Selectc" Then
    '            ' ''        Dim l_Str_Msg As String = btn.Text & "___" & lblCode.Text
    '            ' ''        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
    '            ' ''        Dim str As String = "javascript:Return('" & l_Str_Msg & "');"
    '            ' ''        btn.OnClientClick = str
    '            ' ''    End If
    '            ' ''End If

    '        Next
    '        'If (e.Row.RowType <> DataControlRowType.Header) Then

    '        '    'e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor; this.style.backgroundColor='Silver';")
    '        '    'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
    '        'End If


    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try
    'End Sub

    ''Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    ''    search()
    ''End Sub

     

    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub



    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        ' set_Menu_Img()
    End Sub



    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        'set_Menu_Img()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lblSGPID As New Label
        Dim lblSGPDescr As New Label
        Dim lbClose As New LinkButton
        Dim lstrAccNum As String

        lbClose = sender

        lblcode = sender.Parent.FindControl("lblCode")
        lblSGPID = sender.Parent.FindControl("lblSGP_ID")
        lblSGPDescr = sender.Parent.FindControl("lblSGP_DESCR")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        ' --- Get The Max Number For That Account
        lstrAccNum = GetNext(lblcode.Text)



        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("window.returnValue = '" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblSGPID.Text & "||" & lblSGPDescr.Text.Replace("'", "\'") & "||" & lstrAccNum & "';")
            Response.Write("window.close();")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub
    Public Function GetNext(ByVal pControlAcc As String) As String
        Dim lstrRetVal As String = String.Empty

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim SqlCmd As New SqlCommand("NextAccountNo", objConn)

            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@pControlAcc", pControlAcc)
            SqlCmd.Parameters.AddWithValue("@pLength", 5)

            ' Dim pRetVal As New SqlParameter("@pReturnVal", SqlDbType.VarChar)
            'pRetVal.Direction = ParameterDirection.Output
            'SqlCmd.Parameters.Add(pRetVal)
            'SqlCmd.ExecuteNonQuery()
            lstrRetVal = SqlCmd.ExecuteScalar()
        Catch ex As Exception
        End Try
        Return lstrRetVal
    End Function


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblGuid As New Label
            Dim url As String
            Dim viewid As String
            Dim mainMnu_code As String = String.Empty
            lblGuid = TryCast(sender.FindControl("lblCode"), Label)
            viewid = lblGuid.Text


            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            mainMnu_code = Request.QueryString("MainMnu_code")

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Accounts\AccEditAccount.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", mainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Request could not be processed "
        End Try
    End Sub

     

    Protected Sub rbActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActive.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbInactive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbInactive.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub gvGroup1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim lblGuid As New Label
        Dim hlView As New HyperLink
        Dim url As String
        Dim viewid As String
        Dim mainMnu_code As String = String.Empty
        lblGuid = TryCast(e.Row.FindControl("lblCode"), Label)
        hlView = TryCast(e.Row.FindControl("hlView"), HyperLink)
        If (Not lblGuid Is Nothing) And (Not hlView Is Nothing) Then
            viewid = Encr_decrData.Encrypt(lblGuid.Text)
            mainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            url = String.Format("~\Accounts\AccEditAccount.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", mainMnu_code, ViewState("datamode"), viewid)
            hlView.NavigateUrl = url
        End If
    End Sub
End Class
