﻿<%@ Page Language="VB" AutoEventWireup="true" EnableEventValidation="true" MasterPageFile="~/mainMasterPage.master" CodeFile="MenuDescriptionUpdate.aspx.vb" Inherits="Accounts_MenuDescriptionUpdate" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Menu Description Tagging
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td colspan="3">
                             <asp:Label ID="lblError" runat="server" CssClass="error"> </asp:Label>
                        </td>                       
                    </tr>
                    <tr>
                        <td width="10%" align="left">
                            <span class="field-label">Select Module</span>
                        </td>
                        <td width="50%" align="right" >
                            <asp:DropDownList ID="ddlModule" runat="server" OnSelectedIndexChanged="ddlModule_SelectedIndexChanged" >
                                 <asp:ListItem Text="Select" Value ="0">                                  
                                </asp:ListItem>
                                <asp:ListItem Text="Student Management" Value ="S0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Curriculum Management" Value ="C0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Co-Curricular Activities" Value ="CC">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Library Management" Value ="LB">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="ACe" Value ="SN">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Health Records" Value ="MD">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Fees Management" Value ="F0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Payroll" Value ="P0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Transport Management" Value ="T0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Financial Accounting" Value ="A0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Purchase" Value ="PI">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="System Administration" Value ="D0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Messaging" Value ="M0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="HR" Value ="H0">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Professional Development" Value ="CD">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Facility & Service Management" Value ="FD">                                   
                                </asp:ListItem>
                                 <asp:ListItem Text="Employee Self Service" Value ="SS">                                   
                                </asp:ListItem>                              
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:Button ID="btnSearch" runat="server" Text="Go" OnClick="btnSearch_Click" CssClass="button" />
                        </td>
                    </tr>
                   
                    <tr id="rowgrid" runat="server" visible="false">
                        <td colspan="3">
                            <asp:GridView ID="grdMenu" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="false" 
                                EmptyDataText="Sorry, No Records Found" AllowPaging="true" OnPageIndexChanging="grdMenu_PageIndexChanging"                                                             
                                >
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10%">
                                         <HeaderStyle HorizontalAlign="center" />
                                        <ItemStyle HorizontalAlign="center" />
                                        <HeaderTemplate >
                                             Menu Code
                                         </HeaderTemplate>
                                        <ItemTemplate >
                                            <asp:HiddenField ID="hidmenucode" Value='<%# Bind("MNU_CODE")%>' runat="server"/>
                                            <asp:label ID="labelcode" Text='<%# Bind("MNU_CODE")%>' runat="server"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="MENU" HeaderText="Menu" ItemStyle-Width="30%">
                                        <HeaderStyle HorizontalAlign="center" />
                                        <ItemStyle HorizontalAlign="center" />
                                    </asp:BoundField>
                                     <asp:TemplateField ItemStyle-Width="50%">
                                         <HeaderTemplate >
                                             Description
                                         </HeaderTemplate>
                                         <ItemTemplate >
                                             <asp:TextBox ID="txtdescr" Text='<%# Bind("DESCR")%>' runat="server"  Enabled="true" />
                                             
                                         </ItemTemplate>
                                          <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                     </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="10%" >
                                        <HeaderTemplate >
                                            Update
                                         </HeaderTemplate>
                                        <ItemTemplate >
                                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CssClass="button" OnClick="btnUpdate_Click" />
                                        </ItemTemplate>
                                         <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>
