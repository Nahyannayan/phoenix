<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="accUnpostVoucher.aspx.vb" Inherits="Accounts_accUnpostVoucher" Title="Unpost Voucher" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkedBox) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            //var lstrChk = document.getElementById("chkAL").checked; 
            // alert(checkState);

            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], '');
            } checkedBox.checked = 'checked';
        }


        function ToggleSearch() {//alert('1')//;style.display
            if (document.getElementById('tr_search').style.display == 'none')
                document.getElementById('tr_search').style.display = ''
            else
                document.getElementById('tr_search').style.display = 'none'
        }
        function SearchHide() {
            document.getElementById('tr_search').style.display = 'none'
        }

      
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Unpost Voucher
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" width="100%">
                    <tr >
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_SelectedId" runat="server" type="hidden" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Voucher Type </span>
                                    </td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddVouchertype" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <img src="../Images/search_button_over.gif" border="0" onclick="ToggleSearch();" align="absMiddle" /></td>
                                    <td colspan="2"></td>
                                </tr>

                                <tr id="tr_search">
                                    <td colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Doc No </span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtDocno" runat="server"></asp:TextBox></td>
                                            
                                                    </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Date</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <br />
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked="True"
                                                        Text="Between" /></td>
                                                <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                                <td align="left" width="30%" contenteditable="true">
                                                    <asp:TextBox ID="txtTodate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="ImageButton1" TargetControlID="txtTodate">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" CausesValidation="False" />
                                                    <img src="../Images/close_red.gif" border="0" onclick="SearchHide();" /></td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" width="100%" colspan="4">
                                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Vouchers for Unposting" Width="100%" AllowPaging="True">
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle BackColor="Aqua" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <Columns>
                                                <asp:BoundField DataField="JHD_DOCNO" HeaderText="Document Number" ReadOnly="True"
                                                    SortExpression="JHD_DOCNO" />
                                                <asp:BoundField DataField="JHD_DOCDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Document Date"
                                                    HtmlEncode="False" SortExpression="JHD_DOCDT">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="JHD_NARRATION" HeaderText="Narration" />
                                                <asp:BoundField DataField="JHD_CUR_ID" HeaderText="Currency" SortExpression="JHD_CUR_ID">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="UnPost" SortExpression="JHD_bPOSTED">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <input id="chkPosted" runat="server" onclick="ChangeAllCheckBoxStates(this)" checked='<%# Bind("JHD_bPOSTED") %>' type="checkbox"
                                                            value='<%# Bind("GUID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GUID" SortExpression="GUID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:RequiredFieldValidator ID="rfvNarration" runat="server" ErrorMessage="Narration Cannot be Empty" ControlToValidate="txtNarration" CssClass="error" ValidationGroup="btnGrp"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Narration</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNarration" runat="server" MaxLength="300"  TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Unpost" ValidationGroup="btnGrp" /></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

