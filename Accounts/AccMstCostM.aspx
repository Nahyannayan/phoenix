<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="AccMstCostM.aspx.vb" Inherits="AccMstCostM" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Cost Elements
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" id="Table1" border="0">
                    <tr>
                        <td align="left">
                            <a href="AccAddCCM.aspx">Add New</a>
                        </td>
                    </tr>
                </table>

                <table width="100%" id="tbl" border="0">
                    <tr>
                        <td>
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        </td>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvGroup1" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="gridheader" CssClass="table table-bordered table-row"
                                Width="100%" DataKeyNames="CHB_ID" EmptyDataText="No Data" AllowPaging="True" PageSize="50">
                                <Columns>
                                    <asp:TemplateField HeaderText="Bank Account" SortExpression="CHB_ID">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Cost Center<br />
                                            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" NavigateUrl='<%# Eval("CHB_ID", "AccEditCCM.aspx?editid={0}") %>'
                                                Text="Edit"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="Edit" ShowEditButton="True" Visible="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete"
                                                OnClientClick='return confirm("Are You Sure?");' Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <HeaderStyle CssClass="gridheader" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle />
                                <PagerStyle HorizontalAlign="Left" />
                            </asp:GridView>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <span id="sp_message" class="error" runat="server"></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

